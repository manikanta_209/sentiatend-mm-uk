package com.sc.stmansi;

import org.junit.Test;

import static org.junit.Assert.*;

import com.sc.stmansi.configuration.AppState;

import java.io.File;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void fileGetPathIsCorrect() {
        File logFile = new File(AppState.logDirRef, "stend_mm_sync_log_" + "10001" + ".txt");
        assertNotNull(logFile.getPath());
    }

    @Test
    public void testFileName() {
        File file = new File("C:\\Users\\Admin\\Desktop\\Fixes.txt");
        String path = file.getPath();
        String filename = path.substring(path.lastIndexOf("\\") + 1);
        assertFalse(filename, filename.isEmpty());
    }
}
