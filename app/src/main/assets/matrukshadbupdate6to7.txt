create Table `temptblAdolVisitHeader` (`autoId`INTEGER PRIMARY KEY AUTOINCREMENT,
`UserId` TEXT,
`AdolId` TEXT,
`VisitId` TEXT,
`adolvisHVisitDate` TEXT,
`adolvisHGTS` TEXT,
`adolvisHEducation` TEXT,
`adolvisHIsPeriodstarted` TEXT,
`adolvisHAgeMenstural` TEXT,
`adolvisHUsedforPeriods` TEXT,
`adolvisHUsedforPeriodsOthers` TEXT,
`adolvisHMenstrualProblem` TEXT,
`adolvisHMenstrualProblemOthers` TEXT,
`adolvisHMaritalStatus` INTEGER,
`adolvisHPartnerName` TEXT,
`adolvisHPartnerOccup` TEXT,
`adolvisHAgeatMarriage` TEXT,
`adolvisHIsPregnantatReg` TEXT,
`adolvisHHb` TEXT,
`adolvisHHbDate`TEXT,
`adolvisHHbCategory` TEXT,
`adolvisHHeightType` TEXT,
`adolvisHHeight` TEXT,
`adolvisHWeight` TEXT,
`adolvisHBMI` TEXT,
`adolvisHIsIFATaken`TEXT,
`adolvisHIFADaily` TEXT,
`adolvisHIFATabletsCount` TEXT,
`adolvisHIFANotTakenReason` TEXT,
`adolvisHIFAGivenBy` INTEGER,
`adolvisHDewormingtab` TEXT ,
`adolvisHHealthIssues` TEXT ,
`adolvisHHealthIssuesOthers` TEXT ,
`adolvisHTreatedAt` TEXT,
`adolvisHGeneralTreatment` TEXT,
`adolvisHGeneralTreatmentOthers` TEXT,
`adolvisHContraceptionAware` TEXT,
`adolvisHContraceptionNeed` TEXT,
`adolvisHContraceptionMethod` TEXT,
`adolvisHContraceptionMethodOthers` TEXT,
`adolvisHComplications` TEXT,
`transId` INTEGER,
`RecordCreatedDate` datetime,
`RecordUpdatedDate` datetime);


INSERT INTO temptblAdolVisitHeader SELECT * from tblAdolVisitHeader;
DROP TABLE tblAdolVisitHeader;

CREATE TABLE "tblAdolVisitHeader" (
`autoId`INTEGER PRIMARY KEY AUTOINCREMENT,
`UserId` TEXT,
`AdolId` TEXT,
`VisitId` TEXT,
`adolvisHVisitDate` TEXT,
`adolvisHGTS` TEXT,
`adolvisHEducation` TEXT,
`adolvisHIsPeriodstarted` TEXT,
`adolvisHAgeMenstural` TEXT,
`adolvisHUsedforPeriods` TEXT,
`adolvisHUsedforPeriodsOthers` TEXT,
`adolvisHMenstrualProblem` TEXT,
`adolvisHMenstrualProblemOthers` TEXT,
`adolvisHMaritalStatus` INTEGER,
`adolvisHPartnerName` TEXT,
`adolvisHPartnerOccup` TEXT,
`adolvisHAgeatMarriage` TEXT,
`adolvisHIsPregnantatReg` TEXT,
`adolvisHHb` TEXT,
`adolvisHHbDate`TEXT,
`adolvisHHbCategory` TEXT,
`adolvisHHeightType` TEXT,
`adolvisHHeight` TEXT,
`adolvisHWeight` TEXT,
`adolvisHBMI` TEXT,
`adolvisHIsIFATaken`TEXT,
`adolvisHIFADaily` TEXT,
`adolvisHIFATabletsCount` TEXT,
`adolvisHIFANotTakenReason` TEXT,
`adolvisHIFAGivenBy` INTEGER,
`adolvisHDewormingtab` TEXT ,
`adolvisHHealthIssues` TEXT ,
`adolvisHHealthIssuesOthers` TEXT ,
`adolvisHTreatedAt` TEXT,
`adolvisHGeneralTreatment` TEXT,
`adolvisHGeneralTreatmentOthers` TEXT,
`adolvisHContraceptionAware` TEXT,
`adolvisHContraceptionNeed` TEXT,
`adolvisHContraceptionMethod` TEXT,
`adolvisHContraceptionMethodOthers` TEXT,
`adolvisHComplications` TEXT,
`transId` INTEGER,
`adolvisHAshaAvailable` TEXT,
`RecordCreatedDate` datetime,
`RecordUpdatedDate` datetime
);

INSERT INTO tblAdolVisitHeader SELECT autoId,
UserId,
AdolId,
VisitId,
adolvisHVisitDate,
adolvisHGTS,
adolvisHEducation,
adolvisHIsPeriodstarted,
adolvisHAgeMenstural,
adolvisHUsedforPeriods,
adolvisHUsedforPeriodsOthers,
adolvisHMenstrualProblem,
adolvisHMenstrualProblemOthers,
adolvisHMaritalStatus,
adolvisHPartnerName,
adolvisHPartnerOccup,
adolvisHAgeatMarriage,
adolvisHIsPregnantatReg,
adolvisHHb,
adolvisHHbDate,
adolvisHHbCategory,
adolvisHHeightType,
adolvisHHeight,
adolvisHWeight,
adolvisHBMI,
adolvisHIsIFATaken,
adolvisHIFADaily,
adolvisHIFATabletsCount,
adolvisHIFANotTakenReason,
adolvisHIFAGivenBy,
adolvisHDewormingtab ,
adolvisHHealthIssues ,
adolvisHHealthIssuesOthers ,
adolvisHTreatedAt,
adolvisHGeneralTreatment,
adolvisHGeneralTreatmentOthers,
adolvisHContraceptionAware,
adolvisHContraceptionNeed,
adolvisHContraceptionMethod,
adolvisHContraceptionMethodOthers,
adolvisHComplications,
transId,
'',
RecordCreatedDate,
RecordUpdatedDate FROM temptblAdolVisitHeader;
DROP TABLE temptblAdolVisitHeader;




CREATE TABLE "tempTblVisitHeader" (
 `AutoId` INTEGER PRIMARY KEY AUTOINCREMENT, `UserId` TEXT, `WomanId` TEXT, `VisitNum` INTEGER, `visHVisitType` TEXT, `visHVisitDate` TEXT, `visHStatusAtVisit` TEXT, `visHVisitIsAdviseGiven` TEXT, `visHVisitParacetamolGiven` TEXT, `visHVisitParacetamolQty` TEXT, `visHVisitParacetamolNotGivenReason` TEXT, `visHVisitIFAGiven` TEXT, `visHVisitIFAQty` TEXT, `visHVisitIFANotGivenReason` TEXT, `visHVisitAlbendazoleGiven` TEXT, `visHVisitAlbendazoleQty` TEXT, `visHVisitAlbendazoleNotGivenReason` TEXT,`visHVisitFSFAGiven` TEXT,`visHVisitFSFAQty` TEXT, `visHVisitFSFANotGivenReason` TEXT,`visHWeight` TEXT, `visHWeightDate` TEXT, `visHPulse` TEXT, `visHPulseDate` TEXT, `visHBP` TEXT, `visHBPDate` TEXT, `visHTemperature` TEXT, `visHTemperatureDate` TEXT, `visHUrineprotein` TEXT, `visHUrineproteinDate` TEXT, `visHHb` TEXT, `visHHbDate` TEXT, `visHVisitIsReferred` TEXT, `visHVisitReferredToFacilityType` TEXT, `visHVisitReferredToFacilityName` TEXT,`visHReferralSlipNumber`	TEXT, `visHVisitComments` TEXT, `visHCompl` TEXT, `visHResult` TEXT, `visHAdvise` TEXT, `visHUserType` TEXT, `visHChildSymptoms` TEXT, `transid` INTEGER, `Recordcreateddate` datetime, `Recordupdateddate` datetime
);

INSERT INTO tempTblVisitHeader SELECT * from TblVisitHeader;
DROP TABLE TblVisitHeader;

CREATE TABLE "TblVisitHeader" (
 `AutoId` INTEGER PRIMARY KEY AUTOINCREMENT, `UserId` TEXT, `WomanId` TEXT, `VisitNum` INTEGER, `visHVisitType` TEXT, `visHVisitDate` TEXT, `visHStatusAtVisit` TEXT, `visHVisitIsAdviseGiven` TEXT, `visHVisitParacetamolGiven` TEXT, `visHVisitParacetamolQty` TEXT, `visHVisitParacetamolNotGivenReason` TEXT, `visHVisitIFAGiven` TEXT, `visHVisitIFAQty` TEXT, `visHVisitIFANotGivenReason` TEXT, `visHVisitAlbendazoleGiven` TEXT, `visHVisitAlbendazoleQty` TEXT, `visHVisitAlbendazoleNotGivenReason` TEXT,`visHVisitFSFAGiven` TEXT,`visHVisitFSFAQty` TEXT, `visHVisitFSFANotGivenReason` TEXT,`visHWeight` TEXT, `visHWeightDate` TEXT, `visHPulse` TEXT, `visHPulseDate` TEXT, `visHBP` TEXT, `visHBPDate` TEXT, `visHTemperature` TEXT, `visHTemperatureDate` TEXT, `visHUrineprotein` TEXT, `visHUrineproteinDate` TEXT, `visHHb` TEXT, `visHHbDate` TEXT, `visHVisitIsReferred` TEXT, `visHVisitReferredToFacilityType` TEXT, `visHVisitReferredToFacilityName` TEXT,`visHReferralSlipNumber`	TEXT, `visHVisitComments` TEXT, `visHCompl` TEXT, `visHResult` TEXT, `visHAdvise` TEXT, `visHUserType` TEXT,visHChildSymptoms TEXT, `transid` INTEGER, `visHAshaAvailable` TEXT,`Recordcreateddate` datetime, `Recordupdateddate` datetime
);

INSERT INTO TblVisitHeader SELECT AutoId, UserId, WomanId, VisitNum, visHVisitType, visHVisitDate, visHStatusAtVisit,visHVisitIsAdviseGiven,visHVisitParacetamolGiven,visHVisitParacetamolQty,visHVisitParacetamolNotGivenReason,visHVisitIFAGiven,visHVisitIFAQty,visHVisitIFANotGivenReason,visHVisitAlbendazoleGiven,visHVisitAlbendazoleQty,visHVisitAlbendazoleNotGivenReason,visHVisitFSFAGiven,visHVisitFSFAQty,visHVisitFSFANotGivenReason,visHWeight,visHWeightDate,visHPulse,visHPulseDate,visHBP,visHBPDate,visHTemperature,visHTemperatureDate,visHUrineprotein,visHUrineproteinDate,visHHb,visHHbDate,visHVisitIsReferred,visHVisitReferredToFacilityType,visHVisitReferredToFacilityName,visHReferralSlipNumber,visHVisitComments,visHCompl,visHResult,visHAdvise,visHUserType,visHChildSymptoms,transid,'',Recordcreateddate,Recordupdateddate
FROM tempTblVisitHeader;
DROP TABLE tempTblVisitHeader;

CREATE TABLE "tblregisteredwomentemp" ( `UserId` varchar, `WomanId` varchar, `regPregnantorMother` integer, `regWomenImage` String,
 `regUIDNo` varchar, `regUIDType` varchar, `regUIDDate` varchar, `regCountNo` integer, `regWomanName` varchar, `regNickName` varchar,
 `regRegistrationType` integer, `regRegistrationDate` varchar, `regFinancialYear` varchar, `regPlaceOfReg` varchar,
 `regpregormotheratreg` integer, `regHusbandName` varchar, `regDateofBirth` varchar, `regAge` TEXT, `regCategory` TEXT,
 `regCaste` varchar, `regAPLBPL` varchar, `regReligion` varchar, `regWhoseMobileNo` varchar, `regPhoneNumber` varchar,
 `regLMPNotKnown` integer, `regLMP` varchar, `regEDD` varchar, `regStatusWhileRegistration` varchar, `regADDate` varchar,
 `regHusbandAge` TEXT, `regHusAgeatMarriage` TEXT, `regWifeAgeatMarriage` TEXT, `regMarriageDate` varchar, `regEducationHus` varchar,
 `regEducationWife` varchar, `regheightUnit` integer, `regHeight` varchar, `regWomanWeight` varchar, `regBloodGroup` integer,
 `regGravida` INTEGER, `regPara` INTEGER, `regLiveChildren` INTEGER, `regAbortions` INTEGER, `regChildMortPreg` INTEGER,
 `regChildMortDel` INTEGER, `regNoofHomeDeliveries` INTEGER, `regLastChildAge` TEXT, `regLastChildGender` varchar, `regLastChildWeight` TEXT,
 `regState` varchar, `regDistrict` varchar, `regSubDistrict` varchar, `regFacility` varchar, `regFacilityType` varchar, `regVillage` integer,
 `regAreaName` varchar, `regAddress` varchar, `regPincode` TEXT, `regGPSLocation` varchar, `regGPSPhoto` varchar, `regNotinRegionReason` varchar,
 `regAadharCard` varchar, `regBankName` varchar, `regBranchName` varchar, `regAccountNo` varchar, `regIFSCCode` varchar, `regRationCard` varchar,
 `regVoterId` varchar, `regCurrHealthRiskFactors` varchar, `regPrevHealthRiskFactors` varchar, `regFamilyHistoryRiskFactors` varchar,
 `regOtherhealthissue` varchar, `regOtherfamilyhistory` varchar, `regOthersprevpreg` varchar, `regAmberOrRedColorCode` integer, `regInDanger`
 integer, `regComplicatedpreg` integer, `regPrematureBirth` integer, `regriskFactors` varchar, `regCHriskfactors` varchar, `regPHriskfactors`
 varchar, `regrecommendedPlaceOfDelivery` varchar, `regComments` varchar, `LastServiceListNumber` integer, `CurrentWomenStatus` varchar,
 `DateTransferred` varchar, `DateExpired` varchar, `DateAborted` varchar, `DateDeactivated` varchar, `ReasonforDeactivation` varchar,
 `OtherDeactivationReason` TEXT, `DeacComments` TEXT, `IsCompl` TEXT, `LastAncVisitDate` TEXT,
 `LastPncVisitDate` TEXT, `IsReferred` TEXT, `regEligibleCoupleNo` TEXT, `transId` integer, `regUserType` varchar,
 `RecordCreatedDate` datetime, `RecordUpdatedDate` datetime, `isLMPConfirmed` integer, `regNoofStillBirth` INTEGER ,"regRiskFactorsByCC"	TEXT,
                                                                                                                    	"regOtherRiskByCC"	TEXT,
                                                                                                                    	"regCCConfirmRisk"	TEXT);
 INSERT INTO tblregisteredwomentemp SELECT * from tblregisteredwomen;
 DROP table tblregisteredwomen;

 CREATE TABLE "tblregisteredwomen" ( `UserId` varchar, `WomanId` varchar, `regPregnantorMother` integer, `regWomenImage` String,
  `regUIDNo` varchar, `regUIDType` varchar, `regUIDDate` varchar, `regCountNo` integer, `regWomanName` varchar, `regNickName` varchar,
  `regRegistrationType` integer, `regRegistrationDate` varchar, `regFinancialYear` varchar, `regPlaceOfReg` varchar,
  `regpregormotheratreg` integer, `regHusbandName` varchar, `regDateofBirth` varchar, `regAge` TEXT, `regCategory` TEXT,
  `regCaste` varchar, `regAPLBPL` varchar, `regReligion` varchar, `regWhoseMobileNo` varchar, `regPhoneNumber` varchar,
  `regLMPNotKnown` integer, `regLMP` varchar, `regEDD` varchar, `regStatusWhileRegistration` varchar, `regADDate` varchar,
  `regHusbandAge` TEXT, `regHusAgeatMarriage` TEXT, `regWifeAgeatMarriage` TEXT, `regMarriageDate` varchar, `regEducationHus` varchar,
  `regEducationWife` varchar, `regheightUnit` integer, `regHeight` varchar, `regWomanWeight` varchar, `regBloodGroup` integer,
  `regGravida` INTEGER, `regPara` INTEGER, `regLiveChildren` INTEGER, `regAbortions` INTEGER, `regChildMortPreg` INTEGER,
  `regChildMortDel` INTEGER, `regNoofHomeDeliveries` INTEGER, `regLastChildAge` TEXT, `regLastChildGender` varchar, `regLastChildWeight` TEXT,
  `regState` varchar, `regDistrict` varchar, `regSubDistrict` varchar, `regFacility` varchar, `regFacilityType` varchar, `regVillage` integer,
  `regAreaName` varchar, `regAddress` varchar, `regPincode` TEXT, `regGPSLocation` varchar, `regGPSPhoto` varchar, `regNotinRegionReason` varchar,
  `regAadharCard` varchar, `regBankName` varchar, `regBranchName` varchar, `regAccountNo` varchar, `regIFSCCode` varchar, `regRationCard` varchar,
  `regVoterId` varchar, `regCurrHealthRiskFactors` varchar, `regPrevHealthRiskFactors` varchar, `regFamilyHistoryRiskFactors` varchar,
  `regOtherhealthissue` varchar, `regOtherfamilyhistory` varchar, `regOthersprevpreg` varchar, `regAmberOrRedColorCode` integer, `regInDanger`
  integer, `regComplicatedpreg` integer, `regPrematureBirth` integer, `regriskFactors` varchar, `regCHriskfactors` varchar, `regPHriskfactors`
  varchar, `regrecommendedPlaceOfDelivery` varchar, `regComments` varchar, `LastServiceListNumber` integer, `CurrentWomenStatus` varchar,
  `DateTransferred` varchar, `DateExpired` varchar, `DateAborted` varchar, `DateDeactivated` varchar, `ReasonforDeactivation` varchar,
  `OtherDeactivationReason` TEXT, `DeacComments` TEXT, `IsCompl` TEXT, `LastAncVisitDate` TEXT,
  `LastPncVisitDate` TEXT, `IsReferred` TEXT, `regEligibleCoupleNo` TEXT, `transId` integer, `regUserType` varchar,
  `RecordCreatedDate` datetime, `RecordUpdatedDate` datetime, `isLMPConfirmed` integer, `regNoofStillBirth` INTEGER ,"regRiskFactorsByCC"	TEXT,
                                                                                                                     	"regOtherRiskByCC"	TEXT,
                                                                                                                     	"regCCConfirmRisk"	TEXT,"regPregnancyCount" integer, "regIsAdolescent" TEXT, "regAdolId" TEXT);

INSERT INTO tblregisteredwomen SELECT  UserId,WomanId,regPregnantorMother,regWomenImage,regUIDNo,regUIDType,regUIDDate,regCountNo,regWomanName,regNickName,regRegistrationType,regRegistrationDate,regFinancialYear,regPlaceOfReg,regpregormotheratreg,regHusbandName,regDateofBirth,regAge,'',regCaste,regAPLBPL,regReligion,regWhoseMobileNo,regPhoneNumber,regLMPNotKnown,regLMP,regEDD,regStatusWhileRegistration,regADDate,regHusbandAge,regHusAgeatMarriage,regWifeAgeatMarriage,regMarriageDate,regEducationHus,regEducationWife,regheightUnit,regHeight,regWomanWeight,regBloodGroup,regGravida,regPara,regLiveChildren,regAbortions,regChildMortPreg,regChildMortDel,regNoofHomeDeliveries,regLastChildAge,regLastChildGender,regLastChildWeight,regState,regDistrict,regSubDistrict,regFacility,regFacilityType,regVillage,regAreaName,regAddress,regPincode,regGPSLocation,regGPSPhoto,regNotinRegionReason,regAadharCard,regBankName,regBranchName,regAccountNo,regIFSCCode,regRationCard,regVoterId,regCurrHealthRiskFactors,regPrevHealthRiskFactors,regFamilyHistoryRiskFactors,regOtherhealthissue,regOtherfamilyhistory,regOthersprevpreg,regAmberOrRedColorCode,regInDanger,regComplicatedpreg,regPrematureBirth,regriskFactors,regCHriskfactors,regPHriskfactors,regrecommendedPlaceOfDelivery,regComments,LastServiceListNumber,CurrentWomenStatus,DateTransferred,DateExpired,DateAborted,DateDeactivated,
                                      ReasonforDeactivation,
                                      OtherDeactivationReason , DeacComments , IsCompl , LastAncVisitDate ,
                                       LastPncVisitDate , IsReferred , regEligibleCoupleNo ,
                                      transId,regUserType,RecordCreatedDate,RecordUpdatedDate,isLMPConfirmed, regNoofStillBirth, regRiskFactorsByCC,regOtherRiskByCC,regCCConfirmRisk,0,'','' from tblregisteredwomentemp;

 DROP table tblregisteredwomentemp;

CREATE TABLE "tblchildinfotemp" ( `UserId` TEXT DEFAULT NULL, `WomanId` TEXT DEFAULT NULL, `chlReg` INTEGER, `chlNo` INTEGER DEFAULT NULL, `chlID` TEXT DEFAULT NULL, `chlParentId` TEXT, `chlRCHID` TEXT DEFAULT NULL, `chlRegDate` TEXT, `chlChildname` TEXT DEFAULT NULL, `chlDateOfBirth` TEXT DEFAULT NULL, `chlTimeOfBirth` TEXT, `chlDeliveryType` TEXT DEFAULT NULL, `chlDelTypeOther` TEXT, `chlDeliveryResult` INTEGER, `chlIUDType` TEXT, `chlDeathDate` TEXT, `chlDelPlace` TEXT, `chlDeliveredBy` TEXT, `chlDeliveredByOther` TEXT, `chlDeliveryConductedByName` TEXT, `chlGender` TEXT DEFAULT NULL, `chlWeight` TEXT DEFAULT NULL, `chlCryAfterBirth` TEXT, `chlBreastFeed` TEXT, `chlComplications` TEXT DEFAULT NULL, `chlOtherComplications` TEXT DEFAULT NULL, `chlTribalHamlet` INTEGER, `chlGoingToSchool` TEXT, `chlPhysicalDisability` TEXT, `chlMentalDisability` TEXT, `chlOtherPhysicalDisability` TEXT, `chlOtherMentalDisability` TEXT, `chlIsAshaAcompany` TEXT, `chlComments` TEXT, `chlDeactDate` TEXT, `chlDeactReason` TEXT, `chlDeactOtherReason` TEXT, `chlDeactComments` TEXT, `chlDeactRelocatedDate` TEXT, `chlDeactMortalityDate` TEXT, `chlUserType` TEXT, `transId` INTEGER NOT NULL DEFAULT NULL, `RecordCreatedDate` datetime DEFAULT NULL, `RecordUpdatedDate` datetime, `chlSkintoskincontact` TEXT);

Insert into tblchildinfotemp select * from tblchildinfo ;

drop table tblchildinfo;

CREATE TABLE "tblchildinfo" ( `UserId` TEXT DEFAULT NULL, `WomanId` TEXT DEFAULT NULL, `chlReg` INTEGER, `chlNo` INTEGER DEFAULT NULL, `chlID` TEXT DEFAULT NULL, `chlParentId` TEXT, `chlRCHID` TEXT DEFAULT NULL, `chlRegDate` TEXT, `chlChildname` TEXT DEFAULT NULL, `chlDateOfBirth` TEXT DEFAULT NULL, `chlTimeOfBirth` TEXT, `chlDeliveryType` TEXT DEFAULT NULL, `chlDelTypeOther` TEXT, `chlDeliveryResult` INTEGER, `chlIUDType` TEXT, `chlDeathDate` TEXT, `chlDelPlace` TEXT, `chlDeliveredBy` TEXT, `chlDeliveredByOther` TEXT, `chlDeliveryConductedByName` TEXT, `chlGender` TEXT DEFAULT NULL, `chlWeight` TEXT DEFAULT NULL, `chlCryAfterBirth` TEXT, `chlBreastFeed` TEXT, `chlComplications` TEXT DEFAULT NULL, `chlOtherComplications` TEXT DEFAULT NULL, `chlTribalHamlet` INTEGER, `chlGoingToSchool` TEXT, `chlPhysicalDisability` TEXT, `chlMentalDisability` TEXT, `chlOtherPhysicalDisability` TEXT, `chlOtherMentalDisability` TEXT, `chlIsAshaAcompany` TEXT, `chlComments` TEXT, `chlDeactDate` TEXT, `chlDeactReason` TEXT, `chlDeactOtherReason` TEXT, `chlDeactComments` TEXT, `chlDeactRelocatedDate` TEXT, `chlDeactMortalityDate` TEXT, `chlUserType` TEXT, `transId` INTEGER NOT NULL DEFAULT NULL, `RecordCreatedDate` datetime DEFAULT NULL, `RecordUpdatedDate` datetime, `chlSkintoskincontact` TEXT, `childtoadolID` TEXT);

Insert into tblchildinfo select UserId, WomanId,chlReg, chlNo, chlID, chlParentId , chlRCHID, chlRegDate , chlChildname , chlDateOfBirth, chlTimeOfBirth, chlDeliveryType, chlDelTypeOther, chlDeliveryResult, chlIUDType, chlDeathDate, chlDelPlace, chlDeliveredBy, chlDeliveredByOther, chlDeliveryConductedByName, chlGender, chlWeight, chlCryAfterBirth, chlBreastFeed, chlComplications, chlOtherComplications, chlTribalHamlet, chlGoingToSchool , chlPhysicalDisability , chlMentalDisability , chlOtherPhysicalDisability , chlOtherMentalDisability , chlIsAshaAcompany , chlComments , chlDeactDate , chlDeactReason , chlDeactOtherReason , chlDeactComments , chlDeactRelocatedDate , chlDeactMortalityDate , chlUserType ,transId  , RecordCreatedDate , RecordUpdatedDate , chlSkintoskincontact , "" from tblchildinfotemp;

drop table tblchildinfotemp;


CREATE TABLE tblVisitChildHeadertemp ( `AutoId` INTEGER PRIMARY KEY AUTOINCREMENT, `UserId` TEXT, `WomanId` TEXT, `ChildId` TEXT,`ChildNo` Integer,`VisitNum` INTEGER,`visHVisitDate` TEXT, `visCHWeight` TEXT, `visCHWeightDate` TEXT, `visCHTemperature` TEXT, `visCHTemperatureDate` TEXT, `visCHResprate` TEXT, `visCHResprateDate` TEXT, `visHVisitIsReferred` TEXT, `visHVisitReferredToFacilityType` TEXT, `visHVisitReferredToFacilityName` TEXT,`visHReferralSlipNumber`	TEXT, `visHVisitComments` TEXT, `visCHCompl` TEXT, visHChildSymptoms TEXT,`visCHAdvise` TEXT,`visCHRegAtGovtFac` int, `visCHRegAtGovtFacDate` TEXT, `visCHRegGovtFacId` TEXT, `visHUserType` TEXT, `transid` INTEGER, `Recordcreateddate` datetime, `Recordupdateddate` datetime);
insert into tblVisitChildHeadertemp select * from tblVisitChildHeader;
drop table tblVisitChildHeader;
CREATE TABLE tblVisitChildHeader ( `AutoId` INTEGER PRIMARY KEY AUTOINCREMENT, `UserId` TEXT, `WomanId` TEXT, `ChildId` TEXT,`ChildNo` Integer,`VisitNum` INTEGER,`visHVisitDate` TEXT, `visCHWeight` TEXT, `visCHWeightDate` TEXT, `visCHTemperature` TEXT, `visCHTemperatureDate` TEXT, `visCHResprate` TEXT, `visCHResprateDate` TEXT, `visHVisitIsReferred` TEXT, `visHVisitReferredToFacilityType` TEXT, `visHVisitReferredToFacilityName` TEXT,`visHReferralSlipNumber`	TEXT, `visHVisitComments` TEXT, `visCHCompl` TEXT, `visHChildSymptoms` TEXT,`visCHAdvise` TEXT,`visCHRegAtGovtFac` int, `visCHRegAtGovtFacDate` TEXT, `visCHRegGovtFacId` TEXT, `visHUserType` TEXT, `transid` INTEGER,`visCHAshaAvailable` TEXT, `Recordcreateddate` datetime, `Recordupdateddate` datetime ,`childvisHIsIFATaken` TEXT,`childvisHIFADaily` TEXT, `childvisHIFATabletsCount` TEXT, `childvisHIFANotTakenReason` TEXT,`childvisHIFAGivenBy` Integer, `childvisHDewormingtab` TEXT);
insert into  tblVisitChildHeader select AutoId,UserId,WomanId,ChildId,ChildNo,VisitNum,visHVisitDate,visCHWeight,visCHWeightDate,visCHTemperature,visCHTemperatureDate,visCHResprate,visCHResprateDate,visHVisitIsReferred,visHVisitReferredToFacilityType,visHVisitReferredToFacilityName,visHReferralSlipNumber,visHVisitComments,visCHCompl, visHChildSymptoms,visCHAdvise,visCHRegAtGovtFac,visCHRegAtGovtFacDate, visCHRegGovtFacId ,visHUserType,transid,"",Recordcreateddate,Recordupdateddate,"","","","","","" from tblVisitChildHeadertemp;
drop table tblVisitChildHeadertemp;

CREATE TABLE `tblCaseMgmttemp`(
     `autoId` INTEGER ,
          `UserId` TEXT,
          `MMUserId` TEXT,
          `BeneficiaryType` TEXT,
          `ChlNo` TEXT,
          `BeneficiaryID` TEXT,
          `BeneficiaryParentID` TEXT,
          `VisitMode` TEXT,
          `VisitType` TEXT,
          `VisitNum` INTEGER,
          `VisitDate` TEXT,
          `StatusAtVisit` TEXT,
          `VisitNumbyMM` INTEGER,
          `VisitDatebyMM` TEXT,
          `VisitBeneficiaryDangerSigns` TEXT,
          `hcmBeneficiaryVisitFac` TEXT,
          `hcmReasonForNoVisit` TEXT,
          `hcmReasonOthers` TEXT,
          `hcmActTakByUserType` TEXT,
          `hcmActTakByUserName` TEXT,
          `hcmActTakAtFacility` TEXT,
          `hcmActTakFacilityName` TEXT,
          `hcmActTakDate` TEXT,
          `hcmActTakTime` TEXT,
          `hcmActTakForCompl` TEXT,
          `hcmActMedicationsPres` TEXT,
          `hcmActTakStatus` TEXT,
          `hcmActTakStatusDate` TEXT,
          `hcmActTakStatusTime` TEXT,
          `hcmActTakReferredToFacility` TEXT,
          `hcmActTakReferredToFacilityName` TEXT,
          `hcmActTakComments` TEXT,
          `hcmAdviseGiven` TEXT,
          `hcmBeneficiaryCondatVisit` TEXT,
          `hcmActionToBeTakenClient` TEXT,
          `hcmInputToNextLevel` TEXT,
          `hcmIsANMInformedAbtCompl` TEXT,
          `hcmANMAwareOfRef` TEXT,
          `VisitReferredtoFacType` TEXT,
          `VisitReferredtoFacName` TEXT,
          `VisitReferralSlipNumber` TEXT,
          `hcmCreatedByUserType` TEXT,
          `RecordViewedDate` TEXT,
          `transId` INTEGER,
          `RecordCreatedDate` datetime,
          `RecordUpdatedDate` datetime,
          `ServerCreatedDate` datetime,
          `ServerUpdatedDate` datetime
);
insert into tblCaseMgmttemp select * from tblCaseMgmt;
drop table tblCaseMgmt;
CREATE TABLE `tblCaseMgmt`(
     `autoId` INTEGER,
     `UserId` TEXT,
     `MMUserId` TEXT,
     `BeneficiaryType` TEXT,
     `ChlNo` TEXT,
     `BeneficiaryID` TEXT,
     `BeneficiaryParentID` TEXT,
     `VisitMode` TEXT,
     `VisitType` TEXT,
     `VisitNum` INTEGER,
     `VisitDate` TEXT,
     `StatusAtVisit` TEXT,
     `VisitNumbyMM` INTEGER,
     `VisitDatebyMM` TEXT,
     `VisitBeneficiaryDangerSigns` TEXT,
     `hcmBeneficiaryVisitFac` TEXT,
     `hcmReasonForNoVisit` TEXT,
     `hcmReasonOthers` TEXT,
     `hcmActTakByUserType` TEXT,
     `hcmActTakByUserName` TEXT,
     `hcmActTakAtFacility` TEXT,
     `hcmActTakFacilityName` TEXT,
     `hcmActTakDate` TEXT,
     `hcmActTakTime` TEXT,
     `hcmActTakForCompl` TEXT,
     `hcmActMedicationsPres` TEXT,
     `hcmActTakStatus` TEXT,
     `hcmActTakStatusDate` TEXT,
     `hcmActTakStatusTime` TEXT,
     `hcmActTakReferredToFacility` TEXT,
     `hcmActTakReferredToFacilityName` TEXT,
     `hcmActTakComments` TEXT,
     `hcmAdviseGiven` TEXT,
     `hcmBeneficiaryCondatVisit` TEXT,
     `hcmActionToBeTakenClient` TEXT,
     `hcmInputToNextLevel` TEXT,
     `hcmIsANMInformedAbtCompl` TEXT,
     `hcmANMAwareOfRef` TEXT,
     `VisitReferredtoFacType` TEXT,
     `VisitReferredtoFacName` TEXT,
     `VisitReferralSlipNumber` TEXT,
     `hcmCreatedByUserType` TEXT,
    `RecordViewedDate` TEXT,
     `transId` INTEGER,
      `RecordCreatedDate` datetime,
       `RecordUpdatedDate` datetime,
       `ServerCreatedDate` datetime,
        `ServerUpdatedDate` datetime
);
insert into tblCaseMgmt select * from tblCaseMgmttemp;
drop table tblCaseMgmttemp;

