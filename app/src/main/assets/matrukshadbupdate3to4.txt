CREATE TABLE "tblAncTests" (
	UserId varchar, WomenId varchar,	ServiceNumber int,
	TestId int,TestDone varchar,TestValue varchar,
	transId int, RecordCreatedDate varchar, RecordUpdatedDate varchar);
	CREATE TABLE "tblAncTestsMaster" ( `TestId` INTEGER, `TestName` TEXT );

INSERT INTO tblAncTestsMaster VALUES (1, 'BP Test');
INSERT INTO tblAncTestsMaster VALUES (2, 'Breathing Test');
INSERT INTO tblAncTestsMaster VALUES (3, 'Jaundice Test');
INSERT INTO tblAncTestsMaster VALUES (4, 'Swelling Test');
INSERT INTO tblAncTestsMaster VALUES (5, 'HB Test');
INSERT INTO tblAncTestsMaster VALUES (6, 'Blood Group Test');
INSERT INTO tblAncTestsMaster VALUES (7, 'Urine Test');
INSERT INTO tblAncTestsMaster VALUES (8, 'Malaria Test');
INSERT INTO tblAncTestsMaster VALUES (9, 'VDRL Test');
INSERT INTO tblAncTestsMaster VALUES (10, 'HIV Test');
INSERT INTO tblAncTestsMaster VALUES (11, 'Sugar Test');
INSERT INTO tblAncTestsMaster VALUES (12, 'Baby Growth Scan');
INSERT INTO tblAncTestsMaster VALUES (13, 'Breast &amp; Heart beat Checkup');
INSERT INTO tblAncTestsMaster VALUES (14, 'Height');
INSERT INTO tblAncTestsMaster VALUES (15, 'Weight');
INSERT INTO tblAncTestsMaster VALUES (16, 'Pregnancy Test');

CREATE TABLE tblchildgrowth ( `AutoId` INTEGER PRIMARY KEY AUTOINCREMENT, `chlId` TEXT, `chlGMVisitId` INTEGER, `chlGMAge` TEXT, `chlGMAgeInMonths` INTEGER, `chlGMVisitdate` TEXT, `chlGMHeight` TEXT, `chlGMWeight` TEXT, `chlGMHeadCircum` TEXT, `chlGMBMI` TEXT,`chlGMMuac` TEXT,`chlGMTriceps` TEXT,`chlGMVisitNotes` TEXT,`chlGMRecommendations` TEXT,`chlGMObservations` TEXT,`chlGMSuggestions` TEXT,`transId` INTEGER,`chlGMRecordCreatedDate` datetime,`chlGMRecordUpdatedDate` datetime,`chlGMComplication` TEXT,`chlGMBreastfeeding` TEXT);

CREATE TABLE tblweightforage ( `Xaxis` INTEGER, `cgmBoysWtMedian` TEXT, `cgmBoysWtFirstdNegSD` TEXT, `cgmBoysWtSeconddNegSD` TEXT, `cgmGirlsWtMedian` TEXT, `cgmGirlsWtFirstdNegSD` TEXT, `cgmGirlsWtSeconddNegSD` TEXT);
 INSERT INTO tblweightforage values ('0','4.2','3.1','2.6','4','2.9','2.4');
 INSERT INTO tblweightforage values ('1','4.5','3.3','2.8','4.2','3.1','2.6');
 INSERT INTO tblweightforage values ('2','5.6','4.2','3.7','5.1','3.8','3.3');
 INSERT INTO tblweightforage values ('3','6.4','4.9','4.3','5.8','4.4','3.9');
 INSERT INTO tblweightforage values ('4','7','5.5','4.8','6.4','4.9','4.3');
 INSERT INTO tblweightforage values ('5','7.5','5.9','5.2','6.9','5.3','4.7');
 INSERT INTO tblweightforage values ('6','7.9','6.3','5.6','7.3','5.6','5');
 INSERT INTO tblweightforage values ('7','8.3','6.6','5.8','7.6','5.9','5.2');
 INSERT INTO tblweightforage values ('8','8.6','6.8','6.1','7.9','6.2','5.5');
 INSERT INTO tblweightforage values ('9','8.9','7','6.3','8.2','6.4','5.7');
 INSERT INTO tblweightforage values ('10','9.2','7.3','6.5','8.5','6.6','5.8');
 INSERT INTO tblweightforage values ('11','9.4','7.5','6.7','8.7','6.8','6');
 INSERT INTO tblweightforage values ('12','9.6','7.6','6.8','8.9','6.9','6.2');
 INSERT INTO tblweightforage values ('13','9.9','7.8','7','9.2','7.1','6.3');
 INSERT INTO tblweightforage values ('14','10.1','8','7.1','9.4','7.3','6.5');
 INSERT INTO tblweightforage values ('15','10.3','8.2','7.3','9.6','7.5','6.6');
 INSERT INTO tblweightforage values ('16','10.5','8.3','7.4','9.8','7.6','6.8');
 INSERT INTO tblweightforage values ('17','10.7','8.5','7.6','10','7.8','6.9');
 INSERT INTO tblweightforage values ('18','10.9','8.7','7.7','10.2','8','7.1');
 INSERT INTO tblweightforage values ('19','11.1','8.8','7.9','10.4','8.1','7.2');
 INSERT INTO tblweightforage values ('20','11.3','9','8','10.6','8.3','7.4');
 INSERT INTO tblweightforage values ('21','11.5','9.1','8.1','10.9','8.5','7.5');
 INSERT INTO tblweightforage values ('22','11.8','9.3','8.3','11.1','8.6','7.7');
 INSERT INTO tblweightforage values ('23','12','9.4','8.4','11.3','8.8','7.8');
 INSERT INTO tblweightforage values ('24','12.2','9.6','8.5','11.5','8.9','8');
 INSERT INTO tblweightforage values ('25','12.4','9.7','8.7','11.7','9.1','8.1');
 INSERT INTO tblweightforage values ('26','12.5','9.9','8.8','11.9','9.3','8.3');
 INSERT INTO tblweightforage values ('27','12.7','10','8.9','12.1','9.4','8.4');
 INSERT INTO tblweightforage values ('28','12.9','10.1','9','12.3','9.6','8.5');
 INSERT INTO tblweightforage values ('29','13.1','10.3','9.1','12.5','9.7','8.7');
 INSERT INTO tblweightforage values ('30','13.3','10.4','9.3','12.7','9.9','8.8');
 INSERT INTO tblweightforage values ('31','13.5','10.6','9.4','12.9','10','8.9');
 INSERT INTO tblweightforage values ('32','13.7','10.7','9.5','13.1','10.2','9');
 INSERT INTO tblweightforage values ('33','13.8','10.8','9.6','13.3','10.3','9.2');
 INSERT INTO tblweightforage values ('34','14','10.9','9.7','13.5','10.4','9.3');
 INSERT INTO tblweightforage values ('35','14.2','11.1','9.8','13.7','10.6','9.4');
 INSERT INTO tblweightforage values ('36','14.3','11.2','9.9','13.9','10.7','9.5');
 INSERT INTO tblweightforage values ('37','14.5','11.3','10','14','10.8','9.6');
 INSERT INTO tblweightforage values ('38','14.7','11.4','10.1','14.2','11','9.7');
 INSERT INTO tblweightforage values ('39','14.8','11.5','10.2','14.4','11.1','9.8');
 INSERT INTO tblweightforage values ('40','15','11.7','10.3','14.6','11.2','10');
 INSERT INTO tblweightforage values ('41','15.2','11.8','10.4','14.8','11.4','10.1');
 INSERT INTO tblweightforage values ('42','15.3','11.9','10.5','15','11.5','10.2');
 INSERT INTO tblweightforage values ('43','15.5','12','10.6','15.2','11.6','10.3');
 INSERT INTO tblweightforage values ('44','15.7','12.1','10.7','15.3','11.7','10.4');
 INSERT INTO tblweightforage values ('45','15.8','12.3','10.8','15.5','11.9','10.5');
 INSERT INTO tblweightforage values ('46','16','12.4','10.9','15.7','12','10.6');
 INSERT INTO tblweightforage values ('47','16.2','12.5','11','15.9','12.1','10.7');
 INSERT INTO tblweightforage values ('48','16.3','12.6','11.1','16.1','12.2','10.8');
 INSERT INTO tblweightforage values ('49','16.5','12.7','11.2','16.3','12.3','10.9');
 INSERT INTO tblweightforage values ('50','16.7','12.8','11.3','16.4','12.5','11');
 INSERT INTO tblweightforage values ('51','16.8','13','11.4','16.6','12.6','11.1');
 INSERT INTO tblweightforage values ('52','17','13.1','11.5','16.8','12.7','11.2');
 INSERT INTO tblweightforage values ('53','17.2','13.2','11.6','17','12.8','11.3');
 INSERT INTO tblweightforage values ('54','17.3','13.3','11.7','17.2','12.9','11.4');
 INSERT INTO tblweightforage values ('55','17.5','13.4','11.8','17.3','13.1','11.5');
 INSERT INTO tblweightforage values ('56','17.7','13.5','11.9','17.5','13.2','11.6');
 INSERT INTO tblweightforage values ('57','17.8','13.6','12','17.7','13.3','11.7');
 INSERT INTO tblweightforage values ('58','18','13.7','12.1','17.9','13.4','11.8');
 INSERT INTO tblweightforage values ('59','18.2','13.9','12.2','18','13.5','11.9');
 INSERT INTO tblweightforage values ('60','18.3','14','12.3','18.2','13.6','12');



CREATE TABLE tblheightforage ( `Xaxis` INTEGER, `cgmBoysHtMedian` TEXT, `cgmBoysHtFirstdNegSD` TEXT, `cgmBoysHtSeconddNegSD` TEXT, `cgmGirlsHtMedian` TEXT, `cgmGirlsHtFirstdNegSD` TEXT, `cgmGirlsHtSeconddNegSD` TEXT);
  INSERT INTO tblheightforage values ('0','49.9','46','44.1','49.1','45.3','43.5');
  INSERT INTO tblheightforage values ('1','54.7','50.7','48.8','53.7','49.7','47.7');
  INSERT INTO tblheightforage values ('2','58.4','54.3','52.3','57.1','52.9','49.9');
  INSERT INTO tblheightforage values ('3','61.4','57.2','55.2','59.8','55.5','53.4');
  INSERT INTO tblheightforage values ('4','63.9','59.6','57.5','62.1','57.7','55.5');
  INSERT INTO tblheightforage values ('5','65.9','61.1','59.5','64','59.5','57.3');
  INSERT INTO tblheightforage values ('6','67.6','63.2','61.1','65.7','61.3','58.8');
  INSERT INTO tblheightforage values ('7','69.2','64.7','62.6','67.3','62.6','60.2');
  INSERT INTO tblheightforage values ('8','70.6','66.1','63.9','68.7','63.9','61.6');
  INSERT INTO tblheightforage values ('9','72','67.4','65.1','70.1','65.2','62.8');
  INSERT INTO tblheightforage values ('10','73.3','68.6','66.3','71.5','66.4','64');
  INSERT INTO tblheightforage values ('11','74.5','69.8','67.3','72.8','67.6','65.1');
  INSERT INTO tblheightforage values ('12','75.7','70.9','68.5','74','68.8','66.2');
  INSERT INTO tblheightforage values ('13','76.9','72','69.5','75.2','69.9','67.2');
  INSERT INTO tblheightforage values ('14','78','73','70.5','76.4','70.9','68.2');
  INSERT INTO tblheightforage values ('15','79.1','74','71.5','77.5','71.9','69.2');
  INSERT INTO tblheightforage values ('16','80.2','74.9','72.4','78.6','72.9','70.1');
  INSERT INTO tblheightforage values ('17','81.1','75.9','73.2','79.7','73.9','71');
  INSERT INTO tblheightforage values ('18','82.3','76.8','74.1','80.7','74.8','71.9');
  INSERT INTO tblheightforage values ('19','83.3','77.6','74.9','81.7','75.7','72.7');
  INSERT INTO tblheightforage values ('20','84.2','78.5','75.7','82.7','76.6','73.6');
  INSERT INTO tblheightforage values ('21','85.1','79.3','76.4','83.7','77.4','74.4');
  INSERT INTO tblheightforage values ('22','86','80.1','77.1','84.6','78.3','75.1');
  INSERT INTO tblheightforage values ('23','86.9','80.9','77.9','85.5','79.1','75.9');
  INSERT INTO tblheightforage values ('24','87.1','80.9','77.9','85.6','79.2','75.9');
  INSERT INTO tblheightforage values ('25','88','81.6','78.5','85.7','79.9','76.7');
  INSERT INTO tblheightforage values ('26','88.8','82.4','79.2','87.4','80.7','77.4');
  INSERT INTO tblheightforage values ('27','89.6','83','79.8','88.3','81.4','78');
  INSERT INTO tblheightforage values ('28','90.4','83.7','80.4','89.1','82.1','78.7');
  INSERT INTO tblheightforage values ('29','91.2','84.4','81','89.9','82.8','79.4');
  INSERT INTO tblheightforage values ('30','91.9','85','81.6','90.7','83.5','80');
  INSERT INTO tblheightforage values ('31','92.7','85.6','82.2','91.4','84.2','80.6');
  INSERT INTO tblheightforage values ('32','93.4','86.3','82.7','92.2','84.8','81.2');
  INSERT INTO tblheightforage values ('33','94.1','86.8','83.3','92.9','85.5','81.8');
  INSERT INTO tblheightforage values ('34','94.8','87.4','83.8','93.6','86.1','82.4');
  INSERT INTO tblheightforage values ('35','95.4','88','84.3','94.4','86.7','83');
  INSERT INTO tblheightforage values ('36','96.1','88.6','84.9','95.1','87.3','83.5');
  INSERT INTO tblheightforage values ('37','96.7','89.1','85.4','95.7','87.9','84.1');
  INSERT INTO tblheightforage values ('38','97.4','89.7','85.9','96.4','88.5','84.6');
  INSERT INTO tblheightforage values ('39','98','90.2','86.4','97.1','89.1','85.2');
  INSERT INTO tblheightforage values ('40','98.6','90.8','86.9','97.7','89.7','85.7');
  INSERT INTO tblheightforage values ('41','99.2','91.3','87.4','98.4','90.3','86.2');
  INSERT INTO tblheightforage values ('42','99.9','91.8','87.9','99','90.8','86.7');
  INSERT INTO tblheightforage values ('43','100.4','92.3','88.3','99.7','91.4','87.3');
  INSERT INTO tblheightforage values ('44','101','92.9','88.8','100.3','91.9','87.8');
  INSERT INTO tblheightforage values ('45','101.6','93.4','89.3','100.9','92.4','88.3');
  INSERT INTO tblheightforage values ('46','102.2','93.9','89.7','101.5','93','88.8');
  INSERT INTO tblheightforage values ('47','102.8','94.3','90.2','102.1','93.5','89.2');
  INSERT INTO tblheightforage values ('48','103.3','94.8','90.6','102.7','94','89.7');
  INSERT INTO tblheightforage values ('49','103.9','95.3','91.1','103.3','94.5','90.2');
  INSERT INTO tblheightforage values ('50','104.4','95.8','91.5','103.9','95','90.2');
  INSERT INTO tblheightforage values ('51','105','96.3','92','104.5','95.5','91.1');
  INSERT INTO tblheightforage values ('52','105.6','96.8','92.4','105','96','91.6');
  INSERT INTO tblheightforage values ('53','106.1','97.3','92.9','105.6','96.5','92');
  INSERT INTO tblheightforage values ('54','106.7','97.7','93.3','106.2','97','92.5');
  INSERT INTO tblheightforage values ('55','107.2','98.2','93.8','106.7','97.5','92.9');
  INSERT INTO tblheightforage values ('56','107.8','98.7','94.2','107.3','98','93.3');
  INSERT INTO tblheightforage values ('57','108.3','99.2','94.6','107.8','98.4','93.8');
  INSERT INTO tblheightforage values ('58','108.9','99.6','95.1','108.4','98.9','94.2');
  INSERT INTO tblheightforage values ('59','109.4','100.1','95.5','108.9','99.4','94.6');
  INSERT INTO tblheightforage values ('60','110','100.6','96','109.4','99.8','95.1');

CREATE TABLE tblbmiforage ( `Xaxis` INTEGER, `cgmBoysBMIMedian` TEXT,`cgmBoysBMIFirstdNegSD` TEXT, `cgmBoysBMISeconddNegSD` TEXT,`cgmGirlsBMIMedian` TEXT,`cgmGirlsBMIFirstdNegSD` TEXT, `cgmGirlsBMISeconddNegSD` TEXT);

CREATE TABLE tblmuacforage ( `Xaxis` INTEGER, `cgmBoysHeadcfMedian` TEXT, `cgmBoysHeadcfFirstdNegSD` TEXT, `cgmBoysHeadcfSeconddNegSD` TEXT, `cgmGirlsHeadcfMedian` TEXT, `cgmGirlsHeadcfFirstdNegSD` TEXT, `cgmGirlsHeadcfSeconddNegSD` TEXT);

Insert into tblsymptomsmaster values('95','24','PNC','Child','326','Sunken fontanelle','nbsunkenfontnelle');
Insert into tblsymptomsmaster values('96','24','PNC','Child','327','Vomiting','nbvomiting');
Insert into tblsymptomsmaster values('97','24','PNC','Child','328','Sepsis','nbsepsis');
Insert into tblsymptomsmaster values('98','24','PNC','Child','329','Pneumonia','nbpneumonia');
update tblsymptomsmaster set symXmlStringName='nbfeelswarmtotouch' where SlNo=93;
update tblsymptomsmaster set symXmlStringName='nbfeelstoowarmtouch' where SlNo=94;


CREATE TABLE `tblAdolReg` (`userId` VARCHAR,
`AdolID` VARCHAR,
 `regAdolName` VARCHAR,
  `regAdolAge` VARCHAR,
  `regAdolGender` VARCHAR,
  `regAdolDoB` VARCHAR,
  `regAdolWeight` VARCHAR,
  `regAdolHeightType` INTEGER,
  `regAdolHeight` VARCHAR,
  `regAdolMobile` VARCHAR ,
  `regAdolMobileOf` INTEGER ,
  `regAdolFather` VARCHAR ,
  `regAdolFatherDesc` VARCHAR ,
  `regAdolMother` VARCHAR,
  `regAdolMotherDesc` VARCHAR,
  `regAdolGuardian` VARCHAR,
  `regAdolFamilytype` INTEGER,
  `regAdolPregnant` VARCHAR,
  `regAdolAddress` VARCHAR,
  `regAdolAreaname` VARCHAR,
  `regAdolFacilities` INTEGER,
  `regAdolPincode` VARCHAR,
  `regAdolRelationship` INTEGER,
  `regAdolPartnerName` VARCHAR ,
  `regAdolPartnerOccupa` VARCHAR ,
  `regAdolMarriageType` INTEGER ,
  `regAdolGoingtoSchool` VARCHAR,
   `regAdolNoofChild` INTEGER,
  `regAdolHealthIssues` VARCHAR,
  `regAdolOtherHealthIssues` VARCHAR,
  `regAdolLifeStyle` VARCHAR,
  `regAdolMenstrualPbm` VARCHAR,
  `regAdolOtherMenspbm` VARCHAR,
  `regAdolPhysicalActivity` VARCHAR,
  `regAdolSuggestionGiven` VARCHAR,
  `regAdolComments` VARCHAR,
  `LastVisitCount` Integer,
  `regAdolAadharNo` VARCHAR,
  `regAdolisPeriod` VARCHAR,
  `regAdolAgeatMenstruated` VARCHAR,
  `regAdolEducation` INTEGER,
  `regUserType` VARCHAR,
  `transId` INTEGER,
  `RecordCreatedDate` datetime,
  `RecordUpdatedDate` datetime,
  `regAdolDeactivateDate` VARCHAR,
  `regAdolDeactivateReasons` VARCHAR,
  `regAdolDeactivateReasonOthers` VARCHAR,
  `regAdolDeactivateComments` VARCHAR,
  regAdolImage VARCHAR
  );
create TABLE `tblCampDetails` (`userId` VARCHAR,`tblCampID` VARCHAR, `tblCampName` VARCHAR, `tblCampDateTime` VARCHAR, `tblCampDesc` VARCHAR,`tblCampPresent` VARCHAR,`tblCampAbsent` VARCHAR, `tblCampSchemaType` INTEGER,`tblCampFacilities` VARCHAR, `tblCampCreatedBy` VARCHAR,`tblCampOutcome` VARCHAR,`tblCampisActive` INTEGER, `tblCampSummary` VARCHAR, `tblCampEndTime` VARCHAR,`tblCampCCAccompany` VARCHAR, `tblCampProObjective` VARCHAR, `tblCampProObjOthers` VARCHAR, `transId` INTEGER,`RecordCreatedDate` VARCHAR);
create TABLE `tblAdolAttendanceSavings` (`userID` VARCHAR, `AdolID` VARCHAR, `CampID` VARCHAR, `tblAttSavAttendance`VARCHAR, `transId` INTEGER , `DateCreated` VARCHAR, `DateUpdated` VARCHAR);
create Table `tblAdolVisitHeader` (`autoId`INTEGER PRIMARY KEY AUTOINCREMENT,
`UserId` TEXT,
`AdolId` TEXT,
`VisitId` TEXT,
`adolvisHVisitDate` TEXT,
`adolvisHGTS` TEXT,
`adolvisHEducation` TEXT,
`adolvisHIsPeriodstarted` TEXT,
`adolvisHAgeMenstural` TEXT,
`adolvisHUsedforPeriods` TEXT,
`adolvisHUsedforPeriodsOthers` TEXT,
`adolvisHMenstrualProblem` TEXT,
`adolvisHMenstrualProblemOthers` TEXT,
`adolvisHMaritalStatus` INTEGER,
`adolvisHPartnerName` TEXT,
`adolvisHPartnerOccup` TEXT,
`adolvisHAgeatMarriage` TEXT,
`adolvisHIsPregnantatReg` TEXT,
`adolvisHHb` TEXT,
`adolvisHHbDate`TEXT,
`adolvisHHbCategory` TEXT,
`adolvisHHeightType` TEXT,
`adolvisHHeight` TEXT,
`adolvisHWeight` TEXT,
`adolvisHBMI` TEXT,
`adolvisHIsIFATaken`TEXT,
`adolvisHIFADaily` TEXT,
`adolvisHIFATabletsCount` TEXT,
`adolvisHIFANotTakenReason` TEXT,
`adolvisHIFAGivenBy` INTEGER,
`adolvisHDewormingtab` TEXT ,
`adolvisHHealthIssues` TEXT ,
`adolvisHHealthIssuesOthers` TEXT ,
`adolvisHTreatedAt` TEXT,
`adolvisHGeneralTreatment` TEXT,
`adolvisHGeneralTreatmentOthers` TEXT,
`adolvisHContraceptionAware` TEXT,
`adolvisHContraceptionNeed` TEXT,
`adolvisHContraceptionMethod` TEXT,
`adolvisHContraceptionMethodOthers` TEXT,
`adolvisHComplications` TEXT,
`transId` INTEGER,
`RecordCreatedDate` datetime,
`RecordUpdatedDate` datetime);

create Table `tblAdolVisitDetails` (`autoId`INTEGER PRIMARY KEY AUTOINCREMENT,`UserId` TEXT,`AdolId` TEXT, `VisitId` TEXT, `adolvisDVisitDate` TEXT, `adolvisDFollowUps` TEXT, `transId` INTEGER, `RecordCreatedDate` datetime,`RecordUpdatedDate` datetime);
create Table `tblAdolFollowUpMaster` (`SlNo` Integer,`followupGrpID` Integer , `followupGirlorBoy` TEXT,`followupID` INTEGER, `followupName` TEXT, `followupXmlStringName` TEXT);
Insert into tblAdolFollowUpMaster values('1','1','Girl','101','At what age have you Menstrual','atwhatagemenstural');
Insert into tblAdolFollowUpMaster values('2','1','Girl','102','What do you normally use for Period','normaluseforperiod');
Insert into tblAdolFollowUpMaster values('3','1','Girl','103','Menstrual Problem','menturalproblems');
Insert into tblAdolFollowUpMaster values('4','2','Girl','201','Do you consume IFA tablets daily','consumeifadaily');
Insert into tblAdolFollowUpMaster values('5','2','Girl','202','No of Tablets consumed per Day?','nooftabletconsumes');
Insert into tblAdolFollowUpMaster values('6','2','Girl','203','Reasons','ifareasontxt');
Insert into tblAdolFollowUpMaster values('7','2','Girl','204','Who had given the IFA Tablets?','whogaveifa');
Insert into tblAdolFollowUpMaster values('8','3','Girl','301','Have you taken deworming tablets in last 6 Months?','dewormingtablets');
Insert into tblAdolFollowUpMaster values('9','4','Girl','401','Are you aware of Contraception','awareofcontraception');
Insert into tblAdolFollowUpMaster values('10','4','Girl','402','Do you need Contraceptive?','needcontraceptive');
Insert into tblAdolFollowUpMaster values('11','5','Girl','501','Have you be suffering any Health Issues for last 15 Days?','sufferinghealthissue');

create Table `tblAdolPregnant` (`autoId`INTEGER PRIMARY KEY AUTOINCREMENT,
`UserId` TEXT,
`AdolId` TEXT,
`VisitId` TEXT,
`pregnantCount` TEXT,
`tblAdolPreOutcome` Integer,
`tblAdolPreDoD` TEXT,
`tblAdolPreDeliverWhere` Integer,
`tblAdolPreWhoConductDelivery` Integer,
`tblAdolPreTransport` Integer,
`tblAdolPreDeliveryType` Integer,
`tblAdolPreDeliveryStatus` Integer,
`tblAdolPreChildSex` Integer,
`tblAdolPreChildAge` TEXT,
`transId` Integer,
`RecordCreatedDate` datetime);

CREATE TABLE "tblregisteredwomentemp" ( `UserId` varchar, `WomanId` varchar, `regPregnantorMother` integer, `regWomenImage` String,
 `regUIDNo` varchar, `regUIDType` varchar, `regUIDDate` varchar, `regCountNo` integer, `regWomanName` varchar, `regNickName` varchar,
 `regRegistrationType` integer, `regRegistrationDate` varchar, `regFinancialYear` varchar, `regPlaceOfReg` varchar,
 `regpregormotheratreg` integer, `regHusbandName` varchar, `regDateofBirth` varchar, `regAge` TEXT, `regCategory` TEXT,
 `regCaste` varchar, `regAPLBPL` varchar, `regReligion` varchar, `regWhoseMobileNo` varchar, `regPhoneNumber` varchar,
 `regLMPNotKnown` integer, `regLMP` varchar, `regEDD` varchar, `regStatusWhileRegistration` varchar, `regADDate` varchar,
 `regHusbandAge` TEXT, `regHusAgeatMarriage` TEXT, `regWifeAgeatMarriage` TEXT, `regMarriageDate` varchar, `regEducationHus` varchar,
 `regEducationWife` varchar, `regheightUnit` integer, `regHeight` varchar, `regWomanWeight` varchar, `regBloodGroup` integer,
 `regGravida` INTEGER, `regPara` INTEGER, `regLiveChildren` INTEGER, `regAbortions` INTEGER, `regChildMortPreg` INTEGER,
 `regChildMortDel` INTEGER, `regNoofHomeDeliveries` INTEGER, `regLastChildAge` TEXT, `regLastChildGender` varchar, `regLastChildWeight` TEXT,
 `regState` varchar, `regDistrict` varchar, `regSubDistrict` varchar, `regFacility` varchar, `regFacilityType` varchar, `regVillage` integer,
 `regAreaName` varchar, `regAddress` varchar, `regPincode` TEXT, `regGPSLocation` varchar, `regGPSPhoto` varchar, `regNotinRegionReason` varchar,
 `regAadharCard` varchar, `regBankName` varchar, `regBranchName` varchar, `regAccountNo` varchar, `regIFSCCode` varchar, `regRationCard` varchar,
 `regVoterId` varchar, `regCurrHealthRiskFactors` varchar, `regPrevHealthRiskFactors` varchar, `regFamilyHistoryRiskFactors` varchar,
 `regOtherhealthissue` varchar, `regOtherfamilyhistory` varchar, `regOthersprevpreg` varchar, `regAmberOrRedColorCode` integer, `regInDanger`
 integer, `regComplicatedpreg` integer, `regPrematureBirth` integer, `regriskFactors` varchar, `regCHriskfactors` varchar, `regPHriskfactors`
 varchar, `regrecommendedPlaceOfDelivery` varchar, `regComments` varchar, `LastServiceListNumber` integer, `CurrentWomenStatus` varchar,
 `DateTransferred` varchar, `DateExpired` varchar, `DateAborted` varchar, `DateDeactivated` varchar, `ReasonforDeactivation` varchar,
 `OtherDeactivationReason` TEXT, `DeacComments` TEXT, `IsCompl` TEXT, `LastAncVisitDate` TEXT,
 `LastPncVisitDate` TEXT, `IsReferred` TEXT, `regEligibleCoupleNo` TEXT, `transId` integer, `regUserType` varchar,
 `RecordCreatedDate` datetime, `RecordUpdatedDate` datetime, `isLMPConfirmed` integer);

 INSERT INTO tblregisteredwomentemp SELECT * from tblregisteredwomen;
DROP TABLE tblregisteredwomen;


CREATE TABLE "tblregisteredwomen" ( `UserId` varchar, `WomanId` varchar, `regPregnantorMother` integer, `regWomenImage` String,
 `regUIDNo` varchar, `regUIDType` varchar, `regUIDDate` varchar, `regCountNo` integer, `regWomanName` varchar, `regNickName` varchar,
 `regRegistrationType` integer, `regRegistrationDate` varchar, `regFinancialYear` varchar, `regPlaceOfReg` varchar,
 `regpregormotheratreg` integer, `regHusbandName` varchar, `regDateofBirth` varchar, `regAge` TEXT, `regCategory` TEXT,
 `regCaste` varchar, `regAPLBPL` varchar, `regReligion` varchar, `regWhoseMobileNo` varchar, `regPhoneNumber` varchar,
 `regLMPNotKnown` integer, `regLMP` varchar, `regEDD` varchar, `regStatusWhileRegistration` varchar, `regADDate` varchar,
 `regHusbandAge` TEXT, `regHusAgeatMarriage` TEXT, `regWifeAgeatMarriage` TEXT, `regMarriageDate` varchar, `regEducationHus` varchar,
 `regEducationWife` varchar, `regheightUnit` integer, `regHeight` varchar, `regWomanWeight` varchar, `regBloodGroup` integer,
 `regGravida` INTEGER, `regPara` INTEGER, `regLiveChildren` INTEGER, `regAbortions` INTEGER, `regChildMortPreg` INTEGER,
 `regChildMortDel` INTEGER, `regNoofHomeDeliveries` INTEGER, `regLastChildAge` TEXT, `regLastChildGender` varchar, `regLastChildWeight` TEXT,
 `regState` varchar, `regDistrict` varchar, `regSubDistrict` varchar, `regFacility` varchar, `regFacilityType` varchar, `regVillage` integer,
 `regAreaName` varchar, `regAddress` varchar, `regPincode` TEXT, `regGPSLocation` varchar, `regGPSPhoto` varchar, `regNotinRegionReason` varchar,
 `regAadharCard` varchar, `regBankName` varchar, `regBranchName` varchar, `regAccountNo` varchar, `regIFSCCode` varchar, `regRationCard` varchar,
 `regVoterId` varchar, `regCurrHealthRiskFactors` varchar, `regPrevHealthRiskFactors` varchar, `regFamilyHistoryRiskFactors` varchar,
 `regOtherhealthissue` varchar, `regOtherfamilyhistory` varchar, `regOthersprevpreg` varchar, `regAmberOrRedColorCode` integer, `regInDanger`
 integer, `regComplicatedpreg` integer, `regPrematureBirth` integer, `regriskFactors` varchar, `regCHriskfactors` varchar, `regPHriskfactors`
 varchar, `regrecommendedPlaceOfDelivery` varchar, `regComments` varchar, `LastServiceListNumber` integer, `CurrentWomenStatus` varchar,
 `DateTransferred` varchar, `DateExpired` varchar, `DateAborted` varchar, `DateDeactivated` varchar, `ReasonforDeactivation` varchar,
 `OtherDeactivationReason` TEXT, `DeacComments` TEXT, `IsCompl` TEXT, `LastAncVisitDate` TEXT,
 `LastPncVisitDate` TEXT, `IsReferred` TEXT, `regEligibleCoupleNo` TEXT, `transId` integer, `regUserType` varchar,
 `RecordCreatedDate` datetime, `RecordUpdatedDate` datetime, `isLMPConfirmed` integer, `regNoofStillBirth` INTEGER );

INSERT INTO tblregisteredwomen
SELECT UserId,WomanId,regPregnantorMother,regWomenImage,regUIDNo,regUIDType,regUIDDate,regCountNo,regWomanName,regNickName,regRegistrationType,regRegistrationDate,regFinancialYear,regPlaceOfReg,regpregormotheratreg,regHusbandName,regDateofBirth,regAge,'',regCaste,regAPLBPL,regReligion,regWhoseMobileNo,regPhoneNumber,regLMPNotKnown,regLMP,regEDD,regStatusWhileRegistration,regADDate,regHusbandAge,regHusAgeatMarriage,regWifeAgeatMarriage,regMarriageDate,regEducationHus,regEducationWife,regheightUnit,regHeight,regWomanWeight,regBloodGroup,regGravida,regPara,regLiveChildren,regAbortions,regChildMortPreg,regChildMortDel,regNoofHomeDeliveries,regLastChildAge,regLastChildGender,regLastChildWeight,regState,regDistrict,regSubDistrict,regFacility,regFacilityType,regVillage,regAreaName,regAddress,regPincode,regGPSLocation,regGPSPhoto,regNotinRegionReason,regAadharCard,regBankName,regBranchName,regAccountNo,regIFSCCode,regRationCard,regVoterId,regCurrHealthRiskFactors,regPrevHealthRiskFactors,regFamilyHistoryRiskFactors,regOtherhealthissue,regOtherfamilyhistory,regOthersprevpreg,regAmberOrRedColorCode,regInDanger,regComplicatedpreg,regPrematureBirth,regriskFactors,regCHriskfactors,regPHriskfactors,regrecommendedPlaceOfDelivery,regComments,LastServiceListNumber,CurrentWomenStatus,DateTransferred,DateExpired,DateAborted,DateDeactivated,
ReasonforDeactivation,
OtherDeactivationReason , DeacComments , IsCompl , LastAncVisitDate ,
 LastPncVisitDate , IsReferred , regEligibleCoupleNo ,
transId,regUserType,RecordCreatedDate,RecordUpdatedDate,isLMPConfirmed, 0 from tblregisteredwomentemp;
DROP TABLE tblregisteredwomentemp;
INSERT INTO tblBadgeCount VALUES(15,'Adolescent',0);

CREATE TABLE "tblCovidTestDetails" (
	"UserId"	Text,
	"WomenId"	Text,
	"beneficiaryType"	Text,
	"VisitNum"	INTEGER,
	"HealthIssues"	Text,
	"HealthIssuesOthers"	Text,
	"CovidTest"	Text,
	"CovidResult"	Text,
	"CovidResultDate"	Text,
	"transId"	int,
	"RecordCreatedDate"	Text,
	"RecordUpdatedDate"	Text
);
CREATE TABLE "tblCovidVaccineDetails" (
	UserId Text, BeneficiaryId Text,BeneficiaryType Text,	CovidVaccinatedNo Text,
	CovidVaccinatedDate Text,
	transId int, RecordCreatedDate Text,RecordUpdatedDate Text);

CREATE TABLE "tblinstuserstemp" ( `InstituteId` text, `UserId` text, `EmailId` text, `AshaId` TEXT, `UserName` text, `UserAge` numeric, `UserGender` text, `UserGovtIdType` text, `UserGovtId` text, `UserMobileNumber` text, `Password` text, `UserRole` text, `Address` text, `Pincode` text, `Country` text, `State` text, `District` text, `SubDistrict` text, `FacilityType` text, `FacilityName` text, `Inst_Database` text, `Bankname` text, `Branchname` text, `Ifsccode` text, `Accountno` text, `isValidated` numeric, `UserValidatedDatetime` text, `isTabLost` numeric, `TabLostDate` text, `LastWomennumber` numeric,`LastActivityNumber` numeric, `LastTransNumber` numeric, `LastRequestNumber` numeric, `LastDangerNumber` numeric, `LastParentNumber` INTEGER, `isDeactivated` numeric, `DeactivatedDate` text, `DeactivatedReason` text, `DevicePhnNumber` text, `datecreated` text, `createdby` text, `dateupdated` text, `updatedby` text );



 INSERT INTO tblinstuserstemp  Select * from tblinstusers;
 DROP table tblinstusers;
CREATE TABLE tblinstusers ( `InstituteId` text, `UserId` text, `EmailId` text, `AshaId` TEXT, `UserName` text, `UserAge` numeric,
 `UserGender` text, `UserGovtIdType` text, `UserGovtId` text, `UserMobileNumber` text, `Password` text, `UserRole` text, `Address` text,
 `Pincode` text, `Country` text, `State` text, `District` text, `SubDistrict` text, `FacilityType` text, `FacilityName` text, `Inst_Database`
 text, `Bankname` text, `Branchname` text, `Ifsccode` text, `Accountno` text, `isValidated` numeric, `UserValidatedDatetime` text,
 `isTabLost` numeric, `TabLostDate` text, `LastWomennumber` numeric,`LastActivityNumber` numeric, `LastTransNumber` numeric,
 `LastRequestNumber` numeric, `LastDangerNumber` numeric, `LastParentNumber` INTEGER, `isDeactivated` numeric, `DeactivatedDate` text,
 `DeactivatedReason` text, `DevicePhnNumber` text,`LastAdolNumber` numeric, `datecreated` text, `createdby` text, `dateupdated` text,
 `updatedby` text );



INSERT INTO tblinstusers SELECT `InstituteId` , `UserId` , `EmailId` , `AshaId` , `UserName` ,
`UserAge` , `UserGender` , `UserGovtIdType` , `UserGovtId` , `UserMobileNumber` , `Password` ,
 `UserRole` , `Address` , `Pincode` , `Country` , `State` , `District` , `SubDistrict` ,
 `FacilityType` , `FacilityName` , `Inst_Database` , `Bankname` , `Branchname` , `Ifsccode` ,
 `Accountno` , `isValidated` , `UserValidatedDatetime` , `isTabLost` , `TabLostDate` , `LastWomennumber` ,
 `LastActivityNumber` , `LastTransNumber` , `LastRequestNumber` , `LastDangerNumber` , `LastParentNumber` ,
 `isDeactivated` , `DeactivatedDate` , `DeactivatedReason` , `DevicePhnNumber` ,0 , `datecreated` ,
 `createdby` , `dateupdated` , `updatedby`   from tblinstuserstemp;
 DROP table tblinstuserstemp;

 CREATE TABLE "tblchildimmunizationtemp" ( `UserId` TEXT, `WomenId` TEXT, `immChildId` TEXT, `immId` Integer, `immNumber` Integer, `immType` TEXT, `immMinDate` TEXT, `immMaxDate` TEXT, `immActualDateOfAction` TEXT, `immComments` TEXT, `immFacType` TEXT, `immProvidedFacName` TEXT, `immUserType` TEXT, `immProvidedGPSLoc` TEXT, `transId` Integer, `RecordCreatedDate` Datetime );
INSERT INTO tblchildimmunizationtemp  Select * from tblchildimmunization;
 DROP table tblchildimmunization;
 CREATE TABLE "tblchildimmunization" ( `UserId` TEXT, `WomenId` TEXT, `immChildId` TEXT, `immId` Integer, `immNumber` Integer, `immType` TEXT, `immMinDate` TEXT, `immMaxDate` TEXT, `immActualDateOfAction` TEXT, `immComments` TEXT, `immFacType` TEXT, `immProvidedFacName` TEXT, `immUserType` TEXT, `immProvidedGPSLoc` TEXT, `transId` Integer, `RecordCreatedDate` Datetime, `RecordUpdatedDate` Datetime );
INSERT INTO tblchildimmunization  Select `UserId` , `WomenId` , `immChildId` , `immId` , `immNumber` , `immType` , `immMinDate` , `immMaxDate` , `immActualDateOfAction` , `immComments` , `immFacType` , `immProvidedFacName` , `immUserType` , `immProvidedGPSLoc` , `transId` , `RecordCreatedDate` , `RecordCreatedDate`  from tblchildimmunizationtemp;
 DROP table tblchildimmunizationtemp;

 CREATE TABLE "tblchlparentdetailstemp" ( `chlParentId` TEXT, `UserId` TEXT, `chlRelation` TEXT, `chlGuardianName` TEXT, `chlMotherName` TEXT, `chlMotherAge` Integer, `chlMotherIdType` TEXT, `chlMotherId` TEXT, `chlMotherDeath` Integer, `chlMotherDeathDuringDel` TEXT, `chlMotherDeathReason` Integer, `chlMotherDeathOther` TEXT, `chlMotherAgeWhenPassedAway` Integer, `chlMotherDeathDate` TEXT, `chlFatherName` TEXT, `chlFatherRemarried` TEXT, `chlFatherDeath` Integer, `chlUserType` TEXT, `transId` Integer, `RecordCreatedDate` Datetime );
INSERT INTO tblchlparentdetailstemp  Select * from tblchlparentdetails;
 DROP table tblchlparentdetails;
 CREATE TABLE "tblchlparentdetails" ( `chlParentId` TEXT, `UserId` TEXT, `chlRelation` TEXT, `chlGuardianName` TEXT, `chlMotherName` TEXT, `chlMotherAge` Integer, `chlMotherIdType` TEXT, `chlMotherId` TEXT, `chlMotherDeath` Integer, `chlMotherDeathDuringDel` TEXT, `chlMotherDeathReason` Integer, `chlMotherDeathOther` TEXT, `chlMotherAgeWhenPassedAway` Integer, `chlMotherDeathDate` TEXT, `chlFatherName` TEXT, `chlFatherRemarried` TEXT, `chlFatherDeath` Integer, `chlUserType` TEXT, `transId` Integer, `RecordCreatedDate` Datetime , `RecordUpdatedDate` Datetime);
INSERT INTO tblchlparentdetails  Select `chlParentId` , `UserId` , `chlRelation` , `chlGuardianName` , `chlMotherName` , `chlMotherAge` , `chlMotherIdType` , `chlMotherId` , `chlMotherDeath` , `chlMotherDeathDuringDel` , `chlMotherDeathReason` , `chlMotherDeathOther` , `chlMotherAgeWhenPassedAway` , `chlMotherDeathDate` , `chlFatherName` , `chlFatherRemarried` , `chlFatherDeath` , `chlUserType` , `transId` , `RecordCreatedDate`  , `RecordCreatedDate`  from tblchlparentdetailstemp;
 DROP table tblchlparentdetailstemp;

create table tblmessagelogtemp (
msgId	Integer PRIMARY KEY AUTOINCREMENT,
UserId	TEXT,
WomanId	TEXT,
msgPhoneNo	TEXT,
msgBody	TEXT,
msgSentDate	TEXT,
msgPriority	Integer,
msgNoOfFailures	Integer,
msgSent	Integer,
msgStage	TEXT,
msgUserType	TEXT,
transId	Integer,
RecordCreatedDate	Datetime);

INSERT INTO tblmessagelogtemp Select * from tblmessagelog;
DROP table tblmessagelog;

create table tblmessagelog (
msgId	Integer PRIMARY KEY AUTOINCREMENT,
UserId	TEXT,
WomanId	TEXT,
msgPhoneNo	TEXT,
msgBody	TEXT,
msgSentDate	TEXT,
msgPriority	Integer,
msgNoOfFailures	Integer,
msgSent	Integer,
msgStage	TEXT,
msgUserType	TEXT,
transId	Integer,
RecordCreatedDate	Datetime, `RecordUpdatedDate` Datetime);

INSERT INTO tblmessagelog Select msgId,
                                     UserId	,
                                     WomanId	,
                                     msgPhoneNo	,
                                     msgBody	,
                                     msgSentDate	,
                                     msgPriority	,
                                     msgNoOfFailures	,
                                     msgSent	,
                                     msgStage	,
                                     msgUserType	,
                                     transId	,
                                     RecordCreatedDate	, '' from tblmessagelogtemp;
DROP table tblmessagelogtemp;

