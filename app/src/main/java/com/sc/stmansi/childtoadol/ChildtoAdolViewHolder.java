package com.sc.stmansi.childtoadol;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sc.stmansi.R;

public class ChildtoAdolViewHolder extends RecyclerView.ViewHolder {

    TextView txtName,txtAge,txtGender, txtMCPNo,txtMotherName;

    public ChildtoAdolViewHolder(@NonNull View itemView) {
        super(itemView);
        txtName = itemView.findViewById(R.id.txt_ChltoAdol_Name);
        txtAge = itemView.findViewById(R.id.txt_ChltoAdol_Age);
        txtGender = itemView.findViewById(R.id.txt_ChltoAdol_Gender);
        txtMCPNo = itemView.findViewById(R.id.txt_ChltoAdol_MCPNo);
        txtMotherName = itemView.findViewById(R.id.txt_ChltoAdol_MotherName);
    }
}
