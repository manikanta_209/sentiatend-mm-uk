package com.sc.stmansi.childtoadol;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.sc.stmansi.R;
import com.sc.stmansi.SanNap.ListofSNViewHolder;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.repositories.ChildRepository;
import com.sc.stmansi.tables.TblChildInfo;
import com.sc.stmansi.tables.TblChlParentDetails;

import java.util.ArrayList;
import java.util.List;

public class ChildtoAdolAdapter extends RecyclerView.Adapter<ChildtoAdolViewHolder> {
    private final Context context;
    private final DatabaseHelper databaseHelper;
    private final ArrayList<TblChildInfo> childtoAdolList;

    public ChildtoAdolAdapter(ArrayList<TblChildInfo> childtoAdolList, DatabaseHelper databaseHelper, Context applicationContext) {
        this.childtoAdolList = childtoAdolList;
        this.databaseHelper = databaseHelper;
        context = applicationContext;
    }

    @NonNull
    @Override
    public ChildtoAdolViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CardView v = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_listofchldtoadolitem, parent, false);
        return new ChildtoAdolViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ChildtoAdolViewHolder holder, int position) {
        try {
            holder.txtName.setText(childtoAdolList.get(position).getChlBabyname());
            String age = DateTimeUtil.calculateAgeInYears(childtoAdolList.get(position).getChlDateTimeOfBirth());
            holder.txtAge.setText(age+context.getResources().getString(R.string.yearstxt)+", ");
            String gender = childtoAdolList.get(position).getChlGender();
            if (gender.equals("Male")){
                holder.txtGender.setText(context.getResources().getString(R.string.strmale));
            }else if (gender.equals("Female")){
                holder.txtGender.setText(context.getResources().getString(R.string.strfemale));
            }else if (gender.equals("Ambigious")){
                holder.txtGender.setText(context.getResources().getString(R.string.ambigious));
            }
            if (childtoAdolList.get(position).getChildRCHID().length()>0){
                holder.txtMCPNo.setVisibility(View.VISIBLE);
                holder.txtMCPNo.setText(context.getResources().getString(R.string.rchid)+": "+childtoAdolList.get(position).getChildRCHID());
            }else {
                holder.txtMCPNo.setVisibility(View.INVISIBLE);
            }
            ChildRepository childRepository = new ChildRepository(databaseHelper);
            TblChlParentDetails parentdetail = childRepository.getParentDetails(childtoAdolList.get(position).getChlParentId()).get(0);
            if (parentdetail.getChlMotherName().length()>0){
                holder.txtMotherName.setVisibility(View.VISIBLE);
                holder.txtMotherName.setText(context.getResources().getString(R.string.babyofnew)+" "+parentdetail.getChlMotherName());
            }else {
                holder.txtMotherName.setVisibility(View.INVISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return childtoAdolList.size();
    }
}
