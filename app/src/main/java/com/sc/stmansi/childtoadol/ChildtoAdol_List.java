package com.sc.stmansi.childtoadol;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.ListofAdols.ClickListener;
import com.sc.stmansi.ListofAdols.RecyclerViewTouchListener;
import com.sc.stmansi.R;
import com.sc.stmansi.adolescent.AdolRegistration;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.common.NavDrawerItem;
import com.sc.stmansi.common.NavDrawerListAdapter;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.configuration.SyncState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.followupCaseMgmt.FollowUpTabsActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.repositories.ChildRepository;
import com.sc.stmansi.repositories.FacilityRepository;
import com.sc.stmansi.tables.TblChildInfo;
import com.sc.stmansi.tables.TblChlParentDetails;
import com.sc.stmansi.tables.tblAdolReg;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ChildtoAdol_List extends AppCompatActivity {

    private AppState appState;
    private DatabaseHelper databaseHelper;
    public AQuery aq;
    private SyncState syncState;
    private ArrayList<TblChildInfo> childtoAdolList = new ArrayList<>();
    private RecyclerView recyclerView;
    private DrawerLayout drawer;
    private ListView drawerList;
    private ChildtoAdolAdapter childtoAdolAdapter;
    private boolean spinnerSelectionChanged;
    private String filter;
    private int villagecode;
    private String villageName;
    private Map<String, Integer> villageCodeForName;
    private SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_childtoadol_list);
        try {
            Bundle bundle = getIntent().getBundleExtra("globalState");
            if (bundle != null) {
                appState = bundle.getParcelable("appState");
                syncState = bundle.getParcelable("syncState");
            } else {
                appState = new AppState();
            }

            Initialize();
//            InitializeDrawer();
            getData();
            setData();

            aq.id(R.id.toolbarBtnHome).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        displayDialogExit();
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }
                }
            });

            recyclerView.addOnItemTouchListener(new RecyclerViewTouchListener(getApplicationContext(), recyclerView, new ClickListener() {
                @Override
                public void onLongClick(View child, int childAdapterPosition) {

                }

                @Override
                public void onClick(View child, int childAdapterPosition) {
                    TblChildInfo currentChild = childtoAdolList.get(childAdapterPosition);
                    displayAlert(currentChild);
                }

                @Override
                public void onClick(View view) {

                }

                @Override
                public boolean onLongClick(View view) {
                    return false;
                }
            }));

            aq.id(R.id.toolbarSpnFacilities).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        villageName = (String) parent.getItemAtPosition(position);
                        if (spinnerSelectionChanged) {
                            getData();
                            setData();
                        }
                        spinnerSelectionChanged = true;
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());

                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            aq.id(R.id.imgresetfilter).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        aq.id(R.id.imgresetfilter).getImageView().setImageResource(R.drawable.ic_reset_gray);
                        filter = "";
                        searchView.setQuery("", false);
                        getData();
                        setData();
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());

                    }
                }
            });

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    filter = query;
                    getData();
                    setData();
                    return false;
                }

                @SuppressLint("SetTextI18n")
                @Override
                public boolean onQueryTextChange(String newText) {
                    if (newText.length() != 0) {
                        aq.id(R.id.imgresetfilter).getImageView().setImageResource(R.drawable.ic_reset);
                    } else {
                        aq.id(R.id.imgresetfilter).getImageView().setImageResource(R.drawable.ic_reset_gray);
                    }
                    return true;
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void displayAlert(TblChildInfo currentChild) {
        try {
            AlertDialog.Builder builder = new android.app.AlertDialog.Builder(ChildtoAdol_List.this);
            builder.setMessage(getResources().getString(R.string.doyouwantomovechildtoadol));
            builder.setCancelable(false);
            builder.setNegativeButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent intent = new Intent(ChildtoAdol_List.this, AdolRegistration.class);
                    intent.putExtra("childId", currentChild.getChildID());
                    tblAdolReg newAdolReg = prepareAdolData(currentChild);
                    intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    intent.putExtra("AdolReg", newAdolReg);
                    intent.putExtra("isChildtoAdol", true);
                    if (newAdolReg != null) {
                        startActivity(intent);
                    }
                }
            }).setPositiveButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            }).create().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private tblAdolReg prepareAdolData(TblChildInfo currentChild) {
        try {
            tblAdolReg newAdolReg = new tblAdolReg();
            newAdolReg.setRegAdolName(currentChild.getChlBabyname());
            newAdolReg.setRegAdolDoB(currentChild.getChlDateTimeOfBirth());
            newAdolReg.setRegAdolGender(currentChild.getChlGender());

            TblChlParentDetails localParent = new ChildRepository(databaseHelper).getParentDetails(currentChild.getChlParentId()).get(0);
            newAdolReg.setRegAdolMother(localParent.getChlMotherName());
            newAdolReg.setRegAdolMotherDesc(localParent.getChlMotherDeath() == 1 ? "Yes" : "No");
            newAdolReg.setRegAdolFather(localParent.getChlFatherName());
            newAdolReg.setRegAdolFatherDesc(localParent.getChlFatherDeath() == 1 ? "Yes" : "No");
            newAdolReg.setRegAdolGuardian(localParent.getChlGuardianName());

            newAdolReg.setRegAdolGoingtoSchool(currentChild.getChlSchool().equals("Y") ? "Yes" : "No");
            newAdolReg.setRegAdolFacilities(currentChild.getChlTribalHamlet());
            if (currentChild.getDelBabyWeight() != null && currentChild.getDelBabyWeight().length() > 0) {
                double weight = Double.parseDouble(currentChild.getDelBabyWeight());
                weight = weight / 1000;
                newAdolReg.setRegAdolWeight(String.valueOf(weight));
            }
            return newAdolReg;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void InitializeDrawer() {
        try {
//            drawer = findViewById(R.id.drawer_NotiftyChldtoAdol);
//            drawerList = findViewById(R.id.lvNav_NotifyChldtoAdol);

            ArrayList<NavDrawerItem> navDrawerItems = new ArrayList<>();
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.casemgmt), R.drawable.ic_homevisit));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.childtoadol), R.drawable.ic_adolall));
            drawer.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
            drawerList.setAdapter(new NavDrawerListAdapter(getApplicationContext(), navDrawerItems));
            drawerList.setOnItemClickListener(new DrawerItemClickListener());
            Toolbar toolbar = findViewById(R.id.toolbar_List_SanNap);
            ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close);
            drawer.setDrawerListener(drawerToggle);
            drawerToggle.syncState();
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    @SuppressLint("SetTextI18n")
    private void setData() {
        try {
            if (childtoAdolList.size() > 0) {
                aq.id(R.id.txt_ChldtoAdolNoRecords).getView().setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(ChildtoAdol_List.this));
                childtoAdolAdapter = new ChildtoAdolAdapter(childtoAdolList, databaseHelper, getApplicationContext());
                recyclerView.setAdapter(childtoAdolAdapter);
            } else {
                aq.id(R.id.txt_ChldtoAdolNoRecords).getView().setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            }
            aq.id(R.id.txt_ChildtoAdolCount).getTextView().setText(getResources().getString(R.string.reccount) + childtoAdolList.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getData() {
        try {
            if (villageName != null && villageName.trim().length() > 0 && (!villageName.equalsIgnoreCase(getResources().getString(R.string.all))))
                villagecode = villageCodeForName.get(villageName);
            else if (aq.id(R.id.toolbarSpnFacilities).getSpinner().getSelectedItemPosition() == 0) {
                villagecode = 0;
            }
            ChildRepository childRepository = new ChildRepository(databaseHelper);
            List<TblChildInfo> allChild = childRepository.getallActiveChilds(filter, villagecode);
            if (allChild != null) {
                childtoAdolList = new ArrayList<>();
                for (TblChildInfo s : allChild) {
                    int ageyear = Integer.parseInt(DateTimeUtil.calculateAgeInYears(s.getChlDateTimeOfBirth()));
                    int agemonth = DateTimeUtil.calculateAgeInMonths(s.getChlDateTimeOfBirth());
                    if (ageyear == 10) {
                        if (agemonth > 0) {
                            childtoAdolList.add(s);
                        }
                    } else if (ageyear > 10) {
                        childtoAdolList.add(s);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Initialize() {
        try {
            databaseHelper = getHelper();
            aq = new AQuery(this);
            getSupportActionBar().hide();

            recyclerView = findViewById(R.id.rv_ChildtoAdol);
            searchView = findViewById(R.id.svListofChildtoAdol);
            aq.id(R.id.toolbartxtTitle).getTextView().setText(getResources().getString(R.string.childtoadolescent));
            aq.id(R.id.lltoolbarSpnFacilities).getView().setVisibility(View.VISIBLE);

            FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
            villageCodeForName = facilityRepository.getPlaceMap();

            ArrayAdapter<CharSequence> villageSpinnerAdapter = new ArrayAdapter<CharSequence>(getApplicationContext(), R.layout.actionbar_spinner_list_item);
//        villageSpinnerAdapter.add("All Women");06May2021 Arpitha
            villageSpinnerAdapter.add(getResources().getString(R.string.all));//06May2021 Arpitha
            for (Map.Entry<String, Integer> village : villageCodeForName.entrySet()) {
                villageSpinnerAdapter.add(village.getKey());
            }
            villageSpinnerAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
            aq.id(R.id.toolbarSpnFacilities).adapter(villageSpinnerAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(getApplicationContext(), DatabaseHelper.class);
        }
        return databaseHelper;
    }


    private class DrawerItemClickListener implements android.widget.AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            drawer.closeDrawer(drawerList);
            try {
                switch (i) {
                    case 0:
                        Intent intent = new Intent(ChildtoAdol_List.this, FollowUpTabsActivity.class);
                        intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        intent.putExtra("screen", intent);
                        startActivity(intent);
                        break;
                    case 1:
                        drawer.closeDrawer(drawerList);
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void displayDialogExit() {
        AlertDialog.Builder builder = new android.app.AlertDialog.Builder(ChildtoAdol_List.this);
        builder.setMessage(getResources().getString(R.string.m110));
        builder.setCancelable(false);
        builder.setNegativeButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(ChildtoAdol_List.this, MainMenuActivity.class);
                intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                startActivity(intent);
            }
        }).setPositiveButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }
}