package com.sc.stmansi.viewprofile;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatRadioButton;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.misc.TransactionManager;
import com.sc.stmansi.About.AboutActivity;
import com.sc.stmansi.common.CommonDatePickerFragment;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.registration.HintAdapter;
import com.sc.stmansi.registration.RegistrationActivity;
import com.sc.stmansi.repositories.AuditRepository;
import com.sc.stmansi.repositories.BankRepository;
import com.sc.stmansi.repositories.FacilityRepository;
import com.sc.stmansi.repositories.LoginRepository;
import com.sc.stmansi.repositories.SMSRepository;
import com.sc.stmansi.repositories.SettingsRepository;
import com.sc.stmansi.repositories.TransactionHeaderRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.sms.SendSMS;
import com.sc.stmansi.tables.AuditPojo;
import com.sc.stmansi.tables.MessageLogPojo;
import com.sc.stmansi.tables.TblContactDetails;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.womanlist.RegisteredWomenActionTabs;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import com.sc.stmansi.R;

import static com.sc.stmansi.common.DateTimeUtil.getNumberOfDaysFromLMP;

public class ViewProfileActivity extends AppCompatActivity implements
        View.OnClickListener, View.OnFocusChangeListener, DatePickerDialog.OnDateSetListener,
        View.OnTouchListener {

    private tblregisteredwomen regData;
    private AQuery aqVP;
    private Map<String, Integer> facPlaceOfReg = new HashMap<>();
    private Map<String, Integer> phoneNumber = new HashMap<>();
    private Map<String, Integer> village = new HashMap<>();
    private ArrayList<String> villageList = new ArrayList<>();
    private List<String> bankNamesArray;
    private String healthIssuesString = "",
            prevDelString = "", path = null, religion = "", eddDate = "";
    private tblregisteredwomen woman;
    private String villageName;
    private EditText focusEditText = null;
    private int heightUnit = 0, villageCode, redCnt = 0, amberCnt = 0,BloodGroup;// 08Jan2017 Arpitha
    private int heightNumeric = 0;
    private int heightDecimal = 0;
    private int height = 0, heightD = 0;
    private double weight;
    private int daysFromLmp = 0;
    private boolean isFirstTimeMother = false;
    private ArrayList<String> healthIssuesList, prevDelList;
    private String healthIssuesStringStore = "", prevDelStringStore = "", uidType, aplBpl,
            caste;
    private ArrayList<String> healthIssuesListStore, prevDelListStore;
    private static final int IMAGE_CAPTURE = 0;
    private File imgFileBeforeResult;
    private ByteArrayOutputStream baos;
    private Uri outputFileUri;
    private AuditPojo APJ;
    private int gestationAge;
    private String editedValues = "";
    private boolean isDob;
    private boolean isMarriageDate = false;
    private int pregnantOrMother = 1;
    private String wImage;
    private TblInstusers user;
    private DatabaseHelper databaseHelper;
    private AppState appState;
    boolean isRegSpnBanknameset = false;

    //26Sep2019 - Bindu
    private String category = "";
    private boolean isRhNegSet = false;
    private RadioGroup rdgtribalcase;
    private int regType;
    private boolean isThayiCard;
    //14May2021 Arpitha
    boolean messageSent;
    boolean isMessageLogsaved;
    int transId;
    static EditText etphn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            Bundle bundle = getIntent().getBundleExtra("globalState");
            appState = bundle.getParcelable("appState");

            databaseHelper = getHelper();

            UserRepository userRepository = new UserRepository(databaseHelper);
            user = userRepository.getOneAuditedUser(appState.ashaId);

            WomanRepository womanRepository = new WomanRepository(databaseHelper);
            regData = womanRepository.getRegistartionDetails(appState.selectedWomanId, appState.sessionUserId);

            setContentView(R.layout.activity_registration);

            aqVP = new AQuery(this);

            initializeScreen(aqVP.id(R.id.rlRegDelMainLayout).getView());

            //04Aug2019 - Bindu - set action bar icon
            getSupportActionBar();
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayOptions(getSupportActionBar().DISPLAY_SHOW_CUSTOM | getSupportActionBar().DISPLAY_SHOW_HOME | getSupportActionBar().DISPLAY_SHOW_TITLE);
            getSupportActionBar().setTitle(getResources().getString(R.string.viewprofile) + " - " + regData.getRegWomanName());

            hideAllOptionalViews();
            setSpinnerData();

            // 03Oct2019 - Bindu - Radiogrp
            rdgtribalcase = findViewById(R.id.rdgtribalcaste);

            getData();
            setInitialView();
            setTextChangeListener();
            setFocusChangeListener();
            setOnTouchListener();

            //05Aug2019 - Bindu - Sharing file access for version N and above
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());

            if((regData.getDateDeactivated()!=null &&
                    regData.getDateDeactivated().trim().length()>0)
                    || user.getIsDeactivated()==1) {
                disableScreen();//26Nov2019 Arpitha
                aqVP.id(R.id.spnbloodgroup).getSpinner().setBackground(getResources().getDrawable(R.drawable.edittext_disable));
                aqVP.id(R.id.spnvillage).getSpinner().setBackground(getResources().getDrawable(R.drawable.edittext_disable));

            }



            aqVP.id(R.id.etlmpmother).enabled(false);//24Dec2019 Arpitha

        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    //    set touch lisener
    private void setOnTouchListener() throws Exception {
        aqVP.id(R.id.etdob).getEditText().setOnTouchListener(this);
        aqVP.id(R.id.etmarriagrdate).getEditText().setOnTouchListener(this);
        //09Apr2021 Bindu
        aqVP.id(R.id.etthaicardregdate).getEditText().setOnTouchListener(this);
    }

    //	set text change listener to fields
    private void setTextChangeListener() {
        aqVP.id(R.id.etage).getEditText().addTextChangedListener(watcher);
        aqVP.id(R.id.etWomanWeight).getEditText().addTextChangedListener(watcher);
        aqVP.id(R.id.etheight).getEditText().addTextChangedListener(watcher);
        aqVP.id(R.id.ettotpreg).getEditText().addTextChangedListener(watcher);
        aqVP.id(R.id.ettotlivebirth).getEditText().addTextChangedListener(watcher);
        aqVP.id(R.id.etwifeageMarriage).getEditText().addTextChangedListener(watcher);
        aqVP.id(R.id.ethusageMarriage).getEditText().addTextChangedListener(watcher);
        aqVP.id(R.id.ethusage).getEditText().addTextChangedListener(watcher);
        aqVP.id(R.id.etthaicard).getEditText().addTextChangedListener(watcher);
        aqVP.id(R.id.etdob).getEditText().addTextChangedListener(watcher);
    }

    //	set focus change listener to fields
    private void setFocusChangeListener() throws Exception {
        aqVP.id(R.id.etage).getEditText().setOnFocusChangeListener(this);
        aqVP.id(R.id.ettotlivebirth).getEditText().setOnFocusChangeListener(this);
        aqVP.id(R.id.ettotabort).getEditText().setOnFocusChangeListener(this);
        aqVP.id(R.id.etDeadDuringPreg).getEditText().setOnFocusChangeListener(this);
        aqVP.id(R.id.etDeadAfterDel).getEditText().setOnFocusChangeListener(this);
        aqVP.id(R.id.etWomanWeight).getEditText().setOnFocusChangeListener(this);
    }

    //	initialize db object
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    //	set the initial view of screen
    private void setInitialView() {

        try {
            aqVP.id(R.id.srlWomanBasicInfo).visible();
            ((ScrollView) aqVP.id(R.id.srlWomanBasicInfo).getView()).fullScroll(ScrollView.FOCUS_UP);
            aqVP.id(R.id.btnWBD).getImageView().setBackgroundColor(getResources().getColor(R.color.gray));
            aqVP.id(R.id.btnWBD).getImageView().requestFocus();

            populateEDD(aqVP.id(R.id.etlmp).getText().toString());

            //07Aug2019 -Bindu -Disable place of reg
            aqVP.id(R.id.spnPlaceOfReg).enabled(false);
            aqVP.id(R.id.spnPlaceOfReg).background(R.drawable.spinner_bg_disabled);
            //08Apr2021 Bindu - remove disable
            /*aqVP.id(R.id.rdthayicard).enabled(false);
            aqVP.id(R.id.rdrchid).enabled(false);
            aqVP.id(R.id.etthaicard).enabled(false).background(R.drawable.spinner_bg_disabled); //12Aug2019 - Bindu disabledstate
            aqVP.id(R.id.etthaicardregdate).enabled(false).background(R.drawable.spinner_bg_disabled);*/ //12Aug2019 - Bindu disabledstate
            aqVP.id(R.id.ettotabort).enabled(false);
            aqVP.id(R.id.ettotpreg).enabled(false);
            aqVP.id(R.id.ettotlivebirth).enabled(false);
            aqVP.id(R.id.ethomedel).enabled(false);
            aqVP.id(R.id.etstillbirth).enabled(false);//13May2021 Arpitha
            aqVP.id(R.id.etnooflivechildren).enabled(false);
            aqVP.id(R.id.etlastchildWeight).enabled(false);
            aqVP.id(R.id.etlastchildage).enabled(false);
            aqVP.id(R.id.radiofemale1).enabled(false);
            aqVP.id(R.id.radiomale1).enabled(false);
            aqVP.id(R.id.ettotpreg).enabled(false);
            aqVP.id(R.id.ettotpreg).background(R.drawable.edittext_disable);
            aqVP.id(R.id.ettotlivebirth).background(R.drawable.edittext_disable);
            aqVP.id(R.id.ettotabort).background(R.drawable.edittext_disable);
            aqVP.id(R.id.ethomedel).background(R.drawable.edittext_disable);
            aqVP.id(R.id.etstillbirth).background(R.drawable.edittext_disable);//13May2021 Arpitha
            aqVP.id(R.id.etDeadDuringPreg).background(R.drawable.edittext_disable);
            aqVP.id(R.id.etDeadAfterDel).background(R.drawable.edittext_disable);
            aqVP.id(R.id.etlastchildage).background(R.drawable.edittext_disable);
            aqVP.id(R.id.etlastchildWeight).background(R.drawable.edittext_disable);
            aqVP.id(R.id.etnooflivechildren).background(R.drawable.edittext_disable);
            aqVP.id(R.id.etDeadDuringPreg).enabled(false);
            aqVP.id(R.id.etDeadAfterDel).enabled(false);
            aqVP.id(R.id.txtheadingreg).text(getResources().getString(R.string.viewprofile));
            if (Integer.parseInt(aqVP.id(R.id.ettotpreg).getText().toString()) == 1
                    && aqVP.id(R.id.radiopreg).isChecked())  //15Nov2019 Arpitha)
                aqVP.id(R.id.btnWPAndPPD1).enabled(false).getImageView().setBackgroundResource(R.drawable.disable_button);
            ColorCount();

            aqVP.id(R.id.etdob).getEditText().setKeyListener(null);
            aqVP.id(R.id.ethomereturndate).getEditText().setKeyListener(null);
            aqVP.id(R.id.etthaicardregdate).getEditText().setKeyListener(null);
            aqVP.id(R.id.etmarriagrdate).getEditText().setKeyListener(null);
            aqVP.id(R.id.imgclearthayidate).gone();

            aqVP.id(R.id.btnWBD).getImageView()
                    .setBackgroundColor(getResources().getColor(R.color.gray));
            //26Sep2019 - bindu - set default place of reg and disable
            aqVP.id(R.id.spnPlaceOfReg).enabled(false).background(R.drawable.spinner_bg_disabled);
            aqVP.id(R.id.chkcomplicatedpreg).enabled(false);


            if(regData.getRegPregnantorMother()==2)
            {
                aqVP.id(R.id.tvlmp).gone();
                aqVP.id(R.id.etlmp).gone();
                aqVP.id(R.id.tvedd).gone();
                aqVP.id(R.id.tvpregstatus).gone();

                aqVP.id(R.id.trlmp).visible();
                aqVP.id(R.id.tredd).visible();
            }

            if (regData.getRegCCConfirmRisk()!=null && regData.getRegCCConfirmRisk().trim().length()>0
                    && regData.getRegCCConfirmRisk().equalsIgnoreCase("Y"))
            {
                aqVP.id(R.id.spnbloodgroup).enabled(false);
                aqVP.id(R.id.etage).enabled(false);
                aqVP.id(R.id.etheight).enabled(false);
                aqVP.id(R.id.etWomanWeight).enabled(false);
                aqVP.id(R.id.ettotpreg).enabled(false);

                aqVP.id(R.id.spnHeightDecimal).enabled(false);
                aqVP.id(R.id.spnHeightNumeric).enabled(false);
                aqVP.id(R.id.chkfits).enabled(false);
                aqVP.id(R.id.chkfitsprevioushealth).enabled(false);

                aqVP.id(R.id.chkHsFits).enabled(false);
                aqVP.id(R.id.chkbp).enabled(false);
                aqVP.id(R.id.chkbplow).enabled(false);
//                aqVP.id(R.id.chkBPFamily).enabled(false);
                aqVP.id(R.id.chkHighBP).enabled(false);
                aqVP.id(R.id.chkdiabetes).enabled(false);
               // aqVP.id(R.id.chkdiabetesfamily).enabled(false);
                aqVP.id(R.id.chkdiffcultyinbreathing).enabled(false);
                aqVP.id(R.id.chkAsthama).enabled(false);
                aqVP.id(R.id.chkThyroid).enabled(false);
                aqVP.id(R.id.chkHeart).enabled(false);
                aqVP.id(R.id.chkMentalIllness).enabled(false);
                aqVP.id(R.id.chkTB).enabled(false);
                aqVP.id(R.id.chksurgeryrep).enabled(false);
                aqVP.id(R.id.chkAnemia).enabled(false);
                aqVP.id(R.id.chkrhneg).enabled(false);
                aqVP.id(R.id.chksyphilis).enabled(false);
                aqVP.id(R.id.chkhiv).enabled(false);
                aqVP.id(R.id.chksevanemia).enabled(false);
                aqVP.id(R.id.chktwinsmultiplepregHV).enabled(false);


              //  aqVP.id(R.id.chktwins).enabled(false);
              //  aqVP.id(R.id.chkMentallyRetarded).enabled(false);
//                aqVP.id(R.id.chkpsychiatricHv).enabled(false);

               // aqVP.id(R.id.chkmarriedinrelation).enabled(false);
                aqVP.id(R.id.chkAnemiaAndBloodTransfusion).enabled(false);
                aqVP.id(R.id.chkhospitaladm).enabled(false);
                aqVP.id(R.id.chksurgeryrep).enabled(false);
                aqVP.id(R.id.chkMentalDisturnabce).enabled(false);
                aqVP.id(R.id.chkabortionlessthan3months).enabled(false);
                aqVP.id(R.id.chkabortionbefore6months).enabled(false);
                aqVP.id(R.id.chkabortionbefore7months).enabled(false);
                aqVP.id(R.id.chkInfantDeathBefore28days).enabled(false);
                aqVP.id(R.id.chkcostlyinj).enabled(false);
                aqVP.id(R.id.chksurgeryrep).enabled(false);
                aqVP.id(R.id.chkAnemia).enabled(false);
                aqVP.id(R.id.chkrhneg).enabled(false);
                aqVP.id(R.id.chksyphilis).enabled(false);
                aqVP.id(R.id.chkhiv).enabled(false);
                aqVP.id(R.id.chksevanemia).enabled(false);
                aqVP.id(R.id.chktwinsmultiplepregHV).enabled(false);


                aqVP.id(R.id.chkInfection).enabled(false);
                aqVP.id(R.id.chkexcessivevaginalbleeding).enabled(false);
                aqVP.id(R.id.chkbirthweightgt).enabled(false);
                aqVP.id(R.id.chkstillorneo).enabled(false);

                //aqVP.id(R.id.chkdeficiency).enabled(false);
                aqVP.id(R.id.chkprevioussurgery).enabled(false);
                aqVP.id(R.id.chktwinmultiple).enabled(false);
                aqVP.id(R.id.chkPrevDiabetes).enabled(false);
                aqVP.id(R.id.chksponabor).enabled(false);
                aqVP.id(R.id.chkgreaterThan7monthsBleeding).enabled(false);
                aqVP.id(R.id.chkChildDeathInPregnancy).enabled(false);
                aqVP.id(R.id.chkChildBirthbefore8months).enabled(false);
                aqVP.id(R.id.chkcesarian).enabled(false);
                aqVP.id(R.id.chkchildlessthan2500gms).enabled(false);
                aqVP.id(R.id.chkPPH).enabled(false);
                aqVP.id(R.id.etothershs).enabled(false);
               // aqVP.id(R.id.etothersfh).enabled(false);
                aqVP.id(R.id.etothersprev).enabled(false);
                aqVP.id(R.id.chkrhneg).enabled(false);

                aqVP.id(R.id.rd_cm).enabled(false);
                aqVP.id(R.id.rd_feet).enabled(false);
                aqVP.id(R.id.rd_htdontknow).enabled(false);

//                aqVP.id(R.id.txtccString).text(regData.getRegRiskFactorsByCC() +" "+regData.getRegOtherRiskByCC());

                setRiskIdentifiedbyCC();
               // aqVP.id(R.id.txtAmberStringmm).text(regData.getRegriskFactors());

            }

            setRiskIdentifiedByMM();
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }

    }

    // Calculate EDD and display in EDD Edittext
    private void populateEDD(String xLMP) throws Exception {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        Date LmpDate;
        LmpDate = sdf.parse(xLMP);
        Calendar cal = Calendar.getInstance();
        cal.setTime(LmpDate);
        cal.add(Calendar.DAY_OF_MONTH, 280);

        int day = cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH) + 1;
        int year = cal.get(Calendar.YEAR);

        if (regData.getRegEDD() == null || regData.getRegEDD().length() == 0) {
            eddDate = String.format("%02d", day) + "-" + String.format("%02d", month) + "-" + year;
            aqVP.id(R.id.tvEddValue).text(eddDate);

            int noOfDays = getNumberOfDaysFromLMP(xLMP);
            int noOfWeeks = noOfDays / 7;
            gestationAge = noOfWeeks;
            int days = noOfDays % 7;
            String primi = "";
            if (Integer.parseInt(aqVP.id(R.id.ettotpreg).getText().toString()) == 1) {
                primi = getResources().getString(R.string.primi);

            }
            if (days > 0) {
                if (days == 1)
                    aqVP.id(R.id.tvpregstatus).text((noOfWeeks + "" + getResources().getString(R.string.weeks) + " "
                            + days + " " + getResources().getString(R.string.day) + " " + primi));
                else
                    aqVP.id(R.id.tvpregstatus).text((noOfWeeks + "" + getResources().getString(R.string.weeks) + " "
                            + days + " " + getResources().getString(R.string.days) + " " + primi));
            } else
                aqVP.id(R.id.tvpregstatus)
                        .text((noOfWeeks + "" + getResources().getString(R.string.weeks) + " " + primi));
        } else {

            String primi = "";
            if (Integer.parseInt(aqVP.id(R.id.ettotpreg).getText().toString()) == 1) {
                primi = getResources().getString(R.string.primi);

            }
            aqVP.id(R.id.tvEddValue).text(regData.getRegEDD());
            eddDate = regData.getRegEDD();
            CalculateLMP(regData.getRegEDD());
            int noOfWeeks = DateTimeUtil.getNumberOfWeeksFromLMP(xLMP);
            gestationAge = noOfWeeks;
            aqVP.id(R.id.tvpregstatus)
                    .text((noOfWeeks + "" + getResources().getString(R.string.weeks) + " " + primi));
        }
    }

    //	calculate LMP
    private void CalculateLMP(String EDDate) throws Exception {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        Date LMP;
        LMP = sdf.parse(EDDate);
        Calendar cal = Calendar.getInstance();
        cal.setTime(LMP);
        cal.add(Calendar.DAY_OF_MONTH, -280);

        int day = cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH) + 1;
        int year = cal.get(Calendar.YEAR);

        String lmpDate = String.format("%02d", day) + "-" + String.format("%02d", month) + "-" + year;
        aqVP.id(R.id.etlmp).text(lmpDate);
    }


    //	get data from database
    void getData() {
        try {
            WomanRepository womanRepository = new WomanRepository(databaseHelper);
            regData = womanRepository.getRegistartionDetails(regData.getWomanId(),
                    user.getUserId());
            if (regData != null) {
                aqVP.id(R.id.etwomenname).text(regData.getRegWomanName());
                aqVP.id(R.id.etage).text("" + regData.getRegAge());

                aqVP.id(R.id.etdob).text(regData.getRegDateofBirth());
                aqVP.id(R.id.etWomanWeight).text(regData.getRegWomanWeight());

                aqVP.id(R.id.etaadharcard).text(regData.getRegAadharCard());
                aqVP.id(R.id.etphone).text(regData.getRegPhoneNumber());
                aqVP.id(R.id.ettotpreg).text("" + regData.getRegGravida());
                aqVP.id(R.id.ettotlivebirth).text("" + regData.getRegPara());
                aqVP.id(R.id.etnooflivechildren).text("" + regData.getRegLiveChildren());
                aqVP.id(R.id.ettotabort).text("" + regData.getRegAbortions());
                aqVP.id(R.id.ethomedel).text("" + regData.getRegNoofHomeDeliveries());
                aqVP.id(R.id.etstillbirth).text("" + regData.getRegNoofStillBirth());//13May2021 Arpitha
                aqVP.id(R.id.etlastchildage).text("" + regData.getRegLastChildAge());
                aqVP.id(R.id.etlastchildWeight).text("" + (regData.getRegLastChildWeight() > 0 ?
                        regData.getRegLastChildWeight() : ""));
                aqVP.id(R.id.etDeadDuringPreg).text("" + regData.getRegChildMortPreg());
                aqVP.id(R.id.etDeadAfterDel).text("" + regData.getRegChildMortDel());
                aqVP.id(R.id.etaddr).text(regData.getRegAddress());
                aqVP.id(R.id.etareaname).text(regData.getRegAreaName());
                aqVP.id(R.id.etpincode).text(regData.getRegPincode());
                aqVP.id(R.id.etvoterid).text(regData.getRegVoterId());
                aqVP.id(R.id.etwifeEducation).text(regData.getRegEducationWife());
                aqVP.id(R.id.ethusbname).text(regData.getRegHusbandName());
                aqVP.id(R.id.ethusage).text("" + regData.getRegHusbandAge());
                aqVP.id(R.id.ethusEducation).text(regData.getRegEducationHus());
                aqVP.id(R.id.ethusageMarriage).text("" + regData.getRegHusAgeatMarriage());
                aqVP.id(R.id.etwifeageMarriage).text("" + regData.getRegWifeAgeatMarriage());
                aqVP.id(R.id.etmarriagrdate).text(regData.getRegMarriageDate());
                aqVP.id(R.id.etbranchname).text(regData.getRegBranchName());
                aqVP.id(R.id.etbankaccount).text(regData.getRegAccountNo());
                aqVP.id(R.id.etifsc).text(regData.getRegIFSCCode());
                aqVP.id(R.id.etcomments).text(regData.getRegComments());


                uidType = regData.getRegUIDType();
                if (regData.getRegUIDType().equalsIgnoreCase(getResources().getString(R.string.strrchid))) //03ay2021 Bindu
                    //aqVP.id(R.id.tvthaicard).text("RCHID(Unique)");
                    aqVP.id(R.id.tvthaicard).text(getResources().getString(R.string.rchidlbl)); //15Nov2019 -  Bindu
                else
                    aqVP.id(R.id.tvthaicard).text(getResources().getString(R.string.tvthaicard));
                aqVP.id(R.id.etthaicard).text(regData.getRegUIDNo());
                aqVP.id(R.id.etthaicardregdate).text(regData.getRegUIDDate());

                heightUnit = regData.getRegheightUnit();
                if (heightUnit == 2) {

                    aqVP.id(R.id.etheight).text(regData.getRegHeight());
                    aqVP.id(R.id.tr_heightcm).visible();
                    aqVP.id(R.id.trfeet).gone();
                    aqVP.id(R.id.rd_cm).checked(true);

                }

                if (heightUnit == 3) {

                    aqVP.id(R.id.etheight).text(regData.getRegHeight());
                    aqVP.id(R.id.tr_heightcm).gone();
                    aqVP.id(R.id.trfeet).gone();
                    aqVP.id(R.id.rd_htdontknow).checked(true);

                } else if (heightUnit == 1) {
                    aqVP.id(R.id.rd_feet).checked(true);
                    if (regData.getRegHeight().contains(".")) {
                        String[] nums = regData.getRegHeight().split("\\.");
                        height = Integer.parseInt(nums[0] != null ? nums[0] : "0");
                        heightD = Integer.parseInt(nums[1] != null ? nums[1] : "0");
                    } else {
                        height = Integer.parseInt(regData.getRegHeight().substring(0, 1));// Arpitha
                        // 14Feb2018
                        if (height == 0)
                            height = 5;
                        heightD = 0;
                    }
                    aqVP.id(R.id.spnHeightNumeric).setSelection(height - 1);
                    aqVP.id(R.id.spnHeightDecimal).setSelection(heightD);
                    aqVP.id(R.id.tr_heightcm).gone();
                    aqVP.id(R.id.trfeet).visible();

                }

                calculateBmi();

                String selected = regData.getRegPlaceOfReg();
                int placeOfReg = facPlaceOfReg.get(selected);
                aqVP.id(R.id.spnPlaceOfReg).setSelection(placeOfReg);

                aqVP.id(R.id.spnbloodgroup).setSelection(regData.getRegBloodGroup());

                villageCode = getKey(village, "" + regData.getRegVillage());
                aqVP.id(R.id.spnvillage).setSelection(villageCode);

                String selectedPhn = regData.getRegWhoseMobileNo();
                /* int phn = phoneNumber.get(selectedPhn);
    aqVP.id(R.id.spnphn).setSelection(phn);*/
//12Nov2019 - Bindu - set from array
                String[] phnarray = getResources().getStringArray(R.array.phnnosave);
                for (int i = 0; i < phnarray.length; i++) {
                    if (regData.getRegWhoseMobileNo().equals(phnarray[i]))
                        aqVP.id(R.id.spnphn).setSelection(i)  ;
                }



                int bank = bankNamesArray.indexOf(regData.getRegBankName());
                aqVP.id(R.id.spnbankname).setSelection(bank);
                if (bank > 0) {
                    isRegSpnBanknameset = true;
                }

                if (regData.getRegUIDType() != null && regData.getRegUIDType().equalsIgnoreCase("RCHID"))
                    aqVP.id(R.id.rdrchid).checked(true);
                else if (regData.getRegUIDType() != null && regData.getRegUIDType().equalsIgnoreCase("Thayi"))
                    aqVP.id(R.id.rdthayicard).checked(true);

                if (regData.getRegheightUnit() == 1)
                    aqVP.id(R.id.rd_feet).checked(true);
                else if (regData.getRegheightUnit() == 2)
                    aqVP.id(R.id.rd_cm).checked(true);
                else if (regData.getRegheightUnit() == 3)
                    aqVP.id(R.id.rd_htdontknow).checked(true);

                if (regData.getRegLastChildGender() != null && regData.getRegLastChildGender().equalsIgnoreCase("Male"))
                    aqVP.id(R.id.radiomale1).checked(true);
                else if (regData.getRegLastChildGender() != null && regData.getRegLastChildGender().equalsIgnoreCase("Female"))
                    aqVP.id(R.id.radiofemale1).checked(true);

                aplBpl = regData.getRegAPLBPL();
                if (regData.getRegAPLBPL() != null && regData.getRegAPLBPL().equalsIgnoreCase("APL"))
                    aqVP.id(R.id.rdapl).checked(true);
                else if (regData.getRegAPLBPL() != null && regData.getRegAPLBPL().equalsIgnoreCase("BPL"))
                    aqVP.id(R.id.rdbpl).checked(true);

                //26Sep2019 - Bindu - Eligible couple and category
                aqVP.id(R.id.etecno).text(regData.getRegEligibleCoupleNo());
                category = regData.getRegCategory();
                if (regData.getRegCategory() != null && regData.getRegCategory().equalsIgnoreCase("Non-tribal")) //12Nov2019 - Bindu - change str
                    aqVP.id(R.id.rdnontribal).checked(true);
                else if (regData.getRegCategory() != null && regData.getRegCategory().equalsIgnoreCase("Tribal")) //12Nov2019 - Bindu - change str
                    aqVP.id(R.id.rdtribal).checked(true);

                ShowHideTribalDetails();

                caste = regData.getRegCaste();

                //26Sep2019 - Bindu - set Tribal caste
                category = regData.getRegCategory();
                if (regData.getRegCategory() != null && regData.getRegCategory().equalsIgnoreCase("Non-tribal")) //12Nov2019 - Bindu - change str
                    aqVP.id(R.id.rdnontribal).checked(true);
                else if (regData.getRegCategory() != null && regData.getRegCategory().equalsIgnoreCase("Tribal")) { //12Nov2019 - Bindu - change str
                    aqVP.id(R.id.rdtribal).checked(true);

                    aqVP.id(R.id.trtribalcaste).visible();
                }

                //12Nov2019 - Bindu - Change the comparision str
                String[] tribalarray = getResources().getStringArray(R.array.tribalcaste);
                for (int i = 0; i < tribalarray.length; i++) {
                    if (regData.getRegCaste().equals(tribalarray[i]))
                        aqVP.id(R.id.spntribalcaste).setSelection(i)  ;
                }


                /*if (regData.getRegCaste() != null && regData.getRegCaste().equalsIgnoreCase(getResources().getString(R.string.strjenukurubasave)))
                    aqVP.id(R.id.rdjenukuruba).checked(true);
                else if (regData.getRegCaste() != null && regData.getRegCaste().equalsIgnoreCase(getResources().getString(R.string.strbettakurubasave)))
                    aqVP.id(R.id.rdbettakuruba).checked(true);
                else if (regData.getRegCaste() != null && regData.getRegCaste().equalsIgnoreCase(getResources().getString(R.string.strsoligasave)))
                    aqVP.id(R.id.rdsoliga).checked(true);
                else if (regData.getRegCaste() != null && regData.getRegCaste().equalsIgnoreCase(getResources().getString(R.string.stryarawasave)))
                    aqVP.id(R.id.rdyarawa).checked(true);
                else if (regData.getRegCaste() != null && regData.getRegCaste().equalsIgnoreCase(getResources().getString(R.string.strphaniyasave)))
                    aqVP.id(R.id.rdphaniya).checked(true);*/ // 08Apr2021 Bindu


                religion = regData.getRegReligion();

                if (regData.getRegReligion() != null && regData.getRegReligion().equalsIgnoreCase("Hindu"))
                    aqVP.id(R.id.rdhindu).checked(true);
                else if (regData.getRegReligion() != null && regData.getRegReligion().equalsIgnoreCase("Muslim"))
                    aqVP.id(R.id.rdmuslim).checked(true);
                else if (regData.getRegReligion() != null && regData.getRegReligion().equalsIgnoreCase("Christian"))
                    aqVP.id(R.id.rdchristian).checked(true);
                else if (regData.getRegReligion() != null && regData.getRegReligion().equalsIgnoreCase("Others"))
                    aqVP.id(R.id.rdothers).checked(true);

                // Health Status
                aqVP.id(R.id.etothershs).text(regData.getRegOtherhealthissue());
                aqVP.id(R.id.etothersprev).text(regData.getRegOthersprevpreg());
                aqVP.id(R.id.etothersfh).text(regData.getRegOtherfamilyhistory());

                //10Aug2019 - Bindu check for null and then set values
               /* if (regData.getRegCHriskfactors() != null && regData.getRegCHriskfactors().trim().length() > 0) {
                    String[] currentHeathArr = regData.getRegCHriskfactors().split(",|:");
*/
                String risks ;
                if(regData.getRegCCConfirmRisk()!=null && regData.getRegCCConfirmRisk().trim().length()>0)
                {
                    risks =
                            regData.getRegRiskFactorsByCC().split("PH:")[0];
                    aqVP.id(R.id.etothershs).text(regData.getRegOtherRiskByCC().split("PHO:")[0]);

                }else
                {
                    risks = regData.getRegCHriskfactors();
                    aqVP.id(R.id.etothershs).text(regData.getRegOtherhealthissue());

                }
                if (risks!=null && risks.trim().length()>0){

                    String[] currentHeathArr =risks.split(",|:");
                    for (int i = 0; i < currentHeathArr.length; i++) {
                        if (currentHeathArr[i].trim().equalsIgnoreCase("Convulsions"))
                            aqVP.id(R.id.chkHsFits).checked(true);
                        else if (currentHeathArr[i].trim().equalsIgnoreCase("Asthma"))
                            aqVP.id(R.id.chkAsthama).checked(true);
                        else if (currentHeathArr[i].trim().equalsIgnoreCase("Mental Illness"))
                            aqVP.id(R.id.chkMentalIllness).checked(true);
                        else if (currentHeathArr[i].trim().equalsIgnoreCase("Anemia"))
                            aqVP.id(R.id.chkAnemia).checked(true);
                        else if (currentHeathArr[i].trim().equalsIgnoreCase("HIV"))
                            aqVP.id(R.id.chkhiv).checked(true);
                        else if (currentHeathArr[i].trim().equalsIgnoreCase("BP"))
                            aqVP.id(R.id.chkbp).checked(true);
                        else if (currentHeathArr[i].trim().equalsIgnoreCase("Thyroid"))
                            aqVP.id(R.id.chkThyroid).checked(true);
                        else if (currentHeathArr[i].trim().equalsIgnoreCase("TB"))
                            aqVP.id(R.id.chkTB).checked(true);
                        else if (currentHeathArr[i].trim().equalsIgnoreCase("Rh-ve status")) {
                            aqVP.id(R.id.chkrhneg).checked(true);
                            isRhNegSet = true; //01Oct2019 - bindu - set Rh neg status
                        } else if (currentHeathArr[i].trim().equalsIgnoreCase("Hb < 7"))          //09Aug2019 - Bindu - String compare iCreateNewTrans space
                            aqVP.id(R.id.chksevanemia).checked(true);
                        else if (currentHeathArr[i].trim().equalsIgnoreCase("GDM"))
                            aqVP.id(R.id.chkdiabetes).checked(true);
                        else if (currentHeathArr[i].trim().equalsIgnoreCase("Heart Problems"))
                            aqVP.id(R.id.chkHeart).checked(true);
                        else if (currentHeathArr[i].trim().equalsIgnoreCase("Surgeries"))
                            aqVP.id(R.id.chkprevioussurgery).checked(true);
                        else if (currentHeathArr[i].trim().equalsIgnoreCase("Syphilis"))
                            aqVP.id(R.id.chksyphilis).checked(true);
                        else if (currentHeathArr[i].trim().equalsIgnoreCase("Multiple Pregnancy"))
                            aqVP.id(R.id.chktwinmultiple).checked(true);
                        /*else if (currentHeathArr[i].trim().equalsIgnoreCase("H/o systemic illness"))
                            aqVP.id(R.id.chkillness).checked(true);*/
                    }
                }

                //09Aug2019 - Bindu check for null and then set values
               /* if (regData.getRegFamilyHistoryRiskFactors() != null && regData.getRegFamilyHistoryRiskFactors().trim().length() > 0) {
                    String[] FamilyHistoryArr = regData.getRegFamilyHistoryRiskFactors().split(",");
*/
                String famRisk = "";
                /*if(regData.getRegCCConfirmRisk()!=null && regData.getRegCCConfirmRisk().trim().length()>0)
                {
                    famRisk =
                            regData.getRegRiskFactorsByCC();
                }else
                {*/
                    famRisk = regData.getRegFamilyHistoryRiskFactors();
//                }

                if (famRisk!=null && famRisk.trim().length()>0)

                {

                    String[] FamilyHistoryArr = famRisk.split(",");
                    for (int i = 0; i < FamilyHistoryArr.length; i++) {
                        if (FamilyHistoryArr[i].trim().equalsIgnoreCase("High BP"))
                            aqVP.id(R.id.chkBPFamily).checked(true);
                        else if (FamilyHistoryArr[i].trim().equalsIgnoreCase("Diabetes"))
                            aqVP.id(R.id.chkdiabetesfamily).checked(true);
                        else if (FamilyHistoryArr[i].trim().equalsIgnoreCase("Twins"))
                            aqVP.id(R.id.chktwins).checked(true);
                        else if (FamilyHistoryArr[i].trim().equalsIgnoreCase("Mental Retardation"))
                            aqVP.id(R.id.chkMentallyRetarded).checked(true);
                        else if (FamilyHistoryArr[i].trim().equalsIgnoreCase("Physical Deformation"))
                            aqVP.id(R.id.chkdeficiency).checked(true);
                        else if (FamilyHistoryArr[i].trim().equalsIgnoreCase("Married In Relation"))
                            aqVP.id(R.id.chkmarriedinrelation).checked(true);

                    }
                }

                //10Aug2019 - Bindu check for null and then set values
              /*  if (regData.getRegPHriskfactors() != null && regData.getRegPHriskfactors().trim().length() > 0) {
                    String[] prevHeathArr = regData.getRegPHriskfactors().split(",|:");*/

                String prevRisks = "";
                if(regData.getRegCCConfirmRisk()!=null && regData.getRegCCConfirmRisk().trim().length()>0)
                {
                    if(regData.getRegRiskFactorsByCC().split("PH:").length>1)
                        prevRisks =
                                regData.getRegRiskFactorsByCC().split("PH:")[1];
                    if(regData.getRegOtherRiskByCC().split("PHO:").length>1)
                        aqVP.id(R.id.etothersprev).text(regData.getRegOtherRiskByCC().split("PHO:")[1]);


                }else
                {
                    prevRisks = regData.getRegPHriskfactors();
                    aqVP.id(R.id.etothersprev).text(regData.getRegOthersprevpreg());


                }
                if ((prevRisks!=null &&
                        prevRisks.trim().length()>0)){
                    String[] prevHeathArr = prevRisks.split(",|:");
                    //01Oct2019 - Bindu - compare using strings file
                    for (int i = 0; i < prevHeathArr.length; i++) {
                        if (prevHeathArr[i].trim().equalsIgnoreCase(getResources().getString(R.string.strconvulsions)))
                            aqVP.id(R.id.chkfitsprevioushealth).checked(true);
                        else if (prevHeathArr[i].trim().equalsIgnoreCase(getResources().getString(R.string.strhighbp)))
                            aqVP.id(R.id.chkHighBP).checked(true);
                        else if (prevHeathArr[i].trim().equalsIgnoreCase(getResources().getString(R.string.stranemiaandbloodtransfusion)))
                            aqVP.id(R.id.chkAnemiaAndBloodTransfusion).checked(true);
                        else if (prevHeathArr[i].trim().equalsIgnoreCase(getResources().getString(R.string.strIUD)))
                            aqVP.id(R.id.chkChildDeathInPregnancy).checked(true);
                        else if (prevHeathArr[i].trim().equalsIgnoreCase(getResources().getString(R.string.strlowlyingplacenta)))
                            aqVP.id(R.id.chkgreaterThan7monthsBleeding).checked(true);
                        else if (prevHeathArr[i].trim().equalsIgnoreCase(getResources().getString(R.string.strgdm)))
                            aqVP.id(R.id.chkPrevDiabetes).checked(true);
            /*else if (prevHeathArr[i].trim().equalsIgnoreCase("Insulin For Diabetes"))
                aqVP.id(R.id.chkInsulinForDiabetes).checked(true);*/
                        else if (prevHeathArr[i].trim().equalsIgnoreCase(getResources().getString(R.string.strpretermdel)))
                            aqVP.id(R.id.chkChildBirthbefore8months).checked(true);
                        else if (prevHeathArr[i].trim().equalsIgnoreCase(getResources().getString(R.string.strchildlbw)))
                            aqVP.id(R.id.chkchildlessthan2500gms).checked(true);
                        else if (prevHeathArr[i].trim().equalsIgnoreCase(getResources().getString(R.string.strcesarian)))
                            aqVP.id(R.id.chkcesarian).checked(true);
                        else if (prevHeathArr[i].trim().equalsIgnoreCase(getResources().getString(R.string.strPPH)))
                            aqVP.id(R.id.chkPPH).checked(true);
                        else if (prevHeathArr[i].trim().equalsIgnoreCase(getResources().getString(R.string.strInfantDeathBefore28days)))
                            aqVP.id(R.id.chkInfantDeathBefore28days).checked(true);
                        else if (prevHeathArr[i].trim().equalsIgnoreCase(getResources().getString(R.string.strsepsis)))
                            aqVP.id(R.id.chkInfection).checked(true);
                        else if (prevHeathArr[i].trim().equalsIgnoreCase(getResources().getString(R.string.strantepartum_depression)))
                            aqVP.id(R.id.chkMentalDisturnabce).checked(true);
                        else if (prevHeathArr[i].trim().equalsIgnoreCase(getResources().getString(R.string.strabortionsbftwentyeightwks)))
                            aqVP.id(R.id.chkabortionbefore7months).checked(true);
           /* else if (prevHeathArr[i].trim().equalsIgnoreCase("Abortion before 24 weeks"))
                aqVP.id(R.id.chkabortionbefore6months).checked(true);*/ //01Oct2019 - Bindu
                        else if (prevHeathArr[i].trim().equalsIgnoreCase(getResources().getString(R.string.strabortionlessthan3months)))
                            aqVP.id(R.id.chkabortionlessthan3months).checked(true);
                        else if (prevHeathArr[i].trim().equalsIgnoreCase(getResources().getString(R.string.strstillorneo)))
                            aqVP.id(R.id.chkstillorneo).checked(true);
                        else if (prevHeathArr[i].trim().equalsIgnoreCase(getResources().getString(R.string.strspontabor)))
                            aqVP.id(R.id.chksponabor).checked(true);

                        else if (prevHeathArr[i].trim().equalsIgnoreCase(getResources().getString(R.string.strbirthweightgt)))
                            aqVP.id(R.id.chkbirthweightgt).checked(true);
                        else if (prevHeathArr[i].trim().equalsIgnoreCase(getResources().getString(R.string.strhospitaladmission)))
                            aqVP.id(R.id.chkhospitaladm).checked(true);
                        else if (prevHeathArr[i].trim().equalsIgnoreCase(getResources().getString(R.string.strsurgeryreproductivetract)))
                            aqVP.id(R.id.chksurgeryrep).checked(true);
                        else if (prevHeathArr[i].trim().equalsIgnoreCase(getResources().getString(R.string.strisoimmunization_rhneg)))
                            aqVP.id(R.id.chkcostlyinj).checked(true);

                    }
                }

                if (regData.getRegComplicatedpreg() == 1)
                    aqVP.id(R.id.chkcomplicatedpreg).checked(true);
                else
                    aqVP.id(R.id.chkcomplicatedpreg).checked(false);

                aqVP.id(R.id.etregdate).text(regData.getRegRegistrationDate());
                if(regData.getRegPregnantorMother()!=2) {//28Nov2019 Arpitha
                    aqVP.id(R.id.etlmp).text(regData.getRegLMP());
                    aqVP.id(R.id.tvEddValue).text(regData.getRegEDD());
                    aqVP.id(R.id.tvmotherorpregnant).text(getResources().getString(R.string.radiopreg));
                }else//28Nov2019 Arpitha
                {
                    aqVP.id(R.id.etlmp).text(regData.getRegADDate());
                    aqVP.id(R.id.tvEddValue).gone();
                    aqVP.id(R.id.tvmotherorpregnant).text(getResources().getString(R.string.radiomother)+"("+getResources().getString(R.string.txtADD)+":"+regData.getRegADDate()+")");

                }

                if (regData.getRegpregormotheratreg() == 1)
                    aqVP.id(R.id.regpregormother).text(getResources().getString(R.string.regpreg));
                else
                    aqVP.id(R.id.regpregormother).text(getResources().getString(R.string.regmother));
                aqVP.id(R.id.tvpregstatus).text(regData.getRegStatusWhileRegistration());


                aqVP.id(R.id.txtReferal).text(regData.getRegrecommendedPlaceOfDelivery());

                aqVP.id(R.id.etrationcard).text(regData.getRegRationCard());


                if (regData.getRegWomenImage() != null && regData.getRegWomenImage().trim().length() > 0) {

                    byte[] encodeByte = Base64.decode(regData.getRegWomenImage(), Base64.DEFAULT);
                    Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);

                    if (bitmap!=null){ //Ramesh 3-8-2021 If image is null, setting default image
                        aqVP.id(R.id.imgwomenphoto).getImageView().setImageBitmap(bitmap);
                    }else {
                        aqVP.id(R.id.imgwomenphoto).getImageView().setImageResource(R.drawable.women);
                    }
                }

                aqVP.id(R.id.etlmpmother).text(regData.getRegLMP());
                aqVP.id(R.id.eteddmother).text(regData.getRegEDD());

//                02Sep2021 Arpitha
                if(regData.getRegIsAdolescent()!=null && regData.getRegIsAdolescent().trim().length()>0 &&
                        regData.getRegIsAdolescent().equalsIgnoreCase("Yes"))
                {
                    aqVP.id(R.id.etwomenname).enabled(false);
                    aqVP.id(R.id.etage).enabled(false);
                    aqVP.id(R.id.etWomanWeight).enabled(false);
                    aqVP.id(R.id.etaadharcard).enabled(false);
                    aqVP.id(R.id.etaddr).enabled(false);
                    aqVP.id(R.id.etareaname).enabled(false);
                    aqVP.id(R.id.etphone).enabled(false);
                    aqVP.id(R.id.etdob).enabled(false);
                    aqVP.id(R.id.rd_cm).enabled(false);
                    aqVP.id(R.id.etheight).enabled(false);
                    aqVP.id(R.id.rd_feet).enabled(false);
                    aqVP.id(R.id.spnHeightDecimal).enabled(false);
                    aqVP.id(R.id.spnHeightNumeric).enabled(false);
                    aqVP.id(R.id.rd_htdontknow).enabled(false);
                    aqVP.id(R.id.spnphn).enabled(false);
                    aqVP.id(R.id.spnvillage).enabled(false);
                }
            }
        } catch (SQLException e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    //	set data to spinner
    void setSpinnerData() {

        try {
//        place of reg
            String[] regPl = getResources().getStringArray(R.array.spnPlaceOfRegArr);
            String[] regPlArr = new String[regPl.length];
            for (int i = 0; i < regPl.length; i++) {
                regPlArr[i] = regPl[i];
                facPlaceOfReg.put(regPl[i], i);
            }
            ArrayAdapter<String> plOfRegAdap = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_dropdown_item, regPlArr);
            aqVP.id(R.id.spnPlaceOfReg).adapter(plOfRegAdap);

            //      phone
            String[] phn = getResources().getStringArray(R.array.phnno);
            String[] phnArray = new String[phn.length];
            for (int i = 0; i < phn.length; i++) {
                phnArray[i] = phn[i];
                phoneNumber.put(phn[i], i);
            }
            ArrayAdapter<String> phnAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_dropdown_item, phnArray);
            aqVP.id(R.id.spnphn).adapter(phnAdapter);

//   village
            FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
            village = facilityRepository.getPlaceMap();
            villageList.add(getResources().getString(R.string.selectType));
            for (Map.Entry<String, Integer> village : village.entrySet())
                villageList.add(village.getKey());
            ArrayAdapter<String> VillageAdapter = new ArrayAdapter<String>(this,
                    R.layout.simple_spinner_dropdown_item, villageList);
            VillageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            aqVP.id(R.id.spnvillage).adapter(VillageAdapter);


//    bank name
            BankRepository bankRepository = new BankRepository(databaseHelper);
            bankNamesArray = bankRepository.getBankName();
            HintAdapter bankNameAdapter = new HintAdapter(ViewProfileActivity.this,
                    bankNamesArray, R.layout.simple_spinner_dropdown_item);
            bankNameAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            aqVP.id(R.id.spnbankname).adapter(bankNameAdapter);

            int pos = bankNamesArray.size() - 1;
            aqVP.id(R.id.spnbankname).setSelection(pos);

//            29Nov2019 Arpitha
            ArrayList<String> blood = new ArrayList<String>();
            blood.add((getResources().getString(R.string.selectType)));
            blood.add((getResources().getString(R.string.apositive)));
            blood.add((getResources().getString(R.string.anegative)));
            blood.add((getResources().getString(R.string.bpositive)));
            blood.add((getResources().getString(R.string.bnegative)));
            blood.add((getResources().getString(R.string.abpositive)));
            blood.add((getResources().getString(R.string.abnegative)));
            blood.add((getResources().getString(R.string.opositive)));
            blood.add((getResources().getString(R.string.onegative)));
            blood.add((getResources().getString(R.string.dontknow)));

            ArrayAdapter<String> adapterblood = new ArrayAdapter<String>(this,
                    R.layout.simple_spinner_dropdown_item, blood);
            adapterblood.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            aqVP.id(R.id.spnbloodgroup).adapter(adapterblood);

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }

    }

    // return the position of given key in a map
    public static int getKey(Map<String, Integer> mapref, String value) {

        int pos = 0;
        for (Map.Entry<String, Integer> map : mapref.entrySet()) {
            pos++;
            if (map.getValue().toString().equals(value)) {
                return pos;
            }

        }
        return pos;
    }

    /**
     * This method handles all Click listners, calls - all validation methods
     * and confrimAlert()
     */
    @Override
    public void onClick(View v) {

        try {
            switch (v.getId()) {
                case R.id.imgwomenphoto: {
                    CapturingImage();
                    break;
                }
                case R.id.btnWBD: {
                    aqVP.id(R.id.btnsave).getImageView().setImageResource(R.drawable.ic_arrow_forward); //07Aug2019 - Bindu - change icon
                    aqVP.id(R.id.tvWBIHeading).visible();
                    hideAllOptionalViews();
                    aqVP.id(R.id.srlWomanBasicInfo).visible();
                    aqVP.id(R.id.btnWBD).getImageView().setBackgroundColor(getResources().getColor(R.color.gray));
                    ColorCount();
                    break;
                }
                case R.id.btnHStatus: {
                    if (validateRegFields()) {

                        calculateBmi();
                        aqVP.id(R.id.btnsave).getImageView().setImageResource(R.drawable.ic_arrow_forward); //07Aug2019 - Bindu - change icon
                        hideAllOptionalViews();
                        aqVP.id(R.id.srlhealthhistory).visible();
                        aqVP.id(R.id.btnHStatus).getImageView().setBackgroundColor(getResources().getColor(R.color.gray));
                        ColorCount();
                    } else {
                        hideAllOptionalViews();
                        openBasicInfoAndSetFocus();
                    }
                    break;
                }
                case R.id.btnSummary: {
                    if (validateRegFields()) {
                        hideKeyboard(); //04OCt2019 - Bindu - hide keyboard
                        hideAllOptionalViews();

                        aqVP.id(R.id.srlSummary).visible();

                        villageName = (String) aqVP.id(R.id.spnvillage).getSelectedItem();
                        if (villageName != null) {
                            aqVP.id(R.id.txtVillage).text((villageName + "\t"));
                        }

                        aqVP.id(R.id.btnsave).getImageView().setImageResource(R.drawable.save);
                        aqVP.id(R.id.btnSummary).getImageView().setBackgroundColor(getResources().getColor(R.color.gray));
                        if (aqVP.id(R.id.radiomother).isChecked() && aqVP.id(R.id.etadd).getText().length() > 0) {

                        }
                        ColorCount();
                    } else {
                        hideAllOptionalViews();
                        openBasicInfoAndSetFocus();
                    }
                    break;
                }
                case R.id.btnWPAndPPD1: {
                    if (regData.getRegGravida() <= 1 && aqVP.id(R.id.radiopreg).isChecked()) {//15Nov2019 Arpitha
                        Toast.makeText(this, (getResources().getString(R.string.m065)), Toast.LENGTH_LONG).show();
                    } else {
                        if (validateRegFields()) {
                            aqVP.id(R.id.btnsave).getImageView().setImageResource(R.drawable.ic_arrow_forward); //07Aug2019 - Bindu - change icon
                            hideAllOptionalViews();
                            aqVP.id(R.id.srlWomanPersonalAndPrevPregDetails).visible();
                            aqVP.id(R.id.btnWPAndPPD1).getImageView()
                                    .setBackgroundColor(getResources().getColor(R.color.gray));
                            aqVP.id(R.id.ettotlivebirth).getEditText().requestFocus();
                            ColorCount();
                            aqVP.id(R.id.tvWBIHeading).gone();

                        } else {
                            hideAllOptionalViews();
                            openBasicInfoAndSetFocus();
                        }
                    }
                    break;
                }


              /*  case R.id.btnDelInfo: {
                    if (validateRegFields()) {

                        if (regData.getRegADDate().length() > 0) {
                            hideAllOptionalViews();
                            aqVP.id(R.id.srlDelInfo).visible();
                            aqVP.id(R.id.btnDelInfo).getImageView().setBackgroundColor(getResources().getColor(R.color.gray));
                            aqVP.id(R.id.ethospname).getEditText().requestFocus();
                            ColorCount();
                            aqVP.id(R.id.tvWBIHeading).gone();
                        } else {
                            Toast.makeText(getApplicationContext(), (getResources().getString(R.string.m066)),
                                    Toast.LENGTH_LONG).show();
                        }
                    } else {
                        hideAllOptionalViews();
                        openBasicInfoAndSetFocus();
                    }
                    break;
                }*/

                case R.id.btnsave: {

                    if (aqVP.id(R.id.srlSummary).getView().getVisibility() == View.VISIBLE) {

                        if (regData.getCurrentWomenStatus() != null) {
                            if (regData.getCurrentWomenStatus().equalsIgnoreCase("WT")) {
                                Toast.makeText(getApplicationContext(), (getResources().getString(R.string.m104)),
                                        Toast.LENGTH_LONG).show();
                            } else if (regData.getCurrentWomenStatus().equalsIgnoreCase("WA")) {
                                Toast.makeText(getApplicationContext(), (getResources().getString(R.string.m104)),
                                        Toast.LENGTH_LONG).show();
                            }
                            if (regData.getCurrentWomenStatus().equalsIgnoreCase("WE")) {
                                if (regData.getRegADDate() != null && regData.getRegADDate().length() > 0) {
                                    boolean ischilddead = false;


                                    if (ischilddead)
                                        Toast.makeText(getApplicationContext(),
                                                (getResources().getString(R.string.m104)), Toast.LENGTH_LONG).show();
                                    else
                                        checkEditApplicable();
                                } else
                                    Toast.makeText(getApplicationContext(), (getResources().getString(R.string.m104)),
                                            Toast.LENGTH_LONG).show();
                            }
                        } else {
                            checkEditApplicable();
                        }

                    } else if (aqVP.id(R.id.srlWomanBasicInfo).getView().getVisibility() == View.VISIBLE) {

                        if (validateRegFields()) {
                            hideAllOptionalViews();
                            aqVP.id(R.id.srlhealthhistory).visible();
                            aqVP.id(R.id.tvWBIHeading).gone();
                            aqVP.id(R.id.btnHStatus).getImageView().setBackgroundColor(getResources().getColor(R.color.gray));
                            ColorCount();
                        } else {
                            hideAllOptionalViews();
                            openBasicInfoAndSetFocus();
                        }

                    } else if (aqVP.id(R.id.srlhealthhistory).getView().getVisibility() == View.VISIBLE) {
                        if (regData.getRegGravida() <= 1 && regData.getRegPregnantorMother()==1) {
                            if (validateRegFields()) {
                                calculateBmi();
                                hideAllOptionalViews();

                                aqVP.id(R.id.srlSummary).visible();
                                villageName = (String) aqVP.id(R.id.spnvillage).getSelectedItem();
                                if (villageName != null) {
                                    aqVP.id(R.id.txtVillage).text((villageName + "\t"));
                                }
                                aqVP.id(R.id.btnsave).getImageView().setImageResource(R.drawable.save);
                                aqVP.id(R.id.btnSummary).getImageView()
                                        .setBackgroundColor(getResources().getColor(R.color.gray));

                                ColorCount();
                            } else {
                                hideAllOptionalViews();
                                openBasicInfoAndSetFocus();
                            }

                        } /*else if (aqVP.id(R.id.radiomother).isChecked()) {

                            if (validateRegFields()) {
                                calculateBmi();
                                hideAllOptionalViews();

                                aqVP.id(R.id.srlSummary).visible();

                                villageName = (String) aqVP.id(R.id.spnvillage).getSelectedItem();
                                if (villageName != null) {

                                    aqVP.id(R.id.txtVillage).text((villageName + "\t"));
                                }

                                aqVP.id(R.id.btnsave).getImageView().setImageResource(R.drawable.save);
                                aqVP.id(R.id.btnSummary).getImageView()
                                        .setBackgroundColor(getResources().getColor(R.color.gray));

                                ColorCount();
                            } else {
                                hideAllOptionalViews();
                                openBasicInfoAndSetFocus();
                            }
                        }*/ else {
                            if (validateRegFields()) {
                                calculateBmi();
                                hideAllOptionalViews();
                                aqVP.id(R.id.srlWomanPersonalAndPrevPregDetails).visible();
                                aqVP.id(R.id.btnWPAndPPD1).getImageView()
                                        .setBackgroundColor(getResources().getColor(R.color.gray));
                                aqVP.id(R.id.ettotlivebirth).getEditText().requestFocus();
                                ColorCount();
                            } else {
                                hideAllOptionalViews();
                                openBasicInfoAndSetFocus();
                            }
                        }


                        break;

                    } else if (aqVP.id(R.id.srlWomanPersonalAndPrevPregDetails).getView().getVisibility() == View.VISIBLE) {

                        if (validateRegFields()) {
                            calculateBmi();
                            hideAllOptionalViews();

                            aqVP.id(R.id.srlSummary).visible();

                            villageName = (String) aqVP.id(R.id.spnvillage).getSelectedItem();
                            if (villageName != null) {
                                aqVP.id(R.id.txtVillage).text((villageName + "\t"));
                            }

                            aqVP.id(R.id.btnsave).getImageView().setImageResource(R.drawable.save);
                            aqVP.id(R.id.btnSummary).getImageView().setBackgroundColor(getResources().getColor(R.color.gray));

                            ColorCount();
                        } else {
                            hideAllOptionalViews();
                            openBasicInfoAndSetFocus();
                        }

                    } /*else if (aqVP.id(R.id.srlDelInfo).getView().getVisibility() == View.VISIBLE) {
                        if (validateRegFields()) {
                            calculateBmi();
                            hideAllOptionalViews();
                            aqVP.id(R.id.srlSummary).visible();
                            villageName = (String) aqVP.id(R.id.spnvillage).getSelectedItem();
                            if (villageName != null) {
                                aqVP.id(R.id.txtVillage).text((villageName + "\t"));
                            }
                            aqVP.id(R.id.btnsave).getImageView().setImageResource(R.drawable.save);
                            aqVP.id(R.id.btnSummary).getImageView().setBackgroundColor(getResources().getColor(R.color.gray));
                        }
                    }*/
                    break;
                }
                case R.id.btnclear: {
                    confirmAlertExit();
                    break;
                }
                case R.id.rd_cm: {
                    heightUnit = 2;
                    aqVP.id(R.id.tr_heightcm).visible();
                    aqVP.id(R.id.etheight).getEditText().requestFocus();
                    aqVP.id(R.id.trfeet).gone(); //Bindu 22Jun2019 changed id

                    calculateBmi(); //09Aug2019 - Bindu - Cal bmi
                    aqVP.id(R.id.txtBMI).text("");//06Aug2021 Arpitha
                    break;
                }
                case R.id.rd_feet: {

                    heightUnit = 1;
                    aqVP.id(R.id.tr_heightcm).gone();
                    aqVP.id(R.id.etheight).text("");
                    aqVP.id(R.id.trfeet).visible();
                    aqVP.id(R.id.spnHeightNumeric).setSelection(4);
                    aqVP.id(R.id.spnHeightDecimal).setSelection(0);
                    calculateBmi();
                    break;
                }
                case R.id.rd_htdontknow: {
                    heightUnit = 3;
                    aqVP.id(R.id.trfeet).gone();
                    aqVP.id(R.id.tr_heightcm).gone();
                    aqVP.id(R.id.etheight).text("");
                    calculateBmi();
                    break;
                }
                case R.id.chkAnemia:
                case R.id.chkdiabetes:
                case R.id.chkAsthama:
                case R.id.chkThyroid:
                case R.id.chkHeart:
                case R.id.chkMentalIllness:
                case R.id.chkTB:
                case R.id.chkprevioussurgery:
                case R.id.chktwins:
                case R.id.chkMentallyRetarded:
                case R.id.chkdeficiency:
                case R.id.chkdiabetesfamily:
                case R.id.chkmarriedinrelation:
                case R.id.chkHighBP:
                case R.id.chkHsFits:
                case R.id.chkbp:
                case R.id.chkAnemiaAndBloodTransfusion:
                case R.id.chkChildDeathInPregnancy:
                case R.id.chkPrevDiabetes:
                /*case R.id.chkInsulinForDiabetes:
                    ColorCount();
                    break;*/ //01Oct2019 - Bindu - remove insulin
                case R.id.chkchildlessthan2500gms:
                case R.id.chkcesarian:
                case R.id.chkPPH:
                case R.id.chkInfantDeathBefore28days:
                case R.id.chkInfection:
                case R.id.chkMentalDisturnabce:
                case R.id.chkabortionbefore7months:
               /* case R.id.chkabortionbefore6months:
                    ColorCount();
                    break;*/  //01OCt2019 - Bindu - removed
                case R.id.chkabortionlessthan3months:
                    ColorCount();
                    break;
                case R.id.rdhindu:
                    religion = "Hindu";
                    break;
                case R.id.rdmuslim:
                    religion = "Muslim";
                    break;
                case R.id.rdchristian:
                    religion = "Christian";
                    break;
                case R.id.rdothers:
                    religion = "Others";
                    break;
//                    20Aug2019 Arpitha
                case R.id.imgcleardob:
                    aqVP.id(R.id.etdob).text("");
                    aqVP.id(R.id.imgcleardob).gone();
                    break;
                case R.id.imgclearmarriagedate:
                    aqVP.id(R.id.etmarriagrdate).text("");
                    aqVP.id(R.id.imgclearmarriagedate).gone();
                    break;
                //20Aug2019 Arpitha

                //20Aug2019 Arpitha
                case R.id.chkhiv:
                    ColorCount();
                    break;
                case R.id.chkillness:
                    ColorCount();
                    break;
                case R.id.chkrhneg:
                    ColorCount();
                    break;
                case R.id.chksevanemia:
                    ColorCount();
                    break;
                case R.id.chksyphilis:
                    ColorCount();
                    break;
                case R.id.chktwinmultiple:
                    ColorCount();
                    break;
                case R.id.chkfitsprevioushealth:
                    ColorCount();
                    break;
                case R.id.chkgreaterThan7monthsBleeding:
                    ColorCount();
                    break;
                case R.id.chkChildBirthbefore8months:
                    ColorCount();
                    break;
                case R.id.chkstillorneo:
                    ColorCount();
                    break;
                case R.id.chksponabor:
                    ColorCount();
                    break;
                case R.id.chkbirthweightgt:
                    ColorCount();
                    break;
                case R.id.chkhospitaladm:
                    ColorCount();
                    break;
                case R.id.chksurgeryrep:
                    ColorCount();
                    break;
                case R.id.chkcostlyinj:
                    ColorCount();
                    break;
                //26Sep2019 - Bindu -Add Radiooption - Tribal and non tribal
                case R.id.rdnontribal:
                case R.id.rdtribal:
                    ShowHideTribalDetails();
                    break;
//                    09Apr2021 Bindu
                case R.id.rdrchid:
                    uidType = "RCHID";
                    aqVP.id(R.id.tvthaicard).text(getResources().getString(R.string.rchid)); //14Aug2019 - Bindu - set from resource
                    aqVP.id(R.id.llthayi).background(R.drawable.edittext_disable);
                    aqVP.id(R.id.etthaicardregdate).enabled(false);
                    aqVP.id(R.id.imgclearthayidate).gone();
                    aqVP.id(R.id.etthaicard).text("");
                    aqVP.id(R.id.etthaicardregdate).text("");

                    break;
                case R.id.rdthayicard:
                    uidType = "Thayi";
                    aqVP.id(R.id.etthaicardregdate).text("");
                    aqVP.id(R.id.llthayi).background(R.drawable.edittext_disable);
                    aqVP.id(R.id.etthaicardregdate).enabled(false);
                    aqVP.id(R.id.imgclearthayidate).gone();
                    aqVP.id(R.id.tvthaicard).text(getResources().getString(R.string.tvthaicard));
                    aqVP.id(R.id.etthaicard).text("");

                    break;
                case R.id.imgclearthayidate:
                    aqVP.id(R.id.etthaicardregdate).text("");
                    aqVP.id(R.id.imgclearthayidate).gone();
                    break;
                default:
                    break;
            }
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }


    /**
     * This method invokes camera intent
     */
    @SuppressLint("SimpleDateFormat")
    protected void CapturingImage() {

        try {
            // intent to start device camera
            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            Date xDate = new Date();
            String lasmod = new SimpleDateFormat("dd-hh-mm-ss").format(xDate);

            String fileName = "iWomen" + lasmod;
            File imageDir = new File(AppState.imgDirRef);

            if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
                if (imageDir != null) {
                    if (!imageDir.mkdirs()) {
                        if (!imageDir.exists()) {
                            Log.d("CameraSample", "failed to create directory");
                        } else {
                            Log.d("CameraSample", "created directory");
                        }
                    }
                }
            } else {
                Log.v(getString(R.string.app_name), "External storage is not mounted READ/WRITE.");
            }

            path = imageDir + "/" + fileName + ".jpg";
            imgFileBeforeResult = new File(path);

            outputFileUri = Uri.fromFile(imgFileBeforeResult);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            startActivityForResult(intent, 0);
        } catch (NullPointerException e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }


    /**
     * This method get the result from Camera Intent, gets the image
     */
    @SuppressLint("SimpleDateFormat")
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            wImage = "";
            if (requestCode == IMAGE_CAPTURE) {
                if (resultCode == RESULT_OK) {


                    baos = new ByteArrayOutputStream();
                    Bitmap bitmap = BitmapFactory.decodeFile(path);
                    Bitmap resizedBmp = Bitmap.createScaledBitmap(bitmap, 230, 210, true);
                    resizedBmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    aqVP.id(R.id.imgwomenphoto).getImageView().setImageBitmap(resizedBmp);


                    ByteArrayOutputStream outputstream = new ByteArrayOutputStream();
                    resizedBmp.compress(Bitmap.CompressFormat.PNG, 0, outputstream);
                    wImage = Base64.encodeToString(outputstream.toByteArray(), Base64.DEFAULT);


                } else if (resultCode == RESULT_CANCELED) {
                    deleteImages();
                }
            } else {
                Toast.makeText(getApplicationContext(), (getResources().getString(R.string.m100)),
                        Toast.LENGTH_LONG).show();
            }
        } catch (NullPointerException e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    /**
     * Delete the temporarily stored Images
     */
    private void deleteImages() {

        File imgfileAfterResult = null;
        if (imgfileAfterResult != null)
            imgfileAfterResult.delete();
        if (imgFileBeforeResult != null)
            imgFileBeforeResult.delete();
    }

    /**
     * This method calculate DangerSigns - Generate DangerSigns String Populate
     * summary tab
     */
    private void ColorCount() {
        int age = 0;
        redCnt = 0;
        amberCnt = 0;
        healthIssuesList = new ArrayList<String>();
        prevDelList = new ArrayList<String>();

        healthIssuesString = "";
        prevDelString = "";

        healthIssuesStringStore = "";
        prevDelStringStore = "";
        healthIssuesListStore = new ArrayList<String>();
        prevDelListStore = new ArrayList<String>();

        if (heightUnit == 1) {
            height = Integer.parseInt(aqVP.id(R.id.spnHeightNumeric).getSelectedItem().toString());
            heightD = Integer.parseInt(aqVP.id(R.id.spnHeightDecimal).getSelectedItem().toString());
        } else if (heightUnit == 2 && aqVP.id(R.id.etheight).getText().toString().trim().length() > 0) {
            Double heightinfeet = 0.0328084 * (Double.parseDouble(aqVP.id(R.id.etheight).getText().toString()));
            height = Integer.parseInt(heightinfeet.toString().split("\\.")[0]);
            heightD = Integer.parseInt(heightinfeet.toString().split("\\.")[1].substring(0, 1));
        } else {
            height = 0;
            heightD = 0;
        }

        if (height > 0 || heightD > 0) {

            if (height < 5) {
                if (heightD < 8) {
                    redCnt = redCnt + 1;
                    healthIssuesList.add((getResources().getString(R.string.heightab2)));
                    healthIssuesListStore.add(("Short Stature"));

                } else {
                    amberCnt = amberCnt + 1;
                    healthIssuesList.add((getResources().getString(R.string.heightab2)));
                    healthIssuesListStore.add(("Short Stature"));

                }
            } else if (height < 4.8) {
                redCnt = redCnt + 1;
                healthIssuesList.add((getResources().getString(R.string.heightab1)));
                healthIssuesListStore.add(("Highly short Statured"));

            }
        }

        /*if (aqVP.id(R.id.etWomanWeight).getText().toString().length() > 0) {       //13Aug2019 - Bindu - add .tostring
            weight = Double.parseDouble(aqVP.id(R.id.etWomanWeight).getText().toString());
            if ((weight >= 75 && weight <= 80) || weight <= 35) {
                amberCnt = amberCnt + 1;
                healthIssuesList.add((getResources().getString(R.string.weightabno)));
                healthIssuesListStore.add(("Malnutrition"));
            } else if (weight > 80) {
                redCnt = redCnt + 1;
                healthIssuesList.add((getResources().getString(R.string.weightabnormal2)));
                healthIssuesListStore.add(("Overnutrition"));
            }
        }*/ //02Oct2019 - bindu - disable weight cal

        //02Oct2019 - Bindu - change the condition
        if (aqVP.id(R.id.tvBMI).getText().toString().trim().length() > 0) {
            if (Double.parseDouble(aqVP.id(R.id.tvBMI).getText().toString()) < 18.5) {
                redCnt = redCnt + 1;
                healthIssuesList.add((getResources().getString(R.string.undernutrition)));
                healthIssuesListStore.add((getResources().getString(R.string.strundernutrition)));
            }else if (Double.parseDouble(aqVP.id(R.id.tvBMI).getText().toString()) >= 23) {
                redCnt = redCnt + 1;
                healthIssuesList.add((getResources().getString(R.string.overnutrition)));
                healthIssuesListStore.add((getResources().getString(R.string.strovernutrition)));
            }
        }

        if (aqVP.id(R.id.etage).getText().length() > 0) {
            age = Integer.parseInt(aqVP.id(R.id.etage).getText().toString());
            if (age > 40) {
                redCnt = redCnt + 1;
                healthIssuesList.add((getResources().getString(R.string.moreage)));
                healthIssuesListStore.add(("Elderly Gravida"));

            } else if (age >= 35 && age <= 40) {
                amberCnt = amberCnt + 1;
                healthIssuesList.add((getResources().getString(R.string.moreage)));
                healthIssuesListStore.add(("Elderly Gravida"));

            } else if (age < 18 && age > 15) {
                amberCnt = amberCnt + 1;
                healthIssuesList.add((getResources().getString(R.string.lessage)));
                healthIssuesListStore.add(("Very Less Age"));

            } else if (age <= 15) {
                redCnt = redCnt + 1;
                healthIssuesList.add((getResources().getString(R.string.verylessage)));
                healthIssuesListStore.add(("Less Age"));

            }
        }

        int gravida = (aqVP.id(R.id.ettotpreg).getText().toString().length() > 0)
                ? Integer.parseInt(aqVP.id(R.id.ettotpreg).getText().toString()) : 0;
        if (gravida > 3) {
            amberCnt = amberCnt + 1;
            prevDelList.add(("multiple pregnancies(>3)"));
            prevDelListStore.add(("multiple pregnancies(>3)"));
        }



        if (aqVP.id(R.id.chkbp).isChecked()) {
            redCnt = redCnt + 1;
            healthIssuesList.add(getResources().getString(R.string.chkbp));
            healthIssuesListStore.add(("BP"));

        }
        if (aqVP.id(R.id.chkdiabetes).isChecked()) {
            redCnt = redCnt + 1;
            healthIssuesList.add(getResources().getString(R.string.gdm));
            healthIssuesListStore.add(("GDM"));

        }
        if (aqVP.id(R.id.chkAsthama).isChecked()) {
            amberCnt = amberCnt + 1;
            healthIssuesList.add(getResources().getString(R.string.chkAsthama));
            healthIssuesListStore.add(("Asthma"));

        }
        if (aqVP.id(R.id.chkThyroid).isChecked()) {
            amberCnt = amberCnt + 1;
            healthIssuesList.add(getResources().getString(R.string.chkThyroid));
            healthIssuesListStore.add(("Thyroid"));

        }
        if (aqVP.id(R.id.chkHeart).isChecked()) {
            redCnt = redCnt + 1;
            healthIssuesList.add(getResources().getString(R.string.chkHeart));
            healthIssuesListStore.add(("Heart Problems"));

        }
        if (aqVP.id(R.id.chkMentalIllness).isChecked()) {
            amberCnt = amberCnt + 1;
            healthIssuesList.add(getResources().getString(R.string.chkMentalIllness));
            healthIssuesListStore.add(("Mental Illness"));

        }
        if (aqVP.id(R.id.chkTB).isChecked()) {
            amberCnt = amberCnt + 1;
            healthIssuesList.add(getResources().getString(R.string.chkTB));
            healthIssuesListStore.add(("TB"));

        }



        if (aqVP.id(R.id.chkprevioussurgery).isChecked()) {
            amberCnt = amberCnt + 1;
            healthIssuesList.add(getResources().getString(R.string.chkprevioussurgery));
            healthIssuesListStore.add(("Surgeries"));

        }

        if (aqVP.id(R.id.chkAnemia).isChecked()) {
            amberCnt = amberCnt + 1;
            healthIssuesList.add(getResources().getString(R.string.chkAnemia));
            healthIssuesListStore.add(("Anemia"));

        }









        /*if (aqVP.id(R.id.chkillness).isChecked()) {
            amberCnt = amberCnt + 1;
            healthIssuesList.add(getResources().getString(R.string.patientwithhisofsysillness));
            healthIssuesListStore.add(("H/o systemic illness"));

        }*/ // 01Oct2019 - bindu - unchec illness
        if (aqVP.id(R.id.chktwinmultiple).isChecked()) {
            amberCnt = amberCnt + 1;
            healthIssuesList.add(getResources().getString(R.string.multiplepreg));
            healthIssuesListStore.add(("Multiple Pregnancy"));

        }
        if (aqVP.id(R.id.chksevanemia).isChecked()) {
            amberCnt = amberCnt + 1;
            healthIssuesList.add(getResources().getString(R.string.sevanemia));
            healthIssuesListStore.add(("Hb < 7"));

        }
        if (aqVP.id(R.id.chkhiv).isChecked()) {
            redCnt = redCnt + 1;            //11Aug2019 - Bindu - corrected to redcnt from regcount
            healthIssuesList.add(getResources().getString(R.string.hiv));
            healthIssuesListStore.add(("HIV"));

        }
        if (aqVP.id(R.id.chkrhneg).isChecked()) {
            redCnt = redCnt + 1;  //11Aug2019 - Bindu - corrected to redcnt from regcount
            healthIssuesList.add(getResources().getString(R.string.rhnegstatus));
            healthIssuesListStore.add(("Rh-ve status"));

        }
        if (aqVP.id(R.id.chksyphilis).isChecked()) {
            redCnt = redCnt + 1;
            healthIssuesList.add(getResources().getString(R.string.syphilis));
            healthIssuesListStore.add(("Syphilis"));

        }
        if (aqVP.id(R.id.etothershs).getText().toString().trim().length() > 0) {
            amberCnt = amberCnt + 1;
            healthIssuesList.add((aqVP.id(R.id.etothershs).getText().toString()));
            healthIssuesListStore.add((aqVP.id(R.id.etothershs).getText().toString()));
        }
        if (aqVP.id(R.id.chkHsFits).isChecked()) {
            amberCnt = amberCnt + 1;
            healthIssuesList.add(getResources().getString(R.string.convulsions));
            healthIssuesListStore.add(("Convulsions"));
        }

//01Oct2019 - Bindu - change the naming convention and order and set values from resources
        if (aqVP.id(R.id.chkAnemiaAndBloodTransfusion).isChecked()) {
            amberCnt = amberCnt + 1;
            prevDelList.add(getResources().getString(R.string.chkAnemiaAndBloodTransfusion));
            prevDelListStore.add((getResources().getString(R.string.stranemiaandbloodtransfusion)));
        }

        if (aqVP.id(R.id.chkfitsprevioushealth).isChecked()) {
            amberCnt = amberCnt + 1;
            prevDelList.add(getResources().getString(R.string.convulsions));
            prevDelListStore.add((getResources().getString(R.string.strconvulsions)));
        }
        if (aqVP.id(R.id.chkHighBP).isChecked()) {
            redCnt = redCnt + 1;
            prevDelList.add(getResources().getString(R.string.chkHighBP));
            prevDelListStore.add((getResources().getString(R.string.strhighbp)));
        }
        if(aqVP.id(R.id.chkhospitaladm).isChecked())
        {
            amberCnt = amberCnt + 1;
            prevDelList.add(getResources().getString(R.string.hospitaladmission));
            prevDelListStore.add((getResources().getString(R.string.strhospitaladmission)));
        }
        if (aqVP.id(R.id.chkPrevDiabetes).isChecked()) {
            amberCnt = amberCnt + 1;
            prevDelList.add(getResources().getString(R.string.gdm));
            prevDelListStore.add((getResources().getString(R.string.strgdm)));
        }
        if(aqVP.id(R.id.chksurgeryrep).isChecked())
        {
            amberCnt = amberCnt + 1;
            prevDelList.add(getResources().getString(R.string.surgeryreproductivetract));
            prevDelListStore.add((getResources().getString(R.string.strsurgeryreproductivetract)));
        }
        if (aqVP.id(R.id.chkMentalDisturnabce).isChecked()) {
            amberCnt = amberCnt + 1;
            prevDelList.add(getResources().getString(R.string.antepartum_depression));
            prevDelListStore.add((getResources().getString(R.string.strantepartum_depression)));
        }
        if (aqVP.id(R.id.chkabortionlessthan3months).isChecked()) {
            redCnt = redCnt + 1;
            prevDelList.add(getResources().getString(R.string.chkabortionlessthan3months));
            prevDelListStore.add((getResources().getString(R.string.strabortionlessthan3months)));
        }
        if(aqVP.id(R.id.chkabortionbefore7months).isChecked())
        {
            amberCnt = amberCnt + 1;
            prevDelList.add(getResources().getString(R.string.abortionsbftwentyeightwks));
            prevDelListStore.add((getResources().getString(R.string.strabortionsbftwentyeightwks)));
        }
        if(aqVP.id(R.id.chksponabor).isChecked())
        {
            amberCnt = amberCnt + 1;
            prevDelList.add(getResources().getString(R.string.spontabor));
            prevDelListStore.add((getResources().getString(R.string.strspontabor)));
        }
        if (aqVP.id(R.id.chkgreaterThan7monthsBleeding).isChecked()) {
            redCnt = redCnt + 1;
            prevDelList.add(getResources().getString(R.string.lowlyingplacenta)); //15Nov2019 Bindu
            prevDelListStore.add((getResources().getString(R.string.strlowlyingplacenta)));
        }
        if (aqVP.id(R.id.chkChildDeathInPregnancy).isChecked()) {
            redCnt = redCnt + 1;
            prevDelList.add(getResources().getString(R.string.rdbIUD));
            prevDelListStore.add((getResources().getString(R.string.strIUD)));
        }
        if (aqVP.id(R.id.chkChildBirthbefore8months).isChecked()) {
            redCnt = redCnt + 1;
            prevDelList.add(getResources().getString(R.string.pretermdel));
            prevDelListStore.add((getResources().getString(R.string.strpretermdel)));
        }
        if (aqVP.id(R.id.chkcesarian).isChecked()) {
            redCnt = redCnt + 1;
            prevDelList.add(getResources().getString(R.string.chkcesarian));
            prevDelListStore.add((getResources().getString(R.string.strcesarian)));
        }
        if(aqVP.id(R.id.chkstillorneo).isChecked())
        {
            amberCnt = amberCnt + 1;
            prevDelList.add(getResources().getString(R.string.stillorneo));
            prevDelListStore.add((getResources().getString(R.string.strstillorneo)));
        }
        if (aqVP.id(R.id.chkchildlessthan2500gms).isChecked()) {
            redCnt = redCnt + 1;
            prevDelList.add(getResources().getString(R.string.childlbw));
            prevDelListStore.add((getResources().getString(R.string.strchildlbw)));
        }
        if(aqVP.id(R.id.chkbirthweightgt).isChecked())
        {
            amberCnt = amberCnt + 1;
            prevDelList.add(getResources().getString(R.string.birthweightgt4500)); //15Nov2019 - Bindu
            prevDelListStore.add((getResources().getString(R.string.strbirthweightgt)));// 04Aug2019 - Bindu - change from static
        }
        if (aqVP.id(R.id.chkPPH).isChecked()) {
            amberCnt = amberCnt + 1;
            prevDelList.add(getResources().getString(R.string.chkPPH));
            prevDelListStore.add((getResources().getString(R.string.strPPH)));
        }
        if (aqVP.id(R.id.chkInfection).isChecked()) {
            amberCnt = amberCnt + 1;
            prevDelList.add(getResources().getString(R.string.sepsis));
            prevDelListStore.add((getResources().getString(R.string.strsepsis)));
        }
        if(aqVP.id(R.id.chkcostlyinj).isChecked())
        {
            amberCnt = amberCnt + 1;
            prevDelList.add(getResources().getString(R.string.isoimmunization_rhneg));
            prevDelListStore.add((getResources().getString(R.string.strisoimmunization_rhneg)));
        }
        if (aqVP.id(R.id.chkInfantDeathBefore28days).isChecked()) {
            redCnt = redCnt + 1;
            prevDelList.add(getResources().getString(R.string.chkInfantDeathBefore28days));
            prevDelListStore.add((getResources().getString(R.string.strInfantDeathBefore28days)));
        }

        /*if (aqVP.id(R.id.chkInsulinForDiabetes).isChecked()) {
            redCnt = redCnt + 1;
            prevDelList.add(getResources().getString(R.string.chkInsulinForDiabetes));
            prevDelListStore.add(("Insulin For Diabetes"));
        }

        if (aqVP.id(R.id.chkabortionbefore6months).isChecked()) {
            amberCnt = amberCnt + 1;
            prevDelList.add(getResources().getString(R.string.abortionbftwentyfourwks));
            prevDelListStore.add(("Abortion before 24 weeks"));
        }*/ //01Oct2019 - bindu - Remove the symptoms


        if (aqVP.id(R.id.etothersprev).getText().toString().trim().length() > 0) {
            amberCnt = amberCnt + 1;
            prevDelList.add((aqVP.id(R.id.etothersprev).getText().toString()));
            prevDelListStore.add((aqVP.id(R.id.etothersprev).getText().toString()));
        }

        //01Oct2019 - Change the images icon for stars
        if (amberCnt == 1 && redCnt == 0)
            aqVP.id(R.id.imgStar).getImageView().setImageResource(R.drawable.amberone);
        else if (amberCnt == 2)
            aqVP.id(R.id.imgStar).getImageView().setImageResource(R.drawable.ambertwo);
        else if (amberCnt > 2)
            redCnt = redCnt + 1;
        else
            aqVP.id(R.id.imgStar).getImageView().setImageResource(0);

        if (redCnt == 1)
            aqVP.id(R.id.imgStar).getImageView().setImageResource(R.drawable.redone);
        else if (redCnt == 2)
            aqVP.id(R.id.imgStar).getImageView().setImageResource(R.drawable.redtwo);
        else if (redCnt == 3)
            aqVP.id(R.id.imgStar).getImageView().setImageResource(R.drawable.redthree);
        else if (redCnt > 3)
            aqVP.id(R.id.imgStar).getImageView().setImageResource(R.drawable.redfour);

        for (int i = 0; i < healthIssuesList.size(); i++) {
            if (i == (healthIssuesList.size() - 1)) {
                healthIssuesString = healthIssuesString + healthIssuesList.get(i);
                healthIssuesStringStore = "CH: " + healthIssuesStringStore + healthIssuesListStore.get(i);

            } else {
                healthIssuesString = healthIssuesString + healthIssuesList.get(i) + ",";
                healthIssuesStringStore = healthIssuesStringStore + healthIssuesListStore.get(i) + ",";

            }
        }

        for (int i = 0; i < prevDelList.size(); i++) {
            if (i == (prevDelList.size() - 1)) {
                prevDelString = prevDelString + prevDelList.get(i);
                prevDelStringStore = "PH: " + prevDelStringStore + prevDelListStore.get(i);

            } else {
                prevDelString = prevDelString + prevDelList.get(i) + ",";
                prevDelStringStore = prevDelStringStore + prevDelListStore.get(i) + ",";

            }
        }

        String str = "";
        String spn = null;
        if (regData.getRegPregnantorMother()==1) {//25May2021 Arpitha
            aqVP.id(R.id.txtLMPorADDL).text((getResources().getString(R.string.tvlmp)));
            str = aqVP.id(R.id.etlmp).getText().toString();
            aqVP.id(R.id.txtLMPorADD).text(str);
            aqVP.id(R.id.txtEDDL).text((getResources().getString(R.string.tvedd)));
            str = eddDate;
            aqVP.id(R.id.txtEDD).text(str);
        } else {
            aqVP.id(R.id.txtLMPorADDL).text((getResources().getString(R.string.tvadd)));
//            str = aqVP.id(R.id.etadd).getText().toString();25May2021 Arpitha
            str = regData.getRegADDate();//25May2021 Arpitha
            aqVP.id(R.id.txtLMPorADD).text(str);
            aqVP.id(R.id.txtEDDL).text((getResources().getString(R.string.tvNoofchildren)));
            str = "" + aqVP.id(R.id.spnnoofchild).getSelectedItem();
            aqVP.id(R.id.txtEDD).text(str);
//            25May2021 Arpitha
            aqVP.id(R.id.txtEDD).gone();
            aqVP.id(R.id.txtEDDL).gone();
        }

        spn = (aqVP.id(R.id.etwomenname).getText().toString() + "\t");
        aqVP.id(R.id.txtName).text(spn);
        spn = (aqVP.id(R.id.etage).getText().toString() + "\t");
        aqVP.id(R.id.tvWSAgeVal).text(spn);

        if(aqVP.id(R.id.spnphn).getSelectedItemPosition()>0) {//Arpitha - only when number exists
            str = aqVP.id(R.id.etphone).getText().toString() + " ( " + aqVP.id(R.id.spnphn).getSelectedItem().toString() + " ) "; //11Aug2019 - Bindu Concat mob no of
            aqVP.id(R.id.txtPhNo).text(str);
        }

        str = aqVP.id(R.id.ettotpreg).getText().toString();
        aqVP.id(R.id.txttotPregnancies).text(str);
        if (villageName != null) {
            spn = (villageName + "\t");
            aqVP.id(R.id.txtVillage).text(spn);
        }
        aqVP.id(R.id.txtAmberString).text((prevDelString));
        aqVP.id(R.id.txtRedString).text((healthIssuesString));

      /*  if (healthIssuesString!=null && healthIssuesString.length()>0 && prevDelString!=null && prevDelString.length()>0){
            aqVP.id(R.id.txtccString).text("CH: "+healthIssuesString+"\nPH: "+prevDelString);
        }else if (prevDelString!=null && prevDelString.length()>0)
            aqVP.id(R.id.txtccString).text("PH: "+prevDelString);
        else if (healthIssuesString!=null && healthIssuesString.length()>0)
            aqVP.id(R.id.txtccString).text("CH: "+healthIssuesString);

*/

        aqVP.id(R.id.imgStar).visible();

       // aqVP.id(R.id.txtAmberStringmm).text(regData.getRegriskFactors());

        //27Sep2019 - Bindu - Del plan display if already set to Cemoc
        // if(regData.getRegrecommendedPlaceOfDelivery()!=null && regData.getRegrecommendedPlaceOfDelivery().length() > 0 && regData.getRegrecommendedPlaceOfDelivery().equalsIgnoreCase(getResources().getString(R.string.strcemoc))) {
        //15Nov2019 - also check for compl if any and then set val
        if(regData.getIsCompl() != null && regData.getIsCompl().equalsIgnoreCase(getResources().getString(R.string.yesshortform)) &&
                regData.getRegrecommendedPlaceOfDelivery()!=null && regData.getRegrecommendedPlaceOfDelivery().length() > 0 && regData.getRegrecommendedPlaceOfDelivery().equalsIgnoreCase(getResources().getString(R.string.strcemoc))) {
            aqVP.id(R.id.txtReferal).text(regData.getRegrecommendedPlaceOfDelivery());
            aqVP.id(R.id.txtReferal).visible();
            aqVP.id(R.id.txtReferalL).visible();
        }
        else if(aqVP.id(R.id.chkcostlyinj).isChecked()||
                aqVP.id(R.id.chkcostlyinj).isChecked()||
                aqVP.id(R.id.chksurgeryrep).isChecked()||
                aqVP.id(R.id.chkhospitaladm).isChecked()||
                aqVP.id(R.id.chkbirthweightgt).isChecked()||
                aqVP.id(R.id.chksponabor).isChecked()||
                aqVP.id(R.id.chkstillorneo).isChecked()||
                aqVP.id(R.id.chkMentalDisturnabce).isChecked()||
                /*aqVP.id(R.id.chkInsulinForDiabetes).isChecked()||*/ //01Oct2019 - Bindu - remove insulin
                aqVP.id(R.id.chkInfection).isChecked()||
                aqVP.id(R.id.chkfitsprevioushealth).isChecked()||
                aqVP.id(R.id.chkgreaterThan7monthsBleeding).isChecked()||
                aqVP.id(R.id.chkPrevDiabetes).isChecked()||
                aqVP.id(R.id.chkChildBirthbefore8months).isChecked()||
                aqVP.id(R.id.chkchildlessthan2500gms).isChecked()||
                aqVP.id(R.id.chkChildDeathInPregnancy).isChecked()||
                aqVP.id(R.id.chkcesarian).isChecked()||
                aqVP.id(R.id.chkAnemiaAndBloodTransfusion).isChecked()||
                aqVP.id(R.id.chkabortionbefore7months).isChecked()||
                 /*aqVP.id(R.id.chkabortionbefore6months).isChecked()||
               aqVP.id(R.id.chkillness).isChecked()||*/ //01Oct2019 - Bindu - remove
                aqVP.id(R.id.chkrhneg).isChecked()||
                aqVP.id(R.id.chktwinmultiple).isChecked()||
                aqVP.id(R.id.chksyphilis).isChecked()||
                aqVP.id(R.id.chkhiv).isChecked() ||
                aqVP.id(R.id.chkThyroid).isChecked()||
                aqVP.id(R.id.chkprevioussurgery).isChecked()||
                aqVP.id(R.id.chkMentalIllness).isChecked() ||
                aqVP.id(R.id.chkHeart).isChecked()||
                aqVP.id(R.id.chkHsFits).isChecked()||
                aqVP.id(R.id.chkdiabetes).isChecked()||
                aqVP.id(R.id.chkbp).isChecked()||
                aqVP.id(R.id.chkAsthama).isChecked()||
                aqVP.id(R.id.chksevanemia).isChecked()||
                aqVP.id(R.id.chkPPH).isChecked()||
                aqVP.id(R.id.etothersprev).getText().toString().trim().length()>0 ||
                (age> 0 && (age>40 || age<=15 || (age>=35 && age<=40) || (age<18 && age>15)))||
                (weight>0 && ((weight >= 75 && weight <= 80) || weight <= 35 || weight>80))||
                (height>0 &&  height<4.8)
        ) {
            aqVP.id(R.id.txtReferal).text(getResources().getString(R.string.strcemoc)); //11May2021 Bindu retrieve cemoc from strings
            aqVP.id(R.id.txtReferal).visible();
            aqVP.id(R.id.txtReferalL).visible();
        }
        // 27Sep2019 - Bindu - set Place of del for Home visit - Bemoc updated - set after cemoc cond
        // else if(regData.getRegrecommendedPlaceOfDelivery()!=null && regData.getRegrecommendedPlaceOfDelivery().length() > 0 && regData.getRegrecommendedPlaceOfDelivery().equalsIgnoreCase(getResources().getString(R.string.strbemoc))) {
        //15Nov2019 - Bindu - check for compl also
        else if(regData.getIsCompl() != null && regData.getIsCompl().equalsIgnoreCase(getResources().getString(R.string.yesshortform)) &&
                regData.getRegrecommendedPlaceOfDelivery()!=null && regData.getRegrecommendedPlaceOfDelivery().length() > 0 && regData.getRegrecommendedPlaceOfDelivery().equalsIgnoreCase(getResources().getString(R.string.strbemoc))) {
            aqVP.id(R.id.txtReferal).text(regData.getRegrecommendedPlaceOfDelivery());
            aqVP.id(R.id.txtReferal).visible();
            aqVP.id(R.id.txtReferalL).visible();
        }
        else if((aqVP.id(R.id.tvBMI).getText().toString().trim().length()>0 &&
                Double.parseDouble(aqVP.id(R.id.tvBMI).getText().toString())<19) ||
                aqVP.id(R.id.chkTB).isChecked() || aqVP.id(R.id.etothershs).getText().toString().trim().length()>0) {
            aqVP.id(R.id.txtReferal).text(getResources().getString(R.string.strbemoc)); //11May2021 Bindu retrieve bemoc from strings
            aqVP.id(R.id.txtReferal).visible();
            aqVP.id(R.id.txtReferalL).visible();
        }else
        {
            aqVP.id(R.id.txtReferal).text("");
            aqVP.id(R.id.txtReferal).gone();
            aqVP.id(R.id.txtReferalL).gone();


        }


        calCompPreg();
    }

    /**
     * calculation for counting total pregnancies
     */
    protected void calTotPregnancies() {
        calCompPreg();
        if (regData.getRegGravida() == 1)
            disableLastChildFields();// Arpitha 09Feb2018
    }

    //	check high risk or not
    private void calCompPreg() {
        if (amberCnt > 0 || redCnt > 0)
            aqVP.id(R.id.chkcomplicatedpreg).checked(true);
        else
            aqVP.id(R.id.chkcomplicatedpreg).checked(false);
    }

    //	disable last child fields
    private void disableLastChildFields() {
        aqVP.id(R.id.tvlastchildage).enabled(false);
        aqVP.id(R.id.etlastchildage).enabled(false);
        aqVP.id(R.id.tvlastchildWeight).enabled(false);
        aqVP.id(R.id.etlastchildWeight).enabled(false);
        aqVP.id(R.id.tvchildsex).enabled(false);
        aqVP.id(R.id.radiofemale1).enabled(false);

        aqVP.id(R.id.radiomale1).enabled(false);

        aqVP.id(R.id.etlastchildage).background(R.drawable.edittext_disable);
        aqVP.id(R.id.etlastchildWeight).background(R.drawable.edittext_disable);

        aqVP.id(R.id.etDeadDuringPreg).enabled(false);
        aqVP.id(R.id.etDeadAfterDel).enabled(false);
        aqVP.id(R.id.etDeadDuringPreg).background(R.drawable.edittext_disable);
        aqVP.id(R.id.etDeadAfterDel).background(R.drawable.edittext_disable);
    }


    /**
     * hide all scrollViews, set gray background to tab buttons
     */
    private void hideAllOptionalViews() throws Exception {
        aqVP.id(R.id.srlWomanBasicInfo).gone();
        aqVP.id(R.id.srlWomanPersonalAndPrevPregDetails).gone();
        aqVP.id(R.id.srlhealthhistory).gone();
        aqVP.id(R.id.tvWBIHeading).gone();
        aqVP.id(R.id.srlSummary).gone();
//        aqVP.id(R.id.srlDelInfo).gone();

        aqVP.id(R.id.btnWBD).getImageView().setBackgroundResource(R.drawable.gray_button);
        aqVP.id(R.id.btnWPAndPPD1).getImageView().setBackgroundResource(R.drawable.gray_button);
//        aqVP.id(R.id.btnDelInfo).getImageView().setBackgroundResource(R.drawable.gray_button);
        aqVP.id(R.id.btnHStatus).getImageView().setBackgroundResource(R.drawable.gray_button);
        aqVP.id(R.id.btnSummary).getImageView().setBackgroundResource(R.drawable.gray_button);
//        aqVP.id(R.id.btnIssueAtDeliveryAcc).getImageView().setImageResource(R.drawable.add);

        if (!isFirstTimeMother) {
            if (regData.getRegLMP() != null && regData.getRegLMP().length() > 0) {
                daysFromLmp = DateTimeUtil.getDaysBetweenDates(DateTimeUtil.getTodaysDate(),
                        regData.getRegLMP());
                if (daysFromLmp <= 210 || (regData.getRegADDate() == null || regData.getRegADDate().length() == 0)) {
//                    aqVP.id(R.id.btnDelInfo).enabled(false).getImageView()
//                            .setBackgroundResource(R.drawable.disable_button);
                }
            }
        }

        if (aqVP.id(R.id.ettotpreg).getText().toString() != null
                && aqVP.id(R.id.ettotpreg).getText().toString().trim().length() > 0
                && Integer.parseInt(aqVP.id(R.id.ettotpreg).getText().toString()) == 1
                && aqVP.id(R.id.radiopreg).isChecked())//15Nov2019 Arpitha
            aqVP.id(R.id.btnWPAndPPD1).enabled(false)
                    .getImageView().setBackgroundResource(R.drawable.disable_button);


//        aqVP.id(R.id.btnDelInfo).enabled(false).getImageView()
//                .setBackgroundResource(R.drawable.disable_button);
    }

    //	validations done for fields
    private boolean validateRegFields() throws Exception {
        focusEditText = null;

        if (aqVP.id(R.id.etthaicard).getText().toString().trim().length() > 0
                && aqVP.id(R.id.etthaicard).getText().toString().trim().length() < 7) {
            Toast.makeText(getApplicationContext(),
                    (getResources().getString(R.string.thayicardno_mustcontain_sevendigits)), Toast.LENGTH_LONG)
                    .show();
            aqVP.id(R.id.etthaicard).getSpinner().requestFocus();
            return false;
        } else if (aqVP.id(R.id.etthaicard).getText().toString().trim().length() > 0) {
            ArrayList<String> thayicardNo = new ArrayList<String>();
            if (thayicardNo.contains(aqVP.id(R.id.etthaicard).getText().toString())) {
                Toast.makeText(getApplicationContext(),
                        (getResources().getString(R.string.thayicardno_must_be_unique)), Toast.LENGTH_LONG).show();
                aqVP.id(R.id.etthaicard).getSpinner().requestFocus();
                aqVP.id(R.id.etthaicard).text("");
                return false;
            }

        }

        if ((aqVP.id(R.id.etwomenname).getText().toString().trim().length()) <= 0) {
            Toast.makeText(getApplicationContext(), (getResources().getString(R.string.m073)), Toast.LENGTH_LONG)
                    .show();
            focusEditText = aqVP.id(R.id.etwomenname).getEditText();
            return false;
        }

        int age = (aqVP.id(R.id.etage).getText().toString().length() > 0)
                ? Integer.parseInt(aqVP.id(R.id.etage).getText().toString()) : 0;
        if (aqVP.id(R.id.etage).getText().toString().length() <= 0) {
            Toast.makeText(getApplicationContext(), (getResources().getString(R.string.m072)), Toast.LENGTH_LONG)
                    .show();
            focusEditText = aqVP.id(R.id.etage).getEditText();
            return false;
        } else if (age == 0) {
            Toast.makeText(getApplicationContext(), (getResources().getString(R.string.m155)), Toast.LENGTH_LONG)
                    .show();
            focusEditText = aqVP.id(R.id.etage).getEditText();
            return false;
        }

        Double weight = 0.0;
        if (aqVP.id(R.id.etWomanWeight).getText().toString().trim().length() > 0)
            weight = Double.parseDouble(aqVP.id(R.id.etWomanWeight).getText().toString());
        if (aqVP.id(R.id.etWomanWeight).getText().toString().length() <= 0) {
            Toast.makeText(getApplicationContext(), (getResources().getString(R.string.m071)), Toast.LENGTH_LONG)
                    .show();

            focusEditText = aqVP.id(R.id.etWomanWeight).getEditText();
            return false;
        } else if (weight == 0) {
            Toast.makeText(getApplicationContext(), (getResources().getString(R.string.m155)), Toast.LENGTH_LONG)
                    .show();

            focusEditText = aqVP.id(R.id.etWomanWeight).getEditText();
            return false;
        }

        if (!(aqVP.id(R.id.rd_cm).isChecked() || aqVP.id(R.id.rd_feet).isChecked()
                || aqVP.id(R.id.rd_htdontknow).isChecked())) {
            Toast.makeText(getApplicationContext(), (getResources().getString(R.string.enter_height)),
                    Toast.LENGTH_LONG).show();
            focusEditText = aqVP.id(R.id.etWomanWeight).getEditText();

            return false;
        }

        int height = Integer.parseInt(aqVP.id(R.id.spnHeightNumeric).getSelectedItem().toString());
        if (height <= 0) {

            Toast.makeText(getApplicationContext(), (getResources().getString(R.string.m133)), Toast.LENGTH_LONG)
                    .show();
            return false;
        }
        if (heightUnit == 2 && (aqVP.id(R.id.etheight).getText().toString().trim().length() <= 0
                || Double.parseDouble(aqVP.id(R.id.etheight).getText().toString()) == 0)) {
            Toast.makeText(getApplicationContext(), (getResources().getString(R.string.enter_height)),
                    Toast.LENGTH_LONG).show();

            focusEditText = aqVP.id(R.id.etheight).getEditText();
            return false;
        }

        if (villageCode == 0) {

            Toast.makeText(getApplicationContext(), (getResources().getString(R.string.m184)), Toast.LENGTH_LONG)
                    .show();
            aqVP.id(R.id.etaadharcard).getEditText().requestFocus();
            return false;
        }

        if (aqVP.id(R.id.etaadharcard).getText().toString().trim().length() > 0) {

            if (aqVP.id(R.id.etaadharcard).getText().toString().trim().length() < 12) {
                Toast.makeText(getApplicationContext(),
                        (getResources().getString(R.string.aadharcardno_mustcontain_sevendigits)),
                        Toast.LENGTH_LONG).show();

                aqVP.id(R.id.etaadharcard).getSpinner().requestFocus();
                return false;
            } else {
                ArrayList<String> adharcardNo = new ArrayList<String>();

                String ad = aqVP.id(R.id.etaadharcard).getText().toString();
                if (adharcardNo.contains(ad)) {
                    Toast.makeText(getApplicationContext(),
                            (getResources().getString(R.string.adharcardno_must_be_unique)), Toast.LENGTH_LONG)
                            .show();
                    aqVP.id(R.id.etaadharcard).getSpinner().requestFocus();
                    aqVP.id(R.id.etaadharcard).text("");
                    return false;

                }
            }
        }

        /*if (aqVP.id(R.id.etphone).getText().toString().trim().length() < 10) {
            Toast.makeText(getApplicationContext(), "Please Enter Valid Mobile Number!!", Toast.LENGTH_LONG).show();
            focusEditText = aqVP.id(R.id.etphone).getEditText();
            return false;
        }*/ //08Apr2021 Bindu

        String strPh = aqVP.id(R.id.etphone).getText().toString().trim();
        if (strPh.length() > 0) {
            if (strPh.length() < 10 || strPh.length() > 12) {
                Toast.makeText(getApplicationContext(), (getResources().getString(R.string.m156)),
                        Toast.LENGTH_LONG).show();

                focusEditText = aqVP.id(R.id.etphone).getEditText();
                return false;
            }


            if (aqVP.id(R.id.spnphn).getSelectedItemPosition() == 0) {
                Toast.makeText(getApplicationContext(),
                        getResources().getString(R.string.pleaseselectmobileof),
                        Toast.LENGTH_LONG).show();
                focusEditText = aqVP.id(R.id.etphone).getEditText();
                return false;
            }
        }
        //08Apr2021 Bindu - set either of them as mandatory
        if (aqVP.id(R.id.etthaicard).getText().toString().trim().length() <= 0 && aqVP.id(R.id.etphone).getText().toString().trim().length() <= 0 && aqVP.id(R.id.etaadharcard).getText().toString().trim().length() <= 0) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.plsenteruniqueid), Toast.LENGTH_LONG).show(); //Bindu change static msg
            focusEditText = aqVP.id(R.id.etphone).getEditText();
            return false;
        }

        if (aqVP.id(R.id.etwifeageMarriage).getText().toString().length() > 0) {
            int ageAtMarriage = Integer.parseInt(aqVP.id(R.id.etwifeageMarriage).getText().toString());
            if (ageAtMarriage > age) {
                Toast.makeText(getApplicationContext(), (getResources().getString(R.string.m163)),
                        Toast.LENGTH_LONG).show();

                focusEditText = aqVP.id(R.id.etwifeageMarriage).getEditText();
                return false;
            }
        }

        if (aqVP.id(R.id.ethusageMarriage).getText().toString().trim().length() > 0
                && aqVP.id(R.id.ethusage).getText().toString().trim().length() > 0) {
            int ageAtMarriage = Integer.parseInt(aqVP.id(R.id.ethusageMarriage).getText().toString());
            if (ageAtMarriage > Integer.parseInt(aqVP.id(R.id.ethusage).getText().toString())) {
                Toast.makeText(getApplicationContext(),
                        (getResources().getString(R.string.husband_marriage_age_less_than_current_age)),
                        Toast.LENGTH_LONG).show();

                focusEditText = aqVP.id(R.id.ethusageMarriage).getEditText();
                aqVP.id(R.id.ethusageMarriage).text("");
                return false;
            }
        }

        //22Nov2019 - Bindu - max length from 12 to 9
        if (aqVP.id(R.id.etbankaccount).getText().toString().trim().length() > 0
                && (aqVP.id(R.id.etbankaccount).getText().toString().trim().length() < 9
                || aqVP.id(R.id.etbankaccount).getText().toString().trim().length() > 18)) {
            Toast.makeText(getApplicationContext(), (getResources().getString(R.string.enter_valid_account_no)),
                    Toast.LENGTH_LONG).show();


            aqVP.id(R.id.etbankaccount).getEditText().requestFocus();
            return false;
        }
        return true;
    }


    //	calculates bmi based on height and weight
    void calculateBmi() throws Exception {

        Double heightinfeet;
        double actHeight, Heightinmts = 0;
        if (heightUnit == 2 && aqVP.id(R.id.etheight).getText().toString().trim().length() > 0
                && Double.parseDouble(aqVP.id(R.id.etheight).getText().toString()) > 0) {

            heightinfeet = 0.0328084 * (Double.parseDouble(aqVP.id(R.id.etheight).getText().toString()));


            height = Integer.parseInt(heightinfeet.toString().split("\\.")[0]);
            heightD = Integer.parseInt(heightinfeet.toString().split("\\.")[1].substring(0, 1));


        } else if (heightUnit == 1) {
            height = Integer.parseInt(aqVP.id(R.id.spnHeightNumeric).getSelectedItem().toString());
            heightD = Integer.parseInt(aqVP.id(R.id.spnHeightDecimal).getSelectedItem().toString());
        } else if (heightUnit == 3) {
            height = 0;
            heightD = 0;
        }

        actHeight = Double.parseDouble(height + "." + heightD);
        Heightinmts = actHeight * 0.3048;

        weight = (aqVP.id(R.id.etWomanWeight).getText().toString().length() > 0)
                ? Double.parseDouble(aqVP.id(R.id.etWomanWeight).getText().toString()) : 0;

        if (heightUnit != 3 && Heightinmts > 0 && weight > 0) {
            double bmi = weight / (Heightinmts * Heightinmts);
            aqVP.id(R.id.tvBMI).text("" + bmi);
        } else
            aqVP.id(R.id.tvBMI).text("");

        ColorCount();
    }

    //	visibility of UI
    private void openBasicInfoAndSetFocus() throws Exception {
        aqVP.id(R.id.srlWomanBasicInfo).visible();
        aqVP.id(R.id.tvWBIHeading).visible();
        aqVP.id(R.id.btnWBD).getImageView().setBackgroundColor(getResources().getColor(R.color.gray));

        if (focusEditText != null)
            focusEditText.requestFocus();
    }

    /**
     * This is a Recursive method that traverse through the group and subgroup
     * of view Sets text
     *
     * @throws Exception
     */
    private ArrayList<View> initializeScreen(View v) throws Exception {

        if (!(v instanceof ViewGroup)) {
            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);

            // TextView - Assign Labels
            if (v.getClass() == TextView.class || v.getClass() == CheckBox.class) {
                aqVP.id(v.getId()).text((aqVP.id(v.getId()).getText().toString()));
            }
            if (v.getClass() == AppCompatRadioButton.class) {
                aqVP.id(v.getId()).getButton().setOnClickListener(this);
                aqVP.id(v.getId()).text((aqVP.id(v.getId()).getText().toString()));
            }
            // EditText hint - Assign Labels
            if (v.getClass() == EditText.class) {
                if (aqVP.id(v.getId()).getEditText().getHint() != null)
                    aqVP.id(v.getId()).getEditText().setHint((aqVP.id(v.getId()).getEditText().getHint() + ""));
            }
            // ImageButton/ImageView - OnClickListener commonClick
            if (v.getClass() == AppCompatImageButton.class || v.getClass() == AppCompatImageView.class) {
                aqVP.id(v.getId()).getImageView().setOnClickListener(this);
            }
            // ImageButton/ImageView - OnClickListener commonClick
            if (v.getClass() == AppCompatCheckBox.class || v.getClass() == AppCompatCheckBox.class) {
                aqVP.id(v.getId()).getCheckBox().setOnClickListener(this);
            }

            return viewArrayList;
        }
        if (v instanceof Spinner) {
            aqVP.id(v.getId()).itemSelected(this, "onSpinnerClicked");
        }
        if (v instanceof SearchableSpinner) {
            aqVP.id(v.getId()).itemSelected(this, "onSpinnerClicked");
        }

        ArrayList<View> result = new ArrayList<View>();

        ViewGroup viewGroup = (ViewGroup) v;
        for (int i = 0; i < viewGroup.getChildCount(); i++) {

            View child = viewGroup.getChildAt(i);

            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);
            viewArrayList.addAll(initializeScreen(child));

            result.addAll(viewArrayList);
        }
        return result;
    }

    //	check for edit applicability
    private void checkEditApplicable() throws Exception {

        if (validateRegFields()) {
            confirmAlert();
        } else {
            hideAllOptionalViews();
            openBasicInfoAndSetFocus();
        }

    }

    /**
     * Confirm Alert on Click save
     */
    private void confirmAlert() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set dialog message
        alertDialogBuilder.setMessage((getResources().getString(R.string.m099))).setCancelable(false)
                .setNegativeButton((getResources().getString(R.string.m121)),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                try {
                                    btnEditWomanData();
                                } catch (Exception e) {

                                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                                    crashlytics.log(e.getMessage());
                                }
                            }
                        })
                .setPositiveButton((getResources().getString(R.string.m122)),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    //  set to pojo class
    private void setEditProfilePojo() {

        try {
            ColorCount();
            woman = new tblregisteredwomen();
            woman.setRegEligibleCoupleNo(aqVP.id(R.id.etecno).getText().toString());//09Aug2021 Arpitha
            woman.setUserId(user.getUserId());
            woman.setRegWomenImage(wImage);
            woman.setRegUIDNo(aqVP.id(R.id.etthaicard).getText().toString());
            woman.setRegUIDType(uidType);
            woman.setRegUIDDate(aqVP.id(R.id.etthaicardregdate).getText().toString());
            woman.setRegRegistrationDate(aqVP.id(R.id.etregdate).getText().toString());
            woman.setRegHusbandName(aqVP.id(R.id.ethusbname).getText().toString());
            woman.setRegDateofBirth(aqVP.id(R.id.etdob).getText().toString());
            woman.setRegAge(aqVP.id(R.id.etage).getText().toString().length() > 0 ?
                    Integer.parseInt(aqVP.id(R.id.etage).getText().toString()) : 0);
            woman.setRegReligion(religion);
            woman.setRegPhoneNumber(aqVP.id(R.id.etphone).getText().toString());
            woman.setRegLMP(aqVP.id(R.id.etlmp).getText().toString());
            woman.setRegEDD(aqVP.id(R.id.tvEddValue).getText().toString());
            woman.setRegStatusWhileRegistration(aqVP.id(R.id.tvpregstatus).getText().toString());
            woman.setRegHusbandAge(aqVP.id(R.id.ethusage).getText().toString());
            woman.setRegHusAgeatMarriage(aqVP.id(R.id.ethusageMarriage).getText()
                    .toString());
            woman.setRegWifeAgeatMarriage(aqVP.id(R.id.etwifeageMarriage).getText().toString());
            woman.setRegMarriageDate(aqVP.id(R.id.etmarriagrdate).getText().toString());
            woman.setRegEducationHus(aqVP.id(R.id.ethusEducation).getText().toString());
            woman.setRegEducationWife(aqVP.id(R.id.etwifeEducation).getText().toString());
            woman.setRegheightUnit(heightUnit);

            if (heightUnit == 1) {
                String heightVal;
                heightNumeric = aqVP.id(R.id.spnHeightNumeric).getSelectedItemPosition() + 1;// Arpitha
                // 15Feb2018
                heightDecimal = aqVP.id(R.id.spnHeightDecimal).getSelectedItemPosition();
                heightVal = "" + ((heightNumeric) + "." + heightDecimal);
                woman.setRegHeight(heightVal);// Arpitha 15Feb2018

            } else
                woman.setRegHeight(aqVP.id(R.id.etheight).getText().toString());
            woman.setRegWomanWeight(aqVP.id(R.id.etWomanWeight).getText().toString());
            woman.setRegBloodGroup(aqVP.id(R.id.spnbloodgroup).getSelectedItemPosition());
            woman.setRegGravida(aqVP.id(R.id.ettotpreg).getText().toString().length() > 0 ?
                    Integer.parseInt(aqVP.id(R.id.ettotpreg).getText().toString()) : 0);
            woman.setRegPara(aqVP.id(R.id.ettotlivebirth).getText().toString().length() > 0 ?
                    Integer.parseInt(aqVP.id(R.id.ettotlivebirth).getText().toString()) : 0);
            woman.setRegLiveChildren(aqVP.id(R.id.etnooflivechildren).getText()
                    .toString().length() > 0 ?
                    Integer.parseInt(aqVP.id(R.id.etnooflivechildren).getText().toString()) : 0);
            woman.setRegAbortions(aqVP.id(R.id.ettotabort).getText().toString().length() > 0 ?
                    Integer.parseInt(aqVP.id(R.id.ettotabort).getText().toString()) : 0);
            woman.setRegChildMortPreg(aqVP.id(R.id.etDeadDuringPreg).getText().toString().length() > 0 ?
                    Integer.parseInt(aqVP.id(R.id.etDeadDuringPreg).getText().toString()) : 0);
            woman.setRegChildMortDel(aqVP.id(R.id.etDeadAfterDel).getText().toString().length() > 0 ?
                    Integer.parseInt(aqVP.id(R.id.etDeadAfterDel).getText().toString()) : 0);
            woman.setRegNoofHomeDeliveries(aqVP.id(R.id.ethomedel).getText().toString().length() > 0 ?
                    Integer.parseInt(aqVP.id(R.id.ethomedel).getText().toString()) : 0);
            woman.setRegNoofStillBirth(aqVP.id(R.id.etstillbirth).getText().toString().length() > 0 ?
                    Integer.parseInt(aqVP.id(R.id.etstillbirth).getText().toString()) : 0);//13May2021 Arpitha
            woman.setRegLastChildAge(aqVP.id(R.id.etlastchildage).getText().toString());  //05Aug2019 - Bindu Int to String
            woman.setRegLastChildWeight(aqVP.id(R.id.etlastchildWeight).getText().toString().length() > 0 ?
                    Integer.parseInt(aqVP.id(R.id.etlastchildWeight).getText().toString()) : 0);
            woman.setRegAadharCard(aqVP.id(R.id.etaadharcard).getText().toString());
            woman.setRegWomanName(aqVP.id(R.id.etwomenname).getText().toString());
            woman.setRegPlaceOfReg(aqVP.id(R.id.spnPlaceOfReg).getSelectedItem().toString());
            woman.setRegAddress(aqVP.id(R.id.etaddr).getText().toString());
            woman.setRegPregnantorMother(pregnantOrMother);


            //  woman.setRegLastChildGender(lastChildGender); // 05Aug2019 -Bindu - remove edit
            woman.setRegRegistrationType(regType);
            woman.setRegPregnantorMother(pregnantOrMother);


           /* if(aqVP.id(R.id.rdsc).isChecked())
                caste = "SC";
            else  if(aqVP.id(R.id.rdst).isChecked())
                caste = "ST";
            else  if(aqVP.id(R.id.rdgeneral).isChecked())
                caste = "General";
            else  if(aqVP.id(R.id.rdothercaste).isChecked())
                caste = "Other";*/


            if(aqVP.id(R.id.rdapl).isChecked())
                // aplbpl = "APL"; //12Nov2019 - Bindu - add res for save separately irrespective of lang for apl/bpl
                aplBpl = getResources().getString(R.string.aplsave);
            else  if(aqVP.id(R.id.rdbpl).isChecked())
                //aplbpl = "BPL"; //12Nov2019 - Bindu - add res for save separately irrespective of lang for  apl/bpl
                aplBpl = getResources().getString(R.string.bplsave);


            woman.setRegAPLBPL(aplBpl);
            woman.setRegCaste(caste);
            woman.setRegBankName("" + aqVP.id(R.id.spnbankname).getSelectedItem());
            woman.setRegBranchName(aqVP.id(R.id.etbranchname).getText().toString());
            woman.setRegAccountNo(aqVP.id(R.id.etbankaccount).getText().toString());
            woman.setRegIFSCCode(aqVP.id(R.id.etifsc).getText().toString());
            //12Nov2019 - Bindu - Mobile No of from english array
            //woman.setRegWhoseMobileNo(aqVP.id(R.id.spnphn).getSelectedItem().toString());
            String[] phn = getResources().getStringArray(R.array.phnnosave);
            String mobof = "";
            mobof = phn[aqVP.id(R.id.spnphn).getSelectedItemPosition()];
            woman.setRegWhoseMobileNo(mobof);

            woman.setRegComments(aqVP.id(R.id.etcomments).getText().toString());
            woman.setRegAreaName(aqVP.id(R.id.etareaname).getText().toString());
            woman.setRegPincode(aqVP.id(R.id.etpincode)
                    .getText().toString());
            woman.setRegFinancialYear(DateTimeUtil.getFinYear());
            woman.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
            woman.setRegRationCard(aqVP.id(R.id.etrationcard).getText().toString());
            woman.setRegVoterId(aqVP.id(R.id.etvoterid).getText().toString());
            woman.setRegVillage(villageCode);
            woman.setRegCurrHealthRiskFactors(healthIssuesStringStore);
            woman.setRegPrevHealthRiskFactors(prevDelStringStore);

            String familyHistoryRisk = "";

            if (aqVP.id(R.id.chktwins).isChecked())
                familyHistoryRisk = familyHistoryRisk + "Twins";
            if (aqVP.id(R.id.chkMentallyRetarded).isChecked())
                familyHistoryRisk = familyHistoryRisk + ",Mental Retardation";
            if (aqVP.id(R.id.chkdeficiency).isChecked())
                familyHistoryRisk = familyHistoryRisk + ",Physical Deformation";
            if (aqVP.id(R.id.chkdiabetesfamily).isChecked())
                familyHistoryRisk = familyHistoryRisk + ",Diabetes";
            if (aqVP.id(R.id.chkmarriedinrelation).isChecked())
                familyHistoryRisk = familyHistoryRisk + ",Married in Relation";
            if (aqVP.id(R.id.chkBPFamily).isChecked())
                familyHistoryRisk = familyHistoryRisk + ",High BP";

            familyHistoryRisk = familyHistoryRisk.replace(",,", "");

            if (aqVP.id(R.id.etothersfh).getText().toString().trim().length() > 0
                    && familyHistoryRisk.trim().length() > 0)
                familyHistoryRisk = familyHistoryRisk + "," + aqVP.id(R.id.etothersfh).getText().toString();
            else if (aqVP.id(R.id.etothersfh).getText().toString().trim().length() > 0)
                familyHistoryRisk = aqVP.id(R.id.etothersfh).getText().toString();
            else
                familyHistoryRisk = familyHistoryRisk;

            woman.setRegFamilyHistoryRiskFactors(familyHistoryRisk);

            woman.setRegOtherhealthissue(aqVP.id(R.id.etothershs).getText().toString());
            woman.setRegOtherfamilyhistory(aqVP.id(R.id.etothersfh).getText().toString());
            woman.setRegOthersprevpreg(aqVP.id(R.id.etothersprev).getText().toString());

            if (redCnt > 3)
                woman.setRegAmberOrRedColorCode(4);
            else if (redCnt > 2)
                woman.setRegAmberOrRedColorCode(3);
            else if (redCnt > 1)
                woman.setRegAmberOrRedColorCode(2);
            else if (redCnt == 1)
                woman.setRegAmberOrRedColorCode(1);
            else if (amberCnt == 2)
                woman.setRegAmberOrRedColorCode(11);
            else if (amberCnt == 1 && redCnt == 0)
                woman.setRegAmberOrRedColorCode(9);
            else
                woman.setRegAmberOrRedColorCode(0);

            int complPreg = 0;
            if (aqVP.id(R.id.chkcomplicatedpreg).isChecked())
                complPreg = 1;
            else
                complPreg = 0;

            woman.setRegComplicatedpreg(complPreg);

            if (healthIssuesStringStore.length() > 0 && prevDelStringStore.length() > 0)
                woman.setRegriskFactors(healthIssuesStringStore + "\n" + prevDelStringStore);
            else if (healthIssuesStringStore.length() > 0 && prevDelStringStore.length() <= 0)
                woman.setRegriskFactors(healthIssuesStringStore);
            else if (healthIssuesStringStore.length() <= 0 && prevDelStringStore.length() > 0)
                woman.setRegriskFactors(prevDelStringStore);
            else
                woman.setRegriskFactors("");

            woman.setRegCHriskfactors(healthIssuesStringStore);
            woman.setRegPHriskfactors(prevDelStringStore);
            //27Sep2019 - Bindu- homevisit compl and chk if once cemoc then always cemoc
            //15Nov2019 - Bindu - check compl also along with del place
            if(regData.getIsCompl() != null && regData.getIsCompl().equalsIgnoreCase(getResources().getString(R.string.yesshortform)) &&
                    regData.getRegrecommendedPlaceOfDelivery() != null && regData.getRegrecommendedPlaceOfDelivery().equalsIgnoreCase(getResources().getString(R.string.strcemoc)))
                woman.setRegrecommendedPlaceOfDelivery(regData.getRegrecommendedPlaceOfDelivery());
            else
                woman.setRegrecommendedPlaceOfDelivery(aqVP.id(R.id.txtReferal).getText().toString());

            woman.setRegComments(aqVP.id(R.id.etcomments).getText().toString());

            woman.setRegUserType(appState.userType);


//26Sep2019 - Bindu - Add Eligible couple and Category
            woman.setRegEligibleCoupleNo("" + aqVP.id(R.id.etecno).getText().toString());
            if (aqVP.id(R.id.rdnontribal).isChecked()) {
//                category = getResources().getString(R.string.strnontribalsave); //12Nov2019 - Bindu - Add save string
                category = "Non-tribal"; //Ramesh 17-Aug-2021
                woman.setRegCategory(category);
                woman.setRegCaste("");
            } else if (aqVP.id(R.id.rdtribal).isChecked()) {
//                category = getResources().getString(R.string.strtribalsave); //12Nov2019 - Bindu - Add save string
                category = "Tribal";//Ramesh 17-Aug-2021
                woman.setRegCategory(category);

//26Sep2019 - Bindu - Add caste
                /*if (aqVP.id(R.id.rdjenukuruba).isChecked())
                    caste = getResources().getString(R.string.strjenukurubasave); //12Nov2019 - Bindu - get eng save strings
                else if (aqVP.id(R.id.rdbettakuruba).isChecked())
                    caste = getResources().getString(R.string.strbettakurubasave); //12Nov2019 - Bindu - get eng save strings
                else if (aqVP.id(R.id.rdsoliga).isChecked())
                    caste = getResources().getString(R.string.strsoligasave); //12Nov2019 - Bindu - get eng save strings
                else if (aqVP.id(R.id.rdyarawa).isChecked())
                    caste = getResources().getString(R.string.stryarawasave); //12Nov2019 - Bindu - get eng save strings
                else if (aqVP.id(R.id.rdphaniya).isChecked())
                    caste = getResources().getString(R.string.strphaniyasave); //12Nov2019 - Bindu - get eng save strings*/


                //09Apr2021 Bindu - set tribal caste
                caste = aqVP.id(R.id.spntribalcaste).getSelectedItem().toString();
                woman.setRegCaste(caste);
            }


        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }

    }

    //  update details to table
    void btnEditWomanData() {

        try {
            ColorCount();
            calTotPregnancies();
            setEditProfilePojo();

            final TransactionHeaderRepository transRepo = new TransactionHeaderRepository(databaseHelper);
           transId = transRepo.iCreateNewTrans(appState.sessionUserId, databaseHelper);

            TransactionManager.callInTransaction(databaseHelper.getConnectionSource(), new Callable<Void>() {
                public Void call() throws Exception {//29Aug2019 Arpitha - transaction manager

                    String upSql = checkForAuditTrailNew(transId);
                    if (upSql != null) {   //10Aug2019 - Bindu - check for update st and then cal update, else exit to reg women
                        transRepo.iNewRecordTransNew(appState.sessionUserId, transId, "tblaudittrail", databaseHelper);


                        WomanRepository womanRepo = new WomanRepository(databaseHelper);
                        int sql = womanRepo.updateWomenRegDataNew(woman.getUserId(), upSql, transId, editedValues, databaseHelper);
                        if (sql > 0) {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.updatewoman), Toast.LENGTH_LONG).show();

                            // MainMenuActivity.callSyncMtd(); //12Aug2019 - Bindu - Comment autosync

                            //                            14May2021 Arpitha
                            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ViewProfileActivity.this);
                            if (checkSimState() == TelephonyManager.SIM_STATE_READY &&
                                    prefs.getBoolean("sms", false)
                                    && (!(regData.getRegriskFactors().equalsIgnoreCase
                                    (woman.getRegriskFactors()))) &&  regData.getRegPregnantorMother() == 1) { //23May2021 Bindu add preg cond
                                sendSMS(transRepo, databaseHelper);
                            }else
                            {
                                Intent exit = new Intent(ViewProfileActivity.this, RegisteredWomenActionTabs.class);
                                exit.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                startActivity(exit);
                            }









                        } else {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.m113), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha
                            deleteImages();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.nochangesmadetoedit), Toast.LENGTH_LONG).show();
                        Intent exit = new Intent(ViewProfileActivity.this, RegisteredWomenActionTabs.class);
                        exit.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        startActivity(exit);
                    }
                    return null;
                }
            });

           /* TransactionManager.callInTransaction(databaseHelper.getConnectionSource(), new Callable<Void>() {
                public Void call() throws Exception {//29Aug2019 Arpitha - transaction manager
                    String upSql = checkForAuditTrailNew(transId);
                    if (upSql != null) {   //10Aug2019 - Bindu - check for update st and then cal update, else exit to reg women
                        transRepo.iNewRecordTransNew(user.getUserId(), transId, "tblaudittrail", databaseHelper);

                        WomanRepository womanRepository = new WomanRepository(databaseHelper);
                        int sql = womanRepository.updateWomenRegDataNew(woman.getUserId(), upSql, transId, editedValues, databaseHelper);

                        if (sql > 0) {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.updatewoman), Toast.LENGTH_LONG).show();
                            Intent exit = new Intent(ViewProfileActivity.this, RegisteredWomenActionTabs.class);
                            exit.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(exit);
                        } else {
                            deleteImages();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.nochangesmadetoedit), Toast.LENGTH_LONG).show();
                        Intent exit = new Intent(ViewProfileActivity.this, RegisteredWomenActionTabs.class);
                        exit.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        startActivity(exit);
                    }
                    return null;
                }
            });*/
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.m113), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }


    /**
     * Setting AuditTrail pojo, calls - InserttblAuditTrail()
     *
     * @param transId
     */
    private boolean InserttblAuditTrail(String ColumnName, String Old_Value, String
            New_Value, int transId, byte[] image) throws Exception {

        APJ = new AuditPojo();
        APJ.setUserId(user.getUserId());
        APJ.setWomanId(regData.getWomanId());
        APJ.setTblName("tblregisteredwomen");
        APJ.setDateChanged(DateTimeUtil.getTodaysDate());
        APJ.setColumnName(ColumnName);
        APJ.setOld_Value(Old_Value);
        APJ.setNew_Value(New_Value);
        APJ.setTransId(transId);
//15Aug2019 - Bindu - set recordcreateddate
        APJ.setRecordcreateddate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());

        AuditRepository auditRepository = new AuditRepository(databaseHelper);
        return auditRepository.insertTotblAuditTrailNew(APJ, databaseHelper);

    }

    // insert to audi trail table
    private String checkForAuditTrailNew(int transId) throws Exception {

        String upSql = null;
        String addString = "";
        editedValues = "";

        //09Apr2021 - Bindu UID, type, Date
        if (!regData.getRegUIDType().equalsIgnoreCase(woman.getRegUIDType())) {
            if (addString == "")
                addString = addString + " regUIDType = " + (char) 34 + woman.getRegUIDType() + (char) 34 + "";
            else
                addString = addString + " ,regUIDType = " + (char) 34 + woman.getRegUIDType() + (char) 34 + "";
            InserttblAuditTrail("regUIDType",
                    String.valueOf(regData.getRegUIDType()),
                    String.valueOf(woman.getRegUIDType()),
                    transId, null);
        }

        if (!regData.getRegUIDNo().equalsIgnoreCase(woman.getRegUIDNo())) {
            if (addString == "")
                addString = addString + " regUIDNo = " + (char) 34 + woman.getRegUIDNo() + (char) 34 + "";
            else
                addString = addString + " ,regUIDNo = " + (char) 34 + woman.getRegUIDNo() + (char) 34 + "";
            InserttblAuditTrail("regUIDNo",
                    String.valueOf(regData.getRegUIDNo()),
                    String.valueOf(woman.getRegUIDNo()),
                    transId, null);
        }

        if (!regData.getRegUIDDate().equalsIgnoreCase(woman.getRegUIDDate())) {
            if (addString == "")
                addString = addString + " regUIDDate = " + (char) 34 + woman.getRegUIDDate() + (char) 34 + "";
            else
                addString = addString + " ,regUIDDate = " + (char) 34 + woman.getRegUIDDate() + (char) 34 + "";
            InserttblAuditTrail("regUIDDate",
                    String.valueOf(regData.getRegUIDDate()),
                    String.valueOf(woman.getRegUIDDate()),
                    transId, null);
        }


        //26Se2019 - Bindu - Eligible couple
        if (!regData.getRegEligibleCoupleNo().equalsIgnoreCase(woman.getRegEligibleCoupleNo())) {
            if (addString == "")
                addString = addString + " regEligibleCoupleNo = " + woman.getRegEligibleCoupleNo() + "";
            else
                addString = addString + " ,regEligibleCoupleNo = " + woman.getRegEligibleCoupleNo() + "";
            InserttblAuditTrail("regEligibleCoupleNo",
                    String.valueOf(regData.getRegEligibleCoupleNo()),
                    String.valueOf(woman.getRegEligibleCoupleNo()),
                    transId, null);
        }

        if (!regData.getRegWomanName().equalsIgnoreCase
                (aqVP.id(R.id.etwomenname).getText().toString())) {
            addString = "regWomanName = " + (char) 34 + woman.getRegWomanName() + (char) 34 + "";
            InserttblAuditTrail("regWomanName",
                    regData.getRegWomanName(), woman.getRegWomanName(),
                    transId, null);
        }


        if (regData.getRegWomenImage() != null &&
                woman.getRegWomenImage() != null &&


                !regData.getRegWomenImage().equalsIgnoreCase(woman.getRegWomenImage())) {

            if (addString == "")
                addString = addString + " regWomenImage = '"+woman.getRegWomenImage()+"'";
            else
                addString = addString + " ,regWomenImage = '"+woman.getRegWomenImage()+"'";
            InserttblAuditTrail("regWomenImage",
                    "old image",
                    "New Image",
                    transId, null);
        }


        if (regData.getRegAge() != woman.getRegAge()) {

            if (addString == "")
                addString = addString + " regAge = " + woman.getRegAge() + " ";
            else
                addString = addString + " ,regAge = " + woman.getRegAge() + " ";
            InserttblAuditTrail("regAge",
                    String.valueOf(regData.getRegAge()),
                    String.valueOf(woman.getRegAge()),
                    transId, null);
        }


        String olddob = regData.getRegDateofBirth();
        String newdob = woman.getRegDateofBirth();
        if (!olddob.equalsIgnoreCase(newdob)) {

            if (addString == "")
                addString = addString + " regDateofBirth = " + (char) 34 + woman.getRegDateofBirth() + (char) 34 + "";
            else
                addString = addString + " ,regDateofBirth = " + (char) 34 + woman.getRegDateofBirth() + (char) 34 + "";
            InserttblAuditTrail("regDateofBirth",
                    String.valueOf(regData.getRegDateofBirth()),
                    String.valueOf(woman.getRegDateofBirth()),
                    transId, null);
        }


        if (!regData.getRegWomanWeight().equalsIgnoreCase(woman.getRegWomanWeight())) {

            if (addString == "")
                addString = addString + " regWomanWeight = " + woman.getRegWomanWeight() + "";
            else
                addString = addString + " ,regWomanWeight = " + woman.getRegWomanWeight() + "";
            InserttblAuditTrail("regWomanWeight",
                    String.valueOf(regData.getRegWomanWeight()),
                    String.valueOf(woman.getRegWomanWeight()),
                    transId, null);
        }


        if (regData.getRegheightUnit() != woman.getRegheightUnit()) {

            if (addString == "")
                addString = addString + " regheightUnit = " + woman.getRegheightUnit() + "";
            else
                addString = addString + " ,regheightUnit = " + woman.getRegheightUnit() + "";
            InserttblAuditTrail("regheightUnit",
                    String.valueOf(regData.getRegheightUnit()),
                    String.valueOf(woman.getRegheightUnit()),
                    transId, null);
        }


        if (!regData.getRegHeight().equalsIgnoreCase(woman.getRegHeight())) {

            if (addString == "")
                addString = addString + " regHeight = " + (char) 34 + woman.getRegHeight() + (char) 34 + "";
            else
                addString = addString + " ,regHeight = " + (char) 34 + woman.getRegHeight() + (char) 34 + "";
            InserttblAuditTrail("regHeight",
                    String.valueOf(regData.getRegHeight()),
                    String.valueOf(woman.getRegHeight()),
                    transId, null);
        }

        if (!regData.getRegAadharCard().equalsIgnoreCase(woman.getRegAadharCard())) {

            if (addString == "")
                addString = addString + " regAadharCard = " + woman.getRegAadharCard() + "";
            else
                addString = addString + " ,regAadharCard = " + woman.getRegAadharCard() + "";
            InserttblAuditTrail("regAadharCard",
                    String.valueOf(regData.getRegAadharCard()),
                    String.valueOf(woman.getRegAadharCard()),
                    transId, null);
        }

        if (!regData.getRegWhoseMobileNo().equalsIgnoreCase(woman.getRegWhoseMobileNo())) {

            if (addString == "")
                addString = addString + " regWhoseMobileNo = " + (char) 34 + woman.getRegWhoseMobileNo() + (char) 34 + "";
            else
                addString = addString + " ,regWhoseMobileNo = " + (char) 34 + woman.getRegWhoseMobileNo() + (char) 34 + "";
            InserttblAuditTrail("regWhoseMobileNo",
                    String.valueOf(regData.getRegWhoseMobileNo()),
                    String.valueOf(woman.getRegWhoseMobileNo()),
                    transId, null);
        }


        if (!regData.getRegPhoneNumber().equalsIgnoreCase(woman.getRegPhoneNumber())) {

            if (addString == "")
                addString = addString + " regPhoneNumber = " + woman.getRegPhoneNumber() + "";
            else
                addString = addString + " ,regPhoneNumber = " + woman.getRegPhoneNumber() + "";
            InserttblAuditTrail("regPhoneNumber",
                    String.valueOf(regData.getRegPhoneNumber()),
                    String.valueOf(woman.getRegPhoneNumber()),
                    transId, null);
        }


        if (!regData.getRegAddress().equalsIgnoreCase(woman.getRegAddress())) {

            if (addString == "")
                addString = addString + " regAddress = " + (char) 34 + woman.getRegAddress() + (char) 34 + "";
            else
                addString = addString + " ,regAddress = " + (char) 34 + woman.getRegAddress() + (char) 34 + "";
            InserttblAuditTrail("regAddress",
                    String.valueOf(regData.getRegAddress()),
                    String.valueOf(woman.getRegAddress()),
                    transId, null);
        }


        if (!regData.getRegUserType().equalsIgnoreCase(woman.getRegUserType())) {

            if (addString == "")
                addString = addString + " regUserType = " + (char) 34 + woman.getRegUserType() + (char) 34 + "";
            else
                addString = addString + " ,regUserType = " + (char) 34 + woman.getRegUserType() + (char) 34 + "";
            InserttblAuditTrail("regUserType",
                    regData.getRegUserType(),
                    woman.getRegUserType(),
                    transId, null);
        }


        if (!regData.getRegAreaName().equalsIgnoreCase(woman.getRegAreaName())) {

            if (addString == "")
                addString = addString + " regAreaName = " + (char) 34 + woman.getRegAreaName() + (char) 34 + "";
            else
                addString = addString + " ,regAreaName = " + (char) 34 + woman.getRegAreaName() + (char) 34 + "";
            InserttblAuditTrail("regAreaName",
                    String.valueOf(regData.getRegAreaName()),
                    String.valueOf(woman.getRegAreaName()),
                    transId, null);
        }


        if (regData.getRegPincode() != null && !regData.getRegPincode().equalsIgnoreCase(woman.getRegPincode())) {

            if (addString == "")
                addString = addString + " regPincode = " + (char) 34 + woman.getRegPincode() + (char) 34 + "";
            else
                addString = addString + " ,regPincode = " + (char) 34 + woman.getRegPincode() + (char) 34 + "";
            InserttblAuditTrail("regPincode",
                    String.valueOf(regData.getRegPincode()),
                    String.valueOf(woman.getRegPincode()),
                    transId, null);
        }


        if (woman.getRegReligion() != null && !woman.getRegReligion().
                equalsIgnoreCase(regData.getRegReligion())) {

            if (addString == "")
                addString = addString + " regReligion = " + (char) 34 + woman.getRegReligion() + (char) 34 + "";
            else
                addString = addString + " ,regReligion = " + (char) 34 + woman.getRegReligion() + (char) 34 + "";
            InserttblAuditTrail("regReligion",
                    String.valueOf(regData.getRegReligion()),
                    String.valueOf(woman.getRegReligion()),
                    transId, null);
        }

//26Sep2019 - Bindu - add category
        if (woman.getRegCategory() != null && !woman.getRegCategory()
                .equalsIgnoreCase(regData.getRegCategory())) {

            if (addString == "")
                addString = addString + " regCategory = " + (char) 34 + woman.getRegCategory() + (char) 34 + "";
            else
                addString = addString + " ,regCategory = " + (char) 34 + woman.getRegCategory() + (char) 34 + "";
            InserttblAuditTrail("regCategory",
                    String.valueOf(regData.getRegCategory()),
                    String.valueOf(woman.getRegCategory()),
                    transId, null);
        }

        if (woman.getRegCaste() != null && !woman.getRegCaste()
                .equalsIgnoreCase(regData.getRegCaste())) {

            if (addString == "")
                addString = addString + " regCaste = " + (char) 34 + woman.getRegCaste() + (char) 34 + "";
            else
                addString = addString + " ,regCaste = " + (char) 34 + woman.getRegCaste() + (char) 34 + "";
            InserttblAuditTrail("regCaste",
                    String.valueOf(regData.getRegCaste()),
                    String.valueOf(woman.getRegCaste()),
                    transId, null);
        }


        if (woman.getRegAPLBPL() != null && !woman.getRegAPLBPL()
                .equalsIgnoreCase(regData.getRegAPLBPL())) {

            if (addString == "")
                addString = addString + " regAPLBPL = " + (char) 34 + woman.getRegAPLBPL() + (char) 34 + "";
            else
                addString = addString + " ,regAPLBPL = " + (char) 34 + woman.getRegAPLBPL() + (char) 34 + "";
            InserttblAuditTrail("regAPLBPL",
                    String.valueOf(regData.getRegAPLBPL()),
                    String.valueOf(woman.getRegAPLBPL()),
                    transId, null);
        }


        if (regData.getRegVoterId() != null && !regData.getRegVoterId().equalsIgnoreCase(woman.getRegVoterId())) {

            if (addString == "")
                addString = addString + " regVoterId = " + (char) 34 + woman.getRegVoterId() + (char) 34 + "";
            else
                addString = addString + " ,regVoterId = " + (char) 34 + woman.getRegVoterId() + (char) 34 + "";
            InserttblAuditTrail("regVoterId",
                    String.valueOf(regData.getRegVoterId()),
                    String.valueOf(woman.getRegVoterId()),
                    transId, null);
        }

        if (regData.getRegEducationWife() != null && !regData.getRegEducationWife().equalsIgnoreCase(woman.getRegEducationWife())) {

            if (addString == "")
                addString = addString + " regEducationWife = " + (char) 34 + woman.getRegEducationWife() + (char) 34 + "";
            else
                addString = addString + " ,regEducationWife = " + (char) 34 + woman.getRegEducationWife() + (char) 34 + "";
            InserttblAuditTrail("regEducationWife",
                    String.valueOf(regData.getRegEducationWife()),
                    String.valueOf(woman.getRegEducationWife()),
                    transId, null);
        }


        if (regData.getRegHusbandName() != null && !regData.getRegHusbandName().equalsIgnoreCase(woman.getRegHusbandName())) {

            if (addString == "")
                addString = addString + " regHusbandName = " + (char) 34 + woman.getRegHusbandName() + (char) 34 + "";
            else
                addString = addString + " ,regHusbandName = " + (char) 34 + woman.getRegHusbandName() + (char) 34 + "";
            InserttblAuditTrail("regHusbandName",
                    String.valueOf(regData.getRegHusbandName()),
                    String.valueOf(woman.getRegHusbandName()),
                    transId, null);
        }


        if (regData.getRegHusbandAge() != null && !regData.getRegHusbandAge().equalsIgnoreCase(woman.getRegHusbandAge())) {

            if (addString == "")
                addString = addString + " regHusbandAge = " + (char) 34 + woman.getRegHusbandAge() + (char) 34 + "";
            else
                addString = addString + " ,regHusbandAge = " + (char) 34 + woman.getRegHusbandAge() + (char) 34 + "";
            InserttblAuditTrail("regHusbandAge",
                    String.valueOf(regData.getRegHusbandAge()),
                    String.valueOf(woman.getRegHusbandAge()),
                    transId, null);
        }


        if (regData.getRegEducationHus() != null && !regData.getRegEducationHus().equalsIgnoreCase(woman.getRegEducationHus())) {

            if (addString == "")
                addString = addString + " regEducationHus = " + (char) 34 + woman.getRegEducationHus() + (char) 34 + "";
            else
                addString = addString + " ,regEducationHus = " + (char) 34 + woman.getRegEducationHus() + (char) 34 + "";
            InserttblAuditTrail("regEducationHus",
                    String.valueOf(regData.getRegEducationHus()),
                    String.valueOf(woman.getRegEducationHus()),
                    transId, null);
        }


        if (regData.getRegMarriageDate() != null && !regData.getRegMarriageDate().equalsIgnoreCase(woman.getRegMarriageDate())) {

            if (addString == "")
                addString = addString + " regMarriageDate = " + (char) 34 + woman.getRegMarriageDate() + (char) 34 + "";
            else
                addString = addString + " ,regMarriageDate = " + (char) 34 + woman.getRegMarriageDate() + (char) 34 + "";
            InserttblAuditTrail("regMarriageDate",
                    String.valueOf(regData.getRegMarriageDate()),
                    String.valueOf(woman.getRegMarriageDate()),
                    transId, null);
        }


        if (regData.getRegHusAgeatMarriage() != null && !regData.getRegHusAgeatMarriage().equalsIgnoreCase(woman.getRegHusAgeatMarriage())) {

            if (addString == "")
                addString = addString + " regHusAgeatMarriage = " + (char) 34 + woman.getRegHusAgeatMarriage() + (char) 34 + "";
            else
                addString = addString + " ,regHusAgeatMarriage = " + (char) 34 + woman.getRegHusAgeatMarriage() + (char) 34 + "";
            InserttblAuditTrail("regHusAgeatMarriage",
                    String.valueOf(regData.getRegHusAgeatMarriage()),
                    String.valueOf(woman.getRegHusAgeatMarriage()),
                    transId, null);
        }


        if (regData.getRegWifeAgeatMarriage() != null && !regData.getRegWifeAgeatMarriage().equalsIgnoreCase(woman.getRegWifeAgeatMarriage())) {


            if (addString == "")
                addString = addString + " regWifeAgeatMarriage = " + (char) 34 + woman.getRegWifeAgeatMarriage() + (char) 34 + "";
            else
                addString = addString + " ,regWifeAgeatMarriage = " + (char) 34 + woman.getRegWifeAgeatMarriage() + (char) 34 + "";
            InserttblAuditTrail("regWifeAgeatMarriage",
                    String.valueOf(regData.getRegWifeAgeatMarriage()),
                    String.valueOf(woman.getRegWifeAgeatMarriage()),
                    transId, null);
        }


        if (regData.getRegBankName() != null && !regData.getRegBankName().equalsIgnoreCase(woman.getRegBankName())) {

            if (addString == "")
                addString = addString + " regBankName = " + (char) 34 + woman.getRegBankName() + (char) 34 + "";
            else
                addString = addString + " ,regBankName = " + (char) 34 + woman.getRegBankName() + (char) 34 + "";
            InserttblAuditTrail("regBankName",
                    String.valueOf(regData.getRegBankName()),
                    String.valueOf(woman.getRegBankName()),
                    transId, null);
        }

        if (regData.getRegBranchName() != null && !regData.getRegBranchName().equalsIgnoreCase(woman.getRegBranchName())) {

            if (addString == "")
                addString = addString + " regBranchName = " + (char) 34 + woman.getRegBranchName() + (char) 34 + "";
            else
                addString = addString + " ,regBranchName = " + (char) 34 + woman.getRegBranchName() + (char) 34 + "";
            InserttblAuditTrail("regBranchName",
                    String.valueOf(regData.getRegBranchName()),
                    String.valueOf(woman.getRegBranchName()),
                    transId, null);
        }


        if (regData.getRegAccountNo() != null && !regData.getRegAccountNo().equalsIgnoreCase(woman.getRegAccountNo())) {

            if (addString == "")
                addString = addString + " regAccountNo = " + (char) 34 + woman.getRegAccountNo() + (char) 34 + "";
            else
                addString = addString + " ,regAccountNo = " + (char) 34 + woman.getRegAccountNo() + (char) 34 + "";
            InserttblAuditTrail("regAccountNo",
                    String.valueOf(regData.getRegAccountNo()),
                    String.valueOf(woman.getRegAccountNo()),
                    transId, null);
        }


        if (regData.getRegIFSCCode() != null && !regData.getRegIFSCCode().equalsIgnoreCase(woman.getRegIFSCCode())) {

            if (addString == "")
                addString = addString + " regIFSCCode = " + (char) 34 + woman.getRegIFSCCode() + (char) 34 + "";
            else
                addString = addString + " ,regIFSCCode = " + (char) 34 + woman.getRegIFSCCode() + (char) 34 + "";
            InserttblAuditTrail("regIFSCCode",
                    String.valueOf(regData.getRegIFSCCode()),
                    String.valueOf(woman.getRegIFSCCode()),
                    transId, null);
        }


        if (regData.getRegComments() != null && !regData.getRegComments().equalsIgnoreCase(woman.getRegComments())) {

            if (addString == "")
                addString = addString + " regComments = " + (char) 34 + woman.getRegComments() + (char) 34 + "";
            else
                addString = addString + " ,regComments = " + (char) 34 + woman.getRegComments() + (char) 34 + "";
            InserttblAuditTrail("regComments",
                    String.valueOf(regData.getRegComments()),
                    String.valueOf(woman.getRegComments()),
                    transId, null);
        }


        if (regData.getRegCHriskfactors() != null && !regData.getRegCHriskfactors().equalsIgnoreCase(woman.getRegCHriskfactors())) {

            if (addString == "")
                addString = addString + " regCHriskfactors = " + (char) 34 + woman.getRegCHriskfactors() + (char) 34 + "";
            else
                addString = addString + " ,regCHriskfactors = " + (char) 34 + woman.getRegCHriskfactors() + (char) 34 + "";
            InserttblAuditTrail("regCHriskfactors",
                    String.valueOf(regData.getRegCHriskfactors()),
                    String.valueOf(woman.getRegCHriskfactors()),
                    transId, null);
        }


        if (regData.getRegPHriskfactors() != null && !regData.getRegPHriskfactors().equalsIgnoreCase(woman.getRegPHriskfactors())) {

            if (addString == "")
                addString = addString + " regPHriskfactors = " + (char) 34 + woman.getRegPHriskfactors() + (char) 34 + "";
            else
                addString = addString + " ,regPHriskfactors = " + (char) 34 + woman.getRegPHriskfactors() + (char) 34 + "";
            InserttblAuditTrail("regPHriskfactors",
                    String.valueOf(regData.getRegPHriskfactors()),
                    String.valueOf(woman.getRegPHriskfactors()),
                    transId, null);
        }


        if (regData.getRegriskFactors() != null && !regData.getRegriskFactors().equalsIgnoreCase(woman.getRegriskFactors())) {

            if (addString == "")
                addString = addString + " regriskFactors = " + (char) 34 + woman.getRegriskFactors() + (char) 34 + "";
            else
                addString = addString + " ,regriskFactors = " + (char) 34 + woman.getRegriskFactors() + (char) 34 + "";
            InserttblAuditTrail("regriskFactors",
                    String.valueOf(regData.getRegriskFactors()),
                    String.valueOf(woman.getRegriskFactors()),
                    transId, null);
        }


        if (regData.getRegComplicatedpreg() != woman.getRegComplicatedpreg()) {

            if (addString == "")
                addString = addString + " regComplicatedpreg = " + woman.getRegComplicatedpreg() + "";
            else
                addString = addString + " ,regComplicatedpreg = " + woman.getRegComplicatedpreg() + "";
            InserttblAuditTrail("regComplicatedpreg",
                    String.valueOf(regData.getRegComplicatedpreg()),
                    String.valueOf(woman.getRegComplicatedpreg()),
                    transId, null);
        }

        if (regData.getRegrecommendedPlaceOfDelivery() != null && !regData.getRegrecommendedPlaceOfDelivery().equalsIgnoreCase(woman.getRegrecommendedPlaceOfDelivery())) {

            if (addString == "")
                addString = addString + " regrecommendedPlaceOfDelivery = " + (char) 34 + woman.getRegrecommendedPlaceOfDelivery() + (char) 34 + "";
            else
                addString = addString + " ,regrecommendedPlaceOfDelivery = " + (char) 34 + woman.getRegrecommendedPlaceOfDelivery() + (char) 34 + "";
            InserttblAuditTrail("regrecommendedPlaceOfDelivery",
                    String.valueOf(regData.getRegrecommendedPlaceOfDelivery()),
                    String.valueOf(woman.getRegrecommendedPlaceOfDelivery()),
                    transId, null);
        }


        if (regData.getRegAmberOrRedColorCode() != woman.getRegAmberOrRedColorCode()) {

            if (addString == "")
                addString = addString + " regAmberOrRedColorCode = " + woman.getRegAmberOrRedColorCode() + "";
            else
                addString = addString + " ,regAmberOrRedColorCode = " + woman.getRegAmberOrRedColorCode() + "";
            InserttblAuditTrail("regAmberOrRedColorCode",
                    String.valueOf(regData.getRegAmberOrRedColorCode()),
                    String.valueOf(woman.getRegAmberOrRedColorCode()),
                    transId, null);
        }


        if (regData.getRegOthersprevpreg() != null && !regData.getRegOthersprevpreg().equalsIgnoreCase(woman.getRegOthersprevpreg())) {

            if (addString == "")
                addString = addString + " regOthersprevpreg = " + (char) 34 + woman.getRegOthersprevpreg() + (char) 34 + "";
            else
                addString = addString + " ,regOthersprevpreg = " + (char) 34 + woman.getRegOthersprevpreg() + (char) 34 + "";
            InserttblAuditTrail("regOthersprevpreg",
                    String.valueOf(regData.getRegOthersprevpreg()),
                    String.valueOf(woman.getRegOthersprevpreg()),
                    transId, null);
        }


        if (regData.getRegOtherhealthissue() != null && !regData.getRegOtherhealthissue().equalsIgnoreCase(woman.getRegOtherhealthissue())) {

            if (addString == "")
                addString = addString + " regOtherhealthissue = " + (char) 34 + woman.getRegOtherhealthissue() + (char) 34 + "";
            else
                addString = addString + " ,regOtherhealthissue = " + (char) 34 + woman.getRegOtherhealthissue() + (char) 34 + "";
            InserttblAuditTrail("regOtherhealthissue",
                    String.valueOf(regData.getRegOtherhealthissue()),
                    String.valueOf(woman.getRegOtherhealthissue()),
                    transId, null);
        }


        if (regData.getRegOtherfamilyhistory() != null && !regData.getRegOtherfamilyhistory().equalsIgnoreCase(woman.getRegOtherfamilyhistory())) {

            if (addString == "")
                addString = addString + " regOtherfamilyhistory = " + (char) 34 + woman.getRegOtherfamilyhistory() + (char) 34 + "";
            else
                addString = addString + " ,regOtherfamilyhistory = " + (char) 34 + woman.getRegOtherfamilyhistory() + (char) 34 + "";
            InserttblAuditTrail("regOtherfamilyhistory",
                    String.valueOf(regData.getRegOtherfamilyhistory()),
                    String.valueOf(woman.getRegOtherfamilyhistory()),
                    transId, null);
        }


        if (regData.getRegCurrHealthRiskFactors() != null && !regData.getRegCurrHealthRiskFactors().equalsIgnoreCase(woman.getRegCurrHealthRiskFactors())) {

            if (addString == "")
                addString = addString + " regCurrHealthRiskFactors =  " + (char) 34 + woman.getRegCurrHealthRiskFactors() + (char) 34 + "";
            else
                addString = addString + " ,regCurrHealthRiskFactors =  " + (char) 34 + woman.getRegCurrHealthRiskFactors() + (char) 34 + "";
            InserttblAuditTrail("regCurrHealthRiskFactors",
                    String.valueOf(regData.getRegCurrHealthRiskFactors()),
                    String.valueOf(woman.getRegCurrHealthRiskFactors()),
                    transId, null);
        }


        if (regData.getRegPrevHealthRiskFactors() != null && !regData.getRegPrevHealthRiskFactors().equalsIgnoreCase(woman.getRegPrevHealthRiskFactors())) {

            if (addString == "")
                addString = addString + " regPrevHealthRiskFactors = " + (char) 34 + woman.getRegPrevHealthRiskFactors() + (char) 34 + "";
            else
                addString = addString + " ,regPrevHealthRiskFactors = " + (char) 34 + woman.getRegPrevHealthRiskFactors() + (char) 34 + "";
            InserttblAuditTrail("regPrevHealthRiskFactors",
                    String.valueOf(regData.getRegPrevHealthRiskFactors()),
                    String.valueOf(woman.getRegPrevHealthRiskFactors()),
                    transId, null);
        }


        if (regData.getRegFamilyHistoryRiskFactors() != null && !regData.getRegFamilyHistoryRiskFactors().equalsIgnoreCase(woman.getRegFamilyHistoryRiskFactors())) {

            if (addString == "")
                addString = addString + " regFamilyHistoryRiskFactors = " + (char) 34 + woman.getRegFamilyHistoryRiskFactors() + (char) 34 + "";
            else
                addString = addString + " ,regFamilyHistoryRiskFactors = " + (char) 34 + woman.getRegFamilyHistoryRiskFactors() + (char) 34 + "";
            InserttblAuditTrail("regFamilyHistoryRiskFactors",
                    String.valueOf(regData.getRegFamilyHistoryRiskFactors()),
                    String.valueOf(woman.getRegFamilyHistoryRiskFactors()),
                    transId, null);
        }


        if (regData.getRegVillage() != woman.getRegVillage()) {

            if (addString == "")
                addString = addString + " regVillage = " + woman.getRegVillage() + "";
            else
                addString = addString + " ,regVillage = " + woman.getRegVillage() + "";
            InserttblAuditTrail("regVillage",
                    String.valueOf(regData.getRegVillage()),
                    String.valueOf(woman.getRegVillage()),
                    transId, null);
        }


        if (regData.getRegBloodGroup() != woman.getRegBloodGroup()) {


            if (addString == "")
                addString = addString + " regBloodGroup = " + woman.getRegBloodGroup() + "";
            else
                addString = addString + " ,regBloodGroup = " + woman.getRegBloodGroup() + "";
            InserttblAuditTrail("regBloodGroup",
                    String.valueOf(regData.getRegBloodGroup()),
                    String.valueOf(woman.getRegBloodGroup()), transId, null);
        }

      /*  if (regData.getRegPregnantorMother() != woman.getRegPregnantorMother()) {

            if (addString == "")
                addString = addString + " regPregnantorMother = " + woman.getRegPregnantorMother() + "";
            else
                addString = addString + " ,regPregnantorMother = " + woman.getRegPregnantorMother() + "";
            InserttblAuditTrail("regPregnantorMother", String.valueOf(woman.getRegPregnantorMother()),
                    String.valueOf(woman.getRegPregnantorMother()), transId, null);
        }*/

        if (regData.getRegRationCard() != null &&
                !regData.getRegRationCard().equalsIgnoreCase(woman.getRegRationCard())) {

            if (addString == "")
                addString = addString + " regRationcard =  " + (char) 34 + woman.getRegRationCard() + (char) 34 + "";
            else
                addString = addString + " ,regRationcard =  " + (char) 34 + woman.getRegRationCard() + (char) 34 + "";
            InserttblAuditTrail("regRationcard", regData.getRegRationCard(),
                    woman.getRegRationCard(), transId, null);
        }

        //13Aug2019 - Bindu set recordupdatedate for audit
        if (addString == "")
            addString = addString + " RecordUpdatedDate = " + (char) 34 + woman.getRecordUpdatedDate() + (char) 34 + "";
        else
            addString = addString + " ,RecordUpdatedDate = " + (char) 34 + woman.getRecordUpdatedDate() + (char) 34 + "";
        InserttblAuditTrail("RecordUpdatedDate",
                String.valueOf(regData.getRecordUpdatedDate()),
                String.valueOf(woman.getRecordUpdatedDate()),
                transId, null);

        if (addString != null && addString.length() > 0) {
            upSql = "UPDATE tblregisteredwomen SET ";
            upSql = upSql + addString + " WHERE WomanId = '"
                    + appState.selectedWomanId + "' and UserId = '" + woman.getUserId() + "'";
        }
        return upSql;
    }


    /**
     * This method handle touch listners, calls - displayDatePicker()
     */
    @Override
    public boolean onTouch(View v, MotionEvent event) {

        if (MotionEvent.ACTION_UP == event.getAction()) {
            try {
                switch (v.getId()) {
                    case R.id.etdob: {
                        isDob = true;
                        isMarriageDate = false;
                        isThayiCard = false; //09Apr2021 Bindu
                        assignEditTextAndCallDatePicker(aqVP.id(R.id.etdob).getEditText());
                        break;
                    }
                    case R.id.etmarriagrdate: {
                        isMarriageDate = true;
                        isDob = false;
                        isThayiCard = false; //09Apr2021 Bindu
                        assignEditTextAndCallDatePicker(aqVP.id(R.id.etmarriagrdate).getEditText());
                        break;
                    }
                    //09Apr2021 Bindu
                    case R.id.etthaicardregdate: {
                        isThayiCard = true;
                        isMarriageDate = false;
                        isDob = false;
                        assignEditTextAndCallDatePicker(aqVP.id(R.id.etthaicardregdate).getEditText());
                        break;
                    }
                    default:
                        break;
                }
            } catch (Exception e) {

                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
        }
        return true;
    }


    /**
     * This method opens the Date Dialog and sets Date to respective EditText
     */
    private void assignEditTextAndCallDatePicker(EditText editText) {
        String currDate = (editText.getText().length() > 0) ? editText.getText().toString() : "";
        CommonDatePickerFragment.getCommonDate(this, currDate);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {

        try {
            if (view.isShown()) {
                String bldSmpldate = null;
                bldSmpldate = String.format("%02d", day) + "-" + String.format("%02d", month + 1) + "-"
                        + year;

                if (isThayiCard) {
                    Date birthdate = null;
                    Date taken = new SimpleDateFormat("dd-MM-yyyy").parse(bldSmpldate);

                    if (aqVP.id(R.id.etthaicardregdate).getText().toString() != null
                            && aqVP.id(R.id.etthaicardregdate).getText().toString()
                            .trim().length() > 0 && aqVP.id(R.id.etdob).getText().toString() != null && aqVP.id(R.id.etdob).getText().toString().trim().length() > 0)
                        birthdate = new SimpleDateFormat("dd-MM-yyyy")
                                .parse(aqVP.id(R.id.etdob).getText().toString());

                    if (birthdate != null && taken.before(birthdate)) {
                        Toast.makeText(getApplicationContext(),
                                (getResources().getString(R.string.thayi_date_cannot_be_before_dob)),
                                Toast.LENGTH_LONG).show();
                        aqVP.id(R.id.etthaicardregdate).text("");
                        aqVP.id(R.id.imgclearthayidate).gone();//20Aug2019 Arpitha
                    } else if (taken.after(new Date())) {
                        Toast.makeText(getApplicationContext(),
                                (getResources().getString(R.string.thayi_date_cannot_after_today)),
                                Toast.LENGTH_LONG).show();
                        aqVP.id(R.id.etthaicardregdate).text("");
                        aqVP.id(R.id.imgclearthayidate).gone();//20Aug2019 Arpitha
                    } else {
                        aqVP.id(R.id.etthaicardregdate).text(bldSmpldate);
                        aqVP.id(R.id.imgclearthayidate).visible();
                        aqVP.id(R.id.llthayi).background(R.drawable.edittext_style);
                        aqVP.id(R.id.etthaicardregdate).background(R.drawable.editext_none);
                    }

                }

                if (isMarriageDate) {
                    Date taken = new SimpleDateFormat("dd-MM-yyyy").parse(bldSmpldate);
                    if (taken.after(new Date())) {
                        Toast.makeText(getApplicationContext(),
                                (getResources().getString(R.string.date_time_cannot_be_after_curre_datetime)),
                                Toast.LENGTH_LONG).show();
                        aqVP.id(R.id.etmarriagrdate).text("");
                        aqVP.id(R.id.imgclearmarriagedate).gone();//20Aug2019 Arpitha
                    } else {
                        aqVP.id(R.id.etmarriagrdate).text(bldSmpldate);
                        aqVP.id(R.id.imgclearmarriagedate).visible();//20Aug2019 Arpitha
                    }
                }

                if (isDob) {
                    Date taken = new SimpleDateFormat("dd-MM-yyyy").parse(bldSmpldate);
                    if (taken.after(new Date())) {
                        Toast.makeText(getApplicationContext(),
                                (getResources().getString(R.string.date_time_cannot_be_after_curre_datetime)),
                                Toast.LENGTH_LONG).show();
                        aqVP.id(R.id.etdob).text("");
                        aqVP.id(R.id.imgcleardob).gone();//20Aug2019 Arpitha
                    } else {
                        aqVP.id(R.id.etdob).text(bldSmpldate);
                        aqVP.id(R.id.imgcleardob).visible();//20Aug2019 Arpitha
                        //                        21Aug2019 Arpitha
                        int age = getYearsFromDob(bldSmpldate);

                        if(aqVP.id(R.id.etdob).getText().toString().trim().length()>0) {
                            if (age < 18
                                    || age > 40) {
                                aqVP.id(R.id.txtdoberror).visible();
                                aqVP.id(R.id.txtdoberror).text(getResources().getString(R.string.invaliddob)); //16Nov2019 - Bindu

                            } else {
                                aqVP.id(R.id.txtdoberror).gone();//  21Aug2019 Arpitha
                            }
                        }else
                            aqVP.id(R.id.txtdoberror).gone();
                    }
                }
            }
        } catch (ParseException e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }


    /**
     * This method handles focus change listners Calls - calTotPregnancies(),
     */
    @Override
    public void onFocusChange(View v, boolean hasFocus) {

        try {
            switch (v.getId()) {
                case R.id.etage: {
                    ColorCount();
                    break;
                }
                case R.id.ettotlivebirth: {
                    calTotPregnancies();
                    int totLiveChild = (aqVP.id(R.id.ettotlivebirth).getText().toString().length() > 0)
                            ? Integer.parseInt(aqVP.id(R.id.ettotlivebirth).getText().toString()) : 0;
                    if (totLiveChild <= 0) {
                        aqVP.id(R.id.etlastchildage).text("");
                        aqVP.id(R.id.etlastchildWeight).text("");
                        aqVP.id(R.id.radiofemale1).checked(false);
                        aqVP.id(R.id.radiomale1).checked(false);
                        aqVP.id(R.id.tvlastchildage).enabled(false);
                        aqVP.id(R.id.etlastchildage).enabled(false);
                        aqVP.id(R.id.tvlastchildWeight).enabled(false);
                        aqVP.id(R.id.etlastchildWeight).enabled(false);
                        aqVP.id(R.id.tvchildsex).enabled(false);
                        aqVP.id(R.id.radiofemale1).enabled(false);
                        aqVP.id(R.id.radiomale1).enabled(false);
                    }
                    break;
                }
                case R.id.ettotabort: {
                    calTotPregnancies();
                    ColorCount();
                    break;
                }
                case R.id.etDeadDuringPreg: {
                    calTotPregnancies();
                    break;
                }
                case R.id.etWomanWeight: {
                    calculateBmi();
                }
                break;
                case R.id.etheight:
                    calculateBmi();
                    break;
                case R.id.ettotpreg:
                    ColorCount();
                    break;

                default:
                    break;
            }
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }


    @Override
    public void onBackPressed() {

    }


    TextWatcher watcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            try {

                if (s == aqVP.id(R.id.etage).getEditable()) {

                    //06Aug2019 - Bindu - Change the msg based on cond
                    if (aqVP.id(R.id.etage).getText().toString().length() > 0
                            && Integer.parseInt(aqVP.id(R.id.etage).getText().toString()) < 18) {
                        aqVP.id(R.id.etage).getEditText()
                                .setError(getResources().getString(R.string.agelessthan18));
                    } else if (aqVP.id(R.id.etage).getText().toString().length() > 0
                            && Integer.parseInt(aqVP.id(R.id.etage).getText().toString()) > 40) {
                        aqVP.id(R.id.etage).getEditText()
                                .setError(getResources().getString(R.string.agegreaterthan40));
                    } else if (aqVP.id(R.id.etdob).getText().toString().length() > 0) { //07Aug2019 - Bindu age and dob diff
                        int ageyears = getYearsFromDob(aqVP.id(R.id.etdob).getText().toString());
                        if (aqVP.id(R.id.etage).getText().toString().length() > 0 && aqVP.id(R.id.etdob).getText().toString().length() > 0
                                && Integer.parseInt(aqVP.id(R.id.etage).getText().toString()) != ageyears) {
                            aqVP.id(R.id.etage).getEditText()
                                    .setError(getResources().getString(R.string.dobandagenotmatching));
                            aqVP.id(R.id.txtdoberror).visible();//21AUg2019 Arpitha
                            aqVP.id(R.id.txtdoberror).text(getResources().getString(R.string.dobandagenotmatching));//21AUg2019 Arpitha
                        } else {
                            if (ageyears < 0 || !(ageyears > 0 && (ageyears < 18 || ageyears > 40))) {
                                aqVP.id(R.id.txtdoberror).gone();//21AUg2019 Arpitha
                                aqVP.id(R.id.etdob).getEditText().setError(null); //07Aug2019 - Bindu set dob error
                                aqVP.id(R.id.etage).getEditText().setError(null);//22july2021 Arpitha

                            }
                        }
                    } else {
                        aqVP.id(R.id.etage).getEditText().setError(null);
                        aqVP.id(R.id.etdob).getEditText().setError(null); //07Aug2019 - Bindu set dob error
                    }


                } else if (s == aqVP.id(R.id.etWomanWeight).getEditable()) {

                    if (aqVP.id(R.id.etWomanWeight).getText().toString().length() > 0) {
                        if (Double.parseDouble(aqVP.id(R.id.etWomanWeight).getText().toString()) <= 35
                                || Double.parseDouble(aqVP.id(R.id.etWomanWeight).getText().toString()) > 80) {
                            aqVP.id(R.id.etWomanWeight).getEditText()
                                    //  .setError(getResources().getString(R.string.weightvalis) + aqVP.id(R.id.etWomanWeight).getText().toString() + " kgs");
                                    .setError(getResources().getString(R.string.strweight) + aqVP.id(R.id.etWomanWeight).getText().toString() + getResources().getString(R.string.strkg));

                        }

                        //09Aug2019 - Bindu - calculate bmi
                        if (heightUnit == 1 || heightUnit == 2)
                            calculateBmi();
                    } else {                        // 09Aug2019 - Bindu - reset bmi val
                        aqVP.id(R.id.tvBMI).text("");
                    }

                } else if (s == aqVP.id(R.id.etheight).getEditable()) {

                    if (aqVP.id(R.id.etheight).getText().toString().length() > 0) {
                        if (Double.parseDouble(aqVP.id(R.id.etheight).getText().toString()) < 153) {
                            aqVP.id(R.id.etheight).getEditText()
                                    //.setError(getResources().getString(R.string.heightvalis) + aqVP.id(R.id.etheight).getText().toString() + " " + getResources().getString(R.string.centimeter));
                                    .setError(getResources().getString(R.string.strheight) + aqVP.id(R.id.etheight).getText().toString() + getResources().getString(R.string.strcm));

                        }
                        //09Aug2019 - Bindu - calculate bmi
                        calculateBmi();
                    } else {                        // 09Aug2019 - Bindu - reset bmi val
                        aqVP.id(R.id.tvBMI).text("");
                    }

                }

                // 31Oct2018 - Bindu
                else if (s == aqVP.id(R.id.ettotpreg).getEditable()) {
                    // 07Nov2018 - Bindu - para > gravida chk validation
                    int para = 0;
                    if (aqVP.id(R.id.ettotlivebirth).getText().toString().length() > 0)
                        para = Integer.parseInt(aqVP.id(R.id.ettotlivebirth).getText().toString());
                    if (aqVP.id(R.id.ettotpreg).getText().toString().length() > 0) {
                        if (para > Integer.parseInt(aqVP.id(R.id.ettotpreg).getText().toString())) {
                            aqVP.id(R.id.ettotlivebirth).getEditText().setError("Para cannot be greater than gravida");
                            aqVP.id(R.id.ettotlivebirth).text("");
                        }
                    }

                    if (woman.getRegPregnantorMother() == 0) {
                        if ((woman.getRegGravida() != 1)) {
                            if (aqVP.id(R.id.ettotpreg).getText().toString().length() > 0) {
                                if (Integer.parseInt(aqVP.id(R.id.ettotpreg).getText().toString()) < 2) {
                                    aqVP.id(R.id.ettotpreg).getEditText()
                                            .setError("Gravida cannot be less than 2 since its not a primi case");

                                }
                            }
                        }
                    } else {
                        if (aqVP.id(R.id.ettotpreg).getText().toString().length() > 0) {
                            if (Integer.parseInt(aqVP.id(R.id.ettotpreg).getText().toString()) < 1) {
                                aqVP.id(R.id.ettotpreg).getEditText().setError("Gravida cannot be 0");
                            }
                        }
                    }

                } else if (s == aqVP.id(R.id.ettotlivebirth).getEditable()) {

                    if (woman.getRegGravida() == 1 && gestationAge >= 24) {
                        if (aqVP.id(R.id.ettotlivebirth).getText().toString().length() > 0) {
                            if (Integer.parseInt(aqVP.id(R.id.ettotlivebirth).getText().toString()) > 1) {
                                aqVP.id(R.id.ettotlivebirth).getEditText()
                                        .setError("Para cannot be greater than gravida");
                                aqVP.id(R.id.ettotlivebirth).text("");

                            }
                        }
                    } else {
                        int gravida = 0;
                        if (aqVP.id(R.id.ettotpreg).getText().toString().length() > 0)
                            gravida = Integer.parseInt(aqVP.id(R.id.ettotpreg).getText().toString());
                        if (woman.getRegPregnantorMother() == 0) {
                            if (gestationAge >= 24) {
                                if (aqVP.id(R.id.ettotlivebirth).getText().toString().length() > 0) {
                                    if (Integer.parseInt(aqVP.id(R.id.ettotlivebirth).getText().toString()) > gravida) {
                                        aqVP.id(R.id.ettotlivebirth).getEditText()
                                                .setError("Para cannot be greater than gravida");
                                        aqVP.id(R.id.ettotlivebirth).text("");
                                    }
                                }
                            } else {
                                if (aqVP.id(R.id.ettotlivebirth).getText().toString().length() > 0) {
                                    if (Integer.parseInt(aqVP.id(R.id.ettotlivebirth).getText().toString()) >= gravida) {
                                        aqVP.id(R.id.ettotlivebirth).getEditText()
                                                .setError("Para cannot be greater than or equal to gravida");
                                        aqVP.id(R.id.ettotlivebirth).text("");
                                    }
                                }
                            }
                        } else {
                            if (aqVP.id(R.id.ettotlivebirth).getText().toString().length() > 0) {
                                if (Integer.parseInt(aqVP.id(R.id.ettotlivebirth).getText().toString()) == 0) {
                                    aqVP.id(R.id.ettotlivebirth).getEditText().setError("Para cannot be 0");
                                    aqVP.id(R.id.ettotlivebirth).text("");
                                } else if (Integer
                                        .parseInt(aqVP.id(R.id.ettotlivebirth).getText().toString()) > gravida) {
                                    aqVP.id(R.id.ettotlivebirth).getEditText()
                                            .setError("Para cannot be greater than gravida");
                                    aqVP.id(R.id.ettotlivebirth).text("");
                                }
                            }
                        }
                    }
                } else if (s == aqVP.id(R.id.etwifeageMarriage).getEditable()) {
                    if (count > 0) {
                        if (Integer.parseInt(aqVP.id(R.id.etwifeageMarriage).getText().toString()) == 0)
                            aqVP.id(R.id.etwifeageMarriage).getEditText().setError(getResources().getString(R.string.womanageatmarriageiscannotbezero));
                        else if (Integer.parseInt(aqVP.id(R.id.etwifeageMarriage).getText().toString()) > Integer.parseInt(aqVP.id(R.id.etage).getText().toString())) {
                            aqVP.id(R.id.etwifeageMarriage).getEditText().setError(getResources().getString(R.string.m163));

                        }
                    }
                } else if (s == aqVP.id(R.id.ethusageMarriage).getEditable()) {
                    if (count > 0) {
                        if (Integer.parseInt(aqVP.id(R.id.ethusageMarriage).getText().toString()) == 0)
                            aqVP.id(R.id.ethusageMarriage).getEditText().setError(getResources().getString(R.string.husbandageatmarriageiscannotbezero));
                        else if (Integer.parseInt(aqVP.id(R.id.ethusageMarriage).getText().toString()) > Integer.parseInt(aqVP.id(R.id.ethusage).getText().toString())) {
                            aqVP.id(R.id.ethusageMarriage).getEditText().setError(getResources().getString(R.string.husband_age_lessthan_currrent_age));

                        }
                    }
                } else if (s == aqVP.id(R.id.ethusage).getEditable()) {
                    if (count > 0) {
                        if (Integer.parseInt(aqVP.id(R.id.ethusage).getText().toString()) == 0) {
                            aqVP.id(R.id.ethusage).getEditText().setError(getResources().getString(R.string.husbandageiscannotbezero));
                        } else if (aqVP.id(R.id.ethusage).getText().toString().length() > 0
                                && Integer.parseInt(aqVP.id(R.id.ethusage).getText().toString()) < 18) {
                            aqVP.id(R.id.ethusage).getEditText()
                                    .setError(getResources().getString(R.string.husbagelessthan18));
                        } else
                            aqVP.id(R.id.ethusage).getEditText().setError(null);
                        //                          22Aug2019 Arpitha
                        if (Integer.parseInt(aqVP.id(R.id.ethusageMarriage).getText().toString()) > Integer.parseInt(aqVP.id(R.id.ethusage).getText().toString()))
                            aqVP.id(R.id.ethusageMarriage).getEditText().setError(getResources().getString(R.string.husband_age_lessthan_currrent_age));
                        else
                            aqVP.id(R.id.ethusageMarriage).getEditText().setError(null);//22Aug2019 Arpitha

                    }

                } else if (s == aqVP.id(R.id.etdob).getEditable()) {
                    int years = getYearsFromDob(aqVP.id(R.id.etdob).getText().toString());
                    //05Aug2019 - Bindu - disable age calculation
                    //07Aug2019 - AGe and DOB val
                    int age = (aqVP.id(R.id.etage).getText().toString().length() > 0 ? Integer.parseInt(aqVP.id(R.id.etage).getText().toString()) : 0);
                    if (years != age && aqVP.id(R.id.etdob).getText().toString().length() > 0 &&  aqVP.id(R.id.etage).getText().toString().length() > 0) {
                        //04Aug2019 - Bindu - DOB set error
                        aqVP.id(R.id.etdob).getEditText()
                                .setError(getResources().getString(R.string.dobandagenotmatching));
                        aqVP.id(R.id.etage).getEditText()
                                .setError(getResources().getString(R.string.dobandagenotmatching));
                    } else {
                        aqVP.id(R.id.etage).getEditText().setError(null);
                        aqVP.id(R.id.etdob).getEditText().setError(null);
                    }
                }

                else if (s == aqVP.id(R.id.etthaicard).getEditable()) {
                    if (count > 0) {
                        if (aqVP.id(R.id.etthaicard).getText()
                                .toString().trim().length() < 7)
                            aqVP.id(R.id.etthaicard).getEditText().
                                    setError(getResources().getString
                                            (R.string.thayicardno_mustcontain_sevendigits));

                        if (aqVP.id(R.id.etthaicard).getText().toString().trim().length() > 0) {
                            aqVP.id(R.id.etthaicardregdate).enabled(true).background(R.drawable.edittext_style);  //09Aug2019 - Bindu
                        } else {
                            aqVP.id(R.id.etthaicardregdate).enabled(false).background(R.drawable.edittext_disable);
                            aqVP.id(R.id.etthaicardregdate).text("");  //09Aug2019 Bindu - reset textdate
                        }


                        if (aqVP.id(R.id.etthaicard).getText().toString().trim().length() > 0) {
//                             aqVP.id(R.id.etthaicardregdate).enabled(true);
//                            aqVP.id(R.id.etthaicardregdate).enabled(true).background(R.drawable.editext_none);  //09Aug2019 - Bindu
                            aqVP.id(R.id.etthaicardregdate).enabled(true);
                            aqVP.id(R.id.llthayi).background(R.drawable.edittext_style);  //09Aug2019 - Bindu

                        } else {
                            aqVP.id(R.id.etthaicardregdate).enabled(false);
                            aqVP.id(R.id.etthaicardregdate).text("");  //09Aug2019 Bindu - reset textdate
                            aqVP.id(R.id.llthayi).enabled(true).background(R.drawable.edittext_disable);
                            aqVP.id(R.id.imgclearthayidate).gone();//20Aug2019 Arpitha
                        }
                    }
                }
            } catch (Exception e) {

                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {

        }

    };

    /**
     * This method calculate age from DOB
     */
    protected int getYearsFromDob(String string) throws ParseException {

        int years = 0;
        String dobtxt = aqVP.id(R.id.etdob).getText().toString();
        Calendar curDt = new GregorianCalendar();
        curDt.setTime(new SimpleDateFormat("dd-MM-yyyy").parse(DateTimeUtil.getTodaysDate()));
        Calendar dob = new GregorianCalendar();
        if (dobtxt.length() > 0) {
            dob.setTime(new SimpleDateFormat("dd-MM-yyyy").parse(dobtxt));
            years = curDt.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
        }
        return years;
    }


    /**
     * Confirm Alert
     */
    private void confirmAlertExit() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set dialog message
        alertDialogBuilder.setMessage((getResources().getString(R.string.areyousuretoexit))).setCancelable(false)
                .setNegativeButton((getResources().getString(R.string.m121)),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                try {
                                    Intent exit = new Intent(ViewProfileActivity.this,
                                            MainMenuActivity.class);
                                    exit.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                    startActivity(exit);
                                } catch (Exception e) {

                                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                                    crashlytics.log(e.getMessage());
                                }
                            }
                        })
                .setPositiveButton((getResources().getString(R.string.m122)),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    //	prepare bundle for app state
    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        return b;
    }

    /**
     * Initializes the Action Bar items
     */
    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        try {

            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
            getSupportActionBar().setDisplayShowCustomEnabled(true);

            getMenuInflater().inflate(R.menu.servicesmenu, menu);
            menu.findItem(R.id.logout).setTitle((getResources().getString(R.string.action_logout)));

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        return super.onCreateOptionsMenu(menu);
    }


    /**
     * Called whenever an item clicked in actionbar
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action buttons
        switch (item.getItemId()) {

            case R.id.home: {
                Intent goToScreen = new Intent(ViewProfileActivity.this, MainMenuActivity.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.m110), goToScreen);
                return true;
            }

            case R.id.logout: {
                Intent goToScreen = new Intent(ViewProfileActivity.this, LoginActivity.class);
                displayAlert(getResources().getString(R.string.m111), goToScreen);
                return true;
            }
            case R.id.wlist: {
                Intent goToScreen = new Intent(ViewProfileActivity.this, RegisteredWomenActionTabs.class);
                displayAlert(getResources().getString(R.string.m110), goToScreen);
                return true;
            }
//30Sep2019 - Bindu
            case R.id.about: {
                Intent goToScreen = new Intent(ViewProfileActivity.this, AboutActivity.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * logout Confirmation Alert
     */
    private void displayAlert(final String strMessage, final Intent goToScreen) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set dialog message
        alertDialogBuilder.setMessage(strMessage).setCancelable(false).setNegativeButton(
                getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (goToScreen != null) {
                            goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(goToScreen);
                            if (strMessage.contains("logout")) {
                                try {
                                    LoginRepository loginRepository = new LoginRepository(databaseHelper);
                                    loginRepository.updateLogoutTime(appState.loginAuditId, appState.sessionUserId);
                                } catch (Exception e) {
                                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                                    crashlytics.log(e.getMessage());
                                }
                            }
                        } else
                            dialog.cancel();
                    }
                }).setPositiveButton(getResources().getString(R.string.m122),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        /*
         * You'll need this in your class to release the helper when done.
         */
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

    /**
     * This method invokes when Item clicked in Spinner
     *
     * @throws Exception
     */
    public void onSpinnerClicked(AdapterView<?> adapter, View v, int position, long arg3) throws
            Exception {

        switch (adapter.getId()) {
            case R.id.spnvillage: {
                if (adapter.getSelectedItemPosition() != 0) {
                    villageName = (String) adapter.getSelectedItem();
                    villageCode = village.get(villageName);

                } else {
                    villageCode = 0;
                }
                break;
            }
            case R.id.spnHeightNumeric: {
                heightNumeric = adapter.getSelectedItemPosition();
                calculateBmi();
                checkHeightValue();
            }
            break;
            case R.id.spnHeightDecimal: {
                heightDecimal = adapter.getSelectedItemPosition();
                calculateBmi();
                checkHeightValue();
            }
            break;
            case R.id.spnbankname: {
                //11Aug2019 - Bindu - disable and reset values on change
                if (aqVP.id(R.id.spnbankname).getSpinner().getSelectedItem() != null && aqVP.id(R.id.spnbankname).getSpinner().getSelectedItem().toString().equalsIgnoreCase("Select")) {
                    aqVP.id(R.id.etbranchname).text("");
                    aqVP.id(R.id.etbankaccount).text("");
                    aqVP.id(R.id.etifsc).text("");
                    aqVP.id(R.id.etbranchname).enabled(false).background(R.drawable.edittext_disable);  //11Aug2019 - Bindu
                    aqVP.id(R.id.etbankaccount).enabled(false).background(R.drawable.edittext_disable);  //11Aug2019 - Bindu
                    aqVP.id(R.id.etifsc).enabled(false).background(R.drawable.edittext_disable);  //11Aug2019 - Bindu
                } else if (isRegSpnBanknameset) {
                    aqVP.id(R.id.etbranchname).enabled(true).background(R.drawable.edittext_style);  //11Aug2019 - Bindu
                    aqVP.id(R.id.etbankaccount).enabled(true).background(R.drawable.edittext_style);  //11Aug2019 - Bindu
                    aqVP.id(R.id.etifsc).enabled(true).background(R.drawable.edittext_style);  //11Aug2019 - Bindu
                    isRegSpnBanknameset = false;
                } else {
                    aqVP.id(R.id.etbranchname).text("");
                    aqVP.id(R.id.etbankaccount).text("");
                    aqVP.id(R.id.etifsc).text("");
                    aqVP.id(R.id.etbranchname).enabled(true).background(R.drawable.edittext_style);  //11Aug2019 - Bindu
                    aqVP.id(R.id.etbankaccount).enabled(true).background(R.drawable.edittext_style);  //11Aug2019 - Bindu
                    aqVP.id(R.id.etifsc).enabled(true).background(R.drawable.edittext_style);  //11Aug2019 - Bindu
                }
            }
            break;

//            09Dec2019 Arpitha
            case R.id.spnbloodgroup: {

                BloodGroup = adapter.getSelectedItemPosition();
                if (adapter != null && adapter.getSelectedItem().toString().contains(getResources().getString(R.string.negative))) {
                    aqVP.id(R.id.chkrhneg).checked(true);
                    aqVP.id(R.id.chkrhneg).enabled(false);
                } else if (adapter != null && !(adapter.getSelectedItem().toString().contains(getResources().getString(R.string.select)))) {
                    aqVP.id(R.id.chkrhneg).checked(false);
                    aqVP.id(R.id.chkrhneg).enabled(false);
                } else {
                    aqVP.id(R.id.chkrhneg).checked(false);
                    aqVP.id(R.id.chkrhneg).enabled(true);
                }
                break;
            }
            default:
                break;
        }
    }

    //check height is normal
    private void checkHeightValue() throws Exception {
        heightNumeric = Integer.parseInt(aqVP.id(R.id.spnHeightNumeric).getSelectedItem().toString());
        heightDecimal = Integer.parseInt(aqVP.id(R.id.spnHeightDecimal).getSelectedItem().toString());
        if (heightNumeric < 5) {
            ((TextView) aqVP.id(R.id.spnHeightNumeric).getSpinner().getSelectedView())
                    .setError(getResources().getString(R.string.heightvalis) + heightNumeric + "." + heightDecimal + " " + getResources().getString(R.string.feet));
            ((TextView)
                    aqVP.id(R.id.spnHeightDecimal).getSpinner().getSelectedView()).setError(getResources().getString(R.string.heightvalis) + heightNumeric + "." + heightDecimal + " " + getResources().getString(R.string.feet));
        } else {  //16Aug2019 - Bindu - set else block to remove error
            ((TextView) aqVP.id(R.id.spnHeightNumeric).getSpinner().getSelectedView())
                    .setError(null);
            ((TextView)
                    aqVP.id(R.id.spnHeightDecimal).getSpinner().getSelectedView()).setError(null);
        }
    }

    // 26Sep2019 -Bindu - ShowHideTribal or Non tribal
    private void ShowHideTribalDetails() {
        if(aqVP.id(R.id.rdnontribal).isChecked()) {
            //  aqVP.id(R.id.trnontribalcaste).visible();
            aqVP.id(R.id.trtribalcaste).gone();

            /*// 03Oct2019 - Bindu - Radiogrp
            rdgtribalcase.clearCheck();
            aqVP.id(R.id.rdjenukuruba).checked(false);
            aqVP.id(R.id.rdbettakuruba).checked(false);
            aqVP.id(R.id.rdsoliga).checked(false);
            aqVP.id(R.id.rdyarawa).checked(false);
            aqVP.id(R.id.rdphaniya).checked(false);*/ //08Apr2021 Bindu
            caste = "";
            aqVP.id(R.id.spntribalcaste).setSelection(0);
        }else if(aqVP.id(R.id.rdtribal).isChecked()){
            // aqVP.id(R.id.trnontribalcaste).gone();
            aqVP.id(R.id.trtribalcaste).visible();
           /* aqVP.id(R.id.rdsc).checked(false);
            aqVP.id(R.id.rdst).checked(false);
            aqVP.id(R.id.rdgeneral).checked(false);
            aqVP.id(R.id.rdothercaste).checked(false);*/
        }
    }

    //04Oct2019 - Bindu - Hide keyboard
    public void hideKeyboard() {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        //13Apr2021 Bindu - Change from currentfocus to getwindow
        //inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        inputMethodManager.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
    }


    //    Arpitha 26Nov2019
    void disableScreen()
    {
        ScrollView srlWomanBasicInfo  = findViewById(R.id.srlWomanBasicInfo);
        ScrollView srlhealthhistory  = findViewById(R.id.srlhealthhistory);
        ScrollView srlWomanPersonalAndPrevPregDetails  = findViewById(R.id.srlWomanPersonalAndPrevPregDetails);
        aqVP.id(R.id.btnsave).enabled(false);
        disableEnableControls(false,srlWomanBasicInfo);
        disableEnableControls(false,srlhealthhistory);
        disableEnableControls(false,srlWomanPersonalAndPrevPregDetails);

        aqVP.id(R.id.etbankaccount).enabled(false);
        aqVP.id(R.id.etbranchname).enabled(false);
        aqVP.id(R.id.etbankaccount).enabled(false);
        aqVP.id(R.id.etifsc).enabled(false);
    }

    //    Arpitha 26Nov2019
    private void disableEnableControls(boolean enable, ViewGroup vg) {
        for (int i = 0; i < vg.getChildCount(); i++) {
            View child = vg.getChildAt(i);
            child.setEnabled(enable);
            if (child instanceof ViewGroup) {
                disableEnableControls(enable, (ViewGroup) child);
            }
        }
    }

    //  14MAy2021 Arpitha
    public  int checkSimState() {
        int simState = -1;
        // This is to know the status of the Sim
        TelephonyManager telMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        simState = telMgr.getSimState();

        return simState;
    }


    //    27Nov2019 Arpitha
    private void sendSMS(TransactionHeaderRepository transRepo, DatabaseHelper databaseHelper) {
        try {


           /* p_no = "";
            for (int i = 0; i < values.size(); i++) {

                String s = values.get(i);
                p_no = p_no + values.get(i) + ",";
            }
*/

            String p_no = "";
            List<TblContactDetails> values;

            SettingsRepository settingsRepository = new SettingsRepository(databaseHelper);

            values = settingsRepository.getSpecificPhoneNumber("Registration");
            p_no = "";
            for (int i = 0; i < values.size(); i++) {

                String s = values.get(i).getContactNumber();
                p_no = p_no + values.get(i).getContactNumber() + ",";
            }


            String hrp = "";


            int code = woman.getRegAmberOrRedColorCode();

            int drawable = 0;
            String strStar = "";
            if (code == 9)
                strStar = "1 Amber star";
                //   drawable = R.drawable.amberone;
            else if (code == 1)
                strStar = "1 Red star";

//        drawable = R.drawable.redone;
            else if (code == 2)
                strStar = "2 Red star";

//            drawable = R.drawable.redtwo;
            else if (code == 3)
                strStar = "3 Red Star";
//            drawable = R.drawable.redthree;
            else if (code > 3 && code != 11)
                strStar = "4 Red Star";
//            drawable = R.drawable.redfour;
            else if (code == 11)
                strStar = "2 Amber Star";
//            drawable = R.drawable.ambertwo;

            if (woman.getRegComplicatedpreg() == 1)
                hrp = ",HRP:" + strStar + "," + woman.getRegCurrHealthRiskFactors() + " "
                        + woman.getRegPrevHealthRiskFactors();


            String gestationWks = woman.getRegStatusWhileRegistration();
            gestationWks = gestationWks.replace("Weeks", "W");
            gestationWks = gestationWks.replace("Days", "D");

       /* smsContent = "Matruksha:" + woman.getRegWomanName() + "," + villageName + "," + gestationWks
                + hrp+".. "+", ASHA - " +user.getUserName()
                + " ,"+ user.getUserMobileNumber()+"(REG)";*/
//13May2021 Arpitha
            String uniqueId = "";
            String smsContent = "";
            if (woman.getRegUIDNo() != null && woman.getRegUIDNo().trim().length() > 0) {
                uniqueId = "MCP:" + woman.getRegUIDNo();
            } else if (woman.getRegAadharCard() != null && woman.getRegAadharCard().trim().length() > 0) {
                uniqueId = "A:" + woman.getRegAadharCard();
            } else if (woman.getRegPhoneNumber() != null && woman.getRegPhoneNumber().trim().length() > 0) {
                uniqueId = "Ph:" + woman.getRegPhoneNumber();
            }

            //23May2021 Bindu - get villagename from eng
            FacilityRepository facilityRepo = new FacilityRepository(databaseHelper);
            String hamletname = facilityRepo.getHamletName(villageCode);

            if(regData.getRegPregnantorMother() ==1 )
                smsContent = "Sentiatend:-" + woman.getRegWomanName()
                        +", "+uniqueId+", WID - "+regData.getWomanId()+", "
                        + hamletname + ",HRP-"+strStar
                        + "," +appState.userType + "-" +user.getUserName()
                        + " ,"+ user.getUserMobileNumber()+"(REG)";

                /*smsContent = "Sentiatend:" + appState.userType+", "+uniqueId+", "+woman.getWomanId()+", "
                        +"PW-"
                        +woman.getRegWomanName()+", "+user.getFacilityName()+", Reg";*/ //23MAy2021 Bindu change content
            else
                smsContent = "Sentiatend:" +appState.userType+", "+uniqueId+", "+
                        woman.getWomanId()+", "+"M-"
                        +woman.getRegWomanName()+", "+user.getFacilityName()+", Reg";//13May2021 Arpitha



            if (p_no.length() <= 0) {
                display_messagedialog(transRepo, databaseHelper,smsContent);
            } else
                sending(transRepo, databaseHelper, p_no,smsContent);

           /* String smsContent = "";

            String hrp = "";

            if(woman.getRegComplicatedpreg()==1)
                hrp = " , HRP- "+woman.getRegCurrHealthRiskFactors()+" "+woman.getRegPrevHealthRiskFactors();

            String gestationWks =  woman.getRegStatusWhileRegistration();
            gestationWks = gestationWks.replace("Weeks","W");
            gestationWks = gestationWks.replace("Days","D");

            smsContent = "Matruksha :-"+woman.getRegWomanName() +", "+villageName+" ,"+gestationWks
                    +hrp;
            InserttblMessageLog("8892759870",
                    smsContent, user.getUserId(), transRepo, databaseHelper);
            if (isMessageLogsaved) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.sending_sms),
                        Toast.LENGTH_LONG).show();
                sendSMSFunction();


            }*/
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    /**
     * This method invokes after successfull save of record Sends SMS to the
     * Specified Number
     */// 13Oct2016 Arpitha
    private void sendSMSFunction() throws Exception {
        messageSent = true;
        new SendSMS(this).checkAndSendMsg(databaseHelper);

    }

    // 13Oct2016 Arpitha
    public void InserttblMessageLog(String phoneno, String content, String user_id,
                                    TransactionHeaderRepository transactionHeaderRepository, DatabaseHelper databaseHelper) throws Exception {
        final ArrayList<MessageLogPojo> mlpArr = new ArrayList<MessageLogPojo>();

//        final TransactionHeaderRepository transactionHeaderRepository = new TransactionHeaderRepository(databaseHelper);
//        final int transId = transactionHeaderRepository.iCreateNewTrans(user.getUserId(), databaseHelper);


        if (phoneno.length() > 0) {
            String[] phn = phoneno.split("\\,");
            for (int i = 0; i < phn.length; i++) {
                String num = phn[i];
                if (num != null && num.length() > 0) {
                    MessageLogPojo mlp =
                            new MessageLogPojo();
                    mlp.setUserId(user_id);
                    mlp.setMsgPhoneNo(num);
                    mlp.setMsgBody(content);
                    mlp.setMsgPriority(1);
                    mlp.setMsgNoOfFailures(0);
                    mlp.setTransId(transId);
                    mlp.setWomanId(regData.getWomanId());
                    mlp.setMsgSentDate(DateTimeUtil.getTodaysDate());
                    mlp.setMsgSent(0);
                    mlp.setMsgStage("Registration");//13May2021 Arpitha
                    mlp.setMsgUserType(appState.userType);
                    mlp.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
                    mlp.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());//21May2021 Arpitha
                    mlpArr.add(mlp);
                }
            }
        }


//        TransactionManager.callInTransaction(databaseHelper.getConnectionSource(), new Callable<Void>() {
//            public Void call() throws Exception {

        int addMessage = 0;
        SMSRepository smsRepository = new SMSRepository(databaseHelper);
        if (mlpArr != null) {
            for (final MessageLogPojo mlpp : mlpArr) {

                addMessage = smsRepository.insertIntoMessageLog(mlpp, databaseHelper);
            }

            if (addMessage > 0) {
                isMessageLogsaved = true;
                boolean addRegTrans = transactionHeaderRepository.iNewRecordTransNew(user.getUserId(), transId,
                        "tblmessagelog", databaseHelper);
            }
//                }
//
//                return null;
//            }
//        });


        }
    }

    // 13Oct2016 Arpitha
    public void display_messagedialog(final TransactionHeaderRepository
                                              transactionHeaderRepository,
                                      final DatabaseHelper databaseHelper,String smsContent) throws Exception {
        try {
            final Dialog sms_dialog = new Dialog(this);
//            sms_dialog.setTitle(getResources().getString(R.string.plsentervalidphoneno));

            sms_dialog.setContentView(R.layout.sms_dialog);


            sms_dialog.show();

            ImageButton imgsend = (ImageButton) sms_dialog.findViewById(R.id.imgsend);
            ImageButton imgcancel = (ImageButton) sms_dialog.findViewById(R.id.imgcancel);
            etphn = (EditText) sms_dialog.findViewById(R.id.etphno);
            final EditText etmess = (EditText) sms_dialog.findViewById(R.id.etmess);
            TextView txtcontcats = (TextView) sms_dialog.findViewById(R.id.txtcontacts);


//            etmess.setVisibility(View.GONE);
            etmess.setEnabled(false);
            etmess.setText(smsContent);
            txtcontcats.setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    // TODO Auto-generated method stub

                    Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
//					context.startActivity(intent);
                    startActivity(intent);
                    return true;
                }
            });


            //  etphn.setText(p_no);

            imgsend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {

                        sending(transactionHeaderRepository, databaseHelper,
                                etphn.getText().toString(),smsContent);

                    } catch (Exception e) {

                        e.printStackTrace();
                    }

                }
            });


            imgcancel.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    sms_dialog.cancel();
                    Intent exit = new Intent(ViewProfileActivity.this, RegisteredWomenActionTabs.class);
                    exit.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    startActivity(exit);
                }
            });
            sms_dialog.setCancelable(false);
        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    void sending(TransactionHeaderRepository transRepo, DatabaseHelper databaseHelper,
                 String phnNo, String smsCon) throws Exception {
        String smsContent = smsCon;



        InserttblMessageLog(phnNo,
                smsContent, user.getUserId(), transRepo, databaseHelper);
        if (isMessageLogsaved) {

            Intent exit = new Intent(ViewProfileActivity.this, RegisteredWomenActionTabs.class);
            exit.putExtra("globalState", getIntent().getBundleExtra("globalState"));
            startActivity(exit);
            sendSMSFunction();
        }else
        {
            Intent exit = new Intent(ViewProfileActivity.this, RegisteredWomenActionTabs.class);
            exit.putExtra("globalState", getIntent().getBundleExtra("globalState"));
            startActivity(exit);
        }
    }

    void setRiskIdentifiedByMM()
    {
        try {
            ArrayList<String> compList = new ArrayList<>();
            String complication = "";

            if(regData.getRegriskFactors()!=null &&
                    regData.getRegriskFactors().trim().length()>0
            )
                complication = regData.getRegriskFactors();
//            else
//                complication = filteredRowItems.get(selectedItemPosition).getRegriskFactors();
            if (complication != null && complication.length() > 0 ||
                    (regData.getRegriskFactors()!=null &&
                            regData.getRegriskFactors().trim().length()>0)) {
                compList = new ArrayList<>();
                compList.add(complication.replace(",", ", ").replace("PH:", "\nPH:"));


                ArrayList<String> complListDisp = new ArrayList<>();
                for (int i = 0; i < compList.size(); i++) {
                    String compl[] = compList.get(i).split("[,\n]");
                    for (int j = 0; j < compl.length; j++) {
                        String strComp = compl[j];
                        if (strComp.contains("CH:"))
                            strComp = strComp.replaceAll("CH:", "");
                        if (strComp.contains("PH:"))
                            strComp = strComp.replaceAll("PH:", "");

                        strComp = strComp.replaceAll(" ", "");
                        if (strComp.contains("ormoreconsecutivespontaneousabortions"))
//                            strComp = strComp.split("[<>0123456789(]")[1];
                            strComp = strComp.split("[3<>(]")[1];
                        else
                            strComp = strComp.split("[<>(]")[0];
                        //strComp = strComp.split("[<>0123456789(]")[0];
                        strComp = strComp.replaceAll("/", "");
                        strComp = strComp.toLowerCase().trim();
                        int identifier = getResources().getIdentifier
                                (strComp,
                                        "string", "com.sc.stmansi");
                        if (identifier > 0) {
                            if (compl[j].contains("CH:"))
                                complListDisp.add("CH: " + getResources().getString(identifier));
                            else if (compl[j].contains("PH:"))
                                complListDisp.add("PH: " + getResources().getString(identifier));
                            else
                                complListDisp.add(getResources().getString(identifier));

                        } else {
                           /* if(compl[j].contains("CH:"))
                            complListDisp.add("CH: "+compl[j]);
                           else if(compl[j].contains("PH:"))
                                complListDisp.add("PH: "+compl[j]);
                            else*/
                            complListDisp.add(compl[j]);

                        }
                    }

                }

                ArrayList<String> complications = new ArrayList<>();
                String strStrings = "";
                for (int i = 0; i < complListDisp.size(); i++) {
                    if (i != 0)
                        strStrings = strStrings + ", " + complListDisp.get(i);
                    else
                        strStrings = complListDisp.get(i);
                }

                String[] list = strStrings.split("PH:");

                ArrayList<String> l = new ArrayList<>();
                for (int i = 0; i < list.length; i++) {
                    if(!list[i].trim().equalsIgnoreCase(", ")) {
                        if (i == 0)
                            l.add(list[i]);
                        else
                            l.add("\nPH: " + list[i]);
                    }
                }

                String risk = "";
                for(int k = 0; k< l.size();k++)
                {
                    risk = risk+l.get(k);
                }
                aqVP.id(R.id.txtAmberStringmm).text(risk);

               /* if(filteredRowItems.get(selectedItemPosition).getRegCCConfirmRisk()!=null &&
                        filteredRowItems.get(selectedItemPosition).getRegCCConfirmRisk().trim().length()>0
                &&  filteredRowItems.get(selectedItemPosition)
                        .getRegCCConfirmRisk().equalsIgnoreCase("Y")) {
                   l = new ArrayList<>();
                    l.add("Risk Identified by CC - "+filteredRowItems.get(selectedItemPosition).getRegRiskFactorsByCC() + " " +
                            filteredRowItems.get(selectedItemPosition).getRegOtherRiskByCC());
                }*/


              /*  AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                View convertView = LayoutInflater.from(this).inflate(R.layout.complicated_reasons_list, null, false);

                ListView lv = convertView.findViewById(R.id.lvCompliatedReasonslist);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.listitem, l);

                alertDialog.setView(convertView).setCancelable(true).setTitle((this.getResources().getString(R.string.compl_reasons)))
                        .setPositiveButton((this.getResources().getString(R.string.ok)), new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                lv.setAdapter(adapter);
                alertDialog.show();
            }*/
            }

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    void setRiskIdentifiedbyCC(){
        try {
            ArrayList<String> compList = new ArrayList<>();
            String complication;

            /*if(filteredRowItems.get(selectedItemPosition).getRegCCConfirmRisk()!=null &&
                    filteredRowItems.get(selectedItemPosition).getRegCCConfirmRisk().trim().length()>0
                    &&  filteredRowItems.get(selectedItemPosition)
                    .getRegCCConfirmRisk().equalsIgnoreCase("Y"))*/
                complication = regData.getRegRiskFactorsByCC();
//                        +" "
//                        +filteredRowItems.get(selectedItemPosition).getRegOtherRiskByCC();
//            else
//                complication = filteredRowItems.get(selectedItemPosition).getRegriskFactors();
            if (complication != null && complication.length() > 0 ) {
                compList = new ArrayList<>();
                compList.add(complication.replace(",", ", ")
                        .replace("PH:", "\nPH:").replace(", ,","")
                        .replace(",,",""));


                ArrayList<String> complListDisp = new ArrayList<>();
                for (int i = 0; i < compList.size(); i++) {
                    String compl[] = compList.get(i).split("[,\n]");
                    for (int j = 0; j < compl.length; j++) {
                        String strComp = compl[j];
                        if (strComp.contains("CH:"))
                            strComp = strComp.replaceAll("CH:", "");
                        if (strComp.contains("PH:"))
                            strComp = strComp.replaceAll("PH:", "");

                        strComp = strComp.replaceAll(" ", "");
                        if (strComp.contains("ormoreconsecutivespontaneousabortions"))
//                            strComp = strComp.split("[<>0123456789(]")[1];
                            strComp = strComp.split("[3<>(]")[1];
                        else
                            strComp = strComp.split("[<>(]")[0];
                        //strComp = strComp.split("[<>0123456789(]")[0];
                        strComp = strComp.replaceAll("/","");
                        strComp = strComp.toLowerCase().trim();
                        int identifier = getResources().getIdentifier
                                (strComp,
                                        "string", "com.sc.stmansi");
                        if (identifier > 0) {
                            if (compl[j].contains("CH:"))
                                complListDisp.add("CH: " + getResources().getString(identifier));
                            else if (compl[j].contains("PH:"))
                                complListDisp.add("PH: " + getResources().getString(identifier));
                            else
                                complListDisp.add(getResources().getString(identifier));

                        } else {
                           /* if(compl[j].contains("CH:"))
                            complListDisp.add("CH: "+compl[j]);
                           else if(compl[j].contains("PH:"))
                                complListDisp.add("PH: "+compl[j]);
                            else*/
                            complListDisp.add(compl[j]);

                        }
                    }

                }

                ArrayList<String> complications = new ArrayList<>();
                String strStrings = "";
                for (int i = 0; i < complListDisp.size(); i++) {
                    if (i != 0)
                        strStrings = strStrings + ", " + complListDisp.get(i);
                    else
                        strStrings = complListDisp.get(i);
                }

                strStrings = strStrings.replace(", ,","")
                        .replace(",,","");

                String[] list = strStrings.split("PH:");

                ArrayList<String> l = new ArrayList<>();
                for (int i = 0; i < list.length; i++) {
                    if(!list[i].trim().equalsIgnoreCase(",")) {
                        if (i == 0)
                            l.add(list[i]);
                        else
                            l.add("\nPH: " + list[i]);
                    }
                }


                String risk = "";
                for(int k = 0; k< l.size();k++)
                {
                    risk = risk+l.get(k);
                }
                aqVP.id(R.id.txtccString).text(risk);


               /* if(filteredRowItems.get(selectedItemPosition).getRegCCConfirmRisk()!=null &&
                        filteredRowItems.get(selectedItemPosition).getRegCCConfirmRisk().trim().length()>0
                &&  filteredRowItems.get(selectedItemPosition)
                        .getRegCCConfirmRisk().equalsIgnoreCase("Y")) {
                   l = new ArrayList<>();
                    l.add("Risk Identified by CC - "+filteredRowItems.get(selectedItemPosition).getRegRiskFactorsByCC() + " " +
                            filteredRowItems.get(selectedItemPosition).getRegOtherRiskByCC());
                }*/



            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }
}


