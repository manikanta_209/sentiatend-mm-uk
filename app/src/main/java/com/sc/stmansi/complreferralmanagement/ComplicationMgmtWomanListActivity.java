package com.sc.stmansi.complreferralmanagement;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.About.AboutActivity;
import com.sc.stmansi.HomeVisit.HomeVisit;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.pendingservices.IndicatorViewClickListener;
import com.sc.stmansi.quickAction.ActionItem;
import com.sc.stmansi.quickAction.QuickAction;
import com.sc.stmansi.recyclerview.ClickListener;
import com.sc.stmansi.recyclerview.ComplRefMgmtWCListAdapter;
import com.sc.stmansi.recyclerview.RecyclerViewUpdateListener;
import com.sc.stmansi.repositories.ComplicationMgmtRepository;
import com.sc.stmansi.repositories.FacilityRepository;
import com.sc.stmansi.repositories.LoginRepository;
import com.sc.stmansi.repositories.TransactionHeaderRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.services.ServicesSummaryActivity;
import com.sc.stmansi.services.UnplannedServicesListActivity;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.TblRegComplMgmt;
import com.sc.stmansi.viewprofile.ViewProfileActivity;
import com.sc.stmansi.womanlist.CareType;
import com.sc.stmansi.womanlist.QueryParams;
import com.sc.stmansi.womanlist.RegisteredWomenActionTabs;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.sc.stmansi.R;

public class ComplicationMgmtWomanListActivity extends AppCompatActivity implements
        ClickListener, RecyclerViewUpdateListener,
        IndicatorViewClickListener,AdapterView.OnItemSelectedListener {

    private TblInstusers user;
    private AQuery aQuery;

    private String wFilterStr;

    private TextView allCountView, noDataLabelView;

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private LinearLayoutManager linearLayoutManager;

    private List<TblRegComplMgmt> filteredRowItems;
    private boolean isUpdating;

    private DatabaseHelper databaseHelper;
    private AppState appState;
    //private WomanRepository womanRepository;
    private int nextItemPosition;

    //private String womenCondition;
    private long womenCount;

    //31Aug2019 - Bindu
    private static final int ID_WEDIT = 1;
    private static final int ID_SERVLIST = 2;
    private static final int ID_UNPLANNEDSERVLIST = 3;
    private static final int ID_HOMEVISITADD = 4;
    private static final int ID_DEL = 5;//31Oct2019 Arpitha
    QuickAction mQuickAction = null;
    TblRegComplMgmt woman;
    private Spinner villageSpinner;
    private Map<String, Integer> villageCodeForName;
    private boolean spinnerSelectionChanged = false;
    private static final int ID_DEACT = 7;//19Nov2019 Arpitha
    private ComplicationMgmtRepository complmgmtRepository;

    List<TblRegComplMgmt> list;
    private RecyclerView.Adapter regcomplAdapter;
    private int complAutoId;
    //20Dec2019 - Bindu
    private int complVisitNum;

    /**
     * Initialize font, database, UsersPojo variables and village spinner
     * calls - populateWomenList
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complrefmgmtlist);
        aQuery = new AQuery(this);
        try {
            databaseHelper = getHelper();

            Bundle bundle = this.getIntent().getParcelableExtra("globalState");
            appState = bundle.getParcelable("appState");

            UserRepository userRepository = new UserRepository(databaseHelper);
            user = userRepository.getOneAuditedUser(appState.ashaId);

            complmgmtRepository = new ComplicationMgmtRepository(databaseHelper);
            list = complmgmtRepository.getComplMgmtNotificationData(user.getUserId(),0,"",0);
            if(list != null && list.size() > 0)
                womenCount = list.size();

            wFilterStr = this.getIntent().getStringExtra("wFilterStr");

            Bundle b = getIntent().getBundleExtra("globalState");
          //  womenCondition = "preg";

            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

            //22Jul2019- Bindu
            getSupportActionBar().setCustomView(R.layout.ejanani_village_filter);
            getSupportActionBar().setDisplayShowCustomEnabled(true);

            villageSpinner = aQuery.id(R.id.spVillageFilter).getSpinner();
            villageSpinner.setOnItemSelectedListener(this);
            populateSpinnerVillage();

            aQuery.id(R.id.txtlabell).text(getResources().getString(R.string.casemgmtstatus));


            noDataLabelView = aQuery.id(R.id.txtnodata).getTextView();
            allCountView = aQuery.id(R.id.txtcountwl).getTextView();
            allCountView.setText(getResources().getString(R.string.recordcount, womenCount));

            recyclerView = findViewById(R.id.complmgmt_recyclerview);
            recyclerView.setVisibility(View.VISIBLE);
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            recyclerView.setHasFixedSize(true);

            // use a linear layout manager
            layoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(layoutManager);
            linearLayoutManager = (LinearLayoutManager) layoutManager;

            QueryParams params = new QueryParams();
            params.textViewFilter = "";
            params.selectedVillageCode = 0;
            params.nextItemPosition = 0;

            filteredRowItems = new ArrayList<>();
            filteredRowItems = list;
            isUpdating = true;
             regcomplAdapter = new ComplRefMgmtWCListAdapter(list, this);
            recyclerView.setAdapter(regcomplAdapter);

            updateLabels();
            regcomplAdapter.notifyItemRangeInserted(0, filteredRowItems.size());
            nextItemPosition = filteredRowItems.size() + 1;

//            recyclerView.addOnScrollListener(new OnRecyclerScrollListener());
            isUpdating = false;

          //  new LoadList(this, params, true).execute();


            recyclerView.addOnScrollListener(new OnRecyclerScrollListener());


            //31Aug2019 - Bindu - ADD items for quick action
            String str = this.getResources().getString(R.string.viewprofile);
            ActionItem wEdit = new ActionItem(ID_WEDIT, str, ContextCompat.getDrawable(this, R.drawable.ic_edit_60));

            str = this.getResources().getString(R.string.services);
            ActionItem wPlannedServList = new ActionItem(ID_SERVLIST, str, ContextCompat.getDrawable(this, R.drawable.anm_pending_activities));

            str = this.getResources().getString(R.string.unplannedservices);
            ActionItem wUnplannedServList = new ActionItem(ID_UNPLANNEDSERVLIST, str, ContextCompat.getDrawable(this, R.drawable.anm_pending_activities));

            str = this.getResources().getString(R.string.homevisit);
            ActionItem wHomeVisitAdd = new ActionItem(ID_HOMEVISITADD, str, ContextCompat.getDrawable(this, R.drawable.ic_homevisit));

            str = this.getResources().getString(R.string.deliveryHeading);
            ActionItem wDel = new ActionItem(ID_DEL, str, ContextCompat.getDrawable(this, R.drawable.delivery_info));

            //          19Nov2019 Arpitha
            str = getResources().getString(R.string.deactivate);
            ActionItem wDeact = new ActionItem(ID_DEACT, str, ContextCompat.getDrawable(this,R.drawable.deactivate));


            mQuickAction = new QuickAction(this);

            mQuickAction.addActionItem(wEdit);
            mQuickAction.addActionItem(wPlannedServList);
            mQuickAction.addActionItem(wUnplannedServList);
            mQuickAction.addActionItem(wHomeVisitAdd);
            mQuickAction.addActionItem(wDel);
            mQuickAction.addActionItem(wDeact);//19Nov2019 Arpitha

            //setup the action item click listener
            mQuickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
                @Override
                public void onItemClick(QuickAction quickAction, int pos, int actionId) {
                    ActionItem actionItem = quickAction.getActionItem(pos);
                    try {
                        switch (actionItem.getActionId()) {
                            case ID_WEDIT: {
                                Intent nextScreen = new Intent(ComplicationMgmtWomanListActivity.this, ViewProfileActivity.class);
                                nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                startActivity(nextScreen);
                                mQuickAction.dismiss();
                                break;
                            }
                            case ID_SERVLIST: {
                                Intent nextScreen = new Intent(ComplicationMgmtWomanListActivity.this, ServicesSummaryActivity.class);
                                nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                startActivity(nextScreen);
                                mQuickAction.dismiss();
                                break;
                            }
                            case ID_UNPLANNEDSERVLIST: {
                                Intent nextScreen = new Intent(ComplicationMgmtWomanListActivity.this, UnplannedServicesListActivity.class);
                                nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                startActivity(nextScreen);
                                mQuickAction.dismiss();
                                break;
                            }
                            case ID_HOMEVISITADD: {
                                //18Oct2019 - Bindu - Chk nearing del message

                                int daysDiff = DateTimeUtil.getDaysBetweenDates(DateTimeUtil.getTodaysDate(), woman.getRegLMP());
                                if (daysDiff > 210) {
                                    displayNearingDelAlertMessage(woman);
                                } else {
                                    Intent nextScreen = new Intent(ComplicationMgmtWomanListActivity.this, HomeVisit.class);
                                    nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                    startActivity(nextScreen);
                                    mQuickAction.dismiss();
                                }
                                break;
                            }
                            case ID_DEL: {
                                /*WomanRepository womanRepository = new WomanRepository(databaseHelper);
                                woman = womanRepository.getRegistartionDetails(appState.selectedWomanId, appState.sessionUserId);

                                if (woman.getRegPregnantorMother() == 2 || DateTimeUtil.getNumberOfDaysFromLMP(woman.getRegLMP()) > 210) {

                                    Intent nextScreen = new Intent(ComplicationMgmtWomanListActivity.this, DeliveryInfoActivity.class);
                                    nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                    nextScreen.putExtra("woman", woman);
                                    startActivity(nextScreen);
                                    mQuickAction.dismiss();
                                } else
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.m132), Toast.LENGTH_LONG).show();
*/
                                break;
                            }//                          19Nov2019 Arpitha
                            case ID_DEACT:
                                /*WomanRepository womanRepository = new WomanRepository(databaseHelper);
                                woman = womanRepository.getRegistartionDetails(appState.selectedWomanId, appState.sessionUserId);
                                Intent deact = new Intent(ComplicationMgmtWomanListActivity.this, WomanDeactivateActivity.class);
                                deact.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                deact.putExtra("woman", woman);
                                startActivity(deact);*/
                                mQuickAction.dismiss();
                                break;
                        }
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                    }
                }
            });


            EditText filterTextView = aQuery.id(R.id.etWomanFilter).getEditText();
            filterTextView.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    aQuery.id(R.id.imgresetfilter).getImageView().setBackgroundResource(R.drawable.ic_reset);
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            filterTextView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (v.getText() != null) {
                        wFilterStr = v.getText().toString();
                        updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr, false);
                    }
                    return true;
                }
            });

            aQuery.id(R.id.imgresetfilter).getImageView().setOnClickListener(new FilterResetListener());



            //complmgmtRepository.getComplicationmgmtData(0,"");
        } catch (SQLException e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //    initialize db object
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void updateList(String
                                     villageTitle, Map<String, Integer> villageNameForCode, String textViewFilter, boolean checked) {
        try {
            QueryParams queryParams = new QueryParams();
            queryParams.nextItemPosition = 0;
            queryParams.selectedVillageTitle = villageTitle;
            queryParams.textViewFilter = textViewFilter;

            this.wFilterStr = textViewFilter;

            new LoadList(this, queryParams, true, villageNameForCode).execute();
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    //    load list data
    static class LoadList extends AsyncTask<Void, Void, Void> {

        private ComplicationMgmtWomanListActivity viewsUpdateCallback;
        private QueryParams queryParams;
        private boolean firstLoad;
        private Map<String, Integer> villageCodeForName;

        LoadList(ComplicationMgmtWomanListActivity viewsUpdateCallback, QueryParams queryParams, boolean firstLoad, Map<String, Integer> villageCodeForName) {
            this.viewsUpdateCallback = viewsUpdateCallback;
            this.queryParams = queryParams;
            this.firstLoad = firstLoad;
            this.villageCodeForName = villageCodeForName;
        }

        LoadList(ComplicationMgmtWomanListActivity viewsUpdateCallback, QueryParams queryParams, boolean firstLoad) {
            this.viewsUpdateCallback = viewsUpdateCallback;
            this.queryParams = queryParams;
            this.firstLoad = firstLoad;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            int selectedCode;
            // This means second constructor was used (called by scrollListener)
            if (villageCodeForName == null)
                selectedCode = queryParams.selectedVillageCode;
            else {
                String selectedVillageTitle = queryParams.selectedVillageTitle;
                if (selectedVillageTitle == null)
                    selectedCode = 0;
                else {
                    Integer code = villageCodeForName.get(selectedVillageTitle);
                    selectedCode = (code == null) ? 0 : code;
                }
            }
            try {
                List<TblRegComplMgmt> rows = viewsUpdateCallback.prepareRowItemsForDisplay(queryParams.nextItemPosition,
                        selectedCode, queryParams.textViewFilter);
                if (firstLoad) {
                    int prevSize = viewsUpdateCallback.filteredRowItems.size();
                    viewsUpdateCallback.filteredRowItems.clear();
                    viewsUpdateCallback.regcomplAdapter.notifyItemRangeRemoved(0, prevSize);
                    firstLoad = false;
                }
                if (rows.size() > 0) {
                    viewsUpdateCallback.filteredRowItems.addAll(rows);

                    viewsUpdateCallback.nextItemPosition = viewsUpdateCallback.filteredRowItems.size();
                    viewsUpdateCallback.regcomplAdapter.notifyItemRangeInserted(firstLoad ? 0 : viewsUpdateCallback.nextItemPosition,
                            viewsUpdateCallback.filteredRowItems.size());
                }
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            try {
                viewsUpdateCallback.updateLabels();
                if (viewsUpdateCallback.filteredRowItems.size() > 0) {

                    viewsUpdateCallback.recyclerView.setVisibility(View.VISIBLE);
                    if (!firstLoad && viewsUpdateCallback.filteredRowItems.size() > 0)
                        viewsUpdateCallback.layoutManager.scrollToPosition(viewsUpdateCallback.regcomplAdapter.getItemCount());
                    viewsUpdateCallback.isUpdating = false;

                    if (viewsUpdateCallback.filteredRowItems.size() == 10)
                        viewsUpdateCallback.allCountView.setText(viewsUpdateCallback.getResources().
                                getString(R.string.recordcount,
                                        viewsUpdateCallback.womenCount));
                    else
                        viewsUpdateCallback.allCountView.setText(viewsUpdateCallback.getResources().
                                getString(R.string.recordcount,
                                        viewsUpdateCallback.filteredRowItems.size()));


                }
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

            }
        }
    }

    //    update labels based on data
    private void updateLabels() {
        if (filteredRowItems.size() == 0) {
            recyclerView.setVisibility(View.GONE);

            noDataLabelView.setText(getResources().getString(R.string.no_data));
            noDataLabelView.setVisibility(View.VISIBLE);
            return;
        }
        noDataLabelView.setVisibility(View.GONE);
    }

    /**
     * get the Display pojo list from the given womanIds
     *
     * @param limitStart
     * @param villageCode
     * @param filter
     */
    private List<TblRegComplMgmt> prepareRowItemsForDisplay(int limitStart,
                                                            int villageCode, String filter) throws Exception {
        return complmgmtRepository.getComplMgmtNotificationData(user.getUserId(),
                villageCode, filter, limitStart);
    }

    @Override
    public void onClick(View v) {
        setSelectedWomanId(v);

        TransactionHeaderRepository transRepo = new TransactionHeaderRepository(databaseHelper);
        final int transId = transRepo.iCreateNewTrans(appState.sessionUserId, databaseHelper);

        ComplicationMgmtRepository compRepo = new ComplicationMgmtRepository(databaseHelper);
        compRepo.updateUserSeenDate(appState.selectedWomanId, complAutoId,appState.sessionUserId,transId); //19Dec2019 - Bindu - change from woman userid to appstate userid

        Intent intent = new Intent(this, ComplMgmtVisitViewActivity.class);
        intent.putExtra("globalState", this.getIntent().getBundleExtra("globalState"));
        intent.putExtra("complVisitNum", complVisitNum); //20Dec2019 - Bindu - Add visit num
        startActivity(intent);

      //  mQuickAction.show(v);
    }

    @Override
    public boolean onLongClick(View v) {
        setSelectedWomanId(v);

        Intent intent = new Intent(this, ViewProfileActivity.class);
        intent.putExtra("globalState", this.getIntent().getBundleExtra("globalState"));
        startActivity(intent);
        return true;
    }

    //    set woman id
    private void setSelectedWomanId(View v) {
        int itemPosition = recyclerView.getChildLayoutPosition(v);
        woman = filteredRowItems.get(itemPosition);
        appState.selectedWomanId = woman.getWomanId();
        complAutoId = filteredRowItems.get(itemPosition).getAutoId();
        complVisitNum = filteredRowItems.get(itemPosition).getVisitNum(); //20Dec2019 - bindu - add visit num
    }

    //    prepare bundle
    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        return b;
    }

    //    recycler view scroll
    class OnRecyclerScrollListener extends RecyclerView.OnScrollListener {
        int ydy = 0;

        @Override
        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            ydy = dy;

            int lastCompletelyVisibleItemPosition = linearLayoutManager.findLastCompletelyVisibleItemPosition();
            boolean shouldPullUpRefresh = lastCompletelyVisibleItemPosition == filteredRowItems.size() - 1
                    && recyclerView.getScrollState() == RecyclerView.SCROLL_STATE_DRAGGING;
            if (shouldPullUpRefresh) {
                QueryParams queryParams = new QueryParams();
                queryParams.nextItemPosition = nextItemPosition;
                queryParams.selectedVillageCode = appState.selectedVillageCode;
                queryParams.textViewFilter = wFilterStr;
                if (!isUpdating) {
                    isUpdating = true;
                    new LoadList(ComplicationMgmtWomanListActivity.this, queryParams, false).execute();
                }
            }
        }
    }

    @Override
    public void displayAlert(int selectedItemPosition) {
        /*ArrayList<String> compList;
        String complication = filteredRowItems.get(selectedItemPosition).getRegriskFactors();
        if (complication != null && complication.length() > 0) {
            compList = new ArrayList<>();
            compList.add(complication.replace(",", ", ").replace("PH:", "\nPH:"));

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            View convertView = LayoutInflater.from(this).inflate(R.layout.complicated_reasons_list, null, false);

            ListView lv = convertView.findViewById(R.id.lvCompliatedReasonslist);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.listitem, compList);

            alertDialog.setView(convertView).setCancelable(true).setTitle((this.getResources().getString(R.string.compl_reasons)))
                    .setPositiveButton((this.getResources().getString(R.string.ok)), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
            lv.setAdapter(adapter);
            alertDialog.show();
        }*/
    }

    @Override
    public void displayHomeVisitList(int selectedItemPosition) {
        /*tblregisteredwomen woman = filteredRowItems.get(selectedItemPosition);
        Intent homevisitlist = new Intent(this, HomeVisitListActivity.class);
        appState.selectedWomanId = woman.getWomanId();
        homevisitlist.putExtra("globalState", prepareBundle());
        startActivity(homevisitlist);*/
    }

    @Override
    public void displayDelComplicationAlert(int selectedItemPosition) {

    }

    //18Oct2019 - Bindu - Alert message nearing del
    public void displayNearingDelAlertMessage(TblRegComplMgmt woman) throws Exception {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

       // String alertmsg = woman.getRegWomanName() + ", " + " EDD : " + woman.getRegEDD();
        String alertmsg = ""; //pending
        //alertDialogBuilder.setMessage(message).setCancelable(false).setTitle(Html.fromHtml("<font color='#FF7F27'>Alert -- </font><br><br><p>"+alertmsg + "</p>")).setNegativeButton(
        String msg = "<font color='#CC3333'> Nearing Delivery - </font>" + alertmsg;
        alertDialogBuilder.setMessage(getResources().getText(R.string.alerthomevisitnearingdelmsg)).setCancelable(false).setTitle(Html.fromHtml(msg)).setNegativeButton(
                getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent nextScreen = new Intent(ComplicationMgmtWomanListActivity.this, HomeVisit.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        nextScreen.putExtra("woman", ComplicationMgmtWomanListActivity.this.woman);
                        startActivity(nextScreen);
                        mQuickAction.dismiss();
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    /**
     * populate Village to Spinner
     */
    private void populateSpinnerVillage() throws Exception {
        FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
        villageCodeForName = facilityRepository.getPlaceMap();

        ArrayAdapter<CharSequence> villageSpinnerAdapter = new ArrayAdapter<>(this, R.layout.actionbar_spinner_list_item);
        villageSpinnerAdapter.add("All Women");
        for (Map.Entry<String, Integer> village : villageCodeForName.entrySet()) {
            villageSpinnerAdapter.add(village.getKey());
        }
        villageSpinnerAdapter.setDropDownViewResource(R.layout.actionbar_spinner_layout_view);
        villageSpinner.setAdapter(villageSpinnerAdapter);
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        appState.selectedVillageTitle = (String) parent.getItemAtPosition(position);
        if (spinnerSelectionChanged) {
            updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr, false);
        }
        spinnerSelectionChanged = true;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.servicesmenu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Called whenever an item clicked in actionbar
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action buttons
        switch (item.getItemId()) {

            case R.id.home: {
                Intent goToScreen = new Intent(ComplicationMgmtWomanListActivity.this, MainMenuActivity.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }

            case android.R.id.home: {
                Intent goToScreen = new Intent(ComplicationMgmtWomanListActivity.this, MainMenuActivity.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                startActivity(goToScreen);

                return true;
            }

            case R.id.logout: {
                Intent goToScreen = new Intent(ComplicationMgmtWomanListActivity.this, LoginActivity.class);
                displayAlert(getResources().getString(R.string.m111), goToScreen);
                return true;
            }


            case R.id.wlist: {
                Intent goToScreen = new Intent(ComplicationMgmtWomanListActivity.this, RegisteredWomenActionTabs.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }
            //30Sep2019 - Bindu
            case R.id.about: {
                Intent goToScreen = new Intent(ComplicationMgmtWomanListActivity.this, AboutActivity.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Save Confirmation Alert
     */
    private void displayAlert(final String spanText2, final Intent goToScreen) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set dialog message
        alertDialogBuilder
                .setMessage(spanText2)
                .setCancelable(false)
                .setNegativeButton((getResources().getString(R.string.m121)), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (goToScreen != null) {
                            startActivity(goToScreen);
                            if (spanText2.contains("logout")) {
                                try {
                                    LoginRepository loginRepository = new LoginRepository(databaseHelper);
                                    loginRepository.updateLogoutTime(appState.loginAuditId, appState.sessionUserId);
                                } catch (Exception e) {
                                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                                }
                            }
                        } else
                            dialog.cancel();
                    }
                })
                .setPositiveButton((getResources().getString(R.string.m122)), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    class FilterResetListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            aQuery.id(R.id.imgresetfilter).getImageView().setBackgroundResource(R.drawable.ic_reset_gray);
            aQuery.id(R.id.etWomanFilter).text("");
            wFilterStr = "";
            CareType careType = CareType.GENERAL;
            updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr, false);
        }
    }

    @Override
    public void displayLMPConfirmation(int selectedItemPosition) {

    }
}
