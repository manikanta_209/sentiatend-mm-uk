package com.sc.stmansi.complreferralmanagement;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.text.Html;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;

import com.sc.stmansi.login.LoginActivity;

import com.sc.stmansi.R;

public class NotificationBroadcast extends BroadcastReceiver {
    Context ctx;
    Notification notification;
    @Override
    public void onReceive(Context context, Intent intent) {
        this.ctx = context;
        getData();
        Log.i(NotificationBroadcast.class.getSimpleName(), "Service Stops! Oooooooooooooppppssssss!!!!");
        context.startService(new Intent(context, NotificationService.class));;
    }

    // Get Data
    private void getData() {
        displaynotification();
    }

    // display notification
    private void displaynotification() {
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(ctx);

        Intent notificationIntent = new Intent(ctx, LoginActivity.class);
        notificationIntent.putExtra("nitems", "Received message");
        notificationIntent.putExtra("pos", 0);
        stackBuilder.addParentStack(LoginActivity.class);
        stackBuilder.addNextIntent(notificationIntent);

        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(ctx);
        notification = builder
                // .setContentTitle("Partograph")
                .setContentTitle(Html.fromHtml("<font color='" + ctx.getResources().getColor(R.color.colorPrimary)
                        + "'><b> Matruksha</b></font>"))

                .setStyle(new NotificationCompat.BigTextStyle().bigText("Matruksha Monitoring"))
                .setContentText(Html.fromHtml("<font color='" + ctx.getResources().getColor(R.color.colorPrimary)
                        + "'><b> " + "Msg from Facility" + "</b></font>"))
                .setTicker("Matrukshaa Alert!").setSmallIcon(R.drawable.notification).setNumber(1)
                .setSound(soundUri)

                .setContentIntent(pendingIntent).build();

        notification.contentIntent = pendingIntent;
        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        NotificationManager notificationManager = (NotificationManager) ctx
                .getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(4, notification);
    }


}
