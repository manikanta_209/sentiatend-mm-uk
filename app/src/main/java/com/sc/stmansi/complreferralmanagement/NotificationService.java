package com.sc.stmansi.complreferralmanagement;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.common.InternetConnectionUtil;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.configuration.SyncState;
import com.sc.stmansi.database.DBMethods;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.followupCaseMgmt.MyPeriodicWork;
import com.sc.stmansi.initialization.AppPropertiesLoader;
import com.sc.stmansi.repositories.CaseMgmtRepository;
import com.sc.stmansi.repositories.ComplicationMgmtRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.sync.DSSyncFunction;
import com.sc.stmansi.tables.TblInstusers;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class NotificationService extends Service implements LifecycleOwner {
    public Runnable mRunnable = null;
    static AppState appState;
    private static DatabaseHelper databaseHelper;
    private static SyncState syncState;
    private static TblInstusers user;
    private static DBMethods dbMethods;
    private ArrayList<String> riskDetails = new ArrayList<>();

    Context ctx;
    public int counter=0;
    AsyncCallComplMgmt task1;

    @Override
    public void onCreate() {
        super.onCreate();
        try {
            appState = new AppState();
            databaseHelper = getHelper();
            dbMethods = new DBMethods(databaseHelper);
            syncState = new SyncState();
            UserRepository userRepository = new UserRepository(databaseHelper);
            user = userRepository.getAuditedUsers();
            AppPropertiesLoader.loadProperties(getApplicationContext(), appState);
            ctx = getApplicationContext();
        }catch (Exception e) {
            e.printStackTrace();
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        try {

            startTimer();
            Log.i(NotificationService.class.getSimpleName(), "Service running Matruksha!!!!");

            //notification();
            /*Toast.makeText(getApplicationContext(), "Service running Matruksha", Toast.LENGTH_SHORT).show();

             task1 = new AsyncCallComplMgmt(NotificationService.this);
            task1.execute();*/
            //StartAsyncTaskInParallelDS(task1);

        }catch(Exception e) {
            e.printStackTrace();
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }

        /*final Handler mHandler = new Handler();
        mRunnable = new Runnable() {
            @Override
            public void run() {
                try {
                appState = new AppState();
                databaseHelper = getHelper();
                dbMethods = new DBMethods(databaseHelper);
                syncState = new SyncState();
                UserRepository userRepository = new UserRepository(databaseHelper);
                user = userRepository.getAuditedUsers();
                AppPropertiesLoader.loadProperties(getApplicationContext(), appState);

                    //notification();
                    Toast.makeText(getApplicationContext(), "Service running Matruksha", Toast.LENGTH_SHORT).show();

                    boolean isinternetavail = InternetConnectionUtil.isConnectionAvailable(getApplicationContext(), appState);

                    if(isinternetavail) {
                        String hrpcomplmgmtdatelastupdated = "", serverResult;
                        ComplicationMgmtRepository complrepo = new ComplicationMgmtRepository(databaseHelper);
                        hrpcomplmgmtdatelastupdated = complrepo.getDateLastUpdated(appState.sessionUserId);
                        serverResult = new DSSyncFunction(databaseHelper, appState, syncState, user)
                                .SyncFunctionCompl
                                        (dbMethods, user.getUserId(),
                                                user.getInst_Database(),
                                                appState.webServiceIpAddress + appState.webServicePath,
                                                hrpcomplmgmtdatelastupdated);
                    }

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }
                mHandler.postDelayed(mRunnable, 10 * 8000);
            }
        };*/
       // mHandler.postDelayed(mRunnable, 10 * 8000);
       // return super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        //super.onTaskRemoved(rootIntent);
        /*Intent restartService = new Intent(getApplicationContext(), this.getClass());
        restartService.setPackage(getPackageName());
        PendingIntent restartServicePI = PendingIntent.getService(getApplicationContext(), 1, restartService,
                PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmService.set(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + 1000, restartServicePI);
        notification();*/
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(NotificationService.class.getSimpleName(), "Service Destroyed! Oooooooooooooppppssssss!!!!");

        Intent broadcastIntent = new Intent(this, NotificationBroadcast.class);
        sendBroadcast(broadcastIntent);
        stoptimertask();
    }

    public void notification() {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent notificationIntent = new Intent("android.media.action.DISPLAY_NOTIFICATION");
        notificationIntent.addCategory("android.intent.category.DEFAULT");
        PendingIntent broadcast = PendingIntent.getBroadcast(this, 100, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.SECOND, 10);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), 0, broadcast);

    }


    //	intialize db object
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @NonNull
    @Override
    public Lifecycle getLifecycle() {
        return null;
    }

    public class AsyncCallComplMgmt extends AsyncTask<String, Void, String> {

        public AsyncCallComplMgmt(NotificationService notserv) {
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... params) {
            String serverResult = null;
            try {
                Log.i("Service Async ", "Async task calling");
                boolean isinternetavail = InternetConnectionUtil.isConnectionAvailable(ctx, appState);

                if(isinternetavail) {
                    String hrpcomplmgmtdatelastupdated = "";
                    ComplicationMgmtRepository complrepo = new ComplicationMgmtRepository(databaseHelper);
                    hrpcomplmgmtdatelastupdated = complrepo.getDateLastUpdated(user.getUserId());
                    serverResult = new DSSyncFunction(databaseHelper, appState, syncState, user)
                            .SyncFunctionCompl
                                    (NotificationService.dbMethods, user.getUserId(),
                                            user.getInst_Database(),
                                            NotificationService.appState.webServiceIpAddress + appState.webServicePath,
                                            hrpcomplmgmtdatelastupdated);
                }
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            }
            return serverResult;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                Log.i("Service", "Matruksha");
                /*Intent broadcastIntent = new Intent(ctx, NotificationBroadcast.class);
                sendBroadcast(broadcastIntent);*/
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            }
        }
    }

    //	to execute async class
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void StartAsyncTaskInParallel(AsyncCallComplMgmt task) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            task.execute();
    }

    private Timer timer;
    private TimerTask timerTask;
    long oldTime=0;
    public void startTimer() {
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, delay 5 min and period 5 min in millisec
        timer.schedule(timerTask, 300000, 300000); //
    }

    /**
     * it sets the timer to print the counter every x seconds
     */
    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                Log.i("Service timer", "in timer ++++  "+ (counter++));
                task1 = new AsyncCallComplMgmt(NotificationService.this);
                task1.execute();
                getCaseManagment(); //Raesh 9-7-2021
               // StartAsyncTaskInParallel(task1);
            }
        };
    }

    /**
     * not needed
     */
    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }
    //Ramesh 9-7-2021
    private void getCaseManagment() {
        try {
            CaseMgmtRepository caseMgmtRepository = new CaseMgmtRepository(databaseHelper);
            String lastServerDate = caseMgmtRepository.getLastServerDate();
//            UserRepository userRepo = new UserRepository(databaseHelper);
//            TblInstusers user = userRepo.getOneAuditedUser(appState.ashaId);

            Data data = new Data.Builder()
                    .putString("webServiceIpAddress", appState.webServiceIpAddress)
                    .putInt("webServicePath", appState.apiPort)
                    .putString("lastServerDate", lastServerDate)
                    .putString("clientDB", user.getInst_Database())
                    .putString("MMId", user.getUserId())
                    .build();


            OneTimeWorkRequest periodicWorkRequest = new OneTimeWorkRequest.Builder(MyPeriodicWork.class)
                    .setInputData(data)
                    .build();

            WorkManager.getInstance().getWorkInfoByIdLiveData(periodicWorkRequest.getId()).observe(this, workInfo -> {
                if (workInfo != null) {
                    if (workInfo.getState() == WorkInfo.State.SUCCEEDED) {
                        String result = MyPeriodicWork.result;
                        updateCaseMgmt(result);
                        Log.e("Fetch Service",result);
                    } else if (workInfo.getState() == WorkInfo.State.FAILED) {
                        if (MyPeriodicWork.result.contains("failed to connect")) {
                            Log.e("Fetch Service","Failed to connect to server");
                        }
                    }
                }
            });
            WorkManager.getInstance().enqueue(periodicWorkRequest);
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private void updateCaseMgmt(String result) {
        try {
            ArrayList<ContentValues> arrVillage = null;
            SQLiteDatabase db;
            db = databaseHelper.getWritableDatabase();
            if (result.equals("Error")) {
                Log.e("Fetch Case Mgmt","Data not fetched, Try again later");
            } else {
                String xml = result;

                xml = xml.replace("<NewDataSet>", "")
                        .replace("</NewDataSet>", "").
                                replace("<NewDataSet />", "").trim();

                String[] strVal;
                strVal = xml.split("</Table>");
                for (String s : strVal) {
                    s = s + "</Table>";
                    InputStream is = new ByteArrayInputStream(s.getBytes(StandardCharsets.UTF_8));
                    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                    Document doc = dBuilder.parse(is);
                    doc.getDocumentElement().normalize();

                    NodeList nList = doc.getElementsByTagName("Table");

                    arrVillage = new ArrayList<>();

                    if (nList.getLength() > 0) {

                        for (int temp = 0; temp < nList.getLength(); temp++) {

                            Node nNode = nList.item(temp);

                            ContentValues values = new ContentValues();

                            Element eElement = (Element) nNode;

                            if (eElement.getElementsByTagName("regRiskFactorsByCC").item(0) != null) {

                                String dtemp = "update tblregisteredwomen Set ";

                                dtemp = dtemp + " regRiskFactorsByCC = '" +
                                        getTagValue(eElement, "regRiskFactorsByCC") + "', ";
                                dtemp = dtemp + " regOtherRiskByCC = '" +
                                        getTagValue(eElement, "regOtherRiskByCC") + "', ";
                                dtemp = dtemp + " regCCConfirmRisk = '" +
                                        getTagValue(eElement, "regCCConfirmRisk") +
                                        "' where  WomanId = '" +
                                        getTagValue(eElement, "WomanId");


                                riskDetails.add(dtemp);

                            } else {
                                values.put("UserId", getTagValue(eElement, "UserId"));
                                values.put("MMUserId", getTagValue(eElement, "MMUserId"));
                                values.put("BeneficiaryType", getTagValue(eElement, "BeneficiaryType"));
                                values.put("BeneficiaryID", getTagValue(eElement, "BeneficiaryID"));
                                values.put("chlNo", getTagValue(eElement, "chlNo"));
                                values.put("BeneficiaryParentID", getTagValue(eElement, "BeneficiaryParentID"));
                                values.put("VisitType", getTagValue(eElement, "VisitType"));
                                values.put("VisitMode", getTagValue(eElement, "VisitMode"));
                                values.put("VisitNum", getTagValue(eElement, "VisitNum"));
                                values.put("StatusAtVisit", getTagValue(eElement, "StatusAtVisit"));
                                values.put("VisitDate", getTagValue(eElement, "VisitDate"));
                                values.put("VisitNumbyMM", getTagValue(eElement, "VisitNumbyMM"));
                                values.put("VisitDatebyMM", getTagValue(eElement, "VisitDatebyMM"));
                                values.put("VisitBeneficiaryDangerSigns", getTagValue(eElement, "VisitBeneficiaryDangerSigns"));
                                values.put("hcmBeneficiaryVisitFac", getTagValue(eElement, "hcmBeneficiaryVisitFac"));
                                values.put("hcmReasonForNoVisit", getTagValue(eElement, "hcmReasonForNoVisit"));
                                values.put("hcmReasonOthers", getTagValue(eElement, "hcmReasonOthers"));
                                values.put("hcmActTakByUserType", getTagValue(eElement, "hcmActTakByUserType"));
                                values.put("hcmActTakByUserName", getTagValue(eElement, "hcmActTakByUserName"));
                                values.put("hcmActTakAtFacility", getTagValue(eElement, "hcmActTakAtFacility"));
                                values.put("hcmActTakFacilityName", getTagValue(eElement, "hcmActTakFacilityName"));
                                values.put("hcmActTakDate", getTagValue(eElement, "hcmActTakDate"));
                                values.put("hcmActTakTime", getTagValue(eElement, "hcmActTakTime"));
                                values.put("hcmActTakForCompl", getTagValue(eElement, "hcmActTakForCompl"));
                                values.put("hcmActMedicationsPres", getTagValue(eElement, "hcmActMedicationsPres"));
                                values.put("hcmActTakStatus", getTagValue(eElement, "hcmActTakStatus"));
                                values.put("hcmActTakStatusDate", getTagValue(eElement, "hcmActTakStatusDate"));
                                values.put("hcmActTakStatusTime", getTagValue(eElement, "hcmActTakStatusTime"));
                                values.put("hcmActTakReferredToFacility", getTagValue(eElement, "hcmActTakReferredToFacility"));
                                values.put("hcmActTakReferredToFacilityName", getTagValue(eElement, "hcmActTakReferredToFacilityName"));
                                values.put("hcmActTakComments", getTagValue(eElement, "hcmActTakComments"));
                                values.put("hcmAdviseGiven", getTagValue(eElement, "hcmAdviseGiven"));
                                values.put("hcmBeneficiaryCondatVisit", getTagValue(eElement, "hcmBeneficiaryCondatVisit"));
                                values.put("hcmActionToBeTakenClient", getTagValue(eElement, "hcmActionToBeTakenClient"));
                                values.put("hcmInputToNextLevel", getTagValue(eElement, "hcmInputToNextLevel"));
                                values.put("hcmIsANMInformedAbtCompl", getTagValue(eElement, "hcmIsANMInformedAbtCompl"));
                                values.put("hcmANMAwareOfRef", getTagValue(eElement, "hcmANMAwareOfRef"));
                                values.put("VisitReferredtoFacType", getTagValue(eElement, "VisitReferredtoFacType"));
                                values.put("VisitReferredtoFacName", getTagValue(eElement, "VisitReferredtoFacName"));
                                values.put("VisitReferralSlipNumber", getTagValue(eElement, "VisitReferralSlipNumber"));
                                values.put("hcmCreatedByUserType", getTagValue(eElement, "hcmCreatedByUserType"));
                                values.put("transId", getTagValue(eElement, "transId"));
                                values.put("RecordCreatedDate", getTagValue(eElement, "RecordCreatedDate"));
                                values.put("RecordUpdatedDate", getTagValue(eElement, "RecordUpdatedDate"));
                                values.put("ServerCreatedDate", getTagValue(eElement, "ServerCreatedDate"));
                                values.put("ServerUpdatedDate", getTagValue(eElement, "ServerUpdatedDate"));
                                arrVillage.add(values);
                            }

                        }
                    }

                    if (arrVillage != null && arrVillage.size() > 0) {
                        for (ContentValues values1 : arrVillage) {
                            db.insert("tblCaseMgmt", null, values1);
                        }
                    }

                    if (riskDetails.size() > 0) {
                        for (String str : riskDetails) {
                            db.execSQL(str);
                        }
                    }
                }
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    public String getTagValue(Element eElement, String tagName) {
        if (eElement.getElementsByTagName(tagName).item(0) != null)
            return eElement.getElementsByTagName(tagName).item(0).getTextContent();
        else
            return "";
    }
}
