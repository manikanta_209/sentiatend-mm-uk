package com.sc.stmansi.complreferralmanagement;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.legacy.app.ActionBarDrawerToggle;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.misc.TransactionManager;
import com.sc.stmansi.About.AboutActivity;
import com.sc.stmansi.HomeVisit.HomeVisit;
import com.sc.stmansi.HomeVisit.HomeVisitListActivity;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.common.NavDrawerItem;
import com.sc.stmansi.common.NavDrawerListAdapter;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.delivery.DeliveryInfoActivity;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.repositories.ComplicationMgmtRepository;
import com.sc.stmansi.repositories.LoginRepository;
import com.sc.stmansi.repositories.TransactionHeaderRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.services.ServicesSummaryActivity;
import com.sc.stmansi.services.UnplannedServicesListActivity;
import com.sc.stmansi.tables.tblHRPComplicationMgmt;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.viewprofile.ViewProfileActivity;
import com.sc.stmansi.womanlist.RegisteredWomenActionTabs;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import com.sc.stmansi.R;

public class ComplMgmtVisitViewActivity extends AppCompatActivity {
    private List<tblHRPComplicationMgmt> visitItems;
    private AQuery aq;
    private int index = 0;
    private tblHRPComplicationMgmt prevObj,nextObj;
    private tblHRPComplicationMgmt visitListData;
    private ArrayList<String> aList;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private ArrayList<NavDrawerItem> navDrawerItems;
    private ListView mDrawerList;
    private tblregisteredwomen woman;
    private int itempos = 0;
    private List<tblregisteredwomen> wVisitDetails;

    private AppState appState;
    private DatabaseHelper databaseHelper;
    ComplicationMgmtRepository complrepo;
    //20Dec2019 - Bindu
    int visitNum;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_complmgmt_view);
            aq = new AQuery(this);

            databaseHelper = getHelper();
            Bundle bundle = getIntent().getBundleExtra("globalState");
            appState = bundle.getParcelable("appState");

            WomanRepository womanRepo = new WomanRepository(databaseHelper);
            woman = womanRepo.getRegistartionDetails(appState.selectedWomanId, appState.sessionUserId);

            complrepo = new ComplicationMgmtRepository(databaseHelper);
            //20Dec2019 - Bindu - get visitnum
            visitNum = getIntent().getIntExtra("complVisitNum",0);
            visitItems = complrepo.getComplMgmtVisitDetails(appState.selectedWomanId, appState.sessionUserId, visitNum,""); //add visit num

            itempos = getIntent().getIntExtra("pos",0);
            index = itempos;

            initiateDrawer();

            aq.id(R.id.btnnextvisit).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try{
                        index = index + 1;
                        //nextObj = visitItems.get(index - 1);
                        if(index< visitItems.size()) {
                            if(visitItems.get(index).getHcmCommentsClient() == null || (visitItems.get(index).getHcmCommentsClient() != null && visitItems.get(index).getHcmCommentsClient().length() <= 0))
                                aq.id(R.id.etcomments).text("");
                            setData(visitItems,index);
                        }else {
                            index = index - 1;
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.norecords),Toast.LENGTH_LONG).show();
                        }
                    }catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                    }
                }
            });

            aq.id(R.id.btnpreviousvisit).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        index = index - 1;
                        if (index >= 0) {
                            if (visitItems.get(index).getHcmCommentsClient() == null || (visitItems.get(index).getHcmCommentsClient() != null && visitItems.get(index).getHcmCommentsClient().length() <= 0))
                                aq.id(R.id.etcomments).text("");
                            setData(visitItems, index);
                        } else {
                            index = index + 1;
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.norecords), Toast.LENGTH_LONG).show();
                        }
                    }catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                    }
                }
            });



          //  setInitialValues();
            setData(visitItems,itempos);
            // Symptoms drwable on touch listener
            aq.id(R.id.txtDangSignsL).getTextView().setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if(event.getAction() == MotionEvent.ACTION_UP) {
                        if(event.getRawX() <= aq.id(R.id.txtDangSignsL).getTextView().getTotalPaddingLeft()) {
                            opencloseAccordionDangerSigns(aq.id(R.id.txtDangSigns).getView(), aq.id(R.id.txtDangSignsL).getTextView());
                            return true;
                        }
                    }
                    return true;
                }
            });

            //03Oct2019 - Bindu - Get Visit details from reg table
            wVisitDetails = womanRepo.getVisitDetailsFrmReg(appState.selectedWomanId, appState.sessionUserId);

            //23Dec2019 - Bindu - Image edit button click listener
            aq.id(R.id.imgbtneditashacomments).getImageView().setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if(event.getAction() == MotionEvent.ACTION_UP) {
                            aq.id(R.id.etcomments).enabled(true).background(R.drawable.edittext_style);
                            aq.id(R.id.etcomments).setSelection(aq.id(R.id.etcomments).getText().toString().length());
                            return true;

                    }
                    return true;
                }
            });

            aq.id(R.id.txtChilddangersignsL).getTextView().setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if(event.getAction() == MotionEvent.ACTION_UP) {
                        if(event.getRawX() <= aq.id(R.id.txtChilddangersignsL).getTextView().getTotalPaddingLeft()) {
                            opencloseAccordionDangerSigns(aq.id(R.id.txtChilddangersigns).getView(), aq.id(R.id.txtChilddangersignsL).getTextView());
                            return true;
                        }
                    }
                    return true;
                }
            });
        }catch(Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    // Set Data for view based on item
    private void setData(final List<tblHRPComplicationMgmt> visitItems, final int index) throws Exception{

        aq.id(R.id.txtVisitNum).text(""+visitItems.get(index).getVisitNum());
        aq.id(R.id.txtVisitDateval).text(visitItems.get(index).getVisitDate());

        aq.id(R.id.txtDangSigns).text(visitItems.get(index).getVisitWomanDangerSigns());
        aq.id(R.id.txtusertypeval).text(visitItems.get(index).getHcmActTakByUserType());
        aq.id(R.id.txtusernameval).text(visitItems.get(index).getHcmActTakByUserName());
        aq.id(R.id.txtfacilitytypeval).text(visitItems.get(index).getHcmActTakAtFacility());
        aq.id(R.id.txtfacilitynameval).text(visitItems.get(index).getHcmActTakFacilityName());
        aq.id(R.id.txtActionDateval).text(visitItems.get(index).getHcmActTakDate() + " - " + visitItems.get(index).getHcmActTakTime());
        aq.id(R.id.txtactiontakenval).text(visitItems.get(index).getHcmActTakForCompl());
        aq.id(R.id.txtactiontobetakenval).text(visitItems.get(index).getHcmActionToBeTakenClient());
       // aq.id(R.id.txtstatusval).text(visitItems.get(index).getHcmActTakStatus());
        aq.id(R.id.txtstatusdateval).text(visitItems.get(index).getHcmActTakStatusDate() + " - " + visitItems.get(index).getHcmActTakStatusTime());

        if(visitItems.get(index).getHcmActTakStatus().equalsIgnoreCase(getResources().getString(R.string.refersave))) { //24Dec2019 - Bindu - change string
            aq.id(R.id.txtstatusval).text(getResources().getString(R.string.referredtohigherfacility)); //23Dec2019 - Bindu - if refer then change lbl
            aq.id(R.id.trreffactype).visible();
            aq.id(R.id.trreffacname).visible();
            aq.id(R.id.txtreffacilitytypeval).text(""+visitItems.get(index).getHcmActTakReferredToFacility());
            aq.id(R.id.txtreffacilitynameval).text(""+visitItems.get(index).getHcmActTakReferredToFacilityName()); //05Jan2020 - Bindu - type and name correction
        }else {
            //aq.id(R.id.tblrefdetails).gone();
            aq.id(R.id.trreffactype).gone();  //19Dec2019 - bindu - change the tr instead of tbl
            aq.id(R.id.trreffacname).gone();
            aq.id(R.id.txtstatusval).text(visitItems.get(index).getHcmActTakStatus()); //23Dec2019 - bindu
            aq.id(R.id.txtreffacilitytypeval).text(""); //05Jan2020 - Bindu - set the fac type and name to blank
            aq.id(R.id.txtreffacilitynameval).text("");
        }

        aq.id(R.id.etvisitcomments).text(""+visitItems.get(index).getHcmActTakComments());
        if(visitItems.get(index).getHcmCommentsClient()!= null && visitItems.get(index).getHcmCommentsClient().length() > 0)
            aq.id(R.id.etcomments).text(""+visitItems.get(index).getHcmCommentsClient());


        aq.id(R.id.btnsavecomplmgmt).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    try {
                       if(aq.id(R.id.etcomments).getText().toString().trim().length() >0 ) {
                           updatedata(visitItems, index);
                       } else {
                           Toast.makeText(ComplMgmtVisitViewActivity.this, getResources().getString(R.string.plsentercomments), Toast.LENGTH_LONG).show();

                       }
                    }catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                    }
                }
        });

        expandcollapseSymptoms();

        updateUserSeenDate(visitItems,index);

        //23Dec2019 - Bindu
        aq.id(R.id.etcomments).enabled(false).background(R.drawable.edittext_disable);
        if(visitItems.get(index).getVisitType() != null && visitItems.get(index).getVisitType().equalsIgnoreCase("PNC")){
            aq.id(R.id.txtDangSignsL).text(getResources().getString(R.string.motherdangersigns));
        }else
            aq.id(R.id.txtDangSignsL).text(getResources().getString(R.string.symptoms));

        if(visitItems.get(index).getVisitChildDangerSigns() != null && visitItems.get(index).getVisitChildDangerSigns().trim().length() > 0) {
            aq.id(R.id.llnewborndangersigns).visible();
            aq.id(R.id.txtChilddangersigns).text(visitItems.get(index).getVisitChildDangerSigns());
        }else {
            aq.id(R.id.llnewborndangersigns).gone();
        }

        expandcollapseChilddangerSigns();

    }

    //23Dec2019 - Get Comments data
    private void GetVisitComments(Integer autoId) throws Exception{
        ComplicationMgmtRepository compRepo = new ComplicationMgmtRepository(databaseHelper);
        String cmts = compRepo.getVisitASHAComments(autoId);
        if(cmts != null) {
            aq.id(R.id.etcomments).text(cmts);
            aq.id(R.id.etcomments).enabled(false).background(R.drawable.edittext_disable);
        }
    }

    // Update the User seen date
    private void updateUserSeenDate(List<tblHRPComplicationMgmt> visitItems, int index) {
        if(visitItems.get(index).getHcmClientSeenDate() == null || visitItems.get(index).getHcmClientSeenDate() == "") {
            TransactionHeaderRepository transRepo = new TransactionHeaderRepository(databaseHelper);
            final int transId = transRepo.iCreateNewTrans(woman.getUserId(), databaseHelper);

            ComplicationMgmtRepository compRepo = new ComplicationMgmtRepository(databaseHelper);
            compRepo.updateUserSeenDate(woman.getWomanId(), visitItems.get(index).getAutoId(), woman.getUserId(), transId);
        }

    }

    // Update the data
    private void updatedata(final List<tblHRPComplicationMgmt> visitItems, final int index) throws Exception {

        try {
            TransactionHeaderRepository transRepo = new TransactionHeaderRepository(databaseHelper);
            final int transId = transRepo.iCreateNewTrans(woman.getUserId(), databaseHelper);

            final tblHRPComplicationMgmt regcompl = new tblHRPComplicationMgmt();
            regcompl.setHcmCommentsClient(aq.id(R.id.etcomments).getText().toString());
            regcompl.setHcmActionDateTimeClient(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
            regcompl.setHcmClientReceivedDate(visitItems.get(index).getHcmClientReceivedDate());
            regcompl.setUserType(appState.userType);
            regcompl.setTranid(transId);
            regcompl.setAutoId(visitItems.get(index).getAutoId());
            regcompl.setWomanId(appState.selectedWomanId);
            regcompl.setVisitNum(visitItems.get(index).getVisitNum());
            /*if(visitItems.get(index).getHcmClientSeenDate() == null || visitItems.get(index).getHcmClientSeenDate() == "")
                regcompl.setHcmClientSeenDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());*/

            TransactionManager.callInTransaction(databaseHelper.getConnectionSource(), new Callable<Void>() {
                public Void call() throws Exception {
                    complrepo.updateClientAction(regcompl, databaseHelper, transId);
                    Toast.makeText(ComplMgmtVisitViewActivity.this, getResources().getString(R.string.updatedsuccessfully), Toast.LENGTH_LONG).show();
                    //setData(visitItems,index);
                    GetVisitComments(visitItems.get(index).getAutoId());
                    return null;
                }
            });
        }catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }


    }

    // Expand or Collapse Symtpoms drawable
    private void expandcollapseSymptoms() {
        if(aq.id(R.id.txtDangSigns).getText().toString().length() > 0) {
            aq.id(R.id.txtDangSignsL).getTextView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_colsym, 0, 0, 0);
        }else{
            aq.id(R.id.txtDangSignsL).getTextView().setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
    }

    // Expand or Collapse Advise drawable
    private void expandcollapseChilddangerSigns() {
        if(aq.id(R.id.txtChilddangersigns).getText().toString().length() > 0) {
            aq.id(R.id.txtChilddangersignsL).getTextView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_colsym, 0, 0, 0);
        }else{
            aq.id(R.id.txtChilddangersignsL).getTextView().setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
    }

    // Expand and Collapse Danger Signs
    public void opencloseAccordionDangerSigns(View v, TextView txt) {
        try {

            if (v.getVisibility() == View.VISIBLE) {
                v.setVisibility(View.GONE);
                txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_expsym, 0, 0, 0);
            }
            else {
                v.setVisibility(View.VISIBLE);
                txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_colsym, 0, 0, 0);
            }
        } catch (Exception e) {

        }
    }

    /**
     * Initiates the Navigation drawer
     */
    private void initiateDrawer() {
        try {
            mDrawerLayout = findViewById(R.id.ett_drawer_layout);
            mDrawerList = findViewById(R.id.ett_left_drawer);


            navDrawerItems = new ArrayList<NavDrawerItem>();


            // adding nav drawer items to array
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.sp_WRegEdit),
                    R.drawable.registration));

            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.services),
                    R.drawable.anm_pending_activities));


            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.unplannedserviceslist),
                    R.drawable.anm_pending_activities));

            //14Sep2019 - Bindu - Add Homevisit
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.homevisitadd),
                    R.drawable.ic_homevisit));

            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.womanhomevisithistory),
                    R.drawable.prenatalhomevisit));

            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deliveryHeading), R.drawable.delivery_info));//01Oct2019 Arpitha

            // set a custom shadow that overlays the main content when the drawer
            // opens


            mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

           mDrawerList.setAdapter(new NavDrawerListAdapter(getApplicationContext(), navDrawerItems));
            mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

            // ActionBarDrawerToggle ties together the the proper interactions
            // between the sliding drawer and the action bar app icon
            mDrawerToggle = new ActionBarDrawerToggle(this, /* host Activity */
                    mDrawerLayout, /* DrawerLayout object */
                    R.drawable.ic_drawer, /*
             * nav drawer image to replace 'Up'
             * caret
             */
                    R.string.drawer_open, /*
             * "open drawer" description for
             * accessibility
             */
                    R.string.drawer_close /*
             * "close drawer" description for
             * accessibility
             */
            ) {
                public void onDrawerClosed(View view) {
                    try {
                        displayDrawerItems();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                public void onDrawerOpened(View drawerView) {
                    try {
                        displayDrawerItems();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            mDrawerLayout.setDrawerListener(mDrawerToggle);
        } catch (Exception e) {

        }
    }

    // 18Sep2019 - Bindu Display drawer open close items
    private void displayDrawerItems() throws Exception{

        invalidateOptionsMenu(); // creates call to
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer1);
        getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);


        aq.id(R.id.ivWomanImg).getImageView().setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                mDrawerLayout.closeDrawer(mDrawerList);
            }
        });

        aq.id(R.id.tvWomanName).text((woman.getRegWomanName() + "\t"));
        if (woman.getRegADDate() != null && woman.getRegADDate().length() > 0) {
            aq.id(R.id.tvInfo1).text(woman.getRegADDate());
        }else {
            int noOfWeeks = DateTimeUtil.getNumberOfWeeksFromLMP(woman.getRegLMP());
            aq.id(R.id.tvInfo1).text(getResources().getString(R.string.tvlmp) + woman.getRegLMP() + "  , GA : " + noOfWeeks + "W");
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        try {
            switch (item.getItemId()) {
                case android.R.id.home:
                    Intent home = new Intent(ComplMgmtVisitViewActivity.this, MainMenuActivity.class);
                    home.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                    /*startActivity(home);*/
                    displayAlert(getResources().getString(R.string.exit), home);
                    break;

                case R.id.wlist:
                    Intent wlist = new Intent(ComplMgmtVisitViewActivity.this, RegisteredWomenActionTabs.class);
                    wlist.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                    /*startActivity(wlist);*/
                    displayAlert(getResources().getString(R.string.exit), wlist);
                    break;

                case R.id.logout:
                    Intent logout = new Intent(ComplMgmtVisitViewActivity.this, LoginActivity.class);
                    displayAlert(getResources().getString(R.string.m111), logout);
                    /*startActivity(logout);
                    CommonClass.updateLogoutTime();*/
                    break;

                case R.id.home:
                    Intent intent = new Intent(ComplMgmtVisitViewActivity.this, MainMenuActivity.class);
                    intent.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                    /*startActivity(intent);*/
                    displayAlert(getResources().getString(R.string.exit), intent);
                    break;
                //30Sep2019 - Bindu
                case R.id.about: {
                    Intent goToScreen = new Intent(ComplMgmtVisitViewActivity.this, AboutActivity.class);
                    goToScreen.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                    displayAlert(getResources().getString(R.string.exit), goToScreen);
                    return true;
                }
            }
        } catch (Exception e) {

        }
        return super.onOptionsItemSelected(item);
    }

    private void displayAlert(final String spanText2, final Intent goToScreen) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set dialog message
        alertDialogBuilder.setMessage(spanText2).setCancelable(false).setNegativeButton(
                (getResources().getString(R.string.m121)), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (goToScreen != null) {
                            try {
                                startActivity(goToScreen);
                                if(spanText2.contains("logout"))
                                {
                                    try {
                                        LoginRepository loginRepo = new LoginRepository(databaseHelper);
                                        loginRepo.updateLogoutTime(appState.loginAuditId, appState.sessionUserId);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            dialog.cancel();
                        }
                    }
                }).setPositiveButton((getResources().getString(R.string.m122)),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    /**
     * Initializes the Action Bar items
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        try {
            //16Sep2019 - Bindu
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer1);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);

            aq.id(R.id.tvWomanName).text(woman.getRegWomanName());
            if (woman.getRegADDate() != null && woman.getRegADDate().length() > 0) {
                aq.id(R.id.tvInfo1).text(woman.getRegADDate());
            }else {
                int noOfWeeks = DateTimeUtil.getNumberOfWeeksFromLMP(woman.getRegLMP());
                aq.id(R.id.tvInfo1).text(getResources().getString(R.string.tvlmp) + woman.getRegLMP() + "  , GA : " + noOfWeeks + "W");
            }

            aq.id(R.id.ivWomanImg).gone();

            getMenuInflater().inflate(R.menu.servicesmenu, menu);

            menu.findItem(R.id.logout).setTitle((getResources().getString(R.string.action_logout)));


        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    /**
     * The click listner for ListView in the navigation drawer
     */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            mDrawerLayout.closeDrawer(mDrawerList);
            //   if (woman.getRegADDate() == null || woman.getRegADDate().length() == 0) {
            try {
                if (position == 0) {
                    Intent viewprof = new Intent(ComplMgmtVisitViewActivity.this, ViewProfileActivity.class);
                    viewprof.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                    startActivity(viewprof);
                } else if (position == 1) {
                    Intent unplanned = new Intent(ComplMgmtVisitViewActivity.this, ServicesSummaryActivity.class);
                    unplanned.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                    startActivity(unplanned);
                } else if (position == 2) {
                    Intent unplanned = new Intent(ComplMgmtVisitViewActivity.this, UnplannedServicesListActivity.class);
                    unplanned.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                    startActivity(unplanned);
                }//14Sep2019 - Bindu - Add Home visit
                else if (position == 3) {
                    Intent homevisit = new Intent(ComplMgmtVisitViewActivity.this, HomeVisit.class);
                    homevisit.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                    startActivity(homevisit);
                }else if (position == 4) {
                    //if(woman.getLastAncVisitDate().length() > 0 || woman.getLastPncVisitDate().length() > 0) {
                  //  if(wVisitDetails != null && wVisitDetails.get(0).getLastAncVisitDate().length() > 0 || wVisitDetails.get(0).getLastPncVisitDate().length() > 0 )  {
                        Intent homevisit = new Intent(ComplMgmtVisitViewActivity.this, HomeVisitListActivity.class);
                        homevisit.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                        startActivity(homevisit);
                   /* }else {
                        Toast.makeText(ComplMgmtVisitViewActivity.this, getResources().getString(R.string.norecords), Toast.LENGTH_SHORT).show();
                    }*/
                }//01Oct2019 Arpitha
                else if(position ==5 )
                    if(woman.getRegPregnantorMother()==2 || DateTimeUtil.getNumberOfDaysFromLMP(woman.getRegLMP())>210) {
                        Intent del = new Intent(ComplMgmtVisitViewActivity.this, DeliveryInfoActivity.class);
                        del.putExtra("woman", woman);
                        del.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        startActivity(del);
                    }else
                        Toast.makeText(getApplicationContext(),getResources().getString(R.string.m132),Toast.LENGTH_LONG).show();

            } catch (Exception e) {

            }

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        /*
         * You'll need this in your class to release the helper when done.
         */
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }
}
