package com.sc.stmansi.common;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;


import com.sc.stmansi.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/** Creating default datepicker fragment,
 * and set the selected date to the field */
@SuppressLint("SimpleDateFormat")
public class CommonDatePickerFragmentSupport extends DialogFragment {
	
	public static Activity activity;
	public static Fragment frag;
	public static String defaultDate = "";

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Use the current date as the default date in the picker
		int year = 0, month = 0, day = 0;
		try{
			
			Calendar c = Calendar.getInstance();
			if(defaultDate != null && defaultDate.length() > 0){
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
				Date sendDate = sdf.parse(defaultDate);
				c.setTime(sendDate);
				year = c.get(Calendar.YEAR);
				month = c.get(Calendar.MONTH);
				day = c.get(Calendar.DAY_OF_MONTH);
			}else{
				c = Calendar.getInstance();
				c.setTime(new Date());
				year = c.get(Calendar.YEAR);
				month = c.get(Calendar.MONTH);
				day = c.get(Calendar.DAY_OF_MONTH);
			}

		}catch(Exception e){
			e.printStackTrace();
		}

		// Create a new instance of DatePickerDialog and return it
		DatePickerDialog dpDialog = null;
		if(frag != null) {
			dpDialog = new DatePickerDialog(activity, (OnDateSetListener) frag, year, month, day);
			dpDialog.setButton(DialogInterface.BUTTON_NEGATIVE,
                    getString(R.string.cancel),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,
                                int which) {
                            if (which == DialogInterface.BUTTON_NEGATIVE) {
                                dialog.cancel();
                            }
                        }
                    });
		}else {
			dpDialog = new DatePickerDialog(activity, (OnDateSetListener) activity, year, month, day);
			dpDialog.setButton(DialogInterface.BUTTON_NEGATIVE,
                    getString(R.string.clear),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,
                                int which) {
                            if (which == DialogInterface.BUTTON_NEGATIVE) {
                                dialog.cancel();
                            }
                        }
                    });
		}
		dpDialog.getDatePicker().setCalendarViewShown(false);
		return dpDialog;
	}
	
	
	public static void getCommonDate(Activity act, Fragment fragment, String dDate){
		activity = act;
		frag = fragment;
		defaultDate = dDate;
		DialogFragment newFragment = new CommonDatePickerFragmentSupport();
		newFragment.show(act.getFragmentManager(), "datePicker");
	}

	public static void getCommonDate(Activity act, String dDate){
		activity = act;
		defaultDate = dDate;
		DialogFragment newFragment = new CommonDatePickerFragmentSupport();
		newFragment.show(act.getFragmentManager(), "datePicker");
	}



}
