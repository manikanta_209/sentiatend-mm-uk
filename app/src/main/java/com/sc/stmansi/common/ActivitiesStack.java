//27Aug2019 Arpitha - code clean up
package com.sc.stmansi.common;

import android.app.Activity;
import android.os.Handler;

public class ActivitiesStack {

	private Activity activity;
	
	private Handler _idleHandler = new Handler();
	private Runnable _idleRunnable = new Runnable() {
	    @Override
	    public void run() {
	        if(activity != null) {
	        	if(activity.isFinishing()) return;
				activity.finishAffinity();
			}

	    }
	};

	public ActivitiesStack(Activity activity) {
		this.activity = activity;
	}

//	to remove callbacks
	public void delayedIdle(int delayMinutes) {
	    _idleHandler.removeCallbacks(_idleRunnable);
	    _idleHandler.postDelayed(_idleRunnable, (delayMinutes * 1000 * 60));
	}

}
