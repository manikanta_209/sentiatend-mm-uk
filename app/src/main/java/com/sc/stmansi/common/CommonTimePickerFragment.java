package com.sc.stmansi.common;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.os.Bundle;

/** Creating default datepicker fragment, 
 * and set the selected date to the field */
@SuppressLint("SimpleDateFormat")
public class CommonTimePickerFragment extends DialogFragment {
	
	public static Activity activity;
	public static int mHour;
	public static int mMinute;

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		// Create a new instance of DatePickerDialog and return it

		OnTimeSetListener timeSetter = (OnTimeSetListener) activity;
		return new TimePickerDialog(activity, timeSetter, mHour, mMinute, false);
//		return new TimePickerDialog(activity, R.style.DialogTheme, this, mHour, mMinute,
//				DateFormat.is24HourFormat(activity));
	}
	
	
	public static void getCommonTime(Activity act, int hour, int min){
		activity = act;
		mHour = hour;
		mMinute = min;
		DialogFragment newFragment = new CommonTimePickerFragment();
		newFragment.show(act.getFragmentManager(), "time_Picker");
	}

}
