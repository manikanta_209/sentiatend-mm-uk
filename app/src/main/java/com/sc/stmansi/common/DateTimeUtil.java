package com.sc.stmansi.common;

import android.annotation.SuppressLint;

import com.google.firebase.crashlytics.FirebaseCrashlytics;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateTimeUtil {


    public static String startoftheweek, endoftheweek;

    public static String getTodaysDate() {
        String returnDateStr = "";
        try {

            Date d = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            returnDateStr = sdf.format(d);

        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        return returnDateStr;
    }

    // get number of weeks from LMP
    public static int getNumberOfWeeksFromLMP(String lmp) throws ParseException {
        Date lmpDt = new SimpleDateFormat("dd-MM-yyyy").parse(lmp);
        Date todayDt = new SimpleDateFormat("dd-MM-yyyy").parse(getTodaysDate());

        Calendar cLmp = Calendar.getInstance();
        cLmp.setTime(lmpDt);
        Calendar cTDate = Calendar.getInstance();
        cTDate.setTime(todayDt);

        long diff = cTDate.getTimeInMillis() - cLmp.getTimeInMillis();

        float dayCount = (float) diff / (24 * 60 * 60 * 1000);

        return (int) (dayCount / 7);
    }

    // get number of days from LMP
    public static int getNumberOfDaysFromLMP(String lmp) throws ParseException {
        Date lmpDt = new SimpleDateFormat("dd-MM-yyyy").parse(lmp);
        Date todayDt = new SimpleDateFormat("dd-MM-yyyy").parse(getTodaysDate());

        Calendar cLmp = Calendar.getInstance();
        cLmp.setTime(lmpDt);
        Calendar cTDate = Calendar.getInstance();
        cTDate.setTime(todayDt);

        long diff = cTDate.getTimeInMillis() - cLmp.getTimeInMillis();

        float dayCount = (float) diff / (24 * 60 * 60 * 1000);

        return (int) dayCount;
    }

    @SuppressLint("SimpleDateFormat")
    public static int getDaysBetweenDates(String regdate, String lmpdate) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Date getregdate = null, getlmpdate = null;
        if (regdate.length() != 0 && lmpdate.length() != 0) {
            getregdate = sdf.parse(regdate);
            getlmpdate = sdf.parse(lmpdate);
            return (int) ((getregdate.getTime() - getlmpdate.getTime()) / (1000 * 60 * 60 * 24));
        }
        return 0;
    }

    // get financial year based on current date
    public static String getFinYear() throws Exception {
        Date currDate = new Date();
        int mon = Integer.parseInt(new SimpleDateFormat("M").format(currDate));
        int year = Integer.parseInt(new SimpleDateFormat("yyyy").format(currDate));
        String fyear = null;
        if (mon >= 4) {
            fyear = year + "-" + (year + 1);
        } else {
            fyear = (year - 1) + "-" + year;
        }
        return fyear;
    }

    // Get Current Time
    public static String getCurrentTime() throws Exception {
        String currentTime;
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
        currentTime = df.format(c.getTime());
        return currentTime;
    }

    //22Jul2019 - Bindu
    // Get Date From String
    public static String getDateYYYYMMDD(String date, String def_dateFormat) throws Exception {
        String dateValue = null;
        Date d = new SimpleDateFormat("dd-MM-yyyy").parse(date);
        dateValue = new SimpleDateFormat(def_dateFormat).format(d);
        return dateValue;
    }

    public static Date parse(String date) throws ParseException {
        return new SimpleDateFormat("dd-MM-yyyy").parse(date);
    }

    // 11Sep2019 - Bindu - Cal Gest age between lmp and date selected
// get number of days from LMP
    public static int getNumberOfDaysFromLMPVisitDate(String lmp, String visitdate) throws ParseException {
        Date lmpDt = new SimpleDateFormat("dd-MM-yyyy").parse(lmp);
        Date visitDt = new SimpleDateFormat("dd-MM-yyyy").parse(visitdate);

        Calendar cLmp = Calendar.getInstance();
        cLmp.setTime(lmpDt);
        Calendar cTDate = Calendar.getInstance();
        cTDate.setTime(visitDt);

        long diff = cTDate.getTimeInMillis() - cLmp.getTimeInMillis();

        float dayCount = (float) diff / (24 * 60 * 60 * 1000);

        return (int) dayCount;

    }

//    20Nov2019 Arpitha
    /**
     * Get the start and End date of the current week
     *
     * @throws Exception
     */
    public static void getCurrentWeek() throws Exception {
        Calendar calendarFirstDay = Calendar.getInstance();
        Calendar calendarLastDay = Calendar.getInstance();
        Date now = new Date();

        calendarFirstDay.setTime(now);
        calendarLastDay.setTime(now);

        startoftheweek = firstDayOfWeek(calendarFirstDay);
        endoftheweek = lastDayOfWeek(calendarLastDay);

    }

    /**
     * Get first day of the week
     *
     * @param calendar
     * @return
     * @throws Exception
     */
    public static String firstDayOfWeek(Calendar calendar) throws Exception {
        Calendar cal = (Calendar) calendar.clone();
        int day = cal.get(Calendar.DAY_OF_YEAR);
        while (cal.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
            cal.set(Calendar.DAY_OF_YEAR, --day);
        }
        int year1 = cal.get(Calendar.YEAR);
        int month1 = cal.get(Calendar.MONTH) + 1;
        int day1 = cal.get(Calendar.DATE);

        startoftheweek = String.valueOf(year1) + "-" + String.format("%02d", month1) + "-"
                + String.format("%02d", day1);
        return startoftheweek;
    }

    // Last day of the week

    /**
     * @param calendar
     * @return
     * @throws Exception
     */
    public static String lastDayOfWeek(Calendar calendar) throws Exception {
        Calendar cal = (Calendar) calendar.clone();
        int day = cal.get(Calendar.DAY_OF_YEAR);
        while (cal.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
            cal.set(Calendar.DAY_OF_YEAR, ++day);
        }

        int year7 = cal.get(Calendar.YEAR);
        int month7 = cal.get(Calendar.MONTH) + 1;
        int day7 = cal.get(Calendar.DATE);

        endoftheweek = String.valueOf(year7) + "-" + String.format("%02d", month7) + "-" + String.format("%02d", day7);
        return endoftheweek;
    }

    public static String getCureentMonthName() {
        String month_name;
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat today = new SimpleDateFormat("MMM");
        month_name = today.format(cal.getTime());
        return month_name;
    }


    public static String getPreviousDate(String pattern, int days) {
        String prevDate = null;
        Calendar cal = GregorianCalendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DAY_OF_YEAR, -days);
        Date BeforeDate = cal.getTime();
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        prevDate = format.format(BeforeDate);
        return prevDate;
    }



    /**
     * Get Current date in the format dd-MM-yyyy
     *
     * @param format
     * @return
     */
    public static String getTodaysDate(String format) {
        String returnDateStr;
        Date d = new Date();
        /* SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy"); */
        SimpleDateFormat sdf = new SimpleDateFormat(format);// 25Jan2017 Arpitha
        returnDateStr = sdf.format(d);
        return returnDateStr;
    }

    public static String addDay(String oldDate, int numberOfDays) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(dateFormat.parse(oldDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.add(Calendar.DAY_OF_YEAR,(-numberOfDays));
        dateFormat=new SimpleDateFormat("dd-MM-yyyy");
        Date newDate=new Date(c.getTimeInMillis());
        String resultDate=dateFormat.format(newDate);
        return resultDate;
    }



    public static String calculateAge(String strDate) {

        int years = 0;
        int months = 0;
        int days = 0;
        String strYears ="", strMonth ="", strDay ="";


        try {
          //  long timeInMillis = Long.parseLong(strDate);
       //     Date birthDate = new Date(timeInMillis);

            Date birthDate = new SimpleDateFormat("dd-MM-yyyy").parse(strDate);


            //create calendar object for birth day
            Calendar birthDay = Calendar.getInstance();
            birthDay.setTimeInMillis(birthDate.getTime());
            //create calendar object for current day
            long currentTime = System.currentTimeMillis();
            Calendar now = Calendar.getInstance();
            now.setTimeInMillis(currentTime);
            //Get difference between years
            years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
            int currMonth = now.get(Calendar.MONTH) + 1;
            int birthMonth = birthDay.get(Calendar.MONTH) + 1;

            //Get difference between months
            months = currMonth - birthMonth;


            //if month difference is in negative then reduce years by one and calculate the number of months.
            if (months < 0) {
                years--;
                months = 12 - birthMonth + currMonth;
                if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
                    months--;
            } else if (months == 0 && now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
                years--;
                months = 11;
            }
            //Calculate the days
            if (now.get(Calendar.DATE) > birthDay.get(Calendar.DATE))
                days = now.get(Calendar.DATE) - birthDay.get(Calendar.DATE);
            else if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
                int today = now.get(Calendar.DAY_OF_MONTH);
                now.add(Calendar.MONTH, -1);
                days = now.getActualMaximum(Calendar.DAY_OF_MONTH) - birthDay.get(Calendar.DAY_OF_MONTH) + today;
            } else {
                days = 0;
                if (months == 12) {
                    years++;
                    months = 0;
                }
            }
            //adarsh
            if (currMonth > birthMonth) {
                if (birthDay.get(Calendar.DATE) > now.get(Calendar.DATE)) {
                    months = months - 1;
                }
            }//---------------------------------

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Create new Age object


        if(years>0)
            strYears = years + " Y ";

        if(months>0)
            strMonth = months + " M ";

        if(days>0)
            strDay = days + " D ";



        return strYears+strMonth+strDay;
    }

    public static String getTimeIn12HourFormat(String timein24)
    {
      //  final String time = "23:15";

        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
            final Date dateObj = sdf.parse(timein24);

             String time =  new SimpleDateFormat("K:mm a").format(dateObj);
             return time;
        } catch (final ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String EvalEndDateCalculator(){
        String EvalStartDate = getTodaysDate();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(sdf.parse(EvalStartDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.add(Calendar.DAY_OF_MONTH, 10);
        String EvalEndDate = sdf.format(c.getTime());
        return EvalEndDate;
    }

    //mani 20 march 2021
    public static int calculateAgeInMonths(String strDate) {

        int years = 0;
        int months = 0;
        int days = 0;
        String strYears ="";
        String strMonth ="";
        String strDay ="";
        int ageInMonths=0;


        try {
            //  long timeInMillis = Long.parseLong(strDate);
            //     Date birthDate = new Date(timeInMillis);

            Date birthDate = new SimpleDateFormat("dd-MM-yyyy").parse(strDate);


            //create calendar object for birth day
            Calendar birthDay = Calendar.getInstance();
            birthDay.setTimeInMillis(birthDate.getTime());
            //create calendar object for current day
            long currentTime = System.currentTimeMillis();
            Calendar now = Calendar.getInstance();
            now.setTimeInMillis(currentTime);
            //Get difference between years
            years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
            int currMonth = now.get(Calendar.MONTH) + 1;
            int birthMonth = birthDay.get(Calendar.MONTH) + 1;

            //Get difference between months
            months = currMonth - birthMonth;


            //if month difference is in negative then reduce years by one and calculate the number of months.
            if (months < 0) {
                years--;
                months = 12 - birthMonth + currMonth;
                if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
                    months--;
            } else if (months == 0 && now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
                years--;
                months = 11;
            }
            //Calculate the days
            if (now.get(Calendar.DATE) > birthDay.get(Calendar.DATE))
                days = now.get(Calendar.DATE) - birthDay.get(Calendar.DATE);
            else if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
                int today = now.get(Calendar.DAY_OF_MONTH);
                now.add(Calendar.MONTH, -1);
                days = now.getActualMaximum(Calendar.DAY_OF_MONTH) - birthDay.get(Calendar.DAY_OF_MONTH) + today;
            } else {
                days = 0;
                if (months == 12) {
                    years++;
                    months = 0;
                }
            }
            //adarsh
            if (currMonth > birthMonth) {
                if (birthDay.get(Calendar.DATE) > now.get(Calendar.DATE)) {
                    months = months - 1;
                }
            }//---------------------------------

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Create new Age object

        if (years>0){
            ageInMonths=months+(years*12);
        }else if (months>0){
            ageInMonths=months;
        }else {
            ageInMonths=0;
        }




        return ageInMonths;
    }
    public static int countWorkingDays(String strdateStart, String strdateStop) throws ParseException{
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Date dateStart = null, dateStop = null;
        if (strdateStart.length() != 0 && strdateStop.length() != 0) {
            dateStart = sdf.parse(strdateStart);
            dateStop = sdf.parse(strdateStop);

            Calendar startCal = Calendar.getInstance();
            startCal.setTime(dateStart);

            Calendar endCal = Calendar.getInstance();
            endCal.setTime(dateStop);

            int workDays = 0;

            //Return 0 if start and end are the same
            if (startCal.getTimeInMillis() == endCal.getTimeInMillis()) {
                return 0;
            }

            if (startCal.getTimeInMillis() > endCal.getTimeInMillis()) {
                startCal.setTime(dateStop);
                endCal.setTime(dateStart);
            }

            do {
                //excluding start date
                startCal.add(Calendar.DAY_OF_MONTH, 1);
                if (startCal.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && startCal.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
                    ++workDays;
                }
            } while (startCal.getTimeInMillis() < endCal.getTimeInMillis()); //excluding end date

            return workDays;
        }
        return 0;
    }

    public static String calculateAgeInYears(String strDate) {
        int years = 0;
        int months = 0;
        int days = 0;
        String strYears ="";
        String strMonth ="";
        String strDay ="";
        int ageInMonths=0;


        try {
            //  long timeInMillis = Long.parseLong(strDate);
            //     Date birthDate = new Date(timeInMillis);

            Date birthDate = new SimpleDateFormat("dd-MM-yyyy").parse(strDate);


            //create calendar object for birth day
            Calendar birthDay = Calendar.getInstance();
            birthDay.setTimeInMillis(birthDate.getTime());
            //create calendar object for current day
            long currentTime = System.currentTimeMillis();
            Calendar now = Calendar.getInstance();
            now.setTimeInMillis(currentTime);
            //Get difference between years
            years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
            int currMonth = now.get(Calendar.MONTH) + 1;
            int birthMonth = birthDay.get(Calendar.MONTH) + 1;

            //Get difference between months
            months = currMonth - birthMonth;


            //if month difference is in negative then reduce years by one and calculate the number of months.
            if (months < 0) {
                years--;
                months = 12 - birthMonth + currMonth;
                if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
                    months--;
            } else if (months == 0 && now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
                years--;
                months = 11;
            }
            //Calculate the days
            if (now.get(Calendar.DATE) > birthDay.get(Calendar.DATE))
                days = now.get(Calendar.DATE) - birthDay.get(Calendar.DATE);
            else if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
                int today = now.get(Calendar.DAY_OF_MONTH);
                now.add(Calendar.MONTH, -1);
                days = now.getActualMaximum(Calendar.DAY_OF_MONTH) - birthDay.get(Calendar.DAY_OF_MONTH) + today;
            } else {
                days = 0;
                if (months == 12) {
                    years++;
                    months = 0;
                }
            }
            //adarsh
            if (currMonth > birthMonth) {
                if (birthDay.get(Calendar.DATE) > now.get(Calendar.DATE)) {
                    months = months - 1;
                }
            }//---------------------------------

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Create new Age object

        if (years>0){
            ageInMonths=months+(years*12);
        }else if (months>0){
            ageInMonths=months;
        }else {
            ageInMonths=0;
        }




        return String.valueOf(years);
    }


    //    21Jul2021 Bindu - Cal age between 2 dates
    public static String calculateAgeBetweenDates(String strDate, String visitDate) {

        int years = 0;
        int months = 0;
        int days = 0;
        String strYears ="", strMonth ="", strDay ="";
        try {
            //  long timeInMillis = Long.parseLong(strDate);
            //     Date birthDate = new Date(timeInMillis);

            Date birthDate = new SimpleDateFormat("dd-MM-yyyy").parse(strDate);

            Date VisitDate = new SimpleDateFormat("dd-MM-yyyy").parse(visitDate);

            //create calendar object for birth day
            Calendar birthDay = Calendar.getInstance();
            birthDay.setTimeInMillis(birthDate.getTime());
            //create calendar object for current day
            //   long currentTime = System.currentTimeMillis();
            Calendar now = Calendar.getInstance();
            now.setTimeInMillis(VisitDate.getTime());
            //Get difference between years
            years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
            int currMonth = now.get(Calendar.MONTH) + 1;
            int birthMonth = birthDay.get(Calendar.MONTH) + 1;

            //Get difference between months
            months = currMonth - birthMonth;

            //if month difference is in negative then reduce years by one and calculate the number of months.
            if (months < 0) {
                years--;
                months = 12 - birthMonth + currMonth;
                if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
                    months--;
            } else if (months == 0 && now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
                years--;
                months = 11;
            }
            //Calculate the days
            if (now.get(Calendar.DATE) > birthDay.get(Calendar.DATE))
                days = now.get(Calendar.DATE) - birthDay.get(Calendar.DATE);
            else if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
                int today = now.get(Calendar.DAY_OF_MONTH);
                now.add(Calendar.MONTH, -1);
                days = now.getActualMaximum(Calendar.DAY_OF_MONTH) - birthDay.get(Calendar.DAY_OF_MONTH) + today;
            } else {
                days = 0;
                if (months == 12) {
                    years++;
                    months = 0;
                }
            }
            //adarsh
            if (currMonth > birthMonth) {
                if (birthDay.get(Calendar.DATE) > now.get(Calendar.DATE)) {
                    months = months - 1;
                }
            }//---------------------------------

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Create new Age object


        if(years>0)
            strYears = years + " Y ";

        if(months>0)
            strMonth = months + " M ";

        //if(days>0)
        strDay = days + " D ";

        return strYears+strMonth+strDay;
    }
}
