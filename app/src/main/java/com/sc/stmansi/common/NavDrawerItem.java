//27Aug2019 Arpitha - code clean up
package com.sc.stmansi.common;

public class NavDrawerItem {
	 
    private String title;
    private int icon;

//    cnstructor
    public NavDrawerItem(String title, int icon){
        this.title = title;
        this.icon = icon;
    }

//   get title
    public String getTitle(){
        return this.title;
    }

//    geticon
    public int getIcon(){
        return this.icon;
    }
     

}
