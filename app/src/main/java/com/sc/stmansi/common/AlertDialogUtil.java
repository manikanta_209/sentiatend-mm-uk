//27Aug2019 Arpitha - code clean up
package com.sc.stmansi.common;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

import com.sc.stmansi.R;

public class AlertDialogUtil {

    public static void displayAlertMessage(String message, Activity act) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(act);
        // set dialog message
        alertDialogBuilder.setMessage(message).setCancelable(false).setNegativeButton(
                act.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
