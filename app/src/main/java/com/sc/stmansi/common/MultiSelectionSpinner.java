package com.sc.stmansi.common;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import com.sc.stmansi.HomeVisit.HomeVisit;
import com.sc.stmansi.HomeVisit.PncHomeVisit;
import com.sc.stmansi.R;
import com.sc.stmansi.adolescent.AdolStartTraining;
import com.sc.stmansi.adolescent.AdolVisit_New;
import com.sc.stmansi.childhomevisit.ChildHomeVisit;
import com.sc.stmansi.childregistration.AddSiblingsActivity;
import com.sc.stmansi.childregistration.ChildRegistrationActivity;
import com.sc.stmansi.childregistration.EditChildActivity;
import com.sc.stmansi.childregistration.EditRegChildInfoActivity;
import com.sc.stmansi.delivery.DeliveryInfoActivity;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Aneh Thakur on 5/7/2015.
 */
public class MultiSelectionSpinner extends Spinner implements
        OnMultiChoiceClickListener {
    String[] _items = null;
    boolean[] mSelection = null;

    ArrayAdapter<String> simple_adapter;

    public MultiSelectionSpinner(Context context) {
        super(context);

        simple_adapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_spinner_item);
        super.setAdapter(simple_adapter);
    }

    public MultiSelectionSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);

        simple_adapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_spinner_item);
        super.setAdapter(simple_adapter);
    }

    //    13May2021 Arpitha - added new option vomitting so change position del, child reg, edit child details and add siblings
    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
        try {
            if (mSelection != null && which < mSelection.length) {
                if (which > 0) {
                    mSelection[which] = isChecked;

                    //ramesh 12-May- 2021
                    if (AdolVisit_New.spnNormalusePeriod != null && AdolVisit_New.spnNormalusePeriod.getAdapter() == simple_adapter) {
                        if (mSelection[6]) {
                            AdolVisit_New.trMensturalNormalUseOthers.setVisibility(VISIBLE);
                        } else {
                            AdolVisit_New.trMensturalNormalUseOthers.setVisibility(GONE);
                            if (!mSelection[6])
                                AdolVisit_New.aqAdolVisit.id(R.id.etMensturalnormaluseperiodOthers).text("");
                        }
                    }

                    if (AdolVisit_New.spnMensturalProblems != null && AdolVisit_New.spnMensturalProblems.getAdapter() == simple_adapter) {
                        if (mSelection[8]) {
                            AdolVisit_New.trMensturalProblemOthers.setVisibility(VISIBLE);
                        } else {
                            AdolVisit_New.trMensturalProblemOthers.setVisibility(GONE);
                            if (!mSelection[8])
                                AdolVisit_New.aqAdolVisit.id(R.id.etMensturalMenustralOthers).text("");
                        }
                    }

                    if (AdolVisit_New.spnTreatmentPlaces != null && AdolVisit_New.spnTreatmentPlaces.getAdapter() == simple_adapter) {
                        if (mSelection[5]) {
                            AdolVisit_New.trTreatmentOthers.setVisibility(VISIBLE);
                        } else {
                            AdolVisit_New.trTreatmentOthers.setVisibility(GONE);
                            if (!mSelection[5])
                                AdolVisit_New.aqAdolVisit.id(R.id.etTreatmentOthers).text("");
                        }
                    }


                    if (AdolVisit_New.spnHealthProblems != null && simple_adapter == AdolVisit_New.spnHealthProblems.getAdapter()) {
                        if (mSelection[9]) {
                            AdolVisit_New.trhealthissueothers.setVisibility(VISIBLE);
                        } else {
                            AdolVisit_New.trhealthissueothers.setVisibility(GONE);
                            AdolVisit_New.aqAdolVisit.id(R.id.etAdolVisitHealthIssuesYesOthers).text("");
                        }
                        if (AdolVisit_New.spnHealthProblems.getSelectedIndicies().size() != 0) {
                            AdolVisit_New.llCovidQ.setVisibility(VISIBLE);
                        } else {
                            AdolVisit_New.llCovidQ.setVisibility(GONE);
                        }

                    }

//                    16May2021 Bindu add Home visit covid health issues
                    if (HomeVisit.spnHealthProblemshv != null && simple_adapter == HomeVisit.spnHealthProblemshv.getAdapter()) {
                        if (mSelection[9]) { //if other pbm display other
                            HomeVisit.aq.id(R.id.trHealthProblemYesOthers).visible();
                        } else {
                            HomeVisit.aq.id(R.id.trHealthProblemYesOthers).gone();
                            HomeVisit.aq.id(R.id.etVisitHealthIssuesYesOthers).text("");
                        }
                        if (HomeVisit.spnHealthProblemshv.getSelectedIndicies().size() != 0) {
                            HomeVisit.aq.id(R.id.llCovidQs).visible();
                            HomeVisit.aq.id(R.id.tr_CovidTest).visible();
                            HomeVisit.aq.id(R.id.trCovidResult).gone();
                        } else {
                            HomeVisit.aq.id(R.id.llCovidQs).gone();
                            HomeVisit.aq.id(R.id.tr_CovidTest).gone();
                            HomeVisit.aq.id(R.id.rd_CovidTestYes).checked(false);
                            HomeVisit.aq.id(R.id.rd_CovidTestNo).checked(false);
                            HomeVisit.aq.id(R.id.rd_CovidPositive).checked(false);
                            HomeVisit.aq.id(R.id.rd_CovidNegative).checked(false);
                            HomeVisit.hideshowcovidTestdetails();

                        }
                    }

                    //                    17May2021 Bindu add PNC Home visit covid health issues
                    if (PncHomeVisit.spnHealthProblemshv != null && simple_adapter == PncHomeVisit.spnHealthProblemshv.getAdapter()) {
                        if (mSelection[9]) { //if other pbm display other
                            PncHomeVisit.aq.id(R.id.trHealthProblemYesOthers).visible();
                        } else {
                            PncHomeVisit.aq.id(R.id.trHealthProblemYesOthers).gone();
                            PncHomeVisit.aq.id(R.id.etVisitHealthIssuesYesOthers).text("");
                        }
                        if (PncHomeVisit.spnHealthProblemshv.getSelectedIndicies().size() != 0) {
                            PncHomeVisit.aq.id(R.id.llCovidQs).visible();
                            PncHomeVisit.aq.id(R.id.tr_CovidTest).visible();
                            PncHomeVisit.aq.id(R.id.trCovidResult).gone();
                        } else {
                            PncHomeVisit.aq.id(R.id.llCovidQs).gone();
                            PncHomeVisit.aq.id(R.id.tr_CovidTest).gone();
                            PncHomeVisit.aq.id(R.id.rd_CovidTestYes).checked(false);
                            PncHomeVisit.aq.id(R.id.rd_CovidTestNo).checked(false);
                            PncHomeVisit.aq.id(R.id.rd_CovidPositive).checked(false);
                            PncHomeVisit.aq.id(R.id.rd_CovidNegative).checked(false);
                            PncHomeVisit.hideshowcovidTestdetails();

                        }

                    }

                    if (DeliveryInfoActivity.multiComplications != null && simple_adapter == DeliveryInfoActivity.multiComplications.getAdapter()) {
                        if (mSelection[5]) {
                            DeliveryInfoActivity.aqDel.id(R.id.trothercomplmother).visible();
                            if (DeliveryInfoActivity.aqDel.id(R.id.tblchild1).getView().getVisibility() == View.VISIBLE) { //02May2021 Bindu - Temp fix Samsung 8 inch tab UI was affected so hiding the first screen
                                DeliveryInfoActivity.aqDel.id(R.id.tblchild1).gone();
                                DeliveryInfoActivity.aqDel.id(R.id.imgchl1expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                            }
                        } else {
                            DeliveryInfoActivity.aqDel.id(R.id.trothercomplmother).gone();
                            DeliveryInfoActivity.aqDel.id(R.id.etothercomplmother).text("");
                        }
                    } else if (DeliveryInfoActivity.chlCompl1 != null &&
                            simple_adapter == DeliveryInfoActivity.chlCompl1.getAdapter()) {
                        if (mSelection[9])
                            DeliveryInfoActivity.aqDel.id(R.id.trothercomplchl1).visible();
                        else {
                            DeliveryInfoActivity.aqDel.id(R.id.trothercomplchl1).gone();
                            DeliveryInfoActivity.aqDel.id(R.id.etothercomplchl1).text("");
                        }
                    } else if (DeliveryInfoActivity.chlCompl2 != null && simple_adapter == DeliveryInfoActivity.chlCompl2.getAdapter()) {
                        if (mSelection[9])
                            DeliveryInfoActivity.aqDel.id(R.id.trothercomplchl2).visible();
                        else {
                            DeliveryInfoActivity.aqDel.id(R.id.trothercomplchl2).gone();
                            DeliveryInfoActivity.aqDel.id(R.id.etothercomplchl2).text("");
                        }
                    } else if (DeliveryInfoActivity.chlCompl3 != null && simple_adapter == DeliveryInfoActivity.chlCompl3.getAdapter()) {
                        if (mSelection[9])
                            DeliveryInfoActivity.aqDel.id(R.id.trothercomplchl3).visible();
                        else {
                            DeliveryInfoActivity.aqDel.id(R.id.trothercomplchl3).gone();
                            DeliveryInfoActivity.aqDel.id(R.id.etothercomplchl3).text("");
                        }
                    } else if (DeliveryInfoActivity.chlCompl4 != null && simple_adapter == DeliveryInfoActivity.chlCompl4.getAdapter()) {
                        if (mSelection[9])
                            DeliveryInfoActivity.aqDel.id(R.id.trothercomplchl4).visible();
                        else {
                            DeliveryInfoActivity.aqDel.id(R.id.trothercomplchl4).gone();
                            DeliveryInfoActivity.aqDel.id(R.id.etothercomplchl4).text("");
                        }
                    } else if (ChildRegistrationActivity.aqChildReg != null && ChildRegistrationActivity.aqChildReg.id(R.id.mspnchildcompl + 1)
                            .getSpinner() != null &&
                            simple_adapter == ChildRegistrationActivity.aqChildReg.id(R.id.mspnchildcompl + 1)
                                    .getSpinner().getAdapter()) {
                        if (ChildRegistrationActivity.aqChildReg != null && ChildRegistrationActivity.aqChildReg.id(R.id.trOtherChildCompl1)
                                != null) {
                            if (mSelection[6])//03May2021 Arpitha - 8 -->3
                                ChildRegistrationActivity.aqChildReg.id(R.id.trOtherChildCompl1).visible();
                            else {
                                ChildRegistrationActivity.aqChildReg.id(R.id.trOtherChildCompl1).gone();
                                ChildRegistrationActivity.aqChildReg.id(R.id.etOtherChildCompl1).text("");
                            }
                        }

                    } else if (ChildRegistrationActivity.aqChildReg != null && ChildRegistrationActivity.aqChildReg.id(R.id.mspnchildcompl + 2)
                            .getSpinner() != null &&
                            simple_adapter == ChildRegistrationActivity.aqChildReg.id(R.id.mspnchildcompl + 2)
                                    .getSpinner().getAdapter()) {
                        if (ChildRegistrationActivity.aqChildReg.id(R.id.trOtherChildCompl2)
                                != null) {
                            if (mSelection[6])
                                ChildRegistrationActivity.aqChildReg.id(R.id.trOtherChildCompl2).visible();
                            else {
                                ChildRegistrationActivity.aqChildReg.id(R.id.trOtherChildCompl2).gone();
                                ChildRegistrationActivity.aqChildReg.id(R.id.etOtherChildCompl2).text("");
                            }
                        }

                    } else if (ChildRegistrationActivity.aqChildReg != null && ChildRegistrationActivity.aqChildReg.id(R.id.mspnchildcompl + 3)
                            .getSpinner() != null &&
                            simple_adapter == ChildRegistrationActivity.aqChildReg.id(R.id.mspnchildcompl + 3)
                                    .getSpinner().getAdapter()) {
                        if (ChildRegistrationActivity.aqChildReg.id(R.id.trOtherChildCompl3)
                                != null) {
                            if (mSelection[6])
                                ChildRegistrationActivity.aqChildReg.id(R.id.trOtherChildCompl3).visible();
                            else {
                                ChildRegistrationActivity.aqChildReg.id(R.id.trOtherChildCompl3).gone();
                                ChildRegistrationActivity.aqChildReg.id(R.id.etOtherChildCompl3).text("");
                            }
                        }

                    } else if (ChildRegistrationActivity.aqChildReg != null && ChildRegistrationActivity.aqChildReg.id(R.id.mspnchildcompl + 4)
                            .getSpinner() != null &&
                            simple_adapter == ChildRegistrationActivity.aqChildReg.id(R.id.mspnchildcompl + 4)
                                    .getSpinner().getAdapter()) {
                        if (ChildRegistrationActivity.aqChildReg.id(R.id.trOtherChildCompl4)
                                != null) {
                            if (mSelection[6])
                                ChildRegistrationActivity.aqChildReg.id(R.id.trOtherChildCompl4).visible();
                            else {
                                ChildRegistrationActivity.aqChildReg.id(R.id.trOtherChildCompl4).gone();
                                ChildRegistrationActivity.aqChildReg.id(R.id.etOtherChildCompl4).text("");
                            }
                        }

                    } else if (ChildRegistrationActivity.aqChildReg != null && ChildRegistrationActivity.aqChildReg.id(R.id.mspnchildcompl + 5)
                            .getSpinner() != null &&
                            simple_adapter == ChildRegistrationActivity.aqChildReg.id(R.id.mspnchildcompl + 5)
                                    .getSpinner().getAdapter()) {
                        if (ChildRegistrationActivity.aqChildReg.id(R.id.trOtherChildCompl5)
                                != null) {
                            if (mSelection[6])
                                ChildRegistrationActivity.aqChildReg.id(R.id.trOtherChildCompl5).visible();
                            else {
                                ChildRegistrationActivity.aqChildReg.id(R.id.trOtherChildCompl5).gone();
                                ChildRegistrationActivity.aqChildReg.id(R.id.etOtherChildCompl5).text("");
                            }
                        }

                    } else if (ChildRegistrationActivity.aqChildReg != null && ChildRegistrationActivity.aqChildReg.id(R.id.mspnchildcompl + 6)
                            .getSpinner() != null &&
                            simple_adapter == ChildRegistrationActivity.aqChildReg.id(R.id.mspnchildcompl + 6)
                                    .getSpinner().getAdapter()) {
                        if (ChildRegistrationActivity.aqChildReg.id(R.id.trOtherChildCompl6)
                                != null) {
                            if (mSelection[6])
                                ChildRegistrationActivity.aqChildReg.id(R.id.trOtherChildCompl6).visible();
                            else {
                                ChildRegistrationActivity.aqChildReg.id(R.id.trOtherChildCompl6).gone();
                                ChildRegistrationActivity.aqChildReg.id(R.id.etOtherChildCompl6).text("");
                            }
                        }

                    }
//              01May2021 Arpitha
                    else if (EditChildActivity.aqEditChild != null && EditChildActivity.aqEditChild.id(R.id.spncomplicationschl4) != null &&
                            simple_adapter == EditChildActivity.aqEditChild.id(R.id.spncomplicationschl4).getSpinner().getAdapter()) {
                        if (mSelection[9])
                            EditChildActivity.aqEditChild.id(R.id.trothercomplchl4).visible();
                        else {
                            EditChildActivity.aqEditChild.id(R.id.trothercomplchl4).gone();
                            EditChildActivity.aqEditChild.id(R.id.etothercomplchl4).text("");
                        }
                    } else if (AddSiblingsActivity.aqChildReg != null && AddSiblingsActivity.aqChildReg.id(R.id.mspnchildcompl + 1)
                            .getSpinner() != null &&
                            simple_adapter == AddSiblingsActivity.aqChildReg.id(R.id.mspnchildcompl + 1)
                                    .getSpinner().getAdapter()) {
                        if (AddSiblingsActivity.aqChildReg.id(R.id.trOtherChildCompl1)
                                != null) {
                            if (mSelection[6])
                                AddSiblingsActivity.aqChildReg.id(R.id.trOtherChildCompl1).visible();
                            else {
                                AddSiblingsActivity.aqChildReg.id(R.id.trOtherChildCompl1).gone();
                                AddSiblingsActivity.aqChildReg.id(R.id.etOtherChildCompl1).text("");
                            }
                        }

                    } else if (AddSiblingsActivity.aqChildReg != null && AddSiblingsActivity.aqChildReg.id(R.id.mspnchildcompl + 2)
                            .getSpinner() != null &&
                            simple_adapter == AddSiblingsActivity.aqChildReg.id(R.id.mspnchildcompl + 2)
                                    .getSpinner().getAdapter()) {
                        if (AddSiblingsActivity.aqChildReg.id(R.id.trOtherChildCompl2)
                                != null) {
                            if (mSelection[6])
                                AddSiblingsActivity.aqChildReg.id(R.id.trOtherChildCompl2).visible();
                            else {
                                AddSiblingsActivity.aqChildReg.id(R.id.trOtherChildCompl2).gone();
                                AddSiblingsActivity.aqChildReg.id(R.id.etOtherChildCompl2).text("");
                            }
                        }

                    } else if (AddSiblingsActivity.aqChildReg != null && AddSiblingsActivity.aqChildReg.id(R.id.mspnchildcompl + 3)
                            .getSpinner() != null &&
                            simple_adapter == AddSiblingsActivity.aqChildReg.id(R.id.mspnchildcompl + 3)
                                    .getSpinner().getAdapter()) {
                        if (AddSiblingsActivity.aqChildReg.id(R.id.trOtherChildCompl3)
                                != null) {
                            if (mSelection[6])
                                AddSiblingsActivity.aqChildReg.id(R.id.trOtherChildCompl3).visible();
                            else {
                                AddSiblingsActivity.aqChildReg.id(R.id.trOtherChildCompl3).gone();
                                AddSiblingsActivity.aqChildReg.id(R.id.etOtherChildCompl3).text("");
                            }
                        }

                    } else if (AddSiblingsActivity.aqChildReg != null && AddSiblingsActivity.aqChildReg.id(R.id.mspnchildcompl + 4)
                            .getSpinner() != null &&
                            simple_adapter == AddSiblingsActivity.aqChildReg.id(R.id.mspnchildcompl + 4)
                                    .getSpinner().getAdapter()) {
                        if (AddSiblingsActivity.aqChildReg.id(R.id.trOtherChildCompl4)
                                != null) {
                            if (mSelection[6])
                                AddSiblingsActivity.aqChildReg.id(R.id.trOtherChildCompl4).visible();
                            else {
                                AddSiblingsActivity.aqChildReg.id(R.id.trOtherChildCompl4).gone();
                                AddSiblingsActivity.aqChildReg.id(R.id.etOtherChildCompl4).text("");
                            }
                        }

                    } else if (AddSiblingsActivity.aqChildReg != null && AddSiblingsActivity.aqChildReg.id(R.id.mspnchildcompl + 5)
                            .getSpinner() != null &&
                            simple_adapter == AddSiblingsActivity.aqChildReg.id(R.id.mspnchildcompl + 5)
                                    .getSpinner().getAdapter()) {
                        if (AddSiblingsActivity.aqChildReg.id(R.id.trOtherChildCompl5)
                                != null) {
                            if (mSelection[6])
                                AddSiblingsActivity.aqChildReg.id(R.id.trOtherChildCompl5).visible();
                            else {
                                AddSiblingsActivity.aqChildReg.id(R.id.trOtherChildCompl5).gone();
                                AddSiblingsActivity.aqChildReg.id(R.id.etOtherChildCompl5).text("");
                            }
                        }

                    } else if (AddSiblingsActivity.aqChildReg != null && AddSiblingsActivity.aqChildReg.id(R.id.mspnchildcompl + 6)
                            .getSpinner() != null &&
                            simple_adapter == AddSiblingsActivity.aqChildReg.id(R.id.mspnchildcompl + 6)
                                    .getSpinner().getAdapter()) {
                        if (AddSiblingsActivity.aqChildReg.id(R.id.trOtherChildCompl6)
                                != null) {
                            if (mSelection[6])
                                AddSiblingsActivity.aqChildReg.id(R.id.trOtherChildCompl6).visible();
                            else {
                                AddSiblingsActivity.aqChildReg.id(R.id.trOtherChildCompl6).gone();
                                AddSiblingsActivity.aqChildReg.id(R.id.etOtherChildCompl6).text("");
                            }
                        }

                    } else if (EditRegChildInfoActivity.aqEditChild != null && EditRegChildInfoActivity.aqEditChild.
                            id(R.id.mspnchildcompl + 1)
                            .getSpinner() != null &&
                            simple_adapter == EditRegChildInfoActivity.aqEditChild.id(R.id.mspnchildcompl + 1)
                                    .getSpinner().getAdapter()) {
                        if (EditRegChildInfoActivity.aqEditChild.id(R.id.trOtherChildCompl1)
                                != null) {
                            if (mSelection[6])
                                EditRegChildInfoActivity.aqEditChild.id(R.id.trOtherChildCompl1).visible();
                            else {
                                EditRegChildInfoActivity.aqEditChild.id(R.id.trOtherChildCompl1).gone();
                                EditRegChildInfoActivity.aqEditChild.id(R.id.etOtherChildCompl1).text("");
                            }
                        }

                    }


                    //01MAy2021 Bindu add Edit Child Activity - Compl Spinner - pending - no field in xml
                /*else if (EditChildActivity.aqEditChild!=null && EditChildActivity.aqEditChild.
                        id(R.id.mspnchildcompl + 1)
                        .getSpinner() != null &&
                        simple_adapter == EditRegChildInfoActivity.aqEditChild.id(R.id.mspnchildcompl + 1)
                                .getSpinner().getAdapter())
                {
                    if (EditChildActivity.aqEditChild.id(R.id.trOtherChildCompl1)
                            != null) {
                        if (mSelection[5])
                            EditRegChildInfoActivity.aqEditChild.id(R.id.trOtherChildCompl1).visible();
                        else {
                            EditRegChildInfoActivity.aqEditChild.id(R.id.trOtherChildCompl1).gone();
                            EditRegChildInfoActivity.aqEditChild.id(R.id.etOtherChildCompl1).text("");
                        }
                    }

                }*/

                    simple_adapter.clear();
                    simple_adapter.add(buildSelectedItemString());
                } else {
                    mSelection[0] = false;
                    ((AlertDialog) dialog).getListView().setItemChecked(0, false);
                }
            } else {
                throw new IllegalArgumentException(
                        "Argument 'which' is out of bounds.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean performClick() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMultiChoiceItems(_items, mSelection, this);
        if (AdolStartTraining.msProObj != null && AdolStartTraining.msProObj.getAdapter() != null && simple_adapter == AdolStartTraining.msProObj.getAdapter()) {//Ramesh 12-May-2021
            builder.setTitle(getResources().getString(R.string.trainingtopic));
        } else if (AdolVisit_New.spnHealthProblems != null && AdolVisit_New.spnHealthProblems.getAdapter() != null && simple_adapter == AdolVisit_New.spnHealthProblems.getAdapter()) {
            builder.setTitle(getResources().getString(R.string.healthissues));
        } else if (AdolVisit_New.spnNormalusePeriod != null && AdolVisit_New.spnNormalusePeriod.getAdapter() != null && simple_adapter == AdolVisit_New.spnNormalusePeriod.getAdapter()) {
            builder.setTitle(getResources().getString(R.string.normaluseforperiod));
        } else if (AdolVisit_New.spnMensturalProblems != null && AdolVisit_New.spnMensturalProblems.getAdapter() != null && simple_adapter == AdolVisit_New.spnMensturalProblems.getAdapter()) {
            builder.setTitle(getResources().getString(R.string.menturalproblems));
        } else if (AdolVisit_New.spnTreatmentPlaces != null && AdolVisit_New.spnTreatmentPlaces.getAdapter() != null && simple_adapter == AdolVisit_New.spnTreatmentPlaces.getAdapter()) {
            builder.setTitle(getResources().getString(R.string.agwheretreated));
        } else if (AdolVisit_New.spnIFAReasons != null && AdolVisit_New.spnIFAReasons.getAdapter() != null && simple_adapter == AdolVisit_New.spnIFAReasons.getAdapter()) {
            builder.setTitle(getResources().getString(R.string.ifareasontxt));
        }
        //mani 14Sep2021
        else if (ChildHomeVisit.spnIFAReasons != null && ChildHomeVisit.spnIFAReasons.getAdapter() != null && simple_adapter == ChildHomeVisit.spnIFAReasons.getAdapter()) {
            builder.setTitle(getResources().getString(R.string.ifareasontxt));
        } else if (PncHomeVisit.spnIFAReasons1 != null && PncHomeVisit.spnIFAReasons1.getAdapter() != null && simple_adapter == PncHomeVisit.spnIFAReasons1.getAdapter()) {
            builder.setTitle(getResources().getString(R.string.ifareasontxt));
        }else if (PncHomeVisit.spnIFAReasons2 != null && PncHomeVisit.spnIFAReasons2.getAdapter() != null && simple_adapter == PncHomeVisit.spnIFAReasons2.getAdapter()) {
            builder.setTitle(getResources().getString(R.string.ifareasontxt));
        }else if (PncHomeVisit.spnIFAReasons3 != null && PncHomeVisit.spnIFAReasons3.getAdapter() != null && simple_adapter == PncHomeVisit.spnIFAReasons3.getAdapter()) {
            builder.setTitle(getResources().getString(R.string.ifareasontxt));
        }else if (PncHomeVisit.spnIFAReasons4 != null && PncHomeVisit.spnIFAReasons4.getAdapter() != null && simple_adapter == PncHomeVisit.spnIFAReasons4.getAdapter()) {
            builder.setTitle(getResources().getString(R.string.ifareasontxt));
        }
//        Bindu Home visit
        else if (HomeVisit.spnHealthProblemshv != null && HomeVisit.spnHealthProblemshv.getAdapter() != null && simple_adapter == HomeVisit.spnHealthProblemshv.getAdapter()) {
            builder.setTitle(getResources().getString(R.string.healthissues));
        } else {
            builder.setTitle(getResources().getString(R.string.complications));
        }
        builder.show();


        return true;
    }

    @Override
    public void setAdapter(SpinnerAdapter adapter) {
        throw new RuntimeException(
                "setAdapter is not supported by MultiSelectSpinner.");
    }

    public void setItems(String[] items) {
        _items = items;
        mSelection = new boolean[_items.length];
        simple_adapter.clear();
        simple_adapter.add(_items[0]);
        Arrays.fill(mSelection, false);
    }

    public void setItems(List<String> items) {
        _items = items.toArray(new String[items.size()]);
        mSelection = new boolean[_items.length];
        simple_adapter.clear();
        simple_adapter.add(_items[0]);
        Arrays.fill(mSelection, false);
    }

    public void setSelection(String[] selection) {
        for (String cell : selection) {
            for (int j = 0; j < _items.length; ++j) {
                if ((_items[j]).equals(cell)) {
                    mSelection[j] = true;
                }
            }
        }
        simple_adapter.clear();
        simple_adapter.add(buildSelectedItemString());
    }

    public void setSelection(List<String> selection) {
        for (int i = 0; i < mSelection.length; i++) {
            mSelection[i] = false;
        }
        for (String sel : selection) {
            for (int j = 0; j < _items.length; ++j) {
                if (_items[j].equals(sel)) {
                    mSelection[j] = true;
                }
            }
        }
        simple_adapter.clear();
        simple_adapter.add(buildSelectedItemString());
    }

    public void setSelection(int index) {
        for (int i = 0; i < mSelection.length; i++) {
            mSelection[i] = false;
        }
        if (index >= 0 && index < mSelection.length) {
            mSelection[index] = true;
        } else {
            throw new IllegalArgumentException("Index " + index
                    + " is out of bounds.");
        }
        simple_adapter.clear();
        simple_adapter.add(buildSelectedItemString());
    }

    public void setSelection(int[] selectedIndicies) {
        for (int i = 0; i < mSelection.length; i++) {
            mSelection[i] = false;
        }
        for (int index : selectedIndicies) {
            if (index >= 0 && index < mSelection.length) {
                mSelection[index] = true;
            } else {
                throw new IllegalArgumentException("Index " + index
                        + " is out of bounds.");
            }
        }
        simple_adapter.clear();
        simple_adapter.add(buildSelectedItemString());
    }

    public List<String> getSelectedStrings() {
        List<String> selection = new LinkedList<String>();
        for (int i = 0; i < _items.length; ++i) {
            if (mSelection[i]) {
                selection.add(_items[i]);
            }
        }
        return selection;
    }

    public List<Integer> getSelectedIndicies() {
        List<Integer> selection = new LinkedList<Integer>();
        for (int i = 0; i < _items.length; ++i) {
            if (mSelection[i]) {
                selection.add(i);
            }
        }
        return selection;
    }

    private String buildSelectedItemString() {
        StringBuilder sb = new StringBuilder();
        boolean foundOne = false;

        for (int i = 0; i < _items.length; ++i) {
            if (mSelection[i]) {
                if (foundOne) {
                    sb.append(", ");
                }
                foundOne = true;

                sb.append(_items[i]);
            }
        }
        return sb.toString();
    }

    public String getSelectedItemsAsString() {
        StringBuilder sb = new StringBuilder();
        boolean foundOne = false;

        for (int i = 0; i < _items.length; ++i) {
            if (mSelection[i]) {
                if (foundOne) {
                    sb.append(", ");
                }
                foundOne = true;
                sb.append(_items[i]);
            }
        }
        return sb.toString();
    }


    @Override
    public boolean performItemClick(View view, int position, long id) {

        DeliveryInfoActivity.aqDel.id(R.id.trothercomplmother).visible();


        if (view == DeliveryInfoActivity.multiComplications) {
            if (mSelection[5])
                DeliveryInfoActivity.aqDel.id(R.id.trothercomplmother).visible();
            else
                DeliveryInfoActivity.aqDel.id(R.id.trothercomplmother).gone();

        }
        if (view == DeliveryInfoActivity.chlCompl1) {
            if (mSelection[9])
                DeliveryInfoActivity.aqDel.id(R.id.trothercomplchl1).visible();
            else
                DeliveryInfoActivity.aqDel.id(R.id.trothercomplchl1).gone();
        }

        return super.performItemClick(view, position, id);
    }


}
