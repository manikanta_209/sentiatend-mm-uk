package com.sc.stmansi.common;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Build;
import android.os.Bundle;

import com.google.firebase.crashlytics.FirebaseCrashlytics;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.sc.stmansi.R;

/**
 * Creating default datepicker fragment,
 * and set the selected date to the field
 */
@SuppressLint("SimpleDateFormat")
public class CommonDatePickerFragment extends DialogFragment {

    public static Activity activity;
    public static String defaultDate = "";

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        int year = 0, month = 0, day = 0;
        DatePickerDialog dpDialog = null;
        try {

            Calendar c = Calendar.getInstance();
            if (defaultDate != null && defaultDate.length() > 0) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                Date sendDate = sdf.parse(defaultDate);
                c.setTime(sendDate);
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);
            } else {
                c = Calendar.getInstance();
                c.setTime(new Date());
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);
            }

            // Create a new instance of DatePickerDialog and return it
            //04Dec2018 - Bindu - put the code in try block
            dpDialog = new DatePickerDialog(activity, (OnDateSetListener) activity, year, month, day);
            //Bindu 21Jun2019, 04Aug2019 - Bindu change the theme
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                // Do something for lollipop and above versions
                dpDialog = new DatePickerDialog(activity, R.style.DialogTheme, (OnDateSetListener) activity, year, month, day);
            } else {
                // do something for phones running an SDK before lollipop
                dpDialog = new DatePickerDialog(activity, AlertDialog.THEME_HOLO_LIGHT, (OnDateSetListener) activity, year, month, day);
                dpDialog.setTitle("Set a Date");
            }
            dpDialog.getDatePicker().setCalendarViewShown(false);
            return dpDialog;
        } catch (Exception e) {
            
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            return dpDialog;
        }
    }

    //	get common date
    public static void getCommonDate(Activity act, String dDate) {
        try {
            activity = act;
            defaultDate = dDate;
            DialogFragment newFragment = new CommonDatePickerFragment();
            newFragment.show(act.getFragmentManager(), "datePicker");
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

}
