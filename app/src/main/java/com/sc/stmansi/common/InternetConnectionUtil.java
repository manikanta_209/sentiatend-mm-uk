//27Aug2019 Arpitha - code clean up
package com.sc.stmansi.common;

import android.content.Context;
import android.net.ConnectivityManager;

import com.sc.stmansi.configuration.AppState;

public class InternetConnectionUtil {

	// To Check whether internet is on or not in the device
	public static boolean isConnectionAvailable(Context context, AppState appState) {
		if (isNetworkUp(context)) {
			appState.internetOn = true;
			return true;
		}

		return false;
	}

//	to check for SIM card availability
	private static boolean isNetworkUp(Context context) {
		//		This code checks Network is up or not
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isAvailable() && cm.getActiveNetworkInfo().isConnected();
	}
}
