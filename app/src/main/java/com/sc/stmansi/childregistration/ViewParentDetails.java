package com.sc.stmansi.childregistration;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.legacy.app.ActionBarDrawerToggle;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.About.AboutActivity;
import com.sc.stmansi.childgrowthmonitor.CGMList;
import com.sc.stmansi.childgrowthmonitor.CGMNewVisit;
import com.sc.stmansi.childhomevisit.ChildHomeVisit;
import com.sc.stmansi.childhomevisit.ChildHomeVisitHistory;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.common.NavDrawerItem;
import com.sc.stmansi.common.NavDrawerListAdapter;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.deactivate.ChildDeactivateActivity;
import com.sc.stmansi.immunization.ImmunizationListActivity;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.repositories.ChildRepository;
import com.sc.stmansi.repositories.LoginRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.tables.TblChildInfo;
import com.sc.stmansi.tables.TblChlParentDetails;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.womanlist.RegisteredWomenActionTabs;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sc.stmansi.R;

public class ViewParentDetails extends AppCompatActivity {

    AQuery aqParent;
    AppState appState;
    DatabaseHelper databaseHelper;
    String strParentId;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    TblChildInfo tblChildInfo;
    List<TblChlParentDetails> parentDetailsList;
    TblInstusers user;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewparentdetails);
        try {
            Bundle bundle = getIntent().getBundleExtra("globalState");
            appState = bundle.getParcelable("appState");

            tblChildInfo = (TblChildInfo) getIntent().getSerializableExtra("tblChildInfo");

            strParentId = tblChildInfo.getChlParentId();

            databaseHelper = getHelper();

            aqParent = new AQuery(this);

            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(false);


            //mani 2021 June 17
            UserRepository userRepository = new UserRepository(databaseHelper);
             user = userRepository.getOneAuditedUser(appState.ashaId);


            initiateDrawer();
            getParentData();

            LinearLayout linearLayout = findViewById(R.id.llparent);
            disableEnableControls(false, linearLayout);



            aqParent.id(R.id.btncancel).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent exit = new Intent(ViewParentDetails.this, MainMenuActivity.class);
                    exit.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                    startActivity(exit);
                }
            });
        }catch (Exception e)
        {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private void getParentData() throws SQLException {

        ChildRepository childRepository = new ChildRepository(databaseHelper);

        parentDetailsList = childRepository.getParentDetails(strParentId);

        if(parentDetailsList!=null && parentDetailsList.size()>0)
        {
            aqParent.id(R.id.etname).text(parentDetailsList.get(0).getChlMotherName());
            if(parentDetailsList.get(0).getChlMotherAge()>0)
            aqParent.id(R.id.etage).text(""+parentDetailsList.get(0).getChlMotherAge());
            if(parentDetailsList.get(0).getChlMotherAgeWhenPassedAway()>0)
            aqParent.id(R.id.etagewhenpassedaway).text(""+parentDetailsList.get(0).getChlMotherAgeWhenPassedAway());
            aqParent.id(R.id.etfathername).text(parentDetailsList.get(0).getChlFatherName());
            aqParent.id(R.id.etthaicard).text(parentDetailsList.get(0).getChlMotherId());
            aqParent.id(R.id.etothercauseofdeath).text(parentDetailsList.get(0).getChlMotherDeathOther());
            if(parentDetailsList.get(0).getChlMotherIdType()!=null && parentDetailsList.get(0).getChlMotherIdType().equalsIgnoreCase("RCHID"))
            aqParent.id(R.id.rdrchid).checked(true);
            else
                aqParent.id(R.id.rdthayicard).checked(true);

            if(parentDetailsList.get(0).getChlRelation()!=null && parentDetailsList.get(0).getChlRelation().equalsIgnoreCase("Guardian")) {
                aqParent.id(R.id.rdguardian).checked(true);
                aqParent.id(R.id.trgurname).visible();
                aqParent.id(R.id.etgurname).text(parentDetailsList.get(0).getChlGuardianName());
                aqParent.id(R.id.tvthaicard).text(getResources().getString(R.string.rchid));
                aqParent.id(R.id.trage).gone();
                aqParent.id(R.id.tridtype).gone();
                aqParent.id(R.id.trthayicard).gone();
                aqParent.id(R.id.trmotherdetails).visible();
            }

            else {
                aqParent.id(R.id.rdmother).checked(true);
            }

            if(parentDetailsList.get(0).getChlFatherRemarried()!=null &&
                    parentDetailsList.get(0).getChlFatherRemarried().equalsIgnoreCase("Y"))
                aqParent.id(R.id.rdremarriedyes).checked(true);
            else if(parentDetailsList.get(0).getChlFatherRemarried()!=null &&
                    parentDetailsList.get(0).getChlFatherRemarried().equalsIgnoreCase("N"))//07Aug2021 Arpitha
                aqParent.id(R.id.rdremarriedno).checked(true);

            if(parentDetailsList.get(0).getChlMotherDeath()==1) {
                aqParent.id(R.id.chkmotherdeath).checked(true);
                aqParent.id(R.id.trcausefdeath).visible();
                aqParent.id(R.id.trothercause).visible();
                aqParent.id(R.id.tragewhenpassesaway).visible();
                aqParent.id(R.id.trdeathdate).visible();
                aqParent.id(R.id.spncasuseofdeathchl).setSelection(parentDetailsList.get(0).getChlMotherDeathReason());

                aqParent.id(R.id.etdeathdate).text(parentDetailsList.get(0).getChlMotherDeathDate());
                if(parentDetailsList.get(0).getChlFatherDeath()==1)
                aqParent.id(R.id.chkfatherdeath).checked(true);
                else
                    aqParent.id(R.id.chkfatherdeath).checked(false);

            }



        }

    }

    //    initialize db object
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer1); //27Sep2019 - Bindu - set drawer and enable true home btn
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);

        if(parentDetailsList!=null && parentDetailsList.size()>0) {
            getSupportActionBar().setTitle(parentDetailsList.get(0).getChlMotherName());
            aqParent.id(R.id.tvWomanName).text(parentDetailsList.get(0).getChlMotherName());
        }

//        aqParent.id(R.id.ivWomanImg).gone();
        getMenuInflater().inflate(R.menu.servicesmenu, menu);
        aqParent.id(R.id.ivWomanImg).gone();
        aqParent.id(R.id.tvInfo1).gone();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case R.id.logout: {
                Intent goToScreen = new Intent(ViewParentDetails.this, LoginActivity.class);
                displayAlert(getResources().getString(R.string.m111), goToScreen);
                return true;
            }
            case R.id.home: {
                Intent home = new Intent(ViewParentDetails.this, MainMenuActivity.class);
                home.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                startActivity(home);
                return true;
            }
            case R.id.wlist: {
                Intent goToScreen = new Intent(ViewParentDetails.this, RegisteredWomenActionTabs.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }case R.id.about: {
                Intent goToScreen = new Intent(ViewParentDetails.this, AboutActivity.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    /**
     * Save Confirmation Alert
     *
     * @param goToScreen
     * @param spanText2
     */
    private void displayAlert(final String spanText2, final Intent goToScreen) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        String strMess;
        if (goToScreen != null)
            strMess = getResources().getString(R.string.m121);
        else
            strMess = getResources().getString(R.string.ok);
        alertDialogBuilder.setMessage(spanText2).setCancelable(false).setNegativeButton(
                strMess, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (goToScreen != null) {
                            goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(goToScreen);
                            if (spanText2.contains("logout")) {
                                try {
                                    LoginRepository loginRepository = new LoginRepository(databaseHelper);
                                    loginRepository.updateLogoutTime(appState.loginAuditId, appState.sessionUserId);
                                } catch (Exception e) {
                                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                                }
                            }

                            dialog.cancel();

                        }
                    }
                });

        if (goToScreen != null) {
            alertDialogBuilder.setPositiveButton(getResources().getString(R.string.m122),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
        }
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    private void initiateDrawer() throws Exception {
        mDrawerLayout = findViewById(R.id.ett_drawer_layout);
        mDrawerList = findViewById(R.id.ett_left_drawer);

        ArrayList<NavDrawerItem> navDrawerItems = new ArrayList<>();
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.editchildinfo), R.drawable.ic_edit_60));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.immunizationlist), R.drawable.immunisation));

        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.addsibling), R.drawable.general_examination_baby));
        //15May2021 Bindu added CGM
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.gcmadd), R.drawable.registration)); //15MAy2021 Bindu
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.gcmregisterlist), R.drawable.child_growth));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.chlhv), R.drawable.ic_homevisit));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.chlhvhistory), R.drawable.prenatalhomevisit));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deactivate),R.drawable.deactivate));



        /*navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.services), R.drawable.anm_pending_activities));//01Oct2019 Arpitha

        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.addunplannedservices), R.drawable.anm_pending_activities));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.unplannedserviceslist), R.drawable.anm_pending_activities));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.homevisitadd),
                R.drawable.prenatalhomevisit));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.womanhomevisithistory),
                R.drawable.prenatalhomevisit));*/


        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        mDrawerList.setAdapter(new NavDrawerListAdapter(getApplicationContext(), navDrawerItems));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer,
                R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu(); // creates call to
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                getSupportActionBar().setDisplayShowTitleEnabled(false);
                getSupportActionBar().setDisplayShowCustomEnabled(false);

                int noOfWeeks;
                try {
                    if (tblChildInfo != null) {
                        getSupportActionBar().setTitle((tblChildInfo.getChlBabyname() + "\t"));
                    }
                } catch (Exception e) {


                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                try {
                    invalidateOptionsMenu(); // creates call to
                    getSupportActionBar().setDisplayShowHomeEnabled(false);
                    getSupportActionBar().setDisplayShowTitleEnabled(false);
                    getSupportActionBar().setDisplayShowCustomEnabled(true);
                    getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);

                    aqParent.id(R.id.ivWomanImg).getImageView().setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            mDrawerLayout.closeDrawer(mDrawerList);
                        }
                    });

                    aqParent.id(R.id.tvWomanName).text((tblChildInfo.getChlBabyname() + "("+tblChildInfo.getChlGender()+")"));
                    if (tblChildInfo.getChlDateTimeOfBirth().trim().length() > 0) {
                        aqParent.id(R.id.tvInfo1).text(
                                (getResources().getString(R.string.age) + " " +
                                        DateTimeUtil.calculateAge( tblChildInfo.getChlDateTimeOfBirth())));
                    }
                } catch (Exception e) {


                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    /**
     * This method is to sync draw,erToggle and to replace up caret icon to menu
     * icon in Action Bar
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    /**
     * The click listener for ListView in the navigation drawer
     */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            mDrawerLayout.closeDrawer(mDrawerList);
            try {

                switch (position) {

                    case 0:
                        if(tblChildInfo.getChlReg()==1) {
                            Intent nextScreen = new Intent(ViewParentDetails.this, EditRegChildInfoActivity.class);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                            nextScreen.putExtra("tblChildInfo", tblChildInfo);
                            startActivity(nextScreen);
                        }else
                        {
                            Intent nextScreen = new Intent(ViewParentDetails.this, EditChildActivity.class);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                            nextScreen.putExtra("tblChildInfo", tblChildInfo);
                            startActivity(nextScreen);
                        }
                    break; //26Apr2021 Bindu - add break
                    case 1: {
                        if(tblChildInfo.getChlDeliveryResult()<=1) {
                            Intent addSibling = new Intent(ViewParentDetails.this, ImmunizationListActivity.class);
                            addSibling.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            addSibling.putExtra("appState", getIntent().getBundleExtra("appState"));
                            addSibling.putExtra("tblChildInfo", tblChildInfo);
                            startActivity(addSibling);
                        }
                        else
                            Toast.makeText(getApplicationContext(),getResources().getString(R.string.m053),Toast.LENGTH_LONG).show();

                            break;
                    }
                    case 2: {
                        Intent viewParent = new Intent(ViewParentDetails.this, AddSiblingsActivity.class);
                        viewParent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        viewParent.putExtra("appState", getIntent().getBundleExtra("appState"));
                        if(tblChildInfo.getChlParentId()!=null && tblChildInfo.getChlParentId().trim().length()>0)
                            viewParent.putExtra("chlParentId", tblChildInfo.getChlParentId());
                        else
                            viewParent.putExtra("WomanId", tblChildInfo.getWomanId());                        startActivity(viewParent);
                        break;
                    }

                    case 7: {
                        Intent nextScreen = new Intent(ViewParentDetails.this, ChildDeactivateActivity.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                        nextScreen.putExtra("tblChildInfo", tblChildInfo);
                        startActivity(nextScreen);

                        break;
                    }
                    //15May2021 Bindu add cgm
                   /* case 3: {
                        Intent nextScreen = new Intent(ViewParentDetails.this, CGMNewVisit.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                        nextScreen.putExtra("tblChildInfo", tblChildInfo);
                        startActivity(nextScreen);
                        break;
                    }
*/
                    //Mani 17 June 2021 Checking condition if age is more than 5years
                    case 3:
                    {
                        if (((tblChildInfo.getChlDeactDate() != null &&
                                tblChildInfo.getChlDeactDate().trim().length() > 0)
                                || user.getIsDeactivated() == 1)) {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.userdeactivated), Toast.LENGTH_LONG).show();
                        } else {
                            //Mani 24 June 2021 Delivery result condition checking
                            if ((DateTimeUtil.calculateAgeInMonths(tblChildInfo.getChlDateTimeOfBirth()) >= 60)||tblChildInfo.getChlDeliveryResult()>1)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.aboveagewarning), Toast.LENGTH_LONG).show();
                            else {
                                Intent nextScreen = new Intent(ViewParentDetails.this, CGMNewVisit.class);
                                nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                                nextScreen.putExtra("tblChildInfo", tblChildInfo);
                                startActivity(nextScreen);
                            }

                        }

                        break;
                    }
                    //15May2021 Bindu add cgm
                    case 4: {
                        Intent nextScreen = new Intent(ViewParentDetails.this, CGMList.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                        nextScreen.putExtra("tblChildInfo", tblChildInfo);
                        startActivity(nextScreen);
                        break;
                    }

                    case 5: {
                        if (((tblChildInfo.getChlDeactDate() != null && tblChildInfo.getChlDeactDate().trim().length() > 0) || user.getIsDeactivated() == 1)) {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.userdeactivated), Toast.LENGTH_LONG).show();
                        }else {
                            Intent nextScreen = new Intent(ViewParentDetails.this, ChildHomeVisit.class);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                            nextScreen.putExtra("tblChildInfo", tblChildInfo);
                            startActivity(nextScreen);
                        }

                        break;
                    }

                    case 6: {
                        Intent nextScreen = new Intent(ViewParentDetails.this, ChildHomeVisitHistory.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                        nextScreen.putExtra("tblChildInfo", tblChildInfo);
                        startActivity(nextScreen);

                        break;
                    }

                }

            } catch (Exception e) {


                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            }
        }
    }

    private void disableEnableControls(boolean enable, ViewGroup vg){
        for (int i = 0; i < vg.getChildCount(); i++){
            View child = vg.getChildAt(i);
            child.setEnabled(enable);
            if (child instanceof ViewGroup){
                disableEnableControls(enable, (ViewGroup)child);
            }
        }
    }

}
