package com.sc.stmansi.childregistration;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.About.AboutActivity;
import com.sc.stmansi.HomeVisit.PncHomeVisit;
import com.sc.stmansi.R;
import com.sc.stmansi.childgrowthmonitor.CGMList;
import com.sc.stmansi.childhomevisit.ChildHomeVisit;
import com.sc.stmansi.childhomevisit.ChildHomeVisitHistory;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.deactivate.ChildDeactivateActivity;
import com.sc.stmansi.immunization.ImmunizationListActivity;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.pendingservices.IndicatorViewClickListener;
import com.sc.stmansi.quickAction.ActionItem;
import com.sc.stmansi.quickAction.QuickAction;
import com.sc.stmansi.recyclerview.ChildAdapter;
import com.sc.stmansi.recyclerview.ClickListener;
import com.sc.stmansi.recyclerview.RecyclerViewMargin;
import com.sc.stmansi.repositories.ChildRepository;
import com.sc.stmansi.repositories.FacilityRepository;
import com.sc.stmansi.repositories.LoginRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.tables.TblChildInfo;
import com.sc.stmansi.tables.TblChlParentDetails;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.viewprofile.ViewProfileActivity;
import com.sc.stmansi.womanlist.CareType;
import com.sc.stmansi.womanlist.QueryParams;
import com.sc.stmansi.womanlist.RegisteredWomenActionTabs;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.sc.stmansi.R;

public class RegisteredChildList extends AppCompatActivity implements ClickListener,
        IndicatorViewClickListener {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    AQuery aqChildList;
    private AppState appState;
    private DatabaseHelper databaseHelper;
    List<TblChildInfo> childList;

    //Manikanta 10feb2021
    List<TblChlParentDetails> childparentdetails;
    private List<TblChlParentDetails> parentsname;

    private static final int ID_CEDIT = 1;
    private static final int ID_IMMU = 2;
    QuickAction mQuickAction = null;
    TblChildInfo tblChildInfo;
    private String wFilterStr = "";
    private CareType careType;
    private Map<String, Integer> villageCodeForName;
    private boolean isUpdating;
    private List<TblChildInfo> filteredRowItems;
    private RecyclerView.Adapter  registeredWomanAdapter;
    private int nextItemPosition;
    private Spinner villageSpinner;
    private boolean spinnerSelectionChanged = false;
    static String strFromDate;
    static String strToDate;
    static boolean fromDate;
    EditText filterTextView;
    private static final int ID_ADDSIBLING = 3;
    private static final int ID_VIEWPAR = 4;
    String strWomanId;
    private static final int ID_CDEACT = 5;

    String cond;
    //Manikanta 15 april 2021
    private static final int ID_CDM = 6;
    private static final int ID_CHLHV = 7;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registeredchildlist);

        try {

            Bundle bundle = getIntent().getBundleExtra("globalState");
            appState = bundle.getParcelable("appState");

            strWomanId = (String) getIntent().getSerializableExtra("womanId");

            cond = (String) getIntent().getSerializableExtra("cond");

            databaseHelper = getHelper();


            aqChildList = new AQuery(this);

            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(true);



//            aqChildList.id(R.id.spndatefilter).itemSelected(this);






            recyclerView = (RecyclerView) aqChildList.id(R.id.child_recycler_view).getView();

            recyclerView.setHasFixedSize(true);
            // use a linear layout manager
            layoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(layoutManager);
            RecyclerViewMargin decoration = new RecyclerViewMargin(1, 1);
            recyclerView.addItemDecoration(decoration);
            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                    DividerItemDecoration.VERTICAL);
            DividerItemDecoration dividerItemDecoration1 = new DividerItemDecoration(recyclerView.getContext(),
                    DividerItemDecoration.HORIZONTAL);
            recyclerView.addItemDecoration(dividerItemDecoration);
            recyclerView.addItemDecoration(dividerItemDecoration1);



            QueryParams queryParams = new QueryParams();
            queryParams.selectedVillageCode = 0;
            queryParams.careType = CareType.GENERAL;
            queryParams.nextItemPosition = 0;
            queryParams.textViewFilter = "";
            queryParams.userId = appState.sessionUserId;
            queryParams.selectedDateFilter = 0;
            queryParams.fromDate = strFromDate;
            queryParams.toDate = strToDate;
            queryParams.womanId = strWomanId;
//          25Nov2019 Arpitha
            if(cond!=null && cond.trim().length()>0 && cond.equalsIgnoreCase("deact")) {
                queryParams.isDeactivated = true;
                aqChildList.id(R.id.txtlabell).text(getResources().getString(R.string.deactivatelist));
            }
            else
                queryParams.isDeactivated = false;

            filteredRowItems  = new ArrayList<>();
            filteredRowItems = prepareRowItemsForDisplay(queryParams);
            isUpdating = true;

            registeredWomanAdapter = new ChildAdapter( this,filteredRowItems);
            recyclerView.setAdapter(registeredWomanAdapter);
            updateLabels();
            registeredWomanAdapter.notifyItemRangeInserted(0, filteredRowItems.size());
            nextItemPosition = filteredRowItems.size() + 1;

//            recyclerView.addOnScrollListener(new OnRecyclerScrollListener());
            isUpdating = false;


//            getData();


            String str = getResources().getString(R.string.viewprofile);
            ActionItem cEdit = new ActionItem(ID_CEDIT, str, ContextCompat.getDrawable(this,R.drawable.ic_edit_60));

            str = getResources().getString(R.string.sp_imm);
            ActionItem cImmunization = new ActionItem(ID_IMMU, str, ContextCompat.getDrawable(this,R.drawable.immunisation));


            str = getResources().getString(R.string.addsibling);
            ActionItem cAddSibling = new ActionItem(ID_ADDSIBLING, str,
                    ContextCompat.getDrawable(this,R.drawable.general_examination_baby));

            str = getResources().getString(R.string.viewparentdetails);
            ActionItem cViewPar = new ActionItem(ID_VIEWPAR, str,
                    ContextCompat.getDrawable(this,R.drawable.ic_mother));



//            //Mani 15 april 2021
            str= getResources().getString(R.string.cgm);
            ActionItem CDM=new ActionItem(ID_CDM, str,
                    ContextCompat.getDrawable(this,R.drawable.child_growth));

            str = getResources().getString(R.string.chlhv);
            ActionItem CHLHV = new ActionItem(ID_CHLHV, str,
                    ContextCompat.getDrawable(this, R.drawable.ic_homevisit));

            str = getResources().getString(R.string.deactivate);
            ActionItem cDeact = new ActionItem(ID_CDEACT, str,
                    ContextCompat.getDrawable(this,R.drawable.deactivate));

            mQuickAction = new QuickAction(this);

            mQuickAction.addActionItem(cEdit);
            mQuickAction.addActionItem(cImmunization);
            mQuickAction.addActionItem(cAddSibling);
            mQuickAction.addActionItem(cViewPar);
            //Mani 15 april 2021
            mQuickAction.addActionItem(CDM);
            mQuickAction.addActionItem(CHLHV);
            mQuickAction.addActionItem(cDeact); //15May2021 Bindu - rearrange the position

            //setup the action item click listener
            mQuickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
                @Override
                public void onItemClick(QuickAction quickAction, int pos, int actionId) {
                    ActionItem actionItem = quickAction.getActionItem(pos);
                    try {
                        switch (actionItem.getActionId()) {
                            case ID_CEDIT: {
                                if(tblChildInfo.getChlReg()==1) {
                                    Intent nextScreen = new Intent(RegisteredChildList.this, EditRegChildInfoActivity.class);
                                    nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                    nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                                    nextScreen.putExtra("tblChildInfo", tblChildInfo);
                                    startActivity(nextScreen);
                                }else
                                {
                                    Intent nextScreen = new Intent(RegisteredChildList.this, EditChildActivity.class);
                                    nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                    nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                                    nextScreen.putExtra("tblChildInfo", tblChildInfo);
                                    startActivity(nextScreen);
                                }
                                mQuickAction.dismiss();
                                break;
                            }case ID_IMMU: {
                                if(tblChildInfo.getChlDeliveryResult()<=1) {
                                    Intent nextScreen = new Intent(RegisteredChildList.this, ImmunizationListActivity.class);
                                    nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                    nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                                    nextScreen.putExtra("tblChildInfo", tblChildInfo);
                                    startActivity(nextScreen);
                                }else
                                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.m053),Toast.LENGTH_LONG).show();
                                mQuickAction.dismiss();
                                break;
                            }case ID_ADDSIBLING: {
//                                26Nov2019 Arpitha
                                UserRepository userRepository = new UserRepository(databaseHelper);
                                TblInstusers user = userRepository.getOneAuditedUser(appState.ashaId);
                                if(user.getIsDeactivated()!=1) {
                                    Intent nextScreen = new Intent(RegisteredChildList.this, AddSiblingsActivity.class);
                                    nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                    nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                                    //if (tblChildInfo.getChlReg() == 1)17Jun2021 Arpitha
                                       String strParentId = tblChildInfo.getChlParentId();//17Jun2021 Arpitha
                                       if(strParentId != null && strParentId.trim().length() > 0)//17Jun2021 Arpitha
                                        nextScreen.putExtra("chlParentId", tblChildInfo.getChlParentId());
                                    else
                                        nextScreen.putExtra("WomanId", tblChildInfo.getWomanId());

                                    startActivity(nextScreen);
                                }else
                                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.userdeactivated),Toast.LENGTH_LONG).show();
                                 mQuickAction.dismiss();
                                break;
                            }
                            case ID_VIEWPAR: {
                                if (tblChildInfo.getChlParentId() != null && tblChildInfo.getChlParentId().trim().length() > 0) {
                                    Intent nextScreen = new Intent(RegisteredChildList.this, ViewParentDetails.class);
                                    nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                    nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                                    nextScreen.putExtra("tblChildInfo", tblChildInfo);
                                    startActivity(nextScreen);
                                }else {
                                    WomanRepository womanRepository = new WomanRepository(databaseHelper);
                                    tblregisteredwomen woman = womanRepository.getRegistartionDetails(tblChildInfo.getWomanId(),tblChildInfo.getUserId());
                                    appState.selectedWomanId = tblChildInfo.getWomanId();
                                    Intent nextScreen = new Intent(RegisteredChildList.this, ViewProfileActivity.class);
                                    nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                    nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                                    nextScreen.putExtra("woman", woman);
                                    startActivity(nextScreen);
//                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.ntapplicable), Toast.LENGTH_LONG).show();
                                }
                                mQuickAction.dismiss();
                                break;
                            }
                            case ID_CDEACT: {
                                    Intent nextScreen = new Intent(RegisteredChildList.this, ChildDeactivateActivity.class);
                                    nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                    nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                                    nextScreen.putExtra("tblChildInfo", tblChildInfo);
                                    startActivity(nextScreen);
                                mQuickAction.dismiss();
                                break;
                            }
//                            //manikanta 15april2021
                            case ID_CDM: {
                                Intent nextScreen = new Intent(RegisteredChildList.this, CGMList.class);
                                nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                                nextScreen.putExtra("tblChildInfo", tblChildInfo);
                                startActivity(nextScreen);
                                mQuickAction.dismiss();
                                break;
                            }
                            case ID_CHLHV: {

                                    Intent nextScreen = new Intent(RegisteredChildList.this, ChildHomeVisit.class);
                                    nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                    nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                                    nextScreen.putExtra("tblChildInfo", tblChildInfo);
                                    startActivity(nextScreen);
                                    mQuickAction.dismiss();

                                break;
                            }

                        }
                    }catch(Exception e){
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                    }
                }
            });

            mQuickAction.setOnDismissListener(new QuickAction.OnDismissListener() {
                @Override
                public void onDismiss() {
                }
            });



             filterTextView = aqChildList.id(R.id.etWomanFilter).getEditText();
            filterTextView.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
//                    aqChildList.id(R.id.imgresetfilter).getImageView().setBackgroundResource(R.drawable.ic_reset);
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            filterTextView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (v.getText() != null) {
                        aqChildList.id(R.id.imgresetfilter).getImageView().setBackgroundResource(R.drawable.ic_reset); //10Dec2019 - Bindu
                        wFilterStr = v.getText().toString();
                        updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr);
                    }
                    return true;
                }
            });



            aqChildList.id(R.id.imgresetfilter).getImageView().setOnClickListener(new FilterResetListener());


            aqChildList.id(R.id.txtfromdate).getTextView().setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (MotionEvent.ACTION_UP == event.getAction()) {
                      fromDate = true;
                        DialogFragment newFragment = new DatePickerFragment(aqChildList.id(R.id.txtfromdate));
                        newFragment.show(getSupportFragmentManager(), "datePicker");

                    }
                    return true;
                }
            });

            aqChildList.id(R.id.txttodate).getTextView().setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (MotionEvent.ACTION_UP == event.getAction()) {
                        fromDate = false;
                        DialogFragment newFragment = new DatePickerFragment(aqChildList.id(R.id.txttodate));
                        newFragment.show(getSupportFragmentManager(), "datePicker");

                    }
                    return true;
                }
            });

            aqChildList.id(R.id.btnok).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(aqChildList.id(R.id.spndatefilter).getSelectedItemPosition()==2
                            && (strFromDate.length()<=0|| strToDate.length()<=0))
                    {
                       displayAlert(getResources().getString(R.string.selectdate),null);
                    }else {
                        wFilterStr = filterTextView.getText().toString();
                        updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr);
                    }


                }
            });


            if(strWomanId!=null && strWomanId.trim().length()>0)
            {
                aqChildList.id(R.id.llWomanFilter).gone();
                aqChildList.id(R.id.llFilter).gone();
                getSupportActionBar().setCustomView(View.GONE);
            }else
            {
                getSupportActionBar().setCustomView(R.layout.ejanani_village_filter);
                getSupportActionBar().setDisplayShowCustomEnabled(true);
                villageSpinner = aqChildList.id(R.id.spVillageFilter).getSpinner();
//                villageSpinner.setOnItemSelectedListener(this);
                populateSpinnerVillage();
            }


//            01May2021 Arpitha
            aqChildList.id(R.id.spVillageFilter).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                   /* if(appState.selectedVillageTitle!=null && !appState.selectedVillageTitle.equalsIgnoreCase((String) adapterView.getItemAtPosition(i)))
                        spinnerSelectionChanged = true;*/
                    appState.selectedVillageTitle = (String) adapterView.getItemAtPosition(i);
                    if (spinnerSelectionChanged) {
                        updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr);
//                        updateList( aqChildList.id(R.id.spVillageFilter).getSpinner().getSelectedItem().toString(), villageCodeForName, wFilterStr);

                    }
                    spinnerSelectionChanged = true;
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            //            01May2021 Arpitha
            aqChildList.id(R.id.spndatefilter).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    strFromDate = "";
                    strToDate = DateTimeUtil.getTodaysDate();
                    if(i==2) {

                        strToDate = DateTimeUtil.getTodaysDate();
                        aqChildList.id(R.id.txtfromdate).visible();
                        aqChildList.id(R.id.txttodate).visible();
                        aqChildList.id(R.id.btnok).visible();//01May2021 Arpitha

                        aqChildList.id(R.id.txtfromdate).getEditText().setHint(getResources().getString(R.string.fromdate));
                        aqChildList.id(R.id.txttodate).text(DateTimeUtil.getTodaysDate());

                    }else
                    {
                        aqChildList.id(R.id.txtfromdate).gone();
                        aqChildList.id(R.id.txtfromdate).text("");
                        aqChildList.id(R.id.txttodate).gone();
                        aqChildList.id(R.id.btnok).gone();//01May2021 Arpitha
                        aqChildList.id(R.id.txttodate).text(DateTimeUtil.getTodaysDate());
                        updateList(aqChildList.id(R.id.spVillageFilter).getSpinner().getSelectedItem().toString(), villageCodeForName, wFilterStr);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });


        }catch (Exception e)
        {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }


   /* public void getData() throws SQLException {
        ChildRepository childRepository = new ChildRepository(databaseHelper);
         childList = childRepository.getChildData(queryParams);
        if(childList!=null && childList.size()>0)
        {
             registeredWomanAdapter = new
                    ChildAdapter(RegisteredChildList.this,childList);
            recyclerView.setAdapter(registeredWomanAdapter);
            aqChildList.id(R.id.txtnodata).gone();
            recyclerView.setVisibility(View.VISIBLE);
        }
        else
        {
            aqChildList.id(R.id.txtnodata).visible();
            recyclerView.setVisibility(View.GONE);
        }

        aqChildList.id(R.id.txtcountwl).text(getResources().getString(R.string.recordcount,
                childList.size()));

    }*/

    @Override
    public void onClick(View v) {
        setSelectedChildId(v);
        mQuickAction.show(v);
    }

    @Override
    public boolean onLongClick(View v) {
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)

    {
        getMenuInflater().inflate(R.menu.servicesmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId())
        {
            case R.id.logout: {
                Intent goToScreen = new Intent(RegisteredChildList.this, LoginActivity.class);
                displayAlert(getResources().getString(R.string.m111), goToScreen);
                return true;
            }
            case R.id.home: {
                Intent home = new Intent(RegisteredChildList.this, MainMenuActivity.class);
                home.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                startActivity(home);
                return true;
            }
            case R.id.wlist: {
                Intent goToScreen = new Intent(RegisteredChildList.this, RegisteredWomenActionTabs.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }case R.id.about: {
            Intent goToScreen = new Intent(RegisteredChildList.this, AboutActivity.class);
            goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
            displayAlert(getResources().getString(R.string.exit), goToScreen);
            return true;
        }
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    /**
     * Save Confirmation Alert
     *
     * @param goToScreen
     * @param spanText2
     */
    private void displayAlert(final String spanText2, final Intent goToScreen) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        String strmess = "";

        if(goToScreen!=null)
            strmess = getResources().getString(R.string.yes);
                    else
            strmess = getResources().getString(R.string.ok);

        alertDialogBuilder.setMessage(spanText2).setCancelable(false).setNegativeButton(
                strmess, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (goToScreen != null) {

                            goToScreen.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                            startActivity(goToScreen);
                            if (spanText2.contains("logout")) {
                                try {
                                    LoginRepository loginRepository = new LoginRepository(databaseHelper);
                                    loginRepository.updateLogoutTime(appState.loginAuditId, appState.sessionUserId);
                                } catch (Exception e) {
                                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                                }
                            }
                        } else
                            dialog.cancel();
                    }
                });

        if(goToScreen!=null) {
            alertDialogBuilder.setPositiveButton(getResources().getString(R.string.m122),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
        }
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }
    //    initialize db object
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void onBackPressed() {

    }


    private void setSelectedChildId(View v) {
        int itemPosition = recyclerView.getChildLayoutPosition(v);
        if(itemPosition>=0) {
            tblChildInfo = childList.get(itemPosition);

            appState.selectedWomanId = tblChildInfo.getChildID();
        }
    }

    //    upadate list
    public void updateList(String villageTitle, Map<String, Integer> villageNameForCode, String textViewFilter) {
        try {
            QueryParams queryParams = new QueryParams();
            queryParams.nextItemPosition = 0;
            queryParams.selectedVillageTitle = villageTitle;
            queryParams.textViewFilter = textViewFilter;
            queryParams.careType = this.careType;
            queryParams.noVisitSelected = aqChildList.id(R.id.chknovisitnotdone).isChecked();
            queryParams.userId = appState.sessionUserId;
            queryParams.selectedDateFilter = aqChildList.id(R.id.spndatefilter).getSelectedItemPosition();

            this.wFilterStr = textViewFilter;
            queryParams.fromDate = strFromDate;
            queryParams.toDate = strToDate;

            //          25Nov2019
            if(cond!=null && cond.trim().length()>0 && cond.equalsIgnoreCase("deact"))
                queryParams.isDeactivated = true;
            else
                queryParams.isDeactivated = false;


            new LoadPregnantList(this, queryParams, true, villageNameForCode).execute();
            isUpdating = true;
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

   /* @Override
    public void displayAlert(int selectedItemPosition) {
        ArrayList<String> compList;
        String complication = filteredRowItems.get(selectedItemPosition)
                .getChlComplications();
        if (complication != null && complication.length() > 0
                || (filteredRowItems.get(selectedItemPosition).getChlReg()==0 && filteredRowItems.get(selectedItemPosition).getDelBabyWeight()!=null
                && filteredRowItems.get(selectedItemPosition).getDelBabyWeight().trim().length()>0
                && Double.parseDouble(filteredRowItems.get(selectedItemPosition).getDelBabyWeight())<2500) ) {
            compList = new ArrayList<>();
           // compList.add(complication.replace(",", ", ").replace("PH:", "\nPH:"));


           String[] chlCompl;

           if(filteredRowItems.get(selectedItemPosition).getChlReg()==1)
            chlCompl = getResources().getStringArray(R.array.childComplications);
           else
               chlCompl = getResources().getStringArray(R.array.newborncompl);

           String[] selectedCompl = complication.split(",");

            ArrayList<String> complL = new ArrayList<>();

           for(int i=1;i<=selectedCompl.length;i++)
           {
               if(selectedCompl[i-1]!=null && selectedCompl[i-1].trim().length()>0 && !selectedCompl[i-1].equalsIgnoreCase("0"))
              complL.add(chlCompl[Integer.parseInt(selectedCompl[i-1].trim())]);
           }

           if(filteredRowItems.get(selectedItemPosition).getChlReg()==0 && filteredRowItems.get(selectedItemPosition).getDelBabyWeight()!=null
                   && filteredRowItems.get(selectedItemPosition).getDelBabyWeight().trim().length()>0
                   && Double.parseDouble(filteredRowItems.get(selectedItemPosition).getDelBabyWeight())<2500 )
            complL.add(getResources().getString(R.string.weightlessthan2500gram));



            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            View convertView = LayoutInflater.from(this).inflate(R.layout.complicated_reasons_list, null, false);

            ListView lv = convertView.findViewById(R.id.lvCompliatedReasonslist);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.listitem, complL);

            alertDialog.setView(convertView).setCancelable(true).setTitle((this.getResources().getString(R.string.complications)))
                    .setPositiveButton((this.getResources().getString(R.string.ok)), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
            lv.setAdapter(adapter);
            alertDialog.show();
        }
    }
*/
    @Override
    public void displayHomeVisitList(int selectedItemPosition) {

    }

    @Override
    public void displayDelComplicationAlert(int selectedItemPosition) {

    }


    //    load list data
    static class LoadPregnantList extends AsyncTask<Void, Void, Void> {

        private WeakReference<RegisteredChildList> weakReference;
        private QueryParams queryParams;
        private boolean firstLoad;
        private Map<String, Integer> villageCodeForName;

        LoadPregnantList(RegisteredChildList viewsUpdateCallback, QueryParams queryParams, boolean firstLoad, Map<String, Integer> villageCodeForName) {
            this.weakReference = new WeakReference<>(viewsUpdateCallback);
            this.queryParams = queryParams;
            this.firstLoad = firstLoad;
            this.villageCodeForName = villageCodeForName;
        }

        LoadPregnantList(RegisteredChildList viewsUpdateCallback, QueryParams queryParams, boolean firstLoad) {
            this.weakReference = new WeakReference<>(viewsUpdateCallback);
            this.queryParams = queryParams;
            this.firstLoad = firstLoad;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            int selectedCode;
            // This means second constructor was used (called by scrollListener)
            if (villageCodeForName == null) selectedCode = queryParams.selectedVillageCode;
            else {
                String selectedVillageTitle = queryParams.selectedVillageTitle;
                if (selectedVillageTitle == null)
                    selectedCode = 0;
                else {
                    Integer code = villageCodeForName.get(selectedVillageTitle);
                    selectedCode = (code == null) ? 0 : code;
                }
            }
            try {
                RegisteredChildList activity = weakReference.get();
                queryParams.selectedVillageCode = selectedCode;
                List<TblChildInfo> rows = activity.prepareRowItemsForDisplay(queryParams);
                if (firstLoad) {
                    int prevSize = activity.filteredRowItems.size();
                    activity.filteredRowItems.clear();
                    activity.registeredWomanAdapter.notifyItemRangeRemoved(0, prevSize);
                    firstLoad = false;
                }
                if (rows.size() > 0) {
                    activity.filteredRowItems.addAll(rows);
                    activity.nextItemPosition = activity.filteredRowItems.size() + 1;
                    if (rows.size() > 0) {
                        activity.registeredWomanAdapter.notifyItemRangeInserted(firstLoad ? 0 : activity.nextItemPosition,
                                activity.filteredRowItems.size());
                    }
                }
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            try {
                RegisteredChildList activity = weakReference.get();
                activity.updateLabels();
                if (!firstLoad && activity.filteredRowItems.size() > 0)
                    activity.layoutManager.scrollToPosition(activity.registeredWomanAdapter.getItemCount());
                activity.isUpdating = false;
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

            }
        }
    }

    class FilterResetListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            aqChildList.id(R.id.imgresetfilter).getImageView().setBackgroundResource(R.drawable.ic_reset_gray);
            aqChildList.id(R.id.etWomanFilter).text("");
            wFilterStr = "";
            CareType careType = CareType.GENERAL;
            villageSpinner.setSelection(0);
            aqChildList.id(R.id.spndatefilter).setSelection(0);
            strFromDate = "";
            strToDate= "";
            updateList("", villageCodeForName, wFilterStr);
        }
    }

    //   data to display
    private List<TblChildInfo> prepareRowItemsForDisplay(QueryParams queryParams) throws Exception {

        ChildRepository childRepository = new ChildRepository(databaseHelper);
        childList = childRepository.getChildList(queryParams);
        return childList;
    }

    //    update labels based on data
    private void updateLabels() {
        if (filteredRowItems.size() == 0) {
            aqChildList.id(R.id.txtcountwl).text(getResources().getString(R.string.recordcount,
                    filteredRowItems.size()));

            aqChildList.id(R.id.txtnodata).text(getResources().getString(R.string.no_data));
            aqChildList.id(R.id.txtnodata).visible();
            return;
        }
        aqChildList.id(R.id.txtnodata).gone();
        aqChildList.id(R.id.txtcountwl).text(getResources().getString(R.string.recordcount,
                filteredRowItems.size()));
        aqChildList.id(R.id.txtcountwl).visible();
    }

   /* @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        *//*if(view.getId() == villageSpinner.getId()) {
            appState.selectedVillageTitle = (String) parent.getItemAtPosition(position);
            if (spinnerSelectionChanged) {
                updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr);
            }
            spinnerSelectionChanged = true;
        }*//*

        switch (parent.getId())
        {
            case R.id.spVillageFilter:
                appState.selectedVillageTitle = (String) parent.getItemAtPosition(position);
                if (spinnerSelectionChanged) {
                    updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr);
                }
                spinnerSelectionChanged = true;
                break;

           case R.id.spndatefilter:
               strFromDate = "";
               strToDate = DateTimeUtil.getTodaysDate();
               if(position==2) {

                   strToDate = DateTimeUtil.getTodaysDate();
                   aqChildList.id(R.id.txtfromdate).visible();
                   aqChildList.id(R.id.txttodate).visible();
                   aqChildList.id(R.id.txtfromdate).getEditText().setHint(getResources().getString(R.string.fromdate));
                   aqChildList.id(R.id.txttodate).text(DateTimeUtil.getTodaysDate());

               }else
               {
                   aqChildList.id(R.id.txtfromdate).gone();
                   aqChildList.id(R.id.txtfromdate).text("");
                   aqChildList.id(R.id.txttodate).gone();
                   aqChildList.id(R.id.txttodate).text(DateTimeUtil.getTodaysDate());
                   updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr);
               }
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
*/
    /**
     * populate Village to Spinner
     */
    private void populateSpinnerVillage() throws Exception {
        FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
        villageCodeForName = facilityRepository.getPlaceMap();

        ArrayAdapter<CharSequence> villageSpinnerAdapter = new ArrayAdapter<>(this, R.layout.actionbar_spinner_list_item);
//        villageSpinnerAdapter.add("All");Arpitha 06May2021
        villageSpinnerAdapter.add(getResources().getString(R.string.all));//06May2021 Arpitha
        for (Map.Entry<String, Integer> village : villageCodeForName.entrySet()) {
            villageSpinnerAdapter.add(village.getKey());
        }
        villageSpinnerAdapter.setDropDownViewResource(R.layout.actionbar_spinner_layout_view);
        villageSpinner.setAdapter(villageSpinnerAdapter);
    }


    @SuppressLint("SimpleDateFormat")
    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        private AQuery aqPMSel;
        private tblregisteredwomen woman;

        public DatePickerFragment() {
        }

        public DatePickerFragment(AQuery aQuery) {
            aqPMSel = aQuery;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            int year = 0, month = 0, day = 0;
            try {

                Calendar c = Calendar.getInstance();
//                String defaultDate = DateTimeUtil.getTodaysDate(); //09Aug2019 - Cal set date of Provided date
                String defaultDate = aqPMSel.getText().toString(); //09Aug2019 - Cal set date of Provided date

                if (defaultDate.length() > 0) {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    Date sendDate = sdf.parse(defaultDate);
                    c.setTime(sendDate);
                    year = c.get(Calendar.YEAR);
                    month = c.get(Calendar.MONTH);
                    day = c.get(Calendar.DAY_OF_MONTH);
                } else {
                    c.setTime(new Date());
                    year = c.get(Calendar.YEAR);
                    month = c.get(Calendar.MONTH);
                    day = c.get(Calendar.DAY_OF_MONTH);
                }
            } catch (Exception e) {


                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            }

            // Create a new instance of DatePickerDialog and return it
            DatePickerDialog dpDialog;
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                // Do something for lollipop and above versions
                dpDialog = new DatePickerDialog(getActivity(), R.style.DialogTheme, this, year, month, day);
            } else {
                // do something for phones running an SDK before lollipop
                dpDialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, this, year, month, day);
                dpDialog.setTitle("Set a Date");
                dpDialog.getDatePicker().setCalendarViewShown(false);
            }
            return dpDialog;
        }

        @Override
        public void onDateSet(DatePicker view, int year, int month, int day) {
            if (view.isShown()) {
                try {
                    String SelectedDate = String.format("%02d", day) + "-" +
                            String.format("%02d", month + 1) + "-"
                            + year;
//                    Date lmpDate = null;
                    SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                    Date seldate = format.parse(SelectedDate);
                    Date fromdate = null;
                    Date toDate = null;

                    if(strFromDate!=null && strFromDate.trim().length()>0)
                    fromdate = format.parse(strFromDate);

                    if(strToDate!=null && strToDate.trim().length()>0)
                        toDate = format.parse(strToDate);
//
                    if(fromDate)
                    {
                        if(seldate.after(new Date())) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.date_cannot_af_currentdate), Toast.LENGTH_LONG).show();
                            strFromDate = "";
//                            aqPMSel.id(R.id.txtfromdate).text(getResources().getString(R.string.fromdate));
                        }else if(toDate!=null && seldate.after(toDate)) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.fromdate_cannot_af_todate), Toast.LENGTH_LONG).show();
                            strToDate = "";
                            aqPMSel.id(R.id.txttodate).text(getResources().getString(R.string.todate));
                        }else {
                            strFromDate = SelectedDate;
                            aqPMSel.id(R.id.txtfromdate).text(strFromDate);
                        }
                    }else {

                        if(seldate.after(new Date())) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.date_cannot_af_currentdate), Toast.LENGTH_LONG).show();
                            strToDate = "";
//                            aqPMSel.id(R.id.txttodate).text(getResources().getString(R.string.todate));
                        }else if(fromdate!=null && seldate.before(fromdate)) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.todate_cannot_bf_fromdate), Toast.LENGTH_LONG).show();
                            strToDate = "";
                            aqPMSel.id(R.id.txttodate).text(getResources().getString(R.string.todate));
                        }
                        else {
                            strToDate = SelectedDate;
                            aqPMSel.id(R.id.txttodate).text(strToDate);
                        }
                    }

                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

                }
            }
        }
    }

    @Override
    public void displayAlert(int selectedItemPosition) {
        ArrayList<String> compList;
        String complication = filteredRowItems.get(selectedItemPosition)
                .getChlComplications();
        if (complication != null && complication.length() > 0
                || (filteredRowItems.get(selectedItemPosition).getChlReg()==0 && filteredRowItems.get(selectedItemPosition).getDelBabyWeight()!=null
                && filteredRowItems.get(selectedItemPosition).getDelBabyWeight().trim().length()>0
                && Double.parseDouble(filteredRowItems.get(selectedItemPosition).getDelBabyWeight())<2500) ) {
            compList = new ArrayList<>();
            // compList.add(complication.replace(",", ", ").replace("PH:", "\nPH:"));


            String[] chlCompl;

            if(filteredRowItems.get(selectedItemPosition).getChlReg()==1)
                chlCompl = getResources().getStringArray(R.array.childComplications);
            else
                chlCompl = getResources().getStringArray(R.array.newborncompl);

            String[] selectedCompl = complication.split(",");

            ArrayList<String> complL = new ArrayList<>();

            for(int i=1;i<=selectedCompl.length;i++)
            {
                if(selectedCompl[i-1]!=null && selectedCompl[i-1].trim().length()>0 && !selectedCompl[i-1].equalsIgnoreCase("0"))
                    complL.add(chlCompl[Integer.parseInt(selectedCompl[i-1].trim())]);
            }

            if(filteredRowItems.get(selectedItemPosition).getChlReg()==0 && filteredRowItems.get(selectedItemPosition).getDelBabyWeight()!=null
                    && filteredRowItems.get(selectedItemPosition).getDelBabyWeight().trim().length()>0
                    && Double.parseDouble(filteredRowItems.get(selectedItemPosition).getDelBabyWeight())<2500 )
                complL.add(getResources().getString(R.string.weightlessthan2500gram));


            if(filteredRowItems.get(selectedItemPosition).getChlOtherComplications()!=null
                    && filteredRowItems.get(selectedItemPosition).getChlOtherComplications().trim().length()>0 &&
            !(filteredRowItems.get(selectedItemPosition).getChlOtherComplications().equalsIgnoreCase("null")))//03May21 Arpitha
            complL.add(filteredRowItems.get(selectedItemPosition).getChlOtherComplications());//01May21 Arpitha

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            View convertView = LayoutInflater.from(this).inflate(R.layout.complicated_reasons_list, null, false);

            ListView lv = convertView.findViewById(R.id.lvCompliatedReasonslist);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.listitem, complL);

            alertDialog.setView(convertView).setCancelable(true).setTitle((this.getResources().getString(R.string.complications)))
                    .setPositiveButton((this.getResources().getString(R.string.ok)), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
            lv.setAdapter(adapter);
            alertDialog.show();
        }
    }

    @Override
    public void displayLMPConfirmation(int selectedItemPosition) {

    }
}
