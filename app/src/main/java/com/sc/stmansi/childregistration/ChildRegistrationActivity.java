//03Oct2019 Arpitha
package com.sc.stmansi.childregistration;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.text.method.TextKeyListener;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.misc.TransactionManager;
import com.sc.stmansi.common.CommonDatePickerFragment;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.common.MultiSelectionSpinner;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.repositories.ChildRepository;
import com.sc.stmansi.repositories.DeliveryRepository;
import com.sc.stmansi.repositories.FacilityRepository;
import com.sc.stmansi.repositories.LoginRepository;
import com.sc.stmansi.repositories.TransactionHeaderRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.tables.TblChildInfo;
import com.sc.stmansi.tables.TblChlParentDetails;
import com.sc.stmansi.tables.TblDeliveryInfo;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.womanlist.RegisteredWomenActionTabs;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import com.sc.stmansi.R;


public class ChildRegistrationActivity extends AppCompatActivity implements
        View.OnClickListener,
        View.OnTouchListener, DatePickerDialog.OnDateSetListener {

   public static AQuery aqChildReg;
    LinearLayout linearChild;
    TableLayout tableLayout;
    int noOfChild = 0;
    TextView tvheading, tvdob;
    EditText etdob;
    private AppState appState;
    private DatabaseHelper databaseHelper;
    RadioGroup rgIdtype, rgRelation;
    EditText currentEditTextViewforDate;
    TblInstusers user;
    TblChildInfo tblChildInfo;
    tblregisteredwomen woman;
    String remarried, relation, strPhyDis, strSchool, strGender, strMentalDis;
    String idType;
    List<TblChildInfo> childList;
    TransactionHeaderRepository transactionHeaderRepository;
    int transId;
    List<tblregisteredwomen> tblregisteredwomenList;
    //    TblChildInfo tblChildInfoFromDb;
   /* String strDob1 = DateTimeUtil.getTodaysDate(),strDob2 = DateTimeUtil.getTodaysDate(),strDob3 = DateTimeUtil.getTodaysDate(),
        strDob4 = DateTimeUtil.getTodaysDate(),
        strDob5 = DateTimeUtil.getTodaysDate(),
        strDob6 = DateTimeUtil.getTodaysDate();*/
    String strDob1 = "", strDob2 = "", strDob3 = "",
            strDob4 = "",
            strDob5 = "",
            strDob6 = "";
    TblChlParentDetails tblChlParentDetails;
    Map<String, Integer> mapVillage;
    RadioGroup rgRemarried, rgDuringDel;
    int gramsId, kgsId;
    public  static MultiSelectionSpinner spnCompl;
    public  static TableRow trOtherChildCompl;
    public  static MultiSelectionSpinner spnCompl1,spnCompl2,spnCompl3,spnCompl4,spnCompl5,spnCompl6;
    public  static TableRow trOtherChildCompl1,trOtherChildCompl2,
            trOtherChildCompl3,trOtherChildCompl4,trOtherChildCompl5, trOtherChildCompl6;




    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_childinfo);
        try {
            Bundle bundle = getIntent().getBundleExtra("globalState");
            appState = bundle.getParcelable("appState");
//            tblChildInfoFromDb = (TblChildInfo) getIntent().getSerializableExtra("tblChildInfo");


            databaseHelper = getHelper();

            UserRepository userRepository = new UserRepository(databaseHelper);
            user = userRepository.getOneAuditedUser(appState.ashaId);

            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

            linearChild = findViewById(R.id.llchild);

            rgRelation = findViewById(R.id.rgrelation);
            rgIdtype = findViewById(R.id.rgidtype);

            rgDuringDel = findViewById(R.id.rgduringdel);
            rgRemarried = findViewById(R.id.rgfatherremarried);

            tableLayout = findViewById(R.id.tableLayout);

            aqChildReg = new AQuery(this);

            aqChildReg.id(R.id.spndeltype).itemSelected(this, "onSpinnerClicked");
            aqChildReg.id(R.id.spnnoofchild).itemSelected(this, "onSpinnerClicked");
            aqChildReg.id(R.id.spnwoman).itemSelected(this, "onSpinnerClicked");
            aqChildReg.id(R.id.spncasuseofdeathchl).itemSelected(this, "onSpinnerClicked");

            aqChildReg.id(R.id.rdrchid).getButton().setOnClickListener(this);
            aqChildReg.id(R.id.rdthayicard).getButton().setOnClickListener(this);
            aqChildReg.id(R.id.btnsave).getImageView().setOnClickListener(this);
            aqChildReg.id(R.id.etdeathdate).getEditText().setOnTouchListener(this);


            setSpinnerData();


            aqChildReg.id(R.id.etthaicard).getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    {
                        if (count > 0) {
                            if (aqChildReg.id(R.id.etthaicard).getText()
                                    .toString().trim().length() < 7)
                                aqChildReg.id(R.id.etthaicard).getEditText().
                                        setError(getResources().getString
                                                (R.string.thayicardno_mustcontain_sevendigits));


                        }
                    }

                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });



            aqChildReg.id(R.id.chlWeight1).getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    {
                        if (count > 0) {
                            if (aqChildReg.id(R.id.chlWeight1).getText()
                                    .toString().trim().length() < 3 && aqChildReg.id(R.id.rdgrams1).isChecked())
                                aqChildReg.id(R.id.chlWeight1).getEditText().
                                        setError(getResources().getString
                                                (R.string.weightmusthavethreedigits));
                            else  if (aqChildReg.id(R.id.chlWeight1).getText()
                                    .toString().trim().length() > 4 && aqChildReg.id(R.id.rdkgs1).isChecked())
                                aqChildReg.id(R.id.chlWeight1).getEditText().
                                        setError(getResources().getString
                                                (R.string.weightisabnormal));



                        }
                    }

                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            aqChildReg.id(R.id.chlWeight2).getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    {
                        if (count > 0) {
                            if (aqChildReg.id(R.id.chlWeight2).getText()
                                    .toString().trim().length() < 3 && aqChildReg.id(R.id.rdgrams2).isChecked())
                                aqChildReg.id(R.id.chlWeight2).getEditText().
                                        setError(getResources().getString
                                                (R.string.weightmusthavethreedigits));
                            else  if (aqChildReg.id(R.id.chlWeight2).getText()
                                    .toString().trim().length() > 4 && aqChildReg.id(R.id.rdkgs2).isChecked())
                                aqChildReg.id(R.id.chlWeight2).getEditText().
                                        setError(getResources().getString
                                                (R.string.weightisabnormal));



                        }
                    }

                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            aqChildReg.id(R.id.chlWeight3).getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    {
                        if (count > 0) {
                            if (aqChildReg.id(R.id.chlWeight3).getText()
                                    .toString().trim().length() < 3 && aqChildReg.id(R.id.rdgrams3).isChecked())
                                aqChildReg.id(R.id.chlWeight3).getEditText().
                                        setError(getResources().getString
                                                (R.string.weightmusthavethreedigits));
                            else  if (aqChildReg.id(R.id.chlWeight3).getText()
                                    .toString().trim().length() > 4 && aqChildReg.id(R.id.rdkgs3).isChecked())
                                aqChildReg.id(R.id.chlWeight3).getEditText().
                                        setError(getResources().getString
                                                (R.string.weightisabnormal));



                        }
                    }

                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            aqChildReg.id(R.id.chlWeight4).getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    {
                        if (count > 0) {
                            if (aqChildReg.id(R.id.chlWeight4).getText()
                                    .toString().trim().length() < 3 && aqChildReg.id(R.id.rdgrams4).isChecked())
                                aqChildReg.id(R.id.chlWeight4).getEditText().
                                        setError(getResources().getString
                                                (R.string.weightmusthavethreedigits));
                            else  if (aqChildReg.id(R.id.chlWeight4).getText()
                                    .toString().trim().length() > 4 && aqChildReg.id(R.id.rdkgs4).isChecked())
                                aqChildReg.id(R.id.chlWeight4).getEditText().
                                        setError(getResources().getString
                                                (R.string.weightisabnormal));



                        }
                    }

                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            aqChildReg.id(R.id.chlWeight5).getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    {
                        if (count > 0) {
                            if (aqChildReg.id(R.id.chlWeight5).getText()
                                    .toString().trim().length() < 3 && aqChildReg.id(R.id.rdgrams5).isChecked())
                                aqChildReg.id(R.id.chlWeight5).getEditText().
                                        setError(getResources().getString
                                                (R.string.weightmusthavethreedigits));
                            else  if (aqChildReg.id(R.id.chlWeight5).getText()
                                    .toString().trim().length() > 4 && aqChildReg.id(R.id.rdkgs5).isChecked())
                                aqChildReg.id(R.id.chlWeight5).getEditText().
                                        setError(getResources().getString
                                                (R.string.weightisabnormal));



                        }
                    }

                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            aqChildReg.id(R.id.chlWeight6).getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    {
                        if (count > 0) {
                            if (aqChildReg.id(R.id.chlWeight6).getText()
                                    .toString().trim().length() < 3 && aqChildReg.id(R.id.rdgrams6).isChecked())
                                aqChildReg.id(R.id.chlWeight6).getEditText().
                                        setError(getResources().getString
                                                (R.string.weightmusthavethreedigits));
                            else  if (aqChildReg.id(R.id.chlWeight6).getText()
                                    .toString().trim().length() > 4 && aqChildReg.id(R.id.rdkgs6).isChecked())
                                aqChildReg.id(R.id.chlWeight6).getEditText().
                                        setError(getResources().getString
                                                (R.string.weightisabnormal));



                        }
                    }

                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }

    }

    private void setSpinnerData() throws SQLException {

        WomanRepository womanRepository = new WomanRepository(databaseHelper);
        tblregisteredwomenList = womanRepository.getDisplayMotherList(user.getUserId());


        ArrayList<String> list = new ArrayList<>();

//     list.add(getResources().getString(R.string.select));
        list.add(getResources().getString(R.string.none));
        for (tblregisteredwomen regWoman : tblregisteredwomenList) {
            if (regWoman.getRegUIDNo() != null && regWoman.getRegUIDNo().trim().length() > 0)
                if (regWoman.getRegUIDType().equalsIgnoreCase("RCHID"))
                    list.add(regWoman.getRegWomanName() + ", RCHID: " + regWoman.getRegUIDNo());
                else
                    list.add(regWoman.getRegWomanName() + ", Thayi: " + regWoman.getRegUIDNo());

            else
                list.add(regWoman.getRegWomanName());

        }

        list.add(getResources().getString(R.string.select));

        ArrayAdapter adapter = new ArrayAdapter(this, R.layout.simple_spinner_dropdown_item, list);

        aqChildReg.id(R.id.spnwoman).getSpinner().setAdapter(adapter);

        int pos = list.size() - 1;
        aqChildReg.id(R.id.spnwoman).setSelection(pos);

    }

    /*private void getData() throws SQLException {
     *//* ChildRepository childRepository = new ChildRepository(databaseHelper);
        List<TblChildInfo> childList = childRepository.getChildData();*//*
        if (tblChildInfoFromDb != null) {

            aqChildReg.id(R.id.spnnoofchild).text(tblChildInfoFromDb.getChlBabyname());
        }
    }*/

    private void saveData() {
        try {
            setData();
            writeToDatabase();
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private void writeToDatabase() {
        try {

            final UserRepository userRepository = new UserRepository(databaseHelper);


            TransactionManager.callInTransaction(databaseHelper.getConnectionSource(), new Callable<Void>() {
                public Void call() throws Exception {
                    boolean added;
                    int addParent = 0;
                    RuntimeExceptionDao<TblChildInfo, Integer> childDAO = databaseHelper.getChildInfoRuntimeExceptionDao();
                    int add = 0;
                    Dao<TblChlParentDetails, Integer> chlParentDAO = databaseHelper.getChlParentRuntimeExceptionDao();

                    addParent = chlParentDAO.create(tblChlParentDetails);

                    for (int i = 0; i < childList.size(); i++) {
                        add = childDAO.create(childList.get(i));

                    }
                    if (add > 0 && addParent > 0) {
                        added = transactionHeaderRepository.iNewRecordTransNew(user.getUserId(), transId, childDAO.getTableName(), databaseHelper);
                        added = transactionHeaderRepository.iNewRecordTransNew(user.getUserId(), transId, chlParentDAO.getTableName(), databaseHelper);

                        userRepository.UpdateLastParentNumberNew(user.getUserId(), transId, databaseHelper);


                        //                            if (added)
//                                stradd = userRepository.UpdateLastWomenNumberNew(user.getUserId(), transId,databaseHelper);
                           /* if (stradd.trim().length() > 0) {
                                WomanServiceRepository womanServiceRepository = new WomanServiceRepository(databaseHelper);
                                added = womanServiceRepository.addServicesNew(woman, transId, appState.userType,databaseHelper);
                            }
*/
                        if (added) {

//                                if(woman.getRegPregnantorMother()==0) {
                            Toast.makeText(getApplicationContext(), getApplicationContext().getResources().getString(R.string.regsuccess), Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(ChildRegistrationActivity.this, RegisteredChildList.class);
                            intent.putExtra("appState", appState);
                            intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            intent.putExtra("wFilterStr", "");
                            startActivity(intent);
                               /* }else
                                {
                                    Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_LONG).show();
                                    Intent intent = new Intent(ChildRegistrationActivity.this, DeliveryInfoActivity.class);
                                    intent.putExtra("appState", appState);
                                    intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                    intent.putExtra("wFilterStr", "");
                                    startActivity(intent);

                                }*/
                            //MainMenuActivity.callSyncMtd();  // TODO 12Aug2019 - Bindu - Comment auto sync
                        } else
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.m091), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha

                    }
                    return null;
                }
            });
        } catch (Exception e) {

            Toast.makeText(getApplicationContext(), getResources().getString(R.string.m091), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }

    }

    /**
     * This method invokes when Item clicked in Spinner
     *
     * @throws Exception
     */
    public void onSpinnerClicked(AdapterView<?> adapter, View v, int position, long arg3) throws Exception {

        try {
            switch (adapter.getId()) {


                case R.id.spndeltype: {
                   /* if (aqChildReg.id(R.id.spndeltype).getSelectedItem().toString()
                            .equalsIgnoreCase("Other"))*/
                    if (aqChildReg.id(R.id.spndeltype).getSelectedItem().toString()
                            .equalsIgnoreCase(getResources().getString(R.string.other)))   //17Jun2021 Arpitha
                        aqChildReg.id(R.id.trdeltypeother).visible();
                    else
                        aqChildReg.id(R.id.trdeltypeother).gone();


                }
                break;
                case R.id.spnnoofchild: {
                    if (aqChildReg.id(R.id.spnnoofchild).getSpinner().getSelectedItemPosition() > 0) {
                        noOfChild = aqChildReg.id(R.id.spnnoofchild).getSpinner().getSelectedItemPosition();
                        aqChildReg.id(R.id.spnnoofchild).getSpinner().setEnabled(false);
                        drawUI();

                        aqChildReg.id(R.id.spntribalch1).itemSelected(this, "onSpinnerClicked");


                        aqChildReg.id(R.id.spndeltypechl1).itemSelected(this, "onSpinnerClicked");
                        aqChildReg.id(R.id.spndeltypechl3).itemSelected(this, "onSpinnerClicked");
                        aqChildReg.id(R.id.spndeltypechl2).itemSelected(this, "onSpinnerClicked");
                        aqChildReg.id(R.id.spndeltypechl4).itemSelected(this, "onSpinnerClicked");
                        aqChildReg.id(R.id.spndeltypechl5).itemSelected(this, "onSpinnerClicked");
                        aqChildReg.id(R.id.spndeltypechl6).itemSelected(this, "onSpinnerClicked");



                    }
                }
                break;
                case R.id.spnwoman:

                    aqChildReg.id(R.id.etname).text("");
                    aqChildReg.id(R.id.etage).text("");
                    aqChildReg.id(R.id.etthaicard).text("");
                    aqChildReg.id(R.id.rdrchid).checked(true);
                    aqChildReg.id(R.id.rdthayicard).checked(false);
                    aqChildReg.id(R.id.trrelation).gone();
                    rgRelation.clearCheck();
                    aqChildReg.id(R.id.trmotherdetails).gone();
                    aqChildReg.id(R.id.trage).visible();
                    aqChildReg.id(R.id.tridtype).visible();
                    aqChildReg.id(R.id.trthayicard).visible();
                    aqChildReg.id(R.id.chkmotherdeath).checked(false);
                    aqChildReg.id(R.id.tragewhenpassesaway).gone();
                    aqChildReg.id(R.id.trduringdel).gone();
                    aqChildReg.id(R.id.trdeathdate).gone();
                    aqChildReg.id(R.id.etdeathdate).text("");
                    aqChildReg.id(R.id.trcausefdeath).gone();
                    rgDuringDel.clearCheck();


                    if (aqChildReg.id(R.id.spnwoman).getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.none))) {
                        aqChildReg.id(R.id.trrelation).visible();

                        if (aqChildReg.id(R.id.spnwoman).getSelectedItemPosition() > 0) {
                            tblregisteredwomen woman = tblregisteredwomenList.get(aqChildReg.id(R.id.spnwoman).getSelectedItemPosition() - 1);

                            aqChildReg.id(R.id.etname).text("");
                            aqChildReg.id(R.id.etage).text("");
                            aqChildReg.id(R.id.etthaicard).text("");
                            aqChildReg.id(R.id.rdrchid).checked(true);
                            aqChildReg.id(R.id.rdthayicard).checked(false);
                            if (woman != null) {
                                aqChildReg.id(R.id.etname).text(woman.getRegWomanName());
                                aqChildReg.id(R.id.etage).text("" + woman.getRegAge());
                                aqChildReg.id(R.id.etthaicard).text(woman.getRegUIDNo());
                                if (woman.getRegUIDType().equalsIgnoreCase("RCHID"))
                                    aqChildReg.id(R.id.rdrchid).checked(true);
                                else
                                    aqChildReg.id(R.id.rdthayicard).checked(true);
                            }
                        }
                    } else {
                        aqChildReg.id(R.id.etname).text("");
                        aqChildReg.id(R.id.etage).text("");
                        aqChildReg.id(R.id.etthaicard).text("");
                        aqChildReg.id(R.id.rdrchid).checked(true);
                        aqChildReg.id(R.id.rdthayicard).checked(false);
                        aqChildReg.id(R.id.trrelation).gone();
                        rgRelation.clearCheck();
                        aqChildReg.id(R.id.trmotherdetails).gone();
                        aqChildReg.id(R.id.trage).visible();
                        aqChildReg.id(R.id.tridtype).visible();
                        aqChildReg.id(R.id.trthayicard).visible();
                        if (aqChildReg.id(R.id.spnwoman).getSelectedItemPosition() > 0) {
                            tblregisteredwomen woman = tblregisteredwomenList.get(aqChildReg.id(R.id.spnwoman).getSelectedItemPosition() - 1);

                            if (woman != null) {
                                aqChildReg.id(R.id.etname).text(woman.getRegWomanName());
                                aqChildReg.id(R.id.etage).text("" + woman.getRegAge());
                                aqChildReg.id(R.id.etthaicard).text(woman.getRegUIDNo());
                                if (woman.getRegUIDType().equalsIgnoreCase("RCHID"))
                                    aqChildReg.id(R.id.rdrchid).checked(true);
                                else
                                    aqChildReg.id(R.id.rdthayicard).checked(true);
                            }

                            DeliveryRepository deliveryRepository = new DeliveryRepository(databaseHelper);
                            List<TblDeliveryInfo> delData = deliveryRepository.getDeliveryData(woman.getWomanId(), woman.getUserId());

                            if (delData.get(0).getDelMothersDeath().equalsIgnoreCase("Y")) {
                                aqChildReg.id(R.id.chkmotherdeath).checked(true);

                                aqChildReg.id(R.id.trdeathdate).visible();
                                aqChildReg.id(R.id.trduringdel).visible();
                                aqChildReg.id(R.id.tragewhenpassesaway).visible();

                                aqChildReg.id(R.id.rdduringdelyes).checked(true);
                                aqChildReg.id(R.id.etdeathdate).text(delData.get(0).getDelDateTimeOfDelivery());
                                aqChildReg.id(R.id.trcausefdeath).visible();

                            } else
                                aqChildReg.id(R.id.chkmotherdeath).checked(false);


                        }
                    }

                    break;

                case R.id.spndeltypechl1:
                    /*if (aqChildReg.id(R.id.spndeltypechl1).getSelectedItem().toString()
                            .equalsIgnoreCase("Other")) */
                    if (aqChildReg.id(R.id.spndeltypechl1).getSelectedItem().toString()
                            .equalsIgnoreCase(getResources().getString(R.string.other)))//17Jun2021 Arpitha

                    {
                        aqChildReg.id(R.id.trdeltypeotherchl1).visible();


                    } else {
                        aqChildReg.id(R.id.trdeltypeotherchl1).gone();
                        aqChildReg.id(R.id.etdeltypeotherchl1).text("");
                    }

                    break;

                case R.id.spndeltypechl2:
                 /*   if (aqChildReg.id(R.id.spndeltypechl2).getSelectedItem().toString()
                            .equalsIgnoreCase("Other"))*/
                    if (aqChildReg.id(R.id.spndeltypechl2).getSelectedItem().toString()
                            .equalsIgnoreCase(getResources().getString(R.string.other)))   //17Jun2021 Arpitha
                    {
                        aqChildReg.id(R.id.trdeltypeotherchl2).visible();


                    } else {
                        aqChildReg.id(R.id.trdeltypeotherchl2).gone();
                        aqChildReg.id(R.id.etdeltypeotherchl2).text("");
                    }

                    break;

                case R.id.spndeltypechl3:
                /*    if (aqChildReg.id(R.id.spndeltypechl3).getSelectedItem().toString()
                            .equalsIgnoreCase("Other"))*/
                    if (aqChildReg.id(R.id.spndeltypechl3).getSelectedItem().toString()
                            .equalsIgnoreCase(getResources().getString(R.string.other)))   //17Jun2021 Arpitha
                    {
                        aqChildReg.id(R.id.trdeltypeotherchl3).visible();

                    } else {
                        aqChildReg.id(R.id.trdeltypeotherchl3).gone();
                        aqChildReg.id(R.id.etdeltypeotherchl3).text("");
                    }

                    break;
                case R.id.spndeltypechl4:
         /*           if (aqChildReg.id(R.id.spndeltypechl4).getSelectedItem().toString()
                            .equalsIgnoreCase("Other"))*/
                    if (aqChildReg.id(R.id.spndeltypechl4).getSelectedItem().toString()
                            .equalsIgnoreCase(getResources().getString(R.string.other)))   //17Jun2021 Arpit
                    {
                        aqChildReg.id(R.id.trdeltypeotherchl4).visible();

                    } else {
                        aqChildReg.id(R.id.trdeltypeotherchl4).gone();
                        aqChildReg.id(R.id.etdeltypeotherchl4).text("");
                    }

                    break;

                case R.id.spndeltypechl5:
                    if (aqChildReg.id(R.id.spndeltypechl5).getSelectedItem().toString()
                            .equalsIgnoreCase(getResources().getString(R.string.other)))   //17Jun2021 Arpit
                    {
                        aqChildReg.id(R.id.trdeltypeotherchl5).visible();

                    } else {
                        aqChildReg.id(R.id.trdeltypeotherchl5).gone();
                        aqChildReg.id(R.id.etdeltypeotherchl5).text("");
                    }

                    break;

                case R.id.spndeltypechl6:
                    /*if (aqChildReg.id(R.id.spndeltypechl6).getSelectedItem().toString()
                            .equalsIgnoreCase("Other")) */
                    if (aqChildReg.id(R.id.spndeltypechl6).getSelectedItem().toString()
                            .equalsIgnoreCase(getResources().getString(R.string.other)))   //17Jun2021 Arpit
                    {
                        aqChildReg.id(R.id.trdeltypeotherchl6).visible();

                    } else {
                        aqChildReg.id(R.id.trdeltypeotherchl6).gone();
                        aqChildReg.id(R.id.etdeltypeotherchl6).text("");
                    }

                    break;

                case R.id.spncasuseofdeathchl:

                    if (aqChildReg.id(R.id.spncasuseofdeathchl).getSelectedItem().toString()
                            .equalsIgnoreCase("Others(specify)")) {
                        aqChildReg.id(R.id.trothercausech).visible();
                    } else
                        aqChildReg.id(R.id.trothercausech).gone();
                    break;

                case R.id.spntribalch1:
                    int posofTribalHamlet = aqChildReg.id(R.id.spntribalch1).getSelectedItemPosition();
                    if (aqChildReg.id(R.id.spnnoofchild).getSelectedItemPosition() > 1) {
                        if (aqChildReg.id(R.id.spntribalch2).getSelectedItemPosition() == 0)
                            aqChildReg.id(R.id.spntribalch2).setSelection(posofTribalHamlet);
                    }
                    if (aqChildReg.id(R.id.spnnoofchild).getSelectedItemPosition() > 2) {
                        if (aqChildReg.id(R.id.spntribalch3).getSelectedItemPosition() == 0)
                            aqChildReg.id(R.id.spntribalch3).setSelection(posofTribalHamlet);
                    }
                    if (aqChildReg.id(R.id.spnnoofchild).getSelectedItemPosition() > 3) {
                        if (aqChildReg.id(R.id.spntribalch4).getSelectedItemPosition() == 0)
                            aqChildReg.id(R.id.spntribalch4).setSelection(posofTribalHamlet);
                    }
                    if (aqChildReg.id(R.id.spnnoofchild).getSelectedItemPosition() > 4) {
                        if (aqChildReg.id(R.id.spntribalch5).getSelectedItemPosition() == 0)
                            aqChildReg.id(R.id.spntribalch5).setSelection(posofTribalHamlet);
                    }
                    if (aqChildReg.id(R.id.spnnoofchild).getSelectedItemPosition() > 5) {
                        if (aqChildReg.id(R.id.spntribalch6).getSelectedItemPosition() == 0)
                            aqChildReg.id(R.id.spntribalch6).setSelection(posofTribalHamlet);
                    }
                    break;
                default:
                    break;
            }
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }


    void drawUI() throws Exception {


        for (int i = 1; i <= noOfChild; i++) {

            String childNo = "";
            int reservedId = 0;

            if (i == 1) {
                childNo = getResources().getString(R.string.first);
                reservedId = R.id.llchl1;
            } else if (i == 2) {
                childNo = getResources().getString(R.string.second);
                reservedId = R.id.llchl2;
            } else if (i == 3) {
                childNo = getResources().getString(R.string.third);
                reservedId = R.id.llchl3;
            } else if (i == 4) {
                childNo = getResources().getString(R.string.fourth);
                reservedId = R.id.llchl4;
            } else if (i == 5) {
                childNo = getResources().getString(R.string.fifth);
                reservedId = R.id.llchl5;
            } else if (i == 6) {
                childNo = getResources().getString(R.string.sixth);
                reservedId = R.id.llchl6;
            }


            TableRow trHeading = new TableRow(this);
            trHeading.setOrientation(LinearLayout.HORIZONTAL);
            trHeading.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            trHeading.setPadding(3, 3, 3, 3);
            trHeading.setDividerPadding(5);
            trHeading.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));

            int llchl = R.id.llchl;
            LinearLayout llchld = new LinearLayout(this);
            llchld.setOrientation(LinearLayout.HORIZONTAL);
            llchld.setOnClickListener(this);
            llchld.setId(reservedId);


            tvheading = new TextView(this);
            tvheading.setText(childNo + " " + getResources().getString(R.string.childdetails));
            tvheading.setWidth(730);
            tvheading.setTypeface(null, Typeface.BOLD);
            tvheading.setTextColor(getResources().getColor(R.color.white));
            tvheading.setTextSize(18);

            int imgId = R.id.imgview;
            ImageView imgview = new ImageView(this);
            imgview.setImageDrawable(getResources().getDrawable(R.drawable.ic_add_box));
            imgview.setId(imgId + i);

            llchld.addView(tvheading);
            llchld.addView(imgview);


            trHeading.addView(llchld);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, 10, 0, 0);
            trHeading.setLayoutParams(params);

            linearChild.addView(trHeading);


            int llChildId = R.id.linearchild;
            LinearLayout llchild = new LinearLayout(this);
            llchild.setOrientation(LinearLayout.VERTICAL);
            llchild.setId(llChildId + i);
            llchild.setBackground(getResources().getDrawable(R.drawable.edit_text_style_black));


            TableRow trDelType = new TableRow(this);
            trDelType.setOrientation(LinearLayout.HORIZONTAL);
            trDelType.setPadding(5, 5, 5, 5);

            trDelType.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

            TextView tvDelType = new TextView(this);
            // tvDelType.setText("Delivery Type");
            tvDelType.setText(getResources().getString(R.string.tvDeliveryType)); //06Dec2019 - Bindu
            tvDelType.setWidth(390);
            tvDelType.setTypeface(null, Typeface.BOLD);
            tvDelType.setTextColor(getResources().getColor(R.color.black));
            tvDelType.setTextSize(18);

            TextView tvdeltypeMand = new TextView(this);
            tvdeltypeMand.setText("*");
            tvdeltypeMand.setTextColor(getResources().getColor(R.color.red));
            tvdeltypeMand.setTextSize(18);


            ArrayAdapter<String> adapterDelType = new ArrayAdapter<String>(this,
                    R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.deltype));
            adapterDelType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            int delTypeId = R.id.spndeltypechl;
            Spinner spnDelType = new Spinner(this);
            spnDelType.setId(delTypeId + i);
            spnDelType.setDropDownWidth(420);
            spnDelType.setMinimumHeight(50);
            spnDelType.setMinimumWidth(405);
            spnDelType.setAdapter(adapterDelType);

            spnDelType.setBackground(getResources().getDrawable(R.drawable.selector_slim_spinner));


            trDelType.addView(tvdeltypeMand);
            trDelType.addView(tvDelType);
            trDelType.addView(spnDelType);
            llchild.addView(trDelType);


            int delOtherId = R.id.trdeltypeotherchl;
            TableRow trOtherType = new TableRow(this);
            trOtherType.setOrientation(LinearLayout.HORIZONTAL);
            trOtherType.setPadding(5, 5, 5, 5);
            trOtherType.setId(delOtherId + i);

            trOtherType.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

            TextView tvOtherType = new TextView(this);
            tvOtherType.setText(getResources().getString(R.string.otherdeltype));//17JUn2021 Arpitha
            tvOtherType.setWidth(410);
            tvOtherType.setTypeface(null, Typeface.BOLD);
            tvOtherType.setTextColor(getResources().getColor(R.color.black));
            tvOtherType.setTextSize(18);


            int otherTypeId = R.id.etdeltypeotherchl;
            EditText etOtherType = new EditText(this);
            etOtherType.setId(otherTypeId + i);
            etOtherType.setTextColor(getResources().getColor(R.color.black));
            etOtherType.setTextSize(15);
            etOtherType.setWidth(400);
            etOtherType.setPadding(12, 12, 12, 12);
            etOtherType.setBackground(getResources().getDrawable(R.drawable.edittext_style));
            etOtherType.setInputType(InputType.TYPE_CLASS_TEXT);

            int maxLengthOther = 50;
            InputFilter[] FilterArrayOther = new InputFilter[1];
            FilterArrayOther = new InputFilter[1];
            FilterArrayOther[0] = new InputFilter.LengthFilter(maxLengthOther);
            etOtherType.setFilters(FilterArrayOther);


            trOtherType.setVisibility(View.GONE);
            trOtherType.addView(tvOtherType);
            trOtherType.addView(etOtherType);
            llchild.addView(trOtherType);






               //mani Aug18-2021
            TableRow trDelPlace = new TableRow(this);
            trDelPlace.setOrientation(LinearLayout.HORIZONTAL);
            trDelPlace.setPadding(5, 5, 5, 5);

            trDelPlace.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

            TextView tvPlaceDel = new TextView(this);
            tvPlaceDel.setText(getResources().getString(R.string.tvdeliveryplace));
            tvPlaceDel.setWidth(395);
            tvPlaceDel.setTypeface(null, Typeface.BOLD);
            tvPlaceDel.setTextColor(getResources().getColor(R.color.black));
            tvPlaceDel.setTextSize(18);

            TextView tvdelPlaceMand = new TextView(this);
            tvdelPlaceMand.setText("*");
            tvdelPlaceMand.setTextColor(getResources().getColor(R.color.red));
            tvdelPlaceMand.setTextSize(18);


            ArrayAdapter<String> adapterDelPlace = new ArrayAdapter<String>(this,
                    R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.delalce));
            adapterDelPlace.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            int delPlaceId = R.id.spndelplacech;
            Spinner spnDelPlace = new Spinner(this);
            spnDelPlace.setId(delPlaceId + i);
            spnDelPlace.setDropDownWidth(250);
            spnDelPlace.setMinimumHeight(50);
            spnDelPlace.setMinimumWidth(405);
            spnDelPlace.setAdapter(adapterDelPlace);

            spnDelPlace.setBackground(getResources().getDrawable(R.drawable.selector_slim_spinner));

            trDelPlace.addView(tvdelPlaceMand);
            trDelPlace.addView(tvPlaceDel);
            trDelPlace.addView(spnDelPlace);
            llchild.addView(trDelPlace);

            TableRow trdob = new TableRow(this);
            trdob.setOrientation(LinearLayout.HORIZONTAL);
            trdob.setPadding(0, 5, 5, 5);

            trdob.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

            TextView tvdobMand = new TextView(this);
            tvdobMand.setText("*");
            tvdobMand.setTextColor(getResources().getColor(R.color.red));
            tvdobMand.setTextSize(18);

            tvdob = new TextView(this);
            tvdob.setText(getResources().getString(R.string.tvdobbaby)); //06Dec2019 - Bindu //30Apr2021 Bindu
            tvdob.setWidth(400);
            tvdob.setTypeface(null, Typeface.BOLD);
            tvdob.setTextColor(getResources().getColor(R.color.black));
            tvdob.setTextSize(18);


            int ids = R.id.dob;
            etdob = new EditText(this);
            etdob.setId(ids + i);
            etdob.setTextColor(getResources().getColor(R.color.black));
            etdob.setTextSize(15);
            etdob.setWidth(400);
            etdob.setPadding(12, 12, 12, 12);
            etdob.setBackground(getResources().getDrawable(R.drawable.edittext_style));
            etdob.setInputType(InputType.TYPE_DATETIME_VARIATION_DATE);
            etdob.setOnTouchListener(this);
            etdob.setFocusable(false);


            Drawable img = getResources().getDrawable(R.drawable.brush1);
            img.setBounds(0, 0, 30, 30);


            etdob.setCompoundDrawables(null, null,
                    img, null);
            // etdob.setText(DateTimeUtil.getTodaysDate());


            TextView tvspace = new TextView(this);
            tvspace.setWidth(3);
            tvspace.setTypeface(null, Typeface.BOLD);
            tvspace.setTextColor(getResources().getColor(R.color.black));
            tvspace.setTextSize(18);

            int tobIds = R.id.tob;
            EditText ettob = new EditText(this);
            ettob.setId(tobIds + i);
            ettob.setTextColor(getResources().getColor(R.color.black));
            ettob.setTextSize(15);
            ettob.setWidth(150);
            ettob.setPadding(12, 12, 12, 12);
            ettob.setBackground(getResources().getDrawable(R.drawable.edittext_style));
            ettob.setInputType(InputType.TYPE_DATETIME_VARIATION_TIME);
            ettob.setText(DateTimeUtil.getCurrentTime());


            trdob.addView(tvdobMand);
            trdob.addView(tvdob);
            trdob.addView(etdob);
//            trdob.addView(tvspace);
//            trdob.addView(ettob);
            llchild.addView(trdob);


            TableRow trName = new TableRow(this);
            trName.setOrientation(LinearLayout.HORIZONTAL);
            trName.setPadding(0, 5, 5, 5);

            trName.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

            TextView tvnameMand = new TextView(this);
            tvnameMand.setText("*");
            tvnameMand.setTextColor(getResources().getColor(R.color.red));
            tvnameMand.setTextSize(18);


            TextView tvName = new TextView(this);
            tvName.setText(getResources().getString(R.string.babyname)); //06Dec2019 - Bindu
            tvName.setWidth(400);
            tvName.setTypeface(null, Typeface.BOLD);
            tvName.setTextColor(getResources().getColor(R.color.black));
            tvName.setTextSize(18);


            int nameIds = R.id.etchlname;
            EditText etName = new EditText(this);
            etName.setId(nameIds + i);
            etName.setTextColor(getResources().getColor(R.color.black));
            etName.setTextSize(15);
            etName.setWidth(400);
            etName.setPadding(12, 12, 12, 12);
            etName.setBackground(getResources().getDrawable(R.drawable.edittext_style));
            etName.setInputType(InputType.TYPE_CLASS_TEXT);
            //  etName.setKeyListener(DigitsKeyListener.getInstance("1234567890"));

            etName.setKeyListener(TextKeyListener.getInstance());

            etName.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(45)});

            trName.addView(tvnameMand);
            trName.addView(tvName);
            trName.addView(etName);
            llchild.addView(trName);


            TableRow trRch = new TableRow(this);
            trRch.setOrientation(LinearLayout.HORIZONTAL);
            trRch.setPadding(0, 5, 5, 5);

            trRch.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));


            TextView tvRch = new TextView(this);
            tvRch.setText(getResources().getString(R.string.rchid));
//            tvRch.setId(id);
            tvRch.setWidth(410);
            tvRch.setTypeface(null, Typeface.BOLD);
            tvRch.setTextColor(getResources().getColor(R.color.black));
            tvRch.setTextSize(18);


            int rchIds = R.id.etrch;

            EditText etRch = new EditText(this);
            etRch.setId(rchIds + i);
            etRch.setTextColor(getResources().getColor(R.color.black));
            etRch.setTextSize(15);
            etRch.setWidth(400);
            etRch.setPadding(12, 12, 12, 12);
            etRch.setBackground(getResources().getDrawable(R.drawable.edittext_style));
            etRch.setInputType(InputType.TYPE_CLASS_NUMBER);

            int maxLengthRch = 15;
            //    InputFilter[] FilterArrayRch = new InputFilter[1];
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(maxLengthRch);
            etRch.setFilters(FilterArray);


            trRch.addView(tvRch);
            trRch.addView(etRch);
            llchild.addView(trRch);


            TableRow trWeightUnit = new TableRow(this);
            trWeightUnit.setOrientation(LinearLayout.HORIZONTAL);
            trWeightUnit.setPadding(5, 5, 5, 5);

            trWeightUnit.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

        /*    TextView tvGenderMand = new TextView(this);
            tvGenderMand.setText("*");
            tvGenderMand.setTextColor(getResources().getColor(R.color.red));
            tvGenderMand.setTextSize(18);*/


            TextView tvWeightUnit = new TextView(this);
            tvWeightUnit.setText(getResources().getString(R.string.weightunit)); //06Dec2019 - Bindu
            tvWeightUnit.setWidth(380);
            tvWeightUnit.setTypeface(null, Typeface.BOLD);
            tvWeightUnit.setTextColor(getResources().getColor(R.color.black));
            tvWeightUnit.setTextSize(18);


            RadioGroup rgWeightUnit = new RadioGroup(this);
            rgWeightUnit.setOrientation(LinearLayout.HORIZONTAL);


            gramsId = R.id.rdgrams;
            kgsId = R.id.rdkgs;
            //     int ambiId = R.id.rdambi;

            RadioButton rdGrams = new RadioButton(this);
            rdGrams.setText(getResources().getString(R.string.grams));
            rdGrams.setChecked(true);
            rdGrams.setOnClickListener(this);
            rdGrams.setTypeface(Typeface.DEFAULT_BOLD);

            RadioButton rdKgs = new RadioButton(this);
            rdKgs.setText(getResources().getString(R.string.kgs));
            rdKgs.setOnClickListener(this);
            rdKgs.setTypeface(Typeface.DEFAULT_BOLD);


            rdGrams.setId(gramsId + i);
            rdKgs.setId(kgsId + i);

            rgWeightUnit.addView(rdGrams);
            rgWeightUnit.addView(rdKgs);


            //trWeightUnit.addView(tvGenderMand);
            trWeightUnit.addView(tvWeightUnit);
            trWeightUnit.addView(rgWeightUnit);
            llchild.addView(trWeightUnit);


            TableRow trWeight = new TableRow(this);
            trWeight.setOrientation(LinearLayout.HORIZONTAL);
            trWeight.setPadding(5, 5, 5, 5);

            trWeight.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

            TextView tvWeight = new TextView(this);
            tvWeight.setText(getResources().getString(R.string.currentweight)); //06Dec2019 - Bindu
            tvWeight.setWidth(400);
            tvWeight.setTypeface(null, Typeface.BOLD);
            tvWeight.setTextColor(getResources().getColor(R.color.black));
            tvWeight.setTextSize(18);


            int weightId = R.id.chlWeight;
            EditText etWeight = new EditText(this);
            etWeight.setTextColor(getResources().getColor(R.color.black));
            etWeight.setTextSize(15);
            etWeight.setId(weightId + i);
            etWeight.setWidth(400);
            //  etWeight.setHint("kgs");
            etWeight.setPadding(12, 12, 12, 12);
            etWeight.setBackground(getResources().getDrawable(R.drawable.edittext_style));

            etWeight.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL); //for decimal numbers


            int maxLengthWeight = 5;
            InputFilter[] FilterArrayWeight = new InputFilter[1];
            FilterArrayWeight = new InputFilter[1];
            FilterArrayWeight[0] = new InputFilter.LengthFilter(maxLengthWeight);
            etWeight.setFilters(FilterArrayWeight);


            trWeight.addView(tvWeight);
            trWeight.addView(etWeight);
            llchild.addView(trWeight);


            TableRow trtrbalHamlet = new TableRow(this);
            trtrbalHamlet.setOrientation(LinearLayout.HORIZONTAL);
            trtrbalHamlet.setPadding(5, 5, 5, 5);

            trtrbalHamlet.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

            TextView tvTribal = new TextView(this);
            tvTribal.setText(getResources().getString(R.string.tribalhamlet));
            tvTribal.setWidth(390);
            tvTribal.setTypeface(null, Typeface.BOLD);
            tvTribal.setTextColor(getResources().getColor(R.color.black));
            tvTribal.setTextSize(18);

            TextView tvTribalMand = new TextView(this);
            tvTribalMand.setText("*");
            tvTribalMand.setTextColor(getResources().getColor(R.color.red));
            tvTribalMand.setTextSize(18);


            FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
            mapVillage = facilityRepository.getPlaceMap();
            ArrayList<String> villageList = new ArrayList<String>();
            villageList.add(getResources().getString(R.string.selectType));
            for (Map.Entry<String, Integer> village : mapVillage.entrySet())
                villageList.add(village.getKey());

            ArrayAdapter<String> adapterTribal = new ArrayAdapter<String>(this,
                    R.layout.simple_spinner_dropdown_item, villageList);
            adapterTribal.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            int tribalId = R.id.spntribalch;
            Spinner spnTribal = new Spinner(this);
            spnTribal.setId(tribalId + i);
            spnTribal.setDropDownWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
            spnTribal.setMinimumHeight(50);
            spnTribal.setMinimumWidth(405);
            spnTribal.setAdapter(adapterTribal);

            spnTribal.setBackground(getResources().getDrawable(R.drawable.selector_slim_spinner));


            trtrbalHamlet.addView(tvTribalMand);
            trtrbalHamlet.addView(tvTribal);
            trtrbalHamlet.addView(spnTribal);
            llchild.addView(trtrbalHamlet);


            TableRow trGender = new TableRow(this);
            trGender.setOrientation(LinearLayout.HORIZONTAL);
            trGender.setPadding(5, 5, 5, 5);

            trGender.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

            TextView tvGenderMand = new TextView(this);
            tvGenderMand.setText("*");
            tvGenderMand.setTextColor(getResources().getColor(R.color.red));
            tvGenderMand.setTextSize(18);


            TextView tvGender = new TextView(this);
            tvGender.setText(getResources().getString(R.string.tvchildsex)); //06Dec2019 - Bindu
            tvGender.setWidth(375);
            tvGender.setTypeface(null, Typeface.BOLD);
            tvGender.setTextColor(getResources().getColor(R.color.black));
            tvGender.setTextSize(18);


            RadioGroup rgGender = new RadioGroup(this);
            rgGender.setOrientation(LinearLayout.VERTICAL); //03MAy2021 Bindu - change horizontal to vertical

            int maleId = R.id.rdmale;
            int femaleId = R.id.rdfemale;
            int ambiId = R.id.rdambi;

            RadioButton rdFemale = new RadioButton(this);
            rdFemale.setText(getResources().getString(R.string.radiodfemale1));
            rdFemale.setTypeface(Typeface.DEFAULT_BOLD);

            RadioButton rdMale = new RadioButton(this);
            rdMale.setText(getResources().getString(R.string.radiomale1));
            rdMale.setTypeface(Typeface.DEFAULT_BOLD);

            RadioButton rdAmbi = new RadioButton(this);
            rdAmbi.setText(getResources().getString(R.string.ambigious));
            rdAmbi.setTypeface(Typeface.DEFAULT_BOLD);

            rdMale.setId(maleId + i);
            rdFemale.setId(femaleId + i);
            rdAmbi.setId(ambiId + i);

            rgGender.addView(rdFemale);
            rgGender.addView(rdMale);
            rgGender.addView(rdAmbi);


            trGender.addView(tvGenderMand);
            trGender.addView(tvGender);
            trGender.addView(rgGender);
            llchild.addView(trGender);


            TableRow trSchool = new TableRow(this);
            trSchool.setOrientation(LinearLayout.HORIZONTAL);
            trSchool.setPadding(5, 5, 5, 5);

            trSchool.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

            TextView tvSchool = new TextView(this);
            tvSchool.setText(getResources().getString(R.string.goingschool));
            tvSchool.setWidth(390);
            tvSchool.setTypeface(null, Typeface.BOLD);
            tvSchool.setTextColor(getResources().getColor(R.color.black));
            tvSchool.setTextSize(18);


            RadioGroup rgSchool = new RadioGroup(this);
            rgSchool.setOrientation(LinearLayout.HORIZONTAL);

            int scholYesId = R.id.rdschoolYes;
            RadioButton rdSchoolYes = new RadioButton(this);
            rdSchoolYes.setText(getResources().getString(R.string.m121));
            rdSchoolYes.setId(scholYesId + i);
            rdSchoolYes.setTypeface(Typeface.DEFAULT_BOLD);

            int scholNoId = R.id.rdschoolNo;
            RadioButton rdSchoolNo = new RadioButton(this);
            rdSchoolNo.setId(scholNoId + i);
            rdSchoolNo.setText(getResources().getString(R.string.m122));
            rdSchoolNo.setTypeface(Typeface.DEFAULT_BOLD);


            rgSchool.addView(rdSchoolYes);
            rgSchool.addView(rdSchoolNo);


            trSchool.addView(tvSchool);
            trSchool.addView(rgSchool);
            llchild.addView(trSchool);


            TableRow trPhy = new TableRow(this);
            trPhy.setOrientation(LinearLayout.HORIZONTAL);
            trPhy.setPadding(5, 5, 5, 5);

            trPhy.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));


            TextView tvPhyMand = new TextView(this);
            tvPhyMand.setText("*");
            tvPhyMand.setTextColor(getResources().getColor(R.color.red));
            tvPhyMand.setTextSize(18);


            TextView tvPhy = new TextView(this);
            tvPhy.setText(getResources().getString(R.string.physicaldisability));
            tvPhy.setWidth(380);
            tvPhy.setTypeface(null, Typeface.BOLD);
            tvPhy.setTextColor(getResources().getColor(R.color.black));
            tvPhy.setTextSize(18);


            RadioGroup rgPhy = new RadioGroup(this);
            rgPhy.setOrientation(LinearLayout.HORIZONTAL);

            int rdPhyYesId = R.id.rdPhyYes;

            RadioButton rdPhyYes = new RadioButton(this);
            rdPhyYes.setText(getResources().getString(R.string.m121));
            rdPhyYes.setTypeface(Typeface.DEFAULT_BOLD);
            rdPhyYes.setId(rdPhyYesId + i);
            rdPhyYes.setOnClickListener(this);

            int rdPhyNoId = R.id.rdPhyNo;
            RadioButton rdPhyNo = new RadioButton(this);
            rdPhyNo.setText(getResources().getString(R.string.m122));
            rdPhyNo.setTypeface(Typeface.DEFAULT_BOLD);
            rdPhyNo.setId(rdPhyNoId + i);
            rdPhyNo.setOnClickListener(this);


            rgPhy.addView(rdPhyYes);
            rgPhy.addView(rdPhyNo);


            trPhy.addView(tvPhyMand);
            trPhy.addView(tvPhy);
            trPhy.addView(rgPhy);
            llchild.addView(trPhy);


            int trPhyOtherId = R.id.trPhyOther;

            TableRow trOtherPhyDis = new TableRow(this);
            trOtherPhyDis.setOrientation(LinearLayout.HORIZONTAL);
            trOtherPhyDis.setPadding(5, 5, 5, 5);
            trOtherPhyDis.setId(trPhyOtherId + i);
            trOtherPhyDis.setVisibility(View.GONE);

            trOtherPhyDis.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

            TextView tvOtherPhy = new TextView(this);
            tvOtherPhy.setText(getResources().getString(R.string.otherphysicaldisability));
            tvOtherPhy.setWidth(400);
            tvOtherPhy.setTypeface(null, Typeface.BOLD);
            tvOtherPhy.setTextColor(getResources().getColor(R.color.black));
            tvOtherPhy.setTextSize(18);


            int phyId = R.id.etPhyOther;
            EditText etPhy = new EditText(this);
            etPhy.setId(phyId + i);
            etPhy.setTextColor(getResources().getColor(R.color.black));
            etPhy.setTextSize(15);
            etPhy.setWidth(400);
            etPhy.setPadding(12, 12, 12, 12);
            etPhy.setBackground(getResources().getDrawable(R.drawable.edittext_style));
            etPhy.setInputType(InputType.TYPE_CLASS_TEXT);

            int maxLength = 50;
            FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(maxLength);
            etPhy.setFilters(FilterArray);


            trOtherPhyDis.addView(tvOtherPhy);
            trOtherPhyDis.addView(etPhy);
            llchild.addView(trOtherPhyDis);


            TableRow trMentalDis = new TableRow(this);
            trMentalDis.setOrientation(LinearLayout.HORIZONTAL);
            trMentalDis.setPadding(5, 5, 5, 5);

            trMentalDis.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

            TextView tvMentMand = new TextView(this);
            tvMentMand.setText("*");
            tvMentMand.setTextColor(getResources().getColor(R.color.red));
            tvMentMand.setTextSize(18);

            TextView tvMental = new TextView(this);
            tvMental.setText(getResources().getString(R.string.mentaldisability));
            tvMental.setWidth(380);
            tvMental.setTypeface(null, Typeface.BOLD);
            tvMental.setTextColor(getResources().getColor(R.color.black));
            tvMental.setTextSize(18);


            RadioGroup rgMental = new RadioGroup(this);
            rgMental.setOrientation(LinearLayout.HORIZONTAL);

            int rdMenYesId = R.id.rdMenYes;

            RadioButton rdMentalYes = new RadioButton(this);
            rdMentalYes.setText(getResources().getString(R.string.m121));
            rdMentalYes.setTypeface(Typeface.DEFAULT_BOLD);
            rdMentalYes.setOnClickListener(this);
            rdMentalYes.setId(rdMenYesId + i);

            int rdMenNoId = R.id.rdMenNo;
            RadioButton rdMentalNo = new RadioButton(this);
            rdMentalNo.setText(getResources().getString(R.string.m122));
            rdMentalNo.setTypeface(Typeface.DEFAULT_BOLD);
            rdMentalNo.setOnClickListener(this);
            rdMentalNo.setId(rdMenNoId + i);


            rgMental.addView(rdMentalYes);
            rgMental.addView(rdMentalNo);


            trMentalDis.addView(tvMentMand);
            trMentalDis.addView(tvMental);
            trMentalDis.addView(rgMental);

            llchild.addView(trMentalDis);


            int trMenOther = R.id.trMentalOther;
            TableRow trOtherMentalDis = new TableRow(this);
            trOtherMentalDis.setOrientation(LinearLayout.HORIZONTAL);
            trOtherMentalDis.setPadding(5, 5, 5, 5);
            trOtherMentalDis.setId(trMenOther + i);
            trOtherMentalDis.setVisibility(View.GONE);

            trOtherMentalDis.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

            TextView tvOtherMental = new TextView(this);
            tvOtherMental.setText(getResources().getString(R.string.othermentaldisability));
            tvOtherMental.setWidth(400);
            tvOtherMental.setTypeface(null, Typeface.BOLD);
            tvOtherMental.setTextColor(getResources().getColor(R.color.black));
            tvOtherMental.setTextSize(18);


            int etMentalId = R.id.etMentalOther;
            EditText etMental = new EditText(this);
            etMental.setId(etMentalId + i);
            etMental.setTextColor(getResources().getColor(R.color.black));
            etMental.setTextSize(15);
            etMental.setWidth(400);
            etMental.setPadding(12, 12, 12, 12);
            etMental.setBackground(getResources().getDrawable(R.drawable.edittext_style));
            etMental.setInputType(InputType.TYPE_CLASS_TEXT);

            int maxLengthMen = 50;
            InputFilter[] FilterArrayMen = new InputFilter[1];
            FilterArrayMen[0] = new InputFilter.LengthFilter(maxLengthMen);
            etMental.setFilters(FilterArrayMen);


            trOtherMentalDis.addView(tvOtherMental);
            trOtherMentalDis.addView(etMental);
            llchild.addView(trOtherMentalDis);


            TableRow trCompl = new TableRow(this);
            trCompl.setOrientation(LinearLayout.HORIZONTAL);
            trCompl.setPadding(5, 5, 5, 5);

            trCompl.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

            TextView tvComp = new TextView(this);
            tvComp.setText(getResources().getString(R.string.complications)); //06Dec2019 - Bindu
            tvComp.setWidth(400);
            tvComp.setTypeface(null, Typeface.BOLD);
            tvComp.setTextColor(getResources().getColor(R.color.black));
            tvComp.setTextSize(18);


            int id = R.id.mspnchildcompl;
             spnCompl = new MultiSelectionSpinner(this);
            spnCompl.setId(delTypeId + i);
            spnCompl.setMinimumHeight(50);
            spnCompl.setMinimumWidth(400);
            spnCompl.setId(id + i);
            spnCompl.setItems(getResources().getStringArray(R.array.childComplications));

            spnCompl.setBackground(getResources().getDrawable(R.drawable.selector_slim_spinner));

            /*if(i == 1)
            spnCompl1.setId(R.id.mspnchildcompl1);
            else if(i == 2)
                spnCompl2.setId(R.id.mspnchildcompl2);
            else if(i == 3)
                spnCompl3.setId(R.id.mspnchildcompl3);
            else if(i == 4)
                spnCompl4.setId(R.id.mspnchildcompl4);
            else if(i == 5)
                spnCompl5.setId(R.id.mspnchildcompl5);

            else if(i == 6)
                spnCompl5.setId(R.id.mspnchildcompl6);*/



            trCompl.addView(tvComp);
            trCompl.addView(spnCompl);
            llchild.addView(trCompl);



            int trOther = R.id.trOtherChildCompl;
             trOtherChildCompl = new TableRow(this);
            trOtherChildCompl.setOrientation(LinearLayout.HORIZONTAL);
            trOtherChildCompl.setPadding(5, 5, 5, 5);
            trOtherChildCompl.setId(trOther+i);
            trOtherChildCompl.setVisibility(View.GONE);
//            trComments.setId(delOtherId + i);


          /*  if(i == 1)
                trOtherChildCompl1.setId(R.id.trOtherChildCompl1);
            else if(i == 2)
                trOtherChildCompl2.setId(R.id.trOtherChildCompl2);
            else if(i == 3)
                trOtherChildCompl3.setId(R.id.trOtherChildCompl3);
            else if(i == 4)
                trOtherChildCompl4.setId(R.id.trOtherChildCompl4);
            else if(i == 5)
                trOtherChildCompl5.setId(R.id.trOtherChildCompl5);
            else if(i == 6)
                trOtherChildCompl6.setId(R.id.trOtherChildCompl6);*/

            trOtherChildCompl.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

            TextView tvothercompl = new TextView(this);
            tvothercompl.setText(getResources().getString(R.string.others));
            tvothercompl.setWidth(400);
            tvothercompl.setTypeface(null, Typeface.BOLD);
            tvothercompl.setTextColor(getResources().getColor(R.color.black));
            tvothercompl.setTextSize(18);


            int OtherCompl = R.id.etOtherChildCompl;
            EditText etOtherCompl = new EditText(this);
            etOtherCompl.setTextColor(getResources().getColor(R.color.black));
            etOtherCompl.setTextSize(15);
            etOtherCompl.setId(OtherCompl + i);
            etOtherCompl.setWidth(400);
            etOtherCompl.setPadding(12, 12, 12, 12);
            etOtherCompl.setBackground(getResources().getDrawable(R.drawable.edittext_style));
            etOtherCompl.setInputType(InputType.TYPE_CLASS_TEXT);

            int maxLengthM = 50;
            InputFilter[] flt = new InputFilter[1];
            flt = new InputFilter[1];
            flt[0] = new InputFilter.LengthFilter(maxLengthM);
            etOtherCompl.setFilters(flt);


            trOtherChildCompl.addView(tvothercompl);
            trOtherChildCompl.addView(etOtherCompl);
            llchild.addView(trOtherChildCompl);


            TableRow trComments = new TableRow(this);
            trComments.setOrientation(LinearLayout.HORIZONTAL);
            trComments.setPadding(5, 5, 5, 5);
//            trComments.setId(delOtherId + i);

            trComments.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

            TextView tvComments = new TextView(this);
            tvComments.setText(getResources().getString(R.string.tvComments));
            tvComments.setWidth(400);
            tvComments.setTypeface(null, Typeface.BOLD);
            tvComments.setTextColor(getResources().getColor(R.color.black));
            tvComments.setTextSize(18);


            int comments = R.id.etcomments;
            EditText etComments = new EditText(this);
            etComments.setTextColor(getResources().getColor(R.color.black));
            etComments.setTextSize(15);
            etComments.setId(comments + i);
            etComments.setWidth(400);
            etComments.setPadding(12, 12, 12, 12);
            etComments.setBackground(getResources().getDrawable(R.drawable.edittext_style));
            etComments.setInputType(InputType.TYPE_CLASS_TEXT);

            etComments.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(50)});
            etOtherCompl.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(45)});
            etPhy.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(45)});
            etMental.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(45)});


            int maxLengthC = 50;
            InputFilter[] fltC = new InputFilter[1];
            fltC = new InputFilter[1];
            fltC[0] = new InputFilter.LengthFilter(maxLengthC);
            etComments.setFilters(fltC);







            trComments.addView(tvComments);
            trComments.addView(etComments);
            llchild.addView(trComments);


            llchild.setVisibility(View.GONE);
            linearChild.addView(llchild);

        }
    }


    @Override
    public void onClick(View v) {

        try {

            switch (v.getId()) {
                case R.id.llchl1: {
                    if (aqChildReg.id(R.id.linearchild1).getView().getVisibility() == View.VISIBLE) {
                        aqChildReg.id(R.id.linearchild1).gone();
                        aqChildReg.id(R.id.imgview1).background(R.drawable.ic_add_box);
                    } else {
                        aqChildReg.id(R.id.linearchild1).visible();
                        aqChildReg.id(R.id.imgview1).background(R.drawable.ic_indeterminate_check_box);

                        aqChildReg.id(R.id.linearchild2).gone();
                        aqChildReg.id(R.id.linearchild3).gone();
                        aqChildReg.id(R.id.linearchild4).gone();
                        aqChildReg.id(R.id.linearchild5).gone();
                        aqChildReg.id(R.id.linearchild6).gone();
                        aqChildReg.id(R.id.imgview5).background(R.drawable.ic_add_box);
                        aqChildReg.id(R.id.imgview6).background(R.drawable.ic_add_box);

                        aqChildReg.id(R.id.imgview3).background(R.drawable.ic_add_box);
                        aqChildReg.id(R.id.imgview2).background(R.drawable.ic_add_box);
                        aqChildReg.id(R.id.imgview4).background(R.drawable.ic_add_box);
                    }
                }
                break;

                case R.id.llchl2: {
                    if (aqChildReg.id(R.id.linearchild2).getView().getVisibility() == View.VISIBLE) {
                        aqChildReg.id(R.id.linearchild2).gone();
                        aqChildReg.id(R.id.imgview2).background(R.drawable.ic_add_box);
                    } else {
                        aqChildReg.id(R.id.linearchild2).visible();
                        aqChildReg.id(R.id.imgview2).background(R.drawable.ic_indeterminate_check_box);

                        aqChildReg.id(R.id.linearchild1).gone();
                        aqChildReg.id(R.id.linearchild3).gone();
                        aqChildReg.id(R.id.linearchild4).gone();
                        aqChildReg.id(R.id.linearchild5).gone();
                        aqChildReg.id(R.id.linearchild6).gone();
                        aqChildReg.id(R.id.imgview5).background(R.drawable.ic_add_box);
                        aqChildReg.id(R.id.imgview6).background(R.drawable.ic_add_box);

                        aqChildReg.id(R.id.imgview3).background(R.drawable.ic_add_box);
                        aqChildReg.id(R.id.imgview4).background(R.drawable.ic_add_box);
                        aqChildReg.id(R.id.imgview1).background(R.drawable.ic_add_box);
                    }
                }
                break;

                case R.id.llchl3: {
                    if (aqChildReg.id(R.id.linearchild3).getView().getVisibility() == View.VISIBLE) {
                        aqChildReg.id(R.id.linearchild3).gone();
                        aqChildReg.id(R.id.imgview3).background(R.drawable.ic_add_box);
                    } else {
                        aqChildReg.id(R.id.linearchild3).visible();
                        aqChildReg.id(R.id.imgview3).background(R.drawable.ic_indeterminate_check_box);

                        aqChildReg.id(R.id.linearchild2).gone();
                        aqChildReg.id(R.id.linearchild1).gone();
                        aqChildReg.id(R.id.linearchild4).gone();
                        aqChildReg.id(R.id.linearchild5).gone();
                        aqChildReg.id(R.id.linearchild6).gone();
                        aqChildReg.id(R.id.imgview5).background(R.drawable.ic_add_box);
                        aqChildReg.id(R.id.imgview6).background(R.drawable.ic_add_box);

                        aqChildReg.id(R.id.imgview4).background(R.drawable.ic_add_box);
                        aqChildReg.id(R.id.imgview2).background(R.drawable.ic_add_box);
                        aqChildReg.id(R.id.imgview1).background(R.drawable.ic_add_box);
                    }
                }
                break;

                case R.id.llchl4: {
                    if (aqChildReg.id(R.id.linearchild4).getView().getVisibility() == View.VISIBLE) {
                        aqChildReg.id(R.id.linearchild4).gone();
                        aqChildReg.id(R.id.imgview4).background(R.drawable.ic_add_box);
                    } else {
                        aqChildReg.id(R.id.linearchild4).visible();
                        aqChildReg.id(R.id.imgview4).background(R.drawable.ic_indeterminate_check_box);

                        aqChildReg.id(R.id.linearchild2).gone();
                        aqChildReg.id(R.id.linearchild3).gone();
                        aqChildReg.id(R.id.linearchild1).gone();
                        aqChildReg.id(R.id.linearchild5).gone();
                        aqChildReg.id(R.id.linearchild6).gone();
                        aqChildReg.id(R.id.imgview5).background(R.drawable.ic_add_box);
                        aqChildReg.id(R.id.imgview6).background(R.drawable.ic_add_box);

                        aqChildReg.id(R.id.imgview3).background(R.drawable.ic_add_box);
                        aqChildReg.id(R.id.imgview2).background(R.drawable.ic_add_box);
                        aqChildReg.id(R.id.imgview1).background(R.drawable.ic_add_box);


                    }
                }
                break;

                case R.id.llchl5: {
                    if (aqChildReg.id(R.id.linearchild5).getView().getVisibility() == View.VISIBLE) {
                        aqChildReg.id(R.id.linearchild5).gone();
                        aqChildReg.id(R.id.imgview5).background(R.drawable.ic_add_box);
                    } else {
                        aqChildReg.id(R.id.linearchild5).visible();
                        aqChildReg.id(R.id.imgview5).background(R.drawable.ic_indeterminate_check_box);

                        aqChildReg.id(R.id.linearchild2).gone();
                        aqChildReg.id(R.id.linearchild3).gone();
                        aqChildReg.id(R.id.linearchild1).gone();
                        aqChildReg.id(R.id.linearchild4).gone();
                        aqChildReg.id(R.id.linearchild6).gone();

                        aqChildReg.id(R.id.imgview3).background(R.drawable.ic_add_box);
                        aqChildReg.id(R.id.imgview2).background(R.drawable.ic_add_box);
                        aqChildReg.id(R.id.imgview1).background(R.drawable.ic_add_box);
                        aqChildReg.id(R.id.imgview6).background(R.drawable.ic_add_box);
                        aqChildReg.id(R.id.imgview4).background(R.drawable.ic_add_box);


                    }
                }
                break;

                case R.id.llchl6: {
                    if (aqChildReg.id(R.id.linearchild6).getView().getVisibility() == View.VISIBLE) {
                        aqChildReg.id(R.id.linearchild6).gone();
                        aqChildReg.id(R.id.imgview6).background(R.drawable.ic_add_box);
                    } else {
                        aqChildReg.id(R.id.linearchild6).visible();
                        aqChildReg.id(R.id.imgview6).background(R.drawable.ic_indeterminate_check_box);

                        aqChildReg.id(R.id.linearchild2).gone();
                        aqChildReg.id(R.id.linearchild3).gone();
                        aqChildReg.id(R.id.linearchild1).gone();
                        aqChildReg.id(R.id.linearchild4).gone();
                        aqChildReg.id(R.id.linearchild5).gone();

                        aqChildReg.id(R.id.imgview3).background(R.drawable.ic_add_box);
                        aqChildReg.id(R.id.imgview2).background(R.drawable.ic_add_box);
                        aqChildReg.id(R.id.imgview1).background(R.drawable.ic_add_box);
                        aqChildReg.id(R.id.imgview5).background(R.drawable.ic_add_box);
                        aqChildReg.id(R.id.imgview4).background(R.drawable.ic_add_box);


                    }
                }
                break;

                case R.id.trparentheading:

                    if (aqChildReg.id(R.id.llparent).getView().getVisibility() == View.VISIBLE) {
                        aqChildReg.id(R.id.llparent).gone();
                        aqChildReg.id(R.id.imgviewparent).background(R.drawable.ic_add_box);
                    } else {
                        aqChildReg.id(R.id.llparent).visible();
                        aqChildReg.id(R.id.imgviewparent).background(R.drawable.ic_indeterminate_check_box);
                    }
                    break;

                case R.id.rdmother:
                    aqChildReg.id(R.id.trmotherdetails).gone();
                    aqChildReg.id(R.id.trage).visible();
                    aqChildReg.id(R.id.tridtype).visible();
                    aqChildReg.id(R.id.trthayicard).visible();
                    aqChildReg.id(R.id.trgurname).gone();
                    aqChildReg.id(R.id.etgurname).text("");

                    break;

                case R.id.rdguardian:
                    aqChildReg.id(R.id.trmotherdetails).visible();
                    aqChildReg.id(R.id.trage).gone();
                    aqChildReg.id(R.id.tridtype).gone();
                    aqChildReg.id(R.id.trthayicard).gone();
                    aqChildReg.id(R.id.trgurname).visible();
                    aqChildReg.id(R.id.etage).text("");
                    aqChildReg.id(R.id.etthaicard).text("");
                    aqChildReg.id(R.id.etname).text("");
                    aqChildReg.id(R.id.rdrchid).checked(true);
                    aqChildReg.id(R.id.tvthaicard).text(getResources().getString(R.string.rchid));
                    break;


                case R.id.chkmotherdeath:
                    if (aqChildReg.id(R.id.chkmotherdeath).isChecked()) {
                        aqChildReg.id(R.id.trduringdel).visible();
                        aqChildReg.id(R.id.tragewhenpassesaway).visible();
                        aqChildReg.id(R.id.trdeathdate).visible();
                        aqChildReg.id(R.id.trfatherremarried).visible();
                    } else {
                        aqChildReg.id(R.id.trduringdel).gone();
                        aqChildReg.id(R.id.tragewhenpassesaway).gone();
                        aqChildReg.id(R.id.trdeathdate).gone();
                        aqChildReg.id(R.id.etagewhenpassedaway).text("");
                        aqChildReg.id(R.id.etdeathdate).text("");
                        aqChildReg.id(R.id.trfatherremarried).gone();
                        aqChildReg.id(R.id.spncasuseofdeathchl).setSelection(0);
                        aqChildReg.id(R.id.trothercausech).gone();
                        aqChildReg.id(R.id.etothercauseofdeath).text("");
                        rgRemarried.clearCheck();
                        rgDuringDel.clearCheck();


                    }
                    break;

                case R.id.rdMenNo1:
                    aqChildReg.id(R.id.trMentalOther1).gone();
                    aqChildReg.id(R.id.etMentalOther1).text("");
                    break;
                case R.id.rdMenYes1:
                    aqChildReg.id(R.id.trMentalOther1).visible();
                    break;
                case R.id.rdPhyNo1:
                    aqChildReg.id(R.id.trPhyOther1).gone();
                    aqChildReg.id(R.id.etPhyOther1).text("");
                    break;
                case R.id.rdPhyYes1:
                    aqChildReg.id(R.id.trPhyOther1).visible();
                    break;


                case R.id.rdMenNo2:
                    aqChildReg.id(R.id.trMentalOther2).gone();
                    aqChildReg.id(R.id.etMentalOther2).text("");
                    break;
                case R.id.rdMenYes2:
                    aqChildReg.id(R.id.trMentalOther2).visible();
                    break;
                case R.id.rdPhyNo2:
                    aqChildReg.id(R.id.trPhyOther2).gone();
                    aqChildReg.id(R.id.etPhyOther2).text("");
                    break;
                case R.id.rdPhyYes2:
                    aqChildReg.id(R.id.trPhyOther2).visible();
                    break;


                case R.id.rdMenNo3:
                    aqChildReg.id(R.id.trMentalOther3).gone();
                    aqChildReg.id(R.id.etMentalOther3).text("");
                    break;
                case R.id.rdMenYes3:
                    aqChildReg.id(R.id.trMentalOther3).visible();
                    break;
                case R.id.rdPhyNo3:
                    aqChildReg.id(R.id.trPhyOther3).gone();
                    aqChildReg.id(R.id.etPhyOther3).text("");
                    break;
                case R.id.rdPhyYes3:
                    aqChildReg.id(R.id.trPhyOther3).visible();
                    break;


                case R.id.rdMenNo4:
                    aqChildReg.id(R.id.trMentalOther4).gone();
                    aqChildReg.id(R.id.etMentalOther4).text("");
                    break;
                case R.id.rdMenYes4:
                    aqChildReg.id(R.id.trMentalOther4).visible();
                    break;
                case R.id.rdPhyNo4:
                    aqChildReg.id(R.id.trPhyOther4).gone();
                    aqChildReg.id(R.id.etPhyOther4).text("");
                    break;
                case R.id.rdPhyYes4:
                    aqChildReg.id(R.id.trPhyOther4).visible();
                    break;


                case R.id.rdMenNo5:
                    aqChildReg.id(R.id.trMentalOther5).gone();
                    aqChildReg.id(R.id.etMentalOther5).text("");
                    break;
                case R.id.rdMenYes5:
                    aqChildReg.id(R.id.trMentalOther5).visible();
                    break;
                case R.id.rdPhyNo5:
                    aqChildReg.id(R.id.trPhyOther5).gone();
                    aqChildReg.id(R.id.etPhyOther5).text("");
                    break;
                case R.id.rdPhyYes5:
                    aqChildReg.id(R.id.trPhyOther5).visible();
                    break;


                case R.id.rdMenNo6:
                    aqChildReg.id(R.id.trMentalOther6).gone();
                    aqChildReg.id(R.id.etMentalOther6).text("");
                    break;
                case R.id.rdMenYes6:
                    aqChildReg.id(R.id.trMentalOther6).visible();
                    break;
                case R.id.rdPhyNo6:
                    aqChildReg.id(R.id.trPhyOther6).gone();
                    aqChildReg.id(R.id.etPhyOther6).text("");
                    break;
                case R.id.rdPhyYes6:
                    aqChildReg.id(R.id.trPhyOther6).visible();
                    break;


                case R.id.rdrchid:
                    aqChildReg.id(R.id.tvthaicard).text(getResources().getString(R.string.rchid));
                    aqChildReg.id(R.id.etthaicard).text("");
                    break;
                case R.id.rdthayicard:
                    aqChildReg.id(R.id.tvthaicard).text(getResources().getString(R.string.tvthaicard));
                    aqChildReg.id(R.id.etthaicard).text("");
                    break;
                case R.id.btnsave:
                  /*  Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(ChildRegistrationActivity.this, ListOfImmunizationActivity.class);
                    intent.putExtra("appState", appState);
                    intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    intent.putExtra("wFilterStr", "");
                    startActivity(intent);*/
                    if (validateFields()) {
                        Intent intent = new Intent(ChildRegistrationActivity.this, RegisteredChildList.class);
                        displayAlert(getResources().getString(R.string.m099), intent);
                    }
                    break;


                case R.id.btncancel:

                    Intent exit = new Intent(ChildRegistrationActivity.this, MainMenuActivity.class);
                    displayAlert(getResources().getString(R.string.exit), exit);
                    break;

                case R.id.rdduringdelyes:
                    aqChildReg.id(R.id.trcausefdeath).visible();
                    aqChildReg.id(R.id.trothercausech).gone();
                    aqChildReg.id(R.id.etothercauseofdeath).text("");
                    break;
                case R.id.rdduringdelno:
                    aqChildReg.id(R.id.trcausefdeath).gone();
                    aqChildReg.id(R.id.spncasuseofdeathchl).setSelection(0);
                    aqChildReg.id(R.id.trothercausech).visible();
                    aqChildReg.id(R.id.etothercauseofdeath).text("");
                    break;

                case R.id.rdkgs1:
                    aqChildReg.id(R.id.chlWeight1).text("");
                    break;
                case R.id.rdgrams1:
                    aqChildReg.id(R.id.chlWeight1).text("");
                    break;

                case R.id.rdkgs2:
                    aqChildReg.id(R.id.chlWeight2).text("");
                    break;
                case R.id.rdgrams2:
                    aqChildReg.id(R.id.chlWeight2).text("");
                    break;

                case R.id.rdkgs3:
                    aqChildReg.id(R.id.chlWeight3).text("");
                    break;
                case R.id.rdgrams3:
                    aqChildReg.id(R.id.chlWeight3).text("");
                    break;

                case R.id.rdkgs4:
                    aqChildReg.id(R.id.chlWeight4).text("");
                    break;
                case R.id.rdgrams4:
                    aqChildReg.id(R.id.chlWeight4).text("");
                    break;

                case R.id.rdkgs5:
                    aqChildReg.id(R.id.chlWeight5).text("");
                    break;
                case R.id.rdgrams5:
                    aqChildReg.id(R.id.chlWeight5).text("");
                    break;

                case R.id.rdkgs6:
                    aqChildReg.id(R.id.chlWeight6).text("");
                    break;
                case R.id.rdgrams6:
                    aqChildReg.id(R.id.chlWeight6).text("");
                    break;
                default:
                    break;


            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.servicesmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.logout: {
                Intent goToScreen = new Intent(ChildRegistrationActivity.this, LoginActivity.class);
                displayAlert(getResources().getString(R.string.m111), goToScreen);
                return true;
            }
            case R.id.home: {
                Intent home = new Intent(ChildRegistrationActivity.this, MainMenuActivity.class);

                displayAlert(getResources().getString(R.string.exit), home);
                return true;
            }
            case R.id.wlist: {
                Intent goToScreen = new Intent(ChildRegistrationActivity.this, RegisteredWomenActionTabs.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    /**
     * Save Confirmation Alert
     *
     * @param goToScreen
     * @param spanText2
     */
    private void displayAlert(final String spanText2, final Intent goToScreen) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        String strMess;
        if (goToScreen != null)
            strMess = getResources().getString(R.string.m121);
        else
            strMess = getResources().getString(R.string.ok);
        alertDialogBuilder.setMessage(spanText2).setCancelable(false).setNegativeButton(
                strMess, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (goToScreen != null) {


                            if (spanText2.contains(getResources().getString(R.string.save))) {
                                saveData();
                            } else {
                                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                startActivity(goToScreen);
                                if (spanText2.contains("logout")) {
                                    try {
                                        LoginRepository loginRepository = new LoginRepository(databaseHelper);
                                        loginRepository.updateLogoutTime(appState.loginAuditId, appState.sessionUserId);
                                    } catch (Exception e) {
                                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                                    }
                                }
                            }
                        } else
                            dialog.cancel();
                    }
                });

        if (goToScreen != null) {
            alertDialogBuilder.setPositiveButton(getResources().getString(R.string.m122),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
        }
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    //    initialize db object
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void onBackPressed() {

    }

    boolean validateFields() throws Exception {
        //11Apr2021 Bindu MAke select woman mandatory and from resource file
        if (aqChildReg.id(R.id.spnwoman).getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.strselect))) {
            displayAlert(getResources().getString(R.string.pleaseselectwoman), null);
            return false;
        }
        if (aqChildReg.id(R.id.spnwoman).getSelectedItem() != null &&
                aqChildReg.id(R.id.spnwoman).getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.none)) //11Apr2021 Bindu Change to res file
                && !(aqChildReg.id(R.id.rdguardian).isChecked() || aqChildReg.id(R.id.rdmother).isChecked())) {
            displayAlert(getResources().getString(R.string.selectrelationship), null);
            return false;
        } else if (aqChildReg.id(R.id.etname).getText().toString().trim().length() <= 0) {
            displayAlert(getResources().getString(R.string.entermothername), null);
            return false;
        } else if (aqChildReg.id(R.id.chkmotherdeath).isChecked() && aqChildReg.id(R.id.etdeathdate).getText().toString().trim().length() <= 0) {
            displayAlert(getResources().getString(R.string.enterdeathdate), null);
            return false;
        } else if (aqChildReg.id(R.id.etthaicard).getText().toString().trim().length() > 0
                && aqChildReg.id(R.id.etthaicard).getText().toString().trim().length() < 7) {
            Toast.makeText(getApplicationContext(),
                    (getResources().getString(R.string.thayicardno_mustcontain_sevendigits)),
                    Toast.LENGTH_LONG).show();
            aqChildReg.id(R.id.etthaicard).getEditText().requestFocus();
            return false;
        }

        if (aqChildReg.id(R.id.chkmotherdeath).isChecked()) {

            String strselectedDate = aqChildReg.id(R.id.etdeathdate).getText().toString();

            Date selectedDate = new SimpleDateFormat("dd-MM-yyyy").parse(strselectedDate);

            if (selectedDate.after(new Date())) {
                displayAlert(getResources().getString(R.string.date_cannot_af_currentdate), null);
                aqChildReg.id(R.id.etdeathdate).text("");
            } else if (noOfChild > 0 && aqChildReg.id(R.id.dob1).getText().toString() != null &&
                    aqChildReg.id(R.id.dob1).getText().toString().trim().length() > 0 &&
                    new SimpleDateFormat("dd-MM-yyyy").parse
                            (aqChildReg.id(R.id.dob1).getText().toString()).after(selectedDate)) {
                displayAlert(getResources().getString(R.string.date_cannot_before_dob), null);
                aqChildReg.id(R.id.etdeathdate).text("");
            } else if (noOfChild > 1 && aqChildReg.id(R.id.dob2).getText().toString() != null &&
                    aqChildReg.id(R.id.dob2).getText().toString().trim().length() > 0 &&
                    new SimpleDateFormat("dd-MM-yyyy").parse
                            (aqChildReg.id(R.id.dob2).getText().toString()).after(selectedDate)) {
                displayAlert(getResources().getString(R.string.date_cannot_before_dob), null);
                aqChildReg.id(R.id.etdeathdate).text("");
            } else if (noOfChild > 2 && aqChildReg.id(R.id.dob3).getText().toString() != null &&
                    aqChildReg.id(R.id.dob3).getText().toString().trim().length() > 0 &&
                    new SimpleDateFormat("dd-MM-yyyy").parse
                            (aqChildReg.id(R.id.dob3).getText().toString()).after(selectedDate)) {
                displayAlert(getResources().getString(R.string.date_cannot_before_dob), null);
                aqChildReg.id(R.id.etdeathdate).text("");
            } else if (noOfChild > 3 && aqChildReg.id(R.id.dob4).getText().toString() != null &&
                    aqChildReg.id(R.id.dob4).getText().toString().trim().length() > 0 &&
                    new SimpleDateFormat("dd-MM-yyyy").parse
                            (aqChildReg.id(R.id.dob4).getText().toString()).after(selectedDate)) {
                displayAlert(getResources().getString(R.string.date_cannot_before_dob), null);
                aqChildReg.id(R.id.etdeathdate).text("");
            } else if (noOfChild > 4 && aqChildReg.id(R.id.dob5).getText().toString() != null &&
                    aqChildReg.id(R.id.dob5).getText().toString().trim().length() > 0 &&
                    new SimpleDateFormat("dd-MM-yyyy").parse
                            (aqChildReg.id(R.id.dob5).getText().toString()).after(selectedDate)) {
                displayAlert(getResources().getString(R.string.date_cannot_before_dob), null);
                aqChildReg.id(R.id.etdeathdate).text("");
            } else if (noOfChild > 5 && aqChildReg.id(R.id.dob6).getText().toString() != null &&
                    aqChildReg.id(R.id.dob6).getText().toString().trim().length() > 0 &&
                    new SimpleDateFormat("dd-MM-yyyy").parse
                            (aqChildReg.id(R.id.dob6).getText().toString()).after(selectedDate)) {
                displayAlert(getResources().getString(R.string.date_cannot_before_dob), null);
                aqChildReg.id(R.id.etdeathdate).text("");
            } /*else {
                aqChildReg.id(R.id.etdeathdate).text(bldSmpldate);
                // strDob1 = bldSmpldate;

            }*/
        }


        if (aqChildReg.id(R.id.spnnoofchild).getSelectedItemPosition() == 0) {
            displayAlert(getResources().getString(R.string.selectchildren), null);
            return false;
        }

        if (aqChildReg.id(R.id.etage).getText().toString().trim().length() > 0 && aqChildReg.id(R.id.etagewhenpassedaway).getText().toString().trim().length() > 0) {
            int motherAge = 0;

            int motherDeathAge = Integer.parseInt(aqChildReg.id(R.id.etagewhenpassedaway).getText().toString());

            if (aqChildReg.id(R.id.etage).getText().toString().trim().length() > 0) {
                motherAge = Integer.parseInt(aqChildReg.id(R.id.etage).getText().toString());
            }

            if (motherDeathAge > motherAge) {
                displayAlert(getResources().getString(R.string.deathagegtmotherage), null);
                aqChildReg.id(R.id.etagewhenpassedaway).getEditText().requestFocus();
                return false;
            }
        }

        if (noOfChild > 0) {
            reSetUI();
            aqChildReg.id(R.id.linearchild1).visible();
            aqChildReg.id(R.id.imgview1).background(R.drawable.ic_indeterminate_check_box);

            if (aqChildReg.id(R.id.spndeltypechl1).getSelectedItemPosition() == 0) {
                displayAlert(getResources().getString(R.string.selctdeltype), null);
                return false;
            }

            if (aqChildReg.id(R.id.spndelplacech1).getSelectedItemPosition() == 0) {
                displayAlert(getResources().getString(R.string.selectplaceofdel), null);
                return false;
            }

            if (aqChildReg.id(R.id.dob1).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterdob), null);
                aqChildReg.id(R.id.dob1).getEditText().requestFocus();
                return false;
            }
           /* if (aqChildReg.id(R.id.tob1).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.entertob), null);
                aqChildReg.id(R.id.tob1).getEditText().requestFocus();
                return false;
            }*/
            if (aqChildReg.id(R.id.etchlname1).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterchildname), null);
                aqChildReg.id(R.id.etchlname1).getEditText().requestFocus();
                return false;
            }

            if (aqChildReg.id(R.id.spntribalch1).getSelectedItemPosition() == 0) {
                displayAlert(getResources().getString(R.string.m184), null);
                return false;
            }

            if (!(aqChildReg.id(R.id.rdmale1).isChecked() ||
                    aqChildReg.id(R.id.rdfemale1).isChecked() ||
                    aqChildReg.id(R.id.rdambi1).isChecked())) {
                displayAlert(getResources().getString(R.string.selectgender), null);
                aqChildReg.id(R.id.etrch1).getEditText().requestFocus();
                return false;
            }

            if (!(aqChildReg.id(R.id.rdPhyNo1).isChecked() ||
                    aqChildReg.id(R.id.rdPhyYes1).isChecked())) {
                displayAlert(getResources().getString(R.string.selectphydisability), null);
                aqChildReg.id(R.id.etrch1).getEditText().requestFocus();
                return false;
            }

            if (!(aqChildReg.id(R.id.rdMenNo1).isChecked() ||
                    aqChildReg.id(R.id.rdMenYes1).isChecked())) {
                displayAlert(getResources().getString(R.string.selectmentaldisability), null);
                aqChildReg.id(R.id.etrch1).getEditText().requestFocus();
                return false;
            } else if (aqChildReg.id(R.id.rdPhyYes1).isChecked() && aqChildReg.id(R.id.etPhyOther1).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterdescription), null);
                aqChildReg.id(R.id.etPhyOther1).getEditText().requestFocus();
                return false;
            } else if (aqChildReg.id(R.id.rdMenYes1).isChecked() && aqChildReg.id(R.id.etMentalOther1).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterdescription), null);
                aqChildReg.id(R.id.etMentalOther1).getEditText().requestFocus();
                return false;
            }

            if (aqChildReg.id(R.id.chlWeight1).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight1).getText()
                            .toString())<100 && aqChildReg.id(R.id.rdgrams1).isChecked()) {
                aqChildReg.id(R.id.chlWeight1).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }
            else  if (aqChildReg.id(R.id.chlWeight1).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight1).getText()
                            .toString())>100 && aqChildReg.id(R.id.rdkgs1).isChecked()) {
                aqChildReg.id(R.id.chlWeight1).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }


        }

        if (noOfChild > 1) {
            reSetUI();
            aqChildReg.id(R.id.linearchild2).visible();
            aqChildReg.id(R.id.imgview2).background(R.drawable.ic_indeterminate_check_box);


            if (aqChildReg.id(R.id.spndeltypechl2).getSelectedItemPosition() == 0) {
                displayAlert(getResources().getString(R.string.selctdelresult), null);
                return false;
            }

            if (aqChildReg.id(R.id.dob2).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterdob), null);
                aqChildReg.id(R.id.dob2).getEditText().requestFocus();
                return false;
            }
            /*if (aqChildReg.id(R.id.tob2).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.entertob), null);
                aqChildReg.id(R.id.tob2).getEditText().requestFocus();
                return false;
            }*/
            if (aqChildReg.id(R.id.etchlname2).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterchildname), null);
                aqChildReg.id(R.id.etchlname2).getEditText().requestFocus();
                return false;
            }

            if (aqChildReg.id(R.id.spntribalch2).getSelectedItemPosition() == 0) {
                displayAlert(getResources().getString(R.string.m184), null);
                return false;
            }

            if (!(aqChildReg.id(R.id.rdmale2).isChecked() ||
                    aqChildReg.id(R.id.rdfemale2).isChecked() ||
                    aqChildReg.id(R.id.rdambi2).isChecked())) {
                displayAlert(getResources().getString(R.string.selectgender), null);
                return false;
            }


            if (!(aqChildReg.id(R.id.rdPhyNo2).isChecked() ||
                    aqChildReg.id(R.id.rdPhyYes2).isChecked())) {
                displayAlert(getResources().getString(R.string.selectphydisability), null);
                return false;
            }

            if (!(aqChildReg.id(R.id.rdMenNo2).isChecked() ||
                    aqChildReg.id(R.id.rdMenYes2).isChecked())) {
                displayAlert(getResources().getString(R.string.selectmentaldisability), null);
                return false;
            } else if (aqChildReg.id(R.id.rdPhyYes2).isChecked() && aqChildReg.id(R.id.etPhyOther2).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterdescription), null);
                aqChildReg.id(R.id.etPhyOther2).getEditText().requestFocus();
                return false;
            } else if (aqChildReg.id(R.id.rdMenYes2).isChecked() && aqChildReg.id(R.id.etMentalOther2).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterdescription), null);
                aqChildReg.id(R.id.etMentalOther2).getEditText().requestFocus();
                return false;
            }

            if (aqChildReg.id(R.id.chlWeight2).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight2).getText()
                            .toString())<100 && aqChildReg.id(R.id.rdgrams2).isChecked()) {
                aqChildReg.id(R.id.chlWeight2).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }
            else  if (aqChildReg.id(R.id.chlWeight2).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight2).getText()
                            .toString())>100 && aqChildReg.id(R.id.rdkgs2).isChecked()) {
                aqChildReg.id(R.id.chlWeight2).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }
        }
        if (noOfChild > 2) {
            reSetUI();
            aqChildReg.id(R.id.linearchild3).visible();
            aqChildReg.id(R.id.imgview3).background(R.drawable.ic_indeterminate_check_box);


            if (aqChildReg.id(R.id.spndeltypechl3).getSelectedItemPosition() == 0) {
                displayAlert(getResources().getString(R.string.selctdelresult), null);
                return false;
            }


            if (aqChildReg.id(R.id.dob3).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterdob), null);
                aqChildReg.id(R.id.dob3).getEditText().requestFocus();
                return false;
            }
           /* if (aqChildReg.id(R.id.tob3).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.entertob), null);
                aqChildReg.id(R.id.tob3).getEditText().requestFocus();
                return false;
            }*/
            if (aqChildReg.id(R.id.etchlname3).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterchildname), null);
                aqChildReg.id(R.id.etchlname3).getEditText().requestFocus();
                return false;
            }

            if (aqChildReg.id(R.id.spntribalch3).getSelectedItemPosition() == 0) {
                displayAlert(getResources().getString(R.string.m184), null);
                return false;
            }
            if (!(aqChildReg.id(R.id.rdmale3).isChecked() ||
                    aqChildReg.id(R.id.rdfemale3).isChecked() ||
                    aqChildReg.id(R.id.rdambi3).isChecked())) {
                displayAlert(getResources().getString(R.string.selectgender), null);
                return false;
            }

            if (!(aqChildReg.id(R.id.rdPhyNo3).isChecked() ||
                    aqChildReg.id(R.id.rdPhyYes3).isChecked())) {
                displayAlert(getResources().getString(R.string.selectphydisability), null);
                return false;
            }

            if (!(aqChildReg.id(R.id.rdMenNo3).isChecked() ||
                    aqChildReg.id(R.id.rdMenYes3).isChecked())) {
                displayAlert(getResources().getString(R.string.selectmentaldisability), null);
                return false;
            } else if (aqChildReg.id(R.id.rdPhyYes3).isChecked() && aqChildReg.id(R.id.etPhyOther3).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterdescription), null);
                aqChildReg.id(R.id.etPhyOther3).getEditText().requestFocus();
                return false;
            } else if (aqChildReg.id(R.id.rdMenYes3).isChecked() && aqChildReg.id(R.id.etMentalOther3).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterdescription), null);
                aqChildReg.id(R.id.etMentalOther3).getEditText().requestFocus();
                return false;
            }

            if (aqChildReg.id(R.id.chlWeight1).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight1).getText()
                            .toString())<100 && aqChildReg.id(R.id.rdgrams1).isChecked()) {
                aqChildReg.id(R.id.chlWeight1).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }
            else  if (aqChildReg.id(R.id.chlWeight1).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight1).getText()
                            .toString())>100 && aqChildReg.id(R.id.rdkgs1).isChecked()) {
                aqChildReg.id(R.id.chlWeight1).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }

            if (aqChildReg.id(R.id.chlWeight2).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight2).getText()
                            .toString())<100 && aqChildReg.id(R.id.rdgrams2).isChecked()) {
                aqChildReg.id(R.id.chlWeight2).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }
            else  if (aqChildReg.id(R.id.chlWeight2).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight2).getText()
                            .toString())>100 && aqChildReg.id(R.id.rdkgs2).isChecked()) {
                aqChildReg.id(R.id.chlWeight2).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }

            if (aqChildReg.id(R.id.chlWeight3).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight3).getText()
                            .toString())<100 && aqChildReg.id(R.id.rdgrams3).isChecked()) {
                aqChildReg.id(R.id.chlWeight3).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }
            else  if (aqChildReg.id(R.id.chlWeight3).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight3).getText()
                            .toString())>100 && aqChildReg.id(R.id.rdkgs3).isChecked()) {
                aqChildReg.id(R.id.chlWeight3).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }


        }

        if (noOfChild > 3) {
            reSetUI();
            aqChildReg.id(R.id.linearchild4).visible();
            aqChildReg.id(R.id.imgview4).background(R.drawable.ic_indeterminate_check_box);


            if (aqChildReg.id(R.id.spndeltypechl4).getSelectedItemPosition() == 0) {
                displayAlert(getResources().getString(R.string.selctdelresult), null);
                return false;
            }


            if (aqChildReg.id(R.id.dob4).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterdob), null);
                aqChildReg.id(R.id.dob4).getEditText().requestFocus();
                return false;
            }
            /*if (aqChildReg.id(R.id.tob4).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.entertob), null);
                aqChildReg.id(R.id.tob4).getEditText().requestFocus();
                return false;
            }*/
            if (aqChildReg.id(R.id.etchlname4).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterchildname), null);
                aqChildReg.id(R.id.etchlname4).getEditText().requestFocus();
                return false;
            }

            if (aqChildReg.id(R.id.spntribalch4).getSelectedItemPosition() == 0) {
                displayAlert(getResources().getString(R.string.m184), null);
                return false;
            }

            if (!(aqChildReg.id(R.id.rdmale4).isChecked() ||
                    aqChildReg.id(R.id.rdfemale4).isChecked() ||
                    aqChildReg.id(R.id.rdambi4).isChecked())) {
                displayAlert(getResources().getString(R.string.selectgender), null);
                return false;
            }
            if (!(aqChildReg.id(R.id.rdPhyNo4).isChecked() ||
                    aqChildReg.id(R.id.rdPhyYes4).isChecked())) {
                displayAlert(getResources().getString(R.string.selectphydisability), null);
                return false;
            }

            if (!(aqChildReg.id(R.id.rdMenNo4).isChecked() ||
                    aqChildReg.id(R.id.rdMenYes4).isChecked())) {
                displayAlert(getResources().getString(R.string.selectmentaldisability), null);
                return false;
            } else if (aqChildReg.id(R.id.rdPhyYes4).isChecked() && aqChildReg.id(R.id.etPhyOther4).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterdescription), null);
                aqChildReg.id(R.id.etPhyOther4).getEditText().requestFocus();
                return false;
            } else if (aqChildReg.id(R.id.rdMenYes4).isChecked() && aqChildReg.id(R.id.etMentalOther4).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterdescription), null);
                aqChildReg.id(R.id.etMentalOther4).getEditText().requestFocus();
                return false;
            }

            if (aqChildReg.id(R.id.chlWeight1).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight1).getText()
                            .toString())<100 && aqChildReg.id(R.id.rdgrams1).isChecked()) {
                aqChildReg.id(R.id.chlWeight1).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }
            else  if (aqChildReg.id(R.id.chlWeight1).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight1).getText()
                            .toString())>100 && aqChildReg.id(R.id.rdkgs1).isChecked()) {
                aqChildReg.id(R.id.chlWeight1).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }

            if (aqChildReg.id(R.id.chlWeight2).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight2).getText()
                            .toString())<100 && aqChildReg.id(R.id.rdgrams2).isChecked()) {
                aqChildReg.id(R.id.chlWeight2).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }
            else  if (aqChildReg.id(R.id.chlWeight2).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight2).getText()
                            .toString())>100 && aqChildReg.id(R.id.rdkgs2).isChecked()) {
                aqChildReg.id(R.id.chlWeight2).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }

            if (aqChildReg.id(R.id.chlWeight3).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight3).getText()
                            .toString())<100 && aqChildReg.id(R.id.rdgrams3).isChecked()) {
                aqChildReg.id(R.id.chlWeight3).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }
            else  if (aqChildReg.id(R.id.chlWeight3).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight3).getText()
                            .toString())>100 && aqChildReg.id(R.id.rdkgs3).isChecked()) {
                aqChildReg.id(R.id.chlWeight3).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }

            if (aqChildReg.id(R.id.chlWeight4).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight4).getText()
                            .toString())<100 && aqChildReg.id(R.id.rdgrams4).isChecked()) {
                aqChildReg.id(R.id.chlWeight4).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }
            else  if (aqChildReg.id(R.id.chlWeight4).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight4).getText()
                            .toString())>100 && aqChildReg.id(R.id.rdkgs4).isChecked()) {
                aqChildReg.id(R.id.chlWeight4).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }



        }


        if (noOfChild > 4) {
            reSetUI();
            aqChildReg.id(R.id.linearchild5).visible();
            aqChildReg.id(R.id.imgview5).background(R.drawable.ic_indeterminate_check_box);


            if (aqChildReg.id(R.id.spndeltypechl5).getSelectedItemPosition() == 0) {
                displayAlert(getResources().getString(R.string.selctdelresult), null);
                return false;
            }

            if (aqChildReg.id(R.id.dob5).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterdob), null);
                aqChildReg.id(R.id.dob5).getEditText().requestFocus();
                return false;
            }
           /* if (aqChildReg.id(R.id.tob5).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.entertob), null);
                aqChildReg.id(R.id.tob5).getEditText().requestFocus();
                return false;
            }*/
            if (aqChildReg.id(R.id.etchlname5).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterchildname), null);
                aqChildReg.id(R.id.etchlname5).getEditText().requestFocus();
                return false;
            }
            if (aqChildReg.id(R.id.spntribalch5).getSelectedItemPosition() == 0) {
                displayAlert(getResources().getString(R.string.m184), null);
                return false;
            }

            if (!(aqChildReg.id(R.id.rdmale5).isChecked() ||
                    aqChildReg.id(R.id.rdfemale5).isChecked() ||
                    aqChildReg.id(R.id.rdambi5).isChecked())) {
                displayAlert(getResources().getString(R.string.selectgender), null);
                return false;
            }

            if (!(aqChildReg.id(R.id.rdPhyNo5).isChecked() ||
                    aqChildReg.id(R.id.rdPhyYes5).isChecked())) {
                displayAlert(getResources().getString(R.string.selectphydisability), null);
                return false;
            }

            if (!(aqChildReg.id(R.id.rdMenNo5).isChecked() ||
                    aqChildReg.id(R.id.rdMenYes5).isChecked())) {
                displayAlert(getResources().getString(R.string.selectmentaldisability), null);
                return false;
            } else if (aqChildReg.id(R.id.rdPhyYes5).isChecked() && aqChildReg.id(R.id.etPhyOther5).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterdescription), null);
                aqChildReg.id(R.id.etPhyOther5).getEditText().requestFocus();
                return false;
            } else if (aqChildReg.id(R.id.rdMenYes5).isChecked() && aqChildReg.id(R.id.etMentalOther5).getText().toString().trim().length() <= 0) {  //Ramesh 12-aug-2021 Changed id number from 1 to 5
                displayAlert(getResources().getString(R.string.enterdescription), null);
                aqChildReg.id(R.id.etMentalOther5).getEditText().requestFocus();
                return false;
            }

            if (aqChildReg.id(R.id.chlWeight1).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight1).getText()
                            .toString())<100 && aqChildReg.id(R.id.rdgrams1).isChecked()) {
                aqChildReg.id(R.id.chlWeight1).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }
            else  if (aqChildReg.id(R.id.chlWeight1).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight1).getText()
                            .toString())>100 && aqChildReg.id(R.id.rdkgs1).isChecked()) {
                aqChildReg.id(R.id.chlWeight1).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }

            if (aqChildReg.id(R.id.chlWeight2).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight2).getText()
                            .toString())<100 && aqChildReg.id(R.id.rdgrams2).isChecked()) {
                aqChildReg.id(R.id.chlWeight2).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }
            else  if (aqChildReg.id(R.id.chlWeight2).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight2).getText()
                            .toString())>100 && aqChildReg.id(R.id.rdkgs2).isChecked()) {
                aqChildReg.id(R.id.chlWeight2).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }

            if (aqChildReg.id(R.id.chlWeight3).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight3).getText()
                            .toString())<100 && aqChildReg.id(R.id.rdgrams3).isChecked()) {
                aqChildReg.id(R.id.chlWeight3).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }
            else  if (aqChildReg.id(R.id.chlWeight3).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight3).getText()
                            .toString())>100 && aqChildReg.id(R.id.rdkgs3).isChecked()) {
                aqChildReg.id(R.id.chlWeight3).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }

            if (aqChildReg.id(R.id.chlWeight4).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight4).getText()
                            .toString())<100 && aqChildReg.id(R.id.rdgrams4).isChecked()) {
                aqChildReg.id(R.id.chlWeight4).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }
            else  if (aqChildReg.id(R.id.chlWeight4).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight4).getText()
                            .toString())>100 && aqChildReg.id(R.id.rdkgs4).isChecked()) {
                aqChildReg.id(R.id.chlWeight4).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }
            if (aqChildReg.id(R.id.chlWeight5).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight5).getText()
                            .toString())<100 && aqChildReg.id(R.id.rdgrams5).isChecked()) {
                aqChildReg.id(R.id.chlWeight5).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }
            else  if (aqChildReg.id(R.id.chlWeight5).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight5).getText()
                            .toString())>100 && aqChildReg.id(R.id.rdkgs5).isChecked()) {
                aqChildReg.id(R.id.chlWeight5).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }

        }


        if (noOfChild > 5) {
            reSetUI();
            aqChildReg.id(R.id.linearchild6).visible();
            aqChildReg.id(R.id.imgview6).background(R.drawable.ic_indeterminate_check_box);


            if (aqChildReg.id(R.id.spndeltypechl6).getSelectedItemPosition() == 0) {
                displayAlert(getResources().getString(R.string.selctdelresult), null);
                return false;
            }

            if (aqChildReg.id(R.id.dob6).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterdob), null);
                aqChildReg.id(R.id.dob6).getEditText().requestFocus();
                return false;
            }
           /* if (aqChildReg.id(R.id.tob6).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.entertob), null);
                aqChildReg.id(R.id.tob6).getEditText().requestFocus();
                return false;
            }*/
            if (aqChildReg.id(R.id.etchlname6).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterchildname), null);
                aqChildReg.id(R.id.etchlname6).getEditText().requestFocus();
                return false;
            }

            if (aqChildReg.id(R.id.spntribalch6).getSelectedItemPosition() == 0) {
                displayAlert(getResources().getString(R.string.m184), null);
                return false;
            }
            if (!(aqChildReg.id(R.id.rdmale6).isChecked() ||
                    aqChildReg.id(R.id.rdfemale6).isChecked() ||
                    aqChildReg.id(R.id.rdambi6).isChecked())) {
                displayAlert(getResources().getString(R.string.selectgender), null);
                return false;
            }

            if (!(aqChildReg.id(R.id.rdPhyNo6).isChecked() ||
                    aqChildReg.id(R.id.rdPhyYes6).isChecked())) {
                displayAlert(getResources().getString(R.string.selectphydisability), null);
                return false;
            }

            if (!(aqChildReg.id(R.id.rdMenNo6).isChecked() ||
                    aqChildReg.id(R.id.rdMenYes6).isChecked())) {
                displayAlert(getResources().getString(R.string.selectmentaldisability), null);
                return false;
            } else if (aqChildReg.id(R.id.rdPhyYes6).isChecked() && aqChildReg.id(R.id.etPhyOther6).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterdescription), null);
                aqChildReg.id(R.id.etPhyOther6).getEditText().requestFocus();
                return false;
            } else if (aqChildReg.id(R.id.rdMenYes6).isChecked() && aqChildReg.id(R.id.etMentalOther6).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterdescription), null);
                aqChildReg.id(R.id.etMentalOther6).getEditText().requestFocus();
                return false;
            }

            if (aqChildReg.id(R.id.chlWeight1).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight1).getText()
                            .toString())<100 && aqChildReg.id(R.id.rdgrams1).isChecked()) {
                aqChildReg.id(R.id.chlWeight1).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }
            else  if (aqChildReg.id(R.id.chlWeight1).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight1).getText()
                            .toString())>100 && aqChildReg.id(R.id.rdkgs1).isChecked()) {
                aqChildReg.id(R.id.chlWeight1).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }

            if (aqChildReg.id(R.id.chlWeight2).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight2).getText()
                            .toString())<100 && aqChildReg.id(R.id.rdgrams2).isChecked()) {
                aqChildReg.id(R.id.chlWeight2).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }
            else  if (aqChildReg.id(R.id.chlWeight2).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight2).getText()
                            .toString())>100 && aqChildReg.id(R.id.rdkgs2).isChecked()) {
                aqChildReg.id(R.id.chlWeight2).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }

            if (aqChildReg.id(R.id.chlWeight3).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight3).getText()
                            .toString())<100 && aqChildReg.id(R.id.rdgrams3).isChecked()) {
                aqChildReg.id(R.id.chlWeight3).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }
            else  if (aqChildReg.id(R.id.chlWeight3).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight3).getText()
                            .toString())>100 && aqChildReg.id(R.id.rdkgs3).isChecked()) {
                aqChildReg.id(R.id.chlWeight3).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }

            if (aqChildReg.id(R.id.chlWeight4).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight4).getText()
                            .toString())<100 && aqChildReg.id(R.id.rdgrams4).isChecked()) {
                aqChildReg.id(R.id.chlWeight4).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }
            else  if (aqChildReg.id(R.id.chlWeight4).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight4).getText()
                            .toString())>100 && aqChildReg.id(R.id.rdkgs4).isChecked()) {
                aqChildReg.id(R.id.chlWeight4).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }
            if (aqChildReg.id(R.id.chlWeight5).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight5).getText()
                            .toString())<100 && aqChildReg.id(R.id.rdgrams5).isChecked()) {
                aqChildReg.id(R.id.chlWeight5).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }
            else  if (aqChildReg.id(R.id.chlWeight5).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight5).getText()
                            .toString())>100 && aqChildReg.id(R.id.rdkgs5).isChecked()) {
                aqChildReg.id(R.id.chlWeight5).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }

            if (aqChildReg.id(R.id.chlWeight6).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight6).getText()
                    .toString())<100 && aqChildReg.id(R.id.rdgrams6).isChecked()) {
                aqChildReg.id(R.id.chlWeight6).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }
            else  if (aqChildReg.id(R.id.chlWeight6).getText()
                    .toString().trim().length() > 0 &&
                    Double.parseDouble(aqChildReg.id(R.id.chlWeight6).getText()
                            .toString())>100 && aqChildReg.id(R.id.rdkgs6).isChecked()) {
                aqChildReg.id(R.id.chlWeight6).getEditText().
                        setError(getResources().getString
                                (R.string.weightisabnormal));
                return false;
            }
        }

        return true;
    }

    void reSetUI() throws Exception {
        aqChildReg.id(R.id.linearchild1).gone();
        aqChildReg.id(R.id.linearchild2).gone();
        aqChildReg.id(R.id.linearchild3).gone();
        aqChildReg.id(R.id.linearchild4).gone();

        aqChildReg.id(R.id.linearchild5).gone();
        aqChildReg.id(R.id.linearchild6).gone();

        aqChildReg.id(R.id.imgview5).background(R.drawable.ic_add_box);
        aqChildReg.id(R.id.imgview6).background(R.drawable.ic_add_box);

        aqChildReg.id(R.id.imgview1).background(R.drawable.ic_add_box);
        aqChildReg.id(R.id.imgview2).background(R.drawable.ic_add_box);
        aqChildReg.id(R.id.imgview3).background(R.drawable.ic_add_box);
        aqChildReg.id(R.id.imgview4).background(R.drawable.ic_add_box);

    }

    /**
     * This method handle touch listners, calls - displayDatePicker()
     */
    @Override
    public boolean onTouch(View v, MotionEvent event) {

        if (MotionEvent.ACTION_UP == event.getAction()) {
            try {
                switch (v.getId()) {
                    case R.id.dob1: {
                        if (event.getRawX() >= (aqChildReg.id(R.id.dob1).getEditText().getRight()
                                - aqChildReg.id(R.id.dob1).getEditText().getCompoundDrawables()
                                [2].getBounds().width())) {

                            aqChildReg.id(R.id.dob1).text("");
                        } else
                            assignEditTextAndCallDatePicker(aqChildReg.id(R.id.dob1).getEditText());
                        break;
                    }
                    case R.id.dob2: {
                        if (event.getRawX() >= (aqChildReg.id(R.id.dob2).getEditText().getRight()
                                - aqChildReg.id(R.id.dob2).getEditText().getCompoundDrawables()
                                [2].getBounds().width())) {

                            aqChildReg.id(R.id.dob2).text("");
                        } else
                            assignEditTextAndCallDatePicker(aqChildReg.id(R.id.dob2).getEditText());
                        break;
                    }
                    case R.id.dob3: {
                        if (event.getRawX() >= (aqChildReg.id(R.id.dob3).getEditText().getRight()
                                - aqChildReg.id(R.id.dob3).getEditText().getCompoundDrawables()
                                [2].getBounds().width())) {

                            aqChildReg.id(R.id.dob3).text("");
                        } else
                            assignEditTextAndCallDatePicker(aqChildReg.id(R.id.dob3).getEditText());
                        break;
                    }
                    case R.id.dob4: {
                        if (event.getRawX() >= (aqChildReg.id(R.id.dob4).getEditText().getRight()
                                - aqChildReg.id(R.id.dob4).getEditText().getCompoundDrawables()
                                [2].getBounds().width())) {

                            aqChildReg.id(R.id.dob4).text("");
                        } else
                            assignEditTextAndCallDatePicker(aqChildReg.id(R.id.dob4).getEditText());
                        break;
                    }
                    case R.id.dob5: {
                        if (event.getRawX() >= (aqChildReg.id(R.id.dob5).getEditText().getRight()
                                - aqChildReg.id(R.id.dob5).getEditText().getCompoundDrawables()
                                [2].getBounds().width())) {

                            aqChildReg.id(R.id.dob5).text("");
                        } else
                            assignEditTextAndCallDatePicker(aqChildReg.id(R.id.dob5).getEditText());
                        break;
                    }
                    case R.id.dob6: {
                        if (event.getRawX() >= (aqChildReg.id(R.id.dob6).getEditText().getRight()
                                - aqChildReg.id(R.id.dob6).getEditText().getCompoundDrawables()
                                [2].getBounds().width())) {

                            aqChildReg.id(R.id.dob6).text("");
                        } else
                            assignEditTextAndCallDatePicker(aqChildReg.id(R.id.dob6).getEditText());
                        break;
                    }
                    case R.id.etdeathdate: {
                        if (event.getRawX() >= (aqChildReg.id(R.id.etdeathdate).getEditText().getRight()
                                - aqChildReg.id(R.id.etdeathdate).getEditText().getCompoundDrawables()
                                [2].getBounds().width())) {

                            aqChildReg.id(R.id.etdeathdate).text("");
                        } else
                            assignEditTextAndCallDatePicker(aqChildReg.id(R.id.etdeathdate).getEditText());
                        break;
                    }
                    default:
                        break;
                }
            } catch (Exception e) {

                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            }
        }
        return true;
    }


    /**
     * This method opens the Date Dialog and sets Date to respective EditText
     */
    private void assignEditTextAndCallDatePicker(EditText editText) {

        currentEditTextViewforDate = editText;
        String currDate = (editText.getText().length() > 0) ? editText.getText().toString() : "";
        CommonDatePickerFragment.getCommonDate(this, currDate);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {

        try {
            if (view.isShown()) {
                String bldSmpldate = null;
                bldSmpldate = String.format("%02d", day) + "-" + String.format("%02d", month + 1) + "-"
                        + year;

                Date selectedDate = new SimpleDateFormat("dd-MM-yyyy").parse(bldSmpldate);

                Date motherDeathDate = null, maxDate = null;

                String strmaxDate = DateTimeUtil.addDay(DateTimeUtil.getTodaysDate(), 6205);

                if (strmaxDate != null && strmaxDate.trim().length() > 0)
                    maxDate = new SimpleDateFormat("dd-MM-yyyy").parse(strmaxDate);

                if (aqChildReg.id(R.id.etdeathdate).getText().toString() != null &&
                        aqChildReg.id(R.id.etdeathdate).getText().toString().trim().length() > 0)
                    motherDeathDate = new SimpleDateFormat("dd-MM-yyyy").parse(aqChildReg.id(R.id.etdeathdate).getText().toString());

                if (currentEditTextViewforDate == aqChildReg.id(R.id.dob1).getEditText()) {


                    if (selectedDate.after(new Date())) {
                        displayAlert(getResources().getString(R.string.date_cannot_af_currentdate), null);
                        aqChildReg.id(R.id.dob1).text("");
                    } else if (motherDeathDate != null && selectedDate.after(motherDeathDate)) {
                        displayAlert(getResources().getString(R.string.date_cannot_bf_motherdeathdate), null);
                        aqChildReg.id(R.id.dob1).text("");
                    } else if (maxDate != null && selectedDate.before(maxDate)) {
                        displayAlert(getResources().getString(R.string.dobwithinsixteenyears), null);
                        aqChildReg.id(R.id.dob1).text("");
                    } else {
                        aqChildReg.id(R.id.dob1).text(bldSmpldate);
                        strDob1 = bldSmpldate;
                    }
                } else if (currentEditTextViewforDate == aqChildReg.id(R.id.dob2).getEditText()) {


                    if (selectedDate.after(new Date())) {
                        displayAlert(getResources().getString(R.string.date_cannot_af_currentdate), null);
                        aqChildReg.id(R.id.dob2).text("");
                    } else if (motherDeathDate != null && selectedDate.after(motherDeathDate)) {
                        displayAlert(getResources().getString(R.string.date_cannot_bf_motherdeathdate), null);
                        aqChildReg.id(R.id.dob2).text("");
                    } else if (maxDate != null && selectedDate.before(maxDate)) {
                        displayAlert(getResources().getString(R.string.dobwithinsixteenyears), null);
                        aqChildReg.id(R.id.dob2).text("");
                    } else {
                        strDob2 = bldSmpldate;
                        aqChildReg.id(R.id.dob2).text(bldSmpldate);
                    }
                } else if (currentEditTextViewforDate == aqChildReg.id(R.id.dob3).getEditText()) {


                    if (selectedDate.after(new Date())) {
                        displayAlert(getResources().getString(R.string.date_cannot_af_currentdate), null);
                        aqChildReg.id(R.id.dob3).text("");
                    } else if (motherDeathDate != null && selectedDate.after(motherDeathDate)) {
                        displayAlert(getResources().getString(R.string.date_cannot_bf_motherdeathdate), null);
                        aqChildReg.id(R.id.dob3).text("");
                    } else if (maxDate != null && selectedDate.before(maxDate)) {
                        displayAlert(getResources().getString(R.string.dobwithinsixteenyears), null);
                        aqChildReg.id(R.id.dob3).text("");
                    } else {
                        strDob3 = bldSmpldate;
                        aqChildReg.id(R.id.dob3).text(bldSmpldate);
                    }
                } else if (currentEditTextViewforDate == aqChildReg.id(R.id.dob4).getEditText()) {


                    if (selectedDate.after(new Date())) {
                        displayAlert(getResources().getString(R.string.date_cannot_af_currentdate), null);
                        aqChildReg.id(R.id.dob4).text("");
                    } else if (motherDeathDate != null && selectedDate.after(motherDeathDate)) {
                        displayAlert(getResources().getString(R.string.date_cannot_bf_motherdeathdate), null);
                        aqChildReg.id(R.id.dob4).text("");
                    } else if (maxDate != null && selectedDate.before(maxDate)) {
                        displayAlert(getResources().getString(R.string.dobwithinsixteenyears), null);
                        aqChildReg.id(R.id.dob4).text("");
                    } else {
                        strDob4 = bldSmpldate;
                        aqChildReg.id(R.id.dob4).text(bldSmpldate);
                    }
                } else if (currentEditTextViewforDate == aqChildReg.id(R.id.dob5).getEditText()) {


                    if (selectedDate.after(new Date())) {
                        displayAlert(getResources().getString(R.string.date_cannot_af_currentdate), null);
                        aqChildReg.id(R.id.dob5).text("");
                    } else if (motherDeathDate != null && selectedDate.after(motherDeathDate)) {
                        displayAlert(getResources().getString(R.string.date_cannot_bf_motherdeathdate), null);
                        aqChildReg.id(R.id.dob5).text("");
                    } else if (maxDate != null && selectedDate.before(maxDate)) {
                        displayAlert(getResources().getString(R.string.dobwithinsixteenyears), null);
                        aqChildReg.id(R.id.dob5).text("");
                    } else {
                        strDob5 = bldSmpldate;
                        aqChildReg.id(R.id.dob5).text(bldSmpldate);
                    }
                } else if (currentEditTextViewforDate == aqChildReg.id(R.id.dob6).getEditText()) {


                    if (selectedDate.after(new Date())) {
                        displayAlert(getResources().getString(R.string.date_cannot_af_currentdate), null);
                        aqChildReg.id(R.id.dob6).text("");
                    } else if (motherDeathDate != null && selectedDate.after(motherDeathDate)) {
                        displayAlert(getResources().getString(R.string.date_cannot_bf_motherdeathdate), null);
                        aqChildReg.id(R.id.dob6).text("");
                    } else if (maxDate != null && selectedDate.before(maxDate)) {
                        displayAlert(getResources().getString(R.string.dobwithinsixteenyears), null);
                        aqChildReg.id(R.id.dob6).text("");
                    } else {
                        strDob6 = bldSmpldate;
                        aqChildReg.id(R.id.dob6).text(bldSmpldate);
                    }
                }
                if (currentEditTextViewforDate == aqChildReg.id(R.id.etdeathdate).getEditText()) {


                    if (selectedDate.after(new Date())) {
                        displayAlert(getResources().getString(R.string.date_cannot_af_currentdate), null);
                        aqChildReg.id(R.id.etdeathdate).text("");
                    } else if (noOfChild > 0 && aqChildReg.id(R.id.dob1).getText().toString() != null &&
                            aqChildReg.id(R.id.dob1).getText().toString().trim().length() > 0 &&
                            new SimpleDateFormat("dd-MM-yyyy").parse
                                    (aqChildReg.id(R.id.dob1).getText().toString()).after(selectedDate)) {
                        displayAlert(getResources().getString(R.string.date_cannot_before_dob), null);
                        aqChildReg.id(R.id.etdeathdate).text("");
                    } else if (noOfChild > 1 && aqChildReg.id(R.id.dob2).getText().toString() != null &&
                            aqChildReg.id(R.id.dob2).getText().toString().trim().length() > 0 &&
                            new SimpleDateFormat("dd-MM-yyyy").parse
                                    (aqChildReg.id(R.id.dob2).getText().toString()).after(selectedDate)) {
                        displayAlert(getResources().getString(R.string.date_cannot_before_dob), null);
                        aqChildReg.id(R.id.etdeathdate).text("");
                    } else if (noOfChild > 2 && aqChildReg.id(R.id.dob3).getText().toString() != null &&
                            aqChildReg.id(R.id.dob3).getText().toString().trim().length() > 0 &&
                            new SimpleDateFormat("dd-MM-yyyy").parse
                                    (aqChildReg.id(R.id.dob3).getText().toString()).after(selectedDate)) {
                        displayAlert(getResources().getString(R.string.date_cannot_before_dob), null);
                        aqChildReg.id(R.id.etdeathdate).text("");
                    } else if (noOfChild > 3 && aqChildReg.id(R.id.dob4).getText().toString() != null &&
                            aqChildReg.id(R.id.dob4).getText().toString().trim().length() > 0 &&
                            new SimpleDateFormat("dd-MM-yyyy").parse
                                    (aqChildReg.id(R.id.dob4).getText().toString()).after(selectedDate)) {
                        displayAlert(getResources().getString(R.string.date_cannot_before_dob), null);
                        aqChildReg.id(R.id.etdeathdate).text("");
                    } else if (noOfChild > 4 && aqChildReg.id(R.id.dob5).getText().toString() != null &&
                            aqChildReg.id(R.id.dob5).getText().toString().trim().length() > 0 &&
                            new SimpleDateFormat("dd-MM-yyyy").parse
                                    (aqChildReg.id(R.id.dob5).getText().toString()).after(selectedDate)) {
                        displayAlert(getResources().getString(R.string.date_cannot_before_dob), null);
                        aqChildReg.id(R.id.etdeathdate).text("");
                    } else if (noOfChild > 5 && aqChildReg.id(R.id.dob6).getText().toString() != null &&
                            aqChildReg.id(R.id.dob6).getText().toString().trim().length() > 0 &&
                            new SimpleDateFormat("dd-MM-yyyy").parse
                                    (aqChildReg.id(R.id.dob6).getText().toString()).after(selectedDate)) {
                        displayAlert(getResources().getString(R.string.date_cannot_before_dob), null);
                        aqChildReg.id(R.id.etdeathdate).text("");
                    } else {
                        aqChildReg.id(R.id.etdeathdate).text(bldSmpldate);
                        // strDob1 = bldSmpldate;
                    }
                }


            }
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }


    private void setData() throws Exception {

        UserRepository userRepository = new UserRepository(databaseHelper);

        childList = new ArrayList<>();
        tblChlParentDetails = new TblChlParentDetails();
        ChildRepository childRepository = new ChildRepository(databaseHelper);


        transactionHeaderRepository = new TransactionHeaderRepository(databaseHelper);
        transId = transactionHeaderRepository.iCreateNewTrans(user.getUserId(), databaseHelper);


        if (aqChildReg.id(R.id.rdmother).isChecked())
            relation = "Mother";
        else if (aqChildReg.id(R.id.rdguardian).isChecked())
            relation = "Guardian";


        if (aqChildReg.id(R.id.rdrchid).isChecked() &&
                aqChildReg.id(R.id.etthaicard).getText().toString().trim().length() > 0)
            idType = "RCHID";
        else if (aqChildReg.id(R.id.rdthayicard).isChecked())
            idType = "Thayi";
        else if (aqChildReg.id(R.id.etthaicard).getText().toString().trim().length() > 0)
            idType = "RCHID";

        int motherDeath = 0;
        if (aqChildReg.id(R.id.chkmotherdeath).isChecked())
            motherDeath = 1;


        if (aqChildReg.id(R.id.rdremarriedyes).isChecked())
            remarried = "Y";
        else if (aqChildReg.id(R.id.rdremarriedno).isChecked())
            remarried = "N";

        int fatherDeath = 0;
        if (aqChildReg.id(R.id.chkfatherdeath).isChecked())
            fatherDeath = 1;


        tblChlParentDetails.setChlFatherDeath(fatherDeath);
        tblChlParentDetails.setChlFatherName(aqChildReg.id(R.id.etfathername).getText().toString());
        tblChlParentDetails.setChlFatherRemarried(remarried);
        tblChlParentDetails.setChlGuardianName(aqChildReg.id(R.id.etgurname).getText().toString());
        if (aqChildReg.id(R.id.etage).getText().toString().
                trim().length() > 0)
            tblChlParentDetails.setChlMotherAge(Integer.parseInt(aqChildReg.id(R.id.etage).getText().toString()));
        if (aqChildReg.id(R.id.etagewhenpassedaway).getText().toString().
                trim().length() > 0)
            tblChlParentDetails.setChlMotherAgeWhenPassedAway(Integer.parseInt(aqChildReg.id(R.id.etagewhenpassedaway).getText()
                    .toString()));
        tblChlParentDetails.setChlMotherDeathOther(aqChildReg.id(R.id.etothercauseofdeath).getText().toString());
        tblChlParentDetails.setChlMotherDeathReason(aqChildReg.id(R.id.spncasuseofdeathchl).getSelectedItemPosition());
        tblChlParentDetails.setChlMotherId(aqChildReg.id(R.id.etthaicard).getText().toString());
        tblChlParentDetails.setChlMotherIdType(idType);
        tblChlParentDetails.setChlParentId(userRepository.getNewChildParentId(appState.sessionUserId));
        tblChlParentDetails.setChlRelation(relation);
        tblChlParentDetails.setChlMotherName(aqChildReg.id(R.id.etname).getText().toString());
        tblChlParentDetails.setChlUserType(appState.userType);
        tblChlParentDetails.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
        tblChlParentDetails.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());//21May2021 Arpitha

        tblChlParentDetails.setTransId(transId);
        tblChlParentDetails.setUserId(appState.sessionUserId);
        tblChlParentDetails.setChlMotherDeath(motherDeath);

        if (aqChildReg.id(R.id.rdduringdelno).isChecked())
            tblChlParentDetails.setChlMotherDeathDuringDel("N");
        else if (aqChildReg.id(R.id.rdduringdelyes).isChecked())
            tblChlParentDetails.setChlMotherDeathDuringDel("Y");

        tblChlParentDetails.setChlMotherDeathDate(aqChildReg.id(R.id.etdeathdate).getText().toString());


        for (int i = 1; i <= noOfChild; i++) {

            tblChildInfo = new TblChildInfo();

            tblChildInfo.setTransId(transId);


           /* if (i == 1) {

                tblChildInfo.setWomanId("");

                if (aqChildReg.id(R.id.rdmother).isChecked())
                    relation = "Mother";
                else if (aqChildReg.id(R.id.rdguardian).isChecked())
                    relation = "Guardian";
                tblChildInfo.setChlRelation(relation);
//                tblChildInfo.setChlWoman(aqChildReg.id(R.id.spnwoman).getSelectedItemPosition());
                tblChildInfo.setChlMotherName(aqChildReg.id(R.id.etname).getText().toString());

                if (aqChildReg.id(R.id.rdrchid).isChecked())
                    idType = "RCHID";
                else if (aqChildReg.id(R.id.rdthayicard).isChecked())
                    idType = "Thayi";
                tblChildInfo.setChlMotherIdType(idType);
                tblChildInfo.setChlMotherDeathReason(aqChildReg.id(R.id.spncauseofdeath).getSelectedItemPosition());

                tblChildInfo.setChlMotherDeathOther(aqChildReg.id(R.id.etothercauseofdeath).getText().toString());
                tblChildInfo.setChlMotherAgeWhenPassedAway(aqChildReg.id(R.id.etagewhenpassedaway).getText().toString().trim().length()>0?Integer.parseInt(aqChildReg.id(R.id.etagewhenpassedaway).getText().toString()):0);
                tblChildInfo.setChlGuardianName(aqChildReg.id(R.id.etgurname).getText().toString());
                tblChildInfo.setChlMotherAge(aqChildReg.id(R.id.etagewhenpassedaway).getText().toString().trim().length()>0?Integer.parseInt(aqChildReg.id(R.id.etagewhenpassedaway).getText().toString()):0);
                int motherDeath = 0;
                if (aqChildReg.id(R.id.chkmotherdeath).isChecked())
                    motherDeath = 1;
                tblChildInfo.setChlMotherDeath(motherDeath);
                tblChildInfo.setChlFatherName(aqChildReg.id(R.id.etfathername).getText().toString());

                if (aqChildReg.id(R.id.rdremarriedyes).isChecked())
                    remarried = "Y";
                else if (aqChildReg.id(R.id.rdremarriedno).isChecked())
                    remarried = "N";
                tblChildInfo.setChlFatherRemarried(remarried);

                int fatherDeath = 0;
                if (aqChildReg.id(R.id.chkfatherdeath).isChecked())
                    fatherDeath = 1;
                tblChildInfo.setChlFatherDeath(fatherDeath);

                tblChildInfo.setChlMotherId(aqChildReg.id(R.id.etthaicard).getText().toString());


            }*/


//            tblChildInfo.setChlParentId(childRepository.
//                    getNewChildParentId(appState.sessionUserId));
            tblChildInfo.setChlParentId(userRepository.getNewChildParentId(appState.sessionUserId));
            tblChildInfo.setUserId(user.getUserId());
//            tblChildInfo.setChildID(childRepository.
//            getNewChildId(user.getUserId(),i,childRepository
//            .getNewChildParentId(appState.sessionUserId)));
            tblChildInfo.setChildID(userRepository.
                    getNewChildParentId(appState.sessionUserId) + "000" + i);

            tblChildInfo.setChildNo(i);
            int rchId = R.id.etrch;
            int babyNameId = R.id.etchlname;
            int commentsId = R.id.etcomments;
            int complicationsId = R.id.etrch;
            int dobId = R.id.dob;
            int todId = R.id.tob;
            int delTypeId = R.id.spndeltypechl;
            int schoolIdNo = R.id.rdschoolNo;
            int schoolIdYes = R.id.rdschoolYes;
            int PhyId = R.id.etPhyOther;
//             int otherCompl = R.id.etrch;
//             int genderId = R.id.etrch;
            int weightId = R.id.chlWeight;
            int MentId = R.id.etMentalOther;

            int mspnCompl = R.id.mspnchildcompl;
            int otherCompl = R.id.etOtherChildCompl;
            //mani 18June2021
            int delPlaceId= R.id.spndelplacech;

          /*  Object s = aqChildReg.id(mspnCompl+i).getSelectedItem();

            s =  s.toString().replaceAll(",\\s+",",");


             tblChildInfo.setChlComplications(s.toString());*/


            MultiSelectionSpinner msp = (MultiSelectionSpinner) aqChildReg.id(mspnCompl + i).getSpinner();

            String selIds = msp.getSelectedIndicies().toString();

            if (selIds.contains("0,"))
                selIds = selIds.replace("0,", "");
            if (selIds.contains("["))
                selIds = selIds.replace("[", "");
            if (selIds.contains("]"))
                selIds = selIds.replace("]", "");


            tblChildInfo.setChlComplications(selIds);


            tblChildInfo.setChildRCHID(aqChildReg.id(rchId + i).getText().toString());
            String name = aqChildReg.id(babyNameId + i).getText().toString();
            name = name.replaceAll("[';]","");
          //  name = name.replace('"','\s');
            tblChildInfo.setChlBabyname(name);
            String comments = aqChildReg.id(commentsId + i).getText().toString();
            comments = comments.replaceAll("[';]","");
            tblChildInfo.setChlChildComments(comments);
            String otherComp = aqChildReg.id(otherCompl + i).getText().toString();
            otherComp = otherComp.replaceAll("[';]","");
            tblChildInfo.setChlOtherComplications(otherComp);
//            tblChildInfo.setChlDateTimeOfBirth(aqChildReg.id(dobId + i).getText().toString());
            if (i == 1)
                tblChildInfo.setChlDateTimeOfBirth(strDob1);
            else if (i == 2)
                tblChildInfo.setChlDateTimeOfBirth(strDob2);
            else if (i == 3)
                tblChildInfo.setChlDateTimeOfBirth(strDob3);
            else if (i == 4)
                tblChildInfo.setChlDateTimeOfBirth(strDob4);
            else if (i == 5)
                tblChildInfo.setChlDateTimeOfBirth(strDob5);
            else
                tblChildInfo.setChlDateTimeOfBirth(strDob6);
            String[] delTypeby = getResources().getStringArray(R.array.deltypesave);
            String posofDeltype = "";
            posofDeltype = delTypeby[aqChildReg.id(delTypeId + i).getSelectedItemPosition()];

            tblChildInfo.setChlDeliveryType(posofDeltype);
//            tblChildInfo.setChlDeliveryType(aqChildReg.id(delTypeId + i).getSelectedItem().toString());

            if (aqChildReg.id(weightId + i).getText().toString() != null &&
                    aqChildReg.id(weightId + i).getText().toString().trim().length() > 0) {
                String strWeight = aqChildReg.id(weightId + i).getText().toString();

                if (aqChildReg.id(kgsId + i).isChecked() &&
                        strWeight != null && strWeight.trim().length() > 0) //27Apr2021 Arpitha -remove semicoon
                strWeight = String.valueOf(Double.parseDouble(strWeight) * 1000);
                else  if(aqChildReg.id(gramsId + i).isChecked() &&
                        strWeight != null && strWeight.trim().length() > 0)
                    strWeight = strWeight;
                else
                    strWeight = strWeight;


                tblChildInfo.setDelBabyWeight(strWeight);
            }

            int deltypeOther = R.id.etdeltypeotherchl;
            tblChildInfo.setChlDelTypeOther(aqChildReg.id(deltypeOther + i).getText().toString());



            //mani 18June2021
            String[] delPlace = getResources().getStringArray(R.array.delalce);
            String posofDelPlace = "";
            posofDelPlace = delPlace[aqChildReg.id(delPlaceId + i).getSelectedItemPosition()];

            tblChildInfo.setChlDelPlace(posofDelPlace);


            if (aqChildReg.id(schoolIdYes + i).isChecked())
                strSchool = "Y";
            else if (aqChildReg.id(schoolIdNo + i).isChecked())
                strSchool = "N";
            tblChildInfo.setChlSchool(strSchool);

            int phyDisYesId = R.id.rdPhyYes;
            int phyDisNoId = R.id.rdPhyNo;
            if (aqChildReg.id(phyDisYesId + i).isChecked())
                strPhyDis = "Y";
            else if (aqChildReg.id(phyDisNoId + i).isChecked())
                strPhyDis = "N";


            int mentalDisYesId = R.id.rdMenYes;
            int mentalDisNoId = R.id.rdMenNo;
            if (aqChildReg.id(mentalDisYesId + i).isChecked())
                strMentalDis = "Y";
            else if (aqChildReg.id(mentalDisNoId + i).isChecked())
                strMentalDis = "N";

            tblChildInfo.setChlPhysicalDisability(strPhyDis);
            String phy = aqChildReg.id(PhyId + i).getText().toString();
            phy = phy.replaceAll("[';]","");
            String men = aqChildReg.id(MentId + i).getText().toString();
            men = men.replaceAll("[';]","");
            tblChildInfo.setChlOtherPhysicalDisability(phy);
            tblChildInfo.setChlOtherMentalDisability(men);
//            tblChildInfo.setChlOtherComplications("");
            tblChildInfo.setChlMentalDisability(strMentalDis);


            int maleId = R.id.rdmale;
            int femaleId = R.id.rdfemale;
            int ambId = R.id.rdambi;
            if (aqChildReg.id(maleId + i).isChecked())
                strGender = "Male";
            else if (aqChildReg.id(femaleId + i).isChecked())
                strGender = "Female";
            else if (aqChildReg.id(ambId + i).isChecked())
                strGender = "Ambigious";

            tblChildInfo.setChlGender(strGender);
            tblChildInfo.setChlRegDate(DateTimeUtil.getTodaysDate());
            tblChildInfo.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
            tblChildInfo.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());//17-aug-2021 Ramesh
            tblChildInfo.setChlReg(1);
            tblChildInfo.setChlUserType(appState.userType);

            String villageName = (String) aqChildReg.id(R.id.spntribalch + i).getSelectedItem();
            int villageCode = mapVillage.get(villageName);

            tblChildInfo.setChlTribalHamlet(villageCode);
            childList.add(tblChildInfo);
        }


    }

    // To avoid special characters in Input type
    public static InputFilter filter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            // String blockCharacterSet =
            // "~#^|$%*!@/()-'\":;?{}=!$^';?×÷<>{}€£¥₩%&+*.[]1234567890¶";
            String blockCharacterSet = "1234567890~#^|$%*!@()-'\":;?{}=!$^';?×÷<>{}€£¥₩%&+*[]~#^|$%&*!`@_=?;]}{[()\"\\¥®¿Ì™₹°^√π÷×△¶£•¢€♥♡★☆▲▼↑←↓→¤△♂♀||△©||¿¡℅™®₹°¢`•√π¶∆¢°∆¶π√•`";
            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };



}
