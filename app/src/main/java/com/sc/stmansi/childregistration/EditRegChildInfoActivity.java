package com.sc.stmansi.childregistration;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.method.TextKeyListener;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.legacy.app.ActionBarDrawerToggle;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.misc.TransactionManager;
import com.sc.stmansi.childgrowthmonitor.CGMList;
import com.sc.stmansi.childgrowthmonitor.CGMNewVisit;
import com.sc.stmansi.childhomevisit.ChildHomeVisit;
import com.sc.stmansi.childhomevisit.ChildHomeVisitHistory;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.common.MultiSelectionSpinner;
import com.sc.stmansi.common.NavDrawerItem;
import com.sc.stmansi.common.NavDrawerListAdapter;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.deactivate.ChildDeactivateActivity;
import com.sc.stmansi.immunization.ImmunizationListActivity;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.repositories.AuditRepository;
import com.sc.stmansi.repositories.ChildRepository;
import com.sc.stmansi.repositories.FacilityRepository;
import com.sc.stmansi.repositories.LoginRepository;
import com.sc.stmansi.repositories.TransactionHeaderRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.tables.AuditPojo;
import com.sc.stmansi.tables.TblChildInfo;
import com.sc.stmansi.tables.TblChlParentDetails;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.viewprofile.ViewProfileActivity;
import com.sc.stmansi.womanlist.RegisteredWomenActionTabs;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import com.sc.stmansi.R;

public class EditRegChildInfoActivity extends AppCompatActivity implements
        View.OnClickListener {

    LinearLayout linearChild;
    TextView tvheading, tvdob;
    EditText etdob;
    TblChildInfo tblChildInfoFromDb;
    public static AQuery aqEditChild;
    private AppState appState;
    DatabaseHelper databaseHelper;
    private TblInstusers user;
    MultiSelectionSpinner spinner;
    private AuditPojo APJ;
    TblChildInfo tblChildInfo;
    String remarried, relation, strPhyDis, strSchool, strGender, strMentalDis;
    Map<String, Integer> mapVillage;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editchild);

        try {
            Bundle bundle = getIntent().getBundleExtra("globalState");
            appState = bundle.getParcelable("appState");
            databaseHelper = getHelper();
            linearChild = findViewById(R.id.llchild);
            tblChildInfoFromDb = (TblChildInfo) getIntent().getSerializableExtra("tblChildInfo");

            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(false);


            aqEditChild = new AQuery(this);
            spinner = findViewById(R.id.mspnchildcompl);
            UserRepository userRepository = new UserRepository(databaseHelper);

            user = userRepository.getOneAuditedUser(appState.ashaId);
            initiateDrawer();
            drawUI();

            setDataToFields();

//            26Nov2019 Arpitha
            if ((tblChildInfoFromDb.getChlDeactDate() != null &&
                    tblChildInfoFromDb.getChlDeactDate().trim().length() > 0)
                    || user.getIsDeactivated() == 1)
                disableScreen();
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }

    }

    //    initialize db object
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }


    private void setDataToFields() throws SQLException {

        aqEditChild.id(R.id.etchlname1).text(tblChildInfoFromDb.getChlBabyname());

        if (tblChildInfoFromDb.getChlDeliveryType() != null) {
            if (tblChildInfoFromDb.getChlDeliveryType().equalsIgnoreCase("Normal"))
                aqEditChild.id(R.id.spndeltypechl1).setSelection
                        (1);
            else if (tblChildInfoFromDb.getChlDeliveryType().equalsIgnoreCase("Cesarean"))
                aqEditChild.id(R.id.spndeltypechl1).setSelection
                        (2);
            else if (tblChildInfoFromDb.getChlDeliveryType().equalsIgnoreCase("Instrumental"))
                aqEditChild.id(R.id.spndeltypechl1).setSelection
                        (3);
            else if (tblChildInfoFromDb.getChlDeliveryType().equalsIgnoreCase("Other")) {
                aqEditChild.id(R.id.spndeltypechl1).setSelection
                        (4);
                aqEditChild.id(R.id.trdeltypeotherchl1).visible();
                aqEditChild.id(R.id.trdeltypeotherchl1).enabled(false);
                aqEditChild.id(R.id.etdeltypeotherchl1).text(tblChildInfoFromDb.getChlDelTypeOther());
            } else
                aqEditChild.id(R.id.spndeltypechl1).setSelection
                        (0);

        }

        //mani 18Aug2021
        if(tblChildInfoFromDb.getChlDelPlace()!=null)
        {
            aqEditChild.id(R.id.spndelplacech1).setSelection(Arrays.asList(getResources().getStringArray(R.array.deliveryplaceedit)).indexOf(tblChildInfoFromDb.getChlDelPlace()));
        }

//        if(tblChildInfoFromDb.getChlDateTimeOfBirth().contains(" "))
        aqEditChild.id(R.id.dob1).text(tblChildInfoFromDb.getChlDateTimeOfBirth());
        aqEditChild.id(R.id.etrch1).text(tblChildInfoFromDb.getChildRCHID());
        aqEditChild.id(R.id.chlWeight1).text(tblChildInfoFromDb.getDelBabyWeight());
        if(tblChildInfoFromDb.getChlComplications().contains("6"))//17Jun2021 Arpitha - chnage it to 6 from 5
            aqEditChild.id(R.id.trOtherChildCompl1).visible();
        else
            aqEditChild.id(R.id.trOtherChildCompl1).gone();
        aqEditChild.id(R.id.etOtherChildCompl1).text(tblChildInfoFromDb.getChlOtherComplications());
        aqEditChild.id(R.id.etcomments1).text(tblChildInfoFromDb.getChlChildComments());

        if (tblChildInfoFromDb.getChlGender() != null && tblChildInfoFromDb.getChlGender().equalsIgnoreCase("Male"))
            aqEditChild.id(R.id.rdmale1).checked(true);
        else if (tblChildInfoFromDb.getChlGender() != null && tblChildInfoFromDb.getChlGender().equalsIgnoreCase("Female"))
            aqEditChild.id(R.id.rdfemale1).checked(true);
        else if (tblChildInfoFromDb.getChlGender() != null && tblChildInfoFromDb.getChlGender().equalsIgnoreCase("Ambigious"))
            aqEditChild.id(R.id.rdambi1).checked(true);

        if (tblChildInfoFromDb.getChlSchool() != null
                && tblChildInfoFromDb.getChlSchool().equalsIgnoreCase("Y"))
            aqEditChild.id(R.id.rdschoolYes1).checked(true);
        else if (tblChildInfoFromDb.getChlSchool() != null
                && tblChildInfoFromDb.getChlSchool().equalsIgnoreCase("N"))
            aqEditChild.id(R.id.rdschoolNo1).checked(true);

        if (tblChildInfoFromDb.getChlPhysicalDisability() != null &&
                tblChildInfoFromDb.getChlPhysicalDisability()
                        .equalsIgnoreCase("Y")) {
            aqEditChild.id(R.id.rdPhyYes1).checked(true);
            aqEditChild.id(R.id.trPhyOther1).visible();
            aqEditChild.id(R.id.etPhyOther1).text(tblChildInfoFromDb.getChlOtherPhysicalDisability());
        } else if (tblChildInfoFromDb.getChlPhysicalDisability() != null
                && tblChildInfoFromDb.getChlPhysicalDisability()
                .equalsIgnoreCase("N"))
            aqEditChild.id(R.id.rdPhyNo1).checked(true);

        if (tblChildInfoFromDb.getChlMentalDisability() != null && tblChildInfoFromDb.getChlMentalDisability().equalsIgnoreCase("Y")) {
            aqEditChild.id(R.id.rdMenYes1).checked(true);
            aqEditChild.id(R.id.trMentalOther1).visible();
            aqEditChild.id(R.id.etMentalOther1).text(tblChildInfoFromDb.getChlOtherMentalDisability());

        } else if (tblChildInfoFromDb.getChlMentalDisability() != null && tblChildInfoFromDb.getChlMentalDisability().equalsIgnoreCase("N"))
            aqEditChild.id(R.id.rdMenNo1).checked(true);

        // aqEditChild.id(R.id.etchlname1).enabled(false);
        aqEditChild.id(R.id.spndeltypechl1).enabled(false);
        aqEditChild.id(R.id.etdob1).enabled(false);
        if (tblChildInfoFromDb.getChildRCHID() != null && tblChildInfoFromDb.getChildRCHID().trim().length() > 0)
            aqEditChild.id(R.id.etrch1).enabled(false);
        aqEditChild.id(R.id.chlWeight1).enabled(false);
        aqEditChild.id(R.id.rdmale1).enabled(false);
        aqEditChild.id(R.id.rdfemale1).enabled(false);
        aqEditChild.id(R.id.rdambi1).enabled(false);
        aqEditChild.id(R.id.spntribalch1).enabled(false);

        //mani 23Aug2021
        aqEditChild.id(R.id.spndelplacech1).enabled(false);

       /* aqEditChild.id(R.id.rdschoolNo1).enabled(false);
        aqEditChild.id(R.id.rdschoolYes1).enabled(false);
        aqEditChild.id(R.id.rdphyNoch1).enabled(false);
        aqEditChild.id(R.id.rdphyYesch1).enabled(false);
        aqEditCselectedIds = {int[1]@4867} hild.id(R.id.rdMenNo1).enabled(false);
        aqEditChild.id(R.id.rdMenYes1).enabled(false);*/

        String[] childCompl = tblChildInfoFromDb.getChlComplications().split(",");


        String[] motherCompl = tblChildInfoFromDb.getChlComplications().split(",");


        int[] selectedIds = new int[motherCompl.length];

        if (motherCompl != null && motherCompl.length > 0) {
            for (int i = 0; i < motherCompl.length; i++) {
                if(motherCompl[i]!=null && motherCompl[i].trim().length()>0)
                selectedIds[i] = Integer.parseInt(motherCompl[i].trim());
            }
        }

        int chlCompl = 0;

        MultiSelectionSpinner compl = (MultiSelectionSpinner) aqEditChild.id(R.id.mspnchildcompl1)
                .getSpinner();

        compl.setSelection(selectedIds);


        FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
        mapVillage = facilityRepository.getPlaceMap();


        int villageCode = getKey(mapVillage, "" + tblChildInfoFromDb.getChlTribalHamlet());
//        aqEditChild.id(R.id.spnvillage).setSelection(villageCode);

        aqEditChild.id(R.id.spntribalch1).setSelection(villageCode);

//       aqEditChild.id(R.id.mspnchildcompl1).setSelection(childCompl);


        ChildRepository childRepository = new ChildRepository(databaseHelper);


        List<TblChlParentDetails> tblChlParentDetailsList = childRepository.getParentDetails(tblChildInfoFromDb.getChlParentId());

        if (tblChlParentDetailsList != null && tblChlParentDetailsList.size() > 0) {
            TblChlParentDetails tblChlParentDetails = tblChlParentDetailsList.get(0);


            aqEditChild.id(R.id.etname).text(tblChlParentDetails.getChlMotherName());
            aqEditChild.id(R.id.etage).text("" + tblChlParentDetails.getChlMotherAge());
            aqEditChild.id(R.id.etagewhenpassedaway).text("" + tblChlParentDetails.getChlMotherAgeWhenPassedAway());
            aqEditChild.id(R.id.etfathername).text(tblChlParentDetails.getChlFatherName());
            aqEditChild.id(R.id.etothercauseofdeath).text(tblChlParentDetails.getChlMotherDeathOther());

            if (tblChlParentDetails.getChlMotherDeath() == 1)
                aqEditChild.id(R.id.chkmotherdeath).checked(true);
            else
                aqEditChild.id(R.id.chkmotherdeath).checked(false);

            if (tblChlParentDetails.getChlFatherDeath() == 1)
                aqEditChild.id(R.id.chkfatherdeath).checked(true);
            else
                aqEditChild.id(R.id.chkfatherdeath).checked(false);


            if (tblChlParentDetails.getChlFatherRemarried() != null) {
                if (tblChlParentDetails.getChlFatherRemarried().equalsIgnoreCase("Y"))
                    aqEditChild.id(R.id.rdremarriedyes).checked(true);
                else
                    aqEditChild.id(R.id.rdremarriedno).checked(false);
            }

            if (tblChlParentDetails.getChlMotherIdType() != null) {
                if (tblChlParentDetails.getChlMotherIdType().equalsIgnoreCase("RCHID"))
                    aqEditChild.id(R.id.rdrchid).checked(true);
                else
                    aqEditChild.id(R.id.rdthayicard).checked(false);
            }

            if (tblChlParentDetails.getChlRelation() != null) {
                if (tblChlParentDetails.getChlRelation().equalsIgnoreCase("Mother"))
                    aqEditChild.id(R.id.rdmother).checked(true);
                else
                    aqEditChild.id(R.id.rdguardian).checked(false);
            }


            aqEditChild.id(R.id.rdguardian).enabled(false);
            aqEditChild.id(R.id.rdmother).enabled(false);
            aqEditChild.id(R.id.rdthayicard).enabled(false);
            aqEditChild.id(R.id.rdrchid).enabled(false);
            aqEditChild.id(R.id.rdremarriedyes).enabled(false);
            aqEditChild.id(R.id.rdremarriedno).enabled(false);
            aqEditChild.id(R.id.chkfatherdeath).enabled(false);
            aqEditChild.id(R.id.chkmotherdeath).enabled(false);


            aqEditChild.id(R.id.etname).enabled(false);
            aqEditChild.id(R.id.etage).enabled(false);
            aqEditChild.id(R.id.etagewhenpassedaway).enabled(false);
            aqEditChild.id(R.id.etfathername).enabled(false);
            aqEditChild.id(R.id.etothercauseofdeath).enabled(false);





          /* LinearLayout llParent = findViewById(R.id.llparent);
           disableEnableControls(false, llParent);*/

        }


    }


    void drawUI() throws Exception {


        for (int i = 1; i <= 1; i++) {

            String childNo = "";
            int reservedId = 0;


            TableRow trHeading = new TableRow(this);
            trHeading.setOrientation(LinearLayout.HORIZONTAL);
            trHeading.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            trHeading.setPadding(3, 3, 3, 3);
            trHeading.setDividerPadding(5);
            trHeading.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));

            int llchl = R.id.llchl;
            LinearLayout llchld = new LinearLayout(this);
            llchld.setOrientation(LinearLayout.HORIZONTAL);
            llchld.setOnClickListener(this);
            llchld.setId(reservedId);


            tvheading = new TextView(this);
            tvheading.setText(childNo + getResources().getString(R.string.childdetails));
            tvheading.setWidth(700);
            tvheading.setTypeface(null, Typeface.BOLD);
            tvheading.setTextColor(getResources().getColor(R.color.white));
            tvheading.setTextSize(20);

            int imgId = R.id.imgview;
            ImageView imgview = new ImageView(this);
            imgview.setImageDrawable(getResources().getDrawable(R.drawable.ic_add_box));
            imgview.setId(imgId + i);

            llchld.addView(tvheading);
//            llchld.addView(imgview);


            trHeading.addView(llchld);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, 10, 0, 0);
            trHeading.setLayoutParams(params);

            linearChild.addView(trHeading);


            int llChildId = R.id.linearchild;
            LinearLayout llchild = new LinearLayout(this);
            llchild.setOrientation(LinearLayout.VERTICAL);
            llchild.setId(llChildId + i);
            llchild.setBackground(getResources().getDrawable(R.drawable.edit_text_style_black));


            TableRow trDelType = new TableRow(this);
            trDelType.setOrientation(LinearLayout.HORIZONTAL);
            trDelType.setPadding(5, 5, 5, 5);

            trDelType.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

            TextView tvDelType = new TextView(this);
            tvDelType.setText(getResources().getString(R.string.deliverytype));
            tvDelType.setWidth(395);
            tvDelType.setTypeface(null, Typeface.BOLD);
            tvDelType.setTextColor(getResources().getColor(R.color.black));
            tvDelType.setTextSize(18);

            TextView tvdeltypeMand = new TextView(this);
            tvdeltypeMand.setText("*");
            tvdeltypeMand.setTextColor(getResources().getColor(R.color.red));
            tvdeltypeMand.setTextSize(18);


            ArrayAdapter<String> adapterDelType = new ArrayAdapter<String>(this,
                    R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.deltype));
            adapterDelType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            int delTypeId = R.id.spndeltypechl;
            Spinner spnDelType = new Spinner(this);
            spnDelType.setId(delTypeId + i);
            spnDelType.setDropDownWidth(250);
            spnDelType.setMinimumHeight(50);
            spnDelType.setMinimumWidth(405);
            spnDelType.setAdapter(adapterDelType);

            spnDelType.setBackground(getResources().getDrawable(R.drawable.selector_slim_spinner));


            trDelType.addView(tvdeltypeMand);
            trDelType.addView(tvDelType);
            trDelType.addView(spnDelType);
            llchild.addView(trDelType);


            int delOtherId = R.id.trdeltypeotherchl;
            TableRow trOtherType = new TableRow(this);
            trOtherType.setOrientation(LinearLayout.HORIZONTAL);
            trOtherType.setPadding(5, 5, 5, 5);
            trOtherType.setId(delOtherId + i);

            trOtherType.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

            TextView tvOtherType = new TextView(this);
            tvOtherType.setText(getResources().getString(R.string.otherdeltype));
            tvOtherType.setWidth(400);
            tvOtherType.setTypeface(null, Typeface.BOLD);
            tvOtherType.setTextColor(getResources().getColor(R.color.black));
            tvOtherType.setTextSize(18);


            int otherTypeId = R.id.etdeltypeotherchl;
            EditText etOtherType = new EditText(this);
            etOtherType.setId(otherTypeId + i);
            etOtherType.setTextColor(getResources().getColor(R.color.black));
            etOtherType.setTextSize(15);
            etOtherType.setWidth(400);
            etOtherType.setPadding(12, 12, 12, 12);
            etOtherType.setBackground(getResources().getDrawable(R.drawable.edittext_style));
            etOtherType.setInputType(InputType.TYPE_CLASS_TEXT);

            int maxLengthOther = 50;
            InputFilter[] FilterArrayOther = new InputFilter[1];
            FilterArrayOther = new InputFilter[1];
            FilterArrayOther[0] = new InputFilter.LengthFilter(maxLengthOther);
            etOtherType.setFilters(FilterArrayOther);


            trOtherType.setVisibility(View.GONE);
            trOtherType.addView(tvOtherType);
            trOtherType.addView(etOtherType);
            llchild.addView(trOtherType);


                       //mani Aug18-2021
            TableRow trDelPlace = new TableRow(this);
            trDelPlace.setOrientation(LinearLayout.HORIZONTAL);
            trDelPlace.setPadding(5, 5, 5, 5);

            trDelPlace.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

            TextView tvPlaceDel = new TextView(this);
            tvPlaceDel.setText(getResources().getString(R.string.tvdeliveryplace));
            tvPlaceDel.setWidth(395);
            tvPlaceDel.setTypeface(null, Typeface.BOLD);
            tvPlaceDel.setTextColor(getResources().getColor(R.color.black));
            tvPlaceDel.setTextSize(18);

            TextView tvdelPlaceMand = new TextView(this);
            tvdelPlaceMand.setText("*");
            tvdelPlaceMand.setTextColor(getResources().getColor(R.color.red));
            tvdelPlaceMand.setTextSize(18);


            ArrayAdapter<String> adapterDelPlace = new ArrayAdapter<String>(this,
                    R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.delalce));
            adapterDelPlace.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            int delPlaceId = R.id.spndelplacech;
            Spinner spnDelPlace = new Spinner(this);
            spnDelPlace.setId(delPlaceId + i);
            spnDelPlace.setDropDownWidth(500);
            spnDelPlace.setMinimumHeight(50);
            spnDelPlace.setMinimumWidth(405);
            spnDelPlace.setAdapter(adapterDelPlace);

            spnDelPlace.setBackground(getResources().getDrawable(R.drawable.selector_slim_spinner));

            trDelPlace.addView(tvdelPlaceMand);
            trDelPlace.addView(tvPlaceDel);
            trDelPlace.addView(spnDelPlace);
            llchild.addView(trDelPlace);

            TableRow trdob = new TableRow(this);
            trdob.setOrientation(LinearLayout.HORIZONTAL);
            trdob.setPadding(0, 5, 5, 5);

            trdob.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

            TextView tvdobMand = new TextView(this);
            tvdobMand.setText("*");
            tvdobMand.setTextColor(getResources().getColor(R.color.red));
            tvdobMand.setTextSize(18);

            tvdob = new TextView(this);
            tvdob.setText(getResources().getString(R.string.tvdob));
            tvdob.setWidth(400);
            tvdob.setTypeface(null, Typeface.BOLD);
            tvdob.setTextColor(getResources().getColor(R.color.black));
            tvdob.setTextSize(18);


            int ids = R.id.dob;
            etdob = new EditText(this);
            etdob.setId(ids + i);
            etdob.setTextColor(getResources().getColor(R.color.black));
            etdob.setTextSize(15);
            etdob.setWidth(400);
            etdob.setPadding(12, 12, 12, 12);
            etdob.setBackground(getResources().getDrawable(R.drawable.edittext_style));
            etdob.setInputType(InputType.TYPE_DATETIME_VARIATION_DATE);
//            etdob.setOnTouchListener(this);
            etdob.setFocusable(false);
//            etdob.setText(DateTimeUtil.getTodaysDate());


            TextView tvspace = new TextView(this);
            tvspace.setWidth(3);
            tvspace.setTypeface(null, Typeface.BOLD);
            tvspace.setTextColor(getResources().getColor(R.color.black));
            tvspace.setTextSize(18);

//            int tobIds = R.id.tob;
//            EditText ettob = new EditText(this);
//            ettob.setId(tobIds + i);
//            ettob.setTextColor(getResources().getColor(R.color.black));
//            ettob.setTextSize(15);
//            ettob.setWidth(150);
//            ettob.setPadding(12, 12, 12, 12);
//            ettob.setBackground(getResources().getDrawable(R.drawable.edittext_style));
//            ettob.setInputType(InputType.TYPE_DATETIME_VARIATION_TIME);
//            ettob.setText(DateTimeUtil.getCurrentTime());


            trdob.addView(tvdobMand);
            trdob.addView(tvdob);
            trdob.addView(etdob);
//            trdob.addView(tvspace);
//            trdob.addView(ettob);
            llchild.addView(trdob);


            TableRow trName = new TableRow(this);
            trName.setOrientation(LinearLayout.HORIZONTAL);
            trName.setPadding(0, 5, 5, 5);

            trName.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

            TextView tvnameMand = new TextView(this);
            tvnameMand.setText("*");
            tvnameMand.setTextColor(getResources().getColor(R.color.red));
            tvnameMand.setTextSize(18);


            TextView tvName = new TextView(this);
            tvName.setText(getResources().getString(R.string.name));
            tvName.setWidth(400);
            tvName.setTypeface(null, Typeface.BOLD);
            tvName.setTextColor(getResources().getColor(R.color.black));
            tvName.setTextSize(18);


            int nameIds = R.id.etchlname;
            EditText etName = new EditText(this);
            etName.setId(nameIds + i);
            etName.setTextColor(getResources().getColor(R.color.black));
            etName.setTextSize(15);
            etName.setWidth(400);
            etName.setPadding(12, 12, 12, 12);
            etName.setBackground(getResources().getDrawable(R.drawable.edittext_style));
            etName.setInputType(InputType.TYPE_CLASS_TEXT);

//            int maxLengthName = 50;
//            InputFilter[] FilterArrayName = new InputFilter[1];
//            FilterArrayName[0] = new InputFilter.LengthFilter(maxLengthName);
//            etName.setFilters(FilterArrayName);
            etName.setKeyListener(TextKeyListener.getInstance());
            etName.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(45)});



            trName.addView(tvnameMand);
            trName.addView(tvName);
            trName.addView(etName);
            llchild.addView(trName);


            TableRow trRch = new TableRow(this);
            trRch.setOrientation(LinearLayout.HORIZONTAL);
            trRch.setPadding(0, 5, 5, 5);

            trRch.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));


            TextView tvRch = new TextView(this);
            tvRch.setText(getResources().getString(R.string.rchid));
//            tvRch.setId(id);
            tvRch.setWidth(410);
            tvRch.setTypeface(null, Typeface.BOLD);
            tvRch.setTextColor(getResources().getColor(R.color.black));
            tvRch.setTextSize(18);


            int rchIds = R.id.etrch;

            EditText etRch = new EditText(this);
            etRch.setId(rchIds + i);
            etRch.setTextColor(getResources().getColor(R.color.black));
            etRch.setTextSize(15);
            etRch.setWidth(400);
            etRch.setPadding(12, 12, 12, 12);
            etRch.setBackground(getResources().getDrawable(R.drawable.edittext_style));
            etRch.setInputType(InputType.TYPE_CLASS_NUMBER);

            int maxLengthRch = 15;
            //    InputFilter[] FilterArrayRch = new InputFilter[1];
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(maxLengthRch);
            etRch.setFilters(FilterArray);


            trRch.addView(tvRch);
            trRch.addView(etRch);
            llchild.addView(trRch);


            TableRow trWeight = new TableRow(this);
            trWeight.setOrientation(LinearLayout.HORIZONTAL);
            trWeight.setPadding(5, 5, 5, 5);

            trWeight.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

            TextView tvWeight = new TextView(this);
            tvWeight.setText(getResources().getString(R.string.currentweight));
            tvWeight.setWidth(405);
            tvWeight.setTypeface(null, Typeface.BOLD);
            tvWeight.setTextColor(getResources().getColor(R.color.black));
            tvWeight.setTextSize(18);


            int weightId = R.id.chlWeight;
            EditText etWeight = new EditText(this);
            etWeight.setTextColor(getResources().getColor(R.color.black));
            etWeight.setTextSize(15);
            etWeight.setId(weightId + i);
            etWeight.setWidth(400);
            etWeight.setHint("grams");
            etWeight.setPadding(12, 12, 12, 12);
            etWeight.setBackground(getResources().getDrawable(R.drawable.edittext_style));

            etWeight.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL); //for decimal numbers


            int maxLengthWeight = 8;
            InputFilter[] FilterArrayWeight = new InputFilter[1];
            FilterArrayWeight = new InputFilter[1];
            FilterArrayWeight[0] = new InputFilter.LengthFilter(maxLengthWeight);
            etWeight.setFilters(FilterArrayWeight);


            trWeight.addView(tvWeight);
            trWeight.addView(etWeight);
            llchild.addView(trWeight);


            TableRow trtrbalHamlet = new TableRow(this);
            trtrbalHamlet.setOrientation(LinearLayout.HORIZONTAL);
            trtrbalHamlet.setPadding(5, 5, 5, 5);

            trtrbalHamlet.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

            TextView tvTribal = new TextView(this);
            tvTribal.setText(getResources().getString(R.string.tribalhamlet));
            tvTribal.setWidth(390);
            tvTribal.setTypeface(null, Typeface.BOLD);
            tvTribal.setTextColor(getResources().getColor(R.color.black));
            tvTribal.setTextSize(18);

            TextView tvTribalMand = new TextView(this);
            tvTribalMand.setText("*");
            tvTribalMand.setTextColor(getResources().getColor(R.color.red));
            tvTribalMand.setTextSize(18);


            FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
            mapVillage = facilityRepository.getPlaceMap();
            ArrayList<String> villageList = new ArrayList<String>();
            villageList.add(getResources().getString(R.string.selectType));
            for (Map.Entry<String, Integer> village : mapVillage.entrySet())
                villageList.add(village.getKey());

            ArrayAdapter<String> adapterTribal = new ArrayAdapter<String>(this,
                    R.layout.simple_spinner_dropdown_item, villageList);
            adapterTribal.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            int tribalId = R.id.spntribalch;
            Spinner spnTribal = new Spinner(this);
            spnTribal.setId(tribalId + i);
            spnTribal.setDropDownWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
            spnTribal.setMinimumHeight(50);
            spnTribal.setMinimumWidth(405);
            spnTribal.setAdapter(adapterTribal);

            spnTribal.setBackground(getResources().getDrawable(R.drawable.selector_slim_spinner));


            trtrbalHamlet.addView(tvTribalMand);
            trtrbalHamlet.addView(tvTribal);
            trtrbalHamlet.addView(spnTribal);
            llchild.addView(trtrbalHamlet);





            TableRow trGender = new TableRow(this);
            trGender.setOrientation(LinearLayout.HORIZONTAL);
            trGender.setPadding(5, 5, 5, 5);

            trGender.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

            TextView tvGenderMand = new TextView(this);
            tvGenderMand.setText("*");
            tvGenderMand.setTextColor(getResources().getColor(R.color.red));
            tvGenderMand.setTextSize(18);


            TextView tvGender = new TextView(this);
            tvGender.setText(getResources().getString(R.string.tvchildsex));
            tvGender.setWidth(390);
            tvGender.setTypeface(null, Typeface.BOLD);
            tvGender.setTextColor(getResources().getColor(R.color.black));
            tvGender.setTextSize(18);


            RadioGroup rgGender = new RadioGroup(this);
            rgGender.setOrientation(LinearLayout.HORIZONTAL);

            int maleId = R.id.rdmale;
            int femaleId = R.id.rdfemale;
            int ambiId = R.id.rdambi;

            RadioButton rdFemale = new RadioButton(this);
            rdFemale.setText(getResources().getString(R.string.radiodfemale1));
            rdFemale.setTypeface(Typeface.DEFAULT_BOLD);

            RadioButton rdMale = new RadioButton(this);
            rdMale.setText(getResources().getString(R.string.radiodmale1));
            rdMale.setTypeface(Typeface.DEFAULT_BOLD);

            RadioButton rdAmbi = new RadioButton(this);
            rdAmbi.setText(getResources().getString(R.string.ambigious));
            rdAmbi.setTypeface(Typeface.DEFAULT_BOLD);

            rdMale.setId(maleId + i);
            rdFemale.setId(femaleId + i);
            rdAmbi.setId(ambiId + i);

            rgGender.addView(rdFemale);
            rgGender.addView(rdMale);
            rgGender.addView(rdAmbi);


            trGender.addView(tvGenderMand);
            trGender.addView(tvGender);
            trGender.addView(rgGender);
            llchild.addView(trGender);


            TableRow trSchool = new TableRow(this);
            trSchool.setOrientation(LinearLayout.HORIZONTAL);
            trSchool.setPadding(5, 5, 5, 5);

            trSchool.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

            TextView tvSchool = new TextView(this);
            tvSchool.setText(getResources().getString(R.string.goingschool));
            tvSchool.setWidth(400);
            tvSchool.setTypeface(null, Typeface.BOLD);
            tvSchool.setTextColor(getResources().getColor(R.color.black));
            tvSchool.setTextSize(18);


            RadioGroup rgSchool = new RadioGroup(this);
            rgSchool.setOrientation(LinearLayout.HORIZONTAL);

            int scholYesId = R.id.rdschoolYes;
            RadioButton rdSchoolYes = new RadioButton(this);
            rdSchoolYes.setText(getResources().getString(R.string.m121));
            rdSchoolYes.setId(scholYesId + i);
            rdSchoolYes.setTypeface(Typeface.DEFAULT_BOLD);

            int scholNoId = R.id.rdschoolNo;
            RadioButton rdSchoolNo = new RadioButton(this);
            rdSchoolNo.setId(scholNoId + i);
            rdSchoolNo.setText(getResources().getString(R.string.m122));
            rdSchoolNo.setTypeface(Typeface.DEFAULT_BOLD);


            rgSchool.addView(rdSchoolYes);
            rgSchool.addView(rdSchoolNo);


            trSchool.addView(tvSchool);
            trSchool.addView(rgSchool);
            llchild.addView(trSchool);


            TableRow trPhy = new TableRow(this);
            trPhy.setOrientation(LinearLayout.HORIZONTAL);
            trPhy.setPadding(5, 5, 5, 5);

            trPhy.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));


            TextView tvPhyMand = new TextView(this);
            tvPhyMand.setText("*");
            tvPhyMand.setTextColor(getResources().getColor(R.color.red));
            tvPhyMand.setTextSize(18);


            TextView tvPhy = new TextView(this);
            tvPhy.setText(getResources().getString(R.string.physicaldisability));
            tvPhy.setWidth(390);
            tvPhy.setTypeface(null, Typeface.BOLD);
            tvPhy.setTextColor(getResources().getColor(R.color.black));
            tvPhy.setTextSize(18);


            RadioGroup rgPhy = new RadioGroup(this);
            rgPhy.setOrientation(LinearLayout.HORIZONTAL);

            int rdPhyYesId = R.id.rdPhyYes;

            RadioButton rdPhyYes = new RadioButton(this);
            rdPhyYes.setText(getResources().getString(R.string.m121));
            rdPhyYes.setTypeface(Typeface.DEFAULT_BOLD);
            rdPhyYes.setId(rdPhyYesId + i);
            rdPhyYes.setOnClickListener(this);

            int rdPhyNoId = R.id.rdPhyNo;
            RadioButton rdPhyNo = new RadioButton(this);
            rdPhyNo.setText(getResources().getString(R.string.m122));
            rdPhyNo.setTypeface(Typeface.DEFAULT_BOLD);
            rdPhyNo.setId(rdPhyNoId + i);
            rdPhyNo.setOnClickListener(this);


            rgPhy.addView(rdPhyYes);
            rgPhy.addView(rdPhyNo);


            trPhy.addView(tvPhyMand);
            trPhy.addView(tvPhy);
            trPhy.addView(rgPhy);
            llchild.addView(trPhy);


            int trPhyOtherId = R.id.trPhyOther;

            TableRow trOtherPhyDis = new TableRow(this);
            trOtherPhyDis.setOrientation(LinearLayout.HORIZONTAL);
            trOtherPhyDis.setPadding(5, 5, 5, 5);
            trOtherPhyDis.setId(trPhyOtherId + i);
            trOtherPhyDis.setVisibility(View.GONE);

            trOtherPhyDis.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

            TextView tvOtherPhy = new TextView(this);
            tvOtherPhy.setText(getResources().getString(R.string.otherphysicaldisability));
            tvOtherPhy.setWidth(400);
            tvOtherPhy.setTypeface(null, Typeface.BOLD);
            tvOtherPhy.setTextColor(getResources().getColor(R.color.black));
            tvOtherPhy.setTextSize(18);


            int phyId = R.id.etPhyOther;
            EditText etPhy = new EditText(this);
            etPhy.setId(phyId + i);
            etPhy.setTextColor(getResources().getColor(R.color.black));
            etPhy.setTextSize(15);
            etPhy.setWidth(400);
            etPhy.setPadding(12, 12, 12, 12);
            etPhy.setBackground(getResources().getDrawable(R.drawable.edittext_style));
            etPhy.setInputType(InputType.TYPE_CLASS_TEXT);

            int maxLength = 50;
            FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(maxLength);
            etPhy.setFilters(FilterArray);


            trOtherPhyDis.addView(tvOtherPhy);
            trOtherPhyDis.addView(etPhy);
            llchild.addView(trOtherPhyDis);


            TableRow trMentalDis = new TableRow(this);
            trMentalDis.setOrientation(LinearLayout.HORIZONTAL);
            trMentalDis.setPadding(5, 5, 5, 5);

            trMentalDis.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

            TextView tvMentMand = new TextView(this);
            tvMentMand.setText("*");
            tvMentMand.setTextColor(getResources().getColor(R.color.red));
            tvMentMand.setTextSize(18);

            TextView tvMental = new TextView(this);
            tvMental.setText(getResources().getString(R.string.mentaldisability));
            tvMental.setWidth(390);
            tvMental.setTypeface(null, Typeface.BOLD);
            tvMental.setTextColor(getResources().getColor(R.color.black));
            tvMental.setTextSize(18);


            RadioGroup rgMental = new RadioGroup(this);
            rgMental.setOrientation(LinearLayout.HORIZONTAL);

            int rdMenYesId = R.id.rdMenYes;

            RadioButton rdMentalYes = new RadioButton(this);
            rdMentalYes.setText(getResources().getString(R.string.m121));
            rdMentalYes.setTypeface(Typeface.DEFAULT_BOLD);
            rdMentalYes.setOnClickListener(this);
            rdMentalYes.setId(rdMenYesId + i);

            int rdMenNoId = R.id.rdMenNo;
            RadioButton rdMentalNo = new RadioButton(this);
            rdMentalNo.setText(getResources().getString(R.string.m122));
            rdMentalNo.setTypeface(Typeface.DEFAULT_BOLD);
            rdMentalNo.setOnClickListener(this);
            rdMentalNo.setId(rdMenNoId + i);


            rgMental.addView(rdMentalYes);
            rgMental.addView(rdMentalNo);


            trMentalDis.addView(tvMentMand);
            trMentalDis.addView(tvMental);
            trMentalDis.addView(rgMental);

            llchild.addView(trMentalDis);


            int trMenOther = R.id.trMentalOther;
            TableRow trOtherMentalDis = new TableRow(this);
            trOtherMentalDis.setOrientation(LinearLayout.HORIZONTAL);
            trOtherMentalDis.setPadding(5, 5, 5, 5);
            trOtherMentalDis.setId(trMenOther + i);
            trOtherMentalDis.setVisibility(View.GONE);

            trOtherMentalDis.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

            TextView tvOtherMental = new TextView(this);
            tvOtherMental.setText(getResources().getString(R.string.othermentaldisability));
            tvOtherMental.setWidth(400);
            tvOtherMental.setTypeface(null, Typeface.BOLD);
            tvOtherMental.setTextColor(getResources().getColor(R.color.black));
            tvOtherMental.setTextSize(18);


            int etMentalId = R.id.etMentalOther;
            EditText etMental = new EditText(this);
            etMental.setId(etMentalId + i);
            etMental.setTextColor(getResources().getColor(R.color.black));
            etMental.setTextSize(15);
            etMental.setWidth(400);
            etMental.setPadding(12, 12, 12, 12);
            etMental.setBackground(getResources().getDrawable(R.drawable.edittext_style));
            etMental.setInputType(InputType.TYPE_CLASS_TEXT);

            int maxLengthMen = 50;
            InputFilter[] FilterArrayMen = new InputFilter[1];
            FilterArrayMen[0] = new InputFilter.LengthFilter(maxLengthMen);
            etMental.setFilters(FilterArrayMen);


            trOtherMentalDis.addView(tvOtherMental);
            trOtherMentalDis.addView(etMental);
            llchild.addView(trOtherMentalDis);


            TableRow trCompl = new TableRow(this);
            trCompl.setOrientation(LinearLayout.HORIZONTAL);
            trCompl.setPadding(5, 5, 5, 5);

            trCompl.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

            TextView tvComp = new TextView(this);
            tvComp.setText(getResources().getString(R.string.complications));
            tvComp.setWidth(400);
            tvComp.setTypeface(null, Typeface.BOLD);
            tvComp.setTextColor(getResources().getColor(R.color.black));
            tvComp.setTextSize(18);


            int id = R.id.mspnchildcompl;
            MultiSelectionSpinner spnCompl = new MultiSelectionSpinner(this);
            spnCompl.setMinimumHeight(50);
            spnCompl.setMinimumWidth(400);
            spnCompl.setId(id + i);
            spnCompl.setItems(getResources().getStringArray(R.array.childComplications));

            spnCompl.setBackground(getResources().getDrawable(R.drawable.selector_slim_spinner));


            trCompl.addView(tvComp);
            trCompl.addView(spnCompl);
            llchild.addView(trCompl);


            int idOther = R.id.trOtherChildCompl;
            TableRow trOtherChildCompl = new TableRow(this);
            trOtherChildCompl.setOrientation(LinearLayout.HORIZONTAL);
            trOtherChildCompl.setId(idOther+i);
            trOtherChildCompl.setPadding(5, 5, 5, 5);
            trOtherChildCompl.setVisibility(View.GONE);
//            trComments.setId(delOtherId + i);

            trOtherChildCompl.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

            TextView tvothercompl = new TextView(this);
            tvothercompl.setText(getResources().getString(R.string.others));
            tvothercompl.setWidth(400);
            tvothercompl.setTypeface(null, Typeface.BOLD);
            tvothercompl.setTextColor(getResources().getColor(R.color.black));
            tvothercompl.setTextSize(18);


            int OtherCompl = R.id.etOtherChildCompl;
            EditText etOtherCompl = new EditText(this);
            etOtherCompl.setTextColor(getResources().getColor(R.color.black));
            etOtherCompl.setTextSize(15);
            etOtherCompl.setId(OtherCompl + i);
            etOtherCompl.setWidth(400);
            etOtherCompl.setPadding(12, 12, 12, 12);
            etOtherCompl.setBackground(getResources().getDrawable(R.drawable.edittext_style));
            etOtherCompl.setInputType(InputType.TYPE_CLASS_TEXT);

            int maxLengthM = 50;
            InputFilter[] flt = new InputFilter[1];
            flt = new InputFilter[1];
            flt[0] = new InputFilter.LengthFilter(maxLengthM);
            etOtherCompl.setFilters(flt);


            trOtherChildCompl.addView(tvothercompl);
            trOtherChildCompl.addView(etOtherCompl);
            llchild.addView(trOtherChildCompl);


            TableRow trComments = new TableRow(this);
            trComments.setOrientation(LinearLayout.HORIZONTAL);
            trComments.setPadding(5, 5, 5, 5);
//            trComments.setId(delOtherId + i);

            trComments.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

            TextView tvComments = new TextView(this);
            tvComments.setText(getResources().getString(R.string.tvComments));
            tvComments.setWidth(400);
            tvComments.setTypeface(null, Typeface.BOLD);
            tvComments.setTextColor(getResources().getColor(R.color.black));
            tvComments.setTextSize(18);


            int comments = R.id.etcomments;
            EditText etComments = new EditText(this);
            etComments.setTextColor(getResources().getColor(R.color.black));
            etComments.setTextSize(15);
            etComments.setId(comments + i);
            etComments.setWidth(400);
            etComments.setPadding(12, 12, 12, 12);
            etComments.setBackground(getResources().getDrawable(R.drawable.edittext_style));
            etComments.setInputType(InputType.TYPE_CLASS_TEXT);

            int maxLengthC = 50;
            InputFilter[] fltC = new InputFilter[1];
            fltC = new InputFilter[1];
            fltC[0] = new InputFilter.LengthFilter(maxLengthC);
            etComments.setFilters(fltC);


            trComments.addView(tvComments);
            trComments.addView(etComments);
            llchild.addView(trComments);


//            llchild.setVisibility(View.GONE);
            linearChild.addView(llchild);

        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnsave:
                Intent intent = new Intent(EditRegChildInfoActivity.this,
                        RegisteredChildList.class);
                displayAlert(getResources().getString(R.string.m099), intent);

                break;

            case R.id.btncancel:
                Intent exit = new Intent(EditRegChildInfoActivity.this, MainMenuActivity.class);
                exit.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                startActivity(exit);
                break;

            case R.id.rdMenNo1:
                aqEditChild.id(R.id.trMentalOther1).gone();
                aqEditChild.id(R.id.etMentalOther1).text("");
                break;
            case R.id.rdMenYes1:
                aqEditChild.id(R.id.trMentalOther1).visible();
                break;
            case R.id.rdPhyNo1:
                aqEditChild.id(R.id.trPhyOther1).gone();
                aqEditChild.id(R.id.etPhyOther1).text("");
                break;
            case R.id.rdPhyYes1:
                aqEditChild.id(R.id.trPhyOther1).visible();
                break;


            case R.id.rdMenNo2:
                aqEditChild.id(R.id.trMentalOther2).gone();
                aqEditChild.id(R.id.etMentalOther2).text("");
                break;
            case R.id.rdMenYes2:
                aqEditChild.id(R.id.trMentalOther2).visible();
                break;
            case R.id.rdPhyNo2:
                aqEditChild.id(R.id.trPhyOther2).gone();
                aqEditChild.id(R.id.etPhyOther2).text("");
                break;
            case R.id.rdPhyYes2:
                aqEditChild.id(R.id.trPhyOther2).visible();
                break;


            case R.id.rdMenNo3:
                aqEditChild.id(R.id.trMentalOther3).gone();
                aqEditChild.id(R.id.etMentalOther3).text("");
                break;
            case R.id.rdMenYes3:
                aqEditChild.id(R.id.trMentalOther3).visible();
                break;
            case R.id.rdPhyNo3:
                aqEditChild.id(R.id.trPhyOther3).gone();
                aqEditChild.id(R.id.etPhyOther3).text("");
                break;
            case R.id.rdPhyYes3:
                aqEditChild.id(R.id.trPhyOther3).visible();
                break;


            case R.id.rdMenNo4:
                aqEditChild.id(R.id.trMentalOther4).gone();
                aqEditChild.id(R.id.etMentalOther4).text("");
                break;
            case R.id.rdMenYes4:
                aqEditChild.id(R.id.trMentalOther4).visible();
                break;
            case R.id.rdPhyNo4:
                aqEditChild.id(R.id.trPhyOther4).gone();
                aqEditChild.id(R.id.etPhyOther4).text("");
                break;
            case R.id.rdPhyYes4:
                aqEditChild.id(R.id.trPhyOther4).visible();
                break;

        }
    }


    /**
     * Save Confirmation Alert
     *
     * @param goToScreen
     * @param spanText2
     */
    private void displayAlert(final String spanText2, final Intent goToScreen) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        String strButtonText;
        if (goToScreen != null)
            strButtonText = getResources().getString(R.string.m121);
        else
            strButtonText = getResources().getString(R.string.ok);
        alertDialogBuilder.setMessage(spanText2).setCancelable(false).setNegativeButton(
                strButtonText, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        try {
                            if (goToScreen != null) {

                                if (spanText2.contains(getResources().getString(R.string.save))) {

                                    updateData();

                                } else {
                                    goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                    startActivity(goToScreen);
                                    if (spanText2.contains("logout")) {
                                        try {
                                            LoginRepository loginRepository = new LoginRepository(databaseHelper);
                                            loginRepository.updateLogoutTime(appState.loginAuditId, appState.sessionUserId);
                                        } catch (Exception e) {
                                            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                                        }
                                    }
                                }

                            } else
                                dialog.cancel();
                        } catch (Exception e) {
                            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                        }
                    }
                });
        if (goToScreen != null) {
            alertDialogBuilder.setPositiveButton(getResources().getString(R.string.m122),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
        }
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }


    //  update details to table
    void updateData() {

        try {

            final TransactionHeaderRepository transRepo = new TransactionHeaderRepository(databaseHelper);
            final int transId = transRepo.iCreateNewTrans(appState.sessionUserId, databaseHelper);

            TransactionManager.callInTransaction(databaseHelper.getConnectionSource(), new Callable<Void>() {
                public Void call() throws Exception {//29Aug2019 Arpitha - transaction manager

                    String upSqlChild = updateDelData(transId);


                    if (upSqlChild != null && upSqlChild != null) {   //10Aug2019 - Bindu - check for update st and then cal update, else exit to reg women
                        transRepo.iNewRecordTransNew(appState.sessionUserId, transId, "tblaudittrail", databaseHelper);


                        ChildRepository childRepo = new ChildRepository(databaseHelper);
                        int sql = childRepo.updateChildData(user.getUserId(), upSqlChild, transId, databaseHelper);

                        if (sql > 0) {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.childprofileupdated), Toast.LENGTH_LONG).show();

                            // MainMenuActivity.callSyncMtd(); //12Aug2019 - Bindu - Comment autosync
                            Intent exit = new Intent(EditRegChildInfoActivity.this, RegisteredChildList.class);
                            exit.putExtra("appState", appState);
                            exit.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            exit.putExtra("wFilterStr", "");
                            startActivity(exit);
                        } else {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.m113), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha

                        }
                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.nochangesmadetoedit), Toast.LENGTH_LONG).show();
                        Intent exit = new Intent(EditRegChildInfoActivity.this, RegisteredChildList.class);
                        exit.putExtra("appState", appState);
                        exit.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        exit.putExtra("wFilterStr", "");
                        startActivity(exit);

                    }
                    return null;
                }
            });


        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.m113), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private String updateDelData(int transId) throws Exception {

        tblChildInfo = new TblChildInfo();

       /* String  motherCompl = aqEditChild.id(R.id.mspnchildcompl1).getSelectedItem().toString();
        motherCompl =  motherCompl.replaceAll(",\\s+",",");
        tblChildInfo.setChlComplications(motherCompl);*/


        String selIds = "";


        MultiSelectionSpinner comp = (MultiSelectionSpinner) aqEditChild.id(R.id.mspnchildcompl1).getSpinner();
        selIds = comp.getSelectedIndicies().toString();

        if (selIds.contains("0,"))
            selIds = selIds.replace("0,", "");
        if (selIds.contains("["))
            selIds = selIds.replace("[", "");
        if (selIds.contains("]"))
            selIds = selIds.replace("]", "");

        tblChildInfo.setChlComplications(selIds);


        tblChildInfo.setChlChildComments(aqEditChild.id(R.id.etcomments1).getText().toString());


        if (aqEditChild.id(R.id.rdschoolYes1).isChecked())
            strSchool = "Y";
        else if (aqEditChild.id(R.id.rdschoolNo1).isChecked())
            strSchool = "N";
        tblChildInfo.setChlSchool(strSchool);

        int phyDisYesId = R.id.rdPhyYes;
        int phyDisNoId = R.id.rdPhyNo;
        if (aqEditChild.id(R.id.rdPhyYes1).isChecked())
            strPhyDis = "Y";
        else if (aqEditChild.id(R.id.rdPhyNo1).isChecked())
            strPhyDis = "N";


        if (aqEditChild.id(R.id.rdMenYes1).isChecked())
            strMentalDis = "Y";
        else if (aqEditChild.id(R.id.rdMenNo1).isChecked())
            strMentalDis = "N";

        tblChildInfo.setChlPhysicalDisability(strPhyDis);
        tblChildInfo.setChlOtherPhysicalDisability(aqEditChild.id(R.id.etPhyOther1).getText().toString());
        tblChildInfo.setChlOtherMentalDisability(aqEditChild.id(R.id.etMentalOther1).getText().toString());
        tblChildInfo.setChlMentalDisability(strMentalDis);
        tblChildInfo.setChlOtherComplications(aqEditChild.id(R.id.etOtherChildCompl1).getText().toString());
        tblChildInfo.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());

        tblChildInfo.setChildRCHID(aqEditChild.id(R.id.etrch1).getText().toString());

        tblChildInfo.setChlBabyname(aqEditChild.id(R.id.etchlname1).getText().toString());


        return checkForAudit(transId);
    }

    String checkForAudit(int transId) throws Exception {
        String addString = "", delSql = "";


        if (addString == "")
            addString = addString + " chlChildname = " + (char) 34 + tblChildInfo.getChlBabyname() + (char) 34 + "";
        else
            addString = addString + " ,chlChildname = " + (char) 34 + tblChildInfo.getChlBabyname() + (char) 34 + "";
        InserttblAuditTrail("chlChildname",
                tblChildInfoFromDb.getChlBabyname(),
                tblChildInfo.getChlBabyname(),
                transId, "tblchildinfo");


        if (addString == "")
            addString = addString + " chlComplications = " + (char) 34 + tblChildInfo.getChlComplications() + (char) 34 + "";
        else
            addString = addString + " ,chlComplications = " + (char) 34 + tblChildInfo.getChlComplications() + (char) 34 + "";
        InserttblAuditTrail("chlComplications",
                tblChildInfoFromDb.getChlComplications(),
                tblChildInfo.getChlComplications(),
                transId, "tblchildinfo");


        if (addString == "")
            addString = addString + " chlOtherComplications = " + (char) 34 + tblChildInfo.getChlOtherComplications() + (char) 34 + "";
        else
            addString = addString + " ,chlOtherComplications = " + (char) 34 + tblChildInfo.getChlOtherComplications() + (char) 34 + "";
        InserttblAuditTrail("chlOtherComplications",
                tblChildInfoFromDb.getChlOtherComplications(),
                tblChildInfo.getChlOtherComplications(),
                transId, "tblchildinfo");


        if (addString == "")
            addString = addString + " chlPhysicalDisability = " + (char) 34 + tblChildInfo.getChlPhysicalDisability() + (char) 34 + "";
        else
            addString = addString + " ,chlPhysicalDisability = " + (char) 34 + tblChildInfo.getChlPhysicalDisability() + (char) 34 + "";
        InserttblAuditTrail("chlPhysicalDisability",
                tblChildInfoFromDb.getChlPhysicalDisability(),
                tblChildInfo.getChlPhysicalDisability(),
                transId, "tblchildinfo");


        if (addString == "")
            addString = addString + " chlGoingToSchool = " + (char) 34 + tblChildInfo.getChlSchool() + (char) 34 + "";
        else
            addString = addString + " ,chlGoingToSchool = " + (char) 34 + tblChildInfo.getChlSchool() + (char) 34 + "";
        InserttblAuditTrail("chlGoingToSchool",
                tblChildInfoFromDb.getChlSchool(),
                tblChildInfo.getChlSchool(),
                transId, "tblchildinfo");


        if (addString == "")
            addString = addString + " chlMentalDisability = " + (char) 34 + tblChildInfo.getChlMentalDisability() + (char) 34 + "";
        else
            addString = addString + " ,chlMentalDisability = " + (char) 34 + tblChildInfo.getChlMentalDisability() + (char) 34 + "";
        InserttblAuditTrail("chlMentalDisability",
                tblChildInfoFromDb.getChlMentalDisability(),
                tblChildInfo.getChlMentalDisability(),
                transId, "tblchildinfo");

        if (addString == "")
            addString = addString + " chlOtherPhysicalDisability = " + (char) 34 + tblChildInfo.getChlOtherPhysicalDisability() + (char) 34 + "";
        else
            addString = addString + " ,chlOtherPhysicalDisability = " + (char) 34 + tblChildInfo.getChlOtherPhysicalDisability() + (char) 34 + "";
        InserttblAuditTrail("chlOtherPhysicalDisability",
                tblChildInfoFromDb.getChlOtherPhysicalDisability(),
                tblChildInfo.getChlOtherPhysicalDisability(),
                transId, "tblchildinfo");

        if (addString == "")
            addString = addString + " chlOtherMentalDisability = " + (char) 34 + tblChildInfo.getChlOtherMentalDisability() + (char) 34 + "";
        else
            addString = addString + " ,chlOtherMentalDisability = " + (char) 34 + tblChildInfo.getChlOtherMentalDisability() + (char) 34 + "";
        InserttblAuditTrail("chlOtherMentalDisability",
                tblChildInfoFromDb.getChlOtherMentalDisability(),
                tblChildInfo.getChlOtherMentalDisability(),
                transId, "tblchildinfo");


        if (addString == "")
            addString = addString + " chlRCHID = " + (char) 34 + tblChildInfo.getChildRCHID() + (char) 34 + "";
        else
            addString = addString + " ,chlRCHID = " + (char) 34 + tblChildInfo.getChildRCHID() + (char) 34 + "";
        InserttblAuditTrail("chlRCHID",
                tblChildInfoFromDb.getChildRCHID(),
                tblChildInfo.getChildRCHID(),
                transId, "tblchildinfo");

        if (addString == "")
            addString = addString + " chlComments = " + (char) 34 + tblChildInfo.getChlChildComments() + (char) 34 + "";
        else
            addString = addString + " ,chlComments = " + (char) 34 + tblChildInfo.getChlChildComments() + (char) 34 + "";
        InserttblAuditTrail("chlComments",
                tblChildInfoFromDb.getChlChildComments(),
                tblChildInfo.getChlChildComments(),
                transId, "tblchildinfo");


        if (addString != null && addString.length() > 0) {
            delSql = "UPDATE tblchildinfo SET ";
            delSql = delSql + addString + " WHERE  UserId = '" + user.getUserId() + "' and " +
                    " chlID = '" + tblChildInfoFromDb.getChildID() + "'";
        }


        return delSql;
    }

    /**
     * Setting AuditTrail pojo, calls - InserttblAuditTrail()
     *
     * @param transId
     */
    private boolean InserttblAuditTrail(String ColumnName, String Old_Value, String
            New_Value, int transId, String strTableName) throws Exception {

        APJ = new AuditPojo();
        APJ.setUserId(user.getUserId());
        APJ.setWomanId(tblChildInfoFromDb.getChildID());
        APJ.setTblName(strTableName);
        APJ.setDateChanged(DateTimeUtil.getTodaysDate());
        APJ.setColumnName(ColumnName);
        APJ.setOld_Value(Old_Value);
        APJ.setNew_Value(New_Value);
        APJ.setTransId(transId);
        APJ.setRecordcreateddate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());

        AuditRepository auditRepository = new AuditRepository(databaseHelper);
        return auditRepository.insertTotblAuditTrailNew(APJ, databaseHelper);

    }


    private void disableEnableControls(boolean enable, ViewGroup vg) {
        for (int i = 0; i < vg.getChildCount(); i++) {
            View child = vg.getChildAt(i);
            child.setEnabled(enable);
            if (child instanceof ViewGroup) {
                disableEnableControls(enable, (ViewGroup) child);
            }
        }
    }


    // return the position of given key in a map
    public static int getKey(Map<String, Integer> mapref, String value) {

        int pos = 0;
        for (Map.Entry<String, Integer> map : mapref.entrySet()) {
            pos++;
            if (map.getValue().toString().equals(value)) {
                return pos;
            }

        }
        return pos;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getSupportActionBar().setTitle((tblChildInfoFromDb.getChlBabyname() +
                " (" +
                tblChildInfoFromDb.getChlGender() + ")"));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer1); //27Sep2019 - Bindu - set drawer and enable true home btn
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);

        aqEditChild.id(R.id.tvWomanName).text((tblChildInfoFromDb.getChlBabyname() +
                " (" +
                tblChildInfoFromDb.getChlGender() + ")"));
//            aqServ.id(R.id.tvInfo1).text(getResources().getString(R.string.tvlmp) + " - " + woman.getRegLMP());
        if (tblChildInfoFromDb.getChlDateTimeOfBirth() != null && tblChildInfoFromDb.getChlDateTimeOfBirth().length() > 0)
            aqEditChild.id(R.id.tvInfo1).text((getResources().getString(R.string.age)
                    + " " + DateTimeUtil.calculateAge(tblChildInfoFromDb.getChlDateTimeOfBirth())));

        aqEditChild.id(R.id.ivWomanImg).gone();

        getMenuInflater().inflate(R.menu.servicesmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case R.id.logout: {
                Intent goToScreen = new Intent(EditRegChildInfoActivity.this, LoginActivity.class);
                displayAlert(getResources().getString(R.string.m111), goToScreen);
                return true;
            }
            case R.id.home: {
                Intent home = new Intent(EditRegChildInfoActivity.this, MainMenuActivity.class);
                home.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                startActivity(home);
                return true;
            }
            case R.id.wlist: {
                Intent goToScreen = new Intent(EditRegChildInfoActivity.this, RegisteredWomenActionTabs.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }

    }


    private void initiateDrawer() throws Exception {
        mDrawerLayout = findViewById(R.id.ett_drawer_layout);
        mDrawerList = findViewById(R.id.ett_left_drawer);

        ArrayList<NavDrawerItem> navDrawerItems = new ArrayList<>();
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.immunizationlist), R.drawable.immunisation));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.addsibling), R.drawable.general_examination_baby));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.viewparentdetails), R.drawable.ic_mother));
        //15May2021 Bindu added CGM
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.gcmadd), R.drawable.registration)); //15MAy2021 Bindu
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.gcmregisterlist), R.drawable.child_growth));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.chlhv), R.drawable.ic_homevisit)); //Mani: 10Sep21
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.chlhvhistory), R.drawable.prenatalhomevisit));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deactivate), R.drawable.deactivate));



        /*navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.services), R.drawable.anm_pending_activities));//01Oct2019 Arpitha

        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.addunplannedservices), R.drawable.anm_pending_activities));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.unplannedserviceslist), R.drawable.anm_pending_activities));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.homevisitadd),
                R.drawable.prenatalhomevisit));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.womanhomevisithistory),
                R.drawable.prenatalhomevisit));*/


        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        mDrawerList.setAdapter(new NavDrawerListAdapter(getApplicationContext(), navDrawerItems));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer,
                R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu(); // creates call to
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                getSupportActionBar().setDisplayShowTitleEnabled(false);
                getSupportActionBar().setDisplayShowCustomEnabled(false);

                int noOfWeeks;
                try {
                    if (tblChildInfo != null) {
                        getSupportActionBar().setTitle((tblChildInfo.getChlBabyname() + "\t"));
                    }
                } catch (Exception e) {


                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                try {
                    invalidateOptionsMenu(); // creates call to
                    getSupportActionBar().setDisplayShowHomeEnabled(false);
                    getSupportActionBar().setDisplayShowTitleEnabled(false);
                    getSupportActionBar().setDisplayShowCustomEnabled(true);
                    getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);

                    aqEditChild.id(R.id.ivWomanImg).getImageView().setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            mDrawerLayout.closeDrawer(mDrawerList);
                        }
                    });

                    aqEditChild.id(R.id.tvWomanName).text((tblChildInfo.getChlBabyname() + "\t"));
                    if (tblChildInfo.getChlDateTimeOfBirth().trim().length() > 0) {
                        aqEditChild.id(R.id.tvInfo1).text(
                                (getResources().getString(R.string.age) + " " +
                                        DateTimeUtil.calculateAge(tblChildInfo.getChlDateTimeOfBirth())));
                    }
                } catch (Exception e) {


                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    /**
     * This method is to sync draw,erToggle and to replace up caret icon to menu
     * icon in Action Bar
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    /**
     * The click listener for ListView in the navigation drawer
     */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            mDrawerLayout.closeDrawer(mDrawerList);
            try {

                switch (position) {

                    case 0:
                        if (tblChildInfoFromDb.getChlDeliveryResult() <= 1) {
                            Intent nextScreen = new Intent(EditRegChildInfoActivity.this, ImmunizationListActivity.class);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                            nextScreen.putExtra("tblChildInfo", tblChildInfoFromDb);
                            startActivity(nextScreen);
                        } else
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.m053), Toast.LENGTH_LONG).show();

                        break;

                    case 1: {
                        //                                26Nov2019 Arpitha
                        if (user.getIsDeactivated() != 1) {
                            Intent addSibling = new Intent(EditRegChildInfoActivity.this, AddSiblingsActivity.class);
                            addSibling.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            addSibling.putExtra("appState", getIntent().getBundleExtra("appState"));
                            if (tblChildInfoFromDb.getChlParentId() != null && tblChildInfoFromDb.getChlParentId().trim().length() > 0)
                                addSibling.putExtra("chlParentId", tblChildInfoFromDb.getChlParentId());
                            else
                                addSibling.putExtra("WomanId", tblChildInfoFromDb.getWomanId());
                            startActivity(addSibling);
                        } else
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.userdeactivated), Toast.LENGTH_LONG).show();

                        break;
                    }
                    case 2: {
                        if (tblChildInfoFromDb.getChlParentId() != null && tblChildInfoFromDb.getChlParentId().trim().length() > 0) {
                            Intent viewParent = new Intent(EditRegChildInfoActivity.this, ViewParentDetails.class);
                            viewParent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            viewParent.putExtra("appState", getIntent().getBundleExtra("appState"));
                            viewParent.putExtra("tblChildInfo", tblChildInfoFromDb);
                            startActivity(viewParent);
                        } else {
                            WomanRepository womanRepository = new WomanRepository(databaseHelper);
                            tblregisteredwomen woman = womanRepository.getRegistartionDetails(tblChildInfo.getWomanId(), tblChildInfo.getUserId());
                            appState.selectedWomanId = tblChildInfo.getWomanId();
                            Intent nextScreen = new Intent(EditRegChildInfoActivity.this, ViewProfileActivity.class);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                            nextScreen.putExtra("woman", woman);
                            startActivity(nextScreen);
//                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.ntapplicable), Toast.LENGTH_LONG).show();
                        }
                        break;
                    }
                    case 7: {
                        Intent nextScreen = new Intent(EditRegChildInfoActivity.this, ChildDeactivateActivity.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                        nextScreen.putExtra("tblChildInfo", tblChildInfoFromDb);
                        startActivity(nextScreen);

                        break;
                    }

                    //Mani 24 July 2021 Checking condition if age is more than 5years
                    case 3:
                    {
                        if (((tblChildInfoFromDb.getChlDeactDate() != null &&
                                tblChildInfoFromDb.getChlDeactDate().trim().length() > 0)
                                || user.getIsDeactivated() == 1)) {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.userdeactivated), Toast.LENGTH_LONG).show();
                        } else {
                            if ((DateTimeUtil.calculateAgeInMonths(tblChildInfoFromDb.getChlDateTimeOfBirth()) >= 60)||tblChildInfoFromDb.getChlDeliveryResult() > 1)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.aboveagewarning), Toast.LENGTH_LONG).show();
                            else {
                                Intent nextScreen = new Intent(EditRegChildInfoActivity.this, CGMNewVisit.class);
                                nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                                nextScreen.putExtra("tblChildInfo", tblChildInfoFromDb);
                                startActivity(nextScreen);
                            }

                        }

                        break;
                    }

                    //15May2021 Bindu add cgm
                    case 4: {
                        Intent nextScreen = new Intent(EditRegChildInfoActivity.this, CGMList.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                        nextScreen.putExtra("tblChildInfo", tblChildInfoFromDb);
                        startActivity(nextScreen);
                        break;
                    }

                    case 5: {
                        if (((tblChildInfoFromDb.getChlDeactDate() != null && tblChildInfoFromDb.getChlDeactDate().trim().length() > 0) || user.getIsDeactivated() == 1)) {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.userdeactivated), Toast.LENGTH_LONG).show();
                        }else {
                            Intent nextScreen = new Intent(EditRegChildInfoActivity.this, ChildHomeVisit.class);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                            nextScreen.putExtra("tblChildInfo", tblChildInfoFromDb);
                            startActivity(nextScreen);
                        }

                        break;
                    }
                    case 6: {
                        Intent nextScreen = new Intent(EditRegChildInfoActivity.this, ChildHomeVisitHistory.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                        nextScreen.putExtra("tblChildInfo", tblChildInfoFromDb);
                        startActivity(nextScreen);

                        break;
                    }
                }

            } catch (Exception e) {


                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            }
        }
    }


    //    Arpitha 26Nov2019
    void disableScreen() {
        LinearLayout llchild = findViewById(R.id.llchild);
        aqEditChild.id(R.id.btnsave).enabled(false);
        disableEnableControls(false, llchild);

    }

    // To avoid special characters in Input type
    public static InputFilter filter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            // String blockCharacterSet =
            // "~#^|$%*!@/()-'\":;?{}=!$^';?×÷<>{}€£¥₩%&+*.[]1234567890¶";
            String blockCharacterSet = "1234567890~#^|$%*!@()-'\":;?{}=!$^';?×÷<>{}€£¥₩%&+*[]~#^|$%&*!`@_=?;]}{[()\"\\¥®¿Ì™₹°^√π÷×△¶£•¢€♥♡★☆▲▼↑←↓→¤△♂♀℃||△©||¿¡℅™®₹°¢`•√π¶∆¢°∆¶π√•`";
            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };

}
