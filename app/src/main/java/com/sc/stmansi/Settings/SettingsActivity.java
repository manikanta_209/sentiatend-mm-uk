package com.sc.stmansi.Settings;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.SwitchCompat;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.misc.TransactionManager;
import com.sc.stmansi.About.AboutActivity;
import com.sc.stmansi.R;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.repositories.LoginRepository;
import com.sc.stmansi.repositories.SettingsRepository;
import com.sc.stmansi.repositories.TransactionHeaderRepository;
import com.sc.stmansi.tables.TblContactDetails;
import com.sc.stmansi.womanlist.RegisteredWomenActionTabs;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Callable;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener {

    Locale locale;
    static AQuery aq;
    SharedPreferences.Editor prefEditor;
    private static AppState appState;
    private static DatabaseHelper databaseHelper;
    //26Nov2019 Arpitha
    boolean isAdded = false;
    static Context context;
    static List<TblContactDetails> contactDetailsList;
    static List<String[]> contactsCountList;//13May2021 Arpitha
    private SwitchCompat switchautosync;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        aq = new AQuery(this);
        RelativeLayout rl = (RelativeLayout) findViewById(R.id.rlsettings);
        switchautosync = findViewById(R.id.switchautosync);
        initializeScreen(rl);
        databaseHelper = getHelper();
        Bundle bundle = getIntent().getBundleExtra("globalState");
        appState = bundle.getParcelable("appState");
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefEditor = prefs.edit();

        context = SettingsActivity.this;

        if (prefs.getBoolean("isEnglish", false)) {
            locale = new Locale("en");
            aq.id(R.id.spSelectLang).setSelection(0);
        } else if (prefs.getBoolean("isKannada", false)) {  //10Nov2019 - Bindu - Add kannada
            locale = new Locale("kn");
            aq.id(R.id.spSelectLang).setSelection(1);
        } else if (prefs.getBoolean("isTelugu", false)) {  //10Nov2019 - Bindu - Add kannada
            locale = new Locale("te");
            aq.id(R.id.spSelectLang).setSelection(2);
        } else {
            locale = new Locale("en"); //04Feb2021 - Bindu - change default to english
            aq.id(R.id.spSelectLang).setSelection(0);
        }


        if (prefs.getBoolean("sms", false))
            aq.id(R.id.chksendsms).checked(true);

//        13May2021 Arpitha
        if (prefs.getBoolean("autosync", false)){
            aq.id(R.id.chkautosync).checked(true);
            switchautosync.setChecked(true);
        }


        int smsType = prefs.getInt("smstype", 0);
        aq.id(R.id.spnsmsauto).getSpinner().setSelection(smsType);


        getContactsData();

        if (checkSimState() == TelephonyManager.SIM_STATE_READY) {
            aq.id(R.id.chksendsms).enabled(true);
            aq.id(R.id.txtnosim).gone();
        } else {
            aq.id(R.id.chksendsms).enabled(false);
            aq.id(R.id.chksendsms).checked(false);
            aq.id(R.id.txtnosim).visible();
            prefEditor.putBoolean("sms", aq.id(R.id.chksendsms).isChecked());
            prefEditor.putBoolean("autosync", aq.id(R.id.chkautosync).isChecked());//13May2021 Arpitha
            prefEditor.putBoolean("autosync",switchautosync.isChecked());
            prefEditor.commit();
            Configuration config = getResources().getConfiguration();
            config.locale = locale;
            getResources().updateConfiguration(config, getResources().getDisplayMetrics());

        }
    }


    /**
     * This is a Recursive method that traverse through the group and subgroup of view
     * Sets text, assigns clickListners
     */
    private ArrayList<View> initializeScreen(View v) {
        if (!(v instanceof ViewGroup)) {
            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);

            //TextView - Assign Labels
            if (v.getClass() == TextView.class || v.getClass() == RadioButton.class) {
                //     aq.id(v.getId()).text(getSS(aq.id(v.getId()).getText().toString()));
                aq.id(v.getId()).text(aq.id(v.getId()).getText().toString());

            }
            if (v.getClass() == ImageButton.class || v.getClass() == ImageView.class || v.getClass() == AppCompatImageButton.class || v.getClass() == AppCompatImageView.class) {
                aq.id(v.getId()).getImageView().setOnClickListener(this);
            }


            return viewArrayList;
        } else if (v instanceof Spinner) {
            aq.id(v.getId()).itemSelected(this, "commonSpinnerClick");
        }

        ArrayList<View> result = new ArrayList<View>();

        ViewGroup viewGroup = (ViewGroup) v;
        for (int i = 0; i < viewGroup.getChildCount(); i++) {

            View child = viewGroup.getChildAt(i);

            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);
            viewArrayList.addAll(initializeScreen(child));

            result.addAll(viewArrayList);
        }
        return result;
    }

    /**
     * This method invokes on Spinner Clicks
     */
    public void commonSpinnerClick(AdapterView<?> adapter, View v, int position, long id) {

        switch (adapter.getId()) {
            case R.id.spSelectLang: {


                prefEditor.putBoolean("isEnglish", false);
                prefEditor.putBoolean("isKannada", false);
                prefEditor.putBoolean("isTelugu", false); //Bindu 02Apr2021

                if (position == 0) {
                    prefEditor.putBoolean("isEnglish", true);
                    locale = new Locale("en");
                    Locale.setDefault(locale);
                } else if (position == 1) {
                    prefEditor.putBoolean("isKannada", true);
                    locale = new Locale("kn");
                    Locale.setDefault(locale);
                } else if (position == 2) {
                    prefEditor.putBoolean("isTelugu", true);
                    locale = new Locale("te");
                    Locale.setDefault(locale);
                }

                break;
            }

            default:
                break;
        }
    }


    @Override
    public void onClick(View view) {
        try {
            switch (view.getId()) {
                case R.id.btnsavesettings: {


                    if (validateFields()) {
                        Intent mainmenu = new Intent(SettingsActivity.this, MainMenuActivity.class);
                        displayAlert(getResources().getString(R.string.m099), mainmenu);
                    } else {
                        prefEditor.commit();
                        Configuration config = getResources().getConfiguration();
                        config.locale = locale;
                        getResources().updateConfiguration(config, getResources().getDisplayMetrics());

                    }
                    //  Toast.makeText(getApplicationContext(), getResources().getString(R.string.settingsupdated), Toast.LENGTH_LONG).show();

                    break;
                }

                case R.id.btncancelsettings: {
                    Intent mainmenu = new Intent(SettingsActivity.this, MainMenuActivity.class);
                    displayAlert(getResources().getString(R.string.exit), mainmenu);
                    break;
                }
                default:
                    break;
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    /**
     * Initializes the Action Bar items
     */
    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        try {

            getMenuInflater().inflate(R.menu.servicesmenu, menu);
            menu.findItem(R.id.logout).setTitle((getResources().getString(R.string.action_logout)));

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Called whenever an item clicked in actionbar
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action buttons
        switch (item.getItemId()) {

            case R.id.home: {
                Intent goToScreen = new Intent(SettingsActivity.this, MainMenuActivity.class);
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }

            case R.id.logout: {
                Intent goToScreen = new Intent(SettingsActivity.this, LoginActivity.class);
                displayAlert(getResources().getString(R.string.m111), goToScreen);
                return true;
            }


            case R.id.wlist: {
                Intent goToScreen = new Intent(SettingsActivity.this, RegisteredWomenActionTabs.class);
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }
            //30Sep2019 - Bindu
            case R.id.about: {
                Intent goToScreen = new Intent(SettingsActivity.this, AboutActivity.class);
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Save Confirmation Alert
     *
     * @param goToScreen
     * @param spanText2
     */
    private void displayAlert(final String spanText2, final Intent goToScreen) {

        String strOkMess;
        if (goToScreen != null)
            strOkMess = getResources().getString(R.string.yes);
        else
            strOkMess = getResources().getString(R.string.ok);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set dialog message
        alertDialogBuilder.setMessage(spanText2).setCancelable(false).setNegativeButton(
                strOkMess, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (goToScreen != null) {

                            if (spanText2.contains(getResources().getString(R.string.save))) {
                                saveSettingsData();
                            } else {
                                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));

                                startActivity(goToScreen);
                                if (spanText2.contains("logout")) {
                                    try {
                                        LoginRepository loginRepository = new LoginRepository(databaseHelper);
                                        loginRepository.updateLogoutTime(appState.loginAuditId, appState.sessionUserId);
                                    } catch (Exception e) {
                                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                                        crashlytics.log(e.getMessage());
                                    }
                                }
                            }
                        } else
                            dialog.cancel();
                    }
                });
        if (goToScreen != null) {
            alertDialogBuilder.setPositiveButton(getResources().getString(R.string.m122),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
        }
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    //	intialize db
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }


    //  26Nov2019 Arpitha
    boolean saveSMSContactDetails() throws SQLException {

        //  if (validateFields()) {


        prefEditor.putBoolean("sms", aq.id(R.id.chksendsms).isChecked());
        prefEditor.putBoolean("autosync", aq.id(R.id.chkautosync).isChecked());//13May2021 Arpitha
        prefEditor.putBoolean("autosync", switchautosync.isChecked());//13May2021 Arpitha
        prefEditor.putInt("smstype",aq.id(R.id.spnsmsauto).getSpinner().getSelectedItemPosition()); // 0 -Select, 1- always send sms, 2- Only when Complication is there
        prefEditor.commit();
        Configuration config = getResources().getConfiguration();
        config.locale = locale;
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());


        if (aq.id(R.id.etsmsphnno).getText().toString().trim().length() > 0) {
            final int transId;
            final TransactionHeaderRepository transRepo = new TransactionHeaderRepository(databaseHelper);
            transId = transRepo.iCreateNewTrans(appState.sessionUserId, databaseHelper);

            TransactionManager.callInTransaction(databaseHelper.getConnectionSource(), new Callable<Void>() {
                @Override
                public Void call() throws Exception {

                    TblContactDetails tblContactDetails = new TblContactDetails();
                    tblContactDetails.setContactDesignation(aq.id(R.id.spndesignation).getSelectedItem().toString());
                    tblContactDetails.setContactNumber(aq.id(R.id.etsmsphnno).getText().toString());
                    tblContactDetails.setContactselectedoption(aq.id(R.id.spnmodule).getSelectedItem().toString());//12May2021 Arpitha
                    tblContactDetails.setContactUserType(appState.userType);
                    tblContactDetails.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
                    tblContactDetails.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
                    tblContactDetails.setTransId(transId);
                    tblContactDetails.setUserId(appState.sessionUserId);

                    int add = databaseHelper.getContactDetailsDao().create(tblContactDetails);

                    if (add > 0) {
                        boolean added = transRepo.iNewRecordTransNew(appState.sessionUserId, transId, "tblcontactdetails", databaseHelper);

                        if (added) {
                            isAdded = true;
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.settingsupdated), Toast.LENGTH_LONG).show();

                        }
                    } else
                        isAdded = false;
                    return null;
                }
            });

            getContactsData();
            aq.id(R.id.etsmsphnno).text("");
            aq.id(R.id.spndesignation).setSelection(0);
            aq.id(R.id.spnmodule).setSelection(0);//12May2021 Arpitha


        }/*else {
//                Toast.makeText(getApplicationContext(), getResources().getString(R.string.settingsupdated), Toast.LENGTH_LONG).show();
//                Intent mainmenu = new Intent(SettingsActivity.this, MainMenuActivity.class);
//                mainmenu.putExtra("globalState", getIntent().getBundleExtra("globalState"));
//                startActivity(mainmenu);
              //  getContactsData();
            }*/

        Toast.makeText(getApplicationContext(), getResources().getString(R.string.settingsupdated), Toast.LENGTH_LONG).show();


        return isAdded;
    }

    //    26Nov2019 Arpitha
    private boolean validateFields() {

//        17May2021 Bindu chk if screen selected and designation chk
        if (aq.id(R.id.spnmodule).getSelectedItemPosition() > 0) {
            if (aq.id(R.id.etsmsphnno).getText().toString().trim().length() <= 0 &&
                    aq.id(R.id.spndesignation).getSelectedItemPosition() == 0) {
                displayAlert(getResources().getString(R.string.selectdesignation), null);
                return false;
            }
        }

        if (aq.id(R.id.spndesignation).getSelectedItemPosition() > 0) {
            if (aq.id(R.id.spnmodule).getSelectedItemPosition() == 0) {
                displayAlert(getResources().getString(R.string.selectscreen), null);
                return false;
            } else if (aq.id(R.id.etsmsphnno).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.entermobilenumber), null);
                return false;
            }
        }

        if (aq.id(R.id.etsmsphnno).getText().toString().trim().length() > 0 &&
                aq.id(R.id.spndesignation).getSelectedItemPosition() == 0) {
            displayAlert(getResources().getString(R.string.selectdesignation), null);
            return false;
        }

//        ArrayList<String> phnNoList = new ArrayList<>();

//        12May2021 Arpitha
        boolean numExits = false;
        for (int i = 0; i < contactDetailsList.size(); i++) {
            if (contactDetailsList.get(i).getContactNumber().
                    equals(aq.id(R.id.etsmsphnno).getText().toString()) &&
                    contactDetailsList.get(i).getContactselectedoption()
                            .equalsIgnoreCase(aq.id(R.id.spnmodule).getSelectedItem().toString())) {
                numExits = true;
            }
        }

        if (numExits) {
            displayAlert(getResources().getString(R.string.phnnuexists), null);
            return false;
        }

//        12May2021 Arpitha
        if (aq.id(R.id.etsmsphnno).getText().toString().trim().length() > 0 && aq.id(R.id.spnmodule).getSelectedItemPosition() == 0) {
            displayAlert(getResources().getString(R.string.selectscreen), null);
            return false;
        }

        return true;
    }

    static void getContactsData() {

        try {


            SettingsRepository settingsRepository = new SettingsRepository(databaseHelper);
            contactDetailsList = settingsRepository.getContactDetails(appState.sessionUserId);
//           13May2021 Arpitha
            contactsCountList = settingsRepository.getContactDetailsCount(appState.sessionUserId);
            boolean addContact = false;
            for (int i = 0; i < contactsCountList.size(); i++) {
                if (Integer.parseInt(contactsCountList.get(0)[1]) > 5)
                    addContact = false;
                else
                    addContact = true;
            }


            if (contactDetailsList != null && contactDetailsList.size() > 0) {
                SettingsAdapter settingsAdapter = new SettingsAdapter(context, R.layout.adaptersettings, contactDetailsList, databaseHelper);
                aq.id(R.id.listcontactdetails).getListView().setAdapter(settingsAdapter);
                aq.id(R.id.listcontactdetails).visible();
                if (addContact) {
                    aq.id(R.id.etsmsphnno).enabled(true);
                    aq.id(R.id.spndesignation).enabled(true);
                    aq.id(R.id.etsmsphnno).background(R.drawable.spinner_bg_enabled);
                    aq.id(R.id.spndesignation).background(R.drawable.spinner_bg_enabled);
                } else {
                    aq.id(R.id.etsmsphnno).enabled(false);
                    aq.id(R.id.spndesignation).enabled(false);
                    aq.id(R.id.etsmsphnno).background(R.drawable.edittext_disable);
                    aq.id(R.id.spndesignation).background(R.drawable.edittext_disable);
                }
            } else {
                aq.id(R.id.listcontactdetails).gone();
                aq.id(R.id.txtheadingsettings).gone();

            }


        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private void saveSettingsData() {
        try {

            if (saveSMSContactDetails()) {//26Nov2019 Arpitha


                getContactsData();
               /* Intent mainmenu = new Intent(SettingsActivity.this, MainMenuActivity.class);
                mainmenu.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                startActivity(mainmenu);*/
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    //  05Dec2019 Arpitha
    public int checkSimState() {
        int simState = -1;
        // This is to know the status of the Sim
        TelephonyManager telMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        simState = telMgr.getSimState();

        return simState;
    }
}

