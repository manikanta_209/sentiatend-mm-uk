package com.sc.stmansi.Settings;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.misc.TransactionManager;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.repositories.AuditRepository;
import com.sc.stmansi.repositories.SettingsRepository;
import com.sc.stmansi.repositories.TransactionHeaderRepository;
import com.sc.stmansi.tables.AuditPojo;
import com.sc.stmansi.tables.TblContactDetails;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import com.sc.stmansi.R;

public class SettingsAdapter extends ArrayAdapter<TblContactDetails> {
    Context context;
    int layoutResourceId;
    List<TblContactDetails> data = new ArrayList<TblContactDetails>();
    DatabaseHelper databaseHelper ;
    AuditPojo APJ;
    String strPhn;

    public SettingsAdapter(Context context, int layoutResourceId,
                           List<TblContactDetails> data, DatabaseHelper databaseHelper) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
        this.databaseHelper = databaseHelper;
    }

    public int getCount() {
        return data.size()+1;
    }



    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View row = convertView;
        RecordHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new RecordHolder();
            holder.txtOpt = (TextView) row.findViewById(R.id.txtopt);
            holder.imageDelete = (ImageView) row.findViewById(R.id.imgdele);
            holder.txtDesig = (TextView) row.findViewById(R.id.txtdesg);
            holder.txtPhn = (TextView) row.findViewById(R.id.txtno);


            row.setTag(holder);
        } else {
            holder = (RecordHolder) row.getTag();
        }

        if(position == 0)
        {
            holder.txtOpt.setText(context.getResources().getString(R.string.screen));//12May2021 Arpitha
            holder.txtPhn.setText(context.getResources().getString(R.string.tvphone));
            holder.txtDesig.setText(context.getResources().getString(R.string.designation));
            holder.txtPhn.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
            holder.txtOpt.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));//12May2021 Arpitha
            holder.txtDesig.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
            holder.imageDelete.setVisibility(View.INVISIBLE);


        }else {
            TblContactDetails item = data.get(position - 1);
           /* if(item.getContactselectedoption()!=null && item.getContactselectedoption().equalsIgnoreCase("Select"))
            holder.txtOpt.setText("");
            else
                holder.txtOpt.setText(item.getContactselectedoption());*/

            holder.txtPhn.setText(item.getContactNumber());
            if(item.getContactselectedoption()!=null &&
                    item.getContactDesignation().equalsIgnoreCase("Select"))
                holder.txtDesig.setText("");
            else
                holder.txtDesig.setText(item.getContactDesignation());

            holder.txtOpt.setText(item.getContactselectedoption());//12May2021 Arpitha

            holder.txtPhn.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
            holder.txtOpt.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
            holder.txtDesig.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
            holder.imageDelete.setVisibility(View.VISIBLE);
        }

        holder.imageDelete.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                try {
                    if(MotionEvent.ACTION_UP == event.getAction()) {
                        strPhn = data.get(position - 1).getContactNumber();
//                    deleteContactDetails(position-1,strPhn);
                        displayAlert(context.getResources().getString(R.string.deletecontact), null, position);
                    }
                }catch (Exception e)
                {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }
                return true;
            }
        });



        return row;

    }


    static class RecordHolder {
        TextView txtOpt;
        ImageView imageDelete;
        TextView txtPhn;
        TextView txtDesig;

    }

    private void deleteContactDetails(final int position, final String strPhn) throws Exception {

        final TransactionHeaderRepository transactionHeaderRepository = new TransactionHeaderRepository(databaseHelper);
        final int transId = transactionHeaderRepository.iCreateNewTrans(data.get(0).getUserId(), databaseHelper);

        final SettingsRepository settingsRepository = new SettingsRepository(databaseHelper);
        TransactionManager.callInTransaction(databaseHelper.getConnectionSource(), new Callable<Object>() {


            @Override
            public Object call() throws Exception {

               String sql =  checkForAudit(transId, strPhn);

                settingsRepository.updateSettingsData(data.get(0).getUserId(),sql,databaseHelper, transId);
                transactionHeaderRepository.iNewRecordTransNew(data.get(0).getUserId(), transId, "tblaudittrail", databaseHelper);


                SettingsActivity.getContactsData();
                return null;
            }
        });

    }

    String checkForAudit(int transId, String strPhn) throws Exception {
        String addString = "", delSql = "";

        if (addString == "")
            addString = addString + " contactDeleted = 1" ;
        else
            addString = addString + " ,contactDeleted = 1"  ;
        InserttblAuditTrail("contactDeleted",
                ""+0,
                ""+1,
                transId);

   /*     if (addString == "")
            addString = addString + " chlDeactReason = " + (char) 34 + tblChildInfo.getChlDeactReason() + (char) 34 + "";
        else
            addString = addString + " ,chlDeactReason = " + (char) 34 + tblChildInfo.getChlDeactReason() + (char) 34 + "";
        InserttblAuditTrail("chlDeactReason",
                tblChildInfoFromDb.getChlDeactReason(),
                tblChildInfo.getChlDeactReason(),
                transId);

        if (addString == "")
            addString = addString + " chlDeactOtherReason = " + (char) 34 + tblChildInfo.getChlDeactOtherReason() + (char) 34 + "";
        else
            addString = addString + " ,chlDeactOtherReason = " + (char) 34 + tblChildInfo.getChlDeactOtherReason() + (char) 34 + "";
        InserttblAuditTrail("chlDeactOtherReason",
                tblChildInfoFromDb.getChlDeactOtherReason(),
                tblChildInfo.getChlDeactOtherReason(),
                transId);


        if (addString == "")
            addString = addString + " chlDeactComments = " + (char) 34 + tblChildInfo.getChlDeactComments() + (char) 34 + "";
        else
            addString = addString + " ,chlDeactComments = " + (char) 34 + tblChildInfo.getChlDeactComments() + (char) 34 + "";
        InserttblAuditTrail("chlDeactComments",
                tblChildInfo.getChlDeactComments(),
                tblChildInfoFromDb.getChlDeactComments(),
                transId);


        if(aqDel.id(R.id.spndeacreason).getSelectedItem()
                .toString().equalsIgnoreCase("Relocated")) {
            if (addString == "")
                addString = addString + " chlDeactRelocatedDate = " + (char) 34 + tblChildInfo.getChlDeactRelocatedDate() + (char) 34 + "";
            else
                addString = addString + " ,chlDeactRelocatedDate = " + (char) 34 + tblChildInfo.getChlDeactRelocatedDate() + (char) 34 + "";
            InserttblAuditTrail("chlDeactRelocatedDate",
                    tblChildInfo.getChlDeactRelocatedDate(),
                    tblChildInfoFromDb.getChlDeactRelocatedDate(),
                    transId);
        }else  if(aqDel.id(R.id.spndeacreason).getSelectedItem()
                .toString().equalsIgnoreCase("Mortality")) {
            if (addString == "")
                addString = addString + " chlDeactMortalityDate = " + (char) 34 + tblChildInfo.getChlDeactMortalityDate() + (char) 34 + "";
            else
                addString = addString + " ,chlDeactMortalityDate = " + (char) 34 + tblChildInfo.getChlDeactMortalityDate() + (char) 34 + "";
            InserttblAuditTrail("chlDeactMortalityDate",
                    tblChildInfo.getChlDeactMortalityDate(),
                    tblChildInfoFromDb.getChlDeactMortalityDate(),
                    transId);
        }*/


        if (addString == "")
            addString = addString + " RecordUpdatedDate = '"  + DateTimeUtil.getTodaysDate()
                    +" "+ DateTimeUtil.getCurrentTime()+"'";
        else
            addString = addString + " ,RecordUpdatedDate = '" +
                    DateTimeUtil.getTodaysDate()+" "+ DateTimeUtil.getCurrentTime()+ "'";
        InserttblAuditTrail("RecordUpdatedDate",
                DateTimeUtil.getTodaysDate()+" "+ DateTimeUtil.getCurrentTime(),
                DateTimeUtil.getTodaysDate()+" "+ DateTimeUtil.getCurrentTime(),
                transId);

        if (addString != null && addString.length() > 0) {
            delSql = "UPDATE tblcontactdetails SET ";
            delSql = delSql + addString + " WHERE "
                   + "UserId = '" + data.get(0).getUserId() + "' and contactNumber = '"+strPhn+"'";
        }
        return delSql;
    }


    /**
     * Setting AuditTrail pojo, calls - InserttblAuditTrail()
     *
     * @param transId
     */
    private boolean InserttblAuditTrail(String ColumnName, String Old_Value, String
            New_Value, int transId) throws Exception {

        APJ = new AuditPojo();
        APJ.setUserId(data.get(0).getUserId());
        APJ.setTblName("tblcontactdetails");
        APJ.setDateChanged(DateTimeUtil.getTodaysDate());
        APJ.setColumnName(ColumnName);
        APJ.setOld_Value(Old_Value);
        APJ.setNew_Value(New_Value);
        APJ.setTransId(transId);
        APJ.setRecordcreateddate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());

        AuditRepository auditRepository = new AuditRepository(databaseHelper);
        return auditRepository.insertTotblAuditTrailNew(APJ, databaseHelper);

    }

    /**
     * Save Confirmation Alert
     *
     * @param goToScreen
     */
    private void displayAlert(final String spanText2, final Intent goToScreen , final int pos) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        String strOkMess;
            strOkMess = context.getResources().getString(R.string.yes);

        // set dialog message
        alertDialogBuilder.setMessage(spanText2).setCancelable(false).setNegativeButton(
                strOkMess, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog1, int id) {
                        try {
                           // if (goToScreen != null) {

                            dialog1.cancel();
                              deleteContactDetails(pos,strPhn);


//                              SettingsActivity settingsActivity = new SettingsActivity();
//                              settingsActivity.getContactsData();
                            //    }
                            }catch (Exception e) {
                            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                            //}
                        }

                        dialog1.cancel();

                    }
                });
      //  if(goToScreen!=null) {
            alertDialogBuilder.setPositiveButton(context.getResources().getString(R.string.m122),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog1, int id) {
                            dialog1.cancel();
                        }
                    });
      //  }
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
        alertDialog1.setCancelable(true);

    }

}


