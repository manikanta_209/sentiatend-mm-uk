package com.sc.stmansi.services;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.DialogFragment;
import androidx.legacy.app.ActionBarDrawerToggle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.misc.TransactionManager;
import com.sc.stmansi.About.AboutActivity;
import com.sc.stmansi.HomeVisit.HomeVisit;
import com.sc.stmansi.HomeVisit.HomeVisitListActivity;
import com.sc.stmansi.HomeVisit.PncHomeVisit;
import com.sc.stmansi.common.AlertDialogUtil;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.common.NavDrawerItem;
import com.sc.stmansi.common.NavDrawerListAdapter;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.deactivate.WomanDeactivateActivity;
import com.sc.stmansi.delivery.DeliveryInfoActivity;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.recyclerview.ClickListener;
import com.sc.stmansi.recyclerview.WomanServicesAdapter;
import com.sc.stmansi.repositories.AuditRepository;
import com.sc.stmansi.repositories.ChildRepository;
import com.sc.stmansi.repositories.FacilityRepository;
import com.sc.stmansi.repositories.LoginRepository;
import com.sc.stmansi.repositories.TransactionHeaderRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.repositories.WomanServiceRepository;
import com.sc.stmansi.tables.AuditPojo;
import com.sc.stmansi.tables.ServiceslistPojo;
import com.sc.stmansi.tables.TblAncTests;
import com.sc.stmansi.tables.TblAncTestsMaster;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.tables.tblserviceslist;
import com.sc.stmansi.viewprofile.ViewProfileActivity;
import com.sc.stmansi.womanlist.RegisteredWomenActionTabs;

import java.lang.ref.WeakReference;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import com.sc.stmansi.R;

@SuppressLint("validFragment")
public class ServicesSummaryActivity extends AppCompatActivity implements
        ClickListener {

    private AlertDialog alertdialog = null;
    private tblserviceslist arrVal;
    private AQuery aqPMSel;
    private static AQuery aqServ;
    private DrawerLayout mDrawerLayout;
    private static tblregisteredwomen woman;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    // 25Jul2019 - Bindu
    private Map<String, String> mapFacilityname;
    private ArrayList<String> facilitynamelist;
    //15Aug2019 - Bindu
    private String serviceAddedUserType = "", serviceupdateddate = "";
    private TblInstusers user;
    private DatabaseHelper databaseHelper;
    private AppState appState;
    private RecyclerView recyclerView;
    private List<tblserviceslist> services;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private boolean spinnerSelectionChanged;
    //03Oct2019 - Bindu - get wvisitdetailsregfields
    private List<tblregisteredwomen> wVisitDetails;
    //    10May2021 Arpitha
    Map<Integer, TblAncTests> anctestVal;
    ArrayList<TblAncTests> ancTestsListAdd;
    int rdtestYes = R.id.rdanctestYes;
    int rdtestNo = R.id.rdanctestNo;
    int ettestval = R.id.etanctestval;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servicessummary);
        try {
            Bundle bundle = getIntent().getBundleExtra("globalState");
            appState = bundle.getParcelable("appState");

            databaseHelper = getHelper();
            WomanRepository womanRepository = new WomanRepository(databaseHelper);
            woman = womanRepository.getRegistartionDetails(appState.selectedWomanId, appState.sessionUserId);

            UserRepository userRepository = new UserRepository(databaseHelper);
            user = userRepository.getOneAuditedUser(appState.ashaId);

            aqServ = new AQuery(this);

            initiateDrawer();

            if (woman.getRegADDate() != null && woman.getRegADDate().length() > 0) {
                getSupportActionBar().setTitle(woman.getRegWomanName() + "\t");
            } else {
                int noOfWeeks = DateTimeUtil.getNumberOfWeeksFromLMP(woman.getRegLMP());
                getSupportActionBar().setTitle(woman.getRegWomanName() + " (" + noOfWeeks + " "
                        + getResources().getString(R.string.weeks) + ")");
            }

            aqServ.id(R.id.spnfilter).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        if (spinnerSelectionChanged) {
                            new WomanServiceLoader(ServicesSummaryActivity.this).execute();
                        }
                        spinnerSelectionChanged = true;
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            //06Aug2019 - Bindu - set Custom adapter for spinner with image
            int[] spnFilterImages = new int[]{R.drawable.ic_all, R.drawable.warning, R.drawable.notdone, R.drawable.upcoming, R.drawable.tick};
            CustomSpinnerAdapter mCustomAdapter = new CustomSpinnerAdapter(this, getResources().getStringArray(R.array.servicefilter), spnFilterImages);
            aqServ.id(R.id.spnfilter).adapter(mCustomAdapter);
            wVisitDetails = womanRepository.getVisitDetailsFrmReg(woman.getWomanId(), appState.sessionUserId);

            recyclerView = findViewById(R.id.woman_services_recycler_view);
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            recyclerView.setHasFixedSize(true);

            // use a linear layout manager
            layoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(layoutManager);

            services = new ArrayList<>();
            adapter = new WomanServicesAdapter(this, services, this, woman);
            recyclerView.setAdapter(adapter);

            new WomanServiceLoader(this).execute();

        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    //    initialize db object
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    // No Pagination on RecyclerView.Scroll, method always returns all matching records.
    private List<tblserviceslist> getServicesData() throws Exception {
        WomanServiceRepository womanServiceRepository = new WomanServiceRepository(databaseHelper);
        if (woman.getRegPregnantorMother() == 1)
            return womanServiceRepository.getServices(woman, aqServ.id(R.id.spnfilter).getSelectedItemPosition());
        else
            return womanServiceRepository.getServicesForMother(woman, aqServ.id(R.id.spnfilter).getSelectedItemPosition());
    }

    //    pop up which captures services data
    private void addServiceData(final Context context, final String womanId, final String serviceType,
                                final int serviceId, tblserviceslist service) throws Exception {

        AlertDialog.Builder alert = new AlertDialog.Builder(context, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        final View customView = LayoutInflater.from(context).inflate(R.layout.add_services_info, null);


        aqPMSel = new AQuery(customView);
        initializeScreen(aqPMSel.id(R.id.lladdservices).getView(), context);

        String strServType = service.getServiceType();
        int servNo = service.getServiceNumber();//10May2021 Arpitha


        if (serviceId == 48 || serviceId == 49 ||
                serviceId == 50 || serviceId == 51 || serviceId == 52 || serviceId == 53 ||
                serviceId == 54 || serviceId == 16 || serviceId == 17 ||
                serviceId == 18 || serviceId == 19 || serviceId == 20 || serviceId == 21 ||
                serviceId == 22 || serviceId == 7 || serviceId == 8 || serviceId == 9 || serviceId == 10
                || serviceId == 4 || serviceId == 5 || serviceId == 6 || serviceId == 1 || serviceId == 2
                || serviceId == 11 || serviceId == 12 || serviceId == 13) {

            strServType = service.getServiceType() + service.getServiceNumber();

            //04Aug2019 - Bindu - set name from strings
            int identifier = context.getResources().getIdentifier(service.getServiceType().toLowerCase(),
                    "string", "com.sc.stmansi");
            strServType = (context.getResources().getString(identifier)) + " " + service.getServiceNumber();
        }else//15May2021 Arpitha
        {
            int identifier = context.getResources().getIdentifier(service.getServiceType().toLowerCase(),
                    "string", "com.sc.stmansi");
            strServType = (context.getResources().getString(identifier)) + " " + service.getServiceNumber();

        }

        //        07May2021 Arpitha - set fields based on service type
        if (strServType.contains(getResources().getString(R.string.tt))) {
            aqPMSel.id(R.id.trnooftabgiven).gone();
            aqPMSel.id(R.id.trnooftabconsumed).gone();
            aqPMSel.id(R.id.llanctests).gone();
        } else if (strServType.contains(getResources().getString(R.string.ifa)) ||
                strServType.contains
                        (getResources().getString(R.string.folicacid)) ||
                strServType.contains(getResources().getString(R.string.calcium))) {
            aqPMSel.id(R.id.trnooftabgiven).visible();
            aqPMSel.id(R.id.trnooftabconsumed).visible();
            aqPMSel.id(R.id.llanctests).gone();
        } else if (strServType.contains(getResources().getString(R.string.anc))) {
            aqPMSel.id(R.id.trnooftabgiven).gone();
            aqPMSel.id(R.id.trnooftabconsumed).gone();
            aqPMSel.id(R.id.llanctests).visible();

            if (service.getServiceNumber() == 1) {

                aqPMSel.id(R.id.trpregtest).visible();
                aqPMSel.id(R.id.trpregtestval).visible();
                aqPMSel.id(R.id.trbabygrowthscan).gone();
                aqPMSel.id(R.id.trancbreastandheartbeattest).gone();
                aqPMSel.id(R.id.trbreastheart).gone();
                aqPMSel.id(R.id.trancbabygrowthscantest).gone();
            } else {
                aqPMSel.id(R.id.trpregtest).gone();
                aqPMSel.id(R.id.trpregtestval).gone();
                aqPMSel.id(R.id.trbabygrowthscan).visible();
                aqPMSel.id(R.id.trbreastheart).visible();
                aqPMSel.id(R.id.trancbreastandheartbeattest).visible();
                aqPMSel.id(R.id.trancbabygrowthscantest).visible();
            }
        }


        aqPMSel.id(R.id.txtheading).text(getResources().getString(R.string.addplannedservice) + ": " + strServType);
        aqPMSel.id(R.id.etdate).text(DateTimeUtil.getTodaysDate());
        aqPMSel.id(R.id.txtNameval).text(woman.getRegWomanName());
        if (serviceId <= 15)//15Nov2019 Arpitha
            aqPMSel.id(R.id.txtlmpval).text(woman.getRegLMP());
        else {
            aqPMSel.id(R.id.txtlmpval).text(woman.getRegADDate());
            aqPMSel.id(R.id.tvlmp).text(getResources().getString(R.string.tvadd));
        }

        aqPMSel.id(R.id.trspnfacname).gone();
        aqPMSel.id(R.id.tblservicesdetails).gone();
        aqPMSel.id(R.id.txtservicedeatils).gone();

        //15Aug2019 - Bindu
        serviceAddedUserType = service.getServiceUserType();
        serviceupdateddate = service.getRecordUpdatedDate();


        setOnClickListner();//11May2021 Arpitha


        //20Aug2019 Arpitha
        aqPMSel.id(R.id.imgcleardate).getImageView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aqPMSel.id(R.id.etdate).text(DateTimeUtil.getTodaysDate());
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.dateresettotodaysdate), Toast.LENGTH_LONG).show();//21Aug2019 Arpitha
            }
        });

        //25Jul2019 - Bindu

        aqPMSel.id(R.id.spnfactype).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    String selected = adapterView.getItemAtPosition(i).toString();
//                    15May2021 Bindu - add select, hmevisit and other from eng compare
                    if (!(selected.equalsIgnoreCase(getResources().getString(R.string.selecteng)) || selected.equalsIgnoreCase(getResources().getString(R.string.bhomeeng)) || selected.equalsIgnoreCase(getResources().getString(R.string.bsc)) ||
                            selected.equalsIgnoreCase(getResources().getString(R.string.botherseng)))) {

                        if (arrVal == null) {
                            aqPMSel.id(R.id.trspnfacname).visible();
                            aqPMSel.id(R.id.tvfacname).text(getResources().getString(R.string.facilitynameothers));
                        }
                        facilitynamelist = new ArrayList<>();

                        FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
                        mapFacilityname = facilityRepository.getFacilityNames(aqPMSel.id(R.id.spnfactype).getSelectedItem().toString(), user.getUserId());
                        //facilitynamelist.add(getResources().getString(R.string.selectfacilityname));
                        facilitynamelist.add(getResources().getString(R.string.strselect)); //15Nov2019 - Bindu selectfacilityname  to strselect
                        for (Map.Entry<String, String> village : mapFacilityname.entrySet())
                            facilitynamelist.add(village.getKey());
                        //facilitynamelist.add(getResources().getString(R.string.other));
                        facilitynamelist.add(getResources().getString(R.string.strother)); //15Nov2019 - Bindu other  to strother
                        ArrayAdapter<String> FacNameAdapter = new ArrayAdapter<String>(ServicesSummaryActivity.this,
                                R.layout.simple_spinner_dropdown_item, facilitynamelist);
                        FacNameAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        aqPMSel.id(R.id.spnfacname).adapter(FacNameAdapter);
                    } else {
                        aqPMSel.id(R.id.trspnfacname).gone();
                        aqPMSel.id(R.id.tretfacname).visible();
                        aqPMSel.id(R.id.tvfacname).text(getResources().getString(R.string.facilityname));
                    }
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                    crashlytics.log(e.getMessage());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        // Facility name on select listener
        aqPMSel.id(R.id.spnfacname).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    String selected = adapterView.getItemAtPosition(i).toString();
                    if (selected.equalsIgnoreCase(getResources().getString(R.string.strother))) {  //15Nov2019 - Bindu other  to strother
                        aqPMSel.id(R.id.tretfacname).visible();
                        aqPMSel.id(R.id.etfacname).text("");
                    } else {
                        aqPMSel.id(R.id.tretfacname).gone();
                        aqPMSel.id(R.id.etfacname).text("");
                    }
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                    crashlytics.log(e.getMessage());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        if (service.getServiceDueDateMin() != null
                && service.getServiceDueDateMax() != null
                && service.getServiceDueDateMin().length() > 0
                && service.getServiceDueDateMax().length() > 0) {
            aqPMSel.id(R.id.tvdurationvalmin).text(service.getServiceDueDateMin());

            aqPMSel.id(R.id.tvdurationvalmax).text(service.getServiceDueDateMax());

            /*aqPMSel.id(R.id.tvdurationval).text(getResources().getString(R.string.mindate)+": "+ service.getServiceDueDateMax());

            aqPMSel.id(R.id.tvduration).text(getResources().getString(R.string.maxdate)+": "+service.getServiceDueDateMin());
*/
        }

        WomanServiceRepository womanServiceRepository = new WomanServiceRepository(databaseHelper);
        arrVal = womanServiceRepository.getService(womanId, serviceId, service.getServiceNumber());
        if (arrVal != null) {
            try {
                aqPMSel.id(R.id.etdate).text(arrVal.getServiceActualDateofAction());
                aqPMSel.id(R.id.etcomments).text(arrVal.getServiceComments());
                aqPMSel.id(R.id.etfacname).text(arrVal.getServiceProvidedFacName());
                aqPMSel.id(R.id.etdate).enabled(false);
                aqPMSel.id(R.id.etfacname).enabled(false);
                aqPMSel.id(R.id.spnfactype).enabled(false);
                aqPMSel.id(R.id.etcomments).enabled(false);
                aqPMSel.id(R.id.btnsave).gone();

                aqPMSel.id(R.id.spnfactype).setSelection(Arrays.asList(getResources().getStringArray(R.array.facilitytype)).indexOf(arrVal.getServiceProvidedFacType())); //12Aug2019 - Bindu set spinner value

                //22Jul2019 - Bindu
                /*aqPMSel.id(R.id.txtheading).text(getResources().getString(R.string.view)
                        + " : " + arrVal.getServiceType() + " " + arrVal.getServiceNumber());06May2021 Arpitha*/
                int identifier = context.getResources().getIdentifier(arrVal.getServiceType().toLowerCase(),
                        "string", "com.sc.stmansi");//06May2021 Arpitha
                strServType = (context.getResources().getString(identifier));//06May2021 Arpitha
                aqPMSel.id(R.id.txtheading).text(getResources().getString(R.string.view)
                        + " : " + strServType + " " + arrVal.getServiceNumber());//06May2021 Arpitha
                aqPMSel.id(R.id.trspnfacname).gone(); //25Jul2019 - Bindu
                aqPMSel.id(R.id.tvfacname).text(getResources().getString(R.string.facilityname));

//                07May2021 Arpitha
                aqPMSel.id(R.id.etnooftabgiven).text(arrVal.getServiceDetail1());
                aqPMSel.id(R.id.etnooftabconsumed).text(arrVal.getServiceDetail2());
                aqPMSel.id(R.id.etnooftabgiven).enabled(false);
                aqPMSel.id(R.id.etnooftabconsumed).enabled(false);

//                10May2021 Arpitha
                setAncTestDataToFileds(service.getServiceNumber());


            } catch (Exception e) {

                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
        }


        aqPMSel.id(R.id.btnsave).getImageView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (validateServiceFields(serviceId))
                        confirmAlert(serviceType, serviceId, servNo);
                } catch (Exception e) {


                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                    crashlytics.log(e.getMessage());
                }

            }
        });

        aqPMSel.id(R.id.btncancel).getImageView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    confirmAlert(null, 0, 0);
                } catch (Exception e) {


                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                    crashlytics.log(e.getMessage());
                }

            }
        });

        aqPMSel.id(R.id.etdate).getEditText().setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEvent.ACTION_UP == event.getAction()) {
                    DialogFragment newFragment = new DatePickerFragment(aqPMSel, woman);
                    newFragment.show(getSupportFragmentManager(), "datePicker");

                }
                return true;
            }
        });


        alert.setView(customView).setTitle("").setPositiveButton(
                "", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        alertdialog.setCancelable(false);
                        alertdialog.show();
                        try {
                            confirmAlert(serviceType, serviceId, service.getServiceNumber());
                        } catch (Exception e) {

                            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                            crashlytics.log(e.getMessage());
                        }
                    }
                });

        alertdialog = alert.create();
        alertdialog.show();

        alert.setCancelable(false);

        alertdialog.setTitle(context.getResources().getString(R.string.ok));

        alert.setCancelable(true);
        alertdialog.show();
        alertdialog.setCancelable(true);
    }


    //    update captured data to database
    private void addServiceData(String serviceType, int serviceId, int servNo) {
        try {
            //   int servNo =0;
            TransactionHeaderRepository transRepo = new TransactionHeaderRepository(databaseHelper);
            final int transId = transRepo.iCreateNewTrans(woman.getUserId(), databaseHelper);
            final ServiceslistPojo SLU = new ServiceslistPojo();
            SLU.setUserid(woman.getUserId());
            SLU.setWomanId(woman.getWomanId());
            if (serviceType.contains("1")) {
                serviceType = serviceType.replace("1", "");

            } else if (serviceType.contains("2")) {
                serviceType = serviceType.replace("2", "");

            } else if (serviceType.contains("3")) {
                serviceType = serviceType.replace("3", "");

            }
            SLU.setServicetype(serviceType);
            SLU.setServiceId(serviceId);
            SLU.setServiceNumber(0);//servicenum was not assigned with any value
            SLU.setComments(aqPMSel.id(R.id.etcomments).getText().toString());
            SLU.setFacType(aqPMSel.id(R.id.spnfactype).getSelectedItem().toString()); //12Aug2019 - Bindu - change from positin to direct val
            SLU.setActualDateofAction(aqPMSel.id(R.id.etdate).getText().toString());

            String sel = "";
            if (aqPMSel.id(R.id.spnfacname).getSelectedItem() != null)
                sel = aqPMSel.id(R.id.spnfacname).getSelectedItem().toString();

            //15Nov2019 - Bindu - set res english
            if (sel.equalsIgnoreCase(getResources().getString(R.string.select)))
                sel = getResources().getString(R.string.strselect);
            else if (sel.equalsIgnoreCase(getResources().getString(R.string.other)))
                sel = getResources().getString(R.string.strother);

            if (sel.trim().length() > 0 && !(sel.equalsIgnoreCase("select")
                    || sel.equalsIgnoreCase("other")))
                SLU.setFacilityName(sel);
            else if (sel.equalsIgnoreCase("other")
                    && aqPMSel.id(R.id.etfacname).getText().toString().trim().length() <= 0)
                SLU.setFacilityName("Other");
            else
                SLU.setFacilityName(aqPMSel.id(R.id.etfacname).getText().toString());

            SLU.setRecordupdateddate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime()); //26Jul2019 - Bindu
            SLU.setUsertype(appState.userType);

//            07May2021 Arpitha
            SLU.setServiceDetail1(aqPMSel.id(R.id.etnooftabgiven).getText().toString());
            SLU.setServiceDetail2(aqPMSel.id(R.id.etnooftabconsumed).getText().toString());

            setAncTestData(transId, servNo);//10May2021 Arpitha

            TransactionManager.callInTransaction(databaseHelper.getConnectionSource(), new Callable<Void>() {
                public Void call() throws Exception {//29Aug2019 Arpitha - transaction manager

                    //15Aug2019 - Bindu - write audit
                    if (serviceAddedUserType != "" && !serviceAddedUserType.equalsIgnoreCase(SLU.getUsertype())) {
                        insertAuditTrail("ServiceUserType",
                                serviceAddedUserType,
                                appState.userType,
                                transId, null);
                    }

                    insertAuditTrail("RecordUpdatedDate",
                            String.valueOf(serviceupdateddate),
                            String.valueOf(SLU.getRecordupdateddate()),
                            transId, null);

                    WomanServiceRepository womanServiceRepository = new WomanServiceRepository(databaseHelper);
                    womanServiceRepository.updateServicesListData(SLU, databaseHelper, transId);

                    womanServiceRepository.insertANcTests(ancTestsListAdd, databaseHelper, transId);

                    Toast.makeText(ServicesSummaryActivity.this, getResources().getString(R.string.serviceupdatesuccess), Toast.LENGTH_LONG).show();
                    alertdialog.cancel();

                    return null;
                }
            });
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.serviceupdatefailed), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }


    /**
     * Setting AuditTrail pojo, calls - insertAuditTrail()
     *
     * @param transId
     */
    private boolean insertAuditTrail(String ColumnName, String Old_Value, String New_Value, int transId,
                                     byte[] image) throws Exception {
        AuditPojo audit = new AuditPojo();
        audit.setUserId(user.getUserId());
        audit.setWomanId(woman.getWomanId());
        audit.setTblName("tblServices");
        audit.setDateChanged(DateTimeUtil.getTodaysDate());
        audit.setColumnName(ColumnName);
        audit.setOld_Value(Old_Value);
        audit.setNew_Value(New_Value);
        audit.setTransId(transId);

        //15Aug2019 - Bindu - set recordcreateddate
        audit.setRecordcreateddate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());

        AuditRepository auditRepo = new AuditRepository(databaseHelper);
        return auditRepo.insertTotblAuditTrail(audit);
    }


    boolean validateServiceFields(int serviceId) throws ParseException {
        //Bindu - change static msg
        if (aqPMSel.id(R.id.etdate).getText().toString().trim().length() <= 0) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.enterservicecompdate), Toast.LENGTH_LONG).show();
            return false;
        } else if (aqPMSel.id(R.id.spnfactype).getSelectedItemPosition() == 0) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.selserviceprovfactype), Toast.LENGTH_LONG).show();
            return false;
        }
//        27Nov2019 Arpitha
        Date delDate = null;
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");

        Date seldate = format.parse(aqPMSel.id(R.id.etdate).getText().toString());

        if (woman.getRegADDate() != null && woman.getRegADDate().trim().length() > 0 && serviceId < 16) //11Dec2019 - Bindu - Add service id - only if ANC then display alert
            delDate = format.parse(woman.getRegADDate());
        if (delDate != null && (seldate.after(delDate) || seldate.equals(delDate))) {   //27Nov2019 Arpitha
            /*AlertDialogUtil.displayAlertMessage(
                    getResources().getString(R.string.cantbeafterdeldate)
                    , this);*/
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.cantbeequalafterdeldate), Toast.LENGTH_LONG).show();

            aqPMSel.id(R.id.etdate).text(DateTimeUtil.getTodaysDate());//22Aug2019 Arpitha
            return false;
        }
        //        15May2021 Arpitha
        else if( (serviceId == 10 || serviceId == 11 || serviceId == 12 ||
                serviceId == 13) && !(aqPMSel.id(R.id.rdanctestYes1).isChecked() || aqPMSel.id(R.id.rdanctestNo1).isChecked()))
        {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.enterbp), Toast.LENGTH_LONG).show();

            return false;
        }
//        11May2021 Arpitha
        else if ((serviceId == 10 || serviceId == 11 || serviceId == 12 ||
                serviceId == 13) &&
               aqPMSel.id(R.id.rdanctestYes1).isChecked() &&  (aqPMSel.id(R.id.etbpsystolic)
                .getText().toString().trim().length()
                        <= 0 || aqPMSel.id(R.id.etbpdiastolic).getText().toString()
                .trim().length() <= 0)) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.enterbp), Toast.LENGTH_LONG).show();

            aqPMSel.id(R.id.etbpsystolic).getEditText().requestFocus();//22Aug2019 Arpitha
            return false;
        }
        //        11May2021 Arpitha
        else if ((serviceId == 10 || serviceId == 11 || serviceId == 12 || serviceId == 13) &&
                aqPMSel.id(R.id.etanctestval15).getText().toString().trim().length() <= 0) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.enterweightvalue), Toast.LENGTH_LONG).show();

            aqPMSel.id(R.id.etanctestval15).getEditText().requestFocus();//22Aug2019 Arpitha
            return false;
        }
        return true;
    }

    /**
     * This is a Recursive method that traverse through the group and subgroup
     * of view Sets text, assigns clickListners
     */
    private ArrayList<View> initializeScreen(View v, Context context) {

        if (!(v instanceof ViewGroup)) {
            ArrayList<View> viewArrayList = new ArrayList<>();
            viewArrayList.add(v);

            // TextView - Assign Labels
            if (v.getClass() == TextView.class || v.getClass() == RadioButton.class) {
            }

            if (v.getClass() == RadioButton.class) {
                aqPMSel.id(v.getId()).clicked(context, "commonClick");
            }

            return viewArrayList;
        }

        if (v instanceof Spinner) {
        }

        ArrayList<View> result = new ArrayList<View>();

        ViewGroup viewGroup = (ViewGroup) v;
        for (int i = 0; i < viewGroup.getChildCount(); i++) {

            View child = viewGroup.getChildAt(i);

            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);
            viewArrayList.addAll(initializeScreen(child, context));

            result.addAll(viewArrayList);
        }
        return result;
    }

    private void initiateDrawer() throws Exception {
        mDrawerLayout = findViewById(R.id.ett_drawer_layout);
        mDrawerList = findViewById(R.id.ett_left_drawer);

        ArrayList<NavDrawerItem> navDrawerItems = new ArrayList<>();
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.sp_WRegEdit), R.drawable.registration));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.addunplannedservices), R.drawable.anm_pending_activities));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.unplannedserviceslist), R.drawable.anm_pending_activities));
//29Aug2019 - Bindu - Add Homevisit
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.homevisitadd),
                R.drawable.prenatalhomevisit));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.womanhomevisithistory),
                R.drawable.prenatalhomevisit));

        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deliveryHeading), R.drawable.delivery_info));//01Oct2019 Arpitha


        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deactivate),
                R.drawable.deactivate));//28Nov2019 Arpitha

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        mDrawerList.setAdapter(new NavDrawerListAdapter(getApplicationContext(), navDrawerItems));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer,
                R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu(); // creates call to
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                getSupportActionBar().setDisplayShowTitleEnabled(false);
                getSupportActionBar().setDisplayShowCustomEnabled(false);

                int noOfWeeks;
                try {
                    if (woman.getRegADDate() != null && woman.getRegADDate().length() > 0) {
                        getSupportActionBar().setTitle((woman.getRegWomanName() + "\t"));
                    } else {
                        noOfWeeks = DateTimeUtil.getNumberOfWeeksFromLMP(woman.getRegLMP());
                        getSupportActionBar().setTitle((woman.getRegWomanName() + " (" + noOfWeeks + " "
                                + getResources().getString(R.string.weeks) + ")"));
                    }
                } catch (ParseException e) {


                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                    crashlytics.log(e.getMessage());
                }
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                try {
                    invalidateOptionsMenu(); // creates call to
                    getSupportActionBar().setDisplayShowHomeEnabled(false);
                    getSupportActionBar().setDisplayShowTitleEnabled(false);
                    getSupportActionBar().setDisplayShowCustomEnabled(true);
                    getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);

                    aqServ.id(R.id.ivWomanImg).getImageView().setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            mDrawerLayout.closeDrawer(mDrawerList);
                        }
                    });

                    aqServ.id(R.id.tvWomanName).text((woman.getRegWomanName() + "\t"));
                    if (woman.getRegADDate().length() > 0) {
                        aqServ.id(R.id.tvInfo1).text((getResources().getString(R.string.tvadd) + " " + woman.getRegADDate()));
                    } else {
                        int noOfWeeks = DateTimeUtil.getNumberOfWeeksFromLMP(woman.getRegLMP());
                        aqServ.id(R.id.tvInfo1).text((noOfWeeks + getResources().getString(R.string.weeks)));
                    }
                } catch (Exception e) {


                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                    crashlytics.log(e.getMessage());
                }
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    /**
     * This method is to sync drawerToggle and to replace up caret icon to menu
     * icon in Action Bar
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    /**
     * The click listener for ListView in the navigation drawer
     */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            mDrawerLayout.closeDrawer(mDrawerList);
            try {

                switch (position) {
                    case 0:
                        Intent viewProfile = new Intent(ServicesSummaryActivity.this, ViewProfileActivity.class);
                        viewProfile.putExtra("woman", woman);
                        viewProfile.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        startActivity(viewProfile);
                        break;
                    case 1:
                        Intent unplanned = new Intent(ServicesSummaryActivity.this, UnplannedServicesActivity.class);
                        unplanned.putExtra("woman", woman);
                        unplanned.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        startActivity(unplanned);
                        break;
                    case 2:
                        Intent unPlannedList = new Intent(ServicesSummaryActivity.this, UnplannedServicesListActivity.class);
                        unPlannedList.putExtra("woman", woman);
                        unPlannedList.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        startActivity(unPlannedList);
                        break;
                    //29Aug2019 - Bindu - Add Home visit
                    case 3:
                        if (woman.getRegPregnantorMother() == 2) { //02Dec2019 - Bindu - add condition - ANC/PNC Home visit
                            Intent nextScreen = new Intent(ServicesSummaryActivity.this, PncHomeVisit.class);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(nextScreen);
                        } else {
                            int daysDiff = DateTimeUtil.getDaysBetweenDates(DateTimeUtil.getTodaysDate(), woman.getRegLMP());
                            if (daysDiff > 210) {
                                displayNearingDelAlertMessage(woman);
                            } else {
                                Intent homevisit = new Intent(ServicesSummaryActivity.this, HomeVisit.class);
                                homevisit.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                startActivity(homevisit);
                            }
                        }
                        break;
                    case 4:
                        //  if(woman.getLastAncVisitDate().length() > 0 || woman.getLastPncVisitDate().length() > 0) {
                        if (wVisitDetails != null && wVisitDetails.get(0).getLastAncVisitDate().length() > 0 || wVisitDetails.get(0).getLastPncVisitDate().length() > 0) {
                            Intent homevisithistory = new Intent(ServicesSummaryActivity.this, HomeVisitListActivity.class);
                            homevisithistory.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(homevisithistory);
                        } else {
                            Toast.makeText(ServicesSummaryActivity.this, getResources().getString(R.string.norecords), Toast.LENGTH_SHORT).show();
                        }
                        break;
                    //01Oct2019 Arpitha
                    case 5:
                        if (woman.getRegPregnantorMother() == 2 || DateTimeUtil.getNumberOfDaysFromLMP(woman.getRegLMP()) > 210) {
                            Intent del = new Intent(ServicesSummaryActivity.this, DeliveryInfoActivity.class);
                            del.putExtra("woman", woman);
                            del.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(del);
                        } else
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.m132), Toast.LENGTH_LONG).show();
                        break;

                    //                  28Nov2019 Arpitha
                    case 6: {
                        Intent deact = new Intent(ServicesSummaryActivity.this, WomanDeactivateActivity.class);
                        deact.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        deact.putExtra("woman", woman);
                        startActivity(deact);
                    }
                }
            } catch (Exception e) {


                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
        }
    }

    /**
     * Called whenever an item clicked in actionbar
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action buttons
        switch (item.getItemId()) {
            case R.id.home: {
                Intent goToScreen = new Intent(ServicesSummaryActivity.this, MainMenuActivity.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                startActivity(goToScreen);
                return true;
            }
            case R.id.logout: {
                Intent goToScreen = new Intent(ServicesSummaryActivity.this, LoginActivity.class);
                displayAlert(getResources().getString(R.string.m111), goToScreen);
                return true;
            }
            case R.id.wlist: {
                Intent goToScreen = new Intent(ServicesSummaryActivity.this, RegisteredWomenActionTabs.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }
            //30Sep2019 - Bindu
            case R.id.about: {
                Intent goToScreen = new Intent(ServicesSummaryActivity.this, AboutActivity.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        return b;
    }

    /**
     * Initializes the Action Bar items
     */
    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        try {
            int noOfWeeks = 0;
            getSupportActionBar().setIcon(R.drawable.ett);
            if (woman.getRegADDate() != null && woman.getRegADDate().length() > 0) {
                getSupportActionBar().setTitle((woman.getRegWomanName() + "\t"));
            } else {
                noOfWeeks = DateTimeUtil.getNumberOfWeeksFromLMP(woman.getRegLMP());
                getSupportActionBar().setTitle((woman.getRegWomanName() + " (" + noOfWeeks + " "
                        + getResources().getString(R.string.weeks) + ")"));
            }
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer1); //27Sep2019 - Bindu - set drawer and enable true home btn
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);

           /* aqServ.id(R.id.tvWomanName).text(woman.getRegWomanName() + " (" +
                    (noOfWeeks > 0 ? noOfWeeks : "") + " "
                    + getResources().getString(R.string.weeks) + ")");*/
//            aqServ.id(R.id.tvInfo1).text(getResources().getString(R.string.tvlmp) + " - " + woman.getRegLMP());
            if (woman.getRegADDate() != null && woman.getRegADDate().length() > 0) {
                aqServ.id(R.id.tvInfo1).text((getResources().getString(R.string.tvadd) + " " + woman.getRegADDate()));
                aqServ.id(R.id.tvWomanName).text(woman.getRegWomanName());
            } else {
                int noOfWeeks1 = DateTimeUtil.getNumberOfWeeksFromLMP(woman.getRegLMP());
                aqServ.id(R.id.tvInfo1).text((noOfWeeks1 + getResources().getString(R.string.weeks)));
                aqServ.id(R.id.tvWomanName).text(woman.getRegWomanName() + " (" +
                        (noOfWeeks1 > 0 ? noOfWeeks1 : "") + " "
                        + getResources().getString(R.string.weeks) + ")");
            }
            aqServ.id(R.id.ivWomanImg).gone();

            getMenuInflater().inflate(R.menu.servicesmenu, menu);

            menu.findItem(R.id.logout).setTitle((getResources().getString(R.string.action_logout)));


        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Save Confirmation Alert
     *
     * @param goToScreen
     * @param spanText2
     */
    private void displayAlert(final String spanText2, final Intent goToScreen) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(spanText2).setCancelable(false).setNegativeButton(
                getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (goToScreen != null) {

                            goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(goToScreen);
                            if (spanText2.contains("logout")) {
                                try {
                                    LoginRepository loginRepository = new LoginRepository(databaseHelper);
                                    loginRepository.updateLogoutTime(appState.loginAuditId, appState.sessionUserId);
                                } catch (Exception e) {
                                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                                    crashlytics.log(e.getMessage());
                                }
                            }
                        } else
                            dialog.cancel();
                    }
                }).setPositiveButton(getResources().getString(R.string.m122),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    /**
     * Confirm Alert on Click save
     */
    private void confirmAlert(final String serviceType, final int serviceId, int servNo) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        String message;
        if (serviceType != null || serviceId != 0)
            message = getResources().getString(R.string.m099);
        else {
            if (arrVal != null)
                message = getResources().getString(R.string.areyousuretoexit);
            else
                message = getResources().getString(R.string.m110);
        }

        alertDialogBuilder.setMessage(message).setCancelable(false)
                .setNegativeButton(getResources().getString(R.string.m121),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                try {
                                    if (serviceType != null || serviceId != 0) {
                                        addServiceData(serviceType, serviceId, servNo);
                                        new WomanServiceLoader(ServicesSummaryActivity.this).execute();
                                    } else
                                        alertdialog.cancel();

                                } catch (Exception e) {


                                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                                    crashlytics.log(e.getMessage());
                                }
                            }
                        })
                .setPositiveButton((getResources().getString(R.string.m122)),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    @SuppressLint("SimpleDateFormat")
    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        private AQuery aqPMSel;
        private tblregisteredwomen woman;

        public DatePickerFragment() {
        }

        public DatePickerFragment(AQuery aQuery, tblregisteredwomen woman) {
            aqPMSel = aQuery;
            this.woman = woman;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            int year = 0, month = 0, day = 0;
            try {

                Calendar c = Calendar.getInstance();
                String defaultDate = aqPMSel.id(R.id.etdate).getText().toString(); //09Aug2019 - Cal set date of Provided date
                if (defaultDate.length() > 0) {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    Date sendDate = sdf.parse(defaultDate);
                    c.setTime(sendDate);
                    year = c.get(Calendar.YEAR);
                    month = c.get(Calendar.MONTH);
                    day = c.get(Calendar.DAY_OF_MONTH);
                } else {
                    c.setTime(new Date());
                    year = c.get(Calendar.YEAR);
                    month = c.get(Calendar.MONTH);
                    day = c.get(Calendar.DAY_OF_MONTH);
                }
            } catch (Exception e) {


                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }

            // Create a new instance of DatePickerDialog and return it
            DatePickerDialog dpDialog;
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                // Do something for lollipop and above versions
                dpDialog = new DatePickerDialog(getActivity(), R.style.DialogTheme, this, year, month, day);
            } else {
                // do something for phones running an SDK before lollipop
                dpDialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, this, year, month, day);
                dpDialog.setTitle("Set a Date");
                dpDialog.getDatePicker().setCalendarViewShown(false);
            }
            return dpDialog;
        }

        @Override
        public void onDateSet(DatePicker view, int year, int month, int day) {
            if (view.isShown()) {
                try {
                    String SelectedDate = String.format("%02d", day) + "-" +
                            String.format("%02d", month + 1) + "-"
                            + year;
                    Date lmpDate = null, delDate = null;//27Nov Arpitha
                    SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                    Date seldate = format.parse(SelectedDate);

                    //09Aug2019 - Bindu - Chk min date
                    String strselmindate = aqPMSel.id(R.id.tvdurationvalmin).getText().toString();
                    Date selmindate = null;
                    if (strselmindate != null && strselmindate != "" && strselmindate.trim().length() > 0) {
                        selmindate = format.parse(strselmindate);
                    }
                    if (woman.getRegLMP() != null && woman.getRegLMP().trim().length() > 0)
                        lmpDate = format.parse(woman.getRegLMP());

                    if (woman.getRegADDate() != null && woman.getRegADDate().trim().length() > 0)
                        delDate = format.parse(woman.getRegADDate());

                    if (seldate.after(new Date())) {
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.date_cannot_af_currentdate)
                                , getActivity());
                        aqPMSel.id(R.id.etdate).text(DateTimeUtil.getTodaysDate());
                    } else if (lmpDate != null && seldate.before(lmpDate)) {
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.date_cannot_bf_lmp)
                                , getActivity());
                        aqPMSel.id(R.id.etdate).text(DateTimeUtil.getTodaysDate());
                    }//add delDate condition - 28Apr2021 Arpitha
                    else if (delDate == null && selmindate != null && seldate.before(selmindate)) {   //09Aug2019 - Bindu - chk for min date
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.servntapplibeforemindate)
                                , getActivity());
                        aqPMSel.id(R.id.etdate).text(DateTimeUtil.getTodaysDate());//22Aug2019 Arpitha
                    } else if (delDate != null && (seldate.before(delDate))) {   //27Nov2019 Arpitha - changed on 28Apr2021
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.date_cannot_bf_deldate)
                                , getActivity());
                        aqPMSel.id(R.id.etdate).text(DateTimeUtil.getTodaysDate());//22Aug2019 Arpitha
                    } else {
                        aqPMSel.id(R.id.etdate).text(SelectedDate);
                        aqPMSel.id(R.id.imgcleardate).visible();//20Aug2019 Arpitha
                    }
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                    crashlytics.log(e.getMessage());

                }
            }
        }
    }

    /**
     * This method Calls the idle timeout
     */
    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        /*
         * You'll need this in your class to release the helper when done.
         */
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

    @Override
    public void onClick(View v) {
        int itemPosition = recyclerView.getChildLayoutPosition(v);
        tblserviceslist service = services.get(itemPosition);

        try {

            ChildRepository childRepository = new ChildRepository(databaseHelper);
            boolean isChildAlive = childRepository.checkIsChildAlive(woman.getWomanId());
            boolean isChildActive = childRepository.checkIsChildActive(woman.getWomanId());

            if (((woman.getDateDeactivated() != null &&
                    woman.getDateDeactivated().trim().length() > 0 && (!isChildAlive || !isChildActive))
                    ||
                    (user.getIsDeactivated() == 1)))//26Nov2019 Arpitha
            {
                Toast.makeText(getApplicationContext(), getResources()
                        .getString(R.string.userdeactivated), Toast.LENGTH_LONG).show();

            } else {
                if (!service.getServiceType().equalsIgnoreCase("EDD")) {
                    WomanServiceRepository womanServiceRepository = new WomanServiceRepository(databaseHelper);

                    //06Aug2019 - Bindu chk for TT booster and TT
                    if (service.getServiceType().equalsIgnoreCase("TTBooster") && womanServiceRepository.isTTprovided(woman.getWomanId())) {
                        Toast.makeText(getApplicationContext(), getResources()
                                .getString(R.string.m096), Toast.LENGTH_LONG).show();
                    } else if (service.getServiceType().equalsIgnoreCase("TT") && womanServiceRepository.isTTBoosterprovided(woman.getWomanId())) {
                        Toast.makeText(getApplicationContext(), getResources()
                                .getString(R.string.m096), Toast.LENGTH_LONG).show();
                    } else {
                        if (service.getServiceActualDateofAction() != null ||
                                (woman.getRegPregnantorMother() == 1 && service.getServiceId() > 0)
                                || (woman.getRegPregnantorMother() == 2
                                && service.getServiceId() > 0)) {
                            if (service.getServiceDueDateMin().length() > 1) {
                                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                                Date selectedMinDate = format.parse(service.getServiceDueDateMin());
                                if (selectedMinDate.before(new Date()))
                                    addServiceData(ServicesSummaryActivity.this, woman.getWomanId(), service.getServiceType(),
                                            service.getServiceId(), service);
                                else
                                    Toast.makeText(getApplicationContext(), getResources()
                                            .getString(R.string.servntapplibeforemindate), Toast.LENGTH_LONG).show();
                            }
                        } else
                            Toast.makeText(getApplicationContext(), getResources()
                                    .getString(R.string.m096), Toast.LENGTH_LONG).show();
                    }
                } else//09Nov2019 Arpitha
                {
                    if (woman.getRegPregnantorMother() == 2 || DateTimeUtil.getNumberOfDaysFromLMP(woman.getRegLMP()) > 210) {

                        Intent nextScreen = new Intent(ServicesSummaryActivity.this, DeliveryInfoActivity.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        nextScreen.putExtra("woman", woman);
                        startActivity(nextScreen);
                    } else
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.m132), Toast.LENGTH_LONG).show();

                }
            }

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    @Override
    public boolean onLongClick(View v) {
        return false;
    }

    static class WomanServiceLoader extends AsyncTask<Void, Void, Void> {

        private WeakReference<ServicesSummaryActivity> weakReference;

        public WomanServiceLoader(ServicesSummaryActivity activity) {
            weakReference = new WeakReference<>(activity);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            ServicesSummaryActivity activity = weakReference.get();
            try {
                List<tblserviceslist> rows = activity.getServicesData();
                int previousSize = activity.services.size();
                activity.services.clear();
                activity.adapter.notifyItemRangeRemoved(0, previousSize);

                if (!rows.isEmpty()) {
                  /*  if(aqServ.id(R.id.spnfilter).getSelectedItemPosition()!=0)
                    for(int i=0; i<rows.size();i++) {
                        if(rows.get(i).getServiceId()>16 &&
                                woman.getRegPregnantorMother()==2 &&
                                aqServ.id(R.id.spnfilter).getSelectedItemPosition()==3)
                        activity.services.add(rows.get(i));
                        else
                            activity.services.addAll(rows);
                    }else*/
                    activity.services.addAll(rows);

                    activity.adapter.notifyItemRangeInserted(0, activity.services.size());
                }
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            ServicesSummaryActivity activity = weakReference.get();
            activity.recyclerView.setVisibility(View.GONE);
            if (activity.services.size() == 0) {
                activity.aqServ.id(R.id.txtnodatapending).visible();
//                activity.recyclerView.setVisibility(View.GONE);
            } else {
                activity.aqServ.id(R.id.txtnodatapending).gone();
                activity.recyclerView.setVisibility(View.VISIBLE);
            }
        }
    }

    //18Oct2019 - Bindu - Alert message nearing del
    public void displayNearingDelAlertMessage(tblregisteredwomen regwoman) throws Exception {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        String alertmsg = regwoman.getRegWomanName() + ", " + " EDD : " + regwoman.getRegEDD();
        //alertDialogBuilder.setMessage(message).setCancelable(false).setTitle(Html.fromHtml("<font color='#FF7F27'>Alert -- </font><br><br><p>"+alertmsg + "</p>")).setNegativeButton(
        String msg = "<font color='#CC3333'> Nearing Delivery - </font>" + alertmsg;
        alertDialogBuilder.setMessage(getResources().getText(R.string.alerthomevisitnearingdelmsg)).setCancelable(false).setTitle(Html.fromHtml(msg)).setNegativeButton(
                getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent nextScreen = new Intent(ServicesSummaryActivity.this, HomeVisit.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        nextScreen.putExtra("woman", woman);
                        startActivity(nextScreen);
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    //    10May2021 Arpitha
    private void setAncTestData(int transId, int servNo) throws Exception {

        ancTestsListAdd = new ArrayList<>();
        List<TblAncTestsMaster> ancTestsMasterList = new ArrayList<>();

        WomanServiceRepository womanServiceRepository = new WomanServiceRepository(databaseHelper);
        ancTestsMasterList = womanServiceRepository.getAncTests();

        for (int i = 1; i <= ancTestsMasterList.size(); i++) {
            TblAncTests tblAncTests = new TblAncTests();
            tblAncTests.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
            tblAncTests.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());//21May2021 Arpitha


            tblAncTests.setServiceNumber(servNo);
            /*if (i <=13 || i == 16) {
                if (aqPMSel.id(rdtestYes + i).isChecked()) {
                    tblAncTests.setTestDone("Yes");

                } else if (aqPMSel.id(rdtestNo + i).isChecked())
                    tblAncTests.setTestDone("No");
            }*/


            if (i == 1) {
//                if(aqPMSel.id(R.id.etbpsystolic).getText().toString().trim().length()>0 || aqPMSel.id(R.id.etbpdiastolic).getText().toString().trim().length()>0)

                if (aqPMSel.id(R.id.rdanctestYes1).isChecked()) {
                    tblAncTests.setTestDone("Yes");
                    tblAncTests.setTestValue(aqPMSel.id(R.id.etbpsystolic).getText().toString()
                            +"/"+aqPMSel.id(R.id.etbpdiastolic).getText().toString());
                }
                 else if (aqPMSel.id(R.id.rdanctestNo1).isChecked())
                    tblAncTests.setTestDone("No");
            } else if (i == 2) {
                tblAncTests.setTestValue(aqPMSel.id(R.id.etanctestval2).getText().toString());
                if (aqPMSel.id(R.id.rdanctestYes2).isChecked())
                    tblAncTests.setTestDone("Yes");
                else if (aqPMSel.id(R.id.rdanctestNo2).isChecked())
                    tblAncTests.setTestDone("No");
            } else if (i == 3) {
                tblAncTests.setTestValue(aqPMSel.id(R.id.etanctestval3).getText().toString());
                if (aqPMSel.id(R.id.rdanctestYes3).isChecked())
                    tblAncTests.setTestDone("Yes");
                else if (aqPMSel.id(R.id.rdanctestNo3).isChecked())
                    tblAncTests.setTestDone("No");
            } else if (i == 4) {
                tblAncTests.setTestValue(aqPMSel.id(R.id.etanctestval4).getText().toString());
                if (aqPMSel.id(R.id.rdanctestYes4).isChecked())
                    tblAncTests.setTestDone("Yes");
                else if (aqPMSel.id(R.id.rdanctestNo4).isChecked())
                    tblAncTests.setTestDone("No");
            } else if (i == 5) {
                tblAncTests.setTestValue(aqPMSel.id(R.id.etanctestval5).getText().toString());
                if (aqPMSel.id(R.id.rdanctestYes5).isChecked())
                    tblAncTests.setTestDone("Yes");
                else if (aqPMSel.id(R.id.rdanctestNo5).isChecked())
                    tblAncTests.setTestDone("No");

            } else if (i == 6) {
                tblAncTests.setTestValue(aqPMSel.id(R.id.etanctestval6).getText().toString());
                if (aqPMSel.id(R.id.rdanctestYes6).isChecked())
                    tblAncTests.setTestDone("Yes");
                else if (aqPMSel.id(R.id.rdanctestNo6).isChecked())
                    tblAncTests.setTestDone("No");
            } else if (i == 7) {
                tblAncTests.setTestValue(aqPMSel.id(R.id.etanctestval7).getText().toString());
                if (aqPMSel.id(R.id.rdanctestYes7).isChecked())
                    tblAncTests.setTestDone("Yes");
                else if (aqPMSel.id(R.id.rdanctestNo7).isChecked())
                    tblAncTests.setTestDone("No");

            } else if (i == 8) {
                tblAncTests.setTestValue(aqPMSel.id(R.id.etanctestval8).getText().toString());
                if (aqPMSel.id(R.id.rdanctestYes8).isChecked())
                    tblAncTests.setTestDone("Yes");
                else if (aqPMSel.id(R.id.rdanctestNo8).isChecked())
                    tblAncTests.setTestDone("No");
            } else if (i == 9) {

                tblAncTests.setTestValue(aqPMSel.id(R.id.etanctestval9).getText().toString());
                if (aqPMSel.id(R.id.rdanctestYes9).isChecked())
                    tblAncTests.setTestDone("Yes");
                else if (aqPMSel.id(R.id.rdanctestNo9).isChecked())
                    tblAncTests.setTestDone("No");
            } else if (i == 10) {
                tblAncTests.setTestValue(aqPMSel.id(R.id.etanctestval10).getText().toString());
                if (aqPMSel.id(R.id.rdanctestYes10).isChecked())
                    tblAncTests.setTestDone("Yes");
                else if (aqPMSel.id(R.id.rdanctestNo10).isChecked())
                    tblAncTests.setTestDone("No");
            } else if (i == 11) {
                tblAncTests.setTestValue(aqPMSel.id(R.id.etanctestval11).getText().toString());
                if (aqPMSel.id(R.id.rdanctestYes11).isChecked())
                    tblAncTests.setTestDone("Yes");
                else if (aqPMSel.id(R.id.rdanctestNo11).isChecked())
                    tblAncTests.setTestDone("No");
            } else if (i == 12) {
                tblAncTests.setTestValue(aqPMSel.id(R.id.etanctestval12).getText().toString());
                if (aqPMSel.id(R.id.rdanctestYes12).isChecked())
                    tblAncTests.setTestDone("Yes");
                else if (aqPMSel.id(R.id.rdanctestNo12).isChecked())
                    tblAncTests.setTestDone("No");
            } else if (i == 13) {
                tblAncTests.setTestValue(aqPMSel.id(R.id.etanctestval13).getText().toString());
                if (aqPMSel.id(R.id.rdanctestYes13).isChecked())
                    tblAncTests.setTestDone("Yes");
                else if (aqPMSel.id(R.id.rdanctestNo13).isChecked())
                    tblAncTests.setTestDone("No");
            } else if (i == 14) {
                tblAncTests.setTestValue(aqPMSel.id(R.id.etanctestval14).getText().toString());
            } else if (i == 15) {
                tblAncTests.setTestValue(aqPMSel.id(R.id.etanctestval15).getText().toString());
            } else if (i == 16) {
                tblAncTests.setTestValue(aqPMSel.id(R.id.etanctestval16).getText().toString());
                if (aqPMSel.id(R.id.rdanctestYes16).isChecked())
                    tblAncTests.setTestDone("Yes");
                else if (aqPMSel.id(R.id.rdanctestNo16).isChecked())
                    tblAncTests.setTestDone("No");
            }

            tblAncTests.setTestId(ancTestsMasterList.get(i - 1).getTestId());
            tblAncTests.setUserId(woman.getUserId());
            tblAncTests.setWomenId(woman.getWomanId());
            tblAncTests.setTransId(transId);
            if (tblAncTests.getTestDone() != null ||
                    (tblAncTests.getTestValue() != null && tblAncTests.getTestValue().trim().length() > 0))
                ancTestsListAdd.add(tblAncTests);
        }
    }

    //    11May2021 Arpitha
    private void disableAllTests() {
        aqPMSel.id(R.id.etbpdiastolic).enabled(false);
        aqPMSel.id(R.id.etbpsystolic).enabled(false);
        aqPMSel.id(R.id.etanctestval2).enabled(false);
        aqPMSel.id(R.id.etanctestval3).enabled(false);
        aqPMSel.id(R.id.etanctestval4).enabled(false);
        aqPMSel.id(R.id.etanctestval5).enabled(false);
        aqPMSel.id(R.id.etanctestval6).enabled(false);
        aqPMSel.id(R.id.etanctestval7).enabled(false);
        aqPMSel.id(R.id.etanctestval8).enabled(false);
        aqPMSel.id(R.id.etanctestval9).enabled(false);
        aqPMSel.id(R.id.etanctestval10).enabled(false);
        aqPMSel.id(R.id.etanctestval11).enabled(false);
        aqPMSel.id(R.id.etanctestval12).enabled(false);
        aqPMSel.id(R.id.etanctestval13).enabled(false);
        aqPMSel.id(R.id.etanctestval14).enabled(false);
        aqPMSel.id(R.id.etanctestval15).enabled(false);
        aqPMSel.id(R.id.etanctestval16).enabled(false);

        aqPMSel.id(R.id.rdanctestYes1).enabled(false);
        aqPMSel.id(R.id.rdanctestYes2).enabled(false);
        aqPMSel.id(R.id.rdanctestYes3).enabled(false);
        aqPMSel.id(R.id.rdanctestYes4).enabled(false);
        aqPMSel.id(R.id.rdanctestYes5).enabled(false);
        aqPMSel.id(R.id.rdanctestYes6).enabled(false);
        aqPMSel.id(R.id.rdanctestYes7).enabled(false);
        aqPMSel.id(R.id.rdanctestYes8).enabled(false);
        aqPMSel.id(R.id.rdanctestYes9).enabled(false);
        aqPMSel.id(R.id.rdanctestYes10).enabled(false);
        aqPMSel.id(R.id.rdanctestYes11).enabled(false);
        aqPMSel.id(R.id.rdanctestYes12).enabled(false);
        aqPMSel.id(R.id.rdanctestYes13).enabled(false);
        aqPMSel.id(R.id.rdanctestYes16).enabled(false);


        aqPMSel.id(R.id.rdanctestNo1).enabled(false);
        aqPMSel.id(R.id.rdanctestNo2).enabled(false);
        aqPMSel.id(R.id.rdanctestNo3).enabled(false);
        aqPMSel.id(R.id.rdanctestNo4).enabled(false);
        aqPMSel.id(R.id.rdanctestNo5).enabled(false);
        aqPMSel.id(R.id.rdanctestNo6).enabled(false);
        aqPMSel.id(R.id.rdanctestNo7).enabled(false);
        aqPMSel.id(R.id.rdanctestNo8).enabled(false);
        aqPMSel.id(R.id.rdanctestNo9).enabled(false);
        aqPMSel.id(R.id.rdanctestNo10).enabled(false);
        aqPMSel.id(R.id.rdanctestNo11).enabled(false);
        aqPMSel.id(R.id.rdanctestNo12).enabled(false);
        aqPMSel.id(R.id.rdanctestNo13).enabled(false);
        aqPMSel.id(R.id.rdanctestNo16).enabled(false);
    }

    //    11May2021 Arpitha
    private void setOnClickListner() {

        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo1).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etbpdiastolic).gone();
                aqPMSel.id(R.id.etbpsystolic).gone();
                aqPMSel.id(R.id.etbpdiastolic).text("");
                aqPMSel.id(R.id.etbpsystolic).text("");
                aqPMSel.id(R.id.trancbptest).gone();

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes1).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etbpdiastolic).visible();
                aqPMSel.id(R.id.etbpsystolic).visible();
                aqPMSel.id(R.id.trancbptest).visible();

            }
        });

        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo2).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval2).gone();
                aqPMSel.id(R.id.etanctestval2).text("");

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes2).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval2).visible();

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo3).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval3).gone();
                aqPMSel.id(R.id.etanctestval3).text("");

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes3).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval3).visible();

            }
        });//        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo4).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval4).gone();
                aqPMSel.id(R.id.etanctestval4).text("");

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes4).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval4).visible();

            }
        });//        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo5).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval5).gone();
                aqPMSel.id(R.id.etanctestval5).text("");

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes5).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval5).visible();

            }
        });//        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo6).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval6).gone();
                aqPMSel.id(R.id.etanctestval6).text("");

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes6).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval6).visible();

            }
        });//        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo7).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval7).gone();
                aqPMSel.id(R.id.etanctestval7).text("");

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes7).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval7).visible();

            }
        });//        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo8).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval8).gone();
                aqPMSel.id(R.id.etanctestval8).text("");

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes8).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval8).visible();

            }
        });//        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo9).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval9).gone();
                aqPMSel.id(R.id.etanctestval9).text("");

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes9).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval9).visible();

            }
        });//        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo10).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval10).gone();
                aqPMSel.id(R.id.etanctestval10).text("");

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes10).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval10).visible();

            }
        });//        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo11).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval11).gone();
                aqPMSel.id(R.id.etanctestval11).text("");

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes11).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval11).visible();

            }
        });//        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo12).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval12).gone();
                aqPMSel.id(R.id.etanctestval12).text("");

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes12).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval12).visible();

            }
        });//        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo13).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval13).gone();
                aqPMSel.id(R.id.etanctestval13).text("");

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes13).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval13).visible();

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo16).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval16).gone();
                aqPMSel.id(R.id.etanctestval16).text("");

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes16).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval16).visible();

            }
        });

        aqPMSel.id(R.id.trtests).getView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (aqPMSel.id(R.id.lltest).getView().getVisibility() == View.VISIBLE) {
                    aqPMSel.id(R.id.lltest).gone();
                    aqPMSel.id(R.id.ivtests).background(R.drawable.ic_add_box);

                } else {
                    aqPMSel.id(R.id.lltest).visible();
                    aqPMSel.id(R.id.ivtests).background(R.drawable.ic_indeterminate_check_box);

                }
            }
        });

        aqPMSel.id(R.id.trlabtests).getView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (aqPMSel.id(R.id.lllabtest).getView().getVisibility() == View.VISIBLE) {
                    aqPMSel.id(R.id.lllabtest).gone();
                    aqPMSel.id(R.id.ivlabtest).background(R.drawable.ic_add_box);

                } else {
                    aqPMSel.id(R.id.lllabtest).visible();
                    aqPMSel.id(R.id.ivlabtest).background(R.drawable.ic_indeterminate_check_box);

                }
            }
        });
    }

    //11May2021 Arpitha
    private void setAncTestDataToFileds(int serNo) throws SQLException {

        WomanServiceRepository womanServiceRepository = new WomanServiceRepository(databaseHelper);
        List<TblAncTests> ancTestsList = womanServiceRepository.
                getAncTestsToDisplay(woman.getUserId(), woman.getWomanId(),
                        serNo);
        if (ancTestsList != null && ancTestsList.size() > 0) {
            for (int k = 0; k < ancTestsList.size(); k++) {
                int i = ancTestsList.get(k).getTestId();
                if (i == 14 || i == 15) {
                    if (i == 14)
                        aqPMSel.id(R.id.etanctestval14).text(ancTestsList.get(k).getTestValue());
                    else if (i == 15)
                        aqPMSel.id(R.id.etanctestval15).text(ancTestsList.get(k).getTestValue());

                } else {
                    if (ancTestsList.get(k).getTestDone() != null && ancTestsList.get(k).getTestDone().equalsIgnoreCase("Yes")) {

                        //aqPMSel.id(ettestval + i).visible();
                        if (i == 1) {
                            aqPMSel.id(R.id.rdanctestYes1).checked(true);
//                            aqPMSel.id(R.id.etanctestval1).visible();
                            aqPMSel.id(R.id.trancbptest).visible();
                            aqPMSel.id(R.id.etbpsystolic).text(ancTestsList.get(k).getTestValue().split("/")[0]);
                            if(ancTestsList.get(k).getTestValue().split("/").length>1)
                            aqPMSel.id(R.id.etbpdiastolic).text(ancTestsList.get(k).getTestValue().split("/")[1]);
//                            aqPMSel.id(R.id.etanctestval1).text(ancTestsList.get(k).getTestValue());
                        } else if (i == 2) {
                            aqPMSel.id(R.id.rdanctestYes2).checked(true);
                            aqPMSel.id(R.id.etanctestval2).visible();
                            aqPMSel.id(R.id.etanctestval2).text(ancTestsList.get(k).getTestValue());
                        } else if (i == 3) {
                            aqPMSel.id(R.id.rdanctestYes3).checked(true);
                            aqPMSel.id(R.id.etanctestval3).visible();
                            aqPMSel.id(R.id.etanctestval3).text(ancTestsList.get(k).getTestValue());
                        } else if (i == 4) {
                            aqPMSel.id(R.id.rdanctestYes4).checked(true);
                            aqPMSel.id(R.id.etanctestval4).visible();
                            aqPMSel.id(R.id.etanctestval4).text(ancTestsList.get(k).getTestValue());
                        } else if (i == 5) {
                            aqPMSel.id(R.id.rdanctestYes5).checked(true);
                            aqPMSel.id(R.id.etanctestval5).visible();
                            aqPMSel.id(R.id.etanctestval5).text(ancTestsList.get(k).getTestValue());
                        } else if (i == 6) {
                            aqPMSel.id(R.id.rdanctestYes6).checked(true);
                            aqPMSel.id(R.id.etanctestval6).visible();
                            aqPMSel.id(R.id.etanctestval6).text(ancTestsList.get(k).getTestValue());
                        } else if (i == 7) {
                            aqPMSel.id(R.id.rdanctestYes7).checked(true);
                            aqPMSel.id(R.id.etanctestval7).visible();
                            aqPMSel.id(R.id.etanctestval7).text(ancTestsList.get(k).getTestValue());
                        } else if (i == 8) {
                            aqPMSel.id(R.id.rdanctestYes8).checked(true);
                            aqPMSel.id(R.id.etanctestval8).visible();
                            aqPMSel.id(R.id.etanctestval8).text(ancTestsList.get(k).getTestValue());
                        } else if (i == 9) {
                            aqPMSel.id(R.id.rdanctestYes9).checked(true);
                            aqPMSel.id(R.id.etanctestval9).visible();
                            aqPMSel.id(R.id.etanctestval9).text(ancTestsList.get(k).getTestValue());
                        } else if (i == 10) {
                            aqPMSel.id(R.id.rdanctestYes10).checked(true);
                            aqPMSel.id(R.id.etanctestval10).visible();
                            aqPMSel.id(R.id.etanctestval10).text(ancTestsList.get(k).getTestValue());
                        } else if (i == 11) {
                            aqPMSel.id(R.id.rdanctestYes11).checked(true);
                            aqPMSel.id(R.id.etanctestval11).visible();
                            aqPMSel.id(R.id.etanctestval11).text(ancTestsList.get(k).getTestValue());
                        } else if (i == 12) {
                            aqPMSel.id(R.id.rdanctestYes12).checked(true);
                            aqPMSel.id(R.id.etanctestval12).visible();
                            aqPMSel.id(R.id.etanctestval12).text(ancTestsList.get(k).getTestValue());
                        } else if (i == 13) {
                            aqPMSel.id(R.id.rdanctestYes13).checked(true);
                            aqPMSel.id(R.id.etanctestval13).visible();
                            aqPMSel.id(R.id.etanctestval13).text(ancTestsList.get(k).getTestValue());
                        } else if (i == 16) {
                            aqPMSel.id(R.id.rdanctestYes16).checked(true);
                            aqPMSel.id(R.id.etanctestval16).visible();
                            aqPMSel.id(R.id.etanctestval16).text(ancTestsList.get(k).getTestValue());
                        }
                    } else if (ancTestsList.get(k).getTestDone() != null && ancTestsList.get(k).getTestDone().equalsIgnoreCase("No")) {
                        if (i == 1) {
                            aqPMSel.id(R.id.rdanctestNo1).checked(true);
                            aqPMSel.id(R.id.etbpsystolic).gone();
                            aqPMSel.id(R.id.etbpdiastolic).gone();
                        } else if (i == 2) {
                            aqPMSel.id(R.id.rdanctestNo2).checked(true);
                            aqPMSel.id(R.id.etanctestval2).gone();
                        } else if (i == 3) {
                            aqPMSel.id(R.id.rdanctestNo3).checked(true);
                            aqPMSel.id(R.id.etanctestval3).gone();
                        } else if (i == 4) {
                            aqPMSel.id(R.id.rdanctestNo4).checked(true);
                            aqPMSel.id(R.id.etanctestval4).gone();
                        } else if (i == 5) {
                            aqPMSel.id(R.id.rdanctestNo5).checked(true);
                            aqPMSel.id(R.id.etanctestval5).gone();
                        } else if (i == 6) {
                            aqPMSel.id(R.id.rdanctestNo6).checked(true);
                            aqPMSel.id(R.id.etanctestval6).gone();
                        } else if (i == 7) {
                            aqPMSel.id(R.id.rdanctestNo7).checked(true);
                            aqPMSel.id(R.id.etanctestval7).gone();
                        } else if (i == 8) {
                            aqPMSel.id(R.id.rdanctestNo8).checked(true);
                            aqPMSel.id(R.id.etanctestval8).gone();
                        } else if (i == 9) {
                            aqPMSel.id(R.id.rdanctestNo9).checked(true);
                            aqPMSel.id(R.id.etanctestval9).gone();
                        } else if (i == 10) {
                            aqPMSel.id(R.id.rdanctestNo10).checked(true);
                            aqPMSel.id(R.id.etanctestval10).gone();
                        } else if (i == 11) {
                            aqPMSel.id(R.id.rdanctestNo11).checked(true);
                            aqPMSel.id(R.id.etanctestval11).gone();
                        } else if (i == 12) {
                            aqPMSel.id(R.id.rdanctestNo12).checked(true);
                            aqPMSel.id(R.id.etanctestval12).gone();
                        } else if (i == 13) {
                            aqPMSel.id(R.id.rdanctestNo13).checked(true);
                            aqPMSel.id(R.id.etanctestval13).gone();
                        } else if (i == 16) {
                            aqPMSel.id(R.id.rdanctestNo16).checked(true);
                            aqPMSel.id(R.id.etanctestval16).gone();
                        }
                    }
                }
            /*} else {
                if (i == 14)
                    aqPMSel.id(R.id.etanctestval14).text(ancTestsList.get(i - 1).getTestValue());
                else if (i == 15)
                    aqPMSel.id(R.id.etanctestval15).text(ancTestsList.get(i - 1).getTestValue());

            }*/
            }
        }


        disableAllTests();
    }

}