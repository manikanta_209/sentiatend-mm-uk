//27Aug2019 Arpitha - remove unused code
package com.sc.stmansi.services;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.sc.stmansi.R;

public class CustomSpinnerAdapter extends ArrayAdapter {


    String[] spinnerfiltername;
    int[] spinnerImages;
    Context mContext;

//    constructor
    public CustomSpinnerAdapter(@NonNull Context context, String[] titles, int[] images) {
        super(context, R.layout.custom_spinnerlayout);
        this.spinnerfiltername = titles;
        this.spinnerImages = images;
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return spinnerfiltername.length;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder mViewHolder = new ViewHolder();
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) mContext.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.custom_spinnerlayout, parent, false);
            mViewHolder.mFlag = convertView.findViewById(R.id.ivFiltericon);
            mViewHolder.mName = convertView.findViewById(R.id.tvfiltername);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
        }
        mViewHolder.mFlag.setImageResource(spinnerImages[position]);
        mViewHolder.mName.setText(spinnerfiltername[position]);

        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return getView(position, convertView, parent);
    }

//    static class
    private static class ViewHolder {
        ImageView mFlag;
        TextView mName;
    }
}
