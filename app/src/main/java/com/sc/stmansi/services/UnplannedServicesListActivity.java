package com.sc.stmansi.services;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.legacy.app.ActionBarDrawerToggle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.About.AboutActivity;
import com.sc.stmansi.HomeVisit.HomeVisit;
import com.sc.stmansi.HomeVisit.HomeVisitListActivity;
import com.sc.stmansi.HomeVisit.PncHomeVisit;
import com.sc.stmansi.common.ActivitiesStack;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.common.NavDrawerItem;
import com.sc.stmansi.common.NavDrawerListAdapter;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.deactivate.WomanDeactivateActivity;
import com.sc.stmansi.delivery.DeliveryInfoActivity;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.recyclerview.ClickListener;
import com.sc.stmansi.recyclerview.WomanServicesAdapter;
import com.sc.stmansi.repositories.HomeVisitRepository;
import com.sc.stmansi.repositories.LoginRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.repositories.WomanServiceRepository;
import com.sc.stmansi.tables.TblAncTests;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.tables.tblserviceslist;
import com.sc.stmansi.viewprofile.ViewProfileActivity;
import com.sc.stmansi.womanlist.RegisteredWomenActionTabs;

import java.lang.ref.WeakReference;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.sc.stmansi.R;

public class UnplannedServicesListActivity extends AppCompatActivity implements ClickListener {

    private tblregisteredwomen woman;
    private AQuery aqUnServ;
    private List<tblserviceslist> sList;

    private AlertDialog alertdialog = null;
    private tblserviceslist arrVal;
    private AQuery aqPMSel;
    private AlertDialog.Builder alert;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private ArrayList<NavDrawerItem> navDrawerItems;
    private ListView mDrawerList;

    private RecyclerView recyclerView;
    private List<tblserviceslist> services;
    private RecyclerView.Adapter adapter;

    private DatabaseHelper databaseHelper;
    private AppState appState;

    //03Oct2019 - Bindu - get wvisitdetailsregfields
    private List<tblregisteredwomen> wVisitDetails;

    private TblInstusers user;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servicessummary);
        try {
            Bundle bundle = getIntent().getBundleExtra("globalState");
            appState = bundle.getParcelable("appState");

            databaseHelper = getHelper();
            WomanRepository womanRepository = new WomanRepository(databaseHelper);
            woman = womanRepository.getRegistartionDetails(appState.selectedWomanId, appState.sessionUserId);

            UserRepository userRepo = new UserRepository(databaseHelper);
            user = userRepo.getUserWithUserId(appState.sessionUserId);

            aqUnServ = new AQuery(this);

            aqUnServ.id(R.id.spnfilter).gone();

            initiateDrawer();

            if (woman.getRegADDate() != null && woman.getRegADDate().length() > 0) {
                getSupportActionBar().setTitle(woman.getRegWomanName() + "\t");
            } else {
                int noOfWeeks = DateTimeUtil.getNumberOfWeeksFromLMP(woman.getRegLMP());
                getSupportActionBar().setTitle(woman.getRegWomanName() + " (" + noOfWeeks + " "
                        + getResources().getString(R.string.weeks) + ")");
            }

            /* RecyclerView.ScrollListener not registered; pagination is not required for now as the number of records is less than 20.
             *  Since the view height in RecyclerView is small it may not be correct to rely on position of the last item returned by
             *  linearLayout.findLastCompletelyVisibleItemPosition(). Analyze this and consider relying on RecyclerView.Adapter.getItemCount
             * */

            recyclerView = findViewById(R.id.woman_services_recycler_view);
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            recyclerView.setHasFixedSize(true);

            // use a linear layout manager
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(linearLayoutManager);

            services = new ArrayList<>();
            adapter = new WomanServicesAdapter(this, services, this, woman);
            recyclerView.setAdapter(adapter);

            new UnplannedServicesLoader(this).execute();

            //26Jul2019 - Bindu
            aqUnServ.id(R.id.llnumofDays).gone();
            aqUnServ.id(R.id.txtpending30).text(getResources().getString(R.string.unplannedserviceslist));

            //03Oct2019 - Bindu - Get Visit details from reg table
            HomeVisitRepository homeRepo = new HomeVisitRepository(databaseHelper);
            wVisitDetails = homeRepo.getVisitDetailsFrmReg(woman.getWomanId(),user.getUserId());
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    List<tblserviceslist> getUnplannedServicesForWoman() throws Exception {
        WomanServiceRepository womanServiceRepository = new WomanServiceRepository(databaseHelper);
        return womanServiceRepository.getFilteredServicesForWoman(woman);
    }

    public void addServiceData(final Context context, final tblserviceslist selectedService) throws SQLException {
        final View customView = LayoutInflater.from(context).inflate(R.layout.add_services_info, null);

        aqPMSel = new AQuery(customView);

        //06May2021 Arpitha
        String strServType = "";
        int identifier = context.getResources().getIdentifier(selectedService.getServiceType().toLowerCase(),
                "string", "com.sc.stmansi");
        if(identifier > 0)
        strServType = (context.getResources().getString(identifier));
        else
            strServType = selectedService.getServiceType();

        aqPMSel.id(R.id.txtservicename).text("Service Name: " + strServType);
        aqPMSel.id(R.id.etdate).text(DateTimeUtil.getTodaysDate());
        aqPMSel.id(R.id.txtNameval).text(woman.getRegWomanName());
        if(woman.getRegPregnantorMother()==1)//15Nov2019 Arpitha
            aqPMSel.id(R.id.txtlmpval).text(woman.getRegLMP());
        else {
            aqPMSel.id(R.id.txtlmpval).text(woman.getRegADDate());
            aqPMSel.id(R.id.tvlmp).text(getResources().getString(R.string.tvadd));
        }
        aqPMSel.id(R.id.tvdurationvalmin).text(selectedService.getServiceDueDateMin() );

        aqPMSel.id(R.id.tvdurationvalmin).text(selectedService.getServiceDueDateMax());



        //        07May2021 Arpitha - set fields based on service type
        if (strServType.contains(getResources().getString(R.string.tt))) {
            aqPMSel.id(R.id.trnooftabgiven).gone();
            aqPMSel.id(R.id.trnooftabconsumed).gone();
            aqPMSel.id(R.id.llanctests).gone();
        } else if (strServType.contains(getResources().getString(R.string.ifa)) ||
                strServType.contains
                        (getResources().getString(R.string.folicacid)) ||
                strServType.contains(getResources().getString(R.string.calcium))) {
            aqPMSel.id(R.id.trnooftabgiven).visible();
            aqPMSel.id(R.id.trnooftabconsumed).visible();
            aqPMSel.id(R.id.llanctests).gone();
        } else if (strServType.contains(getResources().getString(R.string.ancsave)) ||
        strServType.contains(getResources().getString(R.string.anc))) {//17May2021 Arpitha
            aqPMSel.id(R.id.trnooftabgiven).gone();
            aqPMSel.id(R.id.trnooftabconsumed).gone();
            aqPMSel.id(R.id.llanctests).visible();

            if(selectedService.getServiceNumber() == 1 )
            {

                aqPMSel.id(R.id.trpregtest).visible();
                aqPMSel.id(R.id.trbabygrowthscan).gone();
                aqPMSel.id(R.id.trbreastheart).gone();
            }else
            {
                aqPMSel.id(R.id.trpregtest).gone();
                aqPMSel.id(R.id.trbabygrowthscan).visible();
                aqPMSel.id(R.id.trbreastheart).visible();
            }
        }

//        15May2021 Arpitha

        aqPMSel.id(R.id.trtests).getView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (aqPMSel.id(R.id.lltest).getView().getVisibility() == View.VISIBLE) {
                    aqPMSel.id(R.id.lltest).gone();
                    aqPMSel.id(R.id.ivtests).background(R.drawable.ic_add_box);

                } else {
                    aqPMSel.id(R.id.lltest).visible();
                    aqPMSel.id(R.id.ivtests).background(R.drawable.ic_indeterminate_check_box);

                }
            }
        });

        aqPMSel.id(R.id.trlabtests).getView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (aqPMSel.id(R.id.lllabtest).getView().getVisibility() == View.VISIBLE) {
                    aqPMSel.id(R.id.lllabtest).gone();
                    aqPMSel.id(R.id.ivlabtest).background(R.drawable.ic_add_box);

                } else {
                    aqPMSel.id(R.id.lllabtest).visible();
                    aqPMSel.id(R.id.ivlabtest).background(R.drawable.ic_indeterminate_check_box);

                }
            }
        });

        WomanServiceRepository womanServiceRepository = new WomanServiceRepository(databaseHelper);
        tblserviceslist arrVal = womanServiceRepository.getUnplannedService(woman.getWomanId(), selectedService.getServiceId(), selectedService.getServiceNumber());//05May2021 Arpitha - serv number
        if (arrVal != null) {
            try {
                aqPMSel.id(R.id.etdate).text(arrVal.getServiceActualDateofAction());
                aqPMSel.id(R.id.etcomments).text(arrVal.getServiceComments());
                aqPMSel.id(R.id.etfacname).text(arrVal.getServiceProvidedFacName());
                aqPMSel.id(R.id.spnfactype).setSelection(Arrays.asList(getResources().getStringArray(R.array.facilitytype))
                        .indexOf(arrVal.getServiceProvidedFacType())); //12Aug2019 - Bindu set spinner value
                aqPMSel.id(R.id.etdate).enabled(false);
                aqPMSel.id(R.id.etfacname).enabled(false);
                aqPMSel.id(R.id.spnfactype).enabled(false);
                aqPMSel.id(R.id.etcomments).enabled(false);
                aqPMSel.id(R.id.btnsave).enabled(false);
                //22Jul2019 - Bindu
                aqPMSel.id(R.id.btnsave).gone();
                aqPMSel.id(R.id.trduration).gone();
                aqPMSel.id(R.id.trspnfacname).gone(); //25Jul2019 - Bindu
                aqPMSel.id(R.id.tvfacname).text(getResources().getString(R.string.facilityname));
                //15Aug2019 -Bindu
                aqPMSel.id(R.id.txtheading).text(getResources().getString(R.string.view) + " : " + arrVal.getServiceType());

                //                07May2021 Arpitha
                aqPMSel.id(R.id.etnooftabgiven).text(arrVal.getServiceDetail1());
                aqPMSel.id(R.id.etnooftabconsumed).text(arrVal.getServiceDetail2());
                aqPMSel.id(R.id.etnooftabgiven).enabled(false);
                aqPMSel.id(R.id.etnooftabconsumed).enabled(false);

//                10May2021 Arpitha
                setAncTestDataToFileds(arrVal.getServiceNumber());
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            }
        }

        aqPMSel.id(R.id.btnsave).getImageView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    confirmAlert(selectedService.getServiceType(), selectedService.getServiceId());
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }
            }
        });

        aqPMSel.id(R.id.btncancel).getImageView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    confirmAlert(null, 0);
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }
            }
        });

        alert = new AlertDialog.Builder(context, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        alert.setView(customView).setTitle("").setPositiveButton(
                "", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        alertdialog.setCancelable(false);
                        alertdialog.show();
                        try {
                            confirmAlert(selectedService.getServiceType(), selectedService.getServiceId());
                        } catch (Exception e) {
                            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                        }
                    }
                });

        alertdialog = alert.create();
//        alertdialog.setTitle(context.getResources().getString(R.string.m123));
        alertdialog.show();
    }

    /**
     * Confirm Alert on Click save
     */
    private void confirmAlert(final String serviceType, final int serviceId) throws Exception {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        String message = getResources().getString(R.string.areyousuretoexit); //22Jul2019 -Bindu
        alertDialogBuilder.setMessage(message).setCancelable(false)
                .setNegativeButton(getResources().getString(R.string.m121),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                try {

                                    alertdialog.cancel();

                                } catch (Exception e) {


                                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                                }
                            }
                        })
                .setPositiveButton((getResources().getString(R.string.m122)),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    private void initiateDrawer() {
        navDrawerItems = new ArrayList<>();
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.sp_WRegEdit), R.drawable.registration));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.services), R.drawable.anm_pending_activities)); // 20Jul2019 - Bindu
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.addunplannedservices), R.drawable.anm_pending_activities));
        //29Aug2019 - Bindu - Add Homevisit
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.homevisitadd),
                R.drawable.prenatalhomevisit));

        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.womanhomevisithistory),
                R.drawable.prenatalhomevisit));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deliveryHeading), R.drawable.delivery_info));//01Oct2019 Arpitha

        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deactivate),
                R.drawable.deactivate));//28Nov2019 Arpitha

        mDrawerLayout = findViewById(R.id.ett_drawer_layout);
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        mDrawerList = findViewById(R.id.ett_left_drawer);
        mDrawerList.setAdapter(new NavDrawerListAdapter(getApplicationContext(), navDrawerItems));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, /* DrawerLayout object */
                R.drawable.ic_drawer, R.string.drawer_open,  R.string.drawer_close) {
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu(); // creates call to
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                getSupportActionBar().setDisplayShowTitleEnabled(false);
                getSupportActionBar().setDisplayShowCustomEnabled(false);
                int noOfWeeks;
                try {
                    if (woman.getRegADDate() != null && woman.getRegADDate().length() > 0) {
                        getSupportActionBar().setTitle((woman.getRegWomanName() + "\t"));
                    } else {
                        noOfWeeks = DateTimeUtil.getNumberOfWeeksFromLMP(woman.getRegLMP());
                        getSupportActionBar().setTitle((woman.getRegWomanName() + " (" + noOfWeeks + " "
                                + getResources().getString(R.string.weeks) + ")"));
                    }
                } catch (ParseException e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }
            }

            public void onDrawerOpened(View drawerView) {
                try {
                    invalidateOptionsMenu(); // creates call to
                    getSupportActionBar().setDisplayShowHomeEnabled(false);
                    getSupportActionBar().setDisplayShowTitleEnabled(false);
                    getSupportActionBar().setDisplayShowCustomEnabled(true);
                    getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);

                    aqUnServ.id(R.id.ivWomanImg).getImageView().setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            mDrawerLayout.closeDrawer(mDrawerList);
                        }
                    });

                    aqUnServ.id(R.id.tvWomanName).text((woman.getRegWomanName() + "\t"));
                    if (woman.getRegADDate().length() > 0) {
                        aqUnServ.id(R.id.tvInfo1).text((getResources().getString(R.string.tvadd) + " " + woman.getRegADDate()));
                    } else {
                        int noOfWeeks = DateTimeUtil.getNumberOfWeeksFromLMP(woman.getRegLMP());
                        aqUnServ.id(R.id.tvInfo1).text((noOfWeeks + getResources().getString(R.string.weeks)));
                    }
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    /**
     * This method is to sync drawertoggle and to replace up caret icon to menu
     * icon in Action Bar
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        try {
            addServiceData(UnplannedServicesListActivity.this, services.get(recyclerView.getChildLayoutPosition(v)));
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    @Override
    public boolean onLongClick(View v) {
        return false;
    }

    /**
     * The click listner for ListView in the navigation drawer
     */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            mDrawerLayout.closeDrawer(mDrawerList);
            try {
                if (position == 0) {
                    Intent viewprof = new Intent(UnplannedServicesListActivity.this, ViewProfileActivity.class);
                    viewprof.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                    startActivity(viewprof);
                } else if (position == 1) {
                    Intent unplanned = new Intent(UnplannedServicesListActivity.this, ServicesSummaryActivity.class);
                    unplanned.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                    startActivity(unplanned);
                } else if (position == 2) {
                    Intent unplanned = new Intent(UnplannedServicesListActivity.this, UnplannedServicesActivity.class);
                    unplanned.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                   unplanned.putExtra("woman",woman);
                    startActivity(unplanned);
                }
                //14Sep2019 - Bindu - Add Home visit
                else if (position == 3) {
                    if(woman.getRegPregnantorMother() == 2 ){ //02Dec2019 - Bindu - add condition - ANC/PNC Home visit
                        Intent nextScreen = new Intent(UnplannedServicesListActivity.this, PncHomeVisit.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        startActivity(nextScreen);
                    } else {
                        int daysDiff = DateTimeUtil.getDaysBetweenDates(DateTimeUtil.getTodaysDate(), woman.getRegLMP());
                        if (daysDiff > 210) {
                            displayNearingDelAlertMessage(woman);
                        } else {
                            Intent homevisit = new Intent(UnplannedServicesListActivity.this, HomeVisit.class);
                            homevisit.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(homevisit);
                        }
                    }
                }else if (position == 4) {
                    //   if(woman.getLastAncVisitDate().length() > 0 || woman.getLastPncVisitDate().length() > 0) {
                    //if(wVisitDetails != null && wVisitDetails.get(0).getLastAncVisitDate().length() > 0 || wVisitDetails.get(0).getLastPncVisitDate().length() > 0 )  {
                        Intent homevisit = new Intent(UnplannedServicesListActivity.this, HomeVisitListActivity.class);
                        homevisit.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                        startActivity(homevisit);
                   /* }else {
                        Toast.makeText(UnplannedServicesListActivity.this, getResources().getString(R.string.norecords), Toast.LENGTH_SHORT).show();
                    }*/
                }
                //01Oct2019 Arpitha
                else if(position == 5) {
                    if(woman.getRegPregnantorMother()==2 || DateTimeUtil.getNumberOfDaysFromLMP(woman.getRegLMP())>210) {
                        Intent del = new Intent(UnplannedServicesListActivity.this, DeliveryInfoActivity.class);
                        del.putExtra("woman", woman);
                        del.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        startActivity(del);
                    }else
                        Toast.makeText(getApplicationContext(),getResources().getString(R.string.m132),Toast.LENGTH_LONG).show();

                }   //                  28Nov2019 Arpitha
                else if(position == 6) {
                    Intent deact = new Intent(UnplannedServicesListActivity.this, WomanDeactivateActivity.class);
                    deact.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    deact.putExtra("woman", woman);
                    startActivity(deact);
                }
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            }
        }
    }

    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        return b;
    }

    /**
     * Called whenever an item clicked in actionbar
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        try {


            if (mDrawerToggle.onOptionsItemSelected(item)) {
                return true;
            }
            // Handle action buttons
            switch (item.getItemId()) {

                case R.id.home: {
                    Intent goToScreen = new Intent(UnplannedServicesListActivity.this, MainMenuActivity.class);
                    goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    displayAlert(getResources().getString(R.string.exit), goToScreen);
                    // startActivity(goToScreen);
                    //  CommonClass.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
                    return true;
                }

                case R.id.logout: {
                    Intent goToScreen = new Intent(UnplannedServicesListActivity.this, LoginActivity.class);
                    displayAlert(getResources().getString(R.string.m111), goToScreen);
                    return true;
                }


                case R.id.wlist: {
                    Intent goToScreen = new Intent(UnplannedServicesListActivity.this, RegisteredWomenActionTabs.class);
                    displayAlert(getResources().getString(R.string.exit), goToScreen); //03Oct2019 - bindu - string chnage from logout to exit
                    return true;
                }
                //30Sep2019 - Bindu
                case R.id.about: {
                    Intent goToScreen = new Intent(UnplannedServicesListActivity.this, AboutActivity.class);
                    goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    displayAlert(getResources().getString(R.string.exit), goToScreen);
                    return true;
                }

                default:
                    return super.onOptionsItemSelected(item);

            }
        }catch (Exception e) {
        }
        return super.onOptionsItemSelected(item);
    }


    /** Initializes the Action Bar items */
    @Override
    public boolean onCreateOptionsMenu ( final Menu menu){

        try {
            getSupportActionBar().setIcon(R.drawable.ett);
            if (woman.getRegADDate() != null && woman.getRegADDate().length() > 0) {
                getSupportActionBar().setTitle((woman.getRegWomanName() + "\t"));
            } else {
                int noOfWeeks = DateTimeUtil.getNumberOfWeeksFromLMP(woman.getRegLMP());
                getSupportActionBar().setTitle((woman.getRegWomanName() + " (" + noOfWeeks + " "
                        + getResources().getString(R.string.weeks) + ")"));
            }
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer1); //27Sep2019 - Bindu - set drawer and enable true home btn
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);


            aqUnServ.id(R.id.tvWomanName).text(woman.getRegWomanName());
//            aqUnServ.id(R.id.tvInfo1).text(getResources().getString(R.string.tvlmp) + " - " + woman.getRegLMP()); // Bindu - set LMP label

            if(woman.getRegPregnantorMother()==1)//15Nov2019 Arpitha
                aqUnServ.id(R.id.tvInfo1).text(getResources().getString(R.string.tvlmp)+" "+woman.getRegLMP());
            else {
                aqUnServ.id(R.id.tvInfo1).text(getResources().getString(R.string.tvadd)+" "+woman.getRegADDate());
            }
            aqUnServ.id(R.id.ivWomanImg).gone();

            getMenuInflater().inflate(R.menu.servicesmenu, menu);

            menu.findItem(R.id.logout).setTitle((getResources().getString(R.string.action_logout)));


        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        return super.onCreateOptionsMenu(menu);
    }

    private void displayAlert(final String spanText2, final Intent goToScreen) throws Exception {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set dialog message
        alertDialogBuilder.setMessage(spanText2).setCancelable(false).setNegativeButton(
                getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (goToScreen != null) {

                            goToScreen.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                            startActivity(goToScreen);
                            if (spanText2.contains("logout")) {
                                try {
                                    LoginRepository loginRepository = new LoginRepository(databaseHelper);
                                    loginRepository.updateLogoutTime(appState.loginAuditId, appState.sessionUserId);
                                } catch (Exception e) {
                                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                                }
                            }
                        } else
                            dialog.cancel();
                    }
                }).setPositiveButton(getResources().getString(R.string.m122),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    static class UnplannedServicesLoader extends AsyncTask<Void, Void, Integer> {

        private WeakReference<UnplannedServicesListActivity> weakReference;

        public UnplannedServicesLoader(UnplannedServicesListActivity activity) {
            weakReference = new WeakReference<>(activity);
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            UnplannedServicesListActivity activity = weakReference.get();
            try {
                List<tblserviceslist> rows = activity.getUnplannedServicesForWoman();
                if(rows.size() > 0) {
                    activity.services.addAll(rows);
                    activity.adapter.notifyItemRangeInserted(0, activity.services.size());
                }
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            }
            return activity.services.size();
        }

        @Override
        protected void onPostExecute(Integer integer) {
            UnplannedServicesListActivity activity = weakReference.get();
            if(integer == 0) {
                activity.aqUnServ.id(R.id.txtnodatapending).text(R.string.no_data).visibility(View.VISIBLE);
                activity.recyclerView.setVisibility(View.GONE);
            } else {
                activity.aqUnServ.id(R.id.txtnodatapending).text(R.string.no_data).visibility(View.GONE);
                activity.recyclerView.setVisibility(View.VISIBLE);
            }
        }
    }

    /**
     * This method Calls the idle timeout
     */ //09Aug2019 - Bindu
    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if(appState!=null)
        new ActivitiesStack(this).delayedIdle(appState.idleTimeOut);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        /*
         * You'll need this in your class to release the helper when done.
         */
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

    //18Oct2019 - Bindu - Alert message nearing del
    public  void displayNearingDelAlertMessage(tblregisteredwomen regwoman) throws Exception{
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        String alertmsg =  regwoman.getRegWomanName() + ", " + " EDD : " + regwoman.getRegEDD() ;
        //alertDialogBuilder.setMessage(message).setCancelable(false).setTitle(Html.fromHtml("<font color='#FF7F27'>Alert -- </font><br><br><p>"+alertmsg + "</p>")).setNegativeButton(
        String msg = "<font color='#CC3333'> Nearing Delivery - </font>"+alertmsg;
        alertDialogBuilder.setMessage(getResources().getText(R.string.alerthomevisitnearingdelmsg)).setCancelable(false).setTitle(Html.fromHtml(msg)).setNegativeButton(
                getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent nextScreen = new Intent(UnplannedServicesListActivity.this, HomeVisit.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        startActivity(nextScreen);
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    //11May2021 Arpitha
    private void setAncTestDataToFileds(int serNo) throws SQLException {

        WomanServiceRepository womanServiceRepository = new WomanServiceRepository(databaseHelper);
        List<TblAncTests> ancTestsList = womanServiceRepository.
                getAncTestsToDisplay(woman.getUserId(), woman.getWomanId(),
                        serNo);
        if (ancTestsList != null && ancTestsList.size() > 0) {
            for (int k = 0; k < ancTestsList.size(); k++) {
                int i = ancTestsList.get(k).getTestId();
                if (i == 14 || i == 15) {
                    if (i == 14)
                        aqPMSel.id(R.id.etanctestval14).text(ancTestsList.get(k).getTestValue());
                    else if (i == 15)
                        aqPMSel.id(R.id.etanctestval15).text(ancTestsList.get(k).getTestValue());

                } else {
                    if (ancTestsList.get(k).getTestDone() != null && ancTestsList.get(k).getTestDone().equalsIgnoreCase("Yes")) {

                        //aqPMSel.id(ettestval + i).visible();
                        if (i == 1) {
                            aqPMSel.id(R.id.rdanctestYes1).checked(true);
//                            aqPMSel.id(R.id.etanctestval1).visible();
                            aqPMSel.id(R.id.trancbptest).visible();
                            aqPMSel.id(R.id.etbpsystolic).text(ancTestsList.get(k).getTestValue().split("/")[0]);
                            if(ancTestsList.get(k).getTestValue().split("/").length>1)
                                aqPMSel.id(R.id.etbpdiastolic).text(ancTestsList.get(k).getTestValue().split("/")[1]);
//                            aqPMSel.id(R.id.etanctestval1).text(ancTestsList.get(k).getTestValue());
                        } else if (i == 2) {
                            aqPMSel.id(R.id.rdanctestYes2).checked(true);
                            aqPMSel.id(R.id.etanctestval2).visible();
                            aqPMSel.id(R.id.etanctestval2).text(ancTestsList.get(k).getTestValue());
                        } else if (i == 3) {
                            aqPMSel.id(R.id.rdanctestYes3).checked(true);
                            aqPMSel.id(R.id.etanctestval3).visible();
                            aqPMSel.id(R.id.etanctestval3).text(ancTestsList.get(k).getTestValue());
                        } else if (i == 4) {
                            aqPMSel.id(R.id.rdanctestYes4).checked(true);
                            aqPMSel.id(R.id.etanctestval4).visible();
                            aqPMSel.id(R.id.etanctestval4).text(ancTestsList.get(k).getTestValue());
                        } else if (i == 5) {
                            aqPMSel.id(R.id.rdanctestYes5).checked(true);
                            aqPMSel.id(R.id.etanctestval5).visible();
                            aqPMSel.id(R.id.etanctestval5).text(ancTestsList.get(k).getTestValue());
                        } else if (i == 6) {
                            aqPMSel.id(R.id.rdanctestYes6).checked(true);
                            aqPMSel.id(R.id.etanctestval6).visible();
                            aqPMSel.id(R.id.etanctestval6).text(ancTestsList.get(k).getTestValue());
                        } else if (i == 7) {
                            aqPMSel.id(R.id.rdanctestYes7).checked(true);
                            aqPMSel.id(R.id.etanctestval7).visible();
                            aqPMSel.id(R.id.etanctestval7).text(ancTestsList.get(k).getTestValue());
                        } else if (i == 8) {
                            aqPMSel.id(R.id.rdanctestYes8).checked(true);
                            aqPMSel.id(R.id.etanctestval8).visible();
                            aqPMSel.id(R.id.etanctestval8).text(ancTestsList.get(k).getTestValue());
                        } else if (i == 9) {
                            aqPMSel.id(R.id.rdanctestYes9).checked(true);
                            aqPMSel.id(R.id.etanctestval9).visible();
                            aqPMSel.id(R.id.etanctestval9).text(ancTestsList.get(k).getTestValue());
                        } else if (i == 10) {
                            aqPMSel.id(R.id.rdanctestYes10).checked(true);
                            aqPMSel.id(R.id.etanctestval10).visible();
                            aqPMSel.id(R.id.etanctestval10).text(ancTestsList.get(k).getTestValue());
                        } else if (i == 11) {
                            aqPMSel.id(R.id.rdanctestYes11).checked(true);
                            aqPMSel.id(R.id.etanctestval11).visible();
                            aqPMSel.id(R.id.etanctestval11).text(ancTestsList.get(k).getTestValue());
                        } else if (i == 12) {
                            aqPMSel.id(R.id.rdanctestYes12).checked(true);
                            aqPMSel.id(R.id.etanctestval12).visible();
                            aqPMSel.id(R.id.etanctestval12).text(ancTestsList.get(k).getTestValue());
                        } else if (i == 13) {
                            aqPMSel.id(R.id.rdanctestYes13).checked(true);
                            aqPMSel.id(R.id.etanctestval13).visible();
                            aqPMSel.id(R.id.etanctestval13).text(ancTestsList.get(k).getTestValue());
                        } else if (i == 16) {
                            aqPMSel.id(R.id.rdanctestYes16).checked(true);
                            aqPMSel.id(R.id.etanctestval16).visible();
                            aqPMSel.id(R.id.etanctestval16).text(ancTestsList.get(k).getTestValue());
                        }
                    } else if (ancTestsList.get(k).getTestDone() != null && ancTestsList.get(k).getTestDone().equalsIgnoreCase("No")) {
                        if (i == 1) {
                            aqPMSel.id(R.id.rdanctestNo1).checked(true);
                            aqPMSel.id(R.id.etbpsystolic).gone();
                            aqPMSel.id(R.id.etbpdiastolic).gone();
                        } else if (i == 2) {
                            aqPMSel.id(R.id.rdanctestNo2).checked(true);
                            aqPMSel.id(R.id.etanctestval2).gone();
                        } else if (i == 3) {
                            aqPMSel.id(R.id.rdanctestNo3).checked(true);
                            aqPMSel.id(R.id.etanctestval3).gone();
                        } else if (i == 4) {
                            aqPMSel.id(R.id.rdanctestNo4).checked(true);
                            aqPMSel.id(R.id.etanctestval4).gone();
                        } else if (i == 5) {
                            aqPMSel.id(R.id.rdanctestNo5).checked(true);
                            aqPMSel.id(R.id.etanctestval5).gone();
                        } else if (i == 6) {
                            aqPMSel.id(R.id.rdanctestNo6).checked(true);
                            aqPMSel.id(R.id.etanctestval6).gone();
                        } else if (i == 7) {
                            aqPMSel.id(R.id.rdanctestNo7).checked(true);
                            aqPMSel.id(R.id.etanctestval7).gone();
                        } else if (i == 8) {
                            aqPMSel.id(R.id.rdanctestNo8).checked(true);
                            aqPMSel.id(R.id.etanctestval8).gone();
                        } else if (i == 9) {
                            aqPMSel.id(R.id.rdanctestNo9).checked(true);
                            aqPMSel.id(R.id.etanctestval9).gone();
                        } else if (i == 10) {
                            aqPMSel.id(R.id.rdanctestNo10).checked(true);
                            aqPMSel.id(R.id.etanctestval10).gone();
                        } else if (i == 11) {
                            aqPMSel.id(R.id.rdanctestNo11).checked(true);
                            aqPMSel.id(R.id.etanctestval11).gone();
                        } else if (i == 12) {
                            aqPMSel.id(R.id.rdanctestNo12).checked(true);
                            aqPMSel.id(R.id.etanctestval12).gone();
                        } else if (i == 13) {
                            aqPMSel.id(R.id.rdanctestNo13).checked(true);
                            aqPMSel.id(R.id.etanctestval13).gone();
                        } else if (i == 16) {
                            aqPMSel.id(R.id.rdanctestNo16).checked(true);
                            aqPMSel.id(R.id.etanctestval16).gone();
                        }
                    }
                }
            /*} else {
                if (i == 14)
                    aqPMSel.id(R.id.etanctestval14).text(ancTestsList.get(i - 1).getTestValue());
                else if (i == 15)
                    aqPMSel.id(R.id.etanctestval15).text(ancTestsList.get(i - 1).getTestValue());

            }*/
            }
        }


        disableAllTests();
    }

    //    11May2021 Arpitha
    private void disableAllTests() {
        aqPMSel.id(R.id.etbpdiastolic).enabled(false);
        aqPMSel.id(R.id.etbpsystolic).enabled(false);
        aqPMSel.id(R.id.etanctestval2).enabled(false);
        aqPMSel.id(R.id.etanctestval3).enabled(false);
        aqPMSel.id(R.id.etanctestval4).enabled(false);
        aqPMSel.id(R.id.etanctestval5).enabled(false);
        aqPMSel.id(R.id.etanctestval6).enabled(false);
        aqPMSel.id(R.id.etanctestval7).enabled(false);
        aqPMSel.id(R.id.etanctestval8).enabled(false);
        aqPMSel.id(R.id.etanctestval9).enabled(false);
        aqPMSel.id(R.id.etanctestval10).enabled(false);
        aqPMSel.id(R.id.etanctestval11).enabled(false);
        aqPMSel.id(R.id.etanctestval12).enabled(false);
        aqPMSel.id(R.id.etanctestval13).enabled(false);
        aqPMSel.id(R.id.etanctestval14).enabled(false);
        aqPMSel.id(R.id.etanctestval15).enabled(false);
        aqPMSel.id(R.id.etanctestval16).enabled(false);

        aqPMSel.id(R.id.rdanctestYes1).enabled(false);
        aqPMSel.id(R.id.rdanctestYes2).enabled(false);
        aqPMSel.id(R.id.rdanctestYes3).enabled(false);
        aqPMSel.id(R.id.rdanctestYes4).enabled(false);
        aqPMSel.id(R.id.rdanctestYes5).enabled(false);
        aqPMSel.id(R.id.rdanctestYes6).enabled(false);
        aqPMSel.id(R.id.rdanctestYes7).enabled(false);
        aqPMSel.id(R.id.rdanctestYes8).enabled(false);
        aqPMSel.id(R.id.rdanctestYes9).enabled(false);
        aqPMSel.id(R.id.rdanctestYes10).enabled(false);
        aqPMSel.id(R.id.rdanctestYes11).enabled(false);
        aqPMSel.id(R.id.rdanctestYes12).enabled(false);
        aqPMSel.id(R.id.rdanctestYes13).enabled(false);
        aqPMSel.id(R.id.rdanctestYes16).enabled(false);


        aqPMSel.id(R.id.rdanctestNo1).enabled(false);
        aqPMSel.id(R.id.rdanctestNo2).enabled(false);
        aqPMSel.id(R.id.rdanctestNo3).enabled(false);
        aqPMSel.id(R.id.rdanctestNo4).enabled(false);
        aqPMSel.id(R.id.rdanctestNo5).enabled(false);
        aqPMSel.id(R.id.rdanctestNo6).enabled(false);
        aqPMSel.id(R.id.rdanctestNo7).enabled(false);
        aqPMSel.id(R.id.rdanctestNo8).enabled(false);
        aqPMSel.id(R.id.rdanctestNo9).enabled(false);
        aqPMSel.id(R.id.rdanctestNo10).enabled(false);
        aqPMSel.id(R.id.rdanctestNo11).enabled(false);
        aqPMSel.id(R.id.rdanctestNo12).enabled(false);
        aqPMSel.id(R.id.rdanctestNo13).enabled(false);
        aqPMSel.id(R.id.rdanctestNo16).enabled(false);
    }

    //    11May2021 Arpitha
    private void setOnClickListner() {

        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo1).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval1).gone();
                aqPMSel.id(R.id.etbpsystolic).text("");
                aqPMSel.id(R.id.etbpdiastolic).text("");
                aqPMSel.id(R.id.trancbptest).gone();

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes1).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.trancbptest).visible();

            }
        });

        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo2).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval2).gone();
                aqPMSel.id(R.id.etanctestval2).text("");

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes2).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval2).visible();

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo3).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval3).gone();
                aqPMSel.id(R.id.etanctestval3).text("");

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes3).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval3).visible();

            }
        });//        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo4).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval4).gone();
                aqPMSel.id(R.id.etanctestval4).text("");

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes4).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval4).visible();

            }
        });//        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo5).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval5).gone();
                aqPMSel.id(R.id.etanctestval5).text("");

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes5).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval5).visible();

            }
        });//        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo6).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval6).gone();
                aqPMSel.id(R.id.etanctestval6).text("");

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes6).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval6).visible();

            }
        });//        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo7).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval7).gone();
                aqPMSel.id(R.id.etanctestval7).text("");

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes7).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval7).visible();

            }
        });//        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo8).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval8).gone();
                aqPMSel.id(R.id.etanctestval8).text("");

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes8).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval8).visible();

            }
        });//        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo9).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval9).gone();
                aqPMSel.id(R.id.etanctestval9).text("");

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes9).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval9).visible();

            }
        });//        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo10).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval10).gone();
                aqPMSel.id(R.id.etanctestval10).text("");

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes10).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval10).visible();

            }
        });//        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo11).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval11).gone();
                aqPMSel.id(R.id.etanctestval11).text("");

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes11).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval11).visible();

            }
        });//        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo12).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval12).gone();
                aqPMSel.id(R.id.etanctestval12).text("");

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes12).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval12).visible();

            }
        });//        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo13).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval13).gone();
                aqPMSel.id(R.id.etanctestval13).text("");

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes13).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval13).visible();

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo16).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval16).gone();
                aqPMSel.id(R.id.etanctestval16).text("");

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes16).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval16).visible();

            }
        });
    }
//    21May2021 Arpitha
    @Override
    public void onBackPressed() {

    }
}
