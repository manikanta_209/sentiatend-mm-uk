package com.sc.stmansi.services;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.DialogFragment;
import androidx.legacy.app.ActionBarDrawerToggle;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.misc.TransactionManager;
import com.sc.stmansi.About.AboutActivity;
import com.sc.stmansi.HomeVisit.HomeVisit;
import com.sc.stmansi.HomeVisit.HomeVisitListActivity;
import com.sc.stmansi.HomeVisit.PncHomeVisit;
import com.sc.stmansi.common.ActivitiesStack;
import com.sc.stmansi.common.AlertDialogUtil;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.common.NavDrawerItem;
import com.sc.stmansi.common.NavDrawerListAdapter;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.deactivate.WomanDeactivateActivity;
import com.sc.stmansi.delivery.DeliveryInfoActivity;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.repositories.ChildRepository;
import com.sc.stmansi.repositories.DeliveryRepository;
import com.sc.stmansi.repositories.FacilityRepository;
import com.sc.stmansi.repositories.LoginRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.repositories.WomanServiceRepository;
import com.sc.stmansi.tables.TblAncTests;
import com.sc.stmansi.tables.TblAncTestsMaster;
import com.sc.stmansi.tables.TblDeliveryInfo;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.tables.tblserviceslist;
import com.sc.stmansi.viewprofile.ViewProfileActivity;
import com.sc.stmansi.womanlist.RegisteredWomenActionTabs;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import com.sc.stmansi.R;

public class UnplannedServicesActivity extends AppCompatActivity {

    private AQuery aqUnplSv;
    private static tblregisteredwomen woman;//28Apr2021 Arpitha - made it static to access in ondateset
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private ArrayList<NavDrawerItem> navDrawerItems;
    private ListView mDrawerList;
    // 25Jul2019 - Bindu
    private Map<String, String> mapFacilityname;
    private ArrayList<String> facilitynamelist;
    private TblInstusers user;

    private DatabaseHelper databaseHelper;
    private AppState appState;
    
    //03Oct2019 - Bindu - get wvisitdetailsregfields
    private List<tblregisteredwomen> wVisitDetails;
    boolean childAlive,motherDead;
    //    10May2021 Arpitha
    Map<Integer, TblAncTests> anctestVal;
    ArrayList<TblAncTests> ancTestsListAdd;
    int rdtestYes = R.id.rdanctestYes;
    int rdtestNo = R.id.rdanctestNo;
    int ettestval = R.id.etanctestval;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unplannedactivity);
        try {
            databaseHelper = getHelper();

            Bundle b = getIntent().getBundleExtra("globalState");
            appState = b.getParcelable("appState");

            UserRepository userRepository = new UserRepository(databaseHelper);
            user = userRepository.getOneAuditedUser(appState.ashaId);

            aqUnplSv = new AQuery(this);
            woman = (tblregisteredwomen) getIntent().getSerializableExtra("woman");

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

            aqUnplSv.id(R.id.etdate).text(DateTimeUtil.getTodaysDate());
            aqUnplSv.id(R.id.txtNameval).text(woman.getRegWomanName());
            if(woman.getRegPregnantorMother()==1)
            aqUnplSv.id(R.id.txtlmpval).text(woman.getRegLMP());
            else {
                aqUnplSv.id(R.id.tvlmp).text(getResources().getString(R.string.tvadd));
                aqUnplSv.id(R.id.txtlmpval).text(woman.getRegADDate());
            }

            aqUnplSv.id(R.id.txtheading).text(getResources().
                    getString(R.string.addunplannedservice));

            initiateDrawer();


            WomanRepository womanRepository = new WomanRepository(databaseHelper);

            wVisitDetails = womanRepository.getVisitDetailsFrmReg(woman.getWomanId(),appState.sessionUserId);//04Dec2019 Arpitha


            aqUnplSv.id(R.id.btsave).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (ValidateServiceFields()) {  // Validate 22Jul2019 - Bindu
//                      06May2021 Arpitha
                            String servName = aqUnplSv.id(R.id.spnservname).getSelectedItem().toString().trim();
                            String s = getResources().getString(R.string.ancsave).trim();
                            if(servName.equalsIgnoreCase(s))
                                servName  = "ANC";
                            else if(aqUnplSv.id(R.id.spnservname).getSelectedItem()
                                    .toString().equalsIgnoreCase(getResources().getString(R.string.pnc)))
                                servName  = "PNC";
                            else if(aqUnplSv.id(R.id.spnservname).getSelectedItem()
                                    .toString().equalsIgnoreCase(getResources().getString(R.string.calcium)))
                                servName  = "Calcium";
                            else if(aqUnplSv.id(R.id.spnservname).getSelectedItem()
                                    .toString().equalsIgnoreCase(getResources().getString(R.string.folicacid)))
                                servName  = "FolicAcid";
                            else if(aqUnplSv.id(R.id.spnservname).getSelectedItem()
                                    .toString().equalsIgnoreCase(getResources().getString(R.string.ifa)))
                                servName  = "IFA";
                            confirmAlert(servName, 0);
                        }
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                    }

                }
            });

            aqUnplSv.id(R.id.btncancel).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        confirmAlertExit();
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                    }

                }
            });


            aqUnplSv.id(R.id.etdate).getEditText().setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (MotionEvent.ACTION_UP == event.getAction()) {
                        DatePickerFragment newFragment = DatePickerFragment.getInstance();
                        newFragment.setAQuery(aqUnplSv);
                        Bundle b = new Bundle();
                        b.putString("registeredLMP", woman.getRegLMP());
                        newFragment.setArguments(b);
                        newFragment.show(getSupportFragmentManager(), "datePicker");

                    }
                    return true;
                }
            });

            //25Jul2019 - Bindu

            aqUnplSv.id(R.id.spnfactype).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    try {
                        String selected = adapterView.getItemAtPosition(i).toString();
                        //                    15May2021 Bindu - add select, hmevisit and other from eng compare
                        if (!(selected.equalsIgnoreCase(getResources().getString(R.string.selecteng)) || selected.equalsIgnoreCase(getResources().getString(R.string.bhomeeng)) || selected.equalsIgnoreCase(getResources().getString(R.string.bsc)) ||
                                selected.equalsIgnoreCase(getResources().getString(R.string.botherseng)))) {

                            aqUnplSv.id(R.id.trspnfacname).visible();
                            aqUnplSv.id(R.id.tvfacname).text(getResources().getString(R.string.facilitynameothers));

                            facilitynamelist = new ArrayList<>();
                            FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
                            mapFacilityname = facilityRepository.getFacilityNames(aqUnplSv.id(R.id.spnfactype).getSelectedItem().toString(), user.getUserId());
                            facilitynamelist.add(getResources().getString(R.string.strselect)); //15Nov2019 - Bindu selectfacilityname  to strselect
                            for (Map.Entry<String, String> village : mapFacilityname.entrySet())
                                facilitynamelist.add(village.getKey());
                            facilitynamelist.add(getResources().getString(R.string.strother)); //15Nov2019 - Bindu other  to strother
                            ArrayAdapter<String> FacNameAdapter = new ArrayAdapter<>(UnplannedServicesActivity.this,
                                    R.layout.simple_spinner_dropdown_item, facilitynamelist);
                            FacNameAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            aqUnplSv.id(R.id.spnfacname).adapter(FacNameAdapter);
                        } else {
                            aqUnplSv.id(R.id.trspnfacname).gone();
                            aqUnplSv.id(R.id.tretfacname).visible();
                            aqUnplSv.id(R.id.tvfacname).text(getResources().getString(R.string.facilityname));
                        }
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            // Facility name on select listener
            aqUnplSv.id(R.id.spnfacname).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    try {
                        String selected = adapterView.getItemAtPosition(i).toString();
                        if (selected.equalsIgnoreCase(getResources().getString(R.string.strother))) {  //15Nov2019 - Bindu other  to strother
                            aqUnplSv.id(R.id.tretfacname).visible();
                            aqUnplSv.id(R.id.etfacname).text("");
                        } else {
                            aqUnplSv.id(R.id.tretfacname).gone();
                            aqUnplSv.id(R.id.etfacname).text("");
                        }
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            //20Aug2019 Arpitha
            aqUnplSv.id(R.id.imgcleardate).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    aqUnplSv.id(R.id.etdate).text(DateTimeUtil.getTodaysDate());
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.dateresettotodaysdate), Toast.LENGTH_LONG).show();//21Aug2019 Arpitha
                }
            });
            if(woman.getRegPregnantorMother() ==2 ) {
                ChildRepository childRepository = new ChildRepository(databaseHelper);
                childAlive = childRepository.checkIsChildAlive(woman.getWomanId());

                DeliveryRepository deliveryRepository = new DeliveryRepository(databaseHelper);
                List<TblDeliveryInfo> tblDeliveryInfo = deliveryRepository.getDeliveryData(woman.getWomanId(), woman.getUserId());

                if (tblDeliveryInfo != null && tblDeliveryInfo.size() > 0
                        && tblDeliveryInfo.get(0).getDelMothersDeath() != null &&
                        tblDeliveryInfo.get(0).getDelMothersDeath().equalsIgnoreCase("Y"))
                    motherDead = true;
            }

            if((woman.getDateDeactivated()!=null &&
                    woman.getDateDeactivated().trim().length()>0)
                    || user.getIsDeactivated()==1 ||
                    (woman.getRegPregnantorMother() ==2 && (!childAlive) && motherDead)  )
                disableScreen();//26Nov2019 Arpitha

//            27Nov2019 Arpitha
            String[] services;
            if(woman.getRegPregnantorMother() == 2)
            services = getResources().getStringArray(R.array.pncunplannedservices);
            else
                services = getResources().getStringArray(R.array.unplannedservices);

            ArrayAdapter adapter = new ArrayAdapter(this, R.layout.simple_spinner_item, services);
            aqUnplSv.id(R.id.spnservname).getSpinner().setAdapter(adapter);

//            11May2021 Arpitha
            aqUnplSv.id(R.id.spnservname).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    
                    if(woman.getRegPregnantorMother() == 1)
                    {
                        if( i == 1)
                        {
                            aqUnplSv.id(R.id.trnooftabgiven).gone();
                            aqUnplSv.id(R.id.trnooftabconsumed).gone();
                            aqUnplSv.id(R.id.llanctests).visible();
                            aqUnplSv.id(R.id.trpregtest).gone();

                        }else if( i > 1 )
                        {
                            aqUnplSv.id(R.id.trnooftabgiven).visible();
                            aqUnplSv.id(R.id.trnooftabconsumed).visible();
                            aqUnplSv.id(R.id.llanctests).gone();
                            
                        }
                    }
                    
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            setOnClickListner();//11May2021 Arpitha
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    /**
     * Confirm Alert on Click save
     */
    private void confirmAlert(final String serviceType, final int serviceId) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        String message;

        if (serviceType != null || serviceId != 0)
            message = getResources().getString(R.string.m099);
        else
            message = getResources().getString(R.string.m110);

        alertDialogBuilder = alertDialogBuilder.setMessage(message).setCancelable(false)
                .setNegativeButton(getResources().getString(R.string.m121),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                try {
                                    addService(serviceType, serviceId);
                                    // MainMenuActivity.callSyncMtd();  //12Aug2019 - Bindu - comment autosync TODO revisit
                                } catch (Exception e) {
                                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                                }
                            }
                        })
                .setPositiveButton((getResources().getString(R.string.m122)),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    void addService(String serviceType, int serviceId) {
        try {
            final tblserviceslist SLU = new tblserviceslist();
            SLU.setUserId(woman.getUserId());
            SLU.setWomenId(woman.getWomanId());
            String servicetype = serviceType;
            if (servicetype.contains("1"))
                servicetype = servicetype.replace("1", "");
            else if (servicetype.contains("2"))
                servicetype = servicetype.replace("2", "");
            else if (servicetype.contains("3"))
                servicetype = servicetype.replace("3", "");

            if(serviceType.equalsIgnoreCase("Folic Acid"))
                servicetype = "FolicAcid";
            SLU.setServiceType(servicetype);
            SLU.setServiceId(serviceId);
            SLU.setServiceComments(aqUnplSv.id(R.id.etcomments).getText().toString());
            SLU.setServiceProvidedFacType(aqUnplSv.id(R.id.spnfactype).getSelectedItem().toString());//12Aug2019 - Bindu - change from positin to direct val
            SLU.setServiceActualDateofAction(aqUnplSv.id(R.id.etdate).getText().toString());

            SLU.setServiceUserType(appState.userType);

            String sel = "";
            if (aqUnplSv.id(R.id.spnfacname).getSelectedItem() != null)
                sel = aqUnplSv.id(R.id.spnfacname).getSelectedItem().toString();
            //15Nov2019 - Bindu - set res english
            if(sel.equalsIgnoreCase(getResources().getString(R.string.select)))
                sel = getResources().getString(R.string.strselect);
            else if(sel.equalsIgnoreCase(getResources().getString(R.string.other)))
                sel = getResources().getString(R.string.strother);

            if (sel.trim().length() > 0 && !(sel.equalsIgnoreCase("select") || sel.equalsIgnoreCase("other")))
                SLU.setServiceProvidedFacName(aqUnplSv.id(R.id.spnfacname).getSelectedItem().toString());
            else if (sel.equalsIgnoreCase("other")
                    && aqUnplSv.id(R.id.etfacname).getText().toString().trim().length() <= 0)
                SLU.setServiceProvidedFacName("Other");
            else
                SLU.setServiceProvidedFacName(aqUnplSv.id(R.id.etfacname).getText().toString());

            SLU.setServicePlanned(3);
            SLU.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime()); //20Jul2019 - Bindu


            //            07May2021 Arpitha
            SLU.setServiceDetail1(aqUnplSv.id(R.id.etnooftabgiven).getText().toString());
            SLU.setServiceDetail2(aqUnplSv.id(R.id.etnooftabconsumed).getText().toString());

            setAncTestData(0, 0);//10May2021 Arpitha
           
            
            TransactionManager.callInTransaction(databaseHelper.getConnectionSource(), new Callable<Void>() {
                public Void call() throws Exception {//29Aug2019 Arpitha - transaction manager

                    WomanServiceRepository womanServiceRepository = new WomanServiceRepository(databaseHelper);
                    boolean isServiceAdded = womanServiceRepository
                            .updateUnplannedServices(SLU,databaseHelper, ancTestsListAdd);
                    if (isServiceAdded) {

                        Toast.makeText(UnplannedServicesActivity.this, getResources().getString(R.string.serviceupdatesuccess), Toast.LENGTH_LONG).show();
                        Intent unPservList = new Intent(UnplannedServicesActivity.this, UnplannedServicesListActivity.class);
                        unPservList.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                        unPservList.putExtra("woman", woman); //26Jul2019 - Bindu
                        startActivity(unPservList);
                    } else
                        Toast.makeText(UnplannedServicesActivity.this, getResources().getString(R.string.serviceupadtefailed), Toast.LENGTH_LONG).show();
                    return null;
                }
            });

            //  }
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.m113), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        try {
            switch (item.getItemId()) {
                case android.R.id.home:
                    Intent home = new Intent(UnplannedServicesActivity.this, MainMenuActivity.class);
                    home.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    startActivity(home);
                    break;

                case R.id.wlist:
                    Intent wlist = new Intent(UnplannedServicesActivity.this, RegisteredWomenActionTabs.class);
                    wlist.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    displayAlert(getResources().getString(R.string.exit), wlist);

                    /*startActivity(wlist);*/

                    break;

                case R.id.logout:
                    Intent logout = new Intent(UnplannedServicesActivity.this, LoginActivity.class);
            /*startActivity(logout);
            CommonClass.updateLogoutTime();*/
                    displayAlert(getResources().getString(R.string.m111), logout);
                    break;

                case R.id.home:
                    Intent intent = new Intent(UnplannedServicesActivity.this, MainMenuActivity.class);
                    intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    displayAlert(getResources().getString(R.string.exit), intent);
                    /*startActivity(intent);*/ //03oct2019-Bindu - set alert to exit
                    break;
                //30Sep2019 - Bindu
                case R.id.about: {
                    Intent goToScreen = new Intent(UnplannedServicesActivity.this, AboutActivity.class);
                    goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    displayAlert(getResources().getString(R.string.exit), goToScreen);
                    return true;
                }
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        return super.onOptionsItemSelected(item);
    }

    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        return b;
    }

    //03Oct2019 - Bindu - add confirmation
    /**
     * Save Confirmation Alert
     *
     * @param goToScreen
     * @param spanText2
     */
    private void displayAlert(final String spanText2, final Intent goToScreen) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set dialog message
        alertDialogBuilder.setMessage(spanText2).setCancelable(false).setNegativeButton(
                getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (goToScreen != null) {

                            startActivity(goToScreen);
                            if (spanText2.contains("logout")) {
                                try {
                                    LoginRepository loginRepo = new LoginRepository(databaseHelper);
                                    loginRepo.updateLogoutTime(appState.loginAuditId, appState.sessionUserId);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        } else
                            dialog.cancel();
                    }
                }).setPositiveButton(getResources().getString(R.string.m122),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    /** Initializes the Action Bar items */
    @Override
    public boolean onCreateOptionsMenu ( Menu menu){
        try {
            if (woman.getRegADDate() != null && woman.getRegADDate().length() > 0) {
                getSupportActionBar().setTitle((woman.getRegWomanName() + "\t"));
            } else {
                int noOfWeeks = DateTimeUtil.getNumberOfWeeksFromLMP(woman.getRegLMP());
                getSupportActionBar().setTitle((woman.getRegWomanName() + " (" + noOfWeeks + " "
                        + getResources().getString(R.string.weeks) + ")"));
            }
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer1); //27Sep2019 - Bindu - set drawer and enable true home btn
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);

            aqUnplSv.id(R.id.tvWomanName).text(woman.getRegWomanName());
            aqUnplSv.id(R.id.tvInfo1).text(getResources().getString(R.string.tvlmp) + " - " + woman.getRegLMP()); // Bindu - set LMP label
            aqUnplSv.id(R.id.ivWomanImg).gone();

            getMenuInflater().inflate(R.menu.servicesmenu, menu);

            menu.findItem(R.id.logout).setTitle((getResources().getString(R.string.action_logout)));
        } catch (Exception e) {

        }
        return super.onCreateOptionsMenu(menu);
    }

    boolean ValidateServiceFields( ) throws Exception {
        if (aqUnplSv.id(R.id.spnservname).getSelectedItemPosition() == 0) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.selservicetoprovide), Toast.LENGTH_LONG).show();
            return false;
        }
        else if (aqUnplSv.id(R.id.etdate).getText().toString().trim().length() <= 0) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.enterservicecompdate), Toast.LENGTH_LONG).show();
            return false;
        }
        else if (aqUnplSv.id(R.id.spnfactype).getSelectedItemPosition() == 0) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.selserviceprovfactype), Toast.LENGTH_LONG).show();
            return false;
        }
        //        15May2021 Arpitha
        else if( woman.getRegPregnantorMother() == 1 &&
                aqUnplSv.id(R.id.spnservname).getSelectedItemPosition() == 1 &&
                !(aqUnplSv.id(R.id.rdanctestYes1).isChecked() ||
                        aqUnplSv.id(R.id.rdanctestNo1).isChecked()))
        {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.enterbp), Toast.LENGTH_LONG).show();

            return false;
        }
        //        11May2021 Arpitha
        else  if( woman.getRegPregnantorMother() == 1 && aqUnplSv.id(R.id.spnservname).getSelectedItemPosition() == 1
                &&  aqUnplSv.id(R.id.rdanctestYes1).isChecked() &&
                (aqUnplSv.id(R.id.etbpsystolic)
                        .getText().toString().trim().length()
                        <= 0 || aqUnplSv.id(R.id.etbpdiastolic).getText().toString()
                        .trim().length() <= 0))
        {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.enterbp), Toast.LENGTH_LONG).show();

            aqUnplSv.id(R.id.etbpsystolic).getEditText().requestFocus();//22Aug2019 Arpitha
            return false;
        }
        //        11May2021 Arpitha
        else if( woman.getRegPregnantorMother() == 1 && (aqUnplSv.id(R.id.spnservname).getSelectedItemPosition() == 1 ) && aqUnplSv.id(R.id.etanctestval15).getText().toString().trim().length()<=0)
        {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.enterweightvalue), Toast.LENGTH_LONG).show();

            aqUnplSv.id(R.id.etanctestval15).getEditText().requestFocus();//22Aug2019 Arpitha
            return false;
        }
        return true;
    }

    static void updateProvidedDate(String date) {

    }


    @SuppressLint("SimpleDateFormat")
    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        private AQuery aQuery;

        static DatePickerFragment getInstance() {
            return new DatePickerFragment();
        }

        public void setAQuery(AQuery aQuery) {
            this.aQuery = aQuery;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            int year = 0, month = 0, day = 0;
            try {
                Calendar c = Calendar.getInstance();
                String defaultDate = aQuery.id(R.id.etdate).getText().toString(); //09Aug2019 - Cal set date of Provided date
                if (defaultDate.length() > 0) {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    Date sendDate = sdf.parse(defaultDate);
                    c.setTime(sendDate);
                    year = c.get(Calendar.YEAR);
                    month = c.get(Calendar.MONTH);
                    day = c.get(Calendar.DAY_OF_MONTH);
                } else {
                    c.setTime(new Date());
                    year = c.get(Calendar.YEAR);
                    month = c.get(Calendar.MONTH);
                    day = c.get(Calendar.DAY_OF_MONTH);
                }
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            }

            // Create a new instance of DatePickerDialog and return it
            DatePickerDialog dpDialog;
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                // Do something for lollipop and above versions
                dpDialog = new DatePickerDialog(getActivity(), R.style.DialogTheme, this, year, month, day);
            } else {
                // do something for phones running an SDK before lollipop
                dpDialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, this, year, month, day);
                dpDialog.setTitle("Set a Date");
                dpDialog.getDatePicker().setCalendarViewShown(false);
            }
            return dpDialog;
        }

        @Override
        public void onDateSet(DatePicker view, int year, int month, int day) {
            if (view.isShown()) {
                try {
                    String SelectedDate = String.format("%02d", day) + "-" + String.format("%02d", month + 1) + "-" + year;
                    String registeredLMP = getArguments().getString("registeredLMP");
                    Date lmpDate = null;
                    SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                    Date seldate = format.parse(SelectedDate);

//                    28Apr2021 Arpitha
                    Date delDate = null;
                    if (woman.getRegADDate() != null && woman.getRegADDate().trim().length() > 0)
                        delDate = format.parse(woman.getRegADDate());//28Apr2021 Arpitha

                    if (registeredLMP != null && registeredLMP.trim().length() > 0)
                        lmpDate = format.parse(registeredLMP);
                    if (seldate.after(new Date())) {
                        AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.date_cannot_af_currentdate), getActivity());
                        aQuery.id(R.id.etdate).text(DateTimeUtil.getTodaysDate());
                    } else if (lmpDate != null && seldate.before(lmpDate)) {
                        AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.date_cannot_bf_lmp), getActivity());
                        aQuery.id(R.id.etdate).text(DateTimeUtil.getTodaysDate());
                    }
                    //28Apr2021 Arpitha
                    else if (delDate != null && (seldate.before(delDate) )) {
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.date_cannot_bf_deldate)
                                , getActivity());
                        aQuery.id(R.id.etdate).text(DateTimeUtil.getTodaysDate());
                    }
                    else {
                        aQuery.id(R.id.etdate).text(SelectedDate);
                        aQuery.id(R.id.imgcleardate).visible();//20Aug2019 Arpitha
                    }
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }
            }
        }
    }

    /**
     * Initiates the Navigation drawer
     */
    private void initiateDrawer() {
        try {
            mDrawerLayout = findViewById(R.id.ett_drawer_layout);
            mDrawerList = findViewById(R.id.ett_left_drawer);

            navDrawerItems = new ArrayList<>();

            // adding nav drawer items to array
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.sp_WRegEdit), R.drawable.registration));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.services), R.drawable.anm_pending_activities));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.unplannedserviceslist), R.drawable.anm_pending_activities));

            //29Aug2019 - Bindu - Add Homevisit
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.homevisitadd),
                    R.drawable.prenatalhomevisit));

            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.womanhomevisithistory),
                    R.drawable.prenatalhomevisit));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deliveryHeading), R.drawable.delivery_info));//01Oct2019 Arpitha

            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deactivate),
                    R.drawable.deactivate));//28Nov2019 Arpitha
            // set a custom shadow that overlays the main content when the drawer
            // opens
            mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
            mDrawerList.setAdapter(new NavDrawerListAdapter(getApplicationContext(), navDrawerItems));
            mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

            // ActionBarDrawerToggle ties together the the proper interactions
            // between the sliding drawer and the action bar app icon
            mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer, R.string.drawer_open, R.string.drawer_close) {
                public void onDrawerClosed(View view) {
                    invalidateOptionsMenu(); // creates call to
                    getSupportActionBar().setDisplayShowHomeEnabled(false);
                    getSupportActionBar().setDisplayShowTitleEnabled(false);
                    getSupportActionBar().setDisplayShowCustomEnabled(false);
                    int noOfWeeks;
                    try {
                        if (woman.getRegADDate() != null && woman.getRegADDate().length() > 0) {
                            getSupportActionBar().setTitle((woman.getRegWomanName() + "\t"));
                        } else {
                            noOfWeeks = DateTimeUtil.getNumberOfWeeksFromLMP(woman.getRegLMP());
                            getSupportActionBar().setTitle((woman.getRegWomanName() + " (" + noOfWeeks + " "
                                    + getResources().getString(R.string.weeks) + ")"));
                        }
                    } catch (ParseException e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                    }
                }

                public void onDrawerOpened(View drawerView) {
                    try {
                        invalidateOptionsMenu(); // creates call to
                        getSupportActionBar().setDisplayShowHomeEnabled(false);
                        getSupportActionBar().setDisplayShowTitleEnabled(false);
                        getSupportActionBar().setDisplayShowCustomEnabled(true);
                        getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);


                        aqUnplSv.id(R.id.ivWomanImg).getImageView().setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View arg0) {
                                mDrawerLayout.closeDrawer(mDrawerList);
                            }
                        });

                        aqUnplSv.id(R.id.tvWomanName).text((woman.getRegWomanName() + "\t"));
                        if (woman.getRegADDate().length() > 0) {
                            aqUnplSv.id(R.id.tvInfo1).text((getResources().getString(R.string.tvadd) + " " + woman.getRegADDate()));
                        } else {
                            int noOfWeeks = DateTimeUtil.getNumberOfWeeksFromLMP(woman.getRegLMP());
                            aqUnplSv.id(R.id.tvInfo1).text((noOfWeeks + getResources().getString(R.string.weeks)));
                        }
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                    }
                }
            };
            mDrawerLayout.setDrawerListener(mDrawerToggle);
        } catch (Exception e) {


            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    /**
     * This method is to sync drawertoggle and to replace up caret icon to menu
     * icon in Action Bar
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);//}catch (Exception e)

    }

    /**
     * The click listner for ListView in the navigation drawer
     */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            mDrawerLayout.closeDrawer(mDrawerList);
            try {
                if (position == 0) {
                    Intent viewprof = new Intent(UnplannedServicesActivity.this, ViewProfileActivity.class);
                    viewprof.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    startActivity(viewprof);
                } else if (position == 1) {
                    Intent unplanned = new Intent(UnplannedServicesActivity.this, ServicesSummaryActivity.class);
                    unplanned.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    startActivity(unplanned);
                } else if (position == 2) {
                    Intent unplanned = new Intent(UnplannedServicesActivity.this, UnplannedServicesListActivity.class);
                    unplanned.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    startActivity(unplanned);
                }
                //14Sep2019 - Bindu - Add Home visit
                else if (position == 3) {
                    if(woman.getRegPregnantorMother() == 2 ){ //02Dec2019 - Bindu - add condition - ANC/PNC Home visit
                        Intent nextScreen = new Intent(UnplannedServicesActivity.this, PncHomeVisit.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        startActivity(nextScreen);
                    } else {
                        int daysDiff = DateTimeUtil.getDaysBetweenDates(DateTimeUtil.getTodaysDate(), woman.getRegLMP());
                        if (daysDiff > 210) {
                            displayNearingDelAlertMessage(woman);
                        } else {
                            Intent homevisit = new Intent(UnplannedServicesActivity.this, HomeVisit.class);
                            homevisit.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(homevisit);
                        }
                    }
                }else if (position == 4) {
                    //      if(woman.getLastAncVisitDate().length() > 0 || woman.getLastPncVisitDate().length() > 0) {
                   // if(wVisitDetails != null && wVisitDetails.get(0).getLastAncVisitDate().length() > 0 || wVisitDetails.get(0).getLastPncVisitDate().length() > 0 )  {
                        Intent homevisit = new Intent(UnplannedServicesActivity.this, HomeVisitListActivity.class);
                        homevisit.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        startActivity(homevisit);
                   /* }else {
                        Toast.makeText(UnplannedServicesActivity.this, getResources().getString(R.string.norecords), Toast.LENGTH_SHORT).show();
                    }*/
                }
                //01Oct2019 Arpitha
                else if(position == 5) {
                    if(woman.getRegPregnantorMother()==2 || DateTimeUtil.getNumberOfDaysFromLMP(woman.getRegLMP())>210) {
                        Intent del = new Intent(UnplannedServicesActivity.this, DeliveryInfoActivity.class);
                        del.putExtra("woman", woman);
                        del.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        startActivity(del);
                    }else
                        Toast.makeText(getApplicationContext(),getResources().getString(R.string.m132),Toast.LENGTH_LONG).show();

                }   //                  28Nov2019 Arpitha
                else if(position == 6) {
                    Intent deact = new Intent(UnplannedServicesActivity.this, WomanDeactivateActivity.class);
                    deact.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    deact.putExtra("woman", woman);
                    startActivity(deact);
                }
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            }
        }
    }

    /**
     * Confirm Alert on Click save
     */
    private void confirmAlertExit() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set dialog message
        alertDialogBuilder.setMessage((getResources().getString(R.string.areyousuretoexit))).setCancelable(false)
                .setNegativeButton((getResources().getString(R.string.m121)),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                try {
                                    Intent exit = new Intent(UnplannedServicesActivity.this, MainMenuActivity.class);
                                    exit.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                    startActivity(exit);
                                } catch (Exception e) {
                                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                                }
                            }
                        })
                .setPositiveButton((getResources().getString(R.string.m122)),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    /**
     * This method Calls the idle timeout
     */ //09Aug2019 - Bindu
    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if(appState!=null)
        new ActivitiesStack(this).delayedIdle(appState.idleTimeOut);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        /*
         * You'll need this in your class to release the helper when done.
         */
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

    //18Oct2019 - Bindu - Alert message nearing del
    public  void displayNearingDelAlertMessage(tblregisteredwomen regwoman) throws Exception{
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        String alertmsg =  regwoman.getRegWomanName() + ", " + " EDD : " + regwoman.getRegEDD() ;
        //alertDialogBuilder.setMessage(message).setCancelable(false).setTitle(Html.fromHtml("<font color='#FF7F27'>Alert -- </font><br><br><p>"+alertmsg + "</p>")).setNegativeButton(
        String msg = "<font color='#CC3333'> Nearing Delivery - </font>"+alertmsg;
        alertDialogBuilder.setMessage(getResources().getText(R.string.alerthomevisitnearingdelmsg)).setCancelable(false).setTitle(Html.fromHtml(msg)).setNegativeButton(
                getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent nextScreen = new Intent(UnplannedServicesActivity.this, HomeVisit.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        startActivity(nextScreen);
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    //    Arpitha 26Nov2019
    void disableScreen()
    {
        TableLayout tbladdunplannedservice  = findViewById(R.id.tbladdunplannedservice);
            aqUnplSv.id(R.id.btnsave).enabled(false);
        disableEnableControls(false,tbladdunplannedservice);

    }

    //    Arpitha 26Nov2019
    private void disableEnableControls(boolean enable, ViewGroup vg) {
        for (int i = 0; i < vg.getChildCount(); i++) {
            View child = vg.getChildAt(i);
            child.setEnabled(enable);
            if (child instanceof ViewGroup) {
                disableEnableControls(enable, (ViewGroup) child);
            }
        }
    }


    //    10May2021 Arpitha
    private void setAncTestData(int transId, int servNo) throws Exception {

        ancTestsListAdd = new ArrayList<>();
        List<TblAncTestsMaster> ancTestsMasterList = new ArrayList<>();

        WomanServiceRepository womanServiceRepository = new WomanServiceRepository(databaseHelper);
        ancTestsMasterList = womanServiceRepository.getAncTests();

        for (int i = 1; i <= ancTestsMasterList.size(); i++) {
            TblAncTests tblAncTests = new TblAncTests();
            tblAncTests.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
            tblAncTests.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());//21May2021 Arpitha


            tblAncTests.setServiceNumber(servNo);
            /*if (i <=13 || i == 16) {
                if (aqUnplSv.id(rdtestYes + i).isChecked()) {
                    tblAncTests.setTestDone("Yes");

                } else if (aqUnplSv.id(rdtestNo + i).isChecked())
                    tblAncTests.setTestDone("No");
            }*/


            if (i == 1) {
               // if(aqUnplSv.id(R.id.etbpsystolic).getText().toString().trim().length()>0 || aqUnplSv.id(R.id.etbpdiastolic).getText().toString().trim().length()>0)

                if (aqUnplSv.id(R.id.rdanctestYes1).isChecked()) {
                    tblAncTests.setTestDone("Yes");
                    tblAncTests.setTestValue(aqUnplSv.id(R.id.etbpsystolic).getText().toString()
                            +"/"+aqUnplSv.id(R.id.etbpdiastolic).getText().toString());
                }
                else if (aqUnplSv.id(R.id.rdanctestNo1).isChecked())
                    tblAncTests.setTestDone("No");
            } else if (i == 2) {
                tblAncTests.setTestValue(aqUnplSv.id(R.id.etanctestval2).getText().toString());
                if (aqUnplSv.id(R.id.rdanctestYes2).isChecked())
                    tblAncTests.setTestDone("Yes");
                else if (aqUnplSv.id(R.id.rdanctestNo2).isChecked())
                    tblAncTests.setTestDone("No");
            } else if (i == 3) {
                tblAncTests.setTestValue(aqUnplSv.id(R.id.etanctestval3).getText().toString());
                if (aqUnplSv.id(R.id.rdanctestYes3).isChecked())
                    tblAncTests.setTestDone("Yes");
                else if (aqUnplSv.id(R.id.rdanctestNo3).isChecked())
                    tblAncTests.setTestDone("No");
            } else if (i == 4) {
                tblAncTests.setTestValue(aqUnplSv.id(R.id.etanctestval4).getText().toString());
                if (aqUnplSv.id(R.id.rdanctestYes4).isChecked())
                    tblAncTests.setTestDone("Yes");
                else if (aqUnplSv.id(R.id.rdanctestNo4).isChecked())
                    tblAncTests.setTestDone("No");
            } else if (i == 5) {
                tblAncTests.setTestValue(aqUnplSv.id(R.id.etanctestval5).getText().toString());
                if (aqUnplSv.id(R.id.rdanctestYes5).isChecked())
                    tblAncTests.setTestDone("Yes");
                else if (aqUnplSv.id(R.id.rdanctestNo5).isChecked())
                    tblAncTests.setTestDone("No");

            } else if (i == 6) {
                tblAncTests.setTestValue(aqUnplSv.id(R.id.etanctestval6).getText().toString());
                if (aqUnplSv.id(R.id.rdanctestYes6).isChecked())
                    tblAncTests.setTestDone("Yes");
                else if (aqUnplSv.id(R.id.rdanctestNo6).isChecked())
                    tblAncTests.setTestDone("No");
            } else if (i == 7) {
                tblAncTests.setTestValue(aqUnplSv.id(R.id.etanctestval7).getText().toString());
                if (aqUnplSv.id(R.id.rdanctestYes7).isChecked())
                    tblAncTests.setTestDone("Yes");
                else if (aqUnplSv.id(R.id.rdanctestNo7).isChecked())
                    tblAncTests.setTestDone("No");

            } else if (i == 8) {
                tblAncTests.setTestValue(aqUnplSv.id(R.id.etanctestval8).getText().toString());
                if (aqUnplSv.id(R.id.rdanctestYes8).isChecked())
                    tblAncTests.setTestDone("Yes");
                else if (aqUnplSv.id(R.id.rdanctestNo8).isChecked())
                    tblAncTests.setTestDone("No");
            } else if (i == 9) {

                tblAncTests.setTestValue(aqUnplSv.id(R.id.etanctestval9).getText().toString());
                if (aqUnplSv.id(R.id.rdanctestYes9).isChecked())
                    tblAncTests.setTestDone("Yes");
                else if (aqUnplSv.id(R.id.rdanctestNo9).isChecked())
                    tblAncTests.setTestDone("No");
            } else if (i == 10) {
                tblAncTests.setTestValue(aqUnplSv.id(R.id.etanctestval10).getText().toString());
                if (aqUnplSv.id(R.id.rdanctestYes10).isChecked())
                    tblAncTests.setTestDone("Yes");
                else if (aqUnplSv.id(R.id.rdanctestNo10).isChecked())
                    tblAncTests.setTestDone("No");
            } else if (i == 11) {
                tblAncTests.setTestValue(aqUnplSv.id(R.id.etanctestval11).getText().toString());
                if (aqUnplSv.id(R.id.rdanctestYes11).isChecked())
                    tblAncTests.setTestDone("Yes");
                else if (aqUnplSv.id(R.id.rdanctestNo11).isChecked())
                    tblAncTests.setTestDone("No");
            } else if (i == 12) {
                tblAncTests.setTestValue(aqUnplSv.id(R.id.etanctestval12).getText().toString());
                if (aqUnplSv.id(R.id.rdanctestYes12).isChecked())
                    tblAncTests.setTestDone("Yes");
                else if (aqUnplSv.id(R.id.rdanctestNo12).isChecked())
                    tblAncTests.setTestDone("No");
            } else if (i == 13) {
                tblAncTests.setTestValue(aqUnplSv.id(R.id.etanctestval13).getText().toString());
                if (aqUnplSv.id(R.id.rdanctestYes13).isChecked())
                    tblAncTests.setTestDone("Yes");
                else if (aqUnplSv.id(R.id.rdanctestNo13).isChecked())
                    tblAncTests.setTestDone("No");
            } else if (i == 14) {
                tblAncTests.setTestValue(aqUnplSv.id(R.id.etanctestval14).getText().toString());
            } else if (i == 15) {
                tblAncTests.setTestValue(aqUnplSv.id(R.id.etanctestval15).getText().toString());
            } else if (i == 16) {
                tblAncTests.setTestValue(aqUnplSv.id(R.id.etanctestval16).getText().toString());
                if (aqUnplSv.id(R.id.rdanctestYes16).isChecked())
                    tblAncTests.setTestDone("Yes");
                else if (aqUnplSv.id(R.id.rdanctestNo16).isChecked())
                    tblAncTests.setTestDone("No");
            }

            tblAncTests.setTestId(ancTestsMasterList.get(i - 1).getTestId());
            tblAncTests.setUserId(woman.getUserId());
            tblAncTests.setWomenId(woman.getWomanId());
            tblAncTests.setTransId(transId);
            if (tblAncTests.getTestDone() != null ||
                    (tblAncTests.getTestValue() != null && tblAncTests.getTestValue().trim().length() > 0))
                ancTestsListAdd.add(tblAncTests);
        }
    }

    //    11May2021 Arpitha
    private void setOnClickListner() {

        //        10May2021 Arpitha
        aqUnplSv.id(R.id.rdanctestNo1).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqUnplSv.id(R.id.etbpdiastolic).gone();
                aqUnplSv.id(R.id.etbpsystolic).gone();
                aqUnplSv.id(R.id.etbpdiastolic).text("");
                aqUnplSv.id(R.id.etbpsystolic).text("");
                aqUnplSv.id(R.id.trancbptest).gone();

            }
        });
        //        10May2021 Arpitha
        aqUnplSv.id(R.id.rdanctestYes1).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqUnplSv.id(R.id.etbpdiastolic).visible();
                aqUnplSv.id(R.id.etbpsystolic).visible();
                aqUnplSv.id(R.id.trancbptest).visible();

            }
        });

        //        10May2021 Arpitha
        aqUnplSv.id(R.id.rdanctestNo2).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqUnplSv.id(R.id.etanctestval2).gone();
                aqUnplSv.id(R.id.etanctestval2).text("");

            }
        });
        //        10May2021 Arpitha
        aqUnplSv.id(R.id.rdanctestYes2).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqUnplSv.id(R.id.etanctestval2).visible();

            }
        });
        //        10May2021 Arpitha
        aqUnplSv.id(R.id.rdanctestNo3).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqUnplSv.id(R.id.etanctestval3).gone();
                aqUnplSv.id(R.id.etanctestval3).text("");

            }
        });
        //        10May2021 Arpitha
        aqUnplSv.id(R.id.rdanctestYes3).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqUnplSv.id(R.id.etanctestval3).visible();

            }
        });//        10May2021 Arpitha
        aqUnplSv.id(R.id.rdanctestNo4).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqUnplSv.id(R.id.etanctestval4).gone();
                aqUnplSv.id(R.id.etanctestval4).text("");

            }
        });
        //        10May2021 Arpitha
        aqUnplSv.id(R.id.rdanctestYes4).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqUnplSv.id(R.id.etanctestval4).visible();

            }
        });//        10May2021 Arpitha
        aqUnplSv.id(R.id.rdanctestNo5).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqUnplSv.id(R.id.etanctestval5).gone();
                aqUnplSv.id(R.id.etanctestval5).text("");

            }
        });
        //        10May2021 Arpitha
        aqUnplSv.id(R.id.rdanctestYes5).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqUnplSv.id(R.id.etanctestval5).visible();

            }
        });//        10May2021 Arpitha
        aqUnplSv.id(R.id.rdanctestNo6).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqUnplSv.id(R.id.etanctestval6).gone();
                aqUnplSv.id(R.id.etanctestval6).text("");

            }
        });
        //        10May2021 Arpitha
        aqUnplSv.id(R.id.rdanctestYes6).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqUnplSv.id(R.id.etanctestval6).visible();

            }
        });//        10May2021 Arpitha
        aqUnplSv.id(R.id.rdanctestNo7).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqUnplSv.id(R.id.etanctestval7).gone();
                aqUnplSv.id(R.id.etanctestval7).text("");

            }
        });
        //        10May2021 Arpitha
        aqUnplSv.id(R.id.rdanctestYes7).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqUnplSv.id(R.id.etanctestval7).visible();

            }
        });//        10May2021 Arpitha
        aqUnplSv.id(R.id.rdanctestNo8).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqUnplSv.id(R.id.etanctestval8).gone();
                aqUnplSv.id(R.id.etanctestval8).text("");

            }
        });
        //        10May2021 Arpitha
        aqUnplSv.id(R.id.rdanctestYes8).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqUnplSv.id(R.id.etanctestval8).visible();

            }
        });//        10May2021 Arpitha
        aqUnplSv.id(R.id.rdanctestNo9).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqUnplSv.id(R.id.etanctestval9).gone();
                aqUnplSv.id(R.id.etanctestval9).text("");

            }
        });
        //        10May2021 Arpitha
        aqUnplSv.id(R.id.rdanctestYes9).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqUnplSv.id(R.id.etanctestval9).visible();

            }
        });//        10May2021 Arpitha
        aqUnplSv.id(R.id.rdanctestNo10).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqUnplSv.id(R.id.etanctestval10).gone();
                aqUnplSv.id(R.id.etanctestval10).text("");

            }
        });
        //        10May2021 Arpitha
        aqUnplSv.id(R.id.rdanctestYes10).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqUnplSv.id(R.id.etanctestval10).visible();

            }
        });//        10May2021 Arpitha
        aqUnplSv.id(R.id.rdanctestNo11).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqUnplSv.id(R.id.etanctestval11).gone();
                aqUnplSv.id(R.id.etanctestval11).text("");

            }
        });
        //        10May2021 Arpitha
        aqUnplSv.id(R.id.rdanctestYes11).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqUnplSv.id(R.id.etanctestval11).visible();

            }
        });//        10May2021 Arpitha
        aqUnplSv.id(R.id.rdanctestNo12).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqUnplSv.id(R.id.etanctestval12).gone();
                aqUnplSv.id(R.id.etanctestval12).text("");

            }
        });
        //        10May2021 Arpitha
        aqUnplSv.id(R.id.rdanctestYes12).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqUnplSv.id(R.id.etanctestval12).visible();

            }
        });//        10May2021 Arpitha
        aqUnplSv.id(R.id.rdanctestNo13).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqUnplSv.id(R.id.etanctestval13).gone();
                aqUnplSv.id(R.id.etanctestval13).text("");

            }
        });
        //        10May2021 Arpitha
        aqUnplSv.id(R.id.rdanctestYes13).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqUnplSv.id(R.id.etanctestval13).visible();

            }
        });
        //        10May2021 Arpitha
        aqUnplSv.id(R.id.rdanctestNo16).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqUnplSv.id(R.id.etanctestval16).gone();
                aqUnplSv.id(R.id.etanctestval16).text("");

            }
        });
        //        10May2021 Arpitha
        aqUnplSv.id(R.id.rdanctestYes16).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqUnplSv.id(R.id.etanctestval16).visible();

            }
        });

        aqUnplSv.id(R.id.trtests).getView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (aqUnplSv.id(R.id.lltest).getView().getVisibility() == View.VISIBLE) {
                    aqUnplSv.id(R.id.lltest).gone();
                    aqUnplSv.id(R.id.ivtests).background(R.drawable.ic_add_box);

                } else {
                    aqUnplSv.id(R.id.lltest).visible();
                    aqUnplSv.id(R.id.ivtests).background(R.drawable.ic_indeterminate_check_box);

                }
            }
        });

        aqUnplSv.id(R.id.trlabtests).getView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (aqUnplSv.id(R.id.lllabtest).getView().getVisibility() == View.VISIBLE) {
                    aqUnplSv.id(R.id.lllabtest).gone();
                    aqUnplSv.id(R.id.ivlabtest).background(R.drawable.ic_add_box);

                } else {
                    aqUnplSv.id(R.id.lllabtest).visible();
                    aqUnplSv.id(R.id.ivlabtest).background(R.drawable.ic_indeterminate_check_box);

                }
            }
        });
    }
    //    21May2021 Arpitha
    @Override
    public void onBackPressed() {

    }
}
