package com.sc.stmansi.datasource;

import android.content.Context;
import android.content.res.AssetManager;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.tables.AuditPojo;
import com.sc.stmansi.tables.MessageLogPojo;
import com.sc.stmansi.tables.TblAncTests;
import com.sc.stmansi.tables.TblAncTestsMaster;
import com.sc.stmansi.tables.TblBadgeCount;
import com.sc.stmansi.tables.TblChildImmunization;
import com.sc.stmansi.tables.TblChildInfo;
import com.sc.stmansi.tables.TblChlParentDetails;
import com.sc.stmansi.tables.TblContactDetails;
import com.sc.stmansi.tables.TblDeactivate;
import com.sc.stmansi.tables.TblDeliveryInfo;
import com.sc.stmansi.tables.TblDiagnosisMaster;
import com.sc.stmansi.tables.TblFacilityDetails;
import com.sc.stmansi.tables.TblInstitutionDetails;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.TblLoginAudit;
import com.sc.stmansi.tables.TblRegComplMgmt;
import com.sc.stmansi.tables.TblSignsMaster;
import com.sc.stmansi.tables.TblSymptomMaster;
import com.sc.stmansi.tables.TblSyncMaster;
import com.sc.stmansi.tables.TblTransHeaderDS;
import com.sc.stmansi.tables.TblVisitChildHeader;
import com.sc.stmansi.tables.TblVisitDetails;
import com.sc.stmansi.tables.TblVisitDetailsPojo;
import com.sc.stmansi.tables.TblVisitHeader;
import com.sc.stmansi.tables.Tblinterpretationmaster;
import com.sc.stmansi.tables.tblAdolAttendanceSavings;
import com.sc.stmansi.tables.tblAdolFollowUpMaster;
import com.sc.stmansi.tables.tblAdolPregnant;
import com.sc.stmansi.tables.tblAdolReg;
import com.sc.stmansi.tables.tblAdolVisitDetails;
import com.sc.stmansi.tables.tblAdolVisitHeader;
import com.sc.stmansi.tables.tblCampDetails;
import com.sc.stmansi.tables.tblCaseMgmt;
import com.sc.stmansi.tables.tblChildVisitDetails;
import com.sc.stmansi.tables.tblCovidTestDetails;
import com.sc.stmansi.tables.tblCovidVaccineDetails;
import com.sc.stmansi.tables.tblHRPComplicationMgmt;
import com.sc.stmansi.tables.tblImageStore;
import com.sc.stmansi.tables.tblSanNap;
import com.sc.stmansi.tables.tblSanNapHeader;
import com.sc.stmansi.tables.tblSanNapMaster;
import com.sc.stmansi.tables.tblSyncDetails;
import com.sc.stmansi.tables.tblVersionDetails;
import com.sc.stmansi.tables.tblbankdetails;
import com.sc.stmansi.tables.tblbmiforage;
import com.sc.stmansi.tables.tblchildgrowth;
import com.sc.stmansi.tables.tblheightforage;
import com.sc.stmansi.tables.tblmuacforage;
import com.sc.stmansi.tables.tblreferralFacility;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.tables.tblserviceslist;
import com.sc.stmansi.tables.tblservicestypemaster;
import com.sc.stmansi.tables.tbltransHeader;
import com.sc.stmansi.tables.tbltransdetail;
import com.sc.stmansi.tables.tblweightforage;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

	private static final String DATABASE_NAME = AppState.dbDirRef + "/stenddb.db";
	private static final int DATABASE_VERSION = 7; //7-Sep-2021 Ramesh change 6 to 7
	private AssetManager assetManager;

	private RuntimeExceptionDao<tblregisteredwomen, Integer> tblregRuntimeDAO;
	private RuntimeExceptionDao<tbltransHeader, Integer> transheaderDAO;
	private RuntimeExceptionDao<tbltransdetail, Integer> transdetailDAO;
	private RuntimeExceptionDao<TblInstusers, Integer> usersDAO;
	private RuntimeExceptionDao<tblserviceslist, Integer> servicesDAO;
	private RuntimeExceptionDao<tblservicestypemaster, Integer> servicestypeDAO;
	private RuntimeExceptionDao<TblFacilityDetails, Integer> villagesDAO;
	private RuntimeExceptionDao<tblbankdetails, Integer> bankdetailsDAO;
	private RuntimeExceptionDao<AuditPojo, Integer> auditsDAO;
	private RuntimeExceptionDao<TblBadgeCount, Integer> badgeDAO;
	private RuntimeExceptionDao<TblSyncMaster, Integer> syncMasterDAO;
	private RuntimeExceptionDao<TblTransHeaderDS, Integer> transheaderDSDAO;
	private RuntimeExceptionDao<TblInstitutionDetails, Integer> instDeatilsSDAO;
	private RuntimeExceptionDao<TblLoginAudit, Integer> loginAuditDAO;
    private RuntimeExceptionDao<TblDeliveryInfo, Integer> delInfoDAO;
    private RuntimeExceptionDao<TblChildInfo, Integer> chlidInfoDAO;
    private Dao<TblVisitHeader, Integer> tblVisitHeaderDao;
    private Dao<TblVisitDetails, Integer> tblVisitDetailDao;
	private Dao<TblChildImmunization, Integer> tblImmunizationDao;
	private Dao<TblChlParentDetails, Integer> tblChlParentDetailsDao;
	private Dao<TblDeactivate, Integer> tblDeactivatesDao;//20Nov2019 Arpitha

	//20Nov2019 - Bindu - Child details
	private Dao<tblChildVisitDetails, Integer> tblChildVisitDetailDao;
	//24Nov2019 - Bindu - Child details
	private Dao<MessageLogPojo, Integer> messageLogDao;//25Nov2019 Arpitha
	private Dao<TblContactDetails, Integer> tblContactDetailsDao;//26Nov2019 Arpitha

    //24Nov2019 - Bindu - Complication details
    private Dao<tblHRPComplicationMgmt, Integer> tblComplicationMgmtDao;
    //25Nov2019 - Bindu - REg and Complication details
    private Dao<TblRegComplMgmt, Integer> tblRegComplMgmtDao;
	//30Nov2019 - Bindu - Child header dao
	private Dao<TblVisitChildHeader, Integer> tblVisitChildHeaderDao;
	private RuntimeExceptionDao<tblSyncDetails, Integer> tblSyncDetailsDAO;
//	08Apr2021 Bindu
private RuntimeExceptionDao<tblreferralFacility, Integer> tblreferralFacilityDao;

	private RuntimeExceptionDao<tblAdolReg, Integer> tblAdolRegDAO;
	private RuntimeExceptionDao<tblCampDetails,Integer> CampaignDAO;
	private RuntimeExceptionDao<tblAdolAttendanceSavings,Integer> AdolAttendanceDAO;//Ramesh 15 april 2021
	private RuntimeExceptionDao<tblAdolVisitHeader,Integer> AdolVisitHeaderDao;
	private RuntimeExceptionDao<tblAdolVisitDetails,Integer> AdolVisitDetailsDao;
	private RuntimeExceptionDao<tblAdolFollowUpMaster,Integer> AdolFollowUpDao;
	private RuntimeExceptionDao<tblAdolPregnant,Integer> AdolPregnantDao;

//10May2021 Arpitha
	private Dao<TblSymptomMaster, Integer> tblSymtomMasterDao;
	private Dao<TblAncTestsMaster, Integer> tblAncTestsMasterDao;
	private Dao<TblAncTests, Integer> tblAncTestsDao;
	private RuntimeExceptionDao<tblchildgrowth, Integer> tblchildgrowthDAO;

	//Mani 17 march 2021
	private RuntimeExceptionDao<tblweightforage, Integer> tblweightforageDAO;
	private RuntimeExceptionDao<tblheightforage, Integer> tblheightforageDAO;
	private RuntimeExceptionDao<tblbmiforage, Integer> tblbmiforageDAO;
	private RuntimeExceptionDao<tblmuacforage, Integer> tblmuacforageDAO;

//	16MAy2021 Bindu
	private Dao<tblCovidTestDetails, Integer> tblCovidTestDao;
	private Dao<tblCovidVaccineDetails, Integer> tblCovidVaccineDao;
	Context context;//17May2021 Arpitha

	//1-6-2021 ramesh
	private Dao<tblImageStore, Integer> tblImageStoreDao;
	//296-2021
	private Dao<tblCaseMgmt, Integer> tblCaseMgmtDao;
	private Dao<tblVersionDetails, Integer> tblversionDetailsDAO;
	private Dao<TblDiagnosisMaster, Integer> tblDiagnosisMasterDao;//23July2021 Arpitha
	private Dao<Tblinterpretationmaster, Integer> tblInterpretationMasterDao;//23July2021 Arpitha
	private Dao<TblSignsMaster, Integer> tblSignsMasterDao;//23July2021 Arpitha

	private Dao<TblVisitDetailsPojo, Integer> tblVisitDetailsPojoDao; //25Jul2021

	private Dao<tblSanNapMaster, Integer> tblSanNapMasterDao; //23 Aug 2021
	private Dao<tblSanNap, Integer> tblSanNapDao; // //23 Aug 2021
	private Dao<tblSanNapHeader, Integer> tblSanNapHeaderDao; // //23 Aug 2021

	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.assetManager = context.getAssets();
        this.context = context;//17May2021 Arpitha
	}

	@Override
	public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {}

	@Override
	public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
		try {
			/*TableUtils.dropTable(connectionSource, tblregisteredwomen.class, true);
            onCreate(sqLiteDatabase,connectionSource);*/

			copyDbToExternalStorage(context, DATABASE_NAME,oldVersion);//17May2021 Arpitha

            SQLiteDatabase db = database;
            int upgradeTo = oldVersion;
            while (upgradeTo < newVersion) {
                String filename = "matrukshadbupdate" + upgradeTo + "to" + (upgradeTo + 1) + ".txt";
                updateDatabase(db, filename);
                upgradeTo++;
            }
		} catch (IOException e) {
			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}
	}

	public RuntimeExceptionDao<tblregisteredwomen, Integer> gettblregRuntimeExceptionDao() {
		try {
			if (tblregRuntimeDAO == null) {
				tblregRuntimeDAO = getRuntimeExceptionDao(tblregisteredwomen.class);
			}
		} catch (Exception e) {
			
			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}
		return tblregRuntimeDAO;

	}

	//    transheader table DAO object creation
	public RuntimeExceptionDao<tbltransHeader, Integer> getTransHeaderRuntimeExceptionDao() {
		try {
			if (transheaderDAO == null) {
				transheaderDAO = getRuntimeExceptionDao(tbltransHeader.class);
			}
		} catch (Exception e) {
			
			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}
		return transheaderDAO;

	}

	//    transdetails table DAO object creation
	public RuntimeExceptionDao<tbltransdetail, Integer> getTransDetailsRuntimeExceptionDao() {
		try {
			if (transdetailDAO == null) {
				transdetailDAO = getRuntimeExceptionDao(tbltransdetail.class);
			}
		} catch (Exception e) {
			

			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}
		return transdetailDAO;

	}

	//    tblusers table DAO object creation
	public RuntimeExceptionDao<TblInstusers, Integer> getUsersRuntimeExceptionDao() {
		try {
			if (usersDAO == null) {
				usersDAO = getRuntimeExceptionDao(TblInstusers.class);
			}
		} catch (Exception e) {
			
			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}
		return usersDAO;

	}

	//    tblserviceslist table DAO object creation
	public RuntimeExceptionDao<tblserviceslist, Integer> getServicesRuntimeExceptionDao() {
		try {
			if (servicesDAO == null) {
				servicesDAO = getRuntimeExceptionDao(tblserviceslist.class);
			}
		} catch (Exception e) {
			

			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}
		return servicesDAO;

	}

	//    Servicetype table DAO object creation
	public RuntimeExceptionDao<tblservicestypemaster, Integer> getServicesTypeRuntimeExceptionDao() {
		try {
			if (servicestypeDAO == null) {
				servicestypeDAO = getRuntimeExceptionDao(tblservicestypemaster.class);
			}
		} catch (Exception e) {
			
			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}
		return servicestypeDAO;

	}

	public RuntimeExceptionDao<TblFacilityDetails, Integer> getVillageRuntimeExceptionDao() {
		try {
			if (villagesDAO == null) {
				villagesDAO = getRuntimeExceptionDao(TblFacilityDetails.class);
			}
		} catch (Exception e) {
			
			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}
		return villagesDAO;

	}


	//    bank details table DAO object creation
	public RuntimeExceptionDao<tblbankdetails, Integer> getBankDetailsRuntimeExceptionDao() {
		try {
			if (bankdetailsDAO == null) {
				bankdetailsDAO = getRuntimeExceptionDao(tblbankdetails.class);
			}
		} catch (Exception e) {
			
			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}
		return bankdetailsDAO;

	}


	//    audit trail table DAO object creation
	public RuntimeExceptionDao<AuditPojo, Integer> getAuditRuntimeExceptionDao() {
		try {
			if (auditsDAO == null) {
				auditsDAO = getRuntimeExceptionDao(AuditPojo.class);
			}
		} catch (Exception e) {
			
			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}
		return auditsDAO;

	}

	// get badge count object
	public RuntimeExceptionDao<TblBadgeCount, Integer> getBadgeRuntimeExceptionDao() {
		try {
			if (badgeDAO == null) {
				badgeDAO = getRuntimeExceptionDao(TblBadgeCount.class);
			}
		} catch (Exception e) {
			
			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}
		return badgeDAO;

	}

	// get sync masterobject
	public RuntimeExceptionDao<TblSyncMaster, Integer> getSyncMasterRuntimeExceptionDao() {
		try {
			if (syncMasterDAO == null) {
				syncMasterDAO = getRuntimeExceptionDao(TblSyncMaster.class);
			}
		} catch (Exception e) {
			
			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}
		return syncMasterDAO;

	}

	// get sync masterobject
	public RuntimeExceptionDao<TblTransHeaderDS, Integer> getTransHeaderDSRuntimeExceptionDao() {
		try {
			if (transheaderDSDAO == null) {
				transheaderDSDAO = getRuntimeExceptionDao(TblTransHeaderDS.class);
			}
		} catch (Exception e) {
			
			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}
		return transheaderDSDAO;
	}


	// get tb inst details object
	public RuntimeExceptionDao<TblInstitutionDetails, Integer> getInstDetailsRuntimeExceptionDao() {
		try {
			if (instDeatilsSDAO == null) {
				instDeatilsSDAO = getRuntimeExceptionDao(TblInstitutionDetails.class);
			}
		} catch (Exception e) {
			
			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}
		return instDeatilsSDAO;

	}

	// login audit object
	public RuntimeExceptionDao<TblLoginAudit, Integer>
	getLoginAuditRuntimeExceptionDao() {
		try {
			if (loginAuditDAO == null) {
				loginAuditDAO = getRuntimeExceptionDao(TblLoginAudit.class);
			}
		} catch (Exception e) {
			
			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}
		return loginAuditDAO;
	}

    // del info object
    public RuntimeExceptionDao<TblDeliveryInfo, Integer>
    getDelInfoRuntimeExceptionDao() {
        try {
            if (delInfoDAO == null) {
                delInfoDAO = getRuntimeExceptionDao(TblDeliveryInfo.class);
            }
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        return delInfoDAO;
    }

    // del info object
    public RuntimeExceptionDao<TblChildInfo, Integer>
    getChildInfoRuntimeExceptionDao() {
        try {
            if (chlidInfoDAO == null) {
                chlidInfoDAO = getRuntimeExceptionDao(TblChildInfo.class);
            }
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        return chlidInfoDAO;
    }


    //  16May2021 Bindu set covid tables
    public Dao<tblCovidTestDetails, Integer> getTblCovidTestDao(){
        try {
            if (tblCovidTestDao == null) {
				tblCovidTestDao = getDao(tblCovidTestDetails.class);
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return tblCovidTestDao;

    }

    public Dao<tblCovidVaccineDetails, Integer> getTblCovidVaccineDao(){
        try {
            if (tblCovidVaccineDao == null) {
				tblCovidVaccineDao = getDao(tblCovidVaccineDetails.class);
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return tblCovidVaccineDao;
    }

    public <D extends Dao<T, ?>, T> D getDao(Class<T> clazz) throws SQLException {
        // special reflection fu is now handled internally by create dao calling the database type
        Dao<T, ?> dao = DaoManager.createDao(getConnectionSource(), clazz);
        @SuppressWarnings("unchecked")
        D castDao = (D) dao;
        return castDao;
    }

    // updating the database with running the update script
    private void updateDatabase(SQLiteDatabase db, String fileName) throws IOException {
        int count = 0;
        InputStream in = assetManager.open(fileName);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte[] buffer = new byte[4096];
        while (true) {
            int read = in.read(buffer);
            if (read == -1) {
                break;
            }
            out.write(buffer, 0, read);
        }

        byte[] bytes = out.toByteArray();
        String sql = new String(bytes, "UTF-8");
        String[] lines = sql.split(";(\\s)*[\n\r]");
        // String[] lines = sql.split(";");
        if (true) {
            db.beginTransaction();

            for (String line : lines) {
                line = line.trim();
                if (line.length() > 0) {
                    db.execSQL(line);
                    count++;
                }
            }
            db.setTransactionSuccessful();
        }
        System.out.println("Executed " + count + " statements from SQL script " + fileName);
        System.out.println("statements excuted are: " + count);
        db.endTransaction();
    }

	// Immunization - 08Nov2019 Arpitha
	public Dao<TblChildImmunization, Integer>
	getImmunizationRuntimeExceptionDao() {
		try {
			if (tblImmunizationDao == null) {
				tblImmunizationDao = getDao(TblChildImmunization.class);
			}
		} catch (Exception e) {

			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}
		return tblImmunizationDao;
	}

	// chlparentdetails - 08Nov2019 Arpitha
	public Dao<TblChlParentDetails, Integer>
	getChlParentRuntimeExceptionDao() {
		try {
			if (tblChlParentDetailsDao == null) {
				tblChlParentDetailsDao = getDao(TblChlParentDetails.class);
			}
		} catch (Exception e) {

			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}
		return tblChlParentDetailsDao;
	}

	//20Nov2019 - Bindu - Child visit details
	public Dao<tblChildVisitDetails, Integer> getTblChildVisitDetailDao() {
		try {
			if (tblChildVisitDetailDao == null) {
				tblChildVisitDetailDao = getDao(tblChildVisitDetails.class);
			}
		} catch (Exception e) {
			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}
		return tblChildVisitDetailDao;
	}

	// tbldeactivate - 20Nov2019 Arpitha
	public Dao<TblDeactivate, Integer> getDeactivateRuntimeExceptionDao() {
		try {
			if (tblDeactivatesDao == null) {
				tblDeactivatesDao = getDao(TblDeactivate.class);
			}
		} catch (Exception e) {

			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}
		return tblDeactivatesDao;
	}

    //24Nov2019 - Bindu - Complication mgmt
    public Dao<tblHRPComplicationMgmt, Integer> gettblComplicationMgmtDao() {
        try {
            if (tblComplicationMgmtDao == null) {
                tblComplicationMgmtDao = getDao(tblHRPComplicationMgmt.class);
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        return tblComplicationMgmtDao;
    }

	//26Nov2019 - Arpitha - messgaelog
	public Dao<MessageLogPojo, Integer> getMessageLogDao() {
		try {
			if (messageLogDao == null) {
				messageLogDao = getDao(MessageLogPojo.class);
			}
		} catch (Exception e) {
			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}
		return messageLogDao;
	}
	//26Nov2019 - Arpitha - messgaelog
	public Dao<TblContactDetails, Integer> getContactDetailsDao() {
		try {
			if (tblContactDetailsDao == null) {
				tblContactDetailsDao = getDao(TblContactDetails.class);
			}
		} catch (Exception e) {
			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}
		return tblContactDetailsDao;
	}


    //25Nov2019 - Bindu - Reg Complication mgmt
    public Dao<TblRegComplMgmt, Integer> gettblRegComplicationMgmtDao() {
        try {
            if (tblRegComplMgmtDao == null) {
                tblRegComplMgmtDao = getDao(TblRegComplMgmt.class);
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        return tblRegComplMgmtDao;
    }

    //30Nov2019 - Bindu - Add Child header dao
	public Dao<TblVisitChildHeader, Integer> getTblVisitChildHeaderDao(){
		try {
			if (tblVisitChildHeaderDao == null) {
				tblVisitChildHeaderDao = getDao(TblVisitChildHeader.class);
			}
		}catch (Exception e)
		{
			e.printStackTrace();
		}
		return tblVisitChildHeaderDao;

	}
	public RuntimeExceptionDao<tblSyncDetails, Integer> getSyncDetailsRuntimeExceptionDao() {
		try {
			if ( tblSyncDetailsDAO== null) {
				tblSyncDetailsDAO = getRuntimeExceptionDao(tblSyncDetails.class);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tblSyncDetailsDAO;
	}

//	08Apr2021 Bindu
	public RuntimeExceptionDao<tblreferralFacility, Integer> gettblreferralFacilityRuntimeExceptionDao() {
		try {
			if ( tblreferralFacilityDao== null) {
				tblreferralFacilityDao = getRuntimeExceptionDao(tblreferralFacility.class);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tblreferralFacilityDao;
	}
	public RuntimeExceptionDao<tblCampDetails, Integer> getCampaignRuntimeExceptionDao() {
		try {
			if (CampaignDAO == null) {
				CampaignDAO = getRuntimeExceptionDao(tblCampDetails.class);
			}
		} catch (Exception e) {
			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
			crashlytics.log(e.getMessage());
		}
		return CampaignDAO;

	}
	public RuntimeExceptionDao<tblAdolAttendanceSavings, Integer> getAttendanceRuntimeExceptionDao() {
		try {
			if (AdolAttendanceDAO == null) {
				AdolAttendanceDAO = getRuntimeExceptionDao(tblAdolAttendanceSavings.class);
			}
		} catch (Exception e) {
			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
			crashlytics.log(e.getMessage());
		}
		return AdolAttendanceDAO;

	}
	public RuntimeExceptionDao<tblAdolReg, Integer> gettblAdolregRuntimeExceptionDao() {
		try {
			if (tblAdolRegDAO == null) {
				tblAdolRegDAO = getRuntimeExceptionDao(tblAdolReg.class);
			}
		} catch (Exception e) {

			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
			crashlytics.log(e.getMessage());
		}
		return tblAdolRegDAO;

	}
	public RuntimeExceptionDao<tblAdolVisitHeader, Integer> gettblAdolVisitHeaderRuntimeExceptionDao() {
		try {
			if (AdolVisitHeaderDao == null) {
				AdolVisitHeaderDao = getRuntimeExceptionDao(tblAdolVisitHeader.class);
			}
		} catch (Exception e) {

			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
			crashlytics.log(e.getMessage());
		}
		return AdolVisitHeaderDao;
	}
	public RuntimeExceptionDao<tblAdolVisitDetails, Integer> gettblAdolVisitDetailsRuntimeExceptionDao() {
		try {
			if (AdolVisitDetailsDao == null) {
				AdolVisitDetailsDao = getRuntimeExceptionDao(tblAdolVisitDetails.class);
			}
		} catch (Exception e) {

			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
			crashlytics.log(e.getMessage());
		}
		return AdolVisitDetailsDao;
	}

	public RuntimeExceptionDao<tblAdolFollowUpMaster, Integer> gettblAdolFollowUpRuntimeExceptionDao() {
		try {
			if (AdolFollowUpDao == null) {
				AdolFollowUpDao = getRuntimeExceptionDao(tblAdolFollowUpMaster.class);
			}
		} catch (Exception e) {

			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
			crashlytics.log(e.getMessage());
		}
		return AdolFollowUpDao;
	}

	public RuntimeExceptionDao<tblAdolPregnant, Integer> gettblAdolPregnantRuntimeExceptionDao() {
		try {
			if (AdolPregnantDao == null) {
				AdolPregnantDao = getRuntimeExceptionDao(tblAdolPregnant.class);
			}
		} catch (Exception e) {

			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
			crashlytics.log(e.getMessage());
		}
		return AdolPregnantDao;
	}


	//mani 15 april 2021
	public RuntimeExceptionDao<tblheightforage, Integer> getTblheightforageDao(){
		try {
			if (tblheightforageDAO == null){
				tblheightforageDAO= getRuntimeExceptionDao(tblheightforage.class);
			}
		} catch (Exception e) {
			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
			crashlytics.log(e.getMessage());
		}
		return tblheightforageDAO;
	}

	//mani 15 april 2021
	public RuntimeExceptionDao<tblweightforage, Integer> getTblweightforageDao(){
		try {
			if (tblweightforageDAO == null){
				tblweightforageDAO= getRuntimeExceptionDao(tblweightforage.class);
			}
		} catch (Exception e) {
			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
			crashlytics.log(e.getMessage());
		}
		return tblweightforageDAO;
	}

	//mani 15 april 2021
	public RuntimeExceptionDao<tblbmiforage, Integer> gettblbmiforageDao(){
		try {
			if (tblbmiforageDAO == null){
				tblbmiforageDAO= getRuntimeExceptionDao(tblbmiforage.class);
			}
		} catch (Exception e) {
			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
			crashlytics.log(e.getMessage());
		}
		return tblbmiforageDAO;
	}

	//mani 15 april 2021
	public RuntimeExceptionDao<tblmuacforage, Integer> gettblmuacforageDao(){
		try {
			if (tblmuacforageDAO == null){
				tblmuacforageDAO= getRuntimeExceptionDao(tblmuacforage.class);
			}
		} catch (Exception e) {
			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
			crashlytics.log(e.getMessage());
		}
		return tblmuacforageDAO;
	}

	//mani 15 april 2021
	public RuntimeExceptionDao<tblchildgrowth, Integer> getChildGrowthExceptionDao(){
		try {
			if (tblchildgrowthDAO == null){
				tblchildgrowthDAO= getRuntimeExceptionDao(tblchildgrowth.class);
			}
		} catch (Exception e) {
			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
			crashlytics.log(e.getMessage());
		}
		return tblchildgrowthDAO;
	}
	//10May2021 -Arpitha
	public Dao<TblSymptomMaster, Integer> getTblSymptomMasterDao(){
		try {
			if (tblSymtomMasterDao == null) {
				tblSymtomMasterDao = getDao(TblSymptomMaster.class);
			}
		}catch (Exception e)
		{
			e.printStackTrace();
		}
		return tblSymtomMasterDao;

	}

	//10May2021 -Arpitha
	public Dao<TblAncTests, Integer> getTblAncTestsDao(){
		try {
			if (tblAncTestsDao == null) {
				tblAncTestsDao = getDao(TblAncTests.class);
			}
		}catch (Exception e)
		{
			e.printStackTrace();
		}
		return tblAncTestsDao;

	}

	//10May2021 -Arpitha
	public Dao<TblAncTestsMaster, Integer> getTblAncTestsMasterDao(){
		try {
			if (tblAncTestsMasterDao == null) {
				tblAncTestsMasterDao = getDao(TblAncTestsMaster.class);
			}
		}catch (Exception e)
		{
			e.printStackTrace();
		}
		return tblAncTestsMasterDao;

	}

	public Dao<TblVisitHeader, Integer> getTblVisitHeaderDao(){
		try {
			if (tblVisitHeaderDao == null) {
				tblVisitHeaderDao = getDao(TblVisitHeader.class);
			}
		}catch (Exception e)
		{
			e.printStackTrace();
		}
		return tblVisitHeaderDao;

	}

	public Dao<TblVisitDetails, Integer> getTblVisitDetailDao(){
		try {
			if (tblVisitDetailDao == null) {
				tblVisitDetailDao = getDao(TblVisitDetails.class);
			}
		}catch (Exception e)
		{
			e.printStackTrace();
		}
		return tblVisitDetailDao;
	}

//	17May2021 Arpitha
	/**
	 * Copy the DB file to the Database Backup Directory
	 *
	 * @param context
	 *            the Context of application
	 * @param dbName
	 *            The name of the DB
	 */
	public static void copyDbToExternalStorage(Context context, String dbName, int oldVersion) {

		try {
			File name = context.getDatabasePath(dbName);

			File path = new File(Environment.getExternalStorageDirectory(), "/Sentiacare/sentiatendAsha/DatabaseBackUp");
			if (!path.exists())
				path.mkdir();

			SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy_HHmmss");
			String currentDateandTime = sdf.format(new Date());

			String dbnewfilename = "stenddb" + currentDateandTime + "_v" + oldVersion + ".db";// 21Nov2016
			// -
			// included
			// version
			// number

			File sdcardFile = new File(path + "/" + dbnewfilename);// The name
			// of output
			// file
			sdcardFile.createNewFile();
			InputStream inputStream = null;
			OutputStream outputStream = null;
			inputStream = new FileInputStream(name);
			outputStream = new FileOutputStream(sdcardFile);
			byte[] buffer = new byte[1024];
			int read;
			while ((read = inputStream.read(buffer)) != -1) {
				outputStream.write(buffer, 0, read);
			}
			inputStream.close();
			outputStream.flush();
			outputStream.close();
		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	//1-6-2020 ramesh
	public Dao<tblImageStore, Integer> getTblImageStoreDao(){
		try {
			if (tblImageStoreDao == null) {
				tblImageStoreDao = getDao(tblImageStore.class);
			}
		}catch (Exception e)
		{
			e.printStackTrace();
		}
		return tblImageStoreDao;
	}
	public Dao<tblCaseMgmt, Integer> getTblCaseMgmtDao(){
		try {
			if (tblCaseMgmtDao == null) {
				tblCaseMgmtDao = getDao(tblCaseMgmt.class);
			}
		}catch (Exception e)
		{
			e.printStackTrace();
		}
		return tblCaseMgmtDao;
	}
	public Dao<tblVersionDetails, Integer> getVersionDetailsDao(){
		try {
			if (tblversionDetailsDAO == null) {
				tblversionDetailsDAO = getDao(tblVersionDetails.class);
			}
		}catch (Exception e)
		{
			e.printStackTrace();
		}
		return tblversionDetailsDAO;
	}

	//23July2021 -Arpitha
	public Dao<TblDiagnosisMaster, Integer> getTblDiagnosisMasterDao(){
		try {
			if (tblDiagnosisMasterDao == null) {
				tblDiagnosisMasterDao = getDao(TblDiagnosisMaster.class);
			}
		}catch (Exception e)
		{
			e.printStackTrace();
		}
		return tblDiagnosisMasterDao;

	}

	//23July2021 -Arpitha
	public Dao<Tblinterpretationmaster, Integer> getTblInterpretationMasterDao(){
		try {
			if (tblInterpretationMasterDao == null) {
				tblInterpretationMasterDao = getDao(Tblinterpretationmaster.class);
			}
		}catch (Exception e)
		{
			e.printStackTrace();
		}
		return tblInterpretationMasterDao;

	}
	//23July2021 -Arpitha
	public Dao<TblSignsMaster, Integer> getTblSignsMasterDao(){
		try {
			if (tblSignsMasterDao== null) {
				tblSignsMasterDao = getDao(TblSignsMaster.class);
			}
		}catch (Exception e)
		{
			e.printStackTrace();
		}
		return tblSignsMasterDao;

	}

	public Dao<TblVisitDetailsPojo, Integer> getTblVisitDetailPojoDao(){
		try {
			if (tblVisitDetailsPojoDao == null) {
				tblVisitDetailsPojoDao = getDao(TblVisitDetailsPojo.class);
			}
		}catch (Exception e)
		{
			e.printStackTrace();
		}
		return tblVisitDetailsPojoDao;
	}
	public Dao<tblSanNapMaster, Integer> getTblSanNapMasterDao(){
		try {
			if (tblSanNapMasterDao == null) {
				tblSanNapMasterDao = getDao(tblSanNapMaster.class);
			}
		}catch (Exception e)
		{
			e.printStackTrace();
		}
		return tblSanNapMasterDao;

	}
	public Dao<tblSanNap, Integer> getTblSanNapDao(){
		try {
			if (tblSanNapDao == null) {
				tblSanNapDao = getDao(tblSanNap.class);
			}
		}catch (Exception e)
		{
			e.printStackTrace();
		}
		return tblSanNapDao;

	}
	public Dao<tblSanNapHeader, Integer> getTblSanNapHeaderDao(){
		try {
			if (tblSanNapHeaderDao == null) {
				tblSanNapHeaderDao = getDao(tblSanNapHeader.class);
			}
		}catch (Exception e)
		{
			e.printStackTrace();
		}
		return tblSanNapHeaderDao;

	}
}
