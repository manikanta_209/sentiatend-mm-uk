package com.sc.stmansi.womanlist;

public class QueryParams {
    public int nextItemPosition;
    public String selectedVillageTitle;
    public int selectedVillageCode;
    public String textViewFilter;
    public CareType careType;
    public String userId;
    public boolean noVisitSelected;
    public  int selectedDateFilter;
    public  String fromDate;//Arpitha 16Nov2019
    public  String toDate;//Arpitha 16Nov2019
    public  String womanId;//Arpitha 16Nov2019
    public  boolean isDeactivated;//Arpitha 25Nov2019
    public boolean isLMPNotConfirmed; //25Mar2021 Bindu
    public String strFromDate;
    public String strToDate;
}
