package com.sc.stmansi.womanlist;

public class ActionBarVillageSpinnerItem {

    private String title;
    private int villageCode;

    public ActionBarVillageSpinnerItem(String title, int villageCode) {
        this.title = title;
        this.villageCode = villageCode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getVillageCode() {
        return villageCode;
    }

    @Override
    public String toString() {
        return title;
    }
}
