package com.sc.stmansi.womanlist;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.About.AboutActivity;
import com.sc.stmansi.HomeVisit.HomeVisit;
import com.sc.stmansi.HomeVisit.HomeVisitListActivity;
import com.sc.stmansi.HomeVisit.PncHomeVisit;
import com.sc.stmansi.childregistration.RegisteredChildList;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.deactivate.WomanDeactivateActivity;
import com.sc.stmansi.delivery.DeliveryInfoActivity;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.pendingservices.IndicatorViewClickListener;
import com.sc.stmansi.quickAction.ActionItem;
import com.sc.stmansi.quickAction.QuickAction;
import com.sc.stmansi.recyclerview.ClickListener;
import com.sc.stmansi.recyclerview.WomanAdapter;
import com.sc.stmansi.registration.ReconfirmationRegActivity;
import com.sc.stmansi.repositories.DeliveryRepository;
import com.sc.stmansi.repositories.FacilityRepository;
import com.sc.stmansi.repositories.LoginRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.services.ServicesSummaryActivity;
import com.sc.stmansi.services.UnplannedServicesListActivity;
import com.sc.stmansi.tables.TblDeliveryInfo;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.viewprofile.ViewProfileActivity;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.sc.stmansi.R;

public class WomanListActivity extends AppCompatActivity
        implements AdapterView.OnItemSelectedListener, View.OnClickListener,
        View.OnLongClickListener, ClickListener, IndicatorViewClickListener {

    private AQuery aq;

    private Spinner villageSpinner;
    private Map<String, Integer> villageCodeForName;
    private boolean spinnerSelectionChanged = false;

    private String wFilterStr = "";

    private TextView allCountView, noDataLabelView;

    private TblInstusers user;

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter registeredWomanAdapter;
    private List<tblregisteredwomen> filteredRowItems;
    private boolean isUpdating;

    private DatabaseHelper databaseHelper;
    private AppState appState;
    private WomanRepository womanRepository;
    private int nextItemPosition;

    private String womenCondition;

    //31Aug2019 - Bindu
    private static final int ID_WEDIT = 1;
    private static final int ID_SERVLIST = 2;
    private static final int ID_UNPLANNEDSERVLIST = 3;
    private static final int ID_HOMEVISITADD = 4;
    private static final int ID_DEL = 5;//31Oct2019 Arpitha
    private QuickAction mQuickAction = null;
    private tblregisteredwomen woman;

    private int status = 0;

    private CareType careType;
    private boolean noVisitSelected;
    private static final int ID_CHILD = 6;//16Nov2019 Arpitha
    private static final int ID_DEACT = 7;//19Nov2019 Arpitha


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_womanlist);
        try {
            databaseHelper = getHelper();

            Bundle bundle = getIntent().getBundleExtra("globalState");
            appState = bundle.getParcelable("appState");

            UserRepository userRepository = new UserRepository(databaseHelper);
            user = userRepository.getOneAuditedUser(appState.ashaId);

            womanRepository = new WomanRepository(databaseHelper);

            String screen = getIntent().getStringExtra("screen");
            womenCondition = screen;
            aq = new AQuery(this);

            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

            //22Jul2019- Bindu
            getSupportActionBar().setCustomView(R.layout.ejanani_village_filter);
            getSupportActionBar().setDisplayShowCustomEnabled(true);

            aq.id(R.id.imgresetfilter).getImageView().setBackgroundResource(R.drawable.ic_reset_gray);//04Dec2019 Arpitha


            villageSpinner = aq.id(R.id.spVillageFilter).getSpinner();
            villageSpinner.setOnItemSelectedListener(this);
            populateSpinnerVillage();

            if (screen.equalsIgnoreCase("nearingdel")) {
                aq.id(R.id.txtlabell).text(getResources().getString(R.string.sp_dwoman));
            } else if (screen.equalsIgnoreCase("deact")) {
                aq.id(R.id.txtlabell).text(getResources().getString(R.string.deactivatelist));
            } else if (screen.equalsIgnoreCase("homevisit")) {
                aq.id(R.id.txtlabell).text(getResources().getString(R.string.homevisit));
                aq.id(R.id.llfilter).visible();
            } else if (screen.equalsIgnoreCase("hrp")) {
               // aq.id(R.id.txtlabell).text(getResources().getString(R.string.hrp));
                aq.id(R.id.txtlabell).text(getResources().getString(R.string.hrpheading));
            } else if (screen.equalsIgnoreCase("compl")) {
                aq.id(R.id.txtlabell).text(getResources().getString(R.string.complications));
                aq.id(R.id.llfilter).visible();
            }else if (screen.equalsIgnoreCase("deact")) {//25Nov2019 Arpitha
                aq.id(R.id.txtlabell).text(getResources().getString(R.string.deactivatelist));
            }

            allCountView = aq.id(R.id.txtcountwl).getTextView();
            noDataLabelView = aq.id(R.id.txtnodata).getTextView();

            if (womenCondition.equalsIgnoreCase("hrp"))
                recyclerView = (RecyclerView) aq.id(R.id.hrp_women_recycler_view).getView();
            else if (womenCondition.equalsIgnoreCase("nearingdel"))
                recyclerView = (RecyclerView) aq.id(R.id.near_deli_women_recycler_view).getView();
            else if(womenCondition.equalsIgnoreCase("homevisit"))
                recyclerView = (RecyclerView) aq.id(R.id.home_visit_women_recycler_view).getView();
            if (womenCondition.equalsIgnoreCase("deact"))//25Nov2019 Arpitha
                recyclerView = (RecyclerView) aq.id(R.id.hrp_women_recycler_view).getView();

            recyclerView.setVisibility(View.VISIBLE);

            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            recyclerView.setHasFixedSize(true);

            // use a linear layout manager
            layoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(layoutManager);

            QueryParams queryParams = new QueryParams();
            queryParams.selectedVillageCode = 0;
            queryParams.careType = CareType.GENERAL;
            queryParams.nextItemPosition = 0;
            queryParams.textViewFilter = "";
            queryParams.userId = appState.sessionUserId;

            filteredRowItems = prepareRowItemsForDisplay(queryParams);
            isUpdating = true;
            registeredWomanAdapter = new WomanAdapter(filteredRowItems, this);
            recyclerView.setAdapter(registeredWomanAdapter);
            updateLabels();
            registeredWomanAdapter.notifyItemRangeInserted(0, filteredRowItems.size());
            nextItemPosition = filteredRowItems.size() + 1;

//            recyclerView.addOnScrollListener(new OnRecyclerScrollListener());
            isUpdating = false;

            aq.id(R.id.imgresetfilter).getImageView().setOnClickListener(new FilterResetListener());

            EditText filterTextView = aq.id(R.id.etWomanFilter).getEditText();
            /*filterTextView.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if(count>0)
                    aq.id(R.id.imgresetfilter).getImageView().setBackgroundResource(R.drawable.ic_reset);
                    else
                        aq.id(R.id.imgresetfilter).getImageView().setBackgroundResource(R.drawable.ic_reset_gray);


                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });*/ //10Dec2019 - Bindu
            filterTextView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (v.getText() != null) {
                        aq.id(R.id.imgresetfilter).getImageView().setBackgroundResource(R.drawable.ic_reset); //10Dec2019 - Bindu
                        wFilterStr = v.getText().toString();
                        updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr);
                    }
                    return true;
                }
            });


            aq.id(R.id.rdall).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        aq.id(R.id.chknovisitnotdone).getCheckBox().setSelected(false);
                        careType = CareType.GENERAL;
                        status = 0;
                        aq.id(R.id.chknovisitnotdone).gone(); //18Sep2019 - Bindu - No visit for all to be removed
                        aq.id(R.id.chklmpnotconfirmed).visible(); //01May2021 Bindu - Display LMP not confirmed
                        aq.id(R.id.chklmpnotconfirmed).getCheckBox().setSelected(false);
                        updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr);
                    } catch (Exception e) {

                    }
                }
            });

            aq.id(R.id.rdpreg).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        aq.id(R.id.chknovisitnotdone).getCheckBox().setSelected(false);
                        aq.id(R.id.chklmpnotconfirmed).visible(); //01May2021 Bindu - Display LMP not confirmed
                        aq.id(R.id.chklmpnotconfirmed).getCheckBox().setSelected(false);
                        noVisitSelected = false;
                        careType = CareType.ANC;
                        status = 1;
                        aq.id(R.id.chknovisitnotdone).visible(); //18Sep2019 - Bindu - No visit for all to be removed
                        updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr);
                    } catch (Exception e) {

                    }
                }
            });

            aq.id(R.id.rdmother).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        aq.id(R.id.chknovisitnotdone).getCheckBox().setSelected(false);
                        noVisitSelected = false;
                        careType = CareType.PNC;
                        status = 2;
                        aq.id(R.id.chknovisitnotdone).visible(); //18Sep2019 - Bindu - No visit for all to be removed
                        aq.id(R.id.chklmpnotconfirmed).gone(); //08Apr2021 Bindu - Hide LMP not confirmed
                        aq.id(R.id.chklmpnotconfirmed).getCheckBox().setChecked(false); // set lmp checked false 02MAy2021 Bindu
                        updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr);
                    } catch (Exception e) {

                    }
                }
            });

            //15Sep2019 - Bindu - Oncheck Change listener~

            CheckBox chk = findViewById(R.id.chknovisitnotdone);
            chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    noVisitSelected = true;
                    updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr);
                }
            });

            //25Mar2021 - Bindu - Oncheck Change listener

            CheckBox chklmpnotconfirmed = findViewById(R.id.chklmpnotconfirmed);
            chklmpnotconfirmed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr);
                }
            });

            /*aq.id(R.id.chknovisitnotdone).getCheckBox().setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    noVisitSelected = true;
                    updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr);
                }
            });*/
//31Aug2019 - Bindu - ADD items for quick action
            String str = getResources().getString(R.string.viewprofile);
            ActionItem wEdit = new ActionItem(ID_WEDIT, str, ContextCompat.getDrawable(this,R.drawable.ic_edit_60));

            str = getResources().getString(R.string.services);
            ActionItem wPlannedServList = new ActionItem(ID_SERVLIST, str, ContextCompat.getDrawable(this,R.drawable.anm_pending_activities));

            str = getResources().getString(R.string.unplannedservices);
            ActionItem wUnplannedServList = new ActionItem(ID_UNPLANNEDSERVLIST, str, ContextCompat.getDrawable(this,R.drawable.anm_pending_activities));

            str = getResources().getString(R.string.homevisit);
            ActionItem wHomeVisitAdd = new ActionItem(ID_HOMEVISITADD, str, ContextCompat.getDrawable(this,R.drawable.ic_homevisit));

            str = getResources().getString(R.string.deliveryHeading);
            ActionItem wDel = new ActionItem(ID_DEL, str, ContextCompat.getDrawable(this,R.drawable.delivery_info));

            //          16Nov2019 Arpitha
            str = getResources().getString(R.string.viewchild);
            ActionItem wChild = new ActionItem(ID_CHILD, str, ContextCompat.getDrawable(this,R.drawable.general_examination_baby));

            //          19Nov2019 Arpitha
            str = getResources().getString(R.string.deactivate);
            ActionItem wDeact = new ActionItem(ID_DEACT, str,
                    ContextCompat.getDrawable(this,R.drawable.deactivate));

            mQuickAction = new QuickAction(this);

            mQuickAction.addActionItem(wEdit);
            mQuickAction.addActionItem(wPlannedServList);
            mQuickAction.addActionItem(wUnplannedServList);
            /*if(!screen.equalsIgnoreCase("deact")) { //29Nov2019 - Bindu - if deactivation and then hide or show home visit add
                mQuickAction.addActionItem(wHomeVisitAdd);
            }*/
            mQuickAction.addActionItem(wHomeVisitAdd);
             mQuickAction.addActionItem(wDel);
             if(screen.equalsIgnoreCase("hrp") ||
                     screen.equalsIgnoreCase("homevisit") ||
                     screen.equalsIgnoreCase("deact"))
            mQuickAction.addActionItem(wChild);//27Nov2019 Arpitha
            mQuickAction.addActionItem(wDeact);//19Nov2019 Arpitha

            //setup the action item click listener
            mQuickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
                @Override
                public void onItemClick(QuickAction quickAction, int pos, int actionId) {
                    ActionItem actionItem = quickAction.getActionItem(pos);
                    try {
                        switch (actionItem.getActionId()) {
                            case ID_WEDIT: {
                                Intent nextScreen = new Intent(WomanListActivity.this, ViewProfileActivity.class);
                                nextScreen.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                                startActivity(nextScreen);
                                mQuickAction.dismiss();
                                break;
                            }case ID_SERVLIST: {
                                Intent nextScreen = new Intent(WomanListActivity.this, ServicesSummaryActivity.class);
                                nextScreen.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                                startActivity(nextScreen);
                                mQuickAction.dismiss();
                                break;
                            }case ID_UNPLANNEDSERVLIST: {
                                Intent nextScreen = new Intent(WomanListActivity.this, UnplannedServicesListActivity.class);
                                nextScreen.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                                startActivity(nextScreen);
                                mQuickAction.dismiss();
                                break;
                            }case ID_HOMEVISITADD :{
                                //18Oct2019 - Bindu - Chk nearing del message
                                WomanRepository womanRepository = new WomanRepository(databaseHelper); //11Nov2019 - Bindu - Add Woman repo and get woman details
                                woman = womanRepository.getRegistartionDetails(appState.selectedWomanId, appState.sessionUserId);

                                if(woman.getRegPregnantorMother() == 2 ){ //29Nov2019 - Bindu - Navigate to Home visit item
                                    Intent nextScreen = new Intent(WomanListActivity.this, PncHomeVisit.class);
                                    nextScreen.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                                    startActivity(nextScreen);
                                } else {
                                    int daysDiff = DateTimeUtil.getDaysBetweenDates(DateTimeUtil.getTodaysDate(), woman.getRegLMP());
                                    if (daysDiff > 210) {
                                        displayNearingDelAlertMessage(woman);
                                    } else {
                                        Intent nextScreen = new Intent(WomanListActivity.this, HomeVisit.class);
                                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                        startActivity(nextScreen);
                                    }
                                }
                                mQuickAction.dismiss();
                                break;
                            }case ID_DEL :{
                                //07Dec2019 - Bindu Get woman details
                                WomanRepository womanRepository = new WomanRepository(databaseHelper); //11Nov2019 - Bindu - Add Woman repo and get woman details
                                woman = womanRepository.getRegistartionDetails(appState.selectedWomanId, appState.sessionUserId);

                                if(woman.getRegPregnantorMother()==2 ||
                                        (woman.getRegPregnantorMother()==1 &&
                                                woman.getRegLMP()!=null &&
                                                DateTimeUtil.getNumberOfDaysFromLMP(woman.getRegLMP())>210)) {

                                    Intent nextScreen = new Intent(WomanListActivity.this, DeliveryInfoActivity.class);
                                    nextScreen.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                                    nextScreen.putExtra("woman", woman);
                                   startActivity(nextScreen);
                                    mQuickAction.dismiss();
                                }else {
                                    if(woman.getDateDeactivated()!=null && woman.getDateDeactivated().trim().length()>0)
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.userdeactivated), Toast.LENGTH_LONG).show();

                                    else
                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.m132), Toast.LENGTH_LONG).show();

                                }
                                break;
                            }
                            //                          16Nov2019 Arpitha
                            case ID_CHILD:
                            {
                                WomanRepository womanRepository = new WomanRepository(databaseHelper);
                                woman = womanRepository.getRegistartionDetails(appState.selectedWomanId, appState.sessionUserId);

                                if(woman.getRegPregnantorMother()==2
                                        && woman.getRegADDate()!=null && woman.getRegADDate().trim().length()>0) {


                                    Intent nextScreen = new Intent(WomanListActivity.this, RegisteredChildList.class);
                                    nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                    nextScreen.putExtra("womanId", woman.getWomanId());

                                    startActivity(nextScreen);
                                }else
                                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.ntapplicable),Toast.LENGTH_LONG).show();

                                mQuickAction.dismiss();
                                break;
                            }

                            //                          19Nov2019 Arpitha
                            case ID_DEACT:
                                WomanRepository womanRepository = new WomanRepository(databaseHelper);
                                woman = womanRepository.getRegistartionDetails(appState.selectedWomanId, appState.sessionUserId);
                                Intent deact = new Intent(WomanListActivity.this, WomanDeactivateActivity.class);
                                deact.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                                deact.putExtra("woman", woman);
                                startActivity(deact);
                                mQuickAction.dismiss();
                                break;
                        }
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                }
            });

            mQuickAction.setOnDismissListener(new QuickAction.OnDismissListener() {
                @Override
                public void onDismiss() {
//				Toast.makeText(activity, "Ups..dismissed", Toast.LENGTH_SHORT).show();
                }
            });

        } catch (Exception e) {
            
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    //    initialize db object
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    //    prepare bundle
    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        return b;
    }

    /**
     * populate Village to Spinner
     */
    private void populateSpinnerVillage() throws Exception {
        FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
        villageCodeForName = facilityRepository.getPlaceMap();

        ArrayAdapter<CharSequence> villageSpinnerAdapter = new ArrayAdapter<>(this, R.layout.actionbar_spinner_list_item);
//        villageSpinnerAdapter.add("All Women");06May2021 Arpitha
        villageSpinnerAdapter.add(getResources().getString(R.string.all_Womans));//06May2021 Arpitha
        for (Map.Entry<String, Integer> village : villageCodeForName.entrySet()) {
            villageSpinnerAdapter.add(village.getKey());
        }
        villageSpinnerAdapter.setDropDownViewResource(R.layout.actionbar_spinner_layout_view);
        villageSpinner.setAdapter(villageSpinnerAdapter);
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * Initializes the Action Bar items
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.servicesmenu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Called whenever an item clicked in actionbar
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action buttons
        switch (item.getItemId()) {

            case R.id.home: {
                Intent goToScreen = new Intent(WomanListActivity.this, MainMenuActivity.class);
                goToScreen.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }

            case android.R.id.home: {
                Intent goToScreen = new Intent(WomanListActivity.this, MainMenuActivity.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                startActivity(goToScreen);

                return true;
            }

            case R.id.logout: {
                Intent goToScreen = new Intent(WomanListActivity.this, LoginActivity.class);
                displayAlert(getResources().getString(R.string.m111), goToScreen);
                return true;
            }


            case R.id.wlist: {
                Intent goToScreen = new Intent(WomanListActivity.this, RegisteredWomenActionTabs.class);
                goToScreen.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }
            //30Sep2019 - Bindu
            case R.id.about: {
                Intent goToScreen = new Intent(WomanListActivity.this, AboutActivity.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Save Confirmation Alert
     */
    private void displayAlert(final String spanText2, final Intent goToScreen) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set dialog message
        alertDialogBuilder
                .setMessage(spanText2)
                .setCancelable(false)
                .setNegativeButton((getResources().getString(R.string.m121)), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (goToScreen != null) {
                            startActivity(goToScreen);
                            if (spanText2.contains("logout")) {
                                try {
                                    LoginRepository loginRepository = new LoginRepository(databaseHelper);
                                    loginRepository.updateLogoutTime(appState.loginAuditId, appState.sessionUserId);
                                } catch (Exception e) {
                                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                                }
                            }
                        } else
                            dialog.cancel();
                    }
                })
                .setPositiveButton((getResources().getString(R.string.m122)), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    //    upadate list
    public void updateList(String villageTitle, Map<String, Integer> villageNameForCode, String textViewFilter) {
        try {
            QueryParams queryParams = new QueryParams();
            queryParams.nextItemPosition = 0;
            queryParams.selectedVillageTitle = villageTitle;
            queryParams.textViewFilter = textViewFilter;
            queryParams.careType = this.careType;
            queryParams.noVisitSelected = aq.id(R.id.chknovisitnotdone).isChecked();
            queryParams.userId = appState.sessionUserId;
            //08Apr2021 Bindu add lmpnotconfirmed
            queryParams.isLMPNotConfirmed = aq.id(R.id.chklmpnotconfirmed).isChecked();

            this.wFilterStr = textViewFilter;

            new LoadPregnantList(this, queryParams, true, villageNameForCode).execute();
            isUpdating = true;
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            
        }
    }

    //    load list data
    static class LoadPregnantList extends AsyncTask<Void, Void, Void> {

        private WeakReference<WomanListActivity> weakReference;
        private QueryParams queryParams;
        private boolean firstLoad;
        private Map<String, Integer> villageCodeForName;

        LoadPregnantList(WomanListActivity viewsUpdateCallback, QueryParams queryParams, boolean firstLoad, Map<String, Integer> villageCodeForName) {
            this.weakReference = new WeakReference<>(viewsUpdateCallback);
            this.queryParams = queryParams;
            this.firstLoad = firstLoad;
            this.villageCodeForName = villageCodeForName;
        }

        LoadPregnantList(WomanListActivity viewsUpdateCallback, QueryParams queryParams, boolean firstLoad) {
            this.weakReference = new WeakReference<>(viewsUpdateCallback);
            this.queryParams = queryParams;
            this.firstLoad = firstLoad;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            int selectedCode;
            // This means second constructor was used (called by scrollListener)
            if (villageCodeForName == null) selectedCode = queryParams.selectedVillageCode;
            else {
                String selectedVillageTitle = queryParams.selectedVillageTitle;
                if (selectedVillageTitle == null)
                    selectedCode = 0;
                else {
                    Integer code = villageCodeForName.get(selectedVillageTitle);
                    selectedCode = (code == null) ? 0 : code;
                }
            }
            try {
                WomanListActivity activity = weakReference.get();
                queryParams.selectedVillageCode = selectedCode;
                List<tblregisteredwomen> rows = activity.prepareRowItemsForDisplay(queryParams);
                if (firstLoad) {
                    int prevSize = activity.filteredRowItems.size();
                    activity.filteredRowItems.clear();
                    activity.registeredWomanAdapter.notifyItemRangeRemoved(0, prevSize);
                    firstLoad = false;
                }
                if (rows.size() > 0) {
                    activity.filteredRowItems.addAll(rows);
                    activity.nextItemPosition = activity.filteredRowItems.size() + 1;
                    if (rows.size() > 0) {
                        activity.registeredWomanAdapter.notifyItemRangeInserted(firstLoad ? 0 : activity.nextItemPosition,
                                activity.filteredRowItems.size());
                    }
                }
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            try {
                WomanListActivity activity = weakReference.get();
                activity.updateLabels();
                if (!firstLoad && activity.filteredRowItems.size() > 0)
                    activity.layoutManager.scrollToPosition(activity.registeredWomanAdapter.getItemCount());
                activity.isUpdating = false;
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                
            }
        }
    }

    //   data to display
    private List<tblregisteredwomen> prepareRowItemsForDisplay(QueryParams queryParams) throws Exception {
        //15Sep2019 - Bindu - add Visit not done parameter
        boolean noVisitDone = false;
        if(aq.id(R.id.chknovisitnotdone).isChecked())
            noVisitDone = true;
        if (womenCondition.equalsIgnoreCase("hrp"))
            return womanRepository.getHRPList(queryParams);
        else if (womenCondition.equalsIgnoreCase("nearingdel"))
            return womanRepository.getNearingDeliveryList(queryParams);
        else if(womenCondition.equalsIgnoreCase("homevisit")) {
            return womanRepository.getWomenForCareType(queryParams);
        }
         else if (womenCondition.equalsIgnoreCase("deact"))//25Nov2019 Arpitha
                return womanRepository.getDeactivatedList(queryParams);

        return new ArrayList<>(1);
    }

    //    update labels based on data
    private void updateLabels() {
        if (filteredRowItems.size() == 0) {
            allCountView.setText(getResources().getString(R.string.recordcount,
                    filteredRowItems.size()));

            noDataLabelView.setText(getResources().getString(R.string.no_data));
            noDataLabelView.setVisibility(View.VISIBLE);
            return;
        }
        noDataLabelView.setVisibility(View.GONE);
        allCountView.setText(getResources().getString(R.string.recordcount,
                filteredRowItems.size()));
        allCountView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        appState.selectedVillageTitle = (String) parent.getItemAtPosition(position);
        if (spinnerSelectionChanged) {
            updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr);
        }
        spinnerSelectionChanged = true;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    class FilterResetListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            aq.id(R.id.imgresetfilter).getImageView().setBackgroundResource(R.drawable.ic_reset_gray);
            aq.id(R.id.etWomanFilter).text("");
            wFilterStr = "";
            CareType careType = CareType.GENERAL;
            updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr);
        }
    }

    @Override
    public void onClick(View v) {
        setSelectedWomanId(v);
        mQuickAction.show(v);
    }

    @Override
    public boolean onLongClick(View v) {
        setSelectedWomanId(v);

        Intent intent = new Intent(this, ViewProfileActivity.class);
        intent.putExtra("globalState",getIntent().getBundleExtra("globalState"));
        startActivity(intent);
        return true;
    }

    //    set woman id
    private void setSelectedWomanId(View v) {
        int itemPosition = recyclerView.getChildLayoutPosition(v);
        woman = filteredRowItems.get(itemPosition);

        appState.selectedWomanId = woman.getWomanId();
    }

    //    recycler view scroll
    public class OnRecyclerScrollListener extends RecyclerView.OnScrollListener {
        int ydy = 0;

        @Override
        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            ydy = dy;

            LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            int lastCompletelyVisibleItemPosition = linearLayoutManager.findLastCompletelyVisibleItemPosition();
            boolean shouldPullUpRefresh = lastCompletelyVisibleItemPosition <= filteredRowItems.size() - 1
                    && recyclerView.getScrollState() == RecyclerView.SCROLL_STATE_DRAGGING;
            if (shouldPullUpRefresh) {
                QueryParams queryParams = new QueryParams();
                queryParams.nextItemPosition = nextItemPosition;
                queryParams.selectedVillageCode = appState.selectedVillageCode;
                queryParams.textViewFilter = wFilterStr;
                if (!isUpdating) {
                    isUpdating = true;
                    new LoadPregnantList(WomanListActivity.this, queryParams, false).execute();
                }
            }
        }
    }

    /**
     * This method Calls the idle timeout
     */ //09Aug2019 - Bindu
   /* @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        new ActivitiesStack(this).delayedIdle(appState.idleTimeOut);
    }*/

    @Override
    protected void onDestroy() {
        super.onDestroy();

        /*
         * You'll need this in your class to release the helper when done.
         */
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

    @Override
    public void displayAlert(int selectedItemPosition) {
        try {
            ArrayList<String> compList = new ArrayList<>();
            String complication;

            if(filteredRowItems.get(selectedItemPosition).getRegCCConfirmRisk()!=null &&
                    filteredRowItems.get(selectedItemPosition).getRegCCConfirmRisk().trim().length()>0
                    &&  filteredRowItems.get(selectedItemPosition)
                    .getRegCCConfirmRisk().equalsIgnoreCase("Y"))
                complication = getResources().getString(R.string.riskidentifiedbycc)+"\n"+
                        filteredRowItems.get(selectedItemPosition)
                                .getRegRiskFactorsByCC();
//                        +" "
//                        +filteredRowItems.get(selectedItemPosition).getRegOtherRiskByCC();
            else
                complication = filteredRowItems.get(selectedItemPosition).getRegriskFactors();
            if (complication != null && complication.length() > 0 ||
                    (filteredRowItems.get(selectedItemPosition).getRegRiskFactorsByCC()!=null && filteredRowItems.get(selectedItemPosition).getRegRiskFactorsByCC().trim().length()>0)) {
                compList = new ArrayList<>();
                compList.add(complication.replace(",", ", ")
                        .replace("PH:", "\nPH:").replace(", ,","")
                        .replace(",,",""));


                ArrayList<String> complListDisp = new ArrayList<>();
                for (int i = 0; i < compList.size(); i++) {
                    String compl[] = compList.get(i).split("[,\n]");
                    for (int j = 0; j < compl.length; j++) {
                        String strComp = compl[j];
                        if (strComp.contains("CH:"))
                            strComp = strComp.replaceAll("CH:", "");
                        if (strComp.contains("PH:"))
                            strComp = strComp.replaceAll("PH:", "");

                        strComp = strComp.replaceAll(" ", "");
                        if (strComp.contains("ormoreconsecutivespontaneousabortions"))
//                            strComp = strComp.split("[<>0123456789(]")[1];
                            strComp = strComp.split("[3<>(]")[1];
                        else
                            strComp = strComp.split("[<>(]")[0];
                        //strComp = strComp.split("[<>0123456789(]")[0];
                        strComp = strComp.replaceAll("/","");
                        strComp = strComp.toLowerCase().trim();
                        int identifier = getResources().getIdentifier
                                (strComp,
                                        "string", "com.sc.stmansi");
                        if (identifier > 0) {
                            if (compl[j].contains("CH:"))
                                complListDisp.add("CH: " + getResources().getString(identifier));
                            else if (compl[j].contains("PH:"))
                                complListDisp.add("PH: " + getResources().getString(identifier));
                            else
                                complListDisp.add(getResources().getString(identifier));

                        } else {

                            complListDisp.add(compl[j]);

                        }
                    }

                }

                ArrayList<String> complications = new ArrayList<>();
                String strStrings = "";
                for (int i = 0; i < complListDisp.size(); i++) {
                    if (i != 0)
                        strStrings = strStrings + ", " + complListDisp.get(i);
                    else
                        strStrings = complListDisp.get(i);
                }

                strStrings = strStrings.replace(", ,","")
                        .replace(",,","");

                String[] list = strStrings.split("PH:");

                ArrayList<String> l = new ArrayList<>();
                for (int i = 0; i < list.length; i++) {
                    if(!list[i].equalsIgnoreCase(", ")) {
                        if (i == 0)
                            l.add(list[i]);
                        else
                            l.add("\nPH: " + list[i]);
                    }
                }




                AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                View convertView = LayoutInflater.from(this).inflate(R.layout.complicated_reasons_list, null, false);

                ListView lv = convertView.findViewById(R.id.lvCompliatedReasonslist);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.listitem, l);

                alertDialog.setView(convertView).setCancelable(true).setTitle((this.getResources().getString(R.string.compl_reasons)))
                        .setPositiveButton((this.getResources().getString(R.string.ok)), new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                lv.setAdapter(adapter);
                alertDialog.show();
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    @Override
    public void displayHomeVisitList(int selectedItemPosition) {
        tblregisteredwomen woman = filteredRowItems.get(selectedItemPosition);
        Intent homevisitlist = new Intent(this, HomeVisitListActivity.class);
        appState.selectedWomanId = woman.getWomanId();
        homevisitlist.putExtra("globalState", prepareBundle());
        startActivity(homevisitlist);
    }

    @Override
    public void displayDelComplicationAlert(int selectedItemPosition) {
        try {
            ArrayList<String> complL = new ArrayList<>();
            if (filteredRowItems.get(selectedItemPosition).getRegInDanger() == 1 &&
                    filteredRowItems.get(selectedItemPosition).getRegPregnantorMother() == 2) {
                DeliveryRepository deliveryRepository = new DeliveryRepository(databaseHelper);
                List<TblDeliveryInfo> delData = deliveryRepository.getDeliveryData(filteredRowItems.get(selectedItemPosition).getWomanId(), filteredRowItems.get(selectedItemPosition).getUserId());

                if (delData != null && delData.size() > 0 && delData.get(0)
                        .getDelMotherComplications() != null && delData.get(0)
                        .getDelMotherComplications().trim().length() > 0) {

                    String[] chlCompl;


                    chlCompl = getResources().getStringArray(R.array.mothercompl);

                    String[] selectedCompl = delData.get(0)
                            .getDelMotherComplications().split(",");

                    complL = new ArrayList<>();

                    for (int i = 1; i <= selectedCompl.length; i++) {
                        if (selectedCompl[i - 1] != null && selectedCompl[i - 1].trim().length() > 0)
                            complL.add(chlCompl[Integer.parseInt(selectedCompl[i - 1].trim())]);
                    }
                }

                complL.add(delData.get(0).getDelOtherComplications());

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                View convertView = LayoutInflater.from(this).inflate(R.layout.complicated_reasons_list, null, false);

                ListView lv = convertView.findViewById(R.id.lvCompliatedReasonslist);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.listitem, complL);

                alertDialog.setView(convertView).setCancelable(true).setTitle((this.getResources().getString(R.string.complication_reasons)))
                        .setPositiveButton((this.getResources().getString(R.string.ok)), new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                lv.setAdapter(adapter);
                alertDialog.show();
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    //18Oct2019 - Bindu - Alert message nearing del
    public  void displayNearingDelAlertMessage(final tblregisteredwomen regwoman) throws Exception{
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        String alertmsg =  woman.getRegWomanName() + ", " + " EDD : " + woman.getRegEDD() ;
        //alertDialogBuilder.setMessage(message).setCancelable(false).setTitle(Html.fromHtml("<font color='#FF7F27'>Alert -- </font><br><br><p>"+alertmsg + "</p>")).setNegativeButton(
        String msg = "<font color='#CC3333'> Nearing Delivery - </font>"+alertmsg;
        alertDialogBuilder.setMessage(getResources().getText(R.string.alerthomevisitnearingdelmsg)).setCancelable(false).setTitle(Html.fromHtml(msg)).setNegativeButton(
                getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent nextScreen = new Intent(WomanListActivity.this, HomeVisit.class);
                        nextScreen.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                        startActivity(nextScreen);
                        mQuickAction.dismiss();
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void displayLMPConfirmation(int selectedItemPosition) {
        tblregisteredwomen woman = filteredRowItems.get(selectedItemPosition);
        Intent nextScreen = new Intent(this, ReconfirmationRegActivity.class);
        appState.selectedWomanId = woman.getWomanId();
        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
        startActivity(nextScreen);
    }

//    01May2021 Arpitha
    @Override
    public void onBackPressed() {

    }
}
