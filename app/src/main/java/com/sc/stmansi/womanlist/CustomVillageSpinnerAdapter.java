package com.sc.stmansi.womanlist;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import com.sc.stmansi.R;

public class CustomVillageSpinnerAdapter extends BaseAdapter {
	
	private TextView txtTitle;
    private ArrayList<ActionBarVillageSpinnerItem> villageSpinnerItem;
    private Context context;
    private int customLayout;
 
    
    /* Constructor to initialize with default values */
    public CustomVillageSpinnerAdapter(Context context, int customLayout, 
            ArrayList<ActionBarVillageSpinnerItem> spinnerNavItem) {
        this.villageSpinnerItem = spinnerNavItem;
        this.context = context;
        this.customLayout = customLayout;
    }
 
    @Override
    public int getCount() {
        return villageSpinnerItem.size();
    }
 
    @Override
    public Object getItem(int index) {
        return villageSpinnerItem.get(index);
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
 
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(customLayout, null);
        }
        
        txtTitle = convertView.findViewById(R.id.txtVillage);
        txtTitle.setText(villageSpinnerItem.get(position).getTitle());
        return convertView;
    }
     
 
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(customLayout, null);
        }
        
        txtTitle = convertView.findViewById(R.id.txtVillage);
        txtTitle.setText(villageSpinnerItem.get(position).getTitle());
        return convertView;
    }

}
