package com.sc.stmansi.womanlist;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.HomeVisit.HomeVisit;
import com.sc.stmansi.HomeVisit.HomeVisitListActivity;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.deactivate.WomanDeactivateActivity;
import com.sc.stmansi.delivery.DeliveryInfoActivity;
import com.sc.stmansi.pendingservices.IndicatorViewClickListener;
import com.sc.stmansi.quickAction.ActionItem;
import com.sc.stmansi.quickAction.QuickAction;
import com.sc.stmansi.recyclerview.ClickListener;
import com.sc.stmansi.recyclerview.RecyclerViewUpdateListener;
import com.sc.stmansi.recyclerview.WomanAdapter;
import com.sc.stmansi.registration.ReconfirmationRegActivity;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.services.ServicesSummaryActivity;
import com.sc.stmansi.services.UnplannedServicesListActivity;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.viewprofile.ViewProfileActivity;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.sc.stmansi.R;

public class PregRegisteredWomanListFrag extends Fragment implements ClickListener, RecyclerViewUpdateListener, IndicatorViewClickListener {

    private TblInstusers user;
    private AQuery aQuery;

    private String wFilterStr;

    private TextView allCountView, noDataLabelView;

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private LinearLayoutManager linearLayoutManager;
    private RecyclerView.Adapter registeredWomanAdapter;
    private List<tblregisteredwomen> filteredRowItems;
    private boolean isUpdating;

    private DatabaseHelper databaseHelper;
    private AppState appState;
    private WomanRepository womanRepository;
    private int nextItemPosition;

    private String womenCondition;
    private long womenCount;

    //31Aug2019 - Bindu
    private static final int ID_WEDIT = 1;
    private static final int ID_SERVLIST = 2;
    private static final int ID_UNPLANNEDSERVLIST = 3;
    private static final int ID_HOMEVISITADD = 4;
    private static final int ID_DEL = 5;//31Oct2019 Arpitha
    QuickAction mQuickAction = null;
    tblregisteredwomen woman;
    private static final int ID_DEACT = 7;//19Nov2019 Arpitha
    public static boolean islmpconfirm = false; //02May2021 Bindu

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        RegisteredWomenActionTabs r = (RegisteredWomenActionTabs) context;
        r.addFragment(this);
    }

    /**
     * Initializes the RegisteredWomenList screen
     * Sets the ejanani_women_list.xml to this class
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_pregregwomanlist, container, false);
    }

    /**
     * Initialize font, database, UsersPojo variables and village spinner
     * calls - populateWomenList
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        aQuery = new AQuery(getActivity());
        try {
            databaseHelper = getHelper();

            Bundle bundle = getActivity().getIntent().getParcelableExtra("globalState");
            appState = bundle.getParcelable("appState");

            UserRepository userRepository = new UserRepository(databaseHelper);
            user = userRepository.getOneAuditedUser(appState.ashaId);

            womanRepository = new WomanRepository(databaseHelper);
            // 30April2021 Guru
            womenCount = getTotalWomenCount(user.getUserId(), 0, "preg", "",
                    false, womanRepository, false);

            wFilterStr = getActivity().getIntent().getStringExtra("wFilterStr");

            Bundle b = getArguments();
            womenCondition = b.getString("womenCondition");

            noDataLabelView = aQuery.id(R.id.tvWomanListLblAllPreg).getTextView();
            allCountView = aQuery.id(R.id.txtcountpreg).getTextView();
            allCountView.setText(getResources().getString(R.string.recordcount, womenCount));

            recyclerView = getView().findViewById(R.id.pregnant_list_recycler_view);
            recyclerView.setVisibility(View.VISIBLE);
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            recyclerView.setHasFixedSize(true);

            // use a linear layout manager
            layoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(layoutManager);
            linearLayoutManager = (LinearLayoutManager) layoutManager;

            filteredRowItems = new ArrayList<>();
            registeredWomanAdapter = new WomanAdapter(filteredRowItems, getActivity(), this);
            recyclerView.setAdapter(registeredWomanAdapter);

            QueryParams params = new QueryParams();
            params.textViewFilter = "";
            params.selectedVillageCode = 0;
            params.nextItemPosition = 0;
            new LoadPregnantList(this, params, true).execute();
            isUpdating = true;

            recyclerView.addOnScrollListener(new OnRecyclerScrollListener());

            //23MAr2021 Bindu - Add Reconfirm LMP
            setQuickActionItems(false);


            //setup the action item click listener
            mQuickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
                @Override
                public void onItemClick(QuickAction quickAction, int pos, int actionId) {
                    ActionItem actionItem = quickAction.getActionItem(pos);
                    try {
                        switch (actionItem.getActionId()) {

                            case ID_WEDIT: {
                                Intent nextScreen = new Intent(getActivity(), ViewProfileActivity.class);
                                nextScreen.putExtra("globalState", getActivity().getIntent().getBundleExtra("globalState"));
                                getActivity().startActivity(nextScreen);
                                mQuickAction.dismiss();
                                break;
                            }
                            case ID_SERVLIST: {
                                Intent nextScreen = new Intent(getActivity(), ServicesSummaryActivity.class);
                                nextScreen.putExtra("globalState", getActivity().getIntent().getBundleExtra("globalState"));
                                getActivity().startActivity(nextScreen);
                                mQuickAction.dismiss();
                                break;
                            }
                            case ID_UNPLANNEDSERVLIST: {
                                Intent nextScreen = new Intent(getActivity(), UnplannedServicesListActivity.class);
                                nextScreen.putExtra("globalState", getActivity().getIntent().getBundleExtra("globalState"));
                                getActivity().startActivity(nextScreen);
                                mQuickAction.dismiss();
                                break;
                            }
                            case ID_HOMEVISITADD: {
                                //18Oct2019 - Bindu - Chk nearing del message
                                WomanRepository womanRepository = new WomanRepository(databaseHelper);
                                woman = womanRepository.getRegistartionDetails(appState.selectedWomanId, appState.sessionUserId);

                                int daysDiff = DateTimeUtil.getDaysBetweenDates(DateTimeUtil.getTodaysDate(), woman.getRegLMP());
                                if (daysDiff > 210) {
                                    displayNearingDelAlertMessage(woman);
                                } else {
                                    Intent nextScreen = new Intent(getActivity(), HomeVisit.class);
                                    nextScreen.putExtra("globalState", getActivity().getIntent().getBundleExtra("globalState"));
                                    getActivity().startActivity(nextScreen);
                                    mQuickAction.dismiss();
                                }
                                break;
                            }
                            case ID_DEL :{
                                WomanRepository womanRepository = new WomanRepository(databaseHelper);
                                woman = womanRepository.getRegistartionDetails(appState.selectedWomanId, appState.sessionUserId);

                                if(woman.getRegPregnantorMother()==2 || DateTimeUtil.getNumberOfDaysFromLMP(woman.getRegLMP())>210) {

                                    Intent nextScreen = new Intent(getActivity(), DeliveryInfoActivity.class);
                                    nextScreen.putExtra("globalState",getActivity().getIntent().getBundleExtra("globalState"));
                                    nextScreen.putExtra("woman", woman);
                                    getActivity().startActivity(nextScreen);
                                    mQuickAction.dismiss();
                                }else
                                {
                                    if(woman.getDateDeactivated()!=null && woman.getDateDeactivated().trim().length()>0)
                                        Toast.makeText(getActivity(), getResources().getString(R.string.userdeactivated), Toast.LENGTH_LONG).show();

                                    else
                                        Toast.makeText(getActivity(), getResources().getString(R.string.m132), Toast.LENGTH_LONG).show();

                                }
                                break;
                            }

                            //                          19Nov2019 Arpitha
                            case ID_DEACT:
                                WomanRepository womanRepository = new WomanRepository(databaseHelper);
                                woman = womanRepository.getRegistartionDetails(appState.selectedWomanId, appState.sessionUserId);
                                Intent deact = new Intent(getActivity(), WomanDeactivateActivity.class);
                                deact.putExtra("globalState", getActivity().getIntent().getBundleExtra("globalState"));
                                deact.putExtra("woman", woman);
                                startActivity(deact);
                                mQuickAction.dismiss();
                                break;
                        }
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                    }
                }
            });
        } catch (SQLException e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private void setQuickActionItems(boolean isLMPConfirmed) {
        String str;
        /*if(!isLMPConfirmed) {
             str = getActivity().getResources().getString(R.string.reconfirmlmp);
            wReconfirmLMP = new ActionItem(ID_WReconfirmLMP, str, ContextCompat.getDrawable(getActivity(), R.drawable.ic_edit_60));
        }*/

        //31Aug2019 - Bindu - ADD items for quick action
        str = getActivity().getResources().getString(R.string.viewprofile);
        ActionItem wEdit = new ActionItem(ID_WEDIT, str, ContextCompat.getDrawable(getActivity(), R.drawable.ic_edit_60));

        str = getActivity().getResources().getString(R.string.services);
        ActionItem wPlannedServList = new ActionItem(ID_SERVLIST, str, ContextCompat.getDrawable(getActivity(), R.drawable.anm_pending_activities));

        str = getActivity().getResources().getString(R.string.unplannedservices);
        ActionItem wUnplannedServList = new ActionItem(ID_UNPLANNEDSERVLIST, str, ContextCompat.getDrawable(getActivity(), R.drawable.anm_pending_activities));

        str = getActivity().getResources().getString(R.string.homevisit);
        ActionItem wHomeVisitAdd = new ActionItem(ID_HOMEVISITADD, str, ContextCompat.getDrawable(getActivity(), R.drawable.ic_homevisit));

        str = getActivity().getResources().getString(R.string.deliveryHeading);
        ActionItem wDel = new ActionItem(ID_DEL, str, ContextCompat.getDrawable(getActivity(),R.drawable.delivery_info));

        //          19Nov2019 Arpitha
        str = getActivity().getResources().getString(R.string.deactivate);
        ActionItem wDeact = new ActionItem(ID_DEACT, str, ContextCompat.getDrawable(getActivity(),R.drawable.deactivate));

        mQuickAction = new QuickAction(getActivity());
        /*if(!isLMPConfirmed) {
            mQuickAction.addActionItem(wReconfirmLMP); // 23Mar2021 Bindu
        }*/

        mQuickAction.addActionItem(wEdit);
        mQuickAction.addActionItem(wPlannedServList);
        mQuickAction.addActionItem(wUnplannedServList);
        mQuickAction.addActionItem(wHomeVisitAdd);
        mQuickAction.addActionItem(wDel);
        mQuickAction.addActionItem(wDeact);//19Nov2019 Arpitha
    }

    //    initialize db object
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(getActivity(), DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void updateList(String
                                     villageTitle, Map<String, Integer> villageNameForCode, String textViewFilter, boolean checked) {
        try {
            QueryParams queryParams = new QueryParams();
            queryParams.nextItemPosition = 0;
            queryParams.selectedVillageTitle = villageTitle;
            queryParams.textViewFilter = textViewFilter;
            queryParams.isLMPNotConfirmed = checked;
            islmpconfirm = checked; //02May2021 Bindu - set LMP to retrieve in onscroll
            this.wFilterStr = textViewFilter;

            new LoadPregnantList(this, queryParams, true, villageNameForCode).execute();
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    //    load list data
    static class LoadPregnantList extends AsyncTask<Void, Void, Void> {

        private PregRegisteredWomanListFrag viewsUpdateCallback;
        private QueryParams queryParams;
        private boolean firstLoad;
        private Map<String, Integer> villageCodeForName;

        LoadPregnantList(PregRegisteredWomanListFrag viewsUpdateCallback, QueryParams queryParams, boolean firstLoad, Map<String, Integer> villageCodeForName) {
            this.viewsUpdateCallback = viewsUpdateCallback;
            this.queryParams = queryParams;
            this.firstLoad = firstLoad;
            this.villageCodeForName = villageCodeForName;
        }

        LoadPregnantList(PregRegisteredWomanListFrag viewsUpdateCallback, QueryParams queryParams, boolean firstLoad) {
            this.viewsUpdateCallback = viewsUpdateCallback;
            this.queryParams = queryParams;
            this.firstLoad = firstLoad;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            int selectedCode;
            // This means second constructor was used (called by scrollListener)
            if (villageCodeForName == null)
                selectedCode = queryParams.selectedVillageCode;
            else {
                String selectedVillageTitle = queryParams.selectedVillageTitle;
                if (selectedVillageTitle == null)
                    selectedCode = 0;
                else {
                    Integer code = villageCodeForName.get(selectedVillageTitle);
                    selectedCode = (code == null) ? 0 : code;
                }
            }
            viewsUpdateCallback.appState.selectedVillageCode = selectedCode; //10Dec2019 - Bindu
            try {
                viewsUpdateCallback.womenCount = viewsUpdateCallback.getTotalWomenCount(viewsUpdateCallback.user.getUserId(),
                        selectedCode, viewsUpdateCallback.womenCondition, queryParams.textViewFilter,
                        islmpconfirm, viewsUpdateCallback.womanRepository,false);// 30April2021 Guru
                List<tblregisteredwomen> rows = viewsUpdateCallback.prepareRowItemsForDisplay(queryParams.nextItemPosition,
                        selectedCode, queryParams.textViewFilter, queryParams.isLMPNotConfirmed); //25Mar2021 Bindu - add lmp not confirmed
                if (firstLoad) {
                    int prevSize = viewsUpdateCallback.filteredRowItems.size();
                    viewsUpdateCallback.filteredRowItems.clear();
                    viewsUpdateCallback.registeredWomanAdapter.notifyItemRangeRemoved(0, prevSize);
                    firstLoad = false;
                }
                if (rows.size() > 0) {
                    viewsUpdateCallback.filteredRowItems.addAll(rows);

                    viewsUpdateCallback.nextItemPosition = viewsUpdateCallback.filteredRowItems.size();
                    viewsUpdateCallback.registeredWomanAdapter.notifyItemRangeInserted(firstLoad ? 0 : viewsUpdateCallback.nextItemPosition,
                            viewsUpdateCallback.filteredRowItems.size());
                }
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            try {
                viewsUpdateCallback.updateLabels();
                // 30April2021 Guru
                if (viewsUpdateCallback.filteredRowItems.size() > 0) {
                    viewsUpdateCallback.recyclerView.setVisibility(View.VISIBLE);
                    if (!firstLoad) {
                        viewsUpdateCallback.layoutManager.scrollToPosition(viewsUpdateCallback.registeredWomanAdapter.getItemCount());
                    }
                    viewsUpdateCallback.isUpdating = false;
                } // 30April2021 Guru
                viewsUpdateCallback.allCountView.setText(viewsUpdateCallback.getResources().
                        getString(R.string.recordcount,
                                viewsUpdateCallback.womenCount)); // 30April2021 Guru
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

            }
        }
    }

    //    update labels based on data
    private void updateLabels() {
        if (filteredRowItems.size() == 0) {
            recyclerView.setVisibility(View.GONE);

            noDataLabelView.setText(getResources().getString(R.string.no_data));
            noDataLabelView.setVisibility(View.VISIBLE);
            return;
        }
        noDataLabelView.setVisibility(View.GONE);
    }

    /**
     * get the Display pojo list from the given womanIds
     *  @param limitStart
     * @param villageCode
     * @param filter
     * @param isLMPNotConfirmed
     */
    private List<tblregisteredwomen> prepareRowItemsForDisplay(int limitStart,
                                                               int villageCode, String filter, boolean isLMPNotConfirmed) throws Exception {
        return womanRepository.getDisplayWomanList(user.getUserId(),
                villageCode, womenCondition, filter, limitStart, isLMPNotConfirmed); //25Mar2021 Bindu add lmp not confirmed
    }

    @Override
    public void onClick(View v) {
        setSelectedWomanId(v);

        /*Intent intent = new Intent(getActivity(), ServicesSummaryActivity.class);
        intent.putExtra("globalState", getActivity().getIntent().getBundleExtra("globalState"));
        startActivity(intent);*/
        //02Apr2021 Bindu check LMP confirmtion
        /*try {
            WomanRepository womanRepository = new WomanRepository(databaseHelper);
            boolean isLMPConfirmed = womanRepository.isWomanLMPConfirmed(appState.selectedWomanId);
          //  setQuickActionItems(isLMPConfirmed);
            *//*if(!isLMPConfirmed)
               // mQuickAction.remove(wReconfirmLMP);
                mQuickAction.addActionItem(wReconfirmLMP);*//*
        }catch (Exception e){
            e.printStackTrace();
        }*/
        mQuickAction.show(v);
    }

    @Override
    public boolean onLongClick(View v) {
        setSelectedWomanId(v);

        Intent intent = new Intent(getActivity(), ViewProfileActivity.class);
        intent.putExtra("globalState", getActivity().getIntent().getBundleExtra("globalState"));
        startActivity(intent);
        return true;
    }

    //    set woman id
    private void setSelectedWomanId(View v) {
        try {
            int itemPosition = recyclerView.getChildLayoutPosition(v);
            woman = filteredRowItems.get(itemPosition);

            appState.selectedWomanId = woman.getWomanId();


        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    //    prepare bundle
    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        return b;
    }

    //    recycler view scroll
    class OnRecyclerScrollListener extends RecyclerView.OnScrollListener {
        int ydy = 0;

        @Override
        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            // 30April2021 Guru
            if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                int lastCompletelyVisibleItemPosition = linearLayoutManager.findLastCompletelyVisibleItemPosition();
                boolean shouldPullUpRefresh = lastCompletelyVisibleItemPosition == filteredRowItems.size() - 1
                        && recyclerView.getScrollState() == RecyclerView.SCROLL_STATE_DRAGGING;
                if (shouldPullUpRefresh) {
                    QueryParams queryParams = new QueryParams();
                    queryParams.nextItemPosition = nextItemPosition;
                    queryParams.selectedVillageCode = appState.selectedVillageCode;
                    queryParams.textViewFilter = wFilterStr;
                    queryParams.isLMPNotConfirmed = islmpconfirm; //02MAy2021 Bindu set isLMPconfirmation
                    if (!isUpdating) {
                        isUpdating = true;
                        new LoadPregnantList(PregRegisteredWomanListFrag.this, queryParams, false).execute();
                    }
                }
            }  // 30April2021 Guru
        }

        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            ydy = dy;

            int lastCompletelyVisibleItemPosition = linearLayoutManager.findLastCompletelyVisibleItemPosition();
            boolean shouldPullUpRefresh = lastCompletelyVisibleItemPosition == filteredRowItems.size() - 1
                    && recyclerView.getScrollState() == RecyclerView.SCROLL_STATE_DRAGGING;
            if (shouldPullUpRefresh) {
                QueryParams queryParams = new QueryParams();
                queryParams.nextItemPosition = nextItemPosition;
                queryParams.selectedVillageCode = appState.selectedVillageCode;
                queryParams.textViewFilter = wFilterStr;
                queryParams.isLMPNotConfirmed = islmpconfirm; //02MAy2021 Bindu set isLMPconfirmation
                if (!isUpdating) {
                    isUpdating = true;
                    new LoadPregnantList(PregRegisteredWomanListFrag.this, queryParams, false).execute();
                }
            }
        }
    }

    @Override
    public void displayAlert(int selectedItemPosition) {
        try {
            ArrayList<String> compList = new ArrayList<>();
            String complication;

            if(filteredRowItems.get(selectedItemPosition).getRegCCConfirmRisk()!=null &&
                    filteredRowItems.get(selectedItemPosition).getRegCCConfirmRisk().trim().length()>0
                    &&  filteredRowItems.get(selectedItemPosition)
                    .getRegCCConfirmRisk().equalsIgnoreCase("Y"))
                complication = getResources().getString(R.string.riskidentifiedbycc)+"\n"+
                        filteredRowItems.get(selectedItemPosition)
                                .getRegRiskFactorsByCC();
//                        +" "
//                        +filteredRowItems.get(selectedItemPosition).getRegOtherRiskByCC();
            else
                complication = filteredRowItems.get(selectedItemPosition).getRegriskFactors();
            if (complication != null && complication.length() > 0 ||
                    (filteredRowItems.get(selectedItemPosition).getRegRiskFactorsByCC()!=null && filteredRowItems.get(selectedItemPosition).getRegRiskFactorsByCC().trim().length()>0)) {
                compList = new ArrayList<>();
                compList.add(complication.replace(",", ", ")
                        .replace("PH:", "\nPH:").replace(", ,","")
                        .replace(",,",""));


                ArrayList<String> complListDisp = new ArrayList<>();
                for (int i = 0; i < compList.size(); i++) {
                    String compl[] = compList.get(i).split("[,\n]");
                    for (int j = 0; j < compl.length; j++) {
                        String strComp = compl[j];
                        if (strComp.contains("CH:"))
                            strComp = strComp.replaceAll("CH:", "");
                        if (strComp.contains("PH:"))
                            strComp = strComp.replaceAll("PH:", "");

                        strComp = strComp.replaceAll(" ", "");
                        if (strComp.contains("ormoreconsecutivespontaneousabortions"))
//                            strComp = strComp.split("[<>0123456789(]")[1];
                            strComp = strComp.split("[3<>(]")[1];
                        else
                            strComp = strComp.split("[<>(]")[0];
                        //strComp = strComp.split("[<>0123456789(]")[0];
                        strComp = strComp.replaceAll("/","");
                        strComp = strComp.toLowerCase().trim();
                        int identifier = getResources().getIdentifier
                                (strComp,
                                        "string", "com.sc.stmansi");
                        if (identifier > 0) {
                            if (compl[j].contains("CH:"))
                                complListDisp.add("CH: " + getResources().getString(identifier));
                            else if (compl[j].contains("PH:"))
                                complListDisp.add("PH: " + getResources().getString(identifier));
                            else
                                complListDisp.add(getResources().getString(identifier));

                        } else {

                            complListDisp.add(compl[j]);

                        }
                    }

                }

                ArrayList<String> complications = new ArrayList<>();
                String strStrings = "";
                for (int i = 0; i < complListDisp.size(); i++) {
                    if (i != 0)
                        strStrings = strStrings + ", " + complListDisp.get(i);
                    else
                        strStrings = complListDisp.get(i);
                }

                strStrings = strStrings.replace(", ,","")
                        .replace(",,","");

                String[] list = strStrings.split("PH:");

                ArrayList<String> l = new ArrayList<>();
                for (int i = 0; i < list.length; i++) {
                    if(!list[i].equalsIgnoreCase(", ")) {
                        if (i == 0)
                            l.add(list[i]);
                        else
                            l.add("\nPH: " + list[i]);
                    }
                }




                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                View convertView = LayoutInflater.from(getActivity()).inflate(R.layout.complicated_reasons_list, null, false);

                ListView lv = convertView.findViewById(R.id.lvCompliatedReasonslist);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.listitem, l);

                alertDialog.setView(convertView).setCancelable(true).setTitle((this.getResources().getString(R.string.compl_reasons)))
                        .setPositiveButton((this.getResources().getString(R.string.ok)), new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                lv.setAdapter(adapter);
                alertDialog.show();
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    @Override
    public void displayHomeVisitList(int selectedItemPosition) {
        tblregisteredwomen woman = filteredRowItems.get(selectedItemPosition);
        Intent homevisitlist = new Intent(getActivity(), HomeVisitListActivity.class);
        appState.selectedWomanId = woman.getWomanId();
        homevisitlist.putExtra("globalState", prepareBundle());
        startActivity(homevisitlist);
    }

    @Override
    public void displayDelComplicationAlert(int selectedItemPosition) {

    }

    //18Oct2019 - Bindu - Alert message nearing del
    public  void displayNearingDelAlertMessage(tblregisteredwomen woman) throws Exception{
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

        String alertmsg =  woman.getRegWomanName() + ", " + " EDD : " + woman.getRegEDD() ;
        //alertDialogBuilder.setMessage(message).setCancelable(false).setTitle(Html.fromHtml("<font color='#FF7F27'>Alert -- </font><br><br><p>"+alertmsg + "</p>")).setNegativeButton(
        String msg = "<font color='#CC3333'> Nearing Delivery - </font>"+alertmsg;
        alertDialogBuilder.setMessage(getResources().getText(R.string.alerthomevisitnearingdelmsg)).setCancelable(false).setTitle(Html.fromHtml(msg)).setNegativeButton(
                getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent nextScreen = new Intent(getActivity(), HomeVisit.class);
                        nextScreen.putExtra("globalState",getActivity().getIntent().getBundleExtra("globalState"));
                        nextScreen.putExtra("woman", PregRegisteredWomanListFrag.this.woman);
                        getActivity().startActivity(nextScreen);
                        mQuickAction.dismiss();
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void displayLMPConfirmation(int selectedItemPosition) {
        tblregisteredwomen woman = filteredRowItems.get(selectedItemPosition);
        Intent nextScreen = new Intent(getActivity(), ReconfirmationRegActivity.class);
        appState.selectedWomanId = woman.getWomanId();
        nextScreen.putExtra("globalState", getActivity().getIntent().getBundleExtra("globalState"));
        getActivity().startActivity(nextScreen);
    }
}
