package com.sc.stmansi.womanlist;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.google.firebase.crashlytics.FirebaseCrashlytics;

public class TabsPagerAdapter extends FragmentPagerAdapter {

    FragmentManager fm;
    public TabsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int index) {
        try {
            switch (index) {
                case 0:
                    AllRegisteredWomanListFrag f1 = new AllRegisteredWomanListFrag();
                    Bundle b1 = new Bundle();
                    b1.remove("womenCondition");
                    b1.putString("womenCondition", "all");
                    f1.setArguments(b1);
                    return f1;
                case 1:
                    PregRegisteredWomanListFrag f2 = new PregRegisteredWomanListFrag();
                    Bundle b2 = new Bundle();
                    b2.remove("womenCondition");
                    b2.putString("womenCondition", "preg");
                    f2.setArguments(b2);
                    return f2;
                case 2:
                    MotherRegisteredWomanListFrag f3 = new MotherRegisteredWomanListFrag();
                    Bundle b3 = new Bundle();
                    b3.remove("womenCondition");
                    b3.putString("womenCondition", "Mother");
                    f3.setArguments(b3);
                    return f3;
            }
        } catch (Exception e) {
            
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }

        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

}
