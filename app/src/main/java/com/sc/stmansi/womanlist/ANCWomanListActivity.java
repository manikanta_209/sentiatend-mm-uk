package com.sc.stmansi.womanlist;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.About.AboutActivity;
import com.sc.stmansi.HomeVisit.HomeVisit;
import com.sc.stmansi.HomeVisit.HomeVisitListActivity;
import com.sc.stmansi.HomeVisit.PncHomeVisit;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.deactivate.WomanDeactivateActivity;
import com.sc.stmansi.delivery.DeliveryInfoActivity;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.pendingservices.IndicatorViewClickListener;
import com.sc.stmansi.quickAction.ActionItem;
import com.sc.stmansi.quickAction.QuickAction;
import com.sc.stmansi.recyclerview.ClickListener;
import com.sc.stmansi.recyclerview.RecyclerViewUpdateListener;
import com.sc.stmansi.recyclerview.WomanAdapter;
import com.sc.stmansi.registration.ReconfirmationRegActivity;
import com.sc.stmansi.repositories.FacilityRepository;
import com.sc.stmansi.repositories.LoginRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.services.ServicesSummaryActivity;
import com.sc.stmansi.services.UnplannedServicesListActivity;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.viewprofile.ViewProfileActivity;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.sc.stmansi.R;

public class ANCWomanListActivity extends AppCompatActivity implements
        ClickListener, RecyclerViewUpdateListener,
        IndicatorViewClickListener,AdapterView.OnItemSelectedListener {

    private TblInstusers user;
    private AQuery aQuery;

    private String wFilterStr;

    private TextView allCountView, noDataLabelView;

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private LinearLayoutManager linearLayoutManager;
    private RecyclerView.Adapter registeredWomanAdapter;
    private List<tblregisteredwomen> filteredRowItems;
    private boolean isUpdating;

    private DatabaseHelper databaseHelper;
    private AppState appState;
    private WomanRepository womanRepository;
    private int nextItemPosition;

    private String womenCondition;
    private long womenCount;

    //31Aug2019 - Bindu
    private static final int ID_WEDIT = 1;
    private static final int ID_SERVLIST = 2;
    private static final int ID_UNPLANNEDSERVLIST = 3;
    private static final int ID_HOMEVISITADD = 4;
    private static final int ID_DEL = 5;//31Oct2019 Arpitha
    QuickAction mQuickAction = null;
    tblregisteredwomen woman;
    private Spinner villageSpinner;
    private Map<String, Integer> villageCodeForName;
    private boolean spinnerSelectionChanged = false;
    private static final int ID_DEACT = 7;//19Nov2019 Arpitha
    public static boolean islmpconfirm = false; //02May2021 Bindu

    /**
     * Initialize font, database, UsersPojo variables and village spinner
     * calls - populateWomenList
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_womanlist);
        aQuery = new AQuery(this);
        try {
            databaseHelper = getHelper();

            Bundle bundle = this.getIntent().getParcelableExtra("globalState");
            appState = bundle.getParcelable("appState");

            UserRepository userRepository = new UserRepository(databaseHelper);
            user = userRepository.getOneAuditedUser(appState.ashaId);

            womanRepository = new WomanRepository(databaseHelper);
//            womenCount = womanRepository.getRegisteredWomenCount("preg", appState.sessionUserId);Arpitha


            wFilterStr = this.getIntent().getStringExtra("wFilterStr");

            Bundle b = getIntent().getBundleExtra("globalState");
            womenCondition = "preg";

            // 30April2021 Guru & Arpitha
            womenCount = getTotalWomenCount(user.getUserId(),
                    0, womenCondition, "", false, womanRepository,false);

            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

            aQuery.id(R.id.imgresetfilter).getImageView().setBackgroundResource(R.drawable.ic_reset_gray);//04Dec2019 Arpitha

            //08Apr2021 Bindu LMP not confirmed
            aQuery.id(R.id.llfilter).visible();
            aQuery.id(R.id.rdgrpoptions).gone();
            aQuery.id(R.id.view).gone();
            aQuery.id(R.id.chklmpnotconfirmed).visible();

//25Mar2021 - Bindu - Oncheck Change listener

            CheckBox chklmpnotconfirmed = findViewById(R.id.chklmpnotconfirmed);
            chklmpnotconfirmed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr, aQuery.id(R.id.chklmpnotconfirmed).isChecked());
                }
            });

            //22Jul2019- Bindu
            getSupportActionBar().setCustomView(R.layout.ejanani_village_filter);
            getSupportActionBar().setDisplayShowCustomEnabled(true);

            villageSpinner = aQuery.id(R.id.spVillageFilter).getSpinner();
            villageSpinner.setOnItemSelectedListener(this);
            populateSpinnerVillage();

            aQuery.id(R.id.txtlabell).text(getResources().getString(R.string.anc));


            noDataLabelView = aQuery.id(R.id.txtnodata).getTextView();
            allCountView = aQuery.id(R.id.txtcountwl).getTextView();
            allCountView.setText(getResources().getString(R.string.recordcount, womenCount));

            recyclerView = findViewById(R.id.hrp_women_recycler_view);
            recyclerView.setVisibility(View.VISIBLE);
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            recyclerView.setHasFixedSize(true);

            // use a linear layout manager
            layoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(layoutManager);
            linearLayoutManager = (LinearLayoutManager) layoutManager;

            filteredRowItems = new ArrayList<>();
            registeredWomanAdapter = new WomanAdapter(filteredRowItems, this);
            recyclerView.setAdapter(registeredWomanAdapter);

            QueryParams params = new QueryParams();
            params.textViewFilter = "";
            params.selectedVillageCode = 0;
            params.nextItemPosition = 0;
            new LoadPregnantList(this, params, true).execute();
            isUpdating = true;

            recyclerView.addOnScrollListener(new OnRecyclerScrollListener());

            //31Aug2019 - Bindu - ADD items for quick action
            String str = this.getResources().getString(R.string.viewprofile);
            ActionItem wEdit = new ActionItem(ID_WEDIT, str, ContextCompat.getDrawable(this, R.drawable.ic_edit_60));

            str = this.getResources().getString(R.string.services);
            ActionItem wPlannedServList = new ActionItem(ID_SERVLIST, str, ContextCompat.getDrawable(this, R.drawable.anm_pending_activities));

            str = this.getResources().getString(R.string.unplannedservices);
            ActionItem wUnplannedServList = new ActionItem(ID_UNPLANNEDSERVLIST, str, ContextCompat.getDrawable(this, R.drawable.anm_pending_activities));

            str = this.getResources().getString(R.string.homevisit);
            ActionItem wHomeVisitAdd = new ActionItem(ID_HOMEVISITADD, str, ContextCompat.getDrawable(this, R.drawable.ic_homevisit));

            str = this.getResources().getString(R.string.deliveryHeading);
            ActionItem wDel = new ActionItem(ID_DEL, str, ContextCompat.getDrawable(this, R.drawable.delivery_info));

            //          19Nov2019 Arpitha
            str = getResources().getString(R.string.deactivate);
            ActionItem wDeact = new ActionItem(ID_DEACT, str, ContextCompat.getDrawable(this,R.drawable.deactivate));


            mQuickAction = new QuickAction(this);
            mQuickAction.addActionItem(wEdit);
            mQuickAction.addActionItem(wPlannedServList);
            mQuickAction.addActionItem(wUnplannedServList);
            mQuickAction.addActionItem(wHomeVisitAdd);
            mQuickAction.addActionItem(wDel);
            mQuickAction.addActionItem(wDeact);//19Nov2019 Arpitha

            //setup the action item click listener
            mQuickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
                @Override
                public void onItemClick(QuickAction quickAction, int pos, int actionId) {
                    ActionItem actionItem = quickAction.getActionItem(pos);
                    try {
                        switch (actionItem.getActionId()) {

                            case ID_WEDIT: {
                                Intent nextScreen = new Intent(ANCWomanListActivity.this, ViewProfileActivity.class);
                                nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                startActivity(nextScreen);
                                mQuickAction.dismiss();
                                break;
                            }
                            case ID_SERVLIST: {
                                Intent nextScreen = new Intent(ANCWomanListActivity.this, ServicesSummaryActivity.class);
                                nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                startActivity(nextScreen);
                                mQuickAction.dismiss();
                                break;
                            }
                            case ID_UNPLANNEDSERVLIST: {
                                Intent nextScreen = new Intent(ANCWomanListActivity.this, UnplannedServicesListActivity.class);
                                nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                startActivity(nextScreen);
                                mQuickAction.dismiss();
                                break;
                            }
                            case ID_HOMEVISITADD: {
                                //18Oct2019 - Bindu - Chk nearing del message
                                WomanRepository womanRepository = new WomanRepository(databaseHelper); //11Nov2019 - Bindu - Add Woman repo and get woman details
                                woman = womanRepository.getRegistartionDetails(appState.selectedWomanId, appState.sessionUserId);

                                if(woman.getRegPregnantorMother() == 2 ){ //29Nov2019 - Bindu - Navigate to Home visit item
                                    Intent nextScreen = new Intent(ANCWomanListActivity.this, PncHomeVisit.class);
                                    nextScreen.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                                    startActivity(nextScreen);
                                } else {
                                    int daysDiff = DateTimeUtil.getDaysBetweenDates(DateTimeUtil.getTodaysDate(), woman.getRegLMP());
                                    if (daysDiff > 210) {
                                        displayNearingDelAlertMessage(woman);
                                    } else {
                                        Intent nextScreen = new Intent(ANCWomanListActivity.this, HomeVisit.class);
                                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                        startActivity(nextScreen);
                                        mQuickAction.dismiss();
                                    }
                                }
                                break;
                            }
                            case ID_DEL: {
                                WomanRepository womanRepository = new WomanRepository(databaseHelper);
                                woman = womanRepository.getRegistartionDetails(appState.selectedWomanId, appState.sessionUserId);

                                if (woman.getRegPregnantorMother() == 2 || DateTimeUtil.getNumberOfDaysFromLMP(woman.getRegLMP()) > 210) {

                                    Intent nextScreen = new Intent(ANCWomanListActivity.this, DeliveryInfoActivity.class);
                                    nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                    nextScreen.putExtra("woman", woman);
                                    startActivity(nextScreen);
                                    mQuickAction.dismiss();
                                } else
                                {
                                   // if(woman.getDateDeactivated()!=null && woman.getDateDeactivated().trim().length()>0)
                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.m132), Toast.LENGTH_LONG).show();

                                    /*else
                                    */   // Toast.makeText(getApplicationContext(), getResources().getString(R.string.userdeactivated), Toast.LENGTH_LONG).show();

                                }
                                break;
                            }//                          19Nov2019 Arpitha
                            case ID_DEACT:
                                WomanRepository womanRepository = new WomanRepository(databaseHelper);
                                woman = womanRepository.getRegistartionDetails(appState.selectedWomanId, appState.sessionUserId);
                                Intent deact = new Intent(ANCWomanListActivity.this, WomanDeactivateActivity.class);
                                deact.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                deact.putExtra("woman", woman);
                                startActivity(deact);
                                mQuickAction.dismiss();
                                break;
                        }
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                    }
                }
            });

            aQuery.id(R.id.imgresetfilter).getImageView().setOnClickListener(new FilterResetListener());

            EditText filterTextView = aQuery.id(R.id.etWomanFilter).getEditText();
           /* filterTextView.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    aQuery.id(R.id.imgresetfilter).getImageView().setBackgroundResource(R.drawable.ic_reset);
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });*/
            filterTextView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (v.getText() != null) {
                        aQuery.id(R.id.imgresetfilter).getImageView().setBackgroundResource(R.drawable.ic_reset); //10Dec2019 - Bindu
                        wFilterStr = v.getText().toString();
                        updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr, aQuery.id(R.id.chklmpnotconfirmed).isChecked()); //03MAy2021 Bindu check islmp condition
                    }
                    return true;
                }
            });



        } catch (SQLException e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    //    initialize db object
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void updateList(String
                                     villageTitle, Map<String, Integer> villageNameForCode, String textViewFilter, boolean checked) {
        try {
            QueryParams queryParams = new QueryParams();
            queryParams.nextItemPosition = 0;
            queryParams.selectedVillageTitle = villageTitle;
            queryParams.textViewFilter = textViewFilter;
            queryParams.isLMPNotConfirmed = checked; //08Apr2021 Bindu - chklmpconfirmation
            islmpconfirm = checked; //02May2021 Bindu - set LMP to retrieve in onscroll
            this.wFilterStr = textViewFilter;

            new LoadPregnantList(this, queryParams, true, villageNameForCode).execute();
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    //    load list data
  /*  static class LoadPregnantList extends AsyncTask<Void, Void, Void> {

        private ANCWomanListActivity viewsUpdateCallback;
        private QueryParams queryParams;
        private boolean firstLoad;
        private Map<String, Integer> villageCodeForName;

        LoadPregnantList(ANCWomanListActivity viewsUpdateCallback, QueryParams queryParams, boolean firstLoad, Map<String, Integer> villageCodeForName) {
            this.viewsUpdateCallback = viewsUpdateCallback;
            this.queryParams = queryParams;
            this.firstLoad = firstLoad;
            this.villageCodeForName = villageCodeForName;
        }

        LoadPregnantList(ANCWomanListActivity viewsUpdateCallback, QueryParams queryParams, boolean firstLoad) {
            this.viewsUpdateCallback = viewsUpdateCallback;
            this.queryParams = queryParams;
            this.firstLoad = firstLoad;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            int selectedCode;
            // This means second constructor was used (called by scrollListener)
            if (villageCodeForName == null)
                selectedCode = queryParams.selectedVillageCode;
            else {
                String selectedVillageTitle = queryParams.selectedVillageTitle;
                if (selectedVillageTitle == null)
                    selectedCode = 0;
                else {
                    Integer code = villageCodeForName.get(selectedVillageTitle);
                    selectedCode = (code == null) ? 0 : code;
                }
            }
            try {
                viewsUpdateCallback.womenCount = viewsUpdateCallback.getTotalWomenCount(viewsUpdateCallback.user.getUserId(),
                        selectedCode, viewsUpdateCallback.womenCondition, queryParams.textViewFilter,
                        queryParams.isLMPNotConfirmed, viewsUpdateCallback.womanRepository,false); // 30April2021 Guru & Arpitha
                List<tblregisteredwomen> rows = viewsUpdateCallback.prepareRowItemsForDisplay(queryParams.nextItemPosition,
                        selectedCode, queryParams.textViewFilter, queryParams.isLMPNotConfirmed); //25MAr2021 Bindu add lmp not confirmed
                if (firstLoad) {
                    int prevSize = viewsUpdateCallback.filteredRowItems.size();
                    viewsUpdateCallback.filteredRowItems.clear();
                    viewsUpdateCallback.registeredWomanAdapter.notifyItemRangeRemoved(0, prevSize);
                    firstLoad = false;
                }
                if (rows.size() > 0) {
                    viewsUpdateCallback.filteredRowItems.addAll(rows);

                    viewsUpdateCallback.nextItemPosition = viewsUpdateCallback.filteredRowItems.size();
                    viewsUpdateCallback.registeredWomanAdapter.notifyItemRangeInserted(firstLoad ? 0 : viewsUpdateCallback.nextItemPosition,
                            viewsUpdateCallback.filteredRowItems.size());
                }
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            try {
                viewsUpdateCallback.updateLabels();
                // 30April2021 Guru & Arpitha
                if (viewsUpdateCallback.filteredRowItems.size() > 0) {
                    viewsUpdateCallback.recyclerView.setVisibility(View.VISIBLE);
                    if (!firstLoad) {
                        viewsUpdateCallback.layoutManager.scrollToPosition(viewsUpdateCallback.registeredWomanAdapter.getItemCount());
                    }
                    viewsUpdateCallback.isUpdating = false;
                } // 30April2021 Guru & Arpitha
                viewsUpdateCallback.allCountView.setText(viewsUpdateCallback.getResources().
                        getString(R.string.recordcount,
                                viewsUpdateCallback.womenCount)); // 30April2021 Guru & Arpitha
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

            }
        }
    }*/

    //    update labels based on data
    private void updateLabels() {
        if (filteredRowItems.size() == 0) {
            recyclerView.setVisibility(View.GONE);

            noDataLabelView.setText(getResources().getString(R.string.no_data));
            noDataLabelView.setVisibility(View.VISIBLE);
            return;
        }
        noDataLabelView.setVisibility(View.GONE);
    }

    /**
     * get the Display pojo list from the given womanIds
     *  @param limitStart
     * @param villageCode
     * @param filter
     * @param isLMPNotConfirmed
     */
    private List<tblregisteredwomen> prepareRowItemsForDisplay(int limitStart,
                                                               int villageCode, String filter, boolean isLMPNotConfirmed) throws Exception {
        return womanRepository.getDisplayWomanList(user.getUserId(),
                villageCode, womenCondition, filter, limitStart, isLMPNotConfirmed);
    }

    @Override
    public void onClick(View v) {
        setSelectedWomanId(v);

        /*Intent intent = new Intent(this, ServicesSummaryActivity.class);
        intent.putExtra("globalState", this.getIntent().getBundleExtra("globalState"));
        startActivity(intent);*/
        /*try {
            WomanRepository womanRepository = new WomanRepository(databaseHelper);
            boolean isLMPConfirmed = womanRepository.isWomanLMPConfirmed(appState.selectedWomanId);
            //  setQuickActionItems(isLMPConfirmed);
            if(isLMPConfirmed)
               mQuickAction.remove(wReconfirmLMP);
        }catch (Exception e){
            e.printStackTrace();
        }*/
        mQuickAction.show(v);
    }

    @Override
    public boolean onLongClick(View v) {
        setSelectedWomanId(v);

        Intent intent = new Intent(this, ViewProfileActivity.class);
        intent.putExtra("globalState", this.getIntent().getBundleExtra("globalState"));
        startActivity(intent);
        return true;
    }

    //    set woman id
    private void setSelectedWomanId(View v) {
        int itemPosition = recyclerView.getChildLayoutPosition(v);
        woman = filteredRowItems.get(itemPosition);

        appState.selectedWomanId = woman.getWomanId();
    }

    //    prepare bundle
    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        return b;
    }

    //    recycler view scroll
    public class OnRecyclerScrollListener extends RecyclerView.OnScrollListener {
        int ydy = 0;

        @Override
        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            // 30April2021 Guru
            if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                int lastCompletelyVisibleItemPosition = linearLayoutManager.findLastCompletelyVisibleItemPosition();
                boolean shouldPullUpRefresh = lastCompletelyVisibleItemPosition == filteredRowItems.size() - 1
                        && recyclerView.getScrollState() == RecyclerView.SCROLL_STATE_DRAGGING;
                if (shouldPullUpRefresh) {
                    QueryParams queryParams = new QueryParams();
                    queryParams.nextItemPosition = nextItemPosition;
                    queryParams.selectedVillageCode = appState.selectedVillageCode;
                    queryParams.textViewFilter = wFilterStr;
                    queryParams.isLMPNotConfirmed = islmpconfirm; //02MAy2021 Bindu set isLMPconfirmation
                    if (!isUpdating) {
                        isUpdating = true;
                        new LoadPregnantList(ANCWomanListActivity.this, queryParams, false).execute();
                    }
                }
            } // 30April2021 Guru
        }

        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            ydy = dy;

            int lastCompletelyVisibleItemPosition = linearLayoutManager.findLastCompletelyVisibleItemPosition();
            boolean shouldPullUpRefresh = lastCompletelyVisibleItemPosition == filteredRowItems.size() - 1
                    && recyclerView.getScrollState() == RecyclerView.SCROLL_STATE_DRAGGING;
            if (shouldPullUpRefresh) {
                QueryParams queryParams = new QueryParams();
                queryParams.nextItemPosition = nextItemPosition;
                queryParams.selectedVillageCode = appState.selectedVillageCode;
                queryParams.textViewFilter = wFilterStr;
                queryParams.isLMPNotConfirmed = islmpconfirm; //02MAy2021 Bindu set isLMPconfirmation
                if (!isUpdating) {
                    isUpdating = true;
                    new LoadPregnantList(ANCWomanListActivity.this, queryParams, false).execute();
                }
            }
        }
    }

    //    recycler view scroll
   /* class OnRecyclerScrollListener extends RecyclerView.OnScrollListener {
        int ydy = 0;

        @Override
        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            // 30April2021 Guru & Arpitha
            if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                int lastCompletelyVisibleItemPosition = linearLayoutManager.findLastCompletelyVisibleItemPosition();
                boolean shouldPullUpRefresh = lastCompletelyVisibleItemPosition == filteredRowItems.size() - 1
                        && recyclerView.getScrollState() == RecyclerView.SCROLL_STATE_DRAGGING;
                if (shouldPullUpRefresh) {
                    QueryParams queryParams = new QueryParams();
                    queryParams.nextItemPosition = nextItemPosition;
                    queryParams.selectedVillageCode = appState.selectedVillageCode;
                    queryParams.textViewFilter = wFilterStr;
                    if (!isUpdating) {
                        isUpdating = true;
                        new LoadPregnantList(ANCWomanListActivity.this, queryParams, false).execute();
                    }
                }
            } // 30April2021 Guru & Arpitha
        }

        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            ydy = dy;

            int lastCompletelyVisibleItemPosition = linearLayoutManager.findLastCompletelyVisibleItemPosition();
            boolean shouldPullUpRefresh = lastCompletelyVisibleItemPosition == filteredRowItems.size() - 1
                    && recyclerView.getScrollState() == RecyclerView.SCROLL_STATE_DRAGGING;
            if (shouldPullUpRefresh) {
                QueryParams queryParams = new QueryParams();
                queryParams.nextItemPosition = nextItemPosition;
                queryParams.selectedVillageCode = appState.selectedVillageCode;
                queryParams.textViewFilter = wFilterStr;
                if (!isUpdating) {
                    isUpdating = true;
                    new LoadPregnantList(ANCWomanListActivity.this, queryParams, false).execute();
                }
            }
        }
    }*/

   /* @Override
    public void displayAlert(int selectedItemPosition) {
        ArrayList<String> compList;
        String complication = filteredRowItems.get(selectedItemPosition).getRegriskFactors();
        if (complication != null && complication.length() > 0) {
            compList = new ArrayList<>();
            compList.add(complication.replace(",", ", ").replace("PH:", "\nPH:"));

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            View convertView = LayoutInflater.from(this).inflate(R.layout.complicated_reasons_list, null, false);

            ListView lv = convertView.findViewById(R.id.lvCompliatedReasonslist);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.listitem, compList);

            alertDialog.setView(convertView).setCancelable(true).setTitle((this.getResources().getString(R.string.compl_reasons)))
                    .setPositiveButton((this.getResources().getString(R.string.ok)), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
            lv.setAdapter(adapter);
            alertDialog.show();
        }
    }*/

    @Override
    public void displayAlert(int selectedItemPosition) {
        try {
            ArrayList<String> compList = new ArrayList<>();
            String complication;

            if(filteredRowItems.get(selectedItemPosition).getRegCCConfirmRisk()!=null &&
                    filteredRowItems.get(selectedItemPosition).getRegCCConfirmRisk().trim().length()>0
                    &&  filteredRowItems.get(selectedItemPosition)
                    .getRegCCConfirmRisk().equalsIgnoreCase("Y"))
                complication = getResources().getString(R.string.riskidentifiedbycc)+"\n"+
                        filteredRowItems.get(selectedItemPosition)
                                .getRegRiskFactorsByCC();
//                        +" "
//                        +filteredRowItems.get(selectedItemPosition).getRegOtherRiskByCC();
            else
                complication = filteredRowItems.get(selectedItemPosition).getRegriskFactors();
            if (complication != null && complication.length() > 0 ||
                    (filteredRowItems.get(selectedItemPosition).getRegRiskFactorsByCC()!=null && filteredRowItems.get(selectedItemPosition).getRegRiskFactorsByCC().trim().length()>0)) {
                compList = new ArrayList<>();
                compList.add(complication.replace(",", ", ")
                        .replace("PH:", "\nPH:").replace(", ,","")
                        .replace(",,",""));


                ArrayList<String> complListDisp = new ArrayList<>();
                for (int i = 0; i < compList.size(); i++) {
                    String compl[] = compList.get(i).split("[,\n]");
                    for (int j = 0; j < compl.length; j++) {
                        String strComp = compl[j];
                        if (strComp.contains("CH:"))
                            strComp = strComp.replaceAll("CH:", "");
                        if (strComp.contains("PH:"))
                            strComp = strComp.replaceAll("PH:", "");

                        strComp = strComp.replaceAll(" ", "");
                        if (strComp.contains("ormoreconsecutivespontaneousabortions"))
//                            strComp = strComp.split("[<>0123456789(]")[1];
                            strComp = strComp.split("[3<>(]")[1];
                        else
                            strComp = strComp.split("[<>(]")[0];
                        //strComp = strComp.split("[<>0123456789(]")[0];
                        strComp = strComp.replaceAll("/","");
                        strComp = strComp.toLowerCase().trim();
                        int identifier = getResources().getIdentifier
                                (strComp,
                                        "string", "com.sc.stmansi");
                        if (identifier > 0) {
                            if (compl[j].contains("CH:"))
                                complListDisp.add("CH: " + getResources().getString(identifier));
                            else if (compl[j].contains("PH:"))
                                complListDisp.add("PH: " + getResources().getString(identifier));
                            else
                                complListDisp.add(getResources().getString(identifier));

                        } else {

                            complListDisp.add(compl[j]);

                        }
                    }

                }

                ArrayList<String> complications = new ArrayList<>();
                String strStrings = "";
                for (int i = 0; i < complListDisp.size(); i++) {
                    if (i != 0)
                        strStrings = strStrings + ", " + complListDisp.get(i);
                    else
                        strStrings = complListDisp.get(i);
                }

                strStrings = strStrings.replace(", ,","")
                        .replace(",,","");

                String[] list = strStrings.split("PH:");

                ArrayList<String> l = new ArrayList<>();
                for (int i = 0; i < list.length; i++) {
                    if(!list[i].equalsIgnoreCase(", ")) {
                        if (i == 0)
                            l.add(list[i]);
                        else
                            l.add("\nPH: " + list[i]);
                    }
                }




                AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                View convertView = LayoutInflater.from(this).inflate(R.layout.complicated_reasons_list, null, false);

                ListView lv = convertView.findViewById(R.id.lvCompliatedReasonslist);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.listitem, l);

                alertDialog.setView(convertView).setCancelable(true).setTitle((this.getResources().getString(R.string.compl_reasons)))
                        .setPositiveButton((this.getResources().getString(R.string.ok)), new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                lv.setAdapter(adapter);
                alertDialog.show();
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    @Override
    public void displayHomeVisitList(int selectedItemPosition) {
        tblregisteredwomen woman = filteredRowItems.get(selectedItemPosition);
        Intent homevisitlist = new Intent(this, HomeVisitListActivity.class);
        appState.selectedWomanId = woman.getWomanId();
        homevisitlist.putExtra("globalState", prepareBundle());
        startActivity(homevisitlist);
    }

    @Override
    public void displayDelComplicationAlert(int selectedItemPosition) {

    }

    //18Oct2019 - Bindu - Alert message nearing del
    public void displayNearingDelAlertMessage(tblregisteredwomen woman) throws Exception {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        String alertmsg = woman.getRegWomanName() + ", " + " EDD : " + woman.getRegEDD();
        //alertDialogBuilder.setMessage(message).setCancelable(false).setTitle(Html.fromHtml("<font color='#FF7F27'>Alert -- </font><br><br><p>"+alertmsg + "</p>")).setNegativeButton(
        String msg = "<font color='#CC3333'> Nearing Delivery - </font>" + alertmsg;
        alertDialogBuilder.setMessage(getResources().getText(R.string.alerthomevisitnearingdelmsg)).setCancelable(false).setTitle(Html.fromHtml(msg)).setNegativeButton(
                getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent nextScreen = new Intent(ANCWomanListActivity.this, HomeVisit.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        nextScreen.putExtra("woman", ANCWomanListActivity.this.woman);
                        startActivity(nextScreen);
                        mQuickAction.dismiss();
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    /**
     * populate Village to Spinner
     */
    private void populateSpinnerVillage() throws Exception {
        FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
        villageCodeForName = facilityRepository.getPlaceMap();

        ArrayAdapter<CharSequence> villageSpinnerAdapter = new ArrayAdapter<>(this, R.layout.actionbar_spinner_list_item);
//        villageSpinnerAdapter.add("All Women");06May2021 Arpitha
        villageSpinnerAdapter.add(getResources().getString(R.string.all_Womans));//06May2021 Arpitha
        for (Map.Entry<String, Integer> village : villageCodeForName.entrySet()) {
            villageSpinnerAdapter.add(village.getKey());
        }
        villageSpinnerAdapter.setDropDownViewResource(R.layout.actionbar_spinner_layout_view);
        villageSpinner.setAdapter(villageSpinnerAdapter);
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        appState.selectedVillageTitle = (String) parent.getItemAtPosition(position);
        if (spinnerSelectionChanged) {
            updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr, aQuery.id(R.id.chklmpnotconfirmed).isChecked()); //03MAy2021 Bindu set islmpcondit
        }
        spinnerSelectionChanged = true;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.servicesmenu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Called whenever an item clicked in actionbar
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action buttons
        switch (item.getItemId()) {

            case R.id.home: {
                islmpconfirm = false; //03MAy2021 Bindu - set false
                Intent goToScreen = new Intent(ANCWomanListActivity.this, MainMenuActivity.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }

            case android.R.id.home: {
                islmpconfirm = false; //03MAy2021 Bindu - set false
                Intent goToScreen = new Intent(ANCWomanListActivity.this, MainMenuActivity.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                startActivity(goToScreen);

                return true;
            }

            case R.id.logout: {
                islmpconfirm = false; //03MAy2021 Bindu - set false
                Intent goToScreen = new Intent(ANCWomanListActivity.this, LoginActivity.class);
                displayAlert(getResources().getString(R.string.m111), goToScreen);
                return true;
            }


            case R.id.wlist: {
                islmpconfirm = false; //03MAy2021 Bindu - set false
                Intent goToScreen = new Intent(ANCWomanListActivity.this, RegisteredWomenActionTabs.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }
            //30Sep2019 - Bindu
            case R.id.about: {
                islmpconfirm = false; //03MAy2021 Bindu - set false
                Intent goToScreen = new Intent(ANCWomanListActivity.this, AboutActivity.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Save Confirmation Alert
     */
    private void displayAlert(final String spanText2, final Intent goToScreen) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set dialog message
        alertDialogBuilder
                .setMessage(spanText2)
                .setCancelable(false)
                .setNegativeButton((getResources().getString(R.string.m121)), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (goToScreen != null) {
                            startActivity(goToScreen);
                            if (spanText2.contains("logout")) {
                                try {
                                    LoginRepository loginRepository = new LoginRepository(databaseHelper);
                                    loginRepository.updateLogoutTime(appState.loginAuditId, appState.sessionUserId);
                                } catch (Exception e) {
                                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                                }
                            }
                        } else
                            dialog.cancel();
                    }
                })
                .setPositiveButton((getResources().getString(R.string.m122)), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    class FilterResetListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            aQuery.id(R.id.imgresetfilter).getImageView().setBackgroundResource(R.drawable.ic_reset_gray);
            aQuery.id(R.id.etWomanFilter).text("");
            wFilterStr = "";
            CareType careType = CareType.GENERAL;
            updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr, aQuery.id(R.id.chklmpnotconfirmed).isChecked()); //03MAy2021 Bindu islmp set
        }
    }

    private void setQuickActionItems(boolean isLMPConfirmed) {
        String str;
        /*if(!isLMPConfirmed) {
             str = getActivity().getResources().getString(R.string.reconfirmlmp);
            wReconfirmLMP = new ActionItem(ID_WReconfirmLMP, str, ContextCompat.getDrawable(getActivity(), R.drawable.ic_edit_60));
        }*/

        //31Aug2019 - Bindu - ADD items for quick action
        str = getResources().getString(R.string.viewprofile);
        ActionItem wEdit = new ActionItem(ID_WEDIT, str, ContextCompat.getDrawable(this, R.drawable.ic_edit_60));

        str = getResources().getString(R.string.services);
        ActionItem wPlannedServList = new ActionItem(ID_SERVLIST, str, ContextCompat.getDrawable(this, R.drawable.anm_pending_activities));

        str = getResources().getString(R.string.unplannedservices);
        ActionItem wUnplannedServList = new ActionItem(ID_UNPLANNEDSERVLIST, str, ContextCompat.getDrawable(this, R.drawable.anm_pending_activities));

        str = getResources().getString(R.string.homevisit);
        ActionItem wHomeVisitAdd = new ActionItem(ID_HOMEVISITADD, str, ContextCompat.getDrawable(this, R.drawable.ic_homevisit));

        str = getResources().getString(R.string.deliveryHeading);
        ActionItem wDel = new ActionItem(ID_DEL, str, ContextCompat.getDrawable(this,R.drawable.delivery_info));

        //          19Nov2019 Arpitha
        str = getResources().getString(R.string.deactivate);
        ActionItem wDeact = new ActionItem(ID_DEACT, str, ContextCompat.getDrawable(this,R.drawable.deactivate));

        mQuickAction = new QuickAction(this);
        /*if(!isLMPConfirmed) {
            mQuickAction.addActionItem(wReconfirmLMP); // 23Mar2021 Bindu
        }*/

        mQuickAction.addActionItem(wEdit);
        mQuickAction.addActionItem(wPlannedServList);
        mQuickAction.addActionItem(wUnplannedServList);
        mQuickAction.addActionItem(wHomeVisitAdd);
        mQuickAction.addActionItem(wDel);
        mQuickAction.addActionItem(wDeact);//19Nov2019 Arpitha
    }

    @Override
    public void displayLMPConfirmation(int selectedItemPosition) {
        tblregisteredwomen woman = filteredRowItems.get(selectedItemPosition);
        Intent nextScreen = new Intent(this, ReconfirmationRegActivity.class);
        appState.selectedWomanId = woman.getWomanId();
        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
        startActivity(nextScreen);
    }










    //    load data in a list
    static class LoadPregnantList extends AsyncTask<Void, Void, Void> {

        private ANCWomanListActivity viewsUpdateCallback;
        private QueryParams queryParams;
        private boolean firstLoad;
        private Map<String, Integer> villageCodeForName;

        LoadPregnantList(ANCWomanListActivity viewsUpdateCallback, QueryParams queryParams, boolean firstLoad, Map<String, Integer> villageCodeForName) {
            this.viewsUpdateCallback = viewsUpdateCallback;
            this.queryParams = queryParams;
            this.firstLoad = firstLoad;
            this.villageCodeForName = villageCodeForName;
        }

        LoadPregnantList(ANCWomanListActivity viewsUpdateCallback, QueryParams queryParams, boolean firstLoad) {
            this.viewsUpdateCallback = viewsUpdateCallback;
            this.queryParams = queryParams;
            this.firstLoad = firstLoad;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            int selectedVillageCode;
            // This means second constructor was used (called by scrollListener)
            if (villageCodeForName == null) selectedVillageCode = queryParams.selectedVillageCode;
            else {
                String selectedVillageTitle = queryParams.selectedVillageTitle;
                if (selectedVillageTitle == null)
                    selectedVillageCode = 0;
                else {
                    Integer code = villageCodeForName.get(selectedVillageTitle);
                    selectedVillageCode = (code == null) ? 0 : code;
                }
            }
            viewsUpdateCallback.appState.selectedVillageCode = selectedVillageCode; //10Dec2019 - Bindu
            try {
                viewsUpdateCallback.womenCount = viewsUpdateCallback.getTotalWomenCount(viewsUpdateCallback.user.getUserId(),
                        selectedVillageCode, viewsUpdateCallback.womenCondition, queryParams.textViewFilter,
                        islmpconfirm, viewsUpdateCallback.womanRepository,false); // 30April2021 Guru //03MAy2021 Bindu islmpconfirm
                List<tblregisteredwomen> rows = viewsUpdateCallback.prepareRowItemsForDisplay(queryParams.nextItemPosition,
                        selectedVillageCode, queryParams.textViewFilter, queryParams.isLMPNotConfirmed); //25Mar2021 Bindu
                if (firstLoad) {
                    int prevSize = viewsUpdateCallback.filteredRowItems.size();
                    viewsUpdateCallback.filteredRowItems.clear();
                    viewsUpdateCallback.registeredWomanAdapter.notifyItemRangeRemoved(0, prevSize);
                    firstLoad = false;
                }
                if (rows.size() > 0) {
                    viewsUpdateCallback.filteredRowItems.addAll(rows);

                    viewsUpdateCallback.nextItemPosition = viewsUpdateCallback.filteredRowItems.size();
                    viewsUpdateCallback.registeredWomanAdapter.notifyItemRangeInserted(firstLoad ? 0 : viewsUpdateCallback.nextItemPosition,
                            viewsUpdateCallback.filteredRowItems.size());
                }
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            try {
                viewsUpdateCallback.updateLabels();
                // 30April2021 Guru
                if (viewsUpdateCallback.filteredRowItems.size() > 0) {
                    viewsUpdateCallback.recyclerView.setVisibility(View.VISIBLE);
                    if (!firstLoad) {
                        viewsUpdateCallback.layoutManager.scrollToPosition(viewsUpdateCallback.registeredWomanAdapter.getItemCount());
                    }
                    viewsUpdateCallback.isUpdating = false;
                } // 30April2021 Guru
                viewsUpdateCallback.allCountView.setText(viewsUpdateCallback.getResources().
                        getString(R.string.recordcount,
                                viewsUpdateCallback.womenCount)); // 30April2021 Guru
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
        }
    }
}
