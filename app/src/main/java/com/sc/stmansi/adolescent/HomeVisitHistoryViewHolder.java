package com.sc.stmansi.adolescent;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sc.stmansi.R;

public class HomeVisitHistoryViewHolder extends RecyclerView.ViewHolder {
TextView txtVisitCount,txtVisitDate,txtVisitAge,txtvisitGts,txtcisitPregnant,txtvisitPregnantDummy,txtActionTaken;
    ImageView imgComplication,imgViewVisit;
    public HomeVisitHistoryViewHolder(@NonNull View itemView) {
        super(itemView);
        txtVisitCount = itemView.findViewById(R.id.txt_visitcount);
        txtVisitDate = itemView.findViewById(R.id.txt_visitdate);
        txtVisitAge = itemView.findViewById(R.id.txt_visitage);
        imgComplication = itemView.findViewById(R.id.imgComplication);
        imgViewVisit = itemView.findViewById(R.id.imgviewVisit);
        txtvisitGts = itemView.findViewById(R.id.txtadolviewGTS);
        txtcisitPregnant = itemView.findViewById(R.id.txtadolviewPregnant);
        txtvisitPregnantDummy = itemView.findViewById(R.id.txtadolviewpregnantdummy);
        txtActionTaken = itemView.findViewById(R.id.txt_actiontaken);
    }
}
