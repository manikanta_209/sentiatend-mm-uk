package com.sc.stmansi.adolescent;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidquery.AQuery;
import com.github.florent37.expansionpanel.ExpansionHeader;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.sc.stmansi.HomeVisit.HomeVisit;
import com.sc.stmansi.HomeVisit.HomeVisitListActivity;
import com.sc.stmansi.ImageStore.FileUtils;
import com.sc.stmansi.ImageStore.ImagePojo;
import com.sc.stmansi.ImageStore.RecyclerViewAdapter;
import com.sc.stmansi.R;
import com.sc.stmansi.SanNap.List_SanNap;
import com.sc.stmansi.adolanchistory.AdolANCHistory;
import com.sc.stmansi.childregistration.ChildRegistrationActivity;
import com.sc.stmansi.common.AlertDialogUtil;
import com.sc.stmansi.common.CommonDatePickerFragment;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.common.MultiSelectionSpinner;
import com.sc.stmansi.common.NavDrawerItem;
import com.sc.stmansi.common.NavDrawerListAdapter;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.configuration.SyncState;
import com.sc.stmansi.database.DBMethods;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.registration.RegistrationActivity;
import com.sc.stmansi.repositories.AdolVisitHeaderRepository;
import com.sc.stmansi.repositories.AdolescentRepository;
import com.sc.stmansi.repositories.AuditRepository;
import com.sc.stmansi.repositories.CovidDetailsRepository;
import com.sc.stmansi.repositories.FacilityRepository;
import com.sc.stmansi.repositories.ImageStoreRepository;
import com.sc.stmansi.repositories.SMSRepository;
import com.sc.stmansi.repositories.SettingsRepository;
import com.sc.stmansi.repositories.TransactionHeaderRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.sms.SendSMS;
import com.sc.stmansi.tables.AuditPojo;
import com.sc.stmansi.tables.MessageLogPojo;
import com.sc.stmansi.tables.TblContactDetails;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblAdolReg;
import com.sc.stmansi.tables.tblAdolVisitDetails;
import com.sc.stmansi.tables.tblAdolVisitHeader;
import com.sc.stmansi.tables.tblCovidTestDetails;
import com.sc.stmansi.tables.tblCovidVaccineDetails;
import com.sc.stmansi.tables.tblImageStore;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.viewprofile.ViewProfileActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class AdolVisit_New extends AppCompatActivity implements View.OnClickListener,
        DatePickerDialog.OnDateSetListener {

    private AppState appState;
    private DatabaseHelper databaseHelper;
    public static AQuery aqAdolVisit;
    private SyncState syncState;
    private String AdolId;
    private tblAdolReg currentAdolDetails;
    private RadioGroup rgIsPeriod, rgPregnant, rgHeight, rgIFA, rgConsumeIFA, rgHealthIssue, rgContraceptionNeed, rgCovidTest, rgCovidVaccine, rgCovidResult;
    private Spinner spnMarital;
    public static MultiSelectionSpinner spnNormalusePeriod, spnMensturalProblems, spnHealthProblems, spnTreatmentPlaces, spnIFAReasons;
    private ImageButton btnSave;
    private tblAdolVisitHeader tblAVHeader;
    private int VISIT_COUNT = 0;
    private int transID;
    private double BMI = 0;
    private tblAdolVisitDetails tblvisitDetails;
    public static TableRow trhealthissueothers, trMensturalNormalUseOthers, trMensturalProblemOthers, trTreatmentOthers;
    //    private tblAdolPregnant tblAPregnant;
    private DrawerLayout drawer;
    boolean messageSent;
    public static LinearLayout llCovidQ;
    boolean isMessageLogsaved;
    static EditText etphn;
    private EditText currentEditText;
    RadioButton radioButton_cm;
    RadioButton radioButton_feet;
    RadioButton radioButton_dontknow;
    private String globalGender;
    private tblCovidTestDetails tblcovtest;
    private tblCovidVaccineDetails tblcovvac;
    private LinkedHashMap<String, String> insertCovidVaccineVal;
    private List<tblCovidVaccineDetails> covVacItems;
    private boolean isvaccinated = false;
    private TblInstusers user;
    private AdolVisitHeaderRepository adolVisitHeaderRepository;

    //28-6-2021
    private static final int REQUEST_IMAGE_CAPTURE = 2;
    private RecyclerView recyclerViewImage;
    private String mCurrentPhotoPath;
    private ArrayList<tblImageStore> AllImagesBeforeSave = new ArrayList<>();
    private ArrayList<ImagePojo> AllImagePojo = new ArrayList<>();
    private Dao<tblImageStore, Integer> tblImageStoreDao;
    String  womanId;
    private AQuery aqPMSel;
    private Activity activity;
    RadioGroup radGrpEDDorLmp;
    private boolean isRegDate;
    Boolean wantToCloseDialog = false;
    private int noOfDays;
    private tblregisteredwomen woman;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adolvisit_bin);
        try {
            Bundle bundle = getIntent().getBundleExtra("globalState");
            if (bundle != null) {
                appState = bundle.getParcelable("appState");
                syncState = bundle.getParcelable("syncState");
            } else {
                appState = new AppState();
            }
            AdolId = getIntent().getStringExtra("adolID");
            Initialize();
            getSupportActionBar().hide();

            activity = AdolVisit_New.this;

            //21May2021 Bindu add
            UserRepository userRepo = new UserRepository(databaseHelper);
            user = userRepo.getOneAuditedUser(appState.ashaId);

            getdata();
            UISetups();
            UIFunctions();
            InitializeDrawer();

            aqAdolVisit.id(R.id.toolbarBtnHome).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        displayDialogExit();
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }
                }
            });
            btnSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        if (validationIFA() && validationDeworming() && validationContraceptionAware() && validationContraceptionNeed()
                                && validationSufferHealthissue() && validationashaavailability()) {
                            saveData();
                        }
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());

                    }
                }
            });
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private void displayDialogExit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(AdolVisit_New.this);
        builder.setMessage(getResources().getString(R.string.m110));
        builder.setCancelable(false);
        builder.setNegativeButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(AdolVisit_New.this, AdolescentHome.class);
                intent.putExtra("tabItem", 0);
                intent.putExtra("globalState", prepareBundle());
                startActivity(intent);
            }
        }).setPositiveButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    @SuppressLint("DefaultLocale")
    private void saveData() {
        try {
            tblAVHeader = new tblAdolVisitHeader();
            adolVisitHeaderRepository = new AdolVisitHeaderRepository(databaseHelper);
            transID = TransactionHeaderRepository.iCreateNewTransNew(user.getUserId(), databaseHelper); //23May2021 Bindu set userid

            tblAVHeader.setUserId(user.getUserId()); //23May2021 Bindu set userid
            tblAVHeader.setAdolId(currentAdolDetails.getAdolID());
            tblAVHeader.setVisitId(String.valueOf(VISIT_COUNT));
            tblAVHeader.setAdolvisHVisitDate(DateTimeUtil.getTodaysDate());

            tblAVHeader.setTransId(transID);
            tblAVHeader.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
            tblAVHeader.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());

            RadioButton rb_GTSYes = findViewById(R.id.rd_goingschoolYes);
            tblAVHeader.setAdolvisHGTS(rb_GTSYes.isChecked() ? "Yes" : "No");

            tblAVHeader.setAdolvisHEducation(String.valueOf(aqAdolVisit.id(R.id.spnadolVisitEducation).getSpinner().getSelectedItemPosition()));

            //mani 18June2021
            RadioButton rbAshaYes=findViewById(R.id.rd_ashaavailableYes);
            RadioButton rbAshaNo=findViewById(R.id.rd_ashaavailableNo);
            RadioButton rdAshaDk=findViewById(R.id.rd_ashaavailabledontknow);
            String ashaavailability="";
            if (rbAshaYes.isChecked())
                ashaavailability="Y";
            else if (rbAshaNo.isChecked())
                ashaavailability="N";
            else if (rdAshaDk.isChecked())
                ashaavailability="Dont Know";
            tblAVHeader.setAdolvisHAshaAvailable(ashaavailability);


            RadioButton rb_isPeriod = findViewById(R.id.rd_isPeriodStartedYes);
            if (rb_isPeriod.isChecked()) {
                tblAVHeader.setAdolvisHIsPeriodstarted("Yes");
                if (aqAdolVisit.id(R.id.etAdolVisitMensturalAge).getEditText().getText().toString().length() > 0) {
                    tblAVHeader.setAdolvisHAgeMenstural(aqAdolVisit.id(R.id.etAdolVisitMensturalAge).getEditText().getText().toString());
                }
                String normaluseperiod = spnNormalusePeriod.getSelectedIndicies().toString();
                normaluseperiod = normaluseperiod.replace("[", "");
                normaluseperiod = normaluseperiod.replace("]", "");
                if (normaluseperiod.contains("6")) {
                    tblAVHeader.setAdolvisHUsedforPeriodsOthers(aqAdolVisit.id(R.id.etMensturalnormaluseperiodOthers).getEditText().getText().toString());
                }
                tblAVHeader.setAdolvisHUsedforPeriods(normaluseperiod);

                String mensProblems = spnMensturalProblems.getSelectedIndicies().toString();
                mensProblems = mensProblems.replace("[", "");
                mensProblems = mensProblems.replace("]", "");
                if (mensProblems.contains("8")) {
                    tblAVHeader.setAdolvisHMenstrualProblemOthers(aqAdolVisit.id(R.id.etMensturalMenustralOthers).getEditText().getText().toString());
                }
                tblAVHeader.setAdolvisHMenstrualProblem(mensProblems);
            } else {
                tblAVHeader.setAdolvisHIsPeriodstarted("No");
            }

            if (spnMarital.getSelectedItemPosition() > 0) {
                tblAVHeader.setAdolvisHMaritalStatus(spnMarital.getSelectedItemPosition());
                tblAVHeader.setAdolvisHPartnerName(aqAdolVisit.id(R.id.etAdolVisitPartnerName).getEditText().getText().toString());
                tblAVHeader.setAdolvisHPartnerOccup(aqAdolVisit.id(R.id.etAdolVisitPartnerOccupa).getEditText().getText().toString());
                tblAVHeader.setAdolvisHAgeatMarriage(aqAdolVisit.id(R.id.etAdolVisitAgeattimeofmarriage).getEditText().getText().toString());
            } else if (spnMarital.getSelectedItemPosition() == 1 || spnMarital.getSelectedItemPosition() == 0) {
                tblAVHeader.setAdolvisHMaritalStatus(spnMarital.getSelectedItemPosition());
            }

            RadioButton rd_ispregnant = findViewById(R.id.rd_isPregnantYes);
            if (rd_ispregnant.isChecked()) {
                tblAVHeader.setAdolvisHIsPregnantatReg("Yes");
            } else {
                tblAVHeader.setAdolvisHIsPregnantatReg("No");
            }

            tblAVHeader.setAdolvisHHb(aqAdolVisit.id(R.id.etAdolVisitHbCount).getEditText().getText().toString());
            tblAVHeader.setAdolvisHHbDate(aqAdolVisit.id(R.id.etAdolVisitHBTestDate).getEditText().getText().toString());
            String category = getHGCategory();
            tblAVHeader.setAdolvisHHbCategory(category);

            if (radioButton_cm.isChecked()) {
                tblAVHeader.setAdolvisHHeightType("1");
                tblAVHeader.setAdolvisHHeight(aqAdolVisit.id(R.id.etAdolVisitheightcm).getEditText().getText().toString());
            } else if (radioButton_feet.isChecked()) {
                tblAVHeader.setAdolvisHHeightType("2");
                String feet = aqAdolVisit.id(R.id.spnAdolVisitHeightFeet).getSpinner().getSelectedItem().toString();
                String inches = aqAdolVisit.id(R.id.spnAdolVisitHeightInches).getSpinner().getSelectedItem().toString();
                feet = feet + "." + inches;
                tblAVHeader.setAdolvisHHeight(feet);
            } else {
                tblAVHeader.setAdolvisHHeightType("3");
            }
            tblAVHeader.setAdolvisHWeight(aqAdolVisit.id(R.id.etAdolVisitWeight).getEditText().getText().toString());
            if (tblAVHeader.getAdolvisHHeightType().equals("3")) {
                tblAVHeader.setAdolvisHBMI("");
            } else {
                tblAVHeader.setAdolvisHBMI(String.format("%.2f", BMI));
            }

            RadioButton rb_IFA = findViewById(R.id.rd_IFAYes);
            if (rb_IFA.isChecked()) {
                tblAVHeader.setAdolvisHIsIFATaken("Yes");
                RadioButton rd_consumeIFAYes = findViewById(R.id.rd_ConsumingIFAYes);
                if (rd_consumeIFAYes.isChecked()) {
                    tblAVHeader.setAdolvisHIFADaily("Yes");
                    tblAVHeader.setAdolvisHIFATabletsCount(aqAdolVisit.id(R.id.etIFANoofTabletsConsumes).getEditText().getText().toString());
                    tblAVHeader.setAdolvisHIFANotTakenReason("");
                } else {
                    tblAVHeader.setAdolvisHIFADaily("No");
                    String reasons = spnIFAReasons.getSelectedIndicies().toString();
                    reasons = reasons.replace("[", "");
                    reasons = reasons.replace("]", "");
                    tblAVHeader.setAdolvisHIFANotTakenReason(reasons);
                    tblAVHeader.setAdolvisHIFATabletsCount("");
                }
                tblAVHeader.setAdolvisHIFAGivenBy(aqAdolVisit.id(R.id.spnWhoGaveIFATablets).getSpinner().getSelectedItemPosition());
            } else {
                tblAVHeader.setAdolvisHIsIFATaken("No");
                tblAVHeader.setAdolvisHIFADaily("");
                tblAVHeader.setAdolvisHIFATabletsCount("");
                tblAVHeader.setAdolvisHIFANotTakenReason("");
                tblAVHeader.setAdolvisHIFAGivenBy(0);
            }
            if (aqAdolVisit.id(R.id.rd_dewormingtabletsYes).isChecked()) {
                tblAVHeader.setAdolvisHDewormingtab("Yes");
            } else if (aqAdolVisit.id(R.id.rd_dewormingtabletsNo).isChecked()) {
                tblAVHeader.setAdolvisHDewormingtab("No");
            } else if (aqAdolVisit.id(R.id.rd_dewormingtabletsDono).isChecked()) {
                tblAVHeader.setAdolvisHDewormingtab("Dont Know");
            }

            //Contraception
            tblAVHeader.setAdolvisHContraceptionNeed(aqAdolVisit.id(R.id.rd_needcontaceptionYes).isChecked() ? "Yes" : "No");
            tblAVHeader.setAdolvisHContraceptionAware(aqAdolVisit.id(R.id.rd_awarecontaceptionYes).isChecked() ? "Yes" : "No");
            String methodContra = "";
            if (aqAdolVisit.id(R.id.rd_needcontaceptionYes).isChecked() && aqAdolVisit.id(R.id.spnadolVisitContraceptionMethod).getSpinner().getSelectedItemPosition() != 0) {
                if (aqAdolVisit.id(R.id.spnadolVisitContraceptionMethod).getSpinner().getSelectedItemPosition() == 7) {
                    tblAVHeader.setAdolvisHContraceptionMethodOthers(aqAdolVisit.id(R.id.etadolVisitContraceptionMethodOther).getEditText().getText().toString());
                    tblAVHeader.setAdolvisHContraceptionMethod("");
                } else {
                    methodContra = String.valueOf(aqAdolVisit.id(R.id.spnadolVisitContraceptionMethod).getSpinner().getSelectedItemPosition());
                    tblAVHeader.setAdolvisHContraceptionMethod(methodContra);
                }
            } else {
                tblAVHeader.setAdolvisHContraceptionMethod("");
                tblAVHeader.setAdolvisHContraceptionMethodOthers("");
            }

            //Health Issue
            RadioButton HealthIssueYes = findViewById(R.id.rd_healthissuesYes);
            if (HealthIssueYes.isChecked()) {
                String healthproblems = spnHealthProblems.getSelectedIndicies().toString();
                healthproblems = healthproblems.replace("[", "");
                healthproblems = healthproblems.replace("]", "");
                tblAVHeader.setAdolvisHHealthIssues(healthproblems);
                if (spnHealthProblems.getSelectedIndicies().toString().contains("9")) {
                    tblAVHeader.setAdolvisHHealthIssuesOthers(aqAdolVisit.id(R.id.etAdolVisitHealthIssuesYesOthers).getEditText().getText().toString());
                }
            }

            tblAVHeader.setAdolvisHTreatedAt(aqAdolVisit.id(R.id.etAdolVisitAGWhereTreated).getEditText().getText().toString());

            //treat site
            String treatmentplaces = spnTreatmentPlaces.getSelectedIndicies().toString();
            treatmentplaces = treatmentplaces.replace("[", "");
            treatmentplaces = treatmentplaces.replace("]", "");
            tblAVHeader.setAdolvisHGeneralTreatment(treatmentplaces);
            if (treatmentplaces.contains("5")) {
                tblAVHeader.setAdolvisHGeneralTreatmentOthers(aqAdolVisit.id(R.id.etTreatmentOthers).getEditText().getText().toString());
            }

            if (complicationCheck()) {
                tblAVHeader.setAdolvisHComplications("Yes");
            } else {
                tblAVHeader.setAdolvisHComplications("No");
            }

            if (aqAdolVisit.id(R.id.rd_CovidTestYes).isChecked() || aqAdolVisit.id(R.id.rd_CovidTestNo).isChecked()) {
                prepareCovidTestData();
                tblcovtest.setTransId(transID); //22May2021 Bindu add transid
            }
            prepareCovidVaccineData();


            TransactionManager.callInTransaction(databaseHelper.getConnectionSource(), new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    AlertDialog.Builder builder = new AlertDialog.Builder(AdolVisit_New.this);
                    builder.setMessage(getResources().getString(R.string.trainingregconfirmation));
                    builder.setNegativeButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            try {
                                insertData();
                            } catch (Exception e) {
                                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                                crashlytics.log(e.getMessage());

                                dialogInterface.dismiss();
                            }
                        }
                    }).setPositiveButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    }).create().show();

                    return null;
                }
            });

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private void insertData() {
        try {
            TransactionManager.callInTransaction(databaseHelper.getConnectionSource(), new Callable<Object>() {
                @Override
                public Object call() throws Exception {
                    Dao<tblAdolVisitHeader, Integer> tblAdolVisitHeaderDao = databaseHelper.gettblAdolVisitHeaderRuntimeExceptionDao();
                    int add;
                    boolean added;
                    add = tblAdolVisitHeaderDao.create(tblAVHeader);
                    if (add > 0) {
                        added = TransactionHeaderRepository.iNewRecordTransNew(appState.ashaId, transID, tblAdolVisitHeaderDao.getTableName(), databaseHelper);
                        if (added) {
                            AdolescentRepository adolescentRepository = new AdolescentRepository(databaseHelper);
                            String sql = "UPDATE tblAdolReg set LastVisitCount = " + VISIT_COUNT + " WHERE AdolID = '" + currentAdolDetails.getAdolID() + "'";
                            InsertAuditData();
                            int update = adolescentRepository.updateVisitCount(sql);
                            if (update > 0) {
                                int covidAdd = 0;
                                Dao<tblCovidTestDetails, Integer> tblCovidTestDao = databaseHelper.getTblCovidTestDao();
                                if (tblcovtest != null) {
                                    covidAdd = tblCovidTestDao.create(tblcovtest);
                                    if (covidAdd > 0) {
                                        added = TransactionHeaderRepository.iNewRecordTransNew(user.getUserId(), transID, tblCovidTestDao.getTableName(), databaseHelper);
                                    }
                                }
                                Dao<tblCovidVaccineDetails, Integer> tblCovidVaccineDao = databaseHelper.getTblCovidVaccineDao();
                                if (insertCovidVaccineVal != null && insertCovidVaccineVal.size() > 0) {
                                    for (Map.Entry<String, String> entry : insertCovidVaccineVal.entrySet()) {
                                        tblcovvac.setCovidVaccinatedNo(entry.getKey());
                                        tblcovvac.setCovidVaccinatedDate(entry.getValue());
                                        add = tblCovidVaccineDao.create(tblcovvac);
                                    }
                                    if (add > 0) {
                                        added = TransactionHeaderRepository.iNewRecordTransNew(user.getUserId(), transID, tblCovidVaccineDao.getTableName(), databaseHelper);
                                    }
                                }
                                if (AllImagesBeforeSave.size() != 0 && AllImagePojo.size() != 0) {
                                    for (tblImageStore s : AllImagesBeforeSave) {
                                        s.setTransId(transID);
                                        s.setImagePath("");
                                        add = tblImageStoreDao.create(s);
                                    }
                                    if (add > 0) {
                                        added = TransactionHeaderRepository.iNewRecordTransNew(user.getUserId(), transID,
                                                tblImageStoreDao.getTableName(), databaseHelper);
                                    }
                                    for (ImagePojo s : AllImagePojo) {
                                        fileUpload();
                                        FileOutputStream fileOutputStream = new FileOutputStream(s.getImagePath());
                                        fileOutputStream.write(s.getImage());
                                    }
                                }

                                if (added) {
                                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(AdolVisit_New.this);
                                    if (checkSimState() == TelephonyManager.SIM_STATE_READY && prefs.getBoolean("sms", false) && tblAVHeader.getAdolvisHComplications().equalsIgnoreCase("Yes")) { //23May2021 Bindu check compl yes
                                        sendSMS(new TransactionHeaderRepository(databaseHelper), databaseHelper);//27Nov2019 Arpitha

                                    } else {
                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.visitsuccess), Toast.LENGTH_LONG).show();
                                        Intent intent = new Intent(AdolVisit_New.this, AdolVisitHistory.class);
                                        intent.putExtra("adolID", AdolId);
                                        intent.putExtra("globalState", prepareBundle());
                                        startActivity(intent);
                                    }
                                }

                            }
                        }
                    }
                    return null;
                }
            });
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

            Toast.makeText(getApplicationContext(), getResources().getString(R.string.erroroccurs), Toast.LENGTH_LONG).show();
        }
    }

    private void prepareCovidVaccineData() {
        try {
            tblcovvac = new tblCovidVaccineDetails();
            insertCovidVaccineVal = new LinkedHashMap<>();
            if (covVacItems != null && covVacItems.size() < 2) { // less than 2 doses only

                tblcovvac.setUserId(user.getUserId()); //23May2021 Bindu set userid
                tblcovvac.setBeneficiaryId(AdolId);
                tblcovvac.setBeneficiaryType("Adol"); //20May2021 Bindu
                tblcovvac.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
                tblcovvac.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
                tblcovvac.setTransId(transID);


                if (covVacItems != null && covVacItems.size() <= 0) {
                    if (aqAdolVisit.id(R.id.etAdolVisitCovidFirstDoseDate).getText().toString().trim().length() > 0)
                        insertCovidVaccineVal.put("1", aqAdolVisit.id(R.id.etAdolVisitCovidFirstDoseDate).getText().toString());
                    if (aqAdolVisit.id(R.id.etAdolVisitCovidSecondDoseDate).getText().toString().trim().length() > 0)
                        insertCovidVaccineVal.put("2", aqAdolVisit.id(R.id.etAdolVisitCovidSecondDoseDate).getText().toString());
                } else if (covVacItems != null && covVacItems.size() == 1) {
                    if (aqAdolVisit.id(R.id.etAdolVisitCovidSecondDoseDate).getText().toString().trim().length() > 0)
                        insertCovidVaccineVal.put("2", aqAdolVisit.id(R.id.etAdolVisitCovidSecondDoseDate).getText().toString());
                }
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private void prepareCovidTestData() {
        try {
            tblcovtest = new tblCovidTestDetails();
            tblcovtest.setUserId(user.getUserId()); //23MAy2021 Bindu set userid
            tblcovtest.setWomenId(currentAdolDetails.getAdolID());
            tblcovtest.setBeneficiaryType("Adol");
            tblcovtest.setVisitNum("" + VISIT_COUNT);
            RadioButton HealthIssueYes = findViewById(R.id.rd_healthissuesYes);
            if (HealthIssueYes.isChecked()) {
                String healthproblems = spnHealthProblems.getSelectedIndicies().toString();
                healthproblems = healthproblems.replace("[", "");
                healthproblems = healthproblems.replace("]", "");
                tblcovtest.setHealthIssues(healthproblems);
                if (spnHealthProblems.getSelectedIndicies().toString().contains("9")) {
                    tblcovtest.setHealthIssuesOthers(aqAdolVisit.id(R.id.etAdolVisitHealthIssuesYesOthers).getEditText().getText().toString());
                }
            }
            tblcovtest.setCovidTest(aqAdolVisit.id(R.id.rd_CovidTestYes).isChecked() ? getResources().getString(R.string.yesshortform) : getResources().getString(R.string.noshortform));
            tblcovtest.setCovidResult(aqAdolVisit.id(R.id.rd_CovidPositive).isChecked() ? getResources().getString(R.string.positivetxt) : getResources().getString(R.string.negativetxt));
            tblcovtest.setCovidResultDate(aqAdolVisit.id(R.id.etAdolVisitCovidTestDate).getText().toString());
            tblcovtest.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
            tblcovtest.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private String getHGCategory() {
        try {
            float hbCount = Float.parseFloat(aqAdolVisit.id(R.id.etAdolVisitHbCount).getEditText().getText().toString());
            if (hbCount < 8.0) {
                //severe
                return "Severe";
            } else if (hbCount >= 8 && hbCount <= 9.9) {
                //moderate
                return "Moderate";
            } else if (hbCount >= 10 && hbCount <= 11.9) {
                //mild
                return "Mild";
            } else if (hbCount >= 11.9 && hbCount <= 24) {
                //normal
                return "Normal";
            } else if (hbCount > 24) {
                return "NA";
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return "";
    }

    @SuppressLint("SetTextI18n")
    private void UISetups() {
        try {
            aqAdolVisit.id(R.id.toolbartxtTitle).getTextView().setText(currentAdolDetails.getRegAdolName() + "\n" + currentAdolDetails.getRegAdolAge() + getResources().getString(R.string.yearstxt));
            int visitCount = currentAdolDetails.getLastVisitCount();
            if (visitCount == 0) {
                VISIT_COUNT = 1;
            } else {
                VISIT_COUNT = visitCount + 1;
            }
            aqAdolVisit.id(R.id.txtAdolVisitCount).getTextView().setText("#" + VISIT_COUNT);           //visit count
            aqAdolVisit.id(R.id.txtAdolVisitDate).getTextView().setText(DateTimeUtil.getTodaysDate()); //visit date


            aqAdolVisit.id(R.id.spnadolVisitEducation).getSpinner().setSelection(currentAdolDetails.getRegAdolEducation());  //Education
            RadioButton rb_GTSYes = findViewById(R.id.rd_goingschoolYes);
            RadioButton rb_GTSNo = findViewById(R.id.rd_goingschoolNo); //going to school
            if (currentAdolDetails.getRegAdolGoingtoSchool().equals("Yes")) {
                rb_GTSYes.setChecked(true);
            } else {
                rb_GTSNo.setChecked(true);
            }


            if (globalGender.equals("Male")) {
                aqAdolVisit.id(R.id.trisPeriodStarted).getView().setVisibility(View.GONE);
                aqAdolVisit.id(R.id.llmenstpbm).getView().setVisibility(View.GONE);
                aqAdolVisit.id(R.id.llcontraception).getView().setVisibility(View.GONE);
                aqAdolVisit.id(R.id.trAdolVisitPregnant).getView().setVisibility(View.GONE);
                aqAdolVisit.id(R.id.tvpartnername).text(getResources().getString(R.string.wifename));
                aqAdolVisit.id(R.id.tvpartneroccupa).text(getResources().getString(R.string.wifeoccupa));

            } else {
                aqAdolVisit.id(R.id.tvpartnername).text(getResources().getString(R.string.tvhusbname));
                aqAdolVisit.id(R.id.tvpartneroccupa).text(getResources().getString(R.string.husbandoccupa));
                aqAdolVisit.id(R.id.trisPeriodStarted).getView().setVisibility(View.VISIBLE);
                aqAdolVisit.id(R.id.llmenstpbm).getView().setVisibility(View.VISIBLE);
                aqAdolVisit.id(R.id.llcontraception).getView().setVisibility(View.VISIBLE);
                aqAdolVisit.id(R.id.trAdolVisitPregnant).getView().setVisibility(View.VISIBLE);
                RadioButton rb_AVPeriodYes = findViewById(R.id.rd_isPeriodStartedYes);
                RadioButton rb_AVPeriodNo = findViewById(R.id.rd_isPeriodStartedNo);
                if (currentAdolDetails.getRegAdolisPeriod().equals("Yes")) {
                    rb_AVPeriodYes.setChecked(true);
                    rb_AVPeriodYes.setEnabled(false);
                    rb_AVPeriodNo.setEnabled(false);

                    aqAdolVisit.id(R.id.ll_AV_Period).getView().setVisibility(View.VISIBLE);
                    aqAdolVisit.id(R.id.llperioduse).getView().setVisibility(View.VISIBLE);
                    aqAdolVisit.id(R.id.llperiodpbm).getView().setVisibility(View.VISIBLE);

                    aqAdolVisit.id(R.id.etAdolVisitMensturalAge).getEditText().setText(currentAdolDetails.getRegAdolAgeatMenstruated());
                    aqAdolVisit.id(R.id.etAdolVisitMensturalAge).getEditText().setEnabled(false);
                } else {
                    rb_AVPeriodNo.setChecked(true);
                }
                RadioButton rbPregnantYes = findViewById(R.id.rd_isPregnantYes);
                RadioButton rbPregnantNo = findViewById(R.id.rd_isPregnantNo);
                if (currentAdolDetails.getRegAdolPregnant().equals("Yes")) {
                    rbPregnantYes.setChecked(true);
                } else {
                    rbPregnantNo.setChecked(true);
                }
            }

            String[] normaluseperiod = getResources().getStringArray(R.array.normaluseperiod);
            spnNormalusePeriod.setItems(normaluseperiod);

            String[] mensturalproblems = getResources().getStringArray(R.array.menustralproblems);
            spnMensturalProblems.setItems(mensturalproblems);

            aqAdolVisit.id(R.id.spnAdolVisitRelationship).getSpinner().setSelection(currentAdolDetails.getRegAdolRelationship());

            if (currentAdolDetails.getRegAdolRelationship() > 1) {
                aqAdolVisit.id(R.id.ll_AV_MaritalStatus).getView().setVisibility(View.VISIBLE);
                aqAdolVisit.id(R.id.etAdolVisitPartnerName).getEditText().setText(currentAdolDetails.getRegAdolPartnerName());
                aqAdolVisit.id(R.id.etAdolVisitPartnerOccupa).getEditText().setText(currentAdolDetails.getRegAdolPartnerOccupa());
            } else {
                aqAdolVisit.id(R.id.ll_AV_MaritalStatus).getView().setVisibility(View.GONE);
            }

            int selectfeet = 0, inch = 0;
            switch (currentAdolDetails.getRegAdolHeightType()) {
                case 1:
                    radioButton_cm.setChecked(true);
                    aqAdolVisit.id(R.id.trAdolVisitHeightFeet).getView().setVisibility(View.GONE);
                    aqAdolVisit.id(R.id.trAdolVisitheightcm).getView().setVisibility(View.VISIBLE);
                    aqAdolVisit.id(R.id.etAdolVisitheightcm).getEditText().setText(currentAdolDetails.getRegAdolHeight());
                    if (currentAdolDetails.getRegAdolHeight().length() > 0) {
                        HeightWarning(Double.parseDouble(currentAdolDetails.getRegAdolHeight()));
                    } else {
                        HeightWarning(0);
                    }
                    break;
                case 2:
                    radioButton_feet.setChecked(true);
                    aqAdolVisit.id(R.id.trAdolVisitHeightFeet).getView().setVisibility(View.VISIBLE);//suresh
                    aqAdolVisit.id(R.id.trAdolVisitheightcm).getView().setVisibility(View.GONE);
                    String height = currentAdolDetails.getRegAdolHeight();
                    int i = 0;
                    StringBuilder Feet = new StringBuilder();
                    for (i = 0; i < height.length(); i++) {
                        char c = height.charAt(i);
                        if (c == '.') {
                            break;
                        } else {
                            Feet.append(c);
                        }
                    }
                    selectfeet = Integer.parseInt(Feet.toString()) - 1;
                    aqAdolVisit.id(R.id.spnAdolVisitHeightFeet).getSpinner().setSelection(selectfeet);

                    i++;
                    StringBuilder inches = new StringBuilder();
                    for (; i < height.length(); i++) {
                        char c = height.charAt(i);
                        inches.append(c);
                    }
                    inch = Integer.parseInt(inches.toString().trim());
                    aqAdolVisit.id(R.id.spnAdolVisitHeightInches).getSpinner().setSelection(inch);
                    break;
                case 3:
                    radioButton_dontknow.setChecked(true);
                    break;
            }//end of heights

            aqAdolVisit.id(R.id.etAdolVisitWeight).getEditText().setText(currentAdolDetails.getRegAdolWeight());
            WeightWarning(Double.parseDouble(currentAdolDetails.getRegAdolWeight()));

            if (currentAdolDetails.getRegAdolHeightType() == 2) {
                calculateBMI(selectfeet, inch, currentAdolDetails.getRegAdolWeight());
                aqAdolVisit.id(R.id.trAdolVisitBMI).getView().setVisibility(View.VISIBLE);
            } else if (currentAdolDetails.getRegAdolHeightType() == 1) {
                calculateBMI(currentAdolDetails.getRegAdolHeight(), currentAdolDetails.getRegAdolWeight());
                aqAdolVisit.id(R.id.trAdolVisitBMI).getView().setVisibility(View.VISIBLE);
            } else if (currentAdolDetails.getRegAdolHeightType() == 3) {
                aqAdolVisit.id(R.id.txtAdolVisitBMI).text(""); //23May2021 Bindu set bmi blank
                aqAdolVisit.id(R.id.trAdolVisitBMI).getView().setVisibility(View.GONE);
            }

            if (globalGender.equals("Male")) {
                aqAdolVisit.id(R.id.trAdolVisitAwareContraception).getView().setVisibility(View.GONE);
                aqAdolVisit.id(R.id.trAdolVisitNeedContraception).getView().setVisibility(View.GONE);
            } else {
                aqAdolVisit.id(R.id.trAdolVisitAwareContraception).getView().setVisibility(View.VISIBLE);
                aqAdolVisit.id(R.id.trAdolVisitNeedContraception).getView().setVisibility(View.VISIBLE);
            }

            String[] healthproblems = getResources().getStringArray(R.array.healthproblemsuffer);
            spnHealthProblems.setItems(healthproblems);

            if (spnHealthProblems.getSelectedItemsAsString().contains(getResources().getString(R.string.others))) {
                trhealthissueothers.setVisibility(View.VISIBLE);
            } else {
                trhealthissueothers.setVisibility(View.GONE);
            }

            spnTreatmentPlaces.setItems(getResources().getStringArray(R.array.treatmentplaces));

            getCovidVaccineDetails();

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private void Initialize() {
        try {
            databaseHelper = getHelper();
            DBMethods dbMethods = new DBMethods(databaseHelper);
            aqAdolVisit = new AQuery(this);

            rgIsPeriod = findViewById(R.id.rgisPeriodStarted);
            spnMarital = findViewById(R.id.spnAdolVisitRelationship);
            rgPregnant = findViewById(R.id.rgisPregnant);
            rgHeight = findViewById(R.id.rgAdolVisitHeight);
            rgIFA = findViewById(R.id.rgAdolVisitIFA);
            rgConsumeIFA = findViewById(R.id.rg_consumingIFA);
            rgHealthIssue = findViewById(R.id.rghealthissues);
            rgContraceptionNeed = findViewById(R.id.rgneedcontraception);

            spnNormalusePeriod = findViewById(R.id.spnMensturalnormalUserPeriod);
            spnMensturalProblems = findViewById(R.id.spnMentustralProblems);
            spnHealthProblems = findViewById(R.id.spnAdolVisitHealthProblems);
            spnTreatmentPlaces = findViewById(R.id.spnIllinessTeatment);
            spnIFAReasons = findViewById(R.id.spnIFAReasons);

            btnSave = findViewById(R.id.btnadolVisitSave);

            llCovidQ = findViewById(R.id.llCovidQs);

            rgCovidTest = findViewById(R.id.rgCovidTest);
            rgCovidResult = findViewById(R.id.rgCovidResult);
            rgCovidVaccine = findViewById(R.id.rgCovidVaccine);

            trhealthissueothers = findViewById(R.id.trHealthProblemYesOthers);
            trMensturalNormalUseOthers = findViewById(R.id.trMensturalnormaluseperiodOthers);
            trMensturalProblemOthers = findViewById(R.id.trMensturalProblemsOthers);
            trTreatmentOthers = findViewById(R.id.trTreatmentOthers);
            radioButton_cm = findViewById(R.id.rd_cm);
            radioButton_feet = findViewById(R.id.rd_feet);
            radioButton_dontknow = findViewById(R.id.rd_htdontknow);

            tblImageStoreDao = databaseHelper.getTblImageStoreDao();
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(getApplicationContext(), DatabaseHelper.class);
        }
        return databaseHelper;
    }

    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        b.putParcelable("syncState", syncState);
        return b;
    }

    private void InitializeDrawer() {
        try {
            drawer = findViewById(R.id.drawer_adolvisit);
            ListView drawerList = findViewById(R.id.lvNav_adolVisit);

            ArrayList<NavDrawerItem> navDrawerItems = new ArrayList<>();
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.editadolescent), R.drawable.ic_edit_icon));
//            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.homevisitadd), R.drawable.ic_homevisit));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.adolvisithistory), R.drawable.anm_pending_activities));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.trainingparticipated), R.drawable.presentation));
            //if (currentAdolDetails != null && currentAdolDetails.getRegAdolGender().equals("Female") && currentAdolDetails.getRegAdolisPeriod().equals("Yes")) {
                navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.mhmform), R.drawable.form_icon));
          //  }

            //            07Sep2021 Arpitha
//            if(currentAdolDetails.getRegAdolPregnant()!=null && currentAdolDetails.getRegAdolPregnant().trim().length()>0
//                    && currentAdolDetails.getRegAdolPregnant().equalsIgnoreCase("Yes")) {

                String strAddPregnancy = "";
                if (new AdolescentRepository(databaseHelper).
                        isAdolANC(currentAdolDetails.getRegAdolAadharNo(), currentAdolDetails.getAdolID()))
                    strAddPregnancy = getResources().getString(R.string.viewpregancny);
                else
                    strAddPregnancy = getResources().getString(R.string.addpregancny);

                navDrawerItems.add(new NavDrawerItem(strAddPregnancy, R.drawable.anc));
                womanId = new AdolescentRepository(databaseHelper).
                        getAdolANCWomanId(currentAdolDetails.getRegAdolAadharNo(), currentAdolDetails.getAdolID());

                navDrawerItems.add(new NavDrawerItem(getResources()
                        .getString(R.string.anchomevisit), R.drawable.ic_homevisit));

            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.preghistory),R.drawable.anc));//12Sep2021 Arpitha



            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deactivate), R.drawable.deactivate));
            drawer.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
            drawerList.setAdapter(new NavDrawerListAdapter(getApplicationContext(), navDrawerItems));
            drawerList.setOnItemClickListener(new DrawerItemClickListener());
            Toolbar toolbar = findViewById(R.id.toolbarCommon);
            ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close);
            drawer.setDrawerListener(drawerToggle);
            drawerToggle.syncState();
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private class DrawerItemClickListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            try {
                switch (i) {
                    case 0:
                        AlertDialog.Builder builder = new AlertDialog.Builder(AdolVisit_New.this);
                        builder.setMessage(getResources().getString(R.string.m110));
                        builder.setCancelable(false);
                        builder.setNegativeButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent(AdolVisit_New.this, AdolRegistration.class);
                                intent.putExtra("globalState", prepareBundle());
                                intent.putExtra("isEdit", true);
                                intent.putExtra("adolID", AdolId);
                                startActivity(intent);

                            }
                        }).setPositiveButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).create().show();

                        break;
//                    case 1:
//                        drawer.closeDrawer(GravityCompat.END);
//                        break;
                    case 1:
                        builder = new AlertDialog.Builder(AdolVisit_New.this);
                        builder.setMessage(getResources().getString(R.string.m110));
                        builder.setCancelable(false);
                        builder.setNegativeButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent(AdolVisit_New.this, AdolVisitHistory.class);
                                intent.putExtra("globalState", prepareBundle());
                                intent.putExtra("adolID", AdolId);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);

                            }
                        }).setPositiveButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).create().show();
                        break;
                    case 2:
                        builder = new AlertDialog.Builder(AdolVisit_New.this);
                        builder.setMessage(getResources().getString(R.string.m110));
                        builder.setCancelable(false);
                        builder.setNegativeButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent(AdolVisit_New.this, AdolParticipatedTrainings.class);
                                intent.putExtra("globalState", prepareBundle());
                                intent.putExtra("adolID", AdolId);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);

                            }
                        }).setPositiveButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).create().show();
                        break;
                    case 3:
                        Intent intent = new Intent(AdolVisit_New.this, AdolDeactivation.class);
                        if (currentAdolDetails != null && currentAdolDetails.getRegAdolGender().equals("Female") && currentAdolDetails.getRegAdolisPeriod().equals("Yes")) {
                            intent = new Intent(AdolVisit_New.this, List_SanNap.class);
                        } else {
                            if (currentAdolDetails.getRegAdolDeactivateDate().length() != 0) {
                                intent.putExtra("isView", true);
                            }
                        }
                        builder = new AlertDialog.Builder(AdolVisit_New.this);
                        builder.setMessage(getResources().getString(R.string.m110));
                        builder.setCancelable(false);
                        Intent finalIntent = intent;
                        builder.setNegativeButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finalIntent.putExtra("globalState", prepareBundle());
                                finalIntent.putExtra("adolID", AdolId);
                                finalIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(finalIntent);

                            }
                        }).setPositiveButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).create().show();
                        break;
                    // 01Sep2021 Arpitha
                    case 4:

                        if((currentAdolDetails.getRegAdolPregnant()!=null && currentAdolDetails.getRegAdolPregnant().trim().length() >0
                                && currentAdolDetails.getRegAdolPregnant().equalsIgnoreCase("Yes")) ||
                        new AdolescentRepository(databaseHelper).
                                isAdolPreganant(currentAdolDetails.getAdolID()))
                        {
                            if (new AdolescentRepository(databaseHelper)
                                    .isAdolANC(currentAdolDetails.getRegAdolAadharNo(), currentAdolDetails.getAdolID())) {
                                appState.selectedWomanId = womanId;
                                Intent intent1 = new Intent(AdolVisit_New.this, ViewProfileActivity.class);
                                intent1.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                startActivity(intent1);
                            } else
                                showPregnantOrMotherSelectionDialog();

                        }else
                            Toast.makeText(getApplicationContext(),getResources().getString(R.string.ntapplicable),
                                    Toast.LENGTH_LONG).show();
                        break;

                    case 5:
                        if (new AdolescentRepository(databaseHelper)
                                .isAdolANC(currentAdolDetails.getRegAdolAadharNo(), currentAdolDetails.getAdolID()))
                        {
                            appState.selectedWomanId = womanId;
                            Intent hv = new Intent(AdolVisit_New.this, HomeVisitListActivity.class);
                            hv.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(hv);
                        }else
                            Toast.makeText(getApplicationContext(),getResources().getString(R.string.ntapplicable),
                                    Toast.LENGTH_LONG).show();
                        break;
                    //                        12Sep2021 Arpitha
                    case 6:
                        if(new AdolescentRepository(databaseHelper)
                                .isAdolANCHistory(currentAdolDetails.getRegAdolAadharNo(), currentAdolDetails.getAdolID()))//10Sep2021 Arpitha
                        {
                            appState.selectedWomanId = womanId;
                            Intent intentHis = new Intent(view.getContext(), AdolANCHistory.class);
                            intentHis.putExtra("globalState", prepareBundle());
                            intentHis.putExtra("aadharno",
                                    currentAdolDetails.getRegAdolAadharNo());
                            startActivity(intentHis);
                        }else
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.ntapplicable),
                                    Toast.LENGTH_LONG).show();

                        break;// 12Sep2021 Arpitha
                    case 7:
                        builder = new AlertDialog.Builder(AdolVisit_New.this);
                        builder.setMessage(getResources().getString(R.string.m110));
                        builder.setCancelable(false);
                        builder.setNegativeButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent(AdolVisit_New.this, AdolDeactivation.class);
                                intent.putExtra("globalState", prepareBundle());
                                if (currentAdolDetails.getRegAdolDeactivateDate().length() != 0) {
                                    intent.putExtra("isView", true);
                                }
                                intent.putExtra("adolID", AdolId);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);

                            }
                        }).setPositiveButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).create().show();
                        break;

                }
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());

            }
        }
    }

    private void getdata() {
        try {
            AdolescentRepository adolescentRepository = new AdolescentRepository(databaseHelper);
            currentAdolDetails = adolescentRepository.getAdolDatafromAdolID(AdolId);
            currentAdolDetails = addingLastVisitDetailstoAdols(currentAdolDetails);

            globalGender = currentAdolDetails.getRegAdolGender();//setting global variable of gender
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private tblAdolReg addingLastVisitDetailstoAdols(tblAdolReg allAdols) {
        try {
            AdolVisitHeaderRepository adolVisitHeaderRepository = new AdolVisitHeaderRepository(databaseHelper);

            if (allAdols.getLastVisitCount() != 0) {
                tblAdolVisitHeader localAdolVisitHeader = adolVisitHeaderRepository.getVisitbyID(String.valueOf(allAdols.getLastVisitCount()), allAdols.getAdolID());
                allAdols.setRegAdolGoingtoSchool(localAdolVisitHeader.getAdolvisHGTS());
                allAdols.setRegAdolEducation(Integer.parseInt(localAdolVisitHeader.getAdolvisHEducation()));
                allAdols.setRegAdolisPeriod(localAdolVisitHeader.getAdolvisHIsPeriodstarted());
                allAdols.setRegAdolAgeatMenstruated(localAdolVisitHeader.getAdolvisHAgeMenstural());
                allAdols.setRegAdolPregnant(localAdolVisitHeader.getAdolvisHIsPregnantatReg());
                allAdols.setRegAdolRelationship(localAdolVisitHeader.getAdolvisHMaritalStatus());
                allAdols.setRegAdolHeightType(Integer.parseInt(localAdolVisitHeader.getAdolvisHHeightType()));
                allAdols.setRegAdolHeight(localAdolVisitHeader.getAdolvisHHeight());
                allAdols.setRegAdolWeight(localAdolVisitHeader.getAdolvisHWeight());

            }

            return allAdols;
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return null;
    }

    private void HeightWarning(double cm) {
        try {
            if (aqAdolVisit.id(R.id.rd_feet).isChecked()) {
                double feet = Double.parseDouble(aqAdolVisit.id(R.id.spnAdolVisitHeightFeet).getSpinner().getSelectedItem().toString());
                double inches = aqAdolVisit.id(R.id.spnAdolVisitHeightInches).getSpinner().getSelectedItemPosition();
                cm = 30.48 * feet;
                cm = cm + (inches * 2.54);
            }
            switch (Integer.parseInt(currentAdolDetails.getRegAdolAge())) {
                case 10:
                    if (!(cm >= 139 && cm <= 150)) {
                        if (aqAdolVisit.id(R.id.rd_feet).isChecked()) {
                            aqAdolVisit.id(R.id.tvHeight).getTextView().setTextColor(Color.RED);
                        } else {
                            aqAdolVisit.id(R.id.etAdolVisitheightcm).getEditText().setError(getResources().getString(R.string.heightis) + cm);
                        }
                    } else {
                        aqAdolVisit.id(R.id.tvHeight).getTextView().setTextColor(Color.BLACK);
                    }
                    break;
                case 11:
                    if (aqAdolVisit.id(R.id.rd_male).isChecked()) {
                        if (!(cm >= 144 && cm <= 155)) {
                            if (aqAdolVisit.id(R.id.rd_feet).isChecked()) {
                                aqAdolVisit.id(R.id.tvHeight).getTextView().setTextColor(Color.RED);
                            } else {
                                aqAdolVisit.id(R.id.etAdolVisitheightcm).getEditText().setError(getResources().getString(R.string.heightis) + cm);
                            }
                        } else {
                            aqAdolVisit.id(R.id.tvHeight).getTextView().setTextColor(Color.BLACK);
                        }
                    } else {
                        if (!(cm >= 144 && cm <= 156)) {
                            if (aqAdolVisit.id(R.id.rd_feet).isChecked()) {
                                aqAdolVisit.id(R.id.tvHeight).getTextView().setTextColor(Color.RED);
                            } else {
                                aqAdolVisit.id(R.id.etAdolVisitheightcm).getEditText().setError(getResources().getString(R.string.heightis) + cm);
                            }
                        } else {
                            aqAdolVisit.id(R.id.tvHeight).getTextView().setTextColor(Color.BLACK);
                        }
                    }

                    break;
                case 12:
                    if (aqAdolVisit.id(R.id.rd_male).isChecked()) {
                        if (!(cm >= 149 && cm <= 163)) {
                            if (aqAdolVisit.id(R.id.rd_feet).isChecked()) {
                                aqAdolVisit.id(R.id.tvHeight).getTextView().setTextColor(Color.RED);
                            } else {
                                aqAdolVisit.id(R.id.etAdolVisitheightcm).getEditText().setError(getResources().getString(R.string.heightis) + cm);
                            }
                        } else {
                            aqAdolVisit.id(R.id.tvHeight).getTextView().setTextColor(Color.BLACK);
                        }
                    } else {
                        if (!(cm >= 151 && cm <= 163)) {
                            if (aqAdolVisit.id(R.id.rd_feet).isChecked()) {
                                aqAdolVisit.id(R.id.tvHeight).getTextView().setTextColor(Color.RED);
                            } else {
                                aqAdolVisit.id(R.id.etAdolVisitheightcm).getEditText().setError(getResources().getString(R.string.heightis) + cm);
                            }
                        } else {
                            aqAdolVisit.id(R.id.tvHeight).getTextView().setTextColor(Color.BLACK);
                        }
                    }
                    break;
                case 13:
                    if (!(cm >= 156 && cm <= 169)) {
                        if (aqAdolVisit.id(R.id.rd_feet).isChecked()) {
                            aqAdolVisit.id(R.id.tvHeight).getTextView().setTextColor(Color.RED);
                        } else {
                            aqAdolVisit.id(R.id.etAdolVisitheightcm).getEditText().setError(getResources().getString(R.string.heightis) + cm);
                        }
                    } else {
                        aqAdolVisit.id(R.id.tvHeight).getTextView().setTextColor(Color.BLACK);
                    }
                    break;
                case 14:
                    if (aqAdolVisit.id(R.id.rd_male).isChecked()) {
                        if (!(cm >= 166 && cm <= 176)) {
                            if (aqAdolVisit.id(R.id.rd_feet).isChecked()) {
                                aqAdolVisit.id(R.id.tvHeight).getTextView().setTextColor(Color.RED);
                            } else {
                                aqAdolVisit.id(R.id.etAdolVisitheightcm).getEditText().setError(getResources().getString(R.string.heightis) + cm);
                            }
                        } else {
                            aqAdolVisit.id(R.id.tvHeight).getTextView().setTextColor(Color.BLACK);
                        }
                    } else {
                        if (!(cm >= 161 && cm <= 173)) {
                            if (aqAdolVisit.id(R.id.rd_feet).isChecked()) {
                                aqAdolVisit.id(R.id.tvHeight).getTextView().setTextColor(Color.RED);
                            } else {
                                aqAdolVisit.id(R.id.etAdolVisitheightcm).getEditText().setError(getResources().getString(R.string.heightis) + cm);
                            }
                        } else {
                            aqAdolVisit.id(R.id.tvHeight).getTextView().setTextColor(Color.BLACK);
                        }
                    }
                    break;
                case 15:
                    if (aqAdolVisit.id(R.id.rd_male).isChecked()) {
                        if (!(cm >= 170 && cm <= 183)) {
                            if (aqAdolVisit.id(R.id.rd_feet).isChecked()) {
                                aqAdolVisit.id(R.id.tvHeight).getTextView().setTextColor(Color.RED);
                            } else {
                                aqAdolVisit.id(R.id.etAdolVisitheightcm).getEditText().setError(getResources().getString(R.string.heightis) + cm);
                            }
                        } else {
                            aqAdolVisit.id(R.id.tvHeight).getTextView().setTextColor(Color.BLACK);
                        }
                    } else {
                        if (!(cm >= 162 && cm <= 173)) {
                            if (aqAdolVisit.id(R.id.rd_feet).isChecked()) {
                                aqAdolVisit.id(R.id.tvHeight).getTextView().setTextColor(Color.RED);
                            } else {
                                aqAdolVisit.id(R.id.etAdolVisitheightcm).getEditText().setError(getResources().getString(R.string.heightis) + cm);
                            }
                        } else {
                            aqAdolVisit.id(R.id.tvHeight).getTextView().setTextColor(Color.BLACK);
                        }
                    }

                    break;
                case 16:
                    if (aqAdolVisit.id(R.id.rd_male).isChecked()) {
                        if (!(cm >= 174 && cm <= 186)) {
                            if (aqAdolVisit.id(R.id.rd_feet).isChecked()) {
                                aqAdolVisit.id(R.id.tvHeight).getTextView().setTextColor(Color.RED);
                            } else {
                                aqAdolVisit.id(R.id.etAdolVisitheightcm).getEditText().setError(getResources().getString(R.string.heightis) + cm);
                            }
                        } else {
                            aqAdolVisit.id(R.id.tvHeight).getTextView().setTextColor(Color.BLACK);
                        }
                    } else {
                        if (!(cm >= 163 && cm <= 173)) {
                            if (aqAdolVisit.id(R.id.rd_feet).isChecked()) {
                                aqAdolVisit.id(R.id.tvHeight).getTextView().setTextColor(Color.RED);
                            } else {
                                aqAdolVisit.id(R.id.etAdolVisitheightcm).getEditText().setError(getResources().getString(R.string.heightis) + cm);
                            }
                        } else {
                            aqAdolVisit.id(R.id.tvHeight).getTextView().setTextColor(Color.BLACK);
                        }
                    }

                    break;
                case 17:
                    if (aqAdolVisit.id(R.id.rd_male).isChecked()) {
                        if (!(cm >= 175 && cm <= 188)) {
                            if (aqAdolVisit.id(R.id.rd_feet).isChecked()) {
                                aqAdolVisit.id(R.id.tvHeight).getTextView().setTextColor(Color.RED);
                            } else {
                                aqAdolVisit.id(R.id.etAdolVisitheightcm).getEditText().setError(getResources().getString(R.string.heightis) + cm);
                            }
                        } else {
                            aqAdolVisit.id(R.id.tvHeight).getTextView().setTextColor(Color.BLACK);
                        }
                    } else {
                        if (!(cm >= 163 && cm <= 174)) {
                            if (aqAdolVisit.id(R.id.rd_feet).isChecked()) {
                                aqAdolVisit.id(R.id.tvHeight).getTextView().setTextColor(Color.RED);
                            } else {
                                aqAdolVisit.id(R.id.etAdolVisitheightcm).getEditText().setError(getResources().getString(R.string.heightis) + cm);
                            }
                        } else {
                            aqAdolVisit.id(R.id.tvHeight).getTextView().setTextColor(Color.BLACK);
                        }
                    }
                    break;
                case 18:
                    if (aqAdolVisit.id(R.id.rd_male).isChecked()) {
                        if (!(cm >= 176 && cm <= 189)) {
                            if (aqAdolVisit.id(R.id.rd_feet).isChecked()) {
                                aqAdolVisit.id(R.id.tvHeight).getTextView().setTextColor(Color.RED);
                            } else {
                                aqAdolVisit.id(R.id.etAdolVisitheightcm).getEditText().setError(getResources().getString(R.string.heightis) + cm);
                            }
                        } else {
                            aqAdolVisit.id(R.id.tvHeight).getTextView().setTextColor(Color.BLACK);
                        }
                    } else {
                        if (!(cm >= 163 && cm <= 174)) {
                            if (aqAdolVisit.id(R.id.rd_feet).isChecked()) {
                                aqAdolVisit.id(R.id.tvHeight).getTextView().setTextColor(Color.RED);
                            } else {
                                aqAdolVisit.id(R.id.etAdolVisitheightcm).getEditText().setError(getResources().getString(R.string.heightis) + cm);
                            }
                        } else {
                            aqAdolVisit.id(R.id.tvHeight).getTextView().setTextColor(Color.BLACK);
                        }
                    }
                    break;
                case 19:
                    if (aqAdolVisit.id(R.id.rd_male).isChecked()) {
                        if (!(cm >= 179 && cm <= 189)) {
                            if (aqAdolVisit.id(R.id.rd_feet).isChecked()) {
                                aqAdolVisit.id(R.id.tvHeight).getTextView().setTextColor(Color.RED);
                            } else {
                                aqAdolVisit.id(R.id.etAdolVisitheightcm).getEditText().setError(getResources().getString(R.string.heightis) + cm);
                            }
                        } else {
                            aqAdolVisit.id(R.id.tvHeight).getTextView().setTextColor(Color.BLACK);
                        }
                    } else {
                        if (!(cm >= 163 && cm <= 174)) {
                            if (aqAdolVisit.id(R.id.rd_feet).isChecked()) {
                                aqAdolVisit.id(R.id.tvHeight).getTextView().setTextColor(Color.RED);
                            } else {
                                aqAdolVisit.id(R.id.etAdolVisitheightcm).getEditText().setError(getResources().getString(R.string.heightis) + cm);
                            }
                        } else {
                            aqAdolVisit.id(R.id.tvHeight).getTextView().setTextColor(Color.BLACK);
                        }
                    }
                    break;
                default:
                    if (aqAdolVisit.id(R.id.rd_feet).isChecked()) {
                        aqAdolVisit.id(R.id.tvHeight).getTextView().setTextColor(Color.RED);
                    } else {
                        aqAdolVisit.id(R.id.etAdolVisitheightcm).getEditText().setError(getResources().getString(R.string.heightis) + cm);
                    }
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private void WeightWarning(double weight) {
        try {
            switch (Integer.parseInt(currentAdolDetails.getRegAdolAge())) {
                case 10:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(weight >= 33 && weight <= 46)) {
                            aqAdolVisit.id(R.id.etAdolVisitWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                        }
                    } else {
                        if (!(weight >= 33 && weight <= 47)) {
                            aqAdolVisit.id(R.id.etAdolVisitWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                        }
                    }
                    break;
                case 11:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(weight >= 36 && weight <= 53)) {
                            aqAdolVisit.id(R.id.etAdolVisitWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                        }
                    } else {
                        if (!(weight >= 37 && weight <= 55)) {
                            aqAdolVisit.id(R.id.etAdolVisitWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                        }
                    }
                    break;
                case 12:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(weight >= 41 && weight <= 58)) {
                            aqAdolVisit.id(R.id.etAdolVisitWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                        }
                    } else {
                        if (!(weight >= 42 && weight <= 61)) {
                            aqAdolVisit.id(R.id.etAdolVisitWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                        }
                    }
                    break;
                case 13:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(weight >= 46 && weight <= 66)) {
                            aqAdolVisit.id(R.id.etAdolVisitWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                        }
                    } else {
                        if (!(weight >= 46 && weight <= 66)) {
                            aqAdolVisit.id(R.id.etAdolVisitWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                        }
                    }
                    break;
                case 14:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(weight >= 51 && weight <= 72)) {
                            aqAdolVisit.id(R.id.etAdolVisitWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                        }
                    } else {
                        if (!(weight >= 49 && weight <= 72)) {
                            aqAdolVisit.id(R.id.etAdolVisitWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                        }
                    }

                    break;
                case 15:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(weight >= 56 && weight <= 79)) {
                            aqAdolVisit.id(R.id.etAdolVisitWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                        }
                    } else {
                        if (!(weight >= 53 && weight <= 76)) {
                            aqAdolVisit.id(R.id.etAdolVisitWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                        }
                    }

                    break;
                case 16:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(weight >= 61 && weight <= 84)) {
                            aqAdolVisit.id(R.id.etAdolVisitWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                        }
                    } else {
                        if (!(weight >= 54 && weight <= 77)) {
                            aqAdolVisit.id(R.id.etAdolVisitWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                        }
                    }
                    break;
                case 17:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(weight >= 64 && weight <= 88)) {
                            aqAdolVisit.id(R.id.etAdolVisitWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                        }
                    } else {
                        if (!(weight >= 55 && weight <= 79)) {
                            aqAdolVisit.id(R.id.etAdolVisitWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                        }
                    }
                    break;
                case 18:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(weight >= 67 && weight <= 92)) {
                            aqAdolVisit.id(R.id.etAdolVisitWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                        }
                    } else {
                        if (!(weight >= 56 && weight <= 81)) {
                            aqAdolVisit.id(R.id.etAdolVisitWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                        }
                    }
                    break;
                case 19:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(weight >= 69 && weight <= 93)) {
                            aqAdolVisit.id(R.id.etAdolVisitWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                        }
                    } else {
                        if (!(weight >= 57 && weight <= 83)) {
                            aqAdolVisit.id(R.id.etAdolVisitWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                        }
                    }

                    break;
                default:
                    aqAdolVisit.id(R.id.etAdolVisitWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private void calculateBMI(String regAdolHeight, String regAdolWeight) {
        if (regAdolHeight.length() > 0 && regAdolWeight.length() > 0) {
            try {
                double height = Double.parseDouble(regAdolHeight);
                double meter = height * 0.01;
                double weight = Double.parseDouble(regAdolWeight);
                BMI = weight / (meter * meter);

                aqAdolVisit.id(R.id.trAdolVisitBMI).getView().setVisibility(View.VISIBLE);
                aqAdolVisit.id(R.id.txtAdolVisitBMI).getTextView().setText(String.format("%.2f", BMI));
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());

            }
        } else {
            aqAdolVisit.id(R.id.txtAdolVisitBMI).getTextView().setText("-");
        }
    }

    private void calculateBMI(int feet, int inch, String weights) {
        if (weights.length() > 0) {
            try {
                double meter = (feet + inch / 12.0) * 0.3048;
                double weight = Double.parseDouble(weights);
                BMI = weight / (meter * meter);

                aqAdolVisit.id(R.id.trAdolVisitBMI).getView().setVisibility(View.VISIBLE);
                aqAdolVisit.id(R.id.txtAdolVisitBMI).getTextView().setText(String.format("%.2f", BMI));
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());

            }
        } else {
            aqAdolVisit.id(R.id.txtAdolVisitBMI).getTextView().setText("-");
        }
    }

    @Override
    public void onClick(View view) {
        try {
            switch (view.getId()) {
                case R.id.lladdphotosid:
                    opencloseAccordion(aqAdolVisit.id(R.id.lladdphotosdetails).getView(), aqAdolVisit.id(R.id.txtaddphotos).getTextView());
                    aqAdolVisit.id(R.id.lladdphotosdetails).backgroundColor(getResources().getColor(R.color.lightgray));
                    if (aqAdolVisit.id(R.id.lladdphotosdetails).getView().getVisibility() == View.VISIBLE) {
                        AsyncImage asyncImage = new AsyncImage();
                        asyncImage.execute();
                    }
                    break;
                case R.id.lleduid:
                    opencloseAccordion(aqAdolVisit.id(R.id.lledudetails).getView(), aqAdolVisit.id(R.id.txtedu).getTextView());
                    aqAdolVisit.id(R.id.lledudetails).backgroundColor(getResources().getColor(R.color.lightgray));
                    break;
//mani 19Aug2021
                case R.id.llashaavailableid:
                    opencloseAccordion(aqAdolVisit.id(R.id.llashaavailable).getView(), aqAdolVisit.id(R.id.txtashaavail).getTextView());
                    aqAdolVisit.id(R.id.llashaavailable).backgroundColor(getResources().getColor(R.color.lightgray));
                    break;
                case R.id.llmenstpbmid:
                    opencloseAccordion(aqAdolVisit.id(R.id.llmenstpbmdetails).getView(), aqAdolVisit.id(R.id.txtmenstpbm).getTextView());
                    aqAdolVisit.id(R.id.llmenstpbmdetails).backgroundColor(getResources().getColor(R.color.lightgray));
                    break;
                case R.id.llmaritalstatid:
                    opencloseAccordion(aqAdolVisit.id(R.id.llmaritalstatdetails).getView(), aqAdolVisit.id(R.id.txtmaritalstat).getTextView());
                    aqAdolVisit.id(R.id.llmaritalstatdetails).backgroundColor(getResources().getColor(R.color.lightgray));
                    break;
                case R.id.llexamid:
                    opencloseAccordion(aqAdolVisit.id(R.id.llexamdetails).getView(), aqAdolVisit.id(R.id.txtexam).getTextView());
                    aqAdolVisit.id(R.id.llexamdetails).backgroundColor(getResources().getColor(R.color.lightgray));
                    break;
                case R.id.lltabid:
                    opencloseAccordion(aqAdolVisit.id(R.id.lltabdetails).getView(), aqAdolVisit.id(R.id.txttab).getTextView());
                    aqAdolVisit.id(R.id.lltabdetails).backgroundColor(getResources().getColor(R.color.lightgray));
                    break;
                case R.id.llcontraceptionid:
                    opencloseAccordion(aqAdolVisit.id(R.id.llcontraceptiondetails).getView(), aqAdolVisit.id(R.id.txtcontraception).getTextView());
                    aqAdolVisit.id(R.id.llcontraceptiondetails).backgroundColor(getResources().getColor(R.color.lightgray));
                    break;
                case R.id.llhealthdetid:
                    opencloseAccordion(aqAdolVisit.id(R.id.llhealthdetdetails).getView(), aqAdolVisit.id(R.id.txthealthdet).getTextView());
                    aqAdolVisit.id(R.id.llhealthdetdetails).backgroundColor(getResources().getColor(R.color.lightgray));
                    break;
                case R.id.llcovidheader:
                    opencloseAccordion(aqAdolVisit.id(R.id.llCovidVaccineQs).getView(), aqAdolVisit.id(R.id.txtcovidheadertitle).getTextView());
                    aqAdolVisit.id(R.id.llCovidVaccineQs).backgroundColor(getResources().getColor(R.color.lightgray));
                    break;
                case R.id.etAdolVisitCovidTestDate:
                    assignEditTextAndCallDatePicker(aqAdolVisit.id(R.id.etAdolVisitCovidTestDate).getEditText());
                    break;
                case R.id.etAdolVisitCovidFirstDoseDate:
                    assignEditTextAndCallDatePicker(aqAdolVisit.id(R.id.etAdolVisitCovidFirstDoseDate).getEditText());
                    break;
                case R.id.etAdolVisitCovidSecondDoseDate:
                    assignEditTextAndCallDatePicker(aqAdolVisit.id(R.id.etAdolVisitCovidSecondDoseDate).getEditText());
                    break;
                case R.id.etAdolVisitHBTestDate:
                    assignEditTextAndCallDatePicker(aqAdolVisit.id(R.id.etAdolVisitHBTestDate).getEditText());
                    break;
            }

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }

    }

    private void assignEditTextAndCallDatePicker(EditText editText) {
        currentEditText = editText;
        String currDate = (editText.getText().length() > 0) ? editText.getText().toString() : "";
        CommonDatePickerFragment.getCommonDate(this, currDate);
    }

    public void opencloseAccordion(View v, TextView txt) {
        try {
            if (v.getVisibility() == View.VISIBLE) {
                v.setVisibility(View.GONE);
                if (txt.getCurrentTextColor() == getResources().getColor(R.color.red))
                    txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.collapse, 0);
                else
                    txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
            } else {
                v.setVisibility(View.VISIBLE);
                txt.setTextColor(getResources().getColor(R.color.red));
                txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);

            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
        try {
            if (datePicker.isShown()) {
                String strseldate = null;
                strseldate = String.format("%02d", dayOfMonth) + "-" + String.format("%02d", month + 1) + "-"
                        + year;

                @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                Date seldate = format.parse(strseldate);
                Calendar myCal = Calendar.getInstance();
                myCal.set(Calendar.YEAR, 2002);
                myCal.set(Calendar.MONTH, 1);
                myCal.set(Calendar.DAY_OF_MONTH, 1);

                Date firstdosedate = null, secdosedate = null;
                if (aqAdolVisit.id(R.id.etAdolVisitCovidFirstDoseDate).getText().toString().length() > 0)
                    firstdosedate = format.parse(aqAdolVisit.id(R.id.etAdolVisitCovidFirstDoseDate).getText().toString());

                if (aqAdolVisit.id(R.id.etAdolVisitCovidSecondDoseDate).getText().toString().length() > 0)
                    secdosedate = format.parse(aqAdolVisit.id(R.id.etAdolVisitCovidSecondDoseDate).getText().toString());

                if (currentEditText == aqAdolVisit.id(R.id.etAdolVisitCovidTestDate).getEditText()) {
                    if (seldate != null && seldate.after(new Date())) {
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.date_cannot_af_currentdate)
                                , this);
                        aqAdolVisit.id(R.id.etAdolVisitCovidTestDate).text("");
                        aqAdolVisit.id(R.id.etAdolVisitCovidTestDate).text("");
                    } else {
                        aqAdolVisit.id(R.id.etAdolVisitCovidTestDate).text(strseldate);
                    }
                }
                if (currentEditText == aqAdolVisit.id(R.id.etAdolVisitCovidFirstDoseDate).getEditText()) {
                    if (seldate != null && seldate.after(new Date())) {
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.date_cannot_af_currentdate)
                                , this);
                        aqAdolVisit.id(R.id.etAdolVisitCovidFirstDoseDate).text("");
                    } else if (secdosedate != null && seldate.after(secdosedate)) { // first dose cant be after sec dose
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.datecantbeaftersecdosedate)
                                , this);
                        aqAdolVisit.id(R.id.etAdolVisitCovidFirstDoseDate).text("");
                    } else {
                        aqAdolVisit.id(R.id.etAdolVisitCovidFirstDoseDate).text(strseldate);
                    }
                }
                if (currentEditText == aqAdolVisit.id(R.id.etAdolVisitCovidSecondDoseDate).getEditText()) {
                    if (seldate != null && seldate.after(new Date())) {
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.date_cannot_af_currentdate)
                                , this);
                        aqAdolVisit.id(R.id.etAdolVisitCovidSecondDoseDate).text("");
                    } else if (firstdosedate != null && seldate.before(firstdosedate)) { // sec dose cant be bef first dose
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.datecantbebefrefirstdosedate)
                                , this);
                        aqAdolVisit.id(R.id.etAdolVisitCovidSecondDoseDate).text("");
                    } else if (firstdosedate != null && firstdosedate.equals(seldate)) {
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.datecantbesame)
                                , this);
                        aqAdolVisit.id(R.id.etAdolVisitCovidSecondDoseDate).text("");
                        if (aqAdolVisit.id(R.id.etAdolVisitCovidFirstDoseDate).getEditText().isEnabled()) {
                            aqAdolVisit.id(R.id.etAdolVisitCovidFirstDoseDate).text("");
                        }
                    } else {
                        aqAdolVisit.id(R.id.etAdolVisitCovidSecondDoseDate).text(strseldate);
                    }
                }
                if (currentEditText == aqAdolVisit.id(R.id.etAdolVisitHBTestDate).getEditText()) {
                    if (seldate != null && seldate.after(new Date())) {
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.date_cannot_af_currentdate)
                                , this);
                        aqAdolVisit.id(R.id.etAdolVisitHBTestDate).text("");
                        aqAdolVisit.id(R.id.etAdolVisitHBTestDate).text("");
                    } else {
                        aqAdolVisit.id(R.id.etAdolVisitHBTestDate).text(strseldate);
                    }
                }else//09Sep2021 Arpitha
                {
                    String str = String.format("%02d", dayOfMonth) + "-" + String.format("%02d", month + 1) + "-"
                            + year;

                    int days = DateTimeUtil.getDaysBetweenDates(DateTimeUtil.getTodaysDate(), str);

                    if (!aqPMSel.id(R.id.radEDD).isChecked()) {
                        if (days >= 0) {
                            if (isRegDate) {
                                if (days < 30) {
                                    aqPMSel.id(R.id.etregdate).text(str);
                                    aqPMSel.id(R.id.imgclearregadate).visible();//20AUg2019 Arpitha
                                } else if (days > 120) {
                                    aqPMSel.id(R.id.etregdate).text("");
                                    aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                                    Toast.makeText(activity, getResources().getString(R.string.date_val_2_toast),
                                            Toast.LENGTH_LONG).show();
                                    aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha

                                } else {
                                    aqPMSel.id(R.id.etregdate).text(str);
                                    aqPMSel.id(R.id.imgclearregadate).visible();//20AUg2019 Arpitha
                                }

                            } else if (aqPMSel.id(R.id.radLMP).isChecked()) {
                                String lmporadd = str;
                                int daysdiff = 0;
                                String regdate = aqPMSel.id(R.id.etregdate).getText().toString();

                                if (regdate != null && regdate.trim().length() > 0) {
                                    daysdiff = DateTimeUtil.getDaysBetweenDates(regdate, lmporadd);

                                    int daysdifffromCUrr = DateTimeUtil
                                            .getDaysBetweenDates(DateTimeUtil.getTodaysDate(), lmporadd);

                                    if (daysdiff <= 30) {
                                        Toast.makeText(activity,
                                                getResources().getString(R.string.date_val_3_toast),
                                                Toast.LENGTH_LONG).show();
                                        aqPMSel.id(R.id.etlmpdate).text("");
                                        aqPMSel.id(R.id.etgest).text("");
                                        aqPMSel.id(R.id.etMotherAdd).text("");
                                        aqPMSel.id(R.id.imgclearlmp).gone();//20Aug2019 Arpitha
                                    } else if (daysdifffromCUrr > 280) {
                                        Toast.makeText(activity,
                                                getResources().getString(R.string.date_val_4_toast),
                                                Toast.LENGTH_LONG).show();
                                        aqPMSel.id(R.id.etlmpdate).text("");
                                        aqPMSel.id(R.id.etgest).text("");
                                        aqPMSel.id(R.id.etMotherAdd).text("");
                                        aqPMSel.id(R.id.imgclearlmp).gone();//20Aug2019 Arpitha
                                    } else {
                                        aqPMSel.id(R.id.etMotherAdd).text(str);
                                        calculateEddGestation("lmp");
                                        aqPMSel.id(R.id.imgclearlmp).visible();//20Aug2019 Arpitha
                                    }
                                } else
                                    Toast.makeText(activity, getResources().getString(R.string.plz_enter_reg_dt),
                                            Toast.LENGTH_LONG).show();
                            } else if (aqPMSel.id(R.id.rdbMother).isChecked()) {

                                int daysdiff = DateTimeUtil
                                        .getDaysBetweenDates(aqPMSel.id(R.id.etregdate).getText().toString(), str);
                                if (daysdiff > 365) {// Arpitha 28Jun2018 - 23Dec2019 Arpitha - allow max 1 year

                                    Toast.makeText(activity, (getResources().getString(R.string.datecannotbeafteroneyearfromcurrentdate)), // 11July2018
                                            // Arpitha
                                            Toast.LENGTH_LONG).show();
                                    aqPMSel.id(R.id.etMotherAdd).text("");
                                } else
                                    aqPMSel.id(R.id.etMotherAdd).text(str);
                            } else if (aqPMSel.id(R.id.rdbchild).isChecked()) {

                                int daysdiff = DateTimeUtil
                                        .getDaysBetweenDates(aqPMSel.id(R.id.etregdate).getText().toString(), str);
                                if (daysdiff > 5844) {// Arpitha 28Jun2018

                                    Toast.makeText(activity, (getResources().getString(R.string.date_val_7_toast)), // 11July2018
                                            // Arpitha
                                            Toast.LENGTH_LONG).show();
                                    aqPMSel.id(R.id.etMotherAdd).text("");
                                } else
                                    aqPMSel.id(R.id.etMotherAdd).text(str);
                            }
                        } else {
                            if (isRegDate) {
                                aqPMSel.id(R.id.etregdate).text("");
                                aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                                Toast.makeText(activity,
                                        getResources().getString(R.string.reg_date_cannot_greater_current_date),
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha
                            } else {
                                aqPMSel.id(R.id.etMotherAdd).text("");
                                aqPMSel.id(R.id.imgclearlmp).gone();//20AUg2019 Arpitha
                                aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                                if (aqPMSel.id(R.id.rdbPreg).isChecked()) {
                                    Toast.makeText(activity,
                                            getResources()
                                                    .getString(R.string.lmp_date_time_cannot_be_after_curre_datetime), // 11July2018

                                            Toast.LENGTH_LONG).show();

                                    aqPMSel.id(R.id.etlmpdate).text("");
                                    aqPMSel.id(R.id.etgest).text("");
                                } else {
                                    Toast.makeText(activity,
                                            getResources()
                                                    .getString(R.string.del_date_cannot_be_after_current_date_time), // 11July2018
                                            Toast.LENGTH_LONG).show();
                                }

                            }
                        }
                    } else {
                        if (!isRegDate) {
                            int days1 = 0;
                            if (aqPMSel.id(R.id.etregdate).getText().toString().trim().length() > 0)
                                days1 = DateTimeUtil.getDaysBetweenDates(str,
                                        aqPMSel.id(R.id.etregdate).getText().toString());

                            if (days1 > 0 && days1 < 273) {
                                aqPMSel.id(R.id.etMotherAdd).text(str);
                                calculateEddGestation("edd");
                                aqPMSel.id(R.id.imgclearlmp).visible();//20Aug2019 Arpitha
                            } else {
                                if (days1 > 280) {
                                    aqPMSel.id(R.id.etMotherAdd).text("");
                                    aqPMSel.id(R.id.imgclearlmp).gone();//21Aug2019 Arpitha
                                    aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                                    Toast.makeText(activity, getResources().getString(R.string.date_val_6_toast),

                                            Toast.LENGTH_LONG).show();

                                    aqPMSel.id(R.id.etlmpdate).text("");
                                    aqPMSel.id(R.id.etgest).text("");

                                } else {
                                    aqPMSel.id(R.id.etMotherAdd).text("");
                                    aqPMSel.id(R.id.imgclearlmp).gone();//21Aug2019 Arpitha
                                    aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                                    Toast.makeText(activity, getResources().getString(R.string.date_val_5_toast),
                                            Toast.LENGTH_LONG).show();
                                    aqPMSel.id(R.id.etlmpdate).text("");
                                    aqPMSel.id(R.id.etgest).text("");
                                }
                            }
                        } else {
                            days = DateTimeUtil.getDaysBetweenDates(DateTimeUtil.getTodaysDate(), str);
                            if (days >= 0) {

                                if (days > 90) {
                                    aqPMSel.id(R.id.etregdate).text("");
                                    aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                                    Toast.makeText(activity, getResources().getString(R.string.date_val_2_toast),

                                            Toast.LENGTH_LONG).show();
                                    aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha
                                } else {
                                    aqPMSel.id(R.id.etregdate).text(str);
                                }

                            } else {
                                aqPMSel.id(R.id.etregdate).text("");
                                aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                                Toast.makeText(activity, getResources().getString(R.string.reg_date_cannot_greater_current_date),

                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha
                            }
                        }
                    }
                }//09Sep2021 Arpitha
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private boolean validationSufferHealthissue() {
        try {
            if ((!aqAdolVisit.id(R.id.rd_healthissuesYes).isChecked()) && (!aqAdolVisit.id(R.id.rd_healthissuesNo).isChecked())) {
                AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.pleaseselecthealthissue), AdolVisit_New.this);
                aqAdolVisit.id(R.id.llhealthdetdetails).getView().setVisibility(View.VISIBLE);
                if (aqAdolVisit.id(R.id.txthealthdet).getTextView().getCurrentTextColor() == getResources().getColor(R.color.red))
                    aqAdolVisit.id(R.id.txthealthdet).getTextView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
                else
                    aqAdolVisit.id(R.id.txthealthdet).getTextView().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.expand, 0);
                aqAdolVisit.id(R.id.llhealthdetdetails).backgroundColor(getResources().getColor(R.color.lightgray));
                return false;
            } else if (aqAdolVisit.id(R.id.rd_healthissuesYes).isChecked() && spnHealthProblems.getSelectedItemsAsString().length() == 0) {
                AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.pleaseselecthealthissue), AdolVisit_New.this);
                aqAdolVisit.id(R.id.llhealthdetdetails).getView().setVisibility(View.VISIBLE);
                if (aqAdolVisit.id(R.id.txthealthdet).getTextView().getCurrentTextColor() == getResources().getColor(R.color.red))
                    aqAdolVisit.id(R.id.txthealthdet).getTextView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
                else
                    aqAdolVisit.id(R.id.txthealthdet).getTextView().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.expand, 0);
                aqAdolVisit.id(R.id.llhealthdetdetails).backgroundColor(getResources().getColor(R.color.lightgray));
                return false;
            } else if (aqAdolVisit.id(R.id.rd_healthissuesYes).isChecked() && spnHealthProblems.getSelectedIndicies().toString().contains("9")) {
                if (aqAdolVisit.id(R.id.etAdolVisitHealthIssuesYesOthers).getEditText().getText().length() == 0) {
                    AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.pleaseselecthealthissueother), AdolVisit_New.this);
                    aqAdolVisit.id(R.id.llhealthdetdetails).getView().setVisibility(View.VISIBLE);
                    if (aqAdolVisit.id(R.id.txthealthdet).getTextView().getCurrentTextColor() == getResources().getColor(R.color.red))
                        aqAdolVisit.id(R.id.txthealthdet).getTextView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
                    else
                        aqAdolVisit.id(R.id.txthealthdet).getTextView().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.expand, 0);
                    aqAdolVisit.id(R.id.llhealthdetdetails).backgroundColor(getResources().getColor(R.color.lightgray));
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return false;
    }

    private boolean validationContraceptionNeed() {
        try {
            if (currentAdolDetails.getRegAdolGender().equals("Female")) {
                if ((!aqAdolVisit.id(R.id.rd_needcontaceptionYes).isChecked()) && (!aqAdolVisit.id(R.id.rd_needcontaceptionNo).isChecked())) {
                    AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.pleaseselectneedcontraception), AdolVisit_New.this);
                    aqAdolVisit.id(R.id.llcontraceptiondetails).getView().setVisibility(View.VISIBLE);
                    if (aqAdolVisit.id(R.id.txtcontraception).getTextView().getCurrentTextColor() == getResources().getColor(R.color.red))
                        aqAdolVisit.id(R.id.txtcontraception).getTextView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
                    else
                        aqAdolVisit.id(R.id.txtcontraception).getTextView().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.expand, 0);
                    aqAdolVisit.id(R.id.llcontraceptiondetails).backgroundColor(getResources().getColor(R.color.lightgray));
                    return false;
                } else if (aqAdolVisit.id(R.id.rd_needcontaceptionYes).isChecked() && aqAdolVisit.id(R.id.spnadolVisitContraceptionMethod).getSpinner().getSelectedItemPosition() == 0) {
                    AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.pleaseselectcontraceptionmethod), AdolVisit_New.this);
                    aqAdolVisit.id(R.id.llcontraceptiondetails).getView().setVisibility(View.VISIBLE);
                    if (aqAdolVisit.id(R.id.txtcontraception).getTextView().getCurrentTextColor() == getResources().getColor(R.color.red))
                        aqAdolVisit.id(R.id.txtcontraception).getTextView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
                    else
                        aqAdolVisit.id(R.id.txtcontraception).getTextView().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.expand, 0);
                    aqAdolVisit.id(R.id.llcontraceptiondetails).backgroundColor(getResources().getColor(R.color.lightgray));
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return false;
    }

    private boolean validationContraceptionAware() {
        try {
            if (currentAdolDetails.getRegAdolGender().equals("Female")) {
                if ((!aqAdolVisit.id(R.id.rd_awarecontaceptionYes).isChecked()) && (!aqAdolVisit.id(R.id.rd_awarecontaceptionNo).isChecked())) {
                    AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.pleaseselectawarecontraception), AdolVisit_New.this);
                    opencloseAccordion(aqAdolVisit.id(R.id.llcontraceptiondetails).getView(), aqAdolVisit.id(R.id.txtcontraception).getTextView());
                    aqAdolVisit.id(R.id.llcontraceptiondetails).getView().setVisibility(View.VISIBLE);
                    if (aqAdolVisit.id(R.id.txtcontraception).getTextView().getCurrentTextColor() == getResources().getColor(R.color.red))
                        aqAdolVisit.id(R.id.txtcontraception).getTextView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
                    else
                        aqAdolVisit.id(R.id.txtcontraception).getTextView().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.expand, 0);
                    aqAdolVisit.id(R.id.llcontraceptiondetails).backgroundColor(getResources().getColor(R.color.lightgray));
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return false;
    }

    private boolean validationIFA() {
        try {
            if ((!aqAdolVisit.id(R.id.rd_IFAYes).isChecked()) && (!aqAdolVisit.id(R.id.rd_IFANo).isChecked())) {
                AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.pleaseselectifatablets), AdolVisit_New.this);
                opencloseAccordion(aqAdolVisit.id(R.id.lltabdetails).getView(), aqAdolVisit.id(R.id.txttab).getTextView());
                aqAdolVisit.id(R.id.lltabdetails).getView().setVisibility(View.VISIBLE);
                if (aqAdolVisit.id(R.id.txttab).getTextView().getCurrentTextColor() == getResources().getColor(R.color.red))
                    aqAdolVisit.id(R.id.txttab).getTextView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
                else
                    aqAdolVisit.id(R.id.txttab).getTextView().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.expand, 0);
                aqAdolVisit.id(R.id.lltabdetails).backgroundColor(getResources().getColor(R.color.lightgray));
                return false;
            } else if (aqAdolVisit.id(R.id.rd_IFAYes).isChecked()) {
                if ((!aqAdolVisit.id(R.id.rd_ConsumingIFAYes).isChecked()) && (!aqAdolVisit.id(R.id.rd_ConsumingIFANo).isChecked())) {
                    AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.pleaseselectdoyouconsumetablets), AdolVisit_New.this);
                    opencloseAccordion(aqAdolVisit.id(R.id.lltabdetails).getView(), aqAdolVisit.id(R.id.txttab).getTextView());
                    aqAdolVisit.id(R.id.lltabdetails).getView().setVisibility(View.VISIBLE);
                    if (aqAdolVisit.id(R.id.txttab).getTextView().getCurrentTextColor() == getResources().getColor(R.color.red))
                        aqAdolVisit.id(R.id.txttab).getTextView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
                    else
                        aqAdolVisit.id(R.id.txttab).getTextView().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.expand, 0);
                    aqAdolVisit.id(R.id.lltabdetails).backgroundColor(getResources().getColor(R.color.lightgray));
                    return false;
                } else if (aqAdolVisit.id(R.id.rd_ConsumingIFAYes).isChecked() && aqAdolVisit.id(R.id.etIFANoofTabletsConsumes).getEditText().getText().length() == 0) {
                    AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.pleaseselecttabletscomsumeperday), AdolVisit_New.this);
                    opencloseAccordion(aqAdolVisit.id(R.id.lltabdetails).getView(), aqAdolVisit.id(R.id.txttab).getTextView());
                    aqAdolVisit.id(R.id.lltabdetails).getView().setVisibility(View.VISIBLE);
                    if (aqAdolVisit.id(R.id.txttab).getTextView().getCurrentTextColor() == getResources().getColor(R.color.red))
                        aqAdolVisit.id(R.id.txttab).getTextView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
                    else
                        aqAdolVisit.id(R.id.txttab).getTextView().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.expand, 0);
                    aqAdolVisit.id(R.id.lltabdetails).backgroundColor(getResources().getColor(R.color.lightgray));
                    return false;
                } else if (aqAdolVisit.id(R.id.rd_ConsumingIFANo).isChecked() && spnIFAReasons.getSelectedIndicies().size() == 0) {
                    AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.pleaseenterreason), AdolVisit_New.this);
                    opencloseAccordion(aqAdolVisit.id(R.id.lltabdetails).getView(), aqAdolVisit.id(R.id.txttab).getTextView());
                    aqAdolVisit.id(R.id.lltabdetails).getView().setVisibility(View.VISIBLE);
                    if (aqAdolVisit.id(R.id.txttab).getTextView().getCurrentTextColor() == getResources().getColor(R.color.red))
                        aqAdolVisit.id(R.id.txttab).getTextView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
                    else
                        aqAdolVisit.id(R.id.txttab).getTextView().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.expand, 0);
                    aqAdolVisit.id(R.id.lltabdetails).backgroundColor(getResources().getColor(R.color.lightgray));
                    return false;
                }
                if (aqAdolVisit.id(R.id.spnWhoGaveIFATablets).getSpinner().getSelectedItemPosition() == 0) {
                    AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.pleaseselectwhogavefatablets), AdolVisit_New.this);
                    opencloseAccordion(aqAdolVisit.id(R.id.lltabdetails).getView(), aqAdolVisit.id(R.id.txttab).getTextView());
                    aqAdolVisit.id(R.id.lltabdetails).getView().setVisibility(View.VISIBLE);
                    if (aqAdolVisit.id(R.id.txttab).getTextView().getCurrentTextColor() == getResources().getColor(R.color.red))
                        aqAdolVisit.id(R.id.txttab).getTextView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
                    else
                        aqAdolVisit.id(R.id.txttab).getTextView().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.expand, 0);
                    aqAdolVisit.id(R.id.lltabdetails).backgroundColor(getResources().getColor(R.color.lightgray));
                    return false;
                }
                return true;
            } else {
                return true;
            }

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return false;
    }



    //mani 23Aug2021
    private boolean validationashaavailability() {
        try {
            if (!(aqAdolVisit.id(R.id.rd_ashaavailableYes).isChecked() || aqAdolVisit.id(R.id.rd_ashaavailableNo).isChecked() || aqAdolVisit.id(R.id.rd_ashaavailabledontknow).isChecked() )) {
                AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plselectavailabilityasha), AdolVisit_New.this);
                opencloseAccordion(aqAdolVisit.id(R.id.llashaavailable).getView(), aqAdolVisit.id(R.id.txttab).getTextView());
                aqAdolVisit.id(R.id.llashaavailable).getView().setVisibility(View.VISIBLE);
                if (aqAdolVisit.id(R.id.txttab).getTextView().getCurrentTextColor() == getResources().getColor(R.color.red))
                    aqAdolVisit.id(R.id.txttab).getTextView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
                else
                    aqAdolVisit.id(R.id.txttab).getTextView().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.expand, 0);
                aqAdolVisit.id(R.id.llashaavailable).backgroundColor(getResources().getColor(R.color.lightgray));
                return false;
            }else {
                return true;
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return false;
    }

    private boolean validationDeworming() {
        try {
            if ((!aqAdolVisit.id(R.id.rd_dewormingtabletsYes).isChecked()) && (!aqAdolVisit.id(R.id.rd_dewormingtabletsNo).isChecked()) && (!aqAdolVisit.id(R.id.rd_dewormingtabletsDono).isChecked())) {
                AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.pleaseselectdewormingtablet), AdolVisit_New.this);
                opencloseAccordion(aqAdolVisit.id(R.id.lltabdetails).getView(), aqAdolVisit.id(R.id.txttab).getTextView());
                aqAdolVisit.id(R.id.lltabdetails).getView().setVisibility(View.VISIBLE);
                if (aqAdolVisit.id(R.id.txttab).getTextView().getCurrentTextColor() == getResources().getColor(R.color.red))
                    aqAdolVisit.id(R.id.txttab).getTextView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
                else
                    aqAdolVisit.id(R.id.txttab).getTextView().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.expand, 0);
                aqAdolVisit.id(R.id.lltabdetails).backgroundColor(getResources().getColor(R.color.lightgray));
                return false;
            } else {
                return true;
            }

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return false;
    }

    private boolean complicationCheck() {
        try {
            if (currentAdolDetails.getRegAdolGender().equals("Female") && aqAdolVisit.id(R.id.rd_isPeriodStartedYes).isChecked() && spnMensturalProblems.getSelectedIndicies().size() != 0) {
                return true;
            } else if (aqAdolVisit.id(R.id.rd_healthissuesYes).isChecked() && spnHealthProblems.getSelectedIndicies().size() != 0) {
                return true;
            } else if (aqAdolVisit.id(R.id.etAdolVisitHbCount).getEditText().getText().toString().length() != 0) {
                double hb = Double.parseDouble(aqAdolVisit.id(R.id.etAdolVisitHbCount).getEditText().getText().toString());
                if (hb < 8.0) {
                    return true;
                } else if (hb >= 8 && hb <= 9.9) {
                    return true;
                } else if (hb >= 10 && hb <= 11.9) {
                    return true;
                } else if (hb >= 11.9 && hb <= 24) {
                    //
                } else if (hb > 24) {
                    return true;
                }
            } else if (radioButton_cm.isChecked()) {
                if (aqAdolVisit.id(R.id.etAdolVisitheightcm).getEditText().getText().toString().length() > 0) {
                    double cm = Double.parseDouble(aqAdolVisit.id(R.id.etAdolVisitheightcm).getEditText().getText().toString());
                    if (showHeightWarning(cm)) {
                        return true;
                    }
                }
            } else if (radioButton_feet.isChecked()) {
                if (showHeightWarning(0))
                    return true;
            } else if (radioButton_cm.isChecked() || radioButton_cm.isSelected()) {
                if (aqAdolVisit.id(R.id.etAdolVisitheightcm).getEditText().getText().toString().length() > 0) {
                    double cm = Double.parseDouble(aqAdolVisit.id(R.id.etAdolVisitheightcm).getEditText().getText().toString());
                    if (showHeightWarning(cm)) {
                        return true;
                    }
                }
            } else if (radioButton_feet.isChecked()) {
                return aqAdolVisit.id(R.id.spnAdolVisitHeightFeet).getSpinner().getSelectedItemPosition() < 3;
            } else if (aqAdolVisit.id(R.id.etAdolVisitWeight).getEditText().getText().length() != 0) {
                double weight = Double.parseDouble(aqAdolVisit.id(R.id.etAdolVisitWeight).getEditText().getText().toString());
                if (showWeightWarning(weight)) {
                    return true;
                }
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return false;
    }

    private void InsertAuditData() {
        try {
            AuditPojo auditPojo = new AuditPojo();
            auditPojo.setUserId(user.getUserId());
            auditPojo.setWomanId(AdolId);
            auditPojo.setTblName("tblAdolReg");
            auditPojo.setColumnName("LastVisitCount");
            auditPojo.setOld_Value(currentAdolDetails.getLastVisitCount() + "");
            auditPojo.setNew_Value(VISIT_COUNT + "");
            auditPojo.setTransId(transID);
            auditPojo.setRecordcreateddate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
            AuditRepository auditRepository = new AuditRepository(databaseHelper);
            auditRepository.insertTotblAuditTrail(auditPojo);

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private void calculateHB() {
        try {
            float hbCount = Float.parseFloat(aqAdolVisit.id(R.id.etAdolVisitHbCount).getEditText().getText().toString());
            if (hbCount < 8.0) {
                //severe
                aqAdolVisit.id(R.id.txtAdolVisitHBCategory).getTextView().setText(getResources().getString(R.string.severe));
                aqAdolVisit.id(R.id.txtAdolVisitHBCategory).getTextView().setTextColor(Color.RED);
            } else if (hbCount >= 8 && hbCount <= 9.9) {
                //moderate
                aqAdolVisit.id(R.id.txtAdolVisitHBCategory).getTextView().setText(getResources().getString(R.string.moderate));
                aqAdolVisit.id(R.id.txtAdolVisitHBCategory).getTextView().setTextColor(getResources().getColor(R.color.amber));
            } else if (hbCount >= 10 && hbCount <= 11.9) {
                //mild
                aqAdolVisit.id(R.id.txtAdolVisitHBCategory).getTextView().setText(getResources().getString(R.string.mild));
                aqAdolVisit.id(R.id.txtAdolVisitHBCategory).getTextView().setTextColor(getResources().getColor(R.color.orange));
            } else if (hbCount >= 11.9 && hbCount <= 24) {
                //normal
                aqAdolVisit.id(R.id.txtAdolVisitHBCategory).getTextView().setText(getResources().getString(R.string.normal));
                aqAdolVisit.id(R.id.txtAdolVisitHBCategory).getTextView().setTextColor(getResources().getColor(R.color.green_800));
            } else if (hbCount > 24) {
                aqAdolVisit.id(R.id.etAdolVisitHbCount).getEditText().setError(getResources().getString(R.string.cannotbegreaterthan24));
                aqAdolVisit.id(R.id.txtAdolVisitHBCategory).getTextView().setText("NA");
                aqAdolVisit.id(R.id.txtAdolVisitHBCategory).getTextView().setTextColor(Color.RED);
                aqAdolVisit.id(R.id.etAdolVisitHbCount).getEditText().requestFocus();
            }
            aqAdolVisit.id(R.id.etAdolVisitHBTestDate).getEditText().setText(DateTimeUtil.getTodaysDate());
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private boolean showHeightWarning(double cm) {
        try {
            if (radioButton_feet.isChecked()) {
                double feet = Double.parseDouble(aqAdolVisit.id(R.id.spnAdolVisitHeightFeet).getSpinner().getSelectedItem().toString());
                double inches = aqAdolVisit.id(R.id.spnAdolVisitHeightInches).getSpinner().getSelectedItemPosition();
                cm = 30.48 * feet;
                cm = cm + (inches * 2.54);
            }
            switch (Integer.parseInt(currentAdolDetails.getRegAdolAge())) {
                case 10:
                    if (!(cm >= 139 && cm <= 150)) {
                        return true;
                    }
                    break;
                case 11:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(cm >= 144 && cm <= 155)) {
                            return true;
                        }
                    } else {
                        if (!(cm >= 144 && cm <= 156)) {
                            return true;
                        }
                    }

                    break;
                case 12:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(cm >= 149 && cm <= 163)) {
                            return true;
                        }
                    } else {
                        if (!(cm >= 151 && cm <= 163)) {
                            return true;
                        }
                    }
                    break;
                case 13:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(cm >= 156 && cm <= 169)) {
                            return true;
                        }
                    } else {
                        if (!(cm >= 156 && cm <= 169)) {
                            return true;
                        }
                    }
                    break;
                case 14:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(cm >= 166 && cm <= 176)) {
                            return true;
                        }
                    } else {
                        if (!(cm >= 161 && cm <= 173)) {
                            return true;
                        }
                    }
                    break;
                case 15:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(cm >= 170 && cm <= 183)) {
                            return true;
                        }
                    } else {
                        if (!(cm >= 162 && cm <= 173)) {
                            return true;
                        }
                    }

                    break;
                case 16:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(cm >= 174 && cm <= 186)) {
                            return true;
                        }
                    } else {
                        if (!(cm >= 163 && cm <= 173)) {
                            return true;
                        }
                    }
                    break;
                case 17:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(cm >= 175 && cm <= 188)) {
                            return true;
                        }
                    }
                case 18:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(cm >= 176 && cm <= 189)) {
                            return true;
                        }
                    }
                case 19:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(cm >= 179 && cm <= 189)) {
                            return true;
                        }
                    } else {
                        if (!(cm >= 163 && cm <= 174)) {
                            return true;
                        }
                    }
                    break;
                default:

            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return false;
    }

    private boolean showWeightWarning(double weight) {
        try {
            switch (Integer.parseInt(currentAdolDetails.getRegAdolAge())) {
                case 10:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(weight >= 33 && weight <= 46)) {
                            return true;
                        }
                    } else {
                        if (!(weight >= 33 && weight <= 47)) {
                            return true;
                        }
                    }
                    break;
                case 11:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(weight >= 36 && weight <= 53)) {
                            return true;
                        }
                    } else {
                        if (!(weight >= 37 && weight <= 55)) {
                            return true;
                        }
                    }
                    break;
                case 12:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(weight >= 41 && weight <= 58)) {
                            return true;
                        }
                    } else {
                        if (!(weight >= 42 && weight <= 61)) {
                            return true;
                        }
                    }
                    break;
                case 13:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(weight >= 46 && weight <= 66)) {
                            return true;
                        }
                    } else {
                        if (!(weight >= 46 && weight <= 66)) {
                            return true;
                        }
                    }
                    break;
                case 14:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(weight >= 51 && weight <= 72)) {
                            return true;
                        }
                    } else {
                        if (!(weight >= 49 && weight <= 72)) {
                            return true;
                        }
                    }

                    break;
                case 15:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(weight >= 56 && weight <= 79)) {
                            return true;
                        }
                    } else {
                        if (!(weight >= 53 && weight <= 76)) {
                            return true;
                        }
                    }

                    break;
                case 16:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(weight >= 61 && weight <= 84)) {
                            return true;
                        }
                    } else {
                        if (!(weight >= 54 && weight <= 77)) {
                            return true;
                        }
                    }
                    break;
                case 17:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(weight >= 64 && weight <= 88)) {
                            return true;
                        }
                    } else {
                        if (!(weight >= 55 && weight <= 79)) {
                            return true;
                        }
                    }
                    break;
                case 18:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(weight >= 67 && weight <= 92)) {
                            return true;
                        }
                    } else {
                        if (!(weight >= 56 && weight <= 81)) {
                            return true;
                        }
                    }
                    break;
                case 19:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(weight >= 69 && weight <= 93)) {
                            return true;
                        }
                    } else {
                        if (!(weight >= 57 && weight <= 83)) {
                            return true;
                        }
                    }
                    break;
                default:

            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return false;
    }

    private void UIFunctions() {
        try {
            aqAdolVisit.id(R.id.imgSummaryAddPhoto).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (AllImagesBeforeSave != null && AllImagesBeforeSave.size() < 2) {
                        callCamera();
                    } else {
                        AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.maxlimitreached), AdolVisit_New.this);
                    }
                }
            });

            rgCovidTest.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    if (i == R.id.rd_CovidTestYes) {
                        aqAdolVisit.id(R.id.trCovidResult).getView().setVisibility(View.VISIBLE);
                        aqAdolVisit.id(R.id.trCovidTesDate).getView().setVisibility(View.VISIBLE);
                    } else if (i == R.id.rd_CovidTestNo) {
                        aqAdolVisit.id(R.id.trCovidResult).getView().setVisibility(View.GONE);
                        aqAdolVisit.id(R.id.trCovidTesDate).getView().setVisibility(View.GONE);
                        aqAdolVisit.id(R.id.etAdolVisitCovidTestDate).text("");
                        rgCovidResult.clearCheck();
                    }
                }
            });

            rgCovidVaccine.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    if (i == R.id.rd_CovidVaccineYes) {
                        aqAdolVisit.id(R.id.trCovidVaccinDose).getView().setVisibility(View.VISIBLE);

                    } else if (i == R.id.rd_CovidVaccineNo) {
                        aqAdolVisit.id(R.id.trCovidVaccinDose).getView().setVisibility(View.GONE);
                        aqAdolVisit.id(R.id.trCovidFirstDoseDate).getView().setVisibility(View.GONE);
                        aqAdolVisit.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.GONE);
                        aqAdolVisit.id(R.id.spnadolVisitVaccineDose).getSpinner().setSelection(0);
                    }
                }
            });

            aqAdolVisit.id(R.id.spnadolVisitVaccineDose).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (covVacItems != null && covVacItems.size() == 0) {
                        aqAdolVisit.id(R.id.etAdolVisitCovidFirstDoseDate).text("");
                        aqAdolVisit.id(R.id.etAdolVisitCovidSecondDoseDate).text("");
                        aqAdolVisit.id(R.id.trCovidFirstDoseDate).getView().setVisibility(View.GONE);
                        aqAdolVisit.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.GONE);
                        if (i == 1) {
                            aqAdolVisit.id(R.id.trCovidFirstDoseDate).getView().setVisibility(View.VISIBLE);
                            aqAdolVisit.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.GONE);
                        } else if (i == 2) {
                            aqAdolVisit.id(R.id.trCovidFirstDoseDate).getView().setVisibility(View.VISIBLE);
                            aqAdolVisit.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.VISIBLE);
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            spnHealthProblems.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (spnHealthProblems.getSelectedIndicies().size() != 0) {
                        aqAdolVisit.id(R.id.llCovidQs).getView().setVisibility(View.VISIBLE);
                    } else {
                        aqAdolVisit.id(R.id.llCovidQs).getView().setVisibility(View.GONE);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            rgContraceptionNeed.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    TableRow trMethodContraception = findViewById(R.id.trContraceptionMethod);
                    TableRow trMethodContraceptionOther = findViewById(R.id.trContraceptionMethodOther);
                    if (i == R.id.rd_needcontaceptionYes) {
                        trMethodContraception.setVisibility(View.VISIBLE);
                    } else if (i == R.id.rd_needcontaceptionNo) {
                        trMethodContraception.setVisibility(View.GONE);
                        trMethodContraceptionOther.setVisibility(View.GONE);
                        aqAdolVisit.id(R.id.spnadolVisitContraceptionMethod).getSpinner().setSelection(0);
                        aqAdolVisit.id(R.id.etadolVisitContraceptionMethodOther).text("");
                    }
                }
            });
            aqAdolVisit.id(R.id.spnadolVisitContraceptionMethod).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    TableRow trMethodContraceptionOther = findViewById(R.id.trContraceptionMethodOther);
                    if (i == 7) {
                        trMethodContraceptionOther.setVisibility(View.VISIBLE);
                    } else {
                        trMethodContraceptionOther.setVisibility(View.GONE);
                        aqAdolVisit.id(R.id.etadolVisitContraceptionMethodOther).text("");
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            aqAdolVisit.id(R.id.etAdolVisitMensturalAge).getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (aqAdolVisit.id(R.id.etAdolVisitMensturalAge).getEditText().getText().toString().length() != 0) {
                        int age = Integer.parseInt(currentAdolDetails.getRegAdolAge());
                        int marriageAge = Integer.parseInt(aqAdolVisit.id(R.id.etAdolVisitMensturalAge).getEditText().getText().toString());
                        if (marriageAge > age) {
                            aqAdolVisit.id(R.id.etAdolVisitMensturalAge).getEditText().setError(getResources().getString(R.string.cannotbegreaterthanaage));
                            aqAdolVisit.id(R.id.etAdolVisitMensturalAge).getEditText().requestFocus();
                        }
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

            aqAdolVisit.id(R.id.etAdolVisitAgeattimeofmarriage).getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (aqAdolVisit.id(R.id.etAdolVisitAgeattimeofmarriage).getEditText().getText().toString().length() != 0) {
                        int age = Integer.parseInt(currentAdolDetails.getRegAdolAge());
                        int marriageAge = Integer.parseInt(aqAdolVisit.id(R.id.etAdolVisitAgeattimeofmarriage).getEditText().getText().toString());
                        if (marriageAge > age) {
                            aqAdolVisit.id(R.id.etAdolVisitAgeattimeofmarriage).getEditText().setError(getResources().getString(R.string.cannotbegreaterthancurrentaage));
                            aqAdolVisit.id(R.id.etAdolVisitAgeattimeofmarriage).getEditText().requestFocus();
                        }
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });


            rgHealthIssue.setOnCheckedChangeListener((radioGroup, i) -> {
                TableRow trHealthIssue = findViewById(R.id.trHealthIssueYes);
                RadioButton rb_HIYes = findViewById(R.id.rd_healthissuesYes);
                if (rb_HIYes.isChecked()) {
                    trHealthIssue.setVisibility(View.VISIBLE);
                } else {
                    trHealthIssue.setVisibility(View.GONE);
                    String[] healthproblems = getResources().getStringArray(R.array.healthproblemsuffer);
                    spnHealthProblems.setItems(healthproblems);
                    aqAdolVisit.id(R.id.llCovidQs).getView().setVisibility(View.GONE);
                    aqAdolVisit.id(R.id.etAdolVisitHealthIssuesYesOthers).text("");
                    aqAdolVisit.id(R.id.trHealthProblemYesOthers).getView().setVisibility(View.GONE);
                    rgCovidTest.clearCheck();
                    rgCovidResult.clearCheck();
                    aqAdolVisit.id(R.id.etAdolVisitCovidTestDate).text("");
                }
            });

            rgIsPeriod.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    aqAdolVisit.id(R.id.ll_AV_Period).getView().setVisibility(View.VISIBLE);
                    try {
                        if (i == R.id.rd_isPeriodStartedYes) {
                            aqAdolVisit.id(R.id.ll_AV_Period).getView().setVisibility(View.VISIBLE);
                        } else if (i == R.id.rd_isPeriodStartedNo) {
                            aqAdolVisit.id(R.id.ll_AV_Period).getView().setVisibility(View.GONE);
                            aqAdolVisit.id(R.id.etAdolVisitMensturalAge).text("");
                            spnNormalusePeriod.setSelection(0);
                            aqAdolVisit.id(R.id.etMensturalnormaluseperiodOthers).text("");
                            aqAdolVisit.id(R.id.etMensturalMenustralOthers).text("");
                            spnMensturalProblems.setSelection(0);
                        }
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());

                    }
                }
            });
            spnMarital.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (i == 0 || i == 1) {
                        aqAdolVisit.id(R.id.ll_AV_MaritalStatus).getView().setVisibility(View.GONE);
                        aqAdolVisit.id(R.id.etAdolVisitPartnerName).text("");
                        aqAdolVisit.id(R.id.etAdolVisitPartnerOccupa).text("");
                        aqAdolVisit.id(R.id.etAdolVisitAgeattimeofmarriage).text("");
                    } else if (i > 1) {
                        aqAdolVisit.id(R.id.ll_AV_MaritalStatus).getView().setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            rgPregnant.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    try {
                        ExpansionHeader expHPergnant = findViewById(R.id.expHPregnancy);
                        if (i == R.id.rd_isPregnantYes) {
                            expHPergnant.setVisibility(View.VISIBLE);
                        } else if (i == R.id.rd_isPregnantNo) {
                            expHPergnant.setVisibility(View.GONE);
                        }
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());

                    }
                }
            });

            aqAdolVisit.id(R.id.etAdolVisitHbCount).getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    TableRow trHbDate = findViewById(R.id.trAdolVisitHbTestedDate);
                    TableRow trHbCategory = findViewById(R.id.trAdolVisitHbCategory);

                    if (aqAdolVisit.id(R.id.etAdolVisitHbCount).getEditText().getText().toString().length() > 0) {
                        trHbCategory.setVisibility(View.VISIBLE);
                        trHbDate.setVisibility(View.VISIBLE);
                        calculateHB();
                    } else {
                        trHbCategory.setVisibility(View.GONE);
                        trHbDate.setVisibility(View.GONE);
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
            rgHeight.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    TableRow tableRowFeet = findViewById(R.id.trAdolVisitHeightFeet);
                    TableRow tableRowCM = findViewById(R.id.trAdolVisitheightcm);
                    if (checkedId == R.id.rd_cm) {
                        aqAdolVisit.id(R.id.trAdolVisitBMI).getView().setVisibility(View.VISIBLE);
                        aqAdolVisit.id(R.id.spnAdolVisitHeightInches).getSpinner().setSelection(0);
                        aqAdolVisit.id(R.id.spnAdolVisitHeightFeet).getSpinner().setSelection(4);
                        tableRowCM.setVisibility(View.VISIBLE);
                        tableRowFeet.setVisibility(View.GONE);
                        calculateBMI(aqAdolVisit.id(R.id.etAdolVisitheightcm).getEditText().getText().toString(), aqAdolVisit.id(R.id.etAdolVisitWeight).getEditText().getText().toString());
                    } else if (checkedId == R.id.rd_feet) {
                        aqAdolVisit.id(R.id.trAdolVisitBMI).getView().setVisibility(View.VISIBLE);
                        aqAdolVisit.id(R.id.etAdolVisitheightcm).getEditText().setText("");
                        tableRowCM.setVisibility(View.GONE);
                        tableRowFeet.setVisibility(View.VISIBLE);
                        calculateBMI(aqAdolVisit.id(R.id.spnAdolVisitHeightFeet).getSpinner().getSelectedItemPosition() + 1, aqAdolVisit.id(R.id.spnAdolVisitHeightInches).getSelectedItemPosition(), aqAdolVisit.id(R.id.etAdolVisitWeight).getEditText().getText().toString());
                    } else if (checkedId == R.id.rd_htdontknow) {
                        tableRowCM.setVisibility(View.GONE);
                        tableRowFeet.setVisibility(View.GONE);
                        aqAdolVisit.id(R.id.trAdolVisitBMI).getView().setVisibility(View.GONE);
                        aqAdolVisit.id(R.id.spnAdolVisitHeightInches).getSpinner().setSelection(0);
                        aqAdolVisit.id(R.id.spnAdolVisitHeightFeet).getSpinner().setSelection(4);
                        aqAdolVisit.id(R.id.etAdolVisitheightcm).getEditText().setText("");
                    }
                }
            });

            rgConsumeIFA.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    TableRow trTabletsCount = findViewById(R.id.trAdolVisitIFANooftabletsperDay);
                    TableRow trReason = findViewById(R.id.trIFAReasons);
                    if (i == R.id.rd_ConsumingIFAYes) {
                        trTabletsCount.setVisibility(View.VISIBLE);
                        trReason.setVisibility(View.GONE);
                        spnIFAReasons.setItems(getResources().getStringArray(R.array.ifareasons));
                    } else if (i == R.id.rd_ConsumingIFANo) {
                        trTabletsCount.setVisibility(View.GONE);
                        aqAdolVisit.id(R.id.etIFANoofTabletsConsumes).text("");
                        trReason.setVisibility(View.VISIBLE);
                    }
                }
            });

            rgIFA.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    if (i == R.id.rd_IFAYes) {
                        aqAdolVisit.id(R.id.ll_AV_IFA).getView().setVisibility(View.VISIBLE);
                        spnIFAReasons.setItems(getResources().getStringArray(R.array.ifareasons));
                    } else if (i == R.id.rd_IFANo) {
                        rgConsumeIFA.clearCheck();
                        aqAdolVisit.id(R.id.rd_ConsumingIFANo).checked(false);
                        aqAdolVisit.id(R.id.rd_ConsumingIFAYes).checked(false);
                        aqAdolVisit.id(R.id.etIFANoofTabletsConsumes).text("");
                        spnIFAReasons.setItems(getResources().getStringArray(R.array.ifareasons));
                        aqAdolVisit.id(R.id.spnWhoGaveIFATablets).getSpinner().setSelection(0);
                        aqAdolVisit.id(R.id.ll_AV_IFA).getView().setVisibility(View.GONE);
                        aqAdolVisit.id(R.id.trAdolVisitIFANooftabletsperDay).getView().setVisibility(View.GONE);
                        aqAdolVisit.id(R.id.trIFAReasons).getView().setVisibility(View.GONE);
                    }
                }
            });
            aqAdolVisit.id(R.id.spnAdolVisitHeightFeet).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    calculateBMI(aqAdolVisit.id(R.id.spnAdolVisitHeightFeet).getSpinner().getSelectedItemPosition() + 1
                            , aqAdolVisit.id(R.id.spnAdolVisitHeightInches).getSelectedItemPosition()
                            , aqAdolVisit.id(R.id.etAdolVisitWeight).getEditText().getText().toString());
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            aqAdolVisit.id(R.id.spnAdolVisitHeightInches).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    calculateBMI(aqAdolVisit.id(R.id.spnAdolVisitHeightFeet).getSpinner().getSelectedItemPosition() + 1
                            , aqAdolVisit.id(R.id.spnAdolVisitHeightInches).getSelectedItemPosition()
                            , aqAdolVisit.id(R.id.etAdolVisitWeight).getEditText().getText().toString());
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            aqAdolVisit.id(R.id.spnAdolVisitHeightFeet).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    HeightWarning(0);
                    calculateBMI(aqAdolVisit.id(R.id.spnAdolVisitHeightFeet).getSpinner().getSelectedItemPosition() + 1
                            , aqAdolVisit.id(R.id.spnAdolVisitHeightInches).getSelectedItemPosition()
                            , aqAdolVisit.id(R.id.etAdolVisitWeight).getEditText().getText().toString()); //23May2021 Bindu
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            aqAdolVisit.id(R.id.spnAdolVisitHeightInches).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    HeightWarning(0);
                    calculateBMI(aqAdolVisit.id(R.id.spnAdolVisitHeightFeet).getSpinner().getSelectedItemPosition() + 1
                            , aqAdolVisit.id(R.id.spnAdolVisitHeightInches).getSelectedItemPosition()
                            , aqAdolVisit.id(R.id.etAdolVisitWeight).getEditText().getText().toString()); //23May2021 Bindu
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            aqAdolVisit.id(R.id.etAdolVisitheightcm).getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (aqAdolVisit.id(R.id.etAdolVisitheightcm).getEditText().getText().toString().length() != 0) {
                        double cm = Double.parseDouble(aqAdolVisit.id(R.id.etAdolVisitheightcm).getEditText().getText().toString());
                        HeightWarning(cm);
                    }
                    calculateBMI(aqAdolVisit.id(R.id.etAdolVisitheightcm).getEditText().getText().toString(), aqAdolVisit.id(R.id.etAdolVisitWeight).getEditText().getText().toString());
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

            aqAdolVisit.id(R.id.etAdolVisitWeight).getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (aqAdolVisit.id(R.id.etAdolVisitWeight).getEditText().getText().toString().length() != 0) {
                        double cm = Double.parseDouble(aqAdolVisit.id(R.id.etAdolVisitWeight).getEditText().getText().toString());
                        WeightWarning(cm);
                    }

                    TableRow tableRow = findViewById(R.id.trAdolVisitBMI);
                    if (radioButton_cm.isChecked()) {
                        calculateBMI(aqAdolVisit.id(R.id.etAdolVisitheightcm).getEditText().getText().toString(), aqAdolVisit.id(R.id.etAdolVisitWeight).getEditText().getText().toString());
                        tableRow.setVisibility(View.VISIBLE);
                    } else if (radioButton_feet.isChecked()) {
                        calculateBMI(aqAdolVisit.id(R.id.spnAdolVisitHeightFeet).getSpinner().getSelectedItemPosition() + 1, aqAdolVisit.id(R.id.spnAdolVisitHeightInches).getSelectedItemPosition(), aqAdolVisit.id(R.id.etAdolVisitWeight).getEditText().getText().toString());
                        tableRow.setVisibility(View.VISIBLE);
                    }
                    if (radioButton_dontknow.isChecked()) {
                        tableRow.setVisibility(View.GONE);
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    public int checkSimState() {
        int simState = -1;
        // This is to know the status of the Sim
        TelephonyManager telMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        simState = telMgr.getSimState();

        return simState;
    }

    private void sendSMS(TransactionHeaderRepository transRepo, DatabaseHelper databaseHelper) {
        try {

            String p_no = "";
            List<TblContactDetails> values;

            SettingsRepository settingsRepository = new SettingsRepository(databaseHelper);

            values = settingsRepository.getSpecificPhoneNumber("Adolescent");
            p_no = "";
            for (int i = 0; i < values.size(); i++) {

                String s = values.get(i).getContactNumber();
                p_no = p_no + values.get(i).getContactNumber() + ",";
            }


            String smsContent = "";

            String hrp = "";
            String uniqueId = "";
            if (currentAdolDetails.getRegAdolAadharNo() != null && currentAdolDetails.getRegAdolAadharNo().trim().length() > 0)
                uniqueId = "A:" + currentAdolDetails.getRegAdolAadharNo();
            else
                uniqueId = "Ph No:" + currentAdolDetails.getRegAdolMobile();

//        16MAy2021 Bindu - compl and sms content change
            FacilityRepository facilityRepo = new FacilityRepository(databaseHelper);
            String hamletname = facilityRepo.getHamletName(currentAdolDetails.getRegAdolFacilities());

            //18May2021 Bindu check compl
            String isCompl = "Compl - ";
            if (aqAdolVisit.id(R.id.etAdolVisitWeight).getEditText().getText().toString().length() > 0) {
                double weight = Double.parseDouble(aqAdolVisit.id(R.id.etAdolVisitWeight).getEditText().getText().toString());
                isCompl = isCompl + " Wt(kg) " + weight + ", ";
            }

            if (currentAdolDetails.getRegAdolGender().equals(getResources().getString(R.string.strfemale)) && aqAdolVisit.id(R.id.rd_isPeriodStartedYes).isChecked() && spnMensturalProblems.getSelectedIndicies().size() != 0) {
                isCompl = isCompl + "Menstrual pbm , ";
            }
            if (aqAdolVisit.id(R.id.rd_healthissuesYes).isChecked() && spnHealthProblems.getSelectedIndicies().size() != 0) {
                isCompl = isCompl + " Health pbm , ";
            }
            if (aqAdolVisit.id(R.id.etAdolVisitHbCount).getEditText().getText().toString().length() != 0) {
                double hb = Double.parseDouble(aqAdolVisit.id(R.id.etAdolVisitHbCount).getEditText().getText().toString());
                if (hb < 8.0 || hb >= 8 && hb <= 9.9) {
                    isCompl = isCompl + "Hb - " + hb;
                }
            }

            isCompl = isCompl.replaceAll(", $", "");

            smsContent = "Sentiatend:-" + currentAdolDetails.getRegAdolName() + ", " + uniqueId +
                    ", AID- " + currentAdolDetails.getAdolID() + ", V#:" + VISIT_COUNT + ", V Dt: " + tblAVHeader.getAdolvisHVisitDate()
                    + ", " + isCompl + ", " + hamletname + ", " + appState.userType + "- " + user.getUserName()
                    + " ," + user.getUserMobileNumber() + "- (ADOL)";


            if (p_no.length() <= 0) {
                display_messagedialog(transRepo, databaseHelper, smsContent);
            } else
                sending(transRepo, databaseHelper, p_no, smsContent);
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    public void display_messagedialog(final TransactionHeaderRepository transactionHeaderRepository,
                                      final DatabaseHelper databaseHelper, String smsContent) throws Exception {
        try {
            final Dialog sms_dialog = new Dialog(this);
//            sms_dialog.setTitle(getResources().getString(R.string.plsentervalidphoneno));

            sms_dialog.setContentView(R.layout.sms_dialog);


            sms_dialog.show();

            ImageButton imgsend = sms_dialog.findViewById(R.id.imgsend);
            ImageButton imgcancel = sms_dialog.findViewById(R.id.imgcancel);
            etphn = sms_dialog.findViewById(R.id.etphno);
            final EditText etmess = sms_dialog.findViewById(R.id.etmess);
            TextView txtcontcats = sms_dialog.findViewById(R.id.txtcontacts);


//            etmess.setVisibility(View.GONE); //23May2021 Bindu - disable
            etmess.setText(smsContent);
            etmess.setEnabled(false);
            txtcontcats.setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    // TODO Auto-generated method stub

                    Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
//					context.startActivity(intent);
                    startActivity(intent);
                    return true;
                }
            });


            //  etphn.setText(p_no);

            imgsend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {

                        sending(transactionHeaderRepository, databaseHelper, etphn.getText().toString(), smsContent);

                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());

                    }

                }
            });


            imgcancel.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    sms_dialog.cancel();
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.visitsuccess), Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(AdolVisit_New.this, AdolVisitHistory.class);
                    intent.putExtra("adolID", AdolId);
                    intent.putExtra("globalState", prepareBundle());
                    startActivity(intent);
                }
            });
            sms_dialog.setCancelable(false);
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }

    }

    void sending(TransactionHeaderRepository transRepo, DatabaseHelper databaseHelper,
                 String phnNo, String smsCont) throws Exception {
        String smsContent = smsCont;


        InserttblMessageLog(phnNo,
                smsContent, appState.sessionUserId, transRepo, databaseHelper);
        if (isMessageLogsaved) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.sending_sms),
                    Toast.LENGTH_LONG).show();


            if (isMessageLogsaved) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.visitsuccess), Toast.LENGTH_LONG).show();
                Intent intent = new Intent(AdolVisit_New.this, AdolVisitHistory.class);
                intent.putExtra("adolID", AdolId);
                intent.putExtra("globalState", prepareBundle());
                startActivity(intent);
            } else
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.m091), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha

            sendSMSFunction();
        } else
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.m091), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha

    }

    private void sendSMSFunction() throws Exception {
        messageSent = true;
        new SendSMS(this).checkAndSendMsg(databaseHelper);

    }

    public void InserttblMessageLog(String phoneno, String content, String user_id,
                                    TransactionHeaderRepository transactionHeaderRepository, DatabaseHelper databaseHelper) throws Exception {
        final ArrayList<MessageLogPojo> mlpArr = new ArrayList<MessageLogPojo>();

        if (phoneno.length() > 0) {
            String[] phn = phoneno.split("\\,");
            for (int i = 0; i < phn.length; i++) {
                String num = phn[i];
                if (num != null && num.length() > 0) {
                    MessageLogPojo mlp =
                            new MessageLogPojo();
                    mlp.setUserId(user_id);
                    mlp.setMsgPhoneNo(num);
                    mlp.setMsgBody(content);
                    mlp.setWomanId(currentAdolDetails.getAdolID());
                    mlp.setMsgPriority(1);
                    mlp.setMsgNoOfFailures(0);
                    mlp.setTransId(transID);
                    mlp.setMsgSentDate(DateTimeUtil.getTodaysDate());
                    mlp.setMsgSent(0);
                    mlp.setMsgStage("Adolescent");
                    mlp.setMsgUserType(appState.userType);
                    mlp.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
                    mlpArr.add(mlp);
                }
            }
        }

        int addMessage = 0;
        SMSRepository smsRepository = new SMSRepository(databaseHelper);
        if (mlpArr != null) {
            for (final MessageLogPojo mlpp : mlpArr) {

                addMessage = smsRepository.insertIntoMessageLog(mlpp, databaseHelper);
            }

            if (addMessage > 0) {
                isMessageLogsaved = true;
                boolean addRegTrans = TransactionHeaderRepository.iNewRecordTransNew(user.getUserId(), transID,
                        "tblmessagelog", databaseHelper);
            }


        }
    }

    private void getCovidVaccineDetails() throws Exception {
        CovidDetailsRepository covRepo = new CovidDetailsRepository(databaseHelper);
        covVacItems = covRepo.getAdolVisitCovidVaccinDetails(AdolId);
        if (covVacItems != null && covVacItems.size() > 0) {
            isvaccinated = true;
            for (int i = 0; i < covVacItems.size(); i++) {
                aqAdolVisit.id(R.id.rd_CovidVaccineYes).enabled(false);
                aqAdolVisit.id(R.id.rd_CovidVaccineNo).enabled(false);
                aqAdolVisit.id(R.id.rd_CovidVaccineYes).checked(true);
                aqAdolVisit.id(R.id.spnadolVisitVaccineDose).enabled(false);
                if (covVacItems.get(i).getCovidVaccinatedNo().equalsIgnoreCase("1")) {
                    aqAdolVisit.id(R.id.trCovidFirstDoseDate).getView().setVisibility(View.VISIBLE);
                    aqAdolVisit.id(R.id.etAdolVisitCovidFirstDoseDate).text(covVacItems.get(i).getCovidVaccinatedDate());
                    aqAdolVisit.id(R.id.etAdolVisitCovidFirstDoseDate).enabled(false).background(R.drawable.edittext_disable);
                    aqAdolVisit.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.VISIBLE);
                }
                if (covVacItems.get(i).getCovidVaccinatedNo().equalsIgnoreCase("2")) {
                    aqAdolVisit.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.VISIBLE);
                    aqAdolVisit.id(R.id.etAdolVisitCovidSecondDoseDate).text(covVacItems.get(i).getCovidVaccinatedDate());
                    aqAdolVisit.id(R.id.etAdolVisitCovidSecondDoseDate).enabled(false).background(R.drawable.edittext_disable);
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        try {
            displayDialogExit();
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private void callCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            try {
                File photoFile = null;
                photoFile = createImagePath();
                // Continue only if the File was successfully created
                if (photoFile != null) {
                    Uri photoURI = FileProvider.getUriForFile(
                            getApplicationContext(),
                            getApplicationContext()
                                    .getPackageName() + ".provider", photoFile);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private File createImagePath() {
        try {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = "JPEG_" + timeStamp + "_";
            File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            File image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );
            mCurrentPhotoPath = image.getAbsolutePath();
            return image;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //Ramesh 1-6-2021
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
                File file = new File(mCurrentPhotoPath);
                FileUtils fileUtils = new FileUtils(getApplicationContext());
                file = new File(fileUtils.getPath(Uri.fromFile(file)));
                file = new Compressor(getApplicationContext()).compressToFile(file);
                Bitmap bitmap = MediaStore.Images.Media
                        .getBitmap(getApplicationContext().getContentResolver(), Uri.fromFile(file));
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100 /*ignored for PNG*/, bos);
                byte[] bitmapdata = bos.toByteArray();
                String timeStamp = new SimpleDateFormat("HHmmssSSS").format(new Date());
                String fn;
                String dir;
                fn = "IMG_" + currentAdolDetails.getAdolID() + "_HV" + VISIT_COUNT + timeStamp + ".jpeg";
                dir = AppState.imgDirRef;
                File fileParent = new File(dir);

                String fileName = fileParent + "/" + fn;

                ImagePojo imagePojo = new ImagePojo();
                imagePojo.setImage(bitmapdata);
                imagePojo.setImagePath(fileName);
                imagePojo.setImageBitmap(bitmap);

                AllImagePojo.add(imagePojo);

                tblImageStore imageStore = new tblImageStore();
                imageStore.setUserId(user.getUserId());
                imageStore.setBeneficiaryId(currentAdolDetails.getAdolID());
                imageStore.setImageName(fn.replace(".jpeg", ""));
                imageStore.setImagePath(fileName);
                imageStore.setTransId(transID);
                imageStore.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
                imageStore.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
                AllImagesBeforeSave.add(imageStore);
                AsyncImage asyncImage = new AsyncImage();
                asyncImage.execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class AsyncImage extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {

            Collections.sort(AllImagesBeforeSave, (file1, file2) -> {
                long k = new File(file1.getImagePath()).lastModified() - new File(file2.getImagePath()).lastModified();
                if (k < 0) {
                    return 1;
                } else if (k == 0) {
                    return 0;
                } else {
                    return -1;
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            startRecyclerView();
        }
    }

    public void startRecyclerView() {
        try {
            recyclerViewImage = findViewById(R.id.recyclerImage);
            if (AllImagesBeforeSave.size() == 0) {
                aqAdolVisit.id(R.id.lladdphotos).gone();
            } else {
                aqAdolVisit.id(R.id.lladdphotos).visible();
                recyclerViewImage.setVisibility(View.VISIBLE);
                aqAdolVisit.id(R.id.txtnoimageadded).getTextView().setVisibility(View.GONE);
                DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
                float screenWidthDp = displayMetrics.widthPixels / displayMetrics.density;
                int noOfColumns = (int) (screenWidthDp / 125 + 0.5); // +0.5 for correct rounding to int.
                GridLayoutManager layoutManager = new GridLayoutManager(AdolVisit_New.this, noOfColumns);
                recyclerViewImage.setLayoutManager(layoutManager);
                recyclerViewImage.setNestedScrollingEnabled(false);
                recyclerViewImage.setHasFixedSize(true);
                RecyclerViewAdapter recyclerViewAdapter = new RecyclerViewAdapter(AdolVisit_New.this, AllImagesBeforeSave, databaseHelper, AllImagePojo, aqAdolVisit);
                recyclerViewImage.setAdapter(recyclerViewAdapter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private class AsyncFileUpload extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            try {
                OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                        .connectTimeout(60, TimeUnit.SECONDS)
                        .readTimeout(60, TimeUnit.SECONDS)
                        .writeTimeout(60, TimeUnit.SECONDS)
                        .build();

                for (tblImageStore s : AllImagesBeforeSave) {
                    String imagepath = AppState.imgDirRef + "/" + s.getImageName() + ".jpeg";
                    File file = new File(imagepath);

                    RequestBody requestBody = new MultipartBody.Builder()
                            .setType(MultipartBody.FORM)
                            .addFormDataPart("filename", s.getImageName())
                            .addFormDataPart(s.getImageName(), s.getImageName(),
                                    RequestBody.create(MediaType.parse("image/jpg"), file))
                            .build();
                    String endpoint = "http://" + appState.webServiceIpAddress + appState.webServicePath + "image";
                    Request request = new Request.Builder()
                            .url(endpoint)
                            .post(requestBody)
                            .build();
                    try (Response r2 = okHttpClient.newCall(request).execute()) {
                        String responseString2 = r2.body().string();
                        Log.i("Image Upload", responseString2);
                        updateImageStatus(s.getImageName());
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.i("Image Upload", e.getMessage());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    private void fileUpload() {
        try {
            AsyncFileUpload asyncFileUpload = new AsyncFileUpload();
            asyncFileUpload.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateImageStatus(String imageName) throws Exception {
        ImageStoreRepository imageStoreRepository = new ImageStoreRepository(databaseHelper);
        int update = imageStoreRepository.updateImageStatusof(imageName);
        if (update > 0) {
            Log.e("Image", "Image uploaded");
        } else {
            Log.e("Image", "Image Not uploaded");
        }
    }

    //    01Sep2021 Arpitha
    /**
     * Alert Dialog to select Pregnant or Mother Registration
     */
    private void showPregnantOrMotherSelectionDialog() {
        try {
            final AlertDialog.Builder alert = new AlertDialog.Builder(activity,
                    AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
            final View customView = LayoutInflater.from(this).inflate(R.layout.pregnant_mother_selection,
                    null);

            aqPMSel = new AQuery(customView);
            radGrpEDDorLmp = customView.findViewById(R.id.radEDDorLmp);

            initializeScreen(aqPMSel.id(R.id.llPregOrMothMainLayout).getView());
            setInitialView();

            aqPMSel.id(R.id.llmother).gone();
            aqPMSel.id(R.id.llchild).gone();
            aqPMSel.id(R.id.llAdol).gone();



            aqPMSel.id(R.id.imgclearedddate).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    aqPMSel.id(R.id.etlmpdate).text("");
                }
            });
            aqPMSel.id(R.id.imgclearlmp).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    aqPMSel.id(R.id.etMotherAdd).text("");
                    aqPMSel.id(R.id.etlmpdate).text("");
                    aqPMSel.id(R.id.etgest).text("");
                    aqPMSel.id(R.id.imgclearlmp).gone();
                }
            });
            aqPMSel.id(R.id.imgclearregadate).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());
                    Toast.makeText(activity, getResources().getString(R.string.dateresettotodaysdate), Toast.LENGTH_LONG).show();//21Aug2019 Arpitha
                }
            });//   Arpitha - 20Aug2019

//            aqPMSel.id(R.id.etMotherAdd).text("20-05-2021");
            aqPMSel.id(R.id.eteddmother).enabled(false);

            aqPMSel.id(R.id.etMotherAdd).getEditText().setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    try {
                        if (MotionEvent.ACTION_UP == event.getAction()) {
                            isRegDate = false;
                            assignEditTextAndCallDatePicker(aqPMSel.id(R.id.etMotherAdd).getEditText());
                        }
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());

                    }
                    return true;
                }
            });

            aqPMSel.id(R.id.etregdate).getEditText().setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    try {
                        if (MotionEvent.ACTION_UP == event.getAction()) {
                            isRegDate = true;
                            assignEditTextAndCallDatePicker(aqPMSel.id(R.id.etregdate).getEditText());
                        }
                    } catch (Exception e) {

                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }
                    return true;
                }
            });

            aqPMSel.id(R.id.radLMP).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        setLMP();
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }
                }
            });

            aqPMSel.id(R.id.radEDD).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        setEDD();
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }
                }
            });

            alert.setView(customView);

            alert.setCancelable(true)
                    .setTitle(getResources().getString(R.string.registrationHeading))
                    .setPositiveButton(getResources().getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {

                                }
                            })
                    .setNegativeButton(getResources().getString(R.string.cancel), //01Oct2019 - Bindu - Add cancel button
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    dialog.cancel();
                                }
                            });
            final AlertDialog dialog = alert.create();
            if (dialog != null)
                dialog.show();

            // Overriding the handler immediately after show is probably a
            // better
            // approach than OnShowListener as described below
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        validateAndRegister();
                        if (wantToCloseDialog)
                            dialog.dismiss();
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }

                }
            });
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }


    //calculate gestation based on LMP
    private void calculateEddGestation(String lmp) {
        try {

            if (lmp.equalsIgnoreCase("lmp")) {
                if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0
                        && aqPMSel.id(R.id.etMotherAdd).getText().length() > 0) {

                    noOfDays = DateTimeUtil
                            .getNumberOfDaysFromLMP(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                    int noOfWeeks = noOfDays / 7;
                    int days = noOfDays % 7;

                    String strdays = "";

                    if (days > 1)
                        strdays = days + " " + activity.getResources().getString(R.string.days);
                    else if (days == 1)
                        strdays = days + " " + activity.getResources().getString(R.string.day);

                    aqPMSel.id(R.id.etgest)
                            .text(noOfWeeks + " " + activity.getResources().getString(R.string.weeks) + " " + strdays);
                }

                String edd = populateEDD(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                aqPMSel.id(R.id.etlmpdate).text(edd);

            } else {

                String strLmp = CalculateLMP(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                aqPMSel.id(R.id.etlmpdate).text(strLmp);

                if (aqPMSel.id(R.id.etlmpdate).getText().length() > 0
                        && aqPMSel.id(R.id.etlmpdate).getText().length() > 0) {

                    noOfDays = DateTimeUtil
                            .getNumberOfDaysFromLMP(aqPMSel.id(R.id.etlmpdate).getText().toString());
                    int noOfWeeks = noOfDays / 7;
                    int days = noOfDays % 7;

                    String strdays = "";

                    if (days > 1)
                        strdays = days + " " + activity.getResources().getString(R.string.days);
                    else if (days == 1)
                        strdays = days + " " + activity.getResources().getString(R.string.day);

                    aqPMSel.id(R.id.etgest)
                            .text(noOfWeeks + " " + activity.getResources().getString(R.string.weeks) + " " + strdays);
                }

            }
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }

    }

    // Calculate EDD and display in EDD Edit text
    private String populateEDD(String xLMP) throws Exception {
        String stredddate = "";
        try {

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());

            Date LmpDate;
            LmpDate = sdf.parse(xLMP);
            Calendar cal = Calendar.getInstance();
            cal.setTime(LmpDate);
            cal.add(Calendar.DAY_OF_MONTH, 280);

            int day = cal.get(Calendar.DAY_OF_MONTH);
            int month = cal.get(Calendar.MONTH) + 1;
            int year = cal.get(Calendar.YEAR);

            stredddate =
                    String.format("%02d", day) + "-" + String.format("%02d", month) + "-" + String.format("%02d", year);

        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        return stredddate;
    }

    //	calculate LMP based on EDD
    private String CalculateLMP(String EDDate) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        Date LMP;
        LMP = sdf.parse(EDDate);
        Calendar cal = Calendar.getInstance();
        cal.setTime(LMP);
        cal.add(Calendar.DAY_OF_MONTH, -280);

        int day = cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH) + 1;
        int year = cal.get(Calendar.YEAR);

        String lmpDate = String.format("%02d", day) + "-" + String.format("%02d", month) + "-" + year;
        return lmpDate;
    }

    /**
     * This method opens the Date Dialog and sets Date to respective EditText
     */
  /*  private void assignEditTextAndCallDatePicker(EditText editText) {
        String currDate = (editText.getText().length() > 0) ? editText.getText().toString() : "";
        CommonDatePickerFragment.getCommonDate(activity, currDate);
    }*/

    /*@Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        try {
            aqPMSel.id(R.id.imgclearlmp).background(R.drawable.brush1);
            if (view.isShown()) {
                String str = String.format("%02d", day) + "-" + String.format("%02d", month + 1) + "-"
                        + year;

                int days = DateTimeUtil.getDaysBetweenDates(DateTimeUtil.getTodaysDate(), str);

                if (!aqPMSel.id(R.id.radEDD).isChecked()) {
                    if (days >= 0) {
                        if (isRegDate) {
                            if (days < 30) {
                                aqPMSel.id(R.id.etregdate).text(str);
                                aqPMSel.id(R.id.imgclearregadate).visible();//20AUg2019 Arpitha
                            } else if (days > 120) {
                                aqPMSel.id(R.id.etregdate).text("");
                                aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                                Toast.makeText(activity, getResources().getString(R.string.date_val_2_toast),
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha

                            } else {
                                aqPMSel.id(R.id.etregdate).text(str);
                                aqPMSel.id(R.id.imgclearregadate).visible();//20AUg2019 Arpitha
                            }

                        } else if (aqPMSel.id(R.id.radLMP).isChecked()) {
                            String lmporadd = str;
                            int daysdiff = 0;
                            String regdate = aqPMSel.id(R.id.etregdate).getText().toString();

                            if (regdate != null && regdate.trim().length() > 0) {
                                daysdiff = DateTimeUtil.getDaysBetweenDates(regdate, lmporadd);

                                int daysdifffromCUrr = DateTimeUtil
                                        .getDaysBetweenDates(DateTimeUtil.getTodaysDate(), lmporadd);

                                if (daysdiff <= 30) {
                                    Toast.makeText(activity,
                                            getResources().getString(R.string.date_val_3_toast),
                                            Toast.LENGTH_LONG).show();
                                    aqPMSel.id(R.id.etlmpdate).text("");
                                    aqPMSel.id(R.id.etgest).text("");
                                    aqPMSel.id(R.id.etMotherAdd).text("");
                                    aqPMSel.id(R.id.imgclearlmp).gone();//20Aug2019 Arpitha
                                } else if (daysdifffromCUrr > 280) {
                                    Toast.makeText(activity,
                                            getResources().getString(R.string.date_val_4_toast),
                                            Toast.LENGTH_LONG).show();
                                    aqPMSel.id(R.id.etlmpdate).text("");
                                    aqPMSel.id(R.id.etgest).text("");
                                    aqPMSel.id(R.id.etMotherAdd).text("");
                                    aqPMSel.id(R.id.imgclearlmp).gone();//20Aug2019 Arpitha
                                } else {
                                    aqPMSel.id(R.id.etMotherAdd).text(str);
                                    calculateEddGestation("lmp");
                                    aqPMSel.id(R.id.imgclearlmp).visible();//20Aug2019 Arpitha
                                }
                            } else
                                Toast.makeText(activity, getResources().getString(R.string.plz_enter_reg_dt),
                                        Toast.LENGTH_LONG).show();
                        } else if (aqPMSel.id(R.id.rdbMother).isChecked()) {

                            int daysdiff = DateTimeUtil
                                    .getDaysBetweenDates(aqPMSel.id(R.id.etregdate).getText().toString(), str);
                            if (daysdiff > 365) {// Arpitha 28Jun2018 - 23Dec2019 Arpitha - allow max 1 year

                                Toast.makeText(activity, (getResources().getString(R.string.datecannotbeafteroneyearfromcurrentdate)), // 11July2018
                                        // Arpitha
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etMotherAdd).text("");
                            } else
                                aqPMSel.id(R.id.etMotherAdd).text(str);
                        } else if (aqPMSel.id(R.id.rdbchild).isChecked()) {

                            int daysdiff = DateTimeUtil
                                    .getDaysBetweenDates(aqPMSel.id(R.id.etregdate).getText().toString(), str);
                            if (daysdiff > 5844) {// Arpitha 28Jun2018

                                Toast.makeText(activity, (getResources().getString(R.string.date_val_7_toast)), // 11July2018
                                        // Arpitha
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etMotherAdd).text("");
                            } else
                                aqPMSel.id(R.id.etMotherAdd).text(str);
                        }
                    } else {
                        if (isRegDate) {
                            aqPMSel.id(R.id.etregdate).text("");
                            aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                            Toast.makeText(activity,
                                    getResources().getString(R.string.reg_date_cannot_greater_current_date),
                                    Toast.LENGTH_LONG).show();
                            aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha
                        } else {
                            aqPMSel.id(R.id.etMotherAdd).text("");
                            aqPMSel.id(R.id.imgclearlmp).gone();//20AUg2019 Arpitha
                            aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                            if (aqPMSel.id(R.id.rdbPreg).isChecked()) {
                                Toast.makeText(activity,
                                        getResources()
                                                .getString(R.string.lmp_date_time_cannot_be_after_curre_datetime), // 11July2018

                                        Toast.LENGTH_LONG).show();

                                aqPMSel.id(R.id.etlmpdate).text("");
                                aqPMSel.id(R.id.etgest).text("");
                            } else {
                                Toast.makeText(activity,
                                        getResources()
                                                .getString(R.string.del_date_cannot_be_after_current_date_time), // 11July2018
                                        Toast.LENGTH_LONG).show();
                            }

                        }
                    }
                } else {
                    if (!isRegDate) {
                        int days1 = 0;
                        if (aqPMSel.id(R.id.etregdate).getText().toString().trim().length() > 0)
                            days1 = DateTimeUtil.getDaysBetweenDates(str,
                                    aqPMSel.id(R.id.etregdate).getText().toString());

                        if (days1 > 0 && days1 < 273) {
                            aqPMSel.id(R.id.etMotherAdd).text(str);
                            calculateEddGestation("edd");
                            aqPMSel.id(R.id.imgclearlmp).visible();//20Aug2019 Arpitha
                        } else {
                            if (days1 > 280) {
                                aqPMSel.id(R.id.etMotherAdd).text("");
                                aqPMSel.id(R.id.imgclearlmp).gone();//21Aug2019 Arpitha
                                aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                                Toast.makeText(activity, getResources().getString(R.string.date_val_6_toast),

                                        Toast.LENGTH_LONG).show();

                                aqPMSel.id(R.id.etlmpdate).text("");
                                aqPMSel.id(R.id.etgest).text("");

                            } else {
                                aqPMSel.id(R.id.etMotherAdd).text("");
                                aqPMSel.id(R.id.imgclearlmp).gone();//21Aug2019 Arpitha
                                aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                                Toast.makeText(activity, getResources().getString(R.string.date_val_5_toast),
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etlmpdate).text("");
                                aqPMSel.id(R.id.etgest).text("");
                            }
                        }
                    } else {
                        days = DateTimeUtil.getDaysBetweenDates(DateTimeUtil.getTodaysDate(), str);
                        if (days >= 0) {

                            if (days > 90) {
                                aqPMSel.id(R.id.etregdate).text("");
                                aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                                Toast.makeText(activity, getResources().getString(R.string.date_val_2_toast),

                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha
                            } else {
                                aqPMSel.id(R.id.etregdate).text(str);
                            }

                        } else {
                            aqPMSel.id(R.id.etregdate).text("");
                            aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                            Toast.makeText(activity, getResources().getString(R.string.reg_date_cannot_greater_current_date),

                                    Toast.LENGTH_LONG).show();
                            aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha
                        }
                    }
                }

            }

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }*/

    /**
     * This is a Recursive method that traverse through the group and subgroup
     * of view Sets text, assigns clickListners
     *
     * @throws Exception
     */
    private ArrayList<View> initializeScreen(View v) throws Exception {
        if (!(v instanceof ViewGroup)) {
            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);

            // TextView - Assign Labels
            if (v.getClass() == TextView.class || v.getClass() == RadioButton.class) {
                // aqPMSel.id(v.getId()).text(getSS(aqPMSel.id(v.getId()).getText().toString()));
            }

            if (v.getClass() == RadioButton.class) {
                aqPMSel.id(v.getId()).clicked(this, "commonClick");
            }

            return viewArrayList;
        }

        ArrayList<View> result = new ArrayList<View>();

        ViewGroup viewGroup = (ViewGroup) v;
        for (int i = 0; i < viewGroup.getChildCount(); i++) {

            View child = viewGroup.getChildAt(i);

            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);
            viewArrayList.addAll(initializeScreen(child));

            result.addAll(viewArrayList);
        }
        return result;
    }


    //	set initial view of the pregnant or mother pop up
    private void setInitialView() {
        aqPMSel.id(R.id.rdbPreg).checked(true);
        aqPMSel.id(R.id.tvMotherAddHeading).text(getResources().getString(R.string.tvPregLmpHeading));
        aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());
        aqPMSel.id(R.id.radLMP).checked(true);
        aqPMSel.id(R.id.etregdate).getEditText().setKeyListener(null);
        aqPMSel.id(R.id.etMotherAdd).getEditText().setKeyListener(null);
        aqPMSel.id(R.id.lllmpdate).visible();
        aqPMSel.id(R.id.llgest).visible();
        aqPMSel.id(R.id.vHLine4).visible();
        aqPMSel.id(R.id.vHLine6).visible();
        aqPMSel.id(R.id.llnote).visible();

        //13Apr2021 Bindu - set here cos not translated from xml
        aqPMSel.id(R.id.anclbl).text(getResources().getString(R.string.anclbl));
        aqPMSel.id(R.id.txtpnclbl).text(getResources().getString(R.string.pnclbl));
        aqPMSel.id(R.id.lmp_date).text(getResources().getString(R.string.tvedd));
        aqPMSel.id(R.id.txtgest).text(getResources().getString(R.string.tvGestationalageL));
        aqPMSel.id(R.id.txtchildreglbl).text(getResources().getString(R.string.childreg));
        aqPMSel.id(R.id.tvFirstPregHeading).text(getResources().getString(R.string.tvFirstPregHeading));
        aqPMSel.id(R.id.reg_date).text(getResources().getString(R.string.reg_date));
        aqPMSel.id(R.id.instructions).text(getResources().getString(R.string.instruction));
        aqPMSel.id(R.id.regdt).text(getResources().getString(R.string.reg_date));
        aqPMSel.id(R.id.txt1).text(getResources().getString(R.string.date_val_0));
        aqPMSel.id(R.id.txt2).text(getResources().getString(R.string.date_val_1));
        aqPMSel.id(R.id.txt3).text(getResources().getString(R.string.date_val_2));
        aqPMSel.id(R.id.txtlmp).text(getResources().getString(R.string.tvPregLmpHeading));
        aqPMSel.id(R.id.txt4).text(getResources().getString(R.string.date_val_3));
        aqPMSel.id(R.id.txt5).text(getResources().getString(R.string.date_val_4));
        aqPMSel.id(R.id.txt6).text(getResources().getString(R.string.date_val_5));
        aqPMSel.id(R.id.txt7).text(getResources().getString(R.string.date_val_6));
        aqPMSel.id(R.id.txt8).text(getResources().getString(R.string.date_val_7));
        aqPMSel.id(R.id.radLMP).text(getResources().getString(R.string.tvlmp));
        aqPMSel.id(R.id.radEDD).text(getResources().getString(R.string.tvedd));
//22May2021 Bindu set adol reg lbl
        aqPMSel.id(R.id.adolreg).text(getResources().getString(R.string.adolescentreg));

    }

    // capture EDD
    private void setEDD() throws Exception {
        aqPMSel.id(R.id.tvMotherAddHeading).text(getResources().getString(R.string.tvedd));
        aqPMSel.id(R.id.etMotherAdd).text("");
        aqPMSel.id(R.id.imgclearlmp).gone();//20AUg2019 Arpitha
        aqPMSel.id(R.id.lmp_date).text(getResources().getString(R.string.tvlmp));
        aqPMSel.id(R.id.etlmpdate).text("");
        aqPMSel.id(R.id.etgest).text("");
        aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());
    }

    // capture LMP
    private void setLMP() throws Exception {
        aqPMSel.id(R.id.tvMotherAddHeading).text(getResources().getString(R.string.tvlmp));
        aqPMSel.id(R.id.etMotherAdd).text("");
        aqPMSel.id(R.id.imgclearlmp).gone();//20AUg2019 Arpitha
        aqPMSel.id(R.id.lmp_date).text(getResources().getString(R.string.tvedd));
        aqPMSel.id(R.id.etlmpdate).text("");
        aqPMSel.id(R.id.etgest).text("");
    }


    /**
     * Common method invokes when any button clicked This method handles
     * Edittext, save clicks. Validate mandatory fields
     */
    public void commonClick(View v) throws Exception {
        switch (v.getId()) {
            case R.id.rdbPreg: {
                aqPMSel.id(R.id.rdbMother).checked(false);
                aqPMSel.id(R.id.tvFirstPregHeading).enabled(true);
                aqPMSel.id(R.id.rdbchild).checked(false);
                aqPMSel.id(R.id.rdbAdol).checked(false);//Ramesh 13-may2021
                aqPMSel.id(R.id.chkFirstPreg).enabled(true);
                aqPMSel.id(R.id.radEDDorLmp).visible();
                //27Nov2018 - Bindu - radiogrp clear chk to reset radiobtn
                aqPMSel.id(R.id.vHLine5).visible();
                aqPMSel.id(R.id.etMotherAdd).text("");
                aqPMSel.id(R.id.radLMP).checked(true);
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.tvPregLmpHeading)));
                // 27Jun2018 Arpitha
                aqPMSel.id(R.id.etregdate).enabled(true);
                aqPMSel.id(R.id.etregdate).textColor(getResources().getColor(R.color.black));
//				aqPMSel.id(R.id.llregdate).background(R.drawable.editext_none);
                aqPMSel.id(R.id.lllmpdate).visible();
                aqPMSel.id(R.id.llgest).visible();
                aqPMSel.id(R.id.vHLine4).visible();
                aqPMSel.id(R.id.vHLine6).visible();

                aqPMSel.id(R.id.llMotherAdd).visible();
                aqPMSel.id(R.id.llFirstPreg).visible();
                aqPMSel.id(R.id.llnote).visible();
                aqPMSel.id(R.id.vHLine5).visible();
//				19Nov2019 Arpitha
                aqPMSel.id(R.id.llancinstruction).visible();
                aqPMSel.id(R.id.llpncinstruction).gone();

                break;
            }
            case R.id.rdbMother: {
                aqPMSel.id(R.id.rdbPreg).checked(false);
                aqPMSel.id(R.id.rdbchild).checked(false);
                aqPMSel.id(R.id.rdbAdol).checked(false);//Ramesh 13-may2021
                //27Nov2018 - Bindu radiogrp clear chk to reset radiobtn
                radGrpEDDorLmp.clearCheck();
                aqPMSel.id(R.id.radLMP).checked(false);
                aqPMSel.id(R.id.radEDD).checked(false);
                aqPMSel.id(R.id.tvFirstPregHeading).enabled(false);
                aqPMSel.id(R.id.chkFirstPreg).enabled(false);
                aqPMSel.id(R.id.chkFirstPreg).checked(false);
                aqPMSel.id(R.id.radEDDorLmp).gone();
                aqPMSel.id(R.id.vHLine5).gone();
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.deldate)));
                aqPMSel.id(R.id.etMotherAdd).text("");
                // 27Jun2018 Arpitha
                aqPMSel.id(R.id.etregdate).enabled(false);
                aqPMSel.id(R.id.etregdate).textColor(R.color.fontcolor_disable);
                //	aqPMSel.id(R.id.llregdate).background(R.drawable.edittext_disable);
                aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());//21March2019 Arpitha
                aqPMSel.id(R.id.lllmpdate).gone();
                aqPMSel.id(R.id.llgest).gone();
                aqPMSel.id(R.id.vHLine4).gone();
                aqPMSel.id(R.id.vHLine6).gone();

                aqPMSel.id(R.id.llMotherAdd).visible();
                aqPMSel.id(R.id.llFirstPreg).visible();
                aqPMSel.id(R.id.llnote).visible();
                aqPMSel.id(R.id.vHLine5).visible();
                //				19Nov2019 Arpitha
                aqPMSel.id(R.id.llancinstruction).gone();
                aqPMSel.id(R.id.llpncinstruction).visible();
                aqPMSel.id(R.id.llFirstPreg).gone();
                aqPMSel.id(R.id.imgclearlmp).visible();
                break;
            }
            case R.id.radEDD: {

                //	aqPMSel.id(R.id.radEDD).checked(true); //27Nov2018 - Bindu- comment the line
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.deldate)));
                aqPMSel.id(R.id.etMotherAdd).text("");
                aqPMSel.id(R.id.lmp_date).text((getResources().getString(R.string.tvlmp)));// 28Jun2018
                // Arpitha
                // 11July2018 Arpitha
                aqPMSel.id(R.id.etlmpdate).text("");
                aqPMSel.id(R.id.etgest).text("");
                break;
            }
            case R.id.rdbchild: {
                aqPMSel.id(R.id.rdbPreg).checked(false);
                aqPMSel.id(R.id.rdbMother).checked(false);
                aqPMSel.id(R.id.rdbAdol).checked(false);//Ramesh 13-may2021

                //27Nov2018 - Bindu radiogrp clear chk to reset radiobtn
                radGrpEDDorLmp.clearCheck();//Arpitha2021
                aqPMSel.id(R.id.radLMP).checked(false);
                aqPMSel.id(R.id.radEDD).checked(false);
                aqPMSel.id(R.id.tvFirstPregHeading).enabled(false);
                aqPMSel.id(R.id.chkFirstPreg).enabled(false).checked(false);
                aqPMSel.id(R.id.radEDDorLmp).gone();
                aqPMSel.id(R.id.vHLine5).gone();
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.deldate)));
                aqPMSel.id(R.id.etMotherAdd).text("");
                aqPMSel.id(R.id.llMotherAdd).gone();
                aqPMSel.id(R.id.llFirstPreg).gone();
                aqPMSel.id(R.id.llnote).gone();
                // 27Jun2018 Arpitha
                aqPMSel.id(R.id.etregdate).enabled(false);
                aqPMSel.id(R.id.etregdate).textColor(R.color.fontcolor_disable);
                //	aqPMSel.id(R.id.llregdate).background(R.drawable.edittext_disable);
                aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());//21March2019 Arpitha
                aqPMSel.id(R.id.lllmpdate).gone();
                aqPMSel.id(R.id.llgest).gone();
                aqPMSel.id(R.id.vHLine4).gone();
                aqPMSel.id(R.id.vHLine6).gone();
                break;
            }
            case R.id.radLMP: {
                //	aqPMSel.id(R.id.radLMP).checked(true); //27Nov2018 - Bindu- comment the line
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.tvlmp)));
                aqPMSel.id(R.id.etMotherAdd).text("");
                aqPMSel.id(R.id.lmp_date).text((getResources().getString(R.string.tvedd)));// 28Jun2018
                // Arpitha

                // 11July2018 Arpitha
                aqPMSel.id(R.id.etlmpdate).text("");
                aqPMSel.id(R.id.etgest).text("");
                break;
            }

            case R.id.gridView1:
                break;
            case R.id.rdbAdol://Ramesh 13-may-2021
                aqPMSel.id(R.id.rdbPreg).checked(false);
                aqPMSel.id(R.id.rdbMother).checked(false);
                aqPMSel.id(R.id.rdbchild).checked(false);

                radGrpEDDorLmp.clearCheck();//Arpitha2021
                aqPMSel.id(R.id.radLMP).checked(false);
                aqPMSel.id(R.id.radEDD).checked(false);
                aqPMSel.id(R.id.tvFirstPregHeading).enabled(false);
                aqPMSel.id(R.id.chkFirstPreg).enabled(false).checked(false);
                aqPMSel.id(R.id.radEDDorLmp).gone();
                aqPMSel.id(R.id.vHLine5).gone();
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.deldate)));
                aqPMSel.id(R.id.etMotherAdd).text("");
                aqPMSel.id(R.id.llMotherAdd).gone();
                aqPMSel.id(R.id.llFirstPreg).gone();
                aqPMSel.id(R.id.llnote).gone();
                aqPMSel.id(R.id.etregdate).enabled(false);
                aqPMSel.id(R.id.etregdate).textColor(R.color.fontcolor_disable);
                aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());//21March2019 Arpitha
                aqPMSel.id(R.id.lllmpdate).gone();
                aqPMSel.id(R.id.llgest).gone();
                aqPMSel.id(R.id.vHLine4).gone();
                aqPMSel.id(R.id.vHLine6).gone();
                break;
            default: {
                break;
            }
        }
    }

    private void validateAndRegister() throws Exception {
        try {
            Intent nextScreen;
            String lmpOrAdd = aqPMSel.id(R.id.etMotherAdd).getText().toString();
            String regDate = aqPMSel.id(R.id.etregdate).getText().toString();

            if ((lmpOrAdd != null && lmpOrAdd.length() > 0) && (regDate != null && regDate.length() > 0)) {

                if (aqPMSel.id(R.id.rdbPreg).isChecked()) {
                    if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0) {
                        int days = DateTimeUtil.getDaysBetweenDates(regDate, lmpOrAdd);
                        int daysdiff = DateTimeUtil
                                .getDaysBetweenDates(DateTimeUtil.getTodaysDate(), lmpOrAdd);
                        if (aqPMSel.id(R.id.radEDD).isChecked()) {
                            if (days >= 0) {
                                wantToCloseDialog = false;
                                Toast.makeText(getApplicationContext(),
                                        getResources().getString(R.string.date_val_5_toast),

                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etMotherAdd).text("");
                            } else if (noOfDays < 30) {
                                wantToCloseDialog = false;
                                Toast.makeText(getApplicationContext(),
                                        getResources().getString(R.string.gestation_must_be_30days),
                                        Toast.LENGTH_LONG).show();
                            } else if (noOfDays > 280) {
                                wantToCloseDialog = false;
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.gest_280),
                                        Toast.LENGTH_LONG).show();
                            } else {
                                wantToCloseDialog = true;
                                woman = new tblregisteredwomen();
                                nextScreen = new Intent(getApplicationContext(), RegistrationActivity.class);
                                nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                nextScreen.putExtra("adolAadhar",currentAdolDetails.getRegAdolAadharNo());//01Sep2021 Arpitha
                                woman.setRegPregnantorMother(1);
                                int gravida = 0;
                                if (aqPMSel.id(R.id.chkFirstPreg).isChecked())
                                    gravida = 1;
                                woman.setRegGravida(gravida);
                                woman.setRegRegistrationDate(aqPMSel.id(R.id.etregdate).getText().toString());
                                woman.setRegEDD(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                                nextScreen.putExtra("woman", woman);
                                startActivity(nextScreen);
                            }
                        } else {
                            if (days < 30) {
                                wantToCloseDialog = false;
                                Toast.makeText(getApplicationContext(),
                                        getResources().getString(R.string.date_val_3_toast),

                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etlmpdate).text("");
                                aqPMSel.id(R.id.etMotherAdd).text("");
                            } else if (daysdiff > 280) {

                                wantToCloseDialog = false;
                                Toast.makeText(getApplicationContext(),
                                        activity.getResources().getString(R.string.date_val_4_toast),
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etlmpdate).text("");
                                aqPMSel.id(R.id.etMotherAdd).text("");
                            } else {
                                wantToCloseDialog = true;
                                woman = new tblregisteredwomen();
                                nextScreen = new Intent(activity, RegistrationActivity.class);
                                nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                nextScreen.putExtra("adolAadhar",currentAdolDetails.getRegAdolAadharNo());//01Sep2021 Arpitha


                                woman.setRegPregnantorMother(1);
                                int gravida = 0;
                                if (aqPMSel.id(R.id.chkFirstPreg).isChecked())
                                    gravida = 1;
                                woman.setRegGravida(gravida);
                                woman.setRegRegistrationDate(aqPMSel.id(R.id.etregdate).getText().toString());
                                if (aqPMSel.id(R.id.radEDD).isChecked())
                                    woman.setRegEDD(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                                else
                                    woman.setRegLMP(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                                nextScreen.putExtra("woman", woman);
                                startActivity(nextScreen);
                            }
                        }

                    }
                } else if (aqPMSel.id(R.id.rdbMother).isChecked()) {
                    if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0) {
                        int days = DateTimeUtil.getDaysBetweenDates(regDate, lmpOrAdd);
                        if (days < 0) {
                            wantToCloseDialog = false;
                            Toast.makeText(activity, (getResources().getString(R.string.m077)),
                                    Toast.LENGTH_LONG).show();
                            // } else if (days > 549) {
                        } else if (days > 5844) {// Arpitha
                            // 28Jun2018
                            wantToCloseDialog = false;
                            Toast.makeText(activity,
                                    (getResources().getString(R.string.date_val_7_toast)), // 11july2018
                                    // Arpitha
                                    Toast.LENGTH_LONG).show();
                        } else {
                            wantToCloseDialog = true;
                            woman = new tblregisteredwomen();
                            nextScreen = new Intent(activity, RegistrationActivity.class);
                            nextScreen.putExtra("adolAadhar",currentAdolDetails.getRegAdolAadharNo());//01Sep2021 Arpitha
                            woman.setRegPregnantorMother(2);
                            woman.setRegADDate(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                            woman.setRegRegistrationDate(aqPMSel.id(R.id.etregdate).getText().toString());
                            nextScreen.putExtra("woman", woman);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(nextScreen);
                        }
                    }
                } else if (aqPMSel.id(R.id.rdbchild).isChecked()) {
                    if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0) {
                        int days = DateTimeUtil.getDaysBetweenDates(regDate, lmpOrAdd);
                        if (days < 0) {
                            wantToCloseDialog = false;
                            Toast.makeText(activity, (getResources().getString(R.string.m077)),
                                    Toast.LENGTH_LONG).show();
                            // } else if (days > 549) {
                        } else if (days > 5844) {// Arpitha
                            // 28Jun2018
                            wantToCloseDialog = false;
                            Toast.makeText(activity,
                                    (getResources().getString(R.string.date_val_7_toast)), // 11july2018
                                    // Arpitha
                                    Toast.LENGTH_LONG).show();
                        } else {
                            wantToCloseDialog = true;
                            woman = new tblregisteredwomen();
                            nextScreen = new Intent(activity, ChildRegistrationActivity.class);
//							woman.setRegPregnantorMother(1);
                            woman.setRegADDate(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                            woman.setRegRegistrationDate(aqPMSel.id(R.id.etregdate).getText().toString());
                            nextScreen.putExtra("woman", woman);
                            nextScreen.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                            startActivity(nextScreen);
                        }
                    }
                } else if (aqPMSel.id(R.id.rdbAdol).isChecked()) { //Ramesh 13-may-2021
                    if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0) {
                        int days = DateTimeUtil.getDaysBetweenDates(regDate, lmpOrAdd);
                        if (days < 0) {
                            wantToCloseDialog = false;
                            Toast.makeText(activity, (getResources().getString(R.string.m077)),
                                    Toast.LENGTH_LONG).show();
                            // } else if (days > 549) {
                        } else if (days > 5844) {// Arpitha
                            // 28Jun2018
                            wantToCloseDialog = false;
                            Toast.makeText(activity,
                                    (getResources().getString(R.string.date_val_7_toast)), // 11july2018
                                    // Arpitha
                                    Toast.LENGTH_LONG).show();
                        } else {
                            wantToCloseDialog = true;
                            tblAdolReg adolReg = new tblAdolReg();
                            nextScreen = new Intent(activity, AdolRegistration.class);
                            adolReg.setRecordCreatedDate(aqPMSel.id(R.id.etregdate).getText().toString());
                            nextScreen.putExtra("AdolReg", adolReg);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(nextScreen);
                        }
                    }
                }
            } else {
                wantToCloseDialog = false;
                if (aqPMSel.id(R.id.rdbPreg).isChecked()) {
                    if (aqPMSel.id(R.id.etMotherAdd).getText().toString().trim().length() <= 0) {
                        aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                        if (aqPMSel.id(R.id.radEDD).isChecked())
                            Toast.makeText(activity,
                                    (activity.getResources().getString(R.string.m170)),
                                    Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(activity,
                                    activity.getResources().getString(R.string.m074),
                                    Toast.LENGTH_LONG).show();
                    } else if (aqPMSel.id(R.id.etregdate).getText().toString().trim().length() <= 0) {
                        aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                        Toast.makeText(getApplicationContext(),
                                (activity.getResources().getString(R.string.m157)),
                                Toast.LENGTH_LONG).show();
                    }
                } else if (aqPMSel.id(R.id.rdbMother).isChecked()) {
                    if (aqPMSel.id(R.id.etMotherAdd).getText().toString().trim().length() <= 0) {
                        aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                        Toast.makeText(activity,
                                (activity.getResources().getString(R.string.m066)),
                                Toast.LENGTH_LONG).show();
                    } else if (aqPMSel.id(R.id.etregdate).getText().toString().trim().length() <= 0) {
                        aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                        Toast.makeText(activity,
                                activity.getResources().getString(R.string.m157),
                                Toast.LENGTH_LONG).show();
                    }
                } else if (aqPMSel.id(R.id.rdbchild).isChecked()) {
                    wantToCloseDialog = true;
//					if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0)
                    {
//						int days = DateTimeUtil.getDaysBetweenDates(regDate, lmpOrAdd);
						/*if (days < 0) {
							wantToCloseDialog = false;
							Toast.makeText(activity, (getResources().getString(R.string.m077)),
									Toast.LENGTH_LONG).show();
							// } else if (days > 549) {
						} else if (days > 5844) {// Arpitha
							// 28Jun2018
							wantToCloseDialog = false;
							Toast.makeText(activity,
									(getResources().getString(R.string.date_val_7_toast)), // 11july2018
									// Arpitha
									Toast.LENGTH_LONG).show();
						} else {*/
                        wantToCloseDialog = true;
                        woman = new tblregisteredwomen();
                        nextScreen = new Intent(activity, ChildRegistrationActivity.class);
                        woman.setRegPregnantorMother(1);
                        woman.setRegADDate(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                        woman.setRegRegistrationDate(aqPMSel.id(R.id.etregdate).getText().toString());
                        nextScreen.putExtra("woman", woman);
                        nextScreen.putExtra("globalState", activity.getIntent().getBundleExtra("globalState"));
                        startActivity(nextScreen);
//						}
                    }
                } else if (aqPMSel.id(R.id.rdbAdol).isChecked()) { //Ramesh 13-may-2021

                    wantToCloseDialog = true;
                    tblAdolReg adolReg = new tblAdolReg();
                    nextScreen = new Intent(activity, AdolRegistration.class);
                    adolReg.setRecordCreatedDate(aqPMSel.id(R.id.etregdate).getText().toString());
                    nextScreen.putExtra("AdolReg", adolReg);
                    nextScreen.putExtra("globalState", activity.getIntent().getBundleExtra("globalState"));
                    startActivity(nextScreen);


                }
            }

        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }
}