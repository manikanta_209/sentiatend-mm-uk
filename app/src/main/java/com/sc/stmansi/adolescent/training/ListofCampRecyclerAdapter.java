package com.sc.stmansi.adolescent.training;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.sc.stmansi.R;
import com.sc.stmansi.tables.tblCampDetails;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ListofCampRecyclerAdapter extends RecyclerView.Adapter<ListofCampViewHolder> {
    private final Context context;
    private final List<tblCampDetails> allCampaigns;
    private final List<tblCampDetails> modellist;

    String[] trainingnameengkan ;
    String[] trainingNames ; //15May2021 Bindu check from eng str

    public ListofCampRecyclerAdapter(List<tblCampDetails> allCampaigns, Context applicationContext) {
        this.allCampaigns = allCampaigns;
        modellist = new ArrayList<>();
        modellist.addAll(allCampaigns);
        context = applicationContext;

        trainingnameengkan = context.getResources().getStringArray(R.array.adoltrainingname);
        trainingNames = context.getResources().getStringArray(R.array.adoltrainingnamecheck); //15May2021 Bindu check from eng str

    }

    @NonNull
    @Override
    public ListofCampViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CardView v = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_campaigndetails, parent, false);
        return new ListofCampViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ListofCampViewHolder holder, int position) {
        try {
//            holder.CampName.setText(context.getResources().getString(R.string.trainingname)+" : "+allCampaigns.get(position).getTblCampName());
            int trainingIndex = 0;
//            for (int i = 0; i < trainingNames.length; i++) {
//                if (trainingNames[i].equals(allCampaigns.get(position).getTblCampName())) {
//                    trainingIndex = i;
//           holder.CampName.setText(context.getResources().getString(R.string.trainingname) + " : " + trainingnameengkan[trainingIndex]);
//                }
//            }
            if (allCampaigns.get(position).getTblCampName().length()>2){ //23May2021 Bindu - check length for 2
                holder.CampName.setText(context.getResources().getString(R.string.trainingname) + " : " + allCampaigns.get(position).getTblCampName());
            }else {
                try{
                    trainingIndex = Integer.parseInt(allCampaigns.get(position).getTblCampName());
                    holder.CampName.setText(context.getResources().getString(R.string.trainingname) + " : " + trainingnameengkan[trainingIndex]);
                }catch (Exception e){
                    Throwable cause = e;
                    while (cause.getCause() != null && cause.getCause() != cause) {
                        cause = cause.getCause();
                    }
                    String exception = cause.toString();
                    if (exception.toLowerCase().contains("numberformatexception")) {
                        holder.CampName.setText(context.getResources().getString(R.string.trainingname) + " : " + allCampaigns.get(position).getTblCampName());
                    }
                }
            }
            holder.CampCreatedDate.setText(context.getResources().getString(R.string.trainingdate)+" : "+allCampaigns.get(position).getTblCampDateTime());
            holder.CampCreatedBy.setText(allCampaigns.get(position).getTblCampCreatedBy());
//            holder.CampSchemaType.setText(allCampaigns.get(position).getTblCampSchemaType() == 1 ? "Private" : "Government");
            String[] schemaTypes = context.getResources().getStringArray(R.array.SchemaType);  //15May2021 Bindu get it from strings array
            holder.CampSchemaType.setText(schemaTypes[allCampaigns.get(position).getTblCampSchemaType()]);

             if (allCampaigns.get(position).getTblCampisActive()==1){
                holder.isActive.setVisibility(View.VISIBLE);
            }else {
                holder.isActive.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return allCampaigns.size();
    }

    public void filter(String charText) {
        try {
            charText = charText.toLowerCase(Locale.getDefault());
            allCampaigns.clear();
            if (charText.length() == 0) {
                allCampaigns.addAll(modellist);
            } else {
                for (tblCampDetails model : modellist) {
                    String tName = null;
                     if (model.getTblCampName().length()>2){
                         tName = model.getTblCampName();
                         if (tName.toLowerCase().contains(charText.toLowerCase())) {
                             allCampaigns.add(model);
                         }
                    }else {
                         try{
                             tName = trainingnameengkan[Integer.parseInt(model.getTblCampName())];
                             if (tName.toLowerCase().contains(charText.toLowerCase())) {
                                 allCampaigns.add(model);
                             }
                         }catch (Exception e){
                             Throwable cause = e;
                             while (cause.getCause() != null && cause.getCause() != cause) {
                                 cause = cause.getCause();
                             }
                             String exception = cause.toString();
                             if (exception.toLowerCase().contains("numberformatexception")) {
                                 if (model.getTblCampName().toLowerCase().contains(charText.toLowerCase())) {
                                     allCampaigns.add(model);
                                 }
                             }
                         }
                     }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        notifyDataSetChanged();
    }
}
