package com.sc.stmansi.adolescent;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableRow;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.ImageStore.RecyclerViewAdapter;
import com.sc.stmansi.R;
import com.sc.stmansi.common.AlertDialogUtil;
import com.sc.stmansi.common.NavDrawerItem;
import com.sc.stmansi.common.NavDrawerListAdapter;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.configuration.SyncState;
import com.sc.stmansi.database.DBMethods;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.repositories.AdolPregnantRepository;
import com.sc.stmansi.repositories.AdolVisitHeaderRepository;
import com.sc.stmansi.repositories.AdolescentRepository;
import com.sc.stmansi.repositories.CovidDetailsRepository;
import com.sc.stmansi.repositories.ImageStoreRepository;
import com.sc.stmansi.tables.tblAdolPregnant;
import com.sc.stmansi.tables.tblAdolReg;
import com.sc.stmansi.tables.tblAdolVisitHeader;
import com.sc.stmansi.tables.tblCovidTestDetails;
import com.sc.stmansi.tables.tblCovidVaccineDetails;
import com.sc.stmansi.tables.tblImageStore;

import java.util.ArrayList;
import java.util.List;

public class AdolVisitView extends AppCompatActivity {
    private AppState appState;
    private DatabaseHelper databaseHelper;
    private AQuery aq;
    private SyncState syncState;
    private String AdolId;
    private String VisitCount;
    private tblAdolVisitHeader currentAdolVisitDetails;
    private tblAdolReg currentAdolDetails;
    private tblAdolPregnant currentPregnantDetails;
    private DrawerLayout drawer;
    private ListView drawerList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adolvisit_view);
        try {
            Bundle bundle = getIntent().getBundleExtra("globalState");
            if (bundle != null) {
                appState = bundle.getParcelable("appState");
                syncState = bundle.getParcelable("syncState");
            } else {
                appState = new AppState();
            }
            AdolId = getIntent().getStringExtra("adolID");
            VisitCount = getIntent().getStringExtra("VisitCount");
            Initialize();
            InitializeDrawer();
            getSupportActionBar().hide();

            getData();
            getImages();//28/6/2021 Ramesh

            UpdateUI();

            aq.id(R.id.toolbarBtnHome).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        displayDialogExit();
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }
                }
            });

            aq.id(R.id.btnAVVNext).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        AdolVisitHeaderRepository adolVisitHeaderRepository = new AdolVisitHeaderRepository(databaseHelper);
                        int count = adolVisitHeaderRepository.getAllVisits(AdolId).size();
                        int vCount = Integer.parseInt(VisitCount);
                        if (vCount < count) {
                            //next page
                            Intent intent = new Intent(AdolVisitView.this, AdolVisitView.class);
                            intent.putExtra("globalState", prepareBundle());
                            intent.putExtra("VisitCount", String.valueOf(vCount + 1));
                            intent.putExtra("adolID", AdolId);
                            startActivity(intent);
                        } else {
                            //no records
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.norecords), Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }
                }
            });
            aq.id(R.id.btnAVVPrev).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        int vCount = Integer.parseInt(VisitCount);
                        if ((vCount - 1) != 0) {
                            Intent intent = new Intent(AdolVisitView.this, AdolVisitView.class);
                            intent.putExtra("globalState", prepareBundle());
                            intent.putExtra("VisitCount", String.valueOf(vCount - 1));
                            intent.putExtra("adolID", AdolId);
                            startActivity(intent);
                        } else {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.norecords), Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }
                }
            });
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    private void UpdateUI() {
        try {
            String dateTime = currentAdolDetails.getRecordCreatedDate();
            int index = dateTime.indexOf(" ");
            String date = dateTime.substring(0, index);
            aq.id(R.id.toolbartxtTitle).getTextView().setText(currentAdolDetails.getRegAdolName() + "\n" + date);
            aq.id(R.id.txt_AVV_Number).getTextView().setText(currentAdolVisitDetails.getVisitId());
            aq.id(R.id.txt_AVV_VisitDate).getTextView().setText(getResources().getString(R.string.visitdatecolon) + currentAdolVisitDetails.getAdolvisHVisitDate());

            aq.id(R.id.txt_AVV_Age).getTextView().setText(currentAdolDetails.getRegAdolAge());
            aq.id(R.id.txt_AVV_GTS).getTextView().setText(currentAdolVisitDetails.getAdolvisHGTS().equals("Yes") ? getResources().getString(R.string.m121) : getResources().getString(R.string.m122));

            //mani AUG-19-2021
            if (currentAdolVisitDetails.getAdolvisHAshaAvailable().equalsIgnoreCase("Y"))
                 aq.id(R.id.txt_AVV_AshaAvail).getTextView().setText(getResources().getString(R.string.yes));
            else if (currentAdolVisitDetails.getAdolvisHAshaAvailable().equalsIgnoreCase("N"))
                aq.id(R.id.txt_AVV_AshaAvail).getTextView().setText(getResources().getString(R.string.no));
            else if (currentAdolVisitDetails.getAdolvisHAshaAvailable().equalsIgnoreCase("Dont Know"))
                aq.id(R.id.txt_AVV_AshaAvail).getTextView().setText(getResources().getString(R.string.dontknow));



            int eduIndex = Integer.parseInt(currentAdolVisitDetails.getAdolvisHEducation());
            String[] education = getResources().getStringArray(R.array.education);
            aq.id(R.id.txt_AVV_Education).getTextView().setText(education[eduIndex]);


            if (currentAdolDetails.getRegAdolGender().equals("Male")) {//ramesh 18 may 2021 to hide female details for men
                aq.id(R.id.txt_AVV_mensturalTitle).getTextView().setVisibility(View.GONE);
                aq.id(R.id.tr_AVV_isPeriod).getView().setVisibility(View.GONE);

                aq.id(R.id.txt_AVV_Pregnant).getTextView().setVisibility(View.GONE);
                aq.id(R.id.tr_AVV_ISPregnant).getView().setVisibility(View.GONE);
            }

            aq.id(R.id.txt_AVV_IsPeriod).getTextView().setText(currentAdolVisitDetails.getAdolvisHIsPeriodstarted().equals("Yes") ? getResources().getString(R.string.m121) : getResources().getString(R.string.m122));
            LinearLayout llPeriod = findViewById(R.id.ll_AVV_Period);
            if (currentAdolVisitDetails.getAdolvisHIsPeriodstarted().equals("Yes")) {
                llPeriod.setVisibility(View.VISIBLE);
                aq.id(R.id.txt_AVV_AtWhatAgePeriod).getTextView().setText(currentAdolVisitDetails.getAdolvisHAgeMenstural());
                String[] useforperiod = getResources().getStringArray(R.array.normaluseperiod);
                String mPeriod = currentAdolVisitDetails.getAdolvisHUsedforPeriods();
                if (mPeriod.length() > 0) {
                    String[] mPeriodaftersplit = mPeriod.split(",");
                    StringBuilder originaluseforPeriod = new StringBuilder();
                    for (String s : mPeriodaftersplit) {
                        if (!s.trim().equals("6")) {
                            originaluseforPeriod.append(useforperiod[Integer.parseInt(s.trim())]).append(",");
                        } else {
                            originaluseforPeriod.append(currentAdolVisitDetails.getAdolvisHUsedforPeriodsOthers()).append(",");
                        }
                    }
                    originaluseforPeriod = originaluseforPeriod.deleteCharAt(originaluseforPeriod.lastIndexOf(","));
                    aq.id(R.id.txt_AVV_normalUseforPeriod).getTextView().setText(originaluseforPeriod);
                } else {
                    aq.id(R.id.txt_AVV_normalUseforPeriod).getTextView().setText("-");
                }
                String[] menustralproblems = getResources().getStringArray(R.array.menustralproblems);
                String mProblems = currentAdolVisitDetails.getAdolvisHMenstrualProblem();
                if (mProblems.length() > 0) {
                    String[] mProblemsaftersplit = mProblems.split(",");
                    StringBuilder originalMensutralProblems = new StringBuilder();
                    for (String s : mProblemsaftersplit) {
                        if (!s.trim().equals("8")) {
                            originalMensutralProblems.append(menustralproblems[Integer.parseInt(s.trim())]).append(",");
                        } else {
                            originalMensutralProblems.append(currentAdolVisitDetails.getAdolvisHMenstrualProblemOthers()).append(",");
                        }
                    }
                    originalMensutralProblems = originalMensutralProblems.deleteCharAt(originalMensutralProblems.lastIndexOf(","));
                    aq.id(R.id.txt_AVV_MensturalProblem).getTextView().setText(originalMensutralProblems);
                } else {
                    aq.id(R.id.txt_AVV_MensturalProblem).getTextView().setText("-");
                }
            } else {
                llPeriod.setVisibility(View.GONE);
            }
            int MStatusIndex = currentAdolVisitDetails.getAdolvisHMaritalStatus();
            String[] MaritalStatus = getResources().getStringArray(R.array.Martialstatus);
            aq.id(R.id.txt_AVV_MaritalStatus).getTextView().setText(MaritalStatus[MStatusIndex]);
            LinearLayout llMarital = findViewById(R.id.ll_AVV_MaritalStatus);
            if (MStatusIndex > 1) {
                llMarital.setVisibility(View.VISIBLE);
                if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                    aq.id(R.id.txt_AVV_partnerName_Key).getTextView().setText(getResources().getString(R.string.wifename));
                    aq.id(R.id.txt_AVV_partnerOccupa_Key).getTextView().setText(getResources().getString(R.string.wifeoccupa));
                } else {
                    aq.id(R.id.txt_AVV_partnerName_Key).getTextView().setText(getResources().getString(R.string.tvhusbname));
                    aq.id(R.id.txt_AVV_partnerOccupa_Key).getTextView().setText(getResources().getString(R.string.husbandoccupa));
                }
                aq.id(R.id.txt_AVV_partnerName).getTextView().setText(currentAdolVisitDetails.getAdolvisHPartnerName());
                aq.id(R.id.txt_AVV_partnerOccupa).getTextView().setText(currentAdolVisitDetails.getAdolvisHPartnerOccup());
                aq.id(R.id.txt_AVV_ageatmarriage).getTextView().setText(currentAdolVisitDetails.getAdolvisHAgeatMarriage());
            } else {
                llMarital.setVisibility(View.GONE);
            }
            aq.id(R.id.txt_AVV_isPregnant).getTextView().setText(currentAdolVisitDetails.getAdolvisHIsPregnantatReg().equals("Yes") ? getResources().getString(R.string.m121) : getResources().getString(R.string.m122));
            LinearLayout llPregnant = findViewById(R.id.ll_AVV_Pregnant);
            llPregnant.setVisibility(View.GONE);


            if (currentAdolVisitDetails.getAdolvisHHb().length() != 0) {
                aq.id(R.id.txt_AVV_HBCount).getTextView().setText(currentAdolVisitDetails.getAdolvisHHb());
                aq.id(R.id.txt_AVV_HBTestDate).getTextView().setText(currentAdolVisitDetails.getAdolvisHHbDate());
                switch (currentAdolVisitDetails.getAdolvisHHbCategory()) {
                    case "Severe":
                        aq.id(R.id.txt_AVV_HBCategory).getTextView().setText(getResources().getString(R.string.severe));
                        break;
                    case "Moderate":
                        aq.id(R.id.txt_AVV_HBCategory).getTextView().setText(getResources().getString(R.string.moderate));
                        break;
                    case "Mild":
                        aq.id(R.id.txt_AVV_HBCategory).getTextView().setText(getResources().getString(R.string.mild));
                        break;
                    case "Normal":
                        aq.id(R.id.txt_AVV_HBCategory).getTextView().setText(getResources().getString(R.string.normal));
                        break;
                    default:
                        aq.id(R.id.txt_AVV_HBCategory).getTextView().setText("-");
                }
            } else {
                aq.id(R.id.txt_AVV_HBCount).getTextView().setText("-");
                aq.id(R.id.txt_AVV_HBTestDate).getTextView().setText("-");
                aq.id(R.id.txt_AVV_HBCategory).getTextView().setText("-");
            }

            aq.id(R.id.txt_AVV_Height).getTextView().setText(currentAdolVisitDetails.getAdolvisHHeight());
            switch (Integer.parseInt(currentAdolVisitDetails.getAdolvisHHeightType())) {
                case 1:
                    aq.id(R.id.txt_AVV_Height).getTextView().setText(currentAdolVisitDetails.getAdolvisHHeight() + getResources().getString(R.string.centimeter));
                    break;
                case 2:
                    String height = currentAdolVisitDetails.getAdolvisHHeight();
                    String[] feetandinches = height.split("\\.");
                    int selectfeet = Integer.parseInt(feetandinches[0]);
                    int inch = Integer.parseInt(feetandinches[1]);

                    aq.id(R.id.txt_AVV_Height).getTextView().setText(selectfeet + "'" + inch + "\"" + getResources().getString(R.string.feet));
                    break;
                case 3:
                    aq.id(R.id.txt_AVV_Height).getTextView().setText(getResources().getString(R.string.dontknow));
                    break;
            }//end of heights
            aq.id(R.id.txt_AVV_Weight).getTextView().setText(currentAdolVisitDetails.getAdolvisHWeight());
            if (currentAdolVisitDetails.getAdolvisHBMI().length()>0 && !currentAdolVisitDetails.getAdolvisHHeightType().equals("3") && currentAdolVisitDetails.getAdolvisHWeight().length()>0){
                double BMI = Double.parseDouble(currentAdolVisitDetails.getAdolvisHBMI());
                if (BMI == 0) {
                    aq.id(R.id.txt_AVV_BMI).getTextView().setText("-");
                } else {
                    aq.id(R.id.txt_AVV_BMI).getTextView().setText(String.format("%.2f", BMI));
                }
            } else {
                aq.id(R.id.txt_AVV_BMI).getTextView().setText("-");
            }

            aq.id(R.id.txt_AVV_UsingIFA).getTextView().setText(currentAdolVisitDetails.getAdolvisHIsIFATaken().equals("Yes") ? getResources().getString(R.string.m121) : getResources().getString(R.string.m122));
            LinearLayout llIFA = findViewById(R.id.ll_AVV_IFA);
            if (currentAdolVisitDetails.getAdolvisHIsIFATaken().equals("Yes")) {
                llIFA.setVisibility(View.VISIBLE);
                aq.id(R.id.txt_AVV_ConsumeIFA).getTextView().setText(currentAdolVisitDetails.getAdolvisHIFADaily().equals("Yes") ? getResources().getString(R.string.m121) : getResources().getString(R.string.m122));
                if (currentAdolVisitDetails.getAdolvisHIFADaily().equals("Yes")) {
                    TableRow trTabletsConsumePerDay = findViewById(R.id.trTabletsConsumePerDay);
                    trTabletsConsumePerDay.setVisibility(View.VISIBLE);
                    aq.id(R.id.txt_AVV_ConsumePerDay).getTextView().setText(currentAdolVisitDetails.getAdolvisHIFATabletsCount());
                } else {
                    TableRow trIFAReason = findViewById(R.id.trIFAReason);
                    trIFAReason.setVisibility(View.VISIBLE);
                    String[] ifaReasons = getResources().getStringArray(R.array.ifareasons);
                    String reasons = currentAdolVisitDetails.getAdolvisHIFANotTakenReason();
                    String[] reasonAfterSplit = reasons.split(",");
                    StringBuilder origonalReasons = new StringBuilder();
                    if (reasons.length() > 0) {
                        for (String s : reasonAfterSplit) {
                            origonalReasons.append(ifaReasons[Integer.parseInt(s.trim())]).append(",");
                        }
                    }
                    origonalReasons = origonalReasons.deleteCharAt(origonalReasons.lastIndexOf(","));
                    aq.id(R.id.txt_AVV_IFAReason).getTextView().setText(origonalReasons.toString());
                }
                int whogivenIndex = currentAdolVisitDetails.getAdolvisHIFAGivenBy();
                String[] whogave = getResources().getStringArray(R.array.whogaveifatablets);
                aq.id(R.id.txt_AVV_WhoGaveIFA).getTextView().setText(whogave[whogivenIndex]);
            } else {
                llIFA.setVisibility(View.GONE);
            }

            if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                aq.id(R.id.txt_AVV_ContraceptionTitle).getTextView().setVisibility(View.GONE);
                aq.id(R.id.ll_aVV_Contraception).getView().setVisibility(View.GONE);
            } else {
                aq.id(R.id.txt_AVV_ContraceptionTitle).getTextView().setVisibility(View.VISIBLE);
                aq.id(R.id.ll_aVV_Contraception).getView().setVisibility(View.VISIBLE);
            }

            aq.id(R.id.txt_AVV_AwareofContraception).getTextView().setText(currentAdolVisitDetails.getAdolvisHContraceptionAware().equals("Yes") ? getResources().getString(R.string.m121) : getResources().getString(R.string.m122));
            aq.id(R.id.txt_AVV_NeedContreception).getTextView().setText(currentAdolVisitDetails.getAdolvisHContraceptionNeed().equals("Yes") ? getResources().getString(R.string.m121) : getResources().getString(R.string.m122));

            //23May2021 Bindu - check null and set val from array
            if (currentAdolVisitDetails.getAdolvisHContraceptionNeed().equals("Yes")) {
                String[] contmtds = getResources().getStringArray(R.array.contraceptionmethod);
                if (currentAdolVisitDetails.getAdolvisHContraceptionMethod() != null && currentAdolVisitDetails.getAdolvisHContraceptionMethod().length() > 0) {
                    int pos = Integer.parseInt(currentAdolVisitDetails.getAdolvisHContraceptionMethod());
                    aq.id(R.id.txt_AVV_MethodContraception).getTextView().setText(contmtds[pos]);
                } else {
                    aq.id(R.id.txt_AVV_MethodContraception).getTextView().setText(currentAdolVisitDetails.getAdolvisHContraceptionMethodOthers());
                }
            } else {
                aq.id(R.id.tr_AVVMethodContraception).getView().setVisibility(View.GONE);
            }

            switch (currentAdolVisitDetails.getAdolvisHDewormingtab()) {
                case "Yes":
                    aq.id(R.id.txt_AVV_Deworming).getTextView().setText(getResources().getString(R.string.m121));
                    break;
                case "No":
                    aq.id(R.id.txt_AVV_Deworming).getTextView().setText(getResources().getString(R.string.m122));
                    break;
                default:
                    aq.id(R.id.txt_AVV_Deworming).getTextView().setText(getResources().getString(R.string.dontknow));
            }


            if (currentAdolVisitDetails.getAdolvisHHealthIssues() != null && currentAdolVisitDetails.getAdolvisHHealthIssues().length() > 0) {
                aq.id(R.id.txt_AVV_SufferHealthIssue).getTextView().setText(getResources().getString(R.string.m121));
            } else {
                aq.id(R.id.txt_AVV_SufferHealthIssue).getTextView().setText(getResources().getString(R.string.m122));
            }

            if (currentAdolVisitDetails.getAdolvisHHealthIssues() != null && currentAdolVisitDetails.getAdolvisHHealthIssues().length() != 0) {
                String[] healthproblems = getResources().getStringArray(R.array.healthproblemsuffer);
                String hProblems = currentAdolVisitDetails.getAdolvisHHealthIssues();
                if (hProblems.length() > 0) {
                    String[] hProblemsaftersplit = hProblems.split(",");
                    StringBuilder originalHealthProblems = new StringBuilder();
                    for (String s : hProblemsaftersplit) {
                        if (!s.trim().equals("9")) {
                            originalHealthProblems.append(healthproblems[Integer.parseInt(s.trim())]).append(",");
                        } else {
                            originalHealthProblems.append(currentAdolVisitDetails.getAdolvisHHealthIssuesOthers()).append(",");
                        }
                    }
                    originalHealthProblems = originalHealthProblems.deleteCharAt(originalHealthProblems.lastIndexOf(","));
                    aq.id(R.id.txt_AVV_HealthIssues).getTextView().setText(originalHealthProblems);
                } else {
                    aq.id(R.id.txt_AVV_HealthIssues).getTextView().setText(getResources().getString(R.string.m122));
                }
            } else {
                aq.id(R.id.txt_AVV_HealthIssues).getTextView().setText(getResources().getString(R.string.m122));
            }

            if (currentAdolVisitDetails.getAdolvisHTreatedAt().length() == 0) {
                aq.id(R.id.txt_AVV_WhereTreated).getTextView().setText("-");
            } else {
                aq.id(R.id.txt_AVV_WhereTreated).getTextView().setText(currentAdolVisitDetails.getAdolvisHTreatedAt());
            }

            if (currentAdolVisitDetails.getAdolvisHGeneralTreatment().length() == 0) {
                aq.id(R.id.txt_AVV_HealthTreatedGenerally).getTextView().setText("-");
            } else {
                String[] treatmentsite = getResources().getStringArray(R.array.treatmentplaces);
                String tsites = currentAdolVisitDetails.getAdolvisHGeneralTreatment();
                if (tsites.length() > 0) {
                    String[] tsitesaftersplit = tsites.split(",");
                    StringBuilder originaltreatmentsite = new StringBuilder();
                    for (String s : tsitesaftersplit) {
                        if (!s.trim().equals("5")) {
                            originaltreatmentsite.append(treatmentsite[Integer.parseInt(s.trim())]).append(",");
                        } else {
                            originaltreatmentsite.append(currentAdolVisitDetails.getAdolvisHGeneralTreatmentOthers()).append(",");
                        }
                    }
                    originaltreatmentsite = originaltreatmentsite.deleteCharAt(originaltreatmentsite.lastIndexOf(","));
                    aq.id(R.id.txt_AVV_HealthTreatedGenerally).getTextView().setText(originaltreatmentsite);
                } else {
                    aq.id(R.id.txt_AVV_HealthTreatedGenerally).getTextView().setText("-");
                }
            }


            getcovidVaccineData();


        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private void getcovidVaccineData() {
        try {
            CovidDetailsRepository covidDetailsRepository = new CovidDetailsRepository(databaseHelper);
            List<tblCovidVaccineDetails> vaccineDatas = covidDetailsRepository.getAdolVisitCovidVaccinDetails(AdolId);
            List<tblCovidTestDetails> testDatas = covidDetailsRepository.getAdolVisitCovidTestDetails(VisitCount, AdolId);

            if (testDatas != null && testDatas.size() > 0) {
                if (testDatas.get(0).getCovidTest().equals("Y")) {
                    aq.id(R.id.txt_AVV_CovidTest).text(getResources().getString(R.string.m121));
                    aq.id(R.id.tr_CovidTestResult).getView().setVisibility(View.VISIBLE);
                    aq.id(R.id.tr_CovidTestResultDate).getView().setVisibility(View.VISIBLE);
                    aq.id(R.id.txt_AVV_CovidTestResult).text(testDatas.get(0).getCovidResult());
                    aq.id(R.id.txt_AVV_CovidTestResultDate).text(testDatas.get(0).getCovidResultDate());
                } else {
                    aq.id(R.id.txt_AVV_CovidTest).text(getResources().getString(R.string.m122));
                    aq.id(R.id.tr_CovidTestResult).getView().setVisibility(View.GONE);
                    aq.id(R.id.tr_CovidTestResultDate).getView().setVisibility(View.GONE);
                }
            } else {
                aq.id(R.id.txt_AVV_CovidTest).text("-");
                aq.id(R.id.tr_CovidTestResult).getView().setVisibility(View.GONE);
                aq.id(R.id.tr_CovidTestResultDate).getView().setVisibility(View.GONE);
            }

            if (vaccineDatas != null && vaccineDatas.size() > 0) {
                aq.id(R.id.txt_AVV_CovidVaccine).text(getResources().getString(R.string.m121));
                aq.id(R.id.txt_AVV_CovidVaccineDose).getTextView().setText(vaccineDatas.size() == 1 ? "1" : "2");
                for (int i = 0; i < vaccineDatas.size(); i++) {
                    if (vaccineDatas.get(i).getCovidVaccinatedNo().equalsIgnoreCase("1")) {
                        aq.id(R.id.tr_CovidVaccineFirstDoseDate).getView().setVisibility(View.VISIBLE);
                        aq.id(R.id.txt_AVV_CovidVaccineFirstDoseDate).getTextView().setText(vaccineDatas.get(i).getCovidVaccinatedDate());
                        aq.id(R.id.tr_CovidVaccineSecondDoseDate).getView().setVisibility(View.GONE);
                    }
                    if (vaccineDatas.get(i).getCovidVaccinatedNo().equalsIgnoreCase("2")) {
                        aq.id(R.id.tr_CovidVaccineSecondDoseDate).getView().setVisibility(View.VISIBLE);
                        aq.id(R.id.txt_AVV_CovidVaccineSecondDoseDate).getTextView().setText(vaccineDatas.get(i).getCovidVaccinatedDate());
                    }
                }
            } else {
                aq.id(R.id.txt_AVV_CovidVaccine).text(getResources().getString(R.string.m122));
                aq.id(R.id.tr_CovidVaccineDose).getView().setVisibility(View.GONE);
                aq.id(R.id.tr_CovidVaccineFirstDoseDate).getView().setVisibility(View.GONE);
                aq.id(R.id.tr_CovidVaccineSecondDoseDate).getView().setVisibility(View.GONE);
            }

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private void getData() {
        try {
            AdolVisitHeaderRepository adolVisitHeaderRepository = new AdolVisitHeaderRepository(databaseHelper);
            currentAdolVisitDetails = adolVisitHeaderRepository.getVisitbyID(VisitCount, AdolId);
            AdolescentRepository adolescentRepository = new AdolescentRepository(databaseHelper);
            currentAdolDetails = adolescentRepository.getAdolDatafromAdolID(AdolId);
            AdolPregnantRepository adolPregnantRepository = new AdolPregnantRepository(databaseHelper);
            currentPregnantDetails = adolPregnantRepository.getPregnantbyIDs(VisitCount, AdolId);
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private void Initialize() {
        try {
            databaseHelper = getHelper();
            DBMethods dbMethods = new DBMethods(databaseHelper);
            aq = new AQuery(this);
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(getApplicationContext(), DatabaseHelper.class);
        }
        return databaseHelper;
    }

    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        b.putParcelable("syncState", syncState);
        return b;
    }

    private void calculateHB(String hb) {
        try {
            float hbCount = Float.parseFloat(hb);
            if (hbCount < 8.0) {
                //severe
                aq.id(R.id.txt_AVV_HBCategory).getTextView().setText(getResources().getString(R.string.severe));
                aq.id(R.id.txt_AVV_HBCategory).getTextView().setTextColor(Color.RED);
            } else if (hbCount >= 8 && hbCount <= 9.9) {
                //moderate
                aq.id(R.id.txt_AVV_HBCategory).getTextView().setText(getResources().getString(R.string.moderate));
                aq.id(R.id.txt_AVV_HBCategory).getTextView().setTextColor(getResources().getColor(R.color.amber));
            } else if (hbCount >= 10 && hbCount <= 11.9) {
                //mild
                aq.id(R.id.txt_AVV_HBCategory).getTextView().setText(getResources().getString(R.string.mild));
                aq.id(R.id.txt_AVV_HBCategory).getTextView().setTextColor(getResources().getColor(R.color.orange));
            } else if (hbCount >= 11.9 && hbCount <= 24) {
                //normal
                aq.id(R.id.txt_AVV_HBCategory).getTextView().setText(getResources().getString(R.string.normal));
                aq.id(R.id.txt_AVV_HBCategory).getTextView().setTextColor(getResources().getColor(R.color.green_800));
            } else if (hbCount > 24) {
                aq.id(R.id.txt_AVV_HBCategory).getTextView().setText("-");
                aq.id(R.id.txt_AVV_HBCategory).getTextView().setTextColor(Color.RED);
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    @Override
    public void onBackPressed() {
        displayDialogExit();
    }

    private void displayDialogExit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(AdolVisitView.this);
        builder.setMessage(getResources().getString(R.string.exit));
        builder.setCancelable(false);
        builder.setNegativeButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(AdolVisitView.this, AdolVisitHistory.class);
                intent.putExtra("tabItem", 0);
                intent.putExtra("adolID", AdolId);
                intent.putExtra("globalState", prepareBundle());
                startActivity(intent);
            }
        }).setPositiveButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    private void InitializeDrawer() {
        try {
            drawer = findViewById(R.id.drawer_adolVisitView);
            drawerList = findViewById(R.id.lvNav_adolVisitView);

            ArrayList<NavDrawerItem> navDrawerItems = new ArrayList<>();
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.editadolescent), R.drawable.ic_edit_icon));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.homevisitadd), R.drawable.ic_homevisit));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.adolvisithistory), R.drawable.anm_pending_activities));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.trainingparticipated), R.drawable.presentation));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deactivate), R.drawable.deactivate));
            drawer.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
            drawerList.setAdapter(new NavDrawerListAdapter(getApplicationContext(), navDrawerItems));
            drawerList.setOnItemClickListener(new DrawerItemClickListener());
            Toolbar toolbar = findViewById(R.id.toolbarCommon);
            ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close);
            drawer.setDrawerListener(drawerToggle);
            drawerToggle.syncState();
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private class DrawerItemClickListener implements android.widget.AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            drawer.closeDrawer(drawerList);
            try {
                switch (i) {
                    case 0:
                        Intent intent = new Intent(AdolVisitView.this, AdolRegistration.class);
                        intent.putExtra("globalState", prepareBundle());
                        intent.putExtra("isEdit", true);
                        intent.putExtra("adolID", AdolId);
                        startActivity(intent);
                        break;
                    case 1:
                        if (currentAdolDetails.getRegAdolDeactivateDate().length() != 0) {
                            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.adolescentdeactivated), AdolVisitView.this);
                            drawer.closeDrawer(GravityCompat.END);
                        } else {
                            intent = new Intent(AdolVisitView.this, AdolVisit_New.class);
                            intent.putExtra("globalState", prepareBundle());
                            intent.putExtra("adolID", AdolId);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                        break;
                    case 2:
                        intent = new Intent(AdolVisitView.this, AdolVisitHistory.class);
                        intent.putExtra("globalState", prepareBundle());
                        intent.putExtra("adolID", AdolId);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        break;
                    case 3:
                        intent = new Intent(AdolVisitView.this, AdolParticipatedTrainings.class);
                        intent.putExtra("globalState", prepareBundle());
                        intent.putExtra("adolID", AdolId);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        break;
                    case 4:
                        intent = new Intent(AdolVisitView.this, AdolDeactivation.class);
                        intent.putExtra("globalState", prepareBundle());
                        if (currentAdolDetails.getRegAdolDeactivateDate().length() != 0) {
                            intent.putExtra("isView", true);
                        }
                        intent.putExtra("adolID", AdolId);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        break;
                }
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
        }
    }

    private void getImages() {
        try {
            RecyclerView recyclerViewImage = findViewById(R.id.recyclerImage);
            ImageStoreRepository imageStoreRepository = new ImageStoreRepository(databaseHelper);
            String str = currentAdolDetails.getAdolID() + "_HV" + (VisitCount);
            ArrayList<tblImageStore> allImages = imageStoreRepository.getAllImagesofIDandHomeVisit(str);
            if (allImages.size() != 0) {
                DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
                float screenWidthDp = displayMetrics.widthPixels / displayMetrics.density;
                int noOfColumns = (int) (screenWidthDp / 125 + 0.5); // +0.5 for correct rounding to int.
                GridLayoutManager layoutManager = new GridLayoutManager(AdolVisitView.this, noOfColumns);
                recyclerViewImage.setLayoutManager(layoutManager);
                recyclerViewImage.setNestedScrollingEnabled(false);
                recyclerViewImage.setHasFixedSize(true);
                aq.id(R.id.llPhotos).visible();
                RecyclerViewAdapter recyclerViewAdapter = new RecyclerViewAdapter(AdolVisitView.this, allImages, databaseHelper);
                recyclerViewImage.setAdapter(recyclerViewAdapter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}