//20May2021 Arpitha - list has been changed

package com.sc.stmansi.adolescent;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.ListofAdols.ClickListener;
import com.sc.stmansi.ListofAdols.RecyclerViewTouchListener;
import com.sc.stmansi.R;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.configuration.SyncState;
import com.sc.stmansi.database.DBMethods;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.recyclerview.RecyclerViewUpdateListener;
import com.sc.stmansi.repositories.AdolescentRepository;
import com.sc.stmansi.repositories.FacilityRepository;
import com.sc.stmansi.tables.tblAdolReg;
import com.sc.stmansi.womanlist.CareType;
import com.sc.stmansi.womanlist.QueryParams;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AdolDeactiveFragment extends Fragment implements RecyclerViewUpdateListener {
    private AppState appState;
    private View view;
    private DatabaseHelper databaseHelper;
    private AQuery aq;
    private static SearchView searchView;
    private RecyclerView recyclerview;
    private Button btToolbarHome;
    private RadioButton rb_All;
    private RadioGroup rg_filter;
    private SyncState syncState;
    private List<tblAdolReg> listofDeactiveAdols;
    private ListofDeactiveAdolRecyclerAdapter listofDeactivateAdolsAdapter;
    //    19May2021 Arpitha
    private LinearLayoutManager linearLayoutManager;
    private List<tblAdolReg> filteredRowItems;
    private boolean isUpdating;
    private int nextItemPosition;
    private long womenCount;
    private String womenCondition;
    AdolescentRepository adolescentRepository;
    TextView allCountView;
    private Map<String, Integer> villageCodeForName;
    TextView noDataLabelView;
    private boolean spinnerSelectionChanged = false;
    private String wFilterStr = "";


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_adol_deactivatelist, container, false);
       /* Bundle bundle = this.getArguments();
        if (bundle != null) {
            appState = bundle.getParcelable("appState");
        } else {
            appState = new AppState();
        } 19May2021 Arpitha*/
        //    19May2021 Arpitha
        Bundle bundle = getActivity().getIntent().getParcelableExtra("globalState");
        if (bundle != null) {
            appState = bundle.getParcelable("appState");
            syncState = bundle.getParcelable("syncState");
        } else {
            appState = new AppState();
        }
        allCountView = view.findViewById(R.id.txtAdolDeactivateCount);//19May2021 Arpitha
        noDataLabelView = view.findViewById(R.id.txt_listofDeactiveAdolsNoRecords);//19May2021 Arpitha
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
            Initialize();

            intialView();//19May2021 Arpitha
//            getData();
//            setData();
            btToolbarHome.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    displayDialogExit();
                }
            });

            rb_All.setChecked(true);
            rg_filter.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (checkedId == R.id.rb_list_all) {
                        if (listofDeactivateAdolsAdapter != null) {
                            wFilterStr = "All";
                            updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr, false);
                        }
                    } else if (checkedId == R.id.rb_list_male) {
                        wFilterStr = "Male";
                        updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr, false);

                    } else if (checkedId == R.id.rb_list_female) {
                        wFilterStr = "Female";
                        updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr, false);
                    }
                }
            });

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String s) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    if (newText.length() != 0) {
                        aq.id(R.id.imgresetfilter).getImageView().setImageResource(R.drawable.ic_reset);
                    } else {
                        aq.id(R.id.imgresetfilter).getImageView().setImageResource(R.drawable.ic_reset_gray);
                    }
                    updateList(appState.selectedVillageTitle, villageCodeForName, "", false);
                    return false;
                }
            });

            aq.id(R.id.imgresetfilter).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
//                        19May2021 Arpitha
                        aq.id(R.id.imgresetfilter).getImageView().setBackgroundResource(R.drawable.ic_reset_gray);
                        searchView.setQuery("", false);
                        CareType careType = CareType.GENERAL;
                        updateList(appState.selectedVillageTitle, villageCodeForName, "", false);
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                        
                    }
                }
            });

            aq.id(R.id.toolbarSpnFacilities).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        appState.selectedVillageTitle = (String) parent.getItemAtPosition(position);
                        if (spinnerSelectionChanged) {
                            updateList(appState.selectedVillageTitle, villageCodeForName, "", false);
                        }
                        spinnerSelectionChanged = true;
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                        
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    try {
                        
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                        
                    }
                }
            });

            recyclerview.addOnItemTouchListener(new RecyclerViewTouchListener(view.getContext(), recyclerview, new ClickListener() {
                @Override
                public void onLongClick(View child, int childAdapterPosition) {

                }

                @Override
                public void onClick(View child, int position) {
                    Intent intent;
                    intent = new Intent(view.getContext(), AdolDeactivation.class);
                    intent.putExtra("globalState", prepareBundle());
                    intent.putExtra("isView", true);
                    intent.putExtra("adolID", filteredRowItems.get(position).getAdolID());
                    startActivity(intent);
                }

                @Override
                public void onClick(View v) {
                }

                @Override
                public boolean onLongClick(View v) {
                    return false;
                }
            }));


        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            
        }
    }

    private void displayDialogExit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
        builder.setMessage(getResources().getString(R.string.exit));
        builder.setCancelable(false);
        builder.setNegativeButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(view.getContext(), MainMenuActivity.class);
                intent.putExtra("globalState", prepareBundle());
                startActivity(intent);
            }
        }).setPositiveButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    private void Initialize() {
        try {
            databaseHelper = getHelper();
            DBMethods dbMethods = new DBMethods(databaseHelper);
            aq = new AQuery(view);

            TextView textViewTitle = view.findViewById(R.id.toolbartxtTitle);
            textViewTitle.setText(getResources().getString(R.string.list_of_adolescent));
            searchView = view.findViewById(R.id.svListofDeactiveAdols);
            recyclerview = view.findViewById(R.id.rvListofDeactiveAdol);

            btToolbarHome = view.findViewById(R.id.toolbarBtnHome);
            btToolbarHome.setVisibility(View.VISIBLE);


            rb_All = view.findViewById(R.id.rb_list_all);

            rg_filter = view.findViewById(R.id.rg_filterList);

            aq.id(R.id.toolbartxtTitle).getTextView().setText(getResources().getString(R.string.deactivatelist));

            aq.id(R.id.lltoolbarSpnFacilities).getView().setVisibility(View.VISIBLE);
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            
        }
    }

    @SuppressLint("SetTextI18n")
    private void setData() {
        try {
            if (listofDeactiveAdols.size() != 0) {
                aq.id(R.id.txt_listofDeactiveAdolsNoRecords).getTextView().setVisibility(View.GONE);
                recyclerview.setVisibility(View.VISIBLE);
                recyclerview.setHasFixedSize(true);
                //19May2021 Arpitha
                listofDeactivateAdolsAdapter = new ListofDeactiveAdolRecyclerAdapter(listofDeactiveAdols, view.getContext(), view);
                recyclerview.setAdapter(listofDeactivateAdolsAdapter);
            } else {
                aq.id(R.id.txtAdolDeactivateCount).getTextView().setText(getResources().getString(R.string.reccount) + 0);
                aq.id(R.id.txt_listofDeactiveAdolsNoRecords).getTextView().setVisibility(View.VISIBLE);
                recyclerview.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            
        }
    }

    private void getData() {
        try {
            AdolescentRepository adolescentRepo = new AdolescentRepository(databaseHelper);
            listofDeactiveAdols = adolescentRepo.getallDeactivaedAdols();

            FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
            Map<String, Integer> facilities = facilityRepository.getPlaceMap();
            ArrayList<String> villageList = new ArrayList<String>();
            villageList.add(getResources().getString(R.string.all));
            for (Map.Entry<String, Integer> village : facilities.entrySet())
                villageList.add(village.getKey());
            ArrayAdapter<String> VillageAdapter = new ArrayAdapter<>(view.getContext(),
                    R.layout.simple_spinner_dropdown_item_white, villageList);
            VillageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            aq.id(R.id.toolbarSpnFacilities).adapter(VillageAdapter);
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            
        }
    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(view.getContext(), DatabaseHelper.class);
        }
        return databaseHelper;
    }

    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        b.putParcelable("syncState", syncState);
        return b;
    }


    //    19May2021 Arpitha
//    recycler view scroll
    public class OnRecyclerScrollListener extends RecyclerView.OnScrollListener {
        int ydy = 0;

        @Override
        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            // 30April2021 Guru
            if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                int lastCompletelyVisibleItemPosition = linearLayoutManager.findLastCompletelyVisibleItemPosition();
                boolean shouldPullUpRefresh = lastCompletelyVisibleItemPosition == filteredRowItems.size() - 1
                        && recyclerView.getScrollState() == RecyclerView.SCROLL_STATE_DRAGGING;
                if (shouldPullUpRefresh) {
                    QueryParams queryParams = new QueryParams();
                    queryParams.nextItemPosition = nextItemPosition;
                    queryParams.selectedVillageCode = appState.selectedVillageCode;
//                queryParams.textViewFilter = wFilterStr;
//                queryParams.isLMPNotConfirmed = islmpconfirm; //02MAy2021 Bindu set isLMPconfirmation
                    if (!isUpdating) {
                        isUpdating = true;
                        new LoadPregnantList(AdolDeactiveFragment.this, queryParams, false).execute();
                    }
                }
            } // 30April2021 Guru
        }

        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            ydy = dy;

            int lastCompletelyVisibleItemPosition = linearLayoutManager.findLastCompletelyVisibleItemPosition();
            boolean shouldPullUpRefresh = lastCompletelyVisibleItemPosition == filteredRowItems.size() - 1
                    && recyclerView.getScrollState() == RecyclerView.SCROLL_STATE_DRAGGING;
            if (shouldPullUpRefresh) {
                QueryParams queryParams = new QueryParams();
                queryParams.nextItemPosition = nextItemPosition;
                queryParams.selectedVillageCode = appState.selectedVillageCode;
//            queryParams.textViewFilter = wFilterStr;
//            queryParams.isLMPNotConfirmed = islmpconfirm; //02MAy2021 Bindu - set isLMPconfirmation
                if (!isUpdating) {
                    isUpdating = true;
                    new LoadPregnantList(AdolDeactiveFragment.this, queryParams, false).execute();
                }
            }
        }
    }

    //    load data in a list - 19May2021 Arpitha
    static class LoadPregnantList extends AsyncTask<Void, Void, Void> {

        private AdolDeactiveFragment viewsUpdateCallback;
        private QueryParams queryParams;
        private boolean firstLoad;
        private Map<String, Integer> villageCodeForName;

        LoadPregnantList(AdolDeactiveFragment viewsUpdateCallback, QueryParams queryParams, boolean firstLoad, Map<String, Integer> villageCodeForName) {
            this.viewsUpdateCallback = viewsUpdateCallback;
            this.queryParams = queryParams;
            this.firstLoad = firstLoad;
            this.villageCodeForName = villageCodeForName;
        }

        LoadPregnantList(AdolDeactiveFragment viewsUpdateCallback, QueryParams queryParams, boolean firstLoad) {
            this.viewsUpdateCallback = viewsUpdateCallback;
            this.queryParams = queryParams;
            this.firstLoad = firstLoad;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            int selectedVillageCode;
            // This means second constructor was used (called by scrollListener)
            if (villageCodeForName == null) selectedVillageCode = queryParams.selectedVillageCode;
            else {
                String selectedVillageTitle = queryParams.selectedVillageTitle;
                if (selectedVillageTitle == null)
                    selectedVillageCode = 0;
                else {
                    Integer code = villageCodeForName.get(selectedVillageTitle);
                    selectedVillageCode = (code == null) ? 0 : code;
                }
            }
//            viewsUpdateCallback.appState.selectedVillageCode = selectedVillageCode; //10Dec2019 - Bindu
            viewsUpdateCallback.appState.selectedVillageCode = queryParams.selectedVillageCode;

            try {
                viewsUpdateCallback.womenCount = viewsUpdateCallback.getRecordCount(viewsUpdateCallback.appState.selectedVillageCode, false, new AdolescentRepository(viewsUpdateCallback.databaseHelper), viewsUpdateCallback.wFilterStr, searchView.getQuery().toString()); // 30April2021 Guru

                List<tblAdolReg> rows = viewsUpdateCallback.prepareRowItemsForDisplay
                        (viewsUpdateCallback.appState.selectedVillageCode, queryParams.nextItemPosition,
                                viewsUpdateCallback.wFilterStr);
                if (firstLoad) {
                    int prevSize = viewsUpdateCallback.filteredRowItems.size();
                    viewsUpdateCallback.filteredRowItems.clear();
                    viewsUpdateCallback.listofDeactivateAdolsAdapter.notifyItemRangeRemoved(0, prevSize);
                    firstLoad = false;
                }
                if (rows.size() > 0) {
                    viewsUpdateCallback.filteredRowItems.addAll(rows);

                    viewsUpdateCallback.nextItemPosition = viewsUpdateCallback.filteredRowItems.size();
                    viewsUpdateCallback.listofDeactivateAdolsAdapter.notifyItemRangeInserted(firstLoad ? 0 : viewsUpdateCallback.nextItemPosition,
                            viewsUpdateCallback.filteredRowItems.size());
                }
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            try {
                viewsUpdateCallback.updateLabels();
                // 30April2021 Guru
                if (viewsUpdateCallback.filteredRowItems.size() > 0) {
                    viewsUpdateCallback.recyclerview.setVisibility(View.VISIBLE);
                    if (!firstLoad) {
                        viewsUpdateCallback.linearLayoutManager.scrollToPosition(viewsUpdateCallback.
                                listofDeactivateAdolsAdapter.getItemCount());
                    }
                    viewsUpdateCallback.isUpdating = false;
                } // 30April2021 Guru
                viewsUpdateCallback.allCountView.setText(viewsUpdateCallback.getResources().
                        getString(R.string.recordcount,
                                viewsUpdateCallback.womenCount)); // 30April2021 Guru
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
        }
    }


    //    19May2021 Arpitha
    @Override
    public void updateList(String villageTitle, Map<String, Integer> villageNameForCode, String textViewFilter, boolean checked) {

        QueryParams params = new QueryParams();
        params.textViewFilter = textViewFilter;
        params.selectedVillageCode = 0;
        params.nextItemPosition = 0;
        if (villageTitle != null && villageTitle.trim().length() > 0 &&
                !villageTitle.equalsIgnoreCase(getResources().getString(R.string.all)))
            params.selectedVillageCode = villageNameForCode.get(villageTitle);
        new LoadPregnantList(this, params, true).execute();
        isUpdating = true;

        recyclerview.addOnScrollListener(new OnRecyclerScrollListener());
    }

    // 19May2021 Arpitha
    private List<tblAdolReg> prepareRowItemsForDisplay(int villageCode,
                                                       int limitStart, String wFilterStr) throws Exception {

        AdolescentRepository adolescentRepository = new AdolescentRepository(databaseHelper);
        return adolescentRepository.getRegisteredAdols(villageCode,
                limitStart, false, wFilterStr, searchView.getQuery().toString());
    }


    //    update labels based on data -19May2021 Arpitha
    private void updateLabels() {
        if (filteredRowItems.size() == 0) {
            recyclerview.setVisibility(View.GONE);

           /* noDataLabelView.setText(getResources().getString(R.string.no_data));
            noDataLabelView.setVisibility(View.VISIBLE);*/
            aq.id(R.id.txt_listofDeactiveAdolsNoRecords).visible();
            return;
        } else {
//            noDataLabelView.setVisibility(View.GONE);
            aq.id(R.id.txt_listofDeactiveAdolsNoRecords).gone();
            recyclerview.setVisibility(View.VISIBLE);
        }
    }

    //    19May2021 Arpitha
    void intialView() throws Exception {
        populateSpinnerVillage();


        // 30April2021 Guru
        womenCount = getRecordCount(0, false, new AdolescentRepository(databaseHelper), "", searchView.getQuery().toString());
        allCountView.setText(getResources().getString(R.string.recordcount, womenCount));

        recyclerview.setVisibility(View.VISIBLE);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerview.setHasFixedSize(true);

        // use a linear layout manager
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerview.setLayoutManager(linearLayoutManager);
        linearLayoutManager = (LinearLayoutManager) linearLayoutManager;

        filteredRowItems = new ArrayList<>();

        QueryParams params = new QueryParams();
        params.textViewFilter = "";
        params.selectedVillageCode = 0;
        params.nextItemPosition = 0;
        new LoadPregnantList(this, params, true).execute();
        isUpdating = true;

        listofDeactivateAdolsAdapter = new ListofDeactiveAdolRecyclerAdapter(filteredRowItems, getActivity(), view);
        recyclerview.setAdapter(listofDeactivateAdolsAdapter);

        recyclerview.addOnScrollListener(new OnRecyclerScrollListener());

    }

    /**
     * populate Village to Spinner
     */
    private void populateSpinnerVillage() throws Exception {
        FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
        villageCodeForName = facilityRepository.getPlaceMap();

        ArrayAdapter<CharSequence> villageSpinnerAdapter = new ArrayAdapter<CharSequence>(getActivity(), R.layout.actionbar_spinner_list_item);
//        villageSpinnerAdapter.add("All Women");06May2021 Arpitha
        villageSpinnerAdapter.add(getResources().getString(R.string.all));//06May2021 Arpitha
        for (Map.Entry<String, Integer> village : villageCodeForName.entrySet()) {
            villageSpinnerAdapter.add(village.getKey());
        }
        villageSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        aq.id(R.id.toolbarSpnFacilities).adapter(villageSpinnerAdapter);
    }
/*

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        appState.selectedVillageTitle = (String) parent.getItemAtPosition(position);
        if (spinnerSelectionChanged) {
            updateList(appState.selectedVillageTitle, villageCodeForName, "",false);
        }
        spinnerSelectionChanged = true;
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
*/

}
