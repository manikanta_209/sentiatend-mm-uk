package com.sc.stmansi.adolescent.training;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sc.stmansi.R;


public class ListofCampViewHolder extends RecyclerView.ViewHolder {
    TextView CampName,CampCreatedDate,CampCreatedBy,CampSchemaType,isActive;

    public ListofCampViewHolder(@NonNull View itemView) {
        super(itemView);
        CampName = itemView.findViewById(R.id.txt_listofCampCampName);
        CampCreatedDate = itemView.findViewById(R.id.txt_listofCampCreatedDate);
        CampCreatedBy = itemView.findViewById(R.id.txt_listofCampCreatedBy);
        CampSchemaType = itemView.findViewById(R.id.txt_listofCampSchemaType);
        isActive = itemView.findViewById(R.id.txt_listofCampisActive);
    }
}
