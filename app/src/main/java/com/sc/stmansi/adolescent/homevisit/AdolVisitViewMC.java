package com.sc.stmansi.adolescent.homevisit;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.R;
import com.sc.stmansi.adolescent.AdolDeactivation;
import com.sc.stmansi.adolescent.AdolRegistration;
import com.sc.stmansi.adolescent.AdolVisitHistory;
import com.sc.stmansi.adolescent.AdolVisit_New;
import com.sc.stmansi.common.AlertDialogUtil;
import com.sc.stmansi.common.CommonDatePickerFragment;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.common.NavDrawerItem;
import com.sc.stmansi.common.NavDrawerListAdapter;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.configuration.SyncState;
import com.sc.stmansi.database.DBMethods;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.repositories.AdolescentRepository;
import com.sc.stmansi.repositories.CaseMgmtRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblAdolReg;
import com.sc.stmansi.tables.tblCaseMgmt;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class AdolVisitViewMC extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    private AppState appState;
    private String AdolId;
    private String VisitCount;
    private SyncState syncState;
    private DatabaseHelper databaseHelper;
    private AQuery aq;
    private DrawerLayout drawer;
    private ListView drawerList;
    private tblAdolReg currentAdolDetails;
    private tblCaseMgmt currentCaseDetails;
    private TblInstusers user;

    Boolean wantToCloseDialog = false;
    RadioButton rdbtnsms , rdbtndirect;
    LinearLayout llsmscontent; EditText etvisitno, etvisitdate;
    AQuery aqhvdialog;
    private String userTypeHV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adol_visitview_mc);
        try {
            Bundle bundle = getIntent().getBundleExtra("globalState");
            appState = bundle.getParcelable("appState");
            syncState = bundle.getParcelable("syncState");
            AdolId = getIntent().getStringExtra("adolID");
            VisitCount = getIntent().getStringExtra("VisitCount");
            userTypeHV = getIntent().getStringExtra("userTypeHV");
            Initalize();
            InitializeDrawer();
            getSupportActionBar().hide();

            getData(VisitCount);

            UpdateUI();

            aq.id(R.id.toolbarBtnHome).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        AlertDialog.Builder builder = new AlertDialog.Builder(com.sc.stmansi.adolescent.homevisit.AdolVisitViewMC.this);
                        builder.setMessage(getResources().getString(R.string.exit));
                        builder.setCancelable(false);
                        builder.setPositiveButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent(com.sc.stmansi.adolescent.homevisit.AdolVisitViewMC.this, AdolVisitHistory.class);
                                intent.putExtra("tabItem", 0);
                                intent.putExtra("adolID", AdolId);
                                intent.putExtra("globalState", prepareBundle());
                                startActivity(intent);
                            }
                        }).setNegativeButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).create().show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            aq.id(R.id.btnAVVNext).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        CaseMgmtRepository caseMgmtRepository = new CaseMgmtRepository(databaseHelper);
                        int caseCount = 0;
                            caseCount = caseMgmtRepository.getLastVisitCountbyUserType(AdolId, "MC").getVisitNum();

                        int vCount = Integer.parseInt(VisitCount);
                        if (vCount < caseCount) {
                            //next page
                            getData(String.valueOf(vCount + 1));
                            UpdateUI();
//                            Intent intent = new Intent(AdolVisitViewCC.this, AdolVisitViewCC.class);
//                            intent.putExtra("globalState", prepareBundle());
//                            intent.putExtra("VisitCount", String.valueOf(vCount + 1));
//                            intent.putExtra("adolID", AdolId);
//                            intent.putExtra("userTypeHV",userTypeHV);
//                            startActivity(intent);
                        } else {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.norecords), Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            aq.id(R.id.btnAVVPrev).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        int vCount = Integer.parseInt(VisitCount);
                        if ((vCount - 1) != 0) {
                            getData(String.valueOf(vCount - 1));
                            UpdateUI();
//                            Intent intent = new Intent(AdolVisitViewCC.this, AdolVisitViewCC.class);
//                            intent.putExtra("globalState", prepareBundle());
//                            intent.putExtra("VisitCount", String.valueOf(vCount - 1));
//                            intent.putExtra("adolID", AdolId);
//                            intent.putExtra("userTypeHV",userTypeHV);
//                            startActivity(intent);
                        } else {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.norecords), Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (Exception e) {
            exception(e);
        }
    }

    @SuppressLint("SetTextI18n")
    private void UpdateUI() {
        try {
            resetUI();
            String dateTime = currentAdolDetails.getRecordCreatedDate();
            int index = dateTime.indexOf(" ");
            String date = dateTime.substring(0, index);
            aq.id(R.id.toolbartxtTitle).getTextView().setText(currentAdolDetails.getRegAdolName() + "\n" + date);
            aq.id(R.id.txt_AVV_Number).getTextView().setText(currentCaseDetails.getVisitNum() + "");
            aq.id(R.id.txt_AVV_VisitDate).getTextView().setText(getResources().getString(R.string.visitdatecolon) + currentCaseDetails.getVisitDate());
            aq.id(R.id.txt_AVV_Age).getTextView().setText(currentAdolDetails.getRegAdolAge()+getResources().getString(R.string.yearstxt));
            /*if (currentCaseDetails.getHcmCreatedByUserType().equals(getResources().getString(R.string.userlevel_mc)))
                aq.id(R.id.txt_AVV_VisitUserLevel).getTextView().setText(getResources().getString(R.string.visitbymc));
            else if (currentCaseDetails.getHcmCreatedByUserType().equals(getResources().getString(R.string.userlevel_cc)))
                aq.id(R.id.txt_AVV_VisitUserLevel).getTextView().setText(getResources().getString(R.string.visitbycc));*/ //Bindu

            String complications = currentCaseDetails.getVisitBeneficiaryDangerSigns();
            if (complications.length() != 0) {
                String[] listofcomplications = complications.split("@");
                for (String s : listofcomplications) {
                    if (s.contains("Health")) {
                        aq.id(R.id.tr_AVVCC_HealthIssue).getView().setVisibility(View.VISIBLE);
                        String hit = getResources().getString(R.string.healthissuetrim);
                        String hi = s.replace(hit, "");
                        String[] hidata = hi.split("  ");
                        StringBuilder hiBuilder = new StringBuilder();
                        for (String tempword : hidata) {
                            String word = tempword.trim();
                            word = word.replace("Anemia",getResources().getString(R.string.anemia));
                            hiBuilder.append(word).append(", ");
                        }
                        if (hiBuilder.toString().contains(","))
                            aq.id(R.id.txt_AVVCC_HealthIssue).getTextView().setText(hiBuilder.deleteCharAt(hiBuilder.lastIndexOf(",")));
                        else
                            aq.id(R.id.txt_AVVCC_HealthIssue).getTextView().setText(hiBuilder.toString());
                    }
                    if (complications.contains("Anemia") && s.contains("hb")) {
                        aq.id(R.id.tr_AVVCC_HB).getView().setVisibility(View.VISIBLE);
                        String hbtemp = s.replace("hb is ", "");
                        aq.id(R.id.txt_AVVCC_HB).getTextView().setText(s.replace("hb is ", "")+" "+getResources().getString(R.string.gramsperliter)+ "( "+getHGCategory(Float.parseFloat(hbtemp))+")");
                    }
                    if (s.contains("Menstrual")) {
                        aq.id(R.id.tr_AVVCC_Menstural).getView().setVisibility(View.VISIBLE);
                        String hi = s.trim().replace("Menstrualpbm ", "");
                        String[] hidata = hi.split("  ");
                        StringBuilder hiBuilder = new StringBuilder();
                        String[] mensuproblems = getResources().getStringArray(R.array.menustralproblems);
                        for (String word : hidata) {
                            try{
                                int intWord = Integer.parseInt(word.trim());
                                hiBuilder.append(mensuproblems[intWord]).append(", ");
                            }catch (Exception e){
                                hiBuilder.append(word).append(",");
                            }
                        }
                        if (hiBuilder.toString().contains(",")){
                            aq.id(R.id.txt_AVVCC_Menstural).getTextView().setText(hiBuilder.deleteCharAt(hiBuilder.lastIndexOf(",")));
                        }else {
                            aq.id(R.id.txt_AVVCC_Menstural).getTextView().setText(hiBuilder.toString());
                        }
                    }
                }
            } else {
                aq.id(R.id.ll_AVVCC_Complication).getView().setVisibility(View.GONE);
            }
            if (currentCaseDetails.getVisitReferredtoFacType().length() != 0) {
                aq.id(R.id.txt_AVVCC_IsReferred).getTextView().setText(getResources().getString(R.string.m121));
                aq.id(R.id.ll_AVVCC_Referred).getView().setVisibility(View.VISIBLE);
                aq.id(R.id.txt_AVVCC_ReferredSlipNum).getTextView().setText(currentCaseDetails.getVisitReferralSlipNumber());
                aq.id(R.id.txt_AVVCC_ReferredFacType).getTextView().setText(currentCaseDetails.getVisitReferredtoFacType());
                aq.id(R.id.txt_AVVCC_ReferredFacName).getTextView().setText(currentCaseDetails.getVisitReferredtoFacName());

            } else {
                aq.id(R.id.ll_AVVCC_Referred).getView().setVisibility(View.GONE);
                aq.id(R.id.txt_AVVCC_IsReferred).getTextView().setText(getResources().getString(R.string.m122));
                aq.id(R.id.txt_AVVCC_ReferredSlipNum).getTextView().setText("");
                aq.id(R.id.txt_AVVCC_ReferredFacType).getTextView().setText("");
                aq.id(R.id.txt_AVVCC_ReferredFacName).getTextView().setText("");
            }

            aq.id(R.id.txt_AVVCC_ReferredComments).getTextView().setText(currentCaseDetails.getHcmActTakComments());
            aq.id(R.id.txt_AVVCC_IsAdviseGiven).getTextView().setText(currentCaseDetails.getHcmAdviseGiven());


            aq.id(R.id.txt_AVVCC_FeedbacktoMM).getTextView().setText(currentCaseDetails.getHcmActionToBeTakenClient());
            aq.id(R.id.txt_AVVCC_InputtoMC).getTextView().setText(currentCaseDetails.getHcmInputToNextLevel());

        } catch (Exception e) {
            exception(e);
        }
    }

    private void resetUI() {
        try{
            aq.id(R.id.tr_AVVCC_Menstural).getView().setVisibility(View.GONE);
            aq.id(R.id.tr_AVVCC_HealthIssue).getView().setVisibility(View.GONE);
            aq.id(R.id.tr_AVVCC_HB).getView().setVisibility(View.GONE);
            aq.id(R.id.txt_AVVCC_HealthIssue).getTextView().setText("");
            aq.id(R.id.txt_AVVCC_HB).getTextView().setText("");
            aq.id(R.id.txt_AVVCC_Menstural).getTextView().setText("");
            aq.id(R.id.txt_AVVCC_ReferredSlipNum).getTextView().setText("");
            aq.id(R.id.txt_AVVCC_ReferredFacType).getTextView().setText("");
            aq.id(R.id.txt_AVVCC_ReferredFacName).getTextView().setText("");
            aq.id(R.id.txt_AVVCC_ReferredComments).getTextView().setText("");
            aq.id(R.id.txt_AVVCC_IsAdviseGiven).getTextView().setText("");
            aq.id(R.id.txt_AVVCC_FeedbacktoMM).getTextView().setText("");
            aq.id(R.id.txt_AVVCC_InputtoMC).getTextView().setText("");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void getData(String vCount) {
        try {
            VisitCount = vCount;
            AdolescentRepository adolescentRepository = new AdolescentRepository(databaseHelper);
            currentAdolDetails = adolescentRepository.getAdolDatafromAdolID(AdolId);

            UserRepository userRepository = new UserRepository(databaseHelper);
            user = userRepository.getOneAuditedUser(appState.ashaId);

            CaseMgmtRepository caseMgmtRepository = new CaseMgmtRepository(databaseHelper);
            currentCaseDetails = caseMgmtRepository.getAdolCaseDetailsbyUserType(AdolId, vCount, userTypeHV);

            /*if (userTypeHV.equals("CC")) {
                currentCaseDetails = caseMgmtRepository.getCaseDetailsforCC(AdolId, vCount, user.getUserId());
            } else if (userTypeHV.equals("MC")) {
                currentCaseDetails = caseMgmtRepository.getCaseDetails(AdolId, vCount, user.getUserId());
            }*/

        } catch (Exception e) {
            exception(e);
        }
    }


    private void Initalize() {
        try {
            databaseHelper = getHelper();
            DBMethods dbMethods = new DBMethods(databaseHelper);
            aq = new AQuery(this);

        } catch (Exception e) {
            exception(e);
        }
    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(getApplicationContext(), DatabaseHelper.class);
        }
        return databaseHelper;
    }

    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        b.putParcelable("syncState", syncState);
        return b;
    }

    private void exception(Exception e) {
        FirebaseCrashlytics firebaseCrashlytics = FirebaseCrashlytics.getInstance();
        firebaseCrashlytics.log(e.getMessage());
    }

    private void InitializeDrawer() {
        try {
            drawer = findViewById(R.id.drawer_adolVisitView);
            drawerList = findViewById(R.id.lvNav_adolVisitView);

            ArrayList<NavDrawerItem> navDrawerItems = new ArrayList<>();
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.view_profile), R.drawable.ic_view));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.homevisitadd), R.drawable.ic_homevisit));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.adolvisithistory), R.drawable.anm_pending_activities));
//            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.trainingparticipated), R.drawable.presentation));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deactivatenew), R.drawable.deactivate));
            drawer.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
            drawerList.setAdapter(new NavDrawerListAdapter(getApplicationContext(), navDrawerItems));
            drawerList.setOnItemClickListener(new DrawerItemClickListener());
            Toolbar toolbar = findViewById(R.id.toolbarCommon);
            ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close);
            drawer.setDrawerListener(drawerToggle);
            drawerToggle.syncState();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class DrawerItemClickListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            drawer.closeDrawer(drawerList);
            try {
                switch (i) {
                    case 0:
                        Intent intent = new Intent(com.sc.stmansi.adolescent.homevisit.AdolVisitViewMC.this, AdolRegistration.class);
                        intent.putExtra("globalState", prepareBundle());
                        intent.putExtra("isEdit", true);
                        intent.putExtra("adolID", AdolId);
                        startActivity(intent);
                        break;
                    case 1:
                        if (currentAdolDetails.getRegAdolDeactivateDate().length() != 0) {
                            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.adolescentdeactivated), com.sc.stmansi.adolescent.homevisit.AdolVisitViewMC.this);
                            drawer.closeDrawer(GravityCompat.END);
                        } else {
                            intent = new Intent(com.sc.stmansi.adolescent.homevisit.AdolVisitViewMC.this, AdolVisit_New.class);
                            intent.putExtra("globalState", prepareBundle());
                            intent.putExtra("adolID", AdolId);
                            displayHomevisitConfirmation(currentAdolDetails, intent);
//                            intent.putExtra("visitType", "Direct");
//                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            startActivity(intent);
                        }
                        break;
                    case 2:
                        intent = new Intent(com.sc.stmansi.adolescent.homevisit.AdolVisitViewMC.this, AdolVisitHistory.class);
                        intent.putExtra("globalState", prepareBundle());
                        intent.putExtra("adolID", AdolId);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        break;
//                    case 3:
//                        intent = new Intent(AdolVisitViewCC.this, AdolParticipatedTrainings.class);
//                        intent.putExtra("globalState", prepareBundle());
//                        intent.putExtra("adolID", AdolId);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        startActivity(intent);
//                        break;
                    case 3:
                        if (currentAdolDetails.getRegAdolDeactivateDate().length() != 0) {
                            intent = new Intent(com.sc.stmansi.adolescent.homevisit.AdolVisitViewMC.this, AdolDeactivation.class);
                            intent.putExtra("globalState", prepareBundle());
                            intent.putExtra("isView", true);
                            intent.putExtra("adolID", AdolId);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        } else {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.nodeactivated), Toast.LENGTH_LONG).show();
                        }
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(com.sc.stmansi.adolescent.homevisit.AdolVisitViewMC.this);
        builder.setMessage(getResources().getString(R.string.exit));
        builder.setCancelable(false);
        builder.setPositiveButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(com.sc.stmansi.adolescent.homevisit.AdolVisitViewMC.this, AdolVisitHistory.class);
                intent.putExtra("tabItem", 0);
                intent.putExtra("adolID", AdolId);
                intent.putExtra("globalState", prepareBundle());
                startActivity(intent);
            }
        }).setNegativeButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    public void displayHomevisitConfirmation(tblAdolReg tblAdolReg, Intent nextScreen) throws Exception {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        View convertView = LayoutInflater.from(this).inflate(R.layout.activity_homevisitdirect, null, false);

//        String alertmsg =  regwoman.getRegWomanName() + ", " + " EDD : " + regwoman.getRegEDD() ;
        String alertmsg = tblAdolReg.getRegAdolName();
        String msg = "<font color='#CC3333'> Home visit - </font>" + alertmsg;

        aqhvdialog = new AQuery(convertView);

        rdbtnsms = convertView.findViewById(R.id.rdbtnsms);
        rdbtndirect = convertView.findViewById(R.id.rdbtndirect);
        llsmscontent = convertView.findViewById(R.id.llsmscontent);
        etvisitno = convertView.findViewById(R.id.etmmvisitnum);
        etvisitdate = convertView.findViewById(R.id.etmmvisitdate);
        etvisitdate.setText(DateTimeUtil.getTodaysDate());
        llsmscontent.setVisibility(View.GONE);
        rdbtnsms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                llsmscontent.setVisibility(View.VISIBLE);
            }
        });

        rdbtndirect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                llsmscontent.setVisibility(View.GONE);

            }
        });

        aqhvdialog.id(R.id.etmmvisitdate).getEditText().setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                try {
                    if (MotionEvent.ACTION_UP == event.getAction()) {
                        assignEditTextAndCallDatePicker(aqhvdialog.id(R.id.etmmvisitdate).getEditText());
                    }
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                    crashlytics.log(e.getMessage());

                }
                return true;
            }
        });

        aqhvdialog.id(R.id.imgcleardate).getImageView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aqhvdialog.id(R.id.etmmvisitdate).getTextView().setText("");
            }
        });

        alertDialogBuilder.setView(convertView);

        alertDialogBuilder.setCancelable(true)
                .setTitle(Html.fromHtml(msg))
                .setPositiveButton(getResources().getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                            }
                        })
                .setNegativeButton(getResources().getString(R.string.cancel), //01Oct2019 - Bindu - Add cancel button
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.cancel();
                            }
                        });
        final AlertDialog dialog = alertDialogBuilder.create();
        if (dialog != null)
            dialog.show();


        // Overriding the handler immediately after show is probably a
        // better
        // approach than OnShowListener as described below
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (validateHomevisit()) {
                        wantToCloseDialog = true;
                        int vno = 0;
                        if (aqhvdialog.id(R.id.etmmvisitnum).getText().toString().trim().length() > 0) {
                            vno = Integer.parseInt(aqhvdialog.id(R.id.etmmvisitnum).getText().toString());
                        }
                        Bundle b = new Bundle();
                        b.putInt("complVisitNum", vno);
                        b.putString("complVisitDate", aqhvdialog.id(R.id.etmmvisitdate).getText().toString());
                        if (rdbtnsms.isChecked()) {
                            b.putString("complvisittype", "SMS");
                        } else
                            b.putString("complvisittype", "Direct");

                        nextScreen.putExtras(b); //01Jul2021 Bindu
                        startActivity(nextScreen);
                        dialog.cancel();
                    }
                    if (wantToCloseDialog)
                        dialog.dismiss();
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                    crashlytics.log(e.getMessage());
                }

            }
        });
    }
    private boolean validateHomevisit() {

        if (!(rdbtnsms.isChecked() || rdbtndirect.isChecked())) {
//            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectvisittype), this );
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.plsselectvisittype), Toast.LENGTH_SHORT).show();
            wantToCloseDialog = false;
            return false;
        }

        if (rdbtnsms.isChecked()) {
            if (aqhvdialog.id(R.id.etmmvisitnum).getText().toString().trim().length() <= 0) {
//            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsentervisitnum), this);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.plsentervisitnum), Toast.LENGTH_SHORT).show();
                aqhvdialog.id(R.id.etmmvisitnum).getEditText().requestFocus();
                wantToCloseDialog = false;
                return false;
            }
            if (aqhvdialog.id(R.id.etmmvisitdate).getText().toString().trim().length() <= 0) {
//            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsentervisitdatemm), this);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.plsentervisitdatemm), Toast.LENGTH_SHORT).show();
                aqhvdialog.id(R.id.etmmvisitdate).getEditText().requestFocus();
                return false;
            }
        }
        return true;
    }

    /**
     * This method opens the Date Dialog and sets Date to respective EditText
     */
    private void assignEditTextAndCallDatePicker(EditText editText) {
        String currDate = (editText.getText().length() > 0) ? editText.getText().toString() : "";
        CommonDatePickerFragment.getCommonDate(this, currDate);
    }
    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        try {
            aqhvdialog.id(R.id.imgcleardate).background(R.drawable.brush1);
            if (view.isShown()) {
                String str = String.format("%02d", day) + "-" + String.format("%02d", month + 1) + "-"
                        + year;
                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                Date seldate = format.parse(str);

                if (seldate.after(new Date())) {
                    AlertDialogUtil.displayAlertMessage(
                            getResources().getString(R.string.date_cannot_af_currentdate)
                            , this);
                    aqhvdialog.id(R.id.etmmvisitdate).text("");
                } else {
                    aqhvdialog.id(R.id.etmmvisitdate).text(str);
                    aqhvdialog.id(R.id.imgcleardate).visible();
                }
            }

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }
    private String getHGCategory(float hbCount) {
        try {
//            float hbCount = Float.parseFloat(aqAdolVisit.id(R.id.etAdolVisitHbCount).getEditText().getText().toString());
            if (hbCount < 8.0) {
                //severe
                return getResources().getString(R.string.severe);
            } else if (hbCount >= 8 && hbCount <= 9.9) {
                //moderate
                return getResources().getString(R.string.moderate);
            } else if (hbCount >= 10 && hbCount <= 11.9) {
                //mild
                return getResources().getString(R.string.mild);
            } else if (hbCount >= 11.9 && hbCount <= 24) {
                //normal
                return getResources().getString(R.string.normal);
            } else if (hbCount > 24) {
                return "NA";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}