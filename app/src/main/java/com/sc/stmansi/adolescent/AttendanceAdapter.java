package com.sc.stmansi.adolescent;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.sc.stmansi.R;

import java.util.List;

public class AttendanceAdapter extends BaseAdapter {

    private final List<AdolParticipatedTrainings.AttendancePojo> list;
    private final Context mcontext;

    public AttendanceAdapter(Context applicationContext, List<AdolParticipatedTrainings.AttendancePojo> attendanceList) {
        this.mcontext = applicationContext;
        this.list = attendanceList;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        try {
            String[] allTrainingNames = mcontext.getResources().getStringArray(R.array.adoltrainingname);
            AttendanceViewholder attendanceViewholder = new AttendanceViewholder();
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.listview_attendance_single, viewGroup, false);
                attendanceViewholder.trainingDate = view.findViewById(R.id.txtAdolTrainingDate);
                attendanceViewholder.trainingName = view.findViewById(R.id.txtAdolTrainingName);
                attendanceViewholder.trainingAttendance = view.findViewById(R.id.txtAdolTrainingAttendance);
            } else {
                attendanceViewholder = (AttendanceViewholder) view.getTag();
            }
            AdolParticipatedTrainings.AttendancePojo data = list.get(i);

            String transTrainingName = data.getTrainingName();
            for (String s: allTrainingNames){
                if (s.equals(data.getTrainingName())){
                    transTrainingName = s;
                }
            }
            attendanceViewholder.trainingDate.setText(data.getTrainingDate());

            attendanceViewholder.trainingName.setText(data.getTrainingName());
            switch (data.getAttendance()) {
                case "Present":
                    attendanceViewholder.trainingAttendance.setText(mcontext.getResources().getString(R.string.present));
                    break;
                case "Absent":
                    attendanceViewholder.trainingAttendance.setText(mcontext.getResources().getString(R.string.absent));
                    break;
                case "NA":
                    attendanceViewholder.trainingAttendance.setText("NA");
                    attendanceViewholder.trainingAttendance.setTextColor(Color.RED);
                    break;
            }


        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        return view;
    }

    private static class AttendanceViewholder {
        TextView trainingDate, trainingName, trainingAttendance;
    }
}
