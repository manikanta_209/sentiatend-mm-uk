package com.sc.stmansi.adolescent;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.sc.stmansi.R;
import com.sc.stmansi.adolescent.homevisit.AdolVisitHistory_CCFrag;
import com.sc.stmansi.adolescent.homevisit.AdolVisitHistory_MCFrag;
import com.sc.stmansi.adolescent.homevisit.AdolVisitHistory_MMFrag;

public class AdolVisitHistoryPagerAdapter extends FragmentPagerAdapter {


    private final Context context;
    private final Bundle bundle;
    private final String adolId;

    public AdolVisitHistoryPagerAdapter(Context context, @NonNull FragmentManager fm, Bundle bundle, String adolId) {
        super(fm);
        this.context = context;
        this.bundle = bundle;
        this.adolId = adolId;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        try{
            switch (position){
                case 0:
                    AdolVisitHistory_MMFrag frag1 = new AdolVisitHistory_MMFrag();
                    bundle.putString("adolID",adolId);
                    frag1.setArguments(bundle);
                    return frag1;
                case 1:
                    AdolVisitHistory_CCFrag frag2 = new AdolVisitHistory_CCFrag();
                    bundle.putString("adolID",adolId);
                    frag2.setArguments(bundle);
                    return frag2;
                case 2:
                    AdolVisitHistory_MCFrag frag3 = new AdolVisitHistory_MCFrag();
                    bundle.putString("adolID",adolId);
                    frag3.setArguments(bundle);
                    return frag3;
            }
        }catch (Exception e){
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position)
    {
        String title = null;
        if (position == 0)
            title = context.getResources().getString(R.string.userlevel_mm);
        else if (position == 1)
            title = context.getResources().getString(R.string.userlevel_cc);
        else if (position == 2)
            title = context.getResources().getString(R.string.userlevel_mc);
        return title;
    }
}
