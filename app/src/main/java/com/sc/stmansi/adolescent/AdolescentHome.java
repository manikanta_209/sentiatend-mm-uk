package com.sc.stmansi.adolescent;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.androidquery.AQuery;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.sc.stmansi.R;
import com.sc.stmansi.adolescent.training.ListofAdolsFragment;
import com.sc.stmansi.adolescent.training.ListofTrainingFragment;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.configuration.SyncState;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.google.firebase.crashlytics.FirebaseCrashlytics;

public class AdolescentHome extends AppCompatActivity {

    private AppState appState;
    private BottomNavigationView bottomNavBar;
    private AQuery aq;
    private Bundle bundle;
    private SyncState syncState;
    private int tabItem;

    @Override
    public void onBackPressed() {
        try{
            Intent intent = new Intent(AdolescentHome.this, MainMenuActivity.class);
            intent.putExtra("globalState", prepareBundle());
            startActivity(intent);
        }catch (Exception e){
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adolescenthome);
        try {
            bundle = getIntent().getBundleExtra("globalState");
            if (bundle!=null){
                appState = bundle.getParcelable("appState");
                syncState = bundle.getParcelable("syncState");
            }else {
                appState = new AppState();
            }
            tabItem = getIntent().getIntExtra("tabItem",0);
            getSupportActionBar().hide();
            Initialize();

            selectTab();

            bottomNavBar.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
                @SuppressLint("NonConstantResourceId")
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    try {
                        switch (menuItem.getItemId()) {
                            case R.id.nav_btm_listofadols:
                                aq.id(R.id.toolbartxtTitle).getTextView().setText(R.string.list_of_adolescent);
                                ListofAdolsFragment listofAdolsFragment = new ListofAdolsFragment();
                                listofAdolsFragment.setArguments(bundle);
                                getSupportFragmentManager().beginTransaction().replace(R.id.framelayoutAdol, listofAdolsFragment).addToBackStack(null).commit();
                                return true;
                            case R.id.nav_btm_listofcamps:
                                aq.id(R.id.toolbartxtTitle).getTextView().setText(R.string.list_of_campaign);
                                ListofTrainingFragment listofTraining = new ListofTrainingFragment();
                                listofTraining.setArguments(bundle);
                                getSupportFragmentManager().beginTransaction().replace(R.id.framelayoutAdol, listofTraining).addToBackStack(null).commit();
                                return true;
                            case R.id.nav_btm_deactivatelist:
                                aq.id(R.id.toolbartxtTitle).getTextView().setText(R.string.deactivatelist);
                                AdolDeactiveFragment adolDeactive = new AdolDeactiveFragment();
                                adolDeactive.setArguments(bundle);
                                getSupportFragmentManager().beginTransaction().replace(R.id.framelayoutAdol, adolDeactive).addToBackStack(null).commit();
                                return true;
                            case R.id.nav_btm_adolReports:
                                aq.id(R.id.toolbartxtTitle).getTextView().setText(R.string.adolescentreports);
                                AdolescentReports adolescentReports = new AdolescentReports();
                                adolescentReports.setArguments(bundle);
                                getSupportFragmentManager().beginTransaction().replace(R.id.framelayoutAdol, adolescentReports).addToBackStack(null).commit();
                                return true;

                        }
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            
                    }
                    return false;
                }
            });

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            
        }
    }

    private void selectTab() {
        try{
            switch (tabItem){
                case 0:
                    bottomNavBar.setSelectedItemId(R.id.nav_btm_listofadols);
                    aq.id(R.id.toolbartxtTitle).getTextView().setText(R.string.list_of_adolescent);//for first tiem set up
                    ListofAdolsFragment listofAdolsFragment = new ListofAdolsFragment();
                    listofAdolsFragment.setArguments(bundle);
                    getSupportFragmentManager().beginTransaction().replace(R.id.framelayoutAdol, listofAdolsFragment).addToBackStack(null).commit();
                    break;
                case 1:
                    bottomNavBar.setSelectedItemId(R.id.nav_btm_listofcamps);
                    aq.id(R.id.toolbartxtTitle).getTextView().setText(R.string.list_of_campaign);
                    ListofTrainingFragment listofTraining = new ListofTrainingFragment();
                    listofTraining.setArguments(bundle);
                    getSupportFragmentManager().beginTransaction().replace(R.id.framelayoutAdol, listofTraining).addToBackStack(null).commit();
                    break;
                case 2:
                    bottomNavBar.setSelectedItemId(R.id.nav_btm_deactivatelist);
                    aq.id(R.id.toolbartxtTitle).getTextView().setText(R.string.deactivatelist);
                    AdolDeactiveFragment adolDeactive = new AdolDeactiveFragment();
                    adolDeactive.setArguments(bundle);
                    getSupportFragmentManager().beginTransaction().replace(R.id.framelayoutAdol, adolDeactive).addToBackStack(null).commit();
                    break;
                case 3:
                    bottomNavBar.setSelectedItemId(R.id.nav_btm_adolReports);
                    aq.id(R.id.toolbartxtTitle).getTextView().setText(R.string.adolescentreports);
                    AdolescentReports adolescentReports = new AdolescentReports();
                    adolescentReports.setArguments(bundle);
                    getSupportFragmentManager().beginTransaction().replace(R.id.framelayoutAdol, adolescentReports).addToBackStack(null).commit();
                    break;
                default:
                    bottomNavBar.setSelectedItemId(R.id.nav_btm_listofadols);
                    aq.id(R.id.toolbartxtTitle).getTextView().setText(R.string.list_of_adolescent);//for first tiem set up
                    listofAdolsFragment = new ListofAdolsFragment();
                    listofAdolsFragment.setArguments(bundle);
                    getSupportFragmentManager().beginTransaction().replace(R.id.framelayoutAdol, listofAdolsFragment).addToBackStack(null).commit();

            }
        }catch (Exception e){
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            
        }
    }

    private void Initialize() {
        try {
            aq = new AQuery(this);
            bottomNavBar = findViewById(R.id.bottomNav_Adol);
            bottomNavBar.setSelectedItemId(R.id.nav_btm_listofadols);

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            
        }
    }
    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        b.putParcelable("syncState", syncState);
        return b;
    }
}