package com.sc.stmansi.adolescent.training;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.R;
import com.sc.stmansi.adolescent.AdolescentHome;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.configuration.SyncState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.repositories.AdolAttendanceRepository;
import com.sc.stmansi.repositories.AdolVisitHeaderRepository;
import com.sc.stmansi.repositories.AdolescentRepository;
import com.sc.stmansi.repositories.AuditRepository;
import com.sc.stmansi.repositories.CampaignRepository;
import com.sc.stmansi.repositories.FacilityRepository;
import com.sc.stmansi.repositories.TransactionHeaderRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.tables.AuditPojo;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblAdolAttendanceSavings;
import com.sc.stmansi.tables.tblAdolReg;
import com.sc.stmansi.tables.tblAdolVisitHeader;
import com.sc.stmansi.tables.tblCampDetails;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class TrainingCreation extends AppCompatActivity {

    private AppState appState;
    private String campID;
    private DatabaseHelper databaseHelper;
    private SyncState syncState;
    private RecyclerView recyclerView;
    private AQuery aq;
    private CampaignAttendanceAdapter campaignAttendanceAdapter;
    private int transId;
    private com.sc.stmansi.tables.tblCampDetails tblCampDetails;
    private ImageButton imgbt_Attendance, imgbt_Summary;
    private boolean isReadytoEnd = false;
    Map<String, Integer> facilities;
    private TblInstusers user;

    @Override
    public void onBackPressed() {
        displayDialogExit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trainingcreation);
        try {
            Bundle bundle = getIntent().getBundleExtra("globalState");
            if (bundle != null) {
                appState = bundle.getParcelable("appState");
                syncState = bundle.getParcelable("syncState");
            } else {
                appState = new AppState();
            }
            campID = getIntent().getStringExtra("campID");
            getSupportActionBar().hide();
            Initialize();
            setData();

            aq.id(R.id.toolbarBtnHome).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    displayDialogExit();
                }
            });

            aq.id(R.id.btnCampAttEndCamp).getImageView().setOnClickListener(new View.OnClickListener() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onClick(View v) {
                    try {
                        if (isReadytoEnd) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(TrainingCreation.this);
                            builder.setCancelable(false).setMessage(getResources().getString(R.string.doyouwanttoend));
                            builder.setNegativeButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                                @SuppressLint("SetTextI18n")
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    try {
                                        saveData();
                                    } catch (Exception e) {

                                    }
                                }
                            }).setPositiveButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).create().show();
                        } else {
                            isReadytoEnd = true;
                            aq.id(R.id.btnCampAttEndCamp).getImageView().setImageResource(R.drawable.save);
                            LinearLayout llAttendance = findViewById(R.id.llCampCrtAttendance);
                            LinearLayout llSummary = findViewById(R.id.llCampCrtSummary);
                            llAttendance.setVisibility(View.GONE);
                            llSummary.setVisibility(View.VISIBLE);
                            imgbt_Summary.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                            imgbt_Attendance.setBackgroundColor(getResources().getColor(R.color.gray));
                            aq.id(R.id.toolbartxtTitle).getTextView().setText(tblCampDetails.getTblCampName() + getResources().getString(R.string.hifensummary));
                            setSummaryUIData();
                        }
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());

                    }
                }
            });

            imgbt_Attendance.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onClick(View v) {
                    isReadytoEnd = false;
                    aq.id(R.id.btnCampAttEndCamp).getImageView().setImageResource(R.drawable.ic_arrow_forward);
                    LinearLayout llAttendance = findViewById(R.id.llCampCrtAttendance);
                    LinearLayout llSummary = findViewById(R.id.llCampCrtSummary);
                    llAttendance.setVisibility(View.VISIBLE);
                    llSummary.setVisibility(View.GONE);
                    imgbt_Summary.setBackgroundColor(getResources().getColor(R.color.gray));
                    imgbt_Attendance.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                }
            });


            imgbt_Summary.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onClick(View v) {
                    isReadytoEnd = true;
                    aq.id(R.id.btnCampAttEndCamp).getImageView().setImageResource(R.drawable.save);
                    LinearLayout llAttendance = findViewById(R.id.llCampCrtAttendance);
                    LinearLayout llSummary = findViewById(R.id.llCampCrtSummary);
                    llAttendance.setVisibility(View.GONE);
                    llSummary.setVisibility(View.VISIBLE);
                    imgbt_Summary.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                    imgbt_Attendance.setBackgroundColor(getResources().getColor(R.color.gray));
                    setSummaryUIData();

                }
            });
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private void displayDialogExit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(TrainingCreation.this);
        builder.setMessage(getResources().getString(R.string.exit));
        builder.setCancelable(false);
        builder.setNegativeButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(TrainingCreation.this, AdolescentHome.class);
                intent.putExtra("tabItem", 1);
                intent.putExtra("globalState", prepareBundle());
                startActivity(intent);
            }
        }).setPositiveButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    private void saveData() {
        try {
            List<tblAdolAttendanceSavings> adolattendanceList = campaignAttendanceAdapter.getAttendanceList();

            AdolAttendanceRepository adolAttendanceRepository = new AdolAttendanceRepository(databaseHelper);
            CampaignRepository campaignRepository = new CampaignRepository(databaseHelper);
            transId = TransactionHeaderRepository.iCreateNewTransNew(user.getUserId(), databaseHelper); //23May2021 Bindu set userid
            int absent = 0, present = 0;
            for (tblAdolAttendanceSavings single : adolattendanceList) {
                if (single.getTblAttSavAttendance().equals("Absent")) {
                    absent = absent + 1;
                } else if (single.getTblAttSavAttendance().equals("Present")) {
                    present = present + 1;
                }
                single.setDateCreated(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
                single.setDateUpdated(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
                single.setTransId(transId);
                adolAttendanceRepository.insert(single);
                TransactionHeaderRepository.iNewRecordTransNew(appState.ashaId, transId, "tblAdolAttendanceSavings", databaseHelper);
            }

            tblCampDetails currentCampDetails = campaignRepository.getCampDetailsbyID(campID);
            currentCampDetails.setTblCampAbsent(String.valueOf(absent));
            currentCampDetails.setTblCampPresent(String.valueOf(present));
            currentCampDetails.setTblCampSummary(aq.id(R.id.etCampCrtSummary).getEditText().getText().toString());
            currentCampDetails.setTransId(transId);
            currentCampDetails.setTblCampOutcome(aq.id(R.id.etCampCrtOutcome).getEditText().getText().toString());
            currentCampDetails.setTblCampEndTime(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
            currentCampDetails.setTblCampisActive(0);

            String sql = insertAuditTrailsDetails(currentCampDetails);
            if (sql.length() != 0) {
                int result = campaignRepository.updaterawSql(sql);
                if (result > 0) {
                    TransactionHeaderRepository.iUpdateRecordTransNew(appState.ashaId, transId, "tblCampDetails", sql, null, null, databaseHelper);
                    Intent intent = new Intent(TrainingCreation.this, AdolescentHome.class);
                    intent.putExtra("tabItem", 1);
                    intent.putExtra("globalState", prepareBundle());
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.erroroccurs), Toast.LENGTH_SHORT).show();
                }
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }


    @SuppressLint("SetTextI18n")
    private void setSummaryUIData() {
        try {
            if (tblCampDetails != null) {

                FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
                facilities = facilityRepository.getPlaceMap();
                ArrayList<String> villageList = new ArrayList<String>();
                villageList.add(getResources().getString(R.string.selectType));
                for (Map.Entry<String, Integer> village : facilities.entrySet())
                    villageList.add(village.getKey());

                if (tblCampDetails.getTblCampFacilities().contains(",")) {
                    String[] facilitiesIndexs = tblCampDetails.getTblCampFacilities().split(",");
                    StringBuilder FacilityName = new StringBuilder();
                    for (String s : facilitiesIndexs) {
                        s = s.trim();
                        FacilityName.append(villageList.get(Integer.parseInt(s))).append(",");
                    }
                    int index = FacilityName.lastIndexOf(",");
                    FacilityName = FacilityName.deleteCharAt(index);
                    aq.id(R.id.txtCampCrtFacility).getTextView().setText(FacilityName);
                } else {
                    String facilitiesIndex = tblCampDetails.getTblCampFacilities();
                    int villagecode = getKey(facilities, facilitiesIndex);
                    String FacilityName = villageList.get(villagecode);
                    aq.id(R.id.txtCampCrtFacility).getTextView().setText(FacilityName);
                }
                int trainingIndex = 0;

                if (!(tblCampDetails.getTblCampName().length() > 2)) { //23MAy2021 Bindu change length
                    try {
                        trainingIndex = Integer.parseInt(tblCampDetails.getTblCampName());
                        String[] topics = getTrainingTopics(trainingIndex);
                        String topicIndex = tblCampDetails.getTblCampProObjective();
                        if (topicIndex.contains(",")) {
                            List<String> topicIndexbeforeInt = Arrays.asList(topicIndex.split(","));
                            ArrayList<Integer> intList = new ArrayList<>();
                            for (String s : topicIndexbeforeInt)
                                intList.add(Integer.valueOf(s.trim()));
                            StringBuilder topicNames = new StringBuilder();
                            for (int s : intList) {
                                topicNames.append(topics[s]).append(", ");
                            }
                            topicNames = topicNames.deleteCharAt(topicNames.lastIndexOf(","));
                            aq.id(R.id.txtCampCrtProgramObj).getTextView().setText(topicNames);
                        } else {
                            aq.id(R.id.txtCampCrtProgramObj).getTextView().setText(topics[Integer.parseInt(topicIndex.trim())]);
                        }
                    } catch (Exception e) {
                        Throwable cause = e;
                        while (cause.getCause() != null && cause.getCause() != cause) {
                            cause = cause.getCause();
                        }
                        String exception = cause.toString();
                        if (exception.toLowerCase().contains("numberformatexception")) {
                            aq.id(R.id.txtCampCrtProgramObj).getTextView().setText(tblCampDetails.getTblCampProObjOthers());
                        }
                    }
                } else {
                    aq.id(R.id.txtCampCrtProgramObj).getTextView().setText(tblCampDetails.getTblCampProObjOthers());
                }


                List<tblAdolAttendanceSavings> adolattendanceList = campaignAttendanceAdapter.getAttendanceList();
                int absent = 0, present = 0;
                for (tblAdolAttendanceSavings single : adolattendanceList) {
                    if (single.getTblAttSavAttendance().equals("Absent")) {
                        absent = absent + 1;
                    } else if (single.getTblAttSavAttendance().equals("Present")) {
                        present = present + 1;
                    }
                }

                aq.id(R.id.txtCampCrtPresent).getTextView().setText(present + "");
                aq.id(R.id.txtCampCrtAbsent).getTextView().setText(absent + "");

                aq.id(R.id.txtCampCrtEndTime).getTextView().setText(_12hoursformate(DateTimeUtil.getCurrentTime()));
                Handler hand = new Handler();
                hand.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            aq.id(R.id.txtCampCrtEndTime).getTextView().setText(_12hoursformate(DateTimeUtil.getCurrentTime()));
                        } catch (Exception e) {
                            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                            crashlytics.log(e.getMessage());

                        }
                        hand.postDelayed(this, 1000);
                    }
                });
            }

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private String[] getTrainingTopics(int i) {
        try {
            String[] topic = new String[0];
            if (i == 1) {
                topic = getResources().getStringArray(R.array.traininggrowthinadolescent);
            } else if (i == 2) {
                topic = getResources().getStringArray(R.array.trainingnutritionandanemia);
            } else if (i == 3) {
                topic = getResources().getStringArray(R.array.trainingmenstruationandreproductive);
            } else if (i == 4) {
                topic = getResources().getStringArray(R.array.trainingmalereproductivesystem);
            } else if (i == 5) {
                topic = getResources().getStringArray(R.array.trainingresponsiblerelatioinshipandbehavior);
            } else if (i == 6) {
                topic = getResources().getStringArray(R.array.trainingresponsiblerelatioinshipandbehavioryears);
            } else if (i == 7) {
                topic = getResources().getStringArray(R.array.trainingharassmentandabuse);
            } else if (i == 8) {
                topic = getResources().getStringArray(R.array.traininginfectionofreproductive);
            } else if (i == 9) {
                topic = getResources().getStringArray(R.array.trainingmarriageandfamily);
            } else if (i == 10) {
                topic = getResources().getStringArray(R.array.traininggivingthechildrenaheadstart);
            } else if (i == 11) {
                topic = getResources().getStringArray(R.array.trainingcontraceptions);
            }
            return topic;
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }

        return new String[0];
    }

    private String insertAuditTrailsDetails(tblCampDetails currentCampDetails) {
        String updateSQL = "";
        String addQuery = "";
        try {
            if (!tblCampDetails.getTblCampAbsent().equals(currentCampDetails.getTblCampAbsent())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " tblCampAbsent = " + (char) 34 + currentCampDetails.getTblCampAbsent() + (char) 34 + "";
                } else {
                    addQuery = addQuery + " ,tblCampAbsent = " + (char) 34 + currentCampDetails.getTblCampAbsent() + (char) 34 + "";
                }
                InserttblAuditTrail("tblCampAbsent", tblCampDetails.getTblCampAbsent(), currentCampDetails.getTblCampAbsent(), transId);
            }
            if (!tblCampDetails.getTblCampPresent().equals(currentCampDetails.getTblCampPresent())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " tblCampPresent = " + (char) 34 + currentCampDetails.getTblCampPresent() + (char) 34 + "";
                } else {
                    addQuery = addQuery + " ,tblCampPresent = " + (char) 34 + currentCampDetails.getTblCampPresent() + (char) 34 + "";
                }
                InserttblAuditTrail("tblCampPresent", tblCampDetails.getTblCampPresent(), currentCampDetails.getTblCampPresent(), transId);
            }
            if (!tblCampDetails.getTblCampSummary().equals(currentCampDetails.getTblCampSummary())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " tblCampSummary = " + (char) 34 + currentCampDetails.getTblCampSummary() + (char) 34 + "";
                } else {
                    addQuery = addQuery + " ,tblCampSummary = " + (char) 34 + currentCampDetails.getTblCampSummary() + (char) 34 + "";
                }
                InserttblAuditTrail("tblCampSummary", tblCampDetails.getTblCampSummary(), currentCampDetails.getTblCampSummary(), transId);
            }
            if (!(tblCampDetails.getTblCampisActive() == (currentCampDetails.getTblCampisActive()))) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " tblCampisActive = " + currentCampDetails.getTblCampisActive() + "";
                } else {
                    addQuery = addQuery + " ,tblCampisActive = " + currentCampDetails.getTblCampisActive() + "";
                }
                InserttblAuditTrail("tblCampisActive", tblCampDetails.getTblCampisActive() + "", currentCampDetails.getTblCampisActive() + "", transId);
            }
            if (!tblCampDetails.getTblCampEndTime().equals(currentCampDetails.getTblCampEndTime())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " tblCampEndTime = " + (char) 34 + currentCampDetails.getTblCampEndTime() + (char) 34 + "";
                } else {
                    addQuery = addQuery + " ,tblCampEndTime = " + (char) 34 + currentCampDetails.getTblCampEndTime() + (char) 34 + "";
                }
                InserttblAuditTrail("tblCampEndTime", tblCampDetails.getTblCampEndTime(), currentCampDetails.getTblCampEndTime(), transId);
            }
            if (!tblCampDetails.getTblCampOutcome().equals(currentCampDetails.getTblCampOutcome())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " tblCampOutcome = " + (char) 34 + currentCampDetails.getTblCampOutcome() + (char) 34 + "";
                } else {
                    addQuery = addQuery + " ,tblCampOutcome = " + (char) 34 + currentCampDetails.getTblCampOutcome() + (char) 34 + "";
                }
                InserttblAuditTrail("tblCampOutcome", tblCampDetails.getTblCampOutcome(), currentCampDetails.getTblCampOutcome(), transId);
            }

            if (addQuery.length() > 0) {
                updateSQL = "UPDATE tblCampDetails SET ";
                updateSQL = updateSQL + addQuery + " WHERE tblCampID = '" + campID + "' and userId ='"+user.getUserId()+"'";
            }
            return updateSQL;
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return "";
    }

    private void InserttblAuditTrail(String ColumnName, String Old_Value, String New_Value, int transId) {
        try {
            AuditPojo tblAudit = new AuditPojo();
            tblAudit.setUserId(user.getUserId());
            tblAudit.setWomanId(tblCampDetails.getTblCampID());
            tblAudit.setTblName("tblCampDetails");
            tblAudit.setColumnName(ColumnName);
            tblAudit.setOld_Value(Old_Value);
            tblAudit.setNew_Value(New_Value);
            tblAudit.setTransId(transId);
            tblAudit.setRecordcreateddate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
            AuditRepository auditRepository = new AuditRepository(databaseHelper);
            auditRepository.insertTotblAuditTrail(tblAudit);
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    @SuppressLint("SetTextI18n")
    private void setData() {
        try {
            CampaignRepository campaignRepository = new CampaignRepository(databaseHelper);
            AdolescentRepository adolescentRepository = new AdolescentRepository(databaseHelper);

            tblCampDetails = campaignRepository.getCampDetailsbyID(campID);
            aq.id(R.id.toolbartxtTitle).getTextView().setText(getResources().getString(R.string.trainingdetails)); //13May2021 Bindu change
            //15May2021 Bindu
            String[] schemaTypes = getResources().getStringArray(R.array.SchemaType);
            String sType = schemaTypes[tblCampDetails.getTblCampSchemaType()];
            aq.id(R.id.txtCampCrtSchemaType).getTextView().setText(sType);

//            15MAy2021 Bindu get training name
            String[] trainingnameengkan = getResources().getStringArray(R.array.adoltrainingname);

            if (!(tblCampDetails.getTblCampName().length() > 2)) {
                try {
                    int trainingIndex = Integer.parseInt(tblCampDetails.getTblCampName());
                    aq.id(R.id.txttrainingname).getTextView().setText(getResources().getString(R.string.trainingname) + " : " + trainingnameengkan[trainingIndex]);
                    aq.id(R.id.txtCampAttName).getTextView().setText(getResources().getString(R.string.trainingname) + " : " + trainingnameengkan[trainingIndex]);
                    aq.id(R.id.txtCampCrtName).getTextView().setText(trainingnameengkan[trainingIndex]);

                    String[] topics = getTrainingTopics(trainingIndex);
                    String topicIndex = tblCampDetails.getTblCampProObjective();
                    if (topicIndex.contains(",")) {
                        List<String> topicIndexbeforeInt = Arrays.asList(topicIndex.split(","));
                        ArrayList<Integer> intList = new ArrayList<>();
                        for (String s : topicIndexbeforeInt)
                            intList.add(Integer.valueOf(s.trim()));
                        StringBuilder topicNames = new StringBuilder();
                        for (int s : intList) {
                            topicNames.append(topics[s]).append(", ");
                        }
                        topicNames = topicNames.deleteCharAt(topicNames.lastIndexOf(","));
                        aq.id(R.id.txttrainingobjective).getTextView().setText(getResources().getString(R.string.programobjective) + " : " + topicNames);
                    } else {
                        aq.id(R.id.txttrainingobjective).getTextView().setText(getResources().getString(R.string.programobjective) + " : " + topics[Integer.parseInt(topicIndex.trim())]);
                    }
                } catch (Exception e) {
                    Throwable cause = e;
                    while (cause.getCause() != null && cause.getCause() != cause) {
                        cause = cause.getCause();
                    }
                    String exception = cause.toString();
                    if (exception.toLowerCase().contains("numberformatexception")) {
                        aq.id(R.id.txttrainingobjective).getTextView().setText(getResources().getString(R.string.programobjective) + " : " + tblCampDetails.getTblCampProObjOthers());
                        aq.id(R.id.txttrainingname).getTextView().setText(getResources().getString(R.string.trainingname) + " : " + tblCampDetails.getTblCampName());
                        aq.id(R.id.txtCampAttName).getTextView().setText(getResources().getString(R.string.trainingname) + " : " + tblCampDetails.getTblCampName());
                        aq.id(R.id.txtCampCrtName).getTextView().setText(tblCampDetails.getTblCampName());
                    }
                }
            } else {
                aq.id(R.id.txttrainingobjective).getTextView().setText(getResources().getString(R.string.programobjective) + " : " + tblCampDetails.getTblCampProObjOthers());
                aq.id(R.id.txttrainingname).getTextView().setText(getResources().getString(R.string.trainingname) + " : " + tblCampDetails.getTblCampName());
                aq.id(R.id.txtCampAttName).getTextView().setText(getResources().getString(R.string.trainingname) + " : " + tblCampDetails.getTblCampName());
                aq.id(R.id.txtCampCrtName).getTextView().setText(tblCampDetails.getTblCampName());
            }


            List<tblAdolAttendanceSavings> AdolsAttendanceList = new ArrayList<>();
            List<tblAdolReg> allAdols = adolescentRepository.getAdolsbyFacility(tblCampDetails.getTblCampFacilities());

            allAdols = addingLastVisitDetailstoAdols(allAdols);

            aq.id(R.id.txtCampCrtTotalAdols).getTextView().setText(String.valueOf(allAdols.size()));

            AdolsAttendanceList = setAttendanceData(AdolsAttendanceList, allAdols);


            campaignAttendanceAdapter = new CampaignAttendanceAdapter(getApplicationContext(), allAdols, AdolsAttendanceList, getApplicationContext());

            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
            recyclerView.setHasFixedSize(true);
            recyclerView.setAdapter(campaignAttendanceAdapter);
            aq.id(R.id.txtCampCrtCount).getTextView().setText(getResources().getString(R.string.reccount) + campaignAttendanceAdapter.getItemCount());


        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    //Ramesh 16-05-2021
    private List<tblAdolReg> addingLastVisitDetailstoAdols(List<tblAdolReg> allAdols) {
        try {
            AdolVisitHeaderRepository adolVisitHeaderRepository = new AdolVisitHeaderRepository(databaseHelper);

            for (tblAdolReg s : allAdols) {
                if (s.getLastVisitCount() != 0) {
                    tblAdolVisitHeader localAdolVisitHeader = adolVisitHeaderRepository.getVisitbyID(String.valueOf(s.getLastVisitCount()), s.getAdolID());
                    s.setRegAdolGoingtoSchool(localAdolVisitHeader.getAdolvisHGTS());
                }
            }
            return allAdols;
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return null;
    }

    private List<tblAdolAttendanceSavings> setAttendanceData(List<tblAdolAttendanceSavings> adolsAttendanceList, List<tblAdolReg> allAdols) {
        try {
            for (int i = 0; i < allAdols.size(); i++) {
                tblAdolAttendanceSavings tblAdolAttendanceSavings = new tblAdolAttendanceSavings();
                tblAdolAttendanceSavings.setAdolID(allAdols.get(i).getAdolID());
                tblAdolAttendanceSavings.setUserID(user.getUserId()); //23May2021 Bindu set userid
                tblAdolAttendanceSavings.setCampID(campID);
                tblAdolAttendanceSavings.setTblAttSavAttendance("Present");
                adolsAttendanceList.add(tblAdolAttendanceSavings);
            }
            return adolsAttendanceList;
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return null;
    }

    private void Initialize() {
        try {
            databaseHelper = getHelper();
            recyclerView = findViewById(R.id.rv_campAttendance);
            aq = new AQuery(this);

            aq.id(R.id.toolbartxtTitle).getTextView().setText(getResources().getString(R.string.campaignattendance));

            imgbt_Attendance = findViewById(R.id.imgbt_attendance);
            imgbt_Summary = findViewById(R.id.imgbt_summary);
            imgbt_Summary.setBackgroundColor(getResources().getColor(R.color.gray));
            imgbt_Attendance.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));

            UserRepository userRepository = new UserRepository(databaseHelper);
            user = userRepository.getOneAuditedUser(appState.ashaId);
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        b.putParcelable("syncState", syncState);
        return b;
    }

    private String _12hoursformate(String currentTime) {
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
            final Date dateObj = sdf.parse(currentTime);

            String time = new SimpleDateFormat("hh:mm a").format(dateObj);
            return time;
        } catch (final ParseException e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

            return "";
        }
    }

    public static int getKey(Map<String, Integer> mapref, String value) {

        int pos = 0;
        for (Map.Entry<String, Integer> map : mapref.entrySet()) {
            pos++;
            if (map.getValue().toString().equals(value)) {
                return pos;
            }

        }
        return pos;
    }
}