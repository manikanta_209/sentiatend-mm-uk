package com.sc.stmansi.adolescent;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.HomeVisit.HomeVisit;
import com.sc.stmansi.HomeVisit.HomeVisitListActivity;
import com.sc.stmansi.R;
import com.sc.stmansi.SanNap.List_SanNap;
import com.sc.stmansi.adolanchistory.AdolANCHistory;
import com.sc.stmansi.childregistration.ChildRegistrationActivity;
import com.sc.stmansi.common.AlertDialogUtil;
import com.sc.stmansi.common.CommonDatePickerFragment;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.common.NavDrawerItem;
import com.sc.stmansi.common.NavDrawerListAdapter;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.configuration.SyncState;
import com.sc.stmansi.database.DBMethods;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.registration.RegistrationActivity;
import com.sc.stmansi.repositories.AdolescentRepository;
import com.sc.stmansi.repositories.AuditRepository;
import com.sc.stmansi.repositories.TransactionHeaderRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.tables.AuditPojo;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblAdolReg;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.viewprofile.ViewProfileActivity;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class AdolDeactivation extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, View.OnClickListener {

    private AppState appState;
    private String AdolId;
    private AQuery aq;
    private DatabaseHelper databaseHelper;
    private SyncState syncState;
    private DrawerLayout drawer;
    private tblAdolReg adolDetails;
    private EditText currentEditText;
    private AuditPojo APJ;
    private int transId;
    private boolean isView = false;
    private TblInstusers user;
    tblregisteredwomen woman;
    String  womanId;
    private AQuery aqPMSel;
    private Activity activity;
    RadioGroup radGrpEDDorLmp;
    private boolean isRegDate;
    Boolean wantToCloseDialog = false;
    private int noOfDays;
  //  private tblregisteredwomen woman;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adol_deactivation);
        try {
            Bundle bundle = getIntent().getBundleExtra("globalState");
            if (bundle != null) {
                appState = bundle.getParcelable("appState");
                syncState = bundle.getParcelable("syncState");
            } else {
                appState = new AppState();
            }
            AdolId = getIntent().getStringExtra("adolID");
            isView = getIntent().getBooleanExtra("isView", false);
            Initialize();
            getSupportActionBar().hide();

            activity = AdolDeactivation.this;

            getData();
            setData();
            clickListeners();
            InitializeDrawer();

            aq.id(R.id.spndeacreason).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (i == 1) {//show mortality field and reset others
                        aq.id(R.id.trreasondate).getView().setVisibility(View.VISIBLE);
                        aq.id(R.id.trotherdeactreason).getView().setVisibility(View.GONE);
                        aq.id(R.id.txtreasondate).getTextView().setText(getResources().getString(R.string.mortalitydate));
                        aq.id(R.id.etotherreason).text("");
                    } else if (i == 2) { //show relocated fields and reset others
                        aq.id(R.id.trreasondate).getView().setVisibility(View.VISIBLE);
                        aq.id(R.id.trotherdeactreason).getView().setVisibility(View.GONE);
                        aq.id(R.id.txtreasondate).getTextView().setText(getResources().getString(R.string.relocateddate));
                        aq.id(R.id.etotherreason).text("");
                    } else if (i == 3) { // show other resond fields
                        aq.id(R.id.trreasondate).getView().setVisibility(View.GONE);
                        aq.id(R.id.trotherdeactreason).getView().setVisibility(View.VISIBLE);
                        aq.id(R.id.txtreasondate).getTextView().setText(getResources().getString(R.string.relocateddate));
                        aq.id(R.id.etreasondate).text("");
                    } else { // hide all other fields
                        aq.id(R.id.trreasondate).getView().setVisibility(View.GONE);
                        aq.id(R.id.trotherdeactreason).getView().setVisibility(View.GONE);
                        aq.id(R.id.etotherreason).text("");
                        aq.id(R.id.etreasondate).text("");
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            aq.id(R.id.btnsave).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (validationDDate() && validationReason() && validaionReasonFields()) {
                        saveData();
                    }
                }
            });

            aq.id(R.id.btncancel).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (isView) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(AdolDeactivation.this);
                        builder.setMessage(getResources().getString(R.string.m110));
                        builder.setCancelable(false);
                        builder.setPositiveButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent(AdolDeactivation.this, AdolescentHome.class);
                                intent.putExtra("tabItem", 2);
                                intent.putExtra("globalState", prepareBundle());
                                startActivity(intent);
                            }
                        }).setNegativeButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).create().show();
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(AdolDeactivation.this);
                        builder.setMessage(getResources().getString(R.string.m110));
                        builder.setCancelable(false);
                        builder.setPositiveButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent(AdolDeactivation.this, AdolescentHome.class);
                                intent.putExtra("tabItem", 0);
                                intent.putExtra("globalState", prepareBundle());
                                startActivity(intent);
                            }
                        }).setNegativeButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).create().show();
                    }
                }
            });
            aq.id(R.id.toolbarBtnHome).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        AlertDialog.Builder builder = new AlertDialog.Builder(AdolDeactivation.this);
                        builder.setMessage(getResources().getString(R.string.m110));
                        builder.setCancelable(false);
                        builder.setPositiveButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent(AdolDeactivation.this, AdolescentHome.class);
                                intent.putExtra("tabItem", 0);
                                intent.putExtra("globalState", prepareBundle());
                                startActivity(intent);
                            }
                        }).setNegativeButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).create().show();
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }
                }
            });

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private void saveData() {
        try {
            int result = 0;
            AdolescentRepository adolescentRepository = new AdolescentRepository(databaseHelper);
            String sql = createsql();
            if (sql.length() != 0) {
                TransactionHeaderRepository.iNewRecordTransNew(user.getUserId(), transId, "tblaudittrail", databaseHelper);
                result = adolescentRepository.updateData(sql);
                TransactionHeaderRepository.iUpdateRecordTransNew(user.getUserId(), transId, "tblAdolReg", sql, null, null, databaseHelper); //23May2021 Bindu set userid
            }
            if (result > 0) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AdolDeactivation.this);
                alertDialogBuilder.setCancelable(false);
                alertDialogBuilder.setMessage(getResources().getString(R.string.userdeactivated));
                alertDialogBuilder.setCancelable(false).setNegativeButton(
                        getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();

                                try {
                                    String womanId = new AdolescentRepository(databaseHelper)
                                            .getAdolANCWomanId(adolDetails.getRegAdolAadharNo(), adolDetails.getAdolID());
                                    if(womanId!=null && womanId.trim().length()>0)
                                    {
                                        displayAlertDeactANC(getResources().getString(R.string.wouldyouliketodeactivateanc)
                                                ,null,
                                                false);
                                    }else {
                                        Intent intent = new Intent(AdolDeactivation.this, AdolescentHome.class);
                                        intent.putExtra("tabItem", 2);
                                        intent.putExtra("globalState", prepareBundle());
                                        startActivity(intent);
                                    }
                                } catch (SQLException throwables) {
                                    throwables.printStackTrace();
                                }
                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private String createsql() {
        try {
            String updateSQL = "";
            String addQuery = "";
            if (addQuery == "")
                addQuery = addQuery + " regAdolDeactivateDate = " + (char) 34 + aq.id(R.id.etdeactivationdate).getEditText().getText().toString() + (char) 34 + "";
            else
                addQuery = addQuery + " ,regAdolDeactivateDate = " + (char) 34 + aq.id(R.id.etdeactivationdate).getEditText().getText().toString() + (char) 34 + "";
            InserttblAuditTrail("regAdolDeactivateDate",
                    adolDetails.getRegAdolDeactivateDate(),
                    aq.id(R.id.etdeactivationdate).getEditText().getText().toString(),
                    transId,"tblAdolReg");

            if (addQuery == "")
                addQuery = addQuery + " regAdolDeactivateReasons = " + (char) 34 + aq.id(R.id.spndeacreason).getSpinner().getSelectedItemPosition() + (char) 34 + "";
            else
                addQuery = addQuery + " ,regAdolDeactivateReasons = " + (char) 34 + aq.id(R.id.spndeacreason).getSpinner().getSelectedItemPosition() + (char) 34 + "";
            InserttblAuditTrail("regAdolDeactivateReasons",
                    adolDetails.getRegAdolDeactivateReasons(),
                    String.valueOf(aq.id(R.id.spndeacreason).getSpinner().getSelectedItemPosition()),
                    transId,"tblAdolReg");
            if (addQuery == "")
                addQuery = addQuery + " regAdolDeactivateReasonOthers = " + (char) 34 + aq.id(R.id.etotherreason).getEditText().getText().toString() + (char) 34 + "";
            else
                addQuery = addQuery + " ,regAdolDeactivateReasonOthers = " + (char) 34 + aq.id(R.id.etotherreason).getEditText().getText().toString() + (char) 34 + "";
            InserttblAuditTrail("regAdolDeactivateReasonOthers",
                    adolDetails.getRegAdolDeactivateReasonOthers(),
                    aq.id(R.id.etotherreason).getEditText().getText().toString(),
                    transId,"tblAdolReg");

            if (aq.id(R.id.etcommentsdeact).getEditText().getText().toString().length() != 0) {
                if (addQuery == "")
                    addQuery = addQuery + " regAdolDeactivateComments = " + (char) 34 + aq.id(R.id.etcommentsdeact).getEditText().getText().toString() + (char) 34 + "";
                else
                    addQuery = addQuery + " ,regAdolDeactivateComments = " + (char) 34 + aq.id(R.id.etcommentsdeact).getEditText().getText().toString() + (char) 34 + "";
                InserttblAuditTrail("regAdolDeactivateComments",
                        adolDetails.getRegAdolDeactivateComments(),
                        aq.id(R.id.etcommentsdeact).getEditText().getText().toString(),
                        transId,"tblAdolReg");
            }
            if (addQuery.length()>0) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " RecordUpdatedDate = " + (char) 34 + DateTimeUtil.getTodaysDate()+" "+DateTimeUtil.getCurrentTime() + (char) 34 + "";
                } else {
                    addQuery = addQuery + " ,RecordUpdatedDate = " + (char) 34 + DateTimeUtil.getTodaysDate()+" "+DateTimeUtil.getCurrentTime() + (char) 34 + "";
                }
                InserttblAuditTrail("RecordUpdatedDate",
                        adolDetails.getRecordUpdatedDate(),
                        DateTimeUtil.getTodaysDate()+" "+DateTimeUtil.getCurrentTime(),
                        transId,"tblAdolReg");
            }

            if (addQuery.length() > 0) {
                updateSQL = "UPDATE tblAdolReg SET ";
                updateSQL = updateSQL + addQuery + " WHERE AdolID = '" + AdolId + "'";
            }
            return updateSQL;
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        return "";
    }

    private boolean validaionReasonFields() {
        try {
            if (aq.id(R.id.spndeacreason).getSpinner().getSelectedItemPosition() != 3) {
                if (aq.id(R.id.etreasondate).getEditText().getText().toString().length() == 0) {
                    aq.id(R.id.etreasondate).getEditText().setError(getResources().getString(R.string.pleaseenterdate));
                    aq.id(R.id.etreasondate).getEditText().requestFocus();
                    return false;
                } else {
                    aq.id(R.id.etreasondate).getEditText().setError(null);
                    return true;
                }
            } else if (aq.id(R.id.spndeacreason).getSpinner().getSelectedItemPosition() == 3) {
                if (aq.id(R.id.etotherreason).getEditText().getText().toString().length() == 0) {
                    aq.id(R.id.etotherreason).getEditText().setError(getResources().getString(R.string.pleaseenterreason));
                    aq.id(R.id.etotherreason).getEditText().requestFocus();
                    return false;
                } else {
                    aq.id(R.id.etotherreason).getEditText().setError(null);
                    return true;
                }
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        return false;
    }

    private boolean validationReason() {
        try {
            if (aq.id(R.id.spndeacreason).getSpinner().getSelectedItemPosition() == 0) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.selectdeactreason), Toast.LENGTH_LONG).show();
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        return false;
    }

    private boolean validationDDate() {
        try {
            if (aq.id(R.id.etdeactivationdate).getEditText().getText().toString().length() == 0) {
                aq.id(R.id.etdeactivationdate).getEditText().setError(getResources().getString(R.string.pleaseenterdate));
                aq.id(R.id.etdeactivationdate).getEditText().requestFocus();
                return false;
            } else {
                aq.id(R.id.etdeactivationdate).getEditText().setError(null);
                return true;
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        return false;
    }

    private void clickListeners() {
        try {
            aq.id(R.id.etreasondate).getEditText().setOnClickListener(this);
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    @SuppressLint("SetTextI18n")
    private void setData() {
        try {
            String dateTime = adolDetails.getRecordCreatedDate();
            int index = dateTime.indexOf(" ");
            String date = dateTime.substring(0, index);
            aq.id(R.id.txtname).text(getResources().getString(R.string.reg_date) + " " + date);

            aq.id(R.id.etdeactivationdate).text(DateTimeUtil.getTodaysDate());

            aq.id(R.id.toolbartxtTitle).getTextView().setText(adolDetails.getRegAdolName() + "\n" + adolDetails.getRegAdolAge() + getResources().getString(R.string.yearstxt));

            if (isView) {
                aq.id(R.id.etdeactivationdate).text(adolDetails.getRegAdolDeactivateDate());
                aq.id(R.id.etdeactivationdate).getEditText().setEnabled(false);
                aq.id(R.id.spndeacreason).getSpinner().setEnabled(false);

                aq.id(R.id.spndeacreason).setSelection(Integer.parseInt(adolDetails.getRegAdolDeactivateReasons()));
                if (Integer.parseInt(adolDetails.getRegAdolDeactivateReasons()) != 3) {
                    aq.id(R.id.trreasondate).getView().setVisibility(View.VISIBLE);
                    aq.id(R.id.etreasondate).text(adolDetails.getRegAdolDeactivateDate());
                    aq.id(R.id.etreasondate).getEditText().setEnabled(false);

                } else if (Integer.parseInt(adolDetails.getRegAdolDeactivateReasons()) == 3) {
                    aq.id(R.id.trotherdeactreason).getView().setVisibility(View.VISIBLE);
                    aq.id(R.id.etotherreason).text(adolDetails.getRegAdolDeactivateReasonOthers());
                    aq.id(R.id.etotherreason).getEditText().setEnabled(false);
                }

                aq.id(R.id.etcommentsdeact).text(adolDetails.getRegAdolDeactivateComments());
                aq.id(R.id.etcommentsdeact).getEditText().setEnabled(false);
                aq.id(R.id.btnsave).getImageView().setVisibility(View.GONE);
            }
            transId = TransactionHeaderRepository.iCreateNewTransNew(user.getUserId(), databaseHelper); //23MAy2021 Bindu change to userid

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private void getData() {
        try {
            AdolescentRepository adolescentRepository = new AdolescentRepository(databaseHelper);
            adolDetails = adolescentRepository.getAdolDatafromAdolID(AdolId);

            UserRepository userRepository = new UserRepository(databaseHelper);
            user = userRepository.getOneAuditedUser(appState.ashaId);
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private void Initialize() {
        try {
            databaseHelper = getHelper();
            DBMethods dbMethods = new DBMethods(databaseHelper);
            aq = new AQuery(this);
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(getApplicationContext(), DatabaseHelper.class);
        }
        return databaseHelper;
    }

    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        b.putParcelable("syncState", syncState);
        return b;
    }

    private void InitializeDrawer() {
        try {
            drawer = findViewById(R.id.drawer_adolDeactivate);
            ListView drawerList = findViewById(R.id.lvNav_adolDeactivate);

            ArrayList<NavDrawerItem> navDrawerItems = new ArrayList<>();
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.editadolescent), R.drawable.ic_edit_icon));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.homevisitadd), R.drawable.ic_homevisit));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.adolvisithistory), R.drawable.anm_pending_activities));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.trainingparticipated), R.drawable.presentation));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.mhmform), R.drawable.form_icon));

            //            07Sep2021 Arpitha
//            if(adolDetails.getRegAdolPregnant()!=null && adolDetails.getRegAdolPregnant().trim().length()>0
//                    && adolDetails.getRegAdolPregnant().equalsIgnoreCase("Yes")) {

                String strAddPregnancy = "";
                if (new AdolescentRepository(databaseHelper).
                        isAdolANC(adolDetails.getRegAdolAadharNo(), adolDetails.getAdolID()))
                    strAddPregnancy = getResources().getString(R.string.viewpregancny);
                else
                    strAddPregnancy = getResources().getString(R.string.addpregancny);

                navDrawerItems.add(new NavDrawerItem(strAddPregnancy, R.drawable.anc));
                womanId = new AdolescentRepository(databaseHelper).getAdolANCWomanId(
                        adolDetails.getRegAdolAadharNo(), adolDetails.getAdolID());
//                if(womanId!=null && womanId.trim().length()>0)
                navDrawerItems.add(new NavDrawerItem(getResources()
                        .getString(R.string.anchomevisit), R.drawable.ic_homevisit));

            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.preghistory),R.drawable.anc));//12Sep2021 Arpitha


            //}

         //   navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deactivate), R.drawable.deactivate));
            drawer.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
            drawerList.setAdapter(new NavDrawerListAdapter(getApplicationContext(), navDrawerItems));
            drawerList.setOnItemClickListener(new DrawerItemClickListener());
            Toolbar toolbar = findViewById(R.id.toolbarCommon);
            ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close);
            drawer.setDrawerListener(drawerToggle);
            drawerToggle.syncState();
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
        try {
            if (datePicker.isShown()) {
                String strseldate = null;
                strseldate = String.format("%02d", dayOfMonth) + "-" + String.format("%02d", month + 1) + "-"
                        + year;

                @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                Date seldate = format.parse(strseldate);
                String dateTime = adolDetails.getRecordCreatedDate();
                int index = dateTime.indexOf(" ");
                String date = dateTime.substring(0, index);
                Date regDate = new SimpleDateFormat("dd-MM-yyyy").parse(date);
                if (currentEditText == aq.id(R.id.etreasondate).getEditText()) {
                    if (seldate != null && seldate.after(new Date())) {
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.date_cannot_af_currentdate)
                                , this);
                        aq.id(R.id.etreasondate).text("");
                    } else if (seldate != null && seldate.before(regDate)) {
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.date_cannot_bf_regdate)
                                , this);
                        aq.id(R.id.etreasondate).text("");
                    } else {
                        aq.id(R.id.etreasondate).text(strseldate);
                        validaionReasonFields();
                    }
                }else//09Sep2021 Arpitha
                {
                    String str = String.format("%02d", dayOfMonth) + "-" + String.format("%02d", month + 1) + "-"
                            + year;

                    int days = DateTimeUtil.getDaysBetweenDates(DateTimeUtil.getTodaysDate(), str);

                    if (!aqPMSel.id(R.id.radEDD).isChecked()) {
                        if (days >= 0) {
                            if (isRegDate) {
                                if (days < 30) {
                                    aqPMSel.id(R.id.etregdate).text(str);
                                    aqPMSel.id(R.id.imgclearregadate).visible();//20AUg2019 Arpitha
                                } else if (days > 120) {
                                    aqPMSel.id(R.id.etregdate).text("");
                                    aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                                    Toast.makeText(activity, getResources().getString(R.string.date_val_2_toast),
                                            Toast.LENGTH_LONG).show();
                                    aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha

                                } else {
                                    aqPMSel.id(R.id.etregdate).text(str);
                                    aqPMSel.id(R.id.imgclearregadate).visible();//20AUg2019 Arpitha
                                }

                            } else if (aqPMSel.id(R.id.radLMP).isChecked()) {
                                String lmporadd = str;
                                int daysdiff = 0;
                                String regdate = aqPMSel.id(R.id.etregdate).getText().toString();

                                if (regdate != null && regdate.trim().length() > 0) {
                                    daysdiff = DateTimeUtil.getDaysBetweenDates(regdate, lmporadd);

                                    int daysdifffromCUrr = DateTimeUtil
                                            .getDaysBetweenDates(DateTimeUtil.getTodaysDate(), lmporadd);

                                    if (daysdiff <= 30) {
                                        Toast.makeText(activity,
                                                getResources().getString(R.string.date_val_3_toast),
                                                Toast.LENGTH_LONG).show();
                                        aqPMSel.id(R.id.etlmpdate).text("");
                                        aqPMSel.id(R.id.etgest).text("");
                                        aqPMSel.id(R.id.etMotherAdd).text("");
                                        aqPMSel.id(R.id.imgclearlmp).gone();//20Aug2019 Arpitha
                                    } else if (daysdifffromCUrr > 280) {
                                        Toast.makeText(activity,
                                                getResources().getString(R.string.date_val_4_toast),
                                                Toast.LENGTH_LONG).show();
                                        aqPMSel.id(R.id.etlmpdate).text("");
                                        aqPMSel.id(R.id.etgest).text("");
                                        aqPMSel.id(R.id.etMotherAdd).text("");
                                        aqPMSel.id(R.id.imgclearlmp).gone();//20Aug2019 Arpitha
                                    } else {
                                        aqPMSel.id(R.id.etMotherAdd).text(str);
                                        calculateEddGestation("lmp");
                                        aqPMSel.id(R.id.imgclearlmp).visible();//20Aug2019 Arpitha
                                    }
                                } else
                                    Toast.makeText(activity, getResources().getString(R.string.plz_enter_reg_dt),
                                            Toast.LENGTH_LONG).show();
                            } else if (aqPMSel.id(R.id.rdbMother).isChecked()) {

                                int daysdiff = DateTimeUtil
                                        .getDaysBetweenDates(aqPMSel.id(R.id.etregdate).getText().toString(), str);
                                if (daysdiff > 365) {// Arpitha 28Jun2018 - 23Dec2019 Arpitha - allow max 1 year

                                    Toast.makeText(activity, (getResources().getString(R.string.datecannotbeafteroneyearfromcurrentdate)), // 11July2018
                                            // Arpitha
                                            Toast.LENGTH_LONG).show();
                                    aqPMSel.id(R.id.etMotherAdd).text("");
                                } else
                                    aqPMSel.id(R.id.etMotherAdd).text(str);
                            } else if (aqPMSel.id(R.id.rdbchild).isChecked()) {

                                int daysdiff = DateTimeUtil
                                        .getDaysBetweenDates(aqPMSel.id(R.id.etregdate).getText().toString(), str);
                                if (daysdiff > 5844) {// Arpitha 28Jun2018

                                    Toast.makeText(activity, (getResources().getString(R.string.date_val_7_toast)), // 11July2018
                                            // Arpitha
                                            Toast.LENGTH_LONG).show();
                                    aqPMSel.id(R.id.etMotherAdd).text("");
                                } else
                                    aqPMSel.id(R.id.etMotherAdd).text(str);
                            }
                        } else {
                            if (isRegDate) {
                                aqPMSel.id(R.id.etregdate).text("");
                                aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                                Toast.makeText(activity,
                                        getResources().getString(R.string.reg_date_cannot_greater_current_date),
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha
                            } else {
                                aqPMSel.id(R.id.etMotherAdd).text("");
                                aqPMSel.id(R.id.imgclearlmp).gone();//20AUg2019 Arpitha
                                aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                                if (aqPMSel.id(R.id.rdbPreg).isChecked()) {
                                    Toast.makeText(activity,
                                            getResources()
                                                    .getString(R.string.lmp_date_time_cannot_be_after_curre_datetime), // 11July2018

                                            Toast.LENGTH_LONG).show();

                                    aqPMSel.id(R.id.etlmpdate).text("");
                                    aqPMSel.id(R.id.etgest).text("");
                                } else {
                                    Toast.makeText(activity,
                                            getResources()
                                                    .getString(R.string.del_date_cannot_be_after_current_date_time), // 11July2018
                                            Toast.LENGTH_LONG).show();
                                }

                            }
                        }
                    } else {
                        if (!isRegDate) {
                            int days1 = 0;
                            if (aqPMSel.id(R.id.etregdate).getText().toString().trim().length() > 0)
                                days1 = DateTimeUtil.getDaysBetweenDates(str,
                                        aqPMSel.id(R.id.etregdate).getText().toString());

                            if (days1 > 0 && days1 < 273) {
                                aqPMSel.id(R.id.etMotherAdd).text(str);
                                calculateEddGestation("edd");
                                aqPMSel.id(R.id.imgclearlmp).visible();//20Aug2019 Arpitha
                            } else {
                                if (days1 > 280) {
                                    aqPMSel.id(R.id.etMotherAdd).text("");
                                    aqPMSel.id(R.id.imgclearlmp).gone();//21Aug2019 Arpitha
                                    aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                                    Toast.makeText(activity, getResources().getString(R.string.date_val_6_toast),

                                            Toast.LENGTH_LONG).show();

                                    aqPMSel.id(R.id.etlmpdate).text("");
                                    aqPMSel.id(R.id.etgest).text("");

                                } else {
                                    aqPMSel.id(R.id.etMotherAdd).text("");
                                    aqPMSel.id(R.id.imgclearlmp).gone();//21Aug2019 Arpitha
                                    aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                                    Toast.makeText(activity, getResources().getString(R.string.date_val_5_toast),
                                            Toast.LENGTH_LONG).show();
                                    aqPMSel.id(R.id.etlmpdate).text("");
                                    aqPMSel.id(R.id.etgest).text("");
                                }
                            }
                        } else {
                            days = DateTimeUtil.getDaysBetweenDates(DateTimeUtil.getTodaysDate(), str);
                            if (days >= 0) {

                                if (days > 90) {
                                    aqPMSel.id(R.id.etregdate).text("");
                                    aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                                    Toast.makeText(activity, getResources().getString(R.string.date_val_2_toast),

                                            Toast.LENGTH_LONG).show();
                                    aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha
                                } else {
                                    aqPMSel.id(R.id.etregdate).text(str);
                                }

                            } else {
                                aqPMSel.id(R.id.etregdate).text("");
                                aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                                Toast.makeText(activity, getResources().getString(R.string.reg_date_cannot_greater_current_date),

                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha
                            }
                        }
                    }
                }//09Sep2021 Arpitha
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.etreasondate) {
            assignEditTextAndCallDatePicker(aq.id(R.id.etreasondate).getEditText());
        }
    }

    private class DrawerItemClickListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            try {
                switch (i) {
                    case 0:
                        AlertDialog.Builder builder = new AlertDialog.Builder(AdolDeactivation.this);
                        builder.setMessage(getResources().getString(R.string.m110));
                        builder.setCancelable(false);
                        builder.setPositiveButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent(AdolDeactivation.this, AdolRegistration.class);
                                intent.putExtra("globalState", prepareBundle());
                                intent.putExtra("isEdit", true);
                                intent.putExtra("adolID", AdolId);
                                startActivity(intent);

                            }
                        }).setNegativeButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).create().show();

                        break;
                    case 1:
                        if (isView) {
                            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.adolescentdeactivated), AdolDeactivation.this);
                            drawer.closeDrawer(GravityCompat.END);
                        } else {
                            Intent intent = new Intent(AdolDeactivation.this, AdolVisit_New.class);
                            intent.putExtra("globalState", prepareBundle());
                            intent.putExtra("adolID", AdolId);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                        break;
                    case 2:
                        builder = new AlertDialog.Builder(AdolDeactivation.this);
                        builder.setMessage(getResources().getString(R.string.m110));
                        builder.setCancelable(false);
                        builder.setPositiveButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent(AdolDeactivation.this, AdolVisitHistory.class);
                                intent.putExtra("globalState", prepareBundle());
                                intent.putExtra("adolID", AdolId);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);

                            }
                        }).setNegativeButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).create().show();
                        break;
                    case 3:
                        builder = new AlertDialog.Builder(AdolDeactivation.this);
                        builder.setMessage(getResources().getString(R.string.m110));
                        builder.setCancelable(false);
                        builder.setPositiveButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent(AdolDeactivation.this, AdolParticipatedTrainings.class);
                                intent.putExtra("globalState", prepareBundle());
                                intent.putExtra("adolID", AdolId);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);

                            }
                        }).setNegativeButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).create().show();
                        break;
                    case 4:
                        if (adolDetails != null && adolDetails.getRegAdolGender().equals("Female") && adolDetails.getRegAdolisPeriod().equals("Yes")) {
                            Intent intent  = new Intent(AdolDeactivation.this, List_SanNap.class);
                            intent.putExtra("globalState", prepareBundle());
                            intent.putExtra("adolID", AdolId);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }else {
                            drawer.closeDrawer(GravityCompat.END);
                        }
                        break;
                    // 01Sep2021 Arpitha
                    case 5:

                        if((adolDetails.getRegAdolPregnant()!=null &&
                                adolDetails.getRegAdolPregnant().trim().length() >0
                                && adolDetails.getRegAdolPregnant()
                                .equalsIgnoreCase("Yes")) ||
                                new AdolescentRepository(databaseHelper).
                                        isAdolPreganant(adolDetails.getAdolID()))
                        {
                            if (new AdolescentRepository(databaseHelper)
                                    .isAdolANC(adolDetails.getRegAdolAadharNo(), adolDetails.getAdolID())) {
                                appState.selectedWomanId = womanId;
                                Intent intent1 = new Intent(AdolDeactivation.this, ViewProfileActivity.class);
                                intent1.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                startActivity(intent1);
                            } else
                                showPregnantOrMotherSelectionDialog();

                        }else
                            Toast.makeText(getApplicationContext(),getResources().getString(R.string.ntapplicable),
                                    Toast.LENGTH_LONG).show();
                        break;

                    case 6:
                        if (new AdolescentRepository(databaseHelper)
                                .isAdolANC(adolDetails.getRegAdolAadharNo(), adolDetails.getAdolID()))
                        {
                            appState.selectedWomanId = womanId;
                            Intent hv = new Intent(AdolDeactivation.this, HomeVisitListActivity.class);
                            hv.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(hv);
                        }else
                            Toast.makeText(getApplicationContext(),getResources().getString(R.string.ntapplicable),
                                    Toast.LENGTH_LONG).show();
                        break;
                    //                        12Sep2021 Arpitha
                    case 7:
                        if(new AdolescentRepository(databaseHelper)
                                .isAdolANCHistory(adolDetails.getRegAdolAadharNo(), adolDetails.getAdolID()))//10Sep2021 Arpitha
                        {
                            appState.selectedWomanId = womanId;
                            Intent intent = new Intent(view.getContext(), AdolANCHistory.class);
                            intent.putExtra("globalState", prepareBundle());
                            intent.putExtra("aadharno",
                                    adolDetails.getRegAdolAadharNo());
                            startActivity(intent);
                        }else
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.ntapplicable),
                                    Toast.LENGTH_LONG).show();

                        break;// 12Sep2021 Arpitha
                    case 8:
                        drawer.closeDrawer(GravityCompat.END);
                        break;

                }
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
        }
    }

    private void assignEditTextAndCallDatePicker(EditText editText) {
        currentEditText = editText;
        String currDate = (editText.getText().length() > 0) ? editText.getText().toString() : "";
        CommonDatePickerFragment.getCommonDate(this, currDate);
    }

    private boolean InserttblAuditTrail(String ColumnName, String Old_Value,
                                        String New_Value, int transId,String tblname) throws Exception {

        APJ = new AuditPojo();
        APJ.setUserId(user.getUserId());
        APJ.setWomanId(AdolId);
        APJ.setTblName(tblname);
        APJ.setDateChanged(DateTimeUtil.getTodaysDate());
        APJ.setColumnName(ColumnName);
        APJ.setOld_Value(Old_Value);
        APJ.setNew_Value(New_Value);
        APJ.setTransId(transId);
        APJ.setRecordcreateddate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());

        AuditRepository auditRepository = new AuditRepository(databaseHelper);
        return AuditRepository.insertTotblAuditTrailNew(APJ, databaseHelper);

    }

    @Override
    public void onBackPressed() {
        if (isView) {
            AlertDialog.Builder builder = new AlertDialog.Builder(AdolDeactivation.this);
            builder.setMessage(getResources().getString(R.string.m110));
            builder.setCancelable(false);
            builder.setPositiveButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent intent = new Intent(AdolDeactivation.this, AdolescentHome.class);
                    intent.putExtra("tabItem", 2);
                    intent.putExtra("globalState", prepareBundle());
                    startActivity(intent);
                }
            }).setNegativeButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            }).create().show();
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(AdolDeactivation.this);
            builder.setMessage(getResources().getString(R.string.m110));
            builder.setCancelable(false);
            builder.setPositiveButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent intent = new Intent(AdolDeactivation.this, AdolescentHome.class);
                    intent.putExtra("tabItem", 2);
                    intent.putExtra("globalState", prepareBundle());
                    startActivity(intent);
                }
            }).setNegativeButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            }).create().show();
        }
    }


    /**
     * Save Confirmation Alert
     *
     * @param goToScreen
     */
    private void displayAlertDeactANC(final String spanText2,
                                      final Intent goToScreen, final boolean isDeactivate) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        String strOkMess;
        if(goToScreen!=null)
            strOkMess = getResources().getString(R.string.yes);
        else
            strOkMess = getResources().getString(R.string.ok);


        // set dialog message
        alertDialogBuilder.setMessage(spanText2).setCancelable(false).setNegativeButton(
                strOkMess, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        try {
                            deactivateANC();
                          /*  if (goToScreen != null) {

                                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));


                                if (spanText2.contains(getResources().getString(R.string.save))) {
                                   // saveDeactivateData();
                                }*/

                               /* else if (isDeactivate) {
                                    int updateChild = 0;
                                    for (int i = 0; i < tblChildInfoFromDbList.size(); i++) {
                                        tblChildInfoFromDb = tblChildInfoFromDbList.get(i);
                                        String sqlChildData = updateChildData(transId, tblChildInfoFromDb);
                                        ChildRepository childRepository = new ChildRepository(databaseHelper);
                                        updateChild = childRepository.updateChildData(user.getUserId(),
                                                sqlChildData, transId, databaseHelper);
                                    }
                                    if (updateChild > 0) {
                                        Intent wlist = new Intent(WomanDeactivateActivity.this, WomanListActivity.class);
                                        wlist.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                        wlist.putExtra("screen", "deact");
                                        startActivity(wlist);
                                    }

                                }else {

                                    startActivity(goToScreen);
                                    LoginRepository loginRepository = new LoginRepository(databaseHelper);
                                    loginRepository.updateLogoutTime(appState.loginAuditId, appState.sessionUserId);


                                }
                            }
                            else*/
                                dialog.cancel();
                        } catch (Exception e) {
                            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                            crashlytics.log(e.getMessage());
                            //}
                        }
                    }
                });
        if(goToScreen!=null) {
            alertDialogBuilder.setPositiveButton(getResources().getString(R.string.m122),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent intent = new Intent(AdolDeactivation.this, AdolescentHome.class);
                            intent.putExtra("tabItem", 2);
                            intent.putExtra("globalState", prepareBundle());
                            startActivity(intent);
                           /* if(spanText2.contains("Deactive"))
                            {
                                Intent wlist = new Intent(AdolDeactivation.this, WomanListActivity.class);
                                wlist.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                wlist.putExtra("screen", "deact");
                                startActivity(wlist);
                            }*/
                            dialog.cancel();
                        }
                    });
        }
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    void deactivateANC() throws Exception {
        String sql = updateDelData(transId);

        WomanRepository womanRepository = new WomanRepository(databaseHelper);
        boolean added = TransactionHeaderRepository.iNewRecordTransNew(appState.sessionUserId, transId, "tblaudittrail", databaseHelper);

        if (added) {
            int updateReg = womanRepository.updateWomenRegDataNew(user.getUserId(), sql, transId, null, databaseHelper);

            if(updateReg>0)
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.ancdeactivated),Toast.LENGTH_LONG).show();
                Intent intent = new Intent(AdolDeactivation.this, AdolescentHome.class);
                intent.putExtra("tabItem", 2);
                intent.putExtra("globalState", prepareBundle());
                startActivity(intent);
            }
        }
    }

        private String updateDelData(int transId) throws Exception {

        woman = new tblregisteredwomen();
        woman.setDateDeactivated(aq.id(R.id.etdeactivationdate).getText().toString());

        String[] arrReason = getResources().getStringArray(R.array.deactwreasonsave);

        int pos = new ArrayList<String>(Arrays.asList(arrReason)).indexOf(aq.id(R.id.spndeacreason).getSelectedItem().toString());
        woman.setReasonforDeactivation(String.
                valueOf(pos));
        woman.setOtherDeactivationReason(aq.id(R.id.etotherreason).getText().toString());
        woman.setDeacComments(aq.id(R.id.etcommentsdeact).getText().toString());
        woman.setRecordUpdatedDate(DateTimeUtil.getTodaysDate()+" "+ DateTimeUtil.getCurrentTime());


        if(aq.id(R.id.spndeacreason).getSelectedItem()
                .toString().equalsIgnoreCase(getResources().getString(R.string.dereaabortion)))
            woman.setDateAborted(aq.id(R.id.etreasondate).getText().toString());
        else if(aq.id(R.id.spndeacreason).getSelectedItem()
                .toString().equalsIgnoreCase(getResources().getString(R.string.derearelocated)))
            woman.setDateTransferred(aq.id(R.id.etreasondate).getText().toString());

        else if(aq.id(R.id.spndeacreason).getSelectedItem()
                .toString().equalsIgnoreCase(getResources().getString(R.string.dereamortality)))
            woman.setDateExpired(aq.id(R.id.etreasondate).getText().toString());

        woman.setCurrentWomenStatus("deact");




        return checkForAudit(transId);
    }

        String checkForAudit(int transId) throws Exception {

        String addString = "", delSql = "";

        if (addString == "")
            addString = addString + " DateDeactivated = " + (char) 34 + woman.getDateDeactivated() + (char) 34 + "";
        else
            addString = addString + " ,DateDeactivated = " + (char) 34 + woman.getDateDeactivated() + (char) 34 + "";
        InserttblAuditTrail("DateDeactivated",
                null,
                woman.getDateDeactivated(),
                transId,"tblregisteredwomen");

        if (addString == "")
            addString = addString + " ReasonforDeactivation = " + (char) 34 + woman.getReasonforDeactivation() + (char) 34 + "";
        else
            addString = addString + " ,ReasonforDeactivation = " + (char) 34 + woman.getReasonforDeactivation() + (char) 34 + "";
        InserttblAuditTrail("ReasonforDeactivation",
                null,
                woman.getReasonforDeactivation(),
                transId,"tblregisteredwomen");

        if (addString == "")
            addString = addString + " OtherDeactivationReason = " + (char) 34 + woman.getOtherDeactivationReason() + (char) 34 + "";
        else
            addString = addString + " ,OtherDeactivationReason = " + (char) 34 + woman.getOtherDeactivationReason() + (char) 34 + "";
        InserttblAuditTrail("OtherDeactivationReason",
                null,
                woman.getOtherDeactivationReason(),
                transId,"tblregisteredwomen");


        if (addString == "")
            addString = addString + " DeacComments = " + (char) 34 + woman.getDeacComments() + (char) 34 + "";
        else
            addString = addString + " ,DeacComments = " + (char) 34 + woman.getDeacComments() + (char) 34 + "";
        InserttblAuditTrail("DeacComments",
                null,
                woman.getDeacComments(),
                transId,"tblregisteredwomen");


        if(aq.id(R.id.spndeacreason).getSelectedItem()
                .toString().equalsIgnoreCase(getResources().getString(R.string.derearelocated))) {
            if (addString == "")
                addString = addString + " DateTransferred = " + (char) 34 + woman.getDateTransferred() + (char) 34 + "";
            else
                addString = addString + " ,DateTransferred = " + (char) 34 + woman.getDateTransferred() + (char) 34 + "";
            InserttblAuditTrail("DateTransferred",
                   null,
                    woman.getDateTransferred(),
                    transId,"tblregisteredwomen");
        }else  if(aq.id(R.id.spndeacreason).getSelectedItem()
                .toString().equalsIgnoreCase(getResources().getString(R.string.dereamortality))) {
            if (addString == "")
                addString = addString + " DateExpired = " + (char) 34 + woman.getDateExpired() + (char) 34 + "";
            else
                addString = addString + " ,DateExpired = " + (char) 34 + woman.getDateExpired() + (char) 34 + "";
            InserttblAuditTrail("DateExpired",
                   null,
                    woman.getDateExpired(),
                    transId,"tblregisteredwomen");
        }else  if(aq.id(R.id.spndeacreason).getSelectedItem()
                .toString().equalsIgnoreCase(getResources().getString(R.string.dereaabortion))) {
            if (addString == "")
                addString = addString + " DateAborted = " + (char) 34 + woman.getDateAborted() + (char) 34 + "";
            else
                addString = addString + " ,DateAborted = " + (char) 34 + woman.getDateAborted() + (char) 34 + "";
            InserttblAuditTrail("DateAborted",
                    null,
                    woman.getDateAborted(),
                    transId,"tblregisteredwomen");
        }


        if (addString == "")
            addString = addString + " CurrentWomenStatus = " + (char) 34 + woman.getCurrentWomenStatus() + (char) 34 + "";
        else
            addString = addString + " ,CurrentWomenStatus = " + (char) 34 + woman.getCurrentWomenStatus() + (char) 34 + "";
        InserttblAuditTrail("CurrentWomenStatus",
               null,
                woman.getCurrentWomenStatus(),
                transId,"tblregisteredwomen");


        if (addString == "")
            addString = addString + " RecordUpdatedDate = " + (char) 34 + woman.getRecordUpdatedDate() + (char) 34 + "";
        else
            addString = addString + " ,RecordUpdatedDate = " + (char) 34 + woman.getRecordUpdatedDate() + (char) 34 + "";
        InserttblAuditTrail("RecordUpdatedDate",
                null,
                woman.getRecordUpdatedDate(),
                transId,"tblregisteredwomen");

        if (addString != null && addString.length() > 0) {
            delSql = "UPDATE tblregisteredwomen SET ";
            delSql = delSql + addString + " WHERE WomanId = '"
                    + appState.selectedWomanId + "' and UserId = '" + user.getUserId() + "'";
        }
        return delSql;
    }

    //    01Sep2021 Arpitha
    /**
     * Alert Dialog to select Pregnant or Mother Registration
     */
    private void showPregnantOrMotherSelectionDialog() {
        try {
            final AlertDialog.Builder alert = new AlertDialog.Builder(activity,
                    AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
            final View customView = LayoutInflater.from(this).inflate(R.layout.pregnant_mother_selection,
                    null);

            aqPMSel = new AQuery(customView);
            radGrpEDDorLmp = customView.findViewById(R.id.radEDDorLmp);

            initializeScreen(aqPMSel.id(R.id.llPregOrMothMainLayout).getView());
            setInitialView();

            aqPMSel.id(R.id.llmother).gone();
            aqPMSel.id(R.id.llchild).gone();
            aqPMSel.id(R.id.llAdol).gone();



            aqPMSel.id(R.id.imgclearedddate).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    aqPMSel.id(R.id.etlmpdate).text("");
                }
            });
            aqPMSel.id(R.id.imgclearlmp).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    aqPMSel.id(R.id.etMotherAdd).text("");
                    aqPMSel.id(R.id.etlmpdate).text("");
                    aqPMSel.id(R.id.etgest).text("");
                    aqPMSel.id(R.id.imgclearlmp).gone();
                }
            });
            aqPMSel.id(R.id.imgclearregadate).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());
                    Toast.makeText(activity, getResources().getString(R.string.dateresettotodaysdate), Toast.LENGTH_LONG).show();//21Aug2019 Arpitha
                }
            });//   Arpitha - 20Aug2019

//            aqPMSel.id(R.id.etMotherAdd).text("20-05-2021");
            aqPMSel.id(R.id.eteddmother).enabled(false);

            aqPMSel.id(R.id.etMotherAdd).getEditText().setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    try {
                        if (MotionEvent.ACTION_UP == event.getAction()) {
                            isRegDate = false;
                            assignEditTextAndCallDatePicker(aqPMSel.id(R.id.etMotherAdd).getEditText());
                        }
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());

                    }
                    return true;
                }
            });

            aqPMSel.id(R.id.etregdate).getEditText().setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    try {
                        if (MotionEvent.ACTION_UP == event.getAction()) {
                            isRegDate = true;
                            assignEditTextAndCallDatePicker(aqPMSel.id(R.id.etregdate).getEditText());
                        }
                    } catch (Exception e) {

                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }
                    return true;
                }
            });

            aqPMSel.id(R.id.radLMP).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        setLMP();
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }
                }
            });

            aqPMSel.id(R.id.radEDD).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        setEDD();
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }
                }
            });

            alert.setView(customView);

            alert.setCancelable(true)
                    .setTitle(getResources().getString(R.string.registrationHeading))
                    .setPositiveButton(getResources().getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {

                                }
                            })
                    .setNegativeButton(getResources().getString(R.string.cancel), //01Oct2019 - Bindu - Add cancel button
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    dialog.cancel();
                                }
                            });
            final AlertDialog dialog = alert.create();
            if (dialog != null)
                dialog.show();

            // Overriding the handler immediately after show is probably a
            // better
            // approach than OnShowListener as described below
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        validateAndRegister();
                        if (wantToCloseDialog)
                            dialog.dismiss();
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }

                }
            });
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }


    //calculate gestation based on LMP
    private void calculateEddGestation(String lmp) {
        try {

            if (lmp.equalsIgnoreCase("lmp")) {
                if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0
                        && aqPMSel.id(R.id.etMotherAdd).getText().length() > 0) {

                    noOfDays = DateTimeUtil
                            .getNumberOfDaysFromLMP(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                    int noOfWeeks = noOfDays / 7;
                    int days = noOfDays % 7;

                    String strdays = "";

                    if (days > 1)
                        strdays = days + " " + activity.getResources().getString(R.string.days);
                    else if (days == 1)
                        strdays = days + " " + activity.getResources().getString(R.string.day);

                    aqPMSel.id(R.id.etgest)
                            .text(noOfWeeks + " " + activity.getResources().getString(R.string.weeks) + " " + strdays);
                }

                String edd = populateEDD(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                aqPMSel.id(R.id.etlmpdate).text(edd);

            } else {

                String strLmp = CalculateLMP(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                aqPMSel.id(R.id.etlmpdate).text(strLmp);

                if (aqPMSel.id(R.id.etlmpdate).getText().length() > 0
                        && aqPMSel.id(R.id.etlmpdate).getText().length() > 0) {

                    noOfDays = DateTimeUtil
                            .getNumberOfDaysFromLMP(aqPMSel.id(R.id.etlmpdate).getText().toString());
                    int noOfWeeks = noOfDays / 7;
                    int days = noOfDays % 7;

                    String strdays = "";

                    if (days > 1)
                        strdays = days + " " + activity.getResources().getString(R.string.days);
                    else if (days == 1)
                        strdays = days + " " + activity.getResources().getString(R.string.day);

                    aqPMSel.id(R.id.etgest)
                            .text(noOfWeeks + " " + activity.getResources().getString(R.string.weeks) + " " + strdays);
                }

            }
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }

    }

    // Calculate EDD and display in EDD Edit text
    private String populateEDD(String xLMP) throws Exception {
        String stredddate = "";
        try {

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());

            Date LmpDate;
            LmpDate = sdf.parse(xLMP);
            Calendar cal = Calendar.getInstance();
            cal.setTime(LmpDate);
            cal.add(Calendar.DAY_OF_MONTH, 280);

            int day = cal.get(Calendar.DAY_OF_MONTH);
            int month = cal.get(Calendar.MONTH) + 1;
            int year = cal.get(Calendar.YEAR);

            stredddate =
                    String.format("%02d", day) + "-" + String.format("%02d", month) + "-" + String.format("%02d", year);

        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        return stredddate;
    }

    //	calculate LMP based on EDD
    private String CalculateLMP(String EDDate) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        Date LMP;
        LMP = sdf.parse(EDDate);
        Calendar cal = Calendar.getInstance();
        cal.setTime(LMP);
        cal.add(Calendar.DAY_OF_MONTH, -280);

        int day = cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH) + 1;
        int year = cal.get(Calendar.YEAR);

        String lmpDate = String.format("%02d", day) + "-" + String.format("%02d", month) + "-" + year;
        return lmpDate;
    }

    /**
     * This method opens the Date Dialog and sets Date to respective EditText
     */
   /* private void assignEditTextAndCallDatePicker(EditText editText) {
        String currDate = (editText.getText().length() > 0) ? editText.getText().toString() : "";
        CommonDatePickerFragment.getCommonDate(activity, currDate);
    }*/

   /* @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        try {
            aqPMSel.id(R.id.imgclearlmp).background(R.drawable.brush1);
            if (view.isShown()) {
                String str = String.format("%02d", day) + "-" + String.format("%02d", month + 1) + "-"
                        + year;

                int days = DateTimeUtil.getDaysBetweenDates(DateTimeUtil.getTodaysDate(), str);

                if (!aqPMSel.id(R.id.radEDD).isChecked()) {
                    if (days >= 0) {
                        if (isRegDate) {
                            if (days < 30) {
                                aqPMSel.id(R.id.etregdate).text(str);
                                aqPMSel.id(R.id.imgclearregadate).visible();//20AUg2019 Arpitha
                            } else if (days > 120) {
                                aqPMSel.id(R.id.etregdate).text("");
                                aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                                Toast.makeText(activity, getResources().getString(R.string.date_val_2_toast),
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha

                            } else {
                                aqPMSel.id(R.id.etregdate).text(str);
                                aqPMSel.id(R.id.imgclearregadate).visible();//20AUg2019 Arpitha
                            }

                        } else if (aqPMSel.id(R.id.radLMP).isChecked()) {
                            String lmporadd = str;
                            int daysdiff = 0;
                            String regdate = aqPMSel.id(R.id.etregdate).getText().toString();

                            if (regdate != null && regdate.trim().length() > 0) {
                                daysdiff = DateTimeUtil.getDaysBetweenDates(regdate, lmporadd);

                                int daysdifffromCUrr = DateTimeUtil
                                        .getDaysBetweenDates(DateTimeUtil.getTodaysDate(), lmporadd);

                                if (daysdiff <= 30) {
                                    Toast.makeText(activity,
                                            getResources().getString(R.string.date_val_3_toast),
                                            Toast.LENGTH_LONG).show();
                                    aqPMSel.id(R.id.etlmpdate).text("");
                                    aqPMSel.id(R.id.etgest).text("");
                                    aqPMSel.id(R.id.etMotherAdd).text("");
                                    aqPMSel.id(R.id.imgclearlmp).gone();//20Aug2019 Arpitha
                                } else if (daysdifffromCUrr > 280) {
                                    Toast.makeText(activity,
                                            getResources().getString(R.string.date_val_4_toast),
                                            Toast.LENGTH_LONG).show();
                                    aqPMSel.id(R.id.etlmpdate).text("");
                                    aqPMSel.id(R.id.etgest).text("");
                                    aqPMSel.id(R.id.etMotherAdd).text("");
                                    aqPMSel.id(R.id.imgclearlmp).gone();//20Aug2019 Arpitha
                                } else {
                                    aqPMSel.id(R.id.etMotherAdd).text(str);
                                    calculateEddGestation("lmp");
                                    aqPMSel.id(R.id.imgclearlmp).visible();//20Aug2019 Arpitha
                                }
                            } else
                                Toast.makeText(activity, getResources().getString(R.string.plz_enter_reg_dt),
                                        Toast.LENGTH_LONG).show();
                        } else if (aqPMSel.id(R.id.rdbMother).isChecked()) {

                            int daysdiff = DateTimeUtil
                                    .getDaysBetweenDates(aqPMSel.id(R.id.etregdate).getText().toString(), str);
                            if (daysdiff > 365) {// Arpitha 28Jun2018 - 23Dec2019 Arpitha - allow max 1 year

                                Toast.makeText(activity, (getResources().getString(R.string.datecannotbeafteroneyearfromcurrentdate)), // 11July2018
                                        // Arpitha
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etMotherAdd).text("");
                            } else
                                aqPMSel.id(R.id.etMotherAdd).text(str);
                        } else if (aqPMSel.id(R.id.rdbchild).isChecked()) {

                            int daysdiff = DateTimeUtil
                                    .getDaysBetweenDates(aqPMSel.id(R.id.etregdate).getText().toString(), str);
                            if (daysdiff > 5844) {// Arpitha 28Jun2018

                                Toast.makeText(activity, (getResources().getString(R.string.date_val_7_toast)), // 11July2018
                                        // Arpitha
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etMotherAdd).text("");
                            } else
                                aqPMSel.id(R.id.etMotherAdd).text(str);
                        }
                    } else {
                        if (isRegDate) {
                            aqPMSel.id(R.id.etregdate).text("");
                            aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                            Toast.makeText(activity,
                                    getResources().getString(R.string.reg_date_cannot_greater_current_date),
                                    Toast.LENGTH_LONG).show();
                            aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha
                        } else {
                            aqPMSel.id(R.id.etMotherAdd).text("");
                            aqPMSel.id(R.id.imgclearlmp).gone();//20AUg2019 Arpitha
                            aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                            if (aqPMSel.id(R.id.rdbPreg).isChecked()) {
                                Toast.makeText(activity,
                                        getResources()
                                                .getString(R.string.lmp_date_time_cannot_be_after_curre_datetime), // 11July2018

                                        Toast.LENGTH_LONG).show();

                                aqPMSel.id(R.id.etlmpdate).text("");
                                aqPMSel.id(R.id.etgest).text("");
                            } else {
                                Toast.makeText(activity,
                                        getResources()
                                                .getString(R.string.del_date_cannot_be_after_current_date_time), // 11July2018
                                        Toast.LENGTH_LONG).show();
                            }

                        }
                    }
                } else {
                    if (!isRegDate) {
                        int days1 = 0;
                        if (aqPMSel.id(R.id.etregdate).getText().toString().trim().length() > 0)
                            days1 = DateTimeUtil.getDaysBetweenDates(str,
                                    aqPMSel.id(R.id.etregdate).getText().toString());

                        if (days1 > 0 && days1 < 273) {
                            aqPMSel.id(R.id.etMotherAdd).text(str);
                            calculateEddGestation("edd");
                            aqPMSel.id(R.id.imgclearlmp).visible();//20Aug2019 Arpitha
                        } else {
                            if (days1 > 280) {
                                aqPMSel.id(R.id.etMotherAdd).text("");
                                aqPMSel.id(R.id.imgclearlmp).gone();//21Aug2019 Arpitha
                                aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                                Toast.makeText(activity, getResources().getString(R.string.date_val_6_toast),

                                        Toast.LENGTH_LONG).show();

                                aqPMSel.id(R.id.etlmpdate).text("");
                                aqPMSel.id(R.id.etgest).text("");

                            } else {
                                aqPMSel.id(R.id.etMotherAdd).text("");
                                aqPMSel.id(R.id.imgclearlmp).gone();//21Aug2019 Arpitha
                                aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                                Toast.makeText(activity, getResources().getString(R.string.date_val_5_toast),
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etlmpdate).text("");
                                aqPMSel.id(R.id.etgest).text("");
                            }
                        }
                    } else {
                        days = DateTimeUtil.getDaysBetweenDates(DateTimeUtil.getTodaysDate(), str);
                        if (days >= 0) {

                            if (days > 90) {
                                aqPMSel.id(R.id.etregdate).text("");
                                aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                                Toast.makeText(activity, getResources().getString(R.string.date_val_2_toast),

                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha
                            } else {
                                aqPMSel.id(R.id.etregdate).text(str);
                            }

                        } else {
                            aqPMSel.id(R.id.etregdate).text("");
                            aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                            Toast.makeText(activity, getResources().getString(R.string.reg_date_cannot_greater_current_date),

                                    Toast.LENGTH_LONG).show();
                            aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha
                        }
                    }
                }

            }

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }*/

    /**
     * This is a Recursive method that traverse through the group and subgroup
     * of view Sets text, assigns clickListners
     *
     * @throws Exception
     */
    private ArrayList<View> initializeScreen(View v) throws Exception {
        if (!(v instanceof ViewGroup)) {
            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);

            // TextView - Assign Labels
            if (v.getClass() == TextView.class || v.getClass() == RadioButton.class) {
                // aqPMSel.id(v.getId()).text(getSS(aqPMSel.id(v.getId()).getText().toString()));
            }

            if (v.getClass() == RadioButton.class) {
                aqPMSel.id(v.getId()).clicked(this, "commonClick");
            }

            return viewArrayList;
        }

        ArrayList<View> result = new ArrayList<View>();

        ViewGroup viewGroup = (ViewGroup) v;
        for (int i = 0; i < viewGroup.getChildCount(); i++) {

            View child = viewGroup.getChildAt(i);

            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);
            viewArrayList.addAll(initializeScreen(child));

            result.addAll(viewArrayList);
        }
        return result;
    }


    //	set initial view of the pregnant or mother pop up
    private void setInitialView() {
        aqPMSel.id(R.id.rdbPreg).checked(true);
        aqPMSel.id(R.id.tvMotherAddHeading).text(getResources().getString(R.string.tvPregLmpHeading));
        aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());
        aqPMSel.id(R.id.radLMP).checked(true);
        aqPMSel.id(R.id.etregdate).getEditText().setKeyListener(null);
        aqPMSel.id(R.id.etMotherAdd).getEditText().setKeyListener(null);
        aqPMSel.id(R.id.lllmpdate).visible();
        aqPMSel.id(R.id.llgest).visible();
        aqPMSel.id(R.id.vHLine4).visible();
        aqPMSel.id(R.id.vHLine6).visible();
        aqPMSel.id(R.id.llnote).visible();

        //13Apr2021 Bindu - set here cos not translated from xml
        aqPMSel.id(R.id.anclbl).text(getResources().getString(R.string.anclbl));
        aqPMSel.id(R.id.txtpnclbl).text(getResources().getString(R.string.pnclbl));
        aqPMSel.id(R.id.lmp_date).text(getResources().getString(R.string.tvedd));
        aqPMSel.id(R.id.txtgest).text(getResources().getString(R.string.tvGestationalageL));
        aqPMSel.id(R.id.txtchildreglbl).text(getResources().getString(R.string.childreg));
        aqPMSel.id(R.id.tvFirstPregHeading).text(getResources().getString(R.string.tvFirstPregHeading));
        aqPMSel.id(R.id.reg_date).text(getResources().getString(R.string.reg_date));
        aqPMSel.id(R.id.instructions).text(getResources().getString(R.string.instruction));
        aqPMSel.id(R.id.regdt).text(getResources().getString(R.string.reg_date));
        aqPMSel.id(R.id.txt1).text(getResources().getString(R.string.date_val_0));
        aqPMSel.id(R.id.txt2).text(getResources().getString(R.string.date_val_1));
        aqPMSel.id(R.id.txt3).text(getResources().getString(R.string.date_val_2));
        aqPMSel.id(R.id.txtlmp).text(getResources().getString(R.string.tvPregLmpHeading));
        aqPMSel.id(R.id.txt4).text(getResources().getString(R.string.date_val_3));
        aqPMSel.id(R.id.txt5).text(getResources().getString(R.string.date_val_4));
        aqPMSel.id(R.id.txt6).text(getResources().getString(R.string.date_val_5));
        aqPMSel.id(R.id.txt7).text(getResources().getString(R.string.date_val_6));
        aqPMSel.id(R.id.txt8).text(getResources().getString(R.string.date_val_7));
        aqPMSel.id(R.id.radLMP).text(getResources().getString(R.string.tvlmp));
        aqPMSel.id(R.id.radEDD).text(getResources().getString(R.string.tvedd));
//22May2021 Bindu set adol reg lbl
        aqPMSel.id(R.id.adolreg).text(getResources().getString(R.string.adolescentreg));

    }

    // capture EDD
    private void setEDD() throws Exception {
        aqPMSel.id(R.id.tvMotherAddHeading).text(getResources().getString(R.string.tvedd));
        aqPMSel.id(R.id.etMotherAdd).text("");
        aqPMSel.id(R.id.imgclearlmp).gone();//20AUg2019 Arpitha
        aqPMSel.id(R.id.lmp_date).text(getResources().getString(R.string.tvlmp));
        aqPMSel.id(R.id.etlmpdate).text("");
        aqPMSel.id(R.id.etgest).text("");
        aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());
    }

    // capture LMP
    private void setLMP() throws Exception {
        aqPMSel.id(R.id.tvMotherAddHeading).text(getResources().getString(R.string.tvlmp));
        aqPMSel.id(R.id.etMotherAdd).text("");
        aqPMSel.id(R.id.imgclearlmp).gone();//20AUg2019 Arpitha
        aqPMSel.id(R.id.lmp_date).text(getResources().getString(R.string.tvedd));
        aqPMSel.id(R.id.etlmpdate).text("");
        aqPMSel.id(R.id.etgest).text("");
    }


    /**
     * Common method invokes when any button clicked This method handles
     * Edittext, save clicks. Validate mandatory fields
     */
    public void commonClick(View v) throws Exception {
        switch (v.getId()) {
            case R.id.rdbPreg: {
                aqPMSel.id(R.id.rdbMother).checked(false);
                aqPMSel.id(R.id.tvFirstPregHeading).enabled(true);
                aqPMSel.id(R.id.rdbchild).checked(false);
                aqPMSel.id(R.id.rdbAdol).checked(false);//Ramesh 13-may2021
                aqPMSel.id(R.id.chkFirstPreg).enabled(true);
                aqPMSel.id(R.id.radEDDorLmp).visible();
                //27Nov2018 - Bindu - radiogrp clear chk to reset radiobtn
                aqPMSel.id(R.id.vHLine5).visible();
                aqPMSel.id(R.id.etMotherAdd).text("");
                aqPMSel.id(R.id.radLMP).checked(true);
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.tvPregLmpHeading)));
                // 27Jun2018 Arpitha
                aqPMSel.id(R.id.etregdate).enabled(true);
                aqPMSel.id(R.id.etregdate).textColor(getResources().getColor(R.color.black));
//				aqPMSel.id(R.id.llregdate).background(R.drawable.editext_none);
                aqPMSel.id(R.id.lllmpdate).visible();
                aqPMSel.id(R.id.llgest).visible();
                aqPMSel.id(R.id.vHLine4).visible();
                aqPMSel.id(R.id.vHLine6).visible();

                aqPMSel.id(R.id.llMotherAdd).visible();
                aqPMSel.id(R.id.llFirstPreg).visible();
                aqPMSel.id(R.id.llnote).visible();
                aqPMSel.id(R.id.vHLine5).visible();
//				19Nov2019 Arpitha
                aqPMSel.id(R.id.llancinstruction).visible();
                aqPMSel.id(R.id.llpncinstruction).gone();

                break;
            }
            case R.id.rdbMother: {
                aqPMSel.id(R.id.rdbPreg).checked(false);
                aqPMSel.id(R.id.rdbchild).checked(false);
                aqPMSel.id(R.id.rdbAdol).checked(false);//Ramesh 13-may2021
                //27Nov2018 - Bindu radiogrp clear chk to reset radiobtn
                radGrpEDDorLmp.clearCheck();
                aqPMSel.id(R.id.radLMP).checked(false);
                aqPMSel.id(R.id.radEDD).checked(false);
                aqPMSel.id(R.id.tvFirstPregHeading).enabled(false);
                aqPMSel.id(R.id.chkFirstPreg).enabled(false);
                aqPMSel.id(R.id.chkFirstPreg).checked(false);
                aqPMSel.id(R.id.radEDDorLmp).gone();
                aqPMSel.id(R.id.vHLine5).gone();
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.deldate)));
                aqPMSel.id(R.id.etMotherAdd).text("");
                // 27Jun2018 Arpitha
                aqPMSel.id(R.id.etregdate).enabled(false);
                aqPMSel.id(R.id.etregdate).textColor(R.color.fontcolor_disable);
                //	aqPMSel.id(R.id.llregdate).background(R.drawable.edittext_disable);
                aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());//21March2019 Arpitha
                aqPMSel.id(R.id.lllmpdate).gone();
                aqPMSel.id(R.id.llgest).gone();
                aqPMSel.id(R.id.vHLine4).gone();
                aqPMSel.id(R.id.vHLine6).gone();

                aqPMSel.id(R.id.llMotherAdd).visible();
                aqPMSel.id(R.id.llFirstPreg).visible();
                aqPMSel.id(R.id.llnote).visible();
                aqPMSel.id(R.id.vHLine5).visible();
                //				19Nov2019 Arpitha
                aqPMSel.id(R.id.llancinstruction).gone();
                aqPMSel.id(R.id.llpncinstruction).visible();
                aqPMSel.id(R.id.llFirstPreg).gone();
                aqPMSel.id(R.id.imgclearlmp).visible();
                break;
            }
            case R.id.radEDD: {

                //	aqPMSel.id(R.id.radEDD).checked(true); //27Nov2018 - Bindu- comment the line
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.deldate)));
                aqPMSel.id(R.id.etMotherAdd).text("");
                aqPMSel.id(R.id.lmp_date).text((getResources().getString(R.string.tvlmp)));// 28Jun2018
                // Arpitha
                // 11July2018 Arpitha
                aqPMSel.id(R.id.etlmpdate).text("");
                aqPMSel.id(R.id.etgest).text("");
                break;
            }
            case R.id.rdbchild: {
                aqPMSel.id(R.id.rdbPreg).checked(false);
                aqPMSel.id(R.id.rdbMother).checked(false);
                aqPMSel.id(R.id.rdbAdol).checked(false);//Ramesh 13-may2021

                //27Nov2018 - Bindu radiogrp clear chk to reset radiobtn
                radGrpEDDorLmp.clearCheck();//Arpitha2021
                aqPMSel.id(R.id.radLMP).checked(false);
                aqPMSel.id(R.id.radEDD).checked(false);
                aqPMSel.id(R.id.tvFirstPregHeading).enabled(false);
                aqPMSel.id(R.id.chkFirstPreg).enabled(false).checked(false);
                aqPMSel.id(R.id.radEDDorLmp).gone();
                aqPMSel.id(R.id.vHLine5).gone();
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.deldate)));
                aqPMSel.id(R.id.etMotherAdd).text("");
                aqPMSel.id(R.id.llMotherAdd).gone();
                aqPMSel.id(R.id.llFirstPreg).gone();
                aqPMSel.id(R.id.llnote).gone();
                // 27Jun2018 Arpitha
                aqPMSel.id(R.id.etregdate).enabled(false);
                aqPMSel.id(R.id.etregdate).textColor(R.color.fontcolor_disable);
                //	aqPMSel.id(R.id.llregdate).background(R.drawable.edittext_disable);
                aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());//21March2019 Arpitha
                aqPMSel.id(R.id.lllmpdate).gone();
                aqPMSel.id(R.id.llgest).gone();
                aqPMSel.id(R.id.vHLine4).gone();
                aqPMSel.id(R.id.vHLine6).gone();
                break;
            }
            case R.id.radLMP: {
                //	aqPMSel.id(R.id.radLMP).checked(true); //27Nov2018 - Bindu- comment the line
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.tvlmp)));
                aqPMSel.id(R.id.etMotherAdd).text("");
                aqPMSel.id(R.id.lmp_date).text((getResources().getString(R.string.tvedd)));// 28Jun2018
                // Arpitha

                // 11July2018 Arpitha
                aqPMSel.id(R.id.etlmpdate).text("");
                aqPMSel.id(R.id.etgest).text("");
                break;
            }

            case R.id.gridView1:
                break;
            case R.id.rdbAdol://Ramesh 13-may-2021
                aqPMSel.id(R.id.rdbPreg).checked(false);
                aqPMSel.id(R.id.rdbMother).checked(false);
                aqPMSel.id(R.id.rdbchild).checked(false);

                radGrpEDDorLmp.clearCheck();//Arpitha2021
                aqPMSel.id(R.id.radLMP).checked(false);
                aqPMSel.id(R.id.radEDD).checked(false);
                aqPMSel.id(R.id.tvFirstPregHeading).enabled(false);
                aqPMSel.id(R.id.chkFirstPreg).enabled(false).checked(false);
                aqPMSel.id(R.id.radEDDorLmp).gone();
                aqPMSel.id(R.id.vHLine5).gone();
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.deldate)));
                aqPMSel.id(R.id.etMotherAdd).text("");
                aqPMSel.id(R.id.llMotherAdd).gone();
                aqPMSel.id(R.id.llFirstPreg).gone();
                aqPMSel.id(R.id.llnote).gone();
                aqPMSel.id(R.id.etregdate).enabled(false);
                aqPMSel.id(R.id.etregdate).textColor(R.color.fontcolor_disable);
                aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());//21March2019 Arpitha
                aqPMSel.id(R.id.lllmpdate).gone();
                aqPMSel.id(R.id.llgest).gone();
                aqPMSel.id(R.id.vHLine4).gone();
                aqPMSel.id(R.id.vHLine6).gone();
                break;
            default: {
                break;
            }
        }
    }

    private void validateAndRegister() throws Exception {
        try {
            Intent nextScreen;
            String lmpOrAdd = aqPMSel.id(R.id.etMotherAdd).getText().toString();
            String regDate = aqPMSel.id(R.id.etregdate).getText().toString();

            if ((lmpOrAdd != null && lmpOrAdd.length() > 0) && (regDate != null && regDate.length() > 0)) {

                if (aqPMSel.id(R.id.rdbPreg).isChecked()) {
                    if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0) {
                        int days = DateTimeUtil.getDaysBetweenDates(regDate, lmpOrAdd);
                        int daysdiff = DateTimeUtil
                                .getDaysBetweenDates(DateTimeUtil.getTodaysDate(), lmpOrAdd);
                        if (aqPMSel.id(R.id.radEDD).isChecked()) {
                            if (days >= 0) {
                                wantToCloseDialog = false;
                                Toast.makeText(getApplicationContext(),
                                        getResources().getString(R.string.date_val_5_toast),

                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etMotherAdd).text("");
                            } else if (noOfDays < 30) {
                                wantToCloseDialog = false;
                                Toast.makeText(getApplicationContext(),
                                        getResources().getString(R.string.gestation_must_be_30days),
                                        Toast.LENGTH_LONG).show();
                            } else if (noOfDays > 280) {
                                wantToCloseDialog = false;
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.gest_280),
                                        Toast.LENGTH_LONG).show();
                            } else {
                                wantToCloseDialog = true;
                                woman = new tblregisteredwomen();
                                nextScreen = new Intent(getApplicationContext(), RegistrationActivity.class);
                                nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                nextScreen.putExtra("adolAadhar",adolDetails.getRegAdolAadharNo());//01Sep2021 Arpitha
                                woman.setRegPregnantorMother(1);
                                int gravida = 0;
                                if (aqPMSel.id(R.id.chkFirstPreg).isChecked())
                                    gravida = 1;
                                woman.setRegGravida(gravida);
                                woman.setRegRegistrationDate(aqPMSel.id(R.id.etregdate).getText().toString());
                                woman.setRegEDD(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                                nextScreen.putExtra("woman", woman);
                                startActivity(nextScreen);
                            }
                        } else {
                            if (days < 30) {
                                wantToCloseDialog = false;
                                Toast.makeText(getApplicationContext(),
                                        getResources().getString(R.string.date_val_3_toast),

                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etlmpdate).text("");
                                aqPMSel.id(R.id.etMotherAdd).text("");
                            } else if (daysdiff > 280) {

                                wantToCloseDialog = false;
                                Toast.makeText(getApplicationContext(),
                                        activity.getResources().getString(R.string.date_val_4_toast),
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etlmpdate).text("");
                                aqPMSel.id(R.id.etMotherAdd).text("");
                            } else {
                                wantToCloseDialog = true;
                                woman = new tblregisteredwomen();
                                nextScreen = new Intent(activity, RegistrationActivity.class);
                                nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                nextScreen.putExtra("adolAadhar",adolDetails.getRegAdolAadharNo());//01Sep2021 Arpitha


                                woman.setRegPregnantorMother(1);
                                int gravida = 0;
                                if (aqPMSel.id(R.id.chkFirstPreg).isChecked())
                                    gravida = 1;
                                woman.setRegGravida(gravida);
                                woman.setRegRegistrationDate(aqPMSel.id(R.id.etregdate).getText().toString());
                                if (aqPMSel.id(R.id.radEDD).isChecked())
                                    woman.setRegEDD(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                                else
                                    woman.setRegLMP(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                                nextScreen.putExtra("woman", woman);
                                startActivity(nextScreen);
                            }
                        }

                    }
                } else if (aqPMSel.id(R.id.rdbMother).isChecked()) {
                    if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0) {
                        int days = DateTimeUtil.getDaysBetweenDates(regDate, lmpOrAdd);
                        if (days < 0) {
                            wantToCloseDialog = false;
                            Toast.makeText(activity, (getResources().getString(R.string.m077)),
                                    Toast.LENGTH_LONG).show();
                            // } else if (days > 549) {
                        } else if (days > 5844) {// Arpitha
                            // 28Jun2018
                            wantToCloseDialog = false;
                            Toast.makeText(activity,
                                    (getResources().getString(R.string.date_val_7_toast)), // 11july2018
                                    // Arpitha
                                    Toast.LENGTH_LONG).show();
                        } else {
                            wantToCloseDialog = true;
                            woman = new tblregisteredwomen();
                            nextScreen = new Intent(activity, RegistrationActivity.class);
                            nextScreen.putExtra("adolAadhar",adolDetails.getRegAdolAadharNo());//01Sep2021 Arpitha
                            woman.setRegPregnantorMother(2);
                            woman.setRegADDate(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                            woman.setRegRegistrationDate(aqPMSel.id(R.id.etregdate).getText().toString());
                            nextScreen.putExtra("woman", woman);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(nextScreen);
                        }
                    }
                } else if (aqPMSel.id(R.id.rdbchild).isChecked()) {
                    if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0) {
                        int days = DateTimeUtil.getDaysBetweenDates(regDate, lmpOrAdd);
                        if (days < 0) {
                            wantToCloseDialog = false;
                            Toast.makeText(activity, (getResources().getString(R.string.m077)),
                                    Toast.LENGTH_LONG).show();
                            // } else if (days > 549) {
                        } else if (days > 5844) {// Arpitha
                            // 28Jun2018
                            wantToCloseDialog = false;
                            Toast.makeText(activity,
                                    (getResources().getString(R.string.date_val_7_toast)), // 11july2018
                                    // Arpitha
                                    Toast.LENGTH_LONG).show();
                        } else {
                            wantToCloseDialog = true;
                            woman = new tblregisteredwomen();
                            nextScreen = new Intent(activity, ChildRegistrationActivity.class);
//							woman.setRegPregnantorMother(1);
                            woman.setRegADDate(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                            woman.setRegRegistrationDate(aqPMSel.id(R.id.etregdate).getText().toString());
                            nextScreen.putExtra("woman", woman);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(nextScreen);
                        }
                    }
                } else if (aqPMSel.id(R.id.rdbAdol).isChecked()) { //Ramesh 13-may-2021
                    if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0) {
                        int days = DateTimeUtil.getDaysBetweenDates(regDate, lmpOrAdd);
                        if (days < 0) {
                            wantToCloseDialog = false;
                            Toast.makeText(activity, (getResources().getString(R.string.m077)),
                                    Toast.LENGTH_LONG).show();
                            // } else if (days > 549) {
                        } else if (days > 5844) {// Arpitha
                            // 28Jun2018
                            wantToCloseDialog = false;
                            Toast.makeText(activity,
                                    (getResources().getString(R.string.date_val_7_toast)), // 11july2018
                                    // Arpitha
                                    Toast.LENGTH_LONG).show();
                        } else {
                            wantToCloseDialog = true;
                            tblAdolReg adolReg = new tblAdolReg();
                            nextScreen = new Intent(activity, AdolRegistration.class);
                            adolReg.setRecordCreatedDate(aqPMSel.id(R.id.etregdate).getText().toString());
                            nextScreen.putExtra("AdolReg", adolReg);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(nextScreen);
                        }
                    }
                }
            } else {
                wantToCloseDialog = false;
                if (aqPMSel.id(R.id.rdbPreg).isChecked()) {
                    if (aqPMSel.id(R.id.etMotherAdd).getText().toString().trim().length() <= 0) {
                        aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                        if (aqPMSel.id(R.id.radEDD).isChecked())
                            Toast.makeText(activity,
                                    (activity.getResources().getString(R.string.m170)),
                                    Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(activity,
                                    activity.getResources().getString(R.string.m074),
                                    Toast.LENGTH_LONG).show();
                    } else if (aqPMSel.id(R.id.etregdate).getText().toString().trim().length() <= 0) {
                        aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                        Toast.makeText(getApplicationContext(),
                                (activity.getResources().getString(R.string.m157)),
                                Toast.LENGTH_LONG).show();
                    }
                } else if (aqPMSel.id(R.id.rdbMother).isChecked()) {
                    if (aqPMSel.id(R.id.etMotherAdd).getText().toString().trim().length() <= 0) {
                        aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                        Toast.makeText(activity,
                                (activity.getResources().getString(R.string.m066)),
                                Toast.LENGTH_LONG).show();
                    } else if (aqPMSel.id(R.id.etregdate).getText().toString().trim().length() <= 0) {
                        aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                        Toast.makeText(activity,
                                activity.getResources().getString(R.string.m157),
                                Toast.LENGTH_LONG).show();
                    }
                } else if (aqPMSel.id(R.id.rdbchild).isChecked()) {
                    wantToCloseDialog = true;
//					if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0)
                    {
//						int days = DateTimeUtil.getDaysBetweenDates(regDate, lmpOrAdd);
						/*if (days < 0) {
							wantToCloseDialog = false;
							Toast.makeText(activity, (getResources().getString(R.string.m077)),
									Toast.LENGTH_LONG).show();
							// } else if (days > 549) {
						} else if (days > 5844) {// Arpitha
							// 28Jun2018
							wantToCloseDialog = false;
							Toast.makeText(activity,
									(getResources().getString(R.string.date_val_7_toast)), // 11july2018
									// Arpitha
									Toast.LENGTH_LONG).show();
						} else {*/
                        wantToCloseDialog = true;
                        woman = new tblregisteredwomen();
                        nextScreen = new Intent(activity, ChildRegistrationActivity.class);
                        woman.setRegPregnantorMother(1);
                        woman.setRegADDate(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                        woman.setRegRegistrationDate(aqPMSel.id(R.id.etregdate).getText().toString());
                        nextScreen.putExtra("woman", woman);
                        nextScreen.putExtra("globalState", activity.getIntent().getBundleExtra("globalState"));
                        startActivity(nextScreen);
//						}
                    }
                } else if (aqPMSel.id(R.id.rdbAdol).isChecked()) { //Ramesh 13-may-2021

                    wantToCloseDialog = true;
                    tblAdolReg adolReg = new tblAdolReg();
                    nextScreen = new Intent(activity, AdolRegistration.class);
                    adolReg.setRecordCreatedDate(aqPMSel.id(R.id.etregdate).getText().toString());
                    nextScreen.putExtra("AdolReg", adolReg);
                    nextScreen.putExtra("globalState", activity.getIntent().getBundleExtra("globalState"));
                    startActivity(nextScreen);


                }
            }

        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

}