package com.sc.stmansi.adolescent;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.R;
import com.sc.stmansi.adolescent.training.TrainingCreation;
import com.sc.stmansi.common.AlertDialogUtil;
import com.sc.stmansi.common.CommonDatePickerFragment;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.common.MultiSelectionSpinner;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.configuration.SyncState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.repositories.AdolescentRepository;
import com.sc.stmansi.repositories.CampaignRepository;
import com.sc.stmansi.repositories.FacilityRepository;
import com.sc.stmansi.repositories.TransactionHeaderRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblCampDetails;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

public class AdolStartTraining extends AppCompatActivity implements View.OnTouchListener, DatePickerDialog.OnDateSetListener, View.OnClickListener, TimePickerDialog.OnTimeSetListener {

    public static AQuery aqView;
    public static MultiSelectionSpinner msProObj;
    private DatabaseHelper databaseHelper;
    private AppState appState;
    private SyncState syncState;
    private EditText etCampCrtStartTime;
    private EditText etCampCrtCreatedDate;
    private String userName;
    private String campID;
    private int transID;
    private SearchableSpinner spnFacility, spnTrainingName;
    //    15MAy2021 Bindu
    private ImageButton imgbtncancel, imgbtnsave;
    int hour, minute;
    private EditText currentEditTextForTime;
    Map<String, Integer> facilities;
    private TblInstusers user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adolstarttraining);
        try {
            Bundle bundle = getIntent().getBundleExtra("globalState");
            if (bundle != null) {
                appState = bundle.getParcelable("appState");
                syncState = bundle.getParcelable("syncState");
            } else {
                appState = new AppState();
            }
            campID = getIntent().getStringExtra("campID");
            transID = getIntent().getIntExtra("transId", 0);

            Initalize();
            //23May2021 Bindu set user
            UserRepository userRepository = new UserRepository(databaseHelper);
            user = userRepository.getOneAuditedUser(appState.ashaId);

//            initializeScreen(aqView.id(R.id.rladoltraining).getView()); //15May2021 Bindu
            getFacilitiesData();
            getData();
            getSupportActionBar().hide();

            msProObj.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (spnTrainingName.getSelectedItemPosition() == 0) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseselecttrainingname), Toast.LENGTH_LONG).show();
                    }
                    return false;
                }
            });

            spnFacility.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (i != 0) {
                        AdolescentRepository adolescentRepository = new AdolescentRepository(databaseHelper);
                        Integer villagecode = facilities.get(spnFacility.getSelectedItem().toString());
                        if (adolescentRepository.getAdolsbyFacility(String.valueOf(villagecode)).size() == 0) {
                            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.noadolescentregisterinthisfacility), AdolStartTraining.this);
                            spnFacility.setSelection(0);
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            spnTrainingName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    String[] topic = new String[0];
                    aqView.id(R.id.trTrainingName).getView().setVisibility(View.GONE);
                    aqView.id(R.id.llprogramobjective).getView().setVisibility(View.VISIBLE);
                    aqView.id(R.id.llProObjOthers).getView().setVisibility(View.GONE);
                    aqView.id(R.id.etProObjOthers).getEditText().setText("");
                    aqView.id(R.id.etCampName).text("");
                    if (i == 0) {
                        topic = getResources().getStringArray(R.array.trainingnothing);
                    } else if (i == 1) {
                        topic = getResources().getStringArray(R.array.traininggrowthinadolescent);
                    } else if (i == 2) {
                        topic = getResources().getStringArray(R.array.trainingnutritionandanemia);
                    } else if (i == 3) {
                        topic = getResources().getStringArray(R.array.trainingmenstruationandreproductive);
                    } else if (i == 4) {
                        topic = getResources().getStringArray(R.array.trainingmalereproductivesystem);
                    } else if (i == 5) {
                        topic = getResources().getStringArray(R.array.trainingresponsiblerelatioinshipandbehavior);
                    } else if (i == 6) {
                        topic = getResources().getStringArray(R.array.trainingresponsiblerelatioinshipandbehavioryears);
                    } else if (i == 7) {
                        topic = getResources().getStringArray(R.array.trainingharassmentandabuse);
                    } else if (i == 8) {
                        topic = getResources().getStringArray(R.array.traininginfectionofreproductive);
                    } else if (i == 9) {
                        topic = getResources().getStringArray(R.array.trainingmarriageandfamily);
                    } else if (i == 10) {
                        topic = getResources().getStringArray(R.array.traininggivingthechildrenaheadstart);
                    } else if (i == 11) {
                        topic = getResources().getStringArray(R.array.trainingcontraceptions);
                    } else if (i == 12) {
                        //others
                        aqView.id(R.id.trTrainingName).getView().setVisibility(View.VISIBLE);
                        aqView.id(R.id.llProObjOthers).getView().setVisibility(View.VISIBLE);
                        aqView.id(R.id.llprogramobjective).getView().setVisibility(View.GONE);
                    }
                    if (topic.length != 0)
                        msProObj.setItems(topic);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            imgbtnsave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (validationName() && validationObjective() && validationDate() && validationStartTime()
                                && validationSchemaType() && validationCCAcompany() && validationFacility()) {
                            displayDialogSave();
                        }
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                        
                    }
                }


            });
            imgbtncancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    displayDialogExit();
                }
            });

//            15May2021 Bindu
            aqView.id(R.id.toolbarBtnHome).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   displayDialogExit();
                }
            });
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            
        }
    }

    private void displayDialogExit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(AdolStartTraining.this);
        builder.setMessage(getResources().getString(R.string.exit));
        builder.setCancelable(false);
        builder.setNegativeButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(AdolStartTraining.this, AdolescentHome.class);
                intent.putExtra("tabItem", 1);
                intent.putExtra("globalState", prepareBundle());
                startActivity(intent);
            }
        }).setPositiveButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    private void displayDialogSave() {
        AlertDialog.Builder bldr = new AlertDialog.Builder(AdolStartTraining.this);
        bldr.setMessage(getResources().getString(R.string.trainingregconfirmation));
        bldr.setCancelable(false);
        bldr.setNegativeButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                try {
                    saveData();
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                    crashlytics.log(e.getMessage());
                }
            }
        }).setPositiveButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    private void saveData() {
        try {
            CampaignRepository campaignRepository = new CampaignRepository(databaseHelper);
            transID = TransactionHeaderRepository.iCreateNewTransNew(user.getUserId(), databaseHelper); //23May2021 Bindu set userid
            tblCampDetails tblCampDetails = new tblCampDetails();
            campID = campaignRepository.getlastCampID();
            tblCampDetails.setUserId(user.getUserId());
            tblCampDetails.setTblCampID(campID);
            if (spnTrainingName.getSelectedItemPosition() == 12) {
                tblCampDetails.setTblCampName(aqView.id(R.id.etCampName).getEditText().getText().toString());
            } else {
                tblCampDetails.setTblCampName(String.valueOf(aqView.id(R.id.spnCampTrainingName).getSpinner().getSelectedItemPosition()));
            }
            String starttime = aqView.id(R.id.etStartTime).getEditText().getText().toString();
            @SuppressLint("SimpleDateFormat") SimpleDateFormat _24sdf = new SimpleDateFormat("HH:mm");
//                                        @SuppressLint("SimpleDateFormat") SimpleDateFormat _12sdf = new SimpleDateFormat("hh:mm a");
//                                        String sTime_12 = _12sdf.format(starttime);
//                                        String startTime_24 = _24sdf.format(sTime_12);
            if (starttime.contains("AM"))
                starttime.replace(" AM", "");
            if (starttime.contains("PM"))
                starttime.replace(" PM", "");
            tblCampDetails.setTblCampDateTime(aqView.id(R.id.etCampDate).getEditText().getText().toString() + " " + starttime);
            tblCampDetails.setTblCampDesc(aqView.id(R.id.etCampDesc).getEditText().getText().toString());
            tblCampDetails.setTblCampSchemaType(aqView.id(R.id.spnCampSchemaType).getSpinner().getSelectedItemPosition());
            tblCampDetails.setTblCampCreatedBy(user.getUserName());
            tblCampDetails.setTblCampisActive(1);
            tblCampDetails.setTblCampSummary("");
            tblCampDetails.setTblCampAbsent("");
            tblCampDetails.setTblCampOutcome("");
            RadioButton rbCCAccompanyYes = findViewById(R.id.rb_ccAccompanyYes);
            if (rbCCAccompanyYes.isChecked()) {
                tblCampDetails.setTblCampCCAccompany("Yes");
            } else {
                tblCampDetails.setTblCampCCAccompany("No");
            }

            if (spnTrainingName.getSelectedItemPosition() == 12) {
                tblCampDetails.setTblCampProObjOthers(aqView.id(R.id.etProObjOthers).getEditText().getText().toString());
                tblCampDetails.setTblCampProObjective("");
            } else {
                tblCampDetails.setTblCampProObjOthers("");
                String ProObjs = msProObj.getSelectedIndicies().toString();
                if (ProObjs.contains("["))
                    ProObjs = ProObjs.replace("[", "");
                if (ProObjs.contains("]"))
                    ProObjs = ProObjs.replace("]", "");
                tblCampDetails.setTblCampProObjective(ProObjs);
            }

            Integer villageCode = facilities.get(aqView.id(R.id.spnCampFacility).getSpinner().getSelectedItem().toString());

            tblCampDetails.setTblCampFacilities(String.valueOf(villageCode));
            tblCampDetails.setTblCampPresent("");
            tblCampDetails.setTblCampEndTime("");
            tblCampDetails.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
            tblCampDetails.setTransId(transID);

            int result = campaignRepository.createCampaign(tblCampDetails);
            boolean add = TransactionHeaderRepository.iNewRecordTransNew(user.getUserId(), transID, "tblCampDetails", databaseHelper);
            if (result > 0 && add) {
                Intent intent = new Intent(getApplicationContext(), TrainingCreation.class);
                intent.putExtra("globalState", prepareBundle());
                intent.putExtra("campID", campID);
                startActivity(intent);
            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.erroroccurs), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            FirebaseCrashlytics firebaseCrashlytics = FirebaseCrashlytics.getInstance();
            firebaseCrashlytics.log(e.getMessage());
        }
    }

    private void getData() {
        try {
            if (transID == 0) {
                transID = TransactionHeaderRepository.iCreateNewTransNew(user.getUserId(), databaseHelper); //23May2021 Bindu chaneg to userid
            }

            String[] trainingName = getResources().getStringArray(R.array.adoltrainingname);
            ArrayAdapter<String> trainingNameAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.simple_spinner_dropdown_item, trainingName);
            spnTrainingName.setAdapter(trainingNameAdapter);

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private void Initalize() {
        try {
            databaseHelper = getHelper();
            aqView = new AQuery(this);

            etCampCrtCreatedDate = findViewById(R.id.etCampDate);
            etCampCrtCreatedDate.setOnTouchListener(this);
            etCampCrtStartTime = findViewById(R.id.etStartTime);
            etCampCrtStartTime.setText(_12hoursformate(DateTimeUtil.getCurrentTime()));
            etCampCrtStartTime.setOnTouchListener(this);
            aqView.id(R.id.etCampDate).getEditText().setText(DateTimeUtil.getTodaysDate().toString());

            aqView.id(R.id.toolbartxtTitle).getTextView().setText(R.string.addtraining);

            msProObj = findViewById(R.id.spnCampProgramObjective);
            spnTrainingName = findViewById(R.id.spnCampTrainingName);
            spnFacility = findViewById(R.id.spnCampFacility);
//            15MAy2021 Bindu
            imgbtncancel = findViewById(R.id.btnCampCrtCancel);
            imgbtnsave = findViewById(R.id.btnCampCrtCreate);
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            
        }
    }

    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        b.putParcelable("syncState", syncState);
        return b;
    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(getApplicationContext(), DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
        try {
            if (datePicker.isShown()) {
                String strseldate = null;
                strseldate = String.format("%02d", dayOfMonth) + "-" + String.format("%02d", month + 1) + "-"
                        + year;

                @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                Date seldate = format.parse(strseldate);
                if (seldate != null && seldate.after(new Date())) {
                    AlertDialogUtil.displayAlertMessage(
                            getResources().getString(R.string.date_cannot_af_currentdate)
                            , AdolStartTraining.this);
                    etCampCrtCreatedDate.setText(DateTimeUtil.getTodaysDate());
                } /*else if (seldate != null && seldate.before(new Date("01-01-2002"))) {
                    AlertDialogUtil.displayAlertMessage(
                            getResources().getString(R.string.date_cannot_bf_currentdate)
                            , getActivity());
                    etCampCrtCreatedDate.setText(DateTimeUtil.getTodaysDate());
                }*/ else {
                    etCampCrtCreatedDate.setText(strseldate);
                }
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (MotionEvent.ACTION_UP == motionEvent.getAction()) {
            try {
                switch (view.getId()) {
                    case R.id.etCampDate:
                        assignEditTextAndCallDatePicker(etCampCrtCreatedDate);
                        break;
                    case R.id.etStartTime:
//                        CommonTimePickerFragment.getCommonTime(this, 0, 0);
                        assignEditTextAndCallTimePicker(aqView.id(R.id.etStartTime).getEditText());
                }
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
                
            }
        }
        return false;
    }

    private boolean validationCCAcompany() {
        try {
            RadioButton rbAccYes = findViewById(R.id.rb_ccAccompanyYes);
            RadioButton rbAccNo = findViewById(R.id.rb_ccAccompanyNo);
            if (!(rbAccYes.isChecked()) && !(rbAccNo.isChecked())) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseselectccacompany), Toast.LENGTH_LONG).show();
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            
        }
        return false;
    }

    private String _12hoursformate(String currentTime) {
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
            final Date dateObj = sdf.parse(currentTime);

            String time = new SimpleDateFormat("hh:mm a").format(dateObj);
            return time;
        } catch (final ParseException e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            
            return "";
        }
    }

    private boolean validationObjective() {
        try {
            if (spnTrainingName.getSelectedItemPosition() != 12) {
                if (msProObj.getSelectedIndicies().size() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseselecttrainingobj), Toast.LENGTH_LONG).show();
                    return false;
                } else {
                    return true;
                }
            } else {
                if (aqView.id(R.id.etProObjOthers).getEditText().getText().length() == 0) {
                    aqView.id(R.id.etProObjOthers).getEditText().setError(getResources().getString(R.string.pleaseselecttrainingobj));
                    aqView.id(R.id.etProObjOthers).getEditText().requestFocus();
                    return false;
                } else {
                    aqView.id(R.id.etProObjOthers).getEditText().setError(null);
                    return true;
                }
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            
        }
        return false;
    }

    private void getAshaData() {
        try {
            UserRepository userRepository = new UserRepository(databaseHelper);
            TblInstusers user = userRepository.getOneAuditedUser(appState.ashaId);
            userName = user.getUserName();
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            
        }
    }

    private void assignEditTextAndCallDatePicker(EditText editText) {
        try {
            String currDate = (editText.getText().length() > 0) ? editText.getText().toString() : "";
            CommonDatePickerFragment.getCommonDate(this, currDate);
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            
        }
    }

    private boolean validationFacility() {
        try {
            if (spnFacility.getSelectedItemPosition() == 0) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseselectvillage), Toast.LENGTH_LONG).show();
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            
        }
        return false;
    }

    private boolean validationStartTime() {
        try {
            if (etCampCrtStartTime.getText().toString().length() == 0) {
                etCampCrtStartTime.requestFocus();
                etCampCrtStartTime.setError(getResources().getString(R.string.pleaseentertime));
                return false;
            } else {
                etCampCrtStartTime.setError(null);
                return true;
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            
        }
        return false;
    }

    private boolean validationSchemaType() {
        try {
            if (aqView.id(R.id.spnCampSchemaType).getSpinner().getSelectedItemPosition() == 0) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseselectprogramconducted), Toast.LENGTH_LONG).show();
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            
        }
        return false;
    }

    private boolean validationName() {
        try {
            if (spnTrainingName.getSelectedItemPosition() == 0) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseselecttrainingname), Toast.LENGTH_LONG).show();
                return false;
            } else if (spnTrainingName.getSelectedItemPosition() == 12) {
                if (aqView.id(R.id.etCampName).getEditText().getText().length() == 0) {
                    aqView.id(R.id.etCampName).getEditText().setError(getResources().getString(R.string.pleaseentername));
                    aqView.id(R.id.etCampName).getEditText().requestFocus();
                    return false;
                } else {
                    aqView.id(R.id.etCampName).getEditText().setError(null);
                    return true;
                }
            } else {
                aqView.id(R.id.etCampName).getEditText().setError(null);
                return true;
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            
        }
        return false;
    }

    private boolean validationDate() {
        try {
            if (aqView.id(R.id.etCampDate).getEditText().getText().length() == 0) {
                aqView.id(R.id.etCampDate).getEditText().setError(getResources().getString(R.string.pleaseenterdate));
                aqView.id(R.id.etCampDate).getEditText().requestFocus();
                return false;
            } else {
                aqView.id(R.id.etCampDate).getEditText().setError(null);
                return true;
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            
        }
        return false;
    }

    //    15May2021 Bindu
    @Override
    public void onClick(View view) {
        try {
            switch (view.getId()) {
                case R.id.btnCampCrtCreate: {

                }
                break;
                case R.id.btnCampCrtCancel: {

                    break;
                }
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            
        }
    }

    @Override
    public void onTimeSet(TimePicker view, int selectedHour, int selectedMinute) {
        hour = selectedHour;
        minute = selectedMinute;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
            final Calendar c = Calendar.getInstance();
            hour = c.get(Calendar.HOUR_OF_DAY);
            minute = c.get(Calendar.MINUTE);

            if (currentEditTextForTime == aqView.id(R.id.etStartTime).getEditText()) {
                String strSelDateTime1 = aqView.id(R.id.etCampDate).getText().toString() + " " + ("" + selectedHour) + ":"
                        + ("" + selectedMinute);

                Date selDateTime1 = null;
                if (strSelDateTime1 != null && strSelDateTime1.trim().length() > 0)
                    selDateTime1 = sdf.parse(strSelDateTime1);
                if (selDateTime1.after(new Date())) {
                    displayAlert(getResources().getString(R.string.time_cannot_be_after_curre_time), null);
                    aqView.id(R.id.etStartTime).text(DateTimeUtil.getTimeIn12HourFormat(DateTimeUtil.getCurrentTime()));
                } else {
                    String time = DateTimeUtil.getTimeIn12HourFormat(selectedHour + ":" + selectedMinute);
                    aqView.id(R.id.etStartTime).getEditText().setText(time);
                }

            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    /**
     * This method opens the Time Dialog and sets Time to respective EditText
     *
     * @return
     */
    private void assignEditTextAndCallTimePicker(EditText editText) {
        currentEditTextForTime = editText;
        Calendar c = Calendar.getInstance();
        hour = c.get(Calendar.HOUR_OF_DAY);
        minute = c.get(Calendar.MINUTE);
        new TimePickerDialog(this, android.app.AlertDialog.THEME_HOLO_LIGHT, this, hour, minute,
                DateFormat.is24HourFormat(this)).show();
        // CommonTimePickerFragment.getCommonTime(DeliveryInfoActivity.this, hour, minute);
    }

    private void displayAlert(final String spanText2, final Intent goToScreen) {
        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(this);
        String strButtonText;
        if (goToScreen != null)
            strButtonText = getResources().getString(R.string.m121);
        else
            strButtonText = getResources().getString(R.string.ok);
        alertDialogBuilder.setMessage(spanText2).setCancelable(false).setNegativeButton(
                strButtonText, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        try {
                            if (goToScreen != null) {
                            } else
                                dialog.cancel();
                        } catch (Exception e) {
                            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                            crashlytics.log(e.getMessage());
                        }
                    }
                });
        android.app.AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    private void getFacilitiesData() {
        try {
            FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
            facilities = facilityRepository.getPlaceMap();
            ArrayList<String> villageList = new ArrayList<String>();
            villageList.add(getResources().getString(R.string.selectType));
            for (Map.Entry<String, Integer> village : facilities.entrySet())
                villageList.add(village.getKey());
            ArrayAdapter<String> VillageAdapter = new ArrayAdapter<>(this,
                    R.layout.simple_spinner_dropdown_item, villageList);
            VillageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            aqView.id(R.id.spnCampFacility).adapter(VillageAdapter);
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            
        }
    }
}