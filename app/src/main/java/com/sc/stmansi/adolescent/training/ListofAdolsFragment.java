
//20May2021 Arpitha - list has been changed
package com.sc.stmansi.adolescent.training;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.androidquery.AQuery;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.HomeVisit.HomeVisitListActivity;
import com.sc.stmansi.ListofAdols.ClickListener;
import com.sc.stmansi.ListofAdols.ListofAdolsRecyclerAdapter;
import com.sc.stmansi.ListofAdols.RecyclerViewTouchListener;
import com.sc.stmansi.R;
import com.sc.stmansi.SanNap.List_SanNap;
import com.sc.stmansi.adolanchistory.AdolANCHistory;
import com.sc.stmansi.adolescent.AdolDeactivation;
import com.sc.stmansi.adolescent.AdolParticipatedTrainings;
import com.sc.stmansi.adolescent.AdolRegistration;
import com.sc.stmansi.adolescent.AdolVisitHistory;
import com.sc.stmansi.adolescent.AdolVisit_New;
import com.sc.stmansi.adolescent.child.Adolescentchildreg;
import com.sc.stmansi.childregistration.ChildRegistrationActivity;
import com.sc.stmansi.common.CommonDatePickerFragment;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.common.WrapContentLinearLayoutManager;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.configuration.SyncState;
import com.sc.stmansi.database.DBMethods;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.immunization.AdolImmunizationListActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.quickAction.ActionItem;
import com.sc.stmansi.quickAction.QuickAction;
import com.sc.stmansi.recyclerview.RecyclerViewUpdateListener;
import com.sc.stmansi.registration.RegistrationActivity;
import com.sc.stmansi.repositories.AdolPregnantRepository;
import com.sc.stmansi.repositories.AdolVisitHeaderRepository;
import com.sc.stmansi.repositories.AdolescentRepository;
import com.sc.stmansi.repositories.ChildRepository;
import com.sc.stmansi.repositories.FacilityRepository;
import com.sc.stmansi.tables.TblChildInfo;
import com.sc.stmansi.tables.tblAdolPregnant;
import com.sc.stmansi.tables.tblAdolReg;
import com.sc.stmansi.tables.tblAdolVisitHeader;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.viewprofile.ViewProfileActivity;
import com.sc.stmansi.womanlist.CareType;
import com.sc.stmansi.womanlist.QueryParams;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class ListofAdolsFragment extends Fragment implements RecyclerViewUpdateListener,
        DatePickerDialog.OnDateSetListener {
    private View view;
    private AppState appState;
    private DatabaseHelper databaseHelper;
    //    private List<tblAdolReg> listofAdols;
    private static SearchView searchView;
    private QuickAction quickAction;
    private ListofAdolsRecyclerAdapter listofAdolsAdapter;
    private RecyclerView recyclerview;
    private FloatingActionButton floatingbtn;
    private SyncState syncState;
    private RadioButton rb_All;
    private RadioGroup rg_filter;
    private AQuery aq;
    private Button btToolbarHome;
    private int itemPosition;
    //    19May2021 Arpitha
    private List<tblAdolReg> filteredRowItems;
    private boolean isUpdating;
    private int nextItemPosition;
    private long womenCount;
    private String womenCondition;
    AdolescentRepository adolescentRepository;
    TextView allCountView;
    private Map<String, Integer> villageCodeForName;
    TextView noDataLabelView;
    private boolean spinnerSelectionChanged = false;
    private String wFilterStr = "";
    //    30Aug2021 Arpitha
    int ADD_PREGNANT = 7;
    ActionItem addPreg;
    private static AQuery aqPMSel;
    private static Activity activity;
    RadioGroup radGrpEDDorLmp;
    private static boolean isRegDate;
    Boolean wantToCloseDialog = false;
    private static int noOfDays;
    private tblregisteredwomen woman;
    String womanId;
    public static QuickAction quickActiontemp;
    List<tblAdolPregnant> allborns = new ArrayList<>();
    boolean isEditChild;
    private WrapContentLinearLayoutManager wrapManager;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_listof_adols, container, false);
//    19May2021 Arpitha
        Bundle bundle = getActivity().getIntent().getParcelableExtra("globalState");
        if (bundle != null) {
            appState = bundle.getParcelable("appState");
            syncState = bundle.getParcelable("syncState");
        } else {
            appState = new AppState();
        }
        allCountView = view.findViewById(R.id.txtAdolCount);//19May2021 Arpitha
        noDataLabelView = view.findViewById(R.id.txt_listofAdolsNoRecords);//19May2021 Arpitha
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
            Initialize();

            intialView();//19May2021 Arpitha
//            getData();
//            setData();

            activity = getActivity();

            aq.id(R.id.toolbarSpnFacilities).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    try {

                        appState.selectedVillageTitle = (String) parent.getItemAtPosition(position);
                        if (spinnerSelectionChanged) {
                            updateList(appState.selectedVillageTitle, villageCodeForName, "", false);
                        }
                        spinnerSelectionChanged = true;
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());

                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            btToolbarHome.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    displayDialogExit();
                }
            });

            rb_All.setChecked(true);
            rg_filter.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (checkedId == R.id.rb_list_all) {
                        if (listofAdolsAdapter != null) {
                            wFilterStr = "All";
                            updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr, false);
                        }
                    } else if (checkedId == R.id.rb_list_male) {
                        if (listofAdolsAdapter != null) {
                            wFilterStr = "Male";
                            updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr, false);
                        }
                    } else if (checkedId == R.id.rb_list_female) {
                        if (listofAdolsAdapter != null) {
                            wFilterStr = "Female";
                            updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr, false);
                        }
                    }
                }
            });

            recyclerview.addOnItemTouchListener(new RecyclerViewTouchListener(view.getContext(), recyclerview, new ClickListener() {
                @Override
                public void onLongClick(View child, int childAdapterPosition) {

                }

                @Override
                public void onClick(View child, int childAdapterPosition) {
                    try {
                        quickActiontemp = new QuickAction(view.getContext());
                        itemPosition = childAdapterPosition;

                        //mani 7Sep2021
                        AdolPregnantRepository adolPregnantRepository = new AdolPregnantRepository(databaseHelper);
                        allborns = new ArrayList<>();
                        try {
                            allborns = adolPregnantRepository.checkisBornChilds(filteredRowItems.get(itemPosition).getAdolID());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        String strAdolChild = "";
                        //mani 2Sep2021
                        try {
                            if (allborns != null && allborns.size() > 0) {
                                strAdolChild = getResources().getString(R.string.viewadolchild);
                                isEditChild = true;
                            } else
                                strAdolChild = getResources().getString(R.string.addchild);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        int AdolAge = 0;
                        if (filteredRowItems.get(childAdapterPosition).getRegAdolDoB().length() != 0) {
                            AdolAge = Integer.parseInt(DateTimeUtil.calculateAgeInYears(filteredRowItems.get(childAdapterPosition).getRegAdolDoB()));
                        } else if (filteredRowItems.get(childAdapterPosition).getRegAdolAge().length() > 0) {
                            int age = Integer.parseInt(filteredRowItems.get(childAdapterPosition).getRegAdolAge());
                            int tempage = Integer.parseInt(DateTimeUtil.calculateAgeInYears(filteredRowItems.get(childAdapterPosition).getRecordCreatedDate()));
                            AdolAge = age + tempage;
                        }

                        //            30Aug2021 Arpitha
                        String strAddPregnancy = "";
                        if (new AdolescentRepository(databaseHelper).
                                isAdolANC(filteredRowItems.get(itemPosition).getRegAdolAadharNo(), filteredRowItems.get(itemPosition).getAdolID()))
                            strAddPregnancy = getResources().getString(R.string.viewpregancny);
                        else
                            strAddPregnancy = getResources().getString(R.string.addpregancny);

                        TblChildInfo tblchilddata = new ChildRepository(databaseHelper).getChildBasedonAdol(filteredRowItems.get(itemPosition).getAdolID());

                        String strSanNap = getResources().getString(R.string.txtmhm);
                        String strEdit = getResources().getString(R.string.edit);
                        String strHomeVisit = getResources().getString(R.string.homevisit);
                        String strDeactivate = getResources().getString(R.string.deactivate);
                        String strHomeList = getResources().getString(R.string.homevisitlist);
                        String strTrainingPart = getResources().getString(R.string.trainingparticipated);
                        String strImmu = getResources().getString(R.string.sp_imm);
                        String strPregHistory = getResources().getString(R.string.preghistory);//10Sep2021 Arpitha

                        ActionItem wEdit = new ActionItem(1, strEdit, ContextCompat.getDrawable(getActivity().getApplicationContext(), R.drawable.ic_edit_icon));
                        ActionItem wHomeVisitAdd = new ActionItem(2, strHomeVisit, ContextCompat.getDrawable(getActivity(), R.drawable.ic_homevisit));
                        ActionItem wDeact = new ActionItem(3, strDeactivate, ContextCompat.getDrawable(getActivity(), R.drawable.deactivate));
                        ActionItem wHomeVisitList = new ActionItem(4, strHomeList, ContextCompat.getDrawable(getActivity(), R.drawable.anm_pending_activities));
                        ActionItem trainingPart = new ActionItem(5, strTrainingPart, ContextCompat.getDrawable(getActivity(), R.drawable.presentation));
                        ActionItem wMHM = new ActionItem(6, strSanNap, ContextCompat.getDrawable(getActivity(), R.drawable.form_icon));
                        ActionItem cImmunization = new ActionItem(7, strImmu, ContextCompat.getDrawable(getActivity().getApplicationContext(), R.drawable.immunisation));

                        ActionItem addPreg = new ActionItem(8, strAddPregnancy, ContextCompat.getDrawable(getActivity(), R.drawable.anc));
                        ActionItem ancHV = new ActionItem(9, getResources().getString(R.string.anchomevisit), ContextCompat.getDrawable(getActivity(), R.drawable.ic_homevisit));

                        ActionItem wChild = new ActionItem(10, strAdolChild, ContextCompat.getDrawable(getActivity(), R.drawable.general_examination_baby));

                        ActionItem ancHistory = new ActionItem(11, strPregHistory, ContextCompat.getDrawable(getActivity(), R.drawable.anc));//10Sep2021 Arpitha


                        quickActiontemp.setOnActionItemClickListener((source, pos, actionId) -> {
                            ActionItem actionItem = source.getActionItem(pos);
                            try {
                                if (actionItem.getActionId() == 1) {
                                    Intent intent = new Intent(view.getContext(), AdolRegistration.class);
                                    intent.putExtra("globalState", prepareBundle());
                                    intent.putExtra("isEdit", true);
                                    intent.putExtra("adolID", filteredRowItems.get(itemPosition).getAdolID());
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                } else if (actionItem.getActionId() == 2) {
                                    Intent intent = new Intent(view.getContext(), AdolVisit_New.class);
                                    intent.putExtra("globalState", prepareBundle());
                                    intent.putExtra("adolID", filteredRowItems.get(itemPosition).getAdolID());
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                } else if (actionItem.getActionId() == 3) {
                                    Intent intent = new Intent(view.getContext(), AdolDeactivation.class);
                                    intent.putExtra("globalState", prepareBundle());
                                    intent.putExtra("adolID", filteredRowItems.get(itemPosition).getAdolID());
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                } else if (actionItem.getActionId() == 4) {
                                    Intent intent = new Intent(view.getContext(), AdolVisitHistory.class);
                                    intent.putExtra("globalState", prepareBundle());
                                    intent.putExtra("adolID", filteredRowItems.get(itemPosition).getAdolID());
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                } else if (actionItem.getActionId() == 5) {
                                    Intent intent = new Intent(view.getContext(), AdolParticipatedTrainings.class);
                                    intent.putExtra("globalState", prepareBundle());
                                    intent.putExtra("adolID", filteredRowItems.get(itemPosition).getAdolID());
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                } else if (actionItem.getActionId() == 6) {
                                    Intent intent = new Intent(view.getContext(), List_SanNap.class);
                                    intent.putExtra("globalState", prepareBundle());
                                    intent.putExtra("adolID", filteredRowItems.get(itemPosition).getAdolID());
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                } else if (actionItem.getActionId() == 7) {
                                    Intent intent = new Intent(view.getContext(), AdolImmunizationListActivity.class);
                                    intent.putExtra("tblChildInfo", new AdolescentRepository(databaseHelper).getAdolDatafromAdolID(filteredRowItems.get(itemPosition).getAdolID()));
                                    intent.putExtra("globalState", prepareBundle());
                                    intent.putExtra("adolID", filteredRowItems.get(itemPosition).getAdolID());//20May2021 Arpitha
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                } else if (actionItem.getActionId() == 8) {
                                    if (new AdolescentRepository(databaseHelper).isAdolANC(filteredRowItems.get(itemPosition).getRegAdolAadharNo(), filteredRowItems.get(itemPosition).getAdolID())) {
                                        appState.selectedWomanId = womanId;
                                        Intent intent = new Intent(getActivity(), ViewProfileActivity.class);
                                        intent.putExtra("globalState", getActivity().getIntent().getBundleExtra("globalState"));
                                        startActivity(intent);
                                    } else
                                        showPregnantOrMotherSelectionDialog();
                                } else if (actionItem.getActionId() == 9) {
                                    appState.selectedWomanId = womanId;
                                    Intent intent = new Intent(getActivity(), HomeVisitListActivity.class);
                                    intent.putExtra("globalState", getActivity().getIntent().getBundleExtra("globalState"));
                                    startActivity(intent);
                                } else if (actionItem.getActionId() == 10) {
                                    if (isEditChild) {
                                        Intent intent = new Intent(view.getContext(), Adolescentchildreg.class);
                                        intent.putExtra("globalState", prepareBundle());
                                        intent.putExtra("adolID", filteredRowItems.get(itemPosition).getAdolID());
                                        intent.putExtra("isEditChild", isEditChild);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    } else {
                                        Intent intent = new Intent(view.getContext(), Adolescentchildreg.class);
                                        intent.putExtra("globalState", prepareBundle());
                                        intent.putExtra("adolID", filteredRowItems.get(itemPosition).getAdolID());
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                }
//                                11Sep2921 Arpitha
                                else if (actionItem.getActionId() == 11) {
                                    appState.selectedWomanId = womanId;
                                    Intent intent = new Intent(view.getContext(), AdolANCHistory.class);
                                    intent.putExtra("globalState", prepareBundle());
                                    intent.putExtra("aadharno",
                                            filteredRowItems.get(itemPosition).getRegAdolAadharNo());
                                    startActivity(intent);
                                }
                            } catch (Exception e) {
                                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                                crashlytics.log(e.getMessage());
                            }
                        });

                        if ((filteredRowItems.get(childAdapterPosition).getRegAdolPregnant() != null && filteredRowItems.get(childAdapterPosition).getRegAdolPregnant().trim().length() > 0
                                && filteredRowItems.get(childAdapterPosition).getRegAdolPregnant().equalsIgnoreCase("Yes")) && filteredRowItems.get(childAdapterPosition).getRegAdolGender().equals("Female")
                                && filteredRowItems.get(childAdapterPosition).getRegAdolisPeriod().equals("Yes") && AdolAge <= 16) { //pregnant and feedback form and immunization
                            quickActiontemp.addActionItem(wEdit);
                            quickActiontemp.addActionItem(cImmunization);
                            quickActiontemp.addActionItem(wHomeVisitAdd);
                            quickActiontemp.addActionItem(addPreg);//01Sep2021 Arpitha
                            womanId = new AdolescentRepository(databaseHelper)
                                    .getAdolANCWomanId(filteredRowItems.get(itemPosition).getRegAdolAadharNo(), filteredRowItems.get(itemPosition).getAdolID());
                            if (womanId != null && womanId.trim().length() > 0)
                                //  if(new AdolescentRepository(databaseHelper).isAdolANC(filteredRowItems.get(childAdapterPosition).getRegAdolAadharNo()))//10Sep2021 Arpitha
                                quickActiontemp.addActionItem(ancHV);
                            if (new AdolescentRepository(databaseHelper).
                                    isAdolANCHistory(filteredRowItems.get(childAdapterPosition)
                                            .getRegAdolAadharNo(), filteredRowItems.get(itemPosition).getAdolID()))//10Sep2021 Arpitha
                                quickActiontemp.addActionItem(ancHistory);//10Sep2021 Arpitha
                            quickActiontemp.addActionItem(wHomeVisitList);
                            quickActiontemp.addActionItem(trainingPart);
                            quickActiontemp.addActionItem(wMHM);
                            quickActiontemp.addActionItem(wChild);
                            quickActiontemp.addActionItem(wDeact);
                            quickActiontemp.show(child);
                        } else if (((filteredRowItems.get(childAdapterPosition).getRegAdolPregnant() != null &&
                                filteredRowItems.get(childAdapterPosition).getRegAdolPregnant().trim().length() > 0
                                && filteredRowItems.get(childAdapterPosition).getRegAdolPregnant()
                                .equalsIgnoreCase("Yes"))) || (new AdolescentRepository(databaseHelper).
                                isAdolPreganant(filteredRowItems.get(childAdapterPosition).getAdolID())) && filteredRowItems.get(childAdapterPosition).getRegAdolGender().equals("Female")
                                && filteredRowItems.get(childAdapterPosition).getRegAdolisPeriod().equals("Yes")) { //only pregnant and MHM
                            quickActiontemp.addActionItem(wEdit);
                            quickActiontemp.addActionItem(wHomeVisitAdd);
                            quickActiontemp.addActionItem(addPreg);//01Sep2021 Arpitha
                            womanId = new AdolescentRepository(databaseHelper).
                                    getAdolANCWomanId(filteredRowItems.get(itemPosition)
                                            .getRegAdolAadharNo(), filteredRowItems.get(itemPosition).getAdolID());
                            if (womanId != null && womanId.trim().length() > 0)
                                //if(new AdolescentRepository(databaseHelper).isAdolANC(filteredRowItems.get(childAdapterPosition).getRegAdolAadharNo()))//10Sep2021 Arpitha
                                quickActiontemp.addActionItem(ancHV);
                            if (new AdolescentRepository(databaseHelper).
                                    isAdolANCHistory(filteredRowItems.get(childAdapterPosition)
                                            .getRegAdolAadharNo(), filteredRowItems.get(itemPosition).getAdolID()))//10Sep2021 Arpitha
                                quickActiontemp.addActionItem(ancHistory);//10Sep2021 Arpitha
                            quickActiontemp.addActionItem(wHomeVisitList);
                            quickActiontemp.addActionItem(trainingPart);
                            quickActiontemp.addActionItem(wMHM);
                            quickActiontemp.addActionItem(wChild);
                            quickActiontemp.addActionItem(wDeact);
                            quickActiontemp.show(child);
                        } else if (((filteredRowItems.get(childAdapterPosition).getRegAdolPregnant() != null &&
                                filteredRowItems.get(childAdapterPosition).getRegAdolPregnant().trim().length() > 0
                                && filteredRowItems.get(childAdapterPosition).getRegAdolPregnant()
                                .equalsIgnoreCase("Yes"))) || (new AdolescentRepository(databaseHelper).
                                isAdolPreganant(filteredRowItems.get(childAdapterPosition).getAdolID()))) { //only pregnant
                            quickActiontemp.addActionItem(wEdit);
                            quickActiontemp.addActionItem(wHomeVisitAdd);
                            quickActiontemp.addActionItem(addPreg);//01Sep2021 Arpitha
                            womanId = new AdolescentRepository(databaseHelper).
                                    getAdolANCWomanId(filteredRowItems.get(itemPosition)
                                            .getRegAdolAadharNo(), filteredRowItems.get(itemPosition).getAdolID());
                            if (womanId != null && womanId.trim().length() > 0)
                                //if(new AdolescentRepository(databaseHelper).isAdolANC(filteredRowItems.get(childAdapterPosition).getRegAdolAadharNo()))//10Sep2021 Arpitha
                                quickActiontemp.addActionItem(ancHV);
                            if (new AdolescentRepository(databaseHelper).
                                    isAdolANCHistory(filteredRowItems.get(childAdapterPosition)
                                            .getRegAdolAadharNo(), filteredRowItems.get(itemPosition).getAdolID()))//10Sep2021 Arpitha
                                quickActiontemp.addActionItem(ancHistory);//10Sep2021 Arpitha
                            quickActiontemp.addActionItem(wHomeVisitList);
                            quickActiontemp.addActionItem(trainingPart);
                            quickActiontemp.addActionItem(wChild);
                            quickActiontemp.addActionItem(wDeact);
                            quickActiontemp.show(child);
                        } else if (filteredRowItems.get(childAdapterPosition).getRegAdolGender().equals("Female") && filteredRowItems.get(childAdapterPosition).getRegAdolisPeriod().equals("Yes") && AdolAge <= 16) { //Both feedback and immunization
                            quickActiontemp.addActionItem(wEdit);
                            quickActiontemp.addActionItem(cImmunization);
                            quickActiontemp.addActionItem(wHomeVisitAdd);
                            quickActiontemp.addActionItem(wHomeVisitList);
                            quickActiontemp.addActionItem(trainingPart);
                            quickActiontemp.addActionItem(wMHM);
                            quickActiontemp.addActionItem(wChild);
                            quickActiontemp.addActionItem(wDeact);
                            quickActiontemp.show(child);
                        } else if (filteredRowItems.get(childAdapterPosition).getRegAdolGender().equals("Female") && filteredRowItems.get(childAdapterPosition).getRegAdolisPeriod().equals("Yes")) { // Feedback form
                            quickActiontemp.addActionItem(wEdit);
                            quickActiontemp.addActionItem(wHomeVisitAdd);
                            quickActiontemp.addActionItem(wHomeVisitList);
                            quickActiontemp.addActionItem(trainingPart);
                            quickActiontemp.addActionItem(wMHM);
                            quickActiontemp.addActionItem(wChild);
                            quickActiontemp.addActionItem(wDeact);
                            quickActiontemp.show(child);
                        } else if (AdolAge <= 16) { //Only immunization
                            quickActiontemp.addActionItem(wEdit);
                            quickActiontemp.addActionItem(cImmunization);
                            quickActiontemp.addActionItem(wHomeVisitAdd);
                            quickActiontemp.addActionItem(wHomeVisitList);
                            quickActiontemp.addActionItem(trainingPart);
                            quickActiontemp.addActionItem(wDeact);
                            quickActiontemp.show(child);
                        } else if (filteredRowItems.get(childAdapterPosition).getRegAdolGender().equals("Female")) { //Child
                            quickActiontemp.addActionItem(wEdit);
                            quickActiontemp.addActionItem(wHomeVisitAdd);
                            quickActiontemp.addActionItem(wHomeVisitList);
                            quickActiontemp.addActionItem(trainingPart);
                            quickActiontemp.addActionItem(wChild);
                            quickActiontemp.addActionItem(wDeact);
                            quickActiontemp.show(child);
                        } else {
                            quickAction.show(child);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onClick(View v) {
                }

                @Override
                public boolean onLongClick(View v) {
                    return false;
                }
            }));

            floatingbtn.setOnClickListener(v -> {
                try {
                    Intent intent = new Intent(view.getContext(), AdolRegistration.class);
                    intent.putExtra("globalState", prepareBundle());
                    startActivity(intent);
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                    crashlytics.log(e.getMessage());
                }
            });

            try {
                quickAction.setOnActionItemClickListener((source, pos, actionId) -> {
                    ActionItem actionItem = source.getActionItem(pos);
                    try {
                        if (actionItem.getActionId() == 1) {
                            Intent intent = new Intent(view.getContext(), AdolRegistration.class);
                            intent.putExtra("globalState", prepareBundle());
                            intent.putExtra("isEdit", true);
                            intent.putExtra("adolID", filteredRowItems.get(itemPosition).getAdolID());
//                            intent.putExtra("adolID", listofAdols.get(itemPosition).getAdolID());
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        } else if (actionItem.getActionId() == 2) {
                            Intent intent = new Intent(view.getContext(), AdolVisit_New.class);
                            intent.putExtra("globalState", prepareBundle());
                            intent.putExtra("adolID", filteredRowItems.get(itemPosition).getAdolID());

//                            intent.putExtra("adolID", listofAdols.get(itemPosition).getAdolID());
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        } else if (actionItem.getActionId() == 3) {
                            Intent intent = new Intent(view.getContext(), AdolDeactivation.class);
                            intent.putExtra("globalState", prepareBundle());
                            intent.putExtra("adolID", filteredRowItems.get(itemPosition).getAdolID());
//                            intent.putExtra("adolID", listofAdols.get(itemPosition).getAdolID());
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        } else if (actionItem.getActionId() == 4) {
                            Intent intent = new Intent(view.getContext(), AdolVisitHistory.class);
                            intent.putExtra("globalState", prepareBundle());
                            intent.putExtra("adolID", filteredRowItems.get(itemPosition).getAdolID());
//                            intent.putExtra("adolID", listofAdols.get(itemPosition).getAdolID());
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        } else if (actionItem.getActionId() == 5) {
                            Intent intent = new Intent(view.getContext(), AdolParticipatedTrainings.class);
                            intent.putExtra("globalState", prepareBundle());
                            intent.putExtra("adolID", filteredRowItems.get(itemPosition).getAdolID());
//                            intent.putExtra("adolID", listofAdols.get(itemPosition).getAdolID());
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
//                        else if (actionItem.getActionId() == 6) {
//                            Toast.makeText(view.getContext(), "MHM" + filteredRowItems.get(itemPosition).getRegAdolName(), Toast.LENGTH_SHORT).show();
//                        }
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());

                    }
                });
//                quickAction.setOnDismissListener(new QuickAction.OnDismissListener() {
//                    @Override
//                    public void onDismiss() {
//                        try {
//                            if (quickAction.getActionItemById(6) == wMHM || quickAction.getActionItemById(5) == wMHM)
//                                quickAction.remove(wMHM);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//
//                    }
//                });
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());

            }

            aq.id(R.id.imgresetfilter).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        aq.id(R.id.imgresetfilter).getImageView().setImageResource(R.drawable.ic_reset_gray);
                        aq.id(R.id.etWomanFilter).text("");
                        searchView.setQuery("", false);
                        // wFilterStr = "";
                        CareType careType = CareType.GENERAL;
                        updateList(appState.selectedVillageTitle, villageCodeForName, "", false);
                      /*  aq.id(R.id.imgresetfilter).getImageView().setImageResource(R.drawable.ic_reset_gray);
                        searchView.setQuery("",false);
                        getData();
                        setData();20May2021 Arpitha*/
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());

                    }
                }
            });

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    updateList(appState.selectedVillageTitle, villageCodeForName, "", false);
                    return false;
                }

                @SuppressLint("SetTextI18n")
                @Override
                public boolean onQueryTextChange(String newText) {
                    if (newText.length() != 0) {
                        aq.id(R.id.imgresetfilter).getImageView().setImageResource(R.drawable.ic_reset);
                    } else {
                        aq.id(R.id.imgresetfilter).getImageView().setImageResource(R.drawable.ic_reset_gray);
                    }
//                    updateList(appState.selectedVillageTitle, villageCodeForName, "", false);
                    return true;
                }
            });

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private void displayDialogExit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
        builder.setMessage(getResources().getString(R.string.exit));
        builder.setCancelable(false);
        builder.setNegativeButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(view.getContext(), MainMenuActivity.class);
                intent.putExtra("globalState", prepareBundle());
                startActivity(intent);
            }
        }).setPositiveButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

  /*  @SuppressLint("SetTextI18n")
    private void setData() {
        try {
            if (listofAdols.size() != 0) {
                aq.id(R.id.txt_listofAdolsNoRecords).getTextView().setVisibility(View.GONE);
                recyclerview.setVisibility(View.VISIBLE);
                recyclerview.setHasFixedSize(true);
                recyclerview.setLayoutManager(new LinearLayoutManager(view.getContext()));
                listofAdolsAdapter = new ListofAdolsRecyclerAdapter(listofAdols, view.getContext(), quickAction, view);
                recyclerview.setAdapter(listofAdolsAdapter);
                aq.id(R.id.txtAdolCount).getTextView().setText(getResources().getString(R.string.reccount) + listofAdolsAdapter.getItemCount());
            } else {
                aq.id(R.id.txtAdolCount).getTextView().setText(getResources().getString(R.string.reccount) + 0);
                aq.id(R.id.txt_listofAdolsNoRecords).getTextView().setVisibility(View.VISIBLE);
                recyclerview.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            
        }
    }

   /* private void getData() {
        try {
            adolescentRepository = new AdolescentRepository(databaseHelper);
            listofAdols = adolescentRepository.getallAdols();
            listofAdols = addingLastVisitDetailstoAdols(listofAdols);

            FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
            Map<String, Integer> facilities = facilityRepository.getPlaceMap();
            ArrayList<String> villageList = new ArrayList<String>();
            villageList.add(getResources().getString(R.string.all));
            for (Map.Entry<String, Integer> village : facilities.entrySet())
                villageList.add(village.getKey());
            ArrayAdapter<String> VillageAdapter = new ArrayAdapter<>(view.getContext(),
                    R.layout.simple_spinner_dropdown_item_white, villageList);
            VillageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            aq.id(R.id.toolbarSpnFacilities).adapter(VillageAdapter);
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            
        }
    }*/

    private void Initialize() {
        try {
            databaseHelper = getHelper();
            DBMethods dbMethods = new DBMethods(databaseHelper);
            aq = new AQuery(view);

            TextView textViewTitle = view.findViewById(R.id.toolbartxtTitle);
            textViewTitle.setText(getResources().getString(R.string.list_of_adolescent));
            searchView = view.findViewById(R.id.svListofAdols);
            recyclerview = view.findViewById(R.id.rvListofAdol);
            floatingbtn = view.findViewById(R.id.floatingbuttonAdolReg);

            quickAction = new QuickAction(view.getContext());
            quickActiontemp = new QuickAction(view.getContext());

            btToolbarHome = view.findViewById(R.id.toolbarBtnHome);
            btToolbarHome.setVisibility(View.VISIBLE);

            String strEdit = getResources().getString(R.string.edit);
            String strHomeVisit = getResources().getString(R.string.homevisit);
            String strDeactivate = getResources().getString(R.string.deactivate);
            String strHomeList = getResources().getString(R.string.homevisitlist);
            String strTrainingPart = getResources().getString(R.string.trainingparticipated);

            ActionItem wEdit = new ActionItem(1, strEdit, ContextCompat.getDrawable(getActivity().getApplicationContext(), R.drawable.ic_edit_icon));
            ActionItem wHomeVisitAdd = new ActionItem(2, strHomeVisit, ContextCompat.getDrawable(getActivity(), R.drawable.ic_homevisit));
            ActionItem wDeact = new ActionItem(3, strDeactivate, ContextCompat.getDrawable(getActivity(), R.drawable.deactivate));
            ActionItem wHomeVisitList = new ActionItem(4, strHomeList, ContextCompat.getDrawable(getActivity(), R.drawable.anm_pending_activities));
            ActionItem trainingPart = new ActionItem(5, strTrainingPart, ContextCompat.getDrawable(getActivity(), R.drawable.presentation));


            quickAction.addActionItem(wEdit);
            quickAction.addActionItem(wHomeVisitAdd);
            quickAction.addActionItem(wHomeVisitList);
            quickAction.addActionItem(trainingPart);
            quickAction.addActionItem(wDeact);
//            quickAction.addActionItem(wMHM);

            rb_All = view.findViewById(R.id.rb_list_all);

            rg_filter = view.findViewById(R.id.rg_filterList);

            aq.id(R.id.toolbartxtTitle).getTextView().setText(getResources().getString(R.string.list_of_adolescent));

            aq.id(R.id.lltoolbarSpnFacilities).getView().setVisibility(View.VISIBLE);
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(view.getContext(), DatabaseHelper.class);
        }
        return databaseHelper;
    }

    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        b.putParcelable("syncState", syncState);
        return b;
    }

    private List<tblAdolReg> addingLastVisitDetailstoAdols(List<tblAdolReg> allAdols) {
        try {
            AdolVisitHeaderRepository adolVisitHeaderRepository = new AdolVisitHeaderRepository(databaseHelper);

            for (tblAdolReg s : allAdols) {
                if (s.getLastVisitCount() != 0) {
                    tblAdolVisitHeader localAdolVisitHeader = adolVisitHeaderRepository.getVisitbyID(String.valueOf(s.getLastVisitCount()), s.getAdolID());
                    s.setRegAdolGoingtoSchool(localAdolVisitHeader.getAdolvisHGTS());
                }
            }
            return allAdols;
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return null;
    }


    //    19May2021 Arpitha
//    recycler view scroll
    public class OnRecyclerScrollListener extends RecyclerView.OnScrollListener {
        int ydy = 0;

        @Override
        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            // 30April2021 Guru
            if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                int lastCompletelyVisibleItemPosition = wrapManager.findLastCompletelyVisibleItemPosition();
                boolean shouldPullUpRefresh = lastCompletelyVisibleItemPosition == filteredRowItems.size() - 1
                        && recyclerView.getScrollState() == RecyclerView.SCROLL_STATE_DRAGGING;
                if (shouldPullUpRefresh) {
                    QueryParams queryParams = new QueryParams();
                    queryParams.nextItemPosition = nextItemPosition;
                    queryParams.selectedVillageCode = appState.selectedVillageCode;
                    queryParams.textViewFilter = "";
//                queryParams.isLMPNotConfirmed = islmpconfirm; //02MAy2021 Bindu set isLMPconfirmation
                    if (!isUpdating) {
                        isUpdating = true;
                        new LoadPregnantList(ListofAdolsFragment.this, queryParams, false).execute();
                    }
                }
            } // 30April2021 Guru
        }

        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            ydy = dy;

            int lastCompletelyVisibleItemPosition = wrapManager.findLastCompletelyVisibleItemPosition();
            boolean shouldPullUpRefresh = lastCompletelyVisibleItemPosition == filteredRowItems.size() - 1
                    && recyclerView.getScrollState() == RecyclerView.SCROLL_STATE_DRAGGING;
            if (shouldPullUpRefresh) {
                QueryParams queryParams = new QueryParams();
                queryParams.nextItemPosition = nextItemPosition;
                queryParams.selectedVillageCode = appState.selectedVillageCode;
//            queryParams.textViewFilter = wFilterStr;
//            queryParams.isLMPNotConfirmed = islmpconfirm; //02MAy2021 Bindu - set isLMPconfirmation
                if (!isUpdating) {
                    isUpdating = true;
                    new LoadPregnantList(ListofAdolsFragment.this, queryParams, false).execute();
                }
            }
        }
    }

    //    load data in a list - 19May2021 Arpitha
    static class LoadPregnantList extends AsyncTask<Void, Void, Void> {

        private ListofAdolsFragment viewsUpdateCallback;
        private QueryParams queryParams;
        private boolean firstLoad;
        private Map<String, Integer> villageCodeForName;

        LoadPregnantList(ListofAdolsFragment viewsUpdateCallback, QueryParams queryParams, boolean firstLoad, Map<String, Integer> villageCodeForName) {
            this.viewsUpdateCallback = viewsUpdateCallback;
            this.queryParams = queryParams;
            this.firstLoad = firstLoad;
            this.villageCodeForName = villageCodeForName;
        }

        LoadPregnantList(ListofAdolsFragment viewsUpdateCallback, QueryParams queryParams, boolean firstLoad) {
            this.viewsUpdateCallback = viewsUpdateCallback;
            this.queryParams = queryParams;
            this.firstLoad = firstLoad;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            int selectedVillageCode;
            // This means second constructor was used (called by scrollListener)
            if (villageCodeForName == null) selectedVillageCode = queryParams.selectedVillageCode;
            else {
                String selectedVillageTitle = queryParams.selectedVillageTitle;
                if (selectedVillageTitle == null)
                    selectedVillageCode = 0;
                else {
                    Integer code = villageCodeForName.get(selectedVillageTitle);
                    selectedVillageCode = (code == null) ? 0 : code;
                }
            }
//            viewsUpdateCallback.appState.selectedVillageCode = selectedVillageCode;
            viewsUpdateCallback.appState.selectedVillageCode = queryParams.selectedVillageCode;

            try {
                viewsUpdateCallback.womenCount = viewsUpdateCallback.getRecordCount(viewsUpdateCallback.appState.selectedVillageCode, true,
                        new AdolescentRepository(viewsUpdateCallback.databaseHelper), viewsUpdateCallback.wFilterStr, searchView.getQuery().toString()); // 30April2021 Guru

                List<tblAdolReg> rows = viewsUpdateCallback.prepareRowItemsForDisplay
                        (viewsUpdateCallback.appState.selectedVillageCode, queryParams.nextItemPosition, viewsUpdateCallback.wFilterStr);
                if (firstLoad) {
                    int prevSize = viewsUpdateCallback.filteredRowItems.size();
                    viewsUpdateCallback.filteredRowItems.clear();
                    viewsUpdateCallback.listofAdolsAdapter.notifyItemRangeRemoved(0, prevSize);
                    firstLoad = false;
                }
                if (rows.size() > 0) {
                    viewsUpdateCallback.filteredRowItems.addAll(rows);

                    viewsUpdateCallback.nextItemPosition = viewsUpdateCallback.filteredRowItems.size();
                    viewsUpdateCallback.listofAdolsAdapter.notifyItemRangeInserted(firstLoad ? 0 : viewsUpdateCallback.nextItemPosition,
                            viewsUpdateCallback.filteredRowItems.size());
                }
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            try {
                viewsUpdateCallback.updateLabels();
                // 30April2021 Guru
                viewsUpdateCallback.listofAdolsAdapter.notifyDataSetChanged();
                if (viewsUpdateCallback.filteredRowItems.size() > 0) {
                    viewsUpdateCallback.recyclerview.setVisibility(View.VISIBLE);
                    if (!firstLoad) {
                        viewsUpdateCallback.wrapManager.scrollToPosition(viewsUpdateCallback.
                                listofAdolsAdapter.getItemCount());
                    }
                    viewsUpdateCallback.isUpdating = false;
                } // 30April2021 Guru
                viewsUpdateCallback.allCountView.setText(viewsUpdateCallback.getResources().
                        getString(R.string.recordcount,
                                viewsUpdateCallback.womenCount)); // 30April2021 Guru
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
        }
    }


    //    19May2021 Arpitha
    @Override
    public void updateList(String villageTitle, Map<String, Integer> villageNameForCode, String textViewFilter, boolean checked) {

        QueryParams params = new QueryParams();
        params.textViewFilter = textViewFilter;
        params.selectedVillageTitle = villageTitle;
        params.nextItemPosition = 0;
        if (villageTitle != null && villageTitle.trim().length() > 0 &&
                !villageTitle.equalsIgnoreCase(getResources().getString(R.string.all)))
            params.selectedVillageCode = villageNameForCode.get(villageTitle);
        new LoadPregnantList(this, params, true).execute();
        isUpdating = true;

        recyclerview.addOnScrollListener(new OnRecyclerScrollListener());
    }

    // 19May2021 Arpitha
    private List<tblAdolReg> prepareRowItemsForDisplay(int villageCode,
                                                       int limitStart, String filter) throws Exception {

        AdolescentRepository adolescentRepository = new AdolescentRepository(databaseHelper);
        return adolescentRepository.getRegisteredAdols(villageCode,
                limitStart, true, filter, searchView.getQuery().toString());
    }


    //    update labels based on data -19May2021 Arpitha
    private void updateLabels() {
        if (filteredRowItems.size() == 0) {
            recyclerview.setVisibility(View.GONE);

           /* noDataLabelView.setText(getResources().getString(R.string.no_data));
            noDataLabelView.setVisibility(View.VISIBLE);*/
            noDataLabelView.setVisibility(View.VISIBLE);
            return;
        } else {
//            noDataLabelView.setVisibility(View.GONE);
//            aq.id(R.id.txt_listofDeactiveAdolsNoRecords).visible();
            noDataLabelView.setVisibility(View.GONE);

        }
    }

    //    19May2021 Arpitha
    void intialView() throws Exception {
        populateSpinnerVillage();


        // 30April2021 Guru
        womenCount = getRecordCount(0, true, new AdolescentRepository(databaseHelper), "", searchView.getQuery().toString());
        allCountView.setText(getResources().getString(R.string.recordcount, womenCount));

        recyclerview.setVisibility(View.VISIBLE);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerview.setHasFixedSize(true);

        // use a linear layout manager
        wrapManager = new WrapContentLinearLayoutManager(view.getContext(), LinearLayout.VERTICAL, false);
        recyclerview.setLayoutManager(wrapManager);

        filteredRowItems = new ArrayList<>();

        QueryParams params = new QueryParams();
        params.textViewFilter = "";
        params.selectedVillageCode = 0;
        params.nextItemPosition = 0;
        new LoadPregnantList(this, params, true).execute();
        isUpdating = true;
        filteredRowItems = addingLastVisitDetailstoAdols(filteredRowItems);//25May2021 Arpitha
        listofAdolsAdapter = new ListofAdolsRecyclerAdapter(filteredRowItems, getActivity(), quickAction, quickActiontemp, view, new ChildRepository(databaseHelper), databaseHelper);
        recyclerview.setAdapter(listofAdolsAdapter);


        recyclerview.addOnScrollListener(new OnRecyclerScrollListener());

    }

    /**
     * populate Village to Spinner
     */
    private void populateSpinnerVillage() throws Exception {
        FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
        villageCodeForName = facilityRepository.getPlaceMap();

        ArrayAdapter<CharSequence> villageSpinnerAdapter = new ArrayAdapter<CharSequence>(getActivity(), R.layout.actionbar_spinner_list_item);
//        villageSpinnerAdapter.add("All Women");06May2021 Arpitha
        villageSpinnerAdapter.add(getResources().getString(R.string.all));//06May2021 Arpitha
        for (Map.Entry<String, Integer> village : villageCodeForName.entrySet()) {
            villageSpinnerAdapter.add(village.getKey());
        }
        villageSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        aq.id(R.id.toolbarSpnFacilities).adapter(villageSpinnerAdapter);
    }


//    01Sep2021 Arpitha

    /**
     * Alert Dialog to select Pregnant or Mother Registration
     */
    private void showPregnantOrMotherSelectionDialog() {
        try {
            final AlertDialog.Builder alert = new AlertDialog.Builder(activity,
                    AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
            final View customView = LayoutInflater.from(getActivity()).inflate(R.layout.pregnant_mother_selection,
                    null);

            aqPMSel = new AQuery(customView);
            radGrpEDDorLmp = customView.findViewById(R.id.radEDDorLmp);

            initializeScreen(aqPMSel.id(R.id.llPregOrMothMainLayout).getView());
            setInitialView();

            aqPMSel.id(R.id.llmother).gone();
            aqPMSel.id(R.id.llchild).gone();
            aqPMSel.id(R.id.llAdol).gone();


            aqPMSel.id(R.id.imgclearedddate).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    aqPMSel.id(R.id.etlmpdate).text("");
                }
            });
            aqPMSel.id(R.id.imgclearlmp).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    aqPMSel.id(R.id.etMotherAdd).text("");
                    aqPMSel.id(R.id.etlmpdate).text("");
                    aqPMSel.id(R.id.etgest).text("");
                    aqPMSel.id(R.id.imgclearlmp).gone();
                }
            });
            aqPMSel.id(R.id.imgclearregadate).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());
                    Toast.makeText(activity, getResources().getString(R.string.dateresettotodaysdate), Toast.LENGTH_LONG).show();//21Aug2019 Arpitha
                }
            });//   Arpitha - 20Aug2019

            aqPMSel.id(R.id.eteddmother).enabled(false);

            aqPMSel.id(R.id.etMotherAdd).getEditText().setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    try {
                        if (MotionEvent.ACTION_UP == event.getAction()) {
                            isRegDate = false;
                            DialogFragment newFragment = new DatePickerFragment(aqPMSel, aqPMSel.id(R.id.etMotherAdd).getEditText(), woman);
                            newFragment.show(getFragmentManager(), "datePicker");
//                            assignEditTextAndCallDatePicker(aqPMSel.id(R.id.etMotherAdd).getEditText());
                        }
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());

                    }
                    return true;
                }
            });

            aqPMSel.id(R.id.etregdate).getEditText().setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    try {
                        if (MotionEvent.ACTION_UP == event.getAction()) {
                            isRegDate = true;
                            DialogFragment newFragment = new DatePickerFragment(aqPMSel, aqPMSel.id(R.id.etregdate).getEditText(), woman);
                            newFragment.show(getFragmentManager(), "datePicker");
//                            assignEditTextAndCallDatePicker(aqPMSel.id(R.id.etregdate).getEditText());
                        }
                    } catch (Exception e) {

                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }
                    return true;
                }
            });

            aqPMSel.id(R.id.radLMP).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        setLMP();
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }
                }
            });

            aqPMSel.id(R.id.radEDD).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        setEDD();
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }
                }
            });

            alert.setView(customView);

            alert.setCancelable(true)
                    .setTitle(getResources().getString(R.string.registrationHeading))
                    .setPositiveButton(getResources().getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {

                                }
                            })
                    .setNegativeButton(getResources().getString(R.string.cancel), //01Oct2019 - Bindu - Add cancel button
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    dialog.cancel();
                                }
                            });
            final AlertDialog dialog = alert.create();
            if (dialog != null)
                dialog.show();

            // Overriding the handler immediately after show is probably a
            // better
            // approach than OnShowListener as described below
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        validateAndRegister();
                        if (wantToCloseDialog)
                            dialog.dismiss();
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }

                }
            });
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }


    //calculate gestation based on LMP
    private static void calculateEddGestation(String lmp) {
        try {

            if (lmp.equalsIgnoreCase("lmp")) {
                if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0
                        && aqPMSel.id(R.id.etMotherAdd).getText().length() > 0) {

                    noOfDays = DateTimeUtil
                            .getNumberOfDaysFromLMP(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                    int noOfWeeks = noOfDays / 7;
                    int days = noOfDays % 7;

                    String strdays = "";

                    if (days > 1)
                        strdays = days + " " + activity.getResources().getString(R.string.days);
                    else if (days == 1)
                        strdays = days + " " + activity.getResources().getString(R.string.day);

                    aqPMSel.id(R.id.etgest)
                            .text(noOfWeeks + " " + activity.getResources().getString(R.string.weeks) + " " + strdays);
                }

                String edd = populateEDD(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                aqPMSel.id(R.id.etlmpdate).text(edd);

            } else {

                String strLmp = CalculateLMP(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                aqPMSel.id(R.id.etlmpdate).text(strLmp);

                if (aqPMSel.id(R.id.etlmpdate).getText().length() > 0
                        && aqPMSel.id(R.id.etlmpdate).getText().length() > 0) {

                    noOfDays = DateTimeUtil
                            .getNumberOfDaysFromLMP(aqPMSel.id(R.id.etlmpdate).getText().toString());
                    int noOfWeeks = noOfDays / 7;
                    int days = noOfDays % 7;

                    String strdays = "";

                    if (days > 1)
                        strdays = days + " " + activity.getResources().getString(R.string.days);
                    else if (days == 1)
                        strdays = days + " " + activity.getResources().getString(R.string.day);

                    aqPMSel.id(R.id.etgest)
                            .text(noOfWeeks + " " + activity.getResources().getString(R.string.weeks) + " " + strdays);
                }

            }
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }

    }

    // Calculate EDD and display in EDD Edit text
    private static String populateEDD(String xLMP) throws Exception {
        String stredddate = "";
        try {

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());

            Date LmpDate;
            LmpDate = sdf.parse(xLMP);
            Calendar cal = Calendar.getInstance();
            cal.setTime(LmpDate);
            cal.add(Calendar.DAY_OF_MONTH, 280);

            int day = cal.get(Calendar.DAY_OF_MONTH);
            int month = cal.get(Calendar.MONTH) + 1;
            int year = cal.get(Calendar.YEAR);

            stredddate =
                    String.format("%02d", day) + "-" + String.format("%02d", month) + "-" + String.format("%02d", year);

        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        return stredddate;
    }

    //	calculate LMP based on EDD
    private static String CalculateLMP(String EDDate) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        Date LMP;
        LMP = sdf.parse(EDDate);
        Calendar cal = Calendar.getInstance();
        cal.setTime(LMP);
        cal.add(Calendar.DAY_OF_MONTH, -280);

        int day = cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH) + 1;
        int year = cal.get(Calendar.YEAR);

        String lmpDate = String.format("%02d", day) + "-" + String.format("%02d", month) + "-" + year;
        return lmpDate;
    }

    /**
     * This method opens the Date Dialog and sets Date to respective EditText
     */
    private void assignEditTextAndCallDatePicker(EditText editText) {
        String currDate = (editText.getText().length() > 0) ? editText.getText().toString() : "";
        CommonDatePickerFragment.getCommonDate(activity, currDate);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        try {
            aqPMSel.id(R.id.imgclearlmp).background(R.drawable.brush1);
            if (view.isShown()) {
                String str = String.format("%02d", day) + "-" + String.format("%02d", month + 1) + "-"
                        + year;

                int days = DateTimeUtil.getDaysBetweenDates(DateTimeUtil.getTodaysDate(), str);

                if (!aqPMSel.id(R.id.radEDD).isChecked()) {
                    if (days >= 0) {
                        if (isRegDate) {
                            if (days < 30) {
                                aqPMSel.id(R.id.etregdate).text(str);
                                aqPMSel.id(R.id.imgclearregadate).visible();//20AUg2019 Arpitha
                            } else if (days > 120) {
                                aqPMSel.id(R.id.etregdate).text("");
                                aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                                Toast.makeText(activity, getResources().getString(R.string.date_val_2_toast),
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha

                            } else {
                                aqPMSel.id(R.id.etregdate).text(str);
                                aqPMSel.id(R.id.imgclearregadate).visible();//20AUg2019 Arpitha
                            }

                        } else if (aqPMSel.id(R.id.radLMP).isChecked()) {
                            String lmporadd = str;
                            int daysdiff = 0;
                            String regdate = aqPMSel.id(R.id.etregdate).getText().toString();

                            if (regdate != null && regdate.trim().length() > 0) {
                                daysdiff = DateTimeUtil.getDaysBetweenDates(regdate, lmporadd);

                                int daysdifffromCUrr = DateTimeUtil
                                        .getDaysBetweenDates(DateTimeUtil.getTodaysDate(), lmporadd);

                                if (daysdiff <= 30) {
                                    Toast.makeText(activity,
                                            getResources().getString(R.string.date_val_3_toast),
                                            Toast.LENGTH_LONG).show();
                                    aqPMSel.id(R.id.etlmpdate).text("");
                                    aqPMSel.id(R.id.etgest).text("");
                                    aqPMSel.id(R.id.etMotherAdd).text("");
                                    aqPMSel.id(R.id.imgclearlmp).gone();//20Aug2019 Arpitha
                                } else if (daysdifffromCUrr > 280) {
                                    Toast.makeText(activity,
                                            getResources().getString(R.string.date_val_4_toast),
                                            Toast.LENGTH_LONG).show();
                                    aqPMSel.id(R.id.etlmpdate).text("");
                                    aqPMSel.id(R.id.etgest).text("");
                                    aqPMSel.id(R.id.etMotherAdd).text("");
                                    aqPMSel.id(R.id.imgclearlmp).gone();//20Aug2019 Arpitha
                                } else {
                                    aqPMSel.id(R.id.etMotherAdd).text(str);
                                    calculateEddGestation("lmp");
                                    aqPMSel.id(R.id.imgclearlmp).visible();//20Aug2019 Arpitha
                                }
                            } else
                                Toast.makeText(activity, getResources().getString(R.string.plz_enter_reg_dt),
                                        Toast.LENGTH_LONG).show();
                        } else if (aqPMSel.id(R.id.rdbMother).isChecked()) {

                            int daysdiff = DateTimeUtil
                                    .getDaysBetweenDates(aqPMSel.id(R.id.etregdate).getText().toString(), str);
                            if (daysdiff > 365) {// Arpitha 28Jun2018 - 23Dec2019 Arpitha - allow max 1 year

                                Toast.makeText(activity, (getResources().getString(R.string.datecannotbeafteroneyearfromcurrentdate)), // 11July2018
                                        // Arpitha
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etMotherAdd).text("");
                            } else
                                aqPMSel.id(R.id.etMotherAdd).text(str);
                        } else if (aqPMSel.id(R.id.rdbchild).isChecked()) {

                            int daysdiff = DateTimeUtil
                                    .getDaysBetweenDates(aqPMSel.id(R.id.etregdate).getText().toString(), str);
                            if (daysdiff > 5844) {// Arpitha 28Jun2018

                                Toast.makeText(activity, (getResources().getString(R.string.date_val_7_toast)), // 11July2018
                                        // Arpitha
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etMotherAdd).text("");
                            } else
                                aqPMSel.id(R.id.etMotherAdd).text(str);
                        }
                    } else {
                        if (isRegDate) {
                            aqPMSel.id(R.id.etregdate).text("");
                            aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                            Toast.makeText(activity,
                                    getResources().getString(R.string.reg_date_cannot_greater_current_date),
                                    Toast.LENGTH_LONG).show();
                            aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha
                        } else {
                            aqPMSel.id(R.id.etMotherAdd).text("");
                            aqPMSel.id(R.id.imgclearlmp).gone();//20AUg2019 Arpitha
                            aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                            if (aqPMSel.id(R.id.rdbPreg).isChecked()) {
                                Toast.makeText(activity,
                                        getResources()
                                                .getString(R.string.lmp_date_time_cannot_be_after_curre_datetime), // 11July2018

                                        Toast.LENGTH_LONG).show();

                                aqPMSel.id(R.id.etlmpdate).text("");
                                aqPMSel.id(R.id.etgest).text("");
                            } else {
                                Toast.makeText(activity,
                                        getResources()
                                                .getString(R.string.del_date_cannot_be_after_current_date_time), // 11July2018
                                        Toast.LENGTH_LONG).show();
                            }

                        }
                    }
                } else {
                    if (!isRegDate) {
                        int days1 = 0;
                        if (aqPMSel.id(R.id.etregdate).getText().toString().trim().length() > 0)
                            days1 = DateTimeUtil.getDaysBetweenDates(str,
                                    aqPMSel.id(R.id.etregdate).getText().toString());

                        if (days1 > 0 && days1 < 273) {
                            aqPMSel.id(R.id.etMotherAdd).text(str);
                            calculateEddGestation("edd");
                            aqPMSel.id(R.id.imgclearlmp).visible();//20Aug2019 Arpitha
                        } else {
                            if (days1 > 280) {
                                aqPMSel.id(R.id.etMotherAdd).text("");
                                aqPMSel.id(R.id.imgclearlmp).gone();//21Aug2019 Arpitha
                                aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                                Toast.makeText(activity, getResources().getString(R.string.date_val_6_toast),

                                        Toast.LENGTH_LONG).show();

                                aqPMSel.id(R.id.etlmpdate).text("");
                                aqPMSel.id(R.id.etgest).text("");

                            } else {
                                aqPMSel.id(R.id.etMotherAdd).text("");
                                aqPMSel.id(R.id.imgclearlmp).gone();//21Aug2019 Arpitha
                                aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                                Toast.makeText(activity, getResources().getString(R.string.date_val_5_toast),
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etlmpdate).text("");
                                aqPMSel.id(R.id.etgest).text("");
                            }
                        }
                    } else {
                        days = DateTimeUtil.getDaysBetweenDates(DateTimeUtil.getTodaysDate(), str);
                        if (days >= 0) {

                            if (days > 90) {
                                aqPMSel.id(R.id.etregdate).text("");
                                aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                                Toast.makeText(activity, getResources().getString(R.string.date_val_2_toast),

                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha
                            } else {
                                aqPMSel.id(R.id.etregdate).text(str);
                            }

                        } else {
                            aqPMSel.id(R.id.etregdate).text("");
                            aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                            Toast.makeText(activity, getResources().getString(R.string.reg_date_cannot_greater_current_date),

                                    Toast.LENGTH_LONG).show();
                            aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha
                        }
                    }
                }

            }

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    /**
     * This is a Recursive method that traverse through the group and subgroup
     * of view Sets text, assigns clickListners
     *
     * @throws Exception
     */
    private ArrayList<View> initializeScreen(View v) throws Exception {
        if (!(v instanceof ViewGroup)) {
            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);

            // TextView - Assign Labels
            if (v.getClass() == TextView.class || v.getClass() == RadioButton.class) {
                // aqPMSel.id(v.getId()).text(getSS(aqPMSel.id(v.getId()).getText().toString()));
            }

            if (v.getClass() == RadioButton.class) {
                aqPMSel.id(v.getId()).clicked(this, "commonClick");
            }

            return viewArrayList;
        }

        ArrayList<View> result = new ArrayList<View>();

        ViewGroup viewGroup = (ViewGroup) v;
        for (int i = 0; i < viewGroup.getChildCount(); i++) {

            View child = viewGroup.getChildAt(i);

            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);
            viewArrayList.addAll(initializeScreen(child));

            result.addAll(viewArrayList);
        }
        return result;
    }


    //	set initial view of the pregnant or mother pop up
    private void setInitialView() {
        aqPMSel.id(R.id.rdbPreg).checked(true);
        aqPMSel.id(R.id.tvMotherAddHeading).text(getResources().getString(R.string.tvPregLmpHeading));
        aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());
        aqPMSel.id(R.id.radLMP).checked(true);
        aqPMSel.id(R.id.etregdate).getEditText().setKeyListener(null);
        aqPMSel.id(R.id.etMotherAdd).getEditText().setKeyListener(null);
        aqPMSel.id(R.id.lllmpdate).visible();
        aqPMSel.id(R.id.llgest).visible();
        aqPMSel.id(R.id.vHLine4).visible();
        aqPMSel.id(R.id.vHLine6).visible();
        aqPMSel.id(R.id.llnote).visible();

        //13Apr2021 Bindu - set here cos not translated from xml
        aqPMSel.id(R.id.anclbl).text(getResources().getString(R.string.anclbl));
        aqPMSel.id(R.id.txtpnclbl).text(getResources().getString(R.string.pnclbl));
        aqPMSel.id(R.id.lmp_date).text(getResources().getString(R.string.tvedd));
        aqPMSel.id(R.id.txtgest).text(getResources().getString(R.string.tvGestationalageL));
        aqPMSel.id(R.id.txtchildreglbl).text(getResources().getString(R.string.childreg));
        aqPMSel.id(R.id.tvFirstPregHeading).text(getResources().getString(R.string.tvFirstPregHeading));
        aqPMSel.id(R.id.reg_date).text(getResources().getString(R.string.reg_date));
        aqPMSel.id(R.id.instructions).text(getResources().getString(R.string.instruction));
        aqPMSel.id(R.id.regdt).text(getResources().getString(R.string.reg_date));
        aqPMSel.id(R.id.txt1).text(getResources().getString(R.string.date_val_0));
        aqPMSel.id(R.id.txt2).text(getResources().getString(R.string.date_val_1));
        aqPMSel.id(R.id.txt3).text(getResources().getString(R.string.date_val_2));
        aqPMSel.id(R.id.txtlmp).text(getResources().getString(R.string.tvPregLmpHeading));
        aqPMSel.id(R.id.txt4).text(getResources().getString(R.string.date_val_3));
        aqPMSel.id(R.id.txt5).text(getResources().getString(R.string.date_val_4));
        aqPMSel.id(R.id.txt6).text(getResources().getString(R.string.date_val_5));
        aqPMSel.id(R.id.txt7).text(getResources().getString(R.string.date_val_6));
        aqPMSel.id(R.id.txt8).text(getResources().getString(R.string.date_val_7));
        aqPMSel.id(R.id.radLMP).text(getResources().getString(R.string.tvlmp));
        aqPMSel.id(R.id.radEDD).text(getResources().getString(R.string.tvedd));
//22May2021 Bindu set adol reg lbl
        aqPMSel.id(R.id.adolreg).text(getResources().getString(R.string.adolescentreg));

    }

    // capture EDD
    private void setEDD() throws Exception {
        aqPMSel.id(R.id.tvMotherAddHeading).text(getResources().getString(R.string.tvedd));
        aqPMSel.id(R.id.etMotherAdd).text("");
        aqPMSel.id(R.id.imgclearlmp).gone();//20AUg2019 Arpitha
        aqPMSel.id(R.id.lmp_date).text(getResources().getString(R.string.tvlmp));
        aqPMSel.id(R.id.etlmpdate).text("");
        aqPMSel.id(R.id.etgest).text("");
        aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());
    }

    // capture LMP
    private void setLMP() throws Exception {
        aqPMSel.id(R.id.tvMotherAddHeading).text(getResources().getString(R.string.tvlmp));
        aqPMSel.id(R.id.etMotherAdd).text("");
        aqPMSel.id(R.id.imgclearlmp).gone();//20AUg2019 Arpitha
        aqPMSel.id(R.id.lmp_date).text(getResources().getString(R.string.tvedd));
        aqPMSel.id(R.id.etlmpdate).text("");
        aqPMSel.id(R.id.etgest).text("");
    }


    /**
     * Common method invokes when any button clicked This method handles
     * Edittext, save clicks. Validate mandatory fields
     */
    public void commonClick(View v) throws Exception {
        switch (v.getId()) {
            case R.id.rdbPreg: {
                aqPMSel.id(R.id.rdbMother).checked(false);
                aqPMSel.id(R.id.tvFirstPregHeading).enabled(true);
                aqPMSel.id(R.id.rdbchild).checked(false);
                aqPMSel.id(R.id.rdbAdol).checked(false);//Ramesh 13-may2021
                aqPMSel.id(R.id.chkFirstPreg).enabled(true);
                aqPMSel.id(R.id.radEDDorLmp).visible();
                //27Nov2018 - Bindu - radiogrp clear chk to reset radiobtn
                aqPMSel.id(R.id.vHLine5).visible();
                aqPMSel.id(R.id.etMotherAdd).text("");
                aqPMSel.id(R.id.radLMP).checked(true);
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.tvPregLmpHeading)));
                // 27Jun2018 Arpitha
                aqPMSel.id(R.id.etregdate).enabled(true);
                aqPMSel.id(R.id.etregdate).textColor(getResources().getColor(R.color.black));
//				aqPMSel.id(R.id.llregdate).background(R.drawable.editext_none);
                aqPMSel.id(R.id.lllmpdate).visible();
                aqPMSel.id(R.id.llgest).visible();
                aqPMSel.id(R.id.vHLine4).visible();
                aqPMSel.id(R.id.vHLine6).visible();

                aqPMSel.id(R.id.llMotherAdd).visible();
                aqPMSel.id(R.id.llFirstPreg).visible();
                aqPMSel.id(R.id.llnote).visible();
                aqPMSel.id(R.id.vHLine5).visible();
//				19Nov2019 Arpitha
                aqPMSel.id(R.id.llancinstruction).visible();
                aqPMSel.id(R.id.llpncinstruction).gone();

                break;
            }
            case R.id.rdbMother: {
                aqPMSel.id(R.id.rdbPreg).checked(false);
                aqPMSel.id(R.id.rdbchild).checked(false);
                aqPMSel.id(R.id.rdbAdol).checked(false);//Ramesh 13-may2021
                //27Nov2018 - Bindu radiogrp clear chk to reset radiobtn
                radGrpEDDorLmp.clearCheck();
                aqPMSel.id(R.id.radLMP).checked(false);
                aqPMSel.id(R.id.radEDD).checked(false);
                aqPMSel.id(R.id.tvFirstPregHeading).enabled(false);
                aqPMSel.id(R.id.chkFirstPreg).enabled(false);
                aqPMSel.id(R.id.chkFirstPreg).checked(false);
                aqPMSel.id(R.id.radEDDorLmp).gone();
                aqPMSel.id(R.id.vHLine5).gone();
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.deldate)));
                aqPMSel.id(R.id.etMotherAdd).text("");
                // 27Jun2018 Arpitha
                aqPMSel.id(R.id.etregdate).enabled(false);
                aqPMSel.id(R.id.etregdate).textColor(R.color.fontcolor_disable);
                //	aqPMSel.id(R.id.llregdate).background(R.drawable.edittext_disable);
                aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());//21March2019 Arpitha
                aqPMSel.id(R.id.lllmpdate).gone();
                aqPMSel.id(R.id.llgest).gone();
                aqPMSel.id(R.id.vHLine4).gone();
                aqPMSel.id(R.id.vHLine6).gone();

                aqPMSel.id(R.id.llMotherAdd).visible();
                aqPMSel.id(R.id.llFirstPreg).visible();
                aqPMSel.id(R.id.llnote).visible();
                aqPMSel.id(R.id.vHLine5).visible();
                //				19Nov2019 Arpitha
                aqPMSel.id(R.id.llancinstruction).gone();
                aqPMSel.id(R.id.llpncinstruction).visible();
                aqPMSel.id(R.id.llFirstPreg).gone();
                aqPMSel.id(R.id.imgclearlmp).visible();
                break;
            }
            case R.id.radEDD: {

                //	aqPMSel.id(R.id.radEDD).checked(true); //27Nov2018 - Bindu- comment the line
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.deldate)));
                aqPMSel.id(R.id.etMotherAdd).text("");
                aqPMSel.id(R.id.lmp_date).text((getResources().getString(R.string.tvlmp)));// 28Jun2018
                // Arpitha
                // 11July2018 Arpitha
                aqPMSel.id(R.id.etlmpdate).text("");
                aqPMSel.id(R.id.etgest).text("");
                break;
            }
            case R.id.rdbchild: {
                aqPMSel.id(R.id.rdbPreg).checked(false);
                aqPMSel.id(R.id.rdbMother).checked(false);
                aqPMSel.id(R.id.rdbAdol).checked(false);//Ramesh 13-may2021

                //27Nov2018 - Bindu radiogrp clear chk to reset radiobtn
                radGrpEDDorLmp.clearCheck();//Arpitha2021
                aqPMSel.id(R.id.radLMP).checked(false);
                aqPMSel.id(R.id.radEDD).checked(false);
                aqPMSel.id(R.id.tvFirstPregHeading).enabled(false);
                aqPMSel.id(R.id.chkFirstPreg).enabled(false).checked(false);
                aqPMSel.id(R.id.radEDDorLmp).gone();
                aqPMSel.id(R.id.vHLine5).gone();
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.deldate)));
                aqPMSel.id(R.id.etMotherAdd).text("");
                aqPMSel.id(R.id.llMotherAdd).gone();
                aqPMSel.id(R.id.llFirstPreg).gone();
                aqPMSel.id(R.id.llnote).gone();
                // 27Jun2018 Arpitha
                aqPMSel.id(R.id.etregdate).enabled(false);
                aqPMSel.id(R.id.etregdate).textColor(R.color.fontcolor_disable);
                //	aqPMSel.id(R.id.llregdate).background(R.drawable.edittext_disable);
                aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());//21March2019 Arpitha
                aqPMSel.id(R.id.lllmpdate).gone();
                aqPMSel.id(R.id.llgest).gone();
                aqPMSel.id(R.id.vHLine4).gone();
                aqPMSel.id(R.id.vHLine6).gone();
                break;
            }
            case R.id.radLMP: {
                //	aqPMSel.id(R.id.radLMP).checked(true); //27Nov2018 - Bindu- comment the line
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.tvlmp)));
                aqPMSel.id(R.id.etMotherAdd).text("");
                aqPMSel.id(R.id.lmp_date).text((getResources().getString(R.string.tvedd)));// 28Jun2018
                // Arpitha

                // 11July2018 Arpitha
                aqPMSel.id(R.id.etlmpdate).text("");
                aqPMSel.id(R.id.etgest).text("");
                break;
            }

            case R.id.gridView1:
                break;
            case R.id.rdbAdol://Ramesh 13-may-2021
                aqPMSel.id(R.id.rdbPreg).checked(false);
                aqPMSel.id(R.id.rdbMother).checked(false);
                aqPMSel.id(R.id.rdbchild).checked(false);

                radGrpEDDorLmp.clearCheck();//Arpitha2021
                aqPMSel.id(R.id.radLMP).checked(false);
                aqPMSel.id(R.id.radEDD).checked(false);
                aqPMSel.id(R.id.tvFirstPregHeading).enabled(false);
                aqPMSel.id(R.id.chkFirstPreg).enabled(false).checked(false);
                aqPMSel.id(R.id.radEDDorLmp).gone();
                aqPMSel.id(R.id.vHLine5).gone();
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.deldate)));
                aqPMSel.id(R.id.etMotherAdd).text("");
                aqPMSel.id(R.id.llMotherAdd).gone();
                aqPMSel.id(R.id.llFirstPreg).gone();
                aqPMSel.id(R.id.llnote).gone();
                aqPMSel.id(R.id.etregdate).enabled(false);
                aqPMSel.id(R.id.etregdate).textColor(R.color.fontcolor_disable);
                aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());//21March2019 Arpitha
                aqPMSel.id(R.id.lllmpdate).gone();
                aqPMSel.id(R.id.llgest).gone();
                aqPMSel.id(R.id.vHLine4).gone();
                aqPMSel.id(R.id.vHLine6).gone();
                break;
            default: {
                break;
            }
        }
    }

    private void validateAndRegister() throws Exception {
        try {
            Intent nextScreen;
            String lmpOrAdd = aqPMSel.id(R.id.etMotherAdd).getText().toString();
            String regDate = aqPMSel.id(R.id.etregdate).getText().toString();

            if ((lmpOrAdd != null && lmpOrAdd.length() > 0) && (regDate != null && regDate.length() > 0)) {

                if (aqPMSel.id(R.id.rdbPreg).isChecked()) {
                    if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0) {
                        int days = DateTimeUtil.getDaysBetweenDates(regDate, lmpOrAdd);
                        int daysdiff = DateTimeUtil
                                .getDaysBetweenDates(DateTimeUtil.getTodaysDate(), lmpOrAdd);
                        if (aqPMSel.id(R.id.radEDD).isChecked()) {
                            if (days >= 0) {
                                wantToCloseDialog = false;
                                Toast.makeText(getActivity(),
                                        getResources().getString(R.string.date_val_5_toast),

                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etMotherAdd).text("");
                            } else if (noOfDays < 30) {
                                wantToCloseDialog = false;
                                Toast.makeText(getActivity(),
                                        getResources().getString(R.string.gestation_must_be_30days),
                                        Toast.LENGTH_LONG).show();
                            } else if (noOfDays > 280) {
                                wantToCloseDialog = false;
                                Toast.makeText(getActivity(), getResources().getString(R.string.gest_280),
                                        Toast.LENGTH_LONG).show();
                            } else {
                                wantToCloseDialog = true;
                                woman = new tblregisteredwomen();
                                nextScreen = new Intent(getActivity(), RegistrationActivity.class);
                                nextScreen.putExtra("globalState", getActivity().getIntent().getBundleExtra("globalState"));
                                nextScreen.putExtra("adolAadhar", filteredRowItems.get(itemPosition).getRegAdolAadharNo());//01Sep2021 Arpitha
                                woman.setRegPregnantorMother(1);
                                int gravida = 0;
                                if (aqPMSel.id(R.id.chkFirstPreg).isChecked())
                                    gravida = 1;
                                woman.setRegGravida(gravida);
                                woman.setRegRegistrationDate(aqPMSel.id(R.id.etregdate).getText().toString());
                                woman.setRegEDD(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                                nextScreen.putExtra("woman", woman);
                                startActivity(nextScreen);
                            }
                        } else {
                            if (days < 30) {
                                wantToCloseDialog = false;
                                Toast.makeText(getActivity(),
                                        getResources().getString(R.string.date_val_3_toast),

                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etlmpdate).text("");
                                aqPMSel.id(R.id.etMotherAdd).text("");
                            } else if (daysdiff > 280) {

                                wantToCloseDialog = false;
                                Toast.makeText(getActivity(),
                                        activity.getResources().getString(R.string.date_val_4_toast),
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etlmpdate).text("");
                                aqPMSel.id(R.id.etMotherAdd).text("");
                            } else {
                                wantToCloseDialog = true;
                                woman = new tblregisteredwomen();
                                nextScreen = new Intent(activity, RegistrationActivity.class);
                                nextScreen.putExtra("globalState", getActivity().getIntent().getBundleExtra("globalState"));
                                nextScreen.putExtra("adolAadhar", filteredRowItems.get(itemPosition).getRegAdolAadharNo());//01Sep2021 Arpitha


                                woman.setRegPregnantorMother(1);
                                int gravida = 0;
                                if (aqPMSel.id(R.id.chkFirstPreg).isChecked())
                                    gravida = 1;
                                woman.setRegGravida(gravida);
                                woman.setRegRegistrationDate(aqPMSel.id(R.id.etregdate).getText().toString());
                                if (aqPMSel.id(R.id.radEDD).isChecked())
                                    woman.setRegEDD(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                                else
                                    woman.setRegLMP(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                                nextScreen.putExtra("woman", woman);
                                startActivity(nextScreen);
                            }
                        }

                    }
                } else if (aqPMSel.id(R.id.rdbMother).isChecked()) {
                    if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0) {
                        int days = DateTimeUtil.getDaysBetweenDates(regDate, lmpOrAdd);
                        if (days < 0) {
                            wantToCloseDialog = false;
                            Toast.makeText(activity, (getResources().getString(R.string.m077)),
                                    Toast.LENGTH_LONG).show();
                            // } else if (days > 549) {
                        } else if (days > 5844) {// Arpitha
                            // 28Jun2018
                            wantToCloseDialog = false;
                            Toast.makeText(activity,
                                    (getResources().getString(R.string.date_val_7_toast)), // 11july2018
                                    // Arpitha
                                    Toast.LENGTH_LONG).show();
                        } else {
                            wantToCloseDialog = true;
                            woman = new tblregisteredwomen();
                            nextScreen = new Intent(activity, RegistrationActivity.class);
                            nextScreen.putExtra("adolAadhar", filteredRowItems.get(itemPosition).getRegAdolAadharNo());//01Sep2021 Arpitha
                            woman.setRegPregnantorMother(2);
                            woman.setRegADDate(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                            woman.setRegRegistrationDate(aqPMSel.id(R.id.etregdate).getText().toString());
                            nextScreen.putExtra("woman", woman);
                            nextScreen.putExtra("globalState", getActivity().getIntent().getBundleExtra("globalState"));
                            startActivity(nextScreen);
                        }
                    }
                } else if (aqPMSel.id(R.id.rdbchild).isChecked()) {
                    if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0) {
                        int days = DateTimeUtil.getDaysBetweenDates(regDate, lmpOrAdd);
                        if (days < 0) {
                            wantToCloseDialog = false;
                            Toast.makeText(activity, (getResources().getString(R.string.m077)),
                                    Toast.LENGTH_LONG).show();
                            // } else if (days > 549) {
                        } else if (days > 5844) {// Arpitha
                            // 28Jun2018
                            wantToCloseDialog = false;
                            Toast.makeText(activity,
                                    (getResources().getString(R.string.date_val_7_toast)), // 11july2018
                                    // Arpitha
                                    Toast.LENGTH_LONG).show();
                        } else {
                            wantToCloseDialog = true;
                            woman = new tblregisteredwomen();
                            nextScreen = new Intent(activity, ChildRegistrationActivity.class);
//							woman.setRegPregnantorMother(1);
                            woman.setRegADDate(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                            woman.setRegRegistrationDate(aqPMSel.id(R.id.etregdate).getText().toString());
                            nextScreen.putExtra("woman", woman);
                            nextScreen.putExtra("globalState", getActivity().getIntent().getBundleExtra("globalState"));
                            startActivity(nextScreen);
                        }
                    }
                } else if (aqPMSel.id(R.id.rdbAdol).isChecked()) { //Ramesh 13-may-2021
                    if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0) {
                        int days = DateTimeUtil.getDaysBetweenDates(regDate, lmpOrAdd);
                        if (days < 0) {
                            wantToCloseDialog = false;
                            Toast.makeText(activity, (getResources().getString(R.string.m077)),
                                    Toast.LENGTH_LONG).show();
                            // } else if (days > 549) {
                        } else if (days > 5844) {// Arpitha
                            // 28Jun2018
                            wantToCloseDialog = false;
                            Toast.makeText(activity,
                                    (getResources().getString(R.string.date_val_7_toast)), // 11july2018
                                    // Arpitha
                                    Toast.LENGTH_LONG).show();
                        } else {
                            wantToCloseDialog = true;
                            tblAdolReg adolReg = new tblAdolReg();
                            nextScreen = new Intent(activity, AdolRegistration.class);
                            adolReg.setRecordCreatedDate(aqPMSel.id(R.id.etregdate).getText().toString());
                            nextScreen.putExtra("AdolReg", adolReg);
                            nextScreen.putExtra("globalState", getActivity().getIntent().getBundleExtra("globalState"));
                            startActivity(nextScreen);
                        }
                    }
                }
            } else {
                wantToCloseDialog = false;
                if (aqPMSel.id(R.id.rdbPreg).isChecked()) {
                    if (aqPMSel.id(R.id.etMotherAdd).getText().toString().trim().length() <= 0) {
                        aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                        if (aqPMSel.id(R.id.radEDD).isChecked())
                            Toast.makeText(activity,
                                    (activity.getResources().getString(R.string.m170)),
                                    Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(activity,
                                    activity.getResources().getString(R.string.m074),
                                    Toast.LENGTH_LONG).show();
                    } else if (aqPMSel.id(R.id.etregdate).getText().toString().trim().length() <= 0) {
                        aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                        Toast.makeText(getActivity(),
                                (activity.getResources().getString(R.string.m157)),
                                Toast.LENGTH_LONG).show();
                    }
                } else if (aqPMSel.id(R.id.rdbMother).isChecked()) {
                    if (aqPMSel.id(R.id.etMotherAdd).getText().toString().trim().length() <= 0) {
                        aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                        Toast.makeText(activity,
                                (activity.getResources().getString(R.string.m066)),
                                Toast.LENGTH_LONG).show();
                    } else if (aqPMSel.id(R.id.etregdate).getText().toString().trim().length() <= 0) {
                        aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                        Toast.makeText(activity,
                                activity.getResources().getString(R.string.m157),
                                Toast.LENGTH_LONG).show();
                    }
                } else if (aqPMSel.id(R.id.rdbchild).isChecked()) {
                    wantToCloseDialog = true;
//					if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0)
                    {
//						int days = DateTimeUtil.getDaysBetweenDates(regDate, lmpOrAdd);
						/*if (days < 0) {
							wantToCloseDialog = false;
							Toast.makeText(activity, (getResources().getString(R.string.m077)),
									Toast.LENGTH_LONG).show();
							// } else if (days > 549) {
						} else if (days > 5844) {// Arpitha
							// 28Jun2018
							wantToCloseDialog = false;
							Toast.makeText(activity,
									(getResources().getString(R.string.date_val_7_toast)), // 11july2018
									// Arpitha
									Toast.LENGTH_LONG).show();
						} else {*/
                        wantToCloseDialog = true;
                        woman = new tblregisteredwomen();
                        nextScreen = new Intent(activity, ChildRegistrationActivity.class);
                        woman.setRegPregnantorMother(1);
                        woman.setRegADDate(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                        woman.setRegRegistrationDate(aqPMSel.id(R.id.etregdate).getText().toString());
                        nextScreen.putExtra("woman", woman);
                        nextScreen.putExtra("globalState", activity.getIntent().getBundleExtra("globalState"));
                        startActivity(nextScreen);
//						}
                    }
                } else if (aqPMSel.id(R.id.rdbAdol).isChecked()) { //Ramesh 13-may-2021

                    wantToCloseDialog = true;
                    tblAdolReg adolReg = new tblAdolReg();
                    nextScreen = new Intent(activity, AdolRegistration.class);
                    adolReg.setRecordCreatedDate(aqPMSel.id(R.id.etregdate).getText().toString());
                    nextScreen.putExtra("AdolReg", adolReg);
                    nextScreen.putExtra("globalState", activity.getIntent().getBundleExtra("globalState"));
                    startActivity(nextScreen);


                }
            }

        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }


    @SuppressLint("SimpleDateFormat")
    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        private AQuery aqPMSel;
        private tblregisteredwomen woman;
        private EditText etDate;

        public DatePickerFragment() {
        }

        public DatePickerFragment(AQuery aQuery, EditText etDate, tblregisteredwomen woman) {
            aqPMSel = aQuery;
            this.woman = woman;
            this.etDate = etDate;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            int year = 0, month = 0, day = 0;
            try {

                Calendar c = Calendar.getInstance();
                String defaultDate = "";
                if (etDate != null && etDate.toString().trim().length() > 0)
                    defaultDate = etDate.getText().toString(); //09Aug2019 - Cal set date of Provided date
                else defaultDate = DateTimeUtil.getTodaysDate();
                if (defaultDate.length() > 0) {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    Date sendDate = sdf.parse(defaultDate);
                    c.setTime(sendDate);
                    year = c.get(Calendar.YEAR);
                    month = c.get(Calendar.MONTH);
                    day = c.get(Calendar.DAY_OF_MONTH);
                } else {
                    c.setTime(new Date());
                    year = c.get(Calendar.YEAR);
                    month = c.get(Calendar.MONTH);
                    day = c.get(Calendar.DAY_OF_MONTH);
                }
            } catch (Exception e) {


                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }

            // Create a new instance of DatePickerDialog and return it
            DatePickerDialog dpDialog;
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                // Do something for lollipop and above versions
                dpDialog = new DatePickerDialog(getActivity(), R.style.DialogTheme, this, year, month, day);
            } else {
                // do something for phones running an SDK before lollipop
                dpDialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, this, year, month, day);
                dpDialog.setTitle("Set a Date");
                dpDialog.getDatePicker().setCalendarViewShown(false);
            }
            return dpDialog;
        }

        @Override
        public void onDateSet(DatePicker view, int year, int month, int day) {
            try {
                aqPMSel.id(R.id.imgclearlmp).background(R.drawable.brush1);
                if (view.isShown()) {
                    String str = String.format("%02d", day) + "-" + String.format("%02d", month + 1) + "-"
                            + year;

                    int days = DateTimeUtil.getDaysBetweenDates(DateTimeUtil.getTodaysDate(), str);

                    if (!aqPMSel.id(R.id.radEDD).isChecked()) {
                        if (days >= 0) {
                            if (isRegDate) {
                                if (days < 30) {
                                    aqPMSel.id(R.id.etregdate).text(str);
                                    aqPMSel.id(R.id.imgclearregadate).visible();//20AUg2019 Arpitha
                                } else if (days > 120) {
                                    aqPMSel.id(R.id.etregdate).text("");
                                    aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                                    Toast.makeText(activity, getResources().getString(R.string.date_val_2_toast),
                                            Toast.LENGTH_LONG).show();
                                    aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha

                                } else {
                                    aqPMSel.id(R.id.etregdate).text(str);
                                    aqPMSel.id(R.id.imgclearregadate).visible();//20AUg2019 Arpitha
                                }

                            } else if (aqPMSel.id(R.id.radLMP).isChecked()) {
                                String lmporadd = str;
                                int daysdiff = 0;
                                String regdate = aqPMSel.id(R.id.etregdate).getText().toString();

                                if (regdate != null && regdate.trim().length() > 0) {
                                    daysdiff = DateTimeUtil.getDaysBetweenDates(regdate, lmporadd);

                                    int daysdifffromCUrr = DateTimeUtil
                                            .getDaysBetweenDates(DateTimeUtil.getTodaysDate(), lmporadd);

                                    if (daysdiff <= 30) {
                                        Toast.makeText(activity,
                                                getResources().getString(R.string.date_val_3_toast),
                                                Toast.LENGTH_LONG).show();
                                        aqPMSel.id(R.id.etlmpdate).text("");
                                        aqPMSel.id(R.id.etgest).text("");
                                        aqPMSel.id(R.id.etMotherAdd).text("");
                                        aqPMSel.id(R.id.imgclearlmp).gone();//20Aug2019 Arpitha
                                    } else if (daysdifffromCUrr > 280) {
                                        Toast.makeText(activity,
                                                getResources().getString(R.string.date_val_4_toast),
                                                Toast.LENGTH_LONG).show();
                                        aqPMSel.id(R.id.etlmpdate).text("");
                                        aqPMSel.id(R.id.etgest).text("");
                                        aqPMSel.id(R.id.etMotherAdd).text("");
                                        aqPMSel.id(R.id.imgclearlmp).gone();//20Aug2019 Arpitha
                                    } else {
                                        aqPMSel.id(R.id.etMotherAdd).text(str);
                                        calculateEddGestation("lmp");
                                        aqPMSel.id(R.id.imgclearlmp).visible();//20Aug2019 Arpitha
                                    }
                                } else
                                    Toast.makeText(activity, getResources().getString(R.string.plz_enter_reg_dt),
                                            Toast.LENGTH_LONG).show();
                            } else if (aqPMSel.id(R.id.rdbMother).isChecked()) {

                                int daysdiff = DateTimeUtil
                                        .getDaysBetweenDates(aqPMSel.id(R.id.etregdate).getText().toString(), str);
                                if (daysdiff > 365) {// Arpitha 28Jun2018 - 23Dec2019 Arpitha - allow max 1 year

                                    Toast.makeText(activity, (getResources().getString(R.string.datecannotbeafteroneyearfromcurrentdate)), // 11July2018
                                            // Arpitha
                                            Toast.LENGTH_LONG).show();
                                    aqPMSel.id(R.id.etMotherAdd).text("");
                                } else
                                    aqPMSel.id(R.id.etMotherAdd).text(str);
                            } else if (aqPMSel.id(R.id.rdbchild).isChecked()) {

                                int daysdiff = DateTimeUtil
                                        .getDaysBetweenDates(aqPMSel.id(R.id.etregdate).getText().toString(), str);
                                if (daysdiff > 5844) {// Arpitha 28Jun2018

                                    Toast.makeText(activity, (getResources().getString(R.string.date_val_7_toast)), // 11July2018
                                            // Arpitha
                                            Toast.LENGTH_LONG).show();
                                    aqPMSel.id(R.id.etMotherAdd).text("");
                                } else
                                    aqPMSel.id(R.id.etMotherAdd).text(str);
                            }
                        } else {
                            if (isRegDate) {
                                aqPMSel.id(R.id.etregdate).text("");
                                aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                                Toast.makeText(activity,
                                        getResources().getString(R.string.reg_date_cannot_greater_current_date),
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha
                            } else {
                                aqPMSel.id(R.id.etMotherAdd).text("");
                                aqPMSel.id(R.id.imgclearlmp).gone();//20AUg2019 Arpitha
                                aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                                if (aqPMSel.id(R.id.rdbPreg).isChecked()) {
                                    Toast.makeText(activity,
                                            getResources()
                                                    .getString(R.string.lmp_date_time_cannot_be_after_curre_datetime), // 11July2018

                                            Toast.LENGTH_LONG).show();

                                    aqPMSel.id(R.id.etlmpdate).text("");
                                    aqPMSel.id(R.id.etgest).text("");
                                } else {
                                    Toast.makeText(activity,
                                            getResources()
                                                    .getString(R.string.del_date_cannot_be_after_current_date_time), // 11July2018
                                            Toast.LENGTH_LONG).show();
                                }

                            }
                        }
                    } else {
                        if (!isRegDate) {
                            int days1 = 0;
                            if (aqPMSel.id(R.id.etregdate).getText().toString().trim().length() > 0)
                                days1 = DateTimeUtil.getDaysBetweenDates(str,
                                        aqPMSel.id(R.id.etregdate).getText().toString());

                            if (days1 > 0 && days1 < 273) {
                                aqPMSel.id(R.id.etMotherAdd).text(str);
                                calculateEddGestation("edd");
                                aqPMSel.id(R.id.imgclearlmp).visible();//20Aug2019 Arpitha
                            } else {
                                if (days1 > 280) {
                                    aqPMSel.id(R.id.etMotherAdd).text("");
                                    aqPMSel.id(R.id.imgclearlmp).gone();//21Aug2019 Arpitha
                                    aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                                    Toast.makeText(activity, getResources().getString(R.string.date_val_6_toast),

                                            Toast.LENGTH_LONG).show();

                                    aqPMSel.id(R.id.etlmpdate).text("");
                                    aqPMSel.id(R.id.etgest).text("");

                                } else {
                                    aqPMSel.id(R.id.etMotherAdd).text("");
                                    aqPMSel.id(R.id.imgclearlmp).gone();//21Aug2019 Arpitha
                                    aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                                    Toast.makeText(activity, getResources().getString(R.string.date_val_5_toast),
                                            Toast.LENGTH_LONG).show();
                                    aqPMSel.id(R.id.etlmpdate).text("");
                                    aqPMSel.id(R.id.etgest).text("");
                                }
                            }
                        } else {
                            days = DateTimeUtil.getDaysBetweenDates(DateTimeUtil.getTodaysDate(), str);
                            if (days >= 0) {

                                if (days > 90) {
                                    aqPMSel.id(R.id.etregdate).text("");
                                    aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                                    Toast.makeText(activity, getResources().getString(R.string.date_val_2_toast),

                                            Toast.LENGTH_LONG).show();
                                    aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha
                                } else {
                                    aqPMSel.id(R.id.etregdate).text(str);
                                }

                            } else {
                                aqPMSel.id(R.id.etregdate).text("");
                                aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                                Toast.makeText(activity, getResources().getString(R.string.reg_date_cannot_greater_current_date),

                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha
                            }
                        }
                    }

                }

            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
        }
    }

}

