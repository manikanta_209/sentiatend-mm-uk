package com.sc.stmansi.adolescent;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.sc.stmansi.R;
import com.sc.stmansi.adolescent.homevisit.AdolVisitViewCC;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.repositories.CaseMgmtRepository;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblAdolReg;
import com.sc.stmansi.tables.tblAdolVisitHeader;
import com.sc.stmansi.tables.tblCaseMgmt;

import java.util.List;

public class HomeVisitHistoryAdapter extends RecyclerView.Adapter<HomeVisitHistoryViewHolder> {
    private final Context context;
    private final List<tblAdolVisitHeader> allHomeVisit;
    private final tblAdolReg currentAdolDetails;
    private final Bundle bundle;
    private final DatabaseHelper databaseHelper;
    private final TblInstusers user;


    public HomeVisitHistoryAdapter(tblAdolReg currentAdolDetails, List<tblAdolVisitHeader> currentAdolHeader, Context applicationContext, Bundle bundle, DatabaseHelper databaseHelper, TblInstusers user) {
        this.currentAdolDetails = currentAdolDetails;
        context = applicationContext;
        allHomeVisit = currentAdolHeader;
        this.bundle = bundle;
        this.databaseHelper = databaseHelper;
        this.user = user;
    }
    @NonNull
    @Override
    public HomeVisitHistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CardView v = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_homevisithistory, parent, false);
        return new HomeVisitHistoryViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull HomeVisitHistoryViewHolder holder, int position) {
        try {
            holder.txtVisitCount.setText(context.getResources().getString(R.string.visitcount) + ":" + allHomeVisit.get(position).getVisitId());
            holder.txtVisitDate.setText(context.getResources().getString(R.string.visitdate) + ":" + allHomeVisit.get(position).getAdolvisHVisitDate());
            String dob = currentAdolDetails.getRegAdolDoB();
            String age = "";
            if (dob.length() == 0) {
                age = currentAdolDetails.getRegAdolAge();
            } else {
                age = DateTimeUtil.calculateAge(dob);
            }
            holder.txtVisitAge.setText("Age: " + age);
            holder.txtvisitGts.setText(allHomeVisit.get(position).getAdolvisHGTS().equals("Yes") ? context.getResources().getString(R.string.m121) : context.getResources().getString(R.string.m122));


            if (allHomeVisit.get(position).getAdolvisHComplications().equals("Yes")) {
                holder.imgComplication.setVisibility(View.VISIBLE);
            } else {
                holder.imgComplication.setVisibility(View.INVISIBLE);
            }

            boolean isfollowup = false;
            CaseMgmtRepository caseMgmtRepository = new CaseMgmtRepository(databaseHelper);
            tblCaseMgmt casemgmts = caseMgmtRepository.getCaseMgmtDetailsWoman(allHomeVisit.get(position).getAdolId(), allHomeVisit.get(position).getVisitId(), user.getUserId());
           // if (casemgmts!=null && casemgmts.getVisitNumbyMM()==Integer.parseInt(allHomeVisit.get(position).getVisitId().trim())&&casemgmts.getVisitType().equals("Followup")){
                if (casemgmts!=null && casemgmts.getVisitNumbyMM()==Integer.parseInt(allHomeVisit.get(position).getVisitId().trim())&&(casemgmts.getVisitType().equals("Followup") || casemgmts.getVisitType().equals("SMS"))){ // 13Aug2021 Bindu check follow up or SMS

                    holder.txtActionTaken.setVisibility(View.VISIBLE);
                isfollowup = true;
            }else{
                holder.txtActionTaken.setVisibility(View.GONE);
                if (currentAdolDetails.getRegAdolGender().equals("Female")) {
                    holder.txtcisitPregnant.setVisibility(View.VISIBLE);
                    holder.txtcisitPregnant.setText(allHomeVisit.get(position).getAdolvisHIsPregnantatReg().equals("Yes") ? context.getResources().getString(R.string.m121) : context.getResources().getString(R.string.m122));
                    holder.txtvisitPregnantDummy.setVisibility(View.VISIBLE);
                }
                isfollowup = false;
            }

            holder.txtActionTaken.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                        Intent intent;
                        intent = new Intent(context, AdolVisitViewCC.class);
                        intent.putExtra("globalState", bundle);
                        intent.putExtra("VisitCount", casemgmts.getVisitNum()+"");
                        intent.putExtra("adolID", casemgmts.getBeneficiaryID());
                        intent.putExtra("userTypeHV",casemgmts.getHcmCreatedByUserType());
                        context.startActivity(intent);
                }
            });


            holder.imgViewVisit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent;
                    intent = new Intent(context, AdolVisitView.class);
                    intent.putExtra("globalState", bundle);
                    intent.putExtra("VisitCount", allHomeVisit.get(position).getVisitId());
                    intent.putExtra("adolID", allHomeVisit.get(position).getAdolId());
                    context.startActivity(intent);
                }
            });

            holder.imgComplication.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        View complicationView = View.inflate(context, R.layout.customdialogavh_complication, null);
                        builder.setView(complicationView);
                        builder.setCancelable(false);
                        builder.setPositiveButton(context.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).create().show();
                        TextView txtCHIHead = complicationView.findViewById(R.id.txtcomplicationHealthIssueHead);
                        TextView txtCHIBody = complicationView.findViewById(R.id.txtcomplicationHealthIssueBody);
                        TextView txtCMPHead = complicationView.findViewById(R.id.txtcomplicationMenstrualproblemsHead);
                        TextView txtCMPBody = complicationView.findViewById(R.id.txtcomplicationMenstrualproblemsBody);
                        TextView txtHBHead = complicationView.findViewById(R.id.txtcomplicationHBHead);
                        TextView txtHBBody = complicationView.findViewById(R.id.txtcomplicationHBBody);
                        TextView txtWeightHead = complicationView.findViewById(R.id.txtcomplicationWeightHead);
                        TextView txtWeightBody = complicationView.findViewById(R.id.txtcomplicationWeightBody);
                        TextView txtHeightHead = complicationView.findViewById(R.id.txtcomplicationHeightHead);
                        TextView txtHeightBody = complicationView.findViewById(R.id.txtcomplicationHeightBody);
                        if (complicationHealthCheck(position).length() != 0) {
                            txtCHIHead.setVisibility(View.VISIBLE);
                            txtCHIBody.setText(complicationHealthCheck(position));
                            txtCHIBody.setVisibility(View.VISIBLE);
                        }
                        if (complicationMensturalCheck(position).length() != 0) {
                            txtCMPHead.setVisibility(View.VISIBLE);
                            txtCMPBody.setText(complicationMensturalCheck(position));
                            txtCMPBody.setVisibility(View.VISIBLE);
                        }
                        if (complicationHBCheck(position).length() != 0) {
                            txtHBHead.setVisibility(View.VISIBLE);
                            txtHBBody.setText(complicationHBCheck(position));
                            txtHBBody.setVisibility(View.VISIBLE);
                        }
                        if (complicationWeightCheck(position).length() != 0) {
                            txtWeightHead.setVisibility(View.VISIBLE);
                            txtWeightBody.setText(complicationWeightCheck(position));
                            txtWeightBody.setVisibility(View.VISIBLE);
                        }
                        if (complicationHeightCheck(position).length() != 0) {
                            txtHeightHead.setVisibility(View.VISIBLE);
                            txtHeightBody.setText(complicationHeightCheck(position));
                            txtHeightBody.setVisibility(View.VISIBLE);
                        }
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());

                    }
                }
            });
            complicationHealthCheck(position);
            complicationMensturalCheck(position);
            complicationHBCheck(position);
            complicationWeightCheck(position);
            complicationHeightCheck(position);
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private String complicationHeightCheck(int position) {
        try {
            if (allHomeVisit.get(position).getAdolvisHHeightType().equals("1")) {
                if (allHomeVisit.get(position).getAdolvisHHeight().length() != 0) {
                    double cm = Double.parseDouble((allHomeVisit.get(position).getAdolvisHHeight()));
                    if (showHeightWarning(cm)) {
                        return context.getResources().getString(R.string.heightis) + cm + " " + context.getResources().getString(R.string.centimeter);
                    }
                }
            } else if (allHomeVisit.get(position).getAdolvisHHeightType().equals("2")) {
                String height = allHomeVisit.get(position).getAdolvisHHeight();
                String[] feetandinches = height.split("\\.");
//                int i = 0;
//                StringBuilder Feet = new StringBuilder();
//                for (i = 0; i < height.length(); i++) {
//                    char c = height.charAt(i);
//                    if (c == '.') {
//                        break;
//                    } else {
//                        Feet.append(c);
//                    }
//                }
//                int selectfeet = Integer.parseInt(Feet.toString()) - 1;
//                i++;
//                String inches = "";
//                for (; i < height.length(); i++) {
//                    char c = height.charAt(i);
////                        if (c == '\"') {
////                            break;
////                        } else {
//                    inches = inches + c;
////                        }
//                }
                int selectfeet = Integer.parseInt(feetandinches[0]);
                int inch = Integer.parseInt(feetandinches[1]);
                double cm = 30.48 * selectfeet;
                cm = cm + (inch * 2.54);

                if (showHeightWarning(cm))
                    return context.getResources().getString(R.string.heightis) + selectfeet + "'" + inch + "\"" + " " + context.getResources().getString(R.string.feet);

            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return "";
    }

    private String complicationWeightCheck(int position) {
        if (allHomeVisit.get(position).getAdolvisHWeight().length() > 0) {
            double weight = Double.parseDouble(allHomeVisit.get(position).getAdolvisHWeight());
            if (showWeightWarning(weight)) {
                return context.getResources().getString(R.string.weightis) + allHomeVisit.get(position).getAdolvisHWeight() +" "+ context.getResources().getString(R.string.kgs);
            }
        }
        return "";
    }

    private String complicationHBCheck(int position) {
        try {
            if (allHomeVisit.get(position).getAdolvisHHb().length() != 0) {
                double hb = Double.parseDouble(allHomeVisit.get(position).getAdolvisHHb());
                if (hb < 8.0) {
                    return context.getResources().getString(R.string.severe);
                } else if (hb >= 8 && hb <= 9.9) {
                    return context.getResources().getString(R.string.moderate);
                } else if (hb >= 10 && hb <= 11.9) {
                    return context.getResources().getString(R.string.mild);
                } else if (hb >= 11.9 && hb <= 24) {
                    //
                } else if (hb > 24) {
                    return context.getResources().getString(R.string.hbis) + " " + hb;
                }
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return "";
    }

    private String complicationMensturalCheck(int position) {
        try {
            if (currentAdolDetails.getRegAdolGender().equals("Female") && allHomeVisit.get(position).getAdolvisHIsPeriodstarted().equals("Yes") && allHomeVisit.get(position).getAdolvisHMenstrualProblem().length() != 0) {
                String[] menustralproblems = context.getResources().getStringArray(R.array.menustralproblems);
                String mProblems = allHomeVisit.get(position).getAdolvisHMenstrualProblem();
                if (mProblems.length() > 0) {
                    String[] mProblemsaftersplit = mProblems.split(",");
                    StringBuilder originalMensutralProblems = new StringBuilder();
                    for (String s : mProblemsaftersplit) {
                        if (!s.trim().equals("8")) {
                            originalMensutralProblems.append(menustralproblems[Integer.parseInt(s.trim())]).append(",");
                        } else {
                            originalMensutralProblems.append(allHomeVisit.get(position).getAdolvisHMenstrualProblemOthers()).append(",");
                        }
                    }
                    originalMensutralProblems = originalMensutralProblems.deleteCharAt(originalMensutralProblems.lastIndexOf(","));
                    return String.valueOf(originalMensutralProblems);
                } else {
                    return "";
                }
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return "";
    }

    private String complicationHealthCheck(int position) {
        try {
            if (allHomeVisit.get(position).getAdolvisHHealthIssues().length() != 0 || allHomeVisit.get(position).getAdolvisHHealthIssuesOthers().length() != 0) {
                String[] healthissues = context.getResources().getStringArray(R.array.healthproblemsuffer);
                StringBuilder Hissues = new StringBuilder();
                String indices = allHomeVisit.get(position).getAdolvisHHealthIssues();
                if (allHomeVisit.get(position).getAdolvisHHealthIssuesOthers() != null) {
                    if (allHomeVisit.get(position).getAdolvisHHealthIssues().length()>0) {
                        String[] indicesvalues = indices.split(",");
                        for (String s : indicesvalues) {
                            if (!s.trim().equals("9"))
                                Hissues.append(healthissues[Integer.parseInt(s.trim())]).append(", ");
                            else
                                Hissues.append(allHomeVisit.get(position).getAdolvisHHealthIssuesOthers()).append(", ");
                        }
                        Hissues = Hissues.deleteCharAt(Hissues.lastIndexOf(","));
                        return Hissues.toString();
                    } else {
                        return healthissues[Integer.parseInt(allHomeVisit.get(position).getAdolvisHHealthIssues())] + allHomeVisit.get(position).getAdolvisHHealthIssuesOthers();
                    }
                } else {
                    if (indices.contains(",")) {
                        String[] indicesvalues = indices.split(",");
                        for (String s : indicesvalues) {
                            if (!s.trim().equals("9"))
                                Hissues.append(healthissues[Integer.parseInt(s.trim())]).append(", ");
                        }
                        Hissues = Hissues.deleteCharAt(Hissues.lastIndexOf(","));
                        return Hissues.toString();
                    } else {
                        if (allHomeVisit.get(position).getAdolvisHHealthIssues().length() != 0)
                            if (allHomeVisit.get(position).getAdolvisHHealthIssues().equals("No")) {
                                return "";
                            } else {
                                return healthissues[Integer.parseInt(allHomeVisit.get(position).getAdolvisHHealthIssues())];
                            }
                        else
                            return "";
                    }
                }
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return "";
    }

    @Override
    public int getItemCount() {
        return allHomeVisit.size();
    }

    private boolean showWeightWarning(double weight) {
        try {
            switch (Integer.parseInt(currentAdolDetails.getRegAdolAge())) {
                case 10:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(weight >= 33 && weight <= 46)) {
                            return true;
                        }
                    } else {
                        if (!(weight >= 33 && weight <= 47)) {
                            return true;
                        }
                    }
                    break;
                case 11:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(weight >= 36 && weight <= 53)) {
                            return true;
                        }
                    } else {
                        if (!(weight >= 37 && weight <= 55)) {
                            return true;
                        }
                    }
                    break;
                case 12:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(weight >= 41 && weight <= 58)) {
                            return true;
                        }
                    } else {
                        if (!(weight >= 42 && weight <= 61)) {
                            return true;
                        }
                    }
                    break;
                case 13:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(weight >= 46 && weight <= 66)) {
                            return true;
                        }
                    } else {
                        if (!(weight >= 46 && weight <= 66)) {
                            return true;
                        }
                    }
                    break;
                case 14:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(weight >= 51 && weight <= 72)) {
                            return true;
                        }
                    } else {
                        if (!(weight >= 49 && weight <= 72)) {
                            return true;
                        }
                    }

                    break;
                case 15:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(weight >= 56 && weight <= 79)) {
                            return true;
                        }
                    } else {
                        if (!(weight >= 53 && weight <= 76)) {
                            return true;
                        }
                    }

                    break;
                case 16:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(weight >= 61 && weight <= 84)) {
                            return true;
                        }
                    } else {
                        if (!(weight >= 54 && weight <= 77)) {
                            return true;
                        }
                    }
                    break;
                case 17:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(weight >= 64 && weight <= 88)) {
                            return true;
                        }
                    } else {
                        if (!(weight >= 55 && weight <= 79)) {
                            return true;
                        }
                    }
                    break;
                case 18:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(weight >= 67 && weight <= 92)) {
                            return true;
                        }
                    } else {
                        if (!(weight >= 56 && weight <= 81)) {
                            return true;
                        }
                    }
                    break;
                case 19:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(weight >= 69 && weight <= 93)) {
                            return true;
                        }
                    } else {
                        if (!(weight >= 57 && weight <= 83)) {
                            return true;
                        }
                    }
                    break;
                default:

            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return false;
    }

    private boolean showHeightWarning(double cm) {
        try {
            switch (Integer.parseInt(currentAdolDetails.getRegAdolAge())) {
                case 10:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(cm >= 139 && cm <= 150)) {
                            return true;
                        }
                    } else {
                        if (!(cm >= 139 && cm <= 150)) {
                            return true;
                        }
                    }
                    break;
                case 11:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(cm >= 144 && cm <= 155)) {
                            return true;
                        }
                    } else {
                        if (!(cm >= 144 && cm <= 156)) {
                            return true;
                        }
                    }

                    break;
                case 12:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(cm >= 149 && cm <= 163)) {
                            return true;
                        }
                    } else {
                        if (!(cm >= 151 && cm <= 163)) {
                            return true;
                        }
                    }
                    break;
                case 13:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(cm >= 156 && cm <= 169)) {
                            return true;
                        }
                    } else {
                        if (!(cm >= 156 && cm <= 169)) {
                            return true;
                        }
                    }
                    break;
                case 14:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(cm >= 166 && cm <= 176)) {
                            return true;
                        }
                    } else {
                        if (!(cm >= 161 && cm <= 173)) {
                            return true;
                        }
                    }
                    break;
                case 15:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(cm >= 170 && cm <= 183)) {
                            return true;
                        }
                    } else {
                        if (!(cm >= 162 && cm <= 173)) {
                            return true;
                        }
                    }

                    break;
                case 16:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(cm >= 174 && cm <= 186)) {
                            return true;
                        }
                    } else {
                        if (!(cm >= 163 && cm <= 173)) {
                            return true;
                        }
                    }
                    break;
                case 17:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(cm >= 175 && cm <= 188)) {
                            return true;
                        }
                    }
                case 18:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(cm >= 176 && cm <= 189)) {
                            return true;
                        }
                    }
                case 19:
                    if (currentAdolDetails.getRegAdolGender().equals("Male")) {
                        if (!(cm >= 179 && cm <= 189)) {
                            return true;
                        }
                    } else {
                        if (!(cm >= 163 && cm <= 174)) {
                            return true;
                        }
                    }
                    break;
                default:

            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return false;
    }
}
