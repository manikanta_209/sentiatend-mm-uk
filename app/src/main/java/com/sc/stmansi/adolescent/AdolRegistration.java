package com.sc.stmansi.adolescent;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager.widget.ViewPager;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.sc.stmansi.HomeVisit.HomeVisit;
import com.sc.stmansi.HomeVisit.HomeVisitListActivity;
import com.sc.stmansi.R;
import com.sc.stmansi.SanNap.List_SanNap;
import com.sc.stmansi.adolanchistory.AdolANCHistory;
import com.sc.stmansi.childregistration.ChildRegistrationActivity;
import com.sc.stmansi.common.AlertDialogUtil;
import com.sc.stmansi.common.CommonDatePickerFragment;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.common.NavDrawerItem;
import com.sc.stmansi.common.NavDrawerListAdapter;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.configuration.SyncState;
import com.sc.stmansi.database.DBMethods;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.initialization.SchemaCreator;
import com.sc.stmansi.registration.RegistrationActivity;
import com.sc.stmansi.repositories.AdolPregnantRepository;
import com.sc.stmansi.repositories.AdolescentRepository;
import com.sc.stmansi.repositories.AuditRepository;
import com.sc.stmansi.repositories.ChildRepository;
import com.sc.stmansi.repositories.FacilityRepository;
import com.sc.stmansi.repositories.TransactionHeaderRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.tables.AuditPojo;
import com.sc.stmansi.tables.TblChildInfo;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblAdolPregnant;
import com.sc.stmansi.tables.tblAdolReg;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.viewprofile.ViewProfileActivity;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class AdolRegistration extends AppCompatActivity implements View.OnTouchListener, DatePickerDialog.OnDateSetListener, View.OnClickListener {

    private AQuery aq;
    private RadioGroup radiogroupHeight;
    private RadioGroup rg_pregnant;
    private RadioGroup rg_gender;
    private RadioGroup rg_isPeriod;
    private RadioGroup rg_topTitle;
    private DatabaseHelper databaseHelper;
    private DBMethods dbMethods;
    private Button btToolbarHome;
    private boolean isEdit = false, childtoAdol = false;
    private String AdolID;
    private TableRow tr_pregnant;
    private tblAdolReg adolEditData;
    private int transID = 0;
    private AppState appState;
    private TblInstusers user;
    private SyncState syncState;
    private LinearLayout llMaternal;
    private tblAdolReg tblAdolReg;
    private DrawerLayout drawer;
    private ArrayList<tblAdolPregnant> bornlist;
    private EditText currentEditText;
    private boolean isReadytoInsert;
    private ScrollView scrollview;
    private ViewPager viewpager;
    Map<String, Integer> facilities;
    //    20May2021 Bindu add
    private ArrayList<String> healthIssuesListStore;
    private ArrayList<String> lifeStyleListStore;
    private ArrayList<String> menstrualPbmListStore;
    private ArrayList<String> physicalActListStore;
    CheckBox cb_Backpain;
    CheckBox cb_StomachPain;
    CheckBox cb_Fatigue;
    CheckBox cb_Vomiting;
    CheckBox cb_Diarrhea;
    CheckBox cb_Iritability;
    CheckBox cb_Bloodloss;
    CheckBox cb_Moodiness;
    CheckBox cb_CycleIssue;
    CheckBox cb_Cramping;
    CheckBox cb_overbleeding;
    CheckBox cb_headache;
    CheckBox cb_earlypregnancy;
    RadioButton rbIsPeriodYes;
    RadioButton rbIsPeriodNo;
    String menstrualPbms = "", healthIssues = "", lifestyle = "", physicalActivity = "";
    CheckBox cbAnemia;
    CheckBox cbUndernut;
    CheckBox cbObesity;
    CheckBox cbInjuries;
    CheckBox cbDeprandAnxi;
    CheckBox cbHivandAids;
    CheckBox cbInfectionDis;
    CheckBox cbOther;
    CheckBox cbAlcohol;
    CheckBox cbDrugs;
    CheckBox cbTobacco;
    CheckBox cbSmoking;
    CheckBox cbWalking;
    CheckBox cbJogging;
    CheckBox cbYoga;
    CheckBox cbWorkout;
    CheckBox cbMeditation;
    //    04Sep2021 Arpitha
    tblregisteredwomen editedWomanData;
    tblregisteredwomen regData;
    String updateSQLReg = "";
    String womanId;
    private AQuery aqPMSel;
    private Activity activity;
    RadioGroup radGrpEDDorLmp;
    private boolean isRegDate;
    Boolean wantToCloseDialog = false;
    private int noOfDays;
    private tblregisteredwomen woman;

    @Override
    public void onBackPressed() {
        try {
            displayDialogExit();
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private void displayDialogExit() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setMessage(getResources().getString(R.string.m110));
        alert.setNegativeButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(AdolRegistration.this, AdolescentHome.class);
                intent.putExtra("tabItem", 0);
                intent.putExtra("globalState", prepareBundle());
                startActivity(intent);
            }
        });
        alert.setPositiveButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alert.create().show();
    }

    @SuppressLint("SetTextI18n")
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adolregister);
        try {
            Initalize();
            clickListeners();
            Bundle bundle = getIntent().getBundleExtra("globalState");
            appState = bundle.getParcelable("appState");
            syncState = bundle.getParcelable("syncState");
            isEdit = getIntent().getBooleanExtra("isEdit", false);
            tblAdolReg = (tblAdolReg) getIntent().getSerializableExtra("AdolReg");
            childtoAdol = getIntent().getBooleanExtra("isChildtoAdol", false);

            UserRepository userRepository = new UserRepository(databaseHelper);
            user = userRepository.getOneAuditedUser(appState.ashaId);
            transID = TransactionHeaderRepository.iCreateNewTransNew(user.getUserId(), databaseHelper); //23May2021 Bindu set userid
            getFacilitiesData();

            activity = AdolRegistration.this;

            if (isEdit) {
                AdolID = getIntent().getStringExtra("adolID");
                AdolescentRepository adolescentRepository = new AdolescentRepository(databaseHelper);
                adolEditData = adolescentRepository.getAdolDatafromAdolID(AdolID);
                InitializeDrawer();
                PreSetUIChanges();
                setEditData(adolEditData);
                TextView textViewTitle = findViewById(R.id.toolbartxtTitle);
                textViewTitle.setText(getResources().getString(R.string.viewprofile) + " - " + adolEditData.getRegAdolName());

                if (adolEditData.getRegAdolNoofChild() != 0) {
                    aq.id(R.id.spnnoofchild).getSpinner().setEnabled(false);
                } else {
                    aq.id(R.id.spnnoofchild).getSpinner().setEnabled(true);
                }
                if (adolEditData.getRegAdolDeactivateDate().trim().length() > 0) {
                    aq.id(R.id.btnsave).getImageView().setVisibility(View.GONE); // to hide the save button on deactive adols
                }

            } else if (childtoAdol) { //Ramesh 1-Sep-2021
                aq.id(R.id.etAdsname).getEditText().setText(tblAdolReg.getRegAdolName());
                aq.id(R.id.etAdsdob).getEditText().setText(tblAdolReg.getRegAdolDoB());
                calculateAge();

                if (tblAdolReg.getRegAdolGender().equals("Male")) {
                    aq.id(R.id.rd_male).checked(true);
                } else if (tblAdolReg.getRegAdolGender().equals("Female")) {
                    aq.id(R.id.rd_female).checked(true);
                    tr_pregnant.setVisibility(View.VISIBLE);
                    aq.id(R.id.tr_ARIsPeriod).getView().setVisibility(View.VISIBLE);
                }

                aq.id(R.id.etAdsWeight).getEditText().setText(tblAdolReg.getRegAdolWeight());

                if (tblAdolReg.getRegAdolMother() != null) {
                    aq.id(R.id.etadsmothername).getEditText().setText(tblAdolReg.getRegAdolMother());
                }
                if (tblAdolReg.getRegAdolMotherDesc() != null && tblAdolReg.getRegAdolMotherDesc().equals("Yes")) {
                    aq.id(R.id.cb_motherdesc).checked(true);
                }
                if (tblAdolReg.getRegAdolFather() != null) {
                    aq.id(R.id.etadsfathername).getEditText().setText(tblAdolReg.getRegAdolFather());
                }
                if (tblAdolReg.getRegAdolFatherDesc() != null && tblAdolReg.getRegAdolFatherDesc().equals("Yes")) {
                    aq.id(R.id.cb_fatherdesc).checked(true);
                }
                if (tblAdolReg.getRegAdolGuardian() != null) {
                    aq.id(R.id.etadsguardianname).getEditText().setText(tblAdolReg.getRegAdolGuardian());
                }
                if (tblAdolReg.getRegAdolGoingtoSchool() != null) {
                    if (tblAdolReg.getRegAdolGoingtoSchool().equals("Yes")) {
                        aq.id(R.id.rd_goingschoolYes).checked(true);
                    } else if (tblAdolReg.getRegAdolGoingtoSchool().equals("No")) {
                        aq.id(R.id.rd_goingschoolNo).checked(true);
                    }
                }

                int villageName = getKey(facilities, String.valueOf(tblAdolReg.getRegAdolFacilities()));
                aq.id(R.id.spnfacilities).getSpinner().setSelection(villageName);

                drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                AdolID = userRepository.getNewAdolId(user);
                TextView textViewTitle = findViewById(R.id.toolbartxtTitle);
                textViewTitle.setText(getResources().getString(R.string.adolescentregister) + " - " + DateTimeUtil.getTodaysDate());
            } else {
                drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                AdolID = userRepository.getNewAdolId(user);
                TextView textViewTitle = findViewById(R.id.toolbartxtTitle);
                textViewTitle.setText(getResources().getString(R.string.adolescentregister) + " - " + DateTimeUtil.getTodaysDate());
            }

            rg_topTitle.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    if (i == R.id.rb_top_basic) {
                        aq.id(R.id.txtAdolRegTitle).getTextView().setText(getResources().getText(R.string.basic_details));
                        aq.id(R.id.btnsave).getImageView().setImageResource(R.drawable.ic_arrow_forward);
                        aq.id(R.id.llbasicView).getView().setVisibility(View.VISIBLE);
                        aq.id(R.id.llhealthissueView).getView().setVisibility(View.GONE);
                        isReadytoInsert = false;
                    } else if (i == R.id.rb_top_health) {
                        aq.id(R.id.txtAdolRegTitle).getTextView().setText(getResources().getText(R.string.healthdetails));
                        isReadytoInsert = true;
                        aq.id(R.id.btnsave).getImageView().setImageResource(R.drawable.save);
                        aq.id(R.id.llbasicView).getView().setVisibility(View.GONE);
                        aq.id(R.id.llhealthissueView).getView().setVisibility(View.VISIBLE);
                    }
                }
            });

            aq.id(R.id.spnAdsHeightNumeric).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    showHeightWarning(0);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            aq.id(R.id.spnAdsHeightDecimal).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    showHeightWarning(0);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            aq.id(R.id.imgAdscleardob).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    aq.id(R.id.etAdsdob).text("");
                    aq.id(R.id.imgAdscleardob).getImageView().setVisibility(View.GONE);
                }
            });

            aq.id(R.id.et_AR_MenstruatedAge).getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    validationAgeatMensturated();
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

            aq.id(R.id.etadsAadharCard).getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    ValidationIsAadharCard();
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

            aq.id(R.id.etAdsage).getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (validationAge())
                        checkDobandandMatch();
                    calculateBornDate("AGE");
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

//            aq.id(R.id.spnnoofchild).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                @Override
//                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                    aq.id(R.id.spnnoofchild).getSpinner().setEnabled(true);
//                    if (i == 0) {
//                        aq.id(R.id.llPP_FirstBorn).getView().setVisibility(View.GONE);
//                        aq.id(R.id.llPP_SecondBorn).getView().setVisibility(View.GONE);
//                        aq.id(R.id.llPP_ThirdBorn).getView().setVisibility(View.GONE);
//                        aq.id(R.id.llPP_FourthBorn).getView().setVisibility(View.GONE);
//                        for (int ini = 1; ini <= 4; ini++) {
//                            aq.id(R.id.spnPregnantoutcomeofPregnancy + ini).getSpinner().setSelection(0);
//                            aq.id(R.id.etPregnantDOD + ini).getEditText().setText("");
//                            aq.id(R.id.spnPregnantwhereDeliverBaby + ini).getSpinner().setSelection(0);
//                            aq.id(R.id.spnPregnantwhoConductedDelivery + ini).getSpinner().setSelection(0);
//                            aq.id(R.id.spnPregnantwhomodeofTransport + ini).getSpinner().setSelection(0);
//                            aq.id(R.id.spnPregnanttypeofdelivery + ini).getSpinner().setSelection(0);
//                            aq.id(R.id.spnPregnantstatusofdelivery + ini).getSpinner().setSelection(0);
//                            aq.id(R.id.spnPregnantsexofchild + ini).getSpinner().setSelection(0);
//                            aq.id(R.id.etPregnantAgeatCurrentDate + ini).getEditText().setText("");
//                        }
//                    }
//                    if (i == 1) {
//                        aq.id(R.id.llPP_FirstBorn).getView().setVisibility(View.GONE);
//                        aq.id(R.id.llPP_SecondBorn).getView().setVisibility(View.GONE);
//                        aq.id(R.id.llPP_ThirdBorn).getView().setVisibility(View.GONE);
//                        aq.id(R.id.llPP_FourthBorn).getView().setVisibility(View.GONE);
//                        for (int ini = 1; ini <= 4; ini++) {
//                            aq.id(R.id.spnPregnantoutcomeofPregnancy + ini).getSpinner().setSelection(0);
//                            aq.id(R.id.etPregnantDOD + ini).getEditText().setText("");
//                            aq.id(R.id.spnPregnantwhereDeliverBaby + ini).getSpinner().setSelection(0);
//                            aq.id(R.id.spnPregnantwhoConductedDelivery + ini).getSpinner().setSelection(0);
//                            aq.id(R.id.spnPregnantwhomodeofTransport + ini).getSpinner().setSelection(0);
//                            aq.id(R.id.spnPregnanttypeofdelivery + ini).getSpinner().setSelection(0);
//                            aq.id(R.id.spnPregnantstatusofdelivery + ini).getSpinner().setSelection(0);
//                            aq.id(R.id.spnPregnantsexofchild + ini).getSpinner().setSelection(0);
//                            aq.id(R.id.etPregnantAgeatCurrentDate + ini).getEditText().setText("");
//                        }
//                    }
//                    if (i == 2) {
//                        aq.id(R.id.llPP_FirstBorn).getView().setVisibility(View.VISIBLE);
//                        aq.id(R.id.llPP_SecondBorn).getView().setVisibility(View.GONE);
//                        aq.id(R.id.llPP_ThirdBorn).getView().setVisibility(View.GONE);
//                        aq.id(R.id.llPP_FourthBorn).getView().setVisibility(View.GONE);
//                        //scrollview.smoothScrollTo(0, aq.id(R.id.llBasicdetails).getView().getBottom());
//                        for (int ini = 2; ini <= 4; ini++) {
//                            aq.id(R.id.spnPregnantoutcomeofPregnancy + ini).getSpinner().setSelection(0);
//                            aq.id(R.id.etPregnantDOD + ini).getEditText().setText("");
//                            aq.id(R.id.spnPregnantwhereDeliverBaby + ini).getSpinner().setSelection(0);
//                            aq.id(R.id.spnPregnantwhoConductedDelivery + ini).getSpinner().setSelection(0);
//                            aq.id(R.id.spnPregnantwhomodeofTransport + ini).getSpinner().setSelection(0);
//                            aq.id(R.id.spnPregnanttypeofdelivery + ini).getSpinner().setSelection(0);
//                            aq.id(R.id.spnPregnantstatusofdelivery + ini).getSpinner().setSelection(0);
//                            aq.id(R.id.spnPregnantsexofchild + ini).getSpinner().setSelection(0);
//                            aq.id(R.id.etPregnantAgeatCurrentDate + ini).getEditText().setText("");
//                        }
//                    } else if (i == 3) {
//                        aq.id(R.id.llPP_FirstBorn).getView().setVisibility(View.VISIBLE);
//                        aq.id(R.id.llPP_SecondBorn).getView().setVisibility(View.VISIBLE);
//                        aq.id(R.id.llPP_ThirdBorn).getView().setVisibility(View.GONE);
//                        aq.id(R.id.llPP_FourthBorn).getView().setVisibility(View.GONE);
//                        //scrollview.smoothScrollTo(0, aq.id(R.id.llBasicdetails).getView().getBottom());
//                        for (int ini = 3; ini <= 4; ini++) {
//                            aq.id(R.id.spnPregnantoutcomeofPregnancy + ini).getSpinner().setSelection(0);
//                            aq.id(R.id.etPregnantDOD + ini).getEditText().setText("");
//                            aq.id(R.id.spnPregnantwhereDeliverBaby + ini).getSpinner().setSelection(0);
//                            aq.id(R.id.spnPregnantwhoConductedDelivery + ini).getSpinner().setSelection(0);
//                            aq.id(R.id.spnPregnantwhomodeofTransport + ini).getSpinner().setSelection(0);
//                            aq.id(R.id.spnPregnanttypeofdelivery + ini).getSpinner().setSelection(0);
//                            aq.id(R.id.spnPregnantstatusofdelivery + ini).getSpinner().setSelection(0);
//                            aq.id(R.id.spnPregnantsexofchild + ini).getSpinner().setSelection(0);
//                            aq.id(R.id.etPregnantAgeatCurrentDate + ini).getEditText().setText("");
//                        }
//                    } else if (i == 4) {
//                        aq.id(R.id.llPP_FirstBorn).getView().setVisibility(View.VISIBLE);
//                        aq.id(R.id.llPP_SecondBorn).getView().setVisibility(View.VISIBLE);
//                        aq.id(R.id.llPP_ThirdBorn).getView().setVisibility(View.VISIBLE);
//                        aq.id(R.id.llPP_FourthBorn).getView().setVisibility(View.GONE);
//                        //scrollview.smoothScrollTo(0, aq.id(R.id.llBasicdetails).getView().getBottom());
//                        for (int ini = 4; ini <= 4; ini++) {
//                            aq.id(R.id.spnPregnantoutcomeofPregnancy + ini).getSpinner().setSelection(0);
//                            aq.id(R.id.etPregnantDOD + ini).getEditText().setText("");
//                            aq.id(R.id.spnPregnantwhereDeliverBaby + ini).getSpinner().setSelection(0);
//                            aq.id(R.id.spnPregnantwhoConductedDelivery + ini).getSpinner().setSelection(0);
//                            aq.id(R.id.spnPregnantwhomodeofTransport + ini).getSpinner().setSelection(0);
//                            aq.id(R.id.spnPregnanttypeofdelivery + ini).getSpinner().setSelection(0);
//                            aq.id(R.id.spnPregnantstatusofdelivery + ini).getSpinner().setSelection(0);
//                            aq.id(R.id.spnPregnantsexofchild + ini).getSpinner().setSelection(0);
//                            aq.id(R.id.etPregnantAgeatCurrentDate + ini).getEditText().setText("");
//                        }
//                    } else if (i == 5) {
//                        aq.id(R.id.llPP_FirstBorn).getView().setVisibility(View.VISIBLE);
//                        aq.id(R.id.llPP_SecondBorn).getView().setVisibility(View.VISIBLE);
//                        aq.id(R.id.llPP_ThirdBorn).getView().setVisibility(View.VISIBLE);
//                        aq.id(R.id.llPP_FourthBorn).getView().setVisibility(View.VISIBLE);
//                        ////scrollview.smoothScrollTo(0, aq.id(R.id.llBasicdetails).getView().getBottom());
//                    }
//                    if (isEdit) {//to get the born details in edit when noofchild from 0 to something
//                        try {
//                            AdolPregnantRepository adolPregnantRepository = new AdolPregnantRepository(databaseHelper);
//                            List<tblAdolPregnant> allborns = new ArrayList<>();
//                            allborns.add(new tblAdolPregnant());
//                            allborns.addAll(adolPregnantRepository.getPregnantsbyIDs(AdolID));
//                            for (int ini = 1; ini <= allborns.size(); ini++) {
//                                int index = allborns.get(ini).getTblAdolPreOutcome();
//                                aq.id(R.id.spnPregnantoutcomeofPregnancy + ini).getSpinner().setSelection(index);
//                                aq.id(R.id.spnPregnantoutcomeofPregnancy + ini).getSpinner().setEnabled(false);
//
//                                String text = allborns.get(ini).getTblAdolPreDoD();
//                                aq.id(R.id.etPregnantDOD + ini).getEditText().setText(text);
//                                aq.id(R.id.etPregnantDOD + ini).getEditText().setEnabled(false);
//
//                                aq.id(R.id.spnPregnantwhereDeliverBaby + ini).getSpinner().setSelection(allborns.get(ini).getTblAdolPreDeliverWhere());
//                                aq.id(R.id.spnPregnantwhereDeliverBaby + ini).getSpinner().setEnabled(false);
//
//                                aq.id(R.id.spnPregnantwhoConductedDelivery + ini).getSpinner().setSelection(allborns.get(ini).getTblAdolPreWhoConductDelivery());
//                                aq.id(R.id.spnPregnantwhoConductedDelivery + ini).getSpinner().setEnabled(false);
//
//                                aq.id(R.id.spnPregnantwhomodeofTransport + ini).getSpinner().setSelection(allborns.get(ini).getTblAdolPreTransport());
//                                aq.id(R.id.spnPregnantwhomodeofTransport + ini).getSpinner().setEnabled(false);
//
//                                aq.id(R.id.spnPregnanttypeofdelivery + ini).getSpinner().setSelection(allborns.get(ini).getTblAdolPreDeliveryType());
//                                aq.id(R.id.spnPregnanttypeofdelivery + ini).getSpinner().setEnabled(false);
//
//                                aq.id(R.id.spnPregnantstatusofdelivery + ini).getSpinner().setSelection(allborns.get(ini).getTblAdolPreDeliveryStatus());
//                                aq.id(R.id.spnPregnantstatusofdelivery + ini).getSpinner().setEnabled(false);
//
//                                aq.id(R.id.spnPregnantsexofchild + ini).getSpinner().setSelection(allborns.get(ini).getTblAdolPreChildSex());
//                                aq.id(R.id.spnPregnantsexofchild + ini).getSpinner().setEnabled(false);
//
//                                aq.id(R.id.etPregnantAgeatCurrentDate + ini).getEditText().setText(allborns.get(ini).getTblAdolPreChildAge());
//                                aq.id(R.id.etPregnantAgeatCurrentDate + ini).getEditText().setEnabled(false);
//                            }
//                        } catch (Exception e) {
//                            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
//                            crashlytics.log(e.getMessage());
//
//                        }
//                    }
//                }
//
//                @Override
//                public void onNothingSelected(AdapterView<?> adapterView) {
//
//                }
//            });

            aq.id(R.id.etAdsheightcm).getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    validationHeight();
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

            aq.id(R.id.etAdsWeight).getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    validationWeight();
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

            aq.id(R.id.etAdsphone).getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (aq.id(R.id.etAdsphone).getEditText().getText().toString().length() != 0) {
                        validationMobile();
                    } else if (aq.id(R.id.etAdsphone).getEditText().getText().toString().length() == 0) {
                        aq.id(R.id.spnphn).getSpinner().setSelection(0);
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });


            aq.id(R.id.spnRelationship).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    TableRow tableRowHusband = findViewById(R.id.trHusbandName);
//                    TableRow tableRowChild = findViewById(R.id.trNoofChild);
                    TableRow tableRowHusbandOccupa = findViewById(R.id.trHusbandOccupa);
                    TableRow tableRowMarriageType = findViewById(R.id.tr_marriagetype);
                    if (position == 0) {
//                        tableRowChild.setVisibility(View.GONE);
                        tableRowHusband.setVisibility(View.GONE);
                        tableRowHusbandOccupa.setVisibility(View.GONE);
                        tableRowMarriageType.setVisibility(View.GONE);
                        aq.id(R.id.etadshusbandname).text("");
                        aq.id(R.id.etadshusbandoccupa).text("");
                        aq.id(R.id.spnmarriagetype).getSpinner().setSelection(0);
                        aq.id(R.id.spnnoofchild).getSpinner().setSelection(0);
                        int size = aq.id(R.id.spnnoofchild).getSpinner().getSelectedItemPosition();
                        for (int i = 1; i <= size; i++) {
                            aq.id(R.id.spnPregnantoutcomeofPregnancy + i).getSpinner().setSelection(0);
                            aq.id(R.id.etPregnantDOD + i).getEditText().setText("");
                            aq.id(R.id.spnPregnantwhereDeliverBaby + i).getSpinner().setSelection(0);
                            aq.id(R.id.spnPregnantwhoConductedDelivery + i).getSpinner().setSelection(0);
                            aq.id(R.id.spnPregnantwhomodeofTransport + i).getSpinner().setSelection(0);
                            aq.id(R.id.spnPregnanttypeofdelivery + i).getSpinner().setSelection(0);
                            aq.id(R.id.spnPregnantstatusofdelivery + i).getSpinner().setSelection(0);
                            aq.id(R.id.spnPregnantsexofchild + i).getSpinner().setSelection(0);
                            aq.id(R.id.etPregnantAgeatCurrentDate + i).getEditText().setText("");
                        }
                        aq.id(R.id.llPP_FirstBorn).gone();
                        aq.id(R.id.llPP_SecondBorn).gone();
                        aq.id(R.id.llPP_ThirdBorn).gone();
                        aq.id(R.id.llPP_FourthBorn).gone();
                    } else if (position == 1) {
//                        tableRowChild.setVisibility(View.GONE);
                        tableRowHusband.setVisibility(View.GONE);
                        tableRowHusbandOccupa.setVisibility(View.GONE);
                        tableRowMarriageType.setVisibility(View.GONE);
                        aq.id(R.id.etadshusbandname).text("");
                        aq.id(R.id.etadshusbandoccupa).text("");
                        aq.id(R.id.spnmarriagetype).getSpinner().setSelection(0);
                        aq.id(R.id.spnnoofchild).getSpinner().setSelection(0);
                        int size = aq.id(R.id.spnnoofchild).getSpinner().getSelectedItemPosition();
                        for (int i = 1; i <= size; i++) {
                            aq.id(R.id.spnPregnantoutcomeofPregnancy + i).getSpinner().setSelection(0);
                            aq.id(R.id.etPregnantDOD + i).getEditText().setText("");
                            aq.id(R.id.spnPregnantwhereDeliverBaby + i).getSpinner().setSelection(0);
                            aq.id(R.id.spnPregnantwhoConductedDelivery + i).getSpinner().setSelection(0);
                            aq.id(R.id.spnPregnantwhomodeofTransport + i).getSpinner().setSelection(0);
                            aq.id(R.id.spnPregnanttypeofdelivery + i).getSpinner().setSelection(0);
                            aq.id(R.id.spnPregnantstatusofdelivery + i).getSpinner().setSelection(0);
                            aq.id(R.id.spnPregnantsexofchild + i).getSpinner().setSelection(0);
                            aq.id(R.id.etPregnantAgeatCurrentDate + i).getEditText().setText("");
                        }
                        aq.id(R.id.llPP_FirstBorn).gone();
                        aq.id(R.id.llPP_SecondBorn).gone();
                        aq.id(R.id.llPP_ThirdBorn).gone();
                        aq.id(R.id.llPP_FourthBorn).gone();
                    } else if (position > 1) {
//                        tableRowChild.setVisibility(View.VISIBLE);
                        tableRowHusband.setVisibility(View.VISIBLE);
                        tableRowHusbandOccupa.setVisibility(View.VISIBLE);
                        tableRowMarriageType.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });


            radiogroupHeight.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (checkedId == R.id.rd_cm) {
                        aq.id(R.id.spnAdsHeightNumeric).getSpinner().setSelection(4);
                        aq.id(R.id.spnAdsHeightDecimal).getSpinner().setSelection(0);
                        aq.id(R.id.tr_heightcm).getView().setVisibility(View.VISIBLE);
                        aq.id(R.id.trfeet).getView().setVisibility(View.GONE);
                    } else if (checkedId == R.id.rd_feet) {
                        aq.id(R.id.etAdsheightcm).getEditText().setText("");
                        aq.id(R.id.etAdsheightcm).getEditText().setError(null);
                        aq.id(R.id.tr_heightcm).getView().setVisibility(View.GONE);
                        aq.id(R.id.trfeet).getView().setVisibility(View.VISIBLE);
                    } else if (checkedId == R.id.rd_htdontknow) {
                        aq.id(R.id.etAdsheightcm).getEditText().setText("");
                        aq.id(R.id.etAdsheightcm).getEditText().setError(null);
                        aq.id(R.id.spnAdsHeightNumeric).getSpinner().setSelection(4);
                        aq.id(R.id.spnAdsHeightDecimal).getSpinner().setSelection(0);
                        aq.id(R.id.trfeet).getView().setVisibility(View.GONE);
                        aq.id(R.id.tr_heightcm).getView().setVisibility(View.GONE);
                    }
                }
            });

            rg_isPeriod.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    if (i == R.id.rb_AR_isPeriodYes) {
                        aq.id(R.id.tr_AR_AgeatMensturated).getView().setVisibility(View.VISIBLE);
                        llMaternal.setVisibility(View.VISIBLE);

                    } else if (i == R.id.rb_AR_isPeriodNo) {
                        aq.id(R.id.tr_AR_AgeatMensturated).getView().setVisibility(View.GONE);
                        aq.id(R.id.et_AR_MenstruatedAge).getEditText().setText("");
                        aq.id(R.id.et_AR_MenstruatedAge).getEditText().setError(null);

                        llMaternal.setVisibility(View.GONE);

                        cb_Backpain.setChecked(false);
                        cb_StomachPain.setChecked(false);
                        cb_Fatigue.setChecked(false);
                        cb_Vomiting.setChecked(false);
                        cb_Diarrhea.setChecked(false);
                        cb_Iritability.setChecked(false);
                        cb_Bloodloss.setChecked(false);
                        cb_Moodiness.setChecked(false);
                        cb_Cramping.setChecked(false);
                        cb_CycleIssue.setChecked(false);
                        cb_overbleeding.setChecked(false);
                        cb_headache.setChecked(false);
                        cb_earlypregnancy.setChecked(false);
                    }
                }
            });

            rg_gender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    TextView partnerName = findViewById(R.id.tvpartnername);
                    TextView partnerOccupa = findViewById(R.id.tvpartneroccupa);
                    RadioButton rbpregnantYes = findViewById(R.id.rb_pregnantYes);
                    RadioButton rbpregnantNo = findViewById(R.id.rb_pregnantNo);
                    if (checkedId == R.id.rd_male) {
                        tr_pregnant.setVisibility(View.GONE);
                        partnerName.setText(getResources().getString(R.string.wifename));
                        partnerOccupa.setText(getResources().getString(R.string.wifeoccupa));


//                        13MAy2021 Bindu rg clear check
                        rg_pregnant.clearCheck();
                        rg_isPeriod.clearCheck();
                        rbpregnantYes.setChecked(false);
                        rbpregnantNo.setChecked(false);
                        aq.id(R.id.rb_AR_isPeriodYes).checked(false);
                        aq.id(R.id.rb_AR_isPeriodNo).checked(false);


                        aq.id(R.id.tr_AR_AgeatMensturated).getView().setVisibility(View.GONE);
                        aq.id(R.id.et_AR_MenstruatedAge).getEditText().setText("");
                        aq.id(R.id.tr_ARIsPeriod).getView().setVisibility(View.GONE);
                        RadioButton rb_isPeriodYes = findViewById(R.id.rb_AR_isPeriodYes);
                        RadioButton rb_isPeriodNo = findViewById(R.id.rb_AR_isPeriodNo);
                        rb_isPeriodYes.setChecked(false);
                        rb_isPeriodNo.setChecked(false);

                        llMaternal.setVisibility(View.GONE);

                        cb_Backpain.setChecked(false);
                        cb_StomachPain.setChecked(false);
                        cb_Fatigue.setChecked(false);
                        cb_Vomiting.setChecked(false);
                        cb_Diarrhea.setChecked(false);
                        cb_Iritability.setChecked(false);
                        cb_Bloodloss.setChecked(false);
                        cb_Moodiness.setChecked(false);
                        cb_Cramping.setChecked(false);
                        cb_CycleIssue.setChecked(false);
                        cb_overbleeding.setChecked(false);
                        cb_headache.setChecked(false);
                        cb_earlypregnancy.setChecked(false);

                    } else if (checkedId == R.id.rd_female) {
                        tr_pregnant.setVisibility(View.VISIBLE);
                        partnerName.setText(getResources().getString(R.string.tvhusbname));
                        partnerOccupa.setText(getResources().getString(R.string.husbandoccupa));

                        aq.id(R.id.tr_ARIsPeriod).getView().setVisibility(View.VISIBLE);
                    }
                }
            });


            cbOther.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        aq.id(R.id.ethealthissueothers).getView().setVisibility(View.VISIBLE);
                    } else {
                        aq.id(R.id.ethealthissueothers).getView().setVisibility(View.GONE);
                        aq.id(R.id.ethealthissueothers).getEditText().setText("");
                    }
                }
            });

            aq.id(R.id.btnsave).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (validationName() && validationAge() && validationGender() && validationisPeriod() && validationAgeatMensturated() && validationPregnant()
                                && validationWeight() && validationHeight() && validationAadharCard() && ValidationIsAadharCard() && validationMobile() && validationMobileOf()
                                && validationAadharandMobile() && validationEducation() && validationGTS() && validationFacility() && validationRelationship() && validationBornFields()) {
                            aq.id(R.id.btnsave).getImageView().setImageResource(R.drawable.save);
                            aq.id(R.id.llbasicView).gone();
                            aq.id(R.id.llhealthissueView).visible();
                            if (validationOtherIssues() && isReadytoInsert) {
                                SaveData();
                            }
                            aq.id(R.id.rb_top_health).checked(true);
                            isReadytoInsert = true;
                        }
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());

                    }
                }
            });
            btToolbarHome.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    displayDialogExit();
                }
            });

            aq.id(R.id.btnclear).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        displayDialogExit();
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());

                    }
                }
            });
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private boolean validationEducation() {
        try {
            if (aq.id(R.id.spnadsEducation).getSpinner().getSelectedItemPosition() == 0) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseselecteducation), Toast.LENGTH_LONG).show();
                aq.id(R.id.txtadseducation).getTextView().requestFocus();
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return false;
    }

    private boolean validationAgeatMensturated() {
        try {
            RadioButton isPeriodYes = findViewById(R.id.rb_AR_isPeriodYes);
            RadioButton rbFemale = findViewById(R.id.rd_female);

            if (rbFemale.isChecked()) {
                if (isPeriodYes.isChecked()) {
                    if (aq.id(R.id.et_AR_MenstruatedAge).getEditText().getText().toString().length() == 0) {
                        aq.id(R.id.et_AR_MenstruatedAge).getEditText().setError(getResources().getString(R.string.pleaseenterageatmenstruated));
                        aq.id(R.id.et_AR_MenstruatedAge).getEditText().requestFocus();
                        return false;
                    } else {
                        int Agemenstruated = Integer.parseInt(aq.id(R.id.et_AR_MenstruatedAge).getEditText().getText().toString());
                        if (aq.id(R.id.etAdsage).getEditText().getText().toString().length() != 0) {
                            int age = Integer.parseInt(aq.id(R.id.etAdsage).getEditText().getText().toString());
                            if (Agemenstruated > age) {
                                aq.id(R.id.et_AR_MenstruatedAge).getEditText().setError(getResources().getString(R.string.cannotbegreaterthanaage));
                                aq.id(R.id.et_AR_MenstruatedAge).getEditText().requestFocus();
                                return false;
                            } else if (Agemenstruated < 5) {
                                aq.id(R.id.et_AR_MenstruatedAge).getEditText().setError(getResources().getString(R.string.cannotbelessthan10));
                                aq.id(R.id.et_AR_MenstruatedAge).getEditText().requestFocus();
                                return false;
                            } else {
                                aq.id(R.id.et_AR_MenstruatedAge).getEditText().setError(null);
                                return true;
                            }
                        } else {
                            aq.id(R.id.etAdsage).getEditText().setError(getResources().getString(R.string.pleaseenterage));
                            aq.id(R.id.etAdsage).getEditText().requestFocus();
                            return false;
                        }
                    }
                } else {
                    return true;
                }
            } else {
                return true;
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return false;
    }

    private boolean validationisPeriod() {
        try {
            RadioButton isPeriodYes = findViewById(R.id.rb_AR_isPeriodYes);
            RadioButton isPeriodNo = findViewById(R.id.rb_AR_isPeriodNo);
            RadioButton rbFemale = findViewById(R.id.rd_female);

            if (rbFemale.isChecked()) {
                if ((!isPeriodYes.isChecked()) && (!isPeriodNo.isChecked())) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseselectisperiod), Toast.LENGTH_LONG).show();
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return false;
    }

    private boolean validationAadharandMobile() {
        try {
            if (aq.id(R.id.etadsAadharCard).getEditText().getText().toString().length() == 0 && aq.id(R.id.etAdsphone).getEditText().getText().toString().length() == 0) {
                AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.pleaseenteraadharnoormobileno), AdolRegistration.this);
                aq.id(R.id.etadsAadharCard).getEditText().requestFocus();
                return false;
            } else if (aq.id(R.id.etAdsphone).getEditText().getText().toString().length() == 10) {
                if (aq.id(R.id.spnphn).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseentermobilenoof), Toast.LENGTH_LONG).show();
                    return false;
                } else {
                    return true;
                }
            } else if (validationAadharCard()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return false;
    }

    private boolean validationAadharCard() {
        try {
            if (aq.id(R.id.etadsAadharCard).getEditText().getText().toString().length() == 0) {
                return true;
            } else if (aq.id(R.id.etadsAadharCard).getEditText().getText().toString().length() == 12) {
                aq.id(R.id.etadsAadharCard).getEditText().setError(null);
                return true;
            } else if (aq.id(R.id.etadsAadharCard).getEditText().getText().toString().length() != 12) {
                aq.id(R.id.etadsAadharCard).getEditText().setError(getResources().getString(R.string.pleaseentevalidaadharnumber));
                aq.id(R.id.etadsAadharCard).getEditText().requestFocus();
                return false;
            }
            //12Sep2021 Arpitha
            List<tblAdolReg> adolList = new AdolescentRepository(databaseHelper).getExistingAadharId();
             if(adolList.contains(aq.id(R.id.etadsAadharCard).getText().toString()))
            {
                aq.id(R.id.etadsAadharCard).getEditText().setError(getResources().getString(R.string.pleaseentevalidaadharnumber));
                aq.id(R.id.etadsAadharCard).getEditText().requestFocus();
                return false;
            }//12Sep2021 Arpitha
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return false;
    }


    private boolean ValidationIsAadharCard() {
        try {
            if (validationAadharCard()) {
                AdolescentRepository adolescentRepository = new AdolescentRepository(databaseHelper);
                if ((!isEdit) && adolescentRepository.checkAadharNoExists(aq.id(R.id.etadsAadharCard).getEditText().getText().toString())) {
                    aq.id(R.id.etadsAadharCard).getEditText().setError(getResources().getString(R.string.aadharcardalreadyexists));
                    aq.id(R.id.etadsAadharCard).getEditText().requestFocus();
                    return false;
                } else {
                    aq.id(R.id.etadsAadharCard).getEditText().setError(null);
                    return true;
                }
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return false;
    }

    private void setBornEditData(int i) {
        try {
            AdolPregnantRepository adolPregnantRepository = new AdolPregnantRepository(databaseHelper);
            List<tblAdolPregnant> allborns = new ArrayList<>();
            allborns.add(new tblAdolPregnant());
            allborns.addAll(adolPregnantRepository.getPregnantsbyIDs(AdolID));
            if (i == 0) {
                aq.id(R.id.llPP_FirstBorn).getView().setVisibility(View.GONE);
                aq.id(R.id.llPP_SecondBorn).getView().setVisibility(View.GONE);
                aq.id(R.id.llPP_ThirdBorn).getView().setVisibility(View.GONE);
                aq.id(R.id.llPP_FourthBorn).getView().setVisibility(View.GONE);
            }
            if (i == 1) {
                aq.id(R.id.llPP_FirstBorn).getView().setVisibility(View.VISIBLE);
                aq.id(R.id.llPP_SecondBorn).getView().setVisibility(View.GONE);
                aq.id(R.id.llPP_ThirdBorn).getView().setVisibility(View.GONE);
                aq.id(R.id.llPP_FourthBorn).getView().setVisibility(View.GONE);
                setFirstBornUI();
            } else if (i == 2) {
                aq.id(R.id.llPP_FirstBorn).getView().setVisibility(View.VISIBLE);
                aq.id(R.id.llPP_SecondBorn).getView().setVisibility(View.VISIBLE);
                aq.id(R.id.llPP_ThirdBorn).getView().setVisibility(View.GONE);
                aq.id(R.id.llPP_FourthBorn).getView().setVisibility(View.GONE);
                setFirstBornUI();
                setSecondBornUI();
            } else if (i == 3) {
                aq.id(R.id.llPP_FirstBorn).getView().setVisibility(View.VISIBLE);
                aq.id(R.id.llPP_SecondBorn).getView().setVisibility(View.VISIBLE);
                aq.id(R.id.llPP_ThirdBorn).getView().setVisibility(View.VISIBLE);
                aq.id(R.id.llPP_FourthBorn).getView().setVisibility(View.GONE);
                setFirstBornUI();
                setSecondBornUI();
                setThirdBornUI();
            } else if (i == 4) {
                aq.id(R.id.llPP_FirstBorn).getView().setVisibility(View.VISIBLE);
                aq.id(R.id.llPP_SecondBorn).getView().setVisibility(View.VISIBLE);
                aq.id(R.id.llPP_ThirdBorn).getView().setVisibility(View.VISIBLE);
                aq.id(R.id.llPP_FourthBorn).getView().setVisibility(View.VISIBLE);
                setFirstBornUI();
                setSecondBornUI();
                setThirdBornUI();
                setFourthBornUI();
            }
            for (int ini = 1; ini <= i; ini++) {
                int index = allborns.get(ini).getTblAdolPreOutcome();
                aq.id(R.id.spnPregnantoutcomeofPregnancy + ini).getSpinner().setSelection(index);
                aq.id(R.id.spnPregnantoutcomeofPregnancy + ini).getSpinner().setEnabled(false);

                String text = allborns.get(ini).getTblAdolPreDoD();
                aq.id(R.id.etPregnantDOD + ini).getEditText().setText(text);
                aq.id(R.id.etPregnantDOD + ini).getEditText().setEnabled(false);

                aq.id(R.id.spnPregnantwhereDeliverBaby + ini).getSpinner().setSelection(allborns.get(ini).getTblAdolPreDeliverWhere());
                aq.id(R.id.spnPregnantwhereDeliverBaby + ini).getSpinner().setEnabled(false);

                aq.id(R.id.spnPregnantwhoConductedDelivery + ini).getSpinner().setSelection(allborns.get(ini).getTblAdolPreWhoConductDelivery());
                aq.id(R.id.spnPregnantwhoConductedDelivery + ini).getSpinner().setEnabled(false);

                aq.id(R.id.spnPregnantwhomodeofTransport + ini).getSpinner().setSelection(allborns.get(ini).getTblAdolPreTransport());
                aq.id(R.id.spnPregnantwhomodeofTransport + ini).getSpinner().setEnabled(false);

                aq.id(R.id.spnPregnanttypeofdelivery + ini).getSpinner().setSelection(allborns.get(ini).getTblAdolPreDeliveryType());
                aq.id(R.id.spnPregnanttypeofdelivery + ini).getSpinner().setEnabled(false);

                aq.id(R.id.spnPregnantstatusofdelivery + ini).getSpinner().setSelection(allborns.get(ini).getTblAdolPreDeliveryStatus());
                aq.id(R.id.spnPregnantstatusofdelivery + ini).getSpinner().setEnabled(false);

                aq.id(R.id.spnPregnantsexofchild + ini).getSpinner().setSelection(allborns.get(ini).getTblAdolPreChildSex());
                aq.id(R.id.spnPregnantsexofchild + ini).getSpinner().setEnabled(false);

                aq.id(R.id.etPregnantAgeatCurrentDate + ini).getEditText().setText(allborns.get(ini).getTblAdolPreChildAge());
                aq.id(R.id.etPregnantAgeatCurrentDate + ini).getEditText().setEnabled(false);
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private boolean validationGTS() {
        try {
            RadioButton rbGTSYes = findViewById(R.id.rd_goingschoolYes);
            RadioButton rbGTSNo = findViewById(R.id.rd_goingschoolNo);
            if ((!rbGTSYes.isChecked()) && (!rbGTSNo.isChecked())) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseselectgts), Toast.LENGTH_SHORT).show();
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return false;
    }

    private void getFacilitiesData() {
        try {
            FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
            facilities = facilityRepository.getPlaceMap();
            ArrayList<String> villageList = new ArrayList<String>();
            villageList.add(getResources().getString(R.string.selectType));
            for (Map.Entry<String, Integer> village : facilities.entrySet())
                villageList.add(village.getKey());
            ArrayAdapter<String> VillageAdapter = new ArrayAdapter<>(this,
                    R.layout.simple_spinner_dropdown_item, villageList);
            VillageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            aq.id(R.id.spnfacilities).adapter(VillageAdapter);
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    @SuppressLint("SetTextI18n")
    private void setEditData(tblAdolReg adolEditData) {
        try {
            aq.id(R.id.etAdsname).getEditText().setText(adolEditData.getRegAdolName());
            aq.id(R.id.etAdsage).getEditText().setText(adolEditData.getRegAdolAge());

            RadioButton rbMale = findViewById(R.id.rd_male);
            RadioButton rbFemale = findViewById(R.id.rd_female);
            TextView partnerName = findViewById(R.id.tvpartnername);
            TextView partnerOccupa = findViewById(R.id.tvpartneroccupa);
            if (adolEditData.getRegAdolGender().equals("Male")) {
                rbMale.setChecked(true);
                llMaternal.setVisibility(View.GONE);
                partnerName.setText(getResources().getString(R.string.wifename));
                partnerOccupa.setText(getResources().getString(R.string.wifeoccupa));
            } else {
                rbFemale.setChecked(true);
                partnerName.setText(getResources().getString(R.string.tvhusbname));
                partnerOccupa.setText(getResources().getString(R.string.husbandoccupa));
                tr_pregnant.setVisibility(View.VISIBLE);
                RadioButton rbPregnantYes = findViewById(R.id.rb_pregnantYes);
                RadioButton rbPregnantNo = findViewById(R.id.rb_pregnantNo);
                RadioButton rbPeriodYes = findViewById(R.id.rb_AR_isPeriodYes);
                RadioButton rbPeriodNo = findViewById(R.id.rb_AR_isPeriodNo);


                if (adolEditData.getRegAdolPregnant().equals("Yes")) {
                    rbPregnantYes.setChecked(true);
                } else {
                    rbPregnantNo.setChecked(true);
                }
                aq.id(R.id.tr_ARIsPeriod).getView().setVisibility(View.VISIBLE);
                if (adolEditData.getRegAdolisPeriod().equals("Yes")) {
                    rbPeriodYes.setChecked(true);
                    aq.id(R.id.tr_AR_AgeatMensturated).getView().setVisibility(View.VISIBLE);
                    aq.id(R.id.et_AR_MenstruatedAge).getEditText().setText(adolEditData.getRegAdolAgeatMenstruated());
                    llMaternal.setVisibility(View.VISIBLE);

                    cb_Backpain.setVisibility(View.VISIBLE);
                    cb_StomachPain.setVisibility(View.VISIBLE);
                    cb_Fatigue.setVisibility(View.VISIBLE);
                    cb_Vomiting.setVisibility(View.VISIBLE);
                    cb_Diarrhea.setVisibility(View.VISIBLE);
                    cb_Iritability.setVisibility(View.VISIBLE);
                    cb_Bloodloss.setVisibility(View.VISIBLE);
                    cb_Moodiness.setVisibility(View.VISIBLE);
                    cb_CycleIssue.setVisibility(View.VISIBLE);
                    cb_Cramping.setVisibility(View.VISIBLE);
                    cb_overbleeding.setVisibility(View.VISIBLE);
                    cb_headache.setVisibility(View.VISIBLE);
                    cb_earlypregnancy.setVisibility(View.VISIBLE);

                    //21May2021 Bindu check menstrual
                    if (adolEditData.getRegAdolMenstrualPbm() != null && adolEditData.getRegAdolMenstrualPbm().trim().length() > 0) {
                        String[] currentMenpbm = adolEditData.getRegAdolMenstrualPbm().split(",|:");

                        for (int i = 0; i < currentMenpbm.length; i++) {
                            if (currentMenpbm[i].trim().equalsIgnoreCase(getResources().getString(R.string.backpainsave)))
                                cb_Backpain.setChecked(true);
                            if (currentMenpbm[i].trim().equalsIgnoreCase(getResources().getString(R.string.stomachachesave)))
                                cb_StomachPain.setChecked(true);
                            if (currentMenpbm[i].trim().equalsIgnoreCase(getResources().getString(R.string.fatiguesave)))
                                cb_Fatigue.setChecked(true);
                            if (currentMenpbm[i].trim().equalsIgnoreCase(getResources().getString(R.string.vomitingsave)))
                                cb_Vomiting.setChecked(true);
                            if (currentMenpbm[i].trim().equalsIgnoreCase(getResources().getString(R.string.diarrheasave)))
                                cb_Diarrhea.setChecked(true);
                            if (currentMenpbm[i].trim().equalsIgnoreCase(getResources().getString(R.string.irritabilitysave)))
                                cb_Iritability.setChecked(true);
                            if (currentMenpbm[i].trim().equalsIgnoreCase(getResources().getString(R.string.bloodlosssave)))
                                cb_Bloodloss.setChecked(true);
                            if (currentMenpbm[i].trim().equalsIgnoreCase(getResources().getString(R.string.moodinesssave)))
                                cb_Moodiness.setChecked(true);
                            if (currentMenpbm[i].trim().equalsIgnoreCase(getResources().getString(R.string.cycleissuessave)))
                                cb_CycleIssue.setChecked(true);
                            if (currentMenpbm[i].trim().equalsIgnoreCase(getResources().getString(R.string.crampingsave)))
                                cb_Cramping.setChecked(true);
                            if (currentMenpbm[i].trim().equalsIgnoreCase(getResources().getString(R.string.overbleedingsave)))
                                cb_overbleeding.setChecked(true);
                            if (currentMenpbm[i].trim().equalsIgnoreCase(getResources().getString(R.string.headachesave)))
                                cb_headache.setChecked(true);
                            if (currentMenpbm[i].trim().equalsIgnoreCase(getResources().getString(R.string.earlypregancyandchildbirthsave)))
                                cb_earlypregnancy.setChecked(true);
                        }
                    }

                } else {
                    llMaternal.setVisibility(View.GONE);
                    rbPeriodNo.setChecked(true);
                }
            }
            aq.id(R.id.etAdsdob).getEditText().setText(adolEditData.getRegAdolDoB());
            aq.id(R.id.etAdsWeight).getEditText().setText(adolEditData.getRegAdolWeight());
            showWeightWarning(Double.parseDouble(aq.id(R.id.etAdsWeight).getEditText().getText().toString()));

            //height
            TableRow tableRowFeet = findViewById(R.id.trfeet);
            TableRow tableRowCM = findViewById(R.id.tr_heightcm);
            RadioButton radioButton_cm = findViewById(R.id.rd_cm);
            RadioButton radioButton_feet = findViewById(R.id.rd_feet);
            RadioButton radioButton_dontknow = findViewById(R.id.rd_htdontknow);
            switch (adolEditData.getRegAdolHeightType()) {
                case 1:
                    radioButton_cm.setChecked(true);
                    tableRowCM.setVisibility(View.VISIBLE);
                    tableRowFeet.setVisibility(View.GONE);
                    aq.id(R.id.etAdsheightcm).getEditText().setText(adolEditData.getRegAdolHeight());
                    showHeightWarning(Double.parseDouble(aq.id(R.id.etAdsheightcm).getEditText().getText().toString()));
                    break;
                case 2:
                    radioButton_feet.setChecked(true);
                    tableRowCM.setVisibility(View.GONE);
                    tableRowFeet.setVisibility(View.VISIBLE);
                    String height = adolEditData.getRegAdolHeight();
                    int i = 0;
                    StringBuilder Feet = new StringBuilder();
                    for (i = 0; i < height.length(); i++) {
                        char c = height.charAt(i);
                        if (c == '.') {
                            break;
                        } else {
                            Feet.append(c);
                        }
                    }
                    int selectfeet = Integer.parseInt(Feet.toString()) - 1;
                    aq.id(R.id.spnAdsHeightNumeric).getSpinner().setSelection(selectfeet);

                    i++;
                    StringBuilder inches = new StringBuilder();
                    for (; i < height.length(); i++) {
                        char c = height.charAt(i);

                        inches.append(c);

                    }
                    int inch = Integer.parseInt(inches.toString().trim());
                    aq.id(R.id.spnAdsHeightDecimal).getSpinner().setSelection(inch);
                    break;
                case 3:
                    radioButton_dontknow.setChecked(true);
                    break;
            }//end of heights

            aq.id(R.id.etadsAadharCard).getEditText().setText(adolEditData.getRegAdolAadharNo());
            if (adolEditData.getRegAdolAadharNo().length() > 0) {
                aq.id(R.id.etadsAadharCard).getEditText().setEnabled(false);
            }

//            08Sep2021 Arpitha
            aq.id(R.id.rb_pregnantYes).enabled(false);
            aq.id(R.id.rb_pregnantNo).enabled(false);// 08Sep2021 Arpitha

            aq.id(R.id.etAdsphone).getEditText().setText(adolEditData.getRegAdolMobile());
            aq.id(R.id.spnphn).getSpinner().setSelection(adolEditData.getRegAdolMobileOf());

            aq.id(R.id.spnadsEducation).getSpinner().setSelection(adolEditData.getRegAdolEducation());

            RadioButton rbGTSYes = findViewById(R.id.rd_goingschoolYes);
            RadioButton rbGTSNo = findViewById(R.id.rd_goingschoolNo);
            if (adolEditData.getRegAdolGoingtoSchool().equals("Yes")) {
                rbGTSYes.setChecked(true);
            } else if (adolEditData.getRegAdolGoingtoSchool().equals("No")) {
                rbGTSNo.setChecked(true);
            }

            aq.id(R.id.etadsfathername).getEditText().setText(adolEditData.getRegAdolFather());
            CheckBox checkBoxFather = findViewById(R.id.cb_fatherdesc);
            checkBoxFather.setChecked(adolEditData.getRegAdolFatherDesc().equals("Yes"));

            aq.id(R.id.etadsmothername).getEditText().setText(adolEditData.getRegAdolMother());
            CheckBox checkBoxMother = findViewById(R.id.cb_motherdesc);
            checkBoxMother.setChecked(adolEditData.getRegAdolMotherDesc().equals("Yes"));

            aq.id(R.id.etadsguardianname).getEditText().setText(adolEditData.getRegAdolGuardian());

            aq.id(R.id.spnfamilytype).getSpinner().setSelection(adolEditData.getRegAdolFamilytype());

            aq.id(R.id.etaddr).getEditText().setText(adolEditData.getRegAdolAddress());

            aq.id(R.id.etareaname).getEditText().setText(adolEditData.getRegAdolAreaname());

            int villageName = getKey(facilities, String.valueOf(adolEditData.getRegAdolFacilities()));
            aq.id(R.id.spnfacilities).getSpinner().setSelection(villageName);

            aq.id(R.id.etpincode).getEditText().setText(adolEditData.getRegAdolPincode());


            aq.id(R.id.spnRelationship).getSpinner().setSelection(adolEditData.getRegAdolRelationship());
            String relation = aq.id(R.id.spnRelationship).getSpinner().getSelectedItem().toString();
            if (relation.equals("Single")) {

            } else {
                TableRow tableRowHusband = findViewById(R.id.trHusbandName);
//                TableRow tableRowChild = findViewById(R.id.trNoofChild);
                TableRow tableRowHusbandOccupa = findViewById(R.id.trHusbandOccupa);
                TableRow tableRowMarriageType = findViewById(R.id.tr_marriagetype);

//                tableRowChild.setVisibility(View.VISIBLE);
                tableRowHusband.setVisibility(View.VISIBLE);
                tableRowHusbandOccupa.setVisibility(View.VISIBLE);
                tableRowMarriageType.setVisibility(View.VISIBLE);

                aq.id(R.id.etadshusbandname).getEditText().setText(adolEditData.getRegAdolPartnerName());

                aq.id(R.id.etadshusbandoccupa).getEditText().setText(adolEditData.getRegAdolPartnerOccupa());

                aq.id(R.id.spnmarriagetype).getSpinner().setSelection(adolEditData.getRegAdolMarriageType());
                int index = adolEditData.getRegAdolNoofChild();
                aq.id(R.id.spnnoofchild).getSpinner().setSelection(index + 1);
//                if (index > 0) {
//                    setBornEditData(index);
//                }
            }

            //21May2021 Bindu set health issues
            if (adolEditData.getRegAdolHealthIssues() != null && adolEditData.getRegAdolHealthIssues().trim().length() > 0) {
                String[] currentHealthissues = adolEditData.getRegAdolHealthIssues().split(",|:");

                for (int i = 0; i < currentHealthissues.length; i++) {
                    if (currentHealthissues[i].trim().equalsIgnoreCase(getResources().getString(R.string.anemiasave)))
                        cbAnemia.setChecked(true);
                    if (currentHealthissues[i].trim().equalsIgnoreCase(getResources().getString(R.string.undernutritionsave)))
                        cbUndernut.setChecked(true);
                    if (currentHealthissues[i].trim().equalsIgnoreCase(getResources().getString(R.string.obesitysave)))
                        cbObesity.setChecked(true);
                    if (currentHealthissues[i].trim().equalsIgnoreCase(getResources().getString(R.string.injuriessave)))
                        cbInjuries.setChecked(true);
                    if (currentHealthissues[i].trim().equalsIgnoreCase(getResources().getString(R.string.depressandanxisave)))
                        cbDeprandAnxi.setChecked(true);
                    if (currentHealthissues[i].trim().equalsIgnoreCase(getResources().getString(R.string.hivandaidssave)))
                        cbHivandAids.setChecked(true);
                    if (currentHealthissues[i].trim().equalsIgnoreCase(getResources().getString(R.string.infectiondiseasessave)))
                        cbInfectionDis.setChecked(true);
                    if (currentHealthissues[i].trim().equalsIgnoreCase(getResources().getString(R.string.othersave)))
                        cbOther.setChecked(true);
                }
            }


            if (adolEditData.getRegAdolOtherHealthIssues().length() != 0) {
                EditText editText = findViewById(R.id.ethealthissueothers);
                editText.setVisibility(View.VISIBLE);
                editText.setText(adolEditData.getRegAdolOtherHealthIssues().toString());
            }

            //21May2021 Bindu set life style
            if (adolEditData.getRegAdolLifeStyle() != null && adolEditData.getRegAdolLifeStyle().trim().length() > 0) {
                String[] currentAdolLifeStyle = adolEditData.getRegAdolLifeStyle().split(",|:");

                for (int i = 0; i < currentAdolLifeStyle.length; i++) {
                    if (currentAdolLifeStyle[i].trim().equalsIgnoreCase(getResources().getString(R.string.alcoholsave)))
                        cbAlcohol.setChecked(true);
                    if (currentAdolLifeStyle[i].trim().equalsIgnoreCase(getResources().getString(R.string.drugssave)))
                        cbDrugs.setChecked(true);
                    if (currentAdolLifeStyle[i].trim().equalsIgnoreCase(getResources().getString(R.string.tobaccosave)))
                        cbTobacco.setChecked(true);
                    if (currentAdolLifeStyle[i].trim().equalsIgnoreCase(getResources().getString(R.string.smokingsave)))
                        cbSmoking.setChecked(true);
                }
            }

            //21May2021 Bindu set life style
            if (adolEditData.getRegAdolPhysicalActivity() != null && adolEditData.getRegAdolPhysicalActivity().trim().length() > 0) {
                String[] currentPhyAct = adolEditData.getRegAdolPhysicalActivity().split(",|:");

                for (int i = 0; i < currentPhyAct.length; i++) {
                    if (currentPhyAct[i].trim().equalsIgnoreCase(getResources().getString(R.string.walkingsave)))
                        cbWalking.setChecked(true);
                    if (currentPhyAct[i].trim().equalsIgnoreCase(getResources().getString(R.string.joggingsave)))
                        cbJogging.setChecked(true);
                    if (currentPhyAct[i].trim().equalsIgnoreCase(getResources().getString(R.string.yogasave)))
                        cbYoga.setChecked(true);
                    if (currentPhyAct[i].trim().equalsIgnoreCase(getResources().getString(R.string.workoutsave)))
                        cbWorkout.setChecked(true);
                    if (currentPhyAct[i].trim().equalsIgnoreCase(getResources().getString(R.string.meditationsave)))
                        cbMeditation.setChecked(true);
                }
            }

            aq.id(R.id.etsuggGiven).getEditText().setText(adolEditData.getRegAdolSuggestionGiven());

            aq.id(R.id.etcomments).getEditText().setText(adolEditData.getRegAdolComments());
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private void PreSetUIChanges() {
        try {
            aq.id(R.id.llPP_FirstBorn).getView().setVisibility(View.GONE);
            aq.id(R.id.llPP_SecondBorn).getView().setVisibility(View.GONE);
            aq.id(R.id.llPP_ThirdBorn).getView().setVisibility(View.GONE);
            aq.id(R.id.llPP_FourthBorn).getView().setVisibility(View.GONE);
            aq.id(R.id.spnfacilities).enabled(false);
            RadioButton rbMale = findViewById(R.id.rd_male);
            RadioButton rbFemale = findViewById(R.id.rd_female);
            rbMale.setEnabled(false);
            rbFemale.setEnabled(false);

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private void clickListeners() {
        try {
            aq.id(R.id.etAdsdob).getEditText().setOnTouchListener(this);
            aq.id(R.id.imgAdscleardob).getImageView().setOnTouchListener(this);
            aq.id(R.id.etAdsage).getEditText().setOnTouchListener(this);
            aq.id(R.id.etPregnantDOD4).getEditText().setOnTouchListener(this);
            aq.id(R.id.etPregnantDOD3).getEditText().setOnTouchListener(this);
            aq.id(R.id.etPregnantDOD2).getEditText().setOnTouchListener(this);
            aq.id(R.id.etPregnantDOD1).getEditText().setOnTouchListener(this);
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private boolean validationPregnant() {
        try {
            RadioButton rbFemlae = findViewById(R.id.rd_female);
            if (rbFemlae.isChecked()) {
                RadioButton rbPregnantYes = findViewById(R.id.rb_pregnantYes);
                RadioButton rbPregnantNo = findViewById(R.id.rb_pregnantNo);
                if ((!rbPregnantYes.isChecked()) && (!rbPregnantNo.isChecked())) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseselectpregnant), Toast.LENGTH_SHORT).show();
                    aq.id(R.id.etAdsage).getEditText().requestFocus();
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return false;
    }

    private boolean validationGender() {
        try {
            RadioButton rbMale = findViewById(R.id.rd_male);
            RadioButton rbFemale = findViewById(R.id.rd_female);
            if ((!rbMale.isChecked()) && (!rbFemale.isChecked())) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseselectgender), Toast.LENGTH_SHORT).show();
                aq.id(R.id.etAdsWeight).getEditText().requestFocus();
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return false;
    }


    private void SaveData() {
        try {
            AdolescentRepository adolescentRepo = new AdolescentRepository(databaseHelper);
            UserRepository userRepository = new UserRepository(databaseHelper);

//            20May2021 Bindu
            calculateHealthData();

            if (tblAdolReg == null) {
                tblAdolReg = new tblAdolReg();
            }
            if (!isEdit) {
                tblAdolReg.setAdolID(AdolID);
                tblAdolReg.setTransId(transID); //set transid only for add 23MAy2021 Bindu
            }
            tblAdolReg.setUserId(user.getUserId()); //21May2021 Bindu change to userid and set user type //23May2021 Bindu chnage to userid
            tblAdolReg.setRegUserType(appState.userType);
            //tblAdolReg.setTransId(transID);
            tblAdolReg.setRegAdolName(aq.id(R.id.etAdsname).getEditText().getText().toString());
            tblAdolReg.setRegAdolAge(aq.id(R.id.etAdsage).getEditText().getText().toString());
            RadioButton rbMale = findViewById(R.id.rd_male);
            RadioButton rbFemale = findViewById(R.id.rd_female);
            if (rbMale.isChecked()) {
                tblAdolReg.setRegAdolGender("Male");
                tblAdolReg.setRegAdolPregnant("");
                tblAdolReg.setRegAdolisPeriod("");
                tblAdolReg.setRegAdolAgeatMenstruated("");
                tblAdolReg.setRegAdolMenstrualPbm("");
            } else if (rbFemale.isChecked()) {// if femlae capture pregnant
                RadioButton rbpregnantYes = findViewById(R.id.rb_pregnantYes);
                RadioButton rbpregnantNo = findViewById(R.id.rb_pregnantNo);

                tblAdolReg.setRegAdolGender("Female");
                if (rbpregnantYes.isChecked()) {
                    tblAdolReg.setRegAdolPregnant("Yes"); //if pregnant capture pregancy issues
                } else if (rbpregnantNo.isChecked()) {
                    tblAdolReg.setRegAdolPregnant("No");
                }
                if (rbIsPeriodYes.isChecked()) {
                    tblAdolReg.setRegAdolisPeriod("Yes");
                    tblAdolReg.setRegAdolAgeatMenstruated(aq.id(R.id.et_AR_MenstruatedAge).getEditText().getText().toString());

                    tblAdolReg.setRegAdolMenstrualPbm(menstrualPbms);
                } else {
                    tblAdolReg.setRegAdolAgeatMenstruated("");
                    tblAdolReg.setRegAdolisPeriod("No");
                    tblAdolReg.setRegAdolMenstrualPbm("");
                }
            }
            tblAdolReg.setRegAdolDoB(aq.id(R.id.etAdsdob).getEditText().getText().toString());
            tblAdolReg.setRegAdolWeight(aq.id(R.id.etAdsWeight).getEditText().getText().toString());
            RadioButton radioButton_cm = findViewById(R.id.rd_cm);
            RadioButton radioButton_feet = findViewById(R.id.rd_feet);
            RadioButton radioButton_dontknow = findViewById(R.id.rd_htdontknow);
            if (radioButton_cm.isChecked()) {
                tblAdolReg.setRegAdolHeightType(1);
                tblAdolReg.setRegAdolHeight(aq.id(R.id.etAdsheightcm).getEditText().getText().toString());
            } else if (radioButton_feet.isChecked()) {
                tblAdolReg.setRegAdolHeightType(2);
                String feet = aq.id(R.id.spnAdsHeightNumeric).getSpinner().getSelectedItem().toString();
                String inches = aq.id(R.id.spnAdsHeightDecimal).getSpinner().getSelectedItem().toString();
                feet = feet + "." + inches;
                tblAdolReg.setRegAdolHeight(feet);
            } else if (radioButton_dontknow.isChecked()) {
                tblAdolReg.setRegAdolHeightType(3);
                tblAdolReg.setRegAdolHeight("");
            }

            tblAdolReg.setRegAdolAadharNo(aq.id(R.id.etadsAadharCard).getEditText().getText().toString());
            tblAdolReg.setRegAdolMobile(aq.id(R.id.etAdsphone).getEditText().getText().toString());
            tblAdolReg.setRegAdolMobileOf(aq.id(R.id.spnphn).getSpinner().getSelectedItemPosition());

            tblAdolReg.setRegAdolEducation(aq.id(R.id.spnadsEducation).getSpinner().getSelectedItemPosition());

            RadioButton rbGTSYes = findViewById(R.id.rd_goingschoolYes);
            tblAdolReg.setRegAdolGoingtoSchool(rbGTSYes.isChecked() ? "Yes" : "No");

            tblAdolReg.setRegAdolFather(aq.id(R.id.etadsfathername).getEditText().getText().toString()); //father
            CheckBox checkBoxFather = findViewById(R.id.cb_fatherdesc);
            tblAdolReg.setRegAdolFatherDesc(checkBoxFather.isChecked() ? "Yes" : "No");

            tblAdolReg.setRegAdolMother(aq.id(R.id.etadsmothername).getEditText().getText().toString()); //mother
            CheckBox checkBoxMother = findViewById(R.id.cb_motherdesc);
            tblAdolReg.setRegAdolMotherDesc(checkBoxMother.isChecked() ? "Yes" : "No");

            tblAdolReg.setRegAdolGuardian(aq.id(R.id.etadsguardianname).getEditText().getText().toString());

            tblAdolReg.setRegAdolFamilytype(aq.id(R.id.spnfamilytype).getSpinner().getSelectedItemPosition());
            tblAdolReg.setRegAdolAddress(aq.id(R.id.etaddr).getEditText().getText().toString());
            tblAdolReg.setRegAdolAreaname(aq.id(R.id.etareaname).getEditText().getText().toString());

            Integer villageCode = facilities.get(aq.id(R.id.spnfacilities).getSpinner().getSelectedItem().toString());
//             = facilities.get(aq.id(R.id.spnfacilities).getSpinner().getSelectedItem().toString());

            tblAdolReg.setRegAdolFacilities(villageCode);

            tblAdolReg.setRegAdolPincode(aq.id(R.id.etpincode).getEditText().getText().toString());

            tblAdolReg.setRegAdolRelationship(aq.id(R.id.spnRelationship).getSpinner().getSelectedItemPosition());

            if (aq.id(R.id.spnRelationship).getSpinner().getSelectedItem().toString().equals("Single")) {
                tblAdolReg.setRegAdolPartnerName("");
                tblAdolReg.setRegAdolPartnerOccupa("");
            } else {
                tblAdolReg.setRegAdolPartnerName(aq.id(R.id.etadshusbandname).getEditText().getText().toString());
                tblAdolReg.setRegAdolPartnerOccupa(aq.id(R.id.etadshusbandoccupa).getEditText().getText().toString());
                tblAdolReg.setRegAdolMarriageType(aq.id(R.id.spnmarriagetype).getSelectedItemPosition());

                if (aq.id(R.id.spnnoofchild).getSpinner().getSelectedItemPosition() == 0) {
                    tblAdolReg.setRegAdolNoofChild(0);
                } else {
                    tblAdolReg.setRegAdolNoofChild(Integer.parseInt(aq.id(R.id.spnnoofchild).getSpinner().getSelectedItem().toString()));
                    if (aq.id(R.id.spnnoofchild).getSpinner().getSelectedItemPosition() != 0 && aq.id(R.id.spnnoofchild).getSpinner().getSelectedItemPosition() != 1) {
                        setBornDetails();
                    }
                }
            }

            tblAdolReg.setRegAdolOtherHealthIssues(cbOther.isChecked() ? aq.id(R.id.ethealthissueothers).getEditText().getText().toString() : "");

            tblAdolReg.setRegAdolHealthIssues(healthIssues); //20May2021 Bindu


            tblAdolReg.setRegAdolLifeStyle(lifestyle); //20May2021 Bindu

            tblAdolReg.setRegAdolPhysicalActivity(physicalActivity); //20May2021 Bindu

            tblAdolReg.setRegAdolSuggestionGiven(aq.id(R.id.etsuggGiven).getEditText().getText().toString());
            tblAdolReg.setRegAdolComments(aq.id(R.id.etcomments).getEditText().getText().toString());
            tblAdolReg.setLastVisitCount(0);
            if (!isEdit) {
                tblAdolReg.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
            }
            tblAdolReg.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
            tblAdolReg.setRegAdolDeactivateDate("");//14-may 2021 ramesh to set deactive
            tblAdolReg.setRegAdolDeactivateReasons("");
            tblAdolReg.setRegAdolDeactivateReasonOthers("");
            tblAdolReg.setRegAdolDeactivateComments("");

//            04Sep2021 Arpitha
            editedWomanData = new tblregisteredwomen();
            editedWomanData.setRegWomanName(tblAdolReg.getRegAdolName());
            editedWomanData.setRegAge(Integer.parseInt(tblAdolReg.getRegAdolAge()));
            editedWomanData.setRegHeight(tblAdolReg.getRegAdolHeight());
            if(tblAdolReg.getRegAdolHeightType() == 1)
            editedWomanData.setRegheightUnit(2);
            else  if(tblAdolReg.getRegAdolHeightType() == 2)
                editedWomanData.setRegheightUnit(1);
            else  if(tblAdolReg.getRegAdolHeightType() == 3)
                editedWomanData.setRegheightUnit(3);
            editedWomanData.setRegWomanWeight(tblAdolReg.getRegAdolWeight());
            editedWomanData.setRegDateofBirth(tblAdolReg.getRegAdolDoB());
            editedWomanData.setRegWhoseMobileNo("" + tblAdolReg.getRegAdolMobileOf());
            editedWomanData.setRegPhoneNumber(tblAdolReg.getRegAdolMobile());
            editedWomanData.setRegVillage(tblAdolReg.getRegAdolFacilities());
            editedWomanData.setRegAddress(tblAdolReg.getRegAdolAddress());
            editedWomanData.setRegAreaName(tblAdolReg.getRegAdolAreaname());

            setErrorsNull();
            AlertDialog.Builder builder = new AlertDialog.Builder(AdolRegistration.this);
            if (childtoAdol) {
                builder.setMessage(getResources().getString(R.string.childwillbedeactivateddoyouwanttosave));
            } else {
                builder.setMessage(getResources().getString(R.string.m099));
            }
            builder.setCancelable(false);
            builder.setNegativeButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).setPositiveButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    try {
                        int result = 0;
                        if (isEdit) {
                            String sql = checkDataareSame(tblAdolReg);
                            if (sql.length() == 0) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.nochangesfound), Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(AdolRegistration.this, AdolescentHome.class);
                                intent.putExtra("tabItem", 0);
                                intent.putExtra("globalState", prepareBundle());
                                startActivity(intent);
                            } else {
                                TransactionHeaderRepository.iNewRecordTransNew(user.getUserId(), transID, "tblaudittrail", databaseHelper);
                                result = adolescentRepo.updateData(sql);
                                if (result > 0) {
                                    TransactionHeaderRepository.iUpdateRecordTransNew(user.getUserId(), transID, "tblAdolReg", sql, null, null, databaseHelper); //23May2021 Bindu set userid
                                }
                                if (updateSQLReg.length() > 0) {
                                    result = new WomanRepository(databaseHelper).updateWomenRegDataNew(appState.sessionUserId, updateSQLReg, transID, null, databaseHelper);
                                }
                            }
                        } else {
                            userRepository.UpdateLastAdolNumberNew(user.getUserId(), transID, databaseHelper);
                            result = adolescentRepo.insertData(tblAdolReg);
                            TransactionHeaderRepository.iNewRecordTransNew(user.getUserId(), transID, "tblAdolReg", databaseHelper);
                        }
                        if (aq.id(R.id.spnnoofchild).getSpinner().getSelectedItemPosition() != 0) {
                            saveBornData();
                        }
                        if (childtoAdol) {
                            String chilId = getIntent().getStringExtra("childId");
                            ChildRepository childRepository = new ChildRepository(databaseHelper);
                            TblChildInfo localChildData = childRepository.getChildbyChilID(chilId);
                            localChildData.setChildtoadolID(tblAdolReg.getAdolID());
                            localChildData.setChlDeactDate(DateTimeUtil.getTodaysDate());
                            localChildData.setChlDeactReason("4"); //string files are there, need to use english to compare string
                            localChildData.setChlDeactOtherReason("Child turned to Adolescent"); //string files are there, need to use english to compare string
                            localChildData.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
                            String sql = "update tblchildinfo set chlDeactDate = '"+localChildData.getChlDeactDate()+"', chlDeactReason = '"+localChildData.getChlDeactReason()+"', chlDeactOtherReason ='"+localChildData.getChlDeactOtherReason()+"' , childtoadolID = '"+localChildData.getChildtoadolID()+"',RecordUpdatedDate = '"+localChildData.getRecordUpdatedDate()
                                    +"' where chlID = '"+localChildData.getChildID()+"' and UserId = '"+localChildData.getUserId()+"';";
                            result = childRepository.updateChildDeact(sql);
                            if (result > 0) {
                                TransactionHeaderRepository.iUpdateRecordTransNew(user.getUserId(),transID,"tblchildinfo",sql,null,null,databaseHelper);
                            }
                        }
                        if (result > 0) {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AdolRegistration.this);
                            alertDialogBuilder.setCancelable(false);
                            if (isEdit) {
                                alertDialogBuilder.setMessage(getResources().getString(R.string.updatecompleted));
                            } else {
                                alertDialogBuilder.setMessage(getResources().getString(R.string.regcompleted));
                            }
                            alertDialogBuilder.setCancelable(false).setNegativeButton(
                                    getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                            Intent intent = new Intent(AdolRegistration.this, AdolescentHome.class);
                                            intent.putExtra("tabItem", 0);
                                            intent.putExtra("globalState", prepareBundle());
                                            startActivity(intent);
                                        }
                                    });
                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();
                        } else {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.registrationfailed), Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());

                    }
                }
            }).create().show();
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private void setErrorsNull() {
        try {
            aq.id(R.id.etAdsWeight).getEditText().setError(null);
            aq.id(R.id.etAdsheightcm).getEditText().setError(null);
            aq.id(R.id.etadsAadharCard).getEditText().setError(null);
            aq.id(R.id.etAdsage).getEditText().setError(null);
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private void saveBornData() {
        try {
            AdolPregnantRepository adolPregnantRepository = new AdolPregnantRepository(databaseHelper);
            Dao<tblAdolPregnant, Integer> dao = databaseHelper.gettblAdolPregnantRuntimeExceptionDao();
            long isAdded = 0;//14Jun2021 Arpitha
            if (bornlist != null)
                for (tblAdolPregnant s : bornlist) {
                    if (adolPregnantRepository.checkisBornexist(s.getAdolId(), s.getPregnantCount()).size() == 0)//to check is the born already exist or not
                        isAdded = dao.create(s);
                }
            //14Jun2021 Arpitha -  happens in single transaction
            if (isAdded > 0)
                TransactionHeaderRepository.iNewRecordTransNew(user.getUserId(), transID, dao.getTableName(), databaseHelper);

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }


    private void setBornDetails() {
        try {
            if (validationBornFields()) {
                setBornData();
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private void setBornData() {
        try {
            bornlist = new ArrayList<>();
            int bornCount = Integer.parseInt(aq.id(R.id.spnnoofchild).getSpinner().getSelectedItem().toString());

            for (int i = 1; i <= bornCount; i++) {
                tblAdolPregnant born = new tblAdolPregnant();
                born.setAdolId(AdolID);
                born.setTransId(transID);
                born.setUserId(user.getUserId()); //23May2021 Bindu - change to userid
                born.setPregnantCount(i + "");
                born.setTblAdolPreOutcome(aq.id(R.id.spnPregnantoutcomeofPregnancy + i).getSpinner().getSelectedItemPosition());
                born.setTblAdolPreDoD(aq.id(R.id.etPregnantDOD + i).getEditText().getText().toString());
                born.setTblAdolPreDeliverWhere(aq.id(R.id.spnPregnantwhereDeliverBaby + i).getSpinner().getSelectedItemPosition());
                born.setTblAdolPreWhoConductDelivery(aq.id(R.id.spnPregnantwhoConductedDelivery + i).getSpinner().getSelectedItemPosition());
                born.setTblAdolPreTransport(aq.id(R.id.spnPregnantwhomodeofTransport + i).getSpinner().getSelectedItemPosition());
                born.setTblAdolPreDeliveryType(aq.id(R.id.spnPregnanttypeofdelivery + i).getSpinner().getSelectedItemPosition());
                born.setTblAdolPreDeliveryStatus(aq.id(R.id.spnPregnantstatusofdelivery + i).getSpinner().getSelectedItemPosition());
                born.setTblAdolPreChildSex(aq.id(R.id.spnPregnantsexofchild + i).getSpinner().getSelectedItemPosition());
                born.setTblAdolPreChildAge(aq.id(R.id.etPregnantAgeatCurrentDate + i).getEditText().getText().toString());
                born.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
                bornlist.add(born);
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private boolean validationBornFields() {
        try {
            if (aq.id(R.id.spnnoofchild).getSpinner().getSelectedItemPosition() > 1) {
                aq.id(R.id.llPP_FirstBornBody).getView().setVisibility(View.VISIBLE);
                aq.id(R.id.imgchl1expand).getImageView().setBackground(getResources().getDrawable(R.drawable.expand));
                aq.id(R.id.llPP_SecondBornBody).getView().setVisibility(View.GONE);
                aq.id(R.id.imgchl2expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                aq.id(R.id.llPP_ThirdBornBody).getView().setVisibility(View.GONE);
                aq.id(R.id.imgchl3expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                aq.id(R.id.llPP_FourthBornBody).getView().setVisibility(View.GONE);
                aq.id(R.id.imgchl4expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));

                if (aq.id(R.id.spnPregnantoutcomeofPregnancy1).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenteroutcomeofpregnancy), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD1).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantoutcomeofPregnancy1).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseenteroutcomeofpregnancy));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.etPregnantDOD1).getEditText().getText().toString().length() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseselectdateofdelivery), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD1).getEditText().requestFocus();
                    return false;
                } else if (aq.id(R.id.spnPregnantwhereDeliverBaby1).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenterdeliveryplace), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD1).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantwhereDeliverBaby1).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseenterdeliveryplace));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnantwhoConductedDelivery1).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenterwhoconducteddelivery), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD1).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantwhoConductedDelivery1).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseenterwhoconducteddelivery));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnantwhomodeofTransport1).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseentermodeoftransport), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD1).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantwhomodeofTransport1).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseentermodeoftransport));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnanttypeofdelivery1).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseentertypeofdelivery), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD1).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnanttypeofdelivery1).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseentertypeofdelivery));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnantstatusofdelivery1).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenterstatusofdelivery), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD1).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantstatusofdelivery1).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseenterstatusofdelivery));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnantsexofchild1).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseselectsexofchild), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD1).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantsexofchild1).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseselectsexofchild));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.etPregnantAgeatCurrentDate1).getEditText().getText().toString().length() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenterageofchildatcurrent), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantAgeatCurrentDate1).getEditText().requestFocus();
                    return false;
                } else {
                    aq.id(R.id.llPP_FirstBornBody).getView().setVisibility(View.GONE);
                    aq.id(R.id.imgchl1expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                }

            }
            if (aq.id(R.id.spnnoofchild).getSpinner().getSelectedItemPosition() > 2) {
                aq.id(R.id.llPP_FirstBornBody).getView().setVisibility(View.GONE);
                aq.id(R.id.imgchl1expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                aq.id(R.id.llPP_SecondBornBody).getView().setVisibility(View.VISIBLE);
                aq.id(R.id.imgchl2expand).getImageView().setBackground(getResources().getDrawable(R.drawable.expand));
                aq.id(R.id.llPP_ThirdBornBody).getView().setVisibility(View.GONE);
                aq.id(R.id.imgchl3expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                aq.id(R.id.llPP_FourthBornBody).getView().setVisibility(View.GONE);
                aq.id(R.id.imgchl4expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));

                if (aq.id(R.id.spnPregnantoutcomeofPregnancy2).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenteroutcomeofpregnancy), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD2).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantoutcomeofPregnancy2).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseenteroutcomeofpregnancy));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.etPregnantDOD2).getEditText().getText().toString().length() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseselectdateofdelivery), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD2).getEditText().requestFocus();
                    return false;
                } else if (aq.id(R.id.spnPregnantwhereDeliverBaby2).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenterdeliveryplace), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD2).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantwhereDeliverBaby2).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseenterdeliveryplace));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnantwhoConductedDelivery2).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenterwhoconducteddelivery), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD2).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantwhoConductedDelivery2).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseenterwhoconducteddelivery));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnantwhomodeofTransport2).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseentermodeoftransport), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD2).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantwhomodeofTransport2).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseentermodeoftransport));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnanttypeofdelivery2).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseentertypeofdelivery), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD2).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnanttypeofdelivery2).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseentertypeofdelivery));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnantstatusofdelivery2).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenterstatusofdelivery), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD2).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantstatusofdelivery2).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseenterstatusofdelivery));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnantsexofchild2).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseselectsexofchild), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD2).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantsexofchild2).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseselectsexofchild));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.etPregnantAgeatCurrentDate2).getEditText().getText().toString().length() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenterageofchildatcurrent), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantAgeatCurrentDate2).getEditText().requestFocus();
                    return false;
                } else {
                    aq.id(R.id.llPP_SecondBornBody).getView().setVisibility(View.GONE);
                    aq.id(R.id.imgchl2expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                }

            }
            if (aq.id(R.id.spnnoofchild).getSpinner().getSelectedItemPosition() > 3) {
                aq.id(R.id.llPP_FirstBornBody).getView().setVisibility(View.GONE);
                aq.id(R.id.imgchl1expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                aq.id(R.id.llPP_SecondBornBody).getView().setVisibility(View.GONE);
                aq.id(R.id.imgchl2expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                aq.id(R.id.llPP_ThirdBornBody).getView().setVisibility(View.VISIBLE);
                aq.id(R.id.imgchl3expand).getImageView().setBackground(getResources().getDrawable(R.drawable.expand));
                aq.id(R.id.llPP_FourthBornBody).getView().setVisibility(View.GONE);
                aq.id(R.id.imgchl4expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));

                if (aq.id(R.id.spnPregnantoutcomeofPregnancy3).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenteroutcomeofpregnancy), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD3).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantoutcomeofPregnancy3).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseenteroutcomeofpregnancy));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.etPregnantDOD3).getEditText().getText().toString().length() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseselectdateofdelivery), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD3).getEditText().requestFocus();
                    return false;
                } else if (aq.id(R.id.spnPregnantwhereDeliverBaby3).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenterdeliveryplace), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD3).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantwhereDeliverBaby3).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseenterdeliveryplace));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnantwhoConductedDelivery3).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenterwhoconducteddelivery), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD3).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantwhoConductedDelivery3).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseenterwhoconducteddelivery));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnantwhomodeofTransport3).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseentermodeoftransport), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD3).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantwhomodeofTransport3).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseentermodeoftransport));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnanttypeofdelivery3).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseentertypeofdelivery), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD3).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnanttypeofdelivery3).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseentertypeofdelivery));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnantstatusofdelivery3).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenterstatusofdelivery), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD3).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantstatusofdelivery3).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseenterstatusofdelivery));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnantsexofchild3).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseselectsexofchild), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD3).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantsexofchild3).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseselectsexofchild));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.etPregnantAgeatCurrentDate3).getEditText().getText().toString().length() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenterageofchildatcurrent), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantAgeatCurrentDate3).getEditText().requestFocus();
                    return false;
                } else {
                    aq.id(R.id.llPP_ThirdBornBody).getView().setVisibility(View.GONE);
                    aq.id(R.id.imgchl4expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                }

            }
            if (aq.id(R.id.spnnoofchild).getSpinner().getSelectedItemPosition() > 4) {
                aq.id(R.id.llPP_FirstBornBody).getView().setVisibility(View.GONE);
                aq.id(R.id.imgchl1expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                aq.id(R.id.llPP_SecondBornBody).getView().setVisibility(View.GONE);
                aq.id(R.id.imgchl2expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                aq.id(R.id.llPP_ThirdBornBody).getView().setVisibility(View.GONE);
                aq.id(R.id.imgchl3expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                aq.id(R.id.llPP_FourthBornBody).getView().setVisibility(View.VISIBLE);
                aq.id(R.id.imgchl4expand).getImageView().setBackground(getResources().getDrawable(R.drawable.expand));

                if (aq.id(R.id.spnPregnantoutcomeofPregnancy4).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenteroutcomeofpregnancy), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD4).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantoutcomeofPregnancy4).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseenteroutcomeofpregnancy));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.etPregnantDOD4).getEditText().getText().toString().length() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseselectdateofdelivery), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD4).getEditText().requestFocus();
                    return false;
                } else if (aq.id(R.id.spnPregnantwhereDeliverBaby4).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenterdeliveryplace), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD4).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantwhereDeliverBaby4).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseenterdeliveryplace));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnantwhoConductedDelivery4).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenterwhoconducteddelivery), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD4).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantwhoConductedDelivery4).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseenterwhoconducteddelivery));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnantwhomodeofTransport4).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseentermodeoftransport), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD4).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantwhomodeofTransport4).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseentermodeoftransport));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnanttypeofdelivery4).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseentertypeofdelivery), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD4).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnanttypeofdelivery4).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseentertypeofdelivery));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnantstatusofdelivery4).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenterstatusofdelivery), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD4).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantstatusofdelivery4).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseenterstatusofdelivery));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnantsexofchild4).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseselectsexofchild), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD4).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantsexofchild4).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseselectsexofchild));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.etPregnantAgeatCurrentDate4).getEditText().getText().toString().length() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenterageofchildatcurrent), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantAgeatCurrentDate4).getEditText().requestFocus();
                    return false;
                } else {
                    aq.id(R.id.llPP_FourthBornBody).getView().setVisibility(View.GONE);
                    aq.id(R.id.imgchl4expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                }

            }
            return true;
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return false;
    }

    private String checkDataareSame(tblAdolReg tblAdolReg) throws SQLException {
        String updateSQL = "";
        String addQuery = "";

        WomanRepository womanRepository = new WomanRepository(databaseHelper);
        String womanId = new AdolescentRepository(databaseHelper).
                getAdolANCWomanId(adolEditData.getRegAdolAadharNo(), adolEditData.getAdolID());
        if (womanId != null && womanId.trim().length() > 0)
            regData = new WomanRepository(databaseHelper).getRegistartionDetails(womanId, appState.sessionUserId);

        String addRegDetailsQuery = "";//04Sep2021 Arpitha

        try {
            if (!tblAdolReg.getRegAdolName().equals(adolEditData.getRegAdolName())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " regAdolName = " + (char) 34 + tblAdolReg.getRegAdolName() + (char) 34 + "";
                    if (regData != null) {
                        if(addRegDetailsQuery.equals(""))
                        addRegDetailsQuery = " regWomanName = " + (char) 34 + editedWomanData.getRegWomanName() + (char) 34 + "";//04Sep2021 Arpitha
                   else
                            addRegDetailsQuery = " , regWomanName = " + (char) 34 + editedWomanData.getRegWomanName() + (char) 34 + "";//04Sep2021 Arpitha

                    }
                } else {
                    addQuery = addQuery + " ,regAdolName = " + (char) 34 + tblAdolReg.getRegAdolName() + (char) 34 + "";
                    if (regData != null)
                    {
                        if(addRegDetailsQuery.equals(""))
                            addRegDetailsQuery = " regWomanName = " + (char) 34 + editedWomanData.getRegWomanName() + (char) 34 + "";//04Sep2021 Arpitha
                        else
                            addRegDetailsQuery = ", regWomanName = " + (char) 34 + editedWomanData.getRegWomanName() + (char) 34 + "";//04Sep2021 Arpitha

                    }                }
                InserttblAuditTrail("regAdolName", adolEditData.getRegAdolName(), tblAdolReg.getRegAdolName(), transID, "tblAdolReg");
                if (regData != null)
                    InserttblAuditTrail("regWomanName",
                            regData.getRegWomanName(), editedWomanData.getRegWomanName(),
                            transID, "tblregisteredwomen");//04Sep2021 Arpitha
            }
            if (!tblAdolReg.getRegAdolAge().equals(adolEditData.getRegAdolAge())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " regAdolAge = " + (char) 34 + tblAdolReg.getRegAdolAge() + (char) 34 + "";
                    if (regData != null) {
                        if(addRegDetailsQuery.equals(""))
                        addRegDetailsQuery = addRegDetailsQuery + " regAge = " + editedWomanData.getRegAge() + " ";//04Sep2021 Arpitha
                        else
                            addRegDetailsQuery = addRegDetailsQuery + " , regAge = " + editedWomanData.getRegAge() + " ";//04Sep2021 Arpitha

                    }
                } else {
                    addQuery = addQuery + " ,regAdolAge = " + (char) 34 + tblAdolReg.getRegAdolAge() + (char) 34 + "";
                    if (regData != null)
                    {
                        if(addRegDetailsQuery.equals(""))
                            addRegDetailsQuery = addRegDetailsQuery + " regAge = " + editedWomanData.getRegAge() + " ";//04Sep2021 Arpitha
                        else
                            addRegDetailsQuery = addRegDetailsQuery + " , regAge = " + editedWomanData.getRegAge() + " ";//04Sep2021 Arpitha

                    }                }
                InserttblAuditTrail("regAdolAge", adolEditData.getRegAdolAge(), tblAdolReg.getRegAdolAge(), transID, "tblAdolReg");
                if (regData != null) InserttblAuditTrail("regAge",
                        String.valueOf(regData.getRegAge()),
                        String.valueOf(editedWomanData.getRegAge()),
                        transID, "tblregisteredwomen");//04Sep2021 Arpitha

            }
            if (!tblAdolReg.getRegAdolPregnant().equals(adolEditData.getRegAdolPregnant())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " regAdolPregnant = " + (char) 34 + tblAdolReg.getRegAdolPregnant() + (char) 34 + "";
                } else {
                    addQuery = addQuery + " ,regAdolPregnant = " + (char) 34 + tblAdolReg.getRegAdolPregnant() + (char) 34 + "";
                }
                InserttblAuditTrail("regAdolPregnant", adolEditData.getRegAdolPregnant(), tblAdolReg.getRegAdolPregnant(), transID, "tblAdolReg");
            }
            if (!tblAdolReg.getRegAdolisPeriod().equals(adolEditData.getRegAdolisPeriod())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " regAdolisPeriod = " + (char) 34 + tblAdolReg.getRegAdolisPeriod() + (char) 34 + "";
                } else {
                    addQuery = addQuery + " ,regAdolisPeriod = " + (char) 34 + tblAdolReg.getRegAdolisPeriod() + (char) 34 + "";
                }
                InserttblAuditTrail("regAdolisPeriod", adolEditData.getRegAdolisPeriod(), tblAdolReg.getRegAdolisPeriod(), transID, "tblAdolReg");
            }
            if (!tblAdolReg.getRegAdolAgeatMenstruated().equals(adolEditData.getRegAdolAgeatMenstruated())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " regAdolAgeatMenstruated = " + (char) 34 + tblAdolReg.getRegAdolAgeatMenstruated() + (char) 34 + "";
                } else {
                    addQuery = addQuery + " ,regAdolAgeatMenstruated = " + (char) 34 + tblAdolReg.getRegAdolAgeatMenstruated() + (char) 34 + "";
                }
                InserttblAuditTrail("regAdolAgeatMenstruated", adolEditData.getRegAdolAgeatMenstruated(), tblAdolReg.getRegAdolAgeatMenstruated(), transID, "tblAdolReg");
            }
            if (!tblAdolReg.getRegAdolDoB().equals(adolEditData.getRegAdolDoB())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " regAdolDoB = " + (char) 34 + tblAdolReg.getRegAdolDoB() + (char) 34 + "";
                    if (regData != null) {
                        if(addRegDetailsQuery.equals(""))
                        addRegDetailsQuery = addRegDetailsQuery + " regDateofBirth = " + (char) 34 + editedWomanData.getRegDateofBirth() + (char) 34 + "";//04Sep2021 Arpitha
                   else
                            addRegDetailsQuery = addRegDetailsQuery + ", regDateofBirth = " + (char) 34 + editedWomanData.getRegDateofBirth() + (char) 34 + "";//04Sep2021 Arpitha

                    }
                } else {
                    addQuery = addQuery + " ,regAdolDoB = " + (char) 34 + tblAdolReg.getRegAdolDoB() + (char) 34 + "";

                    if (regData != null) {
                        if(addRegDetailsQuery.equals(""))
                        addRegDetailsQuery = addRegDetailsQuery + " regDateofBirth = " + (char) 34 + editedWomanData.getRegDateofBirth() + (char) 34 + "";//04Sep2021 Arpitha
                   else
                            addRegDetailsQuery = addRegDetailsQuery + " ,regDateofBirth = " + (char) 34 + editedWomanData.getRegDateofBirth() + (char) 34 + "";//04Sep2021 Arpitha
                    }
                    }
                InserttblAuditTrail("regAdolDoB", adolEditData.getRegAdolDoB(), tblAdolReg.getRegAdolDoB(), transID, "tblAdolReg");

                if (regData != null) InserttblAuditTrail("regDateofBirth",
                        String.valueOf(regData.getRegDateofBirth()),
                        String.valueOf(editedWomanData.getRegDateofBirth()),
                        transID, "tblregisteredwomen");//04Sep2021 Arpitha
            }

            if (!tblAdolReg.getRegAdolWeight().equals(adolEditData.getRegAdolWeight())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " regAdolWeight = " + (char) 34 + tblAdolReg.getRegAdolWeight() + (char) 34 + "";
                    if (regData != null) {
                        if(addRegDetailsQuery.equals(""))
                        addRegDetailsQuery = addRegDetailsQuery + " regWomanWeight = " + editedWomanData.getRegWomanWeight() + "";//04Sep2021 Arpitha
                  else
                            addRegDetailsQuery = addRegDetailsQuery + ", regWomanWeight = " + editedWomanData.getRegWomanWeight() + "";//04Sep2021 Arpitha

                    }
                } else {
                    addQuery = addQuery + " ,regAdolWeight = " + (char) 34 + tblAdolReg.getRegAdolWeight() + (char) 34 + "";
                    if (regData != null) {
                        if(addRegDetailsQuery.equals(""))
                        addRegDetailsQuery = addRegDetailsQuery + " regWomanWeight = " + editedWomanData.getRegWomanWeight() + "";//04Sep2021 Arpitha
                  else
                            addRegDetailsQuery = addRegDetailsQuery + " ,regWomanWeight = " + editedWomanData.getRegWomanWeight() + "";//04Sep2021 Arpitha

                    }
                }
                InserttblAuditTrail("regAdolWeight", adolEditData.getRegAdolWeight(), tblAdolReg.getRegAdolWeight(), transID, "tblAdolReg");
                if (regData != null) InserttblAuditTrail("regWomanWeight",
                        String.valueOf(regData.getRegWomanWeight()),
                        String.valueOf(editedWomanData.getRegWomanWeight()),
                        transID, "tblregisteredwomen");//04Sep2021 Arpitha
            }
            if (!(tblAdolReg.getRegAdolHeightType() == adolEditData.getRegAdolHeightType())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " regAdolHeightType = " + tblAdolReg.getRegAdolHeightType() + "";
                    if (regData != null) {
                        if(addRegDetailsQuery.equals(""))
                        addRegDetailsQuery = addRegDetailsQuery + " regheightUnit = " + editedWomanData.getRegheightUnit() + "";//04Sep2021 Arpitha
                    else
                            addRegDetailsQuery = addRegDetailsQuery + " , regheightUnit = " + editedWomanData.getRegheightUnit() + "";//04Sep2021 Arpitha

                    }
                } else {
                    addQuery = addQuery + " ,regAdolHeightType = " + tblAdolReg.getRegAdolHeightType() + "";
                    if (regData != null)
                        if(addRegDetailsQuery.equals(""))
                        addRegDetailsQuery = addRegDetailsQuery + " regheightUnit = " + editedWomanData.getRegheightUnit() + "";//04Sep2021 Arpitha
else
                            addRegDetailsQuery = addRegDetailsQuery + " ,regheightUnit = " + editedWomanData.getRegheightUnit() + "";//04Sep2021 Arpitha

                }
                InserttblAuditTrail("regAdolHeightType", adolEditData.getRegAdolHeightType() + "", tblAdolReg.getRegAdolHeightType() + "", transID, "tblAdolReg");
                if (regData != null) InserttblAuditTrail("regheightUnit",
                        String.valueOf(regData.getRegheightUnit()),
                        String.valueOf(editedWomanData.getRegheightUnit()),
                        transID, "tblregisteredwomen");//04Sep2021 Arpitha
            }
            if (!tblAdolReg.getRegAdolHeight().equals(adolEditData.getRegAdolHeight())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " regAdolHeight = " + (char) 34 + tblAdolReg.getRegAdolHeight() + (char) 34 + "";
                    if (regData != null) {
                        if(addRegDetailsQuery.equals(""))
                        addRegDetailsQuery = addRegDetailsQuery + " regHeight = " + (char) 34 + editedWomanData.getRegHeight() + (char) 34 + "";//04Sep2021 Arpitha
else
                            addRegDetailsQuery = addRegDetailsQuery + ", regHeight = " + (char) 34 + editedWomanData.getRegHeight() + (char) 34 + "";//04Sep2021 Arpitha

                    }
                } else {
                    addQuery = addQuery + " ,regAdolHeight = " + (char) 34 + tblAdolReg.getRegAdolHeight() + (char) 34 + "";
                    if (regData != null) {
                        if(addRegDetailsQuery.equals(""))
                        addRegDetailsQuery = addRegDetailsQuery + " regHeight = " + (char) 34 + editedWomanData.getRegHeight() + (char) 34 + "";//04Sep2021 Arpitha
else
                            addRegDetailsQuery = addRegDetailsQuery + " ,regHeight = " + (char) 34 + editedWomanData.getRegHeight() + (char) 34 + "";//04Sep2021 Arpitha

                    }
                }

                InserttblAuditTrail("regAdolHeight", adolEditData.getRegAdolHeight(), tblAdolReg.getRegAdolHeight(), transID, "tblAdolReg");
                if (regData != null)
                    InserttblAuditTrail("regHeight",
                            String.valueOf(regData.getRegHeight()),
                            String.valueOf(editedWomanData.getRegHeight()),
                            transID, "tblregisteredwomen"); //04Sep2021 Arpitha
            }

            if (!(tblAdolReg.getRegAdolMobileOf() == adolEditData.getRegAdolMobileOf())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " regAdolMobileOf = " + tblAdolReg.getRegAdolMobileOf() + "";
                    if (regData != null) {
                        if(addRegDetailsQuery.equals(""))
                        addRegDetailsQuery = addRegDetailsQuery + " regWhoseMobileNo = " + (char) 34 + editedWomanData.getRegWhoseMobileNo() + (char) 34 + "";//04Sep2021 Arpitha
else
                            addRegDetailsQuery = addRegDetailsQuery + " , regWhoseMobileNo = " + (char) 34 + editedWomanData.getRegWhoseMobileNo() + (char) 34 + "";//04Sep2021 Arpitha

                    }
                } else {
                    addQuery = addQuery + " ,regAdolMobileOf = " + tblAdolReg.getRegAdolMobileOf() + "";
                    if (regData != null) {
                        if(addRegDetailsQuery.equals(""))
                        addRegDetailsQuery = addRegDetailsQuery + " regWhoseMobileNo = " + (char) 34 + editedWomanData.getRegWhoseMobileNo() + (char) 34 + "";//04Sep2021 Arpitha
else
                            addRegDetailsQuery = addRegDetailsQuery + " , regWhoseMobileNo = " + (char) 34 + editedWomanData.getRegWhoseMobileNo() + (char) 34 + "";//04Sep2021 Arpitha

                    }
                }
                InserttblAuditTrail("regAdolMobileOf", adolEditData.getRegAdolMobileOf() + "", tblAdolReg.getRegAdolMobileOf() + "", transID, "tblAdolReg");
                if (regData != null) InserttblAuditTrail("regWhoseMobileNo",
                        String.valueOf(regData.getRegWhoseMobileNo()),
                        String.valueOf(editedWomanData.getRegWhoseMobileNo()),
                        transID, "tblregisteredwomen");//04Sep2021 Arpitha
            }
            if (!(tblAdolReg.getRegAdolEducation() == adolEditData.getRegAdolEducation())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " regAdolEducation = " + tblAdolReg.getRegAdolEducation() + "";
                } else {
                    addQuery = addQuery + " ,regAdolEducation = " + tblAdolReg.getRegAdolEducation() + "";
                }
                InserttblAuditTrail("regAdolEducation", adolEditData.getRegAdolEducation() + "", tblAdolReg.getRegAdolEducation() + "", transID, "tblAdolReg");
            }
            if (!tblAdolReg.getRegAdolMobile().equals(adolEditData.getRegAdolMobile())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " regAdolMobile = " + (char) 34 + tblAdolReg.getRegAdolMobile() + (char) 34 + "";
                    if (regData != null) {
                        if(addRegDetailsQuery.equals(""))
                        addRegDetailsQuery = addRegDetailsQuery + " regPhoneNumber = " + editedWomanData.getRegPhoneNumber() + "";//04Sep2021 Arpitha
else
                            addRegDetailsQuery = addRegDetailsQuery + " , regPhoneNumber = " + editedWomanData.getRegPhoneNumber() + "";//04Sep2021 Arpitha
                    }
                } else {
                    addQuery = addQuery + " ,regAdolMobile = " + (char) 34 + tblAdolReg.getRegAdolMobile() + (char) 34 + "";
                    if (regData != null) {
                        if(addRegDetailsQuery.equals(""))
                        addRegDetailsQuery = addRegDetailsQuery + " regPhoneNumber = " + editedWomanData.getRegPhoneNumber() + "";//04Sep2021 Arpitha

                        else
                            addRegDetailsQuery = addRegDetailsQuery + " , regPhoneNumber = " + editedWomanData.getRegPhoneNumber() + "";//04Sep2021 Arpitha

                    }
                }
                InserttblAuditTrail("regAdolMobile", adolEditData.getRegAdolMobile(), tblAdolReg.getRegAdolMobile(), transID, "tblAdolReg");
                if (regData != null) InserttblAuditTrail("regPhoneNumber",
                        String.valueOf(regData.getRegPhoneNumber()),
                        String.valueOf(editedWomanData.getRegPhoneNumber()),
                        transID, "tblregisteredwomen");//04Sep2021 Arpitha
            }
            if (!tblAdolReg.getRegAdolFather().equals(adolEditData.getRegAdolFather())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " regAdolFather = " + (char) 34 + tblAdolReg.getRegAdolFather() + (char) 34 + "";
                } else {
                    addQuery = addQuery + " ,regAdolFather = " + (char) 34 + tblAdolReg.getRegAdolFather() + (char) 34 + "";
                }
                InserttblAuditTrail("regAdolFather", adolEditData.getRegAdolFather(), tblAdolReg.getRegAdolFather(), transID, "tblAdolReg");
            }
            if (!tblAdolReg.getRegAdolFatherDesc().equals(adolEditData.getRegAdolFatherDesc())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " regAdolFatherDesc = " + (char) 34 + tblAdolReg.getRegAdolFatherDesc() + (char) 34 + "";
                } else {
                    addQuery = addQuery + " ,regAdolFatherDesc = " + (char) 34 + tblAdolReg.getRegAdolFatherDesc() + (char) 34 + "";
                }
                InserttblAuditTrail("regAdolFatherDesc", adolEditData.getRegAdolFatherDesc(), tblAdolReg.getRegAdolFatherDesc(), transID, "tblAdolReg");
            }
            if (!tblAdolReg.getRegAdolMother().equals(adolEditData.getRegAdolMother())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " regAdolMother = " + (char) 34 + tblAdolReg.getRegAdolMother() + (char) 34 + "";
                } else {
                    addQuery = addQuery + " ,regAdolMother = " + (char) 34 + tblAdolReg.getRegAdolMother() + (char) 34 + "";
                }
                InserttblAuditTrail("regAdolMother", adolEditData.getRegAdolMother(), tblAdolReg.getRegAdolMother(), transID, "tblAdolReg");
            }
            if (!tblAdolReg.getRegAdolMotherDesc().equals(adolEditData.getRegAdolMotherDesc())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " regAdolMotherDesc = " + (char) 34 + tblAdolReg.getRegAdolMotherDesc() + (char) 34 + "";
                } else {
                    addQuery = addQuery + " ,regAdolMotherDesc = " + (char) 34 + tblAdolReg.getRegAdolMotherDesc() + (char) 34 + "";
                }
                InserttblAuditTrail("regAdolMotherDesc", adolEditData.getRegAdolMotherDesc(), tblAdolReg.getRegAdolMotherDesc(), transID, "tblAdolReg");
            }
            if (!tblAdolReg.getRegAdolGuardian().equals(adolEditData.getRegAdolGuardian())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " regAdolGuardian = " + (char) 34 + tblAdolReg.getRegAdolGuardian() + (char) 34 + "";
                } else {
                    addQuery = addQuery + " ,regAdolGuardian = " + (char) 34 + tblAdolReg.getRegAdolGuardian() + (char) 34 + "";
                }
                InserttblAuditTrail("regAdolGuardian", adolEditData.getRegAdolGuardian(), tblAdolReg.getRegAdolGuardian(), transID, "tblAdolReg");
            }

            if (!(tblAdolReg.getRegAdolFamilytype() == adolEditData.getRegAdolFamilytype())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " regAdolFamilytype = " + tblAdolReg.getRegAdolFamilytype() + "";
                } else {
                    addQuery = addQuery + " ,regAdolFamilytype = " + tblAdolReg.getRegAdolFamilytype() + "";
                }
                InserttblAuditTrail("regAdolFamilytype", adolEditData.getRegAdolFamilytype() + "", tblAdolReg.getRegAdolFamilytype() + "", transID, "tblAdolReg");
            }
            if (!tblAdolReg.getRegAdolAddress().equals(adolEditData.getRegAdolAddress())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " regAdolAddress = " + (char) 34 + tblAdolReg.getRegAdolAddress() + (char) 34 + "";
                    if (regData != null) {
                        if(addRegDetailsQuery.equals(""))
                        addRegDetailsQuery = addRegDetailsQuery + " regAddress = " + (char) 34 + editedWomanData.getRegAddress() + (char) 34 + "";//04Sep2021 Arpitha
                        else
                            addRegDetailsQuery = addRegDetailsQuery + " , regAddress = " + (char) 34 + editedWomanData.getRegAddress() + (char) 34 + "";//04Sep2021 Arpitha

                    }
                } else {
                    addQuery = addQuery + " ,regAdolAddress = " + (char) 34 + tblAdolReg.getRegAdolAddress() + (char) 34 + "";
                    if (regData != null) {
                        if(addRegDetailsQuery.equals(""))
                        addRegDetailsQuery = addRegDetailsQuery + " regAddress = " + (char) 34 + editedWomanData.getRegAddress() + (char) 34 + "";//04Sep2021 Arpitha
else
                            addRegDetailsQuery = addRegDetailsQuery + " ,regAddress = " + (char) 34 + editedWomanData.getRegAddress() + (char) 34 + "";//04Sep2021 Arpitha

                    }
                }
                InserttblAuditTrail("regAdolAddress", adolEditData.getRegAdolAddress(), tblAdolReg.getRegAdolAddress(), transID, "tblAdolReg");
                if (regData != null) InserttblAuditTrail("regAddress",
                        String.valueOf(regData.getRegAddress()),
                        String.valueOf(editedWomanData.getRegAddress()),
                        transID, "tblregisteredwomen");//04Sep2021 Arpitha
            }
            if (!tblAdolReg.getRegAdolAreaname().equals(adolEditData.getRegAdolAreaname())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " regAdolAreaname = " + (char) 34 + tblAdolReg.getRegAdolAreaname() + (char) 34 + "";
                    if (regData != null) {
                        if(addRegDetailsQuery.equals(""))
                        addRegDetailsQuery = addRegDetailsQuery + " regAreaName = " + (char) 34 + editedWomanData.getRegAreaName() + (char) 34 + "";//04Sep2021 Arpitha
else
                            addRegDetailsQuery = addRegDetailsQuery + ", regAreaName = " + (char) 34 + editedWomanData.getRegAreaName() + (char) 34 + "";//04Sep2021 Arpitha

                    }
                } else {
                    addQuery = addQuery + " ,regAdolAreaname = " + (char) 34 + tblAdolReg.getRegAdolAreaname() + (char) 34 + "";
                    if (regData != null) {
                        if(addRegDetailsQuery.equals(""))
                        addRegDetailsQuery = addRegDetailsQuery + " regAreaName = " + (char) 34 + editedWomanData.getRegAreaName() + (char) 34 + "";//04Sep2021 Arpitha
else
                            addRegDetailsQuery = addRegDetailsQuery + " ,regAreaName = " + (char) 34 + editedWomanData.getRegAreaName() + (char) 34 + "";//04Sep2021 Arpitha

                    }
                }
                InserttblAuditTrail("regAdolAreaname", adolEditData.getRegAdolAreaname(), tblAdolReg.getRegAdolAreaname(), transID, "tblAdolReg");
                if (regData != null) InserttblAuditTrail("regAreaName",
                        String.valueOf(regData.getRegAreaName()),
                        String.valueOf(editedWomanData.getRegAreaName()),
                        transID, "tblregisteredwomen");//04Sep2021 Arpitha
            }
            if (!(tblAdolReg.getRegAdolFacilities() == adolEditData.getRegAdolFacilities())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " regAdolFacilities = " + tblAdolReg.getRegAdolMobileOf() + "";
                    if (regData != null) {
                        if(addRegDetailsQuery.equals(""))
                        addRegDetailsQuery = addRegDetailsQuery + " regVillage = " + editedWomanData.getRegVillage() + "";//04Sep2021 Arpitha
                   else
                            addRegDetailsQuery = addRegDetailsQuery + ", regVillage = " + editedWomanData.getRegVillage() + "";//04Sep2021 Arpitha

                    }
                } else {
                    addQuery = addQuery + " ,regAdolFacilities = " + tblAdolReg.getRegAdolMobileOf() + "";
                    if (regData != null) {
                        if(addRegDetailsQuery.equals(""))
                        addRegDetailsQuery = addRegDetailsQuery + " regVillage = " + editedWomanData.getRegVillage() + "";//04Sep2021 Arpitha
                  else
                            addRegDetailsQuery = addRegDetailsQuery + " ,regVillage = " + editedWomanData.getRegVillage() + "";//04Sep2021 Arpitha

                    }
                }
                InserttblAuditTrail("regAdolFacilities", adolEditData.getRegAdolFacilities() + "", tblAdolReg.getRegAdolFacilities() + "", transID, "tblAdolReg");
                if (regData != null) InserttblAuditTrail("regVillage",
                        String.valueOf(regData.getRegVillage()),
                        String.valueOf(editedWomanData.getRegVillage()),
                        transID, "tblregisteredwomen");//04Sep2021 Arpitha
            }
            if (!tblAdolReg.getRegAdolPincode().equals(adolEditData.getRegAdolPincode())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " regAdolPincode = " + (char) 34 + tblAdolReg.getRegAdolPincode() + (char) 34 + "";
                } else {
                    addQuery = addQuery + " ,regAdolPincode = " + (char) 34 + tblAdolReg.getRegAdolPincode() + (char) 34 + "";
                }
                InserttblAuditTrail("regAdolPincode", adolEditData.getRegAdolPincode(), tblAdolReg.getRegAdolPincode(), transID, "tblAdolReg");
            }
            if (!(tblAdolReg.getRegAdolRelationship() == adolEditData.getRegAdolRelationship())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " regAdolRelationship = " + tblAdolReg.getRegAdolRelationship() + "";
                } else {
                    addQuery = addQuery + " ,regAdolRelationship = " + tblAdolReg.getRegAdolRelationship() + "";
                }
                InserttblAuditTrail("regAdolRelationship", adolEditData.getRegAdolRelationship() + "", tblAdolReg.getRegAdolRelationship() + "", transID, "tblAdolReg");
            }
            if (!tblAdolReg.getRegAdolPartnerName().equals(adolEditData.getRegAdolPartnerName())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " regAdolPartnerName = " + (char) 34 + tblAdolReg.getRegAdolPartnerName() + (char) 34 + "";
                } else {
                    addQuery = addQuery + " ,regAdolPartnerName = " + (char) 34 + tblAdolReg.getRegAdolPartnerName() + (char) 34 + "";
                }
                InserttblAuditTrail("regAdolPartnerName", adolEditData.getRegAdolPartnerName(), tblAdolReg.getRegAdolPartnerName(), transID, "tblAdolReg");
            }
            if (!tblAdolReg.getRegAdolPartnerOccupa().equals(adolEditData.getRegAdolPartnerOccupa())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " regAdolPartnerOccupa = " + (char) 34 + tblAdolReg.getRegAdolPartnerOccupa() + (char) 34 + "";
                } else {
                    addQuery = addQuery + " ,regAdolPartnerOccupa = " + (char) 34 + tblAdolReg.getRegAdolPartnerOccupa() + (char) 34 + "";
                }
                InserttblAuditTrail("regAdolPartnerOccupa", adolEditData.getRegAdolPartnerOccupa(), tblAdolReg.getRegAdolPartnerOccupa(), transID, "tblAdolReg");
            }
            if (!(tblAdolReg.getRegAdolMarriageType() == adolEditData.getRegAdolMarriageType())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " regAdolMarriageType = " + tblAdolReg.getRegAdolMarriageType() + "";
                } else {
                    addQuery = addQuery + " ,regAdolMarriageType = " + tblAdolReg.getRegAdolMarriageType() + "";
                }
                InserttblAuditTrail("regAdolMarriageType", adolEditData.getRegAdolMarriageType() + "", tblAdolReg.getRegAdolMarriageType() + "", transID, "tblAdolReg");
            }
            if (!tblAdolReg.getRegAdolGoingtoSchool().equals(adolEditData.getRegAdolGoingtoSchool())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " regAdolGoingtoSchool = " + (char) 34 + tblAdolReg.getRegAdolGoingtoSchool() + (char) 34 + "";
                } else {
                    addQuery = addQuery + " ,regAdolGoingtoSchool = " + (char) 34 + tblAdolReg.getRegAdolGoingtoSchool() + (char) 34 + "";
                }
                InserttblAuditTrail("regAdolGoingtoSchool", adolEditData.getRegAdolGoingtoSchool(), tblAdolReg.getRegAdolGoingtoSchool(), transID, "tblAdolReg");
            }
            if (!(tblAdolReg.getRegAdolNoofChild() == adolEditData.getRegAdolNoofChild())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " regAdolNoofChild = " + tblAdolReg.getRegAdolNoofChild() + "";
                } else {
                    addQuery = addQuery + " ,regAdolNoofChild = " + tblAdolReg.getRegAdolNoofChild() + "";
                }
                InserttblAuditTrail("regAdolNoofChild", adolEditData.getRegAdolNoofChild() + "", tblAdolReg.getRegAdolNoofChild() + "", transID, "tblAdolReg");
            }

            if (!tblAdolReg.getRegAdolOtherHealthIssues().equals(adolEditData.getRegAdolOtherHealthIssues())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " regAdolOtherHealthIssues = " + (char) 34 + tblAdolReg.getRegAdolOtherHealthIssues() + (char) 34 + "";
                } else {
                    addQuery = addQuery + " ,regAdolOtherHealthIssues = " + (char) 34 + tblAdolReg.getRegAdolOtherHealthIssues() + (char) 34 + "";
                }
                InserttblAuditTrail("regAdolOtherHealthIssues", adolEditData.getRegAdolOtherHealthIssues(), tblAdolReg.getRegAdolOtherHealthIssues(), transID, "tblAdolReg");
            }

//            21May2021 Bindu add health , menstrual, life , phy
            if (!tblAdolReg.getRegAdolHealthIssues().equals(adolEditData.getRegAdolHealthIssues())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " regAdolHealthIssues = " + (char) 34 + tblAdolReg.getRegAdolHealthIssues() + (char) 34 + "";
                } else {
                    addQuery = addQuery + " ,regAdolHealthIssues = " + (char) 34 + tblAdolReg.getRegAdolHealthIssues() + (char) 34 + "";
                }
                InserttblAuditTrail("regAdolHealthIssues", adolEditData.getRegAdolHealthIssues(), tblAdolReg.getRegAdolHealthIssues(), transID, "tblAdolReg");
            }

            if (!tblAdolReg.getRegAdolLifeStyle().equals(adolEditData.getRegAdolLifeStyle())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " regAdolLifeStyle = " + (char) 34 + tblAdolReg.getRegAdolLifeStyle() + (char) 34 + "";
                } else {
                    addQuery = addQuery + " ,regAdolLifeStyle = " + (char) 34 + tblAdolReg.getRegAdolLifeStyle() + (char) 34 + "";
                }
                InserttblAuditTrail("regAdolLifeStyle", adolEditData.getRegAdolLifeStyle(), tblAdolReg.getRegAdolLifeStyle(), transID, "tblAdolReg");
            }

            if (!tblAdolReg.getRegAdolMenstrualPbm().equals(adolEditData.getRegAdolMenstrualPbm())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " regAdolMenstrualPbm = " + (char) 34 + tblAdolReg.getRegAdolMenstrualPbm() + (char) 34 + "";
                } else {
                    addQuery = addQuery + " ,regAdolMenstrualPbm = " + (char) 34 + tblAdolReg.getRegAdolMenstrualPbm() + (char) 34 + "";
                }
                InserttblAuditTrail("regAdolMenstrualPbm", adolEditData.getRegAdolMenstrualPbm(), tblAdolReg.getRegAdolMenstrualPbm(), transID, "tblAdolReg");
            }

            if (!tblAdolReg.getRegAdolPhysicalActivity().equals(adolEditData.getRegAdolPhysicalActivity())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " regAdolPhysicalActivity = " + (char) 34 + tblAdolReg.getRegAdolPhysicalActivity() + (char) 34 + "";
                } else {
                    addQuery = addQuery + " ,regAdolPhysicalActivity = " + (char) 34 + tblAdolReg.getRegAdolPhysicalActivity() + (char) 34 + "";
                }
                InserttblAuditTrail("regAdolPhysicalActivity", adolEditData.getRegAdolPhysicalActivity(), tblAdolReg.getRegAdolPhysicalActivity(), transID, "tblAdolReg");
            }

            if (!tblAdolReg.getRegAdolSuggestionGiven().equals(adolEditData.getRegAdolSuggestionGiven())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " regAdolSuggestionGiven = " + (char) 34 + tblAdolReg.getRegAdolSuggestionGiven() + (char) 34 + "";
                } else {
                    addQuery = addQuery + " ,regAdolSuggestionGiven = " + (char) 34 + tblAdolReg.getRegAdolSuggestionGiven() + (char) 34 + "";
                }
                InserttblAuditTrail("regAdolSuggestionGiven", adolEditData.getRegAdolSuggestionGiven(), tblAdolReg.getRegAdolSuggestionGiven(), transID, "tblAdolReg");
            }
            if (!tblAdolReg.getRegAdolComments().equals(adolEditData.getRegAdolComments())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " regAdolComments = " + (char) 34 + tblAdolReg.getRegAdolComments() + (char) 34 + "";
                } else {
                    addQuery = addQuery + " ,regAdolComments = " + (char) 34 + tblAdolReg.getRegAdolComments() + (char) 34 + "";
                }
                InserttblAuditTrail("regAdolComments", adolEditData.getRegAdolComments(), tblAdolReg.getRegAdolComments(), transID, "tblAdolReg");
            }
            if (!tblAdolReg.getRecordUpdatedDate().equals(adolEditData.getRecordUpdatedDate())) {
                if (addQuery.equals("")) {
                    addQuery = addQuery + " RecordUpdatedDate = " + (char) 34 + tblAdolReg.getRecordUpdatedDate() + (char) 34 + "";
                } else {
                    addQuery = addQuery + " ,RecordUpdatedDate = " + (char) 34 + tblAdolReg.getRecordUpdatedDate() + (char) 34 + "";
                }
                InserttblAuditTrail("RecordUpdatedDate", adolEditData.getRecordUpdatedDate(), tblAdolReg.getRecordUpdatedDate(), transID, "tblAdolReg");
            }
            if (addQuery.length() > 0) {
                updateSQL = "UPDATE tblAdolReg SET ";
                updateSQL = updateSQL + addQuery + " WHERE AdolID = '" + AdolID + "'";
            }

//          05Sep2021 Arpitha
            if (addRegDetailsQuery.length() > 0) {
                updateSQLReg = "UPDATE tblregisteredwomen SET ";
                updateSQLReg = updateSQLReg + addRegDetailsQuery + " WHERE WomanId = '" + womanId + "'";
            }

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return updateSQL;
    }

    private boolean validationFacility() {
        try {
            if (aq.id(R.id.spnfacilities).getSpinner().getSelectedItemPosition() == 0) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseselectfacility), Toast.LENGTH_LONG).show();
                aq.id(R.id.etpincode).getEditText().requestFocus();
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return false;
    }

    private boolean validationRelationship() {
        try {
            if (aq.id(R.id.spnRelationship).getSpinner().getSelectedItemPosition() == 0) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseselectmaritalstatus), Toast.LENGTH_LONG).show();
                aq.id(R.id.etpincode).getEditText().requestFocus();
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return false;
    }

    private boolean validationMobileOf() {
        try {
            if (aq.id(R.id.etAdsphone).getEditText().getText().toString().length() != 0 && aq.id(R.id.etAdsphone).getEditText().getText().toString().length() == 10) {
                if (aq.id(R.id.spnphn).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseentermobilenoof), Toast.LENGTH_LONG).show();
                    return false;
                } else {
                    return true;
                }
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return true;
    }

    private boolean validationMobile() {
        try {
            if (aq.id(R.id.etAdsphone).getEditText().getText().toString().length() > 0) {
                if (aq.id(R.id.etAdsphone).getEditText().getText().toString().length() == 10) {
                    aq.id(R.id.etAdsphone).getEditText().setError(null);
                    return true;
                } else {
                    aq.id(R.id.etAdsphone).getEditText().setError(getResources().getString(R.string.mobilenumbermustcontainendigits));
                    aq.id(R.id.etAdsphone).getEditText().requestFocus();
                    return false;
                }
            } else {
                aq.id(R.id.etAdsphone).getEditText().setError(null);
                return true;
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return false;
    }

    private boolean validationOtherIssues() {
        try {
            CheckBox cb_otherissue = findViewById(R.id.chk_healthissueOthers);
            if (cb_otherissue.isChecked()) {
                if (aq.id(R.id.ethealthissueothers).getEditText().getText().toString().length() == 0) {
                    aq.id(R.id.ethealthissueothers).getEditText().setError(getResources().getString(R.string.pleaseenterissues));
                    aq.id(R.id.ethealthissueothers).getEditText().requestFocus();
                    return false;
                } else {
                    aq.id(R.id.ethealthissueothers).getEditText().setError(null);
                    return true;
                }
            } else {
                aq.id(R.id.ethealthissueothers).getEditText().setError(null);
                return true;
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return false;
    }

    private boolean validationHeight() {
        try {
            RadioButton radioButton_cm = findViewById(R.id.rd_cm);
            RadioButton radioButton_feet = findViewById(R.id.rd_feet);
            RadioButton radioButton_dontknow = findViewById(R.id.rd_htdontknow);
            TableRow tableRowFeet = findViewById(R.id.trfeet);
            TableRow tableRowCM = findViewById(R.id.tr_heightcm);
            if (radioButton_cm.isChecked()) {
                aq.id(R.id.etAdsheightcm).getEditText().requestFocus();
                if (aq.id(R.id.etAdsheightcm).getEditText().getText().toString().length() == 0) {
                    aq.id(R.id.etAdsheightcm).getEditText().setError(getResources().getString(R.string.pleaseenterheight));
                    aq.id(R.id.etAdsheightcm).getEditText().requestFocus();
                    return false;
                } else if (aq.id(R.id.etAdsheightcm).getEditText().getText().toString().length() > 0) {
                    double cm = Double.parseDouble(aq.id(R.id.etAdsheightcm).getEditText().getText().toString());
                    showHeightWarning(cm);
                    return true;
                } else {
                    aq.id(R.id.etAdsheightcm).getEditText().setError(null);
                    aq.id(R.id.tvHeight).getTextView().setTextColor(Color.BLACK);
                    return true;
                }
            } else if (radioButton_feet.isChecked()) {
                tableRowCM.setVisibility(View.GONE);
                tableRowFeet.setVisibility(View.VISIBLE);
                showHeightWarning(0);
                return true;
            } else if (radioButton_dontknow.isChecked()) {
                tableRowCM.setVisibility(View.GONE);
                tableRowFeet.setVisibility(View.GONE);
                return true;
            }

        } catch (
                Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        Toast.makeText(getApplicationContext(), getResources().

                getString(R.string.pleaseselectheighttype), Toast.LENGTH_LONG).show();
        return false;
    }

    private void showHeightWarning(double cm) {
        try {
            if (aq.id(R.id.rd_feet).isChecked()) {
                double feet = Double.parseDouble(aq.id(R.id.spnAdsHeightNumeric).getSpinner().getSelectedItem().toString());
                double inches = aq.id(R.id.spnAdsHeightDecimal).getSpinner().getSelectedItemPosition();
                cm = 30.48 * feet;
                cm = cm + (inches * 2.54);
            }
            if (aq.id(R.id.etAdsage).getEditText().getText().toString().length() != 0)
                switch (Integer.parseInt(aq.id(R.id.etAdsage).getEditText().getText().toString())) {
                    case 10:
                        if (!(cm >= 139 && cm <= 150)) {
                            if (aq.id(R.id.rd_feet).isChecked()) {
                                aq.id(R.id.tvHeight).getTextView().setTextColor(Color.RED);
                            } else {
                                aq.id(R.id.etAdsheightcm).getEditText().setError(getResources().getString(R.string.heightis) + cm);
                            }
                        } else {
                            aq.id(R.id.tvHeight).getTextView().setTextColor(Color.BLACK);
                        }
                        break;
                    case 11:
                        if (aq.id(R.id.rd_male).isChecked()) {
                            if (!(cm >= 144 && cm <= 155)) {
                                if (aq.id(R.id.rd_feet).isChecked()) {
                                    aq.id(R.id.tvHeight).getTextView().setTextColor(Color.RED);
                                } else {
                                    aq.id(R.id.etAdsheightcm).getEditText().setError(getResources().getString(R.string.heightis) + cm);
                                }
                            } else {
                                aq.id(R.id.tvHeight).getTextView().setTextColor(Color.BLACK);
                            }
                        } else {
                            if (!(cm >= 144 && cm <= 156)) {
                                if (aq.id(R.id.rd_feet).isChecked()) {
                                    aq.id(R.id.tvHeight).getTextView().setTextColor(Color.RED);
                                } else {
                                    aq.id(R.id.etAdsheightcm).getEditText().setError(getResources().getString(R.string.heightis) + cm);
                                }
                            } else {
                                aq.id(R.id.tvHeight).getTextView().setTextColor(Color.BLACK);
                            }
                        }

                        break;
                    case 12:
                        if (aq.id(R.id.rd_male).isChecked()) {
                            if (!(cm >= 149 && cm <= 163)) {
                                if (aq.id(R.id.rd_feet).isChecked()) {
                                    aq.id(R.id.tvHeight).getTextView().setTextColor(Color.RED);
                                } else {
                                    aq.id(R.id.etAdsheightcm).getEditText().setError(getResources().getString(R.string.heightis) + cm);
                                }
                            } else {
                                aq.id(R.id.tvHeight).getTextView().setTextColor(Color.BLACK);
                            }
                        } else {
                            if (!(cm >= 151 && cm <= 163)) {
                                if (aq.id(R.id.rd_feet).isChecked()) {
                                    aq.id(R.id.tvHeight).getTextView().setTextColor(Color.RED);
                                } else {
                                    aq.id(R.id.etAdsheightcm).getEditText().setError(getResources().getString(R.string.heightis) + cm);
                                }
                            } else {
                                aq.id(R.id.tvHeight).getTextView().setTextColor(Color.BLACK);
                            }
                        }
                        break;
                    case 13:
                        if (!(cm >= 156 && cm <= 169)) {
                            if (aq.id(R.id.rd_feet).isChecked()) {
                                aq.id(R.id.tvHeight).getTextView().setTextColor(Color.RED);
                            } else {
                                aq.id(R.id.etAdsheightcm).getEditText().setError(getResources().getString(R.string.heightis) + cm);
                            }
                        } else {
                            aq.id(R.id.tvHeight).getTextView().setTextColor(Color.BLACK);
                        }
                        break;
                    case 14:
                        if (aq.id(R.id.rd_male).isChecked()) {
                            if (!(cm >= 166 && cm <= 176)) {
                                if (aq.id(R.id.rd_feet).isChecked()) {
                                    aq.id(R.id.tvHeight).getTextView().setTextColor(Color.RED);
                                } else {
                                    aq.id(R.id.etAdsheightcm).getEditText().setError(getResources().getString(R.string.heightis) + cm);
                                }
                            } else {
                                aq.id(R.id.tvHeight).getTextView().setTextColor(Color.BLACK);
                            }
                        } else {
                            if (!(cm >= 161 && cm <= 173)) {
                                if (aq.id(R.id.rd_feet).isChecked()) {
                                    aq.id(R.id.tvHeight).getTextView().setTextColor(Color.RED);
                                } else {
                                    aq.id(R.id.etAdsheightcm).getEditText().setError(getResources().getString(R.string.heightis) + cm);
                                }
                            } else {
                                aq.id(R.id.tvHeight).getTextView().setTextColor(Color.BLACK);
                            }
                        }
                        break;
                    case 15:
                        if (aq.id(R.id.rd_male).isChecked()) {
                            if (!(cm >= 170 && cm <= 183)) {
                                if (aq.id(R.id.rd_feet).isChecked()) {
                                    aq.id(R.id.tvHeight).getTextView().setTextColor(Color.RED);
                                } else {
                                    aq.id(R.id.etAdsheightcm).getEditText().setError(getResources().getString(R.string.heightis) + cm);
                                }
                            } else {
                                aq.id(R.id.tvHeight).getTextView().setTextColor(Color.BLACK);
                            }
                        } else {
                            if (!(cm >= 162 && cm <= 173)) {
                                if (aq.id(R.id.rd_feet).isChecked()) {
                                    aq.id(R.id.tvHeight).getTextView().setTextColor(Color.RED);
                                } else {
                                    aq.id(R.id.etAdsheightcm).getEditText().setError(getResources().getString(R.string.heightis) + cm);
                                }
                            } else {
                                aq.id(R.id.tvHeight).getTextView().setTextColor(Color.BLACK);
                            }
                        }

                        break;
                    case 16:
                        if (aq.id(R.id.rd_male).isChecked()) {
                            if (!(cm >= 174 && cm <= 186)) {
                                if (aq.id(R.id.rd_feet).isChecked()) {
                                    aq.id(R.id.tvHeight).getTextView().setTextColor(Color.RED);
                                } else {
                                    aq.id(R.id.etAdsheightcm).getEditText().setError(getResources().getString(R.string.heightis) + cm);
                                }
                            } else {
                                aq.id(R.id.tvHeight).getTextView().setTextColor(Color.BLACK);
                            }
                        } else {
                            if (!(cm >= 163 && cm <= 173)) {
                                if (aq.id(R.id.rd_feet).isChecked()) {
                                    aq.id(R.id.tvHeight).getTextView().setTextColor(Color.RED);
                                } else {
                                    aq.id(R.id.etAdsheightcm).getEditText().setError(getResources().getString(R.string.heightis) + cm);
                                }
                            } else {
                                aq.id(R.id.tvHeight).getTextView().setTextColor(Color.BLACK);
                            }
                        }

                        break;
                    case 17:
                        if (aq.id(R.id.rd_male).isChecked()) {
                            if (!(cm >= 175 && cm <= 188)) {
                                if (aq.id(R.id.rd_feet).isChecked()) {
                                    aq.id(R.id.tvHeight).getTextView().setTextColor(Color.RED);
                                } else {
                                    aq.id(R.id.etAdsheightcm).getEditText().setError(getResources().getString(R.string.heightis) + cm);
                                }
                            } else {
                                aq.id(R.id.tvHeight).getTextView().setTextColor(Color.BLACK);
                            }
                        } else {
                            if (!(cm >= 163 && cm <= 174)) {
                                if (aq.id(R.id.rd_feet).isChecked()) {
                                    aq.id(R.id.tvHeight).getTextView().setTextColor(Color.RED);
                                } else {
                                    aq.id(R.id.etAdsheightcm).getEditText().setError(getResources().getString(R.string.heightis) + cm);
                                }
                            } else {
                                aq.id(R.id.tvHeight).getTextView().setTextColor(Color.BLACK);
                            }
                        }
                        break;
                    case 18:
                        if (aq.id(R.id.rd_male).isChecked()) {
                            if (!(cm >= 176 && cm <= 189)) {
                                if (aq.id(R.id.rd_feet).isChecked()) {
                                    aq.id(R.id.tvHeight).getTextView().setTextColor(Color.RED);
                                } else {
                                    aq.id(R.id.etAdsheightcm).getEditText().setError(getResources().getString(R.string.heightis) + cm);
                                }
                            } else {
                                aq.id(R.id.tvHeight).getTextView().setTextColor(Color.BLACK);
                            }
                        } else {
                            if (!(cm >= 163 && cm <= 174)) {
                                if (aq.id(R.id.rd_feet).isChecked()) {
                                    aq.id(R.id.tvHeight).getTextView().setTextColor(Color.RED);
                                } else {
                                    aq.id(R.id.etAdsheightcm).getEditText().setError(getResources().getString(R.string.heightis) + cm);
                                }
                            } else {
                                aq.id(R.id.tvHeight).getTextView().setTextColor(Color.BLACK);
                            }
                        }
                        break;
                    case 19:
                        if (aq.id(R.id.rd_male).isChecked()) {
                            if (!(cm >= 179 && cm <= 189)) {
                                if (aq.id(R.id.rd_feet).isChecked()) {
                                    aq.id(R.id.tvHeight).getTextView().setTextColor(Color.RED);
                                } else {
                                    aq.id(R.id.etAdsheightcm).getEditText().setError(getResources().getString(R.string.heightis) + cm);
                                }
                            } else {
                                aq.id(R.id.tvHeight).getTextView().setTextColor(Color.BLACK);
                            }
                        } else {
                            if (!(cm >= 163 && cm <= 174)) {
                                if (aq.id(R.id.rd_feet).isChecked()) {
                                    aq.id(R.id.tvHeight).getTextView().setTextColor(Color.RED);
                                } else {
                                    aq.id(R.id.etAdsheightcm).getEditText().setError(getResources().getString(R.string.heightis) + cm);
                                }
                            } else {
                                aq.id(R.id.tvHeight).getTextView().setTextColor(Color.BLACK);
                            }
                        }
                        break;
                    default:
                        if (aq.id(R.id.rd_feet).isChecked()) {
                            aq.id(R.id.tvHeight).getTextView().setTextColor(Color.RED);
                        } else {
                            aq.id(R.id.etAdsheightcm).getEditText().setError(getResources().getString(R.string.heightis) + cm);
                        }
                }
            else
                aq.id(R.id.etAdsage).getEditText().setError(getResources().getString(R.string.pleaseenterage));
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private boolean validationWeight() {
        try {
            if (aq.id(R.id.etAdsWeight).getEditText().getText().toString().length() == 0) {
                aq.id(R.id.etAdsWeight).getEditText().setError(getResources().getString(R.string.pleaseenterweight));
                aq.id(R.id.etAdsWeight).getEditText().requestFocus();
                return false;
            } else if (aq.id(R.id.etAdsWeight).getEditText().getText().toString().length() > 0) {
                double weight = Double.parseDouble(aq.id(R.id.etAdsWeight).getEditText().getText().toString());
                showWeightWarning(weight);
                return true;
            } else {
                aq.id(R.id.etAdsWeight).getEditText().setError(null);
                return true;
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return false;
    }

    private void showWeightWarning(double weight) {
        try {
            if (aq.id(R.id.etAdsage).getEditText().getText().toString().length() != 0)
                switch (Integer.parseInt(aq.id(R.id.etAdsage).getEditText().getText().toString())) {
                    case 10:
                        if (aq.id(R.id.rd_male).isChecked()) {
                            if (!(weight >= 33 && weight <= 46)) {
                                aq.id(R.id.etAdsWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                            }
                        } else {
                            if (!(weight >= 33 && weight <= 47)) {
                                aq.id(R.id.etAdsWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                            }
                        }
                        break;
                    case 11:
                        if (aq.id(R.id.rd_male).isChecked()) {
                            if (!(weight >= 36 && weight <= 53)) {
                                aq.id(R.id.etAdsWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                            }
                        } else {
                            if (!(weight >= 37 && weight <= 55)) {
                                aq.id(R.id.etAdsWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                            }
                        }
                        break;
                    case 12:
                        if (aq.id(R.id.rd_male).isChecked()) {
                            if (!(weight >= 41 && weight <= 58)) {
                                aq.id(R.id.etAdsWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                            }
                        } else {
                            if (!(weight >= 42 && weight <= 61)) {
                                aq.id(R.id.etAdsWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                            }
                        }
                        break;
                    case 13:
                        if (aq.id(R.id.rd_male).isChecked()) {
                            if (!(weight >= 46 && weight <= 66)) {
                                aq.id(R.id.etAdsWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                            }
                        } else {
                            if (!(weight >= 46 && weight <= 66)) {
                                aq.id(R.id.etAdsWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                            }
                        }
                        break;
                    case 14:
                        if (aq.id(R.id.rd_male).isChecked()) {
                            if (!(weight >= 51 && weight <= 72)) {
                                aq.id(R.id.etAdsWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                            }
                        } else {
                            if (!(weight >= 49 && weight <= 72)) {
                                aq.id(R.id.etAdsWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                            }
                        }

                        break;
                    case 15:
                        if (aq.id(R.id.rd_male).isChecked()) {
                            if (!(weight >= 56 && weight <= 79)) {
                                aq.id(R.id.etAdsWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                            }
                        } else {
                            if (!(weight >= 53 && weight <= 76)) {
                                aq.id(R.id.etAdsWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                            }
                        }

                        break;
                    case 16:
                        if (aq.id(R.id.rd_male).isChecked()) {
                            if (!(weight >= 61 && weight <= 84)) {
                                aq.id(R.id.etAdsWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                            }
                        } else {
                            if (!(weight >= 54 && weight <= 77)) {
                                aq.id(R.id.etAdsWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                            }
                        }
                        break;
                    case 17:
                        if (aq.id(R.id.rd_male).isChecked()) {
                            if (!(weight >= 64 && weight <= 88)) {
                                aq.id(R.id.etAdsWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                            }
                        } else {
                            if (!(weight >= 55 && weight <= 79)) {
                                aq.id(R.id.etAdsWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                            }
                        }
                        break;
                    case 18:
                        if (aq.id(R.id.rd_male).isChecked()) {
                            if (!(weight >= 67 && weight <= 92)) {
                                aq.id(R.id.etAdsWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                            }
                        } else {
                            if (!(weight >= 56 && weight <= 81)) {
                                aq.id(R.id.etAdsWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                            }
                        }
                        break;
                    case 19:
                        if (aq.id(R.id.rd_male).isChecked()) {
                            if (!(weight >= 69 && weight <= 93)) {
                                aq.id(R.id.etAdsWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                            }
                        } else {
                            if (!(weight >= 57 && weight <= 83)) {
                                aq.id(R.id.etAdsWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                            }
                        }

                        break;
                    default:
                        aq.id(R.id.etAdsWeight).getEditText().setError(getResources().getString(R.string.weightis) + weight);
                }
            else
                aq.id(R.id.etAdsage).getEditText().setError(getResources().getString(R.string.pleaseenterage));
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private boolean validationAge() {
        try {
            if (aq.id(R.id.etAdsage).getEditText().getText().toString().length() == 0) {
                aq.id(R.id.etAdsage).getEditText().setError(getResources().getString(R.string.pleaseenterage));
                aq.id(R.id.etAdsage).getEditText().requestFocus();
                return false;
            } else {
                int age = Integer.parseInt(aq.id(R.id.etAdsage).getEditText().getText().toString());
                if (age == 0) {
                    aq.id(R.id.etAdsage).getEditText().setError(getResources().getString(R.string.agecannotbezero));
                    aq.id(R.id.etAdsage).getEditText().requestFocus();
                    return false;
                } else if (age < 10 || age > 19) {
                    aq.id(R.id.etAdsage).getEditText().setError(getResources().getString(R.string.ageshouldbe));
                    aq.id(R.id.etAdsage).getEditText().requestFocus();
                    return false;
                } else {
                    aq.id(R.id.etAdsage).getEditText().setError(null);
                    RadioButton rbMale = findViewById(R.id.rd_male);
                    if (!rbMale.isChecked()) {
                        if (aq.id(R.id.etAdsWeight).getEditText().getText().toString().length() != 0) {
                            showWeightWarning(Double.parseDouble(aq.id(R.id.etAdsWeight).getEditText().getText().toString()));
                        }
                        if (aq.id(R.id.etAdsheightcm).getEditText().getText().toString().length() != 0) {
                            showHeightWarning(Double.parseDouble(aq.id(R.id.etAdsheightcm).getEditText().getText().toString()));
                        }
                    }
                    return true;
                }
            }


        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return false;
    }

    private boolean validationName() {
        try {
            if (aq.id(R.id.etAdsname).getEditText().getText().toString().length() == 0) {
                aq.id(R.id.etAdsname).getEditText().setError(getResources().getString(R.string.pleaseentername));
                aq.id(R.id.etAdsname).getEditText().requestFocus();
                return false;
            } else {
                aq.id(R.id.etAdsname).getEditText().setError(null);
                return true;
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return false;
    }

    private void Initalize() {
        try {
            aq = new AQuery(AdolRegistration.this);
            radiogroupHeight = findViewById(R.id.rdheight);
            btToolbarHome = findViewById(R.id.toolbarBtnHome);
            databaseHelper = getHelper();
            dbMethods = new DBMethods(databaseHelper);
            SchemaCreator.createDirectories(new AppState());
            //gender
            rg_gender = findViewById(R.id.rd_gender);
            rg_isPeriod = findViewById(R.id.rg_AR_Isperiod);
            rg_topTitle = findViewById(R.id.rg_top);
            aq.id(R.id.rb_top_basic).checked(true);
            aq.id(R.id.txtAdolRegTitle).getTextView().setText(getResources().getString(R.string.basic_details));
            //pregnant
            tr_pregnant = findViewById(R.id.trPregnant);
            tr_pregnant.setVisibility(View.GONE);

            llMaternal = findViewById(R.id.llMaternal);

            aq.id(R.id.spnAdsHeightNumeric).getSpinner().setSelection(4);// to set default height as 5 feet

            drawer = findViewById(R.id.drawer_adolReg);
            scrollview = findViewById(R.id.srlBasicInfo);
            getSupportActionBar().hide();

//            13May2021 Bindu set
            rg_pregnant = findViewById(R.id.rgpregnant);
            cb_Backpain = findViewById(R.id.chk_backpain);
            cb_StomachPain = findViewById(R.id.chk_stomachache);
            cb_Fatigue = findViewById(R.id.chk_fatigue);
            cb_Vomiting = findViewById(R.id.chk_vomiting);
            cb_Diarrhea = findViewById(R.id.chk_diarrhea);
            cb_Iritability = findViewById(R.id.chk_irritability);
            cb_Bloodloss = findViewById(R.id.chk_bloodloss);
            cb_Moodiness = findViewById(R.id.chk_moodiness);
            cb_CycleIssue = findViewById(R.id.chk_cycleissue);
            cb_Cramping = findViewById(R.id.chk_cramping);
            cb_overbleeding = findViewById(R.id.chk_overbleeding);
            cb_headache = findViewById(R.id.chk_headache);
            cb_earlypregnancy = findViewById(R.id.chk_earlypregnancy);

            rbIsPeriodYes = findViewById(R.id.rb_AR_isPeriodYes);
            rbIsPeriodNo = findViewById(R.id.rb_AR_isPeriodNo);
            cbAnemia = findViewById(R.id.cb_anemia);
            cbUndernut = findViewById(R.id.cb_undernurtition);
            cbObesity = findViewById(R.id.cb_obesity);
            cbInjuries = findViewById(R.id.cb_Injuries);
            cbDeprandAnxi = findViewById(R.id.cb_DepressandAnxi);
            cbHivandAids = findViewById(R.id.chk_hivandaids);
            cbInfectionDis = findViewById(R.id.chk_infectiondis);
            cbOther = findViewById(R.id.chk_healthissueOthers);
            cbAlcohol = findViewById(R.id.chk_alcohol);
            cbDrugs = findViewById(R.id.chk_drugs);
            cbTobacco = findViewById(R.id.chk_tobacco);
            cbSmoking = findViewById(R.id.chk_smoking);
            cbWalking = findViewById(R.id.chk_walking);
            cbJogging = findViewById(R.id.chk_jogging);
            cbYoga = findViewById(R.id.chk_yoga);
            cbWorkout = findViewById(R.id.chk_workout);
            cbMeditation = findViewById(R.id.chk_meditation);

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    private void assignEditTextAndCallDatePicker(EditText editText) {
        currentEditText = editText;
        String currDate = (editText.getText().length() > 0) ? editText.getText().toString() : "";
        CommonDatePickerFragment.getCommonDate(this, currDate);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        try {
            if (view.isShown()) {
                String strseldate = null;
                strseldate = String.format("%02d", dayOfMonth) + "-" + String.format("%02d", month + 1) + "-"
                        + year;

                @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                Date seldate = format.parse(strseldate);
                Calendar myCal = Calendar.getInstance();
                myCal.set(Calendar.YEAR, 2002);
                myCal.set(Calendar.MONTH, 1);
                myCal.set(Calendar.DAY_OF_MONTH, 1);
                Date lowEndLimit = myCal.getTime();

                if (currentEditText == aq.id(R.id.etAdsdob).getEditText()) {
                    if (seldate != null && seldate.after(new Date())) {
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.date_cannot_af_currentdate)
                                , this);
                        aq.id(R.id.etAdsdob).text("");
                        aq.id(R.id.etAdsage).text("");
                        //                        15May2021 Bindu hide
                        aq.id(R.id.imgAdscleardob).getImageView().setVisibility(View.GONE);
                    } else if (seldate != null && seldate.before(lowEndLimit)) {
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.date_cannot_bf_19years)
                                , this);
                        aq.id(R.id.etAdsdob).text("");
                        aq.id(R.id.etAdsage).text("");
                    } else if (strseldate != null && strseldate.equals(DateTimeUtil.getTodaysDate())) {
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.date_time_cannot_be_curre_datetime)
                                , this);
                        aq.id(R.id.etAdsdob).text("");
                        aq.id(R.id.etAdsage).text("");
                        //                        15May2021 Bindu hide
                        aq.id(R.id.imgAdscleardob).getImageView().setVisibility(View.GONE);
                    } else {
                        aq.id(R.id.etAdsdob).text(strseldate);
                        calculateBornDate(strseldate);
//                        15May2021 Bindu show
                        aq.id(R.id.imgAdscleardob).getImageView().setVisibility(View.VISIBLE);
                    }
                    calculateAge();

                } else if (currentEditText == aq.id(R.id.etPregnantDOD1).getEditText()) {
                    if (seldate != null && seldate.after(new Date())) {
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.date_cannot_af_currentdate)
                                , this);
                        aq.id(R.id.etPregnantDOD1).text("");
                    } else {
                        aq.id(R.id.etPregnantDOD1).text(strseldate);
                        int age = Integer.parseInt(aq.id(R.id.etAdsage).getEditText().getText().toString());
                        int childAge = getYearsFromDob(strseldate);
                        if (childAge < age) {
                            aq.id(R.id.etPregnantAgeatCurrentDate1).text(getYearsFromDob(strseldate) + "");
                        } else {
                            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.doddatecannotbemorethandob), AdolRegistration.this);
                            if (aq.id(R.id.etPregnantDOD1).getEditText().isEnabled()) {
                                aq.id(R.id.etPregnantDOD1).text("");
                                aq.id(R.id.etPregnantAgeatCurrentDate1).text("");
                            }
                        }
                    }
                } else if (currentEditText == aq.id(R.id.etPregnantDOD2).getEditText()) {
                    if (seldate != null && seldate.after(new Date())) {
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.date_cannot_af_currentdate)
                                , this);
                        aq.id(R.id.etPregnantDOD2).text("");
                    } else {
                        aq.id(R.id.etPregnantDOD2).text(strseldate);
                        int age = Integer.parseInt(aq.id(R.id.etAdsage).getEditText().getText().toString());
                        int childAge = getYearsFromDob(strseldate);
                        if (childAge < age) {
                            aq.id(R.id.etPregnantAgeatCurrentDate2).text(getYearsFromDob(strseldate) + "");
                        } else {
                            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.doddatecannotbemorethandob), AdolRegistration.this);
                            if (aq.id(R.id.etPregnantDOD2).getEditText().isEnabled()) {
                                aq.id(R.id.etPregnantDOD2).text("");
                                aq.id(R.id.etPregnantAgeatCurrentDate2).text("");
                            }
                        }
                    }
                } else if (currentEditText == aq.id(R.id.etPregnantDOD3).getEditText()) {
                    if (seldate != null && seldate.after(new Date())) {
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.date_cannot_af_currentdate)
                                , this);
                        aq.id(R.id.etPregnantDOD3).text("");
                    } else {
                        aq.id(R.id.etPregnantDOD3).text(strseldate);
                        int age = Integer.parseInt(aq.id(R.id.etAdsage).getEditText().getText().toString());
                        int childAge = getYearsFromDob(strseldate);
                        if (childAge < age) {
                            aq.id(R.id.etPregnantAgeatCurrentDate3).text(getYearsFromDob(strseldate) + "");
                        } else {
                            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.doddatecannotbemorethandob), AdolRegistration.this);
                            if (aq.id(R.id.etPregnantDOD3).getEditText().isEnabled()) {
                                aq.id(R.id.etPregnantDOD3).text("");
                                aq.id(R.id.etPregnantAgeatCurrentDate3).text("");
                            }
                        }
                    }
                } else if (currentEditText == aq.id(R.id.etPregnantDOD4).getEditText()) {
                    if (seldate != null && seldate.after(new Date())) {
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.date_cannot_af_currentdate)
                                , this);
                        aq.id(R.id.etPregnantDOD4).text("");
                    } else {
                        aq.id(R.id.etPregnantDOD4).text(strseldate);
                        int age = Integer.parseInt(aq.id(R.id.etAdsage).getEditText().getText().toString());
                        int childAge = getYearsFromDob(strseldate);
                        if (childAge < age) {
                            aq.id(R.id.etPregnantAgeatCurrentDate4).text(getYearsFromDob(strseldate) + "");
                        } else {
                            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.doddatecannotbemorethandob), AdolRegistration.this);
                            if (aq.id(R.id.etPregnantDOD4).getEditText().isEnabled()) {
                                aq.id(R.id.etPregnantDOD4).text("");
                                aq.id(R.id.etPregnantAgeatCurrentDate4).text("");
                            }

                        }
                    }
                }else//09Sep2021 Arpitha
                {
                    String str = String.format("%02d", dayOfMonth) + "-" + String.format("%02d", month + 1) + "-"
                            + year;

                    int days = DateTimeUtil.getDaysBetweenDates(DateTimeUtil.getTodaysDate(), str);

                    if (!aqPMSel.id(R.id.radEDD).isChecked()) {
                    if (days >= 0) {
                        if (isRegDate) {
                            if (days < 30) {
                                aqPMSel.id(R.id.etregdate).text(str);
                                aqPMSel.id(R.id.imgclearregadate).visible();//20AUg2019 Arpitha
                            } else if (days > 120) {
                                aqPMSel.id(R.id.etregdate).text("");
                                aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                                Toast.makeText(activity, getResources().getString(R.string.date_val_2_toast),
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha

                            } else {
                                aqPMSel.id(R.id.etregdate).text(str);
                                aqPMSel.id(R.id.imgclearregadate).visible();//20AUg2019 Arpitha
                            }

                        } else if (aqPMSel.id(R.id.radLMP).isChecked()) {
                            String lmporadd = str;
                            int daysdiff = 0;
                            String regdate = aqPMSel.id(R.id.etregdate).getText().toString();

                            if (regdate != null && regdate.trim().length() > 0) {
                                daysdiff = DateTimeUtil.getDaysBetweenDates(regdate, lmporadd);

                                int daysdifffromCUrr = DateTimeUtil
                                        .getDaysBetweenDates(DateTimeUtil.getTodaysDate(), lmporadd);

                                if (daysdiff <= 30) {
                                    Toast.makeText(activity,
                                            getResources().getString(R.string.date_val_3_toast),
                                            Toast.LENGTH_LONG).show();
                                    aqPMSel.id(R.id.etlmpdate).text("");
                                    aqPMSel.id(R.id.etgest).text("");
                                    aqPMSel.id(R.id.etMotherAdd).text("");
                                    aqPMSel.id(R.id.imgclearlmp).gone();//20Aug2019 Arpitha
                                } else if (daysdifffromCUrr > 280) {
                                    Toast.makeText(activity,
                                            getResources().getString(R.string.date_val_4_toast),
                                            Toast.LENGTH_LONG).show();
                                    aqPMSel.id(R.id.etlmpdate).text("");
                                    aqPMSel.id(R.id.etgest).text("");
                                    aqPMSel.id(R.id.etMotherAdd).text("");
                                    aqPMSel.id(R.id.imgclearlmp).gone();//20Aug2019 Arpitha
                                } else {
                                    aqPMSel.id(R.id.etMotherAdd).text(str);
                                    calculateEddGestation("lmp");
                                    aqPMSel.id(R.id.imgclearlmp).visible();//20Aug2019 Arpitha
                                }
                            } else
                                Toast.makeText(activity, getResources().getString(R.string.plz_enter_reg_dt),
                                        Toast.LENGTH_LONG).show();
                        } else if (aqPMSel.id(R.id.rdbMother).isChecked()) {

                            int daysdiff = DateTimeUtil
                                    .getDaysBetweenDates(aqPMSel.id(R.id.etregdate).getText().toString(), str);
                            if (daysdiff > 365) {// Arpitha 28Jun2018 - 23Dec2019 Arpitha - allow max 1 year

                                Toast.makeText(activity, (getResources().getString(R.string.datecannotbeafteroneyearfromcurrentdate)), // 11July2018
                                        // Arpitha
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etMotherAdd).text("");
                            } else
                                aqPMSel.id(R.id.etMotherAdd).text(str);
                        } else if (aqPMSel.id(R.id.rdbchild).isChecked()) {

                            int daysdiff = DateTimeUtil
                                    .getDaysBetweenDates(aqPMSel.id(R.id.etregdate).getText().toString(), str);
                            if (daysdiff > 5844) {// Arpitha 28Jun2018

                                Toast.makeText(activity, (getResources().getString(R.string.date_val_7_toast)), // 11July2018
                                        // Arpitha
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etMotherAdd).text("");
                            } else
                                aqPMSel.id(R.id.etMotherAdd).text(str);
                        }
                    } else {
                        if (isRegDate) {
                            aqPMSel.id(R.id.etregdate).text("");
                            aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                            Toast.makeText(activity,
                                    getResources().getString(R.string.reg_date_cannot_greater_current_date),
                                    Toast.LENGTH_LONG).show();
                            aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha
                        } else {
                            aqPMSel.id(R.id.etMotherAdd).text("");
                            aqPMSel.id(R.id.imgclearlmp).gone();//20AUg2019 Arpitha
                            aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                            if (aqPMSel.id(R.id.rdbPreg).isChecked()) {
                                Toast.makeText(activity,
                                        getResources()
                                                .getString(R.string.lmp_date_time_cannot_be_after_curre_datetime), // 11July2018

                                        Toast.LENGTH_LONG).show();

                                aqPMSel.id(R.id.etlmpdate).text("");
                                aqPMSel.id(R.id.etgest).text("");
                            } else {
                                Toast.makeText(activity,
                                        getResources()
                                                .getString(R.string.del_date_cannot_be_after_current_date_time), // 11July2018
                                        Toast.LENGTH_LONG).show();
                            }

                        }
                    }
                } else {
                    if (!isRegDate) {
                        int days1 = 0;
                        if (aqPMSel.id(R.id.etregdate).getText().toString().trim().length() > 0)
                            days1 = DateTimeUtil.getDaysBetweenDates(str,
                                    aqPMSel.id(R.id.etregdate).getText().toString());

                        if (days1 > 0 && days1 < 273) {
                            aqPMSel.id(R.id.etMotherAdd).text(str);
                            calculateEddGestation("edd");
                            aqPMSel.id(R.id.imgclearlmp).visible();//20Aug2019 Arpitha
                        } else {
                            if (days1 > 280) {
                                aqPMSel.id(R.id.etMotherAdd).text("");
                                aqPMSel.id(R.id.imgclearlmp).gone();//21Aug2019 Arpitha
                                aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                                Toast.makeText(activity, getResources().getString(R.string.date_val_6_toast),

                                        Toast.LENGTH_LONG).show();

                                aqPMSel.id(R.id.etlmpdate).text("");
                                aqPMSel.id(R.id.etgest).text("");

                            } else {
                                aqPMSel.id(R.id.etMotherAdd).text("");
                                aqPMSel.id(R.id.imgclearlmp).gone();//21Aug2019 Arpitha
                                aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                                Toast.makeText(activity, getResources().getString(R.string.date_val_5_toast),
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etlmpdate).text("");
                                aqPMSel.id(R.id.etgest).text("");
                            }
                        }
                    } else {
                        days = DateTimeUtil.getDaysBetweenDates(DateTimeUtil.getTodaysDate(), str);
                        if (days >= 0) {

                            if (days > 90) {
                                aqPMSel.id(R.id.etregdate).text("");
                                aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                                Toast.makeText(activity, getResources().getString(R.string.date_val_2_toast),

                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha
                            } else {
                                aqPMSel.id(R.id.etregdate).text(str);
                            }

                        } else {
                            aqPMSel.id(R.id.etregdate).text("");
                            aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                            Toast.makeText(activity, getResources().getString(R.string.reg_date_cannot_greater_current_date),

                                    Toast.LENGTH_LONG).show();
                            aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha
                        }
                    }
                }
                }//09Sep2021 Arpitha
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private void calculateBornDate(String currentdob) {
        try {
            int age = 0;
            if (currentdob.equals("AGE")) {
                age = Integer.parseInt(aq.id(R.id.etAdsage).getEditText().getText().toString());
            } else {
                age = getYearsFromDob(aq.id(R.id.etAdsdob).getEditText().getText().toString());
            }
            if (aq.id(R.id.etPregnantDOD1).getEditText().getText().toString().length() != 0) {
                int bornage = getYearsFromDob(aq.id(R.id.etPregnantDOD1).getEditText().getText().toString());
                if (bornage < age) {
                    aq.id(R.id.etPregnantAgeatCurrentDate1).getEditText().setText(String.valueOf(bornage));
                } else {
                    if (aq.id(R.id.etPregnantDOD1).getEditText().isEnabled()) {
                        aq.id(R.id.etPregnantAgeatCurrentDate1).getEditText().setText("");
                        aq.id(R.id.etPregnantDOD1).getEditText().setText("");
                    }
                }
            }
            if (aq.id(R.id.etPregnantDOD2).getEditText().getText().toString().length() != 0) {
                int bornage = getYearsFromDob(aq.id(R.id.etPregnantDOD2).getEditText().getText().toString());
                if (bornage < age) {
                    aq.id(R.id.etPregnantAgeatCurrentDate2).getEditText().setText(String.valueOf(bornage));
                } else {
                    if (aq.id(R.id.etPregnantDOD2).getEditText().isEnabled()) {
                        aq.id(R.id.etPregnantAgeatCurrentDate2).getEditText().setText("");
                        aq.id(R.id.etPregnantDOD2).getEditText().setText("");
                    }
                }
            }
            if (aq.id(R.id.etPregnantDOD3).getEditText().getText().toString().length() != 0) {
                int bornage = getYearsFromDob(aq.id(R.id.etPregnantDOD3).getEditText().getText().toString());
                if (bornage < age) {
                    aq.id(R.id.etPregnantAgeatCurrentDate3).getEditText().setText(String.valueOf(bornage));
                } else {
                    if (aq.id(R.id.etPregnantDOD3).getEditText().isEnabled()) {
                        aq.id(R.id.etPregnantAgeatCurrentDate3).getEditText().setText("");
                        aq.id(R.id.etPregnantDOD3).getEditText().setText("");
                    }
                }
            }
            if (aq.id(R.id.etPregnantDOD4).getEditText().getText().toString().length() != 0) {
                int bornage = getYearsFromDob(aq.id(R.id.etPregnantDOD4).getEditText().getText().toString());
                if (bornage < age) {
                    aq.id(R.id.etPregnantAgeatCurrentDate4).getEditText().setText(String.valueOf(bornage));
                } else {
                    if (aq.id(R.id.etPregnantDOD4).getEditText().isEnabled()) {
                        aq.id(R.id.etPregnantAgeatCurrentDate4).getEditText().setText("");
                        aq.id(R.id.etPregnantDOD4).getEditText().setText("");
                    }
                }
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private void calculateAge() {
        try {
            if (aq.id(R.id.etAdsdob).getEditText().getText().toString().length() != 0) {
                String age = String.valueOf(getYearsFromDob(aq.id(R.id.etAdsdob).getEditText().getText().toString()));
                aq.id(R.id.etAdsage).getEditText().setText(age);
                aq.id(R.id.etAdsage).getEditText().setSelection(age.length());
                if (validationAge()) {
                    checkDobandandMatch();
                }
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private void checkDobandandMatch() {
        try {
            if (aq.id(R.id.etAdsage).getEditText().getText().toString().length() != 0) {
                int age;
                if (aq.id(R.id.etAdsdob).getText().toString().length() != 0) {
                    age = getYearsFromDob(aq.id(R.id.etAdsdob).getText().toString());
                    if (age != Integer.parseInt(aq.id(R.id.etAdsage).getEditText().getText().toString())) {
                        aq.id(R.id.txtdoberror).text(getResources().getString(R.string.dobandagenotmatching));
                        aq.id(R.id.txtdoberror).visible();
                        aq.id(R.id.imgAdscleardob).getImageView().setVisibility(View.VISIBLE);
                    } else {
                        aq.id(R.id.txtdoberror).gone();
                        aq.id(R.id.imgAdscleardob).getImageView().setVisibility(View.GONE);
                        aq.id(R.id.etAdsage).getEditText().setError(null);
                        aq.id(R.id.etAdsdob).getEditText().setError(null);
                    }
                }
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (MotionEvent.ACTION_UP == event.getAction()) {
            try {
                RadioButton preYes = findViewById(R.id.rb_pregnantYes);
                RadioButton preNo = findViewById(R.id.rb_pregnantNo);
                switch (v.getId()) {
                    case R.id.etAdsdob:
                        assignEditTextAndCallDatePicker(aq.id(R.id.etAdsdob).getEditText());
                        break;
                    case R.id.etPregnantDOD1:
                        assignEditTextAndCallDatePicker(aq.id(R.id.etPregnantDOD1).getEditText());
                        break;
                    case R.id.etPregnantDOD2:
                        assignEditTextAndCallDatePicker(aq.id(R.id.etPregnantDOD2).getEditText());
                        break;
                    case R.id.etPregnantDOD3:
                        assignEditTextAndCallDatePicker(aq.id(R.id.etPregnantDOD3).getEditText());
                        break;
                    case R.id.etPregnantDOD4:
                        assignEditTextAndCallDatePicker(aq.id(R.id.etPregnantDOD4).getEditText());
                        break;
                    case R.id.etAdsage:
                        break;
                    case R.id.imgAdscleardob:
                        aq.id(R.id.etAdsdob).getEditText().setText("");
                }
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());

            }
        }
        return false;
    }

    private void InserttblAuditTrail(String ColumnName, String Old_Value, String
            New_Value, int transId, String tblName) throws Exception {

        AuditPojo tblAudit = new AuditPojo();
        tblAudit.setWomanId(adolEditData.getAdolID());
        tblAudit.setUserId(user.getUserId());
        tblAudit.setTblName(tblName);
        tblAudit.setColumnName(ColumnName);
        tblAudit.setDateChanged(DateTimeUtil.getTodaysDate());
        tblAudit.setOld_Value(Old_Value);
        tblAudit.setNew_Value(New_Value);
        tblAudit.setTransId(transId);
        tblAudit.setRecordcreateddate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
        AuditRepository auditRepository = new AuditRepository(databaseHelper);
        auditRepository.insertTotblAuditTrail(tblAudit);
    }

    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        b.putParcelable("syncState", syncState);
        return b;
    }

    private void InitializeDrawer() {
        try {
            ListView drawerList = findViewById(R.id.lvNav_adolReg);

            ArrayList<NavDrawerItem> navDrawerItems = new ArrayList<>();
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.editadolescent), R.drawable.ic_edit_icon));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.homevisitadd), R.drawable.ic_homevisit));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.adolvisithistory), R.drawable.anm_pending_activities));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.trainingparticipated), R.drawable.presentation));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.mhmform), R.drawable.form_icon));
            String strAddPregnancy = "";
            if (new AdolescentRepository(databaseHelper).
                    isAdolANC(adolEditData.getRegAdolAadharNo(), adolEditData.getAdolID()))
                strAddPregnancy = getResources().getString(R.string.viewpregancny);
            else
                strAddPregnancy = getResources().getString(R.string.addpregancny);
            navDrawerItems.add(new NavDrawerItem(strAddPregnancy, R.drawable.anc));
            womanId = new AdolescentRepository(databaseHelper).getAdolANCWomanId(
                    adolEditData.getRegAdolAadharNo(), adolEditData.getAdolID());
            navDrawerItems.add(new NavDrawerItem(getResources()
                    .getString(R.string.anchomevisit), R.drawable.ic_homevisit));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.preghistory),R.drawable.anc));//12Sep2021 Arpitha
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deactivate), R.drawable.deactivate));
            drawer.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
            drawerList.setAdapter(new NavDrawerListAdapter(getApplicationContext(), navDrawerItems));
            drawerList.setOnItemClickListener(new DrawerItemClickListener());
            Toolbar toolbar = findViewById(R.id.toolbarCommon);
            ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close);
            drawer.setDrawerListener(drawerToggle);
            drawerToggle.syncState();
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        try {
            RadioButton preYes = findViewById(R.id.rb_pregnantYes);
            RadioButton preNo = findViewById(R.id.rb_pregnantNo);
            switch (view.getId()) {

                case R.id.llPP_FirstBornHead:
                case R.id.imgchl1expand:
                    setFirstBornUI();
                    break;
                case R.id.llPP_SecondBornHead:
                case R.id.imgchl2expand:
                    setSecondBornUI();
                    break;
                case R.id.llPP_ThirdBornHead:
                case R.id.imgchl3expand:
                    setThirdBornUI();
                    break;
                case R.id.llPP_FourthBornHead:
                case R.id.imgchl4expand:
                    setFourthBornUI();
                    break;
                case R.id.rb_pregnantYes:
                    preYes.setChecked(true);
                    preNo.setChecked(false);
                    break;
                case R.id.rb_pregnantNo:
                    preNo.setChecked(true);
                    preYes.setChecked(false);
                    break;
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }

    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private void setFirstBornUI() {
        try {
            if (aq.id(R.id.llPP_FirstBornBody).getView().getVisibility() == View.VISIBLE) {
                aq.id(R.id.llPP_FirstBornBody).getView().setVisibility(View.GONE);
                aq.id(R.id.imgchl1expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
            } else {
                aq.id(R.id.llPP_FirstBornBody).getView().setVisibility(View.VISIBLE);
                aq.id(R.id.imgchl1expand).getImageView().setBackground(getResources().getDrawable(R.drawable.expand));

                aq.id(R.id.llPP_SecondBornBody).gone();
                aq.id(R.id.imgchl2expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                aq.id(R.id.llPP_ThirdBornBody).gone();
                aq.id(R.id.imgchl3expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                aq.id(R.id.llPP_FourthBornBody).gone();
                aq.id(R.id.imgchl4expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private void setSecondBornUI() {
        try {
            if (aq.id(R.id.llPP_SecondBornBody).getView().getVisibility() == View.VISIBLE) {
                aq.id(R.id.llPP_SecondBornBody).getView().setVisibility(View.GONE);
                aq.id(R.id.imgchl2expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
            } else {
                aq.id(R.id.llPP_SecondBornBody).getView().setVisibility(View.VISIBLE);
                aq.id(R.id.imgchl2expand).getImageView().setBackground(getResources().getDrawable(R.drawable.expand));

                aq.id(R.id.llPP_FirstBornBody).gone();
                aq.id(R.id.imgchl1expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                aq.id(R.id.llPP_ThirdBornBody).gone();
                aq.id(R.id.imgchl3expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                aq.id(R.id.llPP_FourthBornBody).gone();
                aq.id(R.id.imgchl4expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private void setThirdBornUI() {
        try {
            if (aq.id(R.id.llPP_ThirdBornBody).getView().getVisibility() == View.VISIBLE) {
                aq.id(R.id.llPP_ThirdBornBody).getView().setVisibility(View.GONE);
                aq.id(R.id.imgchl3expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
            } else {
                aq.id(R.id.llPP_ThirdBornBody).getView().setVisibility(View.VISIBLE);
                aq.id(R.id.imgchl3expand).getImageView().setBackground(getResources().getDrawable(R.drawable.expand));

                aq.id(R.id.llPP_SecondBornBody).gone();
                aq.id(R.id.imgchl2expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                aq.id(R.id.llPP_FirstBornBody).gone();
                aq.id(R.id.imgchl1expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                aq.id(R.id.llPP_FourthBornBody).gone();
                aq.id(R.id.imgchl4expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private void setFourthBornUI() {
        try {
            if (aq.id(R.id.llPP_FourthBornBody).getView().getVisibility() == View.VISIBLE) {
                aq.id(R.id.llPP_FourthBornBody).getView().setVisibility(View.GONE);
                aq.id(R.id.imgchl4expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
            } else {
                aq.id(R.id.llPP_FourthBornBody).getView().setVisibility(View.VISIBLE);
                aq.id(R.id.imgchl4expand).getImageView().setBackground(getResources().getDrawable(R.drawable.expand));

                aq.id(R.id.llPP_SecondBornBody).gone();
                aq.id(R.id.imgchl2expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                aq.id(R.id.llPP_ThirdBornBody).gone();
                aq.id(R.id.imgchl3expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                aq.id(R.id.llPP_FirstBornBody).gone();
                aq.id(R.id.imgchl1expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }


    private class DrawerItemClickListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            try {
                switch (i) {
                    case 0:
                        drawer.closeDrawer(GravityCompat.END);
                        break;
                    case 1:
                        if (adolEditData.getRegAdolDeactivateDate().length() != 0) {
                            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.adolescentdeactivated), AdolRegistration.this);
                            drawer.closeDrawer(GravityCompat.END);
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(AdolRegistration.this);
                            builder.setMessage(getResources().getString(R.string.m110));
                            builder.setCancelable(false);
                            builder.setNegativeButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Intent intent = new Intent(AdolRegistration.this, AdolVisit_New.class);
                                    intent.putExtra("globalState", prepareBundle());
                                    intent.putExtra("adolID", AdolID);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);

                                }
                            }).setPositiveButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            }).create().show();
                        }
                        break;
                    case 2:
                        AlertDialog.Builder builder = new AlertDialog.Builder(AdolRegistration.this);
                        builder.setMessage(getResources().getString(R.string.m110));
                        builder.setCancelable(false);
                        builder.setNegativeButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent(AdolRegistration.this, AdolVisitHistory.class);
                                intent.putExtra("globalState", prepareBundle());
                                intent.putExtra("adolID", AdolID);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);

                            }
                        }).setPositiveButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).create().show();
                        break;
                    case 3:
                        builder = new AlertDialog.Builder(AdolRegistration.this);
                        builder.setMessage(getResources().getString(R.string.m110));
                        builder.setCancelable(false);
                        builder.setNegativeButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent(AdolRegistration.this, AdolParticipatedTrainings.class);
                                intent.putExtra("globalState", prepareBundle());
                                intent.putExtra("adolID", AdolID);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);

                            }
                        }).setPositiveButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).create().show();
                        break;
                    case 4:
                        if (adolEditData != null && adolEditData.getRegAdolGender().equals("Female") && adolEditData.getRegAdolisPeriod().equals("Yes")) {
                            Intent intent = new Intent(AdolRegistration.this, List_SanNap.class);
                            builder = new AlertDialog.Builder(AdolRegistration.this);
                            builder.setMessage(getResources().getString(R.string.m110));
                            builder.setCancelable(false);
                            builder.setNegativeButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    intent.putExtra("globalState", prepareBundle());
                                    intent.putExtra("adolID", AdolID);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);

                                }
                            }).setPositiveButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            }).create().show();
                        } else {
                            Toast.makeText(getApplicationContext(),getResources().getString(R.string.mhmfromnotavailforadol),Toast.LENGTH_LONG).show();
                        }
                        break;

                    case 5:

                        if ((adolEditData.getRegAdolPregnant() != null && adolEditData.getRegAdolPregnant()
                                .trim().length() > 0
                                && adolEditData.getRegAdolPregnant().equalsIgnoreCase("Yes")
                        ) ||
                        new AdolescentRepository(databaseHelper).
                                isAdolPreganant(adolEditData.getAdolID())) {
                            if (new AdolescentRepository(databaseHelper)
                                    .isAdolANC(adolEditData.getRegAdolAadharNo(), adolEditData.getAdolID())) {
                                appState.selectedWomanId = womanId;
                                Intent intent1 = new Intent(AdolRegistration.this, ViewProfileActivity.class);
                                intent1.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                startActivity(intent1);
                            } else
                                showPregnantOrMotherSelectionDialog();

                        } else
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.ntapplicable),
                                    Toast.LENGTH_LONG).show();
                        break;

                    case 6:
                        if (new AdolescentRepository(databaseHelper)
                                .isAdolANC(adolEditData.getRegAdolAadharNo(), adolEditData.getAdolID())) {
                            appState.selectedWomanId = womanId;
                            Intent hv = new Intent(AdolRegistration.this, HomeVisitListActivity.class);
                            hv.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(hv);
                        } else
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.ntapplicable),
                                    Toast.LENGTH_LONG).show();
                        break;

//                        12Sep2021 Arpitha
                    case 7:
                        if(new AdolescentRepository(databaseHelper)
                                .isAdolANCHistory(adolEditData.getRegAdolAadharNo(),adolEditData.getAdolID()))//10Sep2021 Arpitha
                        {
                            appState.selectedWomanId = womanId;
                            Intent intent = new Intent(view.getContext(), AdolANCHistory.class);
                            intent.putExtra("globalState", prepareBundle());
                            intent.putExtra("aadharno",
                                    adolEditData.getRegAdolAadharNo());
                            startActivity(intent);
                        }else
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.ntapplicable),
                                    Toast.LENGTH_LONG).show();

                            break;// 12Sep2021 Arpitha

                        case 8:
                        builder = new AlertDialog.Builder(AdolRegistration.this);
                        builder.setMessage(getResources().getString(R.string.m110));
                        builder.setCancelable(false);
                        builder.setNegativeButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent(AdolRegistration.this, AdolDeactivation.class);
                                intent.putExtra("globalState", prepareBundle());
                                if (adolEditData.getRegAdolDeactivateDate().length() != 0) {
                                    intent.putExtra("isView", true);
                                }
                                intent.putExtra("adolID", AdolID);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);

                            }
                        }).setPositiveButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).create().show();
                        break;

                }
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());

            }
        }
    }

    protected int getYearsFromDob(String string) throws ParseException {

        int years = 0;
//        String dobtxt = aq.id(R.id.etAdsdob).getText().toString();
        String dobtxt = string;
        Calendar curDt = new GregorianCalendar();
        curDt.setTime(new SimpleDateFormat("dd-MM-yyyy").parse(DateTimeUtil.getTodaysDate()));
        Calendar dob = new GregorianCalendar();
        if (dobtxt.length() > 0) {
            dob.setTime(new SimpleDateFormat("dd-MM-yyyy").parse(dobtxt));
            years = curDt.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
        }
        return years;
    }

    public static int getKey(Map<String, Integer> mapref, String value) {

        int pos = 0;
        for (Map.Entry<String, Integer> map : mapref.entrySet()) {
            pos++;
            if (map.getValue().toString().equals(value)) {
                return pos;
            }

        }
        return pos;
    }

    private void calculateHealthData() {
        healthIssuesListStore = new ArrayList<>();
        lifeStyleListStore = new ArrayList<>();
        menstrualPbmListStore = new ArrayList<>();
        physicalActListStore = new ArrayList<>();

        if (rbIsPeriodYes.isChecked()) {
            if (cb_Backpain.isChecked())
                menstrualPbmListStore.add(getResources().getString(R.string.backpainsave));
            if (cb_StomachPain.isChecked())
                menstrualPbmListStore.add(getResources().getString(R.string.stomachachesave));
            if (cb_Fatigue.isChecked())
                menstrualPbmListStore.add(getResources().getString(R.string.fatiguesave));
            if (cb_Vomiting.isChecked())
                menstrualPbmListStore.add(getResources().getString(R.string.vomitingsave));
            if (cb_Diarrhea.isChecked())
                menstrualPbmListStore.add(getResources().getString(R.string.diarrheasave));
            if (cb_Iritability.isChecked())
                menstrualPbmListStore.add(getResources().getString(R.string.irritabilitysave));
            if (cb_Bloodloss.isChecked())
                menstrualPbmListStore.add(getResources().getString(R.string.bloodlosssave));
            if (cb_Moodiness.isChecked())
                menstrualPbmListStore.add(getResources().getString(R.string.moodinesssave));
            if (cb_CycleIssue.isChecked())
                menstrualPbmListStore.add(getResources().getString(R.string.cycleissuessave));
            if (cb_Cramping.isChecked())
                menstrualPbmListStore.add(getResources().getString(R.string.crampingsave));
            if (cb_overbleeding.isChecked())
                menstrualPbmListStore.add(getResources().getString(R.string.overbleedingsave));
            if (cb_headache.isChecked())
                menstrualPbmListStore.add(getResources().getString(R.string.headachesave));
            if (cb_earlypregnancy.isChecked())
                menstrualPbmListStore.add(getResources().getString(R.string.earlypregancyandchildbirthsave));

            menstrualPbms = "";
            for (int i = 0; i < menstrualPbmListStore.size(); i++) {
                if (i == (menstrualPbmListStore.size() - 1)) {
                    menstrualPbms = menstrualPbms + menstrualPbmListStore.get(i);
                } else
                    menstrualPbms = menstrualPbms + menstrualPbmListStore.get(i) + ",";
            }
        }

        //health issues
        if (cbAnemia.isChecked())
            healthIssuesListStore.add(getResources().getString(R.string.anemiasave));
        if (cbUndernut.isChecked())
            healthIssuesListStore.add(getResources().getString(R.string.undernutritionsave));
        if (cbObesity.isChecked())
            healthIssuesListStore.add(getResources().getString(R.string.obesitysave));
        if (cbInjuries.isChecked())
            healthIssuesListStore.add(getResources().getString(R.string.injuriessave));
        if (cbDeprandAnxi.isChecked())
            healthIssuesListStore.add(getResources().getString(R.string.depressandanxisave));
        if (cbHivandAids.isChecked())
            healthIssuesListStore.add(getResources().getString(R.string.hivandaidssave));
        if (cbInfectionDis.isChecked())
            healthIssuesListStore.add(getResources().getString(R.string.infectiondiseasessave));
        if (cbOther.isChecked())
            healthIssuesListStore.add(getResources().getString(R.string.othersave));

        healthIssues = "";
        for (int i = 0; i < healthIssuesListStore.size(); i++) {
            if (i == (healthIssuesListStore.size() - 1)) {
                healthIssues = healthIssues + healthIssuesListStore.get(i);
            } else
                healthIssues = healthIssues + healthIssuesListStore.get(i) + ",";
        }

        //lifestyle
        if (cbAlcohol.isChecked())
            lifeStyleListStore.add(getResources().getString(R.string.alcoholsave));
        if (cbDrugs.isChecked())
            lifeStyleListStore.add(getResources().getString(R.string.drugssave));
        if (cbTobacco.isChecked())
            lifeStyleListStore.add(getResources().getString(R.string.tobaccosave));
        if (cbSmoking.isChecked())
            lifeStyleListStore.add(getResources().getString(R.string.smokingsave));

        lifestyle = "";
        for (int i = 0; i < lifeStyleListStore.size(); i++) {
            if (i == (lifeStyleListStore.size() - 1)) {
                lifestyle = lifestyle + lifeStyleListStore.get(i);
            } else
                lifestyle = lifestyle + lifeStyleListStore.get(i) + ",";
        }

        //physical activity
        if (cbWalking.isChecked())
            physicalActListStore.add(getResources().getString(R.string.walkingsave));
        if (cbJogging.isChecked())
            physicalActListStore.add(getResources().getString(R.string.joggingsave));
        if (cbYoga.isChecked())
            physicalActListStore.add(getResources().getString(R.string.yogasave));
        if (cbWorkout.isChecked())
            physicalActListStore.add(getResources().getString(R.string.workoutsave));
        if (cbMeditation.isChecked())
            physicalActListStore.add(getResources().getString(R.string.meditationsave));

        physicalActivity = "";
        for (int i = 0; i < physicalActListStore.size(); i++) {
            if (i == (physicalActListStore.size() - 1)) {
                physicalActivity = physicalActivity + physicalActListStore.get(i);
            } else
                physicalActivity = physicalActivity + physicalActListStore.get(i) + ",";
        }
    }

    //    01Sep2021 Arpitha

    /**
     * Alert Dialog to select Pregnant or Mother Registration
     */
    private void showPregnantOrMotherSelectionDialog() {
        try {
            final AlertDialog.Builder alert = new AlertDialog.Builder(activity,
                    AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
            final View customView = LayoutInflater.from(this).inflate(R.layout.pregnant_mother_selection,
                    null);

            aqPMSel = new AQuery(customView);
            radGrpEDDorLmp = customView.findViewById(R.id.radEDDorLmp);

            initializeScreen(aqPMSel.id(R.id.llPregOrMothMainLayout).getView());
            setInitialView();

            aqPMSel.id(R.id.llmother).gone();
            aqPMSel.id(R.id.llchild).gone();
            aqPMSel.id(R.id.llAdol).gone();


            aqPMSel.id(R.id.imgclearedddate).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    aqPMSel.id(R.id.etlmpdate).text("");
                }
            });
            aqPMSel.id(R.id.imgclearlmp).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    aqPMSel.id(R.id.etMotherAdd).text("");
                    aqPMSel.id(R.id.etlmpdate).text("");
                    aqPMSel.id(R.id.etgest).text("");
                    aqPMSel.id(R.id.imgclearlmp).gone();
                }
            });
            aqPMSel.id(R.id.imgclearregadate).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());
                    Toast.makeText(activity, getResources().getString(R.string.dateresettotodaysdate), Toast.LENGTH_LONG).show();//21Aug2019 Arpitha
                }
            });//   Arpitha - 20Aug2019

//            aqPMSel.id(R.id.etMotherAdd).text("20-05-2021");
            aqPMSel.id(R.id.eteddmother).enabled(false);

            aqPMSel.id(R.id.etMotherAdd).getEditText().setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    try {
                        if (MotionEvent.ACTION_UP == event.getAction()) {
                            isRegDate = false;
                            assignEditTextAndCallDatePicker(aqPMSel.id(R.id.etMotherAdd).getEditText());
                        }
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());

                    }
                    return true;
                }
            });

            aqPMSel.id(R.id.etregdate).getEditText().setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    try {
                        if (MotionEvent.ACTION_UP == event.getAction()) {
                            isRegDate = true;
                            assignEditTextAndCallDatePicker(aqPMSel.id(R.id.etregdate).getEditText());
                        }
                    } catch (Exception e) {

                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }
                    return true;
                }
            });

            aqPMSel.id(R.id.radLMP).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        setLMP();
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }
                }
            });

            aqPMSel.id(R.id.radEDD).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        setEDD();
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }
                }
            });

            alert.setView(customView);

            alert.setCancelable(true)
                    .setTitle(getResources().getString(R.string.registrationHeading))
                    .setPositiveButton(getResources().getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {

                                }
                            })
                    .setNegativeButton(getResources().getString(R.string.cancel), //01Oct2019 - Bindu - Add cancel button
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    dialog.cancel();
                                }
                            });
            final AlertDialog dialog = alert.create();
            if (dialog != null)
                dialog.show();

            // Overriding the handler immediately after show is probably a
            // better
            // approach than OnShowListener as described below
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        validateAndRegister();
                        if (wantToCloseDialog)
                            dialog.dismiss();
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }

                }
            });
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }


    //calculate gestation based on LMP
    private void calculateEddGestation(String lmp) {
        try {

            if (lmp.equalsIgnoreCase("lmp")) {
                if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0
                        && aqPMSel.id(R.id.etMotherAdd).getText().length() > 0) {

                    noOfDays = DateTimeUtil
                            .getNumberOfDaysFromLMP(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                    int noOfWeeks = noOfDays / 7;
                    int days = noOfDays % 7;

                    String strdays = "";

                    if (days > 1)
                        strdays = days + " " + activity.getResources().getString(R.string.days);
                    else if (days == 1)
                        strdays = days + " " + activity.getResources().getString(R.string.day);

                    aqPMSel.id(R.id.etgest)
                            .text(noOfWeeks + " " + activity.getResources().getString(R.string.weeks) + " " + strdays);
                }

                String edd = populateEDD(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                aqPMSel.id(R.id.etlmpdate).text(edd);

            } else {

                String strLmp = CalculateLMP(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                aqPMSel.id(R.id.etlmpdate).text(strLmp);

                if (aqPMSel.id(R.id.etlmpdate).getText().length() > 0
                        && aqPMSel.id(R.id.etlmpdate).getText().length() > 0) {

                    noOfDays = DateTimeUtil
                            .getNumberOfDaysFromLMP(aqPMSel.id(R.id.etlmpdate).getText().toString());
                    int noOfWeeks = noOfDays / 7;
                    int days = noOfDays % 7;

                    String strdays = "";

                    if (days > 1)
                        strdays = days + " " + activity.getResources().getString(R.string.days);
                    else if (days == 1)
                        strdays = days + " " + activity.getResources().getString(R.string.day);

                    aqPMSel.id(R.id.etgest)
                            .text(noOfWeeks + " " + activity.getResources().getString(R.string.weeks) + " " + strdays);
                }

            }
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }

    }

    // Calculate EDD and display in EDD Edit text
    private String populateEDD(String xLMP) throws Exception {
        String stredddate = "";
        try {

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());

            Date LmpDate;
            LmpDate = sdf.parse(xLMP);
            Calendar cal = Calendar.getInstance();
            cal.setTime(LmpDate);
            cal.add(Calendar.DAY_OF_MONTH, 280);

            int day = cal.get(Calendar.DAY_OF_MONTH);
            int month = cal.get(Calendar.MONTH) + 1;
            int year = cal.get(Calendar.YEAR);

            stredddate =
                    String.format("%02d", day) + "-" + String.format("%02d", month) + "-" + String.format("%02d", year);

        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        return stredddate;
    }

    //	calculate LMP based on EDD
    private String CalculateLMP(String EDDate) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        Date LMP;
        LMP = sdf.parse(EDDate);
        Calendar cal = Calendar.getInstance();
        cal.setTime(LMP);
        cal.add(Calendar.DAY_OF_MONTH, -280);

        int day = cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH) + 1;
        int year = cal.get(Calendar.YEAR);

        String lmpDate = String.format("%02d", day) + "-" + String.format("%02d", month) + "-" + year;
        return lmpDate;
    }

    /**
     * This method opens the Date Dialog and sets Date to respective EditText
     */
  /*  private void assignEditTextAndCallDatePicker(EditText editText) {
        String currDate = (editText.getText().length() > 0) ? editText.getText().toString() : "";
        CommonDatePickerFragment.getCommonDate(activity, currDate);
    }*/

  /*  @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        try {
            aqPMSel.id(R.id.imgclearlmp).background(R.drawable.brush1);
            if (view.isShown()) {
                String str = String.format("%02d", day) + "-" + String.format("%02d", month + 1) + "-"
                        + year;

                int days = DateTimeUtil.getDaysBetweenDates(DateTimeUtil.getTodaysDate(), str);

                if (!aqPMSel.id(R.id.radEDD).isChecked()) {
                    if (days >= 0) {
                        if (isRegDate) {
                            if (days < 30) {
                                aqPMSel.id(R.id.etregdate).text(str);
                                aqPMSel.id(R.id.imgclearregadate).visible();//20AUg2019 Arpitha
                            } else if (days > 120) {
                                aqPMSel.id(R.id.etregdate).text("");
                                aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                                Toast.makeText(activity, getResources().getString(R.string.date_val_2_toast),
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha

                            } else {
                                aqPMSel.id(R.id.etregdate).text(str);
                                aqPMSel.id(R.id.imgclearregadate).visible();//20AUg2019 Arpitha
                            }

                        } else if (aqPMSel.id(R.id.radLMP).isChecked()) {
                            String lmporadd = str;
                            int daysdiff = 0;
                            String regdate = aqPMSel.id(R.id.etregdate).getText().toString();

                            if (regdate != null && regdate.trim().length() > 0) {
                                daysdiff = DateTimeUtil.getDaysBetweenDates(regdate, lmporadd);

                                int daysdifffromCUrr = DateTimeUtil
                                        .getDaysBetweenDates(DateTimeUtil.getTodaysDate(), lmporadd);

                                if (daysdiff <= 30) {
                                    Toast.makeText(activity,
                                            getResources().getString(R.string.date_val_3_toast),
                                            Toast.LENGTH_LONG).show();
                                    aqPMSel.id(R.id.etlmpdate).text("");
                                    aqPMSel.id(R.id.etgest).text("");
                                    aqPMSel.id(R.id.etMotherAdd).text("");
                                    aqPMSel.id(R.id.imgclearlmp).gone();//20Aug2019 Arpitha
                                } else if (daysdifffromCUrr > 280) {
                                    Toast.makeText(activity,
                                            getResources().getString(R.string.date_val_4_toast),
                                            Toast.LENGTH_LONG).show();
                                    aqPMSel.id(R.id.etlmpdate).text("");
                                    aqPMSel.id(R.id.etgest).text("");
                                    aqPMSel.id(R.id.etMotherAdd).text("");
                                    aqPMSel.id(R.id.imgclearlmp).gone();//20Aug2019 Arpitha
                                } else {
                                    aqPMSel.id(R.id.etMotherAdd).text(str);
                                    calculateEddGestation("lmp");
                                    aqPMSel.id(R.id.imgclearlmp).visible();//20Aug2019 Arpitha
                                }
                            } else
                                Toast.makeText(activity, getResources().getString(R.string.plz_enter_reg_dt),
                                        Toast.LENGTH_LONG).show();
                        } else if (aqPMSel.id(R.id.rdbMother).isChecked()) {

                            int daysdiff = DateTimeUtil
                                    .getDaysBetweenDates(aqPMSel.id(R.id.etregdate).getText().toString(), str);
                            if (daysdiff > 365) {// Arpitha 28Jun2018 - 23Dec2019 Arpitha - allow max 1 year

                                Toast.makeText(activity, (getResources().getString(R.string.datecannotbeafteroneyearfromcurrentdate)), // 11July2018
                                        // Arpitha
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etMotherAdd).text("");
                            } else
                                aqPMSel.id(R.id.etMotherAdd).text(str);
                        } else if (aqPMSel.id(R.id.rdbchild).isChecked()) {

                            int daysdiff = DateTimeUtil
                                    .getDaysBetweenDates(aqPMSel.id(R.id.etregdate).getText().toString(), str);
                            if (daysdiff > 5844) {// Arpitha 28Jun2018

                                Toast.makeText(activity, (getResources().getString(R.string.date_val_7_toast)), // 11July2018
                                        // Arpitha
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etMotherAdd).text("");
                            } else
                                aqPMSel.id(R.id.etMotherAdd).text(str);
                        }
                    } else {
                        if (isRegDate) {
                            aqPMSel.id(R.id.etregdate).text("");
                            aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                            Toast.makeText(activity,
                                    getResources().getString(R.string.reg_date_cannot_greater_current_date),
                                    Toast.LENGTH_LONG).show();
                            aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha
                        } else {
                            aqPMSel.id(R.id.etMotherAdd).text("");
                            aqPMSel.id(R.id.imgclearlmp).gone();//20AUg2019 Arpitha
                            aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                            if (aqPMSel.id(R.id.rdbPreg).isChecked()) {
                                Toast.makeText(activity,
                                        getResources()
                                                .getString(R.string.lmp_date_time_cannot_be_after_curre_datetime), // 11July2018

                                        Toast.LENGTH_LONG).show();

                                aqPMSel.id(R.id.etlmpdate).text("");
                                aqPMSel.id(R.id.etgest).text("");
                            } else {
                                Toast.makeText(activity,
                                        getResources()
                                                .getString(R.string.del_date_cannot_be_after_current_date_time), // 11July2018
                                        Toast.LENGTH_LONG).show();
                            }

                        }
                    }
                } else {
                    if (!isRegDate) {
                        int days1 = 0;
                        if (aqPMSel.id(R.id.etregdate).getText().toString().trim().length() > 0)
                            days1 = DateTimeUtil.getDaysBetweenDates(str,
                                    aqPMSel.id(R.id.etregdate).getText().toString());

                        if (days1 > 0 && days1 < 273) {
                            aqPMSel.id(R.id.etMotherAdd).text(str);
                            calculateEddGestation("edd");
                            aqPMSel.id(R.id.imgclearlmp).visible();//20Aug2019 Arpitha
                        } else {
                            if (days1 > 280) {
                                aqPMSel.id(R.id.etMotherAdd).text("");
                                aqPMSel.id(R.id.imgclearlmp).gone();//21Aug2019 Arpitha
                                aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                                Toast.makeText(activity, getResources().getString(R.string.date_val_6_toast),

                                        Toast.LENGTH_LONG).show();

                                aqPMSel.id(R.id.etlmpdate).text("");
                                aqPMSel.id(R.id.etgest).text("");

                            } else {
                                aqPMSel.id(R.id.etMotherAdd).text("");
                                aqPMSel.id(R.id.imgclearlmp).gone();//21Aug2019 Arpitha
                                aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                                Toast.makeText(activity, getResources().getString(R.string.date_val_5_toast),
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etlmpdate).text("");
                                aqPMSel.id(R.id.etgest).text("");
                            }
                        }
                    } else {
                        days = DateTimeUtil.getDaysBetweenDates(DateTimeUtil.getTodaysDate(), str);
                        if (days >= 0) {

                            if (days > 90) {
                                aqPMSel.id(R.id.etregdate).text("");
                                aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                                Toast.makeText(activity, getResources().getString(R.string.date_val_2_toast),

                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha
                            } else {
                                aqPMSel.id(R.id.etregdate).text(str);
                            }

                        } else {
                            aqPMSel.id(R.id.etregdate).text("");
                            aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                            Toast.makeText(activity, getResources().getString(R.string.reg_date_cannot_greater_current_date),

                                    Toast.LENGTH_LONG).show();
                            aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha
                        }
                    }
                }

            }

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }*/

    /**
     * This is a Recursive method that traverse through the group and subgroup
     * of view Sets text, assigns clickListners
     *
     * @throws Exception
     */
    private ArrayList<View> initializeScreen(View v) throws Exception {
        if (!(v instanceof ViewGroup)) {
            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);

            // TextView - Assign Labels
            if (v.getClass() == TextView.class || v.getClass() == RadioButton.class) {
                // aqPMSel.id(v.getId()).text(getSS(aqPMSel.id(v.getId()).getText().toString()));
            }

            if (v.getClass() == RadioButton.class) {
                aqPMSel.id(v.getId()).clicked(this, "commonClick");
            }

            return viewArrayList;
        }

        ArrayList<View> result = new ArrayList<View>();

        ViewGroup viewGroup = (ViewGroup) v;
        for (int i = 0; i < viewGroup.getChildCount(); i++) {

            View child = viewGroup.getChildAt(i);

            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);
            viewArrayList.addAll(initializeScreen(child));

            result.addAll(viewArrayList);
        }
        return result;
    }


    //	set initial view of the pregnant or mother pop up
    private void setInitialView() {
        aqPMSel.id(R.id.rdbPreg).checked(true);
        aqPMSel.id(R.id.tvMotherAddHeading).text(getResources().getString(R.string.tvPregLmpHeading));
        aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());
        aqPMSel.id(R.id.radLMP).checked(true);
        aqPMSel.id(R.id.etregdate).getEditText().setKeyListener(null);
        aqPMSel.id(R.id.etMotherAdd).getEditText().setKeyListener(null);
        aqPMSel.id(R.id.lllmpdate).visible();
        aqPMSel.id(R.id.llgest).visible();
        aqPMSel.id(R.id.vHLine4).visible();
        aqPMSel.id(R.id.vHLine6).visible();
        aqPMSel.id(R.id.llnote).visible();

        //13Apr2021 Bindu - set here cos not translated from xml
        aqPMSel.id(R.id.anclbl).text(getResources().getString(R.string.anclbl));
        aqPMSel.id(R.id.txtpnclbl).text(getResources().getString(R.string.pnclbl));
        aqPMSel.id(R.id.lmp_date).text(getResources().getString(R.string.tvedd));
        aqPMSel.id(R.id.txtgest).text(getResources().getString(R.string.tvGestationalageL));
        aqPMSel.id(R.id.txtchildreglbl).text(getResources().getString(R.string.childreg));
        aqPMSel.id(R.id.tvFirstPregHeading).text(getResources().getString(R.string.tvFirstPregHeading));
        aqPMSel.id(R.id.reg_date).text(getResources().getString(R.string.reg_date));
        aqPMSel.id(R.id.instructions).text(getResources().getString(R.string.instruction));
        aqPMSel.id(R.id.regdt).text(getResources().getString(R.string.reg_date));
        aqPMSel.id(R.id.txt1).text(getResources().getString(R.string.date_val_0));
        aqPMSel.id(R.id.txt2).text(getResources().getString(R.string.date_val_1));
        aqPMSel.id(R.id.txt3).text(getResources().getString(R.string.date_val_2));
        aqPMSel.id(R.id.txtlmp).text(getResources().getString(R.string.tvPregLmpHeading));
        aqPMSel.id(R.id.txt4).text(getResources().getString(R.string.date_val_3));
        aqPMSel.id(R.id.txt5).text(getResources().getString(R.string.date_val_4));
        aqPMSel.id(R.id.txt6).text(getResources().getString(R.string.date_val_5));
        aqPMSel.id(R.id.txt7).text(getResources().getString(R.string.date_val_6));
        aqPMSel.id(R.id.txt8).text(getResources().getString(R.string.date_val_7));
        aqPMSel.id(R.id.radLMP).text(getResources().getString(R.string.tvlmp));
        aqPMSel.id(R.id.radEDD).text(getResources().getString(R.string.tvedd));
//22May2021 Bindu set adol reg lbl
        aqPMSel.id(R.id.adolreg).text(getResources().getString(R.string.adolescentreg));

    }

    // capture EDD
    private void setEDD() throws Exception {
        aqPMSel.id(R.id.tvMotherAddHeading).text(getResources().getString(R.string.tvedd));
        aqPMSel.id(R.id.etMotherAdd).text("");
        aqPMSel.id(R.id.imgclearlmp).gone();//20AUg2019 Arpitha
        aqPMSel.id(R.id.lmp_date).text(getResources().getString(R.string.tvlmp));
        aqPMSel.id(R.id.etlmpdate).text("");
        aqPMSel.id(R.id.etgest).text("");
        aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());
    }

    // capture LMP
    private void setLMP() throws Exception {
        aqPMSel.id(R.id.tvMotherAddHeading).text(getResources().getString(R.string.tvlmp));
        aqPMSel.id(R.id.etMotherAdd).text("");
        aqPMSel.id(R.id.imgclearlmp).gone();//20AUg2019 Arpitha
        aqPMSel.id(R.id.lmp_date).text(getResources().getString(R.string.tvedd));
        aqPMSel.id(R.id.etlmpdate).text("");
        aqPMSel.id(R.id.etgest).text("");
    }


    /**
     * Common method invokes when any button clicked This method handles
     * Edittext, save clicks. Validate mandatory fields
     */
    public void commonClick(View v) throws Exception {
        switch (v.getId()) {
            case R.id.rdbPreg: {
                aqPMSel.id(R.id.rdbMother).checked(false);
                aqPMSel.id(R.id.tvFirstPregHeading).enabled(true);
                aqPMSel.id(R.id.rdbchild).checked(false);
                aqPMSel.id(R.id.rdbAdol).checked(false);//Ramesh 13-may2021
                aqPMSel.id(R.id.chkFirstPreg).enabled(true);
                aqPMSel.id(R.id.radEDDorLmp).visible();
                //27Nov2018 - Bindu - radiogrp clear chk to reset radiobtn
                aqPMSel.id(R.id.vHLine5).visible();
                aqPMSel.id(R.id.etMotherAdd).text("");
                aqPMSel.id(R.id.radLMP).checked(true);
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.tvPregLmpHeading)));
                // 27Jun2018 Arpitha
                aqPMSel.id(R.id.etregdate).enabled(true);
                aqPMSel.id(R.id.etregdate).textColor(getResources().getColor(R.color.black));
//				aqPMSel.id(R.id.llregdate).background(R.drawable.editext_none);
                aqPMSel.id(R.id.lllmpdate).visible();
                aqPMSel.id(R.id.llgest).visible();
                aqPMSel.id(R.id.vHLine4).visible();
                aqPMSel.id(R.id.vHLine6).visible();

                aqPMSel.id(R.id.llMotherAdd).visible();
                aqPMSel.id(R.id.llFirstPreg).visible();
                aqPMSel.id(R.id.llnote).visible();
                aqPMSel.id(R.id.vHLine5).visible();
//				19Nov2019 Arpitha
                aqPMSel.id(R.id.llancinstruction).visible();
                aqPMSel.id(R.id.llpncinstruction).gone();

                break;
            }
            case R.id.rdbMother: {
                aqPMSel.id(R.id.rdbPreg).checked(false);
                aqPMSel.id(R.id.rdbchild).checked(false);
                aqPMSel.id(R.id.rdbAdol).checked(false);//Ramesh 13-may2021
                //27Nov2018 - Bindu radiogrp clear chk to reset radiobtn
                radGrpEDDorLmp.clearCheck();
                aqPMSel.id(R.id.radLMP).checked(false);
                aqPMSel.id(R.id.radEDD).checked(false);
                aqPMSel.id(R.id.tvFirstPregHeading).enabled(false);
                aqPMSel.id(R.id.chkFirstPreg).enabled(false);
                aqPMSel.id(R.id.chkFirstPreg).checked(false);
                aqPMSel.id(R.id.radEDDorLmp).gone();
                aqPMSel.id(R.id.vHLine5).gone();
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.deldate)));
                aqPMSel.id(R.id.etMotherAdd).text("");
                // 27Jun2018 Arpitha
                aqPMSel.id(R.id.etregdate).enabled(false);
                aqPMSel.id(R.id.etregdate).textColor(R.color.fontcolor_disable);
                //	aqPMSel.id(R.id.llregdate).background(R.drawable.edittext_disable);
                aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());//21March2019 Arpitha
                aqPMSel.id(R.id.lllmpdate).gone();
                aqPMSel.id(R.id.llgest).gone();
                aqPMSel.id(R.id.vHLine4).gone();
                aqPMSel.id(R.id.vHLine6).gone();

                aqPMSel.id(R.id.llMotherAdd).visible();
                aqPMSel.id(R.id.llFirstPreg).visible();
                aqPMSel.id(R.id.llnote).visible();
                aqPMSel.id(R.id.vHLine5).visible();
                //				19Nov2019 Arpitha
                aqPMSel.id(R.id.llancinstruction).gone();
                aqPMSel.id(R.id.llpncinstruction).visible();
                aqPMSel.id(R.id.llFirstPreg).gone();
                aqPMSel.id(R.id.imgclearlmp).visible();
                break;
            }
            case R.id.radEDD: {

                //	aqPMSel.id(R.id.radEDD).checked(true); //27Nov2018 - Bindu- comment the line
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.deldate)));
                aqPMSel.id(R.id.etMotherAdd).text("");
                aqPMSel.id(R.id.lmp_date).text((getResources().getString(R.string.tvlmp)));// 28Jun2018
                // Arpitha
                // 11July2018 Arpitha
                aqPMSel.id(R.id.etlmpdate).text("");
                aqPMSel.id(R.id.etgest).text("");
                break;
            }
            case R.id.rdbchild: {
                aqPMSel.id(R.id.rdbPreg).checked(false);
                aqPMSel.id(R.id.rdbMother).checked(false);
                aqPMSel.id(R.id.rdbAdol).checked(false);//Ramesh 13-may2021

                //27Nov2018 - Bindu radiogrp clear chk to reset radiobtn
                radGrpEDDorLmp.clearCheck();//Arpitha2021
                aqPMSel.id(R.id.radLMP).checked(false);
                aqPMSel.id(R.id.radEDD).checked(false);
                aqPMSel.id(R.id.tvFirstPregHeading).enabled(false);
                aqPMSel.id(R.id.chkFirstPreg).enabled(false).checked(false);
                aqPMSel.id(R.id.radEDDorLmp).gone();
                aqPMSel.id(R.id.vHLine5).gone();
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.deldate)));
                aqPMSel.id(R.id.etMotherAdd).text("");
                aqPMSel.id(R.id.llMotherAdd).gone();
                aqPMSel.id(R.id.llFirstPreg).gone();
                aqPMSel.id(R.id.llnote).gone();
                // 27Jun2018 Arpitha
                aqPMSel.id(R.id.etregdate).enabled(false);
                aqPMSel.id(R.id.etregdate).textColor(R.color.fontcolor_disable);
                //	aqPMSel.id(R.id.llregdate).background(R.drawable.edittext_disable);
                aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());//21March2019 Arpitha
                aqPMSel.id(R.id.lllmpdate).gone();
                aqPMSel.id(R.id.llgest).gone();
                aqPMSel.id(R.id.vHLine4).gone();
                aqPMSel.id(R.id.vHLine6).gone();
                break;
            }
            case R.id.radLMP: {
                //	aqPMSel.id(R.id.radLMP).checked(true); //27Nov2018 - Bindu- comment the line
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.tvlmp)));
                aqPMSel.id(R.id.etMotherAdd).text("");
                aqPMSel.id(R.id.lmp_date).text((getResources().getString(R.string.tvedd)));// 28Jun2018
                // Arpitha

                // 11July2018 Arpitha
                aqPMSel.id(R.id.etlmpdate).text("");
                aqPMSel.id(R.id.etgest).text("");
                break;
            }

            case R.id.gridView1:
                break;
            case R.id.rdbAdol://Ramesh 13-may-2021
                aqPMSel.id(R.id.rdbPreg).checked(false);
                aqPMSel.id(R.id.rdbMother).checked(false);
                aqPMSel.id(R.id.rdbchild).checked(false);

                radGrpEDDorLmp.clearCheck();//Arpitha2021
                aqPMSel.id(R.id.radLMP).checked(false);
                aqPMSel.id(R.id.radEDD).checked(false);
                aqPMSel.id(R.id.tvFirstPregHeading).enabled(false);
                aqPMSel.id(R.id.chkFirstPreg).enabled(false).checked(false);
                aqPMSel.id(R.id.radEDDorLmp).gone();
                aqPMSel.id(R.id.vHLine5).gone();
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.deldate)));
                aqPMSel.id(R.id.etMotherAdd).text("");
                aqPMSel.id(R.id.llMotherAdd).gone();
                aqPMSel.id(R.id.llFirstPreg).gone();
                aqPMSel.id(R.id.llnote).gone();
                aqPMSel.id(R.id.etregdate).enabled(false);
                aqPMSel.id(R.id.etregdate).textColor(R.color.fontcolor_disable);
                aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());//21March2019 Arpitha
                aqPMSel.id(R.id.lllmpdate).gone();
                aqPMSel.id(R.id.llgest).gone();
                aqPMSel.id(R.id.vHLine4).gone();
                aqPMSel.id(R.id.vHLine6).gone();
                break;
            default: {
                break;
            }
        }
    }

    private void validateAndRegister() throws Exception {
        try {
            Intent nextScreen;
            String lmpOrAdd = aqPMSel.id(R.id.etMotherAdd).getText().toString();
            String regDate = aqPMSel.id(R.id.etregdate).getText().toString();

            if ((lmpOrAdd != null && lmpOrAdd.length() > 0) && (regDate != null && regDate.length() > 0)) {

                if (aqPMSel.id(R.id.rdbPreg).isChecked()) {
                    if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0) {
                        int days = DateTimeUtil.getDaysBetweenDates(regDate, lmpOrAdd);
                        int daysdiff = DateTimeUtil
                                .getDaysBetweenDates(DateTimeUtil.getTodaysDate(), lmpOrAdd);
                        if (aqPMSel.id(R.id.radEDD).isChecked()) {
                            if (days >= 0) {
                                wantToCloseDialog = false;
                                Toast.makeText(getApplicationContext(),
                                        getResources().getString(R.string.date_val_5_toast),

                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etMotherAdd).text("");
                            } else if (noOfDays < 30) {
                                wantToCloseDialog = false;
                                Toast.makeText(getApplicationContext(),
                                        getResources().getString(R.string.gestation_must_be_30days),
                                        Toast.LENGTH_LONG).show();
                            } else if (noOfDays > 280) {
                                wantToCloseDialog = false;
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.gest_280),
                                        Toast.LENGTH_LONG).show();
                            } else {
                                wantToCloseDialog = true;
                                woman = new tblregisteredwomen();
                                nextScreen = new Intent(getApplicationContext(), RegistrationActivity.class);
                                nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                nextScreen.putExtra("adolAadhar", adolEditData.getRegAdolAadharNo());//01Sep2021 Arpitha
                                woman.setRegPregnantorMother(1);
                                int gravida = 0;
                                if (aqPMSel.id(R.id.chkFirstPreg).isChecked())
                                    gravida = 1;
                                woman.setRegGravida(gravida);
                                woman.setRegRegistrationDate(aqPMSel.id(R.id.etregdate).getText().toString());
                                woman.setRegEDD(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                                nextScreen.putExtra("woman", woman);
                                startActivity(nextScreen);
                            }
                        } else {
                            if (days < 30) {
                                wantToCloseDialog = false;
                                Toast.makeText(getApplicationContext(),
                                        getResources().getString(R.string.date_val_3_toast),

                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etlmpdate).text("");
                                aqPMSel.id(R.id.etMotherAdd).text("");
                            } else if (daysdiff > 280) {

                                wantToCloseDialog = false;
                                Toast.makeText(getApplicationContext(),
                                        activity.getResources().getString(R.string.date_val_4_toast),
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etlmpdate).text("");
                                aqPMSel.id(R.id.etMotherAdd).text("");
                            } else {
                                wantToCloseDialog = true;
                                woman = new tblregisteredwomen();
                                nextScreen = new Intent(activity, RegistrationActivity.class);
                                nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                nextScreen.putExtra("adolAadhar", adolEditData.getRegAdolAadharNo());//01Sep2021 Arpitha


                                woman.setRegPregnantorMother(1);
                                int gravida = 0;
                                if (aqPMSel.id(R.id.chkFirstPreg).isChecked())
                                    gravida = 1;
                                woman.setRegGravida(gravida);
                                woman.setRegRegistrationDate(aqPMSel.id(R.id.etregdate).getText().toString());
                                if (aqPMSel.id(R.id.radEDD).isChecked())
                                    woman.setRegEDD(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                                else
                                    woman.setRegLMP(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                                nextScreen.putExtra("woman", woman);
                                startActivity(nextScreen);
                            }
                        }

                    }
                } else if (aqPMSel.id(R.id.rdbMother).isChecked()) {
                    if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0) {
                        int days = DateTimeUtil.getDaysBetweenDates(regDate, lmpOrAdd);
                        if (days < 0) {
                            wantToCloseDialog = false;
                            Toast.makeText(activity, (getResources().getString(R.string.m077)),
                                    Toast.LENGTH_LONG).show();
                            // } else if (days > 549) {
                        } else if (days > 5844) {// Arpitha
                            // 28Jun2018
                            wantToCloseDialog = false;
                            Toast.makeText(activity,
                                    (getResources().getString(R.string.date_val_7_toast)), // 11july2018
                                    // Arpitha
                                    Toast.LENGTH_LONG).show();
                        } else {
                            wantToCloseDialog = true;
                            woman = new tblregisteredwomen();
                            nextScreen = new Intent(activity, RegistrationActivity.class);
                            nextScreen.putExtra("adolAadhar", adolEditData.getRegAdolAadharNo());//01Sep2021 Arpitha
                            woman.setRegPregnantorMother(2);
                            woman.setRegADDate(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                            woman.setRegRegistrationDate(aqPMSel.id(R.id.etregdate).getText().toString());
                            nextScreen.putExtra("woman", woman);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(nextScreen);
                        }
                    }
                } else if (aqPMSel.id(R.id.rdbchild).isChecked()) {
                    if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0) {
                        int days = DateTimeUtil.getDaysBetweenDates(regDate, lmpOrAdd);
                        if (days < 0) {
                            wantToCloseDialog = false;
                            Toast.makeText(activity, (getResources().getString(R.string.m077)),
                                    Toast.LENGTH_LONG).show();
                            // } else if (days > 549) {
                        } else if (days > 5844) {// Arpitha
                            // 28Jun2018
                            wantToCloseDialog = false;
                            Toast.makeText(activity,
                                    (getResources().getString(R.string.date_val_7_toast)), // 11july2018
                                    // Arpitha
                                    Toast.LENGTH_LONG).show();
                        } else {
                            wantToCloseDialog = true;
                            woman = new tblregisteredwomen();
                            nextScreen = new Intent(activity, ChildRegistrationActivity.class);
//							woman.setRegPregnantorMother(1);
                            woman.setRegADDate(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                            woman.setRegRegistrationDate(aqPMSel.id(R.id.etregdate).getText().toString());
                            nextScreen.putExtra("woman", woman);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(nextScreen);
                        }
                    }
                } else if (aqPMSel.id(R.id.rdbAdol).isChecked()) { //Ramesh 13-may-2021
                    if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0) {
                        int days = DateTimeUtil.getDaysBetweenDates(regDate, lmpOrAdd);
                        if (days < 0) {
                            wantToCloseDialog = false;
                            Toast.makeText(activity, (getResources().getString(R.string.m077)),
                                    Toast.LENGTH_LONG).show();
                            // } else if (days > 549) {
                        } else if (days > 5844) {// Arpitha
                            // 28Jun2018
                            wantToCloseDialog = false;
                            Toast.makeText(activity,
                                    (getResources().getString(R.string.date_val_7_toast)), // 11july2018
                                    // Arpitha
                                    Toast.LENGTH_LONG).show();
                        } else {
                            wantToCloseDialog = true;
                            tblAdolReg adolReg = new tblAdolReg();
                            nextScreen = new Intent(activity, AdolRegistration.class);
                            adolReg.setRecordCreatedDate(aqPMSel.id(R.id.etregdate).getText().toString());
                            nextScreen.putExtra("AdolReg", adolReg);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(nextScreen);
                        }
                    }
                }
            } else {
                wantToCloseDialog = false;
                if (aqPMSel.id(R.id.rdbPreg).isChecked()) {
                    if (aqPMSel.id(R.id.etMotherAdd).getText().toString().trim().length() <= 0) {
                        aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                        if (aqPMSel.id(R.id.radEDD).isChecked())
                            Toast.makeText(activity,
                                    (activity.getResources().getString(R.string.m170)),
                                    Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(activity,
                                    activity.getResources().getString(R.string.m074),
                                    Toast.LENGTH_LONG).show();
                    } else if (aqPMSel.id(R.id.etregdate).getText().toString().trim().length() <= 0) {
                        aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                        Toast.makeText(getApplicationContext(),
                                (activity.getResources().getString(R.string.m157)),
                                Toast.LENGTH_LONG).show();
                    }
                } else if (aqPMSel.id(R.id.rdbMother).isChecked()) {
                    if (aqPMSel.id(R.id.etMotherAdd).getText().toString().trim().length() <= 0) {
                        aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                        Toast.makeText(activity,
                                (activity.getResources().getString(R.string.m066)),
                                Toast.LENGTH_LONG).show();
                    } else if (aqPMSel.id(R.id.etregdate).getText().toString().trim().length() <= 0) {
                        aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                        Toast.makeText(activity,
                                activity.getResources().getString(R.string.m157),
                                Toast.LENGTH_LONG).show();
                    }
                } else if (aqPMSel.id(R.id.rdbchild).isChecked()) {
                    wantToCloseDialog = true;
//					if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0)
                    {
//						int days = DateTimeUtil.getDaysBetweenDates(regDate, lmpOrAdd);
						/*if (days < 0) {
							wantToCloseDialog = false;
							Toast.makeText(activity, (getResources().getString(R.string.m077)),
									Toast.LENGTH_LONG).show();
							// } else if (days > 549) {
						} else if (days > 5844) {// Arpitha
							// 28Jun2018
							wantToCloseDialog = false;
							Toast.makeText(activity,
									(getResources().getString(R.string.date_val_7_toast)), // 11july2018
									// Arpitha
									Toast.LENGTH_LONG).show();
						} else {*/
                        wantToCloseDialog = true;
                        woman = new tblregisteredwomen();
                        nextScreen = new Intent(activity, ChildRegistrationActivity.class);
                        woman.setRegPregnantorMother(1);
                        woman.setRegADDate(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                        woman.setRegRegistrationDate(aqPMSel.id(R.id.etregdate).getText().toString());
                        nextScreen.putExtra("woman", woman);
                        nextScreen.putExtra("globalState", activity.getIntent().getBundleExtra("globalState"));
                        startActivity(nextScreen);
//						}
                    }
                } else if (aqPMSel.id(R.id.rdbAdol).isChecked()) { //Ramesh 13-may-2021

                    wantToCloseDialog = true;
                    tblAdolReg adolReg = new tblAdolReg();
                    nextScreen = new Intent(activity, AdolRegistration.class);
                    adolReg.setRecordCreatedDate(aqPMSel.id(R.id.etregdate).getText().toString());
                    nextScreen.putExtra("AdolReg", adolReg);
                    nextScreen.putExtra("globalState", activity.getIntent().getBundleExtra("globalState"));
                    startActivity(nextScreen);


                }
            }

        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }
}
