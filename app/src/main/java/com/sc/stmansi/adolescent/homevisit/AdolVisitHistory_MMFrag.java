package com.sc.stmansi.adolescent.homevisit;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.R;
import com.sc.stmansi.adolescent.HomeVisitHistoryAdapter;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.configuration.SyncState;
import com.sc.stmansi.database.DBMethods;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.repositories.AdolVisitHeaderRepository;
import com.sc.stmansi.repositories.AdolescentRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblAdolReg;
import com.sc.stmansi.tables.tblAdolVisitHeader;

import java.util.List;

public class AdolVisitHistory_MMFrag extends Fragment {

    private View parentview;
    private DatabaseHelper databaseHelper;
    private AQuery aq;
    private AppState appState;
    private Bundle bundle;
    private SyncState syncState;
    private String adolId;
    private AdolVisitHeaderRepository adolVisitHeaderRepository;
    private List<tblAdolVisitHeader> currentAdolHeaders;
    private tblAdolReg currentAdolDetails;
    private RecyclerView recyclerview;
    private TblInstusers user;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        parentview =  inflater.inflate(R.layout.adolhv_mmfragment, container, false);
        bundle = this.getArguments();
        if (bundle != null) {
            appState = bundle.getParcelable("appState");
            adolId = bundle.getString("adolID");
        } else {
            appState = new AppState();
        }
        return parentview;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Initalize();
        getData();

        UpdateUI();
    }

    private void Initalize() {
        try{
            databaseHelper = getHelper();
            DBMethods dbMethods = new DBMethods(databaseHelper);
            aq = new AQuery(parentview);
            recyclerview = parentview.findViewById(R.id.rvAdolVisitHistory_MM);

            UserRepository userRepo = new UserRepository(databaseHelper);
            user = userRepo.getOneAuditedUser(appState.ashaId);
        }catch (Exception e){
            FirebaseCrashlytics firebaseCrashlytics = FirebaseCrashlytics.getInstance();
            firebaseCrashlytics.log(e.getMessage());
        }
    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(parentview.getContext(), DatabaseHelper.class);
        }
        return databaseHelper;
    }

    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        b.putParcelable("syncState", syncState);
        return b;
    }
    private void getData() {
        try {
            AdolescentRepository adolescentRepository = new AdolescentRepository(databaseHelper);
            currentAdolDetails = adolescentRepository.getAdolDatafromAdolID(adolId);
            adolVisitHeaderRepository = new AdolVisitHeaderRepository(databaseHelper);
//            currentAdolHeaders = adolVisitHeaderRepository.getAllVisits(adolId,getActivity().getResources().getString(R.string.userlevel_mm));
            currentAdolHeaders = adolVisitHeaderRepository.getAllVisits(adolId);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void UpdateUI() {
        try {
            if (currentAdolHeaders.size()!=0){
                recyclerview.setHasFixedSize(true);
                aq.id(R.id.txt_AVH_NoRecords_MM).gone();
                recyclerview.setVisibility(View.VISIBLE);
                HomeVisitHistoryAdapter visitDetailHAdapter = new HomeVisitHistoryAdapter(currentAdolDetails, currentAdolHeaders,parentview.getContext(),prepareBundle(),databaseHelper,user);
                recyclerview.setLayoutManager(new LinearLayoutManager(parentview.getContext()));
                recyclerview.setAdapter(visitDetailHAdapter);
                aq.id(R.id.txtAdolVisitHistoryCount_MM).getTextView().setText(parentview.getResources().getString(R.string.reccount) + visitDetailHAdapter.getItemCount());
            }else {
                aq.id(R.id.txt_AVH_NoRecords_MM).visible();
                recyclerview.setVisibility(View.GONE);aq.id(R.id.txtAdolVisitHistoryCount_MM).getTextView().setText(getActivity().getResources().getString(R.string.reccount) + 0);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
