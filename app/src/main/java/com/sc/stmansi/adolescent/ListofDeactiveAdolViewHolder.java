package com.sc.stmansi.adolescent;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sc.stmansi.R;

public class ListofDeactiveAdolViewHolder extends RecyclerView.ViewHolder {
    TextView txtName, txtAge, txtMobile,txtListofAdolDDAte;
    public ListofDeactiveAdolViewHolder(@NonNull View itemView) {
        super(itemView);
        txtName = itemView.findViewById(R.id.txtListofAdolName);
        txtAge = itemView.findViewById(R.id.txtListofAdolAge);
        txtMobile = itemView.findViewById(R.id.txtListofAdolMobile);
        txtListofAdolDDAte = itemView.findViewById(R.id.txtListofAdolDeactivateDate);
    }
}
