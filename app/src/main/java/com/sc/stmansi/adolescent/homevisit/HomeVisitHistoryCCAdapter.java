package com.sc.stmansi.adolescent.homevisit;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.sc.stmansi.R;
import com.sc.stmansi.tables.tblAdolReg;
import com.sc.stmansi.tables.tblCaseMgmt;

import java.util.List;

public class HomeVisitHistoryCCAdapter extends RecyclerView.Adapter<HomeVisitHistoryCCViewHolder> {


    private final Bundle bundle;
    private final tblAdolReg adolDetails;
    private final List<tblCaseMgmt> caseDetails;
    private final Context context;

    public HomeVisitHistoryCCAdapter(tblAdolReg currentAdolDetails, List<tblCaseMgmt> currentcaseDetails, Context context, Bundle prepareBundle) {
        adolDetails = currentAdolDetails;
        caseDetails = currentcaseDetails;
        this.context = context;
        bundle = prepareBundle;
    }

    @NonNull
    @Override
    public HomeVisitHistoryCCViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CardView v = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_homevisithistorycc, parent, false);
        return new HomeVisitHistoryCCViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull HomeVisitHistoryCCViewHolder holder, int position) {
        try{
            holder.txtVisitCount.setText(context.getResources().getString(R.string.visitcount) + ": " + caseDetails.get(position).getVisitNum());
            holder.txtVisitAge.setText(context.getResources().getString(R.string.age)+": "+adolDetails.getRegAdolAge());
            holder.txtVisitDate.setText(context.getResources().getString(R.string.visitdate) + ": " + caseDetails.get(position).getVisitDate());

            String t = context.getResources().getString(R.string.healthissuetrim)+" @ ";

            if (caseDetails.get(position).getVisitBeneficiaryDangerSigns().length()!=0 && !(caseDetails.get(position).getVisitBeneficiaryDangerSigns().equals(t))){
                holder.imgComplication.setVisibility(View.VISIBLE);
            }else {
                holder.imgComplication.setVisibility(View.INVISIBLE);
            }

            if (caseDetails.get(position).getVisitReferredtoFacType().length()!=0){
                holder.imgReferred.setVisibility(View.VISIBLE);
            }else {
                holder.imgReferred.setVisibility(View.INVISIBLE);
            }

            holder.imgComplication.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    View complicationView = View.inflate(context, R.layout.customdialogavh_complication, null);
                    builder.setView(complicationView);
                    builder.setCancelable(false);
                    builder.setPositiveButton(context.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    }).create().show();

                    TextView txtCHIHead = complicationView.findViewById(R.id.txtcomplicationHealthIssueHead);
                    TextView txtCHIBody = complicationView.findViewById(R.id.txtcomplicationHealthIssueBody);
                    TextView txtCMPHead = complicationView.findViewById(R.id.txtcomplicationMenstrualproblemsHead);
                    TextView txtCMPBody = complicationView.findViewById(R.id.txtcomplicationMenstrualproblemsBody);
                    TextView txtHBHead = complicationView.findViewById(R.id.txtcomplicationHBHead);
                    TextView txtHBBody = complicationView.findViewById(R.id.txtcomplicationHBBody);
                    TextView txtWeightHead = complicationView.findViewById(R.id.txtcomplicationWeightHead);
                    TextView txtWeightBody = complicationView.findViewById(R.id.txtcomplicationWeightBody);
                    TextView txtHeightHead = complicationView.findViewById(R.id.txtcomplicationHeightHead);
                    TextView txtHeightBody = complicationView.findViewById(R.id.txtcomplicationHeightBody);

                    String complications = caseDetails.get(position).getVisitBeneficiaryDangerSigns();
                    String[] listofcomplications = complications.split("@");
                    for (String s:listofcomplications){
                        if (s.contains("Health")){
                            txtCHIHead.setVisibility(View.VISIBLE);
                            String hit = context.getResources().getString(R.string.healthissuetrim);
                            String hi = s.replace(hit,"");
                            String[] hidata = hi.split("  ");
                            StringBuilder hiBuilder = new StringBuilder();
                            for (String tempword : hidata) {
                                String word = tempword.trim();
                                word = word.replace("Anemia",context.getResources().getString(R.string.anemia));
                                hiBuilder.append(word).append(", ");
                            }
                            if (hiBuilder.toString().contains(",")){
                                txtCHIBody.setText(hiBuilder.deleteCharAt(hiBuilder.lastIndexOf(",")));
                            }else {
                                txtCHIBody.setText(hiBuilder.toString());
                            }
                            txtCHIBody.setVisibility(View.VISIBLE);
                        }
                        if (s.contains("hb")){
                            txtHBHead.setVisibility(View.VISIBLE);
//                            txtHBBody.setText(s.replace("hb is ",""));
                            String hbtemp = s.replace("hb is ", "");
                            txtHBBody.setText(s.replace("hb is ", "")+" "+context.getResources().getString(R.string.gramsperliter)+ "( "+getHGCategory(Float.parseFloat(hbtemp))+")");
                            txtHBBody.setVisibility(View.VISIBLE);
                        }
                        if (s.contains("Weight")){
                            txtWeightHead.setVisibility(View.VISIBLE);
                            txtWeightBody.setText(s.replace("Weight is",""));
                            txtWeightBody.setVisibility(View.VISIBLE);
                        }
                        if (s.contains("Height")){
                            txtHeightHead.setVisibility(View.VISIBLE);
                            txtHeightBody.setText(s.replace("Height is ",""));
                            txtHeightBody.setVisibility(View.VISIBLE);
                        }
                        if (s.contains("Menstrual")){
                            txtCMPHead.setVisibility(View.VISIBLE);
                            String hi = s.trim().replace("Menstrualpbm " , "");
                            String[] hidata = hi.split("  ");
                            StringBuilder hiBuilder = new StringBuilder();
                            String[] mensuproblems = context.getResources().getStringArray(R.array.menustralproblems);
                            for (String word : hidata) {
                                try{
                                    int intWord = Integer.parseInt(word.trim());
                                    hiBuilder.append(mensuproblems[intWord]).append(", ");
                                }catch (Exception e){
                                    hiBuilder.append(word).append(",");
                                }
                            }
                            if (hiBuilder.toString().contains(",")){
                                txtCMPBody.setText(hiBuilder.deleteCharAt(hiBuilder.lastIndexOf(",")));
                            }else {
                                txtCMPBody.setText(hiBuilder.toString());
                            }
                            txtCMPBody.setVisibility(View.VISIBLE);
                        }
                    }
                }
            });

            holder.imgViewVisit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent;
                    intent = new Intent(context, AdolVisitViewCC.class);
                    intent.putExtra("globalState", bundle);
                    intent.putExtra("VisitCount", caseDetails.get(position).getVisitNum()+"");
                    intent.putExtra("adolID", caseDetails.get(position).getBeneficiaryID());
                    intent.putExtra("userTypeHV",caseDetails.get(position).getHcmCreatedByUserType());
                    context.startActivity(intent);
                }
            });

        }catch (Exception e){
            FirebaseCrashlytics firebaseCrashlytics = FirebaseCrashlytics.getInstance();
            firebaseCrashlytics.log(e.getMessage());
        }
    }

    @Override
    public int getItemCount() {
        return caseDetails.size();
    }
    private String getHGCategory(float hbCount) {
        try {
//            float hbCount = Float.parseFloat(aqAdolVisit.id(R.id.etAdolVisitHbCount).getEditText().getText().toString());
            if (hbCount < 8.0) {
                //severe
                return context.getResources().getString(R.string.severe);
            } else if (hbCount >= 8 && hbCount <= 9.9) {
                //moderate
                return context.getResources().getString(R.string.moderate);
            } else if (hbCount >= 10 && hbCount <= 11.9) {
                //mild
                return context.getResources().getString(R.string.mild);
            } else if (hbCount >= 11.9 && hbCount <= 24) {
                //normal
                return context.getResources().getString(R.string.normal);
            } else if (hbCount > 24) {
                return "NA";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}
