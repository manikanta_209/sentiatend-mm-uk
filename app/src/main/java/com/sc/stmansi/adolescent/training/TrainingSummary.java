package com.sc.stmansi.adolescent.training;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.R;
import com.sc.stmansi.adolescent.AdolescentHome;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.configuration.SyncState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.repositories.CampaignRepository;
import com.sc.stmansi.repositories.FacilityRepository;
import com.sc.stmansi.tables.tblCampDetails;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class TrainingSummary extends AppCompatActivity {

    private AppState appState;
    private DatabaseHelper databaseHelper;
    private SyncState syncState;
    private AQuery aQuery;
    private String campID;
    private tblCampDetails campDetails;
    private Map<String, Integer> facilities;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trainingsummary);
        try {
            Bundle bundle = getIntent().getBundleExtra("globalState");
            if (bundle != null) {
                appState = bundle.getParcelable("appState");
                syncState = bundle.getParcelable("syncState");
            } else {
                appState = new AppState();
            }
            campID = getIntent().getStringExtra("campID");
            getSupportActionBar().hide();
            Initialize();
            getData();

            setData();

            aQuery.id(R.id.toolbarBtnHome).getButton().setVisibility(View.VISIBLE);
            aQuery.id(R.id.toolbarBtnHome).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    displayDialogExit();
                }
            });
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private void displayDialogExit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(TrainingSummary.this);
        builder.setMessage(getResources().getString(R.string.exit));
        builder.setCancelable(false);
        builder.setNegativeButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(TrainingSummary.this, AdolescentHome.class);
                intent.putExtra("tabItem", 1);
                intent.putExtra("globalState", prepareBundle());
                startActivity(intent);
            }
        }).setPositiveButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    @SuppressLint("SetTextI18n")
    private void setData() {
        try {
            if (campDetails.getTblCampID().length() != 0) {

                String dateTime = campDetails.getTblCampDateTime();
                int index = dateTime.indexOf(" ");
                String date = dateTime.substring(0, index);
                aQuery.id(R.id.txtCampSumCreatedDate).getTextView().setText(date);

                String[] schemaTypes = getResources().getStringArray(R.array.SchemaType);
                aQuery.id(R.id.txtCampSumSchemaType).getTextView().setText(schemaTypes[campDetails.getTblCampSchemaType()]);

                aQuery.id(R.id.txtCampSumCCAcompany).getTextView().setText(campDetails.getTblCampCCAccompany());

                //            15MAy2021 Bindu get training name
                String[] trainingnameengkan = getResources().getStringArray(R.array.adoltrainingname);

                String[] trainingNames = getResources().getStringArray(R.array.adoltrainingnamecheck);//14- May -2021 Ramesh //15May2021 Bindu check from eng str
                int trainingIndex = 0;
                if (!(campDetails.getTblCampName().length() > 2)) {
                    try {
                        trainingIndex = Integer.parseInt(campDetails.getTblCampName());
                        aQuery.id(R.id.txtCampSumName).getTextView().setText(trainingnameengkan[trainingIndex]); //17 May2021 Ramesh
                        String[] topics = getTrainingTopics(trainingIndex);
                        String topicIndex = campDetails.getTblCampProObjective();
                        if (topicIndex.contains(",")) {
                            List<String> topicIndexbeforeInt = Arrays.asList(topicIndex.split(","));
                            ArrayList<Integer> intList = new ArrayList<>();
                            for (String s : topicIndexbeforeInt)
                                intList.add(Integer.valueOf(s.trim()));
                            StringBuilder topicNames = new StringBuilder();
                            for (int s : intList) {
                                topicNames.append(topics[s]).append(", ");
                            }
                            topicNames = topicNames.deleteCharAt(topicNames.lastIndexOf(","));
                            aQuery.id(R.id.txtCampSumProObj).getTextView().setText(topicNames);
                        } else {
                            aQuery.id(R.id.txtCampSumProObj).getTextView().setText(topics[Integer.parseInt(topicIndex.trim())]);
                        }
                    } catch (Exception e) {
                        Throwable cause = e;
                        while (cause.getCause() != null && cause.getCause() != cause) {
                            cause = cause.getCause();
                        }
                        String exception = cause.toString();
                        if (exception.toLowerCase().contains("numberformatexception")) {
                            aQuery.id(R.id.txtCampSumProObj).getTextView().setText(campDetails.getTblCampProObjOthers());
                            aQuery.id(R.id.txtCampSumName).getTextView().setText(campDetails.getTblCampName()); //17 May2021 Ramesh
                        }
                    }
                } else {
                    aQuery.id(R.id.txtCampSumProObj).getTextView().setText(campDetails.getTblCampProObjOthers());
                    aQuery.id(R.id.txtCampSumName).getTextView().setText(campDetails.getTblCampName()); //17 May2021 Ramesh
                }

                FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
                facilities = facilityRepository.getPlaceMap();
                ArrayList<String> villageList = new ArrayList<String>();
                villageList.add(getResources().getString(R.string.selectType));
                for (Map.Entry<String, Integer> village : facilities.entrySet())
                    villageList.add(village.getKey());

                if (campDetails.getTblCampFacilities().contains(",")) {
                    String[] facilitiesIndexs = campDetails.getTblCampFacilities().split(",");
                    StringBuilder FacilityName = new StringBuilder();
                    for (String s : facilitiesIndexs) {
                        s = s.trim();
                        FacilityName.append(villageList.get(Integer.parseInt(s))).append(",");
                    }
                    index = FacilityName.lastIndexOf(",");
                    FacilityName = FacilityName.deleteCharAt(index);
                    aQuery.id(R.id.txtCampSumFacility).getTextView().setText(FacilityName);
                } else {
                    String facilitiesIndex = campDetails.getTblCampFacilities();
                    int villagecode = getKey(facilities, facilitiesIndex);
                    String FacilityName = villageList.get(villagecode);
                    aQuery.id(R.id.txtCampSumFacility).getTextView().setText(FacilityName);
                }


                aQuery.id(R.id.txtCampSumDescription).getTextView().setText(campDetails.getTblCampDesc());

                dateTime = campDetails.getTblCampDateTime();
                String endTime = campDetails.getTblCampEndTime();
                index = dateTime.indexOf(" ");
                String startTime = dateTime.substring(index + 1);
                index = endTime.indexOf(" ");
                endTime = endTime.substring(index + 1);
                @SuppressLint("SimpleDateFormat") SimpleDateFormat _24sdf = new SimpleDateFormat("HH:mm");
                @SuppressLint("SimpleDateFormat") SimpleDateFormat _12sdf = new SimpleDateFormat("hh:mm a");
                Date eTime = _24sdf.parse(endTime);
                endTime = _12sdf.format(eTime);
                aQuery.id(R.id.txtCampSumTime).getTextView().setText(date + ", " + startTime + " - " + endTime);

                @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf12 = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
                @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
                Date d1 = sdf12.parse(campDetails.getTblCampDateTime());
                Date d2 = sdf.parse(campDetails.getTblCampEndTime());
                long diffinTime = d2.getTime() - d1.getTime();
                long Minutes = (diffinTime / (1000 * 60)) % 60;
                long Hours = (diffinTime / (1000 * 60 * 60)) % 24;
                long days = (diffinTime / (1000 * 60 * 60 * 24)) % 365;
                long years = (diffinTime / (1000l * 60 * 60 * 24 * 365));
                String duration = "";
                if (years != 0) {
                    if (years == 1) {
                        duration = duration + years + " " + getResources().getString(R.string.year) + " ";
                    } else {
                        duration = duration + years + " " + getResources().getString(R.string.yearstxt) + " ";
                    }
                }
                if (days != 0) {
                    if (days == 1) {
                        duration = duration + days + " " + getResources().getString(R.string.day) + " ";
                    } else {
                        duration = duration + days + " " + getResources().getString(R.string.days) + " ";
                    }
                }
                if (Hours != 0) {
                    duration = duration + Hours + " " + getResources().getString(R.string.hours) + " ";
                }
                    if (Minutes == 1 || Minutes ==0) {
                        duration = duration + Minutes + " " + getResources().getString(R.string.minute) + " ";
                    } else {
                        duration = duration + Minutes + " " + getResources().getString(R.string.minutes) + " ";
                    }
                aQuery.id(R.id.txtCampSumDuration).getTextView().setText(duration); ///duration


                //ramesh 16-05-2021
                int pre = Integer.parseInt(campDetails.getTblCampPresent());
                int abe = Integer.parseInt(campDetails.getTblCampAbsent());
                int totalAdolCount = pre + abe;
                aQuery.id(R.id.txtCampSumTotalAdols).getTextView().setText(String.valueOf(totalAdolCount));

                aQuery.id(R.id.txtCampSumPresent).getTextView().setText(campDetails.getTblCampPresent());
                aQuery.id(R.id.txtCampSumAbsent).getTextView().setText(campDetails.getTblCampAbsent());
                aQuery.id(R.id.txtCampSumSummary).getTextView().setText(campDetails.getTblCampSummary());
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private void getData() {
        try {
            CampaignRepository campaignRepository = new CampaignRepository(databaseHelper);
            campDetails = campaignRepository.getCampDetailsbyID(campID);
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private void Initialize() {
        try {
            databaseHelper = getHelper();
            aQuery = new AQuery(this);

            aQuery.id(R.id.toolbartxtTitle).getTextView().setText(getResources().getString(R.string.trainingsummary));

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        b.putParcelable("syncState", syncState);
        return b;
    }

    @Override
    public void onBackPressed() {
        displayDialogExit();
    }

    private String[] getTrainingTopics(int i) {
        try {
            String[] topic = new String[0];
            if (i == 1) {
                topic = getResources().getStringArray(R.array.traininggrowthinadolescent);
            } else if (i == 2) {
                topic = getResources().getStringArray(R.array.trainingnutritionandanemia);
            } else if (i == 3) {
                topic = getResources().getStringArray(R.array.trainingmenstruationandreproductive);
            } else if (i == 4) {
                topic = getResources().getStringArray(R.array.trainingmalereproductivesystem);
            } else if (i == 5) {
                topic = getResources().getStringArray(R.array.trainingresponsiblerelatioinshipandbehavior);
            } else if (i == 6) {
                topic = getResources().getStringArray(R.array.trainingresponsiblerelatioinshipandbehavioryears);
            } else if (i == 7) {
                topic = getResources().getStringArray(R.array.trainingharassmentandabuse);
            } else if (i == 8) {
                topic = getResources().getStringArray(R.array.traininginfectionofreproductive);
            } else if (i == 9) {
                topic = getResources().getStringArray(R.array.trainingmarriageandfamily);
            } else if (i == 10) {
                topic = getResources().getStringArray(R.array.traininggivingthechildrenaheadstart);
            } else if (i == 11) {
                topic = getResources().getStringArray(R.array.trainingcontraceptions);
            }
            return topic;
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }

        return new String[0];
    }

    public static int getKey(Map<String, Integer> mapref, String value) {

        int pos = 0;
        for (Map.Entry<String, Integer> map : mapref.entrySet()) {
            pos++;
            if (map.getValue().toString().equals(value)) {
                return pos;
            }

        }
        return pos;
    }
}