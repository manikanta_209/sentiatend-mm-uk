//20May2021 Arpitha - list has been changed

package com.sc.stmansi.adolescent.training;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidquery.AQuery;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.ListofAdols.ClickListener;
import com.sc.stmansi.ListofAdols.RecyclerViewTouchListener;
import com.sc.stmansi.R;
import com.sc.stmansi.adolescent.AdolStartTraining;
import com.sc.stmansi.common.AlertDialogUtil;
import com.sc.stmansi.common.CommonDatePickerFragmentSupport;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.configuration.SyncState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.recyclerview.RecyclerViewUpdateListener;
import com.sc.stmansi.repositories.AdolescentRepository;
import com.sc.stmansi.repositories.CampaignRepository;
import com.sc.stmansi.repositories.FacilityRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblCampDetails;
import com.sc.stmansi.womanlist.QueryParams;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class ListofTrainingFragment extends Fragment implements View.OnTouchListener,
        DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener, RecyclerViewUpdateListener {
    private View view;
    private AppState appState;
    private DatabaseHelper databaseHelper;
    private SyncState syncState;
    private AQuery aQuery;
    private FloatingActionButton floatingBtnCrtCampaign;
    private List<tblCampDetails> allCampaigns;
    private RecyclerView recyclerView;
    private String userName;
    private String campID;
    private SearchView searchView;
    private ListofCampRecyclerAdapter listofCampRecyclerAdapter;
    //    19May2021 Arpitha
    private LinearLayoutManager linearLayoutManager;
    private List<tblCampDetails> filteredRowItems;
    private boolean isUpdating;
    private int nextItemPosition;
    private long womenCount;
    AdolescentRepository adolescentRepository;
    TextView allCountView;
    private Map<String, Integer> villageCodeForName;
    TextView noDataLabelView;
    private boolean spinnerSelectionChanged = false;
    List<String> years;
    private static SearchableSpinner spnTrainingName;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_listof_trainings, container, false);
        //    19May2021 Arpitha
        Bundle bundle = getActivity().getIntent().getParcelableExtra("globalState");
        if (bundle != null) {
            appState = bundle.getParcelable("appState");
            syncState = bundle.getParcelable("syncState");
        } else {
            appState = new AppState();
        }
        allCountView = view.findViewById(R.id.txtTrainingCount);//19May2021 Arpitha
        noDataLabelView = view.findViewById(R.id.txt_listofCampNoRecords);//19May2021 Arpitha
        return view;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
            Initialize();

            getAshaData();

            getUIChanges();

            intialView();//19May2021 Arpitha

//            setData();
//            getData();
//            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//                @Override
//                public boolean onQueryTextSubmit(String query) {
//                    return false;
//                }
//
//                @SuppressLint("SetTextI18n")
//                @Override
//                public boolean onQueryTextChange(String newText) {
//                    if (newText.length() != 0) {
//                        aQuery.id(R.id.imgresetfilter).getImageView().setImageResource(R.drawable.ic_reset);
//                        if (listofCampRecyclerAdapter != null) {
//                            if (TextUtils.isEmpty(newText)) {
//                                listofCampRecyclerAdapter.filter("");
//                            } else {
//                                listofCampRecyclerAdapter.filter(newText);
//                            }
//                            aQuery.id(R.id.txtTrainingCount).getTextView().setText(getResources().getString(R.string.reccount) + listofCampRecyclerAdapter.getItemCount());
//                        }
//                    } else {
//                        aQuery.id(R.id.imgresetfilter).getImageView().setImageResource(R.drawable.ic_reset_gray);
//                        getData();
//                        setData();
//                    }
//                    return true;
//                }
//            });

            spnTrainingName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    updateList(appState.selectedVillageTitle, villageCodeForName, "", false);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            aQuery.id(R.id.imgresetfilter).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        aQuery.id(R.id.imgresetfilter).getImageView().setImageResource(R.drawable.ic_reset_gray);
                        spnTrainingName.setSelection(0);
                        aQuery.id(R.id.toolbarSpnFacilities).getSpinner().setSelection(0);
                        aQuery.id(R.id.spnyear).getSpinner().setSelection(0);
                        aQuery.id(R.id.spnmonth).getSpinner().setSelection(0);
                        updateList(appState.selectedVillageTitle, villageCodeForName, "", false);
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());

                    }
                }
            });

            aQuery.id(R.id.toolbarSpnFacilities).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        appState.selectedVillageTitle = (String) parent.getItemAtPosition(position);//21May2021 Arpitha

                        if (spinnerSelectionChanged) {
                            updateList(appState.selectedVillageTitle, villageCodeForName, "", false);
                        }
                        spinnerSelectionChanged = true;

                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());

                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            aQuery.id(R.id.spnyear).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        if (spinnerSelectionChanged) {
                            updateList(appState.selectedVillageTitle, villageCodeForName, "", false);
                        }
                        spinnerSelectionChanged = true;
                        setData();
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());

                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            aQuery.id(R.id.spnmonth).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        if (position > 0 && aQuery.id(R.id.spnyear).getSelectedItemPosition() <= 0) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.selectyear), Toast.LENGTH_LONG).show();
                        } else {
                            if (spinnerSelectionChanged) {
                                updateList(appState.selectedVillageTitle, villageCodeForName, "", false);
                            }
                            spinnerSelectionChanged = true;
                        }
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());

                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            recyclerView.addOnItemTouchListener(new RecyclerViewTouchListener(view.getContext(), recyclerView, new ClickListener() {
                @Override
                public void onLongClick(View child, int childAdapterPosition) {

                }

                @Override
                public void onClick(View child, int position) {
                    Intent intent;
                    if (filteredRowItems.get(position).getTblCampisActive() == 1) {
                        intent = new Intent(view.getContext(), TrainingCreation.class);
                    } else {
                        intent = new Intent(view.getContext(), TrainingSummary.class);
                    }
                    intent.putExtra("globalState", prepareBundle());
                    intent.putExtra("campID", filteredRowItems.get(position).getTblCampID());
                    startActivity(intent);
                }

                @Override
                public void onClick(View v) {

                }

                @Override
                public boolean onLongClick(View v) {
                    return false;
                }
            }));

            aQuery.id(R.id.toolbarBtnHome).getButton().setVisibility(View.VISIBLE);
            aQuery.id(R.id.toolbarBtnHome).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    displayDialogExit();
                }
            });

            floatingBtnCrtCampaign.setOnClickListener(v -> {
                try {
                    if (checkCampaignActive() && checkAdolescent()) {

                        Intent intent = new Intent(view.getContext(), AdolStartTraining.class);
                        intent.putExtra("globalState", prepareBundle());
                        intent.putExtra("campID", campID);
                        startActivity(intent);
                    } else {
                        if (checkAdolescent()) {
                            Intent intent = new Intent(view.getContext(), TrainingCreation.class);
                            intent.putExtra("globalState", prepareBundle());
                            intent.putExtra("campID", campID);
                            startActivity(intent);
                        } else {
                            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.no_adolescent_registered), getActivity());
                        }
                    }
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                    crashlytics.log(e.getMessage());

                }
            });

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private void displayDialogExit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
        builder.setMessage(getResources().getString(R.string.exit));
        builder.setCancelable(false);
        builder.setNegativeButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(view.getContext(), MainMenuActivity.class);
                intent.putExtra("globalState", prepareBundle());
                startActivity(intent);
            }
        }).setPositiveButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    private String _12hoursformate(String currentTime) {
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
            final Date dateObj = sdf.parse(currentTime);

            String time = new SimpleDateFormat("hh:mm a").format(dateObj);
            return time;
        } catch (final ParseException e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

            return "";
        }
    }

    private boolean checkAdolescent() {
        try {
            adolescentRepository = new AdolescentRepository(databaseHelper);
            if (adolescentRepository.getallAdols() != null && adolescentRepository.getallAdols().size() != 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return false;
    }

    private void Initialize() {
        try {
            databaseHelper = getHelper();
            aQuery = new AQuery(view);
            floatingBtnCrtCampaign = view.findViewById(R.id.floatingbuttonCreateCamp);

            aQuery.id(R.id.toolbartxtTitle).getTextView().setText(getResources().getString(R.string.list_of_campaign));

            recyclerView = view.findViewById(R.id.rv_listofCamp);
            searchView = view.findViewById(R.id.svListofCamp);
            aQuery.id(R.id.lltoolbarSpnFacilities).getView().setVisibility(View.VISIBLE);
//            aQuery.id(R.id.toolbarSpnFacilities).getSpinner().setVisibility(View.VISIBLE);
            spnTrainingName = view.findViewById(R.id.spnCampTrainingName);
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(view.getContext(), DatabaseHelper.class);
        }
        return databaseHelper;
    }

    private void getAshaData() {
        try {
            UserRepository userRepository = new UserRepository(databaseHelper);
            TblInstusers user = userRepository.getOneAuditedUser(appState.ashaId);
            userName = user.getUserName();


        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private void getData() {
        try {
            CampaignRepository campaignRepository = new CampaignRepository(databaseHelper);
            allCampaigns = campaignRepository.getAllCampaign();

            FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
            Map<String, Integer> facilities = facilityRepository.getPlaceMap();
            ArrayList<String> villageList = new ArrayList<String>();
            villageList.add(getResources().getString(R.string.all));
            for (Map.Entry<String, Integer> village : facilities.entrySet())
                villageList.add(village.getKey());
            ArrayAdapter<String> VillageAdapter = new ArrayAdapter<>(view.getContext(),
                    R.layout.simple_spinner_dropdown_item_white, villageList);
            VillageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            aQuery.id(R.id.toolbarSpnFacilities).adapter(VillageAdapter);
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private String gettwoDigit(String valueOf) {
        int value = Integer.parseInt(valueOf);
        if (value > 9) {
            return valueOf;
        } else {
            return "0" + valueOf;
        }
    }

    @SuppressLint("RestrictedApi")
    private void getUIChanges() {
        try {
            CampaignRepository campaignRepository = new CampaignRepository(databaseHelper);
            List<tblCampDetails> list = campaignRepository.getAllCampaign();
            if (list.size() != 0) {
                int isActive = list.get(0).getTblCampisActive();
                if (isActive == 0) {
                    floatingBtnCrtCampaign.setVisibility(View.VISIBLE);
                } else {
                    campID = list.get(0).getTblCampID();
                    floatingBtnCrtCampaign.setVisibility(View.GONE);
                }
            } else {
                floatingBtnCrtCampaign.setVisibility(View.VISIBLE);
            }

            String[] trainingName = getResources().getStringArray(R.array.adoltrainingname);
            ArrayAdapter<String> trainingNameAdapter = new ArrayAdapter<String>(view.getContext(), R.layout.simple_spinner_dropdown_item, trainingName);
            spnTrainingName.setAdapter(trainingNameAdapter);
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private boolean checkCampaignActive() {
        try {
            CampaignRepository campaignRepository = new CampaignRepository(databaseHelper);
            List<tblCampDetails> list = campaignRepository.getAllCampaign();
            if (list.size() != 0) {
                int isActive = list.get(list.size() - 1).getTblCampisActive();
                if (isActive == 0) {
                    return true;
                } else {
                    campID = list.get(list.size() - 1).getTblCampID();
                    return false;
                }
            } else {
                return true;
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return false;
    }

    @SuppressLint("SetTextI18n")
    private void setData() {
        try {
            if (allCampaigns != null && allCampaigns.size() != 0) { //22May2021 Bindu check null
                recyclerView.setHasFixedSize(true);
                aQuery.id(R.id.txt_listofCampNoRecords).getTextView().setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
                listofCampRecyclerAdapter = new ListofCampRecyclerAdapter(allCampaigns, view.getContext());
                recyclerView.setAdapter(listofCampRecyclerAdapter);
                aQuery.id(R.id.txtTrainingCount).getTextView().setText(view.getResources().getString(R.string.reccount) + listofCampRecyclerAdapter.getItemCount());
            } else {
                aQuery.id(R.id.txt_listofCampNoRecords).getTextView().setVisibility(View.VISIBLE);
                aQuery.id(R.id.txtTrainingCount).getTextView().setText(getResources().getString(R.string.reccount) + 0);
                recyclerView.setVisibility(View.GONE);
            }

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
        try {
            if (datePicker.isShown()) {
                String strseldate = null;
                strseldate = String.format("%02d", dayOfMonth) + "-" + String.format("%02d", month + 1) + "-"
                        + year;

                @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                Date seldate = format.parse(strseldate);
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (MotionEvent.ACTION_UP == event.getAction()) {
            try {
                switch (v.getId()) {
                    case R.id.etCampDate:

                        break;
                    case R.id.etStartTime:
//                        CommonTimePickerFragment.getCommonTime(getActivity(),0,0);
                }
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());

            }
        }
        return false;
    }

    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        b.putParcelable("syncState", syncState);
        return b;
    }

    private void assignEditTextAndCallDatePicker(EditText editText) {
        try {
            String currDate = (editText.getText().length() > 0) ? editText.getText().toString() : "";
            CommonDatePickerFragmentSupport.getCommonDate(getActivity(), this, currDate);
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onTimeSet(TimePicker timePicker, int hour, int min) {
        try {
            if (hour == 0 && min == 0) {
//                etCampCrtStartTime.setText(DateTimeUtil.getCurrentTime());
            } else {
//                etCampCrtStartTime.setText(hour + ":" + min);
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }


    //    19May2021 Arpitha
//    recycler view scroll
    public class OnRecyclerScrollListener extends RecyclerView.OnScrollListener {
        int ydy = 0;

        @Override
        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            // 30April2021 Guru
            if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                int lastCompletelyVisibleItemPosition = linearLayoutManager.findLastCompletelyVisibleItemPosition();
                boolean shouldPullUpRefresh = lastCompletelyVisibleItemPosition == filteredRowItems.size() - 1
                        && recyclerView.getScrollState() == RecyclerView.SCROLL_STATE_DRAGGING;
                if (shouldPullUpRefresh) {
                    QueryParams queryParams = new QueryParams();
                    queryParams.nextItemPosition = nextItemPosition;
                    queryParams.selectedVillageCode = appState.selectedVillageCode;
//                queryParams.textViewFilter = wFilterStr;
//                queryParams.isLMPNotConfirmed = islmpconfirm; //02MAy2021 Bindu set isLMPconfirmation
                    if (!isUpdating) {
                        isUpdating = true;
                        new LoadPregnantList(ListofTrainingFragment.this, queryParams, false).execute();
                    }
                }
            } // 30April2021 Guru
        }

        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            ydy = dy;

            int lastCompletelyVisibleItemPosition = linearLayoutManager.findLastCompletelyVisibleItemPosition();
            boolean shouldPullUpRefresh = lastCompletelyVisibleItemPosition == filteredRowItems.size() - 1
                    && recyclerView.getScrollState() == RecyclerView.SCROLL_STATE_DRAGGING;
            if (shouldPullUpRefresh) {
                QueryParams queryParams = new QueryParams();
                queryParams.nextItemPosition = nextItemPosition;
                queryParams.selectedVillageCode = appState.selectedVillageCode;
//            queryParams.textViewFilter = wFilterStr;
//            queryParams.isLMPNotConfirmed = islmpconfirm; //02MAy2021 Bindu - set isLMPconfirmation
                if (!isUpdating) {
                    isUpdating = true;
                    new LoadPregnantList(ListofTrainingFragment.this, queryParams, false).execute();
                }
            }
        }
    }

    //    load data in a list - 19May2021 Arpitha
    static class LoadPregnantList extends AsyncTask<Void, Void, Void> {

        private ListofTrainingFragment viewsUpdateCallback;
        private QueryParams queryParams;
        private boolean firstLoad;
        private Map<String, Integer> villageCodeForName;

        LoadPregnantList(ListofTrainingFragment viewsUpdateCallback, QueryParams queryParams, boolean firstLoad, Map<String, Integer> villageCodeForName) {
            this.viewsUpdateCallback = viewsUpdateCallback;
            this.queryParams = queryParams;
            this.firstLoad = firstLoad;
            this.villageCodeForName = villageCodeForName;
        }

        LoadPregnantList(ListofTrainingFragment viewsUpdateCallback, QueryParams queryParams, boolean firstLoad) {
            this.viewsUpdateCallback = viewsUpdateCallback;
            this.queryParams = queryParams;
            this.firstLoad = firstLoad;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            int selectedVillageCode;
            // This means second constructor was used (called by scrollListener)
            if (villageCodeForName == null) selectedVillageCode = queryParams.selectedVillageCode;
            else {
                String selectedVillageTitle = queryParams.selectedVillageTitle;
                if (selectedVillageTitle == null)
                    selectedVillageCode = 0;
                else {
                    Integer code = villageCodeForName.get(selectedVillageTitle);
                    selectedVillageCode = (code == null) ? 0 : code;
                }
            }
//            viewsUpdateCallback.appState.selectedVillageCode = selectedVillageCode;
            viewsUpdateCallback.appState.selectedVillageCode = queryParams.selectedVillageCode;

            try {
                String month, year;
                month = String.valueOf(viewsUpdateCallback.aQuery.id(R.id.spnmonth).getSpinner().getSelectedItemPosition());
                year = viewsUpdateCallback.aQuery.id(R.id.spnyear).getSpinner().getSelectedItem().toString();
                viewsUpdateCallback.womenCount = viewsUpdateCallback.getCampaignsCount
                        (year, viewsUpdateCallback.gettwoDigit(month), viewsUpdateCallback.appState.selectedVillageCode,
                                new CampaignRepository(viewsUpdateCallback.databaseHelper),spnTrainingName.getSelectedItemPosition()); // 30April2021 Guru

                List<tblCampDetails> rows = viewsUpdateCallback.prepareRowItemsForDisplay
                        (viewsUpdateCallback.appState.selectedVillageCode, queryParams.nextItemPosition);
                if (firstLoad) {
                    int prevSize = viewsUpdateCallback.filteredRowItems.size();
                    viewsUpdateCallback.filteredRowItems.clear();
                    viewsUpdateCallback.listofCampRecyclerAdapter.notifyItemRangeRemoved(0, prevSize);
                    firstLoad = false;
                }
                if (rows.size() > 0) {
                    viewsUpdateCallback.filteredRowItems.addAll(rows);

                    viewsUpdateCallback.nextItemPosition = viewsUpdateCallback.filteredRowItems.size();
                    viewsUpdateCallback.listofCampRecyclerAdapter.notifyItemRangeInserted(firstLoad ? 0 : viewsUpdateCallback.nextItemPosition,
                            viewsUpdateCallback.filteredRowItems.size());
                }
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            try {
                viewsUpdateCallback.updateLabels();
                // 30April2021 Guru
                if (viewsUpdateCallback.filteredRowItems.size() > 0) {
                    viewsUpdateCallback.recyclerView.setVisibility(View.VISIBLE);
                    if (!firstLoad) {
                        viewsUpdateCallback.linearLayoutManager.scrollToPosition(viewsUpdateCallback.
                                listofCampRecyclerAdapter.getItemCount());
                    }
                    viewsUpdateCallback.isUpdating = false;
                } // 30April2021 Guru
                viewsUpdateCallback.allCountView.setText(viewsUpdateCallback.getResources().
                        getString(R.string.recordcount,
                                viewsUpdateCallback.womenCount)); // 30April2021 Guru
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
        }
    }


    //    19May2021 Arpitha
    @Override
    public void updateList(String villageTitle, Map<String, Integer> villageNameForCode, String textViewFilter, boolean checked) {

        QueryParams params = new QueryParams();
        params.textViewFilter = "";
        params.selectedVillageTitle = villageTitle;
        params.nextItemPosition = 0;
        if (villageTitle != null && !(villageTitle.equalsIgnoreCase(getResources().getString(R.string.all))))
            params.selectedVillageCode = villageNameForCode.get(villageTitle);
        new LoadPregnantList(this, params, true).execute();
        isUpdating = true;

        recyclerView.addOnScrollListener(new OnRecyclerScrollListener());
    }

    // 19May2021 Arpitha
    private List<tblCampDetails> prepareRowItemsForDisplay(int villageCode,
                                                           int limitStart) throws Exception {

        CampaignRepository campaignRepository = new CampaignRepository(databaseHelper);
        String month, year;
        month = String.valueOf(aQuery.id(R.id.spnmonth).getSpinner().getSelectedItemPosition());
        year = aQuery.id(R.id.spnyear).getSpinner().getSelectedItem().toString();
        return campaignRepository.getCampaigns(year,
                gettwoDigit(month), limitStart, villageCode, spnTrainingName.getSelectedItemPosition()
        );
    }


    //    update labels based on data -19May2021 Arpitha
    private void updateLabels() {
        if (filteredRowItems.size() == 0) {
            recyclerView.setVisibility(View.GONE);

           /* noDataLabelView.setText(getResources().getString(R.string.no_data));
            noDataLabelView.setVisibility(View.VISIBLE);*/
            noDataLabelView.setVisibility(View.VISIBLE);
            return;
        } else {
//            noDataLabelView.setVisibility(View.GONE);
//            aq.id(R.id.txt_listofDeactiveAdolsNoRecords).visible();
            noDataLabelView.setVisibility(View.GONE);

        }
    }

    //    19May2021 Arpitha
    void intialView() throws Exception {
        populateSpinnerVillage();
        getyears();

        // 30April2021 Guru
        String month, year;
        month = String.valueOf(aQuery.id(R.id.spnmonth).getSpinner().getSelectedItemPosition());
        year = aQuery.id(R.id.spnyear).getSpinner().getSelectedItem().toString();

        womenCount = getCampaignsCount
                (year, gettwoDigit(month), 0,
                        new CampaignRepository(databaseHelper),spnTrainingName.getSelectedItemPosition());
        allCountView.setText(getResources().getString(R.string.recordcount, womenCount));

        recyclerView.setVisibility(View.VISIBLE);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        linearLayoutManager = (LinearLayoutManager) linearLayoutManager;

        filteredRowItems = new ArrayList<>();
        listofCampRecyclerAdapter = new ListofCampRecyclerAdapter(filteredRowItems, getActivity());
        recyclerView.setAdapter(listofCampRecyclerAdapter);

        QueryParams params = new QueryParams();
        params.textViewFilter = "";
        params.selectedVillageCode = 0;
        params.nextItemPosition = 0;
        new LoadPregnantList(this, params, true).execute();
        isUpdating = true;

        recyclerView.addOnScrollListener(new OnRecyclerScrollListener());

    }

    private void getyears() {
        try {
            CampaignRepository campaignRepository = new CampaignRepository(databaseHelper);
            years = campaignRepository.getYears();
            years.add(0, "All");

            ArrayAdapter adapter = new ArrayAdapter(getActivity(), R.layout.simple_spinner_item, years);
            aQuery.id(R.id.spnyear).getSpinner().setAdapter(adapter);
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    /**
     * populate Village to Spinner
     */
    private void populateSpinnerVillage() throws Exception {
        FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
        villageCodeForName = facilityRepository.getPlaceMap();

        ArrayAdapter<CharSequence> villageSpinnerAdapter = new ArrayAdapter<CharSequence>(getActivity(), R.layout.actionbar_spinner_list_item);
//        villageSpinnerAdapter.add("All Women");06May2021 Arpitha
        villageSpinnerAdapter.add(getResources().getString(R.string.all));//06May2021 Arpitha
        for (Map.Entry<String, Integer> village : villageCodeForName.entrySet()) {
            villageSpinnerAdapter.add(village.getKey());
        }
        villageSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        aQuery.id(R.id.toolbarSpnFacilities).adapter(villageSpinnerAdapter);
    }
}
