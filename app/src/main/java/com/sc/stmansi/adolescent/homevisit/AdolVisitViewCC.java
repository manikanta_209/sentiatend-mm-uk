package com.sc.stmansi.adolescent.homevisit;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.R;
import com.sc.stmansi.adolescent.AdolDeactivation;
import com.sc.stmansi.adolescent.AdolParticipatedTrainings;
import com.sc.stmansi.adolescent.AdolRegistration;
import com.sc.stmansi.adolescent.AdolVisitHistory;
import com.sc.stmansi.adolescent.AdolVisit_New;
import com.sc.stmansi.common.AlertDialogUtil;
import com.sc.stmansi.common.NavDrawerItem;
import com.sc.stmansi.common.NavDrawerListAdapter;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.configuration.SyncState;
import com.sc.stmansi.database.DBMethods;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.repositories.AdolescentRepository;
import com.sc.stmansi.repositories.CaseMgmtRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblAdolReg;
import com.sc.stmansi.tables.tblCaseMgmt;

import java.util.ArrayList;

public class AdolVisitViewCC extends AppCompatActivity {

    private AppState appState;
    private String AdolId;
    private String VisitCount;
    private SyncState syncState;
    private DatabaseHelper databaseHelper;
    private AQuery aq;
    private DrawerLayout drawer;
    private ListView drawerList;
    private tblAdolReg currentAdolDetails;
    private tblCaseMgmt currentCaseDetails;
    private TblInstusers user;
    private boolean fromNotification;
    private String userTypeHV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adol_visitview_cc);
        try {
            Bundle bundle = getIntent().getBundleExtra("globalState");
            appState = bundle.getParcelable("appState");
            syncState = bundle.getParcelable("syncState");
            AdolId = getIntent().getStringExtra("adolID");
            VisitCount = getIntent().getStringExtra("VisitCount");
            fromNotification = getIntent().getBooleanExtra("fromNotification", false); //ramesh 9-7-2021
            userTypeHV = getIntent().getStringExtra("userTypeHV");
            Initalize();
            InitializeDrawer();
            getSupportActionBar().hide();

            getData(VisitCount);

            UpdateUI();

            if (fromNotification) {
//                updateViewedDate();
            }

            aq.id(R.id.toolbarBtnHome).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        AlertDialog.Builder builder = new AlertDialog.Builder(AdolVisitViewCC.this);
                        builder.setMessage(getResources().getString(R.string.exit));
                        builder.setCancelable(false);
                        builder.setPositiveButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent(AdolVisitViewCC.this, AdolVisitHistory.class);
                                intent.putExtra("tabItem", 0);
                                intent.putExtra("adolID", AdolId);
                                intent.putExtra("globalState", prepareBundle());
                                startActivity(intent);
                            }
                        }).setNegativeButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).create().show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            aq.id(R.id.btnAVVNext).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        CaseMgmtRepository caseMgmtRepository = new CaseMgmtRepository(databaseHelper);

                        int caseCount = caseMgmtRepository.getLastVisitCount(AdolId, user.getUserId(), userTypeHV).getVisitNum();
                        int vCount = Integer.parseInt(VisitCount);
                        if (vCount < caseCount) {
                            //next page
                            getData(String.valueOf(vCount + 1));
                            UpdateUI();
//                            Intent intent = new Intent(AdolVisitViewCC.this, AdolVisitViewCC.class);
//                            intent.putExtra("globalState", prepareBundle());
//                            intent.putExtra("VisitCount", String.valueOf(vCount + 1));
//                            intent.putExtra("adolID", AdolId);
//                            intent.putExtra("userTypeHV",userTypeHV);
//                            startActivity(intent);
                        } else {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.norecords), Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            aq.id(R.id.btnAVVPrev).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        int vCount = Integer.parseInt(VisitCount);
                        if ((vCount - 1) != 0) {
                            getData(String.valueOf(vCount - 1));
                            UpdateUI();
//                            Intent intent = new Intent(AdolVisitViewCC.this, AdolVisitViewCC.class);
//                            intent.putExtra("globalState", prepareBundle());
//                            intent.putExtra("VisitCount", String.valueOf(vCount - 1));
//                            intent.putExtra("adolID", AdolId);
//                            intent.putExtra("userTypeHV",userTypeHV);
//                            startActivity(intent);
                        } else {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.norecords), Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (Exception e) {
            exception(e);
        }
    }

//    private void updateViewedDate() {
//        try{
//            String currentDate = DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime();;
//            CaseMgmtRepository caseMgmtRepository = new CaseMgmtRepository(databaseHelper);
//            String sql = " update tblCaseMgmt set RecordViewedDate = "+currentDate+" where autoId = "+currentCaseDetails.getAutoId();
//            if (caseMgmtRepository.updateViewDate(sql)>0){
//                Log.e("RecordViewedDate","Updated "+currentCaseDetails.getAutoId()+" "+currentDate);
//            }else{
//                Log.e("RecordViewedDate","Not Updated "+currentCaseDetails.getAutoId()+" "+currentDate);
//            }
//        }catch (Exception e){
//            FirebaseCrashlytics firebaseCrashlytics = FirebaseCrashlytics.getInstance();
//            firebaseCrashlytics.log(e.getMessage());
//        }
//    }

    @SuppressLint("SetTextI18n")
    private void UpdateUI() {
        try {
            resetUI();
            String dateTime = currentAdolDetails.getRecordCreatedDate();
            int index = dateTime.indexOf(" ");
            String date = dateTime.substring(0, index);
            aq.id(R.id.toolbartxtTitle).getTextView().setText(currentAdolDetails.getRegAdolName() + "\n" + date);
            aq.id(R.id.txt_AVV_Number).getTextView().setText(currentCaseDetails.getVisitNum() + "");
            aq.id(R.id.txt_AVV_VisitDate).getTextView().setText(getResources().getString(R.string.visitdatecolon) + currentCaseDetails.getVisitDate());
            aq.id(R.id.txt_AVV_Age).getTextView().setText(currentAdolDetails.getRegAdolAge() + getResources().getString(R.string.yearstxt));

            String complications = currentCaseDetails.getVisitBeneficiaryDangerSigns();
            if (complications.length() != 0) {
                String[] listofcomplications = complications.split("@");
                for (String s : listofcomplications) {
                    if (s.contains("Health")) {
                        aq.id(R.id.tr_AVVCC_HealthIssue).getView().setVisibility(View.VISIBLE);
                        String hit = getResources().getString(R.string.healthissuetrim);
                        String hi = s.replace(hit, "");
                        String[] hidata = hi.split("  ");
                        StringBuilder hiBuilder = new StringBuilder();
                        for (String tempword : hidata) {
                            String word = tempword.trim();
                            word = word.replace("Anemia", getResources().getString(R.string.anemia));
                            hiBuilder.append(word).append(", ");
                        }
                        aq.id(R.id.txt_AVVCC_HealthIssue).getTextView().setText(hiBuilder.deleteCharAt(hiBuilder.lastIndexOf(",")));
                    }
                    if (s.contains("hb")) {
                        aq.id(R.id.tr_AVVCC_HB).getView().setVisibility(View.VISIBLE);
                        String hbtemp = s.replace("hb is ", "");
                        aq.id(R.id.txt_AVVCC_HB).getTextView().setText(s.replace("hb is ", "") + " " + getResources().getString(R.string.gramsperliter) + "( " + getHGCategory(Float.parseFloat(hbtemp)) + ")");
                    }
                    if (s.contains("Menstrual")) {
                        aq.id(R.id.tr_AVVCC_Menstural).getView().setVisibility(View.VISIBLE);
                        String hi = s.trim().replace("Menstrualpbm ", "");
                        String[] hidata = hi.split("  ");
                        StringBuilder hiBuilder = new StringBuilder();
                        String[] mensuproblems = getResources().getStringArray(R.array.menustralproblems);
                        for (String word : hidata) {
                            try {
                                int intWord = Integer.parseInt(word.trim());
                                hiBuilder.append(mensuproblems[intWord]).append(", ");
                            } catch (Exception e) {
                                hiBuilder.append(word).append(",");
                            }
                        }
                        if (hiBuilder.toString().contains(",")) {
                            aq.id(R.id.txt_AVVCC_Menstural).getTextView().setText(hiBuilder.deleteCharAt(hiBuilder.lastIndexOf(",")));
                        } else {
                            aq.id(R.id.txt_AVVCC_Menstural).getTextView().setText(hiBuilder.toString());
                        }
                    }
                }
            } else {
                aq.id(R.id.ll_AVVCC_Complication).getView().setVisibility(View.GONE);
            }
            if (currentCaseDetails.getVisitReferredtoFacType().length() != 0) {
                aq.id(R.id.ll_AVVCC_Referred).getView().setVisibility(View.VISIBLE);
                aq.id(R.id.txt_AVVCC_IsReferred).getTextView().setText(getResources().getString(R.string.m121));
                aq.id(R.id.txt_AVVCC_ReferredSlipNum).getTextView().setText(currentCaseDetails.getVisitReferralSlipNumber());
                aq.id(R.id.txt_AVVCC_ReferredFacType).getTextView().setText(currentCaseDetails.getVisitReferredtoFacType());
                aq.id(R.id.txt_AVVCC_ReferredFacName).getTextView().setText(currentCaseDetails.getVisitReferredtoFacName());

            } else {
                aq.id(R.id.ll_AVVCC_Referred).getView().setVisibility(View.GONE);
                aq.id(R.id.txt_AVVCC_IsReferred).getTextView().setText(getResources().getString(R.string.m122));
                aq.id(R.id.txt_AVVCC_ReferredSlipNum).getTextView().setText("");
                aq.id(R.id.txt_AVVCC_ReferredFacType).getTextView().setText("");
                aq.id(R.id.txt_AVVCC_ReferredFacName).getTextView().setText("");
            }

            aq.id(R.id.txt_AVVCC_ReferredComments).getTextView().setText(currentCaseDetails.getHcmActTakComments());
            aq.id(R.id.txt_AVVCC_IsAdviseGiven).getTextView().setText(currentCaseDetails.getHcmAdviseGiven());


            aq.id(R.id.txt_AVVCC_FeedbacktoMM).getTextView().setText(currentCaseDetails.getHcmActionToBeTakenClient());
            aq.id(R.id.txt_AVVCC_InputtoMC).getTextView().setText(currentCaseDetails.getHcmInputToNextLevel());

        } catch (Exception e) {
            exception(e);
        }
    }

    private void getData(String visitCount) {
        try {
            VisitCount = visitCount;
            AdolescentRepository adolescentRepository = new AdolescentRepository(databaseHelper);
            currentAdolDetails = adolescentRepository.getAdolDatafromAdolID(AdolId);

            UserRepository userRepository = new UserRepository(databaseHelper);
            user = userRepository.getOneAuditedUser(appState.ashaId);

            CaseMgmtRepository caseMgmtRepository = new CaseMgmtRepository(databaseHelper);
//            currentCaseDetails = caseMgmtRepository.getCaseDetails(AdolId, VisitCount,user.getUserId(),getResources().getString(R.string.ccusertype));
            if (userTypeHV.equals("CC")) {
                currentCaseDetails = caseMgmtRepository.getCaseDetails(AdolId, visitCount, userTypeHV);
            } else if (userTypeHV.equals("MC")) {
                currentCaseDetails = caseMgmtRepository.getCaseDetails(AdolId, visitCount, userTypeHV);
            }
        } catch (Exception e) {
            exception(e);
        }
    }


    private void Initalize() {
        try {
            databaseHelper = getHelper();
            DBMethods dbMethods = new DBMethods(databaseHelper);
            aq = new AQuery(this);

        } catch (Exception e) {
            exception(e);
        }
    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(getApplicationContext(), DatabaseHelper.class);
        }
        return databaseHelper;
    }

    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        b.putParcelable("syncState", syncState);
        return b;
    }

    private void exception(Exception e) {
        FirebaseCrashlytics firebaseCrashlytics = FirebaseCrashlytics.getInstance();
        firebaseCrashlytics.log(e.getMessage());
    }

    private void InitializeDrawer() {
        try {
            drawer = findViewById(R.id.drawer_adolVisitView);
            drawerList = findViewById(R.id.lvNav_adolVisitView);

            ArrayList<NavDrawerItem> navDrawerItems = new ArrayList<>();
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.editadolescent), R.drawable.ic_edit_icon));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.homevisitadd), R.drawable.ic_homevisit));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.adolvisithistory), R.drawable.anm_pending_activities));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.trainingparticipated), R.drawable.presentation));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deactivate), R.drawable.deactivate));
            drawer.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
            drawerList.setAdapter(new NavDrawerListAdapter(getApplicationContext(), navDrawerItems));
            drawerList.setOnItemClickListener(new DrawerItemClickListener());
            Toolbar toolbar = findViewById(R.id.toolbarCommon);
            ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close);
            drawer.setDrawerListener(drawerToggle);
            drawerToggle.syncState();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class DrawerItemClickListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            drawer.closeDrawer(drawerList);
            try {
                switch (i) {
                    case 0:
                        Intent intent = new Intent(AdolVisitViewCC.this, AdolRegistration.class);
                        intent.putExtra("globalState", prepareBundle());
                        intent.putExtra("isEdit", true);
                        intent.putExtra("adolID", AdolId);
                        startActivity(intent);
                        break;
                    case 1:
                        if (currentAdolDetails.getRegAdolDeactivateDate().length() != 0) {
                            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.adolescentdeactivated), AdolVisitViewCC.this);
                            drawer.closeDrawer(GravityCompat.END);
                        } else {
                            intent = new Intent(AdolVisitViewCC.this, AdolVisit_New.class);
                            intent.putExtra("globalState", prepareBundle());
                            intent.putExtra("adolID", AdolId);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                        break;
                    case 2:
                        intent = new Intent(AdolVisitViewCC.this, AdolVisitHistory.class);
                        intent.putExtra("globalState", prepareBundle());
                        intent.putExtra("adolID", AdolId);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        break;
                    case 3:
                        intent = new Intent(AdolVisitViewCC.this, AdolParticipatedTrainings.class);
                        intent.putExtra("globalState", prepareBundle());
                        intent.putExtra("adolID", AdolId);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        break;
                    case 4:
                        intent = new Intent(AdolVisitViewCC.this, AdolDeactivation.class);
                        intent.putExtra("globalState", prepareBundle());
                        if (currentAdolDetails.getRegAdolDeactivateDate().length() != 0) {
                            intent.putExtra("isView", true);
                        }
                        intent.putExtra("adolID", AdolId);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(AdolVisitViewCC.this);
        builder.setMessage(getResources().getString(R.string.exit));
        builder.setCancelable(false);
        builder.setPositiveButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(AdolVisitViewCC.this, AdolVisitHistory.class);
                intent.putExtra("tabItem", 0);
                intent.putExtra("adolID", AdolId);
                intent.putExtra("globalState", prepareBundle());
                startActivity(intent);
            }
        }).setNegativeButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    private String getHGCategory(float hbCount) {
        try {
//            float hbCount = Float.parseFloat(aqAdolVisit.id(R.id.etAdolVisitHbCount).getEditText().getText().toString());
            if (hbCount < 8.0) {
                //severe
                return getResources().getString(R.string.severe);
            } else if (hbCount >= 8 && hbCount <= 9.9) {
                //moderate
                return getResources().getString(R.string.moderate);
            } else if (hbCount >= 10 && hbCount <= 11.9) {
                //mild
                return getResources().getString(R.string.mild);
            } else if (hbCount >= 11.9 && hbCount <= 24) {
                //normal
                return getResources().getString(R.string.normal);
            } else if (hbCount > 24) {
                return "NA";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private void resetUI() {
        try {
            aq.id(R.id.tr_AVVCC_Menstural).getView().setVisibility(View.GONE);
            aq.id(R.id.tr_AVVCC_HealthIssue).getView().setVisibility(View.GONE);
            aq.id(R.id.tr_AVVCC_HB).getView().setVisibility(View.GONE);
            aq.id(R.id.txt_AVVCC_HealthIssue).getTextView().setText("");
            aq.id(R.id.txt_AVVCC_HB).getTextView().setText("");
            aq.id(R.id.txt_AVVCC_Menstural).getTextView().setText("");
            aq.id(R.id.txt_AVVCC_ReferredSlipNum).getTextView().setText("");
            aq.id(R.id.txt_AVVCC_ReferredFacType).getTextView().setText("");
            aq.id(R.id.txt_AVVCC_ReferredFacName).getTextView().setText("");
            aq.id(R.id.txt_AVVCC_ReferredComments).getTextView().setText("");
            aq.id(R.id.txt_AVVCC_IsAdviseGiven).getTextView().setText("");
            aq.id(R.id.txt_AVVCC_FeedbacktoMM).getTextView().setText("");
            aq.id(R.id.txt_AVVCC_InputtoMC).getTextView().setText("");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}