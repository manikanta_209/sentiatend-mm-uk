package com.sc.stmansi.adolescent.training;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;


import com.sc.stmansi.R;
import com.sc.stmansi.tables.tblAdolAttendanceSavings;
import com.sc.stmansi.tables.tblAdolReg;

import java.util.List;

public class CampaignAttendanceAdapter extends RecyclerView.Adapter<CampaignAttendanceViewHolder> {
    private final List<tblAdolReg> allAdols;
    private final Context context;
    private final List<tblAdolAttendanceSavings> adolsAttendanceList;
    private final Context act;

    public CampaignAttendanceAdapter(Context applicationContext, List<tblAdolReg> allAdols, List<tblAdolAttendanceSavings> adolsAttendanceList, Context context) {
        this.context = applicationContext;
        this.allAdols = allAdols;
        this.adolsAttendanceList = adolsAttendanceList;
        act = context;
    }

    @NonNull
    @Override
    public CampaignAttendanceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_trainingattendance, parent, false);
        return new CampaignAttendanceViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull CampaignAttendanceViewHolder holder, int position) {
        try{
            holder.textViewName.setText(allAdols.get(position).getRegAdolName());
            holder.textViewAge.setText(""+allAdols.get(position).getRegAdolAge());
            holder.textViewGTS.setText((allAdols.get(position).getRegAdolGoingtoSchool().equalsIgnoreCase("No") ?act.getResources().getString(R.string.adolno):act.getResources().getString(R.string.m121)));
            holder.imgPresent.setOnClickListener(v -> {
                if (holder.imgPresent.getVisibility()== View.VISIBLE){
                    holder.imgPresent.setVisibility(View.GONE);
                    holder.imgAbsent.setVisibility(View.VISIBLE);
                    adolsAttendanceList.get(position).setTblAttSavAttendance("Absent");
                }
            });
            holder.imgAbsent.setOnClickListener(v -> {
                if (holder.imgAbsent.getVisibility()== View.VISIBLE){
                    holder.imgAbsent.setVisibility(View.GONE);
                    holder.imgPresent.setVisibility(View.VISIBLE);
                    adolsAttendanceList.get(position).setTblAttSavAttendance("Present");
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return allAdols.size();
    }

    public List<tblAdolAttendanceSavings> getAttendanceList() {
        try {
            return adolsAttendanceList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
