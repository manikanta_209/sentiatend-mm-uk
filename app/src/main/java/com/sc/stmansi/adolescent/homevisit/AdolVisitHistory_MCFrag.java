package com.sc.stmansi.adolescent.homevisit;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidquery.AQuery;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.R;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.configuration.SyncState;
import com.sc.stmansi.database.DBMethods;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.repositories.AdolescentRepository;
import com.sc.stmansi.repositories.CaseMgmtRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblAdolReg;
import com.sc.stmansi.tables.tblCaseMgmt;

import java.util.List;

public class AdolVisitHistory_MCFrag extends Fragment {
    private View parentview;
    private DatabaseHelper databaseHelper;
    private AQuery aq;
    private AppState appState;
    private Bundle bundle;
    private SyncState syncState;
    private String adolId;
    private tblAdolReg currentAdolDetails;
    private RecyclerView recyclerview;
    private List<tblCaseMgmt> currentcaseDetails;
    private TblInstusers user;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        parentview =  inflater.inflate(R.layout.adolhv_mcfragment, container, false);
        bundle = this.getArguments();
        if (bundle != null) {
            appState = bundle.getParcelable("appState");
            syncState = bundle.getParcelable("syncState");
            adolId = bundle.getString("adolID");
        } else {
            appState = new AppState();
        }
        return parentview;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Initalize();
        getData();

        UpdateUI();
    }

    private void Initalize() {
        databaseHelper = getHelper();
        DBMethods dbMethods = new DBMethods(databaseHelper);
        aq = new AQuery(parentview);
        recyclerview = parentview.findViewById(R.id.rvAdolVisitHistory_MC);
    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(parentview.getContext(), DatabaseHelper.class);
        }
        return databaseHelper;
    }

    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        b.putParcelable("syncState", syncState);
        return b;
    }

    private void getData() {
        try {
            AdolescentRepository adolescentRepository = new AdolescentRepository(databaseHelper);
            currentAdolDetails = adolescentRepository.getAdolDatafromAdolID(adolId);

            UserRepository userRepository = new UserRepository(databaseHelper);
            user = userRepository.getOneAuditedUser(appState.ashaId);

            CaseMgmtRepository caseMgmtRepository = new CaseMgmtRepository(databaseHelper);
//            currentAdolHeaders = adolVisitHeaderRepository.getAllVisits(adolId,getActivity().getResources().getString(R.string.userlevel_cc));
            currentcaseDetails = caseMgmtRepository.getAllCasesForMC(adolId,parentview.getResources().getString(R.string.mcusertype));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("SetTextI18n")
    private void UpdateUI() {
        try {
            if (currentcaseDetails.size()!=0){
                recyclerview.setHasFixedSize(true);
                aq.id(R.id.txt_AVH_NoRecords_MC).gone();
                recyclerview.setVisibility(View.VISIBLE);
                HomeVisitHistoryMCAdapter ccAdapter = new HomeVisitHistoryMCAdapter(currentAdolDetails, currentcaseDetails,parentview.getContext(),prepareBundle(),databaseHelper);
                recyclerview.setLayoutManager(new LinearLayoutManager(parentview.getContext()));
                recyclerview.setAdapter(ccAdapter);
                aq.id(R.id.txtAdolVisitHistoryCount_MC).getTextView().setText(parentview.getResources().getString(R.string.reccount) + ccAdapter.getItemCount());
            }else {
                aq.id(R.id.txt_AVH_NoRecords_MC).visible();
                recyclerview.setVisibility(View.GONE);aq.id(R.id.txtAdolVisitHistoryCount_MC).getTextView().setText(getActivity().getResources().getString(R.string.reccount) + 0);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
