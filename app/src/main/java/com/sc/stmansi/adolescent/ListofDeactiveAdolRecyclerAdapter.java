package com.sc.stmansi.adolescent;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.google.firebase.crashlytics.FirebaseCrashlytics;

import com.sc.stmansi.R;
import com.sc.stmansi.tables.tblAdolReg;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ListofDeactiveAdolRecyclerAdapter extends RecyclerView.Adapter<ListofDeactiveAdolViewHolder> {

    private final Context act;
    private final List<tblAdolReg> listofDeactiveAdols;
    private final View view;
    private final ArrayList<tblAdolReg> modellist;

    public ListofDeactiveAdolRecyclerAdapter(List<tblAdolReg> listofDeactiveAdols, Context context, View view) {
        this.listofDeactiveAdols = listofDeactiveAdols;
        modellist = new ArrayList<>();
        modellist.addAll(listofDeactiveAdols);
        act = context;
        this.view = view;
    }

    @NonNull
    @Override
    public ListofDeactiveAdolViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CardView v = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_listofdeactiveadolitem, parent, false);
        return new ListofDeactiveAdolViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ListofDeactiveAdolViewHolder holder, int position) {
        try {
            holder.txtName.setText(listofDeactiveAdols.get(position).getRegAdolName());
            holder.txtAge.setText(listofDeactiveAdols.get(position).getRegAdolAge() + view.getResources().getString(R.string.yearscomma) + (listofDeactiveAdols.get(position).getRegAdolGender().equals("Male") ? view.getResources().getString(R.string.radiomale1) : view.getResources().getString(R.string.radiofemale1)));
            holder.txtMobile.setText(listofDeactiveAdols.get(position).getRegAdolMobile());
            holder.txtListofAdolDDAte.setText(listofDeactiveAdols.get(position).getRegAdolDeactivateDate());

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            
        }
    }

    @Override
    public int getItemCount() {
        return listofDeactiveAdols.size();
    }

    public void genderFilter(int i) {
        try {
            listofDeactiveAdols.clear();
            if (i == 0) {
                listofDeactiveAdols.addAll(modellist);
            } else {
                if (i == 1) {
                    for (tblAdolReg model : modellist) {
                        if (model.getRegAdolGender().contains("Male")) {
                            listofDeactiveAdols.add(model);
                        }
                    }
                } else if (i == 2) {
                    for (tblAdolReg model : modellist) {
                        if (model.getRegAdolGender().contains("Female")) {
                            listofDeactiveAdols.add(model);
                        }
                    }
                }
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            
        }
        notifyDataSetChanged();
    }
    public void filter(String charText) {
        try {
            charText = charText.toLowerCase(Locale.getDefault());
            listofDeactiveAdols.clear();
            if (charText.length() == 0) {
                listofDeactiveAdols.addAll(modellist);
            } else {
                for (tblAdolReg model : modellist) {
                    if (model.getRegAdolName().toLowerCase().contains(charText.toLowerCase())) {
                        listofDeactiveAdols.add(model);
                    }
                }
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            
        }
        notifyDataSetChanged();
    }
}
