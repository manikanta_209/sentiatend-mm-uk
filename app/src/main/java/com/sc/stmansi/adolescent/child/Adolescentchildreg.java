package com.sc.stmansi.adolescent.child;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.legacy.app.ActionBarDrawerToggle;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.sc.stmansi.About.AboutActivity;
import com.sc.stmansi.R;
import com.sc.stmansi.adolescent.AdolDeactivation;
import com.sc.stmansi.adolescent.AdolRegistration;
import com.sc.stmansi.adolescent.AdolVisitHistory;
import com.sc.stmansi.adolescent.AdolVisit_New;
import com.sc.stmansi.adolescent.AdolescentHome;
import com.sc.stmansi.adolescent.training.ListofAdolsFragment;
import com.sc.stmansi.adolescent.training.TrainingCreation;
import com.sc.stmansi.childgrowthmonitor.CGMList;
import com.sc.stmansi.childgrowthmonitor.CGMNewVisit;
import com.sc.stmansi.childregistration.AddSiblingsActivity;
import com.sc.stmansi.childregistration.EditChildActivity;
import com.sc.stmansi.childregistration.EditRegChildInfoActivity;
import com.sc.stmansi.childregistration.ViewParentDetails;
import com.sc.stmansi.common.AlertDialogUtil;
import com.sc.stmansi.common.CommonDatePickerFragment;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.common.NavDrawerItem;
import com.sc.stmansi.common.NavDrawerListAdapter;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.configuration.SyncState;
import com.sc.stmansi.database.DBMethods;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.deactivate.ChildDeactivateActivity;
import com.sc.stmansi.immunization.ImmunizationListActivity;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.repositories.AdolPregnantRepository;
import com.sc.stmansi.repositories.AdolescentRepository;
import com.sc.stmansi.repositories.LoginRepository;
import com.sc.stmansi.repositories.TransactionHeaderRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblAdolPregnant;
import com.sc.stmansi.tables.tblAdolReg;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.viewprofile.ViewProfileActivity;
import com.sc.stmansi.womanlist.RegisteredWomenActionTabs;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class Adolescentchildreg extends AppCompatActivity implements View.OnTouchListener, DatePickerDialog.OnDateSetListener, View.OnClickListener {
    private AQuery aq;
    private tblAdolReg tblAdolReg;
    private RadioGroup radiogroupHeight;
    private RadioGroup rg_pregnant;
    private RadioGroup rg_gender;
    private RadioGroup rg_isPeriod;
    private RadioGroup rg_topTitle;
    private DatabaseHelper databaseHelper;
    private DBMethods dbMethods;
    private Button btToolbarHome;
    private boolean isEditChild = false;
    private String AdolID;
    private TableRow tr_pregnant;
    private tblAdolReg adolEditData;
    private int transID = 0;
    private AppState appState;
    private TblInstusers user;
    private SyncState syncState;
    private LinearLayout llMaternal;
    private DrawerLayout drawer;
    private ArrayList<tblAdolPregnant> bornlist;
    private EditText currentEditText;
    private boolean isReadytoInsert;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adolescentchildreg);
        try {
            aq= new AQuery(this);
            databaseHelper = getHelper();

            Bundle bundle = getIntent().getBundleExtra("globalState");
            appState = bundle.getParcelable("appState");
            syncState = bundle.getParcelable("syncState");
            isEditChild = getIntent().getBooleanExtra("isEditChild", false);
//            tblAdolReg = (tblAdolReg) getIntent().getSerializableExtra("AdolReg");
            UserRepository userRepository = new UserRepository(databaseHelper);
            user = userRepository.getOneAuditedUser(appState.ashaId);
            transID = TransactionHeaderRepository.iCreateNewTransNew(user.getUserId(), databaseHelper); //23May2021 Bindu set userid
            AdolID = getIntent().getStringExtra("adolID");


            AdolescentRepository adolescentRepository = new AdolescentRepository(databaseHelper);
            tblAdolReg = adolescentRepository.getAdolDatafromAdolID(AdolID);

            initiateDrawer();

            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

            //To set click listener
            setOnClickListener();

            aq.id(R.id.spnnoofchild).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    aq.id(R.id.spnnoofchild).getSpinner().setEnabled(true);
                    if (i == 0) {
                        aq.id(R.id.llPP_FirstBorn).getView().setVisibility(View.GONE);
                        aq.id(R.id.llPP_SecondBorn).getView().setVisibility(View.GONE);
                        aq.id(R.id.llPP_ThirdBorn).getView().setVisibility(View.GONE);
                        aq.id(R.id.llPP_FourthBorn).getView().setVisibility(View.GONE);
                        for (int ini = 1; ini <= 4; ini++) {
                            aq.id(R.id.spnPregnantoutcomeofPregnancy + ini).getSpinner().setSelection(0);
                            aq.id(R.id.etPregnantDOD + ini).getEditText().setText("");
                            aq.id(R.id.spnPregnantwhereDeliverBaby + ini).getSpinner().setSelection(0);
                            aq.id(R.id.spnPregnantwhoConductedDelivery + ini).getSpinner().setSelection(0);
                            aq.id(R.id.spnPregnantwhomodeofTransport + ini).getSpinner().setSelection(0);
                            aq.id(R.id.spnPregnanttypeofdelivery + ini).getSpinner().setSelection(0);
                            aq.id(R.id.spnPregnantstatusofdelivery + ini).getSpinner().setSelection(0);
                            aq.id(R.id.spnPregnantsexofchild + ini).getSpinner().setSelection(0);
                            aq.id(R.id.etPregnantAgeatCurrentDate + ini).getEditText().setText("");
                        }
                    }
                    if (i == 1) {
                        aq.id(R.id.llPP_FirstBorn).getView().setVisibility(View.GONE);
                        aq.id(R.id.llPP_SecondBorn).getView().setVisibility(View.GONE);
                        aq.id(R.id.llPP_ThirdBorn).getView().setVisibility(View.GONE);
                        aq.id(R.id.llPP_FourthBorn).getView().setVisibility(View.GONE);
                        for (int ini = 1; ini <= 4; ini++) {
                            aq.id(R.id.spnPregnantoutcomeofPregnancy + ini).getSpinner().setSelection(0);
                            aq.id(R.id.etPregnantDOD + ini).getEditText().setText("");
                            aq.id(R.id.spnPregnantwhereDeliverBaby + ini).getSpinner().setSelection(0);
                            aq.id(R.id.spnPregnantwhoConductedDelivery + ini).getSpinner().setSelection(0);
                            aq.id(R.id.spnPregnantwhomodeofTransport + ini).getSpinner().setSelection(0);
                            aq.id(R.id.spnPregnanttypeofdelivery + ini).getSpinner().setSelection(0);
                            aq.id(R.id.spnPregnantstatusofdelivery + ini).getSpinner().setSelection(0);
                            aq.id(R.id.spnPregnantsexofchild + ini).getSpinner().setSelection(0);
                            aq.id(R.id.etPregnantAgeatCurrentDate + ini).getEditText().setText("");
                        }
                    }
                    if (i == 2) {
                        aq.id(R.id.llPP_FirstBorn).getView().setVisibility(View.VISIBLE);
                        aq.id(R.id.llPP_SecondBorn).getView().setVisibility(View.GONE);
                        aq.id(R.id.llPP_ThirdBorn).getView().setVisibility(View.GONE);
                        aq.id(R.id.llPP_FourthBorn).getView().setVisibility(View.GONE);
                        for (int ini = 2; ini <= 4; ini++) {
                            aq.id(R.id.spnPregnantoutcomeofPregnancy + ini).getSpinner().setSelection(0);
                            aq.id(R.id.etPregnantDOD + ini).getEditText().setText("");
                            aq.id(R.id.spnPregnantwhereDeliverBaby + ini).getSpinner().setSelection(0);
                            aq.id(R.id.spnPregnantwhoConductedDelivery + ini).getSpinner().setSelection(0);
                            aq.id(R.id.spnPregnantwhomodeofTransport + ini).getSpinner().setSelection(0);
                            aq.id(R.id.spnPregnanttypeofdelivery + ini).getSpinner().setSelection(0);
                            aq.id(R.id.spnPregnantstatusofdelivery + ini).getSpinner().setSelection(0);
                            aq.id(R.id.spnPregnantsexofchild + ini).getSpinner().setSelection(0);
                            aq.id(R.id.etPregnantAgeatCurrentDate + ini).getEditText().setText("");
                        }
                    } else if (i == 3) {
                        aq.id(R.id.llPP_FirstBorn).getView().setVisibility(View.VISIBLE);
                        aq.id(R.id.llPP_SecondBorn).getView().setVisibility(View.VISIBLE);
                        aq.id(R.id.llPP_ThirdBorn).getView().setVisibility(View.GONE);
                        aq.id(R.id.llPP_FourthBorn).getView().setVisibility(View.GONE);
                        for (int ini = 3; ini <= 4; ini++) {
                            aq.id(R.id.spnPregnantoutcomeofPregnancy + ini).getSpinner().setSelection(0);
                            aq.id(R.id.etPregnantDOD + ini).getEditText().setText("");
                            aq.id(R.id.spnPregnantwhereDeliverBaby + ini).getSpinner().setSelection(0);
                            aq.id(R.id.spnPregnantwhoConductedDelivery + ini).getSpinner().setSelection(0);
                            aq.id(R.id.spnPregnantwhomodeofTransport + ini).getSpinner().setSelection(0);
                            aq.id(R.id.spnPregnanttypeofdelivery + ini).getSpinner().setSelection(0);
                            aq.id(R.id.spnPregnantstatusofdelivery + ini).getSpinner().setSelection(0);
                            aq.id(R.id.spnPregnantsexofchild + ini).getSpinner().setSelection(0);
                            aq.id(R.id.etPregnantAgeatCurrentDate + ini).getEditText().setText("");
                        }
                    } else if (i == 4) {
                        aq.id(R.id.llPP_FirstBorn).getView().setVisibility(View.VISIBLE);
                        aq.id(R.id.llPP_SecondBorn).getView().setVisibility(View.VISIBLE);
                        aq.id(R.id.llPP_ThirdBorn).getView().setVisibility(View.VISIBLE);
                        aq.id(R.id.llPP_FourthBorn).getView().setVisibility(View.GONE);
                        for (int ini = 4; ini <= 4; ini++) {
                            aq.id(R.id.spnPregnantoutcomeofPregnancy + ini).getSpinner().setSelection(0);
                            aq.id(R.id.etPregnantDOD + ini).getEditText().setText("");
                            aq.id(R.id.spnPregnantwhereDeliverBaby + ini).getSpinner().setSelection(0);
                            aq.id(R.id.spnPregnantwhoConductedDelivery + ini).getSpinner().setSelection(0);
                            aq.id(R.id.spnPregnantwhomodeofTransport + ini).getSpinner().setSelection(0);
                            aq.id(R.id.spnPregnanttypeofdelivery + ini).getSpinner().setSelection(0);
                            aq.id(R.id.spnPregnantstatusofdelivery + ini).getSpinner().setSelection(0);
                            aq.id(R.id.spnPregnantsexofchild + ini).getSpinner().setSelection(0);
                            aq.id(R.id.etPregnantAgeatCurrentDate + ini).getEditText().setText("");
                        }
                    } else if (i == 5) {
                        aq.id(R.id.llPP_FirstBorn).getView().setVisibility(View.VISIBLE);
                        aq.id(R.id.llPP_SecondBorn).getView().setVisibility(View.VISIBLE);
                        aq.id(R.id.llPP_ThirdBorn).getView().setVisibility(View.VISIBLE);
                        aq.id(R.id.llPP_FourthBorn).getView().setVisibility(View.VISIBLE);
                        ////scrollview.smoothScrollTo(0, aq.id(R.id.llBasicdetails).getView().getBottom());
                    }
                if (isEditChild) {//to get the born details in edit when noofchild from 0 to something
                    try {
                        aq.id(R.id.spnnoofchild).getSpinner().setEnabled(false);
                        aq.id(R.id.btnsave).visibility(View.GONE);
                        AdolPregnantRepository adolPregnantRepository = new AdolPregnantRepository(databaseHelper);
                        List<tblAdolPregnant> allborns = new ArrayList<>();
                        allborns.add(new tblAdolPregnant());
                        allborns.addAll(adolPregnantRepository.getPregnantsbyIDs(AdolID));
                        for (int ini = 1; ini <= allborns.size(); ini++) {

                            int index = allborns.get(ini).getTblAdolPreOutcome();

                            String count=allborns.get(ini).getPregnantCount();
                            aq.id(R.id.spnnoofchild).getSpinner().setSelection(Integer.parseInt(count) + 1);
                            setEditdata(Integer.parseInt(count));
                            aq.id(R.id.spnPregnantoutcomeofPregnancy + ini).getSpinner().setSelection(index);
                            aq.id(R.id.spnPregnantoutcomeofPregnancy + ini).getSpinner().setEnabled(false);

                            String text = allborns.get(ini).getTblAdolPreDoD();
                            aq.id(R.id.etPregnantDOD + ini).getEditText().setText(text);
                            aq.id(R.id.etPregnantDOD + ini).getEditText().setEnabled(false);

                            aq.id(R.id.spnPregnantwhereDeliverBaby + ini).getSpinner().setSelection(allborns.get(ini).getTblAdolPreDeliverWhere());
                            aq.id(R.id.spnPregnantwhereDeliverBaby + ini).getSpinner().setEnabled(false);

                            aq.id(R.id.spnPregnantwhoConductedDelivery + ini).getSpinner().setSelection(allborns.get(ini).getTblAdolPreWhoConductDelivery());
                            aq.id(R.id.spnPregnantwhoConductedDelivery + ini).getSpinner().setEnabled(false);

                            aq.id(R.id.spnPregnantwhomodeofTransport + ini).getSpinner().setSelection(allborns.get(ini).getTblAdolPreTransport());
                            aq.id(R.id.spnPregnantwhomodeofTransport + ini).getSpinner().setEnabled(false);

                            aq.id(R.id.spnPregnanttypeofdelivery + ini).getSpinner().setSelection(allborns.get(ini).getTblAdolPreDeliveryType());
                            aq.id(R.id.spnPregnanttypeofdelivery + ini).getSpinner().setEnabled(false);

                            aq.id(R.id.spnPregnantstatusofdelivery + ini).getSpinner().setSelection(allborns.get(ini).getTblAdolPreDeliveryStatus());
                            aq.id(R.id.spnPregnantstatusofdelivery + ini).getSpinner().setEnabled(false);

                            aq.id(R.id.spnPregnantsexofchild + ini).getSpinner().setSelection(allborns.get(ini).getTblAdolPreChildSex());
                            aq.id(R.id.spnPregnantsexofchild + ini).getSpinner().setEnabled(false);

                            aq.id(R.id.etPregnantAgeatCurrentDate + ini).getEditText().setText(allborns.get(ini).getTblAdolPreChildAge());
                            aq.id(R.id.etPregnantAgeatCurrentDate + ini).getEditText().setEnabled(false);
                        }
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());

                    }
                }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });



        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private void setEditdata(int i) {
        if (i == 0) {
            aq.id(R.id.llPP_FirstBorn).getView().setVisibility(View.GONE);
            aq.id(R.id.llPP_SecondBorn).getView().setVisibility(View.GONE);
            aq.id(R.id.llPP_ThirdBorn).getView().setVisibility(View.GONE);
            aq.id(R.id.llPP_FourthBorn).getView().setVisibility(View.GONE);
        }
        if (i == 1) {
            aq.id(R.id.llPP_FirstBorn).getView().setVisibility(View.VISIBLE);
            aq.id(R.id.llPP_SecondBorn).getView().setVisibility(View.GONE);
            aq.id(R.id.llPP_ThirdBorn).getView().setVisibility(View.GONE);
            aq.id(R.id.llPP_FourthBorn).getView().setVisibility(View.GONE);
            setFirstBornUI();
        } else if (i == 2) {
            aq.id(R.id.llPP_FirstBorn).getView().setVisibility(View.VISIBLE);
            aq.id(R.id.llPP_SecondBorn).getView().setVisibility(View.VISIBLE);
            aq.id(R.id.llPP_ThirdBorn).getView().setVisibility(View.GONE);
            aq.id(R.id.llPP_FourthBorn).getView().setVisibility(View.GONE);
            setFirstBornUI();
            setSecondBornUI();
        } else if (i == 3) {
            aq.id(R.id.llPP_FirstBorn).getView().setVisibility(View.VISIBLE);
            aq.id(R.id.llPP_SecondBorn).getView().setVisibility(View.VISIBLE);
            aq.id(R.id.llPP_ThirdBorn).getView().setVisibility(View.VISIBLE);
            aq.id(R.id.llPP_FourthBorn).getView().setVisibility(View.GONE);
            setFirstBornUI();
            setSecondBornUI();
            setThirdBornUI();
        } else if (i == 4) {
            aq.id(R.id.llPP_FirstBorn).getView().setVisibility(View.VISIBLE);
            aq.id(R.id.llPP_SecondBorn).getView().setVisibility(View.VISIBLE);
            aq.id(R.id.llPP_ThirdBorn).getView().setVisibility(View.VISIBLE);
            aq.id(R.id.llPP_FourthBorn).getView().setVisibility(View.VISIBLE);
            setFirstBornUI();
            setSecondBornUI();
            setThirdBornUI();
            setFourthBornUI();
        }
    }

    //    initialize db object
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    private void setOnClickListener() {
        aq.id(R.id.btnsave).getImageView().setOnClickListener(this);
        aq.id(R.id.btncancel).getImageView().setOnClickListener(this);
        aq.id(R.id.etPregnantDOD4).getEditText().setOnTouchListener(this);
        aq.id(R.id.etPregnantDOD3).getEditText().setOnTouchListener(this);
        aq.id(R.id.etPregnantDOD2).getEditText().setOnTouchListener(this);
        aq.id(R.id.etPregnantDOD1).getEditText().setOnTouchListener(this);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        try {
            if (view.isShown()) {
                String strseldate = null;
                strseldate = String.format("%02d", dayOfMonth) + "-" + String.format("%02d", month + 1) + "-"
                        + year;

                @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                Date seldate = format.parse(strseldate);
                Calendar myCal = Calendar.getInstance();
                myCal.set(Calendar.YEAR, 2002);
                myCal.set(Calendar.MONTH, 1);
                myCal.set(Calendar.DAY_OF_MONTH, 1);
                Date lowEndLimit = myCal.getTime();

                     if (currentEditText == aq.id(R.id.etPregnantDOD1).getEditText()) {
                    if (seldate != null && seldate.after(new Date())) {
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.date_cannot_af_currentdate)
                                , this);
                        aq.id(R.id.etPregnantDOD1).text("");
                    } else {
                        aq.id(R.id.etPregnantDOD1).text(strseldate);
                        int age = Integer.parseInt(tblAdolReg.getRegAdolAge());
                        int childAge = getYearsFromDob(strseldate);
                        if (age!=0 && childAge < age) {
                            aq.id(R.id.etPregnantAgeatCurrentDate1).text(getYearsFromDob(strseldate) + "");
                        } else {
                            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.doddatecannotbemorethandob), Adolescentchildreg.this);
                            if (aq.id(R.id.etPregnantDOD1).getEditText().isEnabled()) {
                                aq.id(R.id.etPregnantDOD1).text("");
                                aq.id(R.id.etPregnantAgeatCurrentDate1).text("");
                            }
                        }
                    }
                } else if (currentEditText == aq.id(R.id.etPregnantDOD2).getEditText()) {
                    if (seldate != null && seldate.after(new Date())) {
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.date_cannot_af_currentdate)
                                , this);
                        aq.id(R.id.etPregnantDOD2).text("");
                    } else {
                        aq.id(R.id.etPregnantDOD2).text(strseldate);
                        int age = Integer.parseInt(tblAdolReg.getRegAdolAge());
                        int childAge = getYearsFromDob(strseldate);
                        if (age!=0 && childAge < age) {
                            aq.id(R.id.etPregnantAgeatCurrentDate2).text(getYearsFromDob(strseldate) + "");
                        } else {
                            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.doddatecannotbemorethandob), Adolescentchildreg.this);
                            if (aq.id(R.id.etPregnantDOD2).getEditText().isEnabled()) {
                                aq.id(R.id.etPregnantDOD2).text("");
                                aq.id(R.id.etPregnantAgeatCurrentDate2).text("");
                            }
                        }
                    }
                } else if (currentEditText == aq.id(R.id.etPregnantDOD3).getEditText()) {
                    if (seldate != null && seldate.after(new Date())) {
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.date_cannot_af_currentdate)
                                , this);
                        aq.id(R.id.etPregnantDOD3).text("");
                    } else {
                        aq.id(R.id.etPregnantDOD3).text(strseldate);
                        int age = Integer.parseInt(tblAdolReg.getRegAdolAge());
                        int childAge = getYearsFromDob(strseldate);
                        if (age!=0 && childAge < age) {
                            aq.id(R.id.etPregnantAgeatCurrentDate3).text(getYearsFromDob(strseldate) + "");
                        } else {
                            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.doddatecannotbemorethandob), Adolescentchildreg.this);
                            if (aq.id(R.id.etPregnantDOD3).getEditText().isEnabled()) {
                                aq.id(R.id.etPregnantDOD3).text("");
                                aq.id(R.id.etPregnantAgeatCurrentDate3).text("");
                            }
                        }
                    }
                } else if (currentEditText == aq.id(R.id.etPregnantDOD4).getEditText()) {
                    if (seldate != null && seldate.after(new Date())) {
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.date_cannot_af_currentdate)
                                , this);
                        aq.id(R.id.etPregnantDOD4).text("");
                    } else {
                        aq.id(R.id.etPregnantDOD4).text(strseldate);
                        int age = Integer.parseInt(tblAdolReg.getRegAdolAge());
                        int childAge = getYearsFromDob(strseldate);
                        if ( age!=0 && childAge < age) {
                            aq.id(R.id.etPregnantAgeatCurrentDate4).text(getYearsFromDob(strseldate) + "");
                        } else {
                            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.doddatecannotbemorethandob), Adolescentchildreg.this);
                            if (aq.id(R.id.etPregnantDOD4).getEditText().isEnabled()) {
                                aq.id(R.id.etPregnantDOD4).text("");
                                aq.id(R.id.etPregnantAgeatCurrentDate4).text("");
                            }

                        }
                    }
                }
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    @Override
    public void onClick(View view) {
        try {
            switch (view.getId()) {

                case R.id.imgchl1expand:
                    setFirstBornUI();
                    break;
                case R.id.imgchl2expand:
                    setSecondBornUI();
                    break;
                case R.id.imgchl3expand:
                    setThirdBornUI();
                    break;
                case R.id.imgchl4expand:
                    setFourthBornUI();
                    break;
                case R.id.btnsave:
                    setBornDetails();
                    break;
                case R.id.btncancel:
                    Intent exit = new Intent(Adolescentchildreg.this, MainMenuActivity.class);
                    displayAlert(getResources().getString(R.string.exit), exit);
                    break;
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }


    private void savedata() {

        if (aq.id(R.id.spnnoofchild).getSpinner().getSelectedItemPosition() == 0) {
            tblAdolReg.setRegAdolNoofChild(0);
        } else {
            tblAdolReg.setRegAdolNoofChild(Integer.parseInt(aq.id(R.id.spnnoofchild).getSpinner().getSelectedItem().toString()));
            if (aq.id(R.id.spnnoofchild).getSpinner().getSelectedItemPosition() != 0 && aq.id(R.id.spnnoofchild).getSpinner().getSelectedItemPosition() != 1) {
            }
        }
    }
    private void setBornDetails() {
        try {
            if (validationBornFields()) {
                setBornData();
                saveBornData();
            }

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private void setBornData() {
        try {
            bornlist = new ArrayList<>();
            int bornCount = Integer.parseInt(aq.id(R.id.spnnoofchild).getSpinner().getSelectedItem().toString());

            for (int i = 1; i <= bornCount; i++) {
                tblAdolPregnant born = new tblAdolPregnant();
                born.setAdolId(AdolID);
                born.setTransId(transID);
                born.setUserId(user.getUserId()); //23May2021 Bindu - change to userid
                born.setPregnantCount(i + "");
                born.setTblAdolPreOutcome(aq.id(R.id.spnPregnantoutcomeofPregnancy + i).getSpinner().getSelectedItemPosition());
                born.setTblAdolPreDoD(aq.id(R.id.etPregnantDOD + i).getEditText().getText().toString());
                born.setTblAdolPreDeliverWhere(aq.id(R.id.spnPregnantwhereDeliverBaby + i).getSpinner().getSelectedItemPosition());
                born.setTblAdolPreWhoConductDelivery(aq.id(R.id.spnPregnantwhoConductedDelivery + i).getSpinner().getSelectedItemPosition());
                born.setTblAdolPreTransport(aq.id(R.id.spnPregnantwhomodeofTransport + i).getSpinner().getSelectedItemPosition());
                born.setTblAdolPreDeliveryType(aq.id(R.id.spnPregnanttypeofdelivery + i).getSpinner().getSelectedItemPosition());
                born.setTblAdolPreDeliveryStatus(aq.id(R.id.spnPregnantstatusofdelivery + i).getSpinner().getSelectedItemPosition());
                born.setTblAdolPreChildSex(aq.id(R.id.spnPregnantsexofchild + i).getSpinner().getSelectedItemPosition());
                born.setTblAdolPreChildAge(aq.id(R.id.etPregnantAgeatCurrentDate + i).getEditText().getText().toString());
                born.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
                bornlist.add(born);
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private void saveBornData() {
        try {
            AdolPregnantRepository adolPregnantRepository = new AdolPregnantRepository(databaseHelper);
            Dao<tblAdolPregnant, Integer> dao = databaseHelper.gettblAdolPregnantRuntimeExceptionDao();
            long isAdded = 0;//14Jun2021 Arpitha
            boolean add = false;
            if (bornlist != null)
                for (tblAdolPregnant s : bornlist) {
                    if (adolPregnantRepository.checkisBornexist(s.getAdolId(), s.getPregnantCount()).size() == 0)//to check is the born already exist or not
                        isAdded =     dao.create(s);
                }
            //14Jun2021 Arpitha -  happens in single transaction
            if(isAdded>0)
               add = TransactionHeaderRepository.iNewRecordTransNew(user.getUserId(), transID, dao.getTableName(), databaseHelper);

            if (add){
                Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show();
                Intent nextscreen=new Intent(this, AdolescentHome.class);
                nextscreen.putExtra("globalState", prepareBundle());
                startActivity(nextscreen);
            }else
            {
                Toast.makeText(this, "Failed", Toast.LENGTH_SHORT).show();

            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        b.putParcelable("syncState", syncState);
        return b;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (MotionEvent.ACTION_UP == event.getAction()) {
            try {
                switch (v.getId()) {
                    case R.id.etPregnantDOD1:
                        assignEditTextAndCallDatePicker(aq.id(R.id.etPregnantDOD1).getEditText());
                        break;
                    case R.id.etPregnantDOD2:
                        assignEditTextAndCallDatePicker(aq.id(R.id.etPregnantDOD2).getEditText());
                        break;
                    case R.id.etPregnantDOD3:
                        assignEditTextAndCallDatePicker(aq.id(R.id.etPregnantDOD3).getEditText());
                        break;
                    case R.id.etPregnantDOD4:
                        assignEditTextAndCallDatePicker(aq.id(R.id.etPregnantDOD4).getEditText());
                        break;
                    case R.id.imgAdscleardob:
                        aq.id(R.id.etAdsdob).getEditText().setText("");
                }
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());

            }
        }
        return false;
    }



    @SuppressLint("UseCompatLoadingForDrawables")
    private void setFirstBornUI() {
        try {
            if (aq.id(R.id.llPP_FirstBornBody).getView().getVisibility() == View.VISIBLE) {
                aq.id(R.id.llPP_FirstBornBody).getView().setVisibility(View.GONE);
                aq.id(R.id.imgchl1expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
            } else {
                aq.id(R.id.llPP_FirstBornBody).getView().setVisibility(View.VISIBLE);
                aq.id(R.id.imgchl1expand).getImageView().setBackground(getResources().getDrawable(R.drawable.expand));

                aq.id(R.id.llPP_SecondBornBody).gone();
                aq.id(R.id.imgchl2expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                aq.id(R.id.llPP_ThirdBornBody).gone();
                aq.id(R.id.imgchl3expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                aq.id(R.id.llPP_FourthBornBody).gone();
                aq.id(R.id.imgchl4expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private void setSecondBornUI() {
        try {
            if (aq.id(R.id.llPP_SecondBornBody).getView().getVisibility() == View.VISIBLE) {
                aq.id(R.id.llPP_SecondBornBody).getView().setVisibility(View.GONE);
                aq.id(R.id.imgchl2expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
            } else {
                aq.id(R.id.llPP_SecondBornBody).getView().setVisibility(View.VISIBLE);
                aq.id(R.id.imgchl2expand).getImageView().setBackground(getResources().getDrawable(R.drawable.expand));

                aq.id(R.id.llPP_FirstBornBody).gone();
                aq.id(R.id.imgchl1expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                aq.id(R.id.llPP_ThirdBornBody).gone();
                aq.id(R.id.imgchl3expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                aq.id(R.id.llPP_FourthBornBody).gone();
                aq.id(R.id.imgchl4expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private void setThirdBornUI() {
        try {
            if (aq.id(R.id.llPP_ThirdBornBody).getView().getVisibility() == View.VISIBLE) {
                aq.id(R.id.llPP_ThirdBornBody).getView().setVisibility(View.GONE);
                aq.id(R.id.imgchl3expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
            } else {
                aq.id(R.id.llPP_ThirdBornBody).getView().setVisibility(View.VISIBLE);
                aq.id(R.id.imgchl3expand).getImageView().setBackground(getResources().getDrawable(R.drawable.expand));

                aq.id(R.id.llPP_SecondBornBody).gone();
                aq.id(R.id.imgchl2expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                aq.id(R.id.llPP_FirstBornBody).gone();
                aq.id(R.id.imgchl1expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                aq.id(R.id.llPP_FourthBornBody).gone();
                aq.id(R.id.imgchl4expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private void setFourthBornUI() {
        try {
            if (aq.id(R.id.llPP_FourthBornBody).getView().getVisibility() == View.VISIBLE) {
                aq.id(R.id.llPP_FourthBornBody).getView().setVisibility(View.GONE);
                aq.id(R.id.imgchl4expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
            } else {
                aq.id(R.id.llPP_FourthBornBody).getView().setVisibility(View.VISIBLE);
                aq.id(R.id.imgchl4expand).getImageView().setBackground(getResources().getDrawable(R.drawable.expand));

                aq.id(R.id.llPP_SecondBornBody).gone();
                aq.id(R.id.imgchl2expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                aq.id(R.id.llPP_ThirdBornBody).gone();
                aq.id(R.id.imgchl3expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                aq.id(R.id.llPP_FirstBornBody).gone();
                aq.id(R.id.imgchl1expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private void assignEditTextAndCallDatePicker(EditText editText) {
        currentEditText = editText;
        String currDate = (editText.getText().length() > 0) ? editText.getText().toString() : "";
        CommonDatePickerFragment.getCommonDate(this, currDate);
    }

    protected int getYearsFromDob(String string) throws ParseException {

        int years = 0;
//        String dobtxt = aq.id(R.id.etAdsdob).getText().toString();
        String dobtxt = string;
        Calendar curDt = new GregorianCalendar();
        curDt.setTime(new SimpleDateFormat("dd-MM-yyyy").parse(DateTimeUtil.getTodaysDate()));
        Calendar dob = new GregorianCalendar();
        if (dobtxt.length() > 0) {
            dob.setTime(new SimpleDateFormat("dd-MM-yyyy").parse(dobtxt));
            years = curDt.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
        }
        return years;
    }

    private void initiateDrawer() {
        mDrawerLayout = findViewById(R.id.ett_drawer_layout);
        mDrawerList = findViewById(R.id.ett_left_drawer);

        ArrayList<NavDrawerItem> navDrawerItems = new ArrayList<>();
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.list_of_adolescent), R.drawable.ic_adolall));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.homevisitadd), R.drawable.ic_homevisit));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.adolvisithistory), R.drawable.anm_pending_activities));
//        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.trainingparticipated), R.drawable.presentation));
//        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.mhmform), R.drawable.form_icon));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deactivate), R.drawable.deactivate));


        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        mDrawerList.setAdapter(new NavDrawerListAdapter(getApplicationContext(), navDrawerItems));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

// ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer,
                R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu(); // creates call to
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                getSupportActionBar().setDisplayShowTitleEnabled(false);
                getSupportActionBar().setDisplayShowCustomEnabled(false);

                try {
                    if (tblAdolReg !=null){
                        getSupportActionBar().setTitle(tblAdolReg.getRegAdolName());
                    }

                } catch (Exception e) {


                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                    crashlytics.log(e.getMessage());
                }
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                try {
                    invalidateOptionsMenu(); // creates call to
                    getSupportActionBar().setDisplayShowHomeEnabled(false);
                    getSupportActionBar().setDisplayShowTitleEnabled(false);
                    getSupportActionBar().setDisplayShowCustomEnabled(true);
                    getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);

                    aq.id(R.id.ivWomanImg).getImageView().setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            mDrawerLayout.closeDrawer(mDrawerList);
                        }
                    });

                    aq.id(R.id.tvWomanName).text((tblAdolReg.getRegAdolName() + "(" + tblAdolReg.getRegAdolGender() + ")"));
                    aq.id(R.id.tvInfo1).text((getResources().getString(R.string.age)
                            + " " +tblAdolReg.getRegAdolAge()+"Yr"));

                } catch (Exception e) {


                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                    crashlytics.log(e.getMessage());
                }
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    /**
     * This method is to sync draw,erToggle and to replace up caret icon to menu
     * icon in Action Bar
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    /**
     * The click listener for ListView in the navigation drawer
     */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            mDrawerLayout.closeDrawer(mDrawerList);
            try {

                switch (position) {
                    case 0:{
                        Intent nextscreen=new Intent(Adolescentchildreg.this, AdolescentHome.class);
                        nextscreen.putExtra("globalState", prepareBundle());
                        startActivity(nextscreen);
                        break;
                    }
                    case 1:{
                        Intent nextscreen=new Intent(Adolescentchildreg.this, AdolVisit_New.class);
                        nextscreen.putExtra("globalState", prepareBundle());
                        nextscreen.putExtra("adolID", AdolID);
                        startActivity(nextscreen);
                        break;
                    }

                    case 2:{
                        Intent nextscreen=new Intent(Adolescentchildreg.this, AdolVisitHistory.class);
                        nextscreen.putExtra("globalState", prepareBundle());
                        nextscreen.putExtra("adolID", AdolID);
                        startActivity(nextscreen);
                        break;
                    }

                    case 3:{
                        Intent nextscreen=new Intent(Adolescentchildreg.this, AdolDeactivation.class);
                        nextscreen.putExtra("globalState", prepareBundle());
                        if (tblAdolReg.getRegAdolDeactivateDate().length() != 0) {
                            nextscreen.putExtra("isView", true);
                        }
                        nextscreen.putExtra("adolID", AdolID);
                        startActivity(nextscreen);
                        break;
                    }
                }

            } catch (Exception e) {


                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer1);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);

        aq.id(R.id.tvWomanName).text((tblAdolReg.getRegAdolName() +
                " (" +
                tblAdolReg.getRegAdolGender() + ")"));
        aq.id(R.id.tvInfo1).text((getResources().getString(R.string.age)
                + " " +tblAdolReg.getRegAdolAge()+"Yr"));
        aq.id(R.id.ivWomanImg).gone();
        getMenuInflater().inflate(R.menu.servicesmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            case R.id.logout: {
                Intent goToScreen = new Intent(Adolescentchildreg.this, LoginActivity.class);
                displayAlert(getResources().getString(R.string.m111), goToScreen);
                return true;
            }
            case R.id.home: {
                Intent home = new Intent(Adolescentchildreg.this, MainMenuActivity.class);
                displayAlert(getResources().getString(R.string.exit), home);
                return true;
            }
            case R.id.wlist: {
                Intent goToScreen = new Intent(Adolescentchildreg.this, RegisteredWomenActionTabs.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }
            case R.id.about: {
                Intent goToScreen = new Intent(Adolescentchildreg.this, AboutActivity.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    /**
     * Save Confirmation Alert
     *
     * @param goToScreen
     * @param spanText2
     */
    private void displayAlert(final String spanText2, final Intent goToScreen) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        String strMess;
        if (goToScreen != null)
            strMess = getResources().getString(R.string.m121);
        else
            strMess = getResources().getString(R.string.ok);
        alertDialogBuilder.setMessage(spanText2).setCancelable(false).setNegativeButton(
                strMess, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (goToScreen != null) {
                            if (spanText2.contains(getResources().getString(R.string.save))) {
//                                saveData();
                            } else {
                                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                startActivity(goToScreen);
                                if (spanText2.contains("logout")) {
                                    try {
                                        LoginRepository loginRepository = new LoginRepository(databaseHelper);
                                        loginRepository.updateLogoutTime(appState.loginAuditId, appState.sessionUserId);
                                    } catch (Exception e) {
                                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                                        crashlytics.log(e.getMessage());
                                    }
                                }
                            }
                        } else
                            dialog.cancel();
                    }
                });

        if (goToScreen != null) {
            alertDialogBuilder.setPositiveButton(getResources().getString(R.string.m122),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
        }
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    private boolean validationBornFields() {
        try {
            if (aq.id(R.id.spnnoofchild).getSpinner().getSelectedItemPosition() > 1) {
                aq.id(R.id.llPP_FirstBornBody).getView().setVisibility(View.VISIBLE);
                aq.id(R.id.imgchl1expand).getImageView().setBackground(getResources().getDrawable(R.drawable.expand));
                aq.id(R.id.llPP_SecondBornBody).getView().setVisibility(View.GONE);
                aq.id(R.id.imgchl2expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                aq.id(R.id.llPP_ThirdBornBody).getView().setVisibility(View.GONE);
                aq.id(R.id.imgchl3expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                aq.id(R.id.llPP_FourthBornBody).getView().setVisibility(View.GONE);
                aq.id(R.id.imgchl4expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));

                if (aq.id(R.id.spnPregnantoutcomeofPregnancy1).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenteroutcomeofpregnancy), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD1).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantoutcomeofPregnancy1).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseenteroutcomeofpregnancy));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.etPregnantDOD1).getEditText().getText().toString().length() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseselectdateofdelivery), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD1).getEditText().requestFocus();
                    return false;
                } else if (aq.id(R.id.spnPregnantwhereDeliverBaby1).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenterdeliveryplace), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD1).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantwhereDeliverBaby1).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseenterdeliveryplace));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnantwhoConductedDelivery1).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenterwhoconducteddelivery), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD1).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantwhoConductedDelivery1).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseenterwhoconducteddelivery));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnantwhomodeofTransport1).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseentermodeoftransport), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD1).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantwhomodeofTransport1).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseentermodeoftransport));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnanttypeofdelivery1).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseentertypeofdelivery), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD1).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnanttypeofdelivery1).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseentertypeofdelivery));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnantstatusofdelivery1).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenterstatusofdelivery), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD1).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantstatusofdelivery1).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseenterstatusofdelivery));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnantsexofchild1).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseselectsexofchild), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD1).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantsexofchild1).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseselectsexofchild));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.etPregnantAgeatCurrentDate1).getEditText().getText().toString().length() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenterageofchildatcurrent), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantAgeatCurrentDate1).getEditText().requestFocus();
                    return false;
                } else {
                    aq.id(R.id.llPP_FirstBornBody).getView().setVisibility(View.GONE);
                    aq.id(R.id.imgchl1expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                }

            }
            if (aq.id(R.id.spnnoofchild).getSpinner().getSelectedItemPosition() > 2) {
                aq.id(R.id.llPP_FirstBornBody).getView().setVisibility(View.GONE);
                aq.id(R.id.imgchl1expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                aq.id(R.id.llPP_SecondBornBody).getView().setVisibility(View.VISIBLE);
                aq.id(R.id.imgchl2expand).getImageView().setBackground(getResources().getDrawable(R.drawable.expand));
                aq.id(R.id.llPP_ThirdBornBody).getView().setVisibility(View.GONE);
                aq.id(R.id.imgchl3expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                aq.id(R.id.llPP_FourthBornBody).getView().setVisibility(View.GONE);
                aq.id(R.id.imgchl4expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));

                if (aq.id(R.id.spnPregnantoutcomeofPregnancy2).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenteroutcomeofpregnancy), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD2).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantoutcomeofPregnancy2).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseenteroutcomeofpregnancy));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.etPregnantDOD2).getEditText().getText().toString().length() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseselectdateofdelivery), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD2).getEditText().requestFocus();
                    return false;
                } else if (aq.id(R.id.spnPregnantwhereDeliverBaby2).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenterdeliveryplace), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD2).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantwhereDeliverBaby2).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseenterdeliveryplace));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnantwhoConductedDelivery2).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenterwhoconducteddelivery), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD2).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantwhoConductedDelivery2).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseenterwhoconducteddelivery));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnantwhomodeofTransport2).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseentermodeoftransport), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD2).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantwhomodeofTransport2).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseentermodeoftransport));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnanttypeofdelivery2).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseentertypeofdelivery), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD2).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnanttypeofdelivery2).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseentertypeofdelivery));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnantstatusofdelivery2).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenterstatusofdelivery), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD2).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantstatusofdelivery2).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseenterstatusofdelivery));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnantsexofchild2).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseselectsexofchild), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD2).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantsexofchild2).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseselectsexofchild));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.etPregnantAgeatCurrentDate2).getEditText().getText().toString().length() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenterageofchildatcurrent), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantAgeatCurrentDate2).getEditText().requestFocus();
                    return false;
                } else {
                    aq.id(R.id.llPP_SecondBornBody).getView().setVisibility(View.GONE);
                    aq.id(R.id.imgchl2expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                }

            }
            if (aq.id(R.id.spnnoofchild).getSpinner().getSelectedItemPosition() > 3) {
                aq.id(R.id.llPP_FirstBornBody).getView().setVisibility(View.GONE);
                aq.id(R.id.imgchl1expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                aq.id(R.id.llPP_SecondBornBody).getView().setVisibility(View.GONE);
                aq.id(R.id.imgchl2expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                aq.id(R.id.llPP_ThirdBornBody).getView().setVisibility(View.VISIBLE);
                aq.id(R.id.imgchl3expand).getImageView().setBackground(getResources().getDrawable(R.drawable.expand));
                aq.id(R.id.llPP_FourthBornBody).getView().setVisibility(View.GONE);
                aq.id(R.id.imgchl4expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));

                if (aq.id(R.id.spnPregnantoutcomeofPregnancy3).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenteroutcomeofpregnancy), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD3).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantoutcomeofPregnancy3).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseenteroutcomeofpregnancy));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.etPregnantDOD3).getEditText().getText().toString().length() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseselectdateofdelivery), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD3).getEditText().requestFocus();
                    return false;
                } else if (aq.id(R.id.spnPregnantwhereDeliverBaby3).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenterdeliveryplace), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD3).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantwhereDeliverBaby3).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseenterdeliveryplace));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnantwhoConductedDelivery3).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenterwhoconducteddelivery), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD3).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantwhoConductedDelivery3).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseenterwhoconducteddelivery));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnantwhomodeofTransport3).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseentermodeoftransport), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD3).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantwhomodeofTransport3).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseentermodeoftransport));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnanttypeofdelivery3).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseentertypeofdelivery), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD3).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnanttypeofdelivery3).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseentertypeofdelivery));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnantstatusofdelivery3).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenterstatusofdelivery), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD3).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantstatusofdelivery3).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseenterstatusofdelivery));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnantsexofchild3).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseselectsexofchild), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD3).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantsexofchild3).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseselectsexofchild));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.etPregnantAgeatCurrentDate3).getEditText().getText().toString().length() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenterageofchildatcurrent), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantAgeatCurrentDate3).getEditText().requestFocus();
                    return false;
                } else {
                    aq.id(R.id.llPP_ThirdBornBody).getView().setVisibility(View.GONE);
                    aq.id(R.id.imgchl4expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                }

            }
            if (aq.id(R.id.spnnoofchild).getSpinner().getSelectedItemPosition() > 4) {
                aq.id(R.id.llPP_FirstBornBody).getView().setVisibility(View.GONE);
                aq.id(R.id.imgchl1expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                aq.id(R.id.llPP_SecondBornBody).getView().setVisibility(View.GONE);
                aq.id(R.id.imgchl2expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                aq.id(R.id.llPP_ThirdBornBody).getView().setVisibility(View.GONE);
                aq.id(R.id.imgchl3expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                aq.id(R.id.llPP_FourthBornBody).getView().setVisibility(View.VISIBLE);
                aq.id(R.id.imgchl4expand).getImageView().setBackground(getResources().getDrawable(R.drawable.expand));

                if (aq.id(R.id.spnPregnantoutcomeofPregnancy4).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenteroutcomeofpregnancy), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD4).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantoutcomeofPregnancy4).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseenteroutcomeofpregnancy));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.etPregnantDOD4).getEditText().getText().toString().length() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseselectdateofdelivery), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD4).getEditText().requestFocus();
                    return false;
                } else if (aq.id(R.id.spnPregnantwhereDeliverBaby4).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenterdeliveryplace), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD4).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantwhereDeliverBaby4).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseenterdeliveryplace));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnantwhoConductedDelivery4).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenterwhoconducteddelivery), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD4).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantwhoConductedDelivery4).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseenterwhoconducteddelivery));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnantwhomodeofTransport4).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseentermodeoftransport), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD4).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantwhomodeofTransport4).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseentermodeoftransport));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnanttypeofdelivery4).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseentertypeofdelivery), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD4).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnanttypeofdelivery4).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseentertypeofdelivery));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnantstatusofdelivery4).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenterstatusofdelivery), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD4).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantstatusofdelivery4).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseenterstatusofdelivery));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.spnPregnantsexofchild4).getSpinner().getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseselectsexofchild), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantDOD4).getEditText().requestFocus();
                    TextView errorText = (TextView) aq.id(R.id.spnPregnantsexofchild4).getSpinner().getSelectedView();
                    if (errorText != null) {
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.pleaseselectsexofchild));//changes the selected item text to this
                        errorText.setFocusable(true);
                    }
                    return false;
                } else if (aq.id(R.id.etPregnantAgeatCurrentDate4).getEditText().getText().toString().length() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseenterageofchildatcurrent), Toast.LENGTH_LONG).show();
                    aq.id(R.id.etPregnantAgeatCurrentDate4).getEditText().requestFocus();
                    return false;
                } else {
                    aq.id(R.id.llPP_FourthBornBody).getView().setVisibility(View.GONE);
                    aq.id(R.id.imgchl4expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
                }

            }
            return true;
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return false;
    }

}