package com.sc.stmansi.adolescent.training;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sc.stmansi.R;


public class CampaignAttendanceViewHolder extends RecyclerView.ViewHolder {
    TextView textViewName,textViewGTS,textViewAge;
    ImageView imgAbsent,imgPresent;

    public CampaignAttendanceViewHolder(@NonNull View itemView) {
        super(itemView);
        textViewName = itemView.findViewById(R.id.txt_attendanceAdolName);
        textViewGTS = itemView.findViewById(R.id.txt_attendanceGTS);
        textViewAge = itemView.findViewById(R.id.txt_attendanceAdolAge);
        imgAbsent = itemView.findViewById(R.id.imgabsent);
        imgPresent = itemView.findViewById(R.id.imgpresent);

    }
}
