package com.sc.stmansi.adolescent.homevisit;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sc.stmansi.R;

public class HomeVisitHistoryCCViewHolder extends RecyclerView.ViewHolder {
    TextView txtVisitCount,txtVisitDate,txtVisitAge;
    ImageView imgComplication,imgViewVisit,imgReferred;
    public HomeVisitHistoryCCViewHolder(@NonNull View itemView) {
        super(itemView);
        txtVisitCount = itemView.findViewById(R.id.txt_visitcount);
        txtVisitDate = itemView.findViewById(R.id.txt_visitdate);
        txtVisitAge = itemView.findViewById(R.id.txt_visitage);
        imgComplication = itemView.findViewById(R.id.imgComplication);
        imgViewVisit = itemView.findViewById(R.id.imgviewVisit);
        imgReferred = itemView.findViewById(R.id.imgReferred);
    }
}
