package com.sc.stmansi.adolescent;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.androidquery.AQuery;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.R;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.configuration.SyncState;
import com.sc.stmansi.database.DBMethods;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.repositories.AdolVisitHeaderRepository;
import com.sc.stmansi.repositories.AdolescentRepository;
import com.sc.stmansi.repositories.CampaignRepository;
import com.sc.stmansi.repositories.FacilityRepository;
import com.sc.stmansi.tables.tblAdolReg;
import com.sc.stmansi.tables.tblAdolVisitHeader;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.sc.stmansi.tables.tblCampDetails;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class AdolescentReports extends Fragment {

    private View view;
    private AppState appState;
    private DatabaseHelper databaseHelper;
    private AQuery aq;
    private Button btToolbarHome;
    private SyncState syncState;
    private List<tblAdolReg> listofAdols;
    private List<tblCampDetails> allCamps;
    private Map<String, Integer> facilities;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_adolescentreports, container, false);
       /* Bundle bundle = this.getArguments();
        if (bundle != null) {
            appState = bundle.getParcelable("appState");
        } else {
            appState = new AppState();
        } 19May2021 Arpitha*/

        //    19May2021 Arpitha
        Bundle bundle = getActivity().getIntent().getParcelableExtra("globalState");
        if (bundle != null) {
            appState = bundle.getParcelable("appState");
            syncState = bundle.getParcelable("syncState");
        } else {
            appState = new AppState();
        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
            Initialize();
            getData();
            setData();

            aq.id(R.id.toolbarSpnFacilities).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        if (position != 0) {
                            Integer villagecode = facilities.get(aq.id(R.id.toolbarSpnFacilities).getSpinner().getSelectedItem().toString());
//                            aq.id(R.id.imgresetfilter).getImageView().setImageResource(R.drawable.ic_reset_gray);
                            AdolescentRepository adolescentRepository = new AdolescentRepository(databaseHelper);
                            listofAdols = adolescentRepository.getAdolsbyFacility(String.valueOf(villagecode));

                            CampaignRepository campaignRepository = new CampaignRepository(databaseHelper);
                            allCamps = campaignRepository.getCampaignsbyFacility(String.valueOf(villagecode));
                        } else {
                            AdolescentRepository adolescentRepo = new AdolescentRepository(databaseHelper);
                            listofAdols = adolescentRepo.getallAdols();
                            CampaignRepository campaignRepository = new CampaignRepository(databaseHelper);
                            allCamps = campaignRepository.getAllCampaign();
                        }
                        setData();
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    try {
                        AdolescentRepository adolescentRepo = new AdolescentRepository(databaseHelper);
                        listofAdols = adolescentRepo.getallAdols();
                        CampaignRepository campaignRepository = new CampaignRepository(databaseHelper);
                        allCamps = campaignRepository.getAllCampaign();
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            
                    }
                }
            });

            btToolbarHome.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                    builder.setMessage(getResources().getString(R.string.exit));
                    builder.setCancelable(false);
                    builder.setNegativeButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent intent = new Intent(view.getContext(), MainMenuActivity.class);
                            intent.putExtra("globalState", prepareBundle());
                            startActivity(intent);
                        }
                    }).setPositiveButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    }).create().show();
                }
            });
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            
        }
    }

    @SuppressLint("SetTextI18n")
    private void setData() {
        try {
            listofAdols = addingLastVisitDetailstoAdols(listofAdols);

            aq.id(R.id.lltoolbarSpnFacilities).getView().setVisibility(View.VISIBLE);


            if (listofAdols != null) {

                int femaleCount = 0, maleCount = 0, pregnantCount = 0, GTSYesCount = 0,GTSNoCount = 0;
                for (tblAdolReg s : listofAdols) {
                    if (s.getRegAdolGender().equals("Female")) {
                        femaleCount = femaleCount + 1;
                    } else if (s.getRegAdolGender().equals("Male")) {
                        maleCount = maleCount + 1;
                    }

                    if (s.getRegAdolPregnant().equals("Yes")) {
                        pregnantCount = pregnantCount + 1;
                    }
                    if (s.getRegAdolGoingtoSchool().equals("Yes")) {
                        GTSYesCount = GTSYesCount + 1;
                    }else {
                        GTSNoCount = GTSNoCount + 1;
                    }
                }
                aq.id(R.id.txtAdolRptTotalAdols).getTextView().setText(String.valueOf(listofAdols.size()));
                aq.id(R.id.txtAdolRptTotalGirlAdols).getTextView().setText(String.valueOf(femaleCount));
                aq.id(R.id.txtAdolRptTotalBoyAdols).getTextView().setText(String.valueOf(maleCount));
                aq.id(R.id.txtAdolRptTotalPregnant).getTextView().setText(String.valueOf(pregnantCount));
                aq.id(R.id.txtAdolRptTotalGTS).getTextView().setText(String.valueOf(GTSYesCount));
                aq.id(R.id.txtAdolRptTotalNotGTS).getTextView().setText(String.valueOf(GTSNoCount));
            } else {
                aq.id(R.id.txtAdolRptTotalAdols).getTextView().setText(String.valueOf(0));
                aq.id(R.id.txtAdolRptTotalGirlAdols).getTextView().setText(String.valueOf(0));
                aq.id(R.id.txtAdolRptTotalBoyAdols).getTextView().setText(String.valueOf(0));
                aq.id(R.id.txtAdolRptTotalPregnant).getTextView().setText(String.valueOf(0));
                aq.id(R.id.txtAdolRptTotalGTS).getTextView().setText(String.valueOf(0));
                aq.id(R.id.txtAdolRptTotalNotGTS).getTextView().setText(String.valueOf(0));
            }

            if (allCamps != null) {
                aq.id(R.id.txtAdolRptTotalTrainings).getTextView().setText(String.valueOf(allCamps.size()));
            } else {
                aq.id(R.id.txtAdolRptTotalTrainings).getTextView().setText(String.valueOf(0));
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            
        }
    }

    @SuppressLint("SetTextI18n")
    private void getData() {
        try {
            FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
            facilities = facilityRepository.getPlaceMap();
            ArrayList<String> villageList = new ArrayList<String>();
            villageList.add(getResources().getString(R.string.all));
            for (Map.Entry<String, Integer> village : facilities.entrySet())
                villageList.add(village.getKey());
            ArrayAdapter<String> VillageAdapter = new ArrayAdapter<>(view.getContext(),
                    R.layout.simple_spinner_dropdown_item_white, villageList);
            VillageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            aq.id(R.id.toolbarSpnFacilities).adapter(VillageAdapter);

            AdolescentRepository adolescentRepository = new AdolescentRepository(databaseHelper);
            listofAdols = adolescentRepository.getallAdols();

            CampaignRepository campaignRepository = new CampaignRepository(databaseHelper);
             allCamps = campaignRepository.getAllCampaign();

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            
        }
    }

    private void Initialize() {
        try {
            databaseHelper = getHelper();
            DBMethods dbMethods = new DBMethods(databaseHelper);
            aq = new AQuery(view);
            btToolbarHome = view.findViewById(R.id.toolbarBtnHome);

            aq.id(R.id.toolbartxtTitle).getTextView().setText(getResources().getString(R.string.adolescentreports));
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            
        }
    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(view.getContext(), DatabaseHelper.class);
        }
        return databaseHelper;
    }

    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        b.putParcelable("syncState", syncState);
        return b;
    }

    private List<tblAdolReg> addingLastVisitDetailstoAdols(List<tblAdolReg> allAdols) {
        try {
            AdolVisitHeaderRepository adolVisitHeaderRepository = new AdolVisitHeaderRepository(databaseHelper);
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm");

            for (tblAdolReg s : allAdols) {
                if (s.getLastVisitCount() != 0) {
                    tblAdolVisitHeader localAdolVisitHeader = adolVisitHeaderRepository.getVisitbyID(String.valueOf(s.getLastVisitCount()), s.getAdolID());
                    if (s.getRegAdolGender().equals("Female"))
                        if (!(s.getRegAdolPregnant().equals(localAdolVisitHeader.getAdolvisHIsPregnantatReg()))) {
                            if (s.getRecordUpdatedDate()!=null && s.getRecordUpdatedDate().length() != 0) {
                                Date updateDate = format.parse(s.getRecordUpdatedDate());
                                Date localDate = format.parse(localAdolVisitHeader.getRecordCreatedDate());
                                if (updateDate.before(localDate)) {
                                    s.setRegAdolPregnant(localAdolVisitHeader.getAdolvisHIsPregnantatReg());
                                }
                            }
                        } else {
                            s.setRegAdolPregnant(localAdolVisitHeader.getAdolvisHIsPregnantatReg());
                        }
                    if (!(s.getRegAdolGoingtoSchool().equals(localAdolVisitHeader.getAdolvisHGTS()))) {
                        if (s.getRecordUpdatedDate()!=null && s.getRecordUpdatedDate().length() != 0) {
                            Date updateDate = format.parse(s.getRecordUpdatedDate());
                            Date localDate = format.parse(localAdolVisitHeader.getRecordCreatedDate());
                            if (updateDate.before(localDate)) {
                                s.setRegAdolGoingtoSchool(localAdolVisitHeader.getAdolvisHGTS());
                            }
                        }
                    } else {
                        s.setRegAdolGoingtoSchool(localAdolVisitHeader.getAdolvisHGTS());
                    }
                }
            }
            return allAdols;
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            
        }
        return null;
    }
}
