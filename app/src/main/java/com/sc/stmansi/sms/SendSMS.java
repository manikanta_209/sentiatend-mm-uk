package com.sc.stmansi.sms;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.repositories.SMSRepository;
import com.sc.stmansi.repositories.TransactionHeaderRepository;
import com.sc.stmansi.tables.MessageLogPojo;

import java.util.ArrayList;
import java.util.List;

public class SendSMS{

	private int mMessageSentParts;
	private int mMessageSentTotalParts;

	String SENT = "SMS_SENT";
	String DELIVERED = "SMS_DELIVERED";
	private static MessageLogPojo msgObj = null;
	private DatabaseHelper dbh = null;
	
	BroadcastReceiver sendingBroadcastReceiver;
	public static Context context ;
	private StringBuilder MultiMobileNo;
	private List<MessageLogPojo> localMsgObj;

	public SendSMS(Context context) throws Exception {
	    this.context = context;
	    //mani 15 april 2021 added dbh = getHelper();
//		dbh = new DatabaseHelper(context);
		dbh = getHelper();
//		checkAndSendMsg(dbh);15May2021 Arpitha
	}

	/** This method get first record from the MessageLog table which are not sent
	 * @throws Exception */
	public void checkAndSendMsg(DatabaseHelper dbh) throws Exception{
		//dbh = getHelper();
		SMSRepository smsRepository = new SMSRepository(dbh);
		localMsgObj = smsRepository.getFailedSmsOrNotSent();//RAmesh 15-May-2021
		msgObj = localMsgObj.get(0);
		if (localMsgObj.size()>1){
			MultiMobileNo = new StringBuilder();

			for (MessageLogPojo s: localMsgObj){
				MultiMobileNo.append(s.getMsgPhoneNo()).append("; ");
			}
		}
		if(msgObj != null){
			initializeBroadCastReceivers(context);
			context.registerReceiver(sendingBroadcastReceiver,
					new IntentFilter(SENT));
			sendSMS();
		}
	}

    //    initialize db object
    private DatabaseHelper getHelper() {
        if (dbh == null) {
            dbh = OpenHelperManager.getHelper(context, DatabaseHelper.class);
        }
        return dbh;
    }


    /** This method initiates the SMS to the particular Record */
	private void sendSMS() {
		
		final SmsManager sms = SmsManager.getDefault();
		
		/** This below line will divide the Message body to small parts */
		ArrayList<String> parts = sms.divideMessage(msgObj.getMsgBody());
		mMessageSentTotalParts = parts.size();

		/** Create the ArrayList of sent and delivery Intents */
		ArrayList<PendingIntent> deliveryIntents = new ArrayList<PendingIntent>();
		ArrayList<PendingIntent> sentIntents = new ArrayList<PendingIntent>();

		/** Create the Pending Intent for sent and delivery */
		PendingIntent sentPI = PendingIntent.getBroadcast(context, 0, new Intent(SENT), 0);
		PendingIntent deliveredPI = PendingIntent.getBroadcast(context, 0, new Intent(DELIVERED), 0);

		/** Loop and Populate the arraylist of sent and delivery intents */
		for (int j = 0; j < mMessageSentTotalParts; j++) {
			sentIntents.add(sentPI);
			deliveryIntents.add(deliveredPI);
		}

		/** For each Record set this variable to 0 */
		mMessageSentParts = 0;
		
		
		/** Below code will check the Network is UP or not and send the SMS */
		if(checkSimState() == TelephonyManager.SIM_STATE_READY){
			/*sms.sendMultipartTextMessage(msgObj.getMsgPhoneNo(), null,
					parts, sentIntents, deliveryIntents);*/


		//	String phoneNumber = "8892759870";

			String mss = TextUtils.join(", ", parts);


			Intent i = new Intent(Intent.ACTION_VIEW);
			i.setData(Uri.parse("smsto:"));
			i.setType("vnd.android-dir/mms-sms");
			//Ramesh 15-May-2021
			if (localMsgObj.size()>1){
				int lastcomma = MultiMobileNo.lastIndexOf(";");
				MultiMobileNo = MultiMobileNo.deleteCharAt(lastcomma);
				i.putExtra("address", new String(MultiMobileNo));
			}else {
				i.putExtra("address", new String(msgObj.getMsgPhoneNo()));
			}
			i.putExtra("sms_body",mss);
			i.putExtra("pendingintent",deliveryIntents);
			i.putExtra("pendingintent",sentIntents);
			context.startActivity(Intent.createChooser(i, "Send sms via:"));


		}
		
	}

	
	/** This method will Register the new BroadCastReceiver */
	private void initializeBroadCastReceivers(final Context context){

		sendingBroadcastReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context arg0, Intent arg1) {
				try{
					
					switch (getResultCode()) {
					case Activity.RESULT_OK:
						updateRecords(context, msgObj, true, "SMS sent");
						break;
					case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
						updateRecords(context, msgObj, false, "Generic failure");
						break;
					case SmsManager.RESULT_ERROR_NO_SERVICE:
						updateRecords(context, msgObj, false, "No service");
						break;
					case SmsManager.RESULT_ERROR_NULL_PDU:
						updateRecords(context, msgObj, false, "Null PDU");
						break;
					case SmsManager.RESULT_ERROR_RADIO_OFF:
						updateRecords(context, msgObj, false, "Radio off");
						break;
					}		
					
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		};

		

	}
	
	
	/** This method will Check wheather all parts of the Message body of the Record is Sent or Not
	 *  And Update the record as Sent or not */
	private void updateRecords(Context context, MessageLogPojo mlp, boolean isSent, String errorMsg) throws Exception{
		mMessageSentParts++;
		if ( mMessageSentParts == mMessageSentTotalParts ) {


            final TransactionHeaderRepository transactionHeaderRepository = new TransactionHeaderRepository(dbh);
             int transId = transactionHeaderRepository.iCreateNewTrans(msgObj.getUserId(), dbh);

            SMSRepository smsRepository  = new SMSRepository(dbh);
            smsRepository.updateMessageLog(msgObj, isSent,dbh, transId);

			context.unregisterReceiver(sendingBroadcastReceiver);
			
			if(checkSimState() == TelephonyManager.SIM_STATE_READY)
				checkAndSendMsg(dbh);
		}
	}
    public static int checkSimState() {
        int simState = -1;
        // This is to know the status of the Sim
        TelephonyManager telMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        simState = telMgr.getSimState();

        return simState;
    }
	
}

