//28May2021 Arpitha
package com.sc.stmansi.sms;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.About.AboutActivity;
import com.sc.stmansi.R;
import com.sc.stmansi.Settings.SettingsActivity;
import com.sc.stmansi.Settings.SettingsAdapter;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.repositories.LoginRepository;
import com.sc.stmansi.repositories.SMSRepository;
import com.sc.stmansi.tables.MessageLogPojo;
import com.sc.stmansi.womanlist.RegisteredWomenActionTabs;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SMSLog extends AppCompatActivity {

    DatabaseHelper databaseHelper;
    AQuery aqSMSLog;
    AppState appState;
    List<MessageLogPojo> messageLogPojoList;
    private SMSLogAdapter settingsAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_smslog);
            databaseHelper = getHelper();
            aqSMSLog = new AQuery(this);
            Bundle bundle = getIntent().getBundleExtra("globalState");
            appState = bundle.getParcelable("appState");

            getMessageLogData();
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public void updateAdapter() throws Exception {
        if (settingsAdapter != null) {
            SMSRepository smsRepository = new SMSRepository(databaseHelper);
            messageLogPojoList = smsRepository.getMessageLogData(appState.sessionUserId);
            settingsAdapter = new SMSLogAdapter(this, R.layout.adapter_smslog, messageLogPojoList, databaseHelper, this);
            aqSMSLog.id(R.id.listsmslog).getListView().setAdapter(settingsAdapter);
        }
    }

    //	intialize db
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    void getMessageLogData() throws SQLException {
        messageLogPojoList = new ArrayList<>();
        SMSRepository smsRepository = new SMSRepository(databaseHelper);
        messageLogPojoList = smsRepository.getMessageLogData(appState.sessionUserId);

        if (messageLogPojoList.size() > 0) {
            settingsAdapter = new SMSLogAdapter(SMSLog.this, R.layout.adapter_smslog,
                    messageLogPojoList,databaseHelper,this);
            aqSMSLog.id(R.id.listsmslog).getListView().setAdapter(settingsAdapter);
            aqSMSLog.id(R.id.listsmslog).visible();
            aqSMSLog.id(R.id.txtnodatasmslog).gone();
        } else
        {
            aqSMSLog.id(R.id.listsmslog).gone();
            aqSMSLog.id(R.id.txtnodatasmslog).visible();
        }

    }

    /**
     * Initializes the Action Bar items
     */
    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        try {

            getMenuInflater().inflate(R.menu.servicesmenu, menu);
            menu.findItem(R.id.logout).setTitle((getResources().getString(R.string.action_logout)));

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Called whenever an item clicked in actionbar
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action buttons
        switch (item.getItemId()) {

            case R.id.home: {
                Intent goToScreen = new Intent(SMSLog.this, MainMenuActivity.class);
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }

            case R.id.logout: {
                Intent goToScreen = new Intent(SMSLog.this, LoginActivity.class);
                displayAlert(getResources().getString(R.string.m111), goToScreen);
                return true;
            }


            case R.id.wlist: {
                Intent goToScreen = new Intent(SMSLog.this, RegisteredWomenActionTabs.class);
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }
            //30Sep2019 - Bindu
            case R.id.about: {
                Intent goToScreen = new Intent(SMSLog.this, AboutActivity.class);
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Save Confirmation Alert
     *
     * @param goToScreen
     * @param spanText2
     */
    private void displayAlert(final String spanText2, final Intent goToScreen) {

        String strOkMess;
        if (goToScreen != null)
            strOkMess = getResources().getString(R.string.yes);
        else
            strOkMess = getResources().getString(R.string.ok);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set dialog message
        alertDialogBuilder.setMessage(spanText2).setCancelable(false).setNegativeButton(
                strOkMess, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (goToScreen != null) {


                                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));

                                startActivity(goToScreen);
                                if (spanText2.contains("logout")) {
                                    try {
                                        LoginRepository loginRepository = new LoginRepository(databaseHelper);
                                        loginRepository.updateLogoutTime(appState.loginAuditId, appState.sessionUserId);
                                    } catch (Exception e) {
                                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                                        crashlytics.log(e.getMessage());
                                    }

                            }
                        } else
                            dialog.cancel();
                    }
                });
        if (goToScreen != null) {
            alertDialogBuilder.setPositiveButton(getResources().getString(R.string.m122),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
        }
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

}
