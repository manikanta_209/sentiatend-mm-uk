package com.sc.stmansi.sms;

import android.app.Activity;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.provider.ContactsContract;
import android.telephony.PhoneNumberUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.misc.TransactionManager;
import com.sc.stmansi.R;
import com.sc.stmansi.Settings.SettingsAdapter;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.repositories.SMSRepository;
import com.sc.stmansi.repositories.SettingsRepository;
import com.sc.stmansi.repositories.TransactionHeaderRepository;
import com.sc.stmansi.tables.MessageLogPojo;
import com.sc.stmansi.tables.TblContactDetails;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


public class SMSLogAdapter extends ArrayAdapter<MessageLogPojo> {

    private final SMSLog log;
    List<MessageLogPojo> messageLogList;
    int layoutResourceId;
    Context context;
    boolean isMessageLogsaved;
    DatabaseHelper databaseHelper;

    public SMSLogAdapter(@NonNull Context context, int resource,
                         List<MessageLogPojo> messageLogList, DatabaseHelper databaseHelper,SMSLog log) {
        super(context, resource);
        this.messageLogList = messageLogList;
        layoutResourceId = resource;
        this.context = context;
        this.databaseHelper = databaseHelper;
        this.log = log;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View row = convertView;
        RecordHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new RecordHolder();
            holder.txtPhn = (TextView) row.findViewById(R.id.txtPhnum);
            holder.btnresend = (Button) row.findViewById(R.id.btnresend);
            holder.txtRecvd = (TextView) row.findViewById(R.id.txtRecvd);
            holder.txtSMSContent = (TextView) row.findViewById(R.id.txtMessCont);
            holder.txtModule = (TextView) row.findViewById(R.id.txtModule);
            holder.whatsapp =(ImageButton) row.findViewById(R.id.imgwhatsapp);



            row.setTag(holder);
        } else {
            holder = (RecordHolder) row.getTag();
        }

        if (position == 0) {
            holder.txtPhn.setText(context.getResources().getString(R.string.tvphone));
            holder.txtModule.setText(context.getResources().getString(R.string.screen));
            holder.txtSMSContent.setText(context.getResources().getString(R.string.content));
            holder.txtRecvd.setText(context.getResources().getString(R.string.receivedbycc));

            holder.txtRecvd.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
            holder.txtSMSContent.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
            holder.txtModule.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
            holder.txtPhn.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
            holder.btnresend.setVisibility(View.INVISIBLE);
            holder.whatsapp.setVisibility(View.INVISIBLE);


        } else {

            MessageLogPojo messageLogPojo = messageLogList.get(position - 1);
            holder.txtPhn.setText(messageLogPojo.getMsgPhoneNo());
            holder.txtModule.setText(messageLogPojo.getMsgStage());
            holder.txtSMSContent.setText(messageLogPojo.getMsgBody().substring(0, 10) + "...");
            if (messageLogPojo.getMsgRecvdByCC() != null && messageLogPojo.getMsgRecvdByCC().trim().length() > 0 &&
                    messageLogPojo.getMsgRecvdByCC().equalsIgnoreCase("Yes")) {
                holder.txtRecvd.setText(context.getResources().getString(R.string.yes));
                holder.txtRecvd.setEnabled(false);
            }
            else
                holder.txtRecvd.setText(context.getResources().getString(R.string.no));

        }


        holder.btnresend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    display_messagedialog(messageLogList.get(position - 1));
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        holder.txtRecvd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    isSMSRecvd(position - 1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        holder.whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    sendSMSViawhatsApp(messageLogList.get(position-1).getMsgPhoneNo(),
                            messageLogList.get(position-1).getMsgBody());
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });


        return row;

    }

    static class RecordHolder {
        TextView txtPhn, txtModule, txtSMSContent, txtRecvd;
        Button btnresend;
        ImageButton whatsapp;


    }

    public int getCount() {
        return messageLogList.size() + 1;
    }


    public long getItemId(int position) {
        return position;
    }


    public void display_messagedialog(MessageLogPojo messageLogPojo) throws Exception {
        try {
            final Dialog sms_dialog = new Dialog(context);
//            sms_dialog.setTitle(getResources().getString(R.string.send_sms));

            sms_dialog.setContentView(R.layout.sms_dialog);


            sms_dialog.show();

            ImageButton imgsend = (ImageButton) sms_dialog.findViewById(R.id.imgsend);
            ImageButton imgcancel = (ImageButton) sms_dialog.findViewById(R.id.imgcancel);
            EditText etphn = (EditText) sms_dialog.findViewById(R.id.etphno);
            final EditText etmess = (EditText) sms_dialog.findViewById(R.id.etmess);
            TextView txtcontcats = (TextView) sms_dialog.findViewById(R.id.txtcontacts);
            TextView txtsmsheading = sms_dialog.findViewById(R.id.txtsmsheading);


            etphn.setText(messageLogPojo.getMsgPhoneNo());
            etmess.setText(messageLogPojo.getMsgBody());
            etmess.setEnabled(false);
            txtsmsheading.setText(context.getResources().getString(R.string.resend) + " "
                    + context.getResources().getString(R.string.sms) + " - " + messageLogPojo.getMsgStage());


            imgsend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {

                        if (etphn.getText().toString().trim().length() <= 0) {
                            Toast.makeText(context, context.getResources().getString(R.string.enter_valid_phn_no),
                                    Toast.LENGTH_LONG).show();
                        } else if (etmess.getText().toString().trim().length() <= 0) {
                            Toast.makeText(context, context.getResources().getString(R.string.enter_message),
                                    Toast.LENGTH_LONG).show();
                        } else {
                            sms_dialog.cancel();
                            if (!messageLogPojo.getMsgPhoneNo().equalsIgnoreCase(etphn.getText().toString())) {
                                messageLogPojo.setMsgPhoneNo(etphn.getText().toString());
                                InserttblMessageLog(messageLogPojo);
                                if (isMessageLogsaved) {
                                    Toast.makeText(context, context.getResources().getString(R.string.sending_sms),
                                            Toast.LENGTH_LONG).show();
                                    sendSMSFunction();

                                }
                            } else
                                sendSMSFunction();
                        }

                    } catch (Exception e) {

                        e.printStackTrace();
                    }

                }
            });


            imgcancel.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    sms_dialog.cancel();
                }
            });
            sms_dialog.setCancelable(false);

        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    /**
     * This method invokes after successfull save of record Sends SMS to the
     * Specified Number
     */// 13Oct2016 Arpitha
    private void sendSMSFunction() throws Exception {
        new SendSMS(context).checkAndSendMsg(databaseHelper); //mani 15 april 2021

    }

    public void InserttblMessageLog(MessageLogPojo messageLogPojo) throws Exception {
        final ArrayList<MessageLogPojo> mlpArr = new ArrayList<MessageLogPojo>();

        final TransactionHeaderRepository transactionHeaderRepository = new TransactionHeaderRepository(databaseHelper);
        final int transId = transactionHeaderRepository.iCreateNewTrans(messageLogPojo.getUserId(), databaseHelper);


        if (messageLogPojo.getMsgPhoneNo().length() > 0) {
            String[] phn = messageLogPojo.getMsgPhoneNo().split("\\,");
            for (int i = 0; i < phn.length; i++) {
                String num = phn[i];
                if (num != null && num.length() > 0) {
                    MessageLogPojo mlp =
                            new MessageLogPojo();
                    mlp.setUserId(messageLogPojo.getUserId());
                    mlp.setMsgPhoneNo(num);
                    mlp.setMsgBody(messageLogPojo.getMsgBody());
                    mlp.setMsgPriority(1);
                    mlp.setMsgNoOfFailures(0);
                    mlp.setTransId(transId);
                    mlp.setMsgSentDate(DateTimeUtil.getTodaysDate());
                    mlp.setMsgSent(0);
                    mlp.setMsgStage(messageLogPojo.getMsgStage());
                    mlp.setMsgUserType(messageLogPojo.getMsgUserType());
                    mlp.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
                    mlp.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());//21May2021 Arpitha

                    mlpArr.add(mlp);
                }
            }
        }


        TransactionManager.callInTransaction(databaseHelper.getConnectionSource(), new Callable<Void>() {
            public Void call() throws Exception {

                int addMessage = 0;
                SMSRepository smsRepository = new SMSRepository(databaseHelper);
                if (mlpArr != null) {
                    for (final MessageLogPojo mlpp : mlpArr) {

                        addMessage = smsRepository.insertIntoMessageLog(mlpp, databaseHelper);
                    }

                    if (addMessage > 0) {
                        isMessageLogsaved = true;
                        boolean addRegTrans = transactionHeaderRepository.iNewRecordTransNew(messageLogPojo.getUserId(), transId,
                                "tblmessagelog", databaseHelper);
                    }
                }

                return null;
            }
        });


    }

    public void isSMSRecvd(int pos) throws Exception {
        try {
            final Dialog dialogRecvd = new Dialog(context);

            dialogRecvd.setContentView(R.layout.dialogsmsrecieved);


            dialogRecvd.show();

            Button btnYes = dialogRecvd.findViewById(R.id.btnyes);
            Button btnNo = dialogRecvd.findViewById(R.id.btnno);

            btnNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialogRecvd.cancel();
                }
            });

            btnYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    try {
                        dialogRecvd.cancel();
                        updateIsSMSrecvd(pos);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            dialogRecvd.setCancelable(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateIsSMSrecvd(int pos) throws Exception {

        SMSRepository smsRepository = new SMSRepository(databaseHelper);
        smsRepository.updateIsSMSRecvdByCC(messageLogList.get(pos), databaseHelper);
        Toast.makeText(context,"Updated Successfully!!",Toast.LENGTH_LONG).show();

        log.updateAdapter();
    }

    public void  sendSMSViawhatsApp(String phnNo, String content) throws UnsupportedEncodingException {

        String smsNumber = "+91 "+phnNo;
        boolean isWhatsappInstalled = whatsappInstalledOrNot("com.whatsapp");
        if (isWhatsappInstalled) {

            /*Intent sendIntent = new Intent("android.intent.action.MAIN");
            sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
            sendIntent.putExtra("jid", PhoneNumberUtils.stripSeparators(smsNumber) + "@s.whatsapp.net");//phone number without "+" prefix
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, "hi");
            sendIntent.setType("text/plain");
            context.startActivity(sendIntent);*/
            PackageManager packageManager = context.getPackageManager();
            Intent i = new Intent(Intent.ACTION_VIEW);
            String url = "https://api.whatsapp.com/send?phone="+ smsNumber +"&text="
                    + URLEncoder.encode(content, "UTF-8");
            i.setPackage("com.whatsapp");
            i.setData(Uri.parse(url));
            if (i.resolveActivity(packageManager) != null) {
                context.startActivity(i);
            }
        } else {
            Uri uri = Uri.parse("market://details?id=com.whatsapp");
            Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
            Toast.makeText(context, "WhatsApp not Installed",
                    Toast.LENGTH_SHORT).show();
            context.startActivity(goToMarket);
        }
    }

    private boolean whatsappInstalledOrNot(String uri) {
        PackageManager pm = context.getPackageManager();
        boolean app_installed = false;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }
}




