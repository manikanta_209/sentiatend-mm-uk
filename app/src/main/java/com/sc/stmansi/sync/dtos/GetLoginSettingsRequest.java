package com.sc.stmansi.sync.dtos;

public class GetLoginSettingsRequest extends RESTRequest {

    public String UserId;
    public String lastUpdatedTime;
    public String dbname;
    public String ashaId;
    public String mobileNumber;
    public String Pswd;
    public String DBname;
    public String DeviceId;

}
