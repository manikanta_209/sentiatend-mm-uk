//26Aug2019 Arpitha - code clean up
package com.sc.stmansi.sync;

import java.util.HashMap;
import java.util.List;

public class XMLObject {

	 HashMap<String, String> params;
	 private List<XMLObject> childs;
	 String value;
	 String name;

	   public String getName() {
	  	return name;
	 }

	 public void setName(String name) {
	  this.name = name;
	 }

	 public String getValue() {
	  return value;
	 }

	 public void setValue(String value) {
	  this.value = value;
	 }

	 public void setParams(HashMap<String, String> params) {
	  this.params = params;
	 }

	 public List<XMLObject> getChilds() {
	  return childs;
	 }

	 public void setChilds(List<XMLObject> childs) {
	  this.childs = childs;
	 }

	}
