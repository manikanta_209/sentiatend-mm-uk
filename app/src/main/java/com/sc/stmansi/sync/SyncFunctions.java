// 26Aug2019 Arpitha
package com.sc.stmansi.sync;


import android.os.Environment;
import android.util.Log;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.configuration.SyncState;
import com.sc.stmansi.database.DBMethods;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;


public class SyncFunctions {

    private final DBMethods dbMethods;
    private final SyncState syncState;
    String androidId;

    //mani 23/2/2021 added new field
    public  static boolean isWomanId;

    private WebService webService; //Gururaja 07Jun2021

    //	constructor
    public SyncFunctions(String androidId, DBMethods dbMethods, SyncState syncState) {

        this.dbMethods = dbMethods;
        this.syncState = syncState;
        this.androidId = androidId;//Arpitha 26Aug2019
    }

    //Gururaja 07Jun2021
    public SyncFunctions(String androidId, DBMethods dbMethods, SyncState syncState, WebService webService) {
        this.dbMethods = dbMethods;
        this.syncState = syncState;
        this.androidId = androidId;//Arpitha 26Aug2019
        this.webService = webService; //Gururaja 07Jun2021
    }

    //  display object
    public void display222(XMLObject mObject) {
        if (mObject != null) {

            List<XMLObject> mXmlObjects = mObject.getChilds();

            if (mXmlObjects != null && mXmlObjects.size() > 0) {
                for (XMLObject xmlObject : mXmlObjects) {

                    int varSize = 0;

                    if (xmlObject.getChilds() != null)
                        varSize = xmlObject.getChilds().size();


                    if (xmlObject != null && varSize > 0) {
                        display222(xmlObject);
                    }

                    if ((xmlObject.getName() != null) && (xmlObject.getValue() != null)) {
                        Log.d("KEY:" + xmlObject.getName() + " :: Value=", xmlObject.getValue());

                        String xKey = xmlObject.getName();
                        String xValue = xmlObject.getValue();

                        if (xKey.equals("Messagecnt")) {
                            this.syncState.Messagecnt = xValue;
                        } else if (xKey.equals("RequestId")) {
                            this.syncState.RequestId = xValue;
                            this.syncState.RequestId = syncState.RequestId.replace("'", "");
                        } else if (xKey.equals("Status")) {
                            this.syncState.xStatus = xValue;
                        } else if (xKey.equals("FileStatus")) {
                            this.syncState.FileStatus = xValue;
                        } else if (xKey.equals("DBStatus")) {
                            this.syncState.DBStatus = xValue;
                        } else if (xKey.equals("Validated")) {
                            this.syncState.serValidUser = Boolean.parseBoolean(xValue);
                        } else if (xKey.equals("UserIdreturned")) {
                            this.syncState.userIdReturned = xValue.trim();
                        } else if (xKey.equals("Losttab")) {
                            this.syncState.serTabLost = Boolean.parseBoolean(xValue);
                        } else if (xKey.equals("InvalidDb")) {
                            this.syncState.invalidDb = Boolean.parseBoolean(xValue);
                        } else if (xKey.equals("ReadyToDownload")) {
                            this.syncState.readyToDownload = Boolean.parseBoolean(xValue);
                        } else if (xKey.equals("ReturnFileTag")) {
                            this.syncState.returnFilename = xValue.trim();
                        } else if (xKey.equals("InfoTag")) {
                            this.syncState.returnedInfo = xValue.trim();
                        }
                    }
                }
            }
        }
    }

    //  sync function
    public String sync(String xUserId) throws Exception {
        String SendMessage;
        String displayText;
        try {
            String xLastTranPendingXML = dbMethods.callResponseXML();
            System.out.println("xLastTranPendingXML " + xLastTranPendingXML);

            if (xLastTranPendingXML.equals("")) {                                //Normal Transaction

                SendMessage = dbMethods.callXMLExport(androidId, xUserId);
                if (SendMessage.equals("")) {
                    syncState.syncUptoDate = true;

                    syncState.movedToInputFolder = true; //27Apr2021 Bindu add inputfolder and responseackcount
                    syncState.responseAckCount = 0;

                    displayText = "SYNC UP TO DATE. NO RECORDS FOUND TO SYNC";

                    System.out.println("MessageForwarder " + displayText);

                    syncState.isRecordExistsForSync = false;//09Feb2018 Arpitha
                } else if (SendMessage.equals("ERR")) {
                    System.out.println("Error.......2");

                    return "";
                } else {


                    SendMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + SendMessage;
                    SendMessage = new jEncrypt().jEncryptStr(SendMessage);

                    //mani 23/22021
                    displayText = webService.forwardMessageOrGetDataFromServer(SendMessage, "MessageForwarder");
                    System.out.println("MessageForwarder " + displayText);
                    updateResponse(displayText);
                }
            } else {
                //Get the reponse of LastPending
                SendMessage = xLastTranPendingXML;
                SendMessage = new jEncrypt().jEncryptStr(SendMessage);

                displayText = webService.forwardMessageOrGetDataFromServer(SendMessage, "GetDataFromServer");
                System.out.println("GetDataFromServer  " + displayText);
                updateResponse(displayText);
            }

        } catch (Exception e) {

            throw e;
        }


        return displayText;
    }

    // update response from server
    public void updateResponse(String displayText) {
        if (displayText == null || displayText.trim().equalsIgnoreCase("Error") || displayText.toLowerCase().contains("failed to connect")) {
            this.syncState.movedToInputFolder = false;
        } else {
            try {
                //mani 23/2/2021
//                displayText = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + displayText;
                InputStream is = new ByteArrayInputStream(displayText.getBytes(StandardCharsets.UTF_8));
                XMLObject mObject = XmlParser.parseXml(is);
                if (mObject != null) {
                    syncState.RequestId = "";
                    syncState.xStatus = "";
                    syncState.FileStatus = "";
                    syncState.DBStatus = "";
                    display222(mObject);

                    if (syncState.xStatus.equals("Failure")) {
                        // Serious Issue... To be decided What needs to be done
                        dbMethods.UpdateTransStatus(syncState.RequestId, "R");
                        this.syncState.movedToInputFolder = false;
//                        writeToTextFile(syncState.xStatus+" "+displayText);
                    } else if (syncState.DBStatus.equals("TransactionRollback")) {
                        // Update Database as Request Rolled Back
                        dbMethods.UpdateTransStatus(syncState.RequestId, "R");        // Rejected by Server
                        this.syncState.movedToInputFolder = false;
                    } else if (syncState.DBStatus.equals("TransactionCommit")) {
                        // Update Database as Request Committed
                        dbMethods.UpdateTransStatus(syncState.RequestId, "C");        // Committed by Server
                        this.syncState.movedToInputFolder = true;
                        this.syncState.responseAckCount = 0;
                        this.syncState.isRecordExistsForSync = false;
                    } else if (syncState.FileStatus.equals("SaveToFolder")) {
                        //ACK. Try not to update second time...(In-Progress)
                        dbMethods.UpdateTransStatus(syncState.RequestId, "A");
                        this.syncState.movedToInputFolder = true;
                        this.syncState.responseAckCount++;
                    }
                }
            } catch (IOException e) {

                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            } catch (Exception e) {

                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
        }
    }

    void writeToTextFile(String s) throws IOException {
       // File root = new File(Environment.getExternalStorageDirectory());
        File gpxfile = new File(AppState.logDirRef, "status_log.txt");
        FileWriter writer = new FileWriter(gpxfile);
        writer.append(s);
        writer.flush();
        writer.close();
    }

}