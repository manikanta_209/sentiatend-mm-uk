package com.sc.stmansi.sync;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.mainmenu.MainMenuActivity;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class LogFileUploadWorker extends Worker {

    private static final MediaType MEDIA_TYPE_MARKDOWN = MediaType.parse("text/x-markdown; charset=utf-8");

    private final OkHttpClient client = new OkHttpClient.Builder()
            .connectTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .build();

    public LogFileUploadWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        String path = Objects.requireNonNull(getInputData().getString("LOG_FILE_PATH"));
        File file = new File(path);
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(file));
            if (br.readLine() == null) {
                br.close();
                if (!file.delete()) {
                    Log.d("LogUploadWork", String.format("Empty log file %s could not be deleted", file.getPath()));
                }
                return Result.success(new Data.Builder()
                        .putBoolean("LOG_UPLOADED", false)
                        .putString("LOG_NOT_UPLOADED_CAUSE", "File is empty")
                        .build());
            }
        } catch (IOException e) {
            Log.e("LogUploadWork",
                    "Log file was not created or path provided is incorrect" + path, e);
        }

        HttpUrl url = new HttpUrl.Builder()
                .scheme("http")
                .host(MainMenuActivity.appState.webServiceIpAddress)
                .port(7201)
                .addPathSegment("logs")
                .build();
        RequestBody formBody = new MultipartBody.Builder()
                .setType(MediaType.parse("multipart/form-data"))
                .addFormDataPart(
                        "files",
                        path.substring(path.lastIndexOf("/") + 1),
                        RequestBody.create(file, MEDIA_TYPE_MARKDOWN))
                .build();
        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();

        try (Response response = client.newCall(request).execute()) {
            Log.d("LogUploadWork", response.body().string());
        } catch (IOException e) {
            Log.e("LogUploadWork", e.getMessage());
            if(e.getMessage().toLowerCase().contains("timeout")) {
                return Result.retry();
            }
        }
        return Result.success();
    }
}
