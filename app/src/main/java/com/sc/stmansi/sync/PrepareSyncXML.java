// 26Aug2019 Arpitha
package com.sc.stmansi.sync;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.provider.SyncStateContract.Constants;
import android.util.Base64;
import android.util.Log;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.sc.stmansi.configuration.SyncState;
import com.sc.stmansi.database.DBMethods;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.TblSyncMaster;
import com.sc.stmansi.tables.tbltransHeader;
import com.sc.stmansi.tables.tbltransdetail;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@SuppressLint({"SimpleDateFormat", "DefaultLocale"})
public class PrepareSyncXML {
	String xmlString = null;

	public String remReqId = "";

	private SQLiteDatabase db;
	private DBMethods dbMethods;
	private SyncXmlBuilder xmlBuilder;
	private SyncState syncState;
	private TblInstusers user;

	//	constructor
	public PrepareSyncXML(DatabaseHelper dbHelper, SyncState syncState, TblInstusers user) {
		db = dbHelper.getWritableDatabase();
		dbMethods = new DBMethods(dbHelper);
		this.syncState = syncState;
		this.user = user;
	}

	public PrepareSyncXML(DBMethods dbMethods) {
		this.dbMethods = dbMethods;
		this.db = this.dbMethods.getSQLiteDatabase();
		this.syncState = dbMethods.getSyncState();
		this.user = dbMethods.getUser();
	}

	// Prepares XML for PendingStatus
	public String WriteResponseXML() throws Exception {
		String reqId;
		try {
			reqId = dbMethods.getRequestId() + remReqId;
			if (reqId != null && reqId.length() > 0) {
				// get the status of last request
				prepareResponseXML(reqId);
				// set the current requestid to global variable
				syncState.RequestId = reqId;
				xmlString = xmlBuilder.end();
				// System.out.println("RESPONSE XML " + xmlString);

				return xmlString;
			}
		} catch (Exception e) {

			throw e;
		}
		return "";
	}

	//	write sync xml
	public String WriteSyncXML(String androidId, String UserId) throws Exception {
		String xFirstDate = null;
		String reqId = "";
		try {
			xFirstDate = dbMethods.getTransDate();

			if (xFirstDate != null && xFirstDate.trim().length() > 0) {
				int LastRequestNumber = getLastRequestNumber(UserId);
				reqId = androidId + UserId + String.format("%05d", LastRequestNumber + 1); // Create
				// a
				// new
				// RequestId

				// set the current requestid to global variable
				syncState.RequestId = reqId;

				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				String xTodaysDate = sdf.format(new Date());

				TblSyncMaster syncMaster = new TblSyncMaster();
				syncMaster.setRequestDate(xTodaysDate);
				syncMaster.setRequestStatusDate(xTodaysDate);
				syncMaster.setRequestStatus("N");
				syncMaster.setRequestId(reqId);
				syncMaster.setUserId(UserId);
				dbMethods.insertSyncMaster(syncMaster);

				dbMethods.updateLastRequestNo(LastRequestNumber + 1);

			} else {
				return "";
			}

			List<tbltransHeader> transheaderlist = dbMethods.getTransId(xFirstDate);


			xmlBuilder = new SyncXmlBuilder();
			xmlBuilder.openRow("Request");
			xmlBuilder.addColumn("RequestId", reqId);
			//				xmlBuilder.addColumn("DatabaseId", "stsvymdb");
			//03Oct2019 -Bindu - set db name from userstable
			xmlBuilder.addColumn("DatabaseId", user.getInst_Database());

			for (int i = 0; i < transheaderlist.size(); i++) {
				String xTranId = transheaderlist.get(i).getTransId();
				List<tbltransdetail> listTransDe = dbMethods.getTransDetailsData(xTranId);
				for (int j = 0; j < listTransDe.size(); j++) {
					String xAction = listTransDe.get(j).getAction(); // Action
					String xTableName = listTransDe.get(j).getTableName(); // Table Name
					if (xAction.equals("I")) {
						String xVAL = prepareXMLforInsert(xTableName, xTranId);
						if (xVAL == null) {
							return "ERR";
						}
					} else if (xAction.equals("U")) {
						String xUpdateSQL = listTransDe.get(j).getSQLStatement();
						if (xUpdateSQL != null && xUpdateSQL.length() > 0) {
							xUpdateSQL = xUpdateSQL.replaceAll("&", "&amp;");
							xUpdateSQL = xUpdateSQL.replaceAll("<", "&lt;");
							xUpdateSQL = xUpdateSQL.replaceAll(">", "&gt;");
							xUpdateSQL = xUpdateSQL.replaceAll("\"", "&quot;");
							xUpdateSQL = xUpdateSQL.replaceAll("\\\\", "\\\\\\\\");
						}
						prepareXMLforUpdate(xTableName, xTranId, xUpdateSQL);
					}

				}
			}

			xmlBuilder.openRow("/Request");
			xmlString = xmlBuilder.end();

			System.out.println("SEE THIS" + xmlString);
			Log.i(Constants._COUNT, "exporting database complete");

			dbMethods.updateTransHeader(reqId, xFirstDate);

			System.out.println("send Rid: " + reqId);

			writeTxtFile(xmlString);

			String SendMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + xmlString;
			SendMessage = new jEncrypt().jEncryptStr(SendMessage);
			writeEncryptedTxtFile(SendMessage);

			return xmlString;
		} catch (Exception e) {
			
			throw e;
		}
	}

	//   prepare xml for update
	private String prepareXMLforUpdate(String tableName, final String transId, final String updateSQL)
			throws Exception {
		try {
			tableName = tableName.toLowerCase();

			xmlBuilder.openRow(tableName + "nodes");
			xmlBuilder.openRow(tableName + "node");

			xmlBuilder.addColumn("tablename", tableName);
			xmlBuilder.addColumn("action", "U");

			xmlBuilder.addColumn("SQLSTATEMENT", updateSQL);

			xmlBuilder.openRow("/" + tableName + "node"); // Close
			xmlBuilder.openRow("/" + tableName + "nodes"); // Close

			xmlString = xmlBuilder.end();
			return xmlString;
		} catch (Exception e) {
			throw e;
		}
	}

	//	prepare response xml
	private void prepareResponseXML(final String reqId) throws IOException {
		xmlBuilder = new SyncXmlBuilder();
		xmlBuilder.openRow("Request");
		xmlBuilder.addColumn("RequestId", reqId);
		//		xmlBuilder.addColumn("DatabaseId", "stsvymdb");
		xmlBuilder.addColumn("DatabaseId", user.getInst_Database());
		xmlBuilder.openRow("/Request");
		xmlString = xmlBuilder.end();
	}

	/**
	 * XmlBuilder is used to write XML tags (open and close, and a few
	 * attributes) to a StringBuilder. Here we have nothing to do with IO or
	 * SQL, just a fancy StringBuilder.
	 *
	 * @author ccollins
	 */
	@SuppressWarnings("unused")
	static class SyncXmlBuilder {

		// private static final String SOAP_ENVELOPE = " <soap :Envelope
		// xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		// xmlns:xsd="http://www.w3.org/2001/XMLSchema"
		// xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">";

		private static final String OPEN_XML_STANZA = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
		private static final String CLOSE_WITH_TICK = ">";
		private static final String DB_OPEN = "<database name='";
		private static final String DB_CLOSE = "</database>";
		private static final String TABLE_OPEN = "<table name='";
		private static final String TABLE_CLOSE = "</table>";
		private static final String ROW_OPEN = "<row>";
		private static final String ROW_CLOSE = "</row>";
		private static final String COL_OPEN = "<col name='";
		private static final String COL_CLOSE = "</col>";

		private static final String OPEN_WITH_TICK = "<";

		private final StringBuilder sb;

		public SyncXmlBuilder() throws IOException {
			sb = new StringBuilder();
		}

		public String end() throws IOException {
			return sb.toString();
		}

		void openRow() {
			sb.append(SyncXmlBuilder.ROW_OPEN);
		}

		void openRow(String xVal) {
			sb.append(SyncXmlBuilder.OPEN_WITH_TICK + xVal + SyncXmlBuilder.CLOSE_WITH_TICK);
		}

		void closeRow() {
			sb.append(SyncXmlBuilder.ROW_CLOSE);
		}

		void addColumn(final String name, final String val) throws IOException {


			if (val == null) {

			} else {
				sb.append(SyncXmlBuilder.OPEN_WITH_TICK + name + SyncXmlBuilder.CLOSE_WITH_TICK + val
						+ SyncXmlBuilder.OPEN_WITH_TICK + "/" + name + SyncXmlBuilder.CLOSE_WITH_TICK);
			}

		}
	}

	// get last request number
	public int getLastRequestNumber(String anmId) throws Exception {
		int LastRequestNumber = 0;
		LastRequestNumber = dbMethods.getIntUsersValue(anmId, "LastRequestNumber");
		return LastRequestNumber;
	}

	// 		write to text file
	public void writeTxtFile(String sg) throws Exception {
		InputStream inputStream = new ByteArrayInputStream(sg.getBytes());
		@SuppressWarnings("unused")
		String state = Environment.getExternalStorageState();

		// if (Environment.MEDIA_MOUNTED.equals(state))
		// {
		// SDcard is available
		File f = new File(state + "/test.xml");
		try {
			f.createNewFile();
			OutputStream out = new FileOutputStream(f);
			byte[] buf = new byte[1024];
			int len;
			while ((len = inputStream.read(buf)) > 0)
				out.write(buf, 0, len);
			out.close();
			inputStream.close();
			System.out.println("\nFile is created...................................");

		} catch (IOException e) {
			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}

	}

	//	write encryted xml to file
	public void writeEncryptedTxtFile(String sg) throws Exception {
		InputStream inputStream = new ByteArrayInputStream(sg.getBytes());
		@SuppressWarnings("unused")
		String state = Environment.getExternalStorageState();

		// SDcard is available
		File f = new File(state + "/testEnc.txt");
		try {
			f.createNewFile();
			OutputStream out = new FileOutputStream(f);
			byte[] buf = new byte[1024];
			int len;
			while ((len = inputStream.read(buf)) > 0)
				out.write(buf, 0, len);
			out.close();
			inputStream.close();
			System.out.println("\nFile is created...................................");

		} catch (IOException e) {
			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}

	}

	//	prepares XML for insert
	private String prepareXMLforInsert(String tableName, final String transId) throws Exception {
		String returnValue = "";
		String str = "";

		tableName = tableName.toLowerCase();

		String inputSQL = "SELECT * FROM " + tableName + " WHERE TransId = " + transId;
		System.out.println("inputSQL: " + inputSQL);
		try {
			Cursor c = db.rawQuery(inputSQL, new String[0]);
			if (c.moveToFirst()) {
				int cols = c.getColumnCount();

				xmlBuilder.openRow(tableName + "nodes");
				do {

					xmlBuilder.openRow(tableName + "node");
					xmlBuilder.addColumn("tablename", tableName);
					xmlBuilder.addColumn("action", "I");

					for (int i = 0; i < cols; i++) {
//						if ((tableName.toLowerCase().equals("tblregisteredwomen")) && (i == 3)) {
//							System.out.println("col - blob" + c.getColumnName(i));
//							System.out.println("col - Length" + c.getType(i));
//							if (c.getType(i) > 0) { // Null = 0
//
//								@SuppressWarnings("unused")
//								ByteArrayOutputStream baos = new ByteArrayOutputStream();
//
//								byte[] b = c.getBlob(4);
//								String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
//
//								str = encodedImage;
//
//							}
//							xmlBuilder.addColumn(c.getColumnName(i).toLowerCase(), str);
//						} else
					if (!c.getColumnName(i).toLowerCase().equalsIgnoreCase("serverreceiveddate")) {
							String mStr = c.getString(i);

							if (mStr != null && mStr.length() > 0) {
								mStr = mStr.replaceAll("&", "&amp;");
								mStr = mStr.replaceAll("<", "&lt;");
								mStr = mStr.replaceAll(">", "&gt;");
								mStr = mStr.replaceAll("\"", "&quot;");
								mStr = mStr.replaceAll("'", "''");
								mStr = mStr.replaceAll("\\\\", "\\\\\\\\");
							}

							xmlBuilder.addColumn(c.getColumnName(i).toLowerCase(), mStr);
						}
					}
					xmlBuilder.openRow("/" + tableName + "node"); // Close
				} while (c.moveToNext());
				xmlBuilder.openRow("/" + tableName + "nodes"); // Close
			}
			c.close();
		} catch (Exception e) {
			throw e;
		}
		return returnValue;
	}
}

