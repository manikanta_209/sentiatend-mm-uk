//26Aug2019 Arpitha - removed unused/commented code
package com.sc.stmansi.sync;

import android.annotation.SuppressLint;
import android.util.Base64;
import android.util.Log;

import org.apache.http.util.EncodingUtils;

import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public class jEncrypt {


	@SuppressLint("TrulyRandom")
	public String jEncryptStr(String xInputStr){
		try {
			//Mani 23/2/2021 changed instance to AES/ECB/PKCS5Padding
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			SecretKeySpec myKey = new SecretKeySpec(md5("app@SG!2018"),"AES");

			cipher.init(Cipher.ENCRYPT_MODE, myKey);

			try {
				byte[] encryptedPlainText = cipher.doFinal( xInputStr.getBytes());

				String encrypted = Base64.encodeToString(encryptedPlainText, 0);
				Log.i("ENCRYPT", "Pwd encrypted: "+encrypted);
				return encrypted;

			} catch (IllegalBlockSizeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "";
			} catch (BadPaddingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "";
			}

		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}


	public static final byte[] md5(String s) {
		try {

			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] messageDigest = md.digest(s.getBytes(StandardCharsets.UTF_8));

			String md5 = EncodingUtils.getString(messageDigest, "UTF-8");

			Log.i("Function MD5", md5);
			Log.i("Function MD5 Length","Length: "+ md5.length());

			return messageDigest;

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
	}

}