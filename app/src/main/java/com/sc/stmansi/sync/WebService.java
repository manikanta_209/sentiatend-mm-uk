// 26AUg2019 Arpitha
package com.sc.stmansi.sync;


import android.util.Log;

import com.google.gson.Gson;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.configuration.SyncState;
import com.sc.stmansi.database.DBMethods;
import com.sc.stmansi.sync.dtos.GetComplicationInfoRequest;
import com.sc.stmansi.sync.dtos.GetDeliveryInfoRequest;
import com.sc.stmansi.sync.dtos.GetLoginSettingsRequest;
import com.sc.stmansi.sync.dtos.RESTRequest;
import com.sc.stmansi.tables.TblInstusers;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class WebService {

    private final AppState appState;
    private final DBMethods dbMethods;
    private final TblInstusers user;
    private final SyncState syncState;

    private OkHttpClient httpClient;
    private Gson gson;

    public WebService(DBMethods dbMethods, TblInstusers user, SyncState syncState, AppState appstate) {
        this.dbMethods = dbMethods;
        this.user = user;
        this.syncState = syncState;
        this.appState = appstate;

        initDependencies();//14Jun2021 Arpitha
    }

    private void initDependencies() {
        httpClient = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();
        gson = new Gson();
    }

    public String forwardMessageOrGetDataFromServer(String xMessage, String webMethName) {
        try {
            return invokeRESTService(xMessage, webMethName);
        } catch (Exception e) {
            Log.e("WebService", String.format("Problem invoking %s", webMethName), e);
        }
        return null;
    }

    public String invokeLoginOrSettings(String emailId, String pswd, String serverDb, String androidId,
                                        String webMethName, String URL, boolean isLogin, String lastModifiedDate) throws Exception {
        GetLoginSettingsRequest r = new GetLoginSettingsRequest();
        if (webMethName.equalsIgnoreCase("GetSettingsForUser") ||
                webMethName.equalsIgnoreCase("GetVillagesForUser") ||
                webMethName.equalsIgnoreCase("getReferralcenters")) {
            r.UserId = emailId;
            r.lastUpdatedTime = lastModifiedDate;
            r.dbname = serverDb;

            if (webMethName.equalsIgnoreCase("GetSettingsForUser")) {
                r.ashaId = user.getAshaId();
            }
        } else {
            if (webMethName.equalsIgnoreCase("ValidUserForDownLoad")) {
                r.ashaId = emailId;
            } else {
                r.mobileNumber = emailId;
            }

            if (isLogin) {
                r.Pswd = pswd;
            } else {
                r.lastUpdatedTime = lastModifiedDate;
            }
            r.DBname = serverDb;
            r.DeviceId = androidId;
        }
        return invokeRESTService2(r, webMethName);
    }

    public String getComplicationInfo(String userId, String lastModifiedDate, String serverDb) throws Exception {
        GetComplicationInfoRequest r = new GetComplicationInfoRequest();
        r.UserId = userId;
        r.dateLastupdated = lastModifiedDate;
        r.dbname = serverDb;

        return invokeRESTService2(r, "GetHRPComplicationActionData");
    }

    public String getDeliveryInfo(String thayi, String dbName) throws Exception {
        GetDeliveryInfoRequest r = new GetDeliveryInfoRequest();
        r.ThayiID = thayi;
        r.isWomanId = SyncFunctions.isWomanId;
        r.dbname = dbName;
        return invokeRESTService2(r, "GetDeliveryInfo");
    }

    private String invokeRESTService(String xMessage, String webMethName) throws Exception {
        if (webMethName == null) {
            throw new Exception("REST Endpoint path segment cannot be null");
        }
        if (webMethName.equalsIgnoreCase("MessageForwarder")) {
            dbMethods.UpdateTransStatus(syncState.RequestId, "S"); //27Apr2021 Bindu))
        }
        final MediaType PLAIN
                = MediaType.get("text/plain; charset=utf-8");
        RequestBody requestBody = RequestBody.create(xMessage, PLAIN);
        HttpUrl url = new HttpUrl.Builder()
                .scheme("http")
                .host(appState.webServiceIpAddress)
                .port(appState.apiPort)
                .addPathSegment(webMethName)
                .build();
        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();

        try (Response response = httpClient.newCall(request).execute()) {
            String responseXML = response.body().string();
            response.close();
            return responseXML;
        } catch (IOException e) {
            Log.e("WebService", "Something went wrong when calling/reading response or during client.newCall invocation");
            if (e.getMessage().toLowerCase().contains("failed to connect"))
                return e.getMessage();
        }
        return null;

    }

    private String invokeRESTService2(RESTRequest r, String webMethName) throws Exception {
        if (webMethName == null) {
            throw new Exception("REST Endpoint path segment cannot be null");
        }
        final MediaType JSON
                = MediaType.get("application/json; charset=utf-8");
        RequestBody requestBody = RequestBody.create(gson.toJson(r), JSON);
        HttpUrl url = new HttpUrl.Builder()
                .scheme("http")
                .host(appState.webServiceIpAddress)
                .port(appState.apiPort)
                .addPathSegment(webMethName)
                .build();
        Request request = new Request.Builder()
                .url(url) // must parameterize, ipconfig
                .post(requestBody)
                .build();
        try (Response response = httpClient.newCall(request).execute()) {
            return response.body().string();
        } catch (IOException e) {
            Log.e("WebService", e.getMessage());
            if (e.getMessage().toLowerCase().contains("failed to connect"))
                return e.getMessage();
            else return null;
        }
//        return null;
    }
}
