//26Aug2019 Arpitha - removed unused/commented code
package com.sc.stmansi.sync;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.configuration.LoginData;
import com.sc.stmansi.configuration.SyncState;
import com.sc.stmansi.database.DBMethods;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.tables.TblInstusers;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class DSSyncFunction {

	private String displayText;
	private ArrayList<String> instDetails = new ArrayList<>();
	private ArrayList<String> userDetails = new ArrayList<>();
	private ArrayList<ContentValues> dsDetails = null;
	private ArrayList<ContentValues> arrVillage = null;
	private SQLiteDatabase db;
	private DBMethods dbMethods;
	private SyncState syncState;
	private AppState appState;
	private TblInstusers user;
	private DatabaseHelper dbHelper;

	// 	constructor
	public DSSyncFunction(DatabaseHelper databaseHelper, AppState appState, SyncState syncState, TblInstusers user) {
		this.dbHelper = databaseHelper;
		this.appState = appState;
		this.syncState = syncState;
		this.user = user;
		this.db = dbHelper.getWritableDatabase();
		this.dbMethods = new DBMethods(dbHelper);
	}

	//display object
	public void displayDsObject(String xmlStr) throws Exception {
		String xml = xmlStr;

		if(xml.toLowerCase().contains("userid") || xml.toLowerCase().contains("institutename")) {

			xml = xml.replace("<NewDataSet>", "")
					.replace("</NewDataSet>", "").
							replace("<NewDataSet />", "").trim();

			String[] strVal;
			strVal = xml.split("</Table>");
			for (String s : strVal) {
				s = s + "</Table>";
				InputStream is = new ByteArrayInputStream(s.getBytes(StandardCharsets.UTF_8));

				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

				Document doc = dBuilder.parse(is);
				doc.getDocumentElement().normalize();

				NodeList nList = doc.getElementsByTagName("Table");


				if (nList.getLength() > 0) {

					for (int temp = 0; temp < nList.getLength(); temp++) {

						Node nNode = nList.item(temp);

						String stemp = " Update tblinstusers Set ";
						String dtemp = "update tblinstitutiondetails Set ";

						ContentValues values = new ContentValues();

						if (nNode.getNodeType() == Node.ELEMENT_NODE) {

							Element eElement = (Element) nNode;

							if (eElement.getElementsByTagName("UserId").item(0) != null) {

								stemp = stemp + " UserName = '" + getTagValue(eElement, "UserName") + "', ";

								stemp = stemp + " UserAge = '" + getTagValue(eElement, "UserAge") + "', ";
								stemp = stemp + " UserGender = '" + getTagValue(eElement, "UserGender") + "', ";
								stemp = stemp + " UserGovtIdType = '" + getTagValue(eElement, "UserGovtIdType") + "', ";
								stemp = stemp + " UserGovtId = '" + getTagValue(eElement, "UserGovtId") + "', ";
								stemp = stemp + " UserId = '" + getTagValue(eElement, "UserId") + "', ";
								//	stemp = stemp + " Password = '" + getTagValue(eElement, "Password") + "', ";05Dec2019 Arpitha
								stemp = stemp + " UserRole = '" + getTagValue(eElement, "UserRole") + "', ";
								stemp = stemp + " Address = '" + getTagValue(eElement, "Address") + "', ";
								stemp = stemp + " Pincode = '" + getTagValue(eElement, "Pincode") + "', ";
								stemp = stemp + " Country = '" + getTagValue(eElement, "Country") + "', ";
								stemp = stemp + " State = '" + getTagValue(eElement, "State")
										+ "', ";
								stemp = stemp + " District = '" + getTagValue(eElement, "District") + "', ";
								stemp = stemp + " SubDistrict = '"
										+ getTagValue(eElement, "SubDistrict") + "', ";
								stemp = stemp + " FacilityType = '" + getTagValue(eElement, "FacilityType") + "', ";
								stemp = stemp + " FacilityName = '" + getTagValue(eElement, "FacilityName") + "', ";

								stemp = stemp + " InstituteId = '" + getTagValue(eElement, "InstituteId") + "', ";


								stemp = stemp + " Inst_Database = '" + getTagValue(eElement, "Inst_Database") + "', ";

								stemp = stemp + " Bankname = '" + getTagValue(eElement, "Bankname") + "', ";
								stemp = stemp + " Branchname = '" + getTagValue(eElement, "Branchname") + "', ";
								stemp = stemp + " Ifsccode = '" + getTagValue(eElement, "Ifsccode") + "', ";
								stemp = stemp + " Accountno = '" + getTagValue(eElement, "Accountno") + "', ";
								stemp = stemp + " isValidated = '" + getTagValue(eElement, "isValidated") + "', ";
								stemp = stemp + " UserValidatedDatetime = '" + getTagValue(eElement, "UserValidatedDatetime") + "', ";
								stemp = stemp + " EmailId = '" + getTagValue(eElement, "EmailId") + "', ";
								stemp = stemp + " LastActivityNumber = '" + getTagValue(eElement, "LastActivityNumber") + "', ";
								stemp = stemp + " UserMobileNumber = '" + getTagValue(eElement, "UserMobileNumber") + "', ";
								stemp = stemp + " UserGovtIdType = '" + getTagValue(eElement, "UserGovtIdType") + "', ";


								stemp = stemp + " UserGovtId = '" + getTagValue(eElement, "UserGovtId") + "', ";
								stemp = stemp + " Bankname = '" + getTagValue(eElement, "Bankname") + "', ";

								stemp = stemp + " Branchname = '" + getTagValue(eElement, "Branchname") + "', ";

								stemp = stemp + " Ifsccode = '" + getTagValue(eElement, "Ifsccode") + "', ";

								stemp = stemp + " Accountno = '" + getTagValue(eElement, "Accountno") + "', ";


								stemp = stemp + " isTabLost = '" + getTagValue(eElement, "isTabLost") + "', ";
								stemp = stemp + " TabLostDate = '" + getTagValue(eElement, "TabLostDate") + "', ";
								stemp = stemp + " TabLostDate = '" + getTagValue(eElement, "TabLostDate") + "', ";
								stemp = stemp + " isDeactivated = '" + getTagValue(eElement, "isDeactivated") + "', ";
								stemp = stemp + " DeactivatedReason = '" + getTagValue(eElement, "DeactivatedReason") + "', ";
								stemp = stemp + " datecreated = '" + getTagValue(eElement, "datecreated") + "', ";
								stemp = stemp + " createdby = '" + getTagValue(eElement, "createdby") + "', ";
								stemp = stemp + " dateupdated = '" + getTagValue(eElement, "dateupdated") + "', ";
								stemp = stemp + " updatedby = '" + getTagValue(eElement, "updatedby") + "', ";


								stemp = stemp + " isDeactivated = '" + getTagValue(eElement, "isDeactivated")
										+ "' ";


								stemp = stemp + " Where AshaId = '" + getTagValue(eElement, "AshaId") + "' ";
								stemp = stemp + "; ";

								userDetails.add(stemp);

							} else if (eElement.getElementsByTagName("InstituteName").item(0) != null) {

								dtemp = dtemp + " ipAddress = \"" + getTagValue(eElement, "ipAddress") + "\" ";
								dtemp = dtemp + ", InstituteId = \"" + getTagValue(eElement, "InstituteId") + "\" ";
								dtemp = dtemp + ", InstituteName = \"" + getTagValue(eElement, "InstituteName") + "\" ";
								dtemp = dtemp + ", Country = \"" + getTagValue(eElement, "Country") + "\" ";
								dtemp = dtemp + ", State = \"" + getTagValue(eElement, "State") + "\" ";

								dtemp = dtemp + ", District = \"" + getTagValue(eElement, "District") + "\" ";
								dtemp = dtemp + ", Subdistrict = \"" + getTagValue(eElement, "Subdistrict") + "\" ";
								dtemp = dtemp + ", Address = \"" + getTagValue(eElement, "Address") + "\" ";
								dtemp = dtemp + ", Pincode = \"" + getTagValue(eElement, "Pincode") + "\" ";

								dtemp = dtemp + " , Databasename = '" + getTagValue(eElement, "Databasename") + "' ";

//11Aug2019 - bindu - reset proper column names to retrieve and set
								dtemp = dtemp + " , ContactName = \"" + getTagValue(eElement, "ContactName") + "\" ";
								dtemp = dtemp + ", EmailId = \"" + getTagValue(eElement, "EmailId") + "\" ";
								dtemp = dtemp + ", PhoneNumber = \"" + getTagValue(eElement, "PhoneNumber") + "\" ";
								dtemp = dtemp + ", Comments = \"" + getTagValue(eElement, "Comments") + "\" ";
								dtemp = dtemp + ", Website = \"" + getTagValue(eElement, "Website") + "\" ";

								dtemp = dtemp + ", TypeofInstitution = \"" + getTagValue(eElement, "TypeofInstitution") + "\" ";
								dtemp = dtemp + ", Programstartdate = \"" + getTagValue(eElement, "Programstartdate") + "\" ";
								dtemp = dtemp + ", Programenddate = \"" + getTagValue(eElement, "Programenddate") + "\" ";

								dtemp = dtemp + " , isDeactivated = '" + getTagValue(eElement, "isDeactivated") + "' ";


								dtemp = dtemp + ", DeactivatedReason = \"" + getTagValue(eElement, "DeactivatedReason") + "\" ";

								dtemp = dtemp + ", DeactivatedDate = \"" + getTagValue(eElement, "DeactivatedDate") + "\" ";
								dtemp = dtemp + ", datecreated = \"" + getTagValue(eElement, "datecreated") + "\" ";
								dtemp = dtemp + ", createdby = \"" + getTagValue(eElement, "createdby") + "\" ";
								dtemp = dtemp + ", dateupdated = \"" + getTagValue(eElement, "dateupdated") + "\" ";

								dtemp = dtemp + ", updatedby = \"" + getTagValue(eElement, "updatedby") + "\" ";

//							Manikanta 04Feb2020 - Add Eval start date and End date
								dtemp = dtemp + ", IsEvalPeriod = \"" + getTagValue(eElement, "IsEvalPeriod") + "\" ";
								dtemp = dtemp + ", EvalStartDate = \"" + getTagValue(eElement, "EvalStartDate") + "\" ";
								dtemp = dtemp + " , EvalEndDate = '" + getTagValue(eElement, "EvalEndDate") + "' ";


								dtemp = dtemp + "; ";

								instDetails.add(dtemp);

							} else if (eElement.getElementsByTagName("VillageName").item(0) != null) {

								values.put("UserId", getTagValue(eElement, "userid"));
								values.put("VillageId", getTagValue(eElement, "VillageId"));
								values.put("VillageName", getTagValue(eElement, "VillageName"));
								values.put("LocalName", getTagValue(eElement, "LocalName"));
								values.put("LastUpdatedDate", getTagValue(eElement, "LastUpdatedDate"));

								arrVillage.add(values);

							} else {

								values.put("DangerId", getTagValue(eElement, "DangerId"));
								values.put("LoggedInUser", getTagValue(eElement, "LoggedInUser"));
								values.put("Facility", getTagValue(eElement, "Facility"));
								values.put("ActionDateTime", getTagValue(eElement, "ActionDateAndTime"));
								values.put("Comments", getTagValue(eElement, "Comments"));
								values.put("Status", getTagValue(eElement, "Status"));
								values.put("RefferedTo", getTagValue(eElement, "RefferedTo"));

								int count = 0;
								if (count <= 0)
									dsDetails.add(values);
							}

						}
					}
				} else {
					userDetails = null;
					dsDetails = null;
				}
			}
		}

	}
	// 	get tag values
	public String getTagValue(Element eElement, String tagName) {
		if (eElement.getElementsByTagName(tagName).item(0) != null)
			return eElement.getElementsByTagName(tagName).item(0).getTextContent();
		else
			return "";
	}

	// fetch settings data from server
	public String SyncFunctionSettings(LoginData loginData, String xIPandURLAddress, String serverDb, Context Cx)
			throws Exception {
		try {
			String lastUpdateDatetime;
			lastUpdateDatetime = "";
			//mani 23/2/2021 changed method parameter
			WebService webService = new WebService(dbMethods, user, syncState,appState);
			displayText = webService.invokeLoginOrSettings(user.getUserId(),
					"" + loginData.password, serverDb, "", "GetSettingsForUser", xIPandURLAddress,
					false, lastUpdateDatetime);
			System.out.println("GetSettingsForUser  " + displayText);
			updateSettingsResponse(Cx);
			SyncFunctionVillages(loginData.ashaId, xIPandURLAddress,
					appState.webServiceDBbMster, Cx);
			SyncReferral(loginData.ashaId, xIPandURLAddress,
					appState.webServiceDBbMster, Cx); //09Apr2021 Bindu - Sync referral centers
		} catch (Exception e) {
			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
			crashlytics.log(e.getMessage());
			throw e;
		}
		return displayText;
	}

	// update to settings
	private void updateSettingsResponse(Context Cx) throws Exception {
		if (displayText.equals("Error")) {

		} else {
			displayDsObject(displayText);
			if (userDetails.size() > 0) {
				for (String str : userDetails) {
					db.execSQL(str);
				}
			}
			if (instDetails.size() > 0) {
				for (String str : instDetails) {
					db.execSQL(str);
				}
			}
		}
	}

	// update villages data
	public String SyncFunctionVillages(String xUserId, String xIPandURLAddress, String serverDb, Context Cx)
			throws Exception {
		try {
			String lastUpdateDatetime = dbMethods.getLastupdatedDatetimeSettings(user.getUserId());
			lastUpdateDatetime = "";
			//mani 23/2/2021 changed method parameter
			WebService webService = new WebService(dbMethods, user, syncState,appState);
			displayText = webService.invokeLoginOrSettings(user.getUserId(),
					"" + user.getPassword(), serverDb, "", "GetVillagesForUser", xIPandURLAddress,
					false, lastUpdateDatetime);
			System.out.println("GetVillagesForUser  " + displayText);
			updateUsersResponseVillages(Cx, displayText);
		} catch (Exception e) {
			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
			crashlytics.log(e.getMessage());
			throw e;
		}
		return displayText;
	}

	// 09Apr2021 Bindu - Sync Referral details
	public String SyncReferral(String xUserId, String xIPandURLAddress, String serverDb, Context Cx)
			throws Exception {
		try {
//			//String lastUpdateDatetime = dbMethods.getLastupdatedDatetimeSettings(user.getUserId());
			String lastUpdateDatetime = dbMethods.getReferralLastupdatedDatetime();

			//	lastUpdateDatetime = "";
			WebService webService = new WebService(dbMethods, user, syncState,appState);
			displayText = webService.invokeLoginOrSettings(user.getUserId(),
					"" + user.getPassword(), serverDb, "", "getReferralcenters", xIPandURLAddress,
					false, lastUpdateDatetime);
			System.out.println("getReferralcenters  " + displayText);
			updateReferralcenters(Cx, displayText);
		} catch (Exception e) {
			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
			crashlytics.log(e.getMessage());
			throw e;
		}
		return displayText;
	}

	// update users response
	private void updateUsersResponseVillages(Context Cx, String displaytext)
			throws Exception {
		if (displayText == "Error") {

		} else {

			try (InputStream is = new ByteArrayInputStream(displaytext.getBytes(StandardCharsets.UTF_8))) {
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(is);
				doc.getDocumentElement().normalize();

				NodeList nList = doc.getElementsByTagName("Table");

				instDetails = new ArrayList<>();
				userDetails = new ArrayList<>();
				dsDetails = new ArrayList<>();
				arrVillage = new ArrayList<>();

				if (nList.getLength() > 0) {

					for (int temp = 0; temp < nList.getLength(); temp++) {

						Node nNode = nList.item(temp);

						ContentValues values = new ContentValues();

						Element eElement = (Element) nNode;

						if (eElement.getElementsByTagName("FacilityName").item(0) != null) {

							values.put("AutoId", getTagValue(eElement, "AutoId"));
							values.put("UserId", getTagValue(eElement, "UserId"));
							values.put("FacilityName", getTagValue(eElement, "FacilityName"));
							values.put("LocalName", getTagValue(eElement, "LocalName"));
							values.put("ANMName", getTagValue(eElement, "ANMName"));
							values.put("ANMContactNo", getTagValue(eElement, "ANMContactNo"));

							values.put("BEmOCType", getTagValue(eElement, "BEmOCType"));
							values.put("BEmOCName", getTagValue(eElement, "BEmOCName"));
							values.put("BEmOCDoctor", getTagValue(eElement, "BEmOCDoctor"));
							values.put("BEmOCContactNumber", getTagValue(eElement, "BEmOCContactNumber"));
							values.put("BEmOCDistance", getTagValue(eElement, "BEmOCDistance"));
							values.put("CEmOCType", getTagValue(eElement, "CEmOCType"));
							values.put("CEmOCName", getTagValue(eElement, "CEmOCName"));
							values.put("CEmOCDoctor", getTagValue(eElement, "CEmOCDoctor"));
							values.put("CEmOCContactNumber", getTagValue(eElement, "CEmOCContactNumber"));
							values.put("CEmOCDistance", getTagValue(eElement, "CEmOCDistance"));

							values.put("Country", getTagValue(eElement, "Country"));
							values.put("State", getTagValue(eElement, "State"));
							values.put("District", getTagValue(eElement, "District"));
							values.put("Subdistrict", getTagValue(eElement, "Subdistrict"));

							arrVillage.add(values);

						}
					}
				}

				if (arrVillage != null && arrVillage.size() > 0) {
					for (ContentValues values1 : arrVillage) {

						values1.put("datecreated", DateTimeUtil.getTodaysDate()+" "+ DateTimeUtil.getCurrentTime());//Arpitha 06Jan2019
						Object[] arr = {values1.getAsString("UserId"),
								values1.getAsString("LocalName")};
						String SQL147 = "Delete From tblfacilitydetails where " +
								" userid = ? and LocalName = ? ";

						db.execSQL(SQL147, arr);
						db.insert("tblfacilitydetails", null, values1);
					}
				}
			}
		}
	}

	// Bindu - add Sync func for compl
	public String SyncFunctionCompl(DBMethods dbh, String xUserId, String dbName, String xIPAddress, String datelastupdated) throws Exception {
		String displayText = "";
		try {
			//mani 23/2/2021 changed method parameter and changed method name to getComplicationInfo from invokeComplMgmtWS
			WebService webService = new WebService(dbMethods, user, syncState,appState);
			displayText = webService.getComplicationInfo(xUserId, datelastupdated, dbName);
			updateResponseComplMgmt(displayText);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return displayText;
	}

	// Update Response from server to table
	private void updateResponseComplMgmt(String displayText) {
		if (displayText == "Error") {
			this.syncState.movedToInputFolderComplmgmt = false;
		} else {
			try {
				displayText = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + displayText;
				updateComplMgmtDetails(displayText);
			}catch (Exception e) {

				FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
				crashlytics.log(e.getMessage());
			}
		}

	}

	private void updateComplMgmtDetails(String xmlStr)  throws Exception {

		InputStream is = new ByteArrayInputStream(xmlStr.getBytes("UTF-8"));

		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(is);
		doc.getDocumentElement().normalize();

		NodeList nList = doc.getElementsByTagName("Table");

		ArrayList<ContentValues> complmgmtUpdate = new ArrayList<ContentValues>();

		if (nList.getLength() > 0) {

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				ContentValues values = new ContentValues();

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					{
						values.put("AutoId", getTagValue(eElement, "AutoId"));
						values.put("UserId", getTagValue(eElement, "UserId"));
						values.put("WomanId", getTagValue(eElement, "WomanId"));
						values.put("VisitType", getTagValue(eElement, "VisitType"));
						values.put("VisitNum", getTagValue(eElement, "VisitNum"));
						values.put("VisitDate", getTagValue(eElement, "VisitDate"));
						values.put("VisitWomanDangerSigns", getTagValue(eElement, "VisitWomanDangerSigns"));

						values.put("VisitChildDangerSigns", getTagValue(eElement, "VisitChildDangerSigns"));
						values.put("hcmActTakByUserType", getTagValue(eElement, "hcmActTakByUserType"));
						values.put("hcmActTakByUserName", getTagValue(eElement, "hcmActTakByUserName"));
						values.put("hcmActTakAtFacility", getTagValue(eElement, "hcmActTakAtFacility"));
						values.put("hcmActTakFacilityName", getTagValue(eElement, "hcmActTakFacilityName"));
						if (eElement.getElementsByTagName("hcmActTakDate").item(0) != null) {
							//DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ");
							DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd"); //26Apr2021 Bindu date format change
							String string1 = getTagValue(eElement, "hcmActTakDate");
							Date result = df1.parse(string1);
							SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
							//sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
							values.put("hcmActTakDate", sdf.format(result));
						}
						if (eElement.getElementsByTagName("hcmActTakTime").item(0) != null) {
							String actiontime = getTagValue(eElement, "hcmActTakTime");
							values.put("hcmActTakTime", getTimeFromString(actiontime));
						}
						//values.put("hcmActTakTime", getTagValue(eElement, "hcmActTakTime"));

						values.put("hcmActTakForCompl", getTagValue(eElement, "hcmActTakForCompl"));
						values.put("hcmActTakForHRP", getTagValue(eElement, "hcmActTakForHRP"));
						values.put("hcmActTakStatus", getTagValue(eElement, "hcmActTakStatus"));
						if (eElement.getElementsByTagName("hcmActTakDate").item(0) != null) {
							//DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ");
							DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
							String string1 = getTagValue(eElement, "hcmActTakStatusDate");
							Date result = df1.parse(string1);
							SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
							//sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
							values.put("hcmActTakStatusDate", sdf.format(result));
						}
						//values.put("hcmActTakStatusDate", getTagValue(eElement, "hcmActTakStatusDate"));
						if (eElement.getElementsByTagName("hcmActTakStatusTime").item(0) != null) {
							String statustime = getTagValue(eElement, "hcmActTakStatusTime");
							values.put("hcmActTakStatusTime", getTimeFromString(statustime));
						}
						//values.put("hcmActTakStatusTime", getTagValue(eElement, "hcmActTakStatusTime"));
						values.put("hcmActTakReferredToFacility", getTagValue(eElement, "hcmActTakReferredToFacility"));
						values.put("hcmActTakReferredToFacilityName", getTagValue(eElement, "hcmActTakReferredToFacilityName"));

						values.put("hcmActTakComments", getTagValue(eElement, "hcmActTakComments"));
						values.put("hcmActionToBeTakenClient", getTagValue(eElement, "hcmActionToBeTakenClient"));
						values.put("hcmClientReceivedDate", DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
						values.put("ServerCreatedDate", getTagValue(eElement, "ServerCreatedDate"));
						values.put("ServerUpdatedDate", getTagValue(eElement, "ServerUpdatedDate"));

						complmgmtUpdate.add(values);
					}

				}
			}

			if (complmgmtUpdate != null && complmgmtUpdate.size() > 0) {
				for (ContentValues values1 : complmgmtUpdate) {
					Object[] arr = {values1.getAsString("UserId"),
							values1.getAsString("LocalName")};
					db.insert("tblhrpcomplicationmgmt", null, values1);
					//	long result = db.insert("tblhrpcomplicationmgmt", null, values1);
					//	if(result > 0) {
					//pending update received date
					//	}
				}
			}
		} else {
			complmgmtUpdate = null;
		}
	}

	private String getTimeFromString(String duration) {
		// TODO Auto-generated method stub
		String time = "";
		boolean hourexists = false, minutesexists = false, secondsexists = false;
		if (duration.contains("H"))
			hourexists = true;
		if (duration.contains("M"))
			minutesexists = true;
		/*if (duration.contains("S"))
			secondsexists = true;*/
		if (hourexists) {
			String hour = "";
			hour = duration.substring(duration.indexOf("T") + 1,
					duration.indexOf("H"));
			if (hour.length() == 1)
				hour = "0" + hour;
			time += hour + ":";
		}
		if (minutesexists) {
			String minutes = "";
			if (hourexists)
				minutes = duration.substring(duration.indexOf("H") + 1,
						duration.indexOf("M"));
			else
				minutes = duration.substring(duration.indexOf("T") + 1,
						duration.indexOf("M"));
			if (minutes.length() == 1)
				minutes = "0" + minutes;
			time += minutes ;
		} else {
			time += "00";
		}

		return time;
	}

	//09Apr2021 Bindu - update referral table details
	// update users response
	private void updateReferralcenters(Context Cx, String displaytext)
			throws Exception {
		if (displaytext.toLowerCase().contains("facilitytype")) {
			if (displayText == "Error") {

			} else {
				try (InputStream is = new ByteArrayInputStream(displaytext.getBytes(StandardCharsets.UTF_8))) {
					DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
					DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
					Document doc = dBuilder.parse(is);
					doc.getDocumentElement().normalize();

					NodeList nList = doc.getElementsByTagName("Table");

					arrVillage = new ArrayList<>();

					if (nList.getLength() > 0) {

						for (int temp = 0; temp < nList.getLength(); temp++) {

							Node nNode = nList.item(temp);

							ContentValues values = new ContentValues();

							Element eElement = (Element) nNode;

							if (eElement.getElementsByTagName("FacilityName").item(0) != null) {
								values.put("AutoId", getTagValue(eElement, "AutoId"));
								values.put("FacilityType", getTagValue(eElement, "FacilityType"));
								values.put("FacilityName", getTagValue(eElement, "FacilityName"));
								values.put("Country", getTagValue(eElement, "Country"));
								values.put("State", getTagValue(eElement, "State"));
								values.put("District", getTagValue(eElement, "District"));
								values.put("Subdistrict", getTagValue(eElement, "Subdistrict"));
								values.put("datecreated", getTagValue(eElement, "datecreated"));
								values.put("createdby", getTagValue(eElement, "createdby"));
								values.put("dateupdated", getTagValue(eElement, "dateupdated"));
								values.put("updatedby", getTagValue(eElement, "updatedby"));
								arrVillage.add(values);
							}
						}
					}

					if (arrVillage != null && arrVillage.size() > 0) {
						for (ContentValues values1 : arrVillage) {
							db.insert("tblreferralFacility", null, values1);
						}
					}
				}
			}
		}


	}
}