// 26Aug2019 Arpitha
package com.sc.stmansi.sync;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.provider.SyncStateContract.Constants;
import android.util.Base64;
import android.util.Log;

import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.configuration.SyncState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.tables.TblInstusers;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;


@SuppressLint({ "SimpleDateFormat", "DefaultLocale" })
public class PrepareDSSyncXML {
	
	String xmlString = null;

	private TblInstusers user;
	private AppState appState;

	private SyncXmlBuilder xmlBuilder;
	private SQLiteDatabase db;
	private SyncState syncState;
	private DatabaseHelper dbHelper;

//	constructor
	public PrepareDSSyncXML(DatabaseHelper dbHelper, TblInstusers user, AppState appState, SyncState syncState) {
		this.user = user;
		this.appState = appState;
		db = dbHelper.getWritableDatabase();
		this.syncState = syncState;
		this.dbHelper = dbHelper;
	}

	// Prepares XML for PendingStatus
	public String  WriteResponseXML(boolean isResponse) throws Exception  {
		String SQLStr = null;
		String reqId ;
		String settingsLUD = null, dangLUD = null, villageLUD = null;

		Cursor c4 = null;
		try {
			SQLStr = "SELECT RequestId FROM tblSyncMasterDS WHERE RequestStatus = 'A' or RequestStatus = 'S' " ;
			Cursor c1 = db.rawQuery(SQLStr, null);

			SQLStr = "SELECT max(LastUpdatedDate) FROM tblVillages WHERE  UserId = '" + user.getUserId()+ "' " ;
			c4 = db.rawQuery(SQLStr, null);
			if(c4.moveToFirst() && c4.getCount() > 0)
				villageLUD = c4.getString(0);
			
			if(isResponse){
				if (c1.moveToFirst()) {
					reqId = c1.getString(0);
					c1.close();
	
					// get the status of last request
					prepareResponseXML(reqId, settingsLUD, dangLUD, villageLUD);
					syncState.RequestId = reqId;
					xmlString = xmlBuilder.end();

					return xmlString;
				}
			}else{
				prepareXMLforCurrentServerData(settingsLUD, dangLUD, villageLUD);
				xmlString = xmlBuilder.end();
				return xmlString;
			}
		} catch (Exception e) {
			throw e;
		} finally {
			if(c4 != null) {
				c4.close();
			}
		}
		
		return "";
	}

	public String WriteSyncXML(String androidId, String UserId) throws Exception  {
		String SQLStr = null ;
		String xFirstDate = null;
		Cursor c1 ;
		String reqId ;


		try {
			//tblSyncMaster
			//tblSyncDetails - fields - ReqId, ReqDateTime, Status   (write both, sent and received time and status)
			//Add anmId to all Trans and Sync tables.

			//Get Date, Count(*) from tblTransHeader Where TransStatus = 'N'
			SQLStr  = "select transdate, Count(*), Date(substr(transdate,7) || '-' || substr(transdate,4,2)  || '-' || substr(transdate,1,2)) as tdate from TransHeaderDS where TransStatus = 'N' Group by tdate "; /*having tdate < Date('now') order by tdate*/
			c1 = db.rawQuery(SQLStr, new String[0]);
			if (c1.moveToFirst()) {
				xFirstDate = c1.getString(0);												//Get the First Date
				c1.close();

				int LastRequestNumber = getLastRequestNumber(UserId);
				reqId = androidId + UserId + String.format("%05d", LastRequestNumber + 1);	// Create a new RequestId
				syncState.RequestId = reqId;

				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				String xTodaysDate = sdf.format(new Date());

				//Insert Into tblSyncStatus
				SQLStr = "INSERT INTO tblSyncMasterDS Values ('" + UserId + "', '" + reqId + "', '" + xTodaysDate + "', '" + xTodaysDate + "', 'N') " ;	// Update real Values here....
				db.execSQL(SQLStr);

				//Update tblUsers
				SQLStr = "UPDATE tblUsers SET LastRequestNumber = LastRequestNumber + 1 WHERE UserId = '" + UserId + "'" ;	// Update real Values here....
				db.execSQL(SQLStr);
			}
			else {
				c1.close();
				return "";
			}


			//Get all records from Trans Header - Not SENT
			SQLStr  = "SELECT TransId from TransHeaderDS WHERE TransStatus = 'N' AND TransDate = '" + xFirstDate + "'" ;
			c1 = db.rawQuery(SQLStr, new String[0]);

			if (c1.moveToFirst()) {
				xmlBuilder = new SyncXmlBuilder();
				xmlBuilder.openRow("Request");
				xmlBuilder.addColumn("RequestId", reqId);
				xmlBuilder.addColumn("DatabaseId", appState.webServiceDBbMster); //11Oct2019 - Change db name from properties

				//Loop thru the records in Trans Header
				do {
					String xTranId = c1.getString(0);
					// Get all records from Trans Detail
					String sqlTransDetail = "select Action, TblName, SQLstmt from TransHeaderDS where TransId = '" + xTranId + "'" ;
					Cursor c2 = db.rawQuery(sqlTransDetail, new String[0]);	   
					if (c2.moveToFirst()) {
						do {
							String xAction = c2.getString(0);		//Action
							String xTableName = c2.getString(1);		// Table Name
							if (xAction.equals("I")) {
								String xVAL = prepareXMLforInsert(xTableName, xTranId);
								if (xVAL == null) {
									return "ERR";
								}
							}
							else if (xAction.equals("U")) {
								String xUpdateSQL = c2.getString(2);
								prepareXMLforUpdate(xTableName, xTranId, xUpdateSQL);
							}
						} while (c2.moveToNext());
					}
				} while (c1.moveToNext());
			}

			xmlBuilder.openRow("/Request");
			xmlString = xmlBuilder.end();

			System.out.println("SEE THIS" + xmlString);
			Log.i(Constants._COUNT, "exporting database complete");

			String SQLStr1  = "UPDATE TransHeaderDS SET ReqId = '" + reqId + "' WHERE TransStatus = 'N' AND TransDate = '" + xFirstDate + "'" ;
			db.execSQL(SQLStr1);
			
			System.out.println("send Rid: " + reqId);

			writeTxtFile(xmlString);
			
			return xmlString;
		}
		catch (Exception e) {
			throw e;
		}
	}

	@SuppressLint("DefaultLocale")
	private String prepareXMLforInsert(String tableName, final String transId) throws Exception {
		String returnValue = "";
		String str = "";
		
		tableName = tableName.toLowerCase();

		String inputSQL = "SELECT * FROM " + tableName + " WHERE TransId = " + transId;
		System.out.println("inputSQL: " + inputSQL);
		try {
			Cursor c = db.rawQuery(inputSQL, new String[0]);
			if (c.moveToFirst()) {
				int cols = c.getColumnCount();

				xmlBuilder.openRow(tableName + "nodes");
				do {

					xmlBuilder.openRow(tableName + "node");
					xmlBuilder.addColumn("tablename", tableName);
					xmlBuilder.addColumn("action", "I");

					for (int i = 0; i < cols; i++) {
						if ((tableName.toLowerCase().equals("tblregisteredwomen")) && (i==4)) {
							System.out.println("col - blob" + c.getColumnName(i));
							System.out.println("col - Length" + c.getType(i));
							if (c.getType(i) > 0) {				// Null = 0
								
								@SuppressWarnings("unused")
								ByteArrayOutputStream baos = new ByteArrayOutputStream();  

								byte[] b = c.getBlob(4);
								String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

								str = encodedImage;

							}
							xmlBuilder.addColumn(c.getColumnName(i).toLowerCase(), str);
						} 
							// Skipping the DateClosed column in the tbldangersigns
						else if(((tableName.toLowerCase().equals("tbldangersigns")) && (i==11)) || ((tableName.toLowerCase().equals("tbldangersigns")) && (i==15))){
							continue;
						}
						else {
							String mStr = c.getString(i);
							
							if(mStr != null && mStr.length() > 0){
								mStr = mStr.replaceAll("&", "&amp;");
								mStr = mStr.replaceAll("<", "&lt;");
								mStr = mStr.replaceAll(">", "&gt;");
								mStr = mStr.replaceAll("\"", "&quot;");
								mStr = mStr.replaceAll("'", "''");
								mStr = mStr.replaceAll("\\\\", "\\\\\\\\");
							}
							
							xmlBuilder.addColumn(c.getColumnName(i).toLowerCase(), mStr);

						}
					}
					xmlBuilder.openRow("/" + tableName + "node");		//Close
				} while (c.moveToNext());
				xmlBuilder.openRow("/" + tableName + "nodes");		//Close
			}
			c.close();
		}
		catch (Exception e ) {
			throw e ;
		}
		return returnValue ;
	}

//	prepare xml for update
	private String prepareXMLforUpdate(String tableName, final String transId, final String updateSQL ) throws Exception {
		try {
			tableName = tableName.toLowerCase();

			xmlBuilder.openRow(tableName + "nodes");
			xmlBuilder.openRow(tableName + "node");

			xmlBuilder.addColumn("tablename", tableName);
			xmlBuilder.addColumn("action", "U");

			xmlBuilder.addColumn("SQLSTATEMENT", updateSQL);

			xmlBuilder.openRow("/" + tableName + "node");		//Close
			xmlBuilder.openRow("/" + tableName + "nodes");		//Close

			xmlString = xmlBuilder.end();
			return xmlString;
		}
		catch (Exception e) {
			throw e;
		}
	}

//	prepare response xml
	private void prepareResponseXML(final String reqId, String settingsLUD, String dangLUD, String villageLUD) throws IOException {
		xmlBuilder = new SyncXmlBuilder();
		xmlBuilder.openRow("Request");
		
		xmlBuilder.addColumn("RequestId", reqId);
		xmlBuilder.addColumn("DatabaseId", user.getInst_Database()); //11Oct2019 - Change db name from properties
		xmlBuilder.addColumn("MasterDb", appState.webServiceDBbMster);
		xmlBuilder.addColumn("UserId", user.getUserId());
		
		if(settingsLUD != null)
			xmlBuilder.addColumn("SettingsUpdatedDate", settingsLUD);
		if(dangLUD != null)
			xmlBuilder.addColumn("DangerUpdatedDate", dangLUD);
		if(villageLUD != null)
			xmlBuilder.addColumn("VillageUpdatedDate", villageLUD);
		
		xmlBuilder.openRow("/Request");
		xmlString = xmlBuilder.end();
	}
	
//	prepare XML
	private void prepareXMLforCurrentServerData(String settingsLUD, String dangLUD, String villageLUD) throws IOException, SQLException {
		xmlBuilder = new SyncXmlBuilder();
		xmlBuilder.openRow("Request");
		
		xmlBuilder.addColumn("MasterDb", appState.webServiceDBbMster); //11Oct2019 - Change db name from properties
		if(user != null)
			xmlBuilder.addColumn("DatabaseId", user.getInst_Database());//11Oct2019 - Change db name from user tbl (stsvym - tbldb)
		xmlBuilder.addColumn("UserId", user.getUserId());
		
		if(settingsLUD != null)
			xmlBuilder.addColumn("SettingsUpdatedDate", settingsLUD);
		if(dangLUD != null)
			xmlBuilder.addColumn("DangerUpdatedDate", dangLUD);
		if(villageLUD != null)
			xmlBuilder.addColumn("VillageUpdatedDate", villageLUD);
		
		xmlBuilder.openRow("/Request");
		xmlString = xmlBuilder.end();
	}

	/**
	 * XmlBuilder is used to write XML tags (open and close, and a few attributes)
	 * to a StringBuilder. Here we have nothing to do with IO or SQL, just a fancy StringBuilder. 
	 * 
	 * @author ccollins
	 *
	 */
	@SuppressWarnings("unused")
	static class SyncXmlBuilder {


		private static final String OPEN_XML_STANZA = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
		private static final String CLOSE_WITH_TICK = ">";
		private static final String DB_OPEN = "<database name='";
		private static final String DB_CLOSE = "</database>";
		private static final String TABLE_OPEN = "<table name='";
		private static final String TABLE_CLOSE = "</table>";
		private static final String ROW_OPEN = "<row>";
		private static final String ROW_CLOSE = "</row>";
		private static final String COL_OPEN = "<col name='";
		private static final String COL_CLOSE = "</col>";

		private static final String OPEN_WITH_TICK = "<";

		private final StringBuilder sb;

		public SyncXmlBuilder() {
			sb = new StringBuilder();
		}

		public String end() {
			return sb.toString();
		}

		void openRow() {
			sb.append(SyncXmlBuilder.ROW_OPEN);
		}
		void openRow(String xVal) {
			sb.append(SyncXmlBuilder.OPEN_WITH_TICK + xVal + SyncXmlBuilder.CLOSE_WITH_TICK);
		}

		void closeRow() {
			sb.append(SyncXmlBuilder.ROW_CLOSE);
		}

		void addColumn(final String name, final String val) throws IOException {
			if (val == null) {

			}
			else {
				sb.append(SyncXmlBuilder.OPEN_WITH_TICK + name + SyncXmlBuilder.CLOSE_WITH_TICK + val + SyncXmlBuilder.OPEN_WITH_TICK + "/" + name + SyncXmlBuilder.CLOSE_WITH_TICK);	    		  
			}


		}
	}

//	get last request number
	public int getLastRequestNumber(String anmId) throws Exception {
		int LastRequestNumber = 0;
		String sqlstr = "Select LastRequestNumber from tblusers where UserId = '"+ anmId +"' ";
		Cursor c = db.rawQuery(sqlstr, null);
		if (c != null) {
			if (c.moveToFirst()) {
				LastRequestNumber = c.getInt(0);
			}
		}
		c.close();
		return LastRequestNumber;
	}	

//	write to txt file
	public void writeTxtFile(String sg) throws Exception {

		InputStream inputStream = new ByteArrayInputStream(sg.getBytes());
		@SuppressWarnings("unused")
		String state = Environment.getExternalStorageState();

		//if (Environment.MEDIA_MOUNTED.equals(state)) 
		//{
			//SDcard is available
			File f=new File( appState.mainDir + "/testDS.xml");
			try {
				f.createNewFile();
				OutputStream out=new FileOutputStream(f);
                byte[] buf = new byte[1024];
				int len;
				while((len=inputStream.read(buf))>0)
					out.write(buf,0,len);
				out.close();
				inputStream.close();
				System.out.println("\nFile is created...................................");

			} catch (IOException e) {
				e.printStackTrace();
			}
	}
}