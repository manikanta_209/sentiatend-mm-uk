package com.sc.stmansi.About;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.repositories.LoginRepository;

import com.sc.stmansi.R;


public class AboutActivity extends AppCompatActivity {

    WebView app, developer, wvdonor, wvsvym;
    LinearLayout llabtid, lldevid, llabtappidfields, llabtdevidfields;
    TextView txtabtapp, txtabtdev, txtabtdonor, txtabtsvym;
    LinearLayout lldonorid, llsvymid, llabtdonoridfields, llabtsvymidfields;

    private AppState appState;
    private DatabaseHelper databaseHelper;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        try {
            Bundle bundle = getIntent().getBundleExtra("globalState");
            appState = bundle.getParcelable("appState");
            this.databaseHelper = getHelper();
            getSupportActionBar().setDisplayOptions(getSupportActionBar().DISPLAY_SHOW_CUSTOM | getSupportActionBar().DISPLAY_SHOW_HOME | getSupportActionBar().DISPLAY_SHOW_TITLE);
            app = findViewById(R.id.webviewapp);
            developer = findViewById(R.id.webviewdeveloper);
            wvsvym = findViewById(R.id.webviewsvym);
            wvdonor = findViewById(R.id.webviewdonor);
            llabtid = findViewById(R.id.llaboutappid);
            lldevid = findViewById(R.id.llaboutdevid);
            lldonorid = findViewById(R.id.llaboutdonorid);
            llsvymid = findViewById(R.id.llaboutsvymid);
            app.getSettings().setJavaScriptEnabled(true);
            developer.getSettings().setJavaScriptEnabled(true);

            txtabtapp = findViewById(R.id.txtabtapp);
            txtabtdev = findViewById(R.id.txtabtdev);
            txtabtdonor = findViewById(R.id.txtabtdonor);
            txtabtsvym = findViewById(R.id.txtabtsvym);
            llabtappidfields = findViewById(R.id.llabtappidfields);
            llabtdevidfields = findViewById(R.id.llabtdevidfields);
            llabtdonoridfields = findViewById(R.id.llabtdonoridfields);
            llabtsvymidfields = findViewById(R.id.llabtsvymidfields);

            app.loadUrl("file:///android_asset/aboutapp.html");
            developer.loadUrl("file:///android_asset/aboutdev.html");
            wvdonor.loadUrl("file:///android_asset/aboutdonor.html");
            wvsvym.loadUrl("file:///android_asset/aboutsvym.html");

            llabtid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ShoworHideContent(llabtappidfields, txtabtapp);
                }
            });

            lldevid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ShoworHideContent(llabtdevidfields, txtabtdev);
                }
            });

            lldonorid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ShoworHideContent(llabtdonoridfields, txtabtdonor);
                }
            });

            llsvymid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ShoworHideContent(llabtsvymidfields, txtabtsvym);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    private void ShoworHideContent(LinearLayout llid, TextView txtid) {

        if (llid.getVisibility() == View.VISIBLE) {
            llid.setVisibility(View.GONE);
            txtid.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
        } else {
            llabtappidfields.setVisibility(View.GONE);
            txtabtapp.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);

            llabtdonoridfields.setVisibility(View.GONE);
            txtabtdonor.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);

            llabtsvymidfields.setVisibility(View.GONE);
            txtabtsvym.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);

            llabtdevidfields.setVisibility(View.GONE);
            txtabtdev.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);

            llid.setVisibility(View.VISIBLE);
            txtid.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.expand, 0);

        }
    }

    /**
     * Initializes the Action Bar items
     */
    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        try {
            getMenuInflater().inflate(R.menu.action_overflow_items, menu);
            menu.findItem(R.id.action_logout).setTitle(getResources().getString(R.string.action_logout));
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        return super.onCreateOptionsMenu(menu);
    }


    /**
     * On selecting action bar icons
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home: {
                Intent goToScreen = new Intent(AboutActivity.this, MainMenuActivity.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }
            case R.id.action_logout: {
                Intent goToScreen = new Intent(AboutActivity.this, LoginActivity.class);
                displayAlert(getResources().getString(R.string.m111), goToScreen);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {

    }

    /**
     * Save Confirmation Alert
     *
     * @param goToScreen
     */
    private void displayAlert(final String spanText2, final Intent goToScreen) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set dialog message
        alertDialogBuilder.setMessage(spanText2).setCancelable(false).setNegativeButton(
                getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (goToScreen != null) {
                            startActivity(goToScreen);
                            if (spanText2.contains("logout")) {
                                try {
                                    LoginRepository loginRepo = new LoginRepository(databaseHelper);
                                    loginRepo.updateLogoutTime(appState.loginAuditId, appState.sessionUserId);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        } else
                            dialog.cancel();
                    }
                }).setPositiveButton(getResources().getString(R.string.m122),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        /*
         * You'll need this in your class to release the helper when done.
         */
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }
}
