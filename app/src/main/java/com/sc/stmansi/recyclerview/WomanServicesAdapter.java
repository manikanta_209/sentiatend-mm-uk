//27Aug2019 Arpitha - remove unused code
package com.sc.stmansi.recyclerview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.tables.tblserviceslist;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import com.sc.stmansi.R;

public class WomanServicesAdapter extends RecyclerView.Adapter<WomanServiceViewHolder> {

    private Context context;
    private List<tblserviceslist> services;
    private ClickListener viewClickListener;
    private tblregisteredwomen woman;

    public WomanServicesAdapter(Context context, List<tblserviceslist> services, ClickListener viewClickListener, tblregisteredwomen woman) {
        this.context = context;
        this.services = services;
        this.viewClickListener = viewClickListener;
        this.woman = woman;
    }

    @NonNull
    @Override
    public WomanServiceViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RelativeLayout v = (RelativeLayout) LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.adapter_services, viewGroup, false);
        v.setOnClickListener(viewClickListener);
        return new WomanServiceViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull WomanServiceViewHolder vh, int position) {
        tblserviceslist service = services.get(position);
        int serviceId = service.getServiceId();

        vh.txtDuedateMaxL.setVisibility(View.GONE);

        //12Aug2019 - Bindu - reorder the service id and change drawable accordingly for fa,ifa,anc

        if (serviceId == 1 || serviceId == 2 || serviceId == 3)
            vh.imgind.setImageResource(R.drawable.tt);
        else if (serviceId == 4 || serviceId == 5 || serviceId == 6)
            vh.imgind.setImageResource(R.drawable.fa);
        else if (serviceId == 7 || serviceId == 8 || serviceId == 9)
            vh.imgind.setImageResource(R.drawable.ifa);
        else if (serviceId == 15)
            vh.imgind.setImageResource(R.drawable.delivery_info);
        else if (serviceId >= 16 && serviceId <= 22)
            vh.imgind.setImageResource(R.drawable.postnatalcare);
        else if (serviceId == 10 || serviceId == 11 || serviceId == 12 || serviceId == 13) {
            vh.imgind.setImageResource(R.drawable.anc);
        } else if (serviceId == 14 || serviceId == 23)
            vh.imgind.setImageResource(R.drawable.calcium);
        else
            vh.imgind.setImageResource(R.drawable.immunisation);

        String serviceType;
       /* if (serviceId == 48 || serviceId == 49 ||
                serviceId == 50 || serviceId == 51 || serviceId == 52 || serviceId == 53 ||
                serviceId == 54 || serviceId == 16 || serviceId == 17 ||
                serviceId == 18 || serviceId == 19 || serviceId == 20 || serviceId == 21 ||
                serviceId == 22 || serviceId == 7 || serviceId == 8 || serviceId == 9 || serviceId == 10
                || serviceId == 4 || serviceId == 5 || serviceId == 6 || serviceId == 1 || serviceId == 2
                || serviceId == 11 || serviceId == 12 || serviceId == 13)
            serviceType = service.getServiceType() + service.getServiceNumber();
        else
            serviceType = service.getServiceType();*/
        serviceType = service.getServiceType();
        int identifier = context.getResources().getIdentifier(serviceType.toLowerCase(), "string", "com.sc.stmansi");

        /*if(identifier==0)
        vh.ServiceType.setText(serviceType);
        else*/

        if ((serviceId > 3 && serviceId < 7 && service.getServiceNumber() > 3)
                || (serviceId > 6 && serviceId < 10 && service.getServiceNumber() > 3)
                || (serviceId > 9 && serviceId < 14 && service.getServiceNumber() > 4)
        ) {
            vh.ServiceType.setText(context.getResources().getString(identifier));
        } else if (serviceId == 48 || serviceId == 49 ||
                serviceId == 50 || serviceId == 51 || serviceId == 52 || serviceId == 53 ||
                serviceId == 54 || serviceId == 16 || serviceId == 17 ||
                serviceId == 18 || serviceId == 19 || serviceId == 20 || serviceId == 21 ||
                serviceId == 22 || serviceId == 7 || serviceId == 8 || serviceId == 9 || serviceId == 10
                || serviceId == 4 || serviceId == 5 || serviceId == 6 || serviceId == 1 || serviceId == 2
                || serviceId == 11 || serviceId == 12 || serviceId == 13)

            vh.ServiceType.setText(context.getResources().getString(identifier) + service.getServiceNumber());

        else {
            if(identifier>0)
            vh.ServiceType.setText(context.getResources().getString(identifier));
            else
                vh.ServiceType.setText(serviceType);

        }


        if (service.getServiceActualDateofAction() != null) {
            if (service.getServiceActualDateofAction().length() > 1)
                vh.imgstatus.setImageResource(R.drawable.tick);
        } else {
            {
                try {


                    if (woman.getRegPregnantorMother() == 2 && service.getServiceId() == 15)
                        vh.imgstatus.setImageResource(R.drawable.tick);
                    else if (woman.getRegPregnantorMother() == 2 && service.getServiceId() <= 14)
                        vh.imgstatus.setImageResource(R.drawable.warning);
                    else {
                        if ((service.getServiceDueDateMin() == null)

                                || (service.getServiceDueDateMax() != null &&
                                DateTimeUtil.parse(service.getServiceDueDateMax())
                                        .before(DateTimeUtil.parse(DateTimeUtil.getTodaysDate())))) {  // 05Aug2019 - Bindu - chk for max date expiration //16Aug2019 - Arpitha sorted out to fix date in dd-MM0yyy format
                            vh.imgstatus.setImageResource(R.drawable.warning);
                        } else /*if ((woman.getRegPregnantorMother() == 1 && serviceId <= 15)

                            || (woman.getRegPregnantorMother() == 2
                            && serviceId > 15))*/ {
                            if (service.getServiceDueDateMin() != null &&
                                    DateTimeUtil.parse(service.getServiceDueDateMin())
                                            .after(new Date()))
                                vh.imgstatus.setImageResource(R.drawable.upcoming);
                            else {
                                vh.imgstatus.setImageResource(R.drawable.notdone);  //09Aug2019 - Bindu - change image
                            }
                        }
                    }
                } catch (ParseException e) {
                    e.printStackTrace(); // thrown by DateTimeUtil.parse
                }
            }
        }




        /*{
            try {
                if ((service.getServiceDueDateMin() == null
                        && (woman.getRegPregnantorMother() == 1
                        && serviceId <= 15)

                        || (woman.getRegPregnantorMother() == 2
                        && serviceId > 15)) || (service.getServiceDueDateMax() != null &&
                        DateTimeUtil.parse(service.getServiceDueDateMax())
                                .before(DateTimeUtil.parse(DateTimeUtil.getTodaysDate())))) {  // 05Aug2019 - Bindu - chk for max date expiration //16Aug2019 - Arpitha sorted out to fix date in dd-MM0yyy format
                    vh.imgstatus.setImageResource(R.drawable.warning);
                } else if ((woman.getRegPregnantorMother() == 1 && serviceId <= 15)

                        || (woman.getRegPregnantorMother() == 2
                        && serviceId > 15)) {
                    if (service.getServiceDueDateMin() != null &&
                            DateTimeUtil.parse(service.getServiceDueDateMin())
                                    .after(new Date()))
                        vh.imgstatus.setImageResource(R.drawable.upcoming);
                    else {
                        vh.imgstatus.setImageResource(R.drawable.notdone);  //09Aug2019 - Bindu - change image
                    }
                }
            } catch (ParseException e) {
                e.printStackTrace(); // thrown by DateTimeUtil.parse
            }
        }*/
//    06Dec2019 Arpitha
        if (service.getServiceType().equalsIgnoreCase("EDD"))
            vh.ActualDateOfAction.setText(woman.getRegADDate());
        else//    06Dec2019 Arpitha
            vh.ActualDateOfAction.setText(service.getServiceActualDateofAction());

        if (service.getServiceDueDateMin() != null
                && service.getServiceDueDateMax() != null
                && service.getServiceDueDateMin().length() > 0
                && service.getServiceDueDateMax().length() > 0) {
            vh.DueDateMin.setText(service.getServiceDueDateMin());
            vh.DueDateMax.setText(service.getServiceDueDateMax());
            vh.DueDateMax.setText(" - " + service.getServiceDueDateMax());
        }
        // 20July2019 - Bindu

    }

    @Override
    public int getItemCount() {
        return services.size();
    }
}
