package com.sc.stmansi.recyclerview;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sc.stmansi.R;

public class HomeVisitListViewHolder extends RecyclerView.ViewHolder {

    public TextView txtVisitDate, txtVisitNum, txtReferredToFacType,txtReferredToFacname, txtStatus, txtcovidresult,txtdelayedfollowup;
    public ImageView imgWomanStatusHv, imgVisitOutcome ;
    public Button btnactiontaken,btnactiontakenmc;

    public HomeVisitListViewHolder(@NonNull View itemView) {
        super(itemView);
        txtVisitDate = itemView.findViewById(R.id.txtVisitDate);
        txtVisitNum = itemView.findViewById(R.id.txtVisitNum);
        txtReferredToFacType = itemView.findViewById(R.id.txtreferredtofactype);
        txtReferredToFacname = itemView.findViewById(R.id.txtreferredtofacname);
        txtStatus = itemView.findViewById(R.id.txtStatusatvisit);
//        17MAy2021 Bindu
        txtcovidresult = itemView.findViewById(R.id.txtcovidresult);

        imgWomanStatusHv = itemView.findViewById(R.id.imgWstatus);
        imgVisitOutcome = itemView.findViewById(R.id.imgVisitOutcome);
        //27Nov2019 - Bindu - add action taken items
        btnactiontaken = itemView.findViewById(R.id.btnactiontaken);
        //        04Jul2021 Bindu
        txtdelayedfollowup = itemView.findViewById(R.id.txtdelayedvisit);
        btnactiontakenmc = itemView.findViewById(R.id.btnactiontakenmc);
    }
}
