package com.sc.stmansi.recyclerview;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.HomeVisit.HomeVisitActivityByCc;
import com.sc.stmansi.HomeVisit.HomeVisitActivityByMC;
import com.sc.stmansi.HomeVisit.HomeVisitListActivity;
import com.sc.stmansi.complreferralmanagement.ComplMgmtVisitViewActivity;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.repositories.CaseMgmtRepository;
import com.sc.stmansi.repositories.ComplicationMgmtRepository;
import com.sc.stmansi.repositories.CovidDetailsRepository;
import com.sc.stmansi.repositories.HomeVisitRepository;
import com.sc.stmansi.tables.TblVisitHeader;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.sc.stmansi.R;
import com.sc.stmansi.tables.tblCaseMgmt;
import com.sc.stmansi.tables.tblCovidTestDetails;

public class HomeVisitListAdapter extends RecyclerView.Adapter<HomeVisitListViewHolder> {

    private Context context;
    private List<TblVisitHeader> visits;
    private ClickListener viewClickListener;
    AppState appState;
    DatabaseHelper databaseHelper;
    ComplicationMgmtRepository comprepo;


    public HomeVisitListAdapter(Context context, List<TblVisitHeader> visits, ClickListener viewClickListener) {
        this.context = context;
        this.visits = visits;
        this.viewClickListener = viewClickListener;
    }

    public HomeVisitListAdapter(Context homeVisitListActivity, List<TblVisitHeader> visitHeaderList,
                                ClickListener viewClickListener, AppState appState) {
        this.viewClickListener = viewClickListener;
        this.visits = visitHeaderList;
        this.appState = appState;
        this.context = homeVisitListActivity;
        databaseHelper = getHelper();
        comprepo = new ComplicationMgmtRepository(databaseHelper);
    }

    @NonNull
    @Override
    public HomeVisitListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.homevisit_recycler_item, viewGroup, false);
        linearLayout.setOnClickListener(viewClickListener);
        return new HomeVisitListViewHolder(linearLayout);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeVisitListViewHolder homeVisitViewHolder, int i) {
       final TblVisitHeader visitHeader = visits.get(i);
        homeVisitViewHolder.txtVisitNum.setText("# "+visitHeader.getVisitNum());
        homeVisitViewHolder.txtVisitDate.setText(visitHeader.getVisHVisitDate());
        if(visitHeader.getVisHVisitType().equalsIgnoreCase("ANC")) {
            homeVisitViewHolder.imgWomanStatusHv.setImageResource(R.drawable.ic_pregnant);
            homeVisitViewHolder.txtStatus.setText(context.getResources().getString(R.string.statusatvisit) + " : " + visitHeader.getVisHStatusAtVisit());
        }
        else if(visitHeader.getVisHVisitType().equalsIgnoreCase("PNC")) {
            homeVisitViewHolder.imgWomanStatusHv.setImageResource(R.drawable.ic_mother);
            homeVisitViewHolder.txtStatus.setText(context.getResources().getString(R.string.txtDaysSinceDeliveryName) + " : " + visitHeader.getVisHStatusAtVisit());
        }

        if(visitHeader.getVisHVisitIsReferred().equalsIgnoreCase(context.getResources().getString(R.string.yesshortform)))
            homeVisitViewHolder.imgVisitOutcome.setImageResource(R.drawable.ic_refer);
        else if(visitHeader.getVisHCompl().equalsIgnoreCase("Y") || (visitHeader.getVisHChildSymptoms() != null && visitHeader.getVisHChildSymptoms().trim().length() > 0)) //22Jul2021 Bindu check child compl as well
            homeVisitViewHolder.imgVisitOutcome.setImageResource(R.drawable.ic_hvcompl);
        else
            homeVisitViewHolder.imgVisitOutcome.setImageResource(R.drawable.ic_hvnocompl);

        /*if(visitHeader.getVisHVisitReferredToFacilityType().length() > 0)
            homeVisitViewHolder.txtReferredToFacType.setText(context.getResources().getString(R.string.reffacilitytype) +  visitHeader.getVisHVisitReferredToFacilityType());
*/
        if(visitHeader.getVisHVisitReferredToFacilityType().length() > 0 || visitHeader.getVisHVisitReferredToFacilityName().length() > 0)
            homeVisitViewHolder.txtReferredToFacname.setText(context.getResources().getString(R.string.reffacilityname) + visitHeader.getVisHVisitReferredToFacilityType() + " - " + visitHeader.getVisHVisitReferredToFacilityName());
        else//21May2021 Arpitha
            homeVisitViewHolder.txtReferredToFacname.setText("");


        homeVisitViewHolder.imgVisitOutcome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {


                    if ((visitHeader.getVisHResult() != null && visitHeader.getVisHResult().length() > 0) ||
                            (visitHeader.getVisHChildSymptoms() != null && visitHeader.getVisHChildSymptoms().length() > 0)) {
                        String title = context.getResources().getString(R.string.symptoms);
                        if (visitHeader.getVisHVisitType().equalsIgnoreCase(context.getResources().getString(R.string.strpnc)))
                            title = context.getResources().getString(R.string.dangersigns);


                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                        //View convertView = LayoutInflater.from(context).inflate(R.layout.symptoms_displaylist, null, false);
                        View convertView = LayoutInflater.from(context).inflate(R.layout.custom_hv_dialog, null, false);
                    /*ListView lv = convertView.findViewById(R.id.lvSymplist);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.symtomlistitem, compList);*/
                        TextView txtmotherdangersigns = convertView.findViewById(R.id.txtmotherdangersignval);
                        TextView txtnbdangersigns = convertView.findViewById(R.id.txtnbdangersignval);
                        LinearLayout llnbdangersigns = convertView.findViewById(R.id.llnbds);
                        LinearLayout llwomandangersigns = convertView.findViewById(R.id.llmotherds);

                        if (visitHeader.getVisHResult() != null && visitHeader.getVisHResult().length() > 0) {
                            txtmotherdangersigns.setText(setWomanCompl(i));//15May2021 Arpitha //25Jul2021 Bindu change the mtd
                            llwomandangersigns.setVisibility(View.VISIBLE);
                        } else {
                            llwomandangersigns.setVisibility(View.GONE);
                        }

                        if (visitHeader.getVisHChildSymptoms() != null && visitHeader.getVisHChildSymptoms().length() > 0) {
                            txtnbdangersigns.setText(setChildCompl(i));//15May2021 Arpitha
                            llnbdangersigns.setVisibility(View.VISIBLE);
                        } else {
                            llnbdangersigns.setVisibility(View.GONE);
                        }

                        alertDialog.setView(convertView).setCancelable(true).setTitle(title)
                                .setPositiveButton((context.getResources().getString(R.string.ok)), new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                });
                        // lv.setAdapter(adapter);
                        alertDialog.show();
                    }
                }catch (Exception e)
                {

                    e.printStackTrace();
                }
            }
        });


        try {
            /*if (comprepo.getComplMgmtVisitDetailsWoman(visitHeader.getWomanId(),visitHeader.getVisitNum())) {
                homeVisitViewHolder.btnactiontaken.setVisibility(View.VISIBLE);
                homeVisitViewHolder.btnactiontaken.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, ComplMgmtVisitViewActivity.class);
                        intent.putExtra("globalState", prepareBundle());
                        intent.putExtra("complVisitNum", visitHeader.getVisitNum()); //20Dec2019 - Bindu - Add visit num
                        context.startActivity(intent);
                    }
                });
            } else {
                homeVisitViewHolder.btnactiontaken.setVisibility(View.GONE);
            }*/ //22Jul2021 Bindu comment

            //22Jul2021 Bindu - check for action taken by CC
            try {
                CaseMgmtRepository caseMgmtRepo = new CaseMgmtRepository(databaseHelper);
                /*tblCaseMgmt currentCaseDetails = caseMgmtRepo.getCaseMgmtDetailsWomanNew(visitHeader.getWomanId(), "" + visitHeader.getVisitNum(), appState.sessionUserId, visitHeader.getVisHVisitDate()); //13Aug2021 Bindu add visit date

                if (currentCaseDetails != null && currentCaseDetails.getVisitNumbyMM() == visitHeader.getVisitNum()) {
                    homeVisitViewHolder.btnactiontaken.setVisibility(View.VISIBLE);
                }else
                    homeVisitViewHolder.btnactiontaken.setVisibility(View.GONE);*/

                //            MC Action taken
                tblCaseMgmt currentCaseDetailsMC = caseMgmtRepo.getCaseMgmtDetailsWomanbyUserType(visitHeader.getWomanId(), ""+visitHeader.getVisitNum(), context.getResources().getString(R.string.mcusertype),visitHeader.getVisHVisitDate());
                if(currentCaseDetailsMC != null && currentCaseDetailsMC.getVisitNumbyMM() == visitHeader.getVisitNum()){
                    homeVisitViewHolder.btnactiontakenmc.setVisibility(View.VISIBLE);
                }  else {
                    homeVisitViewHolder.btnactiontakenmc.setVisibility(View.GONE);
                }

                //            CC Action taken
                tblCaseMgmt currentCaseDetailsCC = caseMgmtRepo.getCaseMgmtDetailsWomanbyUserType(visitHeader.getWomanId(), ""+visitHeader.getVisitNum(), context.getResources().getString(R.string.ccusertype),visitHeader.getVisHVisitDate());
                if(currentCaseDetailsCC != null && currentCaseDetailsCC.getVisitNumbyMM() == visitHeader.getVisitNum()){
                    homeVisitViewHolder.btnactiontaken.setVisibility(View.VISIBLE);
                }  else {
                    homeVisitViewHolder.btnactiontaken.setVisibility(View.GONE);
                }
            }catch (Exception e) {
                e.printStackTrace();
            }


                homeVisitViewHolder.btnactiontaken.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //22Jul2021 Bindu check cc has dne the follow up
                    try {
                            Intent intent = new Intent(context, HomeVisitActivityByCc.class);
                            intent.putExtra("globalState", prepareBundle());
                        intent.putExtra("complVisitNum", visitHeader.getVisitNum()); //20Dec2019 - Bindu - Add visit num
                        intent.putExtra("pos",visitHeader.getVisitNum());
                            context.startActivity(intent);

                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            homeVisitViewHolder.btnactiontakenmc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        Intent intent = new Intent(context, HomeVisitActivityByMC.class);
                        intent.putExtra("globalState", prepareBundle());
                        intent.putExtra("complVisitNum", visitHeader.getVisitNum()); //20Dec2019 - Bindu - Add visit num
                        intent.putExtra("pos",visitHeader.getVisitNum()-1);
                        context.startActivity(intent);

                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

        }catch (Exception e) {
            e.printStackTrace();
        }

//        17May2021 Bindu - show covid details
        try {
            CovidDetailsRepository covRepo = new CovidDetailsRepository(databaseHelper);
            List<tblCovidTestDetails> covvactestdetails = covRepo.getVisitCovidTestDetails("" + visitHeader.getVisitNum(), visitHeader.getWomanId());
            if (covvactestdetails != null && covvactestdetails.size() > 0) {
                if (covvactestdetails.get(0).getCovidTest().toString().equalsIgnoreCase(context.getResources().getString(R.string.yesshortform))) {
                    if(covvactestdetails.get(0).getCovidResult().toString().equalsIgnoreCase(context.getResources().getString(R.string.positivetxt))) {
                        homeVisitViewHolder.txtcovidresult.setText(context.getResources().getString(R.string.txtcovid) + covvactestdetails.get(0).getCovidResult());
                    }else
                        homeVisitViewHolder.txtcovidresult.setText("");
                }else
                    homeVisitViewHolder.txtcovidresult.setText("");
            }else
                homeVisitViewHolder.txtcovidresult.setText("");
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        return visits.size();
    }

    //    prepare bundle
    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        return b;
    }

    //    initialize db object
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
        }
        return databaseHelper;
    }

//    16May2021 Arpitha
    private String setWomaCompl(int pos) throws SQLException {
        HomeVisitRepository homeVisitRepository = new HomeVisitRepository(databaseHelper);
        ArrayList<String> list = homeVisitRepository.getHomeVisitSymptoms(visits.get(pos).getUserId(),
                visits.get(pos).getWomanId(),visits.get(pos).getVisitNum());
        String symptom = "";
        if(list!=null && list.size()>0) {

            for(int i=0;i<list.size();i++) {
                String[] compl = list.get(i).split("-");
                for(int k=0;k<compl.length;k++) {
                    String strSy = compl[k];
                  //  if(strSy!=null && strSy.trim().length()>0 && strSy.contains(":"))
                    {
                       String[] com =  strSy.split(":");
                       for(int p=0;p<com.length;p++)
                       {
                           String complication = "";
                           if(p<com.length)
                           complication = com[p];

                   /* strSy = strSy.replace(",","");
                    strSy = strSy.replace(",,","");
                    strSy = strSy.replace(",","");
                    strSy = strSy.replace(", ","");*/
                    if(complication!=null && complication.trim().length()>0) {
                        int identifier = context.getResources().getIdentifier
                                (complication,
                                        "string", "com.sc.stmansi");
                        if (identifier > 0) {
                            if (i == list.size() - 1)
                                symptom = symptom + context.getResources().getString(identifier);
                            else
                                symptom = symptom + context.getResources().getString(identifier) + " , ";
                        } else {
                            if (i == list.size() - 1)
                                symptom = symptom + complication;
                            else
                                symptom = symptom + complication + ",";
                        }
                    }  }
                    }
                }

            }
            symptom = symptom.replace(",,","");
            symptom = symptom.replace(", ,","");
         //   symptom = symptom.replace(", ","");
        }return symptom;
    }

    //25Jul2021 Bindu - Set woman complications
    private String setWomanCompl(int pos) throws SQLException{
        HomeVisitRepository homeVisitRepository = new HomeVisitRepository(databaseHelper);
        ArrayList<String> list = null;
        if(visits.get(pos).getVisHVisitType().equalsIgnoreCase(context.getResources().getString(R.string.stranc))) {
            list = homeVisitRepository.getHomeVisitSymptomsNew(visits.get(pos).getUserId(),
                    visits.get(pos).getWomanId(), visits.get(pos).getVisitNum(), context);
        }else {
            list = homeVisitRepository.getHomeVisitSymptomsPNC(visits.get(pos).getUserId(),
                    visits.get(pos).getWomanId(), visits.get(pos).getVisitNum(), context);
        }
        String symptom = "";
        if(list != null && list.size() > 0) {
            ArrayList<String> a = new ArrayList<>();
            for (int i = 0; i < list.size(); i++) {
                if (i == list.size() - 1)
                    symptom = symptom + list.get(i);
                else
                    symptom = symptom + list.get(i) + "\n*";
                // symptom = symptom + list.get(i);
            }
        }

        return symptom;
    }

    //    16May2021 Arpitha
    private String setChildCompl(int pos) throws SQLException {
        HomeVisitRepository homeVisitRepository = new HomeVisitRepository(databaseHelper);
        ArrayList<String> listChild = homeVisitRepository.getHomeVisitSymptomsForChild(visits.get(pos).getUserId(),
                visits.get(pos).getWomanId(),visits.get(pos).getVisitNum());
        ArrayList<String> chl1 = new ArrayList<>();
        ArrayList<String> chl2 = new ArrayList<>();
        ArrayList<String> chl3 = new ArrayList<>();
        ArrayList<String> chl4 = new ArrayList<>();
        // 26Jul2021 Bindu
        String strother = "";

        for(int j=0;j<listChild.size();j++)
        {
            if(listChild.get(j).toLowerCase().contains("baby1"))
                chl1.add(listChild.get(j));
            if(listChild.get(j).toLowerCase().contains("baby2"))
                chl2.add(listChild.get(j));
            if(listChild.get(j).toLowerCase().contains("baby3"))
                chl3.add(listChild.get(j));
            if(listChild.get(j).toLowerCase().contains("baby4"))
                chl4.add(listChild.get(j));

            //26Jul2021 Bindu
            if(listChild.get(j).contains("-babyAll-")) {
                strother = listChild.get(j);
                strother = strother.replace("-babyAll-", "");
            }
        }
        String symptomChild = "";
        if(chl1!=null && chl1.size()>0) {

            String[] str = new String[0];
            for(int i=0;i<chl1.size();i++) {
                String[] compl = chl1.get(i).split("-");
              //  for (int k = 0; k < compl.length; k++)
                {

               // }
                String strSy = compl[0].trim();
                if (strSy.contains("save"))
                    strSy = strSy.replace("save", "");
                if (strSy.contains("-")) {
                    str = strSy.split("-");
                    strSy = str[0].trim();
                }
                int identifier = context.getResources().getIdentifier
                        (strSy,
                                "string", "com.sc.stmansi");
              /*  String baby = "";
                if(str.length>1)
                    baby = str[1];*/

                if (identifier > 0) {
                    if (i == chl1.size() - 1) {
                        String comp = context.getResources().getString(identifier) ;
                        symptomChild = symptomChild + comp;
                    } else {
                        String comp = context.getResources().getString(identifier);
                        symptomChild = symptomChild + comp + ",";
                    }
                } else {
                    if (i == chl1.size() - 1)
                        symptomChild = symptomChild + strSy;
                    else
                        symptomChild = symptomChild + strSy + ",";
                }
            }


            }

            symptomChild = "Baby1 : "+symptomChild;
        }




        if(chl2!=null && chl2.size()>0) {

            String symptomChild2 ="";

            String[] str = new String[0];
            for(int i=0;i<chl2.size();i++) {
                String[] compl = chl2.get(i).split(",");
                for (int k = 0; k < compl.length; k++) {

                    // }
                    String strSy = compl[k].trim();
                    if (strSy.contains("save"))
                        strSy = strSy.replace("save", "");
                    if (strSy.contains("-")) {
                        str = strSy.split("-");
                        strSy = str[0].trim();
                    }
                    int identifier = context.getResources().getIdentifier
                            (strSy,
                                    "string", "com.sc.stmansi");
              /*  String baby = "";
                if(str.length>1)
                    baby = str[1];*/

                    if (identifier > 0) {
                        if (i == chl2.size() - 1) {
                            String comp = context.getResources().getString(identifier) ;
                            symptomChild2 = symptomChild2 + comp;
                        } else {
                            String comp = context.getResources().getString(identifier);
                            symptomChild2 = symptomChild2 + comp + ",";
                        }
                    } else {
                        if (i == chl2.size() - 1)
                            symptomChild2 = symptomChild2 + strSy;
                        else
                            symptomChild2 = symptomChild2 + strSy + ",";
                    }
                }


            }
            symptomChild2 = "\n\nBaby2 : "+symptomChild2;

            symptomChild = symptomChild+symptomChild2;
        }


        if(chl3!=null && chl3.size()>0) {

            String symptomChild3 ="";

            String[] str = new String[0];
            for(int i=0;i<chl3.size();i++) {
                String[] compl = chl3.get(i).split(",");
                for (int k = 0; k < compl.length; k++) {

                    // }
                    String strSy = compl[k].trim();
                    if (strSy.contains("save"))
                        strSy = strSy.replace("save", "");
                    if (strSy.contains("-")) {
                        str = strSy.split("-");
                        strSy = str[0].trim();
                    }
                    int identifier = context.getResources().getIdentifier
                            (strSy,
                                    "string", "com.sc.stmansi");
              /*  String baby = "";
                if(str.length>1)
                    baby = str[1];*/

                    if (identifier > 0) {
                        if (i == chl3.size() - 1) {
                            String comp = context.getResources().getString(identifier) ;
                            symptomChild3 = symptomChild3 + comp;
                        } else {
                            String comp = context.getResources().getString(identifier);
                            symptomChild3 = symptomChild3 + comp + ",";
                        }
                    } else {
                        if (i == chl3.size() - 1)
                            symptomChild3 = symptomChild3 + strSy;
                        else
                            symptomChild3 = symptomChild3 + strSy + ",";
                    }
                }


            }

            symptomChild3 = "\n\nBaby3 : "+symptomChild3;

            symptomChild = symptomChild+symptomChild3;
        }

        if(chl4!=null && chl4.size()>0) {

            String symptomChld4 = "";
            String[] str = new String[0];
            for(int i=0;i<chl4.size();i++) {

                String[] compl = chl4.get(i).split(",");
                for (int k = 0; k < compl.length; k++) {

                    // }
                    String strSy = compl[k].trim();
                    if (strSy.contains("save"))
                        strSy = strSy.replace("save", "");
                    if (strSy.contains("-")) {
                        str = strSy.split("-");
                        strSy = str[0].trim();
                    }
                    int identifier = context.getResources().getIdentifier
                            (strSy,
                                    "string", "com.sc.stmansi");
              /*  String baby = "";
                if(str.length>1)
                    baby = str[1];*/

                    if (identifier > 0) {
                        if (i == chl4.size() - 1) {
                            String comp = context.getResources().getString(identifier) ;
                            symptomChld4= symptomChld4 + comp;
                        } else {
                            String comp = context.getResources().getString(identifier);
                            symptomChld4 = symptomChld4 + comp + ",";
                        }
                    } else {
                        if (i == chl4.size() - 1)
                            symptomChld4 = symptomChld4 + strSy;
                        else
                            symptomChld4 = symptomChld4 + strSy + ",";
                    }
                }


            }

            symptomChld4 = "\n\nBaby4 : "+symptomChld4;

            symptomChild = symptomChild+symptomChld4;
        }


        return symptomChild + "\n\n" +strother; //26Jul2021 Bindu add strother
    }

}
