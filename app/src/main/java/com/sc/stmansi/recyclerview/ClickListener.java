package com.sc.stmansi.recyclerview;

import android.view.View;

public interface ClickListener extends View.OnClickListener, View.OnLongClickListener {
}
