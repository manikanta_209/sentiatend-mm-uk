package com.sc.stmansi.recyclerview;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.pendingservices.IndicatorViewClickListener;
import com.sc.stmansi.tables.TblRegComplMgmt;

import java.util.List;

import com.sc.stmansi.R;

public class ComplRefMgmtWCListAdapter extends RecyclerView.Adapter<ComplRefViewHolder> {

    private ClickListener viewClickListener;
    private List<TblRegComplMgmt> regComplList;
    Context context;
    private IndicatorViewClickListener indicatorViewClickListener;
    Fragment f;
    private AppState appState;

    public ComplRefMgmtWCListAdapter(List<TblRegComplMgmt> regComplList,
                                     Context context) {
        this.viewClickListener = (ClickListener) context;
        this.indicatorViewClickListener = (IndicatorViewClickListener) context;
        this.regComplList = regComplList;
        this.context = context;
    }

    public ComplRefMgmtWCListAdapter(List<TblRegComplMgmt> regComplList,
                                     Context context, Fragment fragment) {
        this.viewClickListener = (ClickListener) fragment;
        this.indicatorViewClickListener = (IndicatorViewClickListener) fragment;
        this.regComplList = regComplList;
        this.f = fragment;
        this.context = context;
    }

    @NonNull
    @Override
    public ComplRefViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RelativeLayout v = (RelativeLayout) LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.complmgmt_adapterlistitem, viewGroup, false);
        v.setOnClickListener(viewClickListener);
        return new ComplRefViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ComplRefViewHolder registeredWomanViewHolder, final int i) {
        try {
            final TblRegComplMgmt woman = regComplList.get(i);

            registeredWomanViewHolder.womanName.setText(woman.getRegWomanName());
            if(regComplList.get(i).getRegPregnantOrMother() == 1)
                 registeredWomanViewHolder.tvWLItemPregSt.setText(context.getResources().getString(R.string.tvlmp) + " : " + woman.getRegLMP());
            else if(regComplList.get(i).getRegPregnantOrMother() == 2)
                registeredWomanViewHolder.tvWLItemPregSt.setText(context.getResources().getString(R.string.tvadd) + " : " + woman.getRegADD());

            registeredWomanViewHolder.txtVisitNum.setText(context.getResources().getString(R.string.homevisit) + " : " +woman.getVisitNum());
            registeredWomanViewHolder.txtVisitDate.setText("," + woman.getVisitDate());
            registeredWomanViewHolder.txtVisitType.setText("," +woman.getVisitType());

            registeredWomanViewHolder.txtStatus.setText(woman.getHcmActTakStatus());
            if(woman.getHcmActionToBeTakenClient()!= null && woman.getHcmActionToBeTakenClient().length() > 0) {
                registeredWomanViewHolder.txtActiontobetaken.setText("Y");
            }else
                registeredWomanViewHolder.txtActiontobetaken.setText("N");


        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    @Override
    public int getItemCount() {
        return this.regComplList.size();
    }

    //    prepare bundle
    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        return b;
    }

    public void setAppState(AppState appState) {
        this.appState = appState;
    }
}
