package com.sc.stmansi.recyclerview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.pendingservices.IndicatorViewClickListener;
import com.sc.stmansi.repositories.ChildRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.tables.TblChildInfo;

import java.sql.SQLException;
import java.util.List;

import com.sc.stmansi.R;
import com.sc.stmansi.tables.TblChlParentDetails;
import com.sc.stmansi.tables.tblregisteredwomen;

public class ChildAdapter extends RecyclerView.Adapter<WomanViewHolder>{

    private ClickListener viewClickListener;
    List<TblChildInfo> listChildInfo;

    //Manikanta 10feb2021
    List<TblChlParentDetails> tblChlParentDetails;
    DatabaseHelper databaseHelper;

    Context context;
    private IndicatorViewClickListener indicatorViewClickListener;


    public ChildAdapter(Context context, List<TblChildInfo> listChildInfo) {
        this.viewClickListener = (ClickListener) context;
        this.indicatorViewClickListener = (IndicatorViewClickListener) context;
        this.listChildInfo = listChildInfo;
    }
    @NonNull
    @Override
    public WomanViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RelativeLayout v = (RelativeLayout) LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.adapter_childinfo, viewGroup, false);
        v.setOnClickListener(viewClickListener);
        context = viewGroup.getContext();
        return new WomanViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull WomanViewHolder womanViewHolder, final int i) {
        womanViewHolder.womanName.setText(listChildInfo.get(i).getChlBabyname());
        if(listChildInfo.get(i).getChlDeactDate()!=null && listChildInfo.get(i).getChlDeactDate().trim().length()>0) {
            womanViewHolder.tvWLItemPregSt.setText(context.getResources().getString(R.string.deact_date)+ listChildInfo.get(i).getChlDeactDate());
            womanViewHolder.tvWLItemPregSt.setVisibility(View.VISIBLE);
        }else
        {
            womanViewHolder.tvWLItemPregSt.setVisibility(View.GONE);


        }

//        womanViewHolder.womanRegDt.setText(listChildInfo.get(i).getChlGender());06MAy2021 Arpitha

//        06MAy2021 Arpitha
        if(listChildInfo.get(i).getChlGender()!=null && listChildInfo.get(i).getChlGender().toLowerCase().equalsIgnoreCase("male"))
        womanViewHolder.womanRegDt.setText(context.getResources().getString(R.string.radiodmale1));
        else if(listChildInfo.get(i).getChlGender()!=null &&
                listChildInfo.get(i).getChlGender().toLowerCase().equalsIgnoreCase("female"))
            womanViewHolder.womanRegDt.setText(context.getResources().getString(R.string.radiodfemale1));




            //Manikanta 11feb2021 displaying mother/Guardian name
        databaseHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
        ChildRepository childRepository = new ChildRepository(databaseHelper);
        if (listChildInfo.get(i).getChlParentId() != null && listChildInfo.get(i).getChlParentId().trim().length() > 0) {
            try {
                tblChlParentDetails = childRepository.getParentDetails(listChildInfo.get(i).getChlParentId());
                    womanViewHolder.tvParentsName.setText(context.getResources().getString(R.string.babyofnew)+" "+tblChlParentDetails.get(0).getChlMotherName());
                    womanViewHolder.tvParentsName.setTextColor(context.getResources().getColor(R.color.darkblue));
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }else{
            WomanRepository womanRepository = new WomanRepository(databaseHelper);
            try {
                tblregisteredwomen woman = womanRepository.getRegistartionDetails(listChildInfo.get(i).getWomanId(),listChildInfo.get(i).getUserId());
                womanViewHolder.tvParentsName.setText(context.getResources().getString(R.string.babyofnew)+" "+ woman.getRegWomanName());
                womanViewHolder.tvParentsName.setTextColor(context.getResources().getColor(R.color.darkblue));
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }

        }




        if(DateTimeUtil.calculateAge(listChildInfo.get(i).getChlDateTimeOfBirth())!=null &&
                DateTimeUtil.calculateAge(listChildInfo.get(i).getChlDateTimeOfBirth()).trim().length()>0)
        womanViewHolder.tvWLItemRegNum.setText(context.getResources().getString(R.string.age)+ ": "+
                DateTimeUtil.calculateAge(listChildInfo.get(i).getChlDateTimeOfBirth()));
        else
            womanViewHolder.tvWLItemRegNum.setText(context.getResources().getString(R.string.borntoday));
//      /* else
//       */     womanViewHolder.tvWLItemRegNum.setText(context.getResources().getString(R.string.age)+ ": "+ DateTimeUtil.calculateAge(listChildInfo.get(i).getChlDateTimeOfBirth()));

        womanViewHolder.txtdelplan.setText(listChildInfo.get(i).getChlGender());
        if(listChildInfo.get(i).getChildRCHID()!=null && listChildInfo.get(i).getChildRCHID().trim().length()>0)
        womanViewHolder.txtmothername.setText(context.getResources().getString(R.string.rchid)+ ": "+
                listChildInfo.get(i).getChildRCHID());
        else
            womanViewHolder.txtmothername.setText("");


        if((listChildInfo.get(i).getDelBabyWeight()!=null && listChildInfo.get(i).getDelBabyWeight().trim().length()>0
        && Double.parseDouble(listChildInfo.get(i).getDelBabyWeight())<2500 && listChildInfo.get(i).getChlReg()==0)|| (listChildInfo.get(i).getChlComplications()!=null &&
                listChildInfo.get(i).getChlComplications().trim().length()>0
        && !(listChildInfo.get(i).getChlComplications().equalsIgnoreCase("0"))))
        womanViewHolder.imgPrevPregComp.setVisibility(View.VISIBLE);
        else
            womanViewHolder.imgPrevPregComp.setVisibility(View.GONE);

        womanViewHolder.imgPrevPregComp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                indicatorViewClickListener.displayAlert(i);

            }
        });

        //mani 17sep2021
        if ( listChildInfo.get(i).getChlReg() == 0){
            womanViewHolder.childtype.setText(context.getResources().getString(R.string.newbornchild));
        }else {
            womanViewHolder.childtype.setText(context.getResources().getString(R.string.directchild));
        }

    }


    @Override
    public int getItemCount() {
        return listChildInfo.size();
    }
}
