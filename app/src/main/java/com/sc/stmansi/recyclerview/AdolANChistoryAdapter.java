package com.sc.stmansi.recyclerview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.sc.stmansi.R;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.pendingservices.IndicatorViewClickListener;
import com.sc.stmansi.tables.tblregisteredwomen;

import java.util.List;




public class AdolANChistoryAdapter extends RecyclerView.Adapter<AdolPregViewHolder> {

    private ClickListener viewClickListener;
    private List<tblregisteredwomen> registeredWomenList;
    Context context;
    private IndicatorViewClickListener indicatorViewClickListener;
    Fragment f;
    private AppState appState;

    public AdolANChistoryAdapter(List<tblregisteredwomen> registeredWomenList,
                        Context context) {
        this.viewClickListener = (ClickListener) context;
        this.indicatorViewClickListener = (IndicatorViewClickListener) context;
        this.registeredWomenList = registeredWomenList;
        this.context = context;
    }

    public AdolANChistoryAdapter(List<tblregisteredwomen> registeredWomenList,
                        Context context, Fragment fragment) {
        this.viewClickListener = (ClickListener) fragment;
        this.indicatorViewClickListener = (IndicatorViewClickListener) fragment;
        this.registeredWomenList = registeredWomenList;
        this.f = fragment;
        this.context = context;
    }

    @NonNull
    @Override
    public AdolPregViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RelativeLayout v = (RelativeLayout) LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.adapter_adolanchist, viewGroup, false);
        v.setOnClickListener(viewClickListener);
        return new AdolPregViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AdolPregViewHolder registeredWomanViewHolder, final int i) {
        try {
            final tblregisteredwomen woman = registeredWomenList.get(i);

            registeredWomanViewHolder.womanName.setText(woman.getRegStatusWhileRegistration());


                   int noOfDays = DateTimeUtil.getNumberOfDaysFromLMP(woman.getRegLMP());
                    int noOfWeeks = noOfDays / 7;
                    int days = noOfDays % 7;
                    if (days > 0) {
                        if (days == 1)
                            registeredWomanViewHolder.womanName.setText((noOfWeeks + " " + context.getResources().getString(R.string.weeks)) + " " + days + " " + context.getResources().getString(R.string.day) + " " + (woman.getRegStatusWhileRegistration().contains("Primi") ? " Primi " : ""));
                        else
                            registeredWomanViewHolder.womanName.setText((noOfWeeks + " " + context.getResources().getString(R.string.weeks)) + " " + days + " " + context.getResources().getString(R.string.days) + " " + (woman.getRegStatusWhileRegistration().contains("Primi") ? " Primi " : ""));
                    } else
                        registeredWomanViewHolder.womanName.setText((noOfWeeks + " " + context.getResources().getString(R.string.weeks)) + " " + (woman.getRegStatusWhileRegistration().contains("Primi") ? " Primi " : ""));
               // }



            registeredWomanViewHolder.womanRegDt.setText(context.getResources().getString(R.string.tvregdate)+" : " + woman.getRegRegistrationDate());//06May2021 Arpitha - from strimgs file
            if (woman.getRegComplicatedpreg() == 1 && ((woman.getRegriskFactors() != null
                    && woman.getRegriskFactors().length() > 0) ||  (woman.getRegRiskFactorsByCC()!=null && woman.getRegRiskFactorsByCC().trim().length()>0))) {//Arpitha - 06Aug2021 Arpitha
                int code = woman.getRegAmberOrRedColorCode();

                int drawable = 0;
                if (code == 9)
                    drawable = R.drawable.amberone;
                else if (code == 1)
                    drawable = R.drawable.redone;
                else if (code == 2)
                    drawable = R.drawable.redtwo;
                else if (code == 3)
                    drawable = R.drawable.redthree;
                else if (code > 3 && code != 11)
                    drawable = R.drawable.redfour;
                else if (code == 11)
                    drawable = R.drawable.ambertwo;

                registeredWomanViewHolder.imgPrevPregComp.setImageResource(drawable);
                registeredWomanViewHolder.imgPrevPregComp.setVisibility(View.VISIBLE);

                registeredWomanViewHolder.imgPrevPregComp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        indicatorViewClickListener.displayAlert(i);
                    }
                });




            } else
                registeredWomanViewHolder.imgPrevPregComp.setVisibility(View.GONE);


            //                12Dec2019 Arpitha
            if(woman.getRegInDanger()==1) {
                registeredWomanViewHolder.imgDelCompl.setVisibility(View.VISIBLE);
            }else
                registeredWomanViewHolder.imgDelCompl.setVisibility(View.GONE);



            registeredWomanViewHolder.imgLMPnotconfirmed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    indicatorViewClickListener.displayLMPConfirmation(i);
                }
            });

            registeredWomanViewHolder.imgDelCompl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    indicatorViewClickListener.displayDelComplicationAlert(i);
                }
            });

       /*     //16Aug2019 - Bindu set gest age based on curr date from lmp
            if (woman.getCurrentWomenStatus() == null || woman.getCurrentWomenStatus().length() == 0) {
                if (woman.getRegADDate() == null || (woman.getRegADDate() != null && woman.getRegADDate().length() == 0)) {
                    //int noOfWeeks = CommonClass.getNumberOfWeeksFromLMP(woman.getRegLMP());

                    //07Apr2021 Bindu add LMP confirmation icon
                    if (woman.getIsLMPConfirmed() == 1 ) {
                        registeredWomanViewHolder.imgLMPnotconfirmed.setVisibility(View.GONE);
                    } else
                        registeredWomanViewHolder.imgLMPnotconfirmed.setVisibility(View.VISIBLE);

                    int noOfDays = DateTimeUtil.getNumberOfDaysFromLMP(woman.getRegLMP());
                    int noOfWeeks = noOfDays / 7;
                    int days = noOfDays % 7;
                    if (days > 0) {
                        if (days == 1)
                            registeredWomanViewHolder.tvWLItemPregSt.setText((noOfWeeks + " " + context.getResources().getString(R.string.weeks)) + " " + days + " " + context.getResources().getString(R.string.day) + " " + (woman.getRegStatusWhileRegistration().contains("Primi") ? " Primi " : ""));
                        else
                            registeredWomanViewHolder.tvWLItemPregSt.setText((noOfWeeks + " " + context.getResources().getString(R.string.weeks)) + " " + days + " " + context.getResources().getString(R.string.days) + " " + (woman.getRegStatusWhileRegistration().contains("Primi") ? " Primi " : ""));
                    } else
                        registeredWomanViewHolder.tvWLItemPregSt.setText((noOfWeeks + " " + context.getResources().getString(R.string.weeks)) + " " + (woman.getRegStatusWhileRegistration().contains("Primi") ? " Primi " : ""));
                }else {
                    registeredWomanViewHolder.tvWLItemPregSt.setText(context.getResources().getString(R.string.del_date) + ": " + woman.getRegADDate());//31Oct2019 Arpitha
                    registeredWomanViewHolder.imgLMPnotconfirmed.setVisibility(View.GONE);
                }
            }else {
                registeredWomanViewHolder.tvWLItemPregSt.setText(context.getResources().getString(R.string.del_date) + ": " + woman.getRegADDate());//31Oct2019 Arpitha
                registeredWomanViewHolder.imgLMPnotconfirmed.setVisibility(View.GONE);
            }*/
            // 05Aug2019 - Bindu add condition if placeofdel exists
            if (woman.getRegrecommendedPlaceOfDelivery().trim().length() > 0 && woman.getRegPregnantorMother()!=2) {//04Dec2019 Arpitha
                int daysDiff = DateTimeUtil.getDaysBetweenDates(DateTimeUtil.getTodaysDate(), woman.getRegLMP());
                if(woman.getDateDeactivated()!=null && woman.getDateDeactivated().trim().length()>0)
                {
                    registeredWomanViewHolder.txtdelplan.setVisibility(View.VISIBLE);
                    registeredWomanViewHolder.txtdelplan.setText(context.getResources().getString(R.string.deact_date)  +
                            woman.getDateDeactivated());

                }
                else  if (daysDiff > 210 && woman.getDateDeactivated()==null) {//check deact - 25Nov2019 Arpitha
                    registeredWomanViewHolder.txtdelplan.setVisibility(View.VISIBLE);
                    registeredWomanViewHolder.txtdelplan.setText(context.getResources().getString(R.string.delplannedat) + woman.getRegrecommendedPlaceOfDelivery());
                }else
                    registeredWomanViewHolder.txtdelplan.setVisibility(View.GONE);
            }//Arpitha 30Nov2019
            else
            {
                if(woman.getDateDeactivated()!=null && woman.getDateDeactivated().trim().length()>0)
                {
                    registeredWomanViewHolder.txtdelplan.setVisibility(View.VISIBLE);
                    registeredWomanViewHolder.txtdelplan.setText(context.getResources().getString(R.string.deact_date) +
                            woman.getDateDeactivated());
                    //  registeredWomanViewHolder.imgLMPnotconfirmed.setVisibility(View.GONE); //26Apr2021 Bindu hide lmp not confirmed
                }else
                    registeredWomanViewHolder.txtdelplan.setVisibility(View.GONE);
            }

            //13Sep2019 - Bindu - ADD Home visit info
            if (woman.getIsReferred() != null && woman.getIsReferred().equalsIgnoreCase(context.getResources().getString(R.string.yesshortform))) {
                registeredWomanViewHolder.imgHmVisitDetails.setImageResource(R.drawable.ic_refer);
                registeredWomanViewHolder.imgHmVisitDetails.setVisibility(View.VISIBLE);
            } else if (((woman.getLastAncVisitDate() != null && woman.getLastAncVisitDate().trim().length() > 0) || (woman.getLastPncVisitDate() != null && woman.getLastPncVisitDate().trim().length() > 0)) && (woman.getIsCompl() != null && woman.getIsCompl().equalsIgnoreCase(context.getResources().getString(R.string.yesshortform)))) {
                registeredWomanViewHolder.imgHmVisitDetails.setImageResource(R.drawable.ic_hvcompl);
                registeredWomanViewHolder.imgHmVisitDetails.setVisibility(View.VISIBLE);
            } else if (((woman.getLastAncVisitDate() != null && woman.getLastAncVisitDate().trim().length() > 0) || (woman.getLastPncVisitDate() != null && woman.getLastPncVisitDate().trim().length() > 0)) && (woman.getIsCompl() != null && woman.getIsCompl().equalsIgnoreCase(context.getResources().getString(R.string.noshortform)))) {
                registeredWomanViewHolder.imgHmVisitDetails.setImageResource(R.drawable.ic_hvnocompl);
                registeredWomanViewHolder.imgHmVisitDetails.setVisibility(View.VISIBLE);
            } else
                registeredWomanViewHolder.imgHmVisitDetails.setVisibility(View.GONE);  //30Sep2019 - Bindu - Home visit icon display based on cond

            //27Sep2019 - Bindu - Home visit icon click listener
            registeredWomanViewHolder.imgHmVisitDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    indicatorViewClickListener.displayHomeVisitList(i);
                }
            });


//            18Dec2019 Arpitha
            if(woman.getDateDeactivated()!=null && woman.getDateDeactivated().trim().length()>0)
                registeredWomanViewHolder.womanName.setTextColor(context.getResources().getColor(R.color.red));
            else
                registeredWomanViewHolder.womanName.setTextColor(context.getResources().getColor(R.color.black));

            /*if(woman.getRegWomenImage()!=null && woman.getRegWomenImage().trim().length()>0)
            {

                byte [] encodeByte= Base64.decode(woman.getRegWomenImage(),Base64.DEFAULT);
                Bitmap bitmap= BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);


                if (bitmap!=null){ //Ramesh 3-8-2021 If image is null, setting default image
                    registeredWomanViewHolder.womanImg.setImageBitmap(bitmap);
                }else {
                    registeredWomanViewHolder.womanImg.setImageResource(R.drawable.women);
                }
            }
            else
                registeredWomanViewHolder.womanImg.setImageDrawable(context.getResources().getDrawable(R.drawable.women));
*/

            if (woman.getRegCCConfirmRisk()!=null && woman.getRegCCConfirmRisk().trim().length()>0
                    && woman.getRegCCConfirmRisk().equalsIgnoreCase("Y"))

            {
                registeredWomanViewHolder.ivhrp.setVisibility(View.VISIBLE);
            }else
                registeredWomanViewHolder.ivhrp.setVisibility(View.GONE);


//           02Sep2021 Arpitha
            if(woman.getRegIsAdolescent()!=null && woman.getRegIsAdolescent().trim().length()>0
                    && woman.getRegIsAdolescent().equalsIgnoreCase("Yes"))
                registeredWomanViewHolder.txtAdol.setVisibility(View.VISIBLE);
            else
                registeredWomanViewHolder.txtAdol.setVisibility(View.GONE);
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    @Override
    public int getItemCount() {
        return this.registeredWomenList.size();
    }

    //    prepare bundle
    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        return b;
    }

    public void setAppState(AppState appState) {
        this.appState = appState;
    }
}
