package com.sc.stmansi.recyclerview;

import com.sc.stmansi.repositories.AdolescentRepository;
import com.sc.stmansi.repositories.CampaignRepository;
import com.sc.stmansi.repositories.CaseMgmtRepository;
import com.sc.stmansi.repositories.FollowUpRepository;
import com.sc.stmansi.repositories.WomanRepository;

import java.sql.SQLException;
import java.util.Map;

public interface RecyclerViewUpdateListener {
//    void updateList(String villageTitle, Map<String, Integer> villageNameForCode,
//                    String textViewFilter, boolean checked);

    void updateList(String villageTitle, Map<String, Integer> villageNameForCode,
                    String textViewFilter, boolean checked);
        default long getTotalWomenCount(String userId,
                                    int villageCode,
                                    String womenCondition,
                                    String filter,
                                    boolean isLMPNotConfirmed,
                                    WomanRepository womanRepository,boolean all) throws SQLException {
     //   return womanRepository.getTotalWomenCount(userId, villageCode, womenCondition, filter, isLMPNotConfirmed);
        return womanRepository.getWomanListCount(userId, villageCode, womenCondition, filter,all, isLMPNotConfirmed);
    }

//    20Ma2021 Arpitha
    default  long getRecordCount(int villageCode, boolean all,
                                 AdolescentRepository adolescentRepository, String wFilter,String search) throws SQLException {

        return adolescentRepository.getRegisteredAdolsCount(villageCode, all, wFilter,search);
    }

    //    20Ma2021 Arpitha
    default  long getCampaignsCount(String year, String month, int villageCode,
                                    CampaignRepository campaignRepository,int search) throws Exception {

        return campaignRepository.getCampaignsCount(year,month, villageCode,search);
    }

    default  long getWomanFollowUpCount(int villageCode, String womenCondition, String
            filter, FollowUpRepository followUpRepository, String fromDate, String toDate) throws SQLException {

        return followUpRepository.getWomanFollowUpDueCount(villageCode, womenCondition, filter,fromDate,toDate);
    }
    default  long getAdolFollowUpCount(FollowUpRepository followUpRepository,
                                       int villageCode, String
                                               filter, String fromDate, String toDate) throws SQLException {

        return followUpRepository.getAdolFollowUpDueCount( villageCode,
                fromDate,  toDate,filter,10);
    }

    default long getCaseRecordsCount(CaseMgmtRepository caseMgmtRepository, int villageCode, String filter, String beneficaryType)throws SQLException{
            return caseMgmtRepository.getCaseRecordsCount(villageCode,filter,beneficaryType,10);
    }

}
