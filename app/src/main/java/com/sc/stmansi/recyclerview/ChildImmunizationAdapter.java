package com.sc.stmansi.recyclerview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.tables.ChildImmunization;
import com.sc.stmansi.tables.TblChildInfo;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.sc.stmansi.R;

public class ChildImmunizationAdapter extends RecyclerView.Adapter<WomanServiceViewHolder> {

    List<ChildImmunization> tblservicestypemasterList;
    private ClickListener viewClickListener;
//    TblChildInfo tblChildInfo;
    Context context;
    String dob;



    public ChildImmunizationAdapter(List<ChildImmunization> tblservicestypemasterList,
                                    ClickListener viewClickListener,
                                    TblChildInfo tblChildInfo, String dob) {

      this.tblservicestypemasterList = tblservicestypemasterList;
        this.viewClickListener = viewClickListener;
//        this.tblChildInfo = tblChildInfo;
        this.dob = dob;
    }


    @NonNull
    @Override
    public WomanServiceViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RelativeLayout v = (RelativeLayout) LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.adapter_services, viewGroup, false);
        context = viewGroup.getContext();
               v.setOnClickListener(viewClickListener);
        return new WomanServiceViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final WomanServiceViewHolder womanServiceViewHolder, int i) {

try {

    String strDueDateMin, strDueDateMax = "", strDob = "";

    strDob = dob;


    strDueDateMin = getNextDate(strDob,
            tblservicestypemasterList.get(i).getDaysDiffStart(),
            strDob, false);

    strDueDateMax = getNextDate
            (strDob,
                    tblservicestypemasterList.get(i).getDaysDiffEnd(),
                    strDob, false);

    String serviceType = tblservicestypemasterList.get(i).getServiceType();
    int identifier = context.getResources().getIdentifier(serviceType.toLowerCase(), "string", "com.sc.stmansi");
    womanServiceViewHolder.ServiceType.
            setText(context.getResources().getString(identifier));

    womanServiceViewHolder.DueDateMin.setText(strDueDateMin);

    womanServiceViewHolder.DueDateMax.setText(strDueDateMax);

    womanServiceViewHolder.imgind.setImageDrawable(context.getResources()
            .getDrawable(R.drawable.immunisation));



    if(tblservicestypemasterList.get(i).getActualDateOfAction()!=null &&
            tblservicestypemasterList.get(i).getActualDateOfAction().trim().length()>0)
        womanServiceViewHolder.imgstatus.setImageResource(R.drawable.tick);
    else if(new SimpleDateFormat("dd-MM-yyyy").parse(strDueDateMax).before(new SimpleDateFormat("dd-MM-yyyy").parse(DateTimeUtil.getTodaysDate())))
        womanServiceViewHolder.imgstatus.setImageResource(R.drawable.warning);
    else if(new SimpleDateFormat("dd-MM-yyyy").parse(strDueDateMin).after(new Date()))
        womanServiceViewHolder.imgstatus.setImageResource(R.drawable.upcoming);
    else
        womanServiceViewHolder.imgstatus.setImageResource(R.drawable.notdone);


    womanServiceViewHolder.ActualDateOfAction.setText(tblservicestypemasterList.get(i).getActualDateOfAction());



}catch (Exception e)
{
    e.printStackTrace();
}
    }

    @Override
    public int getItemCount() {
        return tblservicestypemasterList.size();
    }

    //   get Next date
    public String getNextDate(String srcDate, int interval, String td, boolean isMaxDt)
            throws Exception {


        SimpleDateFormat xSimpleDate = new SimpleDateFormat("dd-MM-yyyy");
        String nxtDt = null;

        if (srcDate!=null && srcDate.length() != 0) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(xSimpleDate.parse(srcDate));
            cal.add(Calendar.DATE, interval);
            Date xDt = cal.getTime();

                Date eddDt = xSimpleDate.parse(td);
                int i = xDt.compareTo(eddDt);
                    nxtDt = xSimpleDate.format(xDt);
            return nxtDt;
        }
        return nxtDt;
    }

    private void highlightView(WomanServiceViewHolder holder) {
        holder.itemView.setBackgroundColor(ContextCompat.getColor(context, R.color.blue));
    }

    private void unhighlightView(WomanServiceViewHolder holder) {
        holder.itemView.setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent));
    }
}
