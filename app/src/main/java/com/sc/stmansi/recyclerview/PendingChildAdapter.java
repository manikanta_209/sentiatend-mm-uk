package com.sc.stmansi.recyclerview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.pendingservices.IndicatorViewClickListener;
import com.sc.stmansi.repositories.ChildRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.tables.ChildImmunization;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.sc.stmansi.R;
import com.sc.stmansi.tables.TblChildInfo;
import com.sc.stmansi.tables.TblChlParentDetails;
import com.sc.stmansi.tables.tblregisteredwomen;

public class PendingChildAdapter extends RecyclerView.Adapter<WomanViewHolder> {

    private ClickListener viewClickListener;
    List<ChildImmunization> listChildInfo;
    Context context;
    private IndicatorViewClickListener indicatorViewClickListener;

    //Manikanta 10feb2021
    List<TblChlParentDetails> tblChlParentDetails;
    DatabaseHelper databaseHelper;
    List<TblChildInfo> childInfo;


    public PendingChildAdapter(
            Context context, List<ChildImmunization> listChildInfo) {
        this.viewClickListener = (ClickListener) context;
        this.indicatorViewClickListener = (IndicatorViewClickListener) context;
        this.listChildInfo = listChildInfo;
    }

    @NonNull
    @Override
    public WomanViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RelativeLayout v = (RelativeLayout) LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.adapter_childinfo, viewGroup, false);
        v.setOnClickListener(viewClickListener);
        context = viewGroup.getContext();
        return new WomanViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull WomanViewHolder womanViewHolder, final int i) {

        try {
            womanViewHolder.womanName.setText(listChildInfo.get(i).getChlName());
//        if(listChildInfo.get(i).getChlReg()==1)
//            womanViewHolder.tvWLItemPregSt.setText(context.getResources().getString(R.string.childreg));

//            womanViewHolder.womanRegDt.setText(listChildInfo.get(i).getChlGender());06MAy2021 Arpitha

            //        06MAy2021 Arpitha
            if(listChildInfo.get(i).getChlGender()!=null && listChildInfo.get(i).getChlGender().toLowerCase().equalsIgnoreCase("male"))
                womanViewHolder.womanRegDt.setText(context.getResources().getString(R.string.radiodmale1));
            else if(listChildInfo.get(i).getChlGender()!=null &&
                    listChildInfo.get(i).getChlGender().toLowerCase().equalsIgnoreCase("female"))
                womanViewHolder.womanRegDt.setText(context.getResources().getString(R.string.radiodfemale1));


            womanViewHolder.tvWLItemRegNum.setText(context.getResources().getString(R.string.age) + ": " + DateTimeUtil.calculateAge(listChildInfo.get(i).getChlDateofBirth()));
//        womanViewHolder.txtmothername.setText(listChildInfo.get(i).getChlMotherName());

        /*if(listChildInfo.get(i).getChildRCHID()!=null && listChildInfo.get(i).getChildRCHID().trim().length()>0)
//            womanViewHolder.txtmothername.setText(context.getResources().getString(R.string.rchid)+ ": "+listChildInfo.get(i).getChildRCHID());
        else*/
//            womanViewHolder.txtmothername.setVisibility(View.GONE);

            womanViewHolder.txtmothername.setText(listChildInfo.get(i)
                    .getChlGender());


            //Manikanta 11feb2021 displaying mother/Guardian name
            databaseHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
            ChildRepository childRepository = new ChildRepository(databaseHelper);
                if (listChildInfo.get(i).getChlParentId() != null && listChildInfo.get(i).getChlParentId().trim().length() > 0) {
                    tblChlParentDetails = childRepository.getParentDetails(listChildInfo.get(i).getChlParentId());
                        womanViewHolder.tvParentsName.setText(context.getResources().getString(R.string.babyofnew)+" "+tblChlParentDetails.get(0).getChlMotherName());
                        womanViewHolder.tvParentsName.setTextColor(context.getResources().getColor(R.color.darkblue));

                } else {
                    WomanRepository womanRepository = new WomanRepository(databaseHelper);
                    try {
                        tblregisteredwomen woman = womanRepository.getRegistartionDetails(listChildInfo.get(i).getWomanId(), listChildInfo.get(i).getUserId());
                        womanViewHolder.tvParentsName.setText(context.getResources().getString(R.string.babyofnew)+" "+ woman.getRegWomanName());
                        womanViewHolder.tvParentsName.setTextColor(context.getResources().getColor(R.color.darkblue));
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                }




            if (listChildInfo.get(i).getChlComplications() != null &&
                    listChildInfo.get(i).getChlComplications().trim().length() > 0
                    && !(listChildInfo.get(i).getChlComplications().equalsIgnoreCase("0")))
                womanViewHolder.imgPrevPregComp.setVisibility(View.VISIBLE);
            else
                womanViewHolder.imgPrevPregComp.setVisibility(View.GONE);

            womanViewHolder.imgPrevPregComp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    indicatorViewClickListener.displayAlert(i);

                }
            });


            String strDueDateMin, strDueDateMax = "", strDob = "";

            strDob = listChildInfo.get(i).getChlDateofBirth();


            strDueDateMin = getNextDate(strDob,
                    listChildInfo.get(i).getDaysDiffStart(),
                    strDob, false);

            strDueDateMax = getNextDate
                    (strDob,
                            listChildInfo.get(i).getDaysDiffEnd(),
                            strDob, false);

            womanViewHolder.txtdelplan.setText(strDueDateMin + "  -  " + strDueDateMax);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        return listChildInfo.size();
    }

    //   get Next date
    public String getNextDate(String srcDate, int interval, String td, boolean isMaxDt)
            throws Exception {


        SimpleDateFormat xSimpleDate = new SimpleDateFormat("dd-MM-yyyy");
        String nxtDt = null;

        if (srcDate != null && srcDate.length() != 0) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(xSimpleDate.parse(srcDate));
            cal.add(Calendar.DATE, interval);
            Date xDt = cal.getTime();

            Date eddDt = xSimpleDate.parse(td);
            int i = xDt.compareTo(eddDt);
            nxtDt = xSimpleDate.format(xDt);
            return nxtDt;
        }
        return nxtDt;
    }

}
