package com.sc.stmansi.recyclerview;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sc.stmansi.R;

public class WomanServiceViewHolder extends RecyclerView.ViewHolder {

    public TextView ServiceType, ActualDateOfAction, DueDateMin, DueDateMax, txtDuedateMaxL;
    public ImageView imgstatus, imgind;

    public WomanServiceViewHolder(@NonNull View itemView) {
        super(itemView);
        ServiceType = itemView.findViewById(R.id.tvservicetype);
        ActualDateOfAction = itemView.findViewById(R.id.tvActualdateofaction);
        DueDateMin = itemView.findViewById(R.id.txtDueDateMin);
        DueDateMax = itemView.findViewById(R.id.txtDueDateMax);
        imgstatus = itemView.findViewById(R.id.imgstatus);
        imgind = itemView.findViewById(R.id.imgind);
        txtDuedateMaxL = itemView.findViewById(R.id.txtDuedateMaxL);
    }
}
