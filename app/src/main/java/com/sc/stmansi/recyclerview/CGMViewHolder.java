package com.sc.stmansi.recyclerview;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sc.stmansi.R;


public class CGMViewHolder extends RecyclerView.ViewHolder {

    public TextView visitnum;
    public  TextView childAge, childVisitdate, childWeightAtVisit;
    public  ImageView childWeightIndicator;
    public CGMViewHolder(@NonNull View itemView) {
        super(itemView);
        visitnum=itemView.findViewById(R.id.txtCgmVisitNum);
        childAge=itemView.findViewById(R.id.txtCgmAgeatVisit);
        childVisitdate=itemView.findViewById(R.id.txtCgmVisitDate);
        childWeightAtVisit=itemView.findViewById(R.id.txtCgmWeightatVisit);
        childWeightIndicator=itemView.findViewById(R.id.txtCgmIndicator);
    }
}
