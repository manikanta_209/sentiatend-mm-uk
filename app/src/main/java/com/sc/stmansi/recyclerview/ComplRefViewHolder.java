package com.sc.stmansi.recyclerview;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sc.stmansi.R;

// Provide a reference to the views for each data item
// Complex data items may need more than one view per item, and
// you provide access to all the views for a data item in a view holder
public class ComplRefViewHolder extends RecyclerView.ViewHolder {

    ImageView womanImg, imgPrevPregComp, imgHmVisitDetails;
    TextView womanName, tvWLItemPregSt, womanRegDt;
    TextView txtmothername, txtdelplan, tvWLItemRegNum;
    TextView txtVisitNum, txtVisitType, txtVisitDate, txtStatus, txtActiontobetaken;

    public ComplRefViewHolder(@NonNull View itemView) {
        super(itemView);
        womanImg = itemView.findViewById(R.id.ivWLItemImg);
        imgPrevPregComp = itemView.findViewById(R.id.imgPrevPregComp);
        womanName = itemView.findViewById(R.id.tvWLItemWomanName);
        tvWLItemPregSt = itemView.findViewById(R.id.tvWLItemPregSt);
        txtdelplan = itemView.findViewById(R.id.txtdelplan);
        imgHmVisitDetails = itemView.findViewById(R.id.imgVisitDetails);
        txtdelplan = itemView.findViewById(R.id.txtdelplan);
        txtVisitNum = itemView.findViewById(R.id.tvVisitNum);
        txtVisitDate = itemView.findViewById(R.id.tvVisitDate);
        txtVisitType = itemView.findViewById(R.id.tvVisitType);
        txtStatus = itemView.findViewById(R.id.txtstatus);
        txtActiontobetaken = itemView.findViewById(R.id.txtactiontakenval);

    }
}
