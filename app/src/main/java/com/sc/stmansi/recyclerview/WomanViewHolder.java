package com.sc.stmansi.recyclerview;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sc.stmansi.R;

// Provide a reference to the views for each data item
// Complex data items may need more than one view per item, and
// you provide access to all the views for a data item in a view holder
public class WomanViewHolder extends RecyclerView.ViewHolder {

    ImageView womanImg, imgPrevPregComp, imgHmVisitDetails, imgDelCompl, imgLMPnotconfirmed;
    //Manikanta 10feb2021 added new field tvParentName
    TextView womanName, tvWLItemPregSt, womanRegDt ,tvParentsName;
    TextView txtmothername, txtdelplan, tvWLItemRegNum;
    ImageView ivhrp;
    TextView txtAdol;//02Sep2021 Arpitha
    TextView childtype;

    public WomanViewHolder(@NonNull View itemView) {
        super(itemView);
        womanImg = itemView.findViewById(R.id.ivWLItemImg);
        imgPrevPregComp = itemView.findViewById(R.id.imgPrevPregComp);
        womanName = itemView.findViewById(R.id.tvWLItemWomanName);
        tvWLItemPregSt = itemView.findViewById(R.id.tvWLItemPregSt);
        womanRegDt = itemView.findViewById(R.id.tvWLItemRegNum);
        txtmothername = itemView.findViewById(R.id.txtmothername);
        txtdelplan = itemView.findViewById(R.id.txtdelplan);
        tvWLItemRegNum = itemView.findViewById(R.id.tvWLItemRegNum);
        imgHmVisitDetails = itemView.findViewById(R.id.imgVisitDetails);
        txtdelplan = itemView.findViewById(R.id.txtdelplan);
        imgDelCompl = itemView.findViewById(R.id.imgDelComp);

        //Manikanta 10feb2021
        tvParentsName=itemView.findViewById(R.id.txtparentsname);
//    07Apr2021 Bindu
        imgLMPnotconfirmed = itemView.findViewById(R.id.imgLMPnotconfirmed);
        ivhrp = itemView.findViewById(R.id.txthrp);
        txtAdol = itemView.findViewById(R.id.txtadol);//03Sep2021 Arpitha

        childtype=itemView.findViewById(R.id.txtchildtype);

    }
}
