package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;

public class tblAdolReg implements Serializable {

    @DatabaseField
    private String userId;
    @DatabaseField (id = true)
    private String AdolID;
    @DatabaseField
    private String regAdolName;
    @DatabaseField
    private String regAdolAge;
    @DatabaseField
    private String regAdolGender;
    @DatabaseField
    private String regAdolDoB;
    @DatabaseField
    private String regAdolWeight;
    @DatabaseField
    private int regAdolHeightType;
    @DatabaseField
    private String regAdolHeight;
    @DatabaseField
    private String regAdolMobile;
    @DatabaseField
    private int regAdolMobileOf;
    @DatabaseField
    private String regAdolFather;
    @DatabaseField
    private String regAdolFatherDesc;
    @DatabaseField
    private String regAdolMother;
    @DatabaseField
    private String regAdolMotherDesc;
    @DatabaseField
    private String regAdolGuardian;
    @DatabaseField
    private int regAdolFamilytype;
    @DatabaseField
    private String regAdolPregnant;
    @DatabaseField
    private String regAdolAddress;
    @DatabaseField
    private String regAdolAreaname;
    @DatabaseField
    private int regAdolFacilities;
    @DatabaseField
    private String regAdolPincode;
    @DatabaseField
    private int regAdolRelationship;
    @DatabaseField
    private String regAdolPartnerName;
    @DatabaseField
    private String regAdolPartnerOccupa;
    @DatabaseField
    private int regAdolMarriageType;
    @DatabaseField
    private String regAdolGoingtoSchool;
    @DatabaseField
    private int regAdolNoofChild;
    @DatabaseField
    private String regAdolOtherHealthIssues;
    /*@DatabaseField
    private String regAdolAnemia;
    @DatabaseField
    private String regAdolUndernutrition;
    @DatabaseField
    private String regAdolObesity;
    @DatabaseField
    private String regAdolInjuries;
    @DatabaseField
    private String regAdolDepressandAnxi;
    @DatabaseField
    private String regAdolHivandAids;
    @DatabaseField
    private String regAdolInfectionDiseases;
    @DatabaseField
    private String regAdolOtherHealthIssues;
    @DatabaseField
    private String regAdolBackpain;
    @DatabaseField
    private String regAdolStomachache;
    @DatabaseField
    private String regAdolFatigue;
    @DatabaseField
    private String regAdolVomitting;
    @DatabaseField
    private String regAdolDiarrhea;
    @DatabaseField
    private String regAdolIrritability;
    @DatabaseField
    private String regAdolBloodloss;
    @DatabaseField
    private String regAdolMoodiness;
    @DatabaseField
    private String regAdolCycleIssue;
    @DatabaseField
    private String regAdolCramping;
    @DatabaseField
    private String regAdolOverbleeding;
    @DatabaseField
    private String regAdolHeadache;
    @DatabaseField
    private String regAdolEarlyPregancy;
    @DatabaseField
    private String regAdolAlcohol;
    @DatabaseField
    private String regAdolDrug;
    @DatabaseField
    private String regAdolTobacco;
    @DatabaseField
    private String regAdolSmoking;
    @DatabaseField
    private String regAdolWalking;
    @DatabaseField
    private String regAdolJogging;
    @DatabaseField
    private String regAdolYoga;
    @DatabaseField
    private String regAdolWorkout;
    @DatabaseField
    private String regAdolMeditation;*/
    @DatabaseField
    private String regAdolSuggestionGiven;
    @DatabaseField
    private String regAdolComments;
    @DatabaseField
    private int LastVisitCount;
    @DatabaseField
    private String regAdolAadharNo;
    @DatabaseField
    private String regAdolisPeriod;
    @DatabaseField
    private String regAdolAgeatMenstruated;
    @DatabaseField
    private int regAdolEducation;
    @DatabaseField
    private int transId;
    @DatabaseField
    private String RecordCreatedDate;
    @DatabaseField
    private String RecordUpdatedDate;
    @DatabaseField
    private String regAdolDeactivateDate;
    @DatabaseField
    private String regAdolDeactivateReasons;
    @DatabaseField
    private String regAdolDeactivateReasonOthers;
    @DatabaseField
    private String regAdolDeactivateComments;

//    20May2021 Bindu
    @DatabaseField
    private String regAdolHealthIssues;
    @DatabaseField
    private String regAdolLifeStyle;
    @DatabaseField
    private String regAdolMenstrualPbm;
    @DatabaseField
    private String regAdolPhysicalActivity;
    @DatabaseField
    private String regAdolOtherMenspbm;
    @DatabaseField
    private String regUserType;
    @DatabaseField
    private String regAdolImage;

    public String getRegAdolDeactivateDate() {
        return regAdolDeactivateDate;
    }

    public void setRegAdolDeactivateDate(String regAdolDeactivateDate) {
        this.regAdolDeactivateDate = regAdolDeactivateDate;
    }

    public String getRegAdolDeactivateReasons() {
        return regAdolDeactivateReasons;
    }

    public void setRegAdolDeactivateReasons(String regAdolDeactivateReasons) {
        this.regAdolDeactivateReasons = regAdolDeactivateReasons;
    }

    public String getRegAdolDeactivateReasonOthers() {
        return regAdolDeactivateReasonOthers;
    }

    public void setRegAdolDeactivateReasonOthers(String regAdolDeactivateReasonOthers) {
        this.regAdolDeactivateReasonOthers = regAdolDeactivateReasonOthers;
    }

    public String getRegAdolDeactivateComments() {
        return regAdolDeactivateComments;
    }

    public void setRegAdolDeactivateComments(String regAdolDeactivateComments) {
        this.regAdolDeactivateComments = regAdolDeactivateComments;
    }

    public int getRegAdolEducation() {
        return regAdolEducation;
    }

    public void setRegAdolEducation(int regAdolEducation) {
        this.regAdolEducation = regAdolEducation;
    }

    public String getRegAdolAadharNo() {
        return regAdolAadharNo;
    }

    public void setRegAdolAadharNo(String regAdolAadharNo) {
        this.regAdolAadharNo = regAdolAadharNo;
    }

    public String getRegAdolisPeriod() {
        return regAdolisPeriod;
    }

    public void setRegAdolisPeriod(String regAdolisPeriod) {
        this.regAdolisPeriod = regAdolisPeriod;
    }

    public String getRegAdolAgeatMenstruated() {
        return regAdolAgeatMenstruated;
    }

    public void setRegAdolAgeatMenstruated(String regAdolAgeatMenstruated) {
        this.regAdolAgeatMenstruated = regAdolAgeatMenstruated;
    }

    public int getLastVisitCount() {
        return LastVisitCount;
    }

    public void setLastVisitCount(int lastVisitCount) {
        LastVisitCount = lastVisitCount;
    }

    public int getTransId() {
        return transId;
    }

    public void setTransId(int transId) {
        this.transId = transId;
    }

    public String getRegAdolName() {
        return regAdolName;
    }

    public void setRegAdolName(String regAdolName) {
        this.regAdolName = regAdolName;
    }

    public String getRegAdolAge() {
        return regAdolAge;
    }

    public void setRegAdolAge(String regAdolAge) {
        this.regAdolAge = regAdolAge;
    }

    public String getRegAdolDoB() {
        return regAdolDoB;
    }

    public void setRegAdolDoB(String regAdolDoB) {
        this.regAdolDoB = regAdolDoB;
    }

    public String getRegAdolWeight() {
        return regAdolWeight;
    }

    public void setRegAdolWeight(String regAdolWeight) {
        this.regAdolWeight = regAdolWeight;
    }

    public int getRegAdolHeightType() {
        return regAdolHeightType;
    }

    public void setRegAdolHeightType(int regAdolHeightType) {
        this.regAdolHeightType = regAdolHeightType;
    }

    public String getRegAdolHeight() {
        return regAdolHeight;
    }

    public void setRegAdolHeight(String regAdolHeight) {
        this.regAdolHeight = regAdolHeight;
    }

    public String getRegAdolMobile() {
        return regAdolMobile;
    }

    public void setRegAdolMobile(String regAdolMobile) {
        this.regAdolMobile = regAdolMobile;
    }

    public int getRegAdolMobileOf() {
        return regAdolMobileOf;
    }

    public void setRegAdolMobileOf(int regAdolMobileOf) {
        this.regAdolMobileOf = regAdolMobileOf;
    }

    public String getRegAdolFather() {
        return regAdolFather;
    }

    public void setRegAdolFather(String regAdolFather) {
        this.regAdolFather = regAdolFather;
    }

    public String getRegAdolMother() {
        return regAdolMother;
    }

    public void setRegAdolMother(String regAdolMother) {
        this.regAdolMother = regAdolMother;
    }

    public int getRegAdolFamilytype() {
        return regAdolFamilytype;
    }

    public void setRegAdolFamilytype(int regAdolFamilytype) {
        this.regAdolFamilytype = regAdolFamilytype;
    }

    public String getRegAdolAddress() {
        return regAdolAddress;
    }

    public void setRegAdolAddress(String regAdolAddress) {
        this.regAdolAddress = regAdolAddress;
    }

    public String getRegAdolAreaname() {
        return regAdolAreaname;
    }

    public void setRegAdolAreaname(String regAdolAreaname) {
        this.regAdolAreaname = regAdolAreaname;
    }

    public String getRegAdolPincode() {
        return regAdolPincode;
    }

    public void setRegAdolPincode(String regAdolPincode) {
        this.regAdolPincode = regAdolPincode;
    }


    public String getRegAdolPartnerName() {
        return regAdolPartnerName;
    }

    public void setRegAdolPartnerName(String regAdolPartnerName) {
        this.regAdolPartnerName = regAdolPartnerName;
    }


    public int getRegAdolNoofChild() {
        return regAdolNoofChild;
    }

    public void setRegAdolNoofChild(int regAdolNoofChild) {
        this.regAdolNoofChild = regAdolNoofChild;
    }

    public String getAdolID() {
        return AdolID;
    }

    public void setAdolID(String adolID) {
        AdolID = adolID;
    }

    public String getRegAdolGender() {
        return regAdolGender;
    }

    public void setRegAdolGender(String regAdolGender) {
        this.regAdolGender = regAdolGender;
    }

    public String getRegAdolFatherDesc() {
        return regAdolFatherDesc;
    }

    public void setRegAdolFatherDesc(String regAdolFatherDesc) {
        this.regAdolFatherDesc = regAdolFatherDesc;
    }

    public String getRegAdolMotherDesc() {
        return regAdolMotherDesc;
    }

    public void setRegAdolMotherDesc(String regAdolMotherDesc) {
        this.regAdolMotherDesc = regAdolMotherDesc;
    }

    public String getRegAdolPregnant() {
        return regAdolPregnant;
    }

    public void setRegAdolPregnant(String regAdolPregnant) {
        this.regAdolPregnant = regAdolPregnant;
    }

    public String getRegAdolPartnerOccupa() {
        return regAdolPartnerOccupa;
    }

    public void setRegAdolPartnerOccupa(String regAdolPartnerOccupa) {
        this.regAdolPartnerOccupa = regAdolPartnerOccupa;
    }

    public int getRegAdolMarriageType() {
        return regAdolMarriageType;
    }

    public void setRegAdolMarriageType(int regAdolMarriageType) {
        this.regAdolMarriageType = regAdolMarriageType;
    }

    public String getRegAdolGoingtoSchool() {
        return regAdolGoingtoSchool;
    }

    public void setRegAdolGoingtoSchool(String regAdolGoingtoSchool) {
        this.regAdolGoingtoSchool = regAdolGoingtoSchool;
    }

  /*  public String getRegAdolAnemia() {
        return regAdolAnemia;
    }

    public void setRegAdolAnemia(String regAdolAnemia) {
        this.regAdolAnemia = regAdolAnemia;
    }

    public String getRegAdolObesity() {
        return regAdolObesity;
    }

    public void setRegAdolObesity(String regAdolObesity) {
        this.regAdolObesity = regAdolObesity;
    }

    public String getRegAdolInjuries() {
        return regAdolInjuries;
    }

    public void setRegAdolInjuries(String regAdolInjuries) {
        this.regAdolInjuries = regAdolInjuries;
    }

    public String getRegAdolDepressandAnxi() {
        return regAdolDepressandAnxi;
    }

    public void setRegAdolDepressandAnxi(String regAdolDepressandAnxi) {
        this.regAdolDepressandAnxi = regAdolDepressandAnxi;
    }

    public String getRegAdolHivandAids() {
        return regAdolHivandAids;
    }

    public void setRegAdolHivandAids(String regAdolHivandAids) {
        this.regAdolHivandAids = regAdolHivandAids;
    }

    public String getRegAdolInfectionDiseases() {
        return regAdolInfectionDiseases;
    }

    public void setRegAdolInfectionDiseases(String regAdolInfectionDiseases) {
        this.regAdolInfectionDiseases = regAdolInfectionDiseases;
    }

    public String getRegAdolBloodloss() {
        return regAdolBloodloss;
    }

    public void setRegAdolBloodloss(String regAdolBloodloss) {
        this.regAdolBloodloss = regAdolBloodloss;
    }

    public String getRegAdolMoodiness() {
        return regAdolMoodiness;
    }

    public void setRegAdolMoodiness(String regAdolMoodiness) {
        this.regAdolMoodiness = regAdolMoodiness;
    }

    public String getRegAdolCycleIssue() {
        return regAdolCycleIssue;
    }

    public void setRegAdolCycleIssue(String regAdolCycleIssue) {
        this.regAdolCycleIssue = regAdolCycleIssue;
    }

    public String getRegAdolCramping() {
        return regAdolCramping;
    }

    public void setRegAdolCramping(String regAdolCramping) {
        this.regAdolCramping = regAdolCramping;
    }

    public String getRegAdolAlcohol() {
        return regAdolAlcohol;
    }

    public void setRegAdolAlcohol(String regAdolAlcohol) {
        this.regAdolAlcohol = regAdolAlcohol;
    }

    public String getRegAdolDrug() {
        return regAdolDrug;
    }

    public void setRegAdolDrug(String regAdolDrug) {
        this.regAdolDrug = regAdolDrug;
    }

    public String getRegAdolTobacco() {
        return regAdolTobacco;
    }

    public void setRegAdolTobacco(String regAdolTobacco) {
        this.regAdolTobacco = regAdolTobacco;
    }

    public String getRegAdolSmoking() {
        return regAdolSmoking;
    }

    public void setRegAdolSmoking(String regAdolSmoking) {
        this.regAdolSmoking = regAdolSmoking;
    }
*/
    public String getRegAdolSuggestionGiven() {
        return regAdolSuggestionGiven;
    }

    public void setRegAdolSuggestionGiven(String regAdolSuggestionGiven) {
        this.regAdolSuggestionGiven = regAdolSuggestionGiven;
    }

    public String getRegAdolComments() {
        return regAdolComments;
    }

    public void setRegAdolComments(String regAdolComments) {
        this.regAdolComments = regAdolComments;
    }

    public String getRecordCreatedDate() {
        return RecordCreatedDate;
    }

    public void setRecordCreatedDate(String recordCreatedDate) {
        RecordCreatedDate = recordCreatedDate;
    }

    public String getRecordUpdatedDate() {
        return RecordUpdatedDate;
    }

    public void setRecordUpdatedDate(String recordUpdatedDate) {
        RecordUpdatedDate = recordUpdatedDate;
    }

    public String getRegAdolGuardian() {
        return regAdolGuardian;
    }

    public void setRegAdolGuardian(String regAdolGuardian) {
        this.regAdolGuardian = regAdolGuardian;
    }

    public int getRegAdolRelationship() {
        return regAdolRelationship;
    }

    public void setRegAdolRelationship(int regAdolRelationship) {
        this.regAdolRelationship = regAdolRelationship;
    }

    public String getRegAdolOtherHealthIssues() {
        return regAdolOtherHealthIssues;
    }

    public void setRegAdolOtherHealthIssues(String regAdolOtherHealthIssues) {
        this.regAdolOtherHealthIssues = regAdolOtherHealthIssues;
    }

   /* public String getRegAdolWalking() {
        return regAdolWalking;
    }

    public void setRegAdolWalking(String regAdolWalking) {
        this.regAdolWalking = regAdolWalking;
    }

    public String getRegAdolJogging() {
        return regAdolJogging;
    }

    public void setRegAdolJogging(String regAdolJogging) {
        this.regAdolJogging = regAdolJogging;
    }

    public String getRegAdolYoga() {
        return regAdolYoga;
    }

    public void setRegAdolYoga(String regAdolYoga) {
        this.regAdolYoga = regAdolYoga;
    }

    public String getRegAdolWorkout() {
        return regAdolWorkout;
    }

    public void setRegAdolWorkout(String regAdolWorkout) {
        this.regAdolWorkout = regAdolWorkout;
    }

    public String getRegAdolMeditation() {
        return regAdolMeditation;
    }

    public void setRegAdolMeditation(String regAdolMeditation) {
        this.regAdolMeditation = regAdolMeditation;
    }*/

    public int getRegAdolFacilities() {
        return regAdolFacilities;
    }

    public void setRegAdolFacilities(int regAdolFacilities) {
        this.regAdolFacilities = regAdolFacilities;
    }

    /*public String getRegAdolBackpain() {
        return regAdolBackpain;
    }

    public void setRegAdolBackpain(String regAdolBackpain) {
        this.regAdolBackpain = regAdolBackpain;
    }

    public String getRegAdolStomachache() {
        return regAdolStomachache;
    }

    public void setRegAdolStomachache(String regAdolStomachache) {
        this.regAdolStomachache = regAdolStomachache;
    }

    public String getRegAdolFatigue() {
        return regAdolFatigue;
    }

    public void setRegAdolFatigue(String regAdolFatigue) {
        this.regAdolFatigue = regAdolFatigue;
    }

    public String getRegAdolVomitting() {
        return regAdolVomitting;
    }

    public void setRegAdolVomitting(String regAdolVomitting) {
        this.regAdolVomitting = regAdolVomitting;
    }

    public String getRegAdolDiarrhea() {
        return regAdolDiarrhea;
    }

    public void setRegAdolDiarrhea(String regAdolDiarrhea) {
        this.regAdolDiarrhea = regAdolDiarrhea;
    }

    public String getRegAdolIrritability() {
        return regAdolIrritability;
    }

    public void setRegAdolIrritability(String regAdolIrritability) {
        this.regAdolIrritability = regAdolIrritability;
    }

    public String getRegAdolOverbleeding() {
        return regAdolOverbleeding;
    }

    public void setRegAdolOverbleeding(String regAdolOverbleeding) {
        this.regAdolOverbleeding = regAdolOverbleeding;
    }

    public String getRegAdolHeadache() {
        return regAdolHeadache;
    }

    public void setRegAdolHeadache(String regAdolHeadache) {
        this.regAdolHeadache = regAdolHeadache;
    }

    public String getRegAdolEarlyPregancy() {
        return regAdolEarlyPregancy;
    }

    public void setRegAdolEarlyPregancy(String regAdolEarlyPregancy) {
        this.regAdolEarlyPregancy = regAdolEarlyPregancy;
    }

    public String getRegAdolUndernutrition() {
        return regAdolUndernutrition;
    }

    public void setRegAdolUndernutrition(String regAdolUndernutrition) {
        this.regAdolUndernutrition = regAdolUndernutrition;
    }*/

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRegAdolHealthIssues() {
        return regAdolHealthIssues;
    }

    public void setRegAdolHealthIssues(String regAdolHealthIssues) {
        this.regAdolHealthIssues = regAdolHealthIssues;
    }

    public String getRegAdolLifeStyle() {
        return regAdolLifeStyle;
    }

    public void setRegAdolLifeStyle(String regAdolLifeStyle) {
        this.regAdolLifeStyle = regAdolLifeStyle;
    }

    public String getRegAdolMenstrualPbm() {
        return regAdolMenstrualPbm;
    }

    public void setRegAdolMenstrualPbm(String regAdolMenstrualPbm) {
        this.regAdolMenstrualPbm = regAdolMenstrualPbm;
    }

    public String getRegAdolPhysicalActivity() {
        return regAdolPhysicalActivity;
    }

    public void setRegAdolPhysicalActivity(String regAdolPhysicalActivity) {
        this.regAdolPhysicalActivity = regAdolPhysicalActivity;
    }

    public String getRegAdolOtherMenspbm() {
        return regAdolOtherMenspbm;
    }

    public void setRegAdolOtherMenspbm(String regAdolOtherMenspbm) {
        this.regAdolOtherMenspbm = regAdolOtherMenspbm;
    }

    public String getRegUserType() {
        return regUserType;
    }

    public void setRegUserType(String regUserType) {
        this.regUserType = regUserType;
    }

    public String getRegAdolImage() {
        return regAdolImage;
    }

    public void setRegAdolImage(String regAdolImage) {
        this.regAdolImage = regAdolImage;
    }
}
