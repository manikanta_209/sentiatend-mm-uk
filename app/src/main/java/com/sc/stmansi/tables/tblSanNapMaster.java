package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;

public class tblSanNapMaster {
    @DatabaseField
    private Integer SlNo;
    @DatabaseField
    private String sannapmGrpId;
    @DatabaseField
    private String sannapmId;
    @DatabaseField
    private String sannapName;
    @DatabaseField
    private String sannapXmlStringName;

    public String getSannapmGrpId() {
        return sannapmGrpId;
    }

    public void setSannapmGrpId(String sannapmGrpId) {
        this.sannapmGrpId = sannapmGrpId;
    }

    public Integer getSlNo() {
        return SlNo;
    }

    public void setSlNo(Integer slNo) {
        SlNo = slNo;
    }

    public String getSannapmId() {
        return sannapmId;
    }

    public void setSannapmId(String sannapmId) {
        this.sannapmId = sannapmId;
    }

    public String getSannapName() {
        return sannapName;
    }

    public void setSannapName(String sannapName) {
        this.sannapName = sannapName;
    }

    public String getSannapXmlStringName() {
        return sannapXmlStringName;
    }

    public void setSannapXmlStringName(String sannapXmlStringName) {
        this.sannapXmlStringName = sannapXmlStringName;
    }
}
