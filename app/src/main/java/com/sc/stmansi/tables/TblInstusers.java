//27Aug2019 Arpitha - code clean up
package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "tblinstusers")
public class TblInstusers {

    @DatabaseField
    private String InstituteId;
    @DatabaseField
    private String UserId;
    @DatabaseField
    private String EmailId;
    @DatabaseField
    private String UserName;
    @DatabaseField
    private int UserAge;
    @DatabaseField
    private String UserGender;
    @DatabaseField
    private String UserGovtIdType;
    @DatabaseField
    private String UserGovtId;
    @DatabaseField
    private String UserMobileNumber;
    @DatabaseField
    private String Password;
    @DatabaseField
    private String UserRole;
    @DatabaseField
    private String Address;
    @DatabaseField
    private String Pincode;
    @DatabaseField
    private String Country;
    @DatabaseField
    private String State;
    @DatabaseField
    private String District;
    @DatabaseField
    private String SubDistrict;
    @DatabaseField
    private String FacilityType;
    @DatabaseField
    private String FacilityName;
    @DatabaseField
    private String Inst_Database;
    @DatabaseField
    private String Bankname;
    @DatabaseField
    private String Branchname;
    @DatabaseField
    private String Ifsccode;
    @DatabaseField
    private String Accountno;
    @DatabaseField
    private int isValidated;
    @DatabaseField
    private String UserValidatedDatetime;
    @DatabaseField
    private int isTabLost;
    @DatabaseField
    private String TabLostDate;
    @DatabaseField
    private static int LastWomennumber;
    @DatabaseField
    private static int LastAdolNumber;//Ramesh 15-Apr-2021
    @DatabaseField
    private int LastActivityNumber;
    @DatabaseField
    private static int LastTransNumber;
    @DatabaseField
    private static int LastRequestNumber;
    @DatabaseField
    private int LastDangerNumber;
    @DatabaseField
    private int isDeactivated;
    @DatabaseField
    private String DeactivatedDate;
    @DatabaseField
    private String DeactivatedReason;
    @DatabaseField
    private String datecreated;
    @DatabaseField
    private String createdby;
    @DatabaseField
    private String dateupdated;
    @DatabaseField
    private String updatedby;
    @DatabaseField
    private String AshaId;
    @DatabaseField
    private String DevicePhnNumber;

    public TblInstusers() {
    }

    public String getInstituteId() {
        return InstituteId;
    }

    public void setInstituteId(String instituteId) {
        InstituteId = instituteId;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getEmailId() {
        return EmailId;
    }

    public void setEmailId(String emailId) {
        EmailId = emailId;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public int getUserAge() {
        return UserAge;
    }

    public void setUserAge(int userAge) {
        UserAge = userAge;
    }

    public String getUserGender() {
        return UserGender;
    }

    public void setUserGender(String userGender) {
        UserGender = userGender;
    }

    public String getUserGovtIdType() {
        return UserGovtIdType;
    }

    public void setUserGovtIdType(String userGovtIdType) {
        UserGovtIdType = userGovtIdType;
    }

    public String getUserGovtId() {
        return UserGovtId;
    }

    public void setUserGovtId(String userGovtId) {
        UserGovtId = userGovtId;
    }

    public String getUserMobileNumber() {
        return UserMobileNumber;
    }

    public void setUserMobileNumber(String userMobileNumber) {
        UserMobileNumber = userMobileNumber;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getUserRole() {
        return UserRole;
    }

    public void setUserRole(String userRole) {
        UserRole = userRole;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getPincode() {
        return Pincode;
    }

    public void setPincode(String pincode) {
        Pincode = pincode;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getDistrict() {
        return District;
    }

    public void setDistrict(String district) {
        District = district;
    }

    public String getSubDistrict() {
        return SubDistrict;
    }

    public void setSubDistrict(String subDistrict) {
        SubDistrict = subDistrict;
    }

    public String getFacilityType() {
        return FacilityType;
    }

    public void setFacilityType(String facilityType) {
        FacilityType = facilityType;
    }

    public String getFacilityName() {
        return FacilityName;
    }

    public void setFacilityName(String facilityName) {
        FacilityName = facilityName;
    }

    public String getInst_Database() {
        return Inst_Database;
    }

    public void setInst_Database(String inst_Database) {
        Inst_Database = inst_Database;
    }

    public String getBankname() {
        return Bankname;
    }

    public void setBankname(String bankname) {
        Bankname = bankname;
    }

    public String getBranchname() {
        return Branchname;
    }

    public void setBranchname(String branchname) {
        Branchname = branchname;
    }

    public String getIfsccode() {
        return Ifsccode;
    }

    public void setIfsccode(String ifsccode) {
        Ifsccode = ifsccode;
    }

    public String getAccountno() {
        return Accountno;
    }

    public void setAccountno(String accountno) {
        Accountno = accountno;
    }

    public int getIsValidated() {
        return isValidated;
    }

    public void setIsValidated(int isValidated) {
        this.isValidated = isValidated;
    }

    public String getUserValidatedDatetime() {
        return UserValidatedDatetime;
    }

    public void setUserValidatedDatetime(String userValidatedDatetime) {
        UserValidatedDatetime = userValidatedDatetime;
    }

    public int getIsTabLost() {
        return isTabLost;
    }

    public void setIsTabLost(int isTabLost) {
        this.isTabLost = isTabLost;
    }

    public String getTabLostDate() {
        return TabLostDate;
    }

    public void setTabLostDate(String tabLostDate) {
        TabLostDate = tabLostDate;
    }

    public static int getLastWomennumber() {
        return LastWomennumber;
    }

    public static void setLastWomennumber(int lastWomennumber) {
        LastWomennumber = lastWomennumber;
    }

    public int getLastActivityNumber() {
        return LastActivityNumber;
    }

    public void setLastActivityNumber(int lastActivityNumber) {
        LastActivityNumber = lastActivityNumber;
    }

    public static int getLastTransNumber() {
        return LastTransNumber;
    }

    public static void setLastTransNumber(int lastTransNumber) {
        LastTransNumber = lastTransNumber;
    }

    public static int getLastRequestNumber() {
        return LastRequestNumber;
    }

    public static void setLastRequestNumber(int lastRequestNumber) {
        LastRequestNumber = lastRequestNumber;
    }

    public int getLastDangerNumber() {
        return LastDangerNumber;
    }

    public void setLastDangerNumber(int lastDangerNumber) {
        LastDangerNumber = lastDangerNumber;
    }

    public int getIsDeactivated() {
        return isDeactivated;
    }

    public void setIsDeactivated(int isDeactivated) {
        this.isDeactivated = isDeactivated;
    }

    public String getDeactivatedDate() {
        return DeactivatedDate;
    }

    public void setDeactivatedDate(String deactivatedDate) {
        DeactivatedDate = deactivatedDate;
    }

    public String getDeactivatedReason() {
        return DeactivatedReason;
    }

    public void setDeactivatedReason(String deactivatedReason) {
        DeactivatedReason = deactivatedReason;
    }

    public String getDatecreated() {
        return datecreated;
    }

    public void setDatecreated(String datecreated) {
        this.datecreated = datecreated;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public String getDateupdated() {
        return dateupdated;
    }

    public void setDateupdated(String dateupdated) {
        this.dateupdated = dateupdated;
    }

    public String getUpdatedby() {
        return updatedby;
    }

    public void setUpdatedby(String updatedby) {
        this.updatedby = updatedby;
    }

    public String getAshaId() {
        return AshaId;
    }

    public void setAshaId(String ashaId) {
        AshaId = ashaId;
    }

    public String getDevicePhnNumber() {
        return DevicePhnNumber;
    }

    public void setDevicePhnNumber(String devicePhnNumber) {
        DevicePhnNumber = devicePhnNumber;
    }
@DatabaseField
static
int LastParentNumber;//16Nov2019 Arpitha

    public static int getLastParentNumber() {
        return LastParentNumber;
    }

    public void setLastParentNumber(int lastParentNumber) {
        LastParentNumber = lastParentNumber;
    }

    public static int getLastAdolNumber() {
        return LastAdolNumber;
    }

    public static void setLastAdolNumber(int lastAdolNumber) {
        LastAdolNumber = lastAdolNumber;
    }

}
