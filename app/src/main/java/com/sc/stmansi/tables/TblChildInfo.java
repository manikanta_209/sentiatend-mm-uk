package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

@DatabaseTable(tableName = "tblchildinfo")
public class TblChildInfo implements Serializable {


    @DatabaseField private String UserId;
    @DatabaseField private String WomanId;
    @DatabaseField private int chlNo;
    @DatabaseField private String chlID;
    @DatabaseField private String chlRCHID;
    @DatabaseField private String chlChildname;
    @DatabaseField private String chlDateOfBirth;
    @DatabaseField private String chlTimeOfBirth;
    @DatabaseField private String chlDeliveryType;
    @DatabaseField private String chlGender;
    @DatabaseField private String chlWeight;
    @DatabaseField private String chlComplications;
    @DatabaseField private String chlOtherComplications;
    @DatabaseField private int transId;
    @DatabaseField private String chlGoingToSchool;
    @DatabaseField private String chlPhysicalDisability;
    @DatabaseField private String chlMentalDisability;
    @DatabaseField private String chlOtherPhysicalDisability;
    @DatabaseField private String chlOtherMentalDisability;
    @DatabaseField private String chlComments;
    @DatabaseField private String chlParentId;
    //    @DatabaseField private int chlWoman;
  /*  @DatabaseField private String chlRelation;
    @DatabaseField private String chlGuardianName;
    @DatabaseField private String chlMotherName;
    @DatabaseField private int chlMotherAge;
    @DatabaseField private String chlMotherIdType;
    @DatabaseField private String chlMotherId;
    @DatabaseField private int chlMotherDeath;
    @DatabaseField private int chlMotherDeathReason;
    @DatabaseField private String chlMotherDeathOther;
    @DatabaseField private int chlMotherAgeWhenPassedAway;
    @DatabaseField private String chlFatherName;
    @DatabaseField private String chlFatherRemarried;
    @DatabaseField private int chlFatherDeath;*/
    @DatabaseField private String RecordCreatedDate;
    @DatabaseField private int chlDeliveryResult;
    @DatabaseField private String chlCryAfterBirth;
    @DatabaseField private String chlBreastFeed;
    @DatabaseField private String chlIUDType;
    @DatabaseField private String chlDeathDate;
    @DatabaseField private String chlRegDate;
    @DatabaseField private int chlReg;
    @DatabaseField private String chlUserType;
    @DatabaseField private int chlTribalHamlet;

    @DatabaseField private String chlDelTypeOther;
    @DatabaseField private String chlDelPlace;
    @DatabaseField private String chlDeliveredBy;
    @DatabaseField private String chlDeliveredByOther;
    @DatabaseField private String chlDeliveryConductedByName;


    @DatabaseField private String chlDeactDate;
    @DatabaseField private String chlDeactReason;
    @DatabaseField private String chlDeactOtherReason;
    @DatabaseField private String chlDeactComments;
    @DatabaseField private String RecordUpdatedDate;
    @DatabaseField private String chlIsAshaAcompany;
    @DatabaseField private String chlDeactRelocatedDate;
    @DatabaseField private String chlDeactMortalityDate;
    @DatabaseField private String childtoadolID;

    public String getChildtoadolID() {
        return childtoadolID;
    }

    public void setChildtoadolID(String childtoadolID) {
        this.childtoadolID = childtoadolID;
    }

    //MANI 10FEB2021 ADDED NEW COLUMN
    @DatabaseField private String chlSkintoskincontact;

    public String getChlSkintoskincontact() {
        return chlSkintoskincontact;
    }

    public void setChlSkintoskincontact(String chlSkintoskincontact) {
        this.chlSkintoskincontact = chlSkintoskincontact;
    }



    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getWomanId() {
        return WomanId;
    }

    public void setWomanId(String womanId) {
        WomanId = womanId;
    }

    public int getChildNo() {
        return chlNo;
    }

    public void setChildNo(int childNo) {
        chlNo = childNo;
    }

    public String getChildID() {
        return chlID;
    }

    public void setChildID(String childID) {
        chlID = childID;
    }

    public String getChildRCHID() {
        return chlRCHID;
    }

    public void setChildRCHID(String childRCHID) {
        chlRCHID = childRCHID;
    }

    public String getChlBabyname() {
        return chlChildname;
    }

    public void setChlBabyname(String chlBabyname) {
        this.chlChildname = chlBabyname;
    }

    public String getChlDateTimeOfBirth() {
        return chlDateOfBirth;
    }

    public void setChlDateTimeOfBirth(String chlDateTimeOfBirth) {
        this.chlDateOfBirth = chlDateTimeOfBirth;
    }

    public String getChlDeliveryType() {
        return chlDeliveryType;
    }

    public void setChlDeliveryType(String chlDeliveryType) {
        this.chlDeliveryType = chlDeliveryType;
    }

    public String getChlGender() {
        return chlGender;
    }

    public void setChlGender(String chlGender) {
        this.chlGender = chlGender;
    }

    public String getDelBabyWeight() {
        return chlWeight;
    }

    public void setDelBabyWeight(String delBabyWeight) {
        this.chlWeight = delBabyWeight;
    }

    public String getChlComplications() {
        return chlComplications;
    }

    public void setChlComplications(String chlComplications) {
        this.chlComplications = chlComplications;
    }

    public String getChlOtherComplications() {
        return chlOtherComplications;
    }

    public void setChlOtherComplications(String chlOtherComplications) {
        this.chlOtherComplications = chlOtherComplications;
    }

    public int getTransId() {
        return transId;
    }

    public void setTransId(int transId) {
        this.transId = transId;
    }

    public String getChlSchool() {
        return chlGoingToSchool;
    }

    public void setChlSchool(String chlSchool) {
        this.chlGoingToSchool = chlSchool;
    }

    public String getChlPhysicalDisability() {
        return chlPhysicalDisability;
    }

    public void setChlPhysicalDisability(String chlPhysicalDisability) {
        this.chlPhysicalDisability = chlPhysicalDisability;
    }

    public String getChlMentalDisability() {
        return chlMentalDisability;
    }

    public void setChlMentalDisability(String chlMentalDisability) {
        this.chlMentalDisability = chlMentalDisability;
    }

    public String getChlOtherPhysicalDisability() {
        return chlOtherPhysicalDisability;
    }

    public void setChlOtherPhysicalDisability(String chlOtherPhysicalDisability) {
        this.chlOtherPhysicalDisability = chlOtherPhysicalDisability;
    }

    public String getChlOtherMentalDisability() {
        return chlOtherMentalDisability;
    }

    public void setChlOtherMentalDisability(String chlOtherMentalDisability) {
        this.chlOtherMentalDisability = chlOtherMentalDisability;
    }

    public String getChlChildComments() {
        return chlComments;
    }

    public void setChlChildComments(String chlChildComments) {
        this.chlComments = chlChildComments;
    }

   /* public int getChlWoman() {
        return chlWoman;
    }

    public void setChlWoman(int chlWoman) {
        this.chlWoman = chlWoman;
    }*/

    /*public String getChlRelation() {
        return chlRelation;
    }

    public void setChlRelation(String chlRelation) {
        this.chlRelation = chlRelation;
    }

    public String getChlGuardianName() {
        return chlGuardianName;
    }

    public void setChlGuardianName(String chlGuardianName) {
        this.chlGuardianName = chlGuardianName;
    }

    public String getChlMotherName() {
        return chlMotherName;
    }

    public void setChlMotherName(String chlMotherName) {
        this.chlMotherName = chlMotherName;
    }

    public int getChlMotherAge() {
        return chlMotherAge;
    }

    public void setChlMotherAge(int chlMotherAge) {
        this.chlMotherAge = chlMotherAge;
    }

    public String getChlMotherIdType() {
        return chlMotherIdType;
    }

    public void setChlMotherIdType(String chlMotherIdType) {
        this.chlMotherIdType = chlMotherIdType;
    }

    public String getChlMotherId() {
        return chlMotherId;
    }

    public void setChlMotherId(String chlMotherId) {
        this.chlMotherId = chlMotherId;
    }

    public int getChlMotherDeath() {
        return chlMotherDeath;
    }

    public void setChlMotherDeath(int chlMotherDeath) {
        this.chlMotherDeath = chlMotherDeath;
    }

    public int getChlMotherDeathReason() {
        return chlMotherDeathReason;
    }

    public void setChlMotherDeathReason(int chlMotherDeathReason) {
        this.chlMotherDeathReason = chlMotherDeathReason;
    }

    public String getChlMotherDeathOther() {
        return chlMotherDeathOther;
    }

    public void setChlMotherDeathOther(String chlMotherDeathOther) {
        this.chlMotherDeathOther = chlMotherDeathOther;
    }

    public int getChlMotherAgeWhenPassedAway() {
        return chlMotherAgeWhenPassedAway;
    }

    public void setChlMotherAgeWhenPassedAway(int chlMotherAgeWhenPassedAway) {
        this.chlMotherAgeWhenPassedAway = chlMotherAgeWhenPassedAway;
    }

    public String getChlFatherName() {
        return chlFatherName;
    }

    public void setChlFatherName(String chlFatherName) {
        this.chlFatherName = chlFatherName;
    }

    public String getChlFatherRemarried() {
        return chlFatherRemarried;
    }

    public void setChlFatherRemarried(String chlFatherRemarried) {
        this.chlFatherRemarried = chlFatherRemarried;
    }

    public int getChlFatherDeath() {
        return chlFatherDeath;
    }

    public void setChlFatherDeath(int chlFatherDeath) {
        this.chlFatherDeath = chlFatherDeath;
    }
*/
    public String getRecordCreatedDate() {
        return RecordCreatedDate;
    }

    public void setRecordCreatedDate(String recordCreatedDate) {
        RecordCreatedDate = recordCreatedDate;
    }

    public int getChlDeliveryResult() {
        return chlDeliveryResult;
    }

    public void setChlDeliveryResult(int chlDeliveryResult) {
        this.chlDeliveryResult = chlDeliveryResult;
    }

    public String getChlCryAfterBirth() {
        return chlCryAfterBirth;
    }

    public void setChlCryAfterBirth(String chlCryAfterBirth) {
        this.chlCryAfterBirth = chlCryAfterBirth;
    }

    public String getChlBreastFeed() {
        return chlBreastFeed;
    }

    public void setChlBreastFeed(String chlBreastFeed) {
        this.chlBreastFeed = chlBreastFeed;
    }

    public String getChlIUDType() {
        return chlIUDType;
    }

    public void setChlIUDType(String chlIUDType) {
        this.chlIUDType = chlIUDType;
    }

    public String getChlInfantDeathDate() {
        return chlDeathDate;
    }

    public void setChlInfantDeathDate(String chlInfantDeathDate) {
        this.chlDeathDate = chlInfantDeathDate;
    }

    public String getChlRegDate() {
        return chlRegDate;
    }

    public void setChlRegDate(String chlRegDate) {
        this.chlRegDate = chlRegDate;
    }

    public int getChlReg() {
        return chlReg;
    }

    public void setChlReg(int chlReg) {
        this.chlReg = chlReg;
    }

    public String getChlParentId() {
        return chlParentId;
    }

    public void setChlParentId(String chlParentId) {
        this.chlParentId = chlParentId;
    }

    public String getChlTimeOfBirth() {
        return chlTimeOfBirth;
    }

    public void setChlTimeOfBirth(String chlTimeOfBirth) {
        this.chlTimeOfBirth = chlTimeOfBirth;
    }

    public String getChlUserType() {
        return chlUserType;
    }

    public void setChlUserType(String chlUserType) {
        this.chlUserType = chlUserType;
    }

    public int getChlTribalHamlet() {
        return chlTribalHamlet;
    }

    public void setChlTribalHamlet(int getChlTribalHamlet) {
        this.chlTribalHamlet = getChlTribalHamlet;
    }

    public String getChlDelTypeOther() {
        return chlDelTypeOther;
    }

    public void setChlDelTypeOther(String chlDelTypeOther) {
        this.chlDelTypeOther = chlDelTypeOther;
    }

    public String getChlDelPlace() {
        return chlDelPlace;
    }

    public void setChlDelPlace(String chlDelPlace) {
        this.chlDelPlace = chlDelPlace;
    }

    public String getChlDeliveredBy() {
        return chlDeliveredBy;
    }

    public void setChlDeliveredBy(String chlDeliveredBy) {
        this.chlDeliveredBy = chlDeliveredBy;
    }

    public String getChlDeliveredByOther() {
        return chlDeliveredByOther;
    }

    public void setChlDeliveredByOther(String chlDeliveredByOther) {
        this.chlDeliveredByOther = chlDeliveredByOther;
    }

    public String getDelDeliveryConductedByName() {
        return chlDeliveryConductedByName;
    }

    public void setDelDeliveryConductedByName(String delDeliveryConductedByName) {
        this.chlDeliveryConductedByName = delDeliveryConductedByName;
    }

//    @DatabaseField(readOnly = true)
    int count;
//    @DatabaseField(readOnly = true)
    String serviceType;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getChlDeactDate() {
        return chlDeactDate;
    }

    public void setChlDeactDate(String chlDeactDate) {
        this.chlDeactDate = chlDeactDate;
    }

    public String getChlDeactReason() {
        return chlDeactReason;
    }

    public void setChlDeactReason(String chlDeactReason) {
        this.chlDeactReason = chlDeactReason;
    }

    public String getChlDeactOtherReason() {
        return chlDeactOtherReason;
    }

    public void setChlDeactOtherReason(String chlDeactOtherReason) {
        this.chlDeactOtherReason = chlDeactOtherReason;
    }

    public String getChlDeactComments() {
        return chlDeactComments;
    }

    public void setChlDeactComments(String chlDeactComments) {
        this.chlDeactComments = chlDeactComments;
    }

    public String getRecordUpdatedDate() {
        return RecordUpdatedDate;
    }

    public void setRecordUpdatedDate(String recordUpdatedDate) {
        RecordUpdatedDate = recordUpdatedDate;
    }

    public String getChlIsAshaAcompany() {
        return chlIsAshaAcompany;
    }

    public void setChlIsAshaAcompany(String chlIsAshaAcompany) {
        this.chlIsAshaAcompany = chlIsAshaAcompany;
    }

    public String getChlDeactRelocatedDate() {
        return chlDeactRelocatedDate;
    }

    public void setChlDeactRelocatedDate(String chlDeactRelocatedDate) {
        this.chlDeactRelocatedDate = chlDeactRelocatedDate;
    }

    public String getChlDeactMortalityDate() {
        return chlDeactMortalityDate;
    }

    public void setChlDeactMortalityDate(String chlDeactMortalityDate) {
        this.chlDeactMortalityDate = chlDeactMortalityDate;
    }
}
