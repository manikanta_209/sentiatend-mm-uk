package com.sc.stmansi.tables;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "tblmessagelog")

public class MessageLogPojo {


	@DatabaseField(generatedId = true)
	int msgId;
	@DatabaseField String UserId;
	@DatabaseField String WomanId;
	@DatabaseField String msgPhoneNo;
	@DatabaseField String msgBody;
	@DatabaseField String msgSentDate;
	@DatabaseField int msgPriority;
	@DatabaseField int msgNoOfFailures;
	@DatabaseField int msgSent;
	@DatabaseField String msgStage;
	@DatabaseField String msgUserType;
	@DatabaseField int transId;
	@DatabaseField String RecordCreatedDate;
	@DatabaseField String RecordUpdatedDate;//21May2021 Arpitha
	@DatabaseField String msgRecvdByCC;//30May2021 Arpitha





	@Override
	public String toString() {
		String str = "Msg Obj: " + getMsgId() + ", "
				+ getUserId() + ", " + getWomanId() + ", " + getMsgPhoneNo() + ", " + getMsgBody()
				+ ", " + getMsgSentDate() + ", " + getMsgNoOfFailures();
		return str;
	}


	public int getMsgId() {
		return msgId;
	}

	public void setMsgId(int msgId) {
		this.msgId = msgId;
	}

	public String getUserId() {
		return UserId;
	}

	public void setUserId(String userId) {
		UserId = userId;
	}

	public String getWomanId() {
		return WomanId;
	}

	public void setWomanId(String womanId) {
		WomanId = womanId;
	}

	public String getMsgPhoneNo() {
		return msgPhoneNo;
	}

	public void setMsgPhoneNo(String msgPhoneNo) {
		this.msgPhoneNo = msgPhoneNo;
	}

	public String getMsgBody() {
		return msgBody;
	}

	public void setMsgBody(String msgBody) {
		this.msgBody = msgBody;
	}

	public String getMsgSentDate() {
		return msgSentDate;
	}

	public void setMsgSentDate(String msgSentDate) {
		this.msgSentDate = msgSentDate;
	}

	public int getMsgPriority() {
		return msgPriority;
	}

	public void setMsgPriority(int msgPriority) {
		this.msgPriority = msgPriority;
	}

	public int getMsgNoOfFailures() {
		return msgNoOfFailures;
	}

	public void setMsgNoOfFailures(int msgNoOfFailures) {
		this.msgNoOfFailures = msgNoOfFailures;
	}

	public int getMsgSent() {
		return msgSent;
	}

	public void setMsgSent(int msgSent) {
		this.msgSent = msgSent;
	}

	public String getMsgStage() {
		return msgStage;
	}

	public void setMsgStage(String msgStage) {
		this.msgStage = msgStage;
	}

	public String getMsgUserType() {
		return msgUserType;
	}

	public void setMsgUserType(String msgUserType) {
		this.msgUserType = msgUserType;
	}

	public int getTransId() {
		return transId;
	}

	public void setTransId(int transId) {
		this.transId = transId;
	}

	public String getRecordCreatedDate() {
		return RecordCreatedDate;
	}

	public void setRecordCreatedDate(String recordCreatedDate) {
		RecordCreatedDate = recordCreatedDate;
	}

	public String getRecordUpdatedDate() {
		return RecordUpdatedDate;
	}

	public void setRecordUpdatedDate(String recordUpdatedDate) {
		RecordUpdatedDate = recordUpdatedDate;
	}

	public String getMsgRecvdByCC() {
		return msgRecvdByCC;
	}

	public void setMsgRecvdByCC(String msgRecvdByCC) {
		this.msgRecvdByCC = msgRecvdByCC;
	}
}
