//27Aug2019 Arpitha - code clean up
package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

@DatabaseTable(tableName = "tblaudittrail")
public class AuditPojo implements Serializable {
    @DatabaseField
    private String WomanId;
    @DatabaseField
    private String UserId;
    @DatabaseField
    private String TblName;
    @DatabaseField
    private String ColumnName;
    @DatabaseField
    private String DateChanged;
    @DatabaseField
    private String Old_Value;
    @DatabaseField
    private String New_Value;
    @DatabaseField
    private int transId;
    @DatabaseField
    private String Recordcreateddate;  //15Aug2019 - Bindu

    public int getTransId() {
        return transId;
    }

    public void setTransId(int transId) {
        this.transId = transId;
    }

    public String getWomanId() {
        return WomanId;
    }

    public void setWomanId(String womanId) {
        WomanId = womanId;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public void setTblName(String tableName) {
        TblName = tableName;
    }

    public String getColumnName() {
        return ColumnName;
    }

    public void setColumnName(String columnName) {
        ColumnName = columnName;
    }

    public void setDateChanged(String dateChanged) {
        DateChanged = dateChanged;
    }

    public void setOld_Value(String old_Value) {
        Old_Value = old_Value;
    }

    public void setNew_Value(String new_Value) {
        New_Value = new_Value;
    }

    public String getRecordcreateddate() {
        return Recordcreateddate;
    }

    public void setRecordcreateddate(String recordcreateddate) {
        Recordcreateddate = recordcreateddate;
    }
}
