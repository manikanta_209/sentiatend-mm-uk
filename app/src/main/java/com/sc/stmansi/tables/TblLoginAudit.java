//27Aug2019 Arpitha - code clean up
package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

@DatabaseTable(tableName = "tblloginaudit")
public class TblLoginAudit implements Serializable {


    @DatabaseField
    String logintime;
    @DatabaseField
    String logouttime;
    @DatabaseField
    String usertype;
    @DatabaseField
    String createddate;
    @DatabaseField
    String updateddate;
    @DatabaseField
    String userId;
    @DatabaseField(id = true)
    Integer autoId;
//14Aug2019 - Bindu - Add transid
    @DatabaseField
    Integer transId;

    //Mani 12feb2021 added two new fields latitude and longitude

    @DatabaseField
    String latitude;

    @DatabaseField
    String longitude;

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }


    public String getLogintime() {
        return logintime;
    }

    public void setLogintime(String logintime) {
        this.logintime = logintime;
    }

    public String getLogouttime() {
        return logouttime;
    }

    public void setLogouttime(String logouttime) {
        this.logouttime = logouttime;
    }

    public String getUsertype() {
        return usertype;
    }

    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }
    
    public String getCreateddate() {
        return createddate;
    }
    
    public void setCreateddate(String createdate) {
        this.createddate = createdate;
    }

    public String getUpdateddate() {
        return updateddate;
    }

    public void setUpdateddate(String updateddate) {
        this.updateddate = updateddate;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getAutoId() {
        return autoId;
    }
    
    public void setAutoId(int autoId) {
       autoId = autoId;
    }

    public Integer getTransId() {
        return transId;
    }

    public void setTransId(Integer transId) {
        this.transId = transId;
    }
}
