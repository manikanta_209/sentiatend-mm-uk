package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;

public class tblCampDetails implements Serializable {
    @DatabaseField
    private String userId;
    @DatabaseField
    private String tblCampID;
    @DatabaseField
    private String tblCampName;
    @DatabaseField
    private String tblCampDateTime;
    @DatabaseField
    private String tblCampDesc;
    @DatabaseField
    private String tblCampPresent;
    @DatabaseField
    private String tblCampAbsent;
    @DatabaseField
    private int tblCampSchemaType;
    @DatabaseField
    private String tblCampFacilities;
    @DatabaseField
    private String tblCampCreatedBy;
    @DatabaseField
    private String tblCampOutcome;
    @DatabaseField
    private int tblCampisActive;
    @DatabaseField
    private String tblCampSummary;
    @DatabaseField
    private String tblCampEndTime;
    @DatabaseField
    private String tblCampCCAccompany;
    @DatabaseField
    private String tblCampProObjective;
    @DatabaseField
    private String tblCampProObjOthers;
    @DatabaseField
    private int transId;
    @DatabaseField
    private String RecordCreatedDate;

    public String getTblCampCCAccompany() {
        return tblCampCCAccompany;
    }

    public void setTblCampCCAccompany(String tblCampCCAccompany) {
        this.tblCampCCAccompany = tblCampCCAccompany;
    }

    public String getTblCampProObjective() {
        return tblCampProObjective;
    }

    public void setTblCampProObjective(String tblCampProObjective) {
        this.tblCampProObjective = tblCampProObjective;
    }

    public String getTblCampProObjOthers() {
        return tblCampProObjOthers;
    }

    public void setTblCampProObjOthers(String tblCampProObjOthers) {
        this.tblCampProObjOthers = tblCampProObjOthers;
    }

    public String getTblCampFacilities() {
        return tblCampFacilities;
    }

    public void setTblCampFacilities(String tblCampFacilities) {
        this.tblCampFacilities = tblCampFacilities;
    }

    public String getTblCampOutcome() {
        return tblCampOutcome;
    }

    public void setTblCampOutcome(String tblCampOutcome) {
        this.tblCampOutcome = tblCampOutcome;
    }

    public String getTblCampID() {
        return tblCampID;
    }

    public void setTblCampID(String tblCampID) {
        this.tblCampID = tblCampID;
    }

    public String getTblCampName() {
        return tblCampName;
    }

    public void setTblCampName(String tblCampName) {
        this.tblCampName = tblCampName;
    }

    public String getTblCampDateTime() {
        return tblCampDateTime;
    }

    public void setTblCampDateTime(String tblCampDateTime) {
        this.tblCampDateTime = tblCampDateTime;
    }

    public int getTblCampSchemaType() {
        return tblCampSchemaType;
    }

    public void setTblCampSchemaType(int tblCampSchemaType) {
        this.tblCampSchemaType = tblCampSchemaType;
    }

    public String getTblCampCreatedBy() {
        return tblCampCreatedBy;
    }

    public void setTblCampCreatedBy(String tblCampCreatedBy) {
        this.tblCampCreatedBy = tblCampCreatedBy;
    }

    public String getTblCampSummary() {
        return tblCampSummary;
    }

    public void setTblCampSummary(String tblCampSummary) {
        this.tblCampSummary = tblCampSummary;
    }

    public String getTblCampEndTime() {
        return tblCampEndTime;
    }

    public void setTblCampEndTime(String tblCampEndTime) {
        this.tblCampEndTime = tblCampEndTime;
    }

    public int getTransId() {
        return transId;
    }

    public void setTransId(int transId) {
        this.transId = transId;
    }

    public String getRecordCreatedDate() {
        return RecordCreatedDate;
    }

    public void setRecordCreatedDate(String recordCreatedDate) {
        RecordCreatedDate = recordCreatedDate;
    }

    public String getTblCampDesc() {
        return tblCampDesc;
    }

    public void setTblCampDesc(String tblCampDesc) {
        this.tblCampDesc = tblCampDesc;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTblCampPresent() {
        return tblCampPresent;
    }

    public void setTblCampPresent(String tblCampPresent) {
        this.tblCampPresent = tblCampPresent;
    }

    public String getTblCampAbsent() {
        return tblCampAbsent;
    }

    public void setTblCampAbsent(String tblCampAbsent) {
        this.tblCampAbsent = tblCampAbsent;
    }

    public int getTblCampisActive() {
        return tblCampisActive;
    }

    public void setTblCampisActive(int tblCampisActive) {
        this.tblCampisActive = tblCampisActive;
    }
}
