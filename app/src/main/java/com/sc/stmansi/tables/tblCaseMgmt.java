package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;

public class tblCaseMgmt implements Serializable {

    @DatabaseField(id = true)
    Integer autoId;
    @DatabaseField
    private String UserId;
    @DatabaseField
    private String MMUserId;
    @DatabaseField
    private String BeneficiaryType;
    @DatabaseField
    private String BeneficiaryID;
    @DatabaseField
    private String BeneficiaryParentID;
    @DatabaseField
    private String VisitType;
    @DatabaseField
    private int VisitNum;
    @DatabaseField
    private String VisitDate;
    @DatabaseField
    private int VisitNumbyMM;
    @DatabaseField
    private String VisitDatebyMM;
    @DatabaseField
    private String VisitBeneficiaryDangerSigns;
    @DatabaseField
    private String hcmBeneficiaryVisitFac;
    @DatabaseField
    private String hcmReasonForNoVisit;
    @DatabaseField
    private String hcmReasonOthers;
    @DatabaseField
    private String hcmActTakByUserType;
    @DatabaseField
    private String hcmActTakByUserName;
    @DatabaseField
    private String hcmActTakAtFacility;
    @DatabaseField
    private String hcmActTakFacilityName;
    @DatabaseField
    private String hcmActTakDate;
    @DatabaseField
    private String hcmActTakTime;
    @DatabaseField
    private String hcmActTakForCompl;
    @DatabaseField
    private String hcmActMedicationsPres;
    @DatabaseField
    private String hcmActTakStatus;
    @DatabaseField
    private String hcmActTakStatusDate;
    @DatabaseField
    private String hcmActTakStatusTime;
    @DatabaseField
    private String hcmActTakReferredToFacility;
    @DatabaseField
    private String hcmActTakReferredToFacilityName;
    @DatabaseField
    private String hcmActTakComments;
    @DatabaseField
    private String hcmAdviseGiven;
    @DatabaseField
    private String hcmBeneficiaryCondatVisit;
    @DatabaseField
    private String hcmActionToBeTakenClient;
    @DatabaseField
    private String hcmInputToNextLevel;
    @DatabaseField
    private String hcmIsANMInformedAbtCompl;
    @DatabaseField
    private String hcmANMAwareOfRef;
    @DatabaseField
    private String VisitReferredtoFacType;
    @DatabaseField
    private String VisitReferredtoFacName;
    @DatabaseField
    private String VisitReferralSlipNumber;
    @DatabaseField
    private String hcmCreatedByUserType;
    @DatabaseField
    private int transId;
    @DatabaseField
    private String RecordCreatedDate;
    @DatabaseField
    private String RecordUpdatedDate;
    @DatabaseField
    private String ServerCreatedDate;
    @DatabaseField
    private String ServerUpdatedDate;
    @DatabaseField
    private String StatusAtVisit;
    //06Jul2021 Bindu
    @DatabaseField
    private String ChlNo;
    @DatabaseField
    private String VisitMode;
    @DatabaseField
    private String RecordViewedDate;

    public String getRecordViewedDate() {
        return RecordViewedDate;
    }

    public void setRecordViewedDate(String recordViewedDate) {
        RecordViewedDate = recordViewedDate;
    }

    public String getStatusAtVisit() {
        return StatusAtVisit;
    }

    public void setStatusAtVisit(String statusAtVisit) {
        StatusAtVisit = statusAtVisit;
    }

    public int getAutoId() {
        return autoId;
    }

    public void setAutoId(int autoId) {
        this.autoId = autoId;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getBeneficiaryType() {
        return BeneficiaryType;
    }

    public void setBeneficiaryType(String beneficiaryType) {
        BeneficiaryType = beneficiaryType;
    }

    public String getBeneficiaryID() {
        return BeneficiaryID;
    }

    public void setBeneficiaryID(String beneficiaryID) {
        BeneficiaryID = beneficiaryID;
    }

    public String getVisitType() {
        return VisitType;
    }

    public void setVisitType(String visitType) {
        VisitType = visitType;
    }

    public int getVisitNum() {
        return VisitNum;
    }

    public void setVisitNum(int visitNum) {
        VisitNum = visitNum;
    }

    public String getVisitDate() {
        return VisitDate;
    }

    public void setVisitDate(String visitDate) {
        VisitDate = visitDate;
    }

       public String getHcmBeneficiaryVisitFac() {
        return hcmBeneficiaryVisitFac;
    }

    public void setHcmBeneficiaryVisitFac(String hcmBeneficiaryVisitFac) {
        this.hcmBeneficiaryVisitFac = hcmBeneficiaryVisitFac;
    }

    public String getHcmReasonForNoVisit() {
        return hcmReasonForNoVisit;
    }

    public void setHcmReasonForNoVisit(String hcmReasonForNoVisit) {
        this.hcmReasonForNoVisit = hcmReasonForNoVisit;
    }

    public String getHcmReasonOthers() {
        return hcmReasonOthers;
    }

    public void setHcmReasonOthers(String hcmReasonOthers) {
        this.hcmReasonOthers = hcmReasonOthers;
    }

    public String getHcmActTakByUserType() {
        return hcmActTakByUserType;
    }

    public void setHcmActTakByUserType(String hcmActTakByUserType) {
        this.hcmActTakByUserType = hcmActTakByUserType;
    }

    public String getHcmActTakByUserName() {
        return hcmActTakByUserName;
    }

    public void setHcmActTakByUserName(String hcmActTakByUserName) {
        this.hcmActTakByUserName = hcmActTakByUserName;
    }

    public String getHcmActTakAtFacility() {
        return hcmActTakAtFacility;
    }

    public void setHcmActTakAtFacility(String hcmActTakAtFacility) {
        this.hcmActTakAtFacility = hcmActTakAtFacility;
    }

    public String getHcmActTakFacilityName() {
        return hcmActTakFacilityName;
    }

    public void setHcmActTakFacilityName(String hcmActTakFacilityName) {
        this.hcmActTakFacilityName = hcmActTakFacilityName;
    }

    public String getHcmActTakDate() {
        return hcmActTakDate;
    }

    public void setHcmActTakDate(String hcmActTakDate) {
        this.hcmActTakDate = hcmActTakDate;
    }

    public String getHcmActTakTime() {
        return hcmActTakTime;
    }

    public void setHcmActTakTime(String hcmActTakTime) {
        this.hcmActTakTime = hcmActTakTime;
    }

    public String getHcmActTakForCompl() {
        return hcmActTakForCompl;
    }

    public void setHcmActTakForCompl(String hcmActTakForCompl) {
        this.hcmActTakForCompl = hcmActTakForCompl;
    }

    public String getHcmActMedicationsPres() {
        return hcmActMedicationsPres;
    }

    public void setHcmActMedicationsPres(String hcmActMedicationsPres) {
        this.hcmActMedicationsPres = hcmActMedicationsPres;
    }

    public String getHcmActTakStatus() {
        return hcmActTakStatus;
    }

    public void setHcmActTakStatus(String hcmActTakStatus) {
        this.hcmActTakStatus = hcmActTakStatus;
    }

    public String getHcmActTakStatusDate() {
        return hcmActTakStatusDate;
    }

    public void setHcmActTakStatusDate(String hcmActTakStatusDate) {
        this.hcmActTakStatusDate = hcmActTakStatusDate;
    }

    public String getHcmActTakStatusTime() {
        return hcmActTakStatusTime;
    }

    public void setHcmActTakStatusTime(String hcmActTakStatusTime) {
        this.hcmActTakStatusTime = hcmActTakStatusTime;
    }

    public String getHcmActTakReferredToFacility() {
        return hcmActTakReferredToFacility;
    }

    public void setHcmActTakReferredToFacility(String hcmActTakReferredToFacility) {
        this.hcmActTakReferredToFacility = hcmActTakReferredToFacility;
    }

    public String getHcmActTakReferredToFacilityName() {
        return hcmActTakReferredToFacilityName;
    }

    public void setHcmActTakReferredToFacilityName(String hcmActTakReferredToFacilityName) {
        this.hcmActTakReferredToFacilityName = hcmActTakReferredToFacilityName;
    }

    public String getHcmActTakComments() {
        return hcmActTakComments;
    }

    public void setHcmActTakComments(String hcmActTakComments) {
        this.hcmActTakComments = hcmActTakComments;
    }

    public String getHcmAdviseGiven() {
        return hcmAdviseGiven;
    }

    public void setHcmAdviseGiven(String hcmAdviseGiven) {
        this.hcmAdviseGiven = hcmAdviseGiven;
    }

    public String getHcmBeneficiaryCondatVisit() {
        return hcmBeneficiaryCondatVisit;
    }

    public void setHcmBeneficiaryCondatVisit(String hcmBeneficiaryCondatVisit) {
        this.hcmBeneficiaryCondatVisit = hcmBeneficiaryCondatVisit;
    }

    public String getHcmActionToBeTakenClient() {
        return hcmActionToBeTakenClient;
    }

    public void setHcmActionToBeTakenClient(String hcmActionToBeTakenClient) {
        this.hcmActionToBeTakenClient = hcmActionToBeTakenClient;
    }

    public String getHcmInputToNextLevel() {
        return hcmInputToNextLevel;
    }

    public void setHcmInputToNextLevel(String hcmInputToNextLevel) {
        this.hcmInputToNextLevel = hcmInputToNextLevel;
    }

    public String getHcmIsANMInformedAbtCompl() {
        return hcmIsANMInformedAbtCompl;
    }

    public void setHcmIsANMInformedAbtCompl(String hcmIsANMInformedAbtCompl) {
        this.hcmIsANMInformedAbtCompl = hcmIsANMInformedAbtCompl;
    }

    public String getHcmANMAwareOfRef() {
        return hcmANMAwareOfRef;
    }

    public void setHcmANMAwareOfRef(String hcmANMAwareOfRef) {
        this.hcmANMAwareOfRef = hcmANMAwareOfRef;
    }

    public String getHcmCreatedByUserType() {
        return hcmCreatedByUserType;
    }

    public void setHcmCreatedByUserType(String hcmCreatedByUserType) {
        this.hcmCreatedByUserType = hcmCreatedByUserType;
    }

    public int getTransId() {
        return transId;
    }

    public void setTransId(int transId) {
        this.transId = transId;
    }

    public String getRecordCreatedDate() {
        return RecordCreatedDate;
    }

    public void setRecordCreatedDate(String recordCreatedDate) {
        RecordCreatedDate = recordCreatedDate;
    }

    public String getRecordUpdatedDate() {
        return RecordUpdatedDate;
    }

    public void setRecordUpdatedDate(String recordUpdatedDate) {
        RecordUpdatedDate = recordUpdatedDate;
    }

    public String getBeneficiaryParentID() {
        return BeneficiaryParentID;
    }

    public void setBeneficiaryParentID(String beneficiaryParentID) {
        BeneficiaryParentID = beneficiaryParentID;
    }

    public String getVisitBeneficiaryDangerSigns() {
        return VisitBeneficiaryDangerSigns;
    }

    public void setVisitBeneficiaryDangerSigns(String visitBeneficiaryDangerSigns) {
        VisitBeneficiaryDangerSigns = visitBeneficiaryDangerSigns;
    }

    public String getVisitReferredtoFacType() {
        return VisitReferredtoFacType;
    }

    public void setVisitReferredtoFacType(String visitReferredtoFacType) {
        VisitReferredtoFacType = visitReferredtoFacType;
    }

    public String getVisitReferredtoFacName() {
        return VisitReferredtoFacName;
    }

    public void setVisitReferredtoFacName(String visitReferredtoFacName) {
        VisitReferredtoFacName = visitReferredtoFacName;
    }

    public String getVisitReferralSlipNumber() {
        return VisitReferralSlipNumber;
    }

    public void setVisitReferralSlipNumber(String visitReferralSlipNumber) {
        VisitReferralSlipNumber = visitReferralSlipNumber;
    }

    public int getVisitNumbyMM() {
        return VisitNumbyMM;
    }

    public void setVisitNumbyMM(int visitNumbyMM) {
        VisitNumbyMM = visitNumbyMM;
    }

    public String getVisitDatebyMM() {
        return VisitDatebyMM;
    }

    public void setVisitDatebyMM(String visitDatebyMM) {
        VisitDatebyMM = visitDatebyMM;
    }

    public String getServerCreatedDate() {
        return ServerCreatedDate;
    }

    public void setServerCreatedDate(String serverCreatedDate) {
        ServerCreatedDate = serverCreatedDate;
    }

    public String getServerUpdatedDate() {
        return ServerUpdatedDate;
    }

    public void setServerUpdatedDate(String serverUpdatedDate) {
        ServerUpdatedDate = serverUpdatedDate;
    }

    public String getMMUserId() {
        return MMUserId;
    }

    public void setMMUserId(String MMUserId) {
        this.MMUserId = MMUserId;
    }

    public String getChlNo() {
        return ChlNo;
    }

    public void setChlNo(String chlNo) {
        ChlNo = chlNo;
    }

    public String getVisitMode() {
        return VisitMode;
    }

    public void setVisitMode(String visitMode) {
        VisitMode = visitMode;
    }
}

