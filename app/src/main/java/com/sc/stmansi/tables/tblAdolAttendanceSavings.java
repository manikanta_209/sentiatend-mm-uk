package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;

public class tblAdolAttendanceSavings implements Serializable {

    @DatabaseField
    private String userID;

    @DatabaseField
    private String AdolID;

    @DatabaseField
    private String CampID;

    @DatabaseField
    private String tblAttSavAttendance;

    @DatabaseField
    private int transId;

    @DatabaseField
    private String DateCreated;

    @DatabaseField
    private String DateUpdated;

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getAdolID() {
        return AdolID;
    }

    public void setAdolID(String adolID) {
        AdolID = adolID;
    }

    public String getCampID() {
        return CampID;
    }

    public void setCampID(String campID) {
        CampID = campID;
    }

    public String getTblAttSavAttendance() {
        return tblAttSavAttendance;
    }

    public void setTblAttSavAttendance(String tblAttSavAttendance) {
        this.tblAttSavAttendance = tblAttSavAttendance;
    }

    public int getTransId() {
        return transId;
    }

    public void setTransId(int transId) {
        this.transId = transId;
    }

    public String getDateCreated() {
        return DateCreated;
    }

    public void setDateCreated(String dateCreated) {
        DateCreated = dateCreated;
    }

    public String getDateUpdated() {
        return DateUpdated;
    }

    public void setDateUpdated(String dateUpdated) {
        DateUpdated = dateUpdated;
    }
}
