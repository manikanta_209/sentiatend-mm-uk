package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "tbldeliveryinfo")
public class TblDeliveryInfo {

    @DatabaseField private String UserId ;
    @DatabaseField
    public static String WomanId ;
//    @DatabaseField private int ActivityId ;
    @DatabaseField private String delDateTimeOfDelivery ;
//    @DatabaseField private String delPlace ;
//    @DatabaseField private String delType ;
//    @DatabaseField private String delTypeOther ;
    @DatabaseField private int delNoOfChildren ;
    @DatabaseField private String delMotherComplications ;
    @DatabaseField private String delOtherComplications ;
    @DatabaseField private String delMothersDeath ;
    @DatabaseField private String delMCauseofDeath ;
    @DatabaseField private String delMOtherDeathCause ;
    @DatabaseField private String delComments ;
    @DatabaseField private String delUserType ;
    @DatabaseField private int transId ;
    @DatabaseField private String RecordCreatedDate ;
    @DatabaseField private String RecordUpdatedDate ;
    @DatabaseField private String delAshaAccompany ;




    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getWomanId() {
        return WomanId;
    }

    public void setWomanId(String womanId) {
        WomanId = womanId;
    }



    public String getDelDateTimeOfDelivery() {
        return delDateTimeOfDelivery;
    }

    public void setDelDateTimeOfDelivery(String delDateTimeOfDelivery) {
        this.delDateTimeOfDelivery = delDateTimeOfDelivery;
    }

   /* public String getDelPlace() {
        return delPlace;
    }

    public void setDelPlace(String delPlace) {
        this.delPlace = delPlace;
    }

    public String getDelType() {
        return delType;
    }

    public void setDelType(String delType) {
        this.delType = delType;
    }

    public String getDelTypeOther() {
        return delTypeOther;
    }

    public void setDelTypeOther(String delTypeOther) {
        this.delTypeOther = delTypeOther;
    }*/

    public int getDelNoOfChildren() {
        return delNoOfChildren;
    }

    public void setDelNoOfChildren(int delNoOfChildren) {
        this.delNoOfChildren = delNoOfChildren;
    }

    public String getDelMotherComplications() {
        return delMotherComplications;
    }

    public void setDelMotherComplications(String delMotherComplications) {
        this.delMotherComplications = delMotherComplications;
    }

    public String getDelOtherComplications() {
        return delOtherComplications;
    }

    public void setDelOtherComplications(String delOtherComplications) {
        this.delOtherComplications = delOtherComplications;
    }

    public String getDelMothersDeath() {
        return delMothersDeath;
    }

    public void setDelMothersDeath(String delMothersDeath) {
        this.delMothersDeath = delMothersDeath;
    }

    public String getDelCauseofDeath() {
        return delMCauseofDeath;
    }

    public void setDelCauseofDeath(String delCauseofDeath) {
        this.delMCauseofDeath = delCauseofDeath;
    }

    public String getDelOtherDeathCause() {
        return delMOtherDeathCause;
    }

    public void setDelOtherDeathCause(String delOtherDeathCause) {
        this.delMOtherDeathCause = delOtherDeathCause;
    }

    public String getDelComments() {
        return delComments;
    }

    public void setDelComments(String delComments) {
        this.delComments = delComments;
    }

    public String getDelUserType() {
        return delUserType;
    }

    public void setDelUserType(String delUserType) {
        this.delUserType = delUserType;
    }

    public int getTransId() {
        return transId;
    }

    public void setTransId(int transId) {
        this.transId = transId;
    }

    public String getRecordCreatedDate() {
        return RecordCreatedDate;
    }

    public void setRecordCreatedDate(String recordCreatedDate) {
        RecordCreatedDate = recordCreatedDate;
    }

    public String getRecordUpdatedDate() {
        return RecordUpdatedDate;
    }

    public void setRecordUpdatedDate(String recordUpdatedDate) {
        RecordUpdatedDate = recordUpdatedDate;
    }

    public String getDelAshaAccompany() {
        return delAshaAccompany;
    }

    public void setDelAshaAccompany(String delAshaAccompany) {
        this.delAshaAccompany = delAshaAccompany;
    }
}
