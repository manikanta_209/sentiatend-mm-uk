package com.sc.stmansi.tables;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

@DatabaseTable(tableName = "tblchildgrowth")
public class tblchildgrowth implements Serializable {
    public Integer getAutoId() {
        return AutoId;
    }

    public void setAutoId(Integer autoId) {
        AutoId = autoId;
    }

    @DatabaseField(id = true) Integer AutoId;
    @DatabaseField private String chlId;
    @DatabaseField private String chlGMAge;

    public void setChlGMVisitId(int chlGMVisitId) {
        this.chlGMVisitId = chlGMVisitId;
    }

    public int getChlGMVisitId() {
        return chlGMVisitId;
    }

    @DatabaseField private int chlGMVisitId;
    @DatabaseField private String chlGMVisitdate;

    public int getChlGMAgeInMonths() {
        return chlGMAgeInMonths;
    }

    public void setChlGMAgeInMonths(int chlGMAgeInMonths) {
        this.chlGMAgeInMonths = chlGMAgeInMonths;
    }

    @DatabaseField private int chlGMAgeInMonths;
    @DatabaseField private String chlGMHeight;
    @DatabaseField private String chlGMWeight;
    @DatabaseField private String chlGMHeadCircum;
    @DatabaseField private String chlGMBMI;
    @DatabaseField private String chlGMMuac;
    @DatabaseField private String chlGMTriceps;
    @DatabaseField private String chlGMVisitNotes;



    @DatabaseField private Integer transId;
    @DatabaseField private String chlGMRecommendations;
    @DatabaseField private String chlGMObservations;
    @DatabaseField private String chlGMSuggestions;
    @DatabaseField private String chlGMRecordCreatedDate;
    @DatabaseField private String chlGMRecordUpdatedDate;
//    12May2021 Bindu - set chlgmcomplication
    @DatabaseField private String  chlGMComplication;
    @DatabaseField private String  chlGMBreastfeeding;

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    //mani 18july2021
    @DatabaseField private String UserId;

    public String getChlGMObservations() {
        return chlGMObservations;
    }

    public void setChlGMObservations(String chlGMObservations) {
        this.chlGMObservations = chlGMObservations;
    }



    public String getChlId() {
        return chlId;
    }

    public void setChlId(String chlId) {
        this.chlId = chlId;
    }


    public String getChlGMAge() {
        return chlGMAge;
    }

    public void setChlGMAge(String chlGMAge) {
        this.chlGMAge = chlGMAge;
    }

    public String getChlGMVisitdate() {
        return chlGMVisitdate;
    }

    public void setChlGMVisitdate(String chlGMVisitdate) {
        this.chlGMVisitdate = chlGMVisitdate;
    }

    public String getChlGMHeight() {
        return chlGMHeight;
    }

    public void setChlGMHeight(String chlGMHeight) {
        this.chlGMHeight = chlGMHeight;
    }

    public String getChlGMWeight() {
        return chlGMWeight;
    }

    public void setChlGMWeight(String chlGMWeight) {
        this.chlGMWeight = chlGMWeight;
    }

    public String getChlGMHeadCircum() {
        return chlGMHeadCircum;
    }

    public void setChlGMHeadCircum(String chlGMHeadCircum) {
        this.chlGMHeadCircum = chlGMHeadCircum;
    }

    public String getChlGMBMI() {
        return chlGMBMI;
    }

    public void setChlGMBMI(String chlGMBMI) {
        this.chlGMBMI = chlGMBMI;
    }

    public String getChlGMMuac() {
        return chlGMMuac;
    }

    public void setChlGMMuac(String chlGMMuac) {
        this.chlGMMuac = chlGMMuac;
    }

    public String getChlGMTriceps() {
        return chlGMTriceps;
    }

    public void setChlGMTriceps(String chlGMTriceps) {
        this.chlGMTriceps = chlGMTriceps;
    }

    public String getChlGMVisitNotes() {
        return chlGMVisitNotes;
    }

    public void setChlGMVisitNotes(String chlGMVisitNotes) {
        this.chlGMVisitNotes = chlGMVisitNotes;
    }


    public String getChlGMRecommendations() {
        return chlGMRecommendations;
    }

    public void setChlGMRecommendations(String chlGMRecommendations) {
        this.chlGMRecommendations = chlGMRecommendations;
    }

    public String getChlGMSuggestions() {
        return chlGMSuggestions;
    }

    public void setChlGMSuggestions(String chlGMSuggestions) {
        this.chlGMSuggestions = chlGMSuggestions;
    }

    public String getChlGMRecordCreatedDate() {
        return chlGMRecordCreatedDate;
    }

    public void setChlGMRecordCreatedDate(String chlGMRecordCreatedDate) {
        this.chlGMRecordCreatedDate = chlGMRecordCreatedDate;
    }

    public String getChlGMRecordUpdatedDate() {
        return chlGMRecordUpdatedDate;
    }

    public void setChlGMRecordUpdatedDate(String chlGMRecordUpdatedDate) {
        this.chlGMRecordUpdatedDate = chlGMRecordUpdatedDate;
    }

    public String getChlGMComplication() {
        return chlGMComplication;
    }

    public void setChlGMComplication(String chlGMComplication) {
        this.chlGMComplication = chlGMComplication;
    }

    public Integer getTransId() {
        return transId;
    }

    public void setTransId(Integer transId) {
        this.transId = transId;
    }

    public String getChlGMBreastfeeding() {
        return chlGMBreastfeeding;
    }

    public void setChlGMBreastfeeding(String chlGMBreastfeeding) {
        this.chlGMBreastfeeding = chlGMBreastfeeding;
    }
}
