//27Aug2019 Arpitha - code clean up
package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "tblserviceslist")
public class tblserviceslist {
    @DatabaseField
    private String UserId;
    @DatabaseField
    private String WomenId;
    @DatabaseField
    private int ActivityId;
    @DatabaseField
    private int ServiceId;
    @DatabaseField
    private int ServiceNumber;
    @DatabaseField
    private String ServiceType;
    @DatabaseField
    private int ServicePlanned;
    @DatabaseField
    private String ServiceDueDateMin;
    @DatabaseField
    private String ServiceDueDateMax;
    @DatabaseField
    private String ServicePlannedFinYear;
    @DatabaseField
    private String ServiceActualDateofAction;
    @DatabaseField
    private String ServiceActualFinYear;
    @DatabaseField
    private int ServiceTargetMonth;
    @DatabaseField
    private int ServiceAchievedMonth;
    @DatabaseField
    private String ServiceDetail1;
    @DatabaseField
    private String ServiceDetail2;
    @DatabaseField
    private String ServiceDetail3;
    @DatabaseField
    private String ServiceComments;
    @DatabaseField
    private String ServiceProvidedFacType;
    @DatabaseField
    private String ServiceProvidedFacName;
    @DatabaseField
    private String ServiceUserType;
    @DatabaseField
    private String ServiceProvidedGPSLoc;
    @DatabaseField
    private int transId;
    @DatabaseField
    private String RecordCreatedDate;
    //26Jul2019 - Bindu
    @DatabaseField
    private String RecordUpdatedDate;
    int pendingCount;

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getWomenId() {
        return WomenId;
    }

    public void setWomenId(String womenId) {
        WomenId = womenId;
    }

    public int getActivityId() {
        return ActivityId;
    }

    public void setActivityId(int activityId) {
        ActivityId = activityId;
    }

    public int getServiceId() {
        return ServiceId;
    }

    public void setServiceId(int serviceId) {
        ServiceId = serviceId;
    }

    public int getServiceNumber() {
        return ServiceNumber;
    }

    public void setServiceNumber(int serviceNumber) {
        ServiceNumber = serviceNumber;
    }

    public String getServiceType() {
        return ServiceType;
    }

    public void setServiceType(String serviceType) {
        ServiceType = serviceType;
    }

    public int getServicePlanned() {
        return ServicePlanned;
    }

    public void setServicePlanned(int servicePlanned) {
        ServicePlanned = servicePlanned;
    }

    public String getServiceDueDateMin() {
        return ServiceDueDateMin;
    }

    public void setServiceDueDateMin(String serviceDueDateMin) {
        ServiceDueDateMin = serviceDueDateMin;
    }

    public String getServiceDueDateMax() {
        return ServiceDueDateMax;
    }

    public void setServiceDueDateMax(String serviceDueDateMax) {
        ServiceDueDateMax = serviceDueDateMax;
    }

    public String getServicePlannedFinYear() {
        return ServicePlannedFinYear;
    }

    public void setServicePlannedFinYear(String servicePlannedFinYear) {
        ServicePlannedFinYear = servicePlannedFinYear;
    }

    public String getServiceActualDateofAction() {
        return ServiceActualDateofAction;
    }

    public void setServiceActualDateofAction(String serviceActualDateofAction) {
        ServiceActualDateofAction = serviceActualDateofAction;
    }

    public String getServiceActualFinYear() {
        return ServiceActualFinYear;
    }

    public void setServiceActualFinYear(String serviceActualFinYear) {
        ServiceActualFinYear = serviceActualFinYear;
    }

    public int getServiceTargetMonth() {
        return ServiceTargetMonth;
    }

    public void setServiceTargetMonth(int serviceTargetMonth) {
        ServiceTargetMonth = serviceTargetMonth;
    }

    public int getServiceAchievedMonth() {
        return ServiceAchievedMonth;
    }

    public void setServiceAchievedMonth(int serviceAchievedMonth) {
        ServiceAchievedMonth = serviceAchievedMonth;
    }

    public String getServiceDetail1() {
        return ServiceDetail1;
    }

    public void setServiceDetail1(String serviceDetail1) {
        ServiceDetail1 = serviceDetail1;
    }

    public String getServiceDetail2() {
        return ServiceDetail2;
    }

    public void setServiceDetail2(String serviceDetail2) {
        ServiceDetail2 = serviceDetail2;
    }

    public String getServiceDetail3() {
        return ServiceDetail3;
    }

    public void setServiceDetail3(String serviceDetail3) {
        ServiceDetail3 = serviceDetail3;
    }

    public String getServiceComments() {
        return ServiceComments;
    }

    public void setServiceComments(String serviceComments) {
        ServiceComments = serviceComments;
    }

    public String getServiceProvidedFacType() {
        return ServiceProvidedFacType;
    }

    public void setServiceProvidedFacType(String serviceProvidedFacType) {
        ServiceProvidedFacType = serviceProvidedFacType;
    }

    public String getServiceProvidedFacName() {
        return ServiceProvidedFacName;
    }

    public void setServiceProvidedFacName(String serviceProvidedFacName) {
        ServiceProvidedFacName = serviceProvidedFacName;
    }

    public String getServiceUserType() {
        return ServiceUserType;
    }

    public void setServiceUserType(String serviceUserType) {
        ServiceUserType = serviceUserType;
    }

    public String getServiceProvidedGPSLoc() {
        return ServiceProvidedGPSLoc;
    }

    public void setServiceProvidedGPSLoc(String serviceProvidedGPSLoc) {
        ServiceProvidedGPSLoc = serviceProvidedGPSLoc;
    }

    public int getTransId() {
        return transId;
    }

    public void setTransId(int transId) {
        this.transId = transId;
    }

    public String getRecordCreatedDate() {
        return RecordCreatedDate;
    }

    public void setRecordCreatedDate(String recordCreatedDate) {
        RecordCreatedDate = recordCreatedDate;
    }

    public String getRecordUpdatedDate() {
        return RecordUpdatedDate;
    }

    public void setRecordUpdatedDate(String recordUpdatedDate) {
        RecordUpdatedDate = recordUpdatedDate;
    }

    public int getPendingCount() {
        return pendingCount;
    }

    public void setPendingCount(int pendingCount) {
        this.pendingCount = pendingCount;
    }
}