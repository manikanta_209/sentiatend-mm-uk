package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "tblchlparentdetails")
public class TblChlParentDetails {


    @DatabaseField private String chlParentId;
    @DatabaseField
    private String UserId;
    @DatabaseField private String chlRelation;
    @DatabaseField private String chlGuardianName;
    @DatabaseField private String chlMotherName;
    @DatabaseField private int chlMotherAge;
    @DatabaseField private String chlMotherIdType;
    @DatabaseField private String chlMotherId;
    @DatabaseField private int chlMotherDeath;
    @DatabaseField private int chlMotherDeathReason;
    @DatabaseField private String chlMotherDeathOther;
    @DatabaseField private int chlMotherAgeWhenPassedAway;
    @DatabaseField private String chlFatherName;
    @DatabaseField private String chlFatherRemarried;
    @DatabaseField private int chlFatherDeath;
    @DatabaseField private String chlUserType;
    @DatabaseField private int transId;
    @DatabaseField private String RecordCreatedDate;

    @DatabaseField private String chlMotherDeathDuringDel;
    @DatabaseField private String chlMotherDeathDate;
    @DatabaseField String RecordUpdatedDate;//21May2021 Arpitha


    public String getChlParentId() {
        return chlParentId;
    }

    public void setChlParentId(String chlParentId) {
        this.chlParentId = chlParentId;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getChlRelation() {
        return chlRelation;
    }

    public void setChlRelation(String chlRelation) {
        this.chlRelation = chlRelation;
    }

    public String getChlGuardianName() {
        return chlGuardianName;
    }

    public void setChlGuardianName(String chlGuardianName) {
        this.chlGuardianName = chlGuardianName;
    }

    public String getChlMotherName() {
        return chlMotherName;
    }

    public void setChlMotherName(String chlMotherName) {
        this.chlMotherName = chlMotherName;
    }

    public int getChlMotherAge() {
        return chlMotherAge;
    }

    public void setChlMotherAge(int chlMotherAge) {
        this.chlMotherAge = chlMotherAge;
    }

    public String getChlMotherIdType() {
        return chlMotherIdType;
    }

    public void setChlMotherIdType(String chlMotherIdType) {
        this.chlMotherIdType = chlMotherIdType;
    }

    public String getChlMotherId() {
        return chlMotherId;
    }

    public void setChlMotherId(String chlMotherId) {
        this.chlMotherId = chlMotherId;
    }

    public int getChlMotherDeath() {
        return chlMotherDeath;
    }

    public void setChlMotherDeath(int chlMotherDeath) {
        this.chlMotherDeath = chlMotherDeath;
    }

    public int getChlMotherDeathReason() {
        return chlMotherDeathReason;
    }

    public void setChlMotherDeathReason(int chlMotherDeathReason) {
        this.chlMotherDeathReason = chlMotherDeathReason;
    }

    public String getChlMotherDeathOther() {
        return chlMotherDeathOther;
    }

    public void setChlMotherDeathOther(String chlMotherDeathOther) {
        this.chlMotherDeathOther = chlMotherDeathOther;
    }

    public int getChlMotherAgeWhenPassedAway() {
        return chlMotherAgeWhenPassedAway;
    }

    public void setChlMotherAgeWhenPassedAway(int chlMotherAgeWhenPassedAway) {
        this.chlMotherAgeWhenPassedAway = chlMotherAgeWhenPassedAway;
    }

    public String getChlFatherName() {
        return chlFatherName;
    }

    public void setChlFatherName(String chlFatherName) {
        this.chlFatherName = chlFatherName;
    }

    public String getChlFatherRemarried() {
        return chlFatherRemarried;
    }

    public void setChlFatherRemarried(String chlFatherRemarried) {
        this.chlFatherRemarried = chlFatherRemarried;
    }

    public int getChlFatherDeath() {
        return chlFatherDeath;
    }

    public void setChlFatherDeath(int chlFatherDeath) {
        this.chlFatherDeath = chlFatherDeath;
    }

    public String getChlUserType() {
        return chlUserType;
    }

    public void setChlUserType(String chlUserType) {
        this.chlUserType = chlUserType;
    }

    public int getTransId() {
        return transId;
    }

    public void setTransId(int transId) {
        this.transId = transId;
    }

    public String getRecordCreatedDate() {
        return RecordCreatedDate;
    }

    public void setRecordCreatedDate(String recordCreatedDate) {
        RecordCreatedDate = recordCreatedDate;
    }

    public String getChlMotherDeathDuringDel() {
        return chlMotherDeathDuringDel;
    }

    public void setChlMotherDeathDuringDel(String chlMotherDeathDuringDel) {
        this.chlMotherDeathDuringDel = chlMotherDeathDuringDel;
    }

    public String getChlMotherDeathDate() {
        return chlMotherDeathDate;
    }

    public void setChlMotherDeathDate(String chlMotherDeathDate) {
        this.chlMotherDeathDate = chlMotherDeathDate;
    }

    public String getRecordUpdatedDate() {
        return RecordUpdatedDate;
    }

    public void setRecordUpdatedDate(String recordUpdatedDate) {
        RecordUpdatedDate = recordUpdatedDate;
    }
}
