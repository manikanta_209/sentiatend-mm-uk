package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;

public class tblSyncDetails {

    @DatabaseField (id = true)
    private String autoID;
    @DatabaseField
    private String UserId;
    @DatabaseField
    private String lastSyncDate;
    @DatabaseField
    int transId;
    @DatabaseField
    String RecordCreatedDate;
    @DatabaseField
    String RecordUpdatedDate;

    public String getAutoID() {
        return autoID;
    }

    public void setAutoID(String autoID) {
        this.autoID = autoID;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getLastSyncDate() {
        return lastSyncDate;
    }

    public void setLastSyncDate(String lastSyncDate) {
        this.lastSyncDate = lastSyncDate;
    }

    public int getTransId() {
        return transId;
    }

    public void setTransId(int transId) {
        this.transId = transId;
    }

    public String getRecordCreatedDate() {
        return RecordCreatedDate;
    }

    public void setRecordCreatedDate(String recordCreatedDate) {
        RecordCreatedDate = recordCreatedDate;
    }

    public String getRecordUpdatedDate() {
        return RecordUpdatedDate;
    }

    public void setRecordUpdatedDate(String recordUpdatedDate) {
        RecordUpdatedDate = recordUpdatedDate;
    }
}
