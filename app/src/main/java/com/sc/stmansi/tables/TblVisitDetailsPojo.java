package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;

public class TblVisitDetailsPojo {
    @DatabaseField(id = true)
    Integer AutoId;
    @DatabaseField private String  UserId;
    @DatabaseField private String  WomanId;

    @DatabaseField private int  VisitNum;
    @DatabaseField private String  symGroupId;
    @DatabaseField private String  visDSymptoms;
    @DatabaseField private String  visDOtherSymptoms;
    @DatabaseField private String  visDSignsInvestigations;
    @DatabaseField private String  visDSignsInvestInterpretation;
    @DatabaseField private String  visDDiagnosis;
    @DatabaseField private String  visDActionTobeTaken;
    @DatabaseField private String  visDReferredToFacilityType;



    public Integer getAutoId() {
        return AutoId;
    }

    public void setAutoId(Integer autoId) {
        AutoId = autoId;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getWomanId() {
        return WomanId;
    }

    public void setWomanId(String womanId) {
        WomanId = womanId;
    }

    public int getVisitNum() {
        return VisitNum;
    }

    public void setVisitNum(int visitNum) {
        VisitNum = visitNum;
    }



    public String getVisDSymptoms() {
        return visDSymptoms;
    }

    public void setVisDSymptoms(String visDSymptoms) {
        this.visDSymptoms = visDSymptoms;
    }

    public String getVisDOtherSymptoms() {
        return visDOtherSymptoms;
    }

    public void setVisDOtherSymptoms(String visDOtherSymptoms) {
        this.visDOtherSymptoms = visDOtherSymptoms;
    }

    public String getVisDSignsInvestigations() {
        return visDSignsInvestigations;
    }

    public void setVisDSignsInvestigations(String visDSignsInvestigations) {
        this.visDSignsInvestigations = visDSignsInvestigations;
    }

    public String getVisDSignsInvestInterpretation() {
        return visDSignsInvestInterpretation;
    }

    public void setVisDSignsInvestInterpretation(String visDSignsInvestInterpretation) {
        this.visDSignsInvestInterpretation = visDSignsInvestInterpretation;
    }

    public String getVisDDiagnosis() {
        return visDDiagnosis;
    }

    public void setVisDDiagnosis(String visDDiagnosis) {
        this.visDDiagnosis = visDDiagnosis;
    }

    public String getVisDActionTobeTaken() {
        return visDActionTobeTaken;
    }

    public void setVisDActionTobeTaken(String visDActionTobeTaken) {
        this.visDActionTobeTaken = visDActionTobeTaken;
    }

    public String getVisDReferredToFacilityType() {
        return visDReferredToFacilityType;
    }

    public void setVisDReferredToFacilityType(String visDReferredToFacilityType) {
        this.visDReferredToFacilityType = visDReferredToFacilityType;
    }


    public String getSymGroupId() {
        return symGroupId;
    }

    public void setSymGroupId(String symGroupId) {
        this.symGroupId = symGroupId;
    }
}
