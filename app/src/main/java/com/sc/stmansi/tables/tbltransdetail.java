//27Aug2019 Arpitha - code clean up
package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;

public class tbltransdetail {


    @DatabaseField
    private String Action;
    @DatabaseField
    private String TableName;
    @DatabaseField
    private String SQLStatement;
    @DatabaseField
    private String UserId;
    @DatabaseField
    private String transId;

    public String getAction() {
        return Action;
    }

    public void setAction(String action) {
        Action = action;
    }

    public String getTableName() {
        return TableName;
    }

    public void setTableName(String tableName) {
        TableName = tableName;
    }

    public String getSQLStatement() {
        return SQLStatement;
    }

    public void setSQLStatement(String SQLStatement) {
        this.SQLStatement = SQLStatement;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }
}
