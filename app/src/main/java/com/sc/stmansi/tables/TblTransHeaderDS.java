//27Aug2019 Arpitha - code clean up
package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;

public class TblTransHeaderDS {

    @DatabaseField
    private int transId;
    @DatabaseField
    private String TransDate;
    @DatabaseField
    private String TransStatus;
    @DatabaseField
    private String ReqId;
    @DatabaseField
    private String Action;
    @DatabaseField
    private String TblName;
    @DatabaseField
    private String SqlStmt;
    @DatabaseField
    private String UserId;

    public int getTransId() {
        return transId;
    }

    public void setTransId(int transId) {
        this.transId = transId;
    }

    public String getTransDate() {
        return TransDate;
    }

    public void setTransDate(String transDate) {
        TransDate = transDate;
    }

    public String getTransStatus() {
        return TransStatus;
    }

    public void setTransStatus(String transStatus) {
        TransStatus = transStatus;
    }

    public String getReqId() {
        return ReqId;
    }

    public void setReqId(String reqId) {
        ReqId = reqId;
    }

    public String getAction() {
        return Action;
    }

    public void setAction(String action) {
        Action = action;
    }

    public String getTblName() {
        return TblName;
    }

    public void setTblName(String tblName) {
        TblName = tblName;
    }

    public String getSqlStmt() {
        return SqlStmt;
    }

    public void setSqlStmt(String sqlStmt) {
        SqlStmt = sqlStmt;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }
}
