package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;

public class tblAdolVisitHeader implements Serializable {

    @DatabaseField(id = true)
    Integer autoId;
    @DatabaseField
    String UserId;
    @DatabaseField
    String AdolId;
    @DatabaseField
    String VisitId;
    @DatabaseField
    String adolvisHVisitDate;
    @DatabaseField
    String adolvisHGTS;
    @DatabaseField
    String adolvisHEducation;
    @DatabaseField
    String adolvisHIsPeriodstarted;
    @DatabaseField
    String adolvisHAgeMenstural;
    @DatabaseField
    String adolvisHUsedforPeriods;
    @DatabaseField
    String adolvisHUsedforPeriodsOthers;
    @DatabaseField
    String adolvisHMenstrualProblem;
    @DatabaseField
    String adolvisHMenstrualProblemOthers;
    @DatabaseField
    int adolvisHMaritalStatus;
    @DatabaseField
    String adolvisHPartnerName;
    @DatabaseField
    String adolvisHPartnerOccup;
    @DatabaseField
    String adolvisHAgeatMarriage;
    @DatabaseField
    String adolvisHIsPregnantatReg;
    @DatabaseField
    String adolvisHHb;
    @DatabaseField
    String adolvisHHbDate;
    @DatabaseField
    String adolvisHHbCategory;
    @DatabaseField
    String adolvisHHeightType;
    @DatabaseField
    String adolvisHHeight;
    @DatabaseField
    String adolvisHWeight;
    @DatabaseField
    String adolvisHBMI;
    @DatabaseField
    String adolvisHIsIFATaken;
    @DatabaseField
    String adolvisHIFADaily;
    @DatabaseField
    String adolvisHIFATabletsCount;
    @DatabaseField
    String adolvisHIFANotTakenReason;
    @DatabaseField
    int adolvisHIFAGivenBy;
    @DatabaseField
    String adolvisHDewormingtab;
    @DatabaseField
    String adolvisHHealthIssues;
    @DatabaseField
    String adolvisHHealthIssuesOthers;
    @DatabaseField
    String adolvisHTreatedAt;
    @DatabaseField
    String adolvisHGeneralTreatment;
    @DatabaseField
    String adolvisHGeneralTreatmentOthers;
    @DatabaseField
    String adolvisHContraceptionAware;
    @DatabaseField
    String adolvisHContraceptionNeed;
    @DatabaseField
    String adolvisHContraceptionMethod;
    @DatabaseField
    String adolvisHContraceptionMethodOthers;
    @DatabaseField
    String adolvisHComplications;
    @DatabaseField
    Integer transId;
    @DatabaseField
    String RecordCreatedDate;
    @DatabaseField
    String RecordUpdatedDate;

    public String getAdolvisHAshaAvailable() {
        return adolvisHAshaAvailable;
    }

    public void setAdolvisHAshaAvailable(String adolvisHAshaAvailable) {
        this.adolvisHAshaAvailable = adolvisHAshaAvailable;
    }

    //mani 18Aug2021
    @DatabaseField
    String adolvisHAshaAvailable;

    public Integer getAutoId() {
        return autoId;
    }

    public void setAutoId(Integer autoId) {
        this.autoId = autoId;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getAdolId() {
        return AdolId;
    }

    public void setAdolId(String adolId) {
        AdolId = adolId;
    }

    public String getVisitId() {
        return VisitId;
    }

    public void setVisitId(String visitId) {
        VisitId = visitId;
    }

    public String getAdolvisHVisitDate() {
        return adolvisHVisitDate;
    }

    public void setAdolvisHVisitDate(String adolvisHVisitDate) {
        this.adolvisHVisitDate = adolvisHVisitDate;
    }

    public String getAdolvisHGTS() {
        return adolvisHGTS;
    }

    public void setAdolvisHGTS(String adolvisHGTS) {
        this.adolvisHGTS = adolvisHGTS;
    }

    public String getAdolvisHEducation() {
        return adolvisHEducation;
    }

    public void setAdolvisHEducation(String adolvisHEducation) {
        this.adolvisHEducation = adolvisHEducation;
    }

    public String getAdolvisHIsPeriodstarted() {
        return adolvisHIsPeriodstarted;
    }

    public void setAdolvisHIsPeriodstarted(String adolvisHIsPeriodstarted) {
        this.adolvisHIsPeriodstarted = adolvisHIsPeriodstarted;
    }

    public String getAdolvisHAgeMenstural() {
        return adolvisHAgeMenstural;
    }

    public void setAdolvisHAgeMenstural(String adolvisHAgeMenstural) {
        this.adolvisHAgeMenstural = adolvisHAgeMenstural;
    }

    public String getAdolvisHUsedforPeriods() {
        return adolvisHUsedforPeriods;
    }

    public void setAdolvisHUsedforPeriods(String adolvisHUsedforPeriods) {
        this.adolvisHUsedforPeriods = adolvisHUsedforPeriods;
    }

    public String getAdolvisHUsedforPeriodsOthers() {
        return adolvisHUsedforPeriodsOthers;
    }

    public void setAdolvisHUsedforPeriodsOthers(String adolvisHUsedforPeriodsOthers) {
        this.adolvisHUsedforPeriodsOthers = adolvisHUsedforPeriodsOthers;
    }

    public String getAdolvisHMenstrualProblemOthers() {
        return adolvisHMenstrualProblemOthers;
    }

    public void setAdolvisHMenstrualProblemOthers(String adolvisHMenstrualProblemOthers) {
        this.adolvisHMenstrualProblemOthers = adolvisHMenstrualProblemOthers;
    }

    public int getAdolvisHMaritalStatus() {
        return adolvisHMaritalStatus;
    }

    public void setAdolvisHMaritalStatus(int adolvisHMaritalStatus) {
        this.adolvisHMaritalStatus = adolvisHMaritalStatus;
    }

    public String getAdolvisHPartnerName() {
        return adolvisHPartnerName;
    }

    public void setAdolvisHPartnerName(String adolvisHPartnerName) {
        this.adolvisHPartnerName = adolvisHPartnerName;
    }

    public String getAdolvisHPartnerOccup() {
        return adolvisHPartnerOccup;
    }

    public void setAdolvisHPartnerOccup(String adolvisHPartnerOccup) {
        this.adolvisHPartnerOccup = adolvisHPartnerOccup;
    }

    public String getAdolvisHAgeatMarriage() {
        return adolvisHAgeatMarriage;
    }

    public void setAdolvisHAgeatMarriage(String adolvisHAgeatMarriage) {
        this.adolvisHAgeatMarriage = adolvisHAgeatMarriage;
    }

    public String getAdolvisHIsPregnantatReg() {
        return adolvisHIsPregnantatReg;
    }

    public void setAdolvisHIsPregnantatReg(String adolvisHIsPregnantatReg) {
        this.adolvisHIsPregnantatReg = adolvisHIsPregnantatReg;
    }

    public String getAdolvisHHb() {
        return adolvisHHb;
    }

    public void setAdolvisHHb(String adolvisHHb) {
        this.adolvisHHb = adolvisHHb;
    }

    public String getAdolvisHHbDate() {
        return adolvisHHbDate;
    }

    public void setAdolvisHHbDate(String adolvisHHbDate) {
        this.adolvisHHbDate = adolvisHHbDate;
    }

    public String getAdolvisHHbCategory() {
        return adolvisHHbCategory;
    }

    public void setAdolvisHHbCategory(String adolvisHHbCategory) {
        this.adolvisHHbCategory = adolvisHHbCategory;
    }

    public String getAdolvisHHeightType() {
        return adolvisHHeightType;
    }

    public void setAdolvisHHeightType(String adolvisHHeightType) {
        this.adolvisHHeightType = adolvisHHeightType;
    }

    public String getAdolvisHHeight() {
        return adolvisHHeight;
    }

    public void setAdolvisHHeight(String adolvisHHeight) {
        this.adolvisHHeight = adolvisHHeight;
    }

    public String getAdolvisHWeight() {
        return adolvisHWeight;
    }

    public void setAdolvisHWeight(String adolvisHWeight) {
        this.adolvisHWeight = adolvisHWeight;
    }

    public String getAdolvisHBMI() {
        return adolvisHBMI;
    }

    public void setAdolvisHBMI(String adolvisHBMI) {
        this.adolvisHBMI = adolvisHBMI;
    }

    public String getAdolvisHIsIFATaken() {
        return adolvisHIsIFATaken;
    }

    public void setAdolvisHIsIFATaken(String adolvisHIsIFATaken) {
        this.adolvisHIsIFATaken = adolvisHIsIFATaken;
    }

    public String getAdolvisHIFADaily() {
        return adolvisHIFADaily;
    }

    public void setAdolvisHIFADaily(String adolvisHIFADaily) {
        this.adolvisHIFADaily = adolvisHIFADaily;
    }

    public String getAdolvisHIFATabletsCount() {
        return adolvisHIFATabletsCount;
    }

    public void setAdolvisHIFATabletsCount(String adolvisHIFATabletsCount) {
        this.adolvisHIFATabletsCount = adolvisHIFATabletsCount;
    }

    public String getAdolvisHIFANotTakenReason() {
        return adolvisHIFANotTakenReason;
    }

    public void setAdolvisHIFANotTakenReason(String adolvisHIFANotTakenReason) {
        this.adolvisHIFANotTakenReason = adolvisHIFANotTakenReason;
    }

    public int getAdolvisHIFAGivenBy() {
        return adolvisHIFAGivenBy;
    }

    public void setAdolvisHIFAGivenBy(int adolvisHIFAGivenBy) {
        this.adolvisHIFAGivenBy = adolvisHIFAGivenBy;
    }

    public String getAdolvisHDewormingtab() {
        return adolvisHDewormingtab;
    }

    public void setAdolvisHDewormingtab(String adolvisHDewormingtab) {
        this.adolvisHDewormingtab = adolvisHDewormingtab;
    }

    public String getAdolvisHHealthIssues() {
        return adolvisHHealthIssues;
    }

    public void setAdolvisHHealthIssues(String adolvisHHealthIssues) {
        this.adolvisHHealthIssues = adolvisHHealthIssues;
    }

    public String getAdolvisHHealthIssuesOthers() {
        return adolvisHHealthIssuesOthers;
    }

    public void setAdolvisHHealthIssuesOthers(String adolvisHHealthIssuesOthers) {
        this.adolvisHHealthIssuesOthers = adolvisHHealthIssuesOthers;
    }

    public String getAdolvisHTreatedAt() {
        return adolvisHTreatedAt;
    }

    public void setAdolvisHTreatedAt(String adolvisHTreatedAt) {
        this.adolvisHTreatedAt = adolvisHTreatedAt;
    }

    public String getAdolvisHGeneralTreatment() {
        return adolvisHGeneralTreatment;
    }

    public void setAdolvisHGeneralTreatment(String adolvisHGeneralTreatment) {
        this.adolvisHGeneralTreatment = adolvisHGeneralTreatment;
    }

    public String getAdolvisHGeneralTreatmentOthers() {
        return adolvisHGeneralTreatmentOthers;
    }

    public void setAdolvisHGeneralTreatmentOthers(String adolvisHGeneralTreatmentOthers) {
        this.adolvisHGeneralTreatmentOthers = adolvisHGeneralTreatmentOthers;
    }

    public String getAdolvisHContraceptionAware() {
        return adolvisHContraceptionAware;
    }

    public void setAdolvisHContraceptionAware(String adolvisHContraceptionAware) {
        this.adolvisHContraceptionAware = adolvisHContraceptionAware;
    }

    public String getAdolvisHContraceptionNeed() {
        return adolvisHContraceptionNeed;
    }

    public void setAdolvisHContraceptionNeed(String adolvisHContraceptionNeed) {
        this.adolvisHContraceptionNeed = adolvisHContraceptionNeed;
    }

    public String getAdolvisHContraceptionMethod() {
        return adolvisHContraceptionMethod;
    }

    public void setAdolvisHContraceptionMethod(String adolvisHContraceptionMethod) {
        this.adolvisHContraceptionMethod = adolvisHContraceptionMethod;
    }

    public String getAdolvisHContraceptionMethodOthers() {
        return adolvisHContraceptionMethodOthers;
    }

    public void setAdolvisHContraceptionMethodOthers(String adolvisHContraceptionMethodOthers) {
        this.adolvisHContraceptionMethodOthers = adolvisHContraceptionMethodOthers;
    }

    public String getAdolvisHComplications() {
        return adolvisHComplications;
    }

    public void setAdolvisHComplications(String adolvisHComplications) {
        this.adolvisHComplications = adolvisHComplications;
    }

    public Integer getTransId() {
        return transId;
    }

    public void setTransId(Integer transId) {
        this.transId = transId;
    }

    public String getRecordCreatedDate() {
        return RecordCreatedDate;
    }

    public void setRecordCreatedDate(String recordCreatedDate) {
        RecordCreatedDate = recordCreatedDate;
    }

    public String getRecordUpdatedDate() {
        return RecordUpdatedDate;
    }

    public void setRecordUpdatedDate(String recordUpdatedDate) {
        RecordUpdatedDate = recordUpdatedDate;
    }

    public String getAdolvisHMenstrualProblem() {
        return adolvisHMenstrualProblem;
    }

    public void setAdolvisHMenstrualProblem(String adolvisHMenstrualProblem) {
        this.adolvisHMenstrualProblem = adolvisHMenstrualProblem;
    }
}
