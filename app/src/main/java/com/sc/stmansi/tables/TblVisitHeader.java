package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

@DatabaseTable(tableName ="tblVisitHeader")
public class TblVisitHeader implements Serializable {

    @DatabaseField(id = true)
    Integer AutoId;
    @DatabaseField private String  UserId;
    @DatabaseField private String  WomanId;
    @DatabaseField private int  VisitNum;
    @DatabaseField private String  visHVisitType;
    @DatabaseField private String  visHVisitDate;
    @DatabaseField private String  visHStatusAtVisit;
    @DatabaseField private String  visHVisitIsAdviseGiven;
    @DatabaseField private String  visHVisitParacetamolGiven;
    @DatabaseField private String  visHVisitIFAGiven;
    @DatabaseField private String  visHVisitAlbendazoleGiven;
    @DatabaseField private String  visHBP;
    @DatabaseField private String  visHTemperature;
    @DatabaseField private String  visHUrineprotein;
    @DatabaseField private String  visHHb;
    @DatabaseField private String  visHVisitIsReferred;
    @DatabaseField private String  visHVisitReferredToFacilityType;
    @DatabaseField private String  visHVisitReferredToFacilityName;
    @DatabaseField private String  visHVisitComments;
    @DatabaseField private String  visHCompl;
    @DatabaseField private String  visHResult;
    @DatabaseField private String  visHUserType;
    @DatabaseField private int  transid;
    @DatabaseField private String  Recordcreateddate;
    @DatabaseField private String  Recordupdateddate;

    @DatabaseField private String  visHVisitParacetamolQty;
    @DatabaseField private String  visHVisitParacetamolNotGivenReason;
    @DatabaseField private String  visHVisitIFAQty;
    @DatabaseField private String  visHVisitIFANotGivenReason;
    @DatabaseField private String  visHVisitAlbendazoleQty;
    @DatabaseField private String  visHVisitAlbendazoleNotGivenReason;
    @DatabaseField private String  visHBPDate;
    @DatabaseField private String  visHTemperatureDate;
    @DatabaseField private String  visHUrineproteinDate;
    @DatabaseField private String  visHHbDate;
    @DatabaseField private String  visHAdvise;

    @DatabaseField private String  visHPulseDate;
    @DatabaseField private String  visHPulse;

    @DatabaseField private String  visHVisitFSFAGiven;
    @DatabaseField private String  visHVisitFSFAQty;
    @DatabaseField private String  visHVisitFSFANotGivenReason;

    @DatabaseField private String  visHWeight;
    @DatabaseField private String  visHWeightDate;

    //20Nov2019 - Bindu - add childsymtoms
    @DatabaseField private String  visHChildSymptoms;

    //08Apr2021 Bindu add ref slip num
    @DatabaseField private String  visHReferralSlipNumber;

    public String getVisHAshaAvailable() {
        return visHAshaAvailable;
    }

    public void setVisHAshaAvailable(String visHAshaAvailable) {
        this.visHAshaAvailable = visHAshaAvailable;
    }

    //Mani 18Aug2021
    @DatabaseField private String visHAshaAvailable;

    public Integer getAutoId() {
        return AutoId;
    }

    public void setAutoId(Integer autoId) {
        AutoId = autoId;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getWomanId() {
        return WomanId;
    }

    public void setWomanId(String womanId) {
        WomanId = womanId;
    }

    public int getVisitNum() {
        return VisitNum;
    }

    public void setVisitNum(int visitNum) {
        VisitNum = visitNum;
    }

    public String getVisHVisitType() {
        return visHVisitType;
    }

    public void setVisHVisitType(String visHVisitType) {
        this.visHVisitType = visHVisitType;
    }

    public String getVisHVisitDate() {
        return visHVisitDate;
    }

    public void setVisHVisitDate(String visHVisitDate) {
        this.visHVisitDate = visHVisitDate;
    }

    public String getVisHVisitIsAdviseGiven() {
        return visHVisitIsAdviseGiven;
    }

    public void setVisHVisitIsAdviseGiven(String visHVisitIsAdviseGiven) {
        this.visHVisitIsAdviseGiven = visHVisitIsAdviseGiven;
    }

    public String getVisHVisitParacetamolGiven() {
        return visHVisitParacetamolGiven;
    }

    public void setVisHVisitParacetamolGiven(String visHVisitParacetamolGiven) {
        this.visHVisitParacetamolGiven = visHVisitParacetamolGiven;
    }

    public String getVisHVisitIFAGiven() {
        return visHVisitIFAGiven;
    }

    public void setVisHVisitIFAGiven(String visHVisitIFAGiven) {
        this.visHVisitIFAGiven = visHVisitIFAGiven;
    }

    public String getVisHVisitAlbendazoleGiven() {
        return visHVisitAlbendazoleGiven;
    }

    public void setVisHVisitAlbendazoleGiven(String visHVisitAlbendazoleGiven) {
        this.visHVisitAlbendazoleGiven = visHVisitAlbendazoleGiven;
    }

    public String getVisHBP() {
        return visHBP;
    }

    public void setVisHBP(String visHBP) {
        this.visHBP = visHBP;
    }

    public String getVisHTemperature() {
        return visHTemperature;
    }

    public void setVisHTemperature(String visHTemperature) {
        this.visHTemperature = visHTemperature;
    }

    public String getVisHUrineprotein() {
        return visHUrineprotein;
    }

    public void setVisHUrineprotein(String visHUrineprotein) {
        this.visHUrineprotein = visHUrineprotein;
    }

    public String getVisHHb() {
        return visHHb;
    }

    public void setVisHHb(String visHHb) {
        this.visHHb = visHHb;
    }

    public String getVisHVisitIsReferred() {
        return visHVisitIsReferred;
    }

    public void setVisHVisitIsReferred(String visHVisitIsReferred) {
        this.visHVisitIsReferred = visHVisitIsReferred;
    }

    public String getVisHVisitReferredToFacilityType() {
        return visHVisitReferredToFacilityType;
    }

    public void setVisHVisitReferredToFacilityType(String visHVisitReferredToFacilityType) {
        this.visHVisitReferredToFacilityType = visHVisitReferredToFacilityType;
    }

    public String getVisHVisitReferredToFacilityName() {
        return visHVisitReferredToFacilityName;
    }

    public void setVisHVisitReferredToFacilityName(String visHVisitReferredToFacilityName) {
        this.visHVisitReferredToFacilityName = visHVisitReferredToFacilityName;
    }

    public String getVisHVisitComments() {
        return visHVisitComments;
    }

    public void setVisHVisitComments(String visHVisitComments) {
        this.visHVisitComments = visHVisitComments;
    }

    public String getVisHUserType() {
        return visHUserType;
    }

    public void setVisHUserType(String visHUserType) {
        this.visHUserType = visHUserType;
    }

    public int getTransid() {
        return transid;
    }

    public void setTransid(int transid) {
        this.transid = transid;
    }

    public String getRecordcreateddate() {
        return Recordcreateddate;
    }

    public void setRecordcreateddate(String recordcreateddate) {
        Recordcreateddate = recordcreateddate;
    }

    public String getRecordupdateddate() {
        return Recordupdateddate;
    }

    public void setRecordupdateddate(String recordupdateddate) {
        Recordupdateddate = recordupdateddate;
    }

    public String getVisHStatusAtVisit() {
        return visHStatusAtVisit;
    }

    public void setVisHStatusAtVisit(String visHStatusAtVisit) {
        this.visHStatusAtVisit = visHStatusAtVisit;
    }

    public String getVisHCompl() {
        return visHCompl;
    }

    public void setVisHCompl(String visHCompl) {
        this.visHCompl = visHCompl;
    }

    public String getVisHResult() {
        return visHResult;
    }

    public void setVisHResult(String visHResult) {
        this.visHResult = visHResult;
    }

    public String getVisHVisitParacetamolQty() {
        return visHVisitParacetamolQty;
    }

    public void setVisHVisitParacetamolQty(String visHVisitParacetamolQty) {
        this.visHVisitParacetamolQty = visHVisitParacetamolQty;
    }

    public String getVisHVisitParacetamolNotGivenReason() {
        return visHVisitParacetamolNotGivenReason;
    }

    public void setVisHVisitParacetamolNotGivenReason(String visHVisitParacetamolNotGivenReason) {
        this.visHVisitParacetamolNotGivenReason = visHVisitParacetamolNotGivenReason;
    }

    public String getVisHVisitIFAQty() {
        return visHVisitIFAQty;
    }

    public void setVisHVisitIFAQty(String visHVisitIFAQty) {
        this.visHVisitIFAQty = visHVisitIFAQty;
    }

    public String getVisHVisitIFANotGivenReason() {
        return visHVisitIFANotGivenReason;
    }

    public void setVisHVisitIFANotGivenReason(String visHVisitIFANotGivenReason) {
        this.visHVisitIFANotGivenReason = visHVisitIFANotGivenReason;
    }

    public String getVisHVisitAlbendazoleQty() {
        return visHVisitAlbendazoleQty;
    }

    public void setVisHVisitAlbendazoleQty(String visHVisitAlbendazoleQty) {
        this.visHVisitAlbendazoleQty = visHVisitAlbendazoleQty;
    }

    public String getVisHVisitAlbendazoleNotGivenReason() {
        return visHVisitAlbendazoleNotGivenReason;
    }

    public void setVisHVisitAlbendazoleNotGivenReason(String visHVisitAlbendazoleNotGivenReason) {
        this.visHVisitAlbendazoleNotGivenReason = visHVisitAlbendazoleNotGivenReason;
    }

    public String getVisHBPDate() {
        return visHBPDate;
    }

    public void setVisHBPDate(String visHBPDate) {
        this.visHBPDate = visHBPDate;
    }

    public String getVisHTemperatureDate() {
        return visHTemperatureDate;
    }

    public void setVisHTemperatureDate(String visHTemperatureDate) {
        this.visHTemperatureDate = visHTemperatureDate;
    }

    public String getVisHUrineproteinDate() {
        return visHUrineproteinDate;
    }

    public void setVisHUrineproteinDate(String visHUrineproteinDate) {
        this.visHUrineproteinDate = visHUrineproteinDate;
    }

    public String getVisHHbDate() {
        return visHHbDate;
    }

    public void setVisHHbDate(String visHHbDate) {
        this.visHHbDate = visHHbDate;
    }

    public String getVisHAdvise() {
        return visHAdvise;
    }

    public void setVisHAdvise(String visHAdvise) {
        this.visHAdvise = visHAdvise;
    }

    public String getVisHPulseDate() {
        return visHPulseDate;
    }

    public void setVisHPulseDate(String visHPulseDate) {
        this.visHPulseDate = visHPulseDate;
    }

    public String getVisHPulse() {
        return visHPulse;
    }

    public void setVisHPulse(String visHPulse) {
        this.visHPulse = visHPulse;
    }

    public String getVisHVisitFSFAGiven() {
        return visHVisitFSFAGiven;
    }

    public void setVisHVisitFSFAGiven(String visHVisitFSFAGiven) {
        this.visHVisitFSFAGiven = visHVisitFSFAGiven;
    }

    public String getVisHVisitFSFAQty() {
        return visHVisitFSFAQty;
    }

    public void setVisHVisitFSFAQty(String visHVisitFSFAQty) {
        this.visHVisitFSFAQty = visHVisitFSFAQty;
    }

    public String getVisHVisitFSFANotGivenReason() {
        return visHVisitFSFANotGivenReason;
    }

    public void setVisHVisitFSFANotGivenReason(String visHVisitFSFANotGivenReason) {
        this.visHVisitFSFANotGivenReason = visHVisitFSFANotGivenReason;
    }

    public String getVisHWeight() {
        return visHWeight;
    }

    public void setVisHWeight(String visHWeight) {
        this.visHWeight = visHWeight;
    }

    public String getVisHWeightDate() {
        return visHWeightDate;
    }

    public void setVisHWeightDate(String visHWeightDate) {
        this.visHWeightDate = visHWeightDate;
    }

    public String getVisHChildSymptoms() {
        return visHChildSymptoms;
    }

    public void setVisHChildSymptoms(String visHChildSymptoms) {
        this.visHChildSymptoms = visHChildSymptoms;
    }

    public String getVisHReferralSlipNumber() {
        return visHReferralSlipNumber;
    }

    public void setVisHReferralSlipNumber(String visHReferralSlipNumber) {
        this.visHReferralSlipNumber = visHReferralSlipNumber;
    }
}
