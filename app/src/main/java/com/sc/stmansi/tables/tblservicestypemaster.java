//27Aug2019 Arpitha - code clean up
package com.sc.stmansi.tables;

import androidx.annotation.NonNull;

import com.j256.ormlite.field.DatabaseField;

public class tblservicestypemaster {

    @DatabaseField
    private int ServiceId;
    @DatabaseField
    private String ServiceType;
    @DatabaseField
    private int ServiceNumber;
    @DatabaseField
    private String isImmunisation;
    @DatabaseField
    private String TaskReference;
    @DatabaseField
    private int DaysDiffStart;
    @DatabaseField
    private int DaysDiffEnd;


    public int getServiceId() {
        return ServiceId;
    }

    public void setServiceId(int serviceId) {
        ServiceId = serviceId;
    }

    public int getServiceNumber() {
        return ServiceNumber;
    }
    public String getServiceType() {
        return ServiceType;
    }

    public void setServiceType(String serviceType) {
        ServiceType = serviceType;
    }
    public void setServiceNumber(int serviceNumber) {
        ServiceNumber = serviceNumber;
    }

    public String getIsImmunisation() {
        return isImmunisation;
    }

    public void setIsImmunisation(String isImmunisation) {
        this.isImmunisation = isImmunisation;
    }

    public String getTaskReference() {
        return TaskReference;
    }

    public void setTaskReference(String taskReference) {
        TaskReference = taskReference;
    }

    public int getDaysDiffStart() {
        return DaysDiffStart;
    }

    public void setDaysDiffStart(int daysDiffStart) {
        DaysDiffStart = daysDiffStart;
    }

    public int getDaysDiffEnd() {
        return DaysDiffEnd;
    }

    public void setDaysDiffEnd(int daysDiffEnd) {
        DaysDiffEnd = daysDiffEnd;
    }

    @NonNull
    @Override
    public String toString() {
        return getServiceType();
    }
}
