package com.sc.stmansi.tables;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "tblheightforage")
public class tblheightforage {
    @DatabaseField
    private int Xaxis;
    @DatabaseField
    private String cgmBoysHtMedian;
    @DatabaseField
    private String cgmBoysHtFirstdNegSD;
    @DatabaseField
    private String cgmBoysHtSeconddNegSD;
    @DatabaseField
    private String cgmGirlsHtMedian;
    @DatabaseField
    private String cgmGirlsHtFirstdNegSD;
    @DatabaseField
    private String cgmGirlsHtSeconddNegSD;

    public int getXaxis() {
        return Xaxis;
    }

    public void setXaxis(int xaxis) {
        Xaxis = xaxis;
    }

    public String getCgmBoysHtMedian() {
        return cgmBoysHtMedian;
    }

    public void setCgmBoysHtMedian(String cgmBoysHtMedian) {
        this.cgmBoysHtMedian = cgmBoysHtMedian;
    }

    public String getCgmBoysHtFirstdNegSD() {
        return cgmBoysHtFirstdNegSD;
    }

    public void setCgmBoysHtFirstdNegSD(String cgmBoysHtFirstdNegSD) {
        this.cgmBoysHtFirstdNegSD = cgmBoysHtFirstdNegSD;
    }

    public String getCgmBoysHtSeconddNegSD() {
        return cgmBoysHtSeconddNegSD;
    }

    public void setCgmBoysHtSeconddNegSD(String cgmBoysHtSeconddNegSD) {
        this.cgmBoysHtSeconddNegSD = cgmBoysHtSeconddNegSD;
    }

    public String getCgmGirlsHtMedian() {
        return cgmGirlsHtMedian;
    }

    public void setCgmGirlsHtMedian(String cgmGirlsHtMedian) {
        this.cgmGirlsHtMedian = cgmGirlsHtMedian;
    }

    public String getCgmGirlsHtFirstdNegSD() {
        return cgmGirlsHtFirstdNegSD;
    }

    public void setCgmGirlsHtFirstdNegSD(String cgmGirlsHtFirstdNegSD) {
        this.cgmGirlsHtFirstdNegSD = cgmGirlsHtFirstdNegSD;
    }

    public String getCgmGirlsHtSeconddNegSD() {
        return cgmGirlsHtSeconddNegSD;
    }

    public void setCgmGirlsHtSeconddNegSD(String cgmGirlsHtSeconddNegSD) {
        this.cgmGirlsHtSeconddNegSD = cgmGirlsHtSeconddNegSD;
    }
}
