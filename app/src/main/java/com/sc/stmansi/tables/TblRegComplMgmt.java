package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;


public class TblRegComplMgmt implements Serializable {
    @DatabaseField
    private String WomanId;
    @DatabaseField
    private String UserId;
    @DatabaseField
    private String regWomanName;
    @DatabaseField
    private String regLMP;
    @DatabaseField
    private String regADDate;
    @DatabaseField
    private int regPregnantOrMother;
    @DatabaseField
    private String CurrentWomenStatus;
    @DatabaseField
    private String VisitDangerSigns;
    @DatabaseField
    private String VisitDate;
    @DatabaseField
    private int VisitNum;
    @DatabaseField
    private String VisitType;
    @DatabaseField
    private int AutoId;
    //08Dec2019 - Bindu
    @DatabaseField
    private String hcmActTakStatus;
    @DatabaseField
    private String hcmActionToBeTakenClient;



    public String getWomanId() {
        return WomanId;
    }

    public void setWomanId(String womanId) {
        WomanId = womanId;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getRegWomanName() {
        return regWomanName;
    }

    public void setRegWomanName(String regWomanName) {
        this.regWomanName = regWomanName;
    }

    public String getRegLMP() {
        return regLMP;
    }

    public void setRegLMP(String regLMP) {
        this.regLMP = regLMP;
    }

    public String getCurrentWomenStatus() {
        return CurrentWomenStatus;
    }

    public void setCurrentWomenStatus(String currentWomenStatus) {
        CurrentWomenStatus = currentWomenStatus;
    }

    public String getVisitDangerSigns() {
        return VisitDangerSigns;
    }

    public void setVisitDangerSigns(String visitDangerSigns) {
        VisitDangerSigns = visitDangerSigns;
    }

    public String getVisitDate() {
        return VisitDate;
    }

    public void setVisitDate(String visitDate) {
        VisitDate = visitDate;
    }

    public int getVisitNum() {
        return VisitNum;
    }

    public void setVisitNum(int visitNum) {
        VisitNum = visitNum;
    }

    public String getVisitType() {
        return VisitType;
    }

    public void setVisitType(String visitType) {
        VisitType = visitType;
    }

    public String getRegADD() {
        return regADDate;
    }

    public void setRegADD(String regADD) {
        this.regADDate = regADD;
    }

    public int getRegPregnantOrMother() {
        return regPregnantOrMother;
    }

    public void setRegPregnantOrMother(int regPregnantOrMother) {
        this.regPregnantOrMother = regPregnantOrMother;
    }

    public int getAutoId() {
        return AutoId;
    }

    public void setAutoId(int autoId) {
        AutoId = autoId;
    }

    public String getHcmActTakStatus() {
        return hcmActTakStatus;
    }

    public void setHcmActTakStatus(String hcmActTakStatus) {
        this.hcmActTakStatus = hcmActTakStatus;
    }

    public String getHcmActionToBeTakenClient() {
        return hcmActionToBeTakenClient;
    }

    public void setHcmActionToBeTakenClient(String hcmActionToBeTakenClient) {
        this.hcmActionToBeTakenClient = hcmActionToBeTakenClient;
    }
}
