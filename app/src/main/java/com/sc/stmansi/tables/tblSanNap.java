package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;

public class tblSanNap {
    @DatabaseField
    private String UserId;
    @DatabaseField
    private String AdolId;
    @DatabaseField
    private String SanNapId;
    @DatabaseField
    private String SanNapValue;
    @DatabaseField
    private String SanNapReason;
    @DatabaseField
    private String SanNapComments;
    @DatabaseField
    private int transId;
    @DatabaseField
    private String RecordCreatedDate;
    @DatabaseField
    private String RecordUpdatedDate;

    public int getTransId() {
        return transId;
    }

    public void setTransId(int transId) {
        this.transId = transId;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getAdolId() {
        return AdolId;
    }

    public void setAdolId(String adolId) {
        AdolId = adolId;
    }


    public String getSanNapValue() {
        return SanNapValue;
    }

    public void setSanNapValue(String sanNapValue) {
        SanNapValue = sanNapValue;
    }

    public String getSanNapReason() {
        return SanNapReason;
    }

    public void setSanNapReason(String sanNapReason) {
        SanNapReason = sanNapReason;
    }

    public String getSanNapComments() {
        return SanNapComments;
    }

    public void setSanNapComments(String sanNapComments) {
        SanNapComments = sanNapComments;
    }

    public String getRecordCreatedDate() {
        return RecordCreatedDate;
    }

    public void setRecordCreatedDate(String recordCreatedDate) {
        RecordCreatedDate = recordCreatedDate;
    }

    public String getRecordUpdatedDate() {
        return RecordUpdatedDate;
    }

    public void setRecordUpdatedDate(String recordUpdatedDate) {
        RecordUpdatedDate = recordUpdatedDate;
    }

    public String getSanNapId() {
        return SanNapId;
    }

    public void setSanNapId(String sanNapId) {
        SanNapId = sanNapId;
    }
}
