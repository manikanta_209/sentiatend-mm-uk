package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;

public class tblAdolVisitDetails implements Serializable {
    @DatabaseField(id = true)
    Integer autoId;
    @DatabaseField
    String UserId;
    @DatabaseField
    String AdolId;
    @DatabaseField
    String VisitId;
    @DatabaseField
    String adolvisDVisitDate;
    @DatabaseField
    String adolvisDFollowUps;
    @DatabaseField
    Integer transId;
    @DatabaseField
    String RecordCreatedDate;
    @DatabaseField
    String RecordUpdatedDate;

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getAdolId() {
        return AdolId;
    }

    public void setAdolId(String adolId) {
        AdolId = adolId;
    }

    public String getVisitId() {
        return VisitId;
    }

    public void setVisitId(String visitId) {
        VisitId = visitId;
    }

    public String getAdolvisDVisitDate() {
        return adolvisDVisitDate;
    }

    public void setAdolvisDVisitDate(String adolvisDVisitDate) {
        this.adolvisDVisitDate = adolvisDVisitDate;
    }

    public String getAdolvisDFollowUps() {
        return adolvisDFollowUps;
    }

    public void setAdolvisDFollowUps(String adolvisDFollowUps) {
        this.adolvisDFollowUps = adolvisDFollowUps;
    }

    public Integer getTransId() {
        return transId;
    }

    public void setTransId(Integer transId) {
        this.transId = transId;
    }

    public String getRecordCreatedDate() {
        return RecordCreatedDate;
    }

    public void setRecordCreatedDate(String recordCreatedDate) {
        RecordCreatedDate = recordCreatedDate;
    }

    public String getRecordUpdatedDate() {
        return RecordUpdatedDate;
    }

    public void setRecordUpdatedDate(String recordUpdatedDate) {
        RecordUpdatedDate = recordUpdatedDate;
    }

    public Integer getAutoId() {
        return autoId;
    }

    public void setAutoId(Integer autoId) {
        this.autoId = autoId;
    }
}
