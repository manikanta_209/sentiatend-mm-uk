//27Aug2019 Arpitha - code clean up
package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "tblBadgeCount")
public class TblBadgeCount {

    @DatabaseField
    private int MenuItemId;
    @DatabaseField
    private String MenuItemName;
    @DatabaseField
    private int BadgeCount;

    public String getMenuItemName() {
        return MenuItemName;
    }

    public int getBadgeCount() {
        return BadgeCount;
    }

    public int getMenuItemId() {
        return MenuItemId;
    }

    public void setMenuItemId(int menuId) {
        this.MenuItemId = menuId;
    }

    public void setMenuItemName(String MenuItemName) {
        this.MenuItemName = MenuItemName;
    }

    public void setBadgeCount(int BadgeCount) {
        this.BadgeCount = BadgeCount;
    }

}
