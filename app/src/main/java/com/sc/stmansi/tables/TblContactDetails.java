package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "tblcontactdetails")

public class TblContactDetails {


    @DatabaseField String UserId ;
    @DatabaseField
    String contactselectedoption ;
    @DatabaseField String contactNumber ;
    @DatabaseField String contactDesignation ;
    @DatabaseField String contactUserType ;
    @DatabaseField int transId ;
    @DatabaseField String RecordCreatedDate ;
    @DatabaseField String RecordUpdatedDate ;
    @DatabaseField int contactDeleted;

    public String getRecordUpdatedDate() {
        return RecordUpdatedDate;
    }

    public void setRecordUpdatedDate(String recordUpdatedDate) {
        RecordUpdatedDate = recordUpdatedDate;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getContactselectedoption() {
        return contactselectedoption;
    }

    public void setContactselectedoption(String contactselectedoption) {
        this.contactselectedoption = contactselectedoption;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getContactDesignation() {
        return contactDesignation;
    }

    public void setContactDesignation(String contactDesignation) {
        this.contactDesignation = contactDesignation;
    }

    public String getContactUserType() {
        return contactUserType;
    }

    public void setContactUserType(String contactUserType) {
        this.contactUserType = contactUserType;
    }

    public int getTransId() {
        return transId;
    }

    public void setTransId(int transId) {
        this.transId = transId;
    }

    public String getRecordCreatedDate() {
        return RecordCreatedDate;
    }

    public void setRecordCreatedDate(String recordCreatedDate) {
        RecordCreatedDate = recordCreatedDate;
    }


}
