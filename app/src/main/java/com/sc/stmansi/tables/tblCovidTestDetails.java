package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "tblCovidTestDetails")
public class tblCovidTestDetails {

    @DatabaseField
    private String UserId;
    @DatabaseField
    private String WomenId;
    @DatabaseField
    private String beneficiaryType;
    @DatabaseField
    private String VisitNum;
    @DatabaseField
    String HealthIssues;
    @DatabaseField
    String HealthIssuesOthers;
    @DatabaseField
    String CovidTest;
    @DatabaseField
    String CovidResult;
    @DatabaseField
    String CovidResultDate;
    @DatabaseField
    private int transId;
    @DatabaseField
    private String RecordCreatedDate;
    @DatabaseField
    private String RecordUpdatedDate;

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getWomenId() {
        return WomenId;
    }

    public void setWomenId(String womenId) {
        WomenId = womenId;
    }

    public String getBeneficiaryType() {
        return beneficiaryType;
    }

    public void setBeneficiaryType(String beneficiaryType) {
        this.beneficiaryType = beneficiaryType;
    }

    public String getHealthIssues() {
        return HealthIssues;
    }

    public void setHealthIssues(String healthIssues) {
        HealthIssues = healthIssues;
    }

    public String getHealthIssuesOthers() {
        return HealthIssuesOthers;
    }

    public void setHealthIssuesOthers(String healthIssuesOthers) {
        HealthIssuesOthers = healthIssuesOthers;
    }

    public String getCovidTest() {
        return CovidTest;
    }

    public void setCovidTest(String covidTest) {
        CovidTest = covidTest;
    }

    public String getCovidResult() {
        return CovidResult;
    }

    public void setCovidResult(String covidResult) {
        CovidResult = covidResult;
    }

    public String getCovidResultDate() {
        return CovidResultDate;
    }

    public void setCovidResultDate(String covidResultDate) {
        CovidResultDate = covidResultDate;
    }

    public int getTransId() {
        return transId;
    }

    public void setTransId(int transId) {
        this.transId = transId;
    }

    public String getRecordCreatedDate() {
        return RecordCreatedDate;
    }

    public void setRecordCreatedDate(String recordCreatedDate) {
        RecordCreatedDate = recordCreatedDate;
    }

    public String getRecordUpdatedDate() {
        return RecordUpdatedDate;
    }

    public void setRecordUpdatedDate(String recordUpdatedDate) {
        RecordUpdatedDate = recordUpdatedDate;
    }

    public String getVisitNum() {
        return VisitNum;
    }

    public void setVisitNum(String visitNum) {
        VisitNum = visitNum;
    }
}
