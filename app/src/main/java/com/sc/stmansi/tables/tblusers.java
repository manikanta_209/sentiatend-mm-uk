package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;

public class tblusers {
 @DatabaseField
    private String userId;
             @DatabaseField
    private String emailId;
             @DatabaseField
    private String govt_id_type;
             @DatabaseField
    private String govt_id;
             @DatabaseField
    private String password;
             @DatabaseField
    private String facilityName;
             @DatabaseField
    private String role;
             @DatabaseField
    private String firstname;
             @DatabaseField
    private String lastname;
             @DatabaseField
    private String phonenumber;
             @DatabaseField
    private String address;
             @DatabaseField
    private String userImage;
             @DatabaseField
    private String firsttimelogin;
             @DatabaseField
    private String activationkey;
             @DatabaseField
    private String status;
             @DatabaseField
    private String id;
             @DatabaseField
    private String installationid;
             @DatabaseField
    private String deviceid;
             @DatabaseField
    private String RunningId;
             @DatabaseField
    private String LastWomenNumber;
             @DatabaseField
    private String LastActivityNumber;
             @DatabaseField
    private String LastTransNumber;
             @DatabaseField
    private String LastRequestNumber;
             @DatabaseField
    private String LastDangerNumber;
             @DatabaseField
    private String Validated;
             @DatabaseField
    private String isTabLost;
             @DatabaseField
    private String isDeleted;
             @DatabaseField
    private String isActive;
             @DatabaseField
    private String created_by;
             @DatabaseField
    private String updated_by;
             @DatabaseField
    private String createddate;
             @DatabaseField
    private String updateddate;
             @DatabaseField
             private String facilityType;

    @DatabaseField
    private String state;

    @DatabaseField
    private String district;

    @DatabaseField
    private String subDistrict;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getGovt_id_type() {
        return govt_id_type;
    }

    public void setGovt_id_type(String govt_id_type) {
        this.govt_id_type = govt_id_type;
    }

    public String getGovt_id() {
        return govt_id;
    }

    public void setGovt_id(String govt_id) {
        this.govt_id = govt_id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String getFacilityName() {
        return facilityName;
    }

    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getFirsttimelogin() {
        return firsttimelogin;
    }

    public void setFirsttimelogin(String firsttimelogin) {
        this.firsttimelogin = firsttimelogin;
    }

    public String getActivationkey() {
        return activationkey;
    }

    public void setActivationkey(String activationkey) {
        this.activationkey = activationkey;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInstallationid() {
        return installationid;
    }

    public void setInstallationid(String installationid) {
        this.installationid = installationid;
    }

    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }

    public String getRunningId() {
        return RunningId;
    }

    public void setRunningId(String runningId) {
        RunningId = runningId;
    }

    public String getLastWomenNumber() {
        return LastWomenNumber;
    }

    public void setLastWomenNumber(String lastWomenNumber) {
        LastWomenNumber = lastWomenNumber;
    }

    public String getLastActivityNumber() {
        return LastActivityNumber;
    }

    public void setLastActivityNumber(String lastActivityNumber) {
        LastActivityNumber = lastActivityNumber;
    }

    public String getLastTransNumber() {
        return LastTransNumber;
    }

    public void setLastTransNumber(String lastTransNumber) {
        LastTransNumber = lastTransNumber;
    }

    public String getLastRequestNumber() {
        return LastRequestNumber;
    }

    public void setLastRequestNumber(String lastRequestNumber) {
        LastRequestNumber = lastRequestNumber;
    }

    public String getLastDangerNumber() {
        return LastDangerNumber;
    }

    public void setLastDangerNumber(String lastDangerNumber) {
        LastDangerNumber = lastDangerNumber;
    }

    public String getValidated() {
        return Validated;
    }

    public void setValidated(String validated) {
        Validated = validated;
    }

    public String getIsTabLost() {
        return isTabLost;
    }

    public void setIsTabLost(String isTabLost) {
        this.isTabLost = isTabLost;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public String getCreateddate() {
        return createddate;
    }

    public void setCreateddate(String createddate) {
        this.createddate = createddate;
    }

    public String getUpdateddate() {
        return updateddate;
    }

    public void setUpdateddate(String updateddate) {
        this.updateddate = updateddate;
    }

    public String getFacilityType() {
        return facilityType;
    }

    public void setFacilityType(String facilityType) {
        this.facilityType = facilityType;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getSubDistrict() {
        return subDistrict;
    }

    public void setSubDistrict(String subDistrict) {
        this.subDistrict = subDistrict;
    }
}
