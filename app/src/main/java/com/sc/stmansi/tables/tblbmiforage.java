package com.sc.stmansi.tables;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "tblbmiforage")
public class tblbmiforage {
    @DatabaseField
    private int Xaxis;
    @DatabaseField
    private String cgmBoysBMIMedian;
    @DatabaseField
    private String cgmBoysBMIFirstdNegSD;
    @DatabaseField
    private String cgmBoysBMISeconddNegSD;
    @DatabaseField
    private String cgmGirlsBMIMedian;
    @DatabaseField
    private String cgmGirlsBMIFirstdNegSD;
    @DatabaseField
    private String cgmGirlsBMISeconddNegSD;

    public int getXaxis() {
        return Xaxis;
    }

    public void setXaxis(int xaxis) {
        Xaxis = xaxis;
    }

    public String getCgmBoysBMIMedian() {
        return cgmBoysBMIMedian;
    }

    public void setCgmBoysBMIMedian(String cgmBoysBMIMedian) {
        this.cgmBoysBMIMedian = cgmBoysBMIMedian;
    }

    public String getCgmBoysBMIFirstdNegSD() {
        return cgmBoysBMIFirstdNegSD;
    }

    public void setCgmBoysBMIFirstdNegSD(String cgmBoysBMIFirstdNegSD) {
        this.cgmBoysBMIFirstdNegSD = cgmBoysBMIFirstdNegSD;
    }

    public String getCgmBoysBMISeconddNegSD() {
        return cgmBoysBMISeconddNegSD;
    }

    public void setCgmBoysBMISeconddNegSD(String cgmBoysBMISeconddNegSD) {
        this.cgmBoysBMISeconddNegSD = cgmBoysBMISeconddNegSD;
    }

    public String getCgmGirlsBMIMedian() {
        return cgmGirlsBMIMedian;
    }

    public void setCgmGirlsBMIMedian(String cgmGirlsBMIMedian) {
        this.cgmGirlsBMIMedian = cgmGirlsBMIMedian;
    }

    public String getCgmGirlsBMIFirstdNegSD() {
        return cgmGirlsBMIFirstdNegSD;
    }

    public void setCgmGirlsBMIFirstdNegSD(String cgmGirlsBMIFirstdNegSD) {
        this.cgmGirlsBMIFirstdNegSD = cgmGirlsBMIFirstdNegSD;
    }

    public String getCgmGirlsBMISeconddNegSD() {
        return cgmGirlsBMISeconddNegSD;
    }

    public void setCgmGirlsBMISeconddNegSD(String cgmGirlsBMISeconddNegSD) {
        this.cgmGirlsBMISeconddNegSD = cgmGirlsBMISeconddNegSD;
    }

}

