package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "tblchildimmunization")
public class TblChildImmunization {

    @DatabaseField private String UserId ;
    @DatabaseField private String WomenId ;
    @DatabaseField private String immChildId ;
    @DatabaseField private int immId ;
    @DatabaseField private int immNumber ;
    @DatabaseField private String immType ;
    @DatabaseField private String immActualDateOfAction ;
    @DatabaseField private String immMinDate ;
    @DatabaseField private String immMaxDate ;
    @DatabaseField private String immComments ;
    @DatabaseField private String immFacType ;
    @DatabaseField private String immProvidedFacName ;
    @DatabaseField private String immUserType ;
    @DatabaseField private String immProvidedGPSLoc ;
    @DatabaseField private int transId ;
    @DatabaseField private String RecordCreatedDate ;
    @DatabaseField String RecordUpdatedDate;//21May2021 Arpitha


    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        this.UserId = userId;
    }

    public String getWomenId() {
        return WomenId;
    }

    public void setWomenId(String womenId) {
        WomenId = womenId;
    }

    public String getImmChildId() {
        return immChildId;
    }

    public void setImmChildId(String immChildId) {
        this.immChildId = immChildId;
    }

    public int getImmId() {
        return immId;
    }

    public void setImmId(int immId) {
        this.immId = immId;
    }

    public int getImmNumber() {
        return immNumber;
    }

    public void setImmNumber(int immNumber) {
        this.immNumber = immNumber;
    }

    public String getImmType() {
        return immType;
    }

    public void setImmType(String immType) {
        this.immType = immType;
    }

    public String getImmActualDateofAction() {
        return immActualDateOfAction;
    }

    public void setImmActualDateofAction(String immActualDateofAction) {
        this.immActualDateOfAction = immActualDateofAction;
    }

    public String getImmMinDate() {
        return immMinDate;
    }

    public void setImmMinDate(String immMinDate) {
        this.immMinDate = immMinDate;
    }

    public String getImmMaxDate() {
        return immMaxDate;
    }

    public void setImmMaxDate(String immMaxDate) {
        this.immMaxDate = immMaxDate;
    }

    public String getImmComments() {
        return immComments;
    }

    public void setImmComments(String immComments) {
        this.immComments = immComments;
    }

    public String getImmFacType() {
        return immFacType;
    }

    public void setImmFacType(String immFacType) {
        this.immFacType = immFacType;
    }

    public String getImmProvidedFacName() {
        return immProvidedFacName;
    }

    public void setImmProvidedFacName(String immProvidedFacName) {
        this.immProvidedFacName = immProvidedFacName;
    }

    public String getImmUserType() {
        return immUserType;
    }

    public void setImmUserType(String immUserType) {
        this.immUserType = immUserType;
    }

    public String getImmProvidedGPSLoc() {
        return immProvidedGPSLoc;
    }

    public void setImmProvidedGPSLoc(String immProvidedGPSLoc) {
        this.immProvidedGPSLoc = immProvidedGPSLoc;
    }

    public int getTransId() {
        return transId;
    }

    public void setTransId(int transId) {
        this.transId = transId;
    }

    public String getRecordCreatedDate() {
        return RecordCreatedDate;
    }

    public void setRecordCreatedDate(String recordCreatedDate) {
        RecordCreatedDate = recordCreatedDate;
    }

    public String getRecordUpdatedDate() {
        return RecordUpdatedDate;
    }

    public void setRecordUpdatedDate(String recordUpdatedDate) {
        RecordUpdatedDate = recordUpdatedDate;
    }
}
