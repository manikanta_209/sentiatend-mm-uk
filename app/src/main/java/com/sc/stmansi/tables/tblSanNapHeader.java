package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;

public class tblSanNapHeader {
    
    @DatabaseField (id = true)
    private Integer AutoId;
    @DatabaseField
    private String UserId;
    @DatabaseField
    private String AdolId;
    @DatabaseField
    private String tblSNBlockName;
    @DatabaseField
    private String tblSNVillage ;
    @DatabaseField
    private String tblSNAdolName ;
    @DatabaseField
    private String tblSNAdolAge ;
    @DatabaseField
    private String tblSNAdolGTS ;
    @DatabaseField
    private String tblSNDateofSNProvided ;
    @DatabaseField
    private String tblSNDateofFeedback ;
    @DatabaseField
    private String tblSNFieldStaff ;
    @DatabaseField
    private int transId;
    @DatabaseField
    private String RecordCreatedDate ;
    @DatabaseField
    private String RecordUpdatedDate ;

    public int getTransId() {
        return transId;
    }

    public void setTransId(int transId) {
        this.transId = transId;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getAdolId() {
        return AdolId;
    }

    public void setAdolId(String adolId) {
        AdolId = adolId;
    }

    public String getTblSNBlockName() {
        return tblSNBlockName;
    }

    public void setTblSNBlockName(String tblSNBlockName) {
        this.tblSNBlockName = tblSNBlockName;
    }


    public String getTblSNVillage() {
        return tblSNVillage;
    }

    public void setTblSNVillage(String tblSNVillage) {
        this.tblSNVillage = tblSNVillage;
    }

    public String getTblSNAdolName() {
        return tblSNAdolName;
    }

    public void setTblSNAdolName(String tblSNAdolName) {
        this.tblSNAdolName = tblSNAdolName;
    }

    public String getTblSNAdolAge() {
        return tblSNAdolAge;
    }

    public void setTblSNAdolAge(String tblSNAdolAge) {
        this.tblSNAdolAge = tblSNAdolAge;
    }

    public String getTblSNAdolGTS() {
        return tblSNAdolGTS;
    }

    public void setTblSNAdolGTS(String tblSNAdolGTS) {
        this.tblSNAdolGTS = tblSNAdolGTS;
    }

    public String getTblSNDateofSNProvided() {
        return tblSNDateofSNProvided;
    }

    public void setTblSNDateofSNProvided(String tblSNDateofSNProvided) {
        this.tblSNDateofSNProvided = tblSNDateofSNProvided;
    }

    public String getTblSNDateofFeedback() {
        return tblSNDateofFeedback;
    }

    public void setTblSNDateofFeedback(String tblSNDateofFeedback) {
        this.tblSNDateofFeedback = tblSNDateofFeedback;
    }

    public String getTblSNFieldStaff() {
        return tblSNFieldStaff;
    }

    public void setTblSNFieldStaff(String tblSNFieldStaff) {
        this.tblSNFieldStaff = tblSNFieldStaff;
    }

    public String getRecordCreatedDate() {
        return RecordCreatedDate;
    }

    public void setRecordCreatedDate(String recordCreatedDate) {
        RecordCreatedDate = recordCreatedDate;
    }

    public String getRecordUpdatedDate() {
        return RecordUpdatedDate;
    }

    public void setRecordUpdatedDate(String recordUpdatedDate) {
        RecordUpdatedDate = recordUpdatedDate;
    }
}
