package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;

public class tblAdolFollowUpMaster implements Serializable {
    @DatabaseField
    private int SlNo;
    @DatabaseField
    private int followupGrpID;
    @DatabaseField
    private String followupGirlorBoy;
    @DatabaseField
    private int followupID;
    @DatabaseField
    private String followupName;
    @DatabaseField
    private String followupXmlStringName;


    public int getSlNo() {
        return SlNo;
    }

    public void setSlNo(int slNo) {
        SlNo = slNo;
    }

    public int getFollowupGrpID() {
        return followupGrpID;
    }

    public void setFollowupGrpID(int followupGrpID) {
        this.followupGrpID = followupGrpID;
    }

    public String getFollowupGirlorBoy() {
        return followupGirlorBoy;
    }

    public void setFollowupGirlorBoy(String followupGirlorBoy) {
        this.followupGirlorBoy = followupGirlorBoy;
    }

    public int getFollowupID() {
        return followupID;
    }

    public void setFollowupID(int followupID) {
        this.followupID = followupID;
    }

    public String getFollowupName() {
        return followupName;
    }

    public void setFollowupName(String followupName) {
        this.followupName = followupName;
    }

    public String getFollowupXmlStringName() {
        return followupXmlStringName;
    }

    public void setFollowupXmlStringName(String followupXmlStringName) {
        this.followupXmlStringName = followupXmlStringName;
    }
}
