//27Aug2019 Arpitha - code clean up
package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

@DatabaseTable(tableName = "tblfacilitydetails")
public class TblFacilityDetails implements Serializable {

    @DatabaseField
    private int AutoId;
    @DatabaseField
    private String UserId;
    @DatabaseField
    private String FacilityName;
    @DatabaseField
    private String LocalName;
    @DatabaseField
    private String ANMName;
    @DatabaseField
    private String ANMContactNo;
    @DatabaseField
    private String BEmOCType;
    @DatabaseField
    private String BEmOCName;
    @DatabaseField
    private String BEmOCDoctor;
    @DatabaseField
    private String BEmOCContactNumber;
    @DatabaseField
    private String BEmOCDistance;
    @DatabaseField
    private String CEmOCType;
    @DatabaseField
    private String CEmOCName;
    @DatabaseField
    private String CEmOCDoctor;
    @DatabaseField
    private String CEmOCContactNumber;
    @DatabaseField
    private String CEmOCDistance;
    @DatabaseField
    private String Country;
    @DatabaseField
    private String State;
    @DatabaseField
    private String District;
    @DatabaseField
    private String Subdistrict;
    @DatabaseField
    private String datecreated;
    @DatabaseField
    private String createdby;
    @DatabaseField
    private String dateupdated;
    @DatabaseField
    private String updatedby;

    @DatabaseField
    private String PHCName;
    @DatabaseField
    private String SCName;
    @DatabaseField
    private String PanchayathName;

    public int getAutoId() {
        return AutoId;
    }

    public void setAutoId(int autoId) {
        AutoId = autoId;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getFacilityName() {
        return FacilityName;
    }

    public void setFacilityName(String facilityName) {
        FacilityName = facilityName;
    }

    public String getLocalName() {
        return LocalName;
    }

    public void setLocalName(String localName) {
        LocalName = localName;
    }

    public String getANMName() {
        return ANMName;
    }

    public void setANMName(String ANMName) {
        this.ANMName = ANMName;
    }

    public String getANMContactNo() {
        return ANMContactNo;
    }

    public void setANMContactNo(String ANMContactNo) {
        this.ANMContactNo = ANMContactNo;
    }

    public String getBEmOCType() {
        return BEmOCType;
    }

    public void setBEmOCType(String BEmOCType) {
        this.BEmOCType = BEmOCType;
    }

    public String getBEmOCName() {
        return BEmOCName;
    }

    public void setBEmOCName(String BEmOCName) {
        this.BEmOCName = BEmOCName;
    }

    public String getBEmOCDoctor() {
        return BEmOCDoctor;
    }

    public void setBEmOCDoctor(String BEmOCDoctor) {
        this.BEmOCDoctor = BEmOCDoctor;
    }

    public String getBEmOCContactNumber() {
        return BEmOCContactNumber;
    }

    public void setBEmOCContactNumber(String BEmOCContactNumber) {
        this.BEmOCContactNumber = BEmOCContactNumber;
    }

    public String getBEmOCDistance() {
        return BEmOCDistance;
    }

    public void setBEmOCDistance(String BEmOCDistance) {
        this.BEmOCDistance = BEmOCDistance;
    }

    public String getCEmOCType() {
        return CEmOCType;
    }

    public void setCEmOCType(String CEmOCType) {
        this.CEmOCType = CEmOCType;
    }

    public String getCEmOCName() {
        return CEmOCName;
    }

    public void setCEmOCName(String CEmOCName) {
        this.CEmOCName = CEmOCName;
    }

    public String getCEmOCDoctor() {
        return CEmOCDoctor;
    }

    public void setCEmOCDoctor(String CEmOCDoctor) {
        this.CEmOCDoctor = CEmOCDoctor;
    }

    public String getCEmOCContactNumber() {
        return CEmOCContactNumber;
    }

    public void setCEmOCContactNumber(String CEmOCContactNumber) {
        this.CEmOCContactNumber = CEmOCContactNumber;
    }

    public String getCEmOCDistance() {
        return CEmOCDistance;
    }

    public void setCEmOCDistance(String CEmOCDistance) {
        this.CEmOCDistance = CEmOCDistance;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getDistrict() {
        return District;
    }

    public void setDistrict(String district) {
        District = district;
    }

    public String getSubdistrict() {
        return Subdistrict;
    }

    public void setSubdistrict(String subdistrict) {
        Subdistrict = subdistrict;
    }

    public String getDatecreated() {
        return datecreated;
    }

    public void setDatecreated(String datecreated) {
        this.datecreated = datecreated;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public String getDateupdated() {
        return dateupdated;
    }

    public void setDateupdated(String dateupdated) {
        this.dateupdated = dateupdated;
    }

    public String getUpdatedby() {
        return updatedby;
    }

    public void setUpdatedby(String updatedby) {
        this.updatedby = updatedby;
    }

    public String getPHCName() {
        return PHCName;
    }

    public void setPHCName(String PHCName) {
        this.PHCName = PHCName;
    }

    public String getSCName() {
        return SCName;
    }

    public void setSCName(String SCName) {
        this.SCName = SCName;
    }

    public String getPanchayathName() {
        return PanchayathName;
    }

    public void setPanchayathName(String panchayathName) {
        PanchayathName = panchayathName;
    }
}
