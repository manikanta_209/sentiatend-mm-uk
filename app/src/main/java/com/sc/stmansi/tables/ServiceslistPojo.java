//27Aug2019 Arpitha - code clean up
package com.sc.stmansi.tables;

public class ServiceslistPojo {


    String Servicetype;
    int ServiceId;
    int ServiceNumber;
    String ActualDateofAction;
    String deatils;
    String minDueDate;
    String maxDueDate;
    String womanId;
    String userid;
    int transId;
    //20Jul2019 - bindu - iCreateNewTrans 2 fields
    String usertype;
    String Recordupdateddate; //26Jul2019 - Bindu
    String facilityName;
    String comments;
    String facType;
    //20Jul2019 - bindu - add 2 fields
    String Recordcreateddate;
//    07May2021 Arpitha
    String serviceDetail1,serviceDetail2;

    public String getServicetype() {
        return Servicetype;
    }

    public void setServicetype(String servicetype) {
        Servicetype = servicetype;
    }

    public int getServiceId() {
        return ServiceId;
    }

    public void setServiceId(int serviceId) {
        ServiceId = serviceId;
    }

    public int getServiceNumber() {
        return ServiceNumber;
    }

    public void setServiceNumber(int serviceNumber) {
        ServiceNumber = serviceNumber;
    }


    public String getActualDateofAction() {
        return ActualDateofAction;
    }

    public void setActualDateofAction(String actualDateofAction) {
        ActualDateofAction = actualDateofAction;
    }


    public void setDeatils(String deatils) {
        this.deatils = deatils;
    }

    public String getMinDueDate() {
        return minDueDate;
    }

    public void setMinDueDate(String minDueDate) {
        this.minDueDate = minDueDate;
    }

    public String getMaxDueDate() {
        return maxDueDate;
    }

    public void setMaxDueDate(String maxDueDate) {
        this.maxDueDate = maxDueDate;
    }

    public String getWomanId() {
        return womanId;
    }

    public void setWomanId(String womanId) {
        this.womanId = womanId;
    }

    public String getFacilityName() {
        return facilityName;
    }

    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getFacType() {
        return facType;
    }

    public void setFacType(String facType) {
        this.facType = facType;
    }

    public int getTransId() {
        return transId;
    }

    public void setTransId(int transId) {
        this.transId = transId;
    }

    public String getUsertype() {
        return usertype;
    }

    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }

    public String getRecordupdateddate() {
        return Recordupdateddate;
    }

    public void setRecordupdateddate(String recordupdateddate) {
        Recordupdateddate = recordupdateddate;
    }

    public String getRecordcreateddate() {
        return Recordcreateddate;
    }

    public void setRecordcreateddate(String recordcreateddate) {
        Recordcreateddate = recordcreateddate;
    }

//     07May2021 Arpitha

    public String getServiceDetail1() {
        return serviceDetail1;
    }

    public void setServiceDetail1(String serviceDetail1) {
        this.serviceDetail1 = serviceDetail1;
    }

    public String getServiceDetail2() {
        return serviceDetail2;
    }

    public void setServiceDetail2(String serviceDetail2) {
        this.serviceDetail2 = serviceDetail2;
    }
}
