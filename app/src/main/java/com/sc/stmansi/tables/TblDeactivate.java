package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "tbldeactivate")
public class TblDeactivate {


    @DatabaseField private String UserId;
    @DatabaseField
    private String WomanId;
    @DatabaseField private String ChildId;
    @DatabaseField private String deacDate;
    @DatabaseField private String deacReason;
    @DatabaseField private String deacOtherReason;
    @DatabaseField private String deacReasonDate;
    @DatabaseField private String deacComments;
    @DatabaseField private int transId;
    @DatabaseField private String RecordCreatedDate;


    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getWomanId() {
        return WomanId;
    }

    public void setWomanId(String womanId) {
        WomanId = womanId;
    }

    public String getChildId() {
        return ChildId;
    }

    public void setChildId(String childId) {
        ChildId = childId;
    }

    public String getDeacDate() {
        return deacDate;
    }

    public void setDeacDate(String deacDate) {
        this.deacDate = deacDate;
    }

    public String getDeacReason() {
        return deacReason;
    }

    public void setDeacReason(String deacReason) {
        this.deacReason = deacReason;
    }

    public String getDeacOtherReason() {
        return deacOtherReason;
    }

    public void setDeacOtherReason(String deacOtherReason) {
        this.deacOtherReason = deacOtherReason;
    }

    public String getDeacReasonDate() {
        return deacReasonDate;
    }

    public void setDeacReasonDate(String deacReasonDate) {
        this.deacReasonDate = deacReasonDate;
    }

    public String getDeacComments() {
        return deacComments;
    }

    public void setDeacComments(String deacComments) {
        this.deacComments = deacComments;
    }

    public int getTransId() {
        return transId;
    }

    public void setTransId(int transId) {
        this.transId = transId;
    }

    public String getRecordCreatedDate() {
        return RecordCreatedDate;
    }

    public void setRecordCreatedDate(String recordCreatedDate) {
        RecordCreatedDate = recordCreatedDate;
    }
}
