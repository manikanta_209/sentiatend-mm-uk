package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;

public class tblChildVisitDetails {
    @DatabaseField(id = true)
    Integer AutoId;
    @DatabaseField private String  UserId;
    @DatabaseField private String  WomanId;
    @DatabaseField private String  ChildId;
    @DatabaseField private int  ChildNo;
    @DatabaseField private int  VisitNum;
    @DatabaseField private String  visDVisitDate;
    @DatabaseField private String  visDSymptoms;
    @DatabaseField private String  visDOtherSymptoms;
    @DatabaseField private String  visDActionTobeTaken;
    @DatabaseField private String  visDReferredToFacilityType;
    @DatabaseField private String  visHUserType;
    @DatabaseField private int  transid;
    @DatabaseField private String  Recordcreateddate;
    @DatabaseField private String  Recordupdateddate;


    public Integer getAutoId() {
        return AutoId;
    }

    public void setAutoId(Integer autoId) {
        AutoId = autoId;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getWomanId() {
        return WomanId;
    }

    public void setWomanId(String womanId) {
        WomanId = womanId;
    }

    public int getVisitNum() {
        return VisitNum;
    }

    public void setVisitNum(int visitNum) {
        VisitNum = visitNum;
    }



    public String getVisDVisitDate() {
        return visDVisitDate;
    }

    public void setVisDVisitDate(String visDVisitDate) {
        this.visDVisitDate = visDVisitDate;
    }

    public String getVisDSymptoms() {
        return visDSymptoms;
    }

    public void setVisDSymptoms(String visDSymptoms) {
        this.visDSymptoms = visDSymptoms;
    }

    public String getVisDOtherSymptoms() {
        return visDOtherSymptoms;
    }

    public void setVisDOtherSymptoms(String visDOtherSymptoms) {
        this.visDOtherSymptoms = visDOtherSymptoms;
    }

    public String getVisDActionTobeTaken() {
        return visDActionTobeTaken;
    }

    public void setVisDActionTobeTaken(String visDActionTobeTaken) {
        this.visDActionTobeTaken = visDActionTobeTaken;
    }

    public String getVisDReferredToFacilityType() {
        return visDReferredToFacilityType;
    }

    public void setVisDReferredToFacilityType(String visDReferredToFacilityType) {
        this.visDReferredToFacilityType = visDReferredToFacilityType;
    }

    public void setTransid(int transid) {
        this.transid = transid;
    }

    public String getVisHUserType() {
        return visHUserType;
    }

    public void setVisHUserType(String visHUserType) {
        this.visHUserType = visHUserType;
    }

    public Integer getTransid() {
        return transid;
    }

    public void setTransid(Integer transid) {
        this.transid = transid;
    }

    public String getRecordcreateddate() {
        return Recordcreateddate;
    }

    public void setRecordcreateddate(String recordcreateddate) {
        Recordcreateddate = recordcreateddate;
    }

    public String getRecordupdateddate() {
        return Recordupdateddate;
    }

    public void setRecordupdateddate(String recordupdateddate) {
        Recordupdateddate = recordupdateddate;
    }

    public String getChildId() {
        return ChildId;
    }

    public void setChildId(String childId) {
        ChildId = childId;
    }

    public int getChildNo() {
        return ChildNo;
    }

    public void setChildNo(int childNo) {
        ChildNo = childNo;
    }
}
