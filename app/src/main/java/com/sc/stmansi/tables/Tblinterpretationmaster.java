package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "tblinterpretationmaster")
public class Tblinterpretationmaster {

    @DatabaseField
    private int SlNo ;
    @DatabaseField
    private int symptomgroupid ;
    @DatabaseField
    private String interpretationId ;
    @DatabaseField
    private String interpretationName ;
    @DatabaseField
    private String interpretationXmlStringName ;

    public int getSlNo() {
        return SlNo;
    }

    public void setSlNo(int slNo) {
        SlNo = slNo;
    }

    public int getSymptomgroupid() {
        return symptomgroupid;
    }

    public void setSymptomgroupid(int symptomgroupid) {
        this.symptomgroupid = symptomgroupid;
    }

    public String getInterpretationId() {
        return interpretationId;
    }

    public void setInterpretationId(String interpretationId) {
        this.interpretationId = interpretationId;
    }

    public String getInterpretationName() {
        return interpretationName;
    }

    public void setInterpretationName(String interpretationName) {
        this.interpretationName = interpretationName;
    }

    public String getInterpretationXmlStringName() {
        return interpretationXmlStringName;
    }

    public void setInterpretationXmlStringName(String interpretationXmlStringName) {
        this.interpretationXmlStringName = interpretationXmlStringName;
    }
}
