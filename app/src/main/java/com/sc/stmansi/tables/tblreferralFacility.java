//27Aug2019 Arpitha - code clean up
package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

@DatabaseTable(tableName = "tblreferralFacility")
public class tblreferralFacility implements Serializable {

    @DatabaseField
    private int AutoId;
    @DatabaseField
    private String FacilityType;
    @DatabaseField
    private String FacilityName;

    @DatabaseField
    private String Country;
    @DatabaseField
    private String State;
    @DatabaseField
    private String District;
    @DatabaseField
    private String Subdistrict;
    @DatabaseField
    private String datecreated;
    @DatabaseField
    private String createdby;
    @DatabaseField
    private String dateupdated;
    @DatabaseField
    private String updatedby;



    public int getAutoId() {
        return AutoId;
    }

    public void setAutoId(int autoId) {
        AutoId = autoId;
    }



    public String getFacilityName() {
        return FacilityName;
    }

    public void setFacilityName(String facilityName) {
        FacilityName = facilityName;
    }



    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getDistrict() {
        return District;
    }

    public void setDistrict(String district) {
        District = district;
    }

    public String getSubdistrict() {
        return Subdistrict;
    }

    public void setSubdistrict(String subdistrict) {
        Subdistrict = subdistrict;
    }

    public String getDatecreated() {
        return datecreated;
    }

    public void setDatecreated(String datecreated) {
        this.datecreated = datecreated;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public String getDateupdated() {
        return dateupdated;
    }

    public void setDateupdated(String dateupdated) {
        this.dateupdated = dateupdated;
    }

    public String getUpdatedby() {
        return updatedby;
    }

    public void setUpdatedby(String updatedby) {
        this.updatedby = updatedby;
    }

    public String getFacilityType() {
        return FacilityType;
    }

    public void setFacilityType(String facilityType) {
        FacilityType = facilityType;
    }
}
