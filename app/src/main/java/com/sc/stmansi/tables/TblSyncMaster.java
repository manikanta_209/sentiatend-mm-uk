//27Aug2019 Arpitha - code clean up
package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;

public class TblSyncMaster {

    @DatabaseField
    private String userId;
    @DatabaseField
    private String RequestId;
    @DatabaseField
    private String RequestDate;
    @DatabaseField
    private String RequestStatusDate;
    @DatabaseField
    private String RequestStatus;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRequestId() {
        return RequestId;
    }

    public void setRequestId(String requestId) {
        RequestId = requestId;
    }

    public String getRequestDate() {
        return RequestDate;
    }

    public void setRequestDate(String requestDate) {
        RequestDate = requestDate;
    }

    public String getRequestStatusDate() {
        return RequestStatusDate;
    }

    public void setRequestStatusDate(String requestStatusDate) {
        RequestStatusDate = requestStatusDate;
    }

    public String getRequestStatus() {
        return RequestStatus;
    }

    public void setRequestStatus(String requestStatus) {
        RequestStatus = requestStatus;
    }
}
