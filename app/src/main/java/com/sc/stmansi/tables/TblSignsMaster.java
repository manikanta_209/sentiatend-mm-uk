package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "tblsignsmaster")
public class TblSignsMaster {

    @DatabaseField
    private int SlNo ;
    @DatabaseField
    private int symptomgroupid ;
    @DatabaseField
    private String signId ;
    @DatabaseField
    private String signName ;
    @DatabaseField
    private String signXmlStringName ;

    public int getSlNo() {
        return SlNo;
    }

    public void setSlNo(int slNo) {
        SlNo = slNo;
    }

    public int getSymptomgroupid() {
        return symptomgroupid;
    }

    public void setSymptomgroupid(int symptomgroupid) {
        this.symptomgroupid = symptomgroupid;
    }

    public String getSignId() {
        return signId;
    }

    public void setSignId(String signId) {
        this.signId = signId;
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }

    public String getSignXmlStringName() {
        return signXmlStringName;
    }

    public void setSignXmlStringName(String signXmlStringName) {
        this.signXmlStringName = signXmlStringName;
    }
}
