//27Aug2019 Arpitha - code clean up
package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;

public class tbltransHeader {

    @DatabaseField
    private String TransDate;
    @DatabaseField
    private String TransStatus;
    @DatabaseField
    private String RequestId;
    @DatabaseField
    private String transId;

    public String getTransDate() {
        return TransDate;
    }

    public void setTransDate(String transDate) {
        TransDate = transDate;
    }

    public String getTransStatus() {
        return TransStatus;
    }

    public void setTransStatus(String transStatus) {
        TransStatus = transStatus;
    }

    public String getRequestId() {
        return RequestId;
    }

    public void setRequestId(String requestId) {
        RequestId = requestId;
    }

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }
}
