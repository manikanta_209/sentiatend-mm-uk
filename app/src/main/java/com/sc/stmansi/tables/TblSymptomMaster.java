package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "tblsymptomsmaster")
public class TblSymptomMaster {

    @DatabaseField
    private int SlNo;
    @DatabaseField
    private int symGroupId;
    @DatabaseField
    private String symVisitType;
    @DatabaseField
    private String symWomanorChild;
    @DatabaseField
    private String symSymptomId;
    @DatabaseField
    private String symSymptomName;
    @DatabaseField
    private String symXmlStringName;

    public int getSlNo() {
        return SlNo;
    }

    public void setSlNo(int slNo) {
        SlNo = slNo;
    }

    public int getSymGroupId() {
        return symGroupId;
    }

    public void setSymGroupId(int symGroupId) {
        this.symGroupId = symGroupId;
    }

    public String getSymVisitType() {
        return symVisitType;
    }

    public void setSymVisitType(String symVisitType) {
        this.symVisitType = symVisitType;
    }

    public String getSymWomanorChild() {
        return symWomanorChild;
    }

    public void setSymWomanorChild(String symWomanorChild) {
        this.symWomanorChild = symWomanorChild;
    }

    public String getSymSymptomId() {
        return symSymptomId;
    }

    public void setSymSymptomId(String symSymptomId) {
        this.symSymptomId = symSymptomId;
    }

    public String getSymSymptomName() {
        return symSymptomName;
    }

    public void setSymSymptomName(String symSymptomName) {
        this.symSymptomName = symSymptomName;
    }

    public String getSymXmlStringName() {
        return symXmlStringName;
    }

    public void setSymXmlStringName(String symXmlStringName) {
        this.symXmlStringName = symXmlStringName;
    }
}
