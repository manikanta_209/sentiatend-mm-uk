package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "tblmuacforage")
public class tblmuacforage {
    @DatabaseField
    private int Xaxis;
    @DatabaseField
    private String cgmBoysHeadcfMedian;
    @DatabaseField
    private String cgmBoysHeadcfFirstdNegSD;
    @DatabaseField
    private String cgmBoysHeadcfSeconddNegSD;
    @DatabaseField
    private String cgmGirlsHeadcfMedian;
    @DatabaseField
    private String cgmGirlsHeadcfFirstdNegSD;
    @DatabaseField
    private String cgmGirlsHeadcfSeconddNegSD;

    public int getXaxis() {
        return Xaxis;
    }

    public void setXaxis(int xaxis) {
        Xaxis = xaxis;
    }

    public String getCgmBoysHeadcfMedian() {
        return cgmBoysHeadcfMedian;
    }

    public void setCgmBoysHeadcfMedian(String cgmBoysHeadcfMedian) {
        this.cgmBoysHeadcfMedian = cgmBoysHeadcfMedian;
    }

    public String getCgmBoysHeadcfFirstdNegSD() {
        return cgmBoysHeadcfFirstdNegSD;
    }

    public void setCgmBoysHeadcfFirstdNegSD(String cgmBoysHeadcfFirstdNegSD) {
        this.cgmBoysHeadcfFirstdNegSD = cgmBoysHeadcfFirstdNegSD;
    }

    public String getCgmBoysHeadcfSeconddNegSD() {
        return cgmBoysHeadcfSeconddNegSD;
    }

    public void setCgmBoysHeadcfSeconddNegSD(String cgmBoysHeadcfSeconddNegSD) {
        this.cgmBoysHeadcfSeconddNegSD = cgmBoysHeadcfSeconddNegSD;
    }

    public String getCgmGirlsHeadcfMedian() {
        return cgmGirlsHeadcfMedian;
    }

    public void setCgmGirlsHeadcfMedian(String cgmGirlsHeadcfMedian) {
        this.cgmGirlsHeadcfMedian = cgmGirlsHeadcfMedian;
    }

    public String getCgmGirlsHeadcfFirstdNegSD() {
        return cgmGirlsHeadcfFirstdNegSD;
    }

    public void setCgmGirlsHeadcfFirstdNegSD(String cgmGirlsHeadcfFirstdNegSD) {
        this.cgmGirlsHeadcfFirstdNegSD = cgmGirlsHeadcfFirstdNegSD;
    }

    public String getCgmGirlsHeadcfSeconddNegSD() {
        return cgmGirlsHeadcfSeconddNegSD;
    }

    public void setCgmGirlsHeadcfSeconddNegSD(String cgmGirlsHeadcfSeconddNegSD) {
        this.cgmGirlsHeadcfSeconddNegSD = cgmGirlsHeadcfSeconddNegSD;
    }


}
