package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "tblweightforage")
public class tblweightforage {
    @DatabaseField
    private int Xaxis;
    @DatabaseField
    private String cgmBoysWtMedian;
    @DatabaseField
    private String cgmBoysWtFirstdNegSD;
    @DatabaseField
    private String cgmBoysWtSeconddNegSD;
    @DatabaseField
    private String cgmGirlsWtMedian;
    @DatabaseField
    private String cgmGirlsWtFirstdNegSD;
    @DatabaseField
    private String cgmGirlsWtSeconddNegSD;

    public int getXaxis() {
        return Xaxis;
    }

    public void setXaxis(int xaxis) {
        Xaxis = xaxis;
    }

    public String getCgmBoysWtMedian() {
        return cgmBoysWtMedian;
    }

    public void setCgmBoysWtMedian(String cgmBoysWtMedian) {
        this.cgmBoysWtMedian = cgmBoysWtMedian;
    }

    public String getCgmBoysWtFirstdNegSD() {
        return cgmBoysWtFirstdNegSD;
    }

    public void setCgmBoysWtFirstdNegSD(String cgmBoysWtFirstdNegSD) {
        this.cgmBoysWtFirstdNegSD = cgmBoysWtFirstdNegSD;
    }

    public String getCgmBoysWtSeconddNegSD() {
        return cgmBoysWtSeconddNegSD;
    }

    public void setCgmBoysWtSeconddNegSD(String cgmBoysWtSeconddNegSD) {
        this.cgmBoysWtSeconddNegSD = cgmBoysWtSeconddNegSD;
    }

    public String getCgmGirlsWtMedian() {
        return cgmGirlsWtMedian;
    }

    public void setCgmGirlsWtMedian(String cgmGirlsWtMedian) {
        this.cgmGirlsWtMedian = cgmGirlsWtMedian;
    }

    public String getCgmGirlsWtFirstdNegSD() {
        return cgmGirlsWtFirstdNegSD;
    }

    public void setCgmGirlsWtFirstdNegSD(String cgmGirlsWtFirstdNegSD) {
        this.cgmGirlsWtFirstdNegSD = cgmGirlsWtFirstdNegSD;
    }

    public String getCgmGirlsWtSeconddNegSD() {
        return cgmGirlsWtSeconddNegSD;
    }

    public void setCgmGirlsWtSeconddNegSD(String cgmGirlsWtSeconddNegSD) {
        this.cgmGirlsWtSeconddNegSD = cgmGirlsWtSeconddNegSD;
    }


}
