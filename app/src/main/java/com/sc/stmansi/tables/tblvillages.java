package com.sc.stmansi.tables;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName ="tblvillages")
public class tblvillages {

    @DatabaseField
    int VillageId;
    @DatabaseField
    String UserId;

    public int getVillageId() {
        return VillageId;
    }

    public void setVillageId(int villageId) {
        this.VillageId = villageId;
    }

    public String getVillageName() {
        return VillageName;
    }

    public void setVillageName(String villageName) {
        this.VillageName = villageName;
    }
    @DatabaseField
    String VillageName;
}
