package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "tblAncTests")
public class TblAncTests {

    @DatabaseField
    private String UserId;
    @DatabaseField
    private String WomenId;
    @DatabaseField
    private int ServiceNumber;
    @DatabaseField
    private int TestId;
    @DatabaseField
    private String TestDone;
    @DatabaseField
    private String TestValue;
    @DatabaseField
    private int transId;
    @DatabaseField
    private String RecordCreatedDate;
    //21May2021 Bindu
    @DatabaseField
    private String RecordUpdatedDate;

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getWomenId() {
        return WomenId;
    }

    public void setWomenId(String womenId) {
        WomenId = womenId;
    }

    public int getServiceNumber() {
        return ServiceNumber;
    }

    public void setServiceNumber(int serviceNumber) {
        ServiceNumber = serviceNumber;
    }

    public int getTestId() {
        return TestId;
    }

    public void setTestId(int testId) {
        TestId = testId;
    }

    public String getTestDone() {
        return TestDone;
    }

    public void setTestDone(String testDone) {
        TestDone = testDone;
    }

    public String getTestValue() {
        return TestValue;
    }

    public void setTestValue(String testValue) {
        TestValue = testValue;
    }

    public int getTransId() {
        return transId;
    }

    public void setTransId(int transId) {
        this.transId = transId;
    }

    public String getRecordCreatedDate() {
        return RecordCreatedDate;
    }

    public void setRecordCreatedDate(String recordCreatedDate) {
        RecordCreatedDate = recordCreatedDate;
    }

    public String getRecordUpdatedDate() {
        return RecordUpdatedDate;
    }

    public void setRecordUpdatedDate(String recordUpdatedDate) {
        RecordUpdatedDate = recordUpdatedDate;
    }
}
