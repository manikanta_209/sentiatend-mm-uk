package com.sc.stmansi.tables;

public class ChildImmunization {

    String serviceType;
    String actualDateOfAction;
    String dateTimeOfBirth;
    int daysDiffStart;
    int daysDiffEnd;

    String chlId;
    String chlName;
    String chlReg;
    String chlDateofBirth;
    String chlComplications;
    String chlGender;
    String WomanId;
    int ServiceId;
    String chlDeactDate;
    String weight;


    //11Feb2021 - Mani - add parent id and userid
    String chlParentId;
    String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }



    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getActualDateOfAction() {
        return actualDateOfAction;
    }

    public void setActualDateOfAction(String actualDateOfAction) {
        this.actualDateOfAction = actualDateOfAction;
    }

    public String getDateTimeOfBirth() {
        return dateTimeOfBirth;
    }

    public void setDateTimeOfBirth(String dateTimeOfBirth) {
        this.dateTimeOfBirth = dateTimeOfBirth;
    }

    public int getDaysDiffStart() {
        return daysDiffStart;
    }

    public void setDaysDiffStart(int daysDiffStart) {
        this.daysDiffStart = daysDiffStart;
    }

    public int getDaysDiffEnd() {
        return daysDiffEnd;
    }

    public void setDaysDiffEnd(int daysDiffEnd) {
        this.daysDiffEnd = daysDiffEnd;
    }

    public String getChlId() {
        return chlId;
    }

    public void setChlId(String chlId) {
        this.chlId = chlId;
    }

    public String getChlName() {
        return chlName;
    }

    public void setChlName(String chlName) {
        this.chlName = chlName;
    }

    public String getChlReg() {
        return chlReg;
    }

    public void setChlReg(String chlReg) {
        this.chlReg = chlReg;
    }

    public String getChlDateofBirth() {
        return chlDateofBirth;
    }

    public void setChlDateofBirth(String chlDateofBirth) {
        this.chlDateofBirth = chlDateofBirth;
    }

    public String getChlComplications() {
        return chlComplications;
    }

    public void setChlComplications(String chlComplications) {
        this.chlComplications = chlComplications;
    }

    public String getChlGender() {
        return chlGender;
    }

    public void setChlGender(String chlGender) {
        this.chlGender = chlGender;
    }

    public String getWomanId() {
        return WomanId;
    }

    public void setWomanId(String womanId) {
        WomanId = womanId;
    }

    public int getServiceId() {
        return ServiceId;
    }

    public void setServiceId(int serviceId) {
        ServiceId = serviceId;
    }

    public String getChlDeactDate() {
        return chlDeactDate;
    }

    public void setChlDeactDate(String chlDeactDate) {
        this.chlDeactDate = chlDeactDate;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getChlParentId() {
        return chlParentId;
    }

    public void setChlParentId(String chlParentId) {
        this.chlParentId = chlParentId;
    }

}

