package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;

public class tblVersionDetails {
    @DatabaseField
    String UserId;
    @DatabaseField
    int preAppVersion;
    @DatabaseField
    int curAppVersion;
    @DatabaseField
    int preDBVersion;
    @DatabaseField
    int curDBVersion;
    @DatabaseField
    int transId;
    @DatabaseField
    String RecordCreatedDate;
    @DatabaseField
    String RecordUpdatedDate;

    public int getTransId() {
        return transId;
    }

    public void setTransId(int transId) {
        this.transId = transId;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public int getPreAppVersion() {
        return preAppVersion;
    }

    public void setPreAppVersion(int preAppVersion) {
        this.preAppVersion = preAppVersion;
    }

    public int getCurAppVersion() {
        return curAppVersion;
    }

    public void setCurAppVersion(int curAppVersion) {
        this.curAppVersion = curAppVersion;
    }

    public int getPreDBVersion() {
        return preDBVersion;
    }

    public void setPreDBVersion(int preDBVersion) {
        this.preDBVersion = preDBVersion;
    }

    public int getCurDBVersion() {
        return curDBVersion;
    }

    public void setCurDBVersion(int curDBVersion) {
        this.curDBVersion = curDBVersion;
    }

    public String getRecordCreatedDate() {
        return RecordCreatedDate;
    }

    public void setRecordCreatedDate(String recordCreatedDate) {
        RecordCreatedDate = recordCreatedDate;
    }

    public String getRecordUpdatedDate() {
        return RecordUpdatedDate;
    }

    public void setRecordUpdatedDate(String recordUpdatedDate) {
        RecordUpdatedDate = recordUpdatedDate;
    }
}
