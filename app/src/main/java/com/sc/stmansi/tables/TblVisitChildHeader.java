package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

@DatabaseTable(tableName ="tblVisitChildHeader")
public class TblVisitChildHeader implements Serializable {

    @DatabaseField(id = true)
    Integer AutoId;
    @DatabaseField private String  UserId;
    @DatabaseField private String  WomanId;
    @DatabaseField private String  ChildId;
    @DatabaseField private int  ChildNo;
    @DatabaseField private int  VisitNum;
    @DatabaseField private String  visHVisitDate;
    @DatabaseField private String  visCHWeight;
    @DatabaseField private String  visCHWeightDate;
    @DatabaseField private String  visCHTemperature;
    @DatabaseField private String  visCHTemperatureDate;
    @DatabaseField private String  visCHResprate;
    @DatabaseField private String  visCHResprateDate;
    @DatabaseField private String  visHVisitIsReferred;
    @DatabaseField private String  visHVisitReferredToFacilityType;
    @DatabaseField private String  visHVisitReferredToFacilityName;
    @DatabaseField private String  visHVisitComments;
    @DatabaseField private String  visCHCompl;
    @DatabaseField private String  visHChildSymptoms;
    @DatabaseField private String  visCHAdvise;
    @DatabaseField private boolean  visCHRegAtGovtFac;
    @DatabaseField private String  visCHRegAtGovtFacDate;
    @DatabaseField private String  visCHRegGovtFacId;
    @DatabaseField private String  visHUserType;
    @DatabaseField private int  transid;
    @DatabaseField private String  Recordcreateddate;
    @DatabaseField private String  Recordupdateddate;
    //08Apr2021 Bindu add ref slip num
    @DatabaseField private String  visHReferralSlipNumber;

    //mani 14Aug2021 added for tablet details
    @DatabaseField private String  visCHAshaAvailable;
    @DatabaseField private String childvisHIsIFATaken;
    @DatabaseField private String childvisHIFADaily;
    @DatabaseField private String childvisHIFATabletsCount;
    @DatabaseField private String childvisHIFANotTakenReason;
    @DatabaseField private int childvisHIFAGivenBy;
    @DatabaseField private String childvisHDewormingtab;

    public String getChildvisHIsIFATaken() {
        return childvisHIsIFATaken;
    }

    public void setChildvisHIsIFATaken(String childvisHIsIFATaken) {
        this.childvisHIsIFATaken = childvisHIsIFATaken;
    }

    public String getChildvisHIFADaily() {
        return childvisHIFADaily;
    }

    public void setChildvisHIFADaily(String childvisHIFADaily) {
        this.childvisHIFADaily = childvisHIFADaily;
    }

    public String getChildvisHIFATabletsCount() {
        return childvisHIFATabletsCount;
    }

    public void setChildvisHIFATabletsCount(String childvisHIFATabletsCount) {
        this.childvisHIFATabletsCount = childvisHIFATabletsCount;
    }

    public String getChildvisHIFANotTakenReason() {
        return childvisHIFANotTakenReason;
    }

    public void setChildvisHIFANotTakenReason(String childvisHIFANotTakenReason) {
        this.childvisHIFANotTakenReason = childvisHIFANotTakenReason;
    }

    public int getChildvisHIFAGivenBy() {
        return childvisHIFAGivenBy;
    }

    public void setChildvisHIFAGivenBy(int childvisHIFAGivenBy) {
        this.childvisHIFAGivenBy = childvisHIFAGivenBy;
    }

    public String getChildvisHDewormingtab() {
        return childvisHDewormingtab;
    }

    public void setChildvisHDewormingtab(String childvisHDewormingtab) {
        this.childvisHDewormingtab = childvisHDewormingtab;
    }


    public String getVisCHAshaAvailable() {
        return visCHAshaAvailable;
    }

    public void setVisCHAshaAvailable(String visCHAshaAvailable) {
        this.visCHAshaAvailable = visCHAshaAvailable;
    }

    //mani 30Aug2021

    public String getVisHReferralSlipNumber() {
        return visHReferralSlipNumber;
    }

    public void setVisHReferralSlipNumber(String visHReferralSlipNumber) {
        this.visHReferralSlipNumber = visHReferralSlipNumber;
    }

    public Integer getAutoId() {
        return AutoId;
    }

    public void setAutoId(Integer autoId) {
        AutoId = autoId;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getWomanId() {
        return WomanId;
    }

    public void setWomanId(String womanId) {
        WomanId = womanId;
    }

    public String getChildId() {
        return ChildId;
    }

    public void setChildId(String childId) {
        ChildId = childId;
    }

    public int getVisitNum() {
        return VisitNum;
    }

    public void setVisitNum(int visitNum) {
        VisitNum = visitNum;
    }

    public String getVisHVisitDate() {
        return visHVisitDate;
    }

    public void setVisHVisitDate(String visHVisitDate) {
        this.visHVisitDate = visHVisitDate;
    }

    public String getVisCHWeight() {
        return visCHWeight;
    }

    public void setVisCHWeight(String visCHWeight) {
        this.visCHWeight = visCHWeight;
    }

    public String getVisCHWeightDate() {
        return visCHWeightDate;
    }

    public void setVisCHWeightDate(String visCHWeightDate) {
        this.visCHWeightDate = visCHWeightDate;
    }

    public String getVisCHTemperature() {
        return visCHTemperature;
    }

    public void setVisCHTemperature(String visCHTemperature) {
        this.visCHTemperature = visCHTemperature;
    }

    public String getVisCHTemperatureDate() {
        return visCHTemperatureDate;
    }

    public void setVisCHTemperatureDate(String visCHTemperatureDate) {
        this.visCHTemperatureDate = visCHTemperatureDate;
    }

    public String getVisCHResprate() {
        return visCHResprate;
    }

    public void setVisCHResprate(String visCHResprate) {
        this.visCHResprate = visCHResprate;
    }

    public String getVisCHResprateDate() {
        return visCHResprateDate;
    }

    public void setVisCHResprateDate(String visCHResprateDate) {
        this.visCHResprateDate = visCHResprateDate;
    }

    public String getVisHVisitIsReferred() {
        return visHVisitIsReferred;
    }

    public void setVisHVisitIsReferred(String visHVisitIsReferred) {
        this.visHVisitIsReferred = visHVisitIsReferred;
    }

    public String getVisHVisitReferredToFacilityType() {
        return visHVisitReferredToFacilityType;
    }

    public void setVisHVisitReferredToFacilityType(String visHVisitReferredToFacilityType) {
        this.visHVisitReferredToFacilityType = visHVisitReferredToFacilityType;
    }

    public String getVisHVisitReferredToFacilityName() {
        return visHVisitReferredToFacilityName;
    }

    public void setVisHVisitReferredToFacilityName(String visHVisitReferredToFacilityName) {
        this.visHVisitReferredToFacilityName = visHVisitReferredToFacilityName;
    }

    public String getVisHVisitComments() {
        return visHVisitComments;
    }

    public void setVisHVisitComments(String visHVisitComments) {
        this.visHVisitComments = visHVisitComments;
    }

    public String getVisCHCompl() {
        return visCHCompl;
    }

    public void setVisCHCompl(String visCHCompl) {
        this.visCHCompl = visCHCompl;
    }

    public String getVisHChildSymptoms() {
        return visHChildSymptoms;
    }

    public void setVisHChildSymptoms(String visHChildSymptoms) {
        this.visHChildSymptoms = visHChildSymptoms;
    }

    public String getVisCHAdvise() {
        return visCHAdvise;
    }

    public void setVisCHAdvise(String visCHAdvise) {
        this.visCHAdvise = visCHAdvise;
    }

    public boolean isVisCHRegAtGovtFac() {
        return visCHRegAtGovtFac;
    }

    public void setVisCHRegAtGovtFac(boolean visCHRegAtGovtFac) {
        this.visCHRegAtGovtFac = visCHRegAtGovtFac;
    }

    public String getVisCHRegAtGovtFacDate() {
        return visCHRegAtGovtFacDate;
    }

    public void setVisCHRegAtGovtFacDate(String visCHRegAtGovtFacDate) {
        this.visCHRegAtGovtFacDate = visCHRegAtGovtFacDate;
    }

    public String getVisCHRegGovtFacId() {
        return visCHRegGovtFacId;
    }

    public void setVisCHRegGovtFacId(String visCHRegGovtFacId) {
        this.visCHRegGovtFacId = visCHRegGovtFacId;
    }

    public String getVisHUserType() {
        return visHUserType;
    }

    public void setVisHUserType(String visHUserType) {
        this.visHUserType = visHUserType;
    }

    public int getTransid() {
        return transid;
    }

    public void setTransid(int transid) {
        this.transid = transid;
    }

    public String getRecordcreateddate() {
        return Recordcreateddate;
    }

    public void setRecordcreateddate(String recordcreateddate) {
        Recordcreateddate = recordcreateddate;
    }

    public String getRecordupdateddate() {
        return Recordupdateddate;
    }

    public void setRecordupdateddate(String recordupdateddate) {
        Recordupdateddate = recordupdateddate;
    }

    public int getChildNo() {
        return ChildNo;
    }

    public void setChildNo(int childNo) {
        ChildNo = childNo;
    }
}
