package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "tbldiagnosismaster")
public class TblDiagnosisMaster {
    @DatabaseField
    private int SlNo ;
    @DatabaseField
    private int symptomgroupid ;
    @DatabaseField
    private String diagnosisId ;
    @DatabaseField
    private String diagnosisName ;
    @DatabaseField
    private String diagnosisXmlStringName ;

    public int getSlNo() {
        return SlNo;
    }

    public void setSlNo(int slNo) {
        SlNo = slNo;
    }

    public int getSymptomgroupid() {
        return symptomgroupid;
    }

    public void setSymptomgroupid(int symptomgroupid) {
        this.symptomgroupid = symptomgroupid;
    }

    public String getDiagnosisId() {
        return diagnosisId;
    }

    public void setDiagnosisId(String diagnosisId) {
        this.diagnosisId = diagnosisId;
    }

    public String getDiagnosisName() {
        return diagnosisName;
    }

    public void setDiagnosisName(String diagnosisName) {
        this.diagnosisName = diagnosisName;
    }

    public String getDiagnosisXmlStringName() {
        return diagnosisXmlStringName;
    }

    public void setDiagnosisXmlStringName(String diagnosisXmlStringName) {
        this.diagnosisXmlStringName = diagnosisXmlStringName;
    }
}
