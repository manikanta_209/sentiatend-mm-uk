package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;

public class tblHRPComplicationMgmt {
    @DatabaseField(id = true)
    Integer AutoId;
    @DatabaseField private String  UserId;
    @DatabaseField
     private String  WomanId;
  //  @DatabaseField private String  ChildId;
    @DatabaseField private String  VisitType;
    @DatabaseField private int  VisitNum;
    @DatabaseField private String  VisitDate;
    @DatabaseField private String  VisitWomanDangerSigns;
    @DatabaseField private String  VisitChildDangerSigns;
    @DatabaseField private String  hcmActTakByUserType;
    @DatabaseField private String  hcmActTakByUserName;
    @DatabaseField private String  hcmActTakAtFacility;
    @DatabaseField private String  hcmActTakFacilityName;
    @DatabaseField private String  hcmActTakDate;
    @DatabaseField private String  hcmActTakTime;
    @DatabaseField private String  hcmActTakForCompl;
    @DatabaseField private String  hcmActTakForHRP;
    @DatabaseField private String  hcmActTakStatus;
    @DatabaseField private String  hcmActTakStatusDate;
    @DatabaseField private String  hcmActTakStatusTime;
    @DatabaseField private String  hcmActTakReferredToFacility;
    @DatabaseField private String  hcmActTakReferredToFacilityName;
    @DatabaseField private String  hcmActTakComments;
    @DatabaseField private String  hcmActionDateTimeClient;
    @DatabaseField private String  hcmActionToBeTakenClient;
    @DatabaseField private String  hcmCommentsClient;
    @DatabaseField private String  hcmClientReceivedDate;
    @DatabaseField private String  hcmClientSeenDate;
    @DatabaseField private String  ServerCreatedDate;
    @DatabaseField private String  ServerUpdatedDate;

    @DatabaseField private String  UserType;
    @DatabaseField private int  transId;

    public Integer getAutoId() {
        return AutoId;
    }

    public void setAutoId(Integer autoId) {
        AutoId = autoId;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getWomanId() {
        return WomanId;
    }

    public void setWomanId(String womanId) {
        WomanId = womanId;
    }

    public String getVisitType() {
        return VisitType;
    }

    public void setVisitType(String visitType) {
        VisitType = visitType;
    }

    public int getVisitNum() {
        return VisitNum;
    }

    public void setVisitNum(int visitNum) {
        VisitNum = visitNum;
    }

    public String getVisitDate() {
        return VisitDate;
    }

    public void setVisitDate(String visitDate) {
        VisitDate = visitDate;
    }

    public String getVisitWomanDangerSigns() {
        return VisitWomanDangerSigns;
    }

    public void setVisitWomanDangerSigns(String visitWomanDangerSigns) {
        VisitWomanDangerSigns = visitWomanDangerSigns;
    }

    public String getVisitChildDangerSigns() {
        return VisitChildDangerSigns;
    }

    public void setVisitChildDangerSigns(String visitChildDangerSigns) {
        VisitChildDangerSigns = visitChildDangerSigns;
    }

    public String getHcmActTakByUserType() {
        return hcmActTakByUserType;
    }

    public void setHcmActTakByUserType(String hcmActTakByUserType) {
        this.hcmActTakByUserType = hcmActTakByUserType;
    }

    public String getHcmActTakByUserName() {
        return hcmActTakByUserName;
    }

    public void setHcmActTakByUserName(String hcmActTakByUserName) {
        this.hcmActTakByUserName = hcmActTakByUserName;
    }

    public String getHcmActTakAtFacility() {
        return hcmActTakAtFacility;
    }

    public void setHcmActTakAtFacility(String hcmActTakAtFacility) {
        this.hcmActTakAtFacility = hcmActTakAtFacility;
    }

    public String getHcmActTakFacilityName() {
        return hcmActTakFacilityName;
    }

    public void setHcmActTakFacilityName(String hcmActTakFacilityName) {
        this.hcmActTakFacilityName = hcmActTakFacilityName;
    }

    public String getHcmActTakDate() {
        return hcmActTakDate;
    }

    public void setHcmActTakDate(String hcmActTakDate) {
        this.hcmActTakDate = hcmActTakDate;
    }

    public String getHcmActTakTime() {
        return hcmActTakTime;
    }

    public void setHcmActTakTime(String hcmActTakTime) {
        this.hcmActTakTime = hcmActTakTime;
    }

    public String getHcmActTakForCompl() {
        return hcmActTakForCompl;
    }

    public void setHcmActTakForCompl(String hcmActTakForCompl) {
        this.hcmActTakForCompl = hcmActTakForCompl;
    }

    public String getHcmActTakForHRP() {
        return hcmActTakForHRP;
    }

    public void setHcmActTakForHRP(String hcmActTakForHRP) {
        this.hcmActTakForHRP = hcmActTakForHRP;
    }

    public String getHcmActTakStatus() {
        return hcmActTakStatus;
    }

    public void setHcmActTakStatus(String hcmActTakStatus) {
        this.hcmActTakStatus = hcmActTakStatus;
    }

    public String getHcmActTakStatusDate() {
        return hcmActTakStatusDate;
    }

    public void setHcmActTakStatusDate(String hcmActTakStatusDate) {
        this.hcmActTakStatusDate = hcmActTakStatusDate;
    }

    public String getHcmActTakStatusTime() {
        return hcmActTakStatusTime;
    }

    public void setHcmActTakStatusTime(String hcmActTakStatusTime) {
        this.hcmActTakStatusTime = hcmActTakStatusTime;
    }

    public String getHcmActTakReferredToFacility() {
        return hcmActTakReferredToFacility;
    }

    public void setHcmActTakReferredToFacility(String hcmActTakReferredToFacility) {
        this.hcmActTakReferredToFacility = hcmActTakReferredToFacility;
    }

    public String getHcmActTakReferredToFacilityName() {
        return hcmActTakReferredToFacilityName;
    }

    public void setHcmActTakReferredToFacilityName(String hcmActTakReferredToFacilityName) {
        this.hcmActTakReferredToFacilityName = hcmActTakReferredToFacilityName;
    }

    public String getHcmActTakComments() {
        return hcmActTakComments;
    }

    public void setHcmActTakComments(String hcmActTakComments) {
        this.hcmActTakComments = hcmActTakComments;
    }

    public String getHcmActionDateTimeClient() {
        return hcmActionDateTimeClient;
    }

    public void setHcmActionDateTimeClient(String hcmActionDateTimeClient) {
        this.hcmActionDateTimeClient = hcmActionDateTimeClient;
    }

    public String getHcmActionToBeTakenClient() {
        return hcmActionToBeTakenClient;
    }

    public void setHcmActionToBeTakenClient(String hcmActionToBeTakenClient) {
        this.hcmActionToBeTakenClient = hcmActionToBeTakenClient;
    }

    public String getHcmCommentsClient() {
        return hcmCommentsClient;
    }

    public void setHcmCommentsClient(String hcmCommentsClient) {
        this.hcmCommentsClient = hcmCommentsClient;
    }

    public String getHcmClientReceivedDate() {
        return hcmClientReceivedDate;
    }

    public void setHcmClientReceivedDate(String hcmClientReceivedDate) {
        this.hcmClientReceivedDate = hcmClientReceivedDate;
    }

    public String getHcmClientSeenDate() {
        return hcmClientSeenDate;
    }

    public void setHcmClientSeenDate(String hcmClientSeenDate) {
        this.hcmClientSeenDate = hcmClientSeenDate;
    }

    public String getServerCreatedDate() {
        return ServerCreatedDate;
    }

    public void setServerCreatedDate(String serverCreatedDate) {
        ServerCreatedDate = serverCreatedDate;
    }

    public String getServerUpdatedDate() {
        return ServerUpdatedDate;
    }

    public void setServerUpdatedDate(String serverUpdatedDate) {
        ServerUpdatedDate = serverUpdatedDate;
    }

    public String getUserType() {
        return UserType;
    }

    public void setUserType(String userType) {
        UserType = userType;
    }

    public int getTranid() {
        return transId;
    }

    public void setTranid(int tranid) {
        this.transId = tranid;
    }

}
