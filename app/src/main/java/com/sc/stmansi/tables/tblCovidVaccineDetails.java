package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "tblCovidVaccineDetails")
public class tblCovidVaccineDetails {

    @DatabaseField
    private String UserId;
    @DatabaseField
    private String BeneficiaryId;
    @DatabaseField
    String CovidVaccinatedNo;
    @DatabaseField
    String CovidVaccinatedDate;
    /*@DatabaseField
    String CovidFirstDoseDate;
    @DatabaseField
    String CovidSecondDoseDate;*/
    @DatabaseField
    private int transId;
    @DatabaseField
    private String RecordCreatedDate;
    @DatabaseField
    private String RecordUpdatedDate;
    @DatabaseField
    private String BeneficiaryType; //20May2021 Bindu change womenid and add other cols - multiple rows


    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }



   /* public String getCovidVaccinated() {
        return CovidVaccinated;
    }

    public void setCovidVaccinated(String covidVaccinated) {
        CovidVaccinated = covidVaccinated;
    }

    public String getCovidFirstDoseDate() {
        return CovidFirstDoseDate;
    }

    public void setCovidFirstDoseDate(String covidFirstDoseDate) {
        CovidFirstDoseDate = covidFirstDoseDate;
    }

    public String getCovidSecondDoseDate() {
        return CovidSecondDoseDate;
    }

    public void setCovidSecondDoseDate(String covidSecondDoseDate) {
        CovidSecondDoseDate = covidSecondDoseDate;
    }*/

    public int getTransId() {
        return transId;
    }

    public void setTransId(int transId) {
        this.transId = transId;
    }

    public String getRecordCreatedDate() {
        return RecordCreatedDate;
    }

    public void setRecordCreatedDate(String recordCreatedDate) {
        RecordCreatedDate = recordCreatedDate;
    }

    public String getRecordUpdatedDate() {
        return RecordUpdatedDate;
    }

    public void setRecordUpdatedDate(String recordUpdatedDate) {
        RecordUpdatedDate = recordUpdatedDate;
    }

    public String getBeneficiaryId() {
        return BeneficiaryId;
    }

    public void setBeneficiaryId(String beneficiaryId) {
        BeneficiaryId = beneficiaryId;
    }

    public String getBeneficiaryType() {
        return BeneficiaryType;
    }

    public void setBeneficiaryType(String beneficiaryType) {
        BeneficiaryType = beneficiaryType;
    }

    public String getCovidVaccinatedNo() {
        return CovidVaccinatedNo;
    }

    public void setCovidVaccinatedNo(String covidVaccinatedNo) {
        CovidVaccinatedNo = covidVaccinatedNo;
    }

    public String getCovidVaccinatedDate() {
        return CovidVaccinatedDate;
    }

    public void setCovidVaccinatedDate(String covidVaccinatedDate) {
        CovidVaccinatedDate = covidVaccinatedDate;
    }
}
