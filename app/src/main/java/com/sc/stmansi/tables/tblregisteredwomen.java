//27Aug2019 Arpitha - code clean up
package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

@DatabaseTable(tableName = "tblregisteredwomen")
public class tblregisteredwomen implements Serializable {
    @DatabaseField
    public String UserId;
    @DatabaseField
    String WomanId;
    @DatabaseField
    private int regPregnantorMother;
    @DatabaseField
    private String regWomenImage;
    @DatabaseField
    private String regUIDNo;
    @DatabaseField
    private String regUIDType;
    @DatabaseField
    private String regUIDDate;
    @DatabaseField
    private int regCountNo;
    @DatabaseField
    private String regWomanName;
    @DatabaseField
    private String regNickName;
    @DatabaseField
    private int regRegistrationType;
    @DatabaseField
    private String regRegistrationDate;
    @DatabaseField
    private String regFinancialYear;
    @DatabaseField
    private String regPlaceOfReg;
    @DatabaseField
    private int regpregormotheratreg;
    @DatabaseField
    private String regHusbandName;
    @DatabaseField
    private String regDateofBirth;
    @DatabaseField
    private int regAge;
    @DatabaseField
    private String regCaste;
    @DatabaseField
    private String regAPLBPL;
    @DatabaseField
    private String regReligion;
    @DatabaseField
    private String regWhoseMobileNo;
    @DatabaseField
    private String regPhoneNumber;
    @DatabaseField
    private int regLMPNotKnown;
    @DatabaseField
    private String regLMP;
    @DatabaseField
    private String regEDD;
    @DatabaseField
    private String regStatusWhileRegistration;
    @DatabaseField
    private String regADDate;
    @DatabaseField
    private String regHusbandAge;
    @DatabaseField
    private String regHusAgeatMarriage;
    @DatabaseField
    private String regWifeAgeatMarriage;
    @DatabaseField
    private String regMarriageDate;
    @DatabaseField
    private String regEducationHus;
    @DatabaseField
    private String regEducationWife;
    @DatabaseField
    private int regheightUnit;
    @DatabaseField
    private String regHeight;
    @DatabaseField
    private String regWomanWeight;
    @DatabaseField
    private int regBloodGroup;
    @DatabaseField
    private int regGravida;
    @DatabaseField
    private int regPara;
    @DatabaseField
    private int regLiveChildren;
    @DatabaseField
    private int regAbortions;
    @DatabaseField
    private int regChildMortPreg;
    @DatabaseField
    private int regChildMortDel;
    @DatabaseField
    private int regNoofHomeDeliveries;
    @DatabaseField
    private String regLastChildAge;
    @DatabaseField
    private String regLastChildGender;
    @DatabaseField
    private int regLastChildWeight;
    @DatabaseField
    private String regState;
    @DatabaseField
    private String regDistrict;
    @DatabaseField
    private String regSubDistrict;
    @DatabaseField
    private String regFacility;
    @DatabaseField
    private String regFacilityType;
    @DatabaseField
    private int regVillage;
    @DatabaseField
    private String regAreaName;
    @DatabaseField
    private String regAddress;
    @DatabaseField
    private String regPincode;
    @DatabaseField
    private String regGPSLocation;
    @DatabaseField
    private String regGPSPhoto;
    @DatabaseField
    private String regNotinRegionReason;
    @DatabaseField
    private String regAadharCard;
    @DatabaseField
    private String regBankName;
    @DatabaseField
    private String regBranchName;
    @DatabaseField
    private String regAccountNo;
    @DatabaseField
    private String regIFSCCode;
    @DatabaseField
    private String regRationCard;
    @DatabaseField
    private String regVoterId;
    @DatabaseField
    private String regCurrHealthRiskFactors;
    @DatabaseField
    private String regPrevHealthRiskFactors;
    @DatabaseField
    private String regFamilyHistoryRiskFactors;
    @DatabaseField
    private String regOtherhealthissue;
    @DatabaseField
    private String regOtherfamilyhistory;
    @DatabaseField
    private String regOthersprevpreg;
    @DatabaseField
    private int regAmberOrRedColorCode;
    @DatabaseField
    private int regInDanger;
    @DatabaseField
    private int regComplicatedpreg;
    @DatabaseField
    private int regPrematureBirth;
    @DatabaseField
    private String regriskFactors;
    @DatabaseField
    private String regCHriskfactors;
    @DatabaseField
    private String regPHriskfactors;
    @DatabaseField
    private String regrecommendedPlaceOfDelivery;
    @DatabaseField
    private String regComments;
    @DatabaseField
    private int LastServiceListNumber;
    @DatabaseField
    private String CurrentWomenStatus;
    @DatabaseField
    private String DateTransferred;
    @DatabaseField
    private String DateExpired;
    @DatabaseField
    private String DateAborted;
    @DatabaseField
    private String DateDeactivated;
    @DatabaseField
    private String ReasonforDeactivation;
    @DatabaseField
    private int transId;
    @DatabaseField
    private String regUserType;
    @DatabaseField
    private String RecordCreatedDate;
    @DatabaseField
    private String RecordUpdatedDate;
    @DatabaseField(columnName = "IsCompl")
    private String isCompl;
    @DatabaseField(columnName = "LastAncVisitDate")
    private String lastAncVisitDate;
    @DatabaseField(columnName = "LastPncVisitDate")
    private String lastPncVisitDate;
    @DatabaseField(columnName = "IsReferred")
    private String isReferred;
    @DatabaseField
    private String regEligibleCoupleNo;
    @DatabaseField
    private String regCategory;
//   24Nov2019 Arpitha
    @DatabaseField
    private String OtherDeactivationReason;
    @DatabaseField
    private String DeacComments;
    //24Mar2021 Bindu add LMP confirmed
    @DatabaseField
    private int isLMPConfirmed;
    @DatabaseField
    private int regNoofStillBirth;//13May2021 Arpitha

    @DatabaseField
    private String regRiskFactorsByCC;
    @DatabaseField
    private String regOtherRiskByCC;
    @DatabaseField
    private String regCCConfirmRisk;
//    02Sep2021 Arpitha
    @DatabaseField
    private int regPregnancyCount;
    @DatabaseField
    private String regIsAdolescent;
    @DatabaseField
    private String regAdolId;//16Sep2021 Arpitha

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getWomanId() {
        return WomanId;
    }

    public void setWomanId(String womanId) {
        WomanId = womanId;
    }

    public int getRegPregnantorMother() {
        return regPregnantorMother;
    }

    public void setRegPregnantorMother(int regPregnantorMother) {
        this.regPregnantorMother = regPregnantorMother;
    }

    public String getRegWomenImage() {
        return regWomenImage;
    }

    public void setRegWomenImage(String regWomenImage) {
        this.regWomenImage = regWomenImage;
    }

    public String getRegUIDNo() {
        return regUIDNo;
    }

    public void setRegUIDNo(String regUIDNo) {
        this.regUIDNo = regUIDNo;
    }

    public String getRegUIDType() {
        return regUIDType;
    }

    public void setRegUIDType(String regUIDType) {
        this.regUIDType = regUIDType;
    }

    public String getRegUIDDate() {
        return regUIDDate;
    }

    public void setRegUIDDate(String regUIDDate) {
        this.regUIDDate = regUIDDate;
    }

    public int getRegCountNo() {
        return regCountNo;
    }

    public void setRegCountNo(int regCountNo) {
        this.regCountNo = regCountNo;
    }

    public String getRegWomanName() {
        return regWomanName;
    }

    public void setRegWomanName(String regWomanName) {
        this.regWomanName = regWomanName;
    }


    public void setRegRegistrationType(int regRegistrationType) {
        this.regRegistrationType = regRegistrationType;
    }

    public String getRegRegistrationDate() {
        return regRegistrationDate;
    }

    public void setRegRegistrationDate(String regRegistrationDate) {
        this.regRegistrationDate = regRegistrationDate;
    }


    public void setRegFinancialYear(String regFinancialYear) {
        this.regFinancialYear = regFinancialYear;
    }

    public String getRegPlaceOfReg() {
        return regPlaceOfReg;
    }

    public void setRegPlaceOfReg(String regPlaceOfReg) {
        this.regPlaceOfReg = regPlaceOfReg;
    }

    public int getRegpregormotheratreg() {
        return regpregormotheratreg;
    }

    public void setRegpregormotheratreg(int regpregormotheratreg) {
        this.regpregormotheratreg = regpregormotheratreg;
    }

    public String getRegHusbandName() {
        return regHusbandName;
    }

    public void setRegHusbandName(String regHusbandName) {
        this.regHusbandName = regHusbandName;
    }

    public String getRegDateofBirth() {
        return regDateofBirth;
    }

    public void setRegDateofBirth(String regDateofBirth) {
        this.regDateofBirth = regDateofBirth;
    }

    public int getRegAge() {
        return regAge;
    }

    public void setRegAge(int regAge) {
        this.regAge = regAge;
    }

    public String getRegCaste() {
        return regCaste;
    }

    public void setRegCaste(String regCaste) {
        this.regCaste = regCaste;
    }

    public String getRegAPLBPL() {
        return regAPLBPL;
    }

    public void setRegAPLBPL(String regAPLBPL) {
        this.regAPLBPL = regAPLBPL;
    }

    public String getRegReligion() {
        return regReligion;
    }

    public void setRegReligion(String regReligion) {
        this.regReligion = regReligion;
    }

    public String getRegWhoseMobileNo() {
        return regWhoseMobileNo;
    }

    public void setRegWhoseMobileNo(String regWhoseMobileNo) {
        this.regWhoseMobileNo = regWhoseMobileNo;
    }

    public String getRegPhoneNumber() {
        return regPhoneNumber;
    }

    public void setRegPhoneNumber(String regPhoneNumber) {
        this.regPhoneNumber = regPhoneNumber;
    }


    public String getRegLMP() {
        return regLMP;
    }

    public void setRegLMP(String regLMP) {
        this.regLMP = regLMP;
    }

    public String getRegEDD() {
        return regEDD;
    }

    public void setRegEDD(String regEDD) {
        this.regEDD = regEDD;
    }

    public String getRegStatusWhileRegistration() {
        return regStatusWhileRegistration;
    }

    public void setRegStatusWhileRegistration(String regStatusWhileRegistration) {
        this.regStatusWhileRegistration = regStatusWhileRegistration;
    }

    public String getRegADDate() {
        return regADDate;
    }

    public void setRegADDate(String regADDate) {
        this.regADDate = regADDate;
    }

    public String getRegHusbandAge() {
        return regHusbandAge;
    }

    public void setRegHusbandAge(String regHusbandAge) {
        this.regHusbandAge = regHusbandAge;
    }

    public String getRegHusAgeatMarriage() {
        return regHusAgeatMarriage;
    }

    public void setRegHusAgeatMarriage(String regHusAgeatMarriage) {
        this.regHusAgeatMarriage = regHusAgeatMarriage;
    }

    public String getRegWifeAgeatMarriage() {
        return regWifeAgeatMarriage;
    }

    public void setRegWifeAgeatMarriage(String regWifeAgeatMarriage) {
        this.regWifeAgeatMarriage = regWifeAgeatMarriage;
    }

    public String getRegMarriageDate() {
        return regMarriageDate;
    }

    public void setRegMarriageDate(String regMarriageDate) {
        this.regMarriageDate = regMarriageDate;
    }

    public String getRegEducationHus() {
        return regEducationHus;
    }

    public void setRegEducationHus(String regEducationHus) {
        this.regEducationHus = regEducationHus;
    }

    public String getRegEducationWife() {
        return regEducationWife;
    }

    public void setRegEducationWife(String regEducationWife) {
        this.regEducationWife = regEducationWife;
    }

    public int getRegheightUnit() {
        return regheightUnit;
    }

    public void setRegheightUnit(int regheightUnit) {
        this.regheightUnit = regheightUnit;
    }

    public String getRegHeight() {
        return regHeight;
    }

    public void setRegHeight(String regHeight) {
        this.regHeight = regHeight;
    }

    public String getRegWomanWeight() {
        return regWomanWeight;
    }

    public void setRegWomanWeight(String regWomanWeight) {
        this.regWomanWeight = regWomanWeight;
    }

    public int getRegBloodGroup() {
        return regBloodGroup;
    }

    public void setRegBloodGroup(int regBloodGroup) {
        this.regBloodGroup = regBloodGroup;
    }

    public int getRegGravida() {
        return regGravida;
    }

    public void setRegGravida(int regGravida) {
        this.regGravida = regGravida;
    }

    public int getRegPara() {
        return regPara;
    }

    public void setRegPara(int regPara) {
        this.regPara = regPara;
    }

    public int getRegLiveChildren() {
        return regLiveChildren;
    }

    public void setRegLiveChildren(int regLiveChildren) {
        this.regLiveChildren = regLiveChildren;
    }

    public int getRegAbortions() {
        return regAbortions;
    }

    public void setRegAbortions(int regAbortions) {
        this.regAbortions = regAbortions;
    }

    public int getRegChildMortPreg() {
        return regChildMortPreg;
    }

    public void setRegChildMortPreg(int regChildMortPreg) {
        this.regChildMortPreg = regChildMortPreg;
    }

    public int getRegChildMortDel() {
        return regChildMortDel;
    }

    public void setRegChildMortDel(int regChildMortDel) {
        this.regChildMortDel = regChildMortDel;
    }

    public int getRegNoofHomeDeliveries() {
        return regNoofHomeDeliveries;
    }

    public void setRegNoofHomeDeliveries(int regNoofHomeDeliveries) {
        this.regNoofHomeDeliveries = regNoofHomeDeliveries;
    }

    public String getRegLastChildAge() {
        return regLastChildAge;
    }

    public void setRegLastChildAge(String regLastChildAge) {
        this.regLastChildAge = regLastChildAge;
    }

    public String getRegLastChildGender() {
        return regLastChildGender;
    }

    public void setRegLastChildGender(String regLastChildGender) {
        this.regLastChildGender = regLastChildGender;
    }

    public int getRegLastChildWeight() {
        return regLastChildWeight;
    }

    public void setRegLastChildWeight(int regLastChildWeight) {
        this.regLastChildWeight = regLastChildWeight;
    }


    public void setRegState(String regState) {
        this.regState = regState;
    }


    public void setRegDistrict(String regDistrict) {
        this.regDistrict = regDistrict;
    }


    public void setRegSubDistrict(String regSubDistrict) {
        this.regSubDistrict = regSubDistrict;
    }


    public void setRegFacility(String regFacility) {
        this.regFacility = regFacility;
    }


    public void setRegFacilityType(String regFacilityType) {
        this.regFacilityType = regFacilityType;
    }

    public int getRegVillage() {
        return regVillage;
    }

    public void setRegVillage(int regVillage) {
        this.regVillage = regVillage;
    }

    public String getRegAreaName() {
        return regAreaName;
    }

    public void setRegAreaName(String regAreaName) {
        this.regAreaName = regAreaName;
    }

    public String getRegAddress() {
        return regAddress;
    }

    public void setRegAddress(String regAddress) {
        this.regAddress = regAddress;
    }

    public String getRegPincode() {
        return regPincode;
    }

    public void setRegPincode(String regPincode) {
        this.regPincode = regPincode;
    }

    public String getRegNotinRegionReason() {
        return regNotinRegionReason;
    }

    public void setRegNotinRegionReason(String regNotinRegionReason) {
        this.regNotinRegionReason = regNotinRegionReason;
    }

    public String getRegAadharCard() {
        return regAadharCard;
    }

    public void setRegAadharCard(String regAadharCard) {
        this.regAadharCard = regAadharCard;
    }

    public String getRegBankName() {
        return regBankName;
    }

    public void setRegBankName(String regBankName) {
        this.regBankName = regBankName;
    }

    public String getRegBranchName() {
        return regBranchName;
    }

    public void setRegBranchName(String regBranchName) {
        this.regBranchName = regBranchName;
    }

    public String getRegAccountNo() {
        return regAccountNo;
    }

    public void setRegAccountNo(String regAccountNo) {
        this.regAccountNo = regAccountNo;
    }

    public String getRegIFSCCode() {
        return regIFSCCode;
    }

    public void setRegIFSCCode(String regIFSCCode) {
        this.regIFSCCode = regIFSCCode;
    }

    public String getRegRationCard() {
        return regRationCard;
    }

    public void setRegRationCard(String regRationCard) {
        this.regRationCard = regRationCard;
    }

    public String getRegVoterId() {
        return regVoterId;
    }

    public void setRegVoterId(String regVoterId) {
        this.regVoterId = regVoterId;
    }

    public String getRegCurrHealthRiskFactors() {
        return regCurrHealthRiskFactors;
    }

    public void setRegCurrHealthRiskFactors(String regCurrHealthRiskFactors) {
        this.regCurrHealthRiskFactors = regCurrHealthRiskFactors;
    }

    public String getRegPrevHealthRiskFactors() {
        return regPrevHealthRiskFactors;
    }

    public void setRegPrevHealthRiskFactors(String regPrevHealthRiskFactors) {
        this.regPrevHealthRiskFactors = regPrevHealthRiskFactors;
    }

    public String getRegFamilyHistoryRiskFactors() {
        return regFamilyHistoryRiskFactors;
    }

    public void setRegFamilyHistoryRiskFactors(String regFamilyHistoryRiskFactors) {
        this.regFamilyHistoryRiskFactors = regFamilyHistoryRiskFactors;
    }

    public String getRegOtherhealthissue() {
        return regOtherhealthissue;
    }

    public void setRegOtherhealthissue(String regOtherhealthissue) {
        this.regOtherhealthissue = regOtherhealthissue;
    }

    public String getRegOtherfamilyhistory() {
        return regOtherfamilyhistory;
    }

    public void setRegOtherfamilyhistory(String regOtherfamilyhistory) {
        this.regOtherfamilyhistory = regOtherfamilyhistory;
    }

    public String getRegOthersprevpreg() {
        return regOthersprevpreg;
    }

    public void setRegOthersprevpreg(String regOthersprevpreg) {
        this.regOthersprevpreg = regOthersprevpreg;
    }

    public int getRegAmberOrRedColorCode() {
        return regAmberOrRedColorCode;
    }

    public void setRegAmberOrRedColorCode(int regAmberOrRedColorCode) {
        this.regAmberOrRedColorCode = regAmberOrRedColorCode;
    }

    public int getRegComplicatedpreg() {
        return regComplicatedpreg;
    }

    public void setRegComplicatedpreg(int regComplicatedpreg) {
        this.regComplicatedpreg = regComplicatedpreg;
    }

    public void setRegPrematureBirth(int regPrematureBirth) {
        this.regPrematureBirth = regPrematureBirth;
    }

    public String getRegriskFactors() {
        return regriskFactors;
    }

    public void setRegriskFactors(String regriskFactors) {
        this.regriskFactors = regriskFactors;
    }

    public String getRegCHriskfactors() {
        return regCHriskfactors;
    }

    public void setRegCHriskfactors(String regCHriskfactors) {
        this.regCHriskfactors = regCHriskfactors;
    }

    public String getRegPHriskfactors() {
        return regPHriskfactors;
    }

    public void setRegPHriskfactors(String regPHriskfactors) {
        this.regPHriskfactors = regPHriskfactors;
    }

    public String getRegrecommendedPlaceOfDelivery() {
        return regrecommendedPlaceOfDelivery;
    }

    public void setRegrecommendedPlaceOfDelivery(String regrecommendedPlaceOfDelivery) {
        this.regrecommendedPlaceOfDelivery = regrecommendedPlaceOfDelivery;
    }

    public String getRegComments() {
        return regComments;
    }

    public void setRegComments(String regComments) {
        this.regComments = regComments;
    }


    public String getCurrentWomenStatus() {
        return CurrentWomenStatus;
    }


    public int getTransId() {
        return transId;
    }

    public void setTransId(int transId) {
        this.transId = transId;
    }

    public String getRegUserType() {
        return regUserType;
    }

    public void setRegUserType(String regUserType) {
        this.regUserType = regUserType;
    }

    public void setRecordCreatedDate(String recordCreatedDate) {
        RecordCreatedDate = recordCreatedDate;
    }

    public void setRecordUpdatedDate(String recordUpdatedDate) {
        RecordUpdatedDate = recordUpdatedDate;
    }

    public String getRecordUpdatedDate() {
        return RecordUpdatedDate;
    }

    public String getIsCompl() {
        return isCompl;
    }

    public void setIsCompl(String isCompl) {
        this.isCompl = isCompl;
    }

    public String getLastAncVisitDate() {
        return lastAncVisitDate;
    }

    public void setLastAncVisitDate(String lastAncVisitDate) {
        this.lastAncVisitDate = lastAncVisitDate;
    }

    public String getLastPncVisitDate() {
        return lastPncVisitDate;
    }

    public void setLastPncVisitDate(String lastPncVisitDate) {
        this.lastPncVisitDate = lastPncVisitDate;
    }

    public String getIsReferred() {
        return isReferred;
    }

    public void setIsReferred(String isReferred) {
        this.isReferred = isReferred;
    }

    public String getRegEligibleCoupleNo() {
        return regEligibleCoupleNo;
    }

    public void setRegEligibleCoupleNo(String regEligibleCoupleNo) {
        this.regEligibleCoupleNo = regEligibleCoupleNo;
    }

    public String getRegCategory() {
        return regCategory;
    }

    public void setRegCategory(String regCategory) {
        this.regCategory = regCategory;
    }


    public String getOtherDeactivationReason() {
        return OtherDeactivationReason;
    }

    public void setOtherDeactivationReason(String otherDeactivationReason) {
        OtherDeactivationReason = otherDeactivationReason;
    }

    public String getDeacComments() {
        return DeacComments;
    }

    public void setDeacComments(String deacComments) {
        DeacComments = deacComments;
    }

    public String getDateDeactivated() {
        return DateDeactivated;
    }

    public void setDateDeactivated(String dateDeactivated) {
        DateDeactivated = dateDeactivated;
    }

    public String getReasonforDeactivation() {
        return ReasonforDeactivation;
    }

    public void setReasonforDeactivation(String reasonforDeactivation) {
        ReasonforDeactivation = reasonforDeactivation;
    }

    public String getRegNickName() {
        return regNickName;
    }

    public void setRegNickName(String regNickName) {
        this.regNickName = regNickName;
    }

    public String getDateTransferred() {
        return DateTransferred;
    }

    public void setDateTransferred(String dateTransferred) {
        DateTransferred = dateTransferred;
    }

    public String getDateExpired() {
        return DateExpired;
    }

    public void setDateExpired(String dateExpired) {
        DateExpired = dateExpired;
    }

    public String getDateAborted() {
        return DateAborted;
    }

    public void setDateAborted(String dateAborted) {
        DateAborted = dateAborted;
    }

    public int getRegInDanger() {
        return regInDanger;
    }

    public void setRegInDanger(int regInDanger) {
        this.regInDanger = regInDanger;
    }

    public void setCurrentWomenStatus(String currentWomenStatus) {
        CurrentWomenStatus = currentWomenStatus;
    }

    public int getIsLMPConfirmed() {
        return isLMPConfirmed;
    }

    public void setIsLMPConfirmed(int isLMPConfirmed) {
        this.isLMPConfirmed = isLMPConfirmed;
    }

    public int getRegNoofStillBirth() {
        return regNoofStillBirth;
    }

    public void setRegNoofStillBirth(int regNoofStillBirth) {
        this.regNoofStillBirth = regNoofStillBirth;
    }

    public String getRegRiskFactorsByCC() {
        return regRiskFactorsByCC;
    }

    public void setRegRiskFactorsByCC(String regRiskFactorsByCC) {
        this.regRiskFactorsByCC = regRiskFactorsByCC;
    }

    public String getRegOtherRiskByCC() {
        return regOtherRiskByCC;
    }

    public void setRegOtherRiskByCC(String regOtherRiskByCC) {
        this.regOtherRiskByCC = regOtherRiskByCC;
    }

    public String getRegCCConfirmRisk() {
        return regCCConfirmRisk;
    }

    public void setRegCCConfirmRisk(String regCCConfirmRisk) {
        this.regCCConfirmRisk = regCCConfirmRisk;
    }

    public int getRegPregnancyCount() {
        return regPregnancyCount;
    }

    public void setRegPregnancyCount(int regPregnancyCount) {
        this.regPregnancyCount = regPregnancyCount;
    }

    public String getRegIsAdolescent() {
        return regIsAdolescent;
    }

    public void setRegIsAdolescent(String regIsAdolescent) {
        this.regIsAdolescent = regIsAdolescent;
    }

    public String getRegAdolId() {
        return regAdolId;
    }

    public void setRegAdolId(String regAdolId) {
        this.regAdolId = regAdolId;
    }
}



