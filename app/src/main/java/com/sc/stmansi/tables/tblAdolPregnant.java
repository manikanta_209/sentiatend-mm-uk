package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;

public class tblAdolPregnant implements Serializable {
    @DatabaseField(id = true)
    Integer autoId;
    @DatabaseField
    String UserId;
    @DatabaseField
    String AdolId;
    @DatabaseField
    String VisitId;
    @DatabaseField
    String pregnantCount;
    @DatabaseField
    int tblAdolPreOutcome;
    @DatabaseField
    String tblAdolPreDoD;
    @DatabaseField
    int tblAdolPreDeliverWhere;
    @DatabaseField
    int tblAdolPreWhoConductDelivery;
    @DatabaseField
    int tblAdolPreTransport;
    @DatabaseField
    int tblAdolPreDeliveryType;
    @DatabaseField
    int tblAdolPreDeliveryStatus;
    @DatabaseField
    int tblAdolPreChildSex;
    @DatabaseField
    String tblAdolPreChildAge;
    @DatabaseField
    int transId;
    @DatabaseField
    String RecordCreatedDate;

    public Integer getAutoId() {
        return autoId;
    }

    public void setAutoId(Integer autoId) {
        this.autoId = autoId;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getAdolId() {
        return AdolId;
    }

    public void setAdolId(String adolId) {
        AdolId = adolId;
    }

    public String getVisitId() {
        return VisitId;
    }

    public void setVisitId(String visitId) {
        VisitId = visitId;
    }

    public String getPregnantCount() {
        return pregnantCount;
    }

    public void setPregnantCount(String pregnantCount) {
        this.pregnantCount = pregnantCount;
    }

    public int getTblAdolPreOutcome() {
        return tblAdolPreOutcome;
    }

    public void setTblAdolPreOutcome(int tblAdolPreOutcome) {
        this.tblAdolPreOutcome = tblAdolPreOutcome;
    }

    public String getTblAdolPreDoD() {
        return tblAdolPreDoD;
    }

    public void setTblAdolPreDoD(String tblAdolPreDoD) {
        this.tblAdolPreDoD = tblAdolPreDoD;
    }

    public int getTblAdolPreDeliverWhere() {
        return tblAdolPreDeliverWhere;
    }

    public void setTblAdolPreDeliverWhere(int tblAdolPreDeliverWhere) {
        this.tblAdolPreDeliverWhere = tblAdolPreDeliverWhere;
    }

    public int getTblAdolPreWhoConductDelivery() {
        return tblAdolPreWhoConductDelivery;
    }

    public void setTblAdolPreWhoConductDelivery(int tblAdolPreWhoConductDelivery) {
        this.tblAdolPreWhoConductDelivery = tblAdolPreWhoConductDelivery;
    }

    public int getTblAdolPreTransport() {
        return tblAdolPreTransport;
    }

    public void setTblAdolPreTransport(int tblAdolPreTransport) {
        this.tblAdolPreTransport = tblAdolPreTransport;
    }

    public int getTblAdolPreDeliveryType() {
        return tblAdolPreDeliveryType;
    }

    public void setTblAdolPreDeliveryType(int tblAdolPreDeliveryType) {
        this.tblAdolPreDeliveryType = tblAdolPreDeliveryType;
    }

    public int getTblAdolPreDeliveryStatus() {
        return tblAdolPreDeliveryStatus;
    }

    public void setTblAdolPreDeliveryStatus(int tblAdolPreDeliveryStatus) {
        this.tblAdolPreDeliveryStatus = tblAdolPreDeliveryStatus;
    }

    public int getTblAdolPreChildSex() {
        return tblAdolPreChildSex;
    }

    public void setTblAdolPreChildSex(int tblAdolPreChildSex) {
        this.tblAdolPreChildSex = tblAdolPreChildSex;
    }

    public String getTblAdolPreChildAge() {
        return tblAdolPreChildAge;
    }

    public void setTblAdolPreChildAge(String tblAdolPreChildAge) {
        this.tblAdolPreChildAge = tblAdolPreChildAge;
    }

    public int getTransId() {
        return transId;
    }

    public void setTransId(int transId) {
        this.transId = transId;
    }

    public String getRecordCreatedDate() {
        return RecordCreatedDate;
    }

    public void setRecordCreatedDate(String recordCreatedDate) {
        RecordCreatedDate = recordCreatedDate;
    }
}
