//27Aug2019 Arpitha - code clean up
package com.sc.stmansi.tables;

import com.j256.ormlite.field.DatabaseField;

public class TblInstitutionDetails {

    @DatabaseField
    int InstituteId;
    @DatabaseField
    String InstituteName;
    @DatabaseField
    String ContactName;
    @DatabaseField
    String EmailId;
    @DatabaseField
    String PhoneNumber;
    @DatabaseField
    String Country;
    @DatabaseField
    String State;
    @DatabaseField
    String District;
    @DatabaseField
    String Subdistrict;
    @DatabaseField
    String Address;
    @DatabaseField
    String Pincode;
    @DatabaseField
    String Comments;
    @DatabaseField
    String Website;
    @DatabaseField
    String TypeofInstitution;
    @DatabaseField
    String Programstartdate;
    @DatabaseField
    String Programenddate;
    @DatabaseField
    String Databasename;
    @DatabaseField
    String ipAddress;
    @DatabaseField
    String LastGeneratedUserId;
    @DatabaseField
    int isDeactivated;
    @DatabaseField
    String DeactivatedReason;
    @DatabaseField
    String DeactivatedDate;
    @DatabaseField
    String datecreated;
    @DatabaseField
    String createdby;
    @DatabaseField
    String dateupdated;
    @DatabaseField
    String updatedby;

    public String getEvalStartDate() {
        return EvalStartDate;
    }

    public void setEvalStartDate(String evalStartDate) {
        EvalStartDate = evalStartDate;
    }

    public String getEvalEndDate() {
        return EvalEndDate;
    }

    public void setEvalEndDate(String evalEndDate) {
        EvalEndDate = evalEndDate;
    }

    //Mani 04Feb2021
    @DatabaseField
    String EvalStartDate;
    @DatabaseField
    String EvalEndDate;

    public String getIsEvalPeriod() {
        return IsEvalPeriod;
    }

    public void setIsEvalPeriod(String isEvalPeriod) {
        IsEvalPeriod = isEvalPeriod;
    }
    //Mani 04Feb2021
    @DatabaseField
    String IsEvalPeriod;


    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getDatabasename() {
        return Databasename;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public int getIsDeactivated() {
        return isDeactivated;
    }

    public void setIsDeactivated(int isDeactivated) {
        this.isDeactivated = isDeactivated;
    }
}
