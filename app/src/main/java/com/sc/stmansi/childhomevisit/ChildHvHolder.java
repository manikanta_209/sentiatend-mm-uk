package com.sc.stmansi.childhomevisit;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sc.stmansi.R;

public class ChildHvHolder extends RecyclerView.ViewHolder{

    public TextView visitdate,visitnumber,txtReferredToFacname,txtvisitmode;
    public ImageView imgWomanStatusHv, imgVisitOutcome ;

    public ChildHvHolder(@NonNull View itemView) {
        super(itemView);
        visitdate=itemView.findViewById(R.id.chlhvadaptervisitdate);
        visitnumber=itemView.findViewById(R.id.chlhvadaptervisitnumber);
        imgWomanStatusHv = itemView.findViewById(R.id.imgWstatus);
        imgVisitOutcome = itemView.findViewById(R.id.imgVisitOutcome);
        txtReferredToFacname = itemView.findViewById(R.id.txtreferredtofacname);
        txtvisitmode = itemView.findViewById(R.id.txtvisitmode); //13Aug2021 Bindu
    }
}
