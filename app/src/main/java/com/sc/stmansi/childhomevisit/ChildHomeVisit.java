package com.sc.stmansi.childhomevisit;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.core.content.FileProvider;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.legacy.app.ActionBarDrawerToggle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.sc.stmansi.About.AboutActivity;
import com.sc.stmansi.HomeVisit.HomeVisitListActivity;
import com.sc.stmansi.HomeVisit.PNCPojoNb;
import com.sc.stmansi.HomeVisit.PncHomeVisit;
import com.sc.stmansi.HomeVisit.VisitDetailsPojo;
import com.sc.stmansi.ImageStore.FileUtils;
import com.sc.stmansi.ImageStore.ImagePojo;
import com.sc.stmansi.ImageStore.RecyclerViewAdapter;
import com.sc.stmansi.R;
import com.sc.stmansi.childgrowthmonitor.CGMList;
import com.sc.stmansi.childgrowthmonitor.CGMNewVisit;
import com.sc.stmansi.childregistration.AddSiblingsActivity;
import com.sc.stmansi.childregistration.EditChildActivity;
import com.sc.stmansi.childregistration.EditRegChildInfoActivity;
import com.sc.stmansi.childregistration.ViewParentDetails;
import com.sc.stmansi.common.AlertDialogUtil;
import com.sc.stmansi.common.CommonDatePickerFragment;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.common.MultiSelectionSpinner;
import com.sc.stmansi.common.NavDrawerItem;
import com.sc.stmansi.common.NavDrawerListAdapter;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.deactivate.ChildDeactivateActivity;
import com.sc.stmansi.immunization.ImmunizationListActivity;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.repositories.FacilityRepository;
import com.sc.stmansi.repositories.HomeVisitRepository;
import com.sc.stmansi.repositories.ImageStoreRepository;
import com.sc.stmansi.repositories.LoginRepository;
import com.sc.stmansi.repositories.RefFacilityRepository;
import com.sc.stmansi.repositories.SMSRepository;
import com.sc.stmansi.repositories.SettingsRepository;
import com.sc.stmansi.repositories.TransactionHeaderRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.sms.SendSMS;
import com.sc.stmansi.tables.MessageLogPojo;
import com.sc.stmansi.tables.TblChildInfo;
import com.sc.stmansi.tables.TblContactDetails;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.TblVisitChildHeader;
import com.sc.stmansi.tables.tblCaseMgmt;
import com.sc.stmansi.tables.tblChildVisitDetails;
import com.sc.stmansi.tables.tblImageStore;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.viewprofile.ViewProfileActivity;
import com.sc.stmansi.womanlist.RegisteredWomenActionTabs;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ChildHomeVisit extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener, View.OnFocusChangeListener, DatePickerDialog.OnDateSetListener {

    private static final int REQUEST_IMAGE_CAPTURE = 2;
    AQuery aq;
    TblChildInfo tblChildInfo;
    int transId, visitNum = 0;
    TblInstusers user;
    TransactionHeaderRepository transactionHeaderRepository;
    List<String> arrLNbSymptoms, arrLNbIds;
    Map<String, VisitDetailsPojo> nbdangersignsMap;
    LinkedHashMap<String, VisitDetailsPojo> insertchildMap = null;
    String strchildNo;
    tblChildVisitDetails tblCvisD;
    EditText etphn;
    boolean isMessageLogsaved;
    boolean messageSent;
    TblVisitChildHeader tblvisCH;
    LinkedHashMap<String, VisitDetailsPojo> insertchildHeaderMap = null;
    boolean isVisitDate = false;
    private AppState appState;
    private DatabaseHelper databaseHelper;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private tblCaseMgmt tblcaseMgmt;
    private ArrayList<String> reffacilitynamelist = new ArrayList<String>();
    private ArrayList<String> reffacilitytypelist = new ArrayList<String>();
    private Map<String, String> mapRefFacilityType;
    private Map<String, String> mapRefFacilityname;
    private RecyclerView recyclerViewImage;
    private String mCurrentPhotoPath;
    private ArrayList<tblImageStore> AllImagesBeforeSave = new ArrayList<>();
    private ArrayList<ImagePojo> AllImagePojo = new ArrayList<>();
    private Dao<tblImageStore, Integer> tblImageStoreDao;
    private boolean isBtnSummaryClicked = false;
    private List<PNCPojoNb> pncNbs;
    private EditText currentEditTextViewforDate;
    private int cemocCnt = 0, bemocCnt = 0;
    private Dao<tblChildVisitDetails, Integer> tblChildVisDetailDao;
    private List<PNCPojoNb> pncNbssave;
    private ImageView currentImageView;
    private Dao<TblVisitChildHeader, Integer> tblVisChildHeaderDao;
    private RadioGroup  rgIFA, rgConsumeIFA;
    public static MultiSelectionSpinner spnIFAReasons;
    // Returns SpannableStringBuilder for a given String
    public static SpannableStringBuilder getSSC(String msg) {
        SpannableStringBuilder SS;
        SS = new SpannableStringBuilder(msg);
        return SS;
    }

    private static Bitmap rotateImageIfRequired(Bitmap img, Uri selectedImage) throws IOException {

        ExifInterface ei = new ExifInterface(selectedImage.getPath());
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return rotateImage(img, 90);
        }
    }

    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_childhomevisit);

        try {


            aq = new AQuery(this);
            //Get data from intent
            Bundle bundle = getIntent().getBundleExtra("globalState");
            tblChildInfo = (TblChildInfo) getIntent().getSerializableExtra("tblChildInfo");
            appState = bundle.getParcelable("appState");
            databaseHelper = getHelper();

            UserRepository userRepository = new UserRepository(databaseHelper);
            user = userRepository.getOneAuditedUser(appState.ashaId);

            initiateDrawer();
            initializeScreen(aq.id(R.id.rlhomevisit).getView());
            aq.id(R.id.btnnewborndangersigns).getImageView()
                    .setBackgroundColor(getResources().getColor(R.color.gray));
            initialValues();

            rgConsumeIFA.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    TableRow trTabletsCount = findViewById(R.id.trAdolVisitIFANooftabletsperDay);
                    TableRow trReason = findViewById(R.id.trIFAReasons);
                    if (i == R.id.rd_ConsumingIFAYes) {
                        trTabletsCount.setVisibility(View.VISIBLE);
                        trReason.setVisibility(View.GONE);
                        spnIFAReasons.setItems(getResources().getStringArray(R.array.ifareasons));
                    } else if (i == R.id.rd_ConsumingIFANo) {
                        trTabletsCount.setVisibility(View.GONE);
                        aq.id(R.id.etIFANoofTabletsConsumes).text("");
                        trReason.setVisibility(View.VISIBLE);
                    }
                }
            });

            rgIFA.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    if (i == R.id.rd_IFAYes) {
                        aq.id(R.id.ll_AV_IFA).getView().setVisibility(View.VISIBLE);
                        spnIFAReasons.setItems(getResources().getStringArray(R.array.ifareasons));
                    } else if (i == R.id.rd_IFANo) {
                        rgConsumeIFA.clearCheck();
                        aq.id(R.id.rd_ConsumingIFANo).checked(false);
                        aq.id(R.id.rd_ConsumingIFAYes).checked(false);
                        aq.id(R.id.etIFANoofTabletsConsumes).text("");
                        spnIFAReasons.setItems(getResources().getStringArray(R.array.ifareasons));
                        aq.id(R.id.spnWhoGaveIFATablets).getSpinner().setSelection(0);
                        aq.id(R.id.ll_AV_IFA).getView().setVisibility(View.GONE);
                        aq.id(R.id.trAdolVisitIFANooftabletsperDay).getView().setVisibility(View.GONE);
                        aq.id(R.id.trIFAReasons).getView().setVisibility(View.GONE);
                    }
                }
            });

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private void initialValues() throws Exception {
        strchildNo = "" + tblChildInfo.getChildNo();
        HomeVisitRepository homeRepo = new HomeVisitRepository(databaseHelper);
        visitNum = homeRepo.getLastHomeVisitNoChild(appState.sessionUserId, tblChildInfo.getChildID());

        aq.id(R.id.txtheading).text(getResources().getString(R.string.chlhv) + " - " + " # " + visitNum);
        aq.id(R.id.etvisitdate).text(DateTimeUtil.getTodaysDate());

        rgIFA = findViewById(R.id.rgAdolVisitIFA);
        rgConsumeIFA = findViewById(R.id.rg_consumingIFA);
        spnIFAReasons = findViewById(R.id.spnIFAReasons);


        if (tblChildInfo.getChlReg() == 0 && tblChildInfo.getChlDeliveryResult() != 1) { // thru delivery and del res not eql live
            disableNbDangerSigns();
        }

        // Child deactivated
        if (tblChildInfo.getChlDeactDate() != null && tblChildInfo.getChlDeactDate().length() > 0) {
            disableNbDangerSigns();
        }

        aq.id(R.id.etvisitdate).getEditText().setOnTouchListener(this);
        // baby 1 date features
        aq.id(R.id.etweightdatebaby1).getEditText().setOnTouchListener(this);
        aq.id(R.id.ettempdonedatebaby1).getEditText().setOnTouchListener(this);
        aq.id(R.id.etrespratedonedatebaby1).getEditText().setOnTouchListener(this);
        aq.id(R.id.etchildregdate1).getEditText().setOnTouchListener(this);

        setBabyDetailsTextChangeListener(aq.id(R.id.etweightbaby1).getEditText(), aq.id(R.id.etTempbaby1).getEditText(), aq.id(R.id.etrespratebaby1).getEditText(), aq.id(R.id.etchildregid1).getEditText());

        // Facility name on select listener
        aq.id(R.id.spnfacnameHv).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    String selected = adapterView.getItemAtPosition(i).toString();
                    if (selected.equalsIgnoreCase(getResources().getString(R.string.other))) {
                        aq.id(R.id.tretfacname).visible();
                    } else {
                        aq.id(R.id.tretfacname).gone();
                        aq.id(R.id.etreffacilityname).text("");
                    }
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                    crashlytics.log(e.getMessage());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        //08Apr2021 Bindu add ref fac type listener
        aq.id(R.id.spnfactype).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    String selected = adapterView.getItemAtPosition(i).toString();
                    if (selected.equalsIgnoreCase(getResources().getString(R.string.selectfacilityname))) {
                        aq.id(R.id.trspnfacname).gone();
                        aq.id(R.id.spnfacname).setSelection(0);
                    } else {
                        aq.id(R.id.trspnfacname).visible();
                        setRefFacilityName(selected);
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }

    // Set baby text change listner
    private void setBabyDetailsTextChangeListener(EditText wt, EditText temp, EditText rrate, EditText chregid) {
        aq.id(wt).getEditText().addTextChangedListener(watcher);
        aq.id(temp).getEditText().addTextChangedListener(watcher);
        aq.id(rrate).getEditText().addTextChangedListener(watcher);
        aq.id(chregid).getEditText().addTextChangedListener(watcher);
    }

    // Hide New born danger signs
    private void disableNbDangerSigns() {
        aq.id(R.id.btnnewborndangersigns).gone();
        aq.id(R.id.scrnewborndangersigns).gone();
        aq.id(R.id.btnSummary).gone();
        aq.id(R.id.scrhomevisitsummary).gone();
        aq.id(R.id.imgSummaryAddPhoto).gone();
        aq.id(R.id.txtselectedbtnheading).gone();
        aq.id(R.id.txtmortality).visible();
        aq.id(R.id.txtmortality).text(getResources().getString(R.string.servicenotapplicable));
        aq.id(R.id.txtdetailsnote).gone();
        aq.id(R.id.llsave).gone();
        aq.id(R.id.llhmdate).gone();
    }



    //    initialize db object
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    private void showreferaldetails() throws Exception {
        if (aq.id(R.id.chkisreferred).isChecked()) {
            aq.id(R.id.trspnfacname).visible();
            aq.id(R.id.tretfacname).gone();
            aq.id(R.id.trspnfactype).visible();
            aq.id(R.id.trrefslipno).visible();
            setFacilityType();
        } else {
            aq.id(R.id.spnfacnameHv).setSelection(0);
            aq.id(R.id.trspnfacname).gone();
            aq.id(R.id.tretfacname).gone();
            aq.id(R.id.etreffacilityname).text("");
            aq.id(R.id.spnfactype).setSelection(0);
            aq.id(R.id.trspnfactype).gone();
            aq.id(R.id.trrefslipno).gone();
            aq.id(R.id.etrefslipnumber).text("");
        }
    }

    //08Apr2021 Bindu set fac type and fac name - referral table
    private void setFacilityType() throws Exception {
        reffacilitytypelist = new ArrayList<>();
        RefFacilityRepository facilityRepo = new RefFacilityRepository(databaseHelper);
        mapRefFacilityType = facilityRepo.getRefFacilityType();
        reffacilitytypelist.add(getResources().getString(R.string.selectfacilityname));
        for (Map.Entry<String, String> village : mapRefFacilityType.entrySet())
            reffacilitytypelist.add(village.getValue());
        ArrayAdapter<String> FacTypeAdapter = new ArrayAdapter<String>(ChildHomeVisit.this,
                R.layout.simple_spinner_dropdown_item, reffacilitytypelist);
        FacTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        aq.id(R.id.spnfactype).adapter(FacTypeAdapter);
    }

    private void setRefFacilityName(String factype) throws Exception {
        factype = aq.id(R.id.spnfactype).getSelectedItem().toString();
        reffacilitynamelist = new ArrayList<>();
        RefFacilityRepository facilityRepo = new RefFacilityRepository(databaseHelper);
        mapRefFacilityname = facilityRepo.getRefFacilityName(factype);
        reffacilitynamelist.add(getResources().getString(R.string.selectfacilityname));
        for (Map.Entry<String, String> village : mapRefFacilityname.entrySet())
            reffacilitynamelist.add(village.getValue());
        ArrayAdapter<String> FacTypeAdapter = new ArrayAdapter<String>(ChildHomeVisit.this,
                R.layout.simple_spinner_dropdown_item, reffacilitynamelist);
        FacTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        aq.id(R.id.spnfacnameHv).adapter(FacTypeAdapter);
    }




    @Override
    public void onFocusChange(View v, boolean hasFocus) {

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (MotionEvent.ACTION_UP == event.getAction()) {
            try {
                switch (v.getId()) {
                    case R.id.etvisitdate: {
                        isVisitDate = true;
                        assignEditTextAndCallDatePicker(aq.id(R.id.etvisitdate).getEditText(), aq.id(R.id.imgcleardate).getImageView());
                        break;
                    }
                    case R.id.etbpdonedate: {
                        isVisitDate = false;
                        assignEditTextAndCallDatePicker(aq.id(R.id.etbpdonedate).getEditText(), aq.id(R.id.imgcleardatebp).getImageView());
                        break;
                    }
                    case R.id.etpulsedate: {
                        isVisitDate = false;
                        assignEditTextAndCallDatePicker(aq.id(R.id.etpulsedate).getEditText(), aq.id(R.id.imgcleardatepulse).getImageView());
                        break;
                    }
                    case R.id.ethbdonedate: {
                        isVisitDate = false;
                        assignEditTextAndCallDatePicker(aq.id(R.id.ethbdonedate).getEditText(), aq.id(R.id.imgcleardatehb).getImageView());
                        break;
                    }
                    case R.id.ettempdonedate: {
                        isVisitDate = false;
                        assignEditTextAndCallDatePicker(aq.id(R.id.ettempdonedate).getEditText(), aq.id(R.id.imgcleardatetemp).getImageView());
                        break;
                    }
                    //Baby 1
                    case R.id.etweightdatebaby1: {
                        isVisitDate = false;
                        assignEditTextAndCallDatePicker(aq.id(R.id.etweightdatebaby1).getEditText(), aq.id(R.id.imgcleardateweightbaby1).getImageView());
                        break;
                    }
                    case R.id.ettempdonedatebaby1: {
                        isVisitDate = false;
                        assignEditTextAndCallDatePicker(aq.id(R.id.ettempdonedatebaby1).getEditText(), aq.id(R.id.imgcleardatetempbaby1).getImageView());
                        break;
                    }
                    case R.id.etrespratedonedatebaby1: {
                        isVisitDate = false;
                        assignEditTextAndCallDatePicker(aq.id(R.id.etrespratedonedatebaby1).getEditText(), aq.id(R.id.imgcleardaterespratebaby1).getImageView());
                        break;
                    }
                    case R.id.etchildregdate1: {
                        isVisitDate = false;
                        assignEditTextAndCallDatePicker(aq.id(R.id.etchildregdate1).getEditText(), aq.id(R.id.imgclearchildregdate1).getImageView());
                        break;
                    }
                    
                }
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
        }
        return true;
    }

    /**
     * Save Confirmation Alert
     *
     * @param goToScreen
     * @param spanText2
     */
    private void displayAlert(final String spanText2, final Intent goToScreen) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        String strMess;
        if (goToScreen != null)
            strMess = getResources().getString(R.string.m121);
        else
            strMess = getResources().getString(R.string.ok);
        alertDialogBuilder.setMessage(spanText2).setCancelable(false).setNegativeButton(
                strMess, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (goToScreen != null) {
                            if (spanText2.contains(getResources().getString(R.string.save))) {
//                                saveData();
                            } else {
                                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                startActivity(goToScreen);
                                if (spanText2.contains("logout")) {
                                    try {
                                        LoginRepository loginRepository = new LoginRepository(databaseHelper);
                                        loginRepository.updateLogoutTime(appState.loginAuditId, appState.sessionUserId);
                                    } catch (Exception e) {
                                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                                        crashlytics.log(e.getMessage());
                                    }
                                }
                            }
                        } else
                            dialog.cancel();
                    }
                });

        if (goToScreen != null) {
            alertDialogBuilder.setPositiveButton(getResources().getString(R.string.m122),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
        }
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }


    private void initiateDrawer() {

        mDrawerLayout = findViewById(R.id.ett_drawer_layout);
        mDrawerList = findViewById(R.id.ett_left_drawer);

        ArrayList<NavDrawerItem> navDrawerItems = new ArrayList<>();
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.editchildinfo), R.drawable.ic_edit_60));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.viewparentdetails), R.drawable.ic_mother));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.gcmregisterlist), R.drawable.child_growth)); // 11May2021 Bindu change icon
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.chlhvhistory), R.drawable.prenatalhomevisit)); // to be changed
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.sp_imm), R.drawable.immunisation));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.addsibling), R.drawable.general_examination_baby));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deactivate), R.drawable.deactivate));

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        mDrawerList.setAdapter(new NavDrawerListAdapter(getApplicationContext(), navDrawerItems));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

// ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer,
                R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu(); // creates call to
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                getSupportActionBar().setDisplayShowTitleEnabled(false);
                getSupportActionBar().setDisplayShowCustomEnabled(false);

                try {
                    if (tblChildInfo != null) {
                        getSupportActionBar().setTitle((tblChildInfo.getChlBabyname() + "\t"));
                    }
                } catch (Exception e) {


                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                    crashlytics.log(e.getMessage());
                }
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                try {
                    invalidateOptionsMenu(); // creates call to
                    getSupportActionBar().setDisplayShowHomeEnabled(false);
                    getSupportActionBar().setDisplayShowTitleEnabled(false);
                    getSupportActionBar().setDisplayShowCustomEnabled(true);
                    getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);

                    aq.id(R.id.ivWomanImg).getImageView().setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            mDrawerLayout.closeDrawer(mDrawerList);
                        }
                    });

                    aq.id(R.id.tvWomanName).text((tblChildInfo.getChlBabyname() + "(" + tblChildInfo.getChlGender() + ")"));
                    if (tblChildInfo.getChlDateTimeOfBirth().trim().length() > 0) {
                        aq.id(R.id.tvInfo1).text(
                                (getResources().getString(R.string.age) + " " +
                                        DateTimeUtil.calculateAge(tblChildInfo.getChlDateTimeOfBirth())));
                    }
                } catch (Exception e) {


                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                    crashlytics.log(e.getMessage());
                }
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    /**
     * This method is to sync draw,erToggle and to replace up caret icon to menu
     * icon in Action Bar
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportActionBar().setTitle((tblChildInfo.getChlBabyname() +
                " (" +
                tblChildInfo.getChlGender() + ")"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer1);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);

        aq.id(R.id.tvWomanName).text((tblChildInfo.getChlBabyname() +
                " (" +
                tblChildInfo.getChlGender() + ")"));
        if (tblChildInfo.getChlDateTimeOfBirth() != null && tblChildInfo.getChlDateTimeOfBirth().length() > 0)
            aq.id(R.id.tvInfo1).text((getResources().getString(R.string.age)
                    + " " + DateTimeUtil.calculateAge(tblChildInfo.getChlDateTimeOfBirth())));

        aq.id(R.id.ivWomanImg).gone();
        getMenuInflater().inflate(R.menu.servicesmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            case R.id.logout: {
                Intent goToScreen = new Intent(ChildHomeVisit.this, LoginActivity.class);
                displayAlert(getResources().getString(R.string.m111), goToScreen);
                return true;
            }
            case R.id.home: {
                Intent home = new Intent(ChildHomeVisit.this, MainMenuActivity.class);
                displayAlert(getResources().getString(R.string.exit), home);
                return true;
            }
            case R.id.wlist: {
                Intent goToScreen = new Intent(ChildHomeVisit.this, RegisteredWomenActionTabs.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }
            case R.id.about: {
                Intent goToScreen = new Intent(ChildHomeVisit.this, AboutActivity.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    /**
     * This is a Recursive method that traverse through the group and subgroup
     * of view Sets text
     *
     * @throws Exception
     */
    private ArrayList<View> initializeScreen(View v) throws Exception {
        if (!(v instanceof ViewGroup)) {
            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);

            // TextView - Assign Labels
            if (v.getClass() == TextView.class) {
                aq.id(v.getId()).text(getSS(aq.id(v.getId()).getText().toString()));
            }
            if (v.getClass() == RadioButton.class || v.getClass() == AppCompatRadioButton.class) {
                aq.id(v.getId()).getButton().setOnClickListener(this);
                aq.id(v.getId()).text(getSS(aq.id(v.getId()).getText().toString()));
            }

            // EditText hint - Assign Labels
            if (v.getClass() == EditText.class) {
                if (aq.id(v.getId()).getEditText().getHint() != null)
                    aq.id(v.getId()).getEditText().setHint(getSS(aq.id(v.getId()).getEditText().getHint() + ""));
            }

            // ImageButton/ImageView - OnClickListener commonClick
            if (v.getClass() == ImageButton.class || v.getClass() == ImageView.class || v.getClass() == AppCompatImageButton.class || v.getClass() == AppCompatImageView.class) {
                aq.id(v.getId()).getImageView().setOnClickListener(this);
            }

            // Checkbox - OnClickListener commonClick
            if (v.getClass() == CheckBox.class || v.getClass() == AppCompatCheckBox.class) {
                aq.id(v.getId()).getCheckBox().setOnClickListener(this);
            }
            return viewArrayList;
        }

        ArrayList<View> result = new ArrayList<View>();
        ViewGroup viewGroup = (ViewGroup) v;
        for (int i = 0; i < viewGroup.getChildCount(); i++) {

            View child = viewGroup.getChildAt(i);

            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);
            viewArrayList.addAll(initializeScreen(child));

            result.addAll(viewArrayList);
        }
        return result;
    }

    /**
     * Returns SpannableStringBuilder for a given String
     */
    public SpannableStringBuilder getSS(String srcText) {
        return getSSC(srcText);
    }

    private void enabledisabledatefieldsBaby(EditText etval, EditText etvaldate, ImageView imgcleardate) {
        if (etval.getText().toString().trim().length() > 0) {
            etvaldate.setEnabled(true);
            etvaldate.setBackgroundResource(R.drawable.editext_none);
        } else {
            etvaldate.setEnabled(false);
            etvaldate.setText("");
            imgcleardate.setVisibility(View.GONE);
            etvaldate.setBackgroundResource(R.drawable.editextdisable_none);
        }
    }

    private void CheckValidationOnExamination(EditText nbtestname) {
        if (nbtestname.getText().toString().length() > 0
                && Double.parseDouble(nbtestname.getText().toString()) <= 0) {
            nbtestname.setError(getResources().getString(R.string.incorrectinput));
        } else {
            nbtestname.setError(null);
        }
    }

    @Override
    public void onClick(View view) {
        try {
            switch (view.getId()) {

                case R.id.etweightdatebaby1: {
                    isVisitDate = false;
                    assignEditTextAndCallDatePicker(aq.id(R.id.etweightdatebaby1).getEditText(), aq.id(R.id.imgcleardateweightbaby1).getImageView());
                    break;
                }
                case R.id.ettempdonedatebaby1: {
                    isVisitDate = false;
                    assignEditTextAndCallDatePicker(aq.id(R.id.ettempdonedatebaby1).getEditText(), aq.id(R.id.imgcleardatetempbaby1).getImageView());
                    break;
                }
                case R.id.etrespratedonedatebaby1: {
                    isVisitDate = false;
                    assignEditTextAndCallDatePicker(aq.id(R.id.etrespratedonedatebaby1).getEditText(), aq.id(R.id.imgcleardaterespratebaby1).getImageView());
                    break;
                }
                case R.id.etchildregdate1: {
                    isVisitDate = false;
                    assignEditTextAndCallDatePicker(aq.id(R.id.etchildregdate1).getEditText(), aq.id(R.id.imgclearchildregdate1).getImageView());
                    break;
                }
                case R.id.imgcleardateweightbaby1: {
                    clearDate(aq.id(R.id.etweightdatebaby1).getEditText(), aq.id(R.id.imgcleardateweightbaby1).getImageView());
                    break;
                }
                case R.id.imgcleardatetempbaby1: {
                    clearDate(aq.id(R.id.ettempdonedatebaby1).getEditText(), aq.id(R.id.imgcleardatetempbaby1).getImageView());
                    break;
                }
                case R.id.imgcleardaterespratebaby1: {
                    clearDate(aq.id(R.id.etrespratedonedatebaby1).getEditText(), aq.id(R.id.imgcleardaterespratebaby1).getImageView());
                    break;
                }
                case R.id.imgclearchildregdate1: {
                    clearDate(aq.id(R.id.etchildregdate1).getEditText(), aq.id(R.id.imgclearchildregdate1).getImageView());
                    break;
                }

                case R.id.chkischildregatfacchild1: {
                    unCheckChildRegDetails(aq.id(R.id.chkischildregatfacchild1).getCheckBox(), aq.id(R.id.trchildregid1).getView(), aq.id(R.id.trchildregdate1).getView(), aq.id(R.id.etchildregid1).getEditText(), aq.id(R.id.etchildregdate1).getEditText(), aq.id(R.id.imgclearchildregdate1).getImageView());
                    break;
                }
                case R.id.chkchlhvisreferred: {
                    showreferaldetails();
                    break;
                }

                case R.id.btnnewborndangersigns: {
                    aq.id(R.id.txtselectedbtnheading).text(getResources().getString(R.string.newborndangersigns));
                    hidetabs();
                    aq.id(R.id.btnnewborndangersigns).getImageView().setBackgroundColor(getResources().getColor(R.color.gray));
                    aq.id(R.id.scrnewborndangersigns).visible();
                    aq.id(R.id.scrhomevisitsummary).gone();
                    aq.id(R.id.imgSummaryAddPhoto).gone();
//                    aq.id(R.id.scrmotherdangersigns).gone();
                    aq.id(R.id.btnsavehomevisit).getImageView().setImageResource(R.drawable.ic_arrow_forward);
                    isBtnSummaryClicked = false;
                    break;
                }
                case R.id.btnSummary: {
                    if (validateChildCheck())
                        displayBtnSummaryClickedAction();


                    break;
                }
                case R.id.btnsavehomevisit: {
                    if (isBtnSummaryClicked) {
                        if (validatePNCHomevisit())
                            confirmAlert();
                    } else {
                        if (validateChildCheck()) // 26Apr2021 Bindu - check child cond check
                            displayBtnSummaryClickedAction();
                    }
                    break;
                }
                //29Nov2019 - Bindu - add view click listener
                case R.id.btnviewhomevisit: {
                    if (visitNum > 1) {
                        Intent homevisitlist = new Intent(ChildHomeVisit.this, ChildHomeVisitHistory.class);
                        homevisitlist.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        homevisitlist.putExtra("appState", getIntent().getBundleExtra("appState"));
                        homevisitlist.putExtra("tblChildInfo", tblChildInfo);
                        displayAlert(getResources().getString(R.string.exit), homevisitlist);
                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.norecords), Toast.LENGTH_SHORT).show();
                    }
                    break;
                }
                case R.id.chkisreferred: {
                    showreferaldetails();
                    break;
                }

                // New born danger signs
                case R.id.chkothersymptomsnb: {
                    if (aq.id(R.id.chkothersymptomsnb).isChecked()) {
                        aq.id(R.id.etothersymptomsnb).visible();
                    } else {
                        aq.id(R.id.etothersymptomsnb).gone();
                        aq.id(R.id.etothersymptomsnb).text("");
                    }
                    break;
                }


                //mani 20Aug2021
                case R.id.chknbsmallbabylbw: {
                    opencloseAccordionCheckbox(aq.id(R.id.chknbsmallbabylbw).getCheckBox());
                    break;
                }
                case R.id.chknbnourinepassed: {
                    opencloseAccordionCheckbox(aq.id(R.id.chknbnourinepassed).getCheckBox());
                    break;
                }

                case R.id.chknbinabilitytosuck: {
                    opencloseAccordionCheckbox(aq.id(R.id.chknbinabilitytosuck).getCheckBox());
                    break;
                }
                case R.id.chknbfastbreathing: {
                    opencloseAccordionCheckbox(aq.id(R.id.chknbfastbreathing).getCheckBox());
                    break;
                }
                case R.id.chknbinabilitytopassurineandstool: {
                    opencloseAccordionCheckbox(aq.id(R.id.chknbinabilitytopassurineandstool).getCheckBox());
                    break;
                }

                case R.id.chknbumbilicalstumpredorpus: {
                    opencloseAccordionCheckbox(aq.id(R.id.chknbumbilicalstumpredorpus).getCheckBox());
                    break;
                }

                case R.id.chknbpustules: {
                    opencloseAccordionCheckbox(aq.id(R.id.chknbpustules).getCheckBox());
                    break;
                }
                case R.id.chknbjaundice: {
                    opencloseAccordionCheckbox(aq.id(R.id.chknbjaundice).getCheckBox());
                    break;
                }
                case R.id.chknbfever: {
                    opencloseAccordionCheckbox(aq.id(R.id.chknbfever).getCheckBox());
                    break;
                }


                case R.id.chknbsunkenfont: {
                    opencloseAccordionCheckbox(aq.id(R.id.chknbsunkenfont).getCheckBox());
                    break;
                }
                case R.id.chknbvomiting: {
                    opencloseAccordionCheckbox(aq.id(R.id.chknbvomiting).getCheckBox());
                    break;
                }
                case R.id.chknbsepsis: {
                    opencloseAccordionCheckbox(aq.id(R.id.chknbsepsis).getCheckBox());
                    break;
                }

                case R.id.chknbdiarrhoea: {
                    opencloseAccordionCheckbox(aq.id(R.id.chknbdiarrhoea).getCheckBox());
                    break;
                }
                case R.id.chknbpneumonia: {
                    opencloseAccordionCheckbox(aq.id(R.id.chknbpneumonia).getCheckBox());
                    break;
                }
                case R.id.chknbbloodinstools: {
                    opencloseAccordionCheckbox(aq.id(R.id.chknbbloodinstools).getCheckBox());
                    break;
                }


                case R.id.chknblethargicbaby: {
                    opencloseAccordionCheckbox(aq.id(R.id.chknblethargicbaby).getCheckBox());
                    break;
                }
                case R.id.chknbfitsbaby: {
                    opencloseAccordionCheckbox(aq.id(R.id.chknbfitsbaby).getCheckBox());
                    break;
                }
                case R.id.chknbeyesareredbaby: {
                    opencloseAccordionCheckbox(aq.id(R.id.chknbeyesareredbaby).getCheckBox());
                    break;
                }
                case R.id.chknbpallorofpalmsbaby: {
                    opencloseAccordionCheckbox(aq.id(R.id.chknbpallorofpalmsbaby).getCheckBox());
                    break;
                }
                case R.id.chknbyellowpalmsbaby: {
                    opencloseAccordionCheckbox(aq.id(R.id.chknbyellowpalmsbaby).getCheckBox());
                    break;
                }
                case R.id.chknbbluepalmsbaby: {
                    opencloseAccordionCheckbox(aq.id(R.id.chknbbluepalmsbaby).getCheckBox());
                    break;
                }

                case R.id.chknbfeelscoldorhotbaby: {
                    opencloseAccordionCheckbox(aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox());
                    uncheckOtherFields(aq.id(R.id.chknbfeelswarmbaby).getCheckBox(),aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox());
                    break;
                }
                case R.id.chknbfeelswarmbaby: {
                    opencloseAccordionCheckbox(aq.id(R.id.chknbfeelswarmbaby).getCheckBox());
                    uncheckOtherFields(aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox(),aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox());
                    break;
                }
                case R.id.chknbfeelstoowarmbaby: {
                    opencloseAccordionCheckbox(aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox());
                    uncheckOtherFields(aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox(),aq.id(R.id.chknbfeelswarmbaby).getCheckBox());
                    break;
                }

                case R.id.chknbbleedingbaby: {
                    opencloseAccordionCheckbox(aq.id(R.id.chknbbleedingbaby).getCheckBox());
                    break;
                }
                case R.id.chknbabdominaldistensionbaby: {
                    opencloseAccordionCheckbox(aq.id(R.id.chknbabdominaldistensionbaby).getCheckBox());
                    break;
                }
                case R.id.chknbanybirthdefectbaby: {
                    opencloseAccordionCheckbox(aq.id(R.id.chknbanybirthdefectbaby).getCheckBox());
                    break;
                }


                case R.id.chknbpreterm: {
                    opencloseAccordionCheckbox(aq.id(R.id.chknbpreterm).getCheckBox());
                    break;
                }
                case R.id.chknbconvulsion: {
                    opencloseAccordionCheckbox(aq.id(R.id.chknbconvulsion).getCheckBox());
                    break;
                }

                case R.id.chknbexcessiveweightloss: {
                    opencloseAccordionCheckbox(aq.id(R.id.chknbexcessiveweightloss).getCheckBox());
                    break;
                }

                case R.id.chknbcyanosis: {
                    opencloseAccordionCheckbox(aq.id(R.id.chknbcyanosis).getCheckBox());
                    break;
                }

                case R.id.chknbrespiratorydistress: {
                    opencloseAccordionCheckbox(aq.id(R.id.chknbrespiratorydistress).getCheckBox());
                    break;
                }

                case R.id.chknbypothermia: {
                    opencloseAccordionCheckbox(aq.id(R.id.chknbypothermia).getCheckBox());
                    break;
                }

                //29-6-2021 Ramesh
                case R.id.lladdphotosid:
                    opencloseAccordion(aq.id(R.id.lladdphotosdetails).getView(), aq.id(R.id.txtaddphotos).getTextView());
                    aq.id(R.id.lladdphotosdetails).backgroundColor(getResources().getColor(R.color.lightgray));
                    if (aq.id(R.id.lladdphotosdetails).getView().getVisibility() == View.VISIBLE) {
                        AsyncImage asyncImage = new AsyncImage();
                        asyncImage.execute();
                    }
                    break;
                case R.id.imgSummaryAddPhoto:
                    if (AllImagesBeforeSave!=null && AllImagesBeforeSave.size()<2){
                        callCamera();
                    }else {
                        AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.maxlimitreached), ChildHomeVisit.this);
                    }
                    break;


            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }



    private void uncheckOtherFields(CheckBox ch1, CheckBox ch2) {
        try {
            if (ch1.isChecked() || ch2.isChecked()) {
                ch1.setChecked(false);
                ch2.setChecked(false);
                ch1.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                ch1.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

                ch2.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                ch2.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    public void opencloseAccordion(View v, TextView txt) {
        try {
            if (v.getVisibility() == View.VISIBLE) {
                v.setVisibility(View.GONE);
                if (txt.getCurrentTextColor() == getResources().getColor(R.color.red))
                    txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.collapse, 0);
                else
                    txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
            } else {
                v.setVisibility(View.VISIBLE);
                if (txt.getCurrentTextColor() == getResources().getColor(R.color.red))
                    txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
                else
                    txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.expand, 0);

                //     txt.setTextColor(getResources().getColor(R.color.red));
            }
        } catch (Exception e) {

        }
    }

    private boolean validatePNCHomevisit() {
        //mani 23Aug2021
        if (!(aq.id(R.id.ashavailableYes).isChecked() || aq.id(R.id.ashavailableNo).isChecked() ||  aq.id(R.id.ashavailableDontKnow).isChecked())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plselectavailabilityasha), this);
            return false;
        }
        if (aq.id(R.id.chkisreferred).isChecked()) {
            //08Apr2021 Bindu add ref fac type and ref slip num validation
            if (aq.id(R.id.spnfactype).getSelectedItemPosition() == 0) {
                AlertDialogUtil.displayAlertMessage(
                        getResources().getString(R.string.plsselreffactype)
                        , this);
                TextView errorText = (TextView) aq.id(R.id.spnfactype).getSpinner().getSelectedView();
                errorText.setError("");
                errorText.setTextColor(Color.RED);
                errorText.setText(getResources().getString(R.string.plsselreffactype));
                errorText.setFocusable(true);
                return false;
            }

            if (aq.id(R.id.spnfacnameHv).getSelectedItemPosition() == 0) {
                AlertDialogUtil.displayAlertMessage(
                        getResources().getString(R.string.plsselreffac)
                        , this);
                TextView errorText = (TextView) aq.id(R.id.spnfacnameHv).getSpinner().getSelectedView();
                errorText.setError("");
                errorText.setTextColor(Color.RED);
                errorText.setText(getResources().getString(R.string.plsselreffac));
                errorText.setFocusable(true);
                return false;
            }

            if (aq.id(R.id.etrefslipnumber).getText().toString().trim().length() <= 0) {
                AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsenterrefslipnum), this);
                aq.id(R.id.etrefslipnumber).getEditText().requestFocus();
                return false;
            }
        }

        //Baby reg date at gov
        if (aq.id(R.id.chkischildregatfacchild1).isChecked()) {
            if (aq.id(R.id.etchildregid1).getText().toString().trim().length() <= 0) {
                AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsenterchildregid), this);
                aq.id(R.id.etchildregid1).getEditText().requestFocus();
                return false;
            } else if (aq.id(R.id.etchildregdate1).getText().toString().trim().length() <= 0) {
                AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsenterchildregdate), this);
                aq.id(R.id.etchildregid1).getEditText().requestFocus();
                aq.id(R.id.etchildregdate1).getEditText().requestFocus();
                return false;
            }
        }

        return true;
    }

    private boolean validateChildCheck() {

        if (aq.id(R.id.chkothersymptomsnb).isChecked()) {
            if (aq.id(R.id.etothersymptomsnb).getText().toString().trim().length() <= 0) {
                AlertDialogUtil.displayAlertMessage(
                        getResources().getString(R.string.plsenterother), this);
                aq.id(R.id.etothersymptomsnb).getEditText().requestFocus();
                aq.id(R.id.txtselectedbtnheading).text(getResources().getString(R.string.newborndangersigns));
                aq.id(R.id.btnnewborndangersigns).getImageView().setBackgroundColor(getResources().getColor(R.color.gray));
                aq.id(R.id.scrhomevisitsummary).gone();
                aq.id(R.id.imgSummaryAddPhoto).gone();
                aq.id(R.id.scrnewborndangersigns).visible();
                return false;
            }
        }
        return true;
    }

    private boolean setBabySelectionChecked(CheckBox chk, CheckBox chkbaby1) {
        if (chk.isChecked()) {
            if (!(chkbaby1.isChecked())) {
                chk.setError(getResources().getString(R.string.plsselectbaby)); //pending
                return false;
            } else
                chk.setError(null);
        } else
            chk.setError(null); //26Apr2021 Bindu set null
        return true;
    }

    // 20Sep2019 - Bindu - Clear Date for specfic Edittext and hide or display brush
    private void clearDate(EditText editText, ImageView imageView) {
        try {
            editText.setText("");
            aq.id(imageView).gone();
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }


    // Text watcher
    TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            try {
                //Baby weight
                if (s == aq.id(R.id.etweightbaby1).getEditable()) {
                    enabledisabledatefieldsBaby(aq.id(R.id.etweightbaby1).getEditText(), aq.id(R.id.etweightdatebaby1).getEditText(), aq.id(R.id.imgcleardateweightbaby1).getImageView());
                    CheckValidationOnExamination(aq.id(R.id.etweightbaby1).getEditText());

                    //24Dec2019 - Bindu - weight validation
                    if (aq.id(R.id.etweightbaby1).getText().toString().length() > 0
                            && Double.parseDouble(aq.id(R.id.etweightbaby1).getText().toString()) <= 999) {
                        aq.id(R.id.etweightbaby1).getEditText()
                                .setError(getResources().getString(R.string.strweight) + aq.id(R.id.etweightbaby1).getText().toString() + getResources().getString(R.string.etlastchildWeight));
                    }

                }

                //Baby temp
                else if (s == aq.id(R.id.etTempbaby1).getEditable()) {
                    enabledisabledatefieldsBaby(aq.id(R.id.etTempbaby1).getEditText(), aq.id(R.id.ettempdonedatebaby1).getEditText(), aq.id(R.id.imgcleardatetempbaby1).getImageView());
                    CheckValidationOnExamination(aq.id(R.id.etTempbaby1).getEditText());
                }

                //Baby Resp rate
                else if (s == aq.id(R.id.etrespratebaby1).getEditable()) {
                    enabledisabledatefieldsBaby(aq.id(R.id.etrespratebaby1).getEditText(), aq.id(R.id.etrespratedonedatebaby1).getEditText(), aq.id(R.id.imgcleardaterespratebaby1).getImageView());
                    CheckValidationOnExamination(aq.id(R.id.etrespratebaby1).getEditText());
                }
                //child reg id
                else if (s == aq.id(R.id.etchildregid1).getEditable()) {
                    enabledisabledatefieldsBaby(aq.id(R.id.etchildregid1).getEditText(), aq.id(R.id.etchildregdate1).getEditText(), aq.id(R.id.imgclearchildregdate1).getImageView());
                    CheckValidationOnExamination(aq.id(R.id.etchildregid1).getEditText());
                }
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            try {

            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
        }
    };


    // Uncheck and Clear Data for Child reg details
    public void unCheckChildRegDetails(CheckBox chk, View trchildregid, View trchildregdate, EditText etchildregid, EditText etchildregdate, ImageView imgclear) {

        if (chk.isChecked()) {
            trchildregid.setVisibility(View.VISIBLE);
            trchildregdate.setVisibility(View.VISIBLE);
        } else {
            trchildregid.setVisibility(View.GONE);
            trchildregdate.setVisibility(View.GONE);
            etchildregid.setText("");
            etchildregdate.setText("");
            imgclear.setVisibility(View.GONE);
        }

    }

    private void callCamera() {//28-6-2021 Ramesh
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            try {
                File photoFile = null;
                photoFile = createImagePath();
                // Continue only if the File was successfully created
                if (photoFile != null) {
                    Uri photoURI = FileProvider.getUriForFile(
                            getApplicationContext(),
                            getApplicationContext()
                                    .getPackageName() + ".provider", photoFile);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private File createImagePath() {
        try {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = "JPEG_" + timeStamp + "_";
            File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            File image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );
            mCurrentPhotoPath = image.getAbsolutePath();
            return image;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    // Hide tabs and set the proper selected tab
    private void hidetabs() {
        aq.id(R.id.btnSummary).getImageView().setBackgroundResource(R.drawable.gray_button);
        aq.id(R.id.btnnewborndangersigns).getImageView().setBackgroundResource(R.drawable.gray_button);
    }

    // Button summary click action
    private void displayBtnSummaryClickedAction() {
        try {
            aq.id(R.id.txtselectedbtnheading).text(getResources().getString(R.string.homevisitsummary));
            hidetabs();
            aq.id(R.id.btnSummary).getImageView().setBackgroundColor(getResources().getColor(R.color.gray));
            aq.id(R.id.scrnewborndangersigns).gone();
            aq.id(R.id.scrhomevisitsummary).visible();
            aq.id(R.id.imgSummaryAddPhoto).visible();
            aq.id(R.id.btnsavehomevisit).getImageView().setImageResource(R.drawable.save);
            isBtnSummaryClicked = true;

            setSymptomsSummary();

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private void setSymptomsSummary() throws Exception {
        cemocCnt = 0;
        bemocCnt = 0;
        setNbSymptoms();


        //New born danger signs
        aq.id(R.id.txtDangSignsNb).text("");
        aq.id(R.id.txtDangSignsNbE).text(""); //05Dec2019 - Bindu
        if (!pncNbs.isEmpty()) {
            aq.id(R.id.llsummsymtoms).visible();
            aq.id(R.id.txtDangSignsNb).visible();
            aq.id(R.id.txtnbdangersignsL).visible();
            StringBuilder s = new StringBuilder();
            for (PNCPojoNb p : pncNbs) {
                s.append(p).append("\n ");
            }

            aq.id(R.id.txtDangSignsNb).getTextView().append(s.toString().trim());
            //05Dec2019 - Bindu
            StringBuilder s1 = new StringBuilder();
            for (PNCPojoNb p1 : pncNbssave) {
                s1.append(p1).append("\n ");
            }
            aq.id(R.id.txtDangSignsNbE).getTextView().append(s1.toString().trim());

        }


        if (cemocCnt > 0 || bemocCnt > 0) {
            aq.id(R.id.txtapprefferalcentre).text(getResources().getString(R.string.refer));
            aq.id(R.id.txtapprefferalcentre).visible();
            startAnimation();
            setFacilityType();
        } else {
            aq.id(R.id.txtapprefferalcentre).text("");
            aq.id(R.id.txtapprefferalcentre).gone();
            aq.id(R.id.chkisreferred).checked(false);
            aq.id(R.id.spnfacnameHv).setSelection(0);
            aq.id(R.id.etfacname).text("");
            aq.id(R.id.trspnfacname).gone();
            aq.id(R.id.tretfacname).gone();

            aq.id(R.id.spnfactype).setSelection(0);
            aq.id(R.id.trspnfactype).gone();
            aq.id(R.id.trrefslipno).gone();
            aq.id(R.id.etrefslipnumber).text("");

            setRefFacilityName("");
        }
        setAdvise();
    }

    private void setAdvise() {
        if (cemocCnt > 0) {
            aq.id(R.id.llsummadvise).visible();
            aq.id(R.id.txtactionsumm).visible();
            aq.id(R.id.llreferralinfo).visible();
            aq.id(R.id.txtactionsumm).text("ACTION -  " + getResources().getString(R.string.refertocemoc));
        } else if (bemocCnt > 0) {
            aq.id(R.id.llsummadvise).visible();
            aq.id(R.id.txtactionsumm).visible();
            aq.id(R.id.llreferralinfo).visible();
            aq.id(R.id.txtactionsumm).text("ACTION -  " + getResources().getString(R.string.refertobemoc));
        } else {
            aq.id(R.id.llsummadvise).gone();
            aq.id(R.id.txtactionsumm).text("");
            aq.id(R.id.txtactionsumm).gone();
            aq.id(R.id.llreferralinfo).gone();

        }
    }

    private void setNbSymptoms() {
        pncNbs = new ArrayList<>(10);
        insertchildMap = new LinkedHashMap<>();


        arrLNbSymptoms = new ArrayList<>();
        arrLNbIds = new ArrayList<>();
        nbdangersignsMap = new HashMap<>();

        pncNbssave = new ArrayList<>(10);
        if (aq.id(R.id.chknbsmallbabylbw).isChecked()) {
            cemocCnt++;
            PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbsmallbabylbw).getText().toString());
            PNCPojoNb psave = new PNCPojoNb(getResources().getString(R.string.nbsmallbabylbwsave));  //05Dec2019 - Bindu - for saving in eng
            insertchildMap.put("301", new VisitDetailsPojo("301", getResources().getString(R.string.nbsmallbabylbw), getResources().getString(R.string.strcemoc), strchildNo)); //11May2021 Bindu - thru out change cemoc to dh/ah - 109 occurences
            pncNbs.add(p);
            pncNbssave.add(psave);
        }

        if (aq.id(R.id.chknbnomeconiumpassed).isChecked()) {
           cemocCnt++;
            PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbnomeconiumpassed).getText().toString());
            insertchildMap.put("302", new VisitDetailsPojo("302", getResources().getString(R.string.nbnomeconiumpassed), getResources().getString(R.string.strcemoc), strchildNo));
            pncNbs.add(p);
        }

        if (aq.id(R.id.chknbnourinepassed).isChecked()) {
            cemocCnt++;
            PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbnourinepassed).getText().toString());
            insertchildMap.put("303", new VisitDetailsPojo("303", getResources().getString(R.string.nbnourinepassed), getResources().getString(R.string.strcemoc), strchildNo));

            pncNbs.add(p);
        }

        //Inability to suck
        if (aq.id(R.id.chknbinabilitytosuck).isChecked()) {
            cemocCnt++;
            PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbinabilitytosuck).getText().toString());
            insertchildMap.put("304", new VisitDetailsPojo("304", getResources().getString(R.string.nbinabilitytosuck), getResources().getString(R.string.strcemoc), strchildNo));
            pncNbs.add(p);
        }

        //fast breathing
        if (aq.id(R.id.chknbfastbreathing).isChecked()) {
            cemocCnt++;
            PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbfastbreathing).getText().toString());
            insertchildMap.put("305", new VisitDetailsPojo("305", getResources().getString(R.string.nbfastbreathing), getResources().getString(R.string.strcemoc), strchildNo));
            pncNbs.add(p);
        }

        //inability to pass urine and stools
        if (aq.id(R.id.chknbinabilitytopassurineandstool).isChecked()) {
            cemocCnt++;
            PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbinabilitytopassurineandstool).getText().toString());
            insertchildMap.put("306", new VisitDetailsPojo("306", getResources().getString(R.string.nbinabilitytopassurineandstool), getResources().getString(R.string.strcemoc), strchildNo));
            pncNbs.add(p);
        }

        //umbilical stump red or pus
        if (aq.id(R.id.chknbumbilicalstumpredorpus).isChecked()) {
            cemocCnt++;
            PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbumbilicalstumpredorpus).getText().toString());
            insertchildMap.put("307", new VisitDetailsPojo("307", getResources().getString(R.string.nbumbilicalstumpredorpus), getResources().getString(R.string.strcemoc), strchildNo));
            pncNbs.add(p);
        }

        //pustules
        if (aq.id(R.id.chknbpustules).isChecked()) {
            cemocCnt++;
            PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbpustules).getText().toString());
            insertchildMap.put("308", new VisitDetailsPojo("308", getResources().getString(R.string.nbpustules), getResources().getString(R.string.strcemoc), strchildNo));


            pncNbs.add(p);
        }

        //jaundice
        if (aq.id(R.id.chknbjaundice).isChecked()) {
            cemocCnt++;
            PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbjaundice).getText().toString());
            insertchildMap.put("309", new VisitDetailsPojo("309", getResources().getString(R.string.nbjaundice), getResources().getString(R.string.strcemoc), strchildNo));


            pncNbs.add(p);
        }

        //fever
        if (aq.id(R.id.chknbfever).isChecked()) {
            cemocCnt++;
            PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbfever).getText().toString());
            insertchildMap.put("310", new VisitDetailsPojo("310", "", getResources().getString(R.string.strcemoc), strchildNo));


            pncNbs.add(p);
        }

        //diarrhoea
        if (aq.id(R.id.chknbdiarrhoea).isChecked()) {
            cemocCnt++;
            PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbdiarrhoea).getText().toString());
            insertchildMap.put("311", new VisitDetailsPojo("311", "", getResources().getString(R.string.strcemoc), strchildNo));

            pncNbs.add(p);
        }

        //blood in stools
        if (aq.id(R.id.chknbbloodinstools).isChecked()) {
            cemocCnt++;
            PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbbloodinstools).getText().toString());
            insertchildMap.put("312", new VisitDetailsPojo("312", "", getResources().getString(R.string.strcemoc), strchildNo));

            pncNbs.add(p);
        }

        //dull and lethargic
        if (aq.id(R.id.chknblethargicbaby).isChecked()) {
            cemocCnt++;
            PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknblethargicbaby).getText().toString());
            insertchildMap.put("313", new VisitDetailsPojo("313", "", getResources().getString(R.string.strcemoc), strchildNo));

            pncNbs.add(p);
        }

        //09Dec2019 - Bindu
        //nb fits
        if (aq.id(R.id.chknbfitsbaby).isChecked()) {
            cemocCnt++;
            PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbfitsbaby).getText().toString());
            insertchildMap.put("314", new VisitDetailsPojo("314", "", getResources().getString(R.string.strcemoc), strchildNo));

            pncNbs.add(p);
        }

        //eyes are red
        if (aq.id(R.id.chknbeyesareredbaby).isChecked()) {
            cemocCnt++;
            PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbeyesareredbaby).getText().toString());
            insertchildMap.put("315", new VisitDetailsPojo("315", "", getResources().getString(R.string.strcemoc), strchildNo));


            pncNbs.add(p);
        }

        //pallor of palms/soles
        if (aq.id(R.id.chknbpallorofpalmsbaby).isChecked()) {
            cemocCnt++;
            PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbpallorofpalmsbaby).getText().toString());
            insertchildMap.put("316", new VisitDetailsPojo("316", "", getResources().getString(R.string.strcemoc), strchildNo));


            pncNbs.add(p);
        }

        //yellow of palms
        if (aq.id(R.id.chknbyellowpalmsbaby).isChecked()) {
            cemocCnt++;
            PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbyellowpalmsbaby).getText().toString());
            insertchildMap.put("317", new VisitDetailsPojo("317", "", getResources().getString(R.string.strcemoc), strchildNo));

            pncNbs.add(p);
        }
        //Blue palms
        if (aq.id(R.id.chknbbluepalmsbaby).isChecked()) {
            cemocCnt++;
            PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbbluepalmsbaby).getText().toString());
            insertchildMap.put("318", new VisitDetailsPojo("318", "", getResources().getString(R.string.strcemoc), strchildNo));

            pncNbs.add(p);
        }

        //Feels cold
        if (aq.id(R.id.chknbfeelscoldorhotbaby).isChecked()) {
            cemocCnt++;
            PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbfeelscoldorhotbaby).getText().toString());
            insertchildMap.put("319", new VisitDetailsPojo("319", "", getResources().getString(R.string.strcemoc), strchildNo));

            pncNbs.add(p);
        }

        //Bleeding from any site
        if (aq.id(R.id.chknbbleedingbaby).isChecked()) {
            cemocCnt++;
            PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbbleedingbaby).getText().toString());
            insertchildMap.put("320", new VisitDetailsPojo("320", "", getResources().getString(R.string.strcemoc), strchildNo));

            pncNbs.add(p);
        }

        //Abdominal distension
        if (aq.id(R.id.chknbabdominaldistensionbaby).isChecked()) {
            cemocCnt++;
            PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbabdominaldistensionbaby).getText().toString());
            insertchildMap.put("321", new VisitDetailsPojo("321", "", getResources().getString(R.string.strcemoc), strchildNo));

            pncNbs.add(p);
        }

        //Any birth defect
        if (aq.id(R.id.chknbanybirthdefectbaby).isChecked()) {
            cemocCnt++;
            PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbanybirthdefectbaby).getText().toString());
            insertchildMap.put("322", new VisitDetailsPojo("322", "", getResources().getString(R.string.strcemoc), strchildNo));

            pncNbs.add(p);
        }

        //Feels warm
        if (aq.id(R.id.chknbfeelswarmbaby).isChecked()) {
            cemocCnt++;
            PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbfeelswarmbaby).getText().toString());
            insertchildMap.put("324", new VisitDetailsPojo("324", "", getResources().getString(R.string.strcemoc), strchildNo));


            pncNbs.add(p);
        }


        if (aq.id(R.id.chknbfeelstoowarmbaby).isChecked()) {
            cemocCnt++;
            PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbfeelstoowarmbaby).getText().toString());
            insertchildMap.put("325", new VisitDetailsPojo("325", "", getResources().getString(R.string.strcemoc), strchildNo));

            pncNbs.add(p);
        }

        //sunken fontanelle
        if (aq.id(R.id.chknbsunkenfont).isChecked()) {
            cemocCnt++;
            PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbsunkenfont).getText().toString());
            insertchildMap.put("326", new VisitDetailsPojo("326", "", getResources().getString(R.string.strcemoc), strchildNo));

            pncNbs.add(p);
        }
        //nb vomiting
        if (aq.id(R.id.chknbvomiting).isChecked()) {
            cemocCnt++;
            PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbvomiting).getText().toString());
            insertchildMap.put("327", new VisitDetailsPojo("327", "", getResources().getString(R.string.strcemoc), strchildNo));

            pncNbs.add(p);
        }
        //nbsepsis
        if (aq.id(R.id.chknbsepsis).isChecked()) {
            cemocCnt++;
            PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbsepsis).getText().toString());
            insertchildMap.put("328", new VisitDetailsPojo("328", "", getResources().getString(R.string.strcemoc), strchildNo));

            pncNbs.add(p);
        }

        // 14May2021 Bindu pneumonia
        if (aq.id(R.id.chknbpneumonia).isChecked()) {
            cemocCnt++;
            PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbpneumonia).getText().toString());
            insertchildMap.put("329", new VisitDetailsPojo("329", "", getResources().getString(R.string.strcemoc), strchildNo));

            pncNbs.add(p);
        }

//        <!--Aug232021 Mani-->
        if (aq.id(R.id.chknbypothermia).isChecked()) {
            cemocCnt++;
            PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbypothermia).getText().toString());
            insertchildMap.put("330", new VisitDetailsPojo("330", "", getResources().getString(R.string.strcemoc), strchildNo));
            pncNbs.add(p);
        }

        if (aq.id(R.id.chknbrespiratorydistress).isChecked()) {
            cemocCnt++;
            PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbrespiratorydistress).getText().toString());
            insertchildMap.put("331", new VisitDetailsPojo("331", "", getResources().getString(R.string.strcemoc), strchildNo));
            pncNbs.add(p);
        }
        if (aq.id(R.id.chknbcyanosis).isChecked()) {
            cemocCnt++;
            PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbcyanosis).getText().toString());
            insertchildMap.put("332", new VisitDetailsPojo("332", "", getResources().getString(R.string.strcemoc), strchildNo));
            pncNbs.add(p);
        }

        if (aq.id(R.id.chknbexcessiveweightloss).isChecked()) {
            cemocCnt++;
            PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbexcessiveweightloss).getText().toString());
            insertchildMap.put("333", new VisitDetailsPojo("333", "", getResources().getString(R.string.strcemoc), strchildNo));
            pncNbs.add(p);
        }
        if (aq.id(R.id.chknbconvulsion).isChecked()) {
            cemocCnt++;
            PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbconvulsion).getText().toString());
            insertchildMap.put("334", new VisitDetailsPojo("334", "", getResources().getString(R.string.strcemoc), strchildNo));
            pncNbs.add(p);
        }

        if (aq.id(R.id.chknbpreterm).isChecked()) {
            cemocCnt++;
            PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbpreterm).getText().toString());
            insertchildMap.put("335", new VisitDetailsPojo("335", "", getResources().getString(R.string.strcemoc), strchildNo));
            pncNbs.add(p);
        }

        // New born other danger signs //11May2021 Bindu add others to the last
        if (aq.id(R.id.chkothersymptomsnb).isChecked()) {
            if (aq.id(R.id.chkothersymptomsnb).getText().toString().trim().length() > 0) {
                String str= aq.id(R.id.etothersymptomsnb).getText().toString();
                insertchildMap.put("323", new VisitDetailsPojo("323", str, getResources().getString(R.string.strcemoc), strchildNo));
                cemocCnt++;
                PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chkothersymptomsnb).getText().toString() + " : " + str);
                PNCPojoNb psave = new PNCPojoNb(getResources().getString(R.string.othersave) + " :  " + aq.id(R.id.etothersymptomsnb).getText().toString());  //05Dec2019 - Bindu - for saving in eng
                pncNbs.add(p);
                pncNbssave.add(psave); //05Dec2019 - Bindu
            }
        }
    }

    // Blink the textview
    private void startAnimation() {
        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(500); // manage the blinking time with this parameter
        anim.setStartOffset(20);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        aq.id(R.id.txtapprefferalcentre).getTextView().startAnimation(anim);
    }

    private void prepareNbVisitDetailsData() {
        try {
            tblCvisD = new tblChildVisitDetails();
            tblCvisD.setUserId(user.getUserId());
            if (tblChildInfo.getChlReg() == 0)
                tblCvisD.setWomanId(tblChildInfo.getWomanId());
            else if (tblChildInfo.getChlReg() == 1)
                tblCvisD.setWomanId(tblChildInfo.getChlParentId());
            tblCvisD.setVisitNum(visitNum);
            tblCvisD.setVisDVisitDate(aq.id(R.id.etvisitdate).getText().toString());
            tblCvisD.setChildId(tblChildInfo.getChildID());
            tblCvisD.setChildNo(tblChildInfo.getChildNo());

            tblCvisD.setVisDReferredToFacilityType(""); //To be corrected

            tblCvisD.setVisHUserType(appState.userType);
            tblCvisD.setRecordcreateddate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
            tblCvisD.setRecordupdateddate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    /**
     * Confirm Alert on Click save
     */
    private void confirmAlert() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set dialog message
        String mess = getResources().getString(R.string.m099);
        alertDialogBuilder.setMessage(mess).setCancelable(false)
                .setNegativeButton((getResources().getString(R.string.m121)),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                try {
                                    savedata();
                                } catch (Exception e) {
                                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                                    crashlytics.log(e.getMessage());
                                }
                            }
                        })
                .setPositiveButton((getResources().getString(R.string.m122)),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    //04Dec2019 - Bindu
    private void sendSMS(TransactionHeaderRepository transRepo, DatabaseHelper databaseHelper, TblVisitChildHeader tblvisH) {
        try {
            String p_no = "";
            List<TblContactDetails> values;

            SettingsRepository settingsRepository = new SettingsRepository(databaseHelper);

            values = settingsRepository.getSpecificPhoneNumber("Child Home Visit");//13May2021 Arpitha
            p_no = "";
            for (int i = 0; i < values.size(); i++) {

                String s = values.get(i).getContactNumber();
                p_no = p_no + values.get(i).getContactNumber() + ",";
            }

            // 11Dec2019 - Bindu  Get Hamlet name
            FacilityRepository facilityRepo = new FacilityRepository(databaseHelper);
            String hamletname = facilityRepo.getHamletName(tblChildInfo.getChlTribalHamlet());

            String iscompl = "", isref = "";

            if (tblvisH.getVisHChildSymptoms() != null && tblvisH.getVisHChildSymptoms().trim().length() > 0)
                iscompl = iscompl + ", Nb.C - " + "Y";

            if (aq.id(R.id.chkisreferred).isChecked())
                isref = ", Ref - " + tblvisH.getVisHVisitIsReferred() + " , To - " + tblvisH.getVisHVisitReferredToFacilityName();


            String uniqueId = "";
            if (tblChildInfo.getChildRCHID() != null && tblChildInfo.getChildRCHID().trim().length() > 0) {
                uniqueId = "MCP:" + tblChildInfo.getChildRCHID();
            }

            String smsContent = "";
//
            smsContent = "Sentiatend :-" + tblChildInfo.getChlBabyname() + ", " + uniqueId +
                    ", V# :" + visitNum + ", V Dt: " + aq.id(R.id.etvisitdate).getText().toString()
                    + ", Chl "
                    + ", " + hamletname + iscompl + isref + ", " + appState.userType + "- " + user.getUserName()
                    + " ," + user.getUserMobileNumber() + "- (Chl.HV)";

            if (p_no.length() <= 0) {
                display_messagedialog(transRepo, databaseHelper, tblvisH, smsContent);
            } else
                sending(transRepo, databaseHelper, p_no, tblvisH, smsContent);

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    // 13Oct2016 Arpitha
    public void display_messagedialog(final TransactionHeaderRepository
                                              transactionHeaderRepository,
                                      final DatabaseHelper databaseHelper,
                                      final TblVisitChildHeader tblvisH, String smsContent) throws Exception {
        try {
            final Dialog sms_dialog = new Dialog(this);
//            sms_dialog.setTitle(getResources().getString(R.string.plsentervalidphoneno));
            sms_dialog.setContentView(R.layout.sms_dialog);
            sms_dialog.show();

            ImageButton imgsend = (ImageButton) sms_dialog.findViewById(R.id.imgsend);
            ImageButton imgcancel = (ImageButton) sms_dialog.findViewById(R.id.imgcancel);
            etphn = (EditText) sms_dialog.findViewById(R.id.etphno);
            final EditText etmess = (EditText) sms_dialog.findViewById(R.id.etmess);
            TextView txtcontcats = (TextView) sms_dialog.findViewById(R.id.txtcontacts);


//            etmess.setVisibility(View.GONE);
            etmess.setText(smsContent);
            etmess.setEnabled(false);
            txtcontcats.setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    // TODO Auto-generated method stub
                    Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                    startActivity(intent);
                    return true;
                }
            });

            imgsend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        sending(transactionHeaderRepository, databaseHelper,

                                etphn.getText().toString(), tblvisH, smsContent);
                    } catch (Exception e) {
                        e.printStackTrace();
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }
                }
            });
            imgcancel.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    sms_dialog.cancel();
                    //21Ma2021 Arpitha
                    Intent intent = new Intent(ChildHomeVisit.this, ChildHomeVisitHistory.class);
                    intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    intent.putExtra("appState", getIntent().getBundleExtra("appState"));
                    intent.putExtra("tblChildInfo", tblChildInfo);
                    startActivity(intent);
                }
            });
            sms_dialog.setCancelable(false);
        } catch (Exception e) {
            e.printStackTrace();
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }

    }

    void sending(TransactionHeaderRepository transRepo, DatabaseHelper databaseHelper,
                 String phnNo, TblVisitChildHeader tblvisH, String smsCont) throws Exception {
        String smsContent = smsCont;

        InserttblMessageLog(phnNo,
                smsContent, user.getUserId(), transRepo, databaseHelper);

        //21Ma2021 Arpitha
        Intent intent = new Intent(ChildHomeVisit.this, ChildHomeVisitHistory.class);
        intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
        intent.putExtra("appState", getIntent().getBundleExtra("appState"));
        intent.putExtra("tblChildInfo", tblChildInfo);
        startActivity(intent);

        if (isMessageLogsaved) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.sending_sms),
                    Toast.LENGTH_LONG).show();
            sendSMSFunction();
        }

    }

    public void InserttblMessageLog(String phoneno, String content, String user_id,
                                    TransactionHeaderRepository transactionHeaderRepository, DatabaseHelper databaseHelper) throws Exception {
        final ArrayList<MessageLogPojo> mlpArr = new ArrayList<MessageLogPojo>();

        if (phoneno.length() > 0) {
            String[] phn = phoneno.split("\\,");
            for (int i = 0; i < phn.length; i++) {
                String num = phn[i];
                if (num != null && num.length() > 0) {
                    MessageLogPojo mlp =
                            new MessageLogPojo();
                    mlp.setUserId(user_id);
                    mlp.setMsgPhoneNo(num);
                    mlp.setMsgBody(content);
                    mlp.setMsgPriority(1);
                    mlp.setMsgNoOfFailures(0);
                    mlp.setTransId(transId);
                    mlp.setMsgSentDate(DateTimeUtil.getTodaysDate());
                    mlp.setMsgSent(0);
                    mlp.setMsgStage("Home visit - Child");
                    mlp.setMsgUserType(appState.userType);
                    mlp.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
                    mlp.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());//21May2021 Arpitha
                    mlpArr.add(mlp);
                }
            }
        }
        int addMessage = 0;
        SMSRepository smsRepository = new SMSRepository(databaseHelper);
        if (mlpArr != null) {
            for (final MessageLogPojo mlpp : mlpArr) {
                addMessage = smsRepository.insertIntoMessageLog(mlpp, databaseHelper);
            }
            if (addMessage > 0) {
                isMessageLogsaved = true;
                boolean addRegTrans = transactionHeaderRepository.iNewRecordTransNew(user.getUserId(), transId,
                        "tblmessagelog", databaseHelper);
            }
        }
    }

    private void sendSMSFunction() throws Exception {
        messageSent = true;
        new SendSMS(this).checkAndSendMsg(databaseHelper);
    }

    //  07Dec2019 Bindu
    public int checkSimState() {
        int simState = -1;
        // This is to know the status of the Sim
        TelephonyManager telMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        simState = telMgr.getSimState();

        return simState;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
                File file = new File(mCurrentPhotoPath);
                FileUtils fileUtils = new FileUtils(getApplicationContext());
                file = new File(fileUtils.getPath(Uri.fromFile(file)));
                file = new Compressor(getApplicationContext()).compressToFile(file);
                Bitmap bitmap = MediaStore.Images.Media
                        .getBitmap(getApplicationContext().getContentResolver(), Uri.fromFile(file));
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100 /*ignored for PNG*/, bos);
                byte[] bitmapdata = bos.toByteArray();
                String timeStamp = new SimpleDateFormat("HHmmssSSS").format(new Date());
                String fn;
                String dir;
                fn = "IMG_" + tblChildInfo.getChildID() + "_HV" + visitNum + timeStamp + ".jpeg";
                dir = AppState.imgDirRef;
                File fileParent = new File( dir);

                String fileName = fileParent + "/" + fn;

                ImagePojo imagePojo = new ImagePojo();
                imagePojo.setImage(bitmapdata);
                imagePojo.setImagePath(fileName);
                imagePojo.setImageBitmap(bitmap);

                AllImagePojo.add(imagePojo);

                tblImageStore imageStore = new tblImageStore();
                imageStore.setUserId(user.getUserId());
                imageStore.setBeneficiaryId(tblChildInfo.getChildID());
                imageStore.setImageName(fn.replace(".jpeg", ""));
                imageStore.setImagePath(fileName);
                imageStore.setTransId(transId);
                imageStore.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
                imageStore.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
                AllImagesBeforeSave.add(imageStore);
                AsyncImage asyncImage = new AsyncImage();
                asyncImage.execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void startRecyclerView() {
        try {
            recyclerViewImage = findViewById(R.id.recyclerImage);
            if (AllImagesBeforeSave.size() == 0) {
                aq.id(R.id.lladdphotos).gone();
            } else {
                aq.id(R.id.lladdphotos).visible();
                recyclerViewImage.setVisibility(View.VISIBLE);
                aq.id(R.id.txtnoimageadded).getTextView().setVisibility(View.GONE);
                DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
                float screenWidthDp = displayMetrics.widthPixels / displayMetrics.density;
                int noOfColumns = (int) (screenWidthDp / 125 + 0.5); // +0.5 for correct rounding to int.
                GridLayoutManager layoutManager = new GridLayoutManager(ChildHomeVisit.this, noOfColumns);
                recyclerViewImage.setLayoutManager(layoutManager);
                recyclerViewImage.setNestedScrollingEnabled(false);
                recyclerViewImage.setHasFixedSize(true);
                RecyclerViewAdapter recyclerViewAdapter = new RecyclerViewAdapter(ChildHomeVisit.this, AllImagesBeforeSave, databaseHelper, AllImagePojo, aq);
                recyclerViewImage.setAdapter(recyclerViewAdapter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void fileUpload() {
        try {
            AsyncFileUpload asyncFileUpload = new AsyncFileUpload();
            asyncFileUpload.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method opens the Date Dialog and sets Date to respective EditText
     */
    private void assignEditTextAndCallDatePicker(EditText editText, ImageView imageView) {
        currentEditTextViewforDate = editText;
        currentImageView = imageView;
        String currDate = (editText.getText().length() > 0) ? editText.getText().toString() : "";
        CommonDatePickerFragment.getCommonDate(ChildHomeVisit.this, currDate);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        if (view.isShown()) {
            try {

                String SelectedDate = String.format("%02d", day) + "-" + String.format("%02d", month + 1) + "-" + year;

                Date dobdate = null;
                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                Date seldate = format.parse(SelectedDate);

                if (tblChildInfo.getChlDateTimeOfBirth() != null && tblChildInfo.getChlDateTimeOfBirth().trim().length() > 0)
                    dobdate = format.parse(tblChildInfo.getChlDateTimeOfBirth());

                if (isVisitDate) {
                    if (seldate.after(new Date())) {
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.date_cannot_af_currentdate), this);
                        aq.id(R.id.etvisitdate).text(DateTimeUtil.getTodaysDate());
                    } else if (dobdate != null && seldate.before(dobdate)) {

                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.date_cannot_bf_deldate)
                                , this);
                        aq.id(R.id.etvisitdate).text(DateTimeUtil.getTodaysDate());
                    } else {
                        aq.id(R.id.etvisitdate).text(SelectedDate);
                        aq.id(R.id.imgcleardate).visible();

                        calculateDaysfromDel(SelectedDate);
                    }
                } else {
                    if (seldate.after(new Date())) {
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.date_cannot_af_currentdate), this);
                        currentEditTextViewforDate.setText("");
                        currentEditTextViewforDate.requestFocus();
                        currentEditTextViewforDate.setError("Error");
                        aq.id(currentImageView).gone();
                    } else if (dobdate != null && seldate.before(dobdate)) { //17May2021 Bindu set covid test date
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.date_cannot_bf_deldate)
                                , this);
                        currentEditTextViewforDate.setText("");
                        currentEditTextViewforDate.requestFocus();
                        currentEditTextViewforDate.setError("Error");
                        aq.id(currentImageView).gone();
                    } else {
                        currentEditTextViewforDate.setText(SelectedDate);
                        aq.id(currentImageView).visible();
                        currentEditTextViewforDate.setError(null);
                    }
                }
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
        }
    }

    // Save data
    private void savedata() {
        try {

            tblChildVisDetailDao = databaseHelper.getTblChildVisitDetailDao();
            tblVisChildHeaderDao = databaseHelper.getTblVisitChildHeaderDao();
            //28-6-2021
            tblImageStoreDao = databaseHelper.getTblImageStoreDao();

            prepareNbVisitDetailsData();
            prepareChildVisitHeaderData();


            final TransactionHeaderRepository transRepo = new TransactionHeaderRepository(databaseHelper);
            transId = transRepo.iCreateNewTrans(appState.sessionUserId, databaseHelper);

            tblCvisD.setTransid(transId);
            tblvisCH.setTransid(transId);


            TransactionManager.callInTransaction(databaseHelper.getConnectionSource(), new Callable<Void>() {
                public Void call() throws Exception {
                    int add = 0;
                    boolean added;
                    boolean isNbdatainseted = true;


                    if (insertchildHeaderMap != null && insertchildHeaderMap.size() > 0) {
                        for (Map.Entry<String, VisitDetailsPojo> entry : insertchildHeaderMap.entrySet()) {
                            tblvisCH.setVisCHTemperature(entry.getValue().getTemp());
                            tblvisCH.setVisCHTemperatureDate(entry.getValue().getTempdate());
                            tblvisCH.setVisCHWeight(entry.getValue().getWeight());
                            tblvisCH.setVisCHWeightDate(entry.getValue().getWeightdate());
                            tblvisCH.setVisCHResprate(entry.getValue().getResprate());
                            tblvisCH.setVisCHResprateDate(entry.getValue().getRespratedate());
                            tblvisCH.setVisCHRegAtGovtFac(entry.getValue().isChildregatgovtfac());
                            tblvisCH.setVisCHRegGovtFacId(entry.getValue().getChildregid());
                            tblvisCH.setVisCHRegAtGovtFacDate(entry.getValue().getChildregdate());

                            add = tblVisChildHeaderDao.create(tblvisCH);

                        }

                        if (add > 0) {
                            added = transRepo.iNewRecordTransNew(appState.sessionUserId, transId,
                                    tblVisChildHeaderDao.getTableName(), databaseHelper);
                            isNbdatainseted = true;
                        } else
                            isNbdatainseted = false;
                    }

                    if (isNbdatainseted && insertchildMap != null && insertchildMap.size() > 0) {
                        for (Map.Entry<String, VisitDetailsPojo> entry : insertchildMap.entrySet()) {
                            tblCvisD.setVisDSymptoms(entry.getValue().getSympid());
//                            tblCvisD.setChildId(entry.getValue().getChildId());
                            tblCvisD.setVisDReferredToFacilityType(entry.getValue().getRefTo());
                            if (entry.getKey().equals("323") && aq.id(R.id.etothersymptomsnb).getText().toString().length() > 0) {
                                tblCvisD.setVisDOtherSymptoms(aq.id(R.id.etothersymptomsnb).getText().toString());
                            } else
                                tblCvisD.setVisDOtherSymptoms("");
                            add = tblChildVisDetailDao.create(tblCvisD);

                        }
                        if (add > 0) {
                            isNbdatainseted = true;
                        } else
                            isNbdatainseted = false;

                        if (isNbdatainseted) {
                            added = transRepo.iNewRecordTransNew(appState.sessionUserId, transId,
                                    tblChildVisDetailDao.getTableName(), databaseHelper);
                        }
                    }
                    if (AllImagesBeforeSave.size()!=0 && AllImagePojo.size()!=0){
                        for (tblImageStore s: AllImagesBeforeSave){
                            s.setTransId(transId);
                            s.setImagePath("");
                            add = tblImageStoreDao.create(s);
                        }
                        if (add>0){
                            added = transRepo.iNewRecordTransNew(appState.sessionUserId, transId,
                                    tblImageStoreDao.getTableName(), databaseHelper);
                        }
                        for (ImagePojo s:AllImagePojo){
                            fileUpload();
                            FileOutputStream fileOutputStream = new FileOutputStream(s.getImagePath());
                            fileOutputStream.write(s.getImage());
                        }
                    }


                    if (add > 0) {

                        if (isNbdatainseted) {

                            if (tblvisCH.getVisCHCompl().equalsIgnoreCase("Y")) { //If compl, then send sms //16May2021 Bindu - check for newborn compl as well
                                //04Dec2019 - Bindu
                                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ChildHomeVisit.this);
                                //mani 15 april 2021 uncommented lines
                                if (checkSimState() == TelephonyManager.SIM_STATE_READY &&
                                        prefs.getBoolean("sms", false)) {
                                    sendSMS(transRepo, databaseHelper, tblvisCH);
                                } else {
                                    Intent intent = new Intent(ChildHomeVisit.this, ChildHomeVisitHistory.class);
                                    intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                    intent.putExtra("appState", getIntent().getBundleExtra("appState"));
                                    intent.putExtra("tblChildInfo", tblChildInfo);
                                    startActivity(intent);
                                }
                            } else {
                                Intent intent = new Intent(ChildHomeVisit.this, ChildHomeVisitHistory.class);
                                intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                intent.putExtra("appState", getIntent().getBundleExtra("appState"));
                                intent.putExtra("tblChildInfo", tblChildInfo);
                                startActivity(intent);
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.failedtoaddhomevisit), Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.failedtoaddhomevisit), Toast.LENGTH_LONG).show();
                    }

                    return null;
                }
            });
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            e.printStackTrace();
        }
    }

    // Create data for Child visit header tbl
    private void prepareChildVisitHeaderData() {
        try {
            tblvisCH = new TblVisitChildHeader();

            tblvisCH.setUserId(user.getUserId());
            if (tblChildInfo.getChlReg() == 0)
                tblvisCH.setWomanId(tblChildInfo.getWomanId());
            else if (tblChildInfo.getChlReg() == 1)
                tblvisCH.setWomanId(tblChildInfo.getChlParentId());

            tblvisCH.setChildId(tblChildInfo.getChildID());
            tblvisCH.setChildNo(tblChildInfo.getChildNo());

            tblvisCH.setVisitNum(visitNum);
            tblvisCH.setVisHVisitDate(aq.id(R.id.etvisitdate).getText().toString());
            tblvisCH.setVisHVisitIsReferred(aq.id(R.id.chkisreferred).isChecked() ? getResources().getString(R.string.yesshortform) : getResources().getString(R.string.noshortform));

            //mani 18June2021
            String ashaavailability="";
            if (aq.id(R.id.ashavailableYes).isChecked())
                ashaavailability="Y";
            else if (aq.id(R.id.ashavailableNo).isChecked())
                ashaavailability="N";
            else if (aq.id(R.id.ashavailableDontKnow).isChecked())
                ashaavailability="Dont Know";
            tblvisCH.setVisCHAshaAvailable(ashaavailability);

            // Tablet details
            if (aq.id(R.id.rd_IFAYes).isChecked()) {
                tblvisCH.setChildvisHIsIFATaken("Yes");
                if (aq.id(R.id.rd_ConsumingIFAYes).isChecked()) {
                    tblvisCH.setChildvisHIFADaily("Yes");
                    tblvisCH.setChildvisHIFATabletsCount(aq.id(R.id.etIFANoofTabletsConsumes).getEditText().getText().toString());
                    tblvisCH.setChildvisHIFANotTakenReason("");

                } else {
                    tblvisCH.setChildvisHIFADaily("No");
                    String reasons = spnIFAReasons.getSelectedIndicies().toString();
                    reasons = reasons.replace("[", "");
                    reasons = reasons.replace("]", "");
                    tblvisCH.setChildvisHIFANotTakenReason(reasons);
                    tblvisCH.setChildvisHIFATabletsCount("");
                }
                tblvisCH.setChildvisHIFAGivenBy(aq.id(R.id.spnWhoGaveIFATablets).getSpinner().getSelectedItemPosition());
            } else {

                tblvisCH.setChildvisHIsIFATaken("No");
                tblvisCH.setChildvisHIFADaily("");
                tblvisCH.setChildvisHIFATabletsCount("");
                tblvisCH.setChildvisHIFANotTakenReason("");
                tblvisCH.setChildvisHIFAGivenBy(0);
            }

            if (aq.id(R.id.rd_dewormingtabletsYes).isChecked()) {
                tblvisCH.setChildvisHDewormingtab("Yes");
            } else if (aq.id(R.id.rd_dewormingtabletsNo).isChecked()) {
                tblvisCH.setChildvisHDewormingtab("No");
            } else if (aq.id(R.id.rd_dewormingtabletsDono).isChecked()) {
                tblvisCH.setChildvisHDewormingtab("Dont Know");
            }


            String sel = "";
            if (aq.id(R.id.spnfacnameHv).getSelectedItem() != null)
                sel = aq.id(R.id.spnfacnameHv).getSelectedItem().toString();
            if (sel.trim().length() > 0 && !(sel.equalsIgnoreCase(getResources().getString(R.string.select)) || sel.equalsIgnoreCase(getResources().getString(R.string.other)))) {
                tblvisCH.setVisHVisitReferredToFacilityName(aq.id(R.id.spnfacnameHv).getSelectedItem().toString());
                tblvisCH.setVisHVisitReferredToFacilityType(aq.id(R.id.spnfactype).getSelectedItem().toString()); //15Apr2021 - Bindu - set facility type
                /*for (Map.Entry<String, String> village : mapRefFacilityname.entrySet()) {
                    if (aq.id(R.id.spnfacnameHv).getSelectedItem().toString().equalsIgnoreCase(village.getValue()))
                        tblvisCH.setVisHVisitReferredToFacilityType(village.getKey());
                }*/
            } else if (sel.equalsIgnoreCase(getResources().getString(R.string.other))
                    && aq.id(R.id.etreffacilityname).getText().toString().trim().length() <= 0) {
                tblvisCH.setVisHVisitReferredToFacilityName(aq.id(R.id.etreffacilityname).getText().toString());
                tblvisCH.setVisHVisitReferredToFacilityType(getResources().getString(R.string.other));
            } else {
                tblvisCH.setVisHVisitReferredToFacilityName(aq.id(R.id.etreffacilityname).getText().toString());
                if (aq.id(R.id.etreffacilityname).getText().toString().length() > 0)
                    tblvisCH.setVisHVisitReferredToFacilityType(getResources().getString(R.string.other));
                else
                    tblvisCH.setVisHVisitReferredToFacilityType("");
            }

            tblvisCH.setVisHVisitComments(aq.id(R.id.etvisitcomments).getText().toString());
            // if (arrLNbSymptoms.size() > 0)
            if (pncNbs.size() > 0)  //26Apr2021 Bindu - NB danger signs size and set
                tblvisCH.setVisCHCompl(getResources().getString(R.string.yesshortform));
            else
                tblvisCH.setVisCHCompl(getResources().getString(R.string.noshortform));
            tblvisCH.setVisCHAdvise(aq.id(R.id.txtactionsumm).getText().toString());

            tblvisCH.setVisHChildSymptoms(aq.id(R.id.txtDangSignsNbE).getText().toString()); //pending
            tblvisCH.setVisHUserType(appState.userType);
            tblvisCH.setRecordcreateddate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
            tblvisCH.setRecordupdateddate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
            tblvisCH.setVisHReferralSlipNumber("" + aq.id(R.id.etrefslipnumber).getText().toString()); //26Apr2021 Bindu add referal slip number


            insertchildHeaderMap = new LinkedHashMap<>();

            insertchildHeaderMap.put(strchildNo, new VisitDetailsPojo(strchildNo,
                    aq.id(R.id.etweightbaby1).getText().toString(), aq.id(R.id.etweightdatebaby1).getText().toString(),
                    aq.id(R.id.etTempbaby1).getText().toString(), aq.id(R.id.ettempdonedatebaby1).getText().toString(),
                    aq.id(R.id.etrespratebaby1).getText().toString(), aq.id(R.id.etrespratedonedatebaby1).getText().toString(),
                    aq.id(R.id.chkischildregatfacchild1).isChecked(), aq.id(R.id.etchildregid1).getText().toString(), aq.id(R.id.etchildregdate1).getText().toString(), 1));

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    // Calculate diff of days from del
    private void calculateDaysfromDel(String selectedDate) {
        try {
            aq.id(R.id.txtgestage).text(getResources().getString(R.string.statusatvisit) + "- " + DateTimeUtil.calculateAge(selectedDate));
//            strstatus = noOfDays + "D";
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    //mani 20Aug2021
    public void opencloseAccordionCheckbox(CheckBox chk) {
        try {
            if (chk.isChecked()) {
                chk.setTextColor(getResources().getColor(R.color.red));
                chk.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, 0, 0);
            } else {
                chk.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                chk.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    /**
     * The click listener for ListView in the navigation drawer
     */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            mDrawerLayout.closeDrawer(mDrawerList);
            try {

                switch (position) {

                    case 0:
                        if (tblChildInfo.getChlReg() == 1) {
                            Intent nextScreen = new Intent(ChildHomeVisit.this, EditRegChildInfoActivity.class);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                            nextScreen.putExtra("tblChildInfo", tblChildInfo);
                            startActivity(nextScreen);
                        } else {
                            Intent nextScreen = new Intent(ChildHomeVisit.this, EditChildActivity.class);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                            nextScreen.putExtra("tblChildInfo", tblChildInfo);
                            startActivity(nextScreen);
                        }
                        break;

                    case 1: {
                        if (tblChildInfo.getChlParentId() != null && tblChildInfo.getChlParentId().trim().length() > 0) {
                            Intent viewParent = new Intent(ChildHomeVisit.this, ViewParentDetails.class);
                            viewParent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            viewParent.putExtra("appState", getIntent().getBundleExtra("appState"));
                            viewParent.putExtra("tblChildInfo", tblChildInfo);
                            startActivity(viewParent);
                        } else {
                            WomanRepository womanRepository = new WomanRepository(databaseHelper);
                            tblregisteredwomen woman = womanRepository.getRegistartionDetails(tblChildInfo.getWomanId(), tblChildInfo.getUserId());
                            appState.selectedWomanId = tblChildInfo.getWomanId();
                            Intent nextScreen = new Intent(ChildHomeVisit.this, ViewProfileActivity.class);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                            nextScreen.putExtra("woman", woman);
                            startActivity(nextScreen);
                        }
                        break;
                    }


                    case 2: {
                        Intent nextScreen = new Intent(ChildHomeVisit.this, CGMList.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                        nextScreen.putExtra("tblChildInfo", tblChildInfo);
                        startActivity(nextScreen);

                        break;
                    }


                    case 3: {
                        Intent nextScreen = new Intent(ChildHomeVisit.this, ChildHomeVisitHistory.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                        nextScreen.putExtra("tblChildInfo", tblChildInfo);
                        startActivity(nextScreen);

                        break;
                    }
                    case 4: {
                        if (tblChildInfo.getChlDeliveryResult() <= 1) {
                            Intent nextScreen = new Intent(ChildHomeVisit.this, ImmunizationListActivity.class);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                            nextScreen.putExtra("tblChildInfo", tblChildInfo);
                            startActivity(nextScreen);
                        } else
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.m053), Toast.LENGTH_LONG).show();
                        break;
                    }

                    case 5: {
                        if (user.getIsDeactivated() == 1
                        )
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.userdeactivated), Toast.LENGTH_LONG).show();
                        else {
                            Intent addSibling = new Intent(ChildHomeVisit.this, AddSiblingsActivity.class);
                            addSibling.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            addSibling.putExtra("appState", getIntent().getBundleExtra("appState"));
                            if (tblChildInfo.getChlReg() == 1)
                                addSibling.putExtra("chlParentId", tblChildInfo.getChlParentId());
                            else
                                addSibling.putExtra("WomanId", tblChildInfo.getWomanId());
                            startActivity(addSibling);
                        }
                        break;
                    }

                    case 6: {
                        Intent nextScreen = new Intent(ChildHomeVisit.this, ChildDeactivateActivity.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                        nextScreen.putExtra("tblChildInfo", tblChildInfo);
                        startActivity(nextScreen);

                        break;
                    }
                }

            } catch (Exception e) {


                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
        }
    }

    private class AsyncImage extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {

            Collections.sort(AllImagesBeforeSave, (file1, file2) -> {
                long k = new File(file1.getImagePath()).lastModified() - new File(file2.getImagePath()).lastModified();
                if (k < 0) {
                    return 1;
                } else if (k == 0) {
                    return 0;
                } else {
                    return -1;
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            startRecyclerView();
        }
    }

    private class AsyncFileUpload extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            try {
                OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                        .connectTimeout(60, TimeUnit.SECONDS)
                        .readTimeout(60, TimeUnit.SECONDS)
                        .writeTimeout(60, TimeUnit.SECONDS)
                        .build();

                for (tblImageStore s : AllImagesBeforeSave) {
                    String imagepath = AppState.imgDirRef +"/"+ s.getImageName() + ".jpeg";
                    File file = new File(imagepath);

                    RequestBody requestBody = new MultipartBody.Builder()
                            .setType(MultipartBody.FORM)
                            .addFormDataPart("filename", s.getImageName())
                            .addFormDataPart(s.getImageName(), s.getImageName(),
                                    RequestBody.create(MediaType.parse("image/jpg"), file))
                            .build();
                    String endpoint = "http://" + appState.webServiceIpAddress + appState.webServicePath + "image";
                    Request request = new Request.Builder()
                            .url(endpoint)
                            .post(requestBody)
                            .build();
                    try (Response r2 = okHttpClient.newCall(request).execute()) {
                        String responseString2 = r2.body().string();
                        Log.i("Image Upload", responseString2);
                        updateImageStatus(s.getImageName());
                    }catch (Exception e){
                        Log.i("Image Upload",e.getMessage());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
    private void updateImageStatus(String imageName) throws Exception {
        ImageStoreRepository imageStoreRepository = new ImageStoreRepository(databaseHelper);
        int update = imageStoreRepository.updateImageStatusof(imageName);
        if (update > 0) {
            Log.e("Image", "Image uploaded");
        } else {
            Log.e("Image", "Image Not uploaded");
        }
    }
}