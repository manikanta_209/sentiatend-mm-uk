package com.sc.stmansi.childhomevisit;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.R;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.recyclerview.ClickListener;
import com.sc.stmansi.tables.TblSymptomMaster;
import com.sc.stmansi.tables.tblCaseMgmt;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class ChildHomeVisitAdapter extends RecyclerView.Adapter<ChildHvHolder> {

    Context context;
    private ClickListener viewClickListener;
    List<tblCaseMgmt> tblcasemgmtslist;
    DatabaseHelper databaseHelper;

    public ChildHomeVisitAdapter(Context context, ClickListener viewClickListener, List<tblCaseMgmt> tblcasemgmtslist) {
        this.context = context;
        this.viewClickListener = viewClickListener;
        this.tblcasemgmtslist = tblcasemgmtslist;
        databaseHelper = getHelper();

    }

    @NonNull
    @Override
    public ChildHvHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.child_homevisit_item, parent, false);
        v.setOnClickListener(viewClickListener);
        context = parent.getContext();
        return new ChildHvHolder(v);
    }

    //    initialize db object
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void onBindViewHolder(@NonNull ChildHvHolder holder, int i) {
        try {
            holder.visitdate.setText(context.getResources().getString(R.string.visit_date) + tblcasemgmtslist.get(i).getVisitDate());
            holder.visitnumber.setText("# " + Integer.toString(tblcasemgmtslist.get(i).getVisitNum()));

            holder.txtvisitmode.setText("@ "+   tblcasemgmtslist.get(i).getVisitMode()); //13Aug2021 Bindu

            if (tblcasemgmtslist.get(i).getVisitReferredtoFacType() != null && tblcasemgmtslist.get(i).getVisitReferredtoFacType().length() > 0)
                holder.imgVisitOutcome.setImageResource(R.drawable.ic_refer);
            else
                holder.imgVisitOutcome.setImageResource(R.drawable.ic_hvcompl);


            if (tblcasemgmtslist.get(i).getVisitReferredtoFacType() != null && tblcasemgmtslist.get(i).getVisitReferredtoFacType().length() > 0 || tblcasemgmtslist.get(i).getVisitReferredtoFacName().length() > 0)
                holder.txtReferredToFacname.setText(context.getResources().getString(R.string.reffacilityname) + tblcasemgmtslist.get(i).getVisitReferredtoFacType() + " - " + tblcasemgmtslist.get(i).getVisitReferredtoFacName());
            else
                holder.txtReferredToFacname.setText("");


            holder.imgVisitOutcome.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        if ((tblcasemgmtslist.get(i).getVisitBeneficiaryDangerSigns() != null && tblcasemgmtslist.get(i).getVisitBeneficiaryDangerSigns().length() > 0)) {
                            String title = context.getResources().getString(R.string.complications);

                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                            View convertView = LayoutInflater.from(context).inflate(R.layout.customchildhv_dialog, null, false);
                            TextView txtchilddangersigns = convertView.findViewById(R.id.tvdialogchlhv);

                            txtchilddangersigns.setText(setChildCompl(tblcasemgmtslist.get(i).getVisitBeneficiaryDangerSigns() ));

                            alertDialog.setView(convertView).setCancelable(true).setTitle(title)
                                    .setPositiveButton((context.getResources().getString(R.string.ok)), new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    });
                            // lv.setAdapter(adapter);
                            alertDialog.show();
                        }
                    } catch (Exception e) {

                    }
                }
            });


        } catch (Exception e) {
            FirebaseCrashlytics firebaseCrashlytics = FirebaseCrashlytics.getInstance();
            firebaseCrashlytics.log(e.getMessage());
        }
    }


    @Override
    public int getItemCount() {
        return tblcasemgmtslist.size();
    }

    private String setChildCompl(String dangersign) throws SQLException {

        ArrayList<String> symptoms = new ArrayList<>();
        ArrayList<String> list = new ArrayList<>();
        String symptom = "";
        if (dangersign != null && dangersign.length() > 0) {
            list = new ArrayList<String>(Arrays.asList(dangersign.split(",")));

            if (list != null && list.size() > 0) {

                for (int i = 0; i < list.size(); i++) {
                    String strSy = list.get(i);

                    List<TblSymptomMaster> symptomMasterList = databaseHelper.getTblSymptomMasterDao().queryBuilder()
                            .selectColumns("symXmlStringName").where()
                            .eq("symSymptomName", strSy).query();

                    if (symptomMasterList != null && symptomMasterList.size() > 0) {
                        symptoms.add(symptomMasterList.get(0).getSymXmlStringName());
                    } else {
                        if (strSy.toLowerCase().trim().contains("grunting")) //21Jul2021 Bindu manually add
                            symptoms.add("nbfastbreathing");
                        if (strSy.toLowerCase().trim().contains("diarrhea")) //21Jul2021 Bindu manually add
                            symptoms.add("nbdiarrhoea");
                        if(strSy.toLowerCase().trim().contains("others :")) //25Sep2021 Bindu add others
                            symptoms.add(strSy);
                    }

                }

                if(symptoms != null) {
                    String strSy = "";
                    for(int i=0; i< symptoms.size();i++) {
                        strSy = symptoms.get(i);
                        strSy = strSy.replaceAll(" ", "");
                        strSy = strSy.toLowerCase().trim();
                        int identifier = context.getResources().getIdentifier
                                (strSy,
                                        "string", context.getPackageName()); //25Jun2021 Bindu get package name from app
                        if (identifier > 0) {
                            if (i == list.size() - 1)
                                symptom = symptom + context.getResources().getString(identifier);
                            else
                                symptom = symptom + context.getResources().getString(identifier) + ",";
                        } else {
                            if (i == list.size() - 1)
                                symptom = symptom + strSy;
                            else
                                symptom = symptom + strSy + ",";
                        }
                    }
                }
            }
        }return symptom;
    }
}
