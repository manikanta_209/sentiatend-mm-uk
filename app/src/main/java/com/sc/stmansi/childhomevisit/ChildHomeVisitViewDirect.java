package com.sc.stmansi.childhomevisit;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.legacy.app.ActionBarDrawerToggle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.About.AboutActivity;
import com.sc.stmansi.ImageStore.RecyclerViewAdapter;
import com.sc.stmansi.R;
import com.sc.stmansi.childgrowthmonitor.CGMList;
import com.sc.stmansi.childregistration.AddSiblingsActivity;
import com.sc.stmansi.childregistration.EditChildActivity;
import com.sc.stmansi.childregistration.EditRegChildInfoActivity;
import com.sc.stmansi.childregistration.ViewParentDetails;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.common.NavDrawerItem;
import com.sc.stmansi.common.NavDrawerListAdapter;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.deactivate.ChildDeactivateActivity;
import com.sc.stmansi.immunization.ImmunizationListActivity;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.repositories.HomeVisitRepository;
import com.sc.stmansi.repositories.ImageStoreRepository;
import com.sc.stmansi.repositories.LoginRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.tables.TblChildInfo;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.TblVisitChildHeader;
import com.sc.stmansi.tables.TblVisitHeader;
import com.sc.stmansi.tables.tblImageStore;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.viewprofile.ViewProfileActivity;
import com.sc.stmansi.womanlist.RegisteredWomenActionTabs;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ChildHomeVisitViewDirect extends AppCompatActivity {
    private List<TblVisitChildHeader> visitItems;
    private AQuery aq;
    private int index = 0;
    private TblVisitHeader prevObj, nextObj;
    private TblVisitHeader visitListData;
    private ArrayList<String> aList;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private ArrayList<NavDrawerItem> navDrawerItems;
    private ListView mDrawerList;
    private int itempos = 0;
    private List<tblregisteredwomen> wVisitDetails;

    private AppState appState;
    private DatabaseHelper databaseHelper;
    String childId;
    TblChildInfo tblChildInfo;
    TblInstusers user;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_child_home_visit_view_direct);
            aq = new AQuery(this);

            databaseHelper = getHelper();
            Bundle bundle = getIntent().getBundleExtra("globalState");
            appState = bundle.getParcelable("appState");
            itempos = getIntent().getIntExtra("pos", 0);
            childId = getIntent().getStringExtra("childId");
            index = itempos;
            tblChildInfo = (TblChildInfo) getIntent().getSerializableExtra("tblChildInfo");


            HomeVisitRepository homeRepo = new HomeVisitRepository(databaseHelper);
            visitItems = homeRepo.getVisitHeaderDetailsForChild(childId, appState.sessionUserId);




            initiateDrawer();

            aq.id(R.id.btnnextvisit).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        index = index + 1;
                        if (index < visitItems.size()) {
                            setData(visitItems, index);
                        } else {
                            index = index - 1;
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.norecords), Toast.LENGTH_LONG).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            aq.id(R.id.btnpreviousvisit).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {

                        index = index - 1;
                        if (index >= 0) {
                            setData(visitItems, index);
                        } else {
                            index = index + 1;
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.norecords), Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });

            //  setInitialValues();
            setData(visitItems, itempos);
            // Symptoms drwable on touch listener
            aq.id(R.id.txtDangSignsL).getTextView().setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (event.getRawX() <= aq.id(R.id.txtDangSignsL).getTextView().getTotalPaddingLeft()) {
                            opencloseAccordionDangerSigns(aq.id(R.id.txtDangSigns).getView(), aq.id(R.id.txtDangSignsL).getTextView());
                            return true;
                        }
                    }
                    return true;
                }
            });

            // Advise drwable on touch listener
            aq.id(R.id.txtadvisegiven).getTextView().setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (event.getRawX() <= aq.id(R.id.txtadvisegiven).getTextView().getTotalPaddingLeft()) {
                            opencloseAccordionDangerSigns(aq.id(R.id.txtadvisesumm).getView(), aq.id(R.id.txtadvisegiven).getTextView());
                            return true;
                        }
                    }
                    return true;
                }
            });

//            Ramesh 1-6-2021
            aq.id(R.id.txtPhotos).getTextView().setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (event.getRawX() <= aq.id(R.id.txtPhotos).getTextView().getTotalPaddingLeft()) {
                            opencloseAccordionDangerSigns(aq.id(R.id.recyclerImage).getView(), aq.id(R.id.txtPhotos).getTextView());
                            return true;
                        }
                    }
                    return true;
                }
            });
            aq.id(R.id.txtPhotospnc).getTextView().setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (event.getRawX() <= aq.id(R.id.txtPhotospnc).getTextView().getTotalPaddingLeft()) {
                            opencloseAccordionDangerSigns(aq.id(R.id.recyclerImagepnc).getView(), aq.id(R.id.txtPhotospnc).getTextView());
                            return true;
                        }
                    }
                    return true;
                }
            });

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    // Set Initial Values
    private void setInitialValues() {
        aq.id(R.id.txtVisitNum).text("" + visitListData.getVisitNum());
        aq.id(R.id.txtVisitDateval).text(visitListData.getVisHVisitDate());

        if (visitListData.getVisHVisitType().equalsIgnoreCase(getResources().getString(R.string.stranc))) {
            aq.id(R.id.txtgestage).text(getResources().getString(R.string.statusatvisit));
            aq.id(R.id.txtgestageval).text(visitListData.getVisHStatusAtVisit());
        }

        aq.id(R.id.txtDangSigns).text(visitListData.getVisHResult());
        aq.id(R.id.txtbpval).text(visitListData.getVisHBP());
        aq.id(R.id.txtpulseval).text("" + visitListData.getVisHPulse());
        aq.id(R.id.txttempval).text("" + visitListData.getVisHTemperature());
        aq.id(R.id.txthbval).text("" + visitListData.getVisHHb());
        aq.id(R.id.txtproteinuriaval).text("" + visitListData.getVisHUrineprotein());

        if (visitListData.getVisHVisitIsAdviseGiven().equalsIgnoreCase(getResources().getString(R.string.yesshortform)))
            aq.id(R.id.chkisAdviceGiven).checked(true);
        if (visitListData.getVisHVisitParacetamolGiven().equalsIgnoreCase(getResources().getString(R.string.yesshortform)))
            aq.id(R.id.chkparacetamol).checked(true);
        if (visitListData.getVisHVisitAlbendazoleGiven().equalsIgnoreCase(getResources().getString(R.string.yesshortform)))
            aq.id(R.id.chkalbedazolegiven).checked(true);
        if (visitListData.getVisHVisitIFAGiven().equalsIgnoreCase(getResources().getString(R.string.yesshortform)))
            aq.id(R.id.chkifa).checked(true);

        if (visitListData.getVisHVisitIsReferred().equalsIgnoreCase(getResources().getString(R.string.yesshortform))) {
            aq.id(R.id.chkisreferred).checked(true);
            aq.id(R.id.txtreffacilitynameval).text("" + visitListData.getVisHVisitReferredToFacilityName());
        }

        aq.id(R.id.etvisitcomments).text("" + visitListData.getVisHVisitComments());

        aq.id(R.id.txtadvisesumm).text(visitListData.getVisHAdvise());
    }

    // Set Data for view based on item
    private void setData(List<TblVisitChildHeader> visitItems, int index) throws SQLException, Exception {

        aq.id(R.id.scrhomevisitsummarypnc).visible();
        aq.id(R.id.scrhomevisitsummary).gone();

        aq.id(R.id.txtVisitNum).text("" + visitItems.get(index).getVisitNum());
        aq.id(R.id.txtVisitDateval).text(visitItems.get(index).getVisHVisitDate());

        aq.id(R.id.txtgestage).text(getResources().getString(R.string.statusatvisit));
        aq.id(R.id.txtgestageval).text(DateTimeUtil.calculateAgeBetweenDates(tblChildInfo.getChlDateTimeOfBirth(), visitItems.get(index).getVisHVisitDate()));

        //mani AUG-18-2021
        if (visitItems.get(index).getVisCHAshaAvailable().equalsIgnoreCase("Y"))
            aq.id(R.id.ashavailability).text(getResources().getString(R.string.yes));
        else if (visitItems.get(index).getVisCHAshaAvailable().equalsIgnoreCase("N"))
            aq.id(R.id.ashavailability).text(getResources().getString(R.string.no));
        else if (visitItems.get(index).getVisCHAshaAvailable().equalsIgnoreCase("Dont Know"))
            aq.id(R.id.ashavailability).text(getResources().getString(R.string.dontknow));


        setPncAndNbSigns();


        aq.id(R.id.txtactionsumm).text("" + visitItems.get(index).getVisCHAdvise());

        if (visitItems.get(index).getVisHVisitIsReferred().equalsIgnoreCase(getResources().getString(R.string.yesshortform))) {
            aq.id(R.id.tblrefdetailspnc).visible();
            aq.id(R.id.txtreffacilitynamevalpnc).text("" + visitItems.get(index).getVisHVisitReferredToFacilityName());
            aq.id(R.id.txtreffacilitytypevalpnc).text("" + visitItems.get(index).getVisHVisitReferredToFacilityType());
            aq.id(R.id.txtrefslipnumbervalpnc).text("" + visitItems.get(index).getVisHReferralSlipNumber()); //13Apr2021 Bindu
        } else
            aq.id(R.id.tblrefdetailspnc).gone();

        aq.id(R.id.etvisitcommentspnc).text("" + visitItems.get(index).getVisHVisitComments());

        aq.id(R.id.txtweightvalpncbaby1).text("" + visitItems.get(index).getVisCHWeight());
        aq.id(R.id.txttweightdatevalpncbaby1).text("" + visitItems.get(index).getVisCHWeightDate());
        aq.id(R.id.txttempvalpncbaby1).text("" + visitItems.get(index).getVisCHTemperature());
        aq.id(R.id.txttempdatepncbaby1).text("" + visitItems.get(index).getVisCHTemperatureDate());
        aq.id(R.id.txtrespratevalpncbaby1).text("" + visitItems.get(index).getVisCHResprate());
        aq.id(R.id.txtrespratedatepncbaby1).text("" + visitItems.get(index).getVisCHResprateDate());
        aq.id(R.id.tblexaminationschild1).visible();
        if (visitItems.get(index).isVisCHRegAtGovtFac() == true) {
            aq.id(R.id.txtchildregidvalbaby1).text("" + visitItems.get(index).getVisCHRegGovtFacId());
            aq.id(R.id.txtchildregdatevalbaby1).text("" + visitItems.get(index).getVisCHRegAtGovtFacDate());
            aq.id(R.id.tblregatgovtfacchild1).visible();
        } else {
            aq.id(R.id.txtchildregidvalbaby1).text("");
            aq.id(R.id.txtchildregdatevalbaby1).text("");
            aq.id(R.id.tblregatgovtfacchild1).gone();
        }

//tablet details
        aq.id(R.id.txt_AVV_UsingIFA).getTextView().setText(visitItems.get(index).getChildvisHIsIFATaken().equals("Yes") ? getResources().getString(R.string.m121) : getResources().getString(R.string.m122));
        LinearLayout llIFA = findViewById(R.id.ll_AVV_IFA);
        if (visitItems.get(index).getChildvisHIsIFATaken().equals("Yes")) {
            llIFA.setVisibility(View.VISIBLE);
            aq.id(R.id.txt_AVV_ConsumeIFA).getTextView().setText(visitItems.get(index).getChildvisHIFADaily().equals("Yes") ? getResources().getString(R.string.m121) : getResources().getString(R.string.m122));
            if (visitItems.get(index).getChildvisHIFADaily().equals("Yes")) {
                TableRow trTabletsConsumePerDay = findViewById(R.id.trTabletsConsumePerDay);
                trTabletsConsumePerDay.setVisibility(View.VISIBLE);
                aq.id(R.id.txt_AVV_ConsumePerDay).getTextView().setText(visitItems.get(index).getChildvisHIFATabletsCount());
            } else {
                TableRow trIFAReason = findViewById(R.id.trIFAReason);
                trIFAReason.setVisibility(View.VISIBLE);
                String[] ifaReasons = getResources().getStringArray(R.array.ifareasons);
                String reasons = visitItems.get(index).getChildvisHIFANotTakenReason();
                String[] reasonAfterSplit = reasons.split(",");
                StringBuilder origonalReasons = new StringBuilder();
                if (reasons.length() > 0) {
                    for (String s : reasonAfterSplit) {
                        origonalReasons.append(ifaReasons[Integer.parseInt(s.trim())]).append(",");
                    }
                }
                origonalReasons = origonalReasons.deleteCharAt(origonalReasons.lastIndexOf(","));
                aq.id(R.id.txt_AVV_IFAReason).getTextView().setText(origonalReasons.toString());
            }
            int whogivenIndex = visitItems.get(index).getChildvisHIFAGivenBy();
            String[] whogave = getResources().getStringArray(R.array.whogaveifatablets);
            aq.id(R.id.txt_AVV_WhoGaveIFA).getTextView().setText(whogave[whogivenIndex]);
        } else {
            llIFA.setVisibility(View.GONE);
        }

        switch (visitItems.get(index).getChildvisHDewormingtab()) {
            case "Yes":
                aq.id(R.id.txt_AVV_Deworming).getTextView().setText(getResources().getString(R.string.m121));
                break;
            case "No":
                aq.id(R.id.txt_AVV_Deworming).getTextView().setText(getResources().getString(R.string.m122));
                break;
            default:
                aq.id(R.id.txt_AVV_Deworming).getTextView().setText(getResources().getString(R.string.dontknow));
        }


        getImages();
    }


    // Expand or Collapse Symtpoms drawable
    private void expandcollapseSymptoms() {
        if (aq.id(R.id.txtDangSigns).getText().toString().length() > 0) {
            aq.id(R.id.txtDangSignsL).getTextView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_colsym, 0, 0, 0);
        } else {
            aq.id(R.id.txtDangSignsL).getTextView().setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
    }

    // Expand or Collapse Advise drawable
    private void expandcollapseAdvise() {
        if (aq.id(R.id.txtadvisesumm).getText().toString().length() > 0) {
            aq.id(R.id.txtadvisegiven).getTextView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_colsym, 0, 0, 0);
        } else {
            aq.id(R.id.txtadvisegiven).getTextView().setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
    }

    // Expand and Collapse Danger Signs
    public void opencloseAccordionDangerSigns(View v, TextView txt) {
        try {

            if (v.getVisibility() == View.VISIBLE) {
                v.setVisibility(View.GONE);
                txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_expsym, 0, 0, 0);
            } else {
                v.setVisibility(View.VISIBLE);
                txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_colsym, 0, 0, 0);
            }
        } catch (Exception e) {

        }
    }

    /**
     * Initiates the Navigation drawer
     */
    private void initiateDrawer() {
        try {
            mDrawerLayout = findViewById(R.id.ett_drawer_layout);
            mDrawerList = findViewById(R.id.ett_left_drawer);


            navDrawerItems = new ArrayList<NavDrawerItem>();


            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.editchildinfo), R.drawable.ic_edit_60));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.viewparentdetails), R.drawable.ic_mother));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.gcmregisterlist), R.drawable.child_growth));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.chlhv), R.drawable.ic_homevisit));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.chlhvhistory), R.drawable.prenatalhomevisit)); // to be changed
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.sp_imm), R.drawable.immunisation));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.addsibling), R.drawable.general_examination_baby));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deactivate), R.drawable.deactivate));
            // set a custom shadow that overlays the main content when the drawer
            // opens


            mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

            mDrawerList.setAdapter(new NavDrawerListAdapter(getApplicationContext(), navDrawerItems));
            mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

            // ActionBarDrawerToggle ties together the the proper interactions
            // between the sliding drawer and the action bar app icon
            mDrawerToggle = new ActionBarDrawerToggle(this, /* host Activity */
                    mDrawerLayout, /* DrawerLayout object */
                    R.drawable.ic_drawer, /*
             * nav drawer image to replace 'Up'
             * caret
             */
                    R.string.drawer_open, /*
             * "open drawer" description for
             * accessibility
             */
                    R.string.drawer_close /*
             * "close drawer" description for
             * accessibility
             */
            ) {
                public void onDrawerClosed(View view) {
                    try {
                        displayDrawerItems();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                public void onDrawerOpened(View drawerView) {
                    try {
                        displayDrawerItems();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            mDrawerLayout.setDrawerListener(mDrawerToggle);
        } catch (Exception e) {

        }
    }

    // 18Sep2019 - Bindu Display drawer open close items
    private void displayDrawerItems() throws Exception {

        invalidateOptionsMenu(); // creates call to
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer1);
        getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);


        aq.id(R.id.ivWomanImg).getImageView().setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                mDrawerLayout.closeDrawer(mDrawerList);
            }
        });

        //pending
        aq.id(R.id.tvWomanName).text((tblChildInfo.getChlBabyname() + "(" + tblChildInfo.getChlGender() + ")"));
        if (tblChildInfo.getChlDateTimeOfBirth().trim().length() > 0) {
            aq.id(R.id.tvInfo1).text(
                    (getResources().getString(R.string.age) + " " +
                            DateTimeUtil.calculateAge(tblChildInfo.getChlDateTimeOfBirth())));
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        try {
            switch (item.getItemId()) {
                case android.R.id.home:
                    Intent home = new Intent(ChildHomeVisitViewDirect.this, MainMenuActivity.class);
                    home.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    /*startActivity(home);*/
                    displayAlert(getResources().getString(R.string.exit), home);
                    break;

                case R.id.wlist:
                    Intent wlist = new Intent(ChildHomeVisitViewDirect.this, RegisteredWomenActionTabs.class);
                    wlist.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    /*startActivity(wlist);*/
                    displayAlert(getResources().getString(R.string.exit), wlist);
                    break;

                case R.id.logout:
                    Intent logout = new Intent(ChildHomeVisitViewDirect.this, LoginActivity.class);
                    displayAlert(getResources().getString(R.string.m111), logout);
                    /*startActivity(logout);
                    CommonClass.updateLogoutTime();*/
                    break;

                case R.id.home:
                    Intent intent = new Intent(ChildHomeVisitViewDirect.this, MainMenuActivity.class);
                    intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    /*startActivity(intent);*/
                    displayAlert(getResources().getString(R.string.exit), intent);
                    break;
                //30Sep2019 - Bindu
                case R.id.about: {
                    Intent goToScreen = new Intent(ChildHomeVisitViewDirect.this, AboutActivity.class);
                    goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    displayAlert(getResources().getString(R.string.exit), goToScreen);
                    return true;
                }

            }
        } catch (Exception e) {

        }
        return super.onOptionsItemSelected(item);
    }

    private void displayAlert(final String spanText2, final Intent goToScreen) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set dialog message
        alertDialogBuilder.setMessage(spanText2).setCancelable(false).setNegativeButton(
                (getResources().getString(R.string.m121)), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (goToScreen != null) {
                            try {
                                startActivity(goToScreen);
                                if (spanText2.contains("logout")) {
                                    try {
                                        LoginRepository loginRepo = new LoginRepository(databaseHelper);
                                        loginRepo.updateLogoutTime(appState.loginAuditId, appState.sessionUserId);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            dialog.cancel();
                        }
                    }
                }).setPositiveButton((getResources().getString(R.string.m122)),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    /**
     * Initializes the Action Bar items
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        try {
            //16Sep2019 - Bindu
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer1);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);

            aq.id(R.id.tvWomanName).text((tblChildInfo.getChlBabyname() + "(" + tblChildInfo.getChlGender() + ")"));
            if (tblChildInfo.getChlDateTimeOfBirth().trim().length() > 0) {
                aq.id(R.id.tvInfo1).text(
                        (getResources().getString(R.string.age) + " " +
                                DateTimeUtil.calculateAge(tblChildInfo.getChlDateTimeOfBirth())));
            }

            aq.id(R.id.ivWomanImg).gone();

            getMenuInflater().inflate(R.menu.servicesmenu, menu);

            menu.findItem(R.id.logout).setTitle((getResources().getString(R.string.action_logout)));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    /**
     * The click listner for ListView in the navigation drawer
     */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            mDrawerLayout.closeDrawer(mDrawerList);
            //   if (woman.getRegADDate() == null || woman.getRegADDate().length() == 0) {
            try {
                switch (position) {

                    case 0:
                        if (tblChildInfo.getChlReg() == 1) {
                            Intent nextScreen = new Intent(ChildHomeVisitViewDirect.this, EditRegChildInfoActivity.class);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                            nextScreen.putExtra("tblChildInfo", tblChildInfo);
                            startActivity(nextScreen);
                        } else {
                            Intent nextScreen = new Intent(ChildHomeVisitViewDirect.this, EditChildActivity.class);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                            nextScreen.putExtra("tblChildInfo", tblChildInfo);
                            startActivity(nextScreen);
                        }
                        break;

                    case 1: {
                        if (tblChildInfo.getChlParentId() != null && tblChildInfo.getChlParentId().trim().length() > 0) {
                            Intent viewParent = new Intent(ChildHomeVisitViewDirect.this, ViewParentDetails.class);
                            viewParent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            viewParent.putExtra("appState", getIntent().getBundleExtra("appState"));
                            viewParent.putExtra("tblChildInfo", tblChildInfo);
                            startActivity(viewParent);
                        } else {
                            WomanRepository womanRepository = new WomanRepository(databaseHelper);
                            tblregisteredwomen woman = womanRepository.getRegistartionDetails(tblChildInfo.getWomanId(), tblChildInfo.getUserId());
                            appState.selectedWomanId = tblChildInfo.getWomanId();
                            Intent nextScreen = new Intent(ChildHomeVisitViewDirect.this, ViewProfileActivity.class);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                            nextScreen.putExtra("woman", woman);
                            startActivity(nextScreen);
                        }
                        break;
                    }


                    case 2: {
                        Intent nextScreen = new Intent(ChildHomeVisitViewDirect.this, CGMList.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                        nextScreen.putExtra("tblChildInfo", tblChildInfo);
                        startActivity(nextScreen);

                        break;
                    }
                    case 3: {
                        Intent nextScreen = new Intent(ChildHomeVisitViewDirect.this, ChildHomeVisit.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                        nextScreen.putExtra("tblChildInfo", tblChildInfo);
                        startActivity(nextScreen);
                        break;
                    }
                    case 4: {
                        Intent nextScreen = new Intent(ChildHomeVisitViewDirect.this, ChildHomeVisitHistory.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                        nextScreen.putExtra("tblChildInfo", tblChildInfo);
                        startActivity(nextScreen);

                        break;
                    }


                    case 5: {
                        if (tblChildInfo.getChlDeliveryResult() <= 1) {
                            Intent nextScreen = new Intent(ChildHomeVisitViewDirect.this, ImmunizationListActivity.class);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                            nextScreen.putExtra("tblChildInfo", tblChildInfo);
                            startActivity(nextScreen);
                        } else
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.m053), Toast.LENGTH_LONG).show();
                        break;
                    }

                    case 6: {
                        UserRepository userRepository = new UserRepository(databaseHelper);
                        user = userRepository.getOneAuditedUser(appState.ashaId);
                        if (user.getIsDeactivated() == 1)
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.userdeactivated), Toast.LENGTH_LONG).show();
                        else {
                            Intent addSibling = new Intent(ChildHomeVisitViewDirect.this, AddSiblingsActivity.class);
                            addSibling.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            addSibling.putExtra("appState", getIntent().getBundleExtra("appState"));
                            if (tblChildInfo.getChlReg() == 1)
                                addSibling.putExtra("chlParentId", tblChildInfo.getChlParentId());
                            else
                                addSibling.putExtra("WomanId", tblChildInfo.getWomanId());
                            startActivity(addSibling);
                        }
                        break;
                    }

                    case 7: {
                        Intent nextScreen = new Intent(ChildHomeVisitViewDirect.this, ChildDeactivateActivity.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                        nextScreen.putExtra("tblChildInfo", tblChildInfo);
                        startActivity(nextScreen);

                        break;
                    }
                }
            } catch (Exception e) {

            }

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        /*
         * You'll need this in your class to release the helper when done.
         */
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

    @Override
    public void onBackPressed() {

    }


    //    15May2021 Arpitha
    private void setPncAndNbSigns() throws SQLException {

        HomeVisitRepository homeVisitRepository = new HomeVisitRepository(databaseHelper);

        ArrayList<String> listChild = homeVisitRepository.getHomeVisitSymptomsChild(tblChildInfo.getUserId(), tblChildInfo.getChildID(), visitItems.get(index).getVisitNum());
        String symptomChild = "";
        if (listChild != null && listChild.size() > 0) {

            for (int i = 0; i < listChild.size(); i++) {
                String strSy = listChild.get(i);
                if (strSy.contains("save"))
                    strSy = strSy.replace("save", "");
                int identifier = getResources().getIdentifier
                        (strSy,
                                "string", "com.sc.stmansi");
                if (identifier > 0) {
                    if (i == listChild.size() - 1)
                        symptomChild = symptomChild + getResources().getString(identifier);
                    else
                        symptomChild = symptomChild + getResources().getString(identifier) + ",";
                } else {
                    if (i == listChild.size() - 1)
                        symptomChild = symptomChild + strSy;
                    else
                        symptomChild = symptomChild + strSy + ",";
                }

            }
        }
        aq.id(R.id.txtDangSignsNb).text(symptomChild);
    }


    private void getImages() {
        try {
            RecyclerView recyclerViewImage;
            LinearLayout llphotos;
            recyclerViewImage = findViewById(R.id.recyclerImagepnc);
            llphotos = findViewById(R.id.llPhotospnc);

            ImageStoreRepository imageStoreRepository = new ImageStoreRepository(databaseHelper);
            String str = childId + "_HV" + (index + 1);
            ArrayList<tblImageStore> allImages = imageStoreRepository.getAllImagesofIDandHomeVisit(str);
            if (allImages.size() != 0) {
                llphotos.setVisibility(View.VISIBLE);
                recyclerViewImage.setVisibility(View.VISIBLE);
                DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
                float screenWidthDp = displayMetrics.widthPixels / displayMetrics.density;
                int noOfColumns = (int) (screenWidthDp / 125 + 0.5); // +0.5 for correct rounding to int.
                GridLayoutManager layoutManager = new GridLayoutManager(ChildHomeVisitViewDirect.this, noOfColumns);
                recyclerViewImage.setLayoutManager(layoutManager);
                recyclerViewImage.setNestedScrollingEnabled(false);
                recyclerViewImage.setHasFixedSize(true);
                RecyclerViewAdapter recyclerViewAdapter = new RecyclerViewAdapter(ChildHomeVisitViewDirect.this, allImages, databaseHelper);
                recyclerViewImage.setAdapter(recyclerViewAdapter);
            } else {
                llphotos.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
