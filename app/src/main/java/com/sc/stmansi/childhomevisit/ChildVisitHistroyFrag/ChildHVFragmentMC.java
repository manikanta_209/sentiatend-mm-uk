package com.sc.stmansi.childhomevisit.ChildVisitHistroyFrag;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.legacy.app.ActionBarDrawerToggle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidquery.AQuery;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.R;
import com.sc.stmansi.childhomevisit.ChildHomeVisitAdapter;
import com.sc.stmansi.childhomevisit.ChildHomeVisitView;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.configuration.SyncState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.recyclerview.ClickListener;
import com.sc.stmansi.repositories.CaseMgmtRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.tables.TblChildInfo;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblCaseMgmt;

import java.sql.SQLException;
import java.util.List;


public class ChildHVFragmentMC extends Fragment implements ClickListener {

    AQuery aqChlHvCC;
    TblChildInfo tblChildInfo;
    DatabaseHelper databaseHelper;
    TblInstusers user;
    List<tblCaseMgmt> tblcasemgmtslist;
    tblCaseMgmt tblCaseMgmtdata;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private AppState appState;
    private RecyclerView.Adapter childHomeVisitAdapter;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private int itemPosition;
    private View parentview;
    String childId;
    private SyncState syncState;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        parentview = inflater.inflate(R.layout.fragment_child_h_v_m_c, container, false);
        aqChlHvCC = new AQuery(parentview);
        databaseHelper = getHelper();

        //Get data from intent
        Bundle bundle = this.getArguments();
        appState = bundle.getParcelable("appState");
        syncState = bundle.getParcelable("syncState");
        childId = this.getArguments().getString("childId");
        tblChildInfo=(TblChildInfo)bundle.getSerializable("tblChildInfo");
        recyclerView = (RecyclerView) aqChlHvCC.id(R.id.rvChlHomevisitMC).getView();

        return parentview;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
            initialValues();
            setData();

        } catch (Exception throwables) {
            throwables.printStackTrace();
        }

    }

    private void setData() throws Exception {
        recyclerView.setHasFixedSize(true);
        // use a linear layout manager
        layoutManager = new LinearLayoutManager(parentview.getContext());
        recyclerView.setLayoutManager(layoutManager);

        CaseMgmtRepository caseMgmtRepository = new CaseMgmtRepository(databaseHelper);
        tblcasemgmtslist = caseMgmtRepository.getChildHomeVisitHistory(childId, getResources().getString(R.string.mcusertype));

        if (tblcasemgmtslist.size() != 0) {
            aqChlHvCC.id(R.id.txtNoRecordsMC).getTextView().setVisibility(View.GONE);
            childHomeVisitAdapter = new ChildHomeVisitAdapter(parentview.getContext(), this, tblcasemgmtslist);
            recyclerView.setAdapter(childHomeVisitAdapter);
        } else {
            aqChlHvCC.id(R.id.txtNoRecordsMC).getTextView().setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }


    }

    private void initialValues() throws SQLException {
        UserRepository userRepository = new UserRepository(databaseHelper);
        user = userRepository.getOneAuditedUser(appState.ashaId);
        recyclerView = parentview.findViewById(R.id.rvChlHomevisitMC);
    }

    @Override
    public void onClick(View v) {
        setSelectedVisitDetails(v);
        Intent intent = new Intent(getContext(), ChildHomeVisitView.class);
        intent.putExtra("globalState", this.getArguments());
        intent.putExtra("pos", itemPosition);
        intent.putExtra("childId", childId);
        intent.putExtra("tblChildInfo",tblChildInfo);
        intent.putExtra("usertype",getResources().getString(R.string.mcusertype)); //12Jul2021 Bindu add usertype
        startActivity(intent);
    }

    private void setSelectedVisitDetails(View v) {
        itemPosition = recyclerView.getChildLayoutPosition(v);
        tblCaseMgmtdata = tblcasemgmtslist.get(itemPosition);
    }

    @Override
    public boolean onLongClick(View v) {
        return false;
    }
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(parentview.getContext(), DatabaseHelper.class);
        }
        return databaseHelper;
    }
}