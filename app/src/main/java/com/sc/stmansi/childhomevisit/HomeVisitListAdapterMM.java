package com.sc.stmansi.childhomevisit;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.R;
import com.sc.stmansi.childhomevisit.ChildVisitHistroyFrag.ChildHVFragmentMM;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.complreferralmanagement.ComplMgmtVisitViewActivity;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.recyclerview.ClickListener;
import com.sc.stmansi.recyclerview.HomeVisitListViewHolder;
import com.sc.stmansi.repositories.CaseMgmtRepository;
import com.sc.stmansi.repositories.ComplicationMgmtRepository;
import com.sc.stmansi.repositories.CovidDetailsRepository;
import com.sc.stmansi.repositories.HomeVisitRepository;
import com.sc.stmansi.tables.TblChildInfo;
import com.sc.stmansi.tables.TblVisitChildHeader;
import com.sc.stmansi.tables.TblVisitHeader;
import com.sc.stmansi.tables.tblCaseMgmt;
import com.sc.stmansi.tables.tblCovidTestDetails;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class HomeVisitListAdapterMM extends RecyclerView.Adapter<HomeVisitListViewHolder> {

    private Context context;
    private List<TblVisitChildHeader> visits;
    private ClickListener viewClickListener;
    AppState appState;
    DatabaseHelper databaseHelper;
    ComplicationMgmtRepository comprepo;
    TblChildInfo tblChildInfos;

    public HomeVisitListAdapterMM(Context context, List<TblVisitChildHeader> visits, ClickListener viewClickListener) {
        this.context = context;
        this.visits = visits;
        this.viewClickListener = viewClickListener;
    }

    public HomeVisitListAdapterMM(Context homeVisitListActivity, List<TblVisitChildHeader> visitHeaderList,
                                  ClickListener viewClickListener, AppState appState) {
        this.viewClickListener = viewClickListener;
        this.visits = visitHeaderList;
        this.appState = appState;
        this.context = homeVisitListActivity;
        databaseHelper = getHelper();
        comprepo = new ComplicationMgmtRepository(databaseHelper);
    }

    public HomeVisitListAdapterMM(Context homeVisitListActivity, List<TblVisitChildHeader> visitHeaderList,
                                  ClickListener viewClickListener, AppState appState, TblChildInfo tblChildInfo) {
        this.viewClickListener = viewClickListener;
        this.visits = visitHeaderList;
        this.appState = appState;
        this.context = homeVisitListActivity;
        databaseHelper = getHelper();
        comprepo = new ComplicationMgmtRepository(databaseHelper);
        tblChildInfos = tblChildInfo;
    }

    @NonNull
    @Override
    public HomeVisitListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.childhomevisit_recycler_item, viewGroup, false);
        linearLayout.setOnClickListener(viewClickListener);
        return new HomeVisitListViewHolder(linearLayout);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeVisitListViewHolder homeVisitViewHolder, int i) {
       final TblVisitChildHeader visitHeader = visits.get(i);
        homeVisitViewHolder.txtVisitNum.setText("# "+visitHeader.getVisitNum());
        homeVisitViewHolder.txtVisitDate.setText(visitHeader.getVisHVisitDate());


        homeVisitViewHolder.txtStatus.setText(context.getResources().getString(R.string.statusatvisit) + " : " + DateTimeUtil.calculateAgeBetweenDates(tblChildInfos.getChlDateTimeOfBirth(),visitHeader.getVisHVisitDate()));


        if(visitHeader.getVisHVisitIsReferred().equalsIgnoreCase(context.getResources().getString(R.string.yesshortform)))
            homeVisitViewHolder.imgVisitOutcome.setImageResource(R.drawable.ic_refer);
        else if(visitHeader.getVisCHCompl().equalsIgnoreCase("Y"))
            homeVisitViewHolder.imgVisitOutcome.setImageResource(R.drawable.ic_hvcompl);
        else
            homeVisitViewHolder.imgVisitOutcome.setImageResource(R.drawable.ic_hvnocompl);


        if(visitHeader.getVisHVisitReferredToFacilityType().length() > 0 || visitHeader.getVisHVisitReferredToFacilityName().length() > 0)
            homeVisitViewHolder.txtReferredToFacname.setText(context.getResources().getString(R.string.reffacilityname) + visitHeader.getVisHVisitReferredToFacilityType() + " - " + visitHeader.getVisHVisitReferredToFacilityName());
        else//21May2021 Arpitha
            homeVisitViewHolder.txtReferredToFacname.setText("");


        homeVisitViewHolder.imgVisitOutcome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {


                    if (visitHeader.getVisHChildSymptoms() != null && visitHeader.getVisHChildSymptoms().length() > 0) {
                        String title = context.getResources().getString(R.string.newborndangersigns);



                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                        //View convertView = LayoutInflater.from(context).inflate(R.layout.symptoms_displaylist, null, false);
                        View convertView = LayoutInflater.from(context).inflate(R.layout.child_custom_hv_dialog, null, false);

                        TextView txtnbdangersigns = convertView.findViewById(R.id.txtnbdangersignval);
                        LinearLayout llnbdangersigns = convertView.findViewById(R.id.llnbds);



                        if (visitHeader.getVisHChildSymptoms() != null && visitHeader.getVisHChildSymptoms().length() > 0) {
                            txtnbdangersigns.setText(setChildCompl(i));//15May2021 Arpitha
                            llnbdangersigns.setVisibility(View.VISIBLE);
                        } else {
                            llnbdangersigns.setVisibility(View.GONE);
                        }

                        alertDialog.setView(convertView).setCancelable(true).setTitle(title)
                                .setPositiveButton((context.getResources().getString(R.string.ok)), new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                });
                        // lv.setAdapter(adapter);
                        alertDialog.show();
                    }
                }catch (Exception e)
                {

                }
            }
        });

//
//        try {
//            if (comprepo.getComplMgmtVisitDetailsWoman(visitHeader.getWomanId(),visitHeader.getVisitNum())) {
//                homeVisitViewHolder.btnactiontaken.setVisibility(View.VISIBLE);
//                homeVisitViewHolder.btnactiontaken.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        Intent intent = new Intent(context, ComplMgmtVisitViewActivity.class);
//                        intent.putExtra("globalState", prepareBundle());
//                        intent.putExtra("complVisitNum", visitHeader.getVisitNum()); //20Dec2019 - Bindu - Add visit num
//                        context.startActivity(intent);
//                    }
//                });
//            } else {
//                homeVisitViewHolder.btnactiontaken.setVisibility(View.GONE);
//            }
//        }catch (Exception e) {
//            e.printStackTrace();
//        }

        try {
            //27Jun2021 Bindu - check follow up or action taken
            boolean isfollowup = false;
            CaseMgmtRepository caseMgmtRepository = new CaseMgmtRepository(databaseHelper);
            tblCaseMgmt currentCaseDetails = null;
            currentCaseDetails = caseMgmtRepository.getCaseMgmtDetailsChildNew(visitHeader.getChildId(), ""+visitHeader.getVisitNum(), appState.sessionUserId, visitHeader.getVisHVisitDate());
            //mc
            if(currentCaseDetails != null && currentCaseDetails.getVisitNumbyMM() == visitHeader.getVisitNum()){
                isfollowup = false;
                homeVisitViewHolder.btnactiontakenmc.setVisibility(View.VISIBLE);
                homeVisitViewHolder.btnactiontakenmc.setText(context.getResources().getString(R.string.actiontakenmc));
            }  else {
                homeVisitViewHolder.btnactiontakenmc.setVisibility(View.GONE);
            }

            //cc
            tblCaseMgmt currentCaseDetailsCC = caseMgmtRepository.getCaseMgmtDetailsChildNewCC(visitHeader.getChildId(), ""+visitHeader.getVisitNum(), visitHeader.getVisHVisitDate(),"CC");
            if(currentCaseDetailsCC != null && currentCaseDetailsCC.getVisitNumbyMM() == visitHeader.getVisitNum()){
                homeVisitViewHolder.btnactiontaken.setVisibility(View.VISIBLE);
                homeVisitViewHolder.btnactiontaken.setText(context.getResources().getString(R.string.actiontakencc));
            }  else {
                homeVisitViewHolder.btnactiontaken.setVisibility(View.GONE);
            }

            boolean finalIsfollowup = isfollowup;
            homeVisitViewHolder.btnactiontakenmc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!finalIsfollowup) {
                        try{
                            tblCaseMgmt CCaseDetails = caseMgmtRepository.getCaseMgmtDetailsChildNew(visitHeader.getChildId(), "" + visitHeader.getVisitNum(), appState.sessionUserId, visitHeader.getVisHVisitDate());
                            List<tblCaseMgmt> tblCaseMgmtList = caseMgmtRepository.getChildHomeVisitHistory(visitHeader.getChildId(), context.getResources().getString(R.string.mcusertype)); // 25Sep2021 Bindu - change from CC to MC and also add from string file
                            for (tblCaseMgmt s: tblCaseMgmtList){
                                if (s.getVisitNumbyMM() == CCaseDetails.getVisitNumbyMM()){
                                    Intent intent = new Intent(context, ChildHomeVisitView.class);
                                    intent.putExtra("globalState", prepareBundle());
                                    intent.putExtra("complVisitNum", visitHeader.getVisitNum());
                                    intent.putExtra("pos",tblCaseMgmtList.indexOf(s));
                                    intent.putExtra("tblChildInfo",tblChildInfos);
                                    intent.putExtra("usertype",context.getResources().getString(R.string.mcusertype)); //25Sep2021 Bindu pass usertype
                                    context.startActivity(intent);
                                }
                            }
                        }catch (Exception e){
                            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                            crashlytics.log(e.getMessage());                        }
                    }else {

                        Bundle b = new Bundle();
                        b.putInt("complVisitNum", visitHeader.getVisitNum());
                        b.putString("complVisitDate",visitHeader.getVisHVisitDate());
                        b.putString("complvisittype","Followup");


                        Intent nextScreen = new Intent(context, ChildHomeVisit.class);
                        nextScreen.putExtra("globalState", prepareBundle());
                        nextScreen.putExtra("tblChildInfo",tblChildInfos);
                        nextScreen.putExtra("child", "child");
                        nextScreen.putExtras(b);
                        context.startActivity(nextScreen);



                    }
                }
            });


            homeVisitViewHolder.btnactiontaken.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!finalIsfollowup) {
                        try{
                            tblCaseMgmt CCaseDetails = caseMgmtRepository.getCaseMgmtDetailsChildNewCC(visitHeader.getChildId(), "" + visitHeader.getVisitNum(), appState.sessionUserId, visitHeader.getVisHVisitDate());
                            List<tblCaseMgmt> tblCaseMgmtList = caseMgmtRepository.getChildHomeVisitHistory(visitHeader.getChildId(), context.getResources().getString(R.string.mcusertype)); // 25Sep2021 Bindu - change from CC to MC and also add from string file
                            for (tblCaseMgmt s: tblCaseMgmtList){
                                if (s.getVisitNumbyMM() == CCaseDetails.getVisitNumbyMM()){
                                    Intent intent = new Intent(context, ChildHomeVisitView.class);
                                    intent.putExtra("globalState", prepareBundle());
                                    intent.putExtra("complVisitNum", visitHeader.getVisitNum());
                                    intent.putExtra("pos",tblCaseMgmtList.indexOf(s));
                                    intent.putExtra("tblChildInfo",tblChildInfos);
                                    intent.putExtra("usertype",context.getResources().getString(R.string.ccusertype)); //25Sep2021 Bindu pass usertype
                                    context.startActivity(intent);
                                }
                            }
                        }catch (Exception e){
                            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                            crashlytics.log(e.getMessage());                        }
                    }else {

                        Bundle b = new Bundle();
                        b.putInt("complVisitNum", visitHeader.getVisitNum());
                        b.putString("complVisitDate",visitHeader.getVisHVisitDate());
                        b.putString("complvisittype","Followup");


                        Intent nextScreen = new Intent(context, ChildHomeVisit.class);
                        nextScreen.putExtra("globalState", prepareBundle());
                        nextScreen.putExtra("tblChildInfo",tblChildInfos);
                        nextScreen.putExtra("child", "child");
                        nextScreen.putExtras(b);
                        context.startActivity(nextScreen);



                    }
                }
            });
        }catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }


    }


    @Override
    public int getItemCount() {
        return visits.size();
    }

    //    prepare bundle
    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        return b;
    }

    //    initialize db object
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
        }
        return databaseHelper;
    }


    //    16May2021 Arpitha
    private String setChildCompl(int pos) throws SQLException {
        HomeVisitRepository homeVisitRepository = new HomeVisitRepository(databaseHelper);
        ArrayList<String> listChild = homeVisitRepository.getHomeVisitSymptomsChild(visits.get(pos).getUserId(),
                visits.get(pos).getChildId(),visits.get(pos).getVisitNum());        String symptomChild = "";
        if(listChild!=null && listChild.size()>0) {

            for(int i=0;i<listChild.size();i++) {
                String strSy = listChild.get(i);
                if(strSy.contains("save"))
                    strSy = strSy.replace("save","");
                int identifier = context.getResources().getIdentifier
                        (strSy,
                                "string", "com.sc.stmansi");
                if(identifier>0) {
                    if (i == listChild.size() - 1)
                        symptomChild = symptomChild + context.getResources().getString(identifier);
                    else
                        symptomChild = symptomChild + context.getResources().getString(identifier) + ",";
                }else {
                    if (i == listChild.size() - 1)
                        symptomChild = symptomChild + strSy;
                    else
                        symptomChild = symptomChild + strSy + ",";
                }

            }
        }
        return symptomChild;
    }

}
