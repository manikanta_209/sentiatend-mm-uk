package com.sc.stmansi.childhomevisit.ChildVisitHistroyFrag;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.sc.stmansi.R;
import com.sc.stmansi.tables.TblChildInfo;

public class ChildHomVisitHistoryPageAdapter extends FragmentPagerAdapter {
    Context context;
    String childId;
    Bundle bundle;
    TblChildInfo tblChildInfo;

    public ChildHomVisitHistoryPageAdapter(@NonNull FragmentManager fm, Context context, String childId, Bundle bundle, TblChildInfo tblChildInfo) {
        super(fm);
        this.context = context;
        this.childId = childId;
        this.bundle = bundle;
        this.tblChildInfo = tblChildInfo;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        try{
            switch (position){
                case 0:
                    ChildHVFragmentMM fragmentMM = new ChildHVFragmentMM();
                    bundle.putString("childId",childId);
                    bundle.putSerializable("tblChildInfo",tblChildInfo);
                    fragmentMM.setArguments(bundle);
                    return fragmentMM;
                case 1:
                    ChildHVFragmentCC fragmentCC = new ChildHVFragmentCC();
                    bundle.putString("childId",childId);
                    bundle.putSerializable("tblChildInfo",tblChildInfo);
                    fragmentCC.setArguments(bundle);
                    return fragmentCC;
                case 2:
                    ChildHVFragmentMC fragmentMC = new ChildHVFragmentMC();
                    bundle.putString("childId",childId);
                    bundle.putSerializable("tblChildInfo",tblChildInfo);
                    fragmentMC.setArguments(bundle);
                    return fragmentMC;
            }
        }catch (Exception e){
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position)
    {
        String title = null;
        if (position == 0)
            title = context.getResources().getString(R.string.userlevel_mm);
        else if (position == 1)
            title = context.getResources().getString(R.string.userlevel_cc);
        else if (position == 2)
            title = context.getResources().getString(R.string.userlevel_mc);

        return title;
    }
}

