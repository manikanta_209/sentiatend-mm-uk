package com.sc.stmansi.childgrowthmonitor;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.legacy.app.ActionBarDrawerToggle;

import com.androidquery.AQuery;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.About.AboutActivity;
import com.sc.stmansi.R;
import com.sc.stmansi.childregistration.AddSiblingsActivity;
import com.sc.stmansi.childregistration.EditChildActivity;
import com.sc.stmansi.childregistration.EditRegChildInfoActivity;
import com.sc.stmansi.childregistration.ViewParentDetails;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.common.NavDrawerItem;
import com.sc.stmansi.common.NavDrawerListAdapter;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.deactivate.ChildDeactivateActivity;
import com.sc.stmansi.immunization.ImmunizationListActivity;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.repositories.CGMRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.tables.TblChildInfo;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblbmiforage;
import com.sc.stmansi.tables.tblchildgrowth;
import com.sc.stmansi.tables.tblheightforage;
import com.sc.stmansi.tables.tblmuacforage;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.tables.tblweightforage;
import com.sc.stmansi.viewprofile.ViewProfileActivity;
import com.sc.stmansi.womanlist.RegisteredWomenActionTabs;

import java.io.ByteArrayOutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BoysGraph extends AppCompatActivity implements View.OnClickListener {

    LineChart mLineChart, mLineChartHeight, mLineChartBmi, mLineChartHeadcf;
    Integer i;
    Bitmap bitmapweight, bitmapheight, bitmapbmi, bitmapheadcf;
    byte[] byteArrayweight, byteArrayHeight, byteArrayBMI, byteArrayHeadcf;
    List<tblweightforage> tblweightforageList;
    List<tblheightforage> tblheightforageList;
    List<tblbmiforage> tblbmiforageList;
    List<tblchildgrowth> tblchildgrowthList;
    List<tblmuacforage> tblheadcfforagesList;
    TblChildInfo tblChildInfo;
    AQuery aqChildGraph;
    RelativeLayout rlWeight, rlHeight, rlBmi, rlHeadcf;
    TblInstusers user;
    int a = 0, b = 0, c = 0, d = 0;
    RelativeLayout relativeLayoutHeadcf;
    private DatabaseHelper databaseHelper;
    private AppState appState;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cgm_graph);

        try {
            aqChildGraph = new AQuery(this);
            databaseHelper = getHelper();

            //Get data from intent
            Bundle bundle = getIntent().getBundleExtra("globalState");
            tblChildInfo = (TblChildInfo) getIntent().getSerializableExtra("tblChildInfo");
            appState = bundle.getParcelable("appState");

            UserRepository userRepository = new UserRepository(databaseHelper);
            user = userRepository.getOneAuditedUser(appState.ashaId);

//            View convertView = LayoutInflater.from(this).inflate(R.layout.activity_cgm_graph, null, false);
//             relativeLayoutHeadcf = convertView.findViewById(R.id.rlHeadcf);

            mDrawerLayout = findViewById(R.id.ett_drawer_layout);
            mDrawerList = findViewById(R.id.ett_left_drawer);

            initiateDrawer();
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

            mLineChart = findViewById(R.id.cgmGraphWeight);
            mLineChartHeight = findViewById(R.id.cgmGraphHeight);
            mLineChartBmi = findViewById(R.id.cgmGraphBMI);
            mLineChartHeadcf = findViewById(R.id.cgmGraphHeadCf);


            rlWeight = findViewById(R.id.rlWeight);
            rlHeight = findViewById(R.id.rlHeight);
            rlBmi = findViewById(R.id.rlBmi);
            rlHeadcf = findViewById(R.id.rlHeadcf);


            setOnClickListeners();

            getData(tblChildInfo.getChildID());

            setBackgroundColor();

            getWeightForAge();
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }

    }

    private void setBackgroundColor() {
        if (tblChildInfo.getChlGender().equalsIgnoreCase("Female")) {
            mDrawerLayout.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.pink));
        }
    }


    private void getWeightForAge() {
        String[] legendNames = new String[]{"Normal", "Moderately Underweight", "Severely Underweight"};




        aqChildGraph.id(R.id.btnWeight).getButton().setBackgroundColor(getResources().getColor(R.color.gray));
        aqChildGraph.id(R.id.btnHeight).getButton().setBackgroundColor(getResources().getColor(R.color.appdarkcolor));
        aqChildGraph.id(R.id.btnBMI).getButton().setBackgroundColor(getResources().getColor(R.color.appdarkcolor));
        aqChildGraph.id(R.id.btnHeadcf).getButton().setBackgroundColor(getResources().getColor(R.color.appdarkcolor));

        int[] colorArray = new int[]{getApplicationContext().getResources().getColor(R.color.parratgreen), getApplicationContext().getResources().getColor(R.color.graphyellow),
                getApplicationContext().getResources().getColor(R.color.darkorange)};
        rlWeight.setVisibility(View.VISIBLE);
        rlHeight.setVisibility(View.GONE);
        rlBmi.setVisibility(View.GONE);
        rlHeadcf.setVisibility(View.GONE);

        String[] monthsArray = new String[tblweightforageList.size()];
//        String[] weightArray = new String[32];
        LineDataSet lineDataSetMedian = null, lineDataSetFirstNegSd = null, lineDataSetSecondNegSd = null, lineDataSetWeight = null;

        if (tblChildInfo.getChlGender().equalsIgnoreCase("Male")) {
            lineDataSetMedian = new LineDataSet(medianData(), "Median");
            lineDataSetFirstNegSd = new LineDataSet(firstNegativeSD(), "First Negative SD");
            lineDataSetSecondNegSd = new LineDataSet(secondNegativeSD(), "Second Negative SD");
            lineDataSetWeight = new LineDataSet(weightForAgeOfChild(), "Weight For Age");
        } else if (tblChildInfo.getChlGender().equalsIgnoreCase("Female")) {
            lineDataSetMedian = new LineDataSet(medianDataGirls(), "Median");
            lineDataSetFirstNegSd = new LineDataSet(firstNegativeSDGirls(), "First Negative SD");
            lineDataSetSecondNegSd = new LineDataSet(secondNegativeSDGirls(), "Second Negative SD");
            lineDataSetWeight = new LineDataSet(weightForAgeOfChildGirls(), "Weight For Age");
        }


        lineDataSetMedian.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        lineDataSetMedian.setDrawCircles(false);
        lineDataSetMedian.setDrawCircleHole(false);
        lineDataSetMedian.setColor(getApplicationContext().getResources().getColor(R.color.parratgreen));
        lineDataSetMedian.setLineWidth(1f);
        lineDataSetMedian.setFillAlpha(255);
        lineDataSetMedian.setDrawFilled(true);
        lineDataSetMedian.setFillColor(getResources().getColor(R.color.parratgreen));


//        lineDataSetFirstPosSd.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        lineDataSetFirstPosSd.setDrawCircles(false);
//        lineDataSetFirstPosSd.setColor(getApplicationContext().getResources().getColor(R.color.amber));
//        lineDataSetFirstPosSd.setLineWidth(1f);
//        lineDataSetSecondPosSd.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        lineDataSetSecondPosSd.setDrawCircles(false);
//        lineDataSetSecondPosSd.setColor(getApplicationContext().getResources().getColor(R.color.red));
//        lineDataSetSecondPosSd.setLineWidth(1f);
//
//        lineDataSetThirdPosSd.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        lineDataSetThirdPosSd.setDrawCircles(false);
//        lineDataSetThirdPosSd.setColor(getApplicationContext().getResources().getColor(R.color.black));
//        lineDataSetThirdPosSd.setLineWidth(1f);

        lineDataSetWeight.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        lineDataSetWeight.setColor(getApplicationContext().getResources().getColor(R.color.navyblue));
        lineDataSetWeight.setLineWidth(4f);
        lineDataSetWeight.setDrawCircles(true);
        lineDataSetWeight.setCircleColor(getApplicationContext().getResources().getColor(R.color.navyblue));
        lineDataSetWeight.setCircleHoleColor(getApplicationContext().getResources().getColor(R.color.white));
        lineDataSetWeight.setCircleRadius(5f);

        //Negative
        lineDataSetFirstNegSd.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        lineDataSetFirstNegSd.setDrawCircles(false);
        lineDataSetFirstNegSd.setColor(getApplicationContext().getResources().getColor(R.color.graphyellow));
        lineDataSetFirstNegSd.setLineWidth(1f);
        lineDataSetFirstNegSd.setFillAlpha(255);
        lineDataSetFirstNegSd.setDrawFilled(true);
        lineDataSetFirstNegSd.setFillColor(getApplicationContext().getResources().getColor(R.color.graphyellow));


        lineDataSetSecondNegSd.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        lineDataSetSecondNegSd.setDrawCircles(false);
        lineDataSetSecondNegSd.setColor(getApplicationContext().getResources().getColor(R.color.darkorange));
        lineDataSetSecondNegSd.setLineWidth(1f);
        lineDataSetSecondNegSd.setFillAlpha(255);
        lineDataSetSecondNegSd.setDrawFilled(true);
        lineDataSetSecondNegSd.setFillColor(getResources().getColor(R.color.darkorange));


        //Add dataset here
        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(lineDataSetMedian);
        dataSets.add(lineDataSetFirstNegSd);
        dataSets.add(lineDataSetSecondNegSd);
        dataSets.add(lineDataSetWeight);

        LineData data = new LineData(dataSets);
        mLineChart.setData(data);
        mLineChart.invalidate();
        //Customise chart
        mLineChart.setDrawBorders(true);
        mLineChart.setBorderColor(Color.BLACK);
        mLineChart.setBorderWidth(2);
        mLineChart.setPinchZoom(false);
        mLineChart.setMaxVisibleValueCount(20);
        for (i = 0; i <= 60; i++) {
            monthsArray[i] = String.valueOf(i);
        }
        Legend legend = mLineChart.getLegend();
        legend.setForm(Legend.LegendForm.LINE);
        legend.setFormSize(30);
        legend.setTextSize(15);

        LegendEntry[] legendEntries = new LegendEntry[3];
        for (i = 0; i < legendEntries.length; i++) {
            LegendEntry entry = new LegendEntry();
            entry.label = legendNames[i];
            entry.formColor = colorArray[i];
            legendEntries[i] = entry;
        }
        legend.setCustom(legendEntries);


        XAxis xAxis = mLineChart.getXAxis();
        xAxis.setValueFormatter(new MyXAxisValueFormatter(monthsArray));
        xAxis.setLabelCount(25);
        xAxis.setPosition(XAxis.XAxisPosition.BOTH_SIDED);
        xAxis.setDrawGridLines(true);
        xAxis.setGranularityEnabled(true);
        xAxis.setGranularity(2f);

        YAxis yAxis = mLineChart.getAxisLeft();
        YAxis yAxis1 = mLineChart.getAxisRight();
        yAxis.setDrawGridLines(true);
        yAxis1.setDrawGridLines(true);

        yAxis.setGranularityEnabled(true);
        yAxis1.setGranularityEnabled(true);

        yAxis.setGranularity(2f);
        yAxis1.setGranularity(2f);
        yAxis.setLabelCount(30);
        yAxis1.setLabelCount(30);

        //        yAxis.setValueFormatter(new MyYAxisValueFormatter(heightArray));
        rlWeight.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT));

//        //Pre-measure the view so that height and width don't remain null.
        rlWeight.measure(View.MeasureSpec.makeMeasureSpec(600, View.MeasureSpec.AT_MOST),
                View.MeasureSpec.makeMeasureSpec(870, View.MeasureSpec.AT_MOST));
        rlWeight.layout(0, 0, rlWeight.getMeasuredWidth(), rlWeight.getMeasuredHeight());

        //Assign a size and position to the view and all of its descendants
        bitmapweight = Bitmap.createBitmap(rlWeight.getWidth(),
                rlWeight.getHeight(),
                Bitmap.Config.ARGB_8888);
        //Create a canvas with the specified bitmap to draw into
        Canvas c = new Canvas(bitmapweight);
        //Render this view (and all of its children) to the given Canvas
        rlWeight.draw(c);

        //Convert to byte array
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmapweight.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byteArrayweight = stream.toByteArray();
    }

    private void getHeightWidthForWeight() {
        ViewTreeObserver vto = rlWeight.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                rlWeight.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                int width = rlWeight.getWidth();
                int height = rlWeight.getHeight();
                // Generate bitmap to display in pdf
                bitmapweight = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmapweight);
                Drawable bgDrawable = rlWeight.getBackground();
                if (bgDrawable != null) {
                    bgDrawable.draw(canvas);
                } else {
                    canvas.drawColor(Color.WHITE);
                }
                rlWeight.draw(canvas);

                //Convert to byte array
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmapweight.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byteArrayweight = stream.toByteArray();
            }
        });
    }

    private void setOnClickListeners() {
        aqChildGraph.id(R.id.btnWeight).getButton().setOnClickListener(this);
        aqChildGraph.id(R.id.btnHeight).getButton().setOnClickListener(this);
        aqChildGraph.id(R.id.btnBMI).getButton().setOnClickListener(this);
        aqChildGraph.id(R.id.btnHeadcf).getButton().setOnClickListener(this);
        aqChildGraph.id(R.id.btnReset).getButton().setOnClickListener(this);

    }

    //Get data from database
    private void getData(String childID) {
        CGMRepository cgmRepository = new CGMRepository(databaseHelper);
        try {
            tblweightforageList = cgmRepository.getweightforages();
            tblheightforageList = cgmRepository.getheightforages();
//            tblbmiforageList = cgmRepository.getbmiforages();
//            tblheadcfforagesList = cgmRepository.getheadcfforages();
            tblchildgrowthList = cgmRepository.getChildGrowthWeightHeight(childID);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnWeight:
                getWeightForAge();
                break;
            case R.id.btnHeight:
                getHeightForage();

                break;
            case R.id.btnBMI:

                break;

            case R.id.btnHeadcf:
                break;

            case R.id.btnReset:
                mLineChart.fitScreen();
                mLineChartHeight.fitScreen();
        }

    }

    private void getHeadcfForAge() {
        String[] legendNames = new String[]{"Normal", "Moderately Underweight", "Severely Underweight"};

        aqChildGraph.id(R.id.btnHeadcf).getButton().setBackgroundColor(getResources().getColor(R.color.gray));
        aqChildGraph.id(R.id.btnHeight).getButton().setBackgroundColor(getResources().getColor(R.color.appdarkcolor));
        aqChildGraph.id(R.id.btnWeight).getButton().setBackgroundColor(getResources().getColor(R.color.appdarkcolor));
        aqChildGraph.id(R.id.btnBMI).getButton().setBackgroundColor(getResources().getColor(R.color.appdarkcolor));
        int[] colorArray = new int[]{getApplicationContext().getResources().getColor(R.color.green), getApplicationContext().getResources().getColor(R.color.amber),
                getApplicationContext().getResources().getColor(R.color.red)};
        rlHeadcf.setVisibility(View.VISIBLE);
        rlWeight.setVisibility(View.GONE);
        rlHeight.setVisibility(View.GONE);
        rlBmi.setVisibility(View.GONE);
        String[] monthsArray = new String[tblheadcfforagesList.size()];
        LineDataSet lineDataSetMedianHeadcf = null, lineDataSetFirstNegSdHeadcf = null, lineDataSetSecondNegSdHeadcf = null, lineDataSetWeightHeadcf = null;

        if (tblChildInfo.getChlGender().equalsIgnoreCase("Male")) {
            lineDataSetMedianHeadcf = new LineDataSet(medianDataHeadcf(), "Median");
            lineDataSetFirstNegSdHeadcf = new LineDataSet(firstNegativeSDHeadcf(), "First Negative SD");
            lineDataSetSecondNegSdHeadcf = new LineDataSet(secondNegativeSDHeadcf(), "Second Negative SD");
            lineDataSetWeightHeadcf = new LineDataSet(headcfForAgeOfChild(), "Weight For Age");
        } else if (tblChildInfo.getChlGender().equalsIgnoreCase("Female")) {
            lineDataSetMedianHeadcf = new LineDataSet(medianDataHeadcfGirls(), "Median");
            lineDataSetFirstNegSdHeadcf = new LineDataSet(firstNegativeSDHeadcfGirls(), "First Negative SD");
            lineDataSetSecondNegSdHeadcf = new LineDataSet(secondNegativeSDHeadcfGirls(), "Second Negative SD");
            lineDataSetWeightHeadcf = new LineDataSet(headcfForAgeOfChildGirls(), "Weight For Age");
        }


        lineDataSetMedianHeadcf.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        lineDataSetMedianHeadcf.setDrawCircles(false);
        lineDataSetMedianHeadcf.setDrawCircleHole(false);
        lineDataSetMedianHeadcf.setColor(getApplicationContext().getResources().getColor(R.color.green));
        lineDataSetMedianHeadcf.setLineWidth(1f);


        lineDataSetWeightHeadcf.setColor(getApplicationContext().getResources().getColor(R.color.navyblue));
        lineDataSetWeightHeadcf.setLineWidth(4f);
        lineDataSetWeightHeadcf.setCircleColor(getApplicationContext().getResources().getColor(R.color.navyblue));
        lineDataSetWeightHeadcf.setCircleHoleColor(getApplicationContext().getResources().getColor(R.color.white));
        lineDataSetWeightHeadcf.setCircleRadius(5f);

        //Negative
        lineDataSetFirstNegSdHeadcf.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        lineDataSetFirstNegSdHeadcf.setDrawCircles(false);
        lineDataSetFirstNegSdHeadcf.setColor(getApplicationContext().getResources().getColor(R.color.amber));
        lineDataSetFirstNegSdHeadcf.setLineWidth(1f);

        lineDataSetSecondNegSdHeadcf.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        lineDataSetSecondNegSdHeadcf.setDrawCircles(false);
        lineDataSetSecondNegSdHeadcf.setColor(getApplicationContext().getResources().getColor(R.color.red));
        lineDataSetSecondNegSdHeadcf.setLineWidth(1f);
//
//        lineDataSetThirdNegSdHeadcf.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        lineDataSetThirdNegSdHeadcf.setDrawCircles(false);
//        lineDataSetThirdNegSdHeadcf.setColor(getApplicationContext().getResources().getColor(R.color.black));
//        lineDataSetThirdNegSdHeadcf.setLineWidth(1f);

        //Add dataset here
        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(lineDataSetMedianHeadcf);
        dataSets.add(lineDataSetFirstNegSdHeadcf);
        dataSets.add(lineDataSetSecondNegSdHeadcf);
        dataSets.add(lineDataSetWeightHeadcf);

        LineData data = new LineData(dataSets);
        mLineChartHeadcf.setData(data);
        mLineChartHeadcf.invalidate();

        //Customise chart
        mLineChartHeadcf.setDrawBorders(true);
        mLineChartHeadcf.setBorderColor(Color.BLACK);
        mLineChartHeadcf.setBorderWidth(2);
        mLineChartHeadcf.setPinchZoom(false);
        mLineChartHeadcf.setMaxVisibleValueCount(20);
        mLineChartHeadcf.getAxisLeft().setDrawAxisLine(false);
        mLineChartHeadcf.getAxisRight().setDrawGridLines(false);


        for (i = 0; i <= 60; i++) {
            monthsArray[i] = String.valueOf(i);
        }

        Legend legend = mLineChartHeadcf.getLegend();
        legend.setForm(Legend.LegendForm.LINE);
        legend.setFormSize(50);
        legend.setTextSize(15);

        LegendEntry[] legendEntries = new LegendEntry[4];
        for (i = 0; i < legendEntries.length; i++) {
            LegendEntry entry = new LegendEntry();
            entry.label = legendNames[i];
            entry.formColor = colorArray[i];
            legendEntries[i] = entry;
        }
        legend.setCustom(legendEntries);

        XAxis xAxis = mLineChartHeadcf.getXAxis();
        xAxis.setValueFormatter(new MyXAxisValueFormatter(monthsArray));
        xAxis.setLabelCount(25);
        xAxis.setPosition(XAxis.XAxisPosition.BOTH_SIDED);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularityEnabled(true);
        xAxis.setGranularity(2f);

        YAxis yAxis = mLineChartHeadcf.getAxisLeft();
        YAxis yAxis1 = mLineChartHeadcf.getAxisRight();
        yAxis.setDrawGridLines(false);
        yAxis1.setDrawGridLines(false);

        yAxis.setGranularityEnabled(true);
        yAxis1.setGranularityEnabled(true);

        yAxis.setGranularity(2f);
        yAxis1.setGranularity(2f);
        yAxis.setLabelCount(30);
        yAxis1.setLabelCount(30);


//        // Generate bitmap to display in pdf

        rlHeadcf.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT));

//        //Pre-measure the view so that height and width don't remain null.
        rlHeadcf.measure(View.MeasureSpec.makeMeasureSpec(600, View.MeasureSpec.AT_MOST),
                View.MeasureSpec.makeMeasureSpec(906, View.MeasureSpec.AT_MOST));
        rlHeadcf.layout(0, 0, rlHeadcf.getMeasuredWidth(), rlHeadcf.getMeasuredHeight());

        //Assign a size and position to the view and all of its descendants
        bitmapheadcf = Bitmap.createBitmap(rlHeadcf.getWidth(),
                rlHeadcf.getHeight(),
                Bitmap.Config.ARGB_8888);
        //Create a canvas with the specified bitmap to draw into
        Canvas c = new Canvas(bitmapheadcf);
        //Render this view (and all of its children) to the given Canvas
        rlHeadcf.draw(c);

        //Convert to byte array
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmapheadcf.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byteArrayHeadcf = stream.toByteArray();
    }

    private void getHeightWidthforHeadcf() {
        ViewTreeObserver vto = rlHeadcf.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                rlHeadcf.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                // Generate bitmap to display in pdf
                bitmapheadcf = Bitmap.createBitmap(600, 908, Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmapheadcf);
                Drawable bgDrawable = rlHeadcf.getBackground();
                if (bgDrawable != null) {
                    bgDrawable.draw(canvas);
                } else {
                    canvas.drawColor(Color.WHITE);
                }
                rlHeadcf.draw(canvas);


                //Convert to byte array
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmapheadcf.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byteArrayHeadcf = stream.toByteArray();

            }
        });
    }

    private void getBMIForAge() {
        String[] legendNames = new String[]{"Normal", "Moderately Underweight", "Severely Underweight"};

        aqChildGraph.id(R.id.btnBMI).getButton().setBackgroundColor(getResources().getColor(R.color.gray));
        aqChildGraph.id(R.id.btnHeight).getButton().setBackgroundColor(getResources().getColor(R.color.appdarkcolor));
        aqChildGraph.id(R.id.btnWeight).getButton().setBackgroundColor(getResources().getColor(R.color.appdarkcolor));
        aqChildGraph.id(R.id.btnHeadcf).getButton().setBackgroundColor(getResources().getColor(R.color.appdarkcolor));

        int[] colorArray = new int[]{getApplicationContext().getResources().getColor(R.color.green), getApplicationContext().getResources().getColor(R.color.amber),
                getApplicationContext().getResources().getColor(R.color.red)};
        rlBmi.setVisibility(View.VISIBLE);
        rlWeight.setVisibility(View.GONE);
        rlHeight.setVisibility(View.GONE);
        rlHeadcf.setVisibility(View.GONE);
        String[] monthsArray = new String[tblbmiforageList.size()];
        LineDataSet lineDataSetMedianBmi = null, lineDataSetFirstNegSdBMI = null, lineDataSetSecondNegSdBMI = null, lineDataSetBmiForAge = null;


        if (tblChildInfo.getChlGender().equalsIgnoreCase("Male")) {
            lineDataSetMedianBmi = new LineDataSet(medianDataBMI(), "Median");
            lineDataSetFirstNegSdBMI = new LineDataSet(firstNegativeSDBMI(), "First Negative SD");
            lineDataSetSecondNegSdBMI = new LineDataSet(secondNegativeSDBMI(), "Second Negative SD");
            lineDataSetBmiForAge = new LineDataSet(bmiForAgeOfChild(), "BMI for age");
        } else if (tblChildInfo.getChlGender().equalsIgnoreCase("Female")) {
            lineDataSetMedianBmi = new LineDataSet(medianDataBMIGirls(), "Median");
            lineDataSetFirstNegSdBMI = new LineDataSet(firstNegativeSDBMIGirls(), "First Negative SD");
            lineDataSetSecondNegSdBMI = new LineDataSet(secondNegativeSDBMIGirls(), "Second Negative SD");
            lineDataSetBmiForAge = new LineDataSet(bmiForAgeOfChildGirls(), "BMI for age");
        }


        lineDataSetMedianBmi.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        lineDataSetMedianBmi.setDrawCircles(false);
        lineDataSetMedianBmi.setDrawCircleHole(false);
        lineDataSetMedianBmi.setColor(getApplicationContext().getResources().getColor(R.color.green));
        lineDataSetMedianBmi.setLineWidth(1f);


        lineDataSetBmiForAge.setColor(getApplicationContext().getResources().getColor(R.color.navyblue));
        lineDataSetBmiForAge.setCircleHoleColor(getApplicationContext().getResources().getColor(R.color.white));
        lineDataSetBmiForAge.setCircleRadius(5f);
        lineDataSetBmiForAge.setLineWidth(4f);
        //Negative
        lineDataSetFirstNegSdBMI.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        lineDataSetFirstNegSdBMI.setColor(Color.YELLOW);
        lineDataSetFirstNegSdBMI.setDrawCircles(false);
        lineDataSetFirstNegSdBMI.setColor(getApplicationContext().getResources().getColor(R.color.amber));
        lineDataSetFirstNegSdBMI.setLineWidth(1f);

        lineDataSetSecondNegSdBMI.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        lineDataSetSecondNegSdBMI.setColor(Color.YELLOW);
        lineDataSetSecondNegSdBMI.setDrawCircles(false);
        lineDataSetSecondNegSdBMI.setColor(getApplicationContext().getResources().getColor(R.color.red));
        lineDataSetSecondNegSdBMI.setLineWidth(1f);

        //Add dataset here
        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(lineDataSetMedianBmi);
        dataSets.add(lineDataSetFirstNegSdBMI);
        dataSets.add(lineDataSetSecondNegSdBMI);
        dataSets.add(lineDataSetBmiForAge);

        LineData data = new LineData(dataSets);
        mLineChartBmi.setData(data);
        mLineChartBmi.invalidate();

        //Customise chart
        mLineChartBmi.setDrawBorders(true);
        mLineChartBmi.setBorderColor(Color.BLACK);
        mLineChartBmi.setBorderWidth(2);
        mLineChartBmi.setPinchZoom(false);


        for (i = 0; i <= 60; i++) {
            monthsArray[i] = String.valueOf(i);
        }
//        for (i = 0; i <= 31; i++) {
//            heightArray[i] = String.valueOf(i);
//        }

        //CHanging legend info
        Legend legend = mLineChartBmi.getLegend();
        legend.setForm(Legend.LegendForm.LINE);
        legend.setFormSize(50);
        legend.setTextSize(15);

        LegendEntry[] legendEntries = new LegendEntry[4];
        for (i = 0; i < legendEntries.length; i++) {
            LegendEntry entry = new LegendEntry();
            entry.label = legendNames[i];
            entry.formColor = colorArray[i];
            legendEntries[i] = entry;
        }
        legend.setCustom(legendEntries);

        XAxis xAxis = mLineChartBmi.getXAxis();
        xAxis.setValueFormatter(new MyXAxisValueFormatter(monthsArray));
        xAxis.setPosition(XAxis.XAxisPosition.BOTH_SIDED);
        xAxis.setLabelCount(25);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularityEnabled(true);
        xAxis.setGranularity(2f);


        YAxis yAxis = mLineChartBmi.getAxisLeft();
        YAxis yAxis1 = mLineChartBmi.getAxisRight();
        yAxis.setDrawGridLines(false);
        yAxis1.setDrawGridLines(false);

        yAxis.setGranularityEnabled(true);
        yAxis1.setGranularityEnabled(true);

        yAxis.setGranularity(2f);
        yAxis1.setGranularity(2f);
        yAxis.setLabelCount(30);
        yAxis1.setLabelCount(30);

//        YAxis yAxis = mLineChartBmi.getAxisLeft();
//        YAxis yAxis1=mLineChartBmi.getAxisRight();
//        yAxis.setDrawGridLines(false);
//        yAxis1.setDrawGridLines(false);
//        yAxis.setGranularity(1f);
//        yAxis1.setGranularity(1f);

//        yAxis.setValueFormatter(new MyYAxisValueFormatter(heightArray));
        rlBmi.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT));

//        //Pre-measure the view so that height and width don't remain null.
        rlBmi.measure(View.MeasureSpec.makeMeasureSpec(600, View.MeasureSpec.AT_MOST),
                View.MeasureSpec.makeMeasureSpec(906, View.MeasureSpec.AT_MOST));
        rlBmi.layout(0, 0, rlBmi.getMeasuredWidth(), rlBmi.getMeasuredHeight());

        //Assign a size and position to the view and all of its descendants
        bitmapbmi = Bitmap.createBitmap(rlBmi.getWidth(),
                rlBmi.getHeight(),
                Bitmap.Config.ARGB_8888);
        //Create a canvas with the specified bitmap to draw into
        Canvas c = new Canvas(bitmapbmi);
        //Render this view (and all of its children) to the given Canvas
        rlBmi.draw(c);

        //Convert to byte array
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmapbmi.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byteArrayBMI = stream.toByteArray();
    }

    private void getHeightWidthforBmi() {
        ViewTreeObserver vto = rlBmi.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                rlBmi.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                // Generate bitmap to display in pdf
                bitmapbmi = Bitmap.createBitmap(rlBmi.getWidth(), rlBmi.getHeight(), Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmapbmi);
//                Drawable bgDrawable = rlBmi.getBackground();
//                if (bgDrawable != null) {
//                    bgDrawable.draw(canvas);
//                } else {
//                    canvas.drawColor(Color.WHITE);
//                }
                rlBmi.draw(canvas);

                //Convert to byte array
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmapbmi.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byteArrayBMI = stream.toByteArray();
            }
        });
    }

    private void getHeightForage() {
        String[] legendNames = new String[]{"Normal", "Moderately Stunted", "Severely Stunted"};

        aqChildGraph.id(R.id.btnHeight).getButton().setBackgroundColor(getResources().getColor(R.color.gray));
        aqChildGraph.id(R.id.btnWeight).getButton().setBackgroundColor(getResources().getColor(R.color.appdarkcolor));
        aqChildGraph.id(R.id.btnBMI).getButton().setBackgroundColor(getResources().getColor(R.color.appdarkcolor));
        aqChildGraph.id(R.id.btnHeadcf).getButton().setBackgroundColor(getResources().getColor(R.color.appdarkcolor));


        int[] colorArray = new int[]{getApplicationContext().getResources().getColor(R.color.parratgreen), getApplicationContext().getResources().getColor(R.color.graphyellow),
                getApplicationContext().getResources().getColor(R.color.darkorange)};
        rlHeight.setVisibility(View.VISIBLE);
        rlWeight.setVisibility(View.GONE);
        rlBmi.setVisibility(View.GONE);
        rlHeadcf.setVisibility(View.GONE);

        String[] monthsArray = new String[tblheightforageList.size()];
        String[] heightArray = new String[35];
        LineDataSet lineDataSetMedian = null, lineDataSetFirstNegSdHeight = null, lineDataSetSecondNegSdHeight = null, lineDataSetHeightForAge = null;

        if (tblChildInfo.getChlGender().equalsIgnoreCase("Male")) {
            lineDataSetMedian = new LineDataSet(medianDataHeight(), "Median");
            lineDataSetFirstNegSdHeight = new LineDataSet(firstNegativeSDHeight(), "First Negative SD");
            lineDataSetSecondNegSdHeight = new LineDataSet(secondNegativeSDHeight(), "Second Negative SD");
            lineDataSetHeightForAge = new LineDataSet(heightForAgeOfChild(), "Height for age");
        } else if (tblChildInfo.getChlGender().equalsIgnoreCase("Female")) {
            lineDataSetMedian = new LineDataSet(medianDataHeightGirls(), "Median");
            lineDataSetFirstNegSdHeight = new LineDataSet(firstNegativeSDHeightGirls(), "First Negative SD");
            lineDataSetSecondNegSdHeight = new LineDataSet(secondNegativeSDHeightGirls(), "Second Negative SD");
            lineDataSetHeightForAge = new LineDataSet(heightForAgeOfChildGirls(), "Height for age");
        }


        lineDataSetMedian.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        lineDataSetMedian.setDrawCircles(false);
        lineDataSetMedian.setDrawCircleHole(false);
        lineDataSetMedian.setColor(getApplicationContext().getResources().getColor(R.color.parratgreen));
        lineDataSetMedian.setLineWidth(1f);
        lineDataSetMedian.setFillAlpha(255);
        lineDataSetMedian.setDrawFilled(true);
        lineDataSetMedian.setFillColor(getApplicationContext().getResources().getColor(R.color.parratgreen));




        lineDataSetHeightForAge.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        lineDataSetHeightForAge.setColor(getApplicationContext().getResources().getColor(R.color.navyblue));
        lineDataSetHeightForAge.setLineWidth(4f);
        lineDataSetHeightForAge.setDrawCircles(true);
        lineDataSetHeightForAge.setCircleColor(getApplicationContext().getResources().getColor(R.color.navyblue));
        lineDataSetHeightForAge.setCircleHoleColor(getApplicationContext().getResources().getColor(R.color.white));
        lineDataSetHeightForAge.setCircleRadius(5f);


        //Negative
        lineDataSetFirstNegSdHeight.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        lineDataSetFirstNegSdHeight.setDrawCircles(false);
        lineDataSetFirstNegSdHeight.setColor(getApplicationContext().getResources().getColor(R.color.graphyellow));
        lineDataSetFirstNegSdHeight.setLineWidth(1f);

        lineDataSetFirstNegSdHeight.setFillAlpha(255);
        lineDataSetFirstNegSdHeight.setDrawFilled(true);
        lineDataSetFirstNegSdHeight.setFillColor(getApplicationContext().getResources().getColor(R.color.graphyellow));

        lineDataSetSecondNegSdHeight.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        lineDataSetSecondNegSdHeight.setDrawCircles(false);
        lineDataSetSecondNegSdHeight.setColor(getApplicationContext().getResources().getColor(R.color.darkorange));
        lineDataSetSecondNegSdHeight.setLineWidth(1f);

        lineDataSetSecondNegSdHeight.setFillAlpha(255);
        lineDataSetSecondNegSdHeight.setDrawFilled(true);
        lineDataSetSecondNegSdHeight.setFillColor(getApplicationContext().getResources().getColor(R.color.darkorange));

        //Add dataset here
        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(lineDataSetMedian);
        dataSets.add(lineDataSetFirstNegSdHeight);
        dataSets.add(lineDataSetSecondNegSdHeight);
        dataSets.add(lineDataSetHeightForAge);

        LineData data = new LineData(dataSets);
        mLineChartHeight.setData(data);
        mLineChartHeight.invalidate();

        //Customise chart
        mLineChartHeight.setDrawBorders(true);
        mLineChartHeight.setBorderColor(Color.BLACK);
        mLineChartHeight.setBorderWidth(2);
        mLineChartHeight.setPinchZoom(false);
        mLineChartHeight.setMaxVisibleValueCount(20);
        mLineChartHeight.getAxisLeft().setDrawAxisLine(false);
        mLineChartHeight.getAxisRight().setDrawGridLines(false);


        for (i = 0; i <= 60; i++) {
            monthsArray[i] = String.valueOf(i);
        }
//        for (i = 0; i <= 31; i++) {
//            heightArray[i] = String.valueOf(i);
//        }

        //Customise legends
        Legend legend = mLineChartHeight.getLegend();
        legend.setForm(Legend.LegendForm.LINE);
        legend.setFormSize(50);
        legend.setTextSize(15);

        LegendEntry[] legendEntries = new LegendEntry[3];
        for (i = 0; i < legendEntries.length; i++) {
            LegendEntry entry = new LegendEntry();
            entry.label = legendNames[i];
            entry.formColor = colorArray[i];
            legendEntries[i] = entry;
        }
        legend.setCustom(legendEntries);

        XAxis xAxis = mLineChartHeight.getXAxis();
        xAxis.setValueFormatter(new MyXAxisValueFormatter(monthsArray));
        xAxis.setPosition(XAxis.XAxisPosition.BOTH_SIDED);
        xAxis.setLabelCount(25);
        xAxis.setDrawGridLines(true);
        xAxis.setGranularityEnabled(true);
        xAxis.setGranularity(2f);

//        YAxis yAxis = mLineChartHeight.getAxisLeft();
//        YAxis yAxis1=mLineChartHeight.getAxisRight();
//        yAxis.setDrawGridLines(false);
//        yAxis1.setDrawGridLines(false);
//        yAxis.setGranularity(1f);
//        yAxis1.setGranularity(1f);

        YAxis yAxis = mLineChartHeight.getAxisLeft();
        YAxis yAxis1 = mLineChartHeight.getAxisRight();
        yAxis.setDrawGridLines(true);
        yAxis1.setDrawGridLines(true);

        yAxis.setGranularityEnabled(true);
        yAxis1.setGranularityEnabled(true);

        yAxis.setGranularity(2f);
        yAxis1.setGranularity(2f);
        yAxis.setLabelCount(30);
        yAxis1.setLabelCount(30);

//        yAxis.setValueFormatter(new MyYAxisValueFormatter(heightArray));
        rlHeight.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT));

//        //Pre-measure the view so that height and width don't remain null.
        rlHeight.measure(View.MeasureSpec.makeMeasureSpec(600, View.MeasureSpec.AT_MOST),
                View.MeasureSpec.makeMeasureSpec(870, View.MeasureSpec.AT_MOST));
        rlHeight.layout(0, 0, rlHeight.getMeasuredWidth(), rlHeight.getMeasuredHeight());

        //Assign a size and position to the view and all of its descendants
        bitmapheight = Bitmap.createBitmap(rlHeight.getWidth(),
                rlHeight.getHeight(),
                Bitmap.Config.ARGB_8888);
        //Create a canvas with the specified bitmap to draw into
        Canvas c = new Canvas(bitmapheight);
        //Render this view (and all of its children) to the given Canvas
        rlHeight.draw(c);

        //Convert to byte array
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmapheight.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byteArrayHeight = stream.toByteArray();


    }

    private void getHeightWidth() {
        ViewTreeObserver vto = rlHeight.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                rlHeight.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                int width = rlHeight.getWidth();
                int height = rlHeight.getHeight();


                // Generate bitmap to display in pdf
                bitmapheight = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmapheight);
                Drawable bgDrawable = rlHeight.getBackground();
                if (bgDrawable != null) {
                    bgDrawable.draw(canvas);
                } else {
                    canvas.drawColor(Color.WHITE);
                }
                rlHeight.draw(canvas);

                //Convert to byte array
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmapheight.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byteArrayHeight = stream.toByteArray();
            }
        });
    }

    //    initialize db object
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    //Getting median Values
    private ArrayList<Entry> medianData() {
        ArrayList<Entry> dataValue = new ArrayList<>();
        for (int i = 0; i < tblweightforageList.size(); i++) {
            dataValue.add(new Entry(tblweightforageList.get(i).getXaxis(), Float.parseFloat(tblweightforageList.get(i).getCgmBoysWtMedian())));
        }
        return dataValue;
    }

    private ArrayList<Entry> medianDataHeight() {
        ArrayList<Entry> dataValue = new ArrayList<>();
        for (int i = 0; i < tblheightforageList.size(); i++) {
            dataValue.add(new Entry(tblheightforageList.get(i).getXaxis(), Float.parseFloat(tblheightforageList.get(i).getCgmBoysHtMedian())));
        }
        return dataValue;
    }

    private ArrayList<Entry> medianDataBMI() {
        ArrayList<Entry> dataValue = new ArrayList<>();
        for (int i = 0; i < tblbmiforageList.size(); i++) {
            dataValue.add(new Entry(tblbmiforageList.get(i).getXaxis(), Float.parseFloat(tblbmiforageList.get(i).getCgmBoysBMIMedian())));
        }
        return dataValue;
    }

    private ArrayList<Entry> medianDataHeadcf() {
        ArrayList<Entry> dataValue = new ArrayList<>();
        for (int i = 0; i < tblheadcfforagesList.size(); i++) {
            dataValue.add(new Entry(tblheadcfforagesList.get(i).getXaxis(), Float.parseFloat(tblheadcfforagesList.get(i).getCgmBoysHeadcfMedian())));
        }
        return dataValue;
    }

    //Girls
    private ArrayList<Entry> medianDataGirls() {
        ArrayList<Entry> dataValue = new ArrayList<>();
        for (int i = 0; i < tblweightforageList.size(); i++) {
            dataValue.add(new Entry(tblweightforageList.get(i).getXaxis(), Float.parseFloat(tblweightforageList.get(i).getCgmGirlsWtMedian())));
        }
        return dataValue;
    }

    private ArrayList<Entry> medianDataHeightGirls() {
        ArrayList<Entry> dataValue = new ArrayList<>();
        for (int i = 0; i < tblheightforageList.size(); i++) {
            dataValue.add(new Entry(tblheightforageList.get(i).getXaxis(), Float.parseFloat(tblheightforageList.get(i).getCgmGirlsHtMedian())));
        }
        return dataValue;
    }

    private ArrayList<Entry> medianDataBMIGirls() {
        ArrayList<Entry> dataValue = new ArrayList<>();
        for (int i = 0; i < tblbmiforageList.size(); i++) {
            dataValue.add(new Entry(tblbmiforageList.get(i).getXaxis(), Float.parseFloat(tblbmiforageList.get(i).getCgmGirlsBMIMedian())));
        }
        return dataValue;
    }

    private ArrayList<Entry> medianDataHeadcfGirls() {
        ArrayList<Entry> dataValue = new ArrayList<>();
        for (int i = 0; i < tblheadcfforagesList.size(); i++) {
            dataValue.add(new Entry(tblheadcfforagesList.get(i).getXaxis(), Float.parseFloat(tblheadcfforagesList.get(i).getCgmGirlsHeadcfMedian())));
        }
        return dataValue;
    }


//    //Getting First Positive SD Values
//    private ArrayList<Entry> firstPositiveSD() {
//        ArrayList<Entry> dataValue = new ArrayList<>();
//        for (int i = 0; i < tblweightforageList.size(); i++) {
//            dataValue.add(new Entry(tblweightforageList.get(i).getXaxis(), Float.parseFloat(tblweightforageList.get(i).getCgmBoysWtFirstPosSD())));
//        }
//        return dataValue;
//    }
//
//    private ArrayList<Entry> firstPositiveSDHeight() {
//        ArrayList<Entry> dataValue = new ArrayList<>();
//        for (int i = 0; i < tblheightforageList.size(); i++) {
//            dataValue.add(new Entry(tblheightforageList.get(i).getXaxis(), Float.parseFloat(tblheightforageList.get(i).getCgmBoysHtFirstPosSD())));
//        }
//        return dataValue;
//    }
//
//    private ArrayList<Entry> firstPositiveSDBMI() {
//        ArrayList<Entry> dataValue = new ArrayList<>();
//        for (int i = 0; i < tblbmiforageList.size(); i++) {
//            dataValue.add(new Entry(tblbmiforageList.get(i).getXaxis(), Float.parseFloat(tblbmiforageList.get(i).getCgmBoysBMIFirstPosSD())));
//        }
//        return dataValue;
//    }
//
//    private ArrayList<Entry> firstPositiveSDHeadcf() {
//        ArrayList<Entry> dataValue = new ArrayList<>();
//        for (int i = 0; i < tblheadcfforagesList.size(); i++) {
//            dataValue.add(new Entry(tblheadcfforagesList.get(i).getXaxis(), Float.parseFloat(tblheadcfforagesList.get(i).getCgmBoysHeadcfFirstPosSD())));
//        }
//        return dataValue;
//    }
//
//    //Girls
//    private ArrayList<Entry> firstPositiveSDGirls() {
//        ArrayList<Entry> dataValue = new ArrayList<>();
//        for (int i = 0; i < tblweightforageList.size(); i++) {
//            dataValue.add(new Entry(tblweightforageList.get(i).getXaxis(), Float.parseFloat(tblweightforageList.get(i).getCgmGirlsWtFirstPosSD())));
//        }
//        return dataValue;
//    }
//
//    private ArrayList<Entry> firstPositiveSDHeightGirls() {
//        ArrayList<Entry> dataValue = new ArrayList<>();
//        for (int i = 0; i < tblheightforageList.size(); i++) {
//            dataValue.add(new Entry(tblheightforageList.get(i).getXaxis(), Float.parseFloat(tblheightforageList.get(i).getCgmGirlsHtFirstPosSD())));
//        }
//        return dataValue;
//    }
//
//    private ArrayList<Entry> firstPositiveSDBMIGirls() {
//        ArrayList<Entry> dataValue = new ArrayList<>();
//        for (int i = 0; i < tblbmiforageList.size(); i++) {
//            dataValue.add(new Entry(tblbmiforageList.get(i).getXaxis(), Float.parseFloat(tblbmiforageList.get(i).getCgmGirlsBMIFirstPosSD())));
//        }
//        return dataValue;
//    }
//
//    private ArrayList<Entry> firstPositiveSDHeadcfGirls() {
//        ArrayList<Entry> dataValue = new ArrayList<>();
//        for (int i = 0; i < tblheadcfforagesList.size(); i++) {
//            dataValue.add(new Entry(tblheadcfforagesList.get(i).getXaxis(), Float.parseFloat(tblheadcfforagesList.get(i).getCgmGirlsHeadcfFirstPosSD())));
//        }
//        return dataValue;
//    }

    //Getting Second Positive SD Values
//    private ArrayList<Entry> secondPositiveSD() {
//        ArrayList<Entry> dataValue = new ArrayList<>();
//        for (int i = 0; i < tblweightforageList.size(); i++) {
//            dataValue.add(new Entry(tblweightforageList.get(i).getXaxis(), Float.parseFloat(tblweightforageList.get(i).getCgmBoysWtSecondPosSD())));
//        }
//        return dataValue;
//    }
//
//    private ArrayList<Entry> secondPositiveSDHeight() {
//        ArrayList<Entry> dataValue = new ArrayList<>();
//        for (int i = 0; i < tblheightforageList.size(); i++) {
//            dataValue.add(new Entry(tblheightforageList.get(i).getXaxis(), Float.parseFloat(tblheightforageList.get(i).getCgmBoysHtSecondPosSD())));
//        }
//        return dataValue;
//    }
//
//    private ArrayList<Entry> secondPositiveSDBMI() {
//        ArrayList<Entry> dataValue = new ArrayList<>();
//        for (int i = 0; i < tblbmiforageList.size(); i++) {
//            dataValue.add(new Entry(tblbmiforageList.get(i).getXaxis(), Float.parseFloat(tblbmiforageList.get(i).getCgmBoysBMISecondPosSD())));
//        }
//        return dataValue;
//    }
//
//    private ArrayList<Entry> secondPositiveSDHeadcf() {
//        ArrayList<Entry> dataValue = new ArrayList<>();
//        for (int i = 0; i < tblheadcfforagesList.size(); i++) {
//            dataValue.add(new Entry(tblheadcfforagesList.get(i).getXaxis(), Float.parseFloat(tblheadcfforagesList.get(i).getCgmBoysHeadcfSecondPosSD())));
//        }
//        return dataValue;
//    }
//
//    //Girls
//    private ArrayList<Entry> secondPositiveSDGirls() {
//        ArrayList<Entry> dataValue = new ArrayList<>();
//        for (int i = 0; i < tblweightforageList.size(); i++) {
//            dataValue.add(new Entry(tblweightforageList.get(i).getXaxis(), Float.parseFloat(tblweightforageList.get(i).getCgmGirlsWtSecondPosSD())));
//        }
//        return dataValue;
//    }
//
//    private ArrayList<Entry> secondPositiveSDHeightGirls() {
//        ArrayList<Entry> dataValue = new ArrayList<>();
//        for (int i = 0; i < tblheightforageList.size(); i++) {
//            dataValue.add(new Entry(tblheightforageList.get(i).getXaxis(), Float.parseFloat(tblheightforageList.get(i).getCgmGirlsHtSecondPosSD())));
//        }
//        return dataValue;
//    }
//
//    private ArrayList<Entry> secondPositiveSDBMIGirls() {
//        ArrayList<Entry> dataValue = new ArrayList<>();
//        for (int i = 0; i < tblbmiforageList.size(); i++) {
//            dataValue.add(new Entry(tblbmiforageList.get(i).getXaxis(), Float.parseFloat(tblbmiforageList.get(i).getCgmGirlsBMISecondPosSD())));
//        }
//        return dataValue;
//    }
//
//    private ArrayList<Entry> secondPositiveSDHeadcfGirls() {
//        ArrayList<Entry> dataValue = new ArrayList<>();
//        for (int i = 0; i < tblheadcfforagesList.size(); i++) {
//            dataValue.add(new Entry(tblheadcfforagesList.get(i).getXaxis(), Float.parseFloat(tblheadcfforagesList.get(i).getCgmGirlsHeadcfSecondPosSD())));
//        }
//        return dataValue;
//    }
//
//
//    //Getting Third Positive SD Values
//    private ArrayList<Entry> thirdPositiveSD() {
//        ArrayList<Entry> dataValue = new ArrayList<>();
//        for (int i = 0; i < tblweightforageList.size(); i++) {
//            dataValue.add(new Entry(tblweightforageList.get(i).getXaxis(), Float.parseFloat(tblweightforageList.get(i).getCgmBoysWtThirdPosSD())));
//        }
//        return dataValue;
//    }
//
//    private ArrayList<Entry> thirdPositiveSDHeight() {
//        ArrayList<Entry> dataValue = new ArrayList<>();
//        for (int i = 0; i < tblheightforageList.size(); i++) {
//            dataValue.add(new Entry(tblheightforageList.get(i).getXaxis(), Float.parseFloat(tblheightforageList.get(i).getCgmBoysHtThirdPosSD())));
//        }
//        return dataValue;
//    }
//
//    private ArrayList<Entry> thirdPositiveSDBMI() {
//        ArrayList<Entry> dataValue = new ArrayList<>();
//        for (int i = 0; i < tblbmiforageList.size(); i++) {
//            dataValue.add(new Entry(tblbmiforageList.get(i).getXaxis(), Float.parseFloat(tblbmiforageList.get(i).getCgmBoysBMIThirdPosSD())));
//        }
//        return dataValue;
//    }
//
//    private ArrayList<Entry> thirdPositiveSDHeadcf() {
//        ArrayList<Entry> dataValue = new ArrayList<>();
//        for (int i = 0; i < tblheadcfforagesList.size(); i++) {
//            dataValue.add(new Entry(tblheadcfforagesList.get(i).getXaxis(), Float.parseFloat(tblheadcfforagesList.get(i).getCgmBoysHeadcfThirdPosSD())));
//        }
//        return dataValue;
//    }
//
//    //Girls
//    private ArrayList<Entry> thirdPositiveSDGirls() {
//        ArrayList<Entry> dataValue = new ArrayList<>();
//        for (int i = 0; i < tblweightforageList.size(); i++) {
//            dataValue.add(new Entry(tblweightforageList.get(i).getXaxis(), Float.parseFloat(tblweightforageList.get(i).getCgmGirlsWtThirdPosSD())));
//        }
//        return dataValue;
//    }
//
//    private ArrayList<Entry> thirdPositiveSDHeightGirls() {
//        ArrayList<Entry> dataValue = new ArrayList<>();
//        for (int i = 0; i < tblheightforageList.size(); i++) {
//            dataValue.add(new Entry(tblheightforageList.get(i).getXaxis(), Float.parseFloat(tblheightforageList.get(i).getCgmGirlsHtThirdPosSD())));
//        }
//        return dataValue;
//    }
//
//    private ArrayList<Entry> thirdPositiveSDBMIGirls() {
//        ArrayList<Entry> dataValue = new ArrayList<>();
//        for (int i = 0; i < tblbmiforageList.size(); i++) {
//            dataValue.add(new Entry(tblbmiforageList.get(i).getXaxis(), Float.parseFloat(tblbmiforageList.get(i).getCgmGirlsBMIThirdPosSD())));
//        }
//        return dataValue;
//    }
//
//    private ArrayList<Entry> thirdPositiveSDHeadcfGirls() {
//        ArrayList<Entry> dataValue = new ArrayList<>();
//        for (int i = 0; i < tblheadcfforagesList.size(); i++) {
//            dataValue.add(new Entry(tblheadcfforagesList.get(i).getXaxis(), Float.parseFloat(tblheadcfforagesList.get(i).getCgmGirlsHeadcfThirdPosSD())));
//        }
//        return dataValue;
//    }


    //Getting First Negative SD Values
    private ArrayList<Entry> firstNegativeSD() {
        ArrayList<Entry> dataValue = new ArrayList<>();
        for (int i = 0; i < tblweightforageList.size(); i++) {
            dataValue.add(new Entry(tblweightforageList.get(i).getXaxis(), Float.parseFloat(tblweightforageList.get(i).getCgmBoysWtFirstdNegSD())));
        }
        return dataValue;
    }

    private ArrayList<Entry> firstNegativeSDHeight() {
        ArrayList<Entry> dataValue = new ArrayList<>();
        for (int i = 0; i < tblheightforageList.size(); i++) {
            dataValue.add(new Entry(tblheightforageList.get(i).getXaxis(), Float.parseFloat(tblheightforageList.get(i).getCgmBoysHtFirstdNegSD())));
        }
        return dataValue;
    }

    private ArrayList<Entry> firstNegativeSDBMI() {
        ArrayList<Entry> dataValue = new ArrayList<>();
        for (int i = 0; i < tblbmiforageList.size(); i++) {
            dataValue.add(new Entry(tblbmiforageList.get(i).getXaxis(), Float.parseFloat(tblbmiforageList.get(i).getCgmBoysBMIFirstdNegSD())));
        }
        return dataValue;
    }

    private ArrayList<Entry> firstNegativeSDHeadcf() {
        ArrayList<Entry> dataValue = new ArrayList<>();
        for (int i = 0; i < tblheadcfforagesList.size(); i++) {
            dataValue.add(new Entry(tblheadcfforagesList.get(i).getXaxis(), Float.parseFloat(tblheadcfforagesList.get(i).getCgmBoysHeadcfFirstdNegSD())));
        }
        return dataValue;
    }


    //Girls
    private ArrayList<Entry> firstNegativeSDGirls() {
        ArrayList<Entry> dataValue = new ArrayList<>();
        for (int i = 0; i < tblweightforageList.size(); i++) {
            dataValue.add(new Entry(tblweightforageList.get(i).getXaxis(), Float.parseFloat(tblweightforageList.get(i).getCgmGirlsWtFirstdNegSD())));
        }
        return dataValue;
    }

    private ArrayList<Entry> firstNegativeSDHeightGirls() {
        ArrayList<Entry> dataValue = new ArrayList<>();
        for (int i = 0; i < tblheightforageList.size(); i++) {
            dataValue.add(new Entry(tblheightforageList.get(i).getXaxis(), Float.parseFloat(tblheightforageList.get(i).getCgmGirlsHtFirstdNegSD())));
        }
        return dataValue;
    }

    private ArrayList<Entry> firstNegativeSDBMIGirls() {
        ArrayList<Entry> dataValue = new ArrayList<>();
        for (int i = 0; i < tblbmiforageList.size(); i++) {
            dataValue.add(new Entry(tblbmiforageList.get(i).getXaxis(), Float.parseFloat(tblbmiforageList.get(i).getCgmGirlsBMIFirstdNegSD())));
        }
        return dataValue;
    }

    private ArrayList<Entry> firstNegativeSDHeadcfGirls() {
        ArrayList<Entry> dataValue = new ArrayList<>();
        for (int i = 0; i < tblheadcfforagesList.size(); i++) {
            dataValue.add(new Entry(tblheadcfforagesList.get(i).getXaxis(), Float.parseFloat(tblheadcfforagesList.get(i).getCgmGirlsHeadcfFirstdNegSD())));
        }
        return dataValue;
    }


    //Getting Second Negative SD Values
    private ArrayList<Entry> secondNegativeSD() {
        ArrayList<Entry> dataValue = new ArrayList<>();
        for (int i = 0; i < tblweightforageList.size(); i++) {
            dataValue.add(new Entry(tblweightforageList.get(i).getXaxis(), Float.parseFloat(tblweightforageList.get(i).getCgmBoysWtSeconddNegSD())));
        }
        return dataValue;
    }

    private ArrayList<Entry> secondNegativeSDHeight() {
        ArrayList<Entry> dataValue = new ArrayList<>();
        for (int i = 0; i < tblheightforageList.size(); i++) {
            dataValue.add(new Entry(tblheightforageList.get(i).getXaxis(), Float.parseFloat(tblheightforageList.get(i).getCgmBoysHtSeconddNegSD())));
        }
        return dataValue;
    }

    private ArrayList<Entry> secondNegativeSDBMI() {
        ArrayList<Entry> dataValue = new ArrayList<>();
        for (int i = 0; i < tblbmiforageList.size(); i++) {
            dataValue.add(new Entry(tblbmiforageList.get(i).getXaxis(), Float.parseFloat(tblbmiforageList.get(i).getCgmBoysBMISeconddNegSD())));
        }
        return dataValue;
    }

    private ArrayList<Entry> secondNegativeSDHeadcf() {
        ArrayList<Entry> dataValue = new ArrayList<>();
        for (int i = 0; i < tblheadcfforagesList.size(); i++) {
            dataValue.add(new Entry(tblheadcfforagesList.get(i).getXaxis(), Float.parseFloat(tblheadcfforagesList.get(i).getCgmBoysHeadcfSeconddNegSD())));
        }
        return dataValue;
    }

    //Girls
    private ArrayList<Entry> secondNegativeSDGirls() {
        ArrayList<Entry> dataValue = new ArrayList<>();
        for (int i = 0; i < tblweightforageList.size(); i++) {
            dataValue.add(new Entry(tblweightforageList.get(i).getXaxis(), Float.parseFloat(tblweightforageList.get(i).getCgmGirlsWtSeconddNegSD())));
        }
        return dataValue;
    }

    private ArrayList<Entry> secondNegativeSDHeightGirls() {
        ArrayList<Entry> dataValue = new ArrayList<>();
        for (int i = 0; i < tblheightforageList.size(); i++) {
            dataValue.add(new Entry(tblheightforageList.get(i).getXaxis(), Float.parseFloat(tblheightforageList.get(i).getCgmGirlsHtSeconddNegSD())));
        }
        return dataValue;
    }

    private ArrayList<Entry> secondNegativeSDBMIGirls() {
        ArrayList<Entry> dataValue = new ArrayList<>();
        for (int i = 0; i < tblbmiforageList.size(); i++) {
            dataValue.add(new Entry(tblbmiforageList.get(i).getXaxis(), Float.parseFloat(tblbmiforageList.get(i).getCgmGirlsBMISeconddNegSD())));
        }
        return dataValue;
    }

    private ArrayList<Entry> secondNegativeSDHeadcfGirls() {
        ArrayList<Entry> dataValue = new ArrayList<>();
        for (int i = 0; i < tblheadcfforagesList.size(); i++) {
            dataValue.add(new Entry(tblheadcfforagesList.get(i).getXaxis(), Float.parseFloat(tblheadcfforagesList.get(i).getCgmGirlsHeadcfSeconddNegSD())));
        }
        return dataValue;
    }

//    //Getting Third Negative SD Values
//    private ArrayList<Entry> thirdNegativeSD() {
//        ArrayList<Entry> dataValue = new ArrayList<>();
//        for (int i = 0; i < tblweightforageList.size(); i++) {
//            dataValue.add(new Entry(tblweightforageList.get(i).getXaxis(), Float.parseFloat(tblweightforageList.get(i).getCgmBoysWtThirddNegSD())));
//        }
//        return dataValue;
//    }
//
//    private ArrayList<Entry> thirdNegativeSDHeight() {
//        ArrayList<Entry> dataValue = new ArrayList<>();
//        for (int i = 0; i < tblheightforageList.size(); i++) {
//            dataValue.add(new Entry(tblheightforageList.get(i).getXaxis(), Float.parseFloat(tblheightforageList.get(i).getCgmBoysHtThirddNegSD())));
//        }
//        return dataValue;
//    }
//
//    private ArrayList<Entry> thirdNegativeSDBMI() {
//        ArrayList<Entry> dataValue = new ArrayList<>();
//        for (int i = 0; i < tblbmiforageList.size(); i++) {
//            dataValue.add(new Entry(tblbmiforageList.get(i).getXaxis(), Float.parseFloat(tblbmiforageList.get(i).getCgmBoysBMIThirddNegSD())));
//        }
//        return dataValue;
//    }
//
//    private ArrayList<Entry> thirdNegativeSDHeadcf() {
//        ArrayList<Entry> dataValue = new ArrayList<>();
//        for (int i = 0; i < tblheadcfforagesList.size(); i++) {
//            dataValue.add(new Entry(tblheadcfforagesList.get(i).getXaxis(), Float.parseFloat(tblheadcfforagesList.get(i).getCgmBoysHeadcfThirddNegSD())));
//        }
//        return dataValue;
//    }
//
//    //Girls
//    private ArrayList<Entry> thirdNegativeSDGirls() {
//        ArrayList<Entry> dataValue = new ArrayList<>();
//        for (int i = 0; i < tblweightforageList.size(); i++) {
//            dataValue.add(new Entry(tblweightforageList.get(i).getXaxis(), Float.parseFloat(tblweightforageList.get(i).getCgmGirlsWtThirddNegSD())));
//        }
//        return dataValue;
//    }
//
//    private ArrayList<Entry> thirdNegativeSDHeightGirls() {
//        ArrayList<Entry> dataValue = new ArrayList<>();
//        for (int i = 0; i < tblheightforageList.size(); i++) {
//            dataValue.add(new Entry(tblheightforageList.get(i).getXaxis(), Float.parseFloat(tblheightforageList.get(i).getCgmGirlsHtThirddNegSD())));
//        }
//        return dataValue;
//    }
//
//    private ArrayList<Entry> thirdNegativeSDBMIGirls() {
//        ArrayList<Entry> dataValue = new ArrayList<>();
//        for (int i = 0; i < tblbmiforageList.size(); i++) {
//            dataValue.add(new Entry(tblbmiforageList.get(i).getXaxis(), Float.parseFloat(tblbmiforageList.get(i).getCgmGirlsBMIThirddNegSD())));
//        }
//        return dataValue;
//    }
//
//    private ArrayList<Entry> thirdNegativeSDHeadcfGirls() {
//        ArrayList<Entry> dataValue = new ArrayList<>();
//        for (int i = 0; i < tblheadcfforagesList.size(); i++) {
//            dataValue.add(new Entry(tblheadcfforagesList.get(i).getXaxis(), Float.parseFloat(tblheadcfforagesList.get(i).getCgmGirlsHeadcfThirddNegSD())));
//        }
//        return dataValue;
//    }

    //Actual values
    private ArrayList<Entry> weightForAgeOfChild() {
        ArrayList<Entry> dataValue = new ArrayList<>();
        for (int i = 0; i < tblchildgrowthList.size(); i++) {
            dataValue.add(new Entry(tblchildgrowthList.get(i).getChlGMAgeInMonths(), Float.parseFloat(tblchildgrowthList.get(i).getChlGMWeight())));
        }
        return dataValue;
    }

    private ArrayList<Entry> heightForAgeOfChild() {
        ArrayList<Entry> dataValue = new ArrayList<>();
        for (int i = 0; i < tblchildgrowthList.size(); i++) {
            dataValue.add(new Entry(tblchildgrowthList.get(i).getChlGMAgeInMonths(), Float.parseFloat(tblchildgrowthList.get(i).getChlGMHeight())));
        }
        return dataValue;
    }

    private ArrayList<Entry> bmiForAgeOfChild() {
        ArrayList<Entry> dataValue = new ArrayList<>();
        for (int i = 0; i < tblchildgrowthList.size(); i++) {
            dataValue.add(new Entry(tblchildgrowthList.get(i).getChlGMAgeInMonths(), Float.parseFloat(tblchildgrowthList.get(i).getChlGMBMI())));
        }
        return dataValue;
    }

    private ArrayList<Entry> headcfForAgeOfChild() {
        ArrayList<Entry> dataValue = new ArrayList<>();
        for (int i = 0; i < tblchildgrowthList.size(); i++) {
            dataValue.add(new Entry(tblchildgrowthList.get(i).getChlGMAgeInMonths(), Float.parseFloat(tblchildgrowthList.get(i).getChlGMMuac())));
        }
        return dataValue;
    }

    private ArrayList<Entry> weightForAgeOfChildGirls() {
        ArrayList<Entry> dataValue = new ArrayList<>();
        for (int i = 0; i < tblchildgrowthList.size(); i++) {
            dataValue.add(new Entry(tblchildgrowthList.get(i).getChlGMAgeInMonths(), Float.parseFloat(tblchildgrowthList.get(i).getChlGMWeight())));
        }
        return dataValue;
    }

    private ArrayList<Entry> heightForAgeOfChildGirls() {
        ArrayList<Entry> dataValue = new ArrayList<>();
        for (int i = 0; i < tblchildgrowthList.size(); i++) {
            dataValue.add(new Entry(tblchildgrowthList.get(i).getChlGMAgeInMonths(), Float.parseFloat(tblchildgrowthList.get(i).getChlGMHeight())));
        }
        return dataValue;
    }

    private ArrayList<Entry> bmiForAgeOfChildGirls() {
        ArrayList<Entry> dataValue = new ArrayList<>();
        for (int i = 0; i < tblchildgrowthList.size(); i++) {
            dataValue.add(new Entry(tblchildgrowthList.get(i).getChlGMAgeInMonths(), Float.parseFloat(tblchildgrowthList.get(i).getChlGMBMI())));
        }
        return dataValue;
    }


    private ArrayList<Entry> headcfForAgeOfChildGirls() {
        ArrayList<Entry> dataValue = new ArrayList<>();
        for (int i = 0; i < tblchildgrowthList.size(); i++) {
            dataValue.add(new Entry(tblchildgrowthList.get(i).getChlGMAgeInMonths(), Float.parseFloat(tblchildgrowthList.get(i).getChlGMMuac())));
        }
        return dataValue;
    }

    private void initiateDrawer() {


        ArrayList<NavDrawerItem> navDrawerItems = new ArrayList<>();
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.editchildinfo), R.drawable.ic_edit_60));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.addsibling), R.drawable.general_examination_baby));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.viewparentdetails), R.drawable.ic_mother));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.sp_imm), R.drawable.immunisation));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.gcmaddnewvisit), R.drawable.registration));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.gcmregisterlist), R.drawable.child_growth));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deactivate), R.drawable.deactivate));

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        mDrawerList.setAdapter(new NavDrawerListAdapter(getApplicationContext(), navDrawerItems));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

// ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer,
                R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu(); // creates call to
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                getSupportActionBar().setDisplayShowTitleEnabled(false);
                getSupportActionBar().setDisplayShowCustomEnabled(false);

                try {
                    if (tblChildInfo != null) {
                        getSupportActionBar().setTitle((tblChildInfo.getChlBabyname() + "\t"));
                    }
                } catch (Exception e) {


                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                    crashlytics.log(e.getMessage());
                }
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                try {
                    invalidateOptionsMenu(); // creates call to
                    getSupportActionBar().setDisplayShowHomeEnabled(false);
                    getSupportActionBar().setDisplayShowTitleEnabled(false);
                    getSupportActionBar().setDisplayShowCustomEnabled(true);
                    getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);

                    aqChildGraph.id(R.id.ivWomanImg).getImageView().setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            mDrawerLayout.closeDrawer(mDrawerList);
                        }
                    });

                    aqChildGraph.id(R.id.tvWomanName).text((tblChildInfo.getChlBabyname() + "(" + tblChildInfo.getChlGender() + ")"));
                    if (tblChildInfo.getChlDateTimeOfBirth().trim().length() > 0) {
                        aqChildGraph.id(R.id.tvInfo1).text(
                                (getResources().getString(R.string.age) + " " +
                                        DateTimeUtil.calculateAge(tblChildInfo.getChlDateTimeOfBirth())));
                    }
                } catch (Exception e) {


                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                    crashlytics.log(e.getMessage());
                }
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    /**
     * This method is to sync draw,erToggle and to replace up caret icon to menu
     * icon in Action Bar
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportActionBar().setTitle((tblChildInfo.getChlBabyname() +
                " (" +
                tblChildInfo.getChlGender() + ")"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer1);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);

        aqChildGraph.id(R.id.tvWomanName).text((tblChildInfo.getChlBabyname() +
                " (" +
                tblChildInfo.getChlGender() + ")"));
        if (tblChildInfo.getChlDateTimeOfBirth() != null && tblChildInfo.getChlDateTimeOfBirth().length() > 0)
            aqChildGraph.id(R.id.tvInfo1).text((getResources().getString(R.string.age)
                    + " " + DateTimeUtil.calculateAge(tblChildInfo.getChlDateTimeOfBirth())));

        aqChildGraph.id(R.id.ivWomanImg).gone();
        getMenuInflater().inflate(R.menu.graphmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            case R.id.logout: {
                Intent goToScreen = new Intent(BoysGraph.this, LoginActivity.class);
                displayAlert(getResources().getString(R.string.m111), goToScreen);
                return true;
            }
            case R.id.home: {
                Intent home = new Intent(BoysGraph.this, MainMenuActivity.class);
                displayAlert(getResources().getString(R.string.exit), home);
                return true;
            }
            case R.id.wlist: {
                Intent goToScreen = new Intent(BoysGraph.this, RegisteredWomenActionTabs.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }
            case R.id.about: {
                Intent goToScreen = new Intent(BoysGraph.this, AboutActivity.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }

            case R.id.pdf: {
                resetGraphs();
                getWeightForAge();
                getHeightForage();
                Intent goToScreen = new Intent(BoysGraph.this, PdfGeneratorActivity.class);
                goToScreen.putExtra("tblChildInfo", tblChildInfo);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                goToScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                goToScreen.putExtra("weight", byteArrayweight);
                goToScreen.putExtra("height", byteArrayHeight);
                startActivity(goToScreen);
                return true;
            }
//            20May2021 Bindu add reset button
            case R.id.reset:{
               resetGraphs();
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }

    }


    /**
     * Save Confirmation Alert
     *
     * @param goToScreen
     * @param spanText2
     */
    private void displayAlert(final String spanText2, final Intent goToScreen) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        String strMess;
        if (goToScreen != null)
            strMess = getResources().getString(R.string.m121);
        else
            strMess = getResources().getString(R.string.ok);
        alertDialogBuilder.setMessage(spanText2).setCancelable(false).setNegativeButton(
                strMess, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (goToScreen != null) {
                            if (spanText2.contains(getResources().getString(R.string.save))) {

                            } else {
                                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                startActivity(goToScreen);
                                if (spanText2.contains("logout")) {
                                    try {
                                    } catch (Exception e) {
                                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                                        crashlytics.log(e.getMessage());
                                    }
                                }
                            }
                        } else
                            dialog.cancel();
                    }
                });

        if (goToScreen != null) {
            alertDialogBuilder.setPositiveButton(getResources().getString(R.string.m122),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
        }
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    public class MyXAxisValueFormatter extends IndexAxisValueFormatter {
        private String[] mValues;

        public MyXAxisValueFormatter(String[] values) {
            this.mValues = values;
        }

        @Override
        public String getFormattedValue(float value) {
            return mValues[(int) value];
        }
    }

    public class MyYAxisValueFormatter extends IndexAxisValueFormatter {
        private String[] valuesY;

        public MyYAxisValueFormatter(String[] values) {
            this.valuesY = values;
        }

        @Override
        public String getFormattedValue(float value) {
            return valuesY[(int) value];

        }
    }

    /**
     * The click listener for ListView in the navigation drawer
     */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            mDrawerLayout.closeDrawer(mDrawerList);
            try {

                switch (position) {

                    case 0:
                        if (tblChildInfo.getChlReg() == 1) {
                            Intent nextScreen = new Intent(BoysGraph.this, EditRegChildInfoActivity.class);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                            nextScreen.putExtra("tblChildInfo", tblChildInfo);
                            startActivity(nextScreen);
                        } else {
                            Intent nextScreen = new Intent(BoysGraph.this, EditChildActivity.class);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                            nextScreen.putExtra("tblChildInfo", tblChildInfo);
                            startActivity(nextScreen);
                        }
                        break;

                    case 1: {
                        if (user.getIsDeactivated() == 1)
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.userdeactivated), Toast.LENGTH_LONG).show();
                        else {
                            Intent addSibling = new Intent(BoysGraph.this, AddSiblingsActivity.class);
                            addSibling.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            addSibling.putExtra("appState", getIntent().getBundleExtra("appState"));
//                            if (tblChildInfo.getChlReg() == 1)
                            String strParentId = tblChildInfo.getChlParentId();//17Jun2021 Arpitha
                            if(strParentId != null && strParentId.trim().length() > 0)//17Jun2021 Arpitha
                                addSibling.putExtra("chlParentId", tblChildInfo.getChlParentId());
                            else
                                addSibling.putExtra("WomanId", tblChildInfo.getWomanId());
                            startActivity(addSibling);
                        }
                        break;
                    }
                    case 2: {
                        if (tblChildInfo.getChlParentId() != null && tblChildInfo.getChlParentId().trim().length() > 0) {
                            Intent viewParent = new Intent(BoysGraph.this, ViewParentDetails.class);
                            viewParent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            viewParent.putExtra("appState", getIntent().getBundleExtra("appState"));
                            viewParent.putExtra("tblChildInfo", tblChildInfo);
                            startActivity(viewParent);
                        } else {
                            WomanRepository womanRepository = new WomanRepository(databaseHelper);
                            tblregisteredwomen woman = womanRepository.getRegistartionDetails(tblChildInfo.getWomanId(), tblChildInfo.getUserId());
                            appState.selectedWomanId = tblChildInfo.getWomanId();
                            Intent nextScreen = new Intent(BoysGraph.this, ViewProfileActivity.class);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                            nextScreen.putExtra("woman", woman);
                            startActivity(nextScreen);
//                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.ntapplicable), Toast.LENGTH_LONG).show();
                        }
                        break;
                    }

                    case 3: {
                        if (tblChildInfo.getChlDeliveryResult() <= 1) {
                            Intent nextScreen = new Intent(BoysGraph.this, ImmunizationListActivity.class);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                            nextScreen.putExtra("tblChildInfo", tblChildInfo);
                            startActivity(nextScreen);
                        } else
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.m053), Toast.LENGTH_LONG).show();
                        break;
                    }

                    case 4:     {
                        if (((tblChildInfo.getChlDeactDate() != null &&
                                tblChildInfo.getChlDeactDate().trim().length() > 0)
                                || user.getIsDeactivated() == 1)) {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.userdeactivated), Toast.LENGTH_LONG).show();
                        } else {
                            //Mani 17 June 2021
                            if ((DateTimeUtil.calculateAgeInMonths(tblChildInfo.getChlDateTimeOfBirth()) >= 60)||tblChildInfo.getChlDeliveryResult()>=1)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.aboveagewarning), Toast.LENGTH_LONG).show();
                            else {
                                Intent goToScreen = new Intent(BoysGraph.this, CGMNewVisit.class);
                                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                goToScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                                goToScreen.putExtra("tblChildInfo", tblChildInfo);
                                startActivity(goToScreen);
                            }

                        }
                        break;
                    }
                    case 5: {
                        Intent nextScreen = new Intent(BoysGraph.this, CGMList.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                        nextScreen.putExtra("tblChildInfo", tblChildInfo);
                        startActivity(nextScreen);

                        break;
                    }


                    case 6: {
                        Intent nextScreen = new Intent(BoysGraph.this, ChildDeactivateActivity.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                        nextScreen.putExtra("tblChildInfo", tblChildInfo);
                        startActivity(nextScreen);

                        break;
                    }
                }

            } catch (Exception e) {


                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
        }
    }

//    private void createpdf(){
//        // create a new document
//        PdfDocument document = new PdfDocument();
//        Paint paint = new Paint();
//        paint.setColor(Color.RED);
//        // crate a page description
//        PdfDocument.PageInfo pageInfo =
//                new PdfDocument.PageInfo.Builder(100, 100, 1).create();
//        // start a page
//        PdfDocument.Page page = document.startPage(pageInfo);
//        Canvas canvas = page.getCanvas();
//        canvas.drawText("Welcome to sentiacare",40,50,paint);
//        // finish the page
//        document.finishPage(page);
//
//        // write the document content
//        String targetPdf = "/sdcard/test.pdf";
//        File filePath = new File(targetPdf);
//        try {
//            document.writeTo(new FileOutputStream(filePath));
//            Toast.makeText(this, "Done", Toast.LENGTH_LONG).show();
//        } catch (IOException e) {
//            e.printStackTrace();
//            Toast.makeText(this, "Something wrong: " + e.toString(),
//                    Toast.LENGTH_LONG).show();
//        }
//
//        // close the document
//        document.close();
//
//
//    }

    private void resetGraphs() {
//        20May2021 Bindu check for null
        if(mLineChart != null)
            mLineChart.fitScreen();
        if(mLineChartHeight != null)
            mLineChartHeight.fitScreen();
    }

}