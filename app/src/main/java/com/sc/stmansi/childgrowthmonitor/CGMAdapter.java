package com.sc.stmansi.childgrowthmonitor;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.R;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.pendingservices.IndicatorViewClickListener;
import com.sc.stmansi.recyclerview.CGMViewHolder;
import com.sc.stmansi.recyclerview.ClickListener;
import com.sc.stmansi.repositories.CGMRepository;
import com.sc.stmansi.repositories.DeliveryRepository;
import com.sc.stmansi.tables.TblChildInfo;
import com.sc.stmansi.tables.TblDeliveryInfo;
import com.sc.stmansi.tables.tblchildgrowth;
import com.sc.stmansi.tables.tblweightforage;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CGMAdapter extends RecyclerView.Adapter<CGMViewHolder> {

    Context context;
    List<tblchildgrowth> listChildGrowthData;
    List<TblChildInfo> tblChildInfoList;
    DatabaseHelper databaseHelper;
//    List<tblweightforage> tblweightforageList;
    private ClickListener viewClickListener;
    private IndicatorViewClickListener indicatorViewClickListener;

    public CGMAdapter(Context context, List<tblchildgrowth> childData, ClickListener viewClickListener) {
        this.viewClickListener = viewClickListener;
        this.indicatorViewClickListener = (IndicatorViewClickListener) context;
        this.listChildGrowthData = childData;
//        13May2021 Bindu
        context = context;
    }

    @NonNull
    @Override
    public CGMViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_cgmchildinfo, parent, false);
        v.setOnClickListener(viewClickListener);
        context = parent.getContext();
        return new CGMViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CGMViewHolder cgmViewHolder, int i) {
        final tblchildgrowth data = listChildGrowthData.get(i);


        databaseHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);

        CGMRepository cgmRepository = new CGMRepository(databaseHelper);

        try {
            tblChildInfoList = cgmRepository.getChildData(data.getChlId());
//            Integer age = DateTimeUtil.calculateAgeInMonths(tblChildInfoList.get(0).getChlDateTimeOfBirth());
//            tblweightforageList = cgmRepository.getWeightBasedOnAge(age); //13MAy2021 Bindu comment
        } catch (SQLException e) {
            e.printStackTrace();
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        /*cgmViewHolder.visitnum.setText("# " + listChildGrowthData.get(i).getChlGMVisitId());
//        cgmViewHolder.childAge.setText(context.getResources().getString(R.string.ageatvisit) + ": " +
//                DateTimeUtil.calculateAge(tblChildInfoList.get(i).getChlDateTimeOfBirth()));
        cgmViewHolder.childAge.setText(context.getResources().getString(R.string.ageatvisit) + ": " + listChildGrowthData.get(i).getChlGMAge());
        cgmViewHolder.childVisitdate.setText(context.getResources().getString(R.string.gcmvisitdate) + ": " + listChildGrowthData.get(i).getChlGMVisitdate());
        cgmViewHolder.childWeightAtVisit.setText(context.getResources().getString(R.string.gcmweightatvisit) + ": " + listChildGrowthData.get(i).getChlGMWeight() + " " + context.getResources().getString(R.string.kgs));
*/

        cgmViewHolder.visitnum.setText("# " + listChildGrowthData.get(i).getChlGMVisitId());
        cgmViewHolder.childAge.setText(""+listChildGrowthData.get(i).getChlGMAge());
        cgmViewHolder.childVisitdate.setText(""+listChildGrowthData.get(i).getChlGMVisitdate());
        cgmViewHolder.childWeightAtVisit.setText(""+listChildGrowthData.get(i).getChlGMWeight());


//        12May2021 Bindu set compl indicator //14May2021 bindu add muac
        if(listChildGrowthData.get(i).getChlGMComplication() != null && listChildGrowthData.get(i).getChlGMComplication().length() > 0) {
            if(listChildGrowthData.get(i).getChlGMComplication().contains(context.getResources().getString(R.string.strsevunderweight)) || listChildGrowthData.get(i).getChlGMComplication().contains(context.getResources().getString(R.string.strsevacmal)))
                cgmViewHolder.childWeightIndicator.setBackgroundColor(context.getResources().getColor(R.color.red));
            else if(listChildGrowthData.get(i).getChlGMComplication().equalsIgnoreCase(context.getResources().getString(R.string.strmodunderweight)) || listChildGrowthData.get(i).getChlGMComplication().contains(context.getResources().getString(R.string.strmodacmal)))
                cgmViewHolder.childWeightIndicator.setBackgroundColor(context.getResources().getColor(R.color.yellow_A700));
            else if(listChildGrowthData.get(i).getChlGMComplication().equalsIgnoreCase(context.getResources().getString(R.string.strnormalweight)) || listChildGrowthData.get(i).getChlGMComplication().contains(context.getResources().getString(R.string.strnormalacmal)))
                cgmViewHolder.childWeightIndicator.setBackgroundColor(context.getResources().getColor(R.color.green));
        }else
            cgmViewHolder.childWeightIndicator.setBackgroundColor(context.getResources().getColor(R.color.white));

        cgmViewHolder.childWeightIndicator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayComplicationAlert(listChildGrowthData.get(i).getChlGMComplication());
            }
        });


        /*if ((Float.parseFloat(tblweightforageList.get(0).getCgmBoysWtFirstdNegSD()) < Float.parseFloat(listChildGrowthData.get(i).getChlGMWeight())) &&
                (Float.parseFloat(listChildGrowthData.get(i).getChlGMWeight()) < Float.parseFloat(tblweightforageList.get(0).getCgmBoysWtFirstdNegSD()))) {
            cgmViewHolder.childWeightIndicator.setBackgroundColor(context.getResources().getColor(R.color.green));
        } else if ((Float.parseFloat(listChildGrowthData.get(i).getChlGMWeight()) >= Float.parseFloat(tblweightforageList.get(0).getCgmBoysWtFirstdNegSD())) &&
                (Float.parseFloat(listChildGrowthData.get(i).getChlGMWeight()) < Float.parseFloat(tblweightforageList.get(0).getCgmBoysWtFirstdNegSD()))
        ) {
            cgmViewHolder.childWeightIndicator.setBackgroundColor(context.getResources().getColor(R.color.yellow_A700));
        } else {
            cgmViewHolder.childWeightIndicator.setBackgroundColor(context.getResources().getColor(R.color.red));
        }*/


    }

    @Override
    public int getItemCount() {
        return listChildGrowthData.size();
    }

    public void displayComplicationAlert(String compl) {
        try {
            ArrayList<String> complL = new ArrayList<>();
            String[] chlCompl =compl.split(",");
            complL = new ArrayList<>();

            for (int i = 1; i <= chlCompl.length; i++) {
//                Weight
                if (chlCompl[i - 1] != null && chlCompl[i - 1].trim().length() > 0)
                    if(chlCompl[i-1].equalsIgnoreCase(context.getResources().getString(R.string.strsevunderweight)))
                        complL.add(context.getResources().getString(R.string.sevunderweight));
                    else if(chlCompl[i-1].equalsIgnoreCase(context.getResources().getString(R.string.strmodunderweight)))
                        complL.add(context.getResources().getString(R.string.modunderweight));
                    else if(chlCompl[i-1].equalsIgnoreCase(context.getResources().getString(R.string.strnormalweight)))
                        complL.add(context.getResources().getString(R.string.normalweight));

//                    MUAC
                if(chlCompl[i-1].equalsIgnoreCase(context.getResources().getString(R.string.strsevacmal)))
                    complL.add(context.getResources().getString(R.string.sevacumal));
                else if(chlCompl[i-1].equalsIgnoreCase(context.getResources().getString(R.string.strmodacmal)))
                    complL.add(context.getResources().getString(R.string.modacumal));
                else if(chlCompl[i-1].equalsIgnoreCase(context.getResources().getString(R.string.strnormalacmal)))
                    complL.add(context.getResources().getString(R.string.normalacumal));
            }

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                View convertView = LayoutInflater.from(context).inflate(R.layout.complicated_reasons_list, null, false);

                ListView lv = convertView.findViewById(R.id.lvCompliatedReasonslist);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.listitem_cgm, complL);

                alertDialog.setView(convertView).setCancelable(true).setTitle((context.getResources().getString(R.string.statusatvisit)))
                        .setPositiveButton((context.getResources().getString(R.string.ok)), new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                lv.setAdapter(adapter);
                alertDialog.show();

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }
}
