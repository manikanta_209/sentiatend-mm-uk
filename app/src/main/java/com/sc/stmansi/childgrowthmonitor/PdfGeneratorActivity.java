package com.sc.stmansi.childgrowthmonitor;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.R;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.repositories.ChildRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.tables.TblChildInfo;
import com.sc.stmansi.tables.TblChlParentDetails;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.tejpratapsingh.pdfcreator.activity.PDFCreatorActivity;
import com.tejpratapsingh.pdfcreator.utils.PDFUtil;
import com.tejpratapsingh.pdfcreator.views.PDFBody;
import com.tejpratapsingh.pdfcreator.views.PDFFooterView;
import com.tejpratapsingh.pdfcreator.views.PDFHeaderView;
import com.tejpratapsingh.pdfcreator.views.basic.PDFHorizontalView;
import com.tejpratapsingh.pdfcreator.views.basic.PDFImageView;
import com.tejpratapsingh.pdfcreator.views.basic.PDFLineSeparatorView;
import com.tejpratapsingh.pdfcreator.views.basic.PDFTextView;
import com.tejpratapsingh.pdfcreator.views.basic.PDFVerticalView;

import java.io.File;
import java.sql.SQLException;
import java.util.List;
import java.util.Locale;

public class PdfGeneratorActivity extends PDFCreatorActivity {

    byte[] byteArrayWeight,byteArrayHeight,byteArrayBmi,byteArrayHeadcf;
    Bitmap bmpWeight,bmpHeight,bmpBmi,bmpHeadcf;
    TblChildInfo tblChildInfo;
    private AppState appState;
    DatabaseHelper databaseHelper;
    List<TblChlParentDetails> tblChlParentDetails;
    tblregisteredwomen tblregisteredwomenList;
    List<View> contentViews;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Get intent extras
        byteArrayWeight = getIntent().getByteArrayExtra("weight");
        byteArrayHeight=getIntent().getByteArrayExtra("height");
//        byteArrayBmi=getIntent().getByteArrayExtra("bmi");
//        byteArrayHeadcf=getIntent().getByteArrayExtra("headcf");

        Bundle bundle = getIntent().getBundleExtra("globalState");
        tblChildInfo = (TblChildInfo) getIntent().getSerializableExtra("tblChildInfo");
        appState = bundle.getParcelable("appState");

        databaseHelper = OpenHelperManager.getHelper(getApplicationContext(), DatabaseHelper.class);

        bmpWeight = BitmapFactory.decodeByteArray(byteArrayWeight, 0, byteArrayWeight.length);
        bmpHeight = BitmapFactory.decodeByteArray(byteArrayHeight, 0, byteArrayHeight.length);
//        bmpBmi = BitmapFactory.decodeByteArray(byteArrayBmi, 0, byteArrayBmi.length);
//        bmpHeadcf = BitmapFactory.decodeByteArray(byteArrayHeadcf, 0, byteArrayHeadcf.length);




        ChildRepository childRepository = new ChildRepository(databaseHelper);
        WomanRepository womanRepository = new WomanRepository(databaseHelper);
        try {
            if (tblChildInfo.getChlParentId()!=null && tblChildInfo.getChlParentId().trim().length()>0)
                tblChlParentDetails = childRepository.getParentDetails(tblChildInfo.getChlParentId());
            else
                tblregisteredwomenList= womanRepository.getRegistartionDetails(tblChildInfo.getWomanId(),tblChildInfo.getUserId());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }


        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        createPDF(tblChildInfo.getChlBabyname()+"_Childgrowthgraph", new PDFUtil.PDFUtilListener() {
            @Override
            public void pdfGenerationSuccess(File savedPDFFile) {
                Toast.makeText(PdfGeneratorActivity.this, "PDF Created", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void pdfGenerationFailure(Exception exception) {
                Toast.makeText(PdfGeneratorActivity.this, "PDF NOT Created", Toast.LENGTH_SHORT).show();
            }
        });

//        PDFUtil.getInstance().generatePDF();

    }

    @Override
    protected PDFHeaderView getHeaderView(int forPage) {
        PDFHeaderView headerView = new PDFHeaderView(getApplicationContext());

        PDFHorizontalView horizontalView = new PDFHorizontalView(getApplicationContext());



        PDFTextView pdfTextView1 = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.H3);
        SpannableString word1 = null;
        try {
            word1 = new SpannableString(DateTimeUtil.getTodaysDate() +" "+DateTimeUtil.getCurrentTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        word1.setSpan(new ForegroundColorSpan(Color.DKGRAY), 0, word1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        pdfTextView1.setText(word1);
        pdfTextView1.setLayout(new LinearLayout.LayoutParams(
                0,
                ViewGroup.LayoutParams.MATCH_PARENT,1));
        pdfTextView1.getView().setGravity(Gravity.CENTER_VERTICAL);

        pdfTextView1.getView().setTypeface(pdfTextView1.getView().getTypeface(), Typeface.NORMAL);

        horizontalView.addView(pdfTextView1);



        PDFTextView pdfTextView = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.H2);
        SpannableString word = new SpannableString("Child Growth Record");
        word.setSpan(new ForegroundColorSpan(Color.DKGRAY), 0, word.length(), Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        pdfTextView.setText(word);
        pdfTextView.setLayout(new LinearLayout.LayoutParams(
                0,
               ViewGroup.LayoutParams.MATCH_PARENT, 1));
        pdfTextView.getView().setGravity(Gravity.CENTER_VERTICAL);
        pdfTextView.getView().setTypeface(pdfTextView.getView().getTypeface(), Typeface.BOLD);

        horizontalView.addView(pdfTextView);




        PDFImageView imageView = new PDFImageView(getApplicationContext());
        LinearLayout.LayoutParams imageLayoutParam = new LinearLayout.LayoutParams(
                0,
                ViewGroup.LayoutParams.MATCH_PARENT, 1);
        imageView.setImageResource(R.mipmap.ic_sentiatendasha);
//        imageLayoutParam.setMargins(0, 0, 0, 0);
        imageView.setLayout(imageLayoutParam);

        horizontalView.addView(imageView);
        headerView.addView(horizontalView);

        PDFLineSeparatorView lineSeparatorView1 = new PDFLineSeparatorView(getApplicationContext()).setBackgroundColor(Color.BLACK);
        headerView.addView(lineSeparatorView1);

        return headerView;
    }

    @Override
    protected PDFBody getBodyViews() {
        PDFFooterView pdfFooterView=new PDFFooterView(getApplicationContext());

        PDFBody pdfBody = new PDFBody();
        PDFVerticalView verticalView = new PDFVerticalView(getApplicationContext());

        PDFHorizontalView horizontalView = new PDFHorizontalView(getApplicationContext());
        PDFHorizontalView horizontalView1 = new PDFHorizontalView(getApplicationContext());

        PDFTextView pdfChildName = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.H3);
        pdfChildName.setText("Child Name" + " : " + tblChildInfo.getChlBabyname());
        pdfChildName.setLayout(new LinearLayout.LayoutParams(
                0,
                LinearLayout.LayoutParams.MATCH_PARENT, 1));
        pdfChildName.getView().setGravity(Gravity.CENTER_VERTICAL);
//        pdfBody.addView(pdfChildName);
        horizontalView.addView(pdfChildName);

        PDFTextView pdfChildGender = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.H3);
        pdfChildGender.setText("Gender" + " : " + tblChildInfo.getChlGender());
        pdfChildGender.setLayout(new LinearLayout.LayoutParams(
                0,
                LinearLayout.LayoutParams.MATCH_PARENT, 1));
        pdfChildGender.getView().setGravity(Gravity.CENTER_VERTICAL);
//        pdfBody.addView(pdfChildGender);
        horizontalView.addView(pdfChildGender);

        PDFTextView pdfChildob = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.H3);
        pdfChildob.setText("Date Of Birth" + " : " + tblChildInfo.getChlDateTimeOfBirth());
        pdfChildob.setLayout(new LinearLayout.LayoutParams(
                0,
                LinearLayout.LayoutParams.MATCH_PARENT, 1));
        pdfChildob.getView().setGravity(Gravity.CENTER_VERTICAL);
        horizontalView.addView(pdfChildob);
        pdfBody.addView(horizontalView);


        PDFTextView pdfChildRchid = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.H3);
        pdfChildRchid.setText("RCH Id" + " : " + tblChildInfo.getChildRCHID());
        pdfChildRchid.setLayout(new LinearLayout.LayoutParams(
                0,
                LinearLayout.LayoutParams.MATCH_PARENT, 1));
        pdfChildRchid.getView().setGravity(Gravity.CENTER_VERTICAL);
        horizontalView1.addView(pdfChildRchid);

        PDFTextView pdfChildMother = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.H3);

        if (tblChildInfo.getChlParentId() != null && tblChildInfo.getChlParentId().trim().length() > 0)
             pdfChildMother.setText("Mother Name" + " : " + tblChlParentDetails.get(0).getChlMotherName());
        else
            pdfChildMother.setText("Mother Name" + " : " + tblregisteredwomenList.getRegWomanName());

        pdfChildMother.setLayout(new LinearLayout.LayoutParams(
                0,
                LinearLayout.LayoutParams.MATCH_PARENT, 1));
        pdfChildMother.getView().setGravity(Gravity.CENTER_VERTICAL);
        horizontalView1.addView(pdfChildMother);

        pdfBody.addView(horizontalView1);

        PDFLineSeparatorView lineSeparatorView4 = new PDFLineSeparatorView(getApplicationContext()).setBackgroundColor(Color.BLACK);
        pdfBody.addView(lineSeparatorView4);




        PDFImageView pdfImageView = new PDFImageView(getApplicationContext());
        pdfImageView.setImageBitmap(bmpWeight);
        pdfImageView.setLayout(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                550, 0));
        pdfBody.addView(pdfImageView);


        PDFTextView graphNameWeight = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.H3);
        graphNameWeight.setText("Fig.Weight For Age Graph");
        graphNameWeight.setLayout(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                20, 0));
        graphNameWeight.getView().setGravity(Gravity.CENTER_HORIZONTAL);
        pdfBody.addView(graphNameWeight);


        PDFImageView pdfImageView1 = new PDFImageView(getApplicationContext());
        pdfImageView1.setImageBitmap(bmpHeight);
        pdfImageView1.setLayout(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                600, 0));
        pdfBody.addView(pdfImageView1);

        PDFTextView graphNameHeight = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.H3);
        graphNameHeight.setText("Fig.Height For Age Graph");
        graphNameHeight.setLayout(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                20, 0));
        graphNameHeight.getView().setGravity(Gravity.CENTER_HORIZONTAL);
        pdfBody.addView(graphNameHeight);

//        PDFImageView pdfImageView2 = new PDFImageView(getApplicationContext());
//        pdfImageView2.setImageBitmap(bmpBmi);
//        pdfImageView2.setLayout(new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.MATCH_PARENT,
//                600, 0));
//        pdfBody.addView(pdfImageView2);

//        PDFTextView graphNameBmi = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.H3);
//        graphNameBmi.setText("Fig.BMI For Age Graph");
//        graphNameBmi.setLayout(new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.MATCH_PARENT,
//                20, 0));
//        graphNameBmi.getView().setGravity(Gravity.CENTER_HORIZONTAL);
//        pdfBody.addView(graphNameBmi);


//        PDFImageView pdfImageView3 = new PDFImageView(getApplicationContext());
//        pdfImageView3.setImageBitmap(bmpHeadcf);
//        pdfImageView3.setLayout(new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.MATCH_PARENT,
//                600, 0));
//        pdfBody.addView(pdfImageView3);
//
//        PDFTextView graphNameHeadcf = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.H3);
//        graphNameHeadcf.setText("Fig.Head Circumference For Age Graph");
//        graphNameHeadcf.setLayout(new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.MATCH_PARENT,
//                20, 0));
//        graphNameHeadcf.getView().setGravity(Gravity.CENTER_HORIZONTAL);
//        pdfBody.addView(graphNameHeadcf);

        PDFLineSeparatorView lineSeparatorView5 = new PDFLineSeparatorView(getApplicationContext()).setBackgroundColor(Color.BLACK);
        pdfFooterView.addView(lineSeparatorView5);

        return  pdfBody;    }

    @Override
    protected PDFFooterView getFooterView(int forPage) {
        PDFFooterView footerView = new PDFFooterView(getApplicationContext());

        PDFHorizontalView horizontalView = new PDFHorizontalView(getApplicationContext());



        PDFTextView pdfTextViewPage = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.SMALL);
        pdfTextViewPage.setText(String.format(Locale.getDefault(), "Page: %d", forPage + 1));
        pdfTextViewPage.setLayout(new LinearLayout.LayoutParams(
                0,
                ViewGroup.LayoutParams.MATCH_PARENT, 3));
        pdfTextViewPage.getView().setGravity(Gravity.CENTER_VERTICAL);
//        footerView.addView(pdfTextViewPage);
        horizontalView.addView(pdfTextViewPage);

        PDFTextView pdfTextSentiaName = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.SMALL);
        pdfTextSentiaName.setText("www.sentiacare.com");
        pdfTextSentiaName.setLayout(new LinearLayout.LayoutParams(
                0,
                ViewGroup.LayoutParams.MATCH_PARENT, 1));
        pdfTextSentiaName.getView().setGravity(Gravity.CENTER_VERTICAL);
        horizontalView.addView(pdfTextSentiaName);



        footerView.addView(horizontalView);

        return footerView;    }

    @Override
    protected void onNextClicked(File savedPDFFile) {
        Uri pdfUri = Uri.fromFile(savedPDFFile);

        Intent intentPdfViewer = new Intent(PdfGeneratorActivity.this, PdfViewerActivity.class);
        intentPdfViewer.putExtra(PdfViewerActivity.PDF_FILE_URI, pdfUri);

        startActivity(intentPdfViewer);
    }

    }