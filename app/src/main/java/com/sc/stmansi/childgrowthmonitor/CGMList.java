package com.sc.stmansi.childgrowthmonitor;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.legacy.app.ActionBarDrawerToggle;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.stmt.query.In;
import com.sc.stmansi.HomeVisit.HomeVisitViewActivity;
import com.sc.stmansi.R;
import com.sc.stmansi.childhomevisit.ChildHomeVisit;
import com.sc.stmansi.childhomevisit.ChildHomeVisitHistory;
import com.sc.stmansi.childregistration.AddSiblingsActivity;
import com.sc.stmansi.childregistration.EditChildActivity;
import com.sc.stmansi.childregistration.EditRegChildInfoActivity;
import com.sc.stmansi.childregistration.ViewParentDetails;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.common.NavDrawerItem;
import com.sc.stmansi.common.NavDrawerListAdapter;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.deactivate.ChildDeactivateActivity;
import com.sc.stmansi.immunization.ImmunizationListActivity;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.pendingservices.IndicatorViewClickListener;
import com.sc.stmansi.recyclerview.ClickListener;
import com.sc.stmansi.recyclerview.RecyclerViewMargin;
import com.sc.stmansi.repositories.CGMRepository;
import com.sc.stmansi.repositories.LoginRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.tables.TblChildInfo;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblchildgrowth;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.viewprofile.ViewProfileActivity;
import com.sc.stmansi.womanlist.RegisteredWomenActionTabs;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CGMList extends AppCompatActivity implements ClickListener,
        IndicatorViewClickListener, AdapterView.OnItemSelectedListener, View.OnClickListener {
    AQuery aqChildgmList;
    List<TblChildInfo> childList;
    List<tblchildgrowth> tblchildgrowthList;
    List<tblchildgrowth> tblchildgrowthListData;
    String childid;
    TblChildInfo tblChildInfo;
    tblchildgrowth childgrowth;
    TblInstusers user;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private AppState appState;
    private RecyclerView.Adapter registeredChildAdapter;
    private DatabaseHelper databaseHelper;
    private List<TblChildInfo> childData;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private int itemPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cgm_list);
        try {
            databaseHelper = getHelper();
            aqChildgmList = new AQuery(this);

            //Get data from intent
            Bundle bundle = getIntent().getBundleExtra("globalState");
            appState = bundle.getParcelable("appState");
            tblChildInfo = (TblChildInfo) getIntent().getSerializableExtra("tblChildInfo");

            UserRepository userRepository = new UserRepository(databaseHelper);
            user = userRepository.getOneAuditedUser(appState.ashaId);

            initiateDrawer();

            childid = tblChildInfo.getChildID();


            recyclerView = (RecyclerView) aqChildgmList.id(R.id.cgm_child_recycler_view).getView();

            recyclerView.setHasFixedSize(true);
            // use a linear layout manager
            layoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(layoutManager);
            RecyclerViewMargin decoration = new RecyclerViewMargin(1, 1);
            recyclerView.addItemDecoration(decoration);
            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                    DividerItemDecoration.VERTICAL);
            DividerItemDecoration dividerItemDecoration1 = new DividerItemDecoration(recyclerView.getContext(),
                    DividerItemDecoration.HORIZONTAL);
            recyclerView.addItemDecoration(dividerItemDecoration);
            recyclerView.addItemDecoration(dividerItemDecoration1);


            CGMRepository cgmRepository = new CGMRepository(databaseHelper);

            tblchildgrowthList = cgmRepository.getChildGrowthData(childid);


            registeredChildAdapter = new CGMAdapter(this, tblchildgrowthList, this);
            recyclerView.setAdapter(registeredChildAdapter);
            updateLabels();
//        registeredChildAdapter.notifyItemInserted(0);
        }catch (Exception e) {
            e.printStackTrace();
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }

    }

    //    initialize db object
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }


//    private List<tblchildgrowth> getchildgrowthdata() throws SQLException{
//        CGMRepository cgmRepository=new CGMRepository(databaseHelper);
//        tblchildgrowthList=cgmRepository.getChildGrowthData(childid);
//        return tblchildgrowthList;
//    }


    @Override
    public void onClick(View v) {
        setSelectedVisitDetails(v);

        Intent intent=new Intent(this,EditCGMNewVisit.class);
        intent.putExtra("globalState",getIntent().getBundleExtra("globalState"));
        intent.putExtra("pos",itemPosition);
        intent.putExtra("childgrowth", childgrowth);
        intent.putExtra("tblChildInfo", tblChildInfo);

        startActivity(intent);

    }

    private void setSelectedVisitDetails(View v) {
        itemPosition = recyclerView.getChildLayoutPosition(v);
        childgrowth=tblchildgrowthList.get(itemPosition);
    }

    @Override
    public boolean onLongClick(View v) {
        return false;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void displayAlert(int selectedItemPosition) {

    }

    @Override
    public void displayHomeVisitList(int selectedItemPosition) {

    }

    @Override
    public void displayDelComplicationAlert(int selectedItemPosition) {

    }

    @Override
    public void displayLMPConfirmation(int selectedItemPosition) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getSupportActionBar().setTitle((tblChildInfo.getChlBabyname() +
                " (" +
                tblChildInfo.getChlGender() + ")"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer1);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);

        aqChildgmList.id(R.id.tvWomanName).text((tblChildInfo.getChlBabyname() +
                " (" +
                tblChildInfo.getChlGender() + ")"));
        if (tblChildInfo.getChlDateTimeOfBirth() != null && tblChildInfo.getChlDateTimeOfBirth().length() > 0)
            aqChildgmList.id(R.id.tvInfo1).text((getResources().getString(R.string.age)
                    + " " + DateTimeUtil.calculateAge(tblChildInfo.getChlDateTimeOfBirth())));
        aqChildgmList.id(R.id.ivWomanImg).gone();

        getMenuInflater().inflate(R.menu.cgmlistmenu, menu);
        return true;
    }

    //    update labels based on data
    private void updateLabels() {
        if (tblchildgrowthList.size() == 0) {
            aqChildgmList.id(R.id.txtcountcgm).text(getResources().getString(R.string.recordcount,
                    tblchildgrowthList.size()));

            aqChildgmList.id(R.id.txtnodata).text(getResources().getString(R.string.no_data));
            aqChildgmList.id(R.id.txtnodata).visible();
            return;
        }
        aqChildgmList.id(R.id.txtnodata).gone();
        aqChildgmList.id(R.id.txtcountcgm).text(getResources().getString(R.string.recordcount,
                tblchildgrowthList.size()));
        aqChildgmList.id(R.id.txtcountcgm).visible();
    }

    private void initiateDrawer() {
        mDrawerLayout = findViewById(R.id.ett_drawer_layout);
        mDrawerList = findViewById(R.id.ett_left_drawer);

        ArrayList<NavDrawerItem> navDrawerItems = new ArrayList<>();
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.editchildinfo), R.drawable.ic_edit_60));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.addsibling), R.drawable.general_examination_baby));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.viewparentdetails), R.drawable.ic_mother));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.sp_imm), R.drawable.immunisation));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.gcmaddnewvisit), R.drawable.registration)); //15MAy2021 Bindu
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.chlhvhistory), R.drawable.prenatalhomevisit)); //8Aug2021 Mani
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.chlhv), R.drawable.ic_homevisit));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.chlhvhistory), R.drawable.prenatalhomevisit));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deactivate), R.drawable.deactivate));

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        mDrawerList.setAdapter(new NavDrawerListAdapter(getApplicationContext(), navDrawerItems));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

// ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer,
                R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu(); // creates call to
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                getSupportActionBar().setDisplayShowTitleEnabled(false);
                getSupportActionBar().setDisplayShowCustomEnabled(false);

                try {
                    if (tblChildInfo != null) {
                        getSupportActionBar().setTitle((tblChildInfo.getChlBabyname() + "\t"));
                    }
                } catch (Exception e) {


                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                    crashlytics.log(e.getMessage());
                }
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                try {
                    invalidateOptionsMenu(); // creates call to
                    getSupportActionBar().setDisplayShowHomeEnabled(false);
                    getSupportActionBar().setDisplayShowTitleEnabled(false);
                    getSupportActionBar().setDisplayShowCustomEnabled(true);
                    getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);

                    aqChildgmList.id(R.id.ivWomanImg).getImageView().setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            mDrawerLayout.closeDrawer(mDrawerList);
                        }
                    });

                    aqChildgmList.id(R.id.tvWomanName).text((tblChildInfo.getChlBabyname() + "(" + tblChildInfo.getChlGender() + ")"));
                    if (tblChildInfo.getChlDateTimeOfBirth().trim().length() > 0) {
                        aqChildgmList.id(R.id.tvInfo1).text(
                                (getResources().getString(R.string.age) + " " +
                                        DateTimeUtil.calculateAge(tblChildInfo.getChlDateTimeOfBirth())));
                    }
                } catch (Exception e) {


                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                    crashlytics.log(e.getMessage());
                }
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    /**
     * This method is to sync draw,erToggle and to replace up caret icon to menu
     * icon in Action Bar
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            case R.id.logout: {
                Intent goToScreen = new Intent(CGMList.this, LoginActivity.class);
                displayAlert(getResources().getString(R.string.m111), goToScreen);
                return true;
            }
            case R.id.home: {
                Intent home = new Intent(CGMList.this, MainMenuActivity.class);
                displayAlert(getResources().getString(R.string.exit), home);
                return true;
            }
            case R.id.wlist: {
                Intent goToScreen = new Intent(CGMList.this, RegisteredWomenActionTabs.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }
            case R.id.cgmadd: {
                if (((tblChildInfo.getChlDeactDate() != null &&
                        tblChildInfo.getChlDeactDate().trim().length() > 0)
                        || user.getIsDeactivated() == 1)) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.userdeactivated), Toast.LENGTH_LONG).show();
                } else {
                    //mani 26July2021 Added deliveryresult condition
                    if (DateTimeUtil.calculateAgeInMonths(tblChildInfo.getChlDateTimeOfBirth()) >= 60 ||tblChildInfo.getChlDeliveryResult()>1)
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.aboveagewarning), Toast.LENGTH_LONG).show();
                    else {
                        Intent goToScreen = new Intent(CGMList.this, CGMNewVisit.class);
                        goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        goToScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                        goToScreen.putExtra("tblChildInfo", tblChildInfo);
                        startActivity(goToScreen);
                    }

                }

                return true;
            }
            case R.id.cgmgraph:
                if (tblchildgrowthList.size() > 0) {
                    Intent goToScreen = new Intent(CGMList.this, BoysGraph.class);
                    goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    goToScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                    goToScreen.putExtra("tblChildInfo", tblChildInfo);
                    startActivity(goToScreen);
                    return true;
                } else {
                    Toast.makeText(getApplicationContext(), "Please Add Atleast One CGM Visit", Toast.LENGTH_SHORT).show();
                }

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    /**
     * Save Confirmation Alert
     *
     * @param goToScreen
     * @param spanText2
     */
    private void displayAlert(final String spanText2, final Intent goToScreen) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        String strMess;
        if (goToScreen != null)
            strMess = getResources().getString(R.string.m121);
        else
            strMess = getResources().getString(R.string.ok);
        alertDialogBuilder.setMessage(spanText2).setCancelable(false).setNegativeButton(
                strMess, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (goToScreen != null) {
                            if (spanText2.contains(getResources().getString(R.string.save))) {
                            } else {
                                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                startActivity(goToScreen);
                                if (spanText2.contains("logout")) {
                                    try {
                                        LoginRepository loginRepository = new LoginRepository(databaseHelper);
                                        loginRepository.updateLogoutTime(appState.loginAuditId, appState.sessionUserId);
                                    } catch (Exception e) {
                                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                                        crashlytics.log(e.getMessage());
                                    }
                                }
                            }
                        } else
                            dialog.cancel();
                    }
                });

        if (goToScreen != null) {
            alertDialogBuilder.setPositiveButton(getResources().getString(R.string.m122),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
        }
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    /**
     * The click listener for ListView in the navigation drawer
     */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            mDrawerLayout.closeDrawer(mDrawerList);
            try {

                switch (position) {

                    case 0:
                        if (tblChildInfo.getChlReg() == 1) {
                            Intent nextScreen = new Intent(CGMList.this, EditRegChildInfoActivity.class);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                            nextScreen.putExtra("tblChildInfo", tblChildInfo);
                            startActivity(nextScreen);
                        } else {
                            Intent nextScreen = new Intent(CGMList.this, EditChildActivity.class);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                            nextScreen.putExtra("tblChildInfo", tblChildInfo);
                            startActivity(nextScreen);
                        }
                        break;

                    case 1: {
                        if (user.getIsDeactivated() == 1
                        )
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.userdeactivated), Toast.LENGTH_LONG).show();
                        else {
                            Intent addSibling = new Intent(CGMList.this, AddSiblingsActivity.class);
                            addSibling.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            addSibling.putExtra("appState", getIntent().getBundleExtra("appState"));
//                            if (tblChildInfo.getChlReg() == 1)
                            String strParentId = tblChildInfo.getChlParentId();//17Jun2021 Arpitha
                            if(strParentId != null && strParentId.trim().length() > 0)//17Jun2021 Arpitha
                                addSibling.putExtra("chlParentId", tblChildInfo.getChlParentId());
                            else
                                addSibling.putExtra("WomanId", tblChildInfo.getWomanId());
                            startActivity(addSibling);
                        }
                        break;
                    }
                    case 2: {
                        if (tblChildInfo.getChlParentId() != null && tblChildInfo.getChlParentId().trim().length() > 0) {
                            Intent viewParent = new Intent(CGMList.this, ViewParentDetails.class);
                            viewParent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            viewParent.putExtra("appState", getIntent().getBundleExtra("appState"));
                            viewParent.putExtra("tblChildInfo", tblChildInfo);
                            startActivity(viewParent);
                        } else {
                            WomanRepository womanRepository = new WomanRepository(databaseHelper);
                            tblregisteredwomen woman = womanRepository.getRegistartionDetails(tblChildInfo.getWomanId(), tblChildInfo.getUserId());
                            appState.selectedWomanId = tblChildInfo.getWomanId();
                            Intent nextScreen = new Intent(CGMList.this, ViewProfileActivity.class);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                            nextScreen.putExtra("woman", woman);
                            startActivity(nextScreen);
//                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.ntapplicable), Toast.LENGTH_LONG).show();
                        }
                        break;
                    }

                    case 3: {
                        if (tblChildInfo.getChlDeliveryResult() <= 1) {
                            Intent nextScreen = new Intent(CGMList.this, ImmunizationListActivity.class);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                            nextScreen.putExtra("tblChildInfo", tblChildInfo);
                            startActivity(nextScreen);
                        } else
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.m053), Toast.LENGTH_LONG).show();
                        break;
                    }

                    case 4: {

                        if (((tblChildInfo.getChlDeactDate() != null && tblChildInfo.getChlDeactDate().trim().length() > 0) || user.getIsDeactivated() == 1)) {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.userdeactivated), Toast.LENGTH_LONG).show();
                        } else {
                            if (DateTimeUtil.calculateAgeInMonths(tblChildInfo.getChlDateTimeOfBirth()) >= 60||tblChildInfo.getChlDeliveryResult()>1)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.aboveagewarning), Toast.LENGTH_LONG).show();
                            else {
                                Intent nextScreen = new Intent(CGMList.this, CGMNewVisit.class);
                                nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                                nextScreen.putExtra("tblChildInfo", tblChildInfo);
                                startActivity(nextScreen);
                            }
                        }
                        break;
                    }
                    case 6: {
                        if (((tblChildInfo.getChlDeactDate() != null && tblChildInfo.getChlDeactDate().trim().length() > 0) || user.getIsDeactivated() == 1)) {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.userdeactivated), Toast.LENGTH_LONG).show();
                        }else {
                            Intent nextScreen = new Intent(CGMList.this, ChildHomeVisit.class);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                            nextScreen.putExtra("tblChildInfo", tblChildInfo);
                            startActivity(nextScreen);
                        }

                        break;
                    }
                    case 7: {
                        Intent nextScreen = new Intent(CGMList.this, ChildHomeVisitHistory.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                        nextScreen.putExtra("tblChildInfo", tblChildInfo);
                        startActivity(nextScreen);
                        break;
                    }

                    case 8: {
                        Intent nextScreen = new Intent(CGMList.this, ChildDeactivateActivity.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                        nextScreen.putExtra("tblChildInfo", tblChildInfo);
                        startActivity(nextScreen);
                        break;
                    }

                    case 5: {
                        Intent nextScreen = new Intent(CGMList.this, ChildHomeVisitHistory.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                        nextScreen.putExtra("tblChildInfo", tblChildInfo);
                        startActivity(nextScreen);

                        break;
                    }
                }

            } catch (Exception e) {


                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
        }
    }


}