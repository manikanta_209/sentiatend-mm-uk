package com.sc.stmansi.childgrowthmonitor;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.legacy.app.ActionBarDrawerToggle;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.misc.TransactionManager;
import com.sc.stmansi.About.AboutActivity;
import com.sc.stmansi.R;
import com.sc.stmansi.childhomevisit.ChildHomeVisit;
import com.sc.stmansi.childhomevisit.ChildHomeVisitHistory;
import com.sc.stmansi.childregistration.AddSiblingsActivity;
import com.sc.stmansi.childregistration.EditChildActivity;
import com.sc.stmansi.childregistration.EditRegChildInfoActivity;
import com.sc.stmansi.childregistration.ViewParentDetails;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.common.NavDrawerItem;
import com.sc.stmansi.common.NavDrawerListAdapter;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.deactivate.ChildDeactivateActivity;
import com.sc.stmansi.delivery.DeliveryInfoActivity;
import com.sc.stmansi.immunization.ImmunizationListActivity;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.registration.RegistrationActivity;
import com.sc.stmansi.repositories.CGMRepository;
import com.sc.stmansi.repositories.FacilityRepository;
import com.sc.stmansi.repositories.LoginRepository;
import com.sc.stmansi.repositories.SMSRepository;
import com.sc.stmansi.repositories.SettingsRepository;
import com.sc.stmansi.repositories.TransactionHeaderRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.sms.SendSMS;
import com.sc.stmansi.tables.MessageLogPojo;
import com.sc.stmansi.tables.TblChildInfo;
import com.sc.stmansi.tables.TblContactDetails;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblchildgrowth;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.tables.tblweightforage;
import com.sc.stmansi.viewprofile.ViewProfileActivity;
import com.sc.stmansi.womanlist.RegisteredWomenActionTabs;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
public class CGMNewVisit extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener, View.OnFocusChangeListener {

    AQuery aqChildGM;
    List<tblchildgrowth> childGowth;
    com.sc.stmansi.tables.tblchildgrowth tblchildgrowth;
    TransactionHeaderRepository transactionHeaderRepository;
    int transId, visitNum = 0, heightUnit = 0, height, heightD;
    String lastHeightValue;
    TblChildInfo tblChildInfo;
    TblInstusers user;
    LinearLayout llAdvise;
    private DatabaseHelper databaseHelper;
    private Double weight;
    private AppState appState;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private static int childagemonths;
    private Map<Integer, tblweightforage> mapChildWeight;
    boolean messageSent;
    boolean isMessageLogsaved;
    static EditText etphn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cgm_newvisit);
        try {
            aqChildGM = new AQuery(this);
            databaseHelper = getHelper();

            //Get data from intent
            Bundle bundle = getIntent().getBundleExtra("globalState");
            tblChildInfo = (TblChildInfo) getIntent().getSerializableExtra("tblChildInfo");
            appState = bundle.getParcelable("appState");

            UserRepository userRepository = new UserRepository(databaseHelper);
            user = userRepository.getOneAuditedUser(appState.ashaId);

            llAdvise = findViewById(R.id.lladvise);
            initiateDrawer();


            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            //To set initial data
            setInitialdata();
            setOnTextChangeListeners();

            //To set click listener
            setOnClickListener();
            aqChildGM.id(R.id.etcgmweight).getEditText().setOnFocusChangeListener(this);
            aqChildGM.id(R.id.etcgmheight).getEditText().setOnFocusChangeListener(this);

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private void setOnClickListener() {
        aqChildGM.id(R.id.btnsave).getImageView().setOnClickListener(this);
        aqChildGM.id(R.id.btncancel).getImageView().setOnClickListener(this);

    }

    private void setOnTextChangeListeners() {
        aqChildGM.id(R.id.etcgmweight).getEditText().addTextChangedListener(textWatcher);
        aqChildGM.id(R.id.etcgmheight).getEditText().addTextChangedListener(textWatcher);
//        aqChildGM.id(R.id.etcgmheadcircum).getEditText().addTextChangedListener(textWatcher); //16May2021 Bindu remove warning
        aqChildGM.id(R.id.etcgmmuac).getEditText().addTextChangedListener(textWatcher); //16May2021 Bindu set muac listener

    }


    //Text watcher
    TextWatcher textWatcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s == aqChildGM.id(R.id.etcgmweight).getEditable()) {

                if (aqChildGM.id(R.id.etcgmweight).getText().toString().length() > 0) {
                    if (DateTimeUtil.calculateAgeInMonths(tblChildInfo.getChlDateTimeOfBirth()) <= 12) {
                        if (Double.parseDouble(aqChildGM.id(R.id.etcgmweight).getText().toString()) < 2.5 || Double.parseDouble(aqChildGM.id(R.id.etcgmweight).getText().toString()) > 9.7) {
                            aqChildGM.id(R.id.etcgmweight).getEditText()
                                    .setError(getResources().getString(R.string.strweight) + aqChildGM.id(R.id.etcgmweight).getText().toString() + getResources().getString(R.string.strkg));
                        }
                    } else if (DateTimeUtil.calculateAgeInMonths(tblChildInfo.getChlDateTimeOfBirth()) > 12 && DateTimeUtil.calculateAgeInMonths(tblChildInfo.getChlDateTimeOfBirth()) <= 24) {
                        if (Double.parseDouble(aqChildGM.id(R.id.etcgmweight).getText().toString()) < 6.9 || Double.parseDouble(aqChildGM.id(R.id.etcgmweight).getText().toString()) > 12.3) {
                            aqChildGM.id(R.id.etcgmweight).getEditText()
                                    .setError(getResources().getString(R.string.strweight) + aqChildGM.id(R.id.etcgmweight).getText().toString() + getResources().getString(R.string.strkg));
                        }
                    } else if (DateTimeUtil.calculateAgeInMonths(tblChildInfo.getChlDateTimeOfBirth()) > 24 && DateTimeUtil.calculateAgeInMonths(tblChildInfo.getChlDateTimeOfBirth()) <= 36) {
                        if (Double.parseDouble(aqChildGM.id(R.id.etcgmweight).getText().toString()) < 8.6 || Double.parseDouble(aqChildGM.id(R.id.etcgmweight).getText().toString()) > 14.4) {
                            aqChildGM.id(R.id.etcgmweight).getEditText()
                                    .setError(getResources().getString(R.string.strweight) + aqChildGM.id(R.id.etcgmweight).getText().toString() + getResources().getString(R.string.strkg));
                        }
                    } else if (DateTimeUtil.calculateAgeInMonths(tblChildInfo.getChlDateTimeOfBirth()) > 36 && DateTimeUtil.calculateAgeInMonths(tblChildInfo.getChlDateTimeOfBirth()) <= 48) {
                        if (Double.parseDouble(aqChildGM.id(R.id.etcgmweight).getText().toString()) < 9.9 || Double.parseDouble(aqChildGM.id(R.id.etcgmweight).getText().toString()) > 16.4) {
                            aqChildGM.id(R.id.etcgmweight).getEditText()
                                    .setError(getResources().getString(R.string.strweight) + aqChildGM.id(R.id.etcgmweight).getText().toString() + getResources().getString(R.string.strkg));
                        }
                    } else if (DateTimeUtil.calculateAgeInMonths(tblChildInfo.getChlDateTimeOfBirth()) > 48 && DateTimeUtil.calculateAgeInMonths(tblChildInfo.getChlDateTimeOfBirth()) <= 60) {
                        if (Double.parseDouble(aqChildGM.id(R.id.etcgmweight).getText().toString()) < 11.1 || Double.parseDouble(aqChildGM.id(R.id.etcgmweight).getText().toString()) > 18.4) {
                            aqChildGM.id(R.id.etcgmweight).getEditText()
                                    .setError(getResources().getString(R.string.strweight) + aqChildGM.id(R.id.etcgmweight).getText().toString() + getResources().getString(R.string.strkg));
                        }
                    }
                    calculateBMI();
//                    12May2021 Bindu - show advise
                    setAdvise();
                    //mani 16June2021
                    setWarningMessage();
                }
                else {
                    aqChildGM.id(R.id.etcgmbmi).text("");
                    aqChildGM.id(R.id.lladvise).gone(); //12May2021 Bindu - hide advise
                    aqChildGM.id(R.id.trcgmweightcompl).gone(); //15May2021 Bindu - hide weight compl
                }


            } else if (s == aqChildGM.id(R.id.etcgmheight).getEditable()) {

                if (aqChildGM.id(R.id.etcgmheight).getText().toString().length() > 0) {
                    if (DateTimeUtil.calculateAgeInMonths(tblChildInfo.getChlDateTimeOfBirth()) <= 12) {
                        if (Double.parseDouble(aqChildGM.id(R.id.etcgmheight).getText().toString()) < 44 || Double.parseDouble(aqChildGM.id(R.id.etcgmheight).getText().toString()) > 75.8) {
                            aqChildGM.id(R.id.etcgmheight).getEditText()
                                    .setError(getResources().getString(R.string.strheight) + aqChildGM.id(R.id.etcgmheight).getText().toString() + getResources().getString(R.string.strcm));
                        }
                    } else if (DateTimeUtil.calculateAgeInMonths(tblChildInfo.getChlDateTimeOfBirth()) > 12 && DateTimeUtil.calculateAgeInMonths(tblChildInfo.getChlDateTimeOfBirth()) <= 24) {
                        if (Double.parseDouble(aqChildGM.id(R.id.etcgmheight).getText().toString()) < 69.4 || Double.parseDouble(aqChildGM.id(R.id.etcgmheight).getText().toString()) > 87.2) {
                            aqChildGM.id(R.id.etcgmheight).getEditText()
                                    .setError(getResources().getString(R.string.strheight) + aqChildGM.id(R.id.etcgmheight).getText().toString() + getResources().getString(R.string.strcm));
                        }
                    } else if (DateTimeUtil.calculateAgeInMonths(tblChildInfo.getChlDateTimeOfBirth()) > 24 && DateTimeUtil.calculateAgeInMonths(tblChildInfo.getChlDateTimeOfBirth()) <= 36) {
                        if (Double.parseDouble(aqChildGM.id(R.id.etcgmheight).getText().toString()) < 78.4 || Double.parseDouble(aqChildGM.id(R.id.etcgmheight).getText().toString()) > 96.2) {
                            aqChildGM.id(R.id.etcgmheight).getEditText()
                                    .setError(getResources().getString(R.string.strheight) + aqChildGM.id(R.id.etcgmheight).getText().toString() + getResources().getString(R.string.strcm));
                        }
                    } else if (DateTimeUtil.calculateAgeInMonths(tblChildInfo.getChlDateTimeOfBirth()) > 36 && DateTimeUtil.calculateAgeInMonths(tblChildInfo.getChlDateTimeOfBirth()) <= 48) {
                        if (Double.parseDouble(aqChildGM.id(R.id.etcgmheight).getText().toString()) < 85.4 || Double.parseDouble(aqChildGM.id(R.id.etcgmheight).getText().toString()) > 103.4) {
                            aqChildGM.id(R.id.etcgmheight).getEditText()
                                    .setError(getResources().getString(R.string.strheight) + aqChildGM.id(R.id.etcgmheight).getText().toString() + getResources().getString(R.string.strcm));
                        }
                    } else if (DateTimeUtil.calculateAgeInMonths(tblChildInfo.getChlDateTimeOfBirth()) > 48 && DateTimeUtil.calculateAgeInMonths(tblChildInfo.getChlDateTimeOfBirth()) <= 60) {
                        if (Double.parseDouble(aqChildGM.id(R.id.etcgmheight).getText().toString()) < 91 || Double.parseDouble(aqChildGM.id(R.id.etcgmheight).getText().toString()) > 110) {
                            aqChildGM.id(R.id.etcgmheight).getEditText()
                                    .setError(getResources().getString(R.string.strheight) + aqChildGM.id(R.id.etcgmheight).getText().toString() + getResources().getString(R.string.strcm));
                        }
                    }
                    calculateBMI();
                } else {
                    aqChildGM.id(R.id.etcgmbmi).text("");
                }

            }
            /*{else if (s == aqChildGM.id(R.id.etcgmheadcircum).getEditable())

                if (aqChildGM.id(R.id.etcgmheadcircum).getText().toString().length() > 0) {
                    if (DateTimeUtil.calculateAgeInMonths(tblChildInfo.getChlDateTimeOfBirth()) <= 12) {
                        if (Double.parseDouble(aqChildGM.id(R.id.etcgmheadcircum).getText().toString()) < 30.7 || Double.parseDouble(aqChildGM.id(R.id.etcgmheadcircum).getText().toString()) > 50) {
                            aqChildGM.id(R.id.etcgmheadcircum).getEditText()
                                    .setError(getResources().getString(R.string.stringheadcf) + aqChildGM.id(R.id.etcgmheadcircum).getText().toString() + getResources().getString(R.string.strcm));
                        }
                    } else if (DateTimeUtil.calculateAgeInMonths(tblChildInfo.getChlDateTimeOfBirth()) > 12 && DateTimeUtil.calculateAgeInMonths(tblChildInfo.getChlDateTimeOfBirth()) <= 24) {
                        if (Double.parseDouble(aqChildGM.id(R.id.etcgmheadcircum).getText().toString()) < 42.5 || Double.parseDouble(aqChildGM.id(R.id.etcgmheadcircum).getText().toString()) > 52.4) {
                            aqChildGM.id(R.id.etcgmheadcircum).getEditText()
                                    .setError(getResources().getString(R.string.stringheadcf) + aqChildGM.id(R.id.etcgmheadcircum).getText().toString() + getResources().getString(R.string.strcm));
                        }
                    } else if (DateTimeUtil.calculateAgeInMonths(tblChildInfo.getChlDateTimeOfBirth()) > 24 && DateTimeUtil.calculateAgeInMonths(tblChildInfo.getChlDateTimeOfBirth()) <= 36) {
                        if (Double.parseDouble(aqChildGM.id(R.id.etcgmheadcircum).getText().toString()) < 44.3 || Double.parseDouble(aqChildGM.id(R.id.etcgmheadcircum).getText().toString()) > 53.8) {
                            aqChildGM.id(R.id.etcgmheadcircum).getEditText()
                                    .setError(getResources().getString(R.string.stringheadcf) + aqChildGM.id(R.id.etcgmheadcircum).getText().toString() + getResources().getString(R.string.strcm));
                        }
                    } else if (DateTimeUtil.calculateAgeInMonths(tblChildInfo.getChlDateTimeOfBirth()) > 36 && DateTimeUtil.calculateAgeInMonths(tblChildInfo.getChlDateTimeOfBirth()) <= 48) {
                        if (Double.parseDouble(aqChildGM.id(R.id.etcgmheadcircum).getText().toString()) < 45.3 || Double.parseDouble(aqChildGM.id(R.id.etcgmheadcircum).getText().toString()) > 54.6) {
                            aqChildGM.id(R.id.etcgmheadcircum).getEditText()
                                    .setError(getResources().getString(R.string.stringheadcf) + aqChildGM.id(R.id.etcgmheadcircum).getText().toString() + getResources().getString(R.string.strcm));
                        }
                    } else if (DateTimeUtil.calculateAgeInMonths(tblChildInfo.getChlDateTimeOfBirth()) > 48 && DateTimeUtil.calculateAgeInMonths(tblChildInfo.getChlDateTimeOfBirth()) <= 60) {
                        if (Double.parseDouble(aqChildGM.id(R.id.etcgmheadcircum).getText().toString()) < 45.9 || Double.parseDouble(aqChildGM.id(R.id.etcgmheadcircum).getText().toString()) > 55.2) {
                            aqChildGM.id(R.id.etcgmheadcircum).getEditText()
                                    .setError(getResources().getString(R.string.stringheadcf) + aqChildGM.id(R.id.etcgmheadcircum).getText().toString() + getResources().getString(R.string.strcm));
                        }
                    }
                }

            }*/ //16May2021 Bindu remove warning

//            16May2021 Bindu for muac
            else if (s == aqChildGM.id(R.id.etcgmmuac).getEditable()) {
                if (aqChildGM.id(R.id.etcgmmuac).getText().toString().length() > 0) {
                    setMUACcompl();
                }else{
                    aqChildGM.id(R.id.trcgmmuaccompl).gone(); //15May2021 Bindu - hide muac compl
                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private void initiateDrawer() {
        mDrawerLayout = findViewById(R.id.ett_drawer_layout);
        mDrawerList = findViewById(R.id.ett_left_drawer);

        ArrayList<NavDrawerItem> navDrawerItems = new ArrayList<>();
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.editchildinfo), R.drawable.ic_edit_60));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.addsibling), R.drawable.general_examination_baby));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.viewparentdetails), R.drawable.ic_mother));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.sp_imm), R.drawable.immunisation));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.gcmregisterlist), R.drawable.child_growth)); // 11May2021 Bindu change icon
//mani10sep2021
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.chlhv), R.drawable.ic_homevisit));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.chlhvhistory), R.drawable.prenatalhomevisit));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deactivate), R.drawable.deactivate));

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        mDrawerList.setAdapter(new NavDrawerListAdapter(getApplicationContext(), navDrawerItems));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

// ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer,
                R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu(); // creates call to
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                getSupportActionBar().setDisplayShowTitleEnabled(false);
                getSupportActionBar().setDisplayShowCustomEnabled(false);

                try {
                    if (tblChildInfo != null) {
                        getSupportActionBar().setTitle((tblChildInfo.getChlBabyname() + "\t"));
                    }
                } catch (Exception e) {


                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                    crashlytics.log(e.getMessage());
                }
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                try {
                    invalidateOptionsMenu(); // creates call to
                    getSupportActionBar().setDisplayShowHomeEnabled(false);
                    getSupportActionBar().setDisplayShowTitleEnabled(false);
                    getSupportActionBar().setDisplayShowCustomEnabled(true);
                    getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);

                    aqChildGM.id(R.id.ivWomanImg).getImageView().setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            mDrawerLayout.closeDrawer(mDrawerList);
                        }
                    });

                    aqChildGM.id(R.id.tvWomanName).text((tblChildInfo.getChlBabyname() + "(" + tblChildInfo.getChlGender() + ")"));
                    if (tblChildInfo.getChlDateTimeOfBirth().trim().length() > 0) {
                        aqChildGM.id(R.id.tvInfo1).text(
                                (getResources().getString(R.string.age) + " " +
                                        DateTimeUtil.calculateAge(tblChildInfo.getChlDateTimeOfBirth())));
                    }
                } catch (Exception e) {


                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                    crashlytics.log(e.getMessage());
                }
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    /**
     * This method is to sync draw,erToggle and to replace up caret icon to menu
     * icon in Action Bar
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        try {
            switch (v.getId()) {

                case R.id.etcgmheight: {
                    calculateBMI();
                    break;
                }
                case R.id.etcgmweight: {
                    calculateBMI();
                    break;
                }
                default:
                    break;
            }
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private void setInitialdata() throws Exception {
        aqChildGM.id(R.id.etcgmvisitdate).getEditText().setText(DateTimeUtil.getTodaysDate());

        CGMRepository cgmRepository = new CGMRepository(databaseHelper);
        visitNum = cgmRepository.getLastChildGMVisit(tblChildInfo.getChildID());

        // set the last height
        lastHeightValue = cgmRepository.getLastChildLastHeight(tblChildInfo.getChildID(), visitNum);
        aqChildGM.id(R.id.etcgmheight).getEditText().setText(lastHeightValue);

        aqChildGM.id(R.id.txtcgmnewvisitheading).text(getResources().getString(R.string.cgmnewvisit) + " - " + " # " + visitNum);

        childagemonths = DateTimeUtil.calculateAgeInMonths(tblChildInfo.getChlDateTimeOfBirth());

//        get weight for age Bindu
        mapChildWeight = cgmRepository.getChildWeightforAgeMasterData();
//        14May2021 Bindu - set enable or disable MUAC based on age
        if(childagemonths < 7) {
            aqChildGM.id(R.id.etcgmmuac).enabled(false);
            aqChildGM.id(R.id.etcgmmuac).enabled(false).background(R.drawable.edittext_disable);
//  20May2021 Bindu display breastfeeding
            aqChildGM.id(R.id.trbreastfeeding).visible();
        }else {
            aqChildGM.id(R.id.etcgmmuac).enabled(true);
            aqChildGM.id(R.id.etcgmmuac).enabled(true).background(R.drawable.edittext_style);
            // 20May2021 Bindu display breastfeeding
            aqChildGM.id(R.id.trbreastfeeding).gone();
        }





//Set the action to be taken
        /*if (age > 24) {
            aqChildGM.id(R.id.tvActionTaken).text(getResources().getString(R.string.action_taken_desc));
        } else {
            rlActionTaken.setVisibility(View.GONE);
        }*/

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnsave: {
                if (validatesfields()) {
                    calculateBMI();
                    saveData();
                }
                break;
            }
            case R.id.btncancel:
                Intent exit = new Intent(CGMNewVisit.this, MainMenuActivity.class);
                displayAlert(getResources().getString(R.string.exit), exit);
                break;

        }
    }

    private void calculateBMI() {
        Double heightInFeet;
        double actHeight, heightInMts = 0;
        if (aqChildGM.id(R.id.etcgmheight).getText().toString().trim().length() > 0
                && Double.parseDouble(aqChildGM.id(R.id.etcgmheight).getText().toString()) > 0) {
            heightInFeet = 0.0328084 * (Double.parseDouble(aqChildGM.id(R.id.etcgmheight).getText().toString()));

            height = Integer.parseInt(heightInFeet.toString().split("\\.")[0]);
            heightD = Integer.parseInt(heightInFeet.toString().split("\\.")[1].substring(0, 1));

        }

        actHeight = Double.parseDouble(height + "." + heightD);
        heightInMts = actHeight * 0.3048;

        weight = (aqChildGM.id(R.id.etcgmweight).getText().toString().length() > 0)
                ? Double.parseDouble(aqChildGM.id(R.id.etcgmweight).getText().toString()) : 0;

        if (heightInMts > 0 && weight > 0) {
            double bmi = weight / (heightInMts * heightInMts);
            aqChildGM.id(R.id.etcgmbmi).text("" + bmi);
        } else
            aqChildGM.id(R.id.etcgmbmi).text("");
    }

    private boolean validatesfields() {

//        20May2021 Bindu add brest feed
        if(childagemonths < 7) {
            if(!(aqChildGM.id(R.id.rdbreastyes).isChecked() || aqChildGM.id(R.id.rdbreastno).isChecked())) {
                displayAlert(getResources().getString(R.string.plsselbreastfeeding), null);
                return false;
            }
        }

        if (aqChildGM.id(R.id.etcgmweight).getText().toString().trim().length() <= 0) {
            displayAlert(getResources().getString(R.string.cgmenterweight), null);
            aqChildGM.id(R.id.etcgmweight).getEditText().requestFocus();
            return false;
        }
        //17May2021 Bindu - set weight > 0
        if (aqChildGM.id(R.id.etcgmweight).getText().toString().trim().length() > 0) {
            if (Double.parseDouble(aqChildGM.id(R.id.etcgmweight).getText().toString()) == 0 || Double.parseDouble(aqChildGM.id(R.id.etcgmweight).getText().toString()) > 25) { //20May2021 Bindu add max weight limit
                displayAlert(getResources().getString(R.string.pleaseentervalidweight), null);
                aqChildGM.id(R.id.etcgmweight).getEditText().requestFocus();
                return false;
            }
        }

        if (aqChildGM.id(R.id.etcgmheight).getText().toString().trim().length() <= 0) {
            displayAlert(getResources().getString(R.string.cgmenterheight), null);
            aqChildGM.id(R.id.etcgmheight).getEditText().requestFocus();
            return false;
        }

        //17May2021 Bindu - set height > 0
        if (aqChildGM.id(R.id.etcgmheight).getText().toString().trim().length() > 0) {
            if (Double.parseDouble(aqChildGM.id(R.id.etcgmheight).getText().toString()) == 0 || Double.parseDouble(aqChildGM.id(R.id.etcgmheight).getText().toString()) > 125) {
                displayAlert(getResources().getString(R.string.pleaseentervalidheight), null);
                aqChildGM.id(R.id.etcgmheight).getEditText().requestFocus();
                return false;
            }
        }

//        if (aqChildGM.id(R.id.etcgmheadcircum).getText().toString().trim().length() <= 0) {
//            displayAlert(getResources().getString(R.string.cgmenterheadcircum), null);
//            aqChildGM.id(R.id.etcgmheadcircum).getEditText().requestFocus();
//            return false;
//        }
//
        /*if (aqChildGM.id(R.id.etcgmmuac).getText().toString().trim().length() <= 0) {
            displayAlert(getResources().getString(R.string.cgmentermuac), null);
            aqChildGM.id(R.id.etcgmmuac).getEditText().requestFocus();
            return false;
        }*/ //11May2021 Bindu remove MUAC mandatory
//
//        if (aqChildGM.id(R.id.etcgmtriceps).getText().toString().trim().length() <= 0) {
//            displayAlert(getResources().getString(R.string.cgmentertricpes), null);
//            aqChildGM.id(R.id.etcgmtriceps).getEditText().requestFocus();
//            return false;
//        }

        return true;

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        return false;
    }

    //    initialize db object
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    /**
     * Save Confirmation Alert
     *
     * @param goToScreen
     * @param spanText2
     */
    private void displayAlert(final String spanText2, final Intent goToScreen) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        String strMess;
        if (goToScreen != null)
            strMess = getResources().getString(R.string.m121);
        else
            strMess = getResources().getString(R.string.ok);
        alertDialogBuilder.setMessage(spanText2).setCancelable(false).setNegativeButton(
                strMess, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (goToScreen != null) {
                            if (spanText2.contains(getResources().getString(R.string.save))) {
                                saveData();
                            } else {
                                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                startActivity(goToScreen);
                                if (spanText2.contains("logout")) {
                                    try {
                                        LoginRepository loginRepository = new LoginRepository(databaseHelper);
                                        loginRepository.updateLogoutTime(appState.loginAuditId, appState.sessionUserId);
                                    } catch (Exception e) {
                                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                                        crashlytics.log(e.getMessage());
                                    }
                                }
                            }
                        } else
                            dialog.cancel();
                    }
                });

        if (goToScreen != null) {
            alertDialogBuilder.setPositiveButton(getResources().getString(R.string.m122),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
        }
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    private void saveData() {
        try {
            setdata();
            writeToDatabase();
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private void writeToDatabase() {
        try {
            TransactionManager.callInTransaction(databaseHelper.getConnectionSource(), new Callable<Object>() {
                @Override
                public Void call() throws Exception {
                    int add;
                    boolean added;
                    RuntimeExceptionDao<tblchildgrowth, Integer> childGrowthDao = databaseHelper.getChildGrowthExceptionDao();
                    add=childGrowthDao.create(tblchildgrowth);
                    if (add>0)
                    {
                        added = transactionHeaderRepository.iNewRecordTransNew(user.getUserId(), transId, childGrowthDao.getTableName(), databaseHelper);

                        if (added){
                            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(CGMNewVisit.this);

                            //  (tblchildgrowth.getChlGMComplication().equalsIgnoreCase("SUW") || tblchildgrowth.getChlGMComplication().equalsIgnoreCase("MUW")))
                            if (checkSimState() == TelephonyManager.SIM_STATE_READY &&
                                    prefs.getBoolean("sms", false) &&
                                    (tblchildgrowth.getChlGMComplication().contains(getResources().getString(R.string.strsevunderweight)) || tblchildgrowth.getChlGMComplication().contains(getResources().getString(R.string.strmodunderweight)) || tblchildgrowth.getChlGMComplication().contains(getResources().getString(R.string.strsevacmal)) || tblchildgrowth.getChlGMComplication().contains(getResources().getString(R.string.modacumal)))) { //21May2021 Bindu check underweight and malnutrition
                                sendSMS(transactionHeaderRepository, databaseHelper);//27Nov2019 Arpitha

                            }else{
                                Intent intent = new Intent(CGMNewVisit.this, CGMList.class);
                                intent.putExtra("appState", getIntent().getBundleExtra("appState"));
                                intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                intent.putExtra("tblChildInfo", tblChildInfo);
                                startActivity(intent);
                            }
                        }
                        else
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.m091), Toast.LENGTH_LONG).show();
                    }

                    return null;
                }
            });
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.m091), Toast.LENGTH_LONG).show();
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private void setdata() throws Exception {

        tblchildgrowth = new tblchildgrowth();

        transactionHeaderRepository = new TransactionHeaderRepository(databaseHelper);
        transId = transactionHeaderRepository.iCreateNewTrans(user.getUserId(), databaseHelper);

        //mani 18July2021
        tblchildgrowth.setUserId(tblChildInfo.getUserId());

        tblchildgrowth.setChlId(tblChildInfo.getChildID());
        tblchildgrowth.setChlGMVisitId(visitNum);
        tblchildgrowth.setChlGMAge(DateTimeUtil.calculateAge(tblChildInfo.getChlDateTimeOfBirth()));
        tblchildgrowth.setChlGMAgeInMonths(DateTimeUtil.calculateAgeInMonths(tblChildInfo.getChlDateTimeOfBirth()));
        tblchildgrowth.setChlGMVisitdate(aqChildGM.id(R.id.etcgmvisitdate).getText().toString());
        tblchildgrowth.setChlGMWeight(aqChildGM.id(R.id.etcgmweight).getText().toString());
        tblchildgrowth.setChlGMHeight(aqChildGM.id(R.id.etcgmheight).getText().toString());
        tblchildgrowth.setChlGMBMI(aqChildGM.id(R.id.etcgmbmi).getText().toString());
        tblchildgrowth.setChlGMHeadCircum(aqChildGM.id(R.id.etcgmheadcircum).getText().toString());
        tblchildgrowth.setChlGMMuac(aqChildGM.id(R.id.etcgmmuac).getText().toString());
        tblchildgrowth.setChlGMTriceps(aqChildGM.id(R.id.etcgmtriceps).getText().toString());
        tblchildgrowth.setChlGMVisitNotes(aqChildGM.id(R.id.etcgmvisitnotes).getText().toString());
        tblchildgrowth.setTransId(transId); //20May2021 Bindu change
//        20May2021 Bindu set Brest feeding
        if(aqChildGM.id(R.id.rdbreastyes).isChecked())
            tblchildgrowth.setChlGMBreastfeeding(getResources().getString(R.string.yesshortform));
        else if(aqChildGM.id(R.id.rdbreastno).isChecked())
            tblchildgrowth.setChlGMBreastfeeding(getResources().getString(R.string.noshortform));
        else
            tblchildgrowth.setChlGMBreastfeeding("");

        tblchildgrowth.setChlGMRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
//        12May2021 Bindu add chlcompl and updated date
        tblchildgrowth.setChlGMRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
        int i = childagemonths;
        double chlwt = Double.parseDouble(aqChildGM.id(R.id.etcgmweight).getText().toString());

//14May2021 Add MUAC risks
        String childvisitRisks = "";
        if(tblChildInfo.getChlGender().equalsIgnoreCase(getResources().getString(R.string.strmale))) {
            double redvalboy = Double.parseDouble(mapChildWeight.get(i).getCgmBoysWtSeconddNegSD());
            double ambvalboy = Double.parseDouble(mapChildWeight.get(i).getCgmBoysWtFirstdNegSD());
            double greenvalboy = Double.parseDouble(mapChildWeight.get(i).getCgmBoysWtMedian());
//            for weight
            if (chlwt <= redvalboy)
                childvisitRisks = getResources().getString(R.string.strsevunderweight);
            else if(chlwt > redvalboy &&  chlwt<= ambvalboy)
                childvisitRisks =getResources().getString(R.string.strmodunderweight);
            else if(chlwt > ambvalboy &&  chlwt<= greenvalboy)
                childvisitRisks =getResources().getString(R.string.strnormalweight);
        }if(tblChildInfo.getChlGender().equalsIgnoreCase(getResources().getString(R.string.strfemale))) {
            double redvalgal = Double.parseDouble(mapChildWeight.get(i).getCgmGirlsWtSeconddNegSD());
            double ambvalgal = Double.parseDouble(mapChildWeight.get(i).getCgmGirlsWtFirstdNegSD());
            double greenvalgal = Double.parseDouble(mapChildWeight.get(i).getCgmGirlsWtMedian());
            if (chlwt <= redvalgal)
                childvisitRisks =getResources().getString(R.string.strsevunderweight);
            else if(chlwt > redvalgal &&  chlwt<= ambvalgal)
                childvisitRisks =getResources().getString(R.string.strmodunderweight);
            else if(chlwt > ambvalgal &&  chlwt<= greenvalgal)
                childvisitRisks =getResources().getString(R.string.strnormalweight);
        }
//        MUAC risks
        if(aqChildGM.id(R.id.etcgmmuac).getText().toString().length()>0) {
            double muacval = Double.parseDouble(aqChildGM.id(R.id.etcgmmuac).getText().toString());
            if(muacval <= 11.4)
                childvisitRisks = childvisitRisks.length() > 0 ? childvisitRisks + "," + getResources().getString(R.string.strsevacmal) : getResources().getString(R.string.strsevacmal);
            else if(muacval > 11.4 && muacval <12.5)
                childvisitRisks = childvisitRisks.length() > 0 ? childvisitRisks + "," + getResources().getString(R.string.strmodacmal) : getResources().getString(R.string.strmodacmal);
            else if(muacval >= 12.5)
                childvisitRisks = childvisitRisks.length() > 0 ? childvisitRisks + "," + getResources().getString(R.string.strnormalacmal) : getResources().getString(R.string.strnormalacmal);
        }

        tblchildgrowth.setChlGMComplication(childvisitRisks);

    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportActionBar().setTitle((tblChildInfo.getChlBabyname() +
                " (" +
                tblChildInfo.getChlGender() + ")"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer1);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);

        aqChildGM.id(R.id.tvWomanName).text((tblChildInfo.getChlBabyname() +
                " (" +
                tblChildInfo.getChlGender() + ")"));
        if (tblChildInfo.getChlDateTimeOfBirth() != null && tblChildInfo.getChlDateTimeOfBirth().length() > 0)
            aqChildGM.id(R.id.tvInfo1).text((getResources().getString(R.string.age)
                    + " " + DateTimeUtil.calculateAge(tblChildInfo.getChlDateTimeOfBirth())));

        aqChildGM.id(R.id.ivWomanImg).gone();
        getMenuInflater().inflate(R.menu.servicesmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            case R.id.logout: {
                Intent goToScreen = new Intent(CGMNewVisit.this, LoginActivity.class);
                displayAlert(getResources().getString(R.string.m111), goToScreen);
                return true;
            }
            case R.id.home: {
                Intent home = new Intent(CGMNewVisit.this, MainMenuActivity.class);
                displayAlert(getResources().getString(R.string.exit), home);
                return true;
            }
            case R.id.wlist: {
                Intent goToScreen = new Intent(CGMNewVisit.this, RegisteredWomenActionTabs.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }
            case R.id.about: {
                Intent goToScreen = new Intent(CGMNewVisit.this, AboutActivity.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    /**
     * The click listener for ListView in the navigation drawer
     */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            mDrawerLayout.closeDrawer(mDrawerList);
            try {

                switch (position) {

                    case 0:
                        if (tblChildInfo.getChlReg() == 1) {
                            Intent nextScreen = new Intent(CGMNewVisit.this, EditRegChildInfoActivity.class);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                            nextScreen.putExtra("tblChildInfo", tblChildInfo);
                            startActivity(nextScreen);
                        } else {
                            Intent nextScreen = new Intent(CGMNewVisit.this, EditChildActivity.class);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                            nextScreen.putExtra("tblChildInfo", tblChildInfo);
                            startActivity(nextScreen);
                        }
                        break;

                    case 1: {
                        if (user.getIsDeactivated() == 1
                        )
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.userdeactivated), Toast.LENGTH_LONG).show();
                        else {
                            Intent addSibling = new Intent(CGMNewVisit.this, AddSiblingsActivity.class);
                            addSibling.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            addSibling.putExtra("appState", getIntent().getBundleExtra("appState"));
                            if (tblChildInfo.getChlReg() == 1)
                                addSibling.putExtra("chlParentId", tblChildInfo.getChlParentId());
                            else
                                addSibling.putExtra("WomanId", tblChildInfo.getWomanId());
                            startActivity(addSibling);
                        }
                        break;
                    }
                    case 2: {
                        if (tblChildInfo.getChlParentId() != null && tblChildInfo.getChlParentId().trim().length() > 0) {
                            Intent viewParent = new Intent(CGMNewVisit.this, ViewParentDetails.class);
                            viewParent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            viewParent.putExtra("appState", getIntent().getBundleExtra("appState"));
                            viewParent.putExtra("tblChildInfo", tblChildInfo);
                            startActivity(viewParent);
                        } else {
                            WomanRepository womanRepository = new WomanRepository(databaseHelper);
                            tblregisteredwomen woman = womanRepository.getRegistartionDetails(tblChildInfo.getWomanId(), tblChildInfo.getUserId());
                            appState.selectedWomanId = tblChildInfo.getWomanId();
                            Intent nextScreen = new Intent(CGMNewVisit.this, ViewProfileActivity.class);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                            nextScreen.putExtra("woman", woman);
                            startActivity(nextScreen);
//                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.ntapplicable), Toast.LENGTH_LONG).show();
                        }
                        break;
                    }

                    case 3: {
                        if (tblChildInfo.getChlDeliveryResult() <= 1) {
                            Intent nextScreen = new Intent(CGMNewVisit.this, ImmunizationListActivity.class);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                            nextScreen.putExtra("tblChildInfo", tblChildInfo);
                            startActivity(nextScreen);
                        } else
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.m053), Toast.LENGTH_LONG).show();
                        break;
                    }
                    case 4: {
                        Intent nextScreen = new Intent(CGMNewVisit.this, CGMList.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                        nextScreen.putExtra("tblChildInfo", tblChildInfo);
                        startActivity(nextScreen);

                        break;
                    }


                    case 5: {
                        if (((tblChildInfo.getChlDeactDate() != null && tblChildInfo.getChlDeactDate().trim().length() > 0) || user.getIsDeactivated() == 1)) {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.userdeactivated), Toast.LENGTH_LONG).show();
                        }else {
                            Intent nextScreen = new Intent(CGMNewVisit.this, ChildHomeVisit.class);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                            nextScreen.putExtra("tblChildInfo", tblChildInfo);
                            startActivity(nextScreen);
                        }

                        break;
                    }
                    case 6: {
                        Intent nextScreen = new Intent(CGMNewVisit.this, ChildHomeVisitHistory.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                        nextScreen.putExtra("tblChildInfo", tblChildInfo);
                        startActivity(nextScreen);
                        break;
                    }

                    case 7: {
                        Intent nextScreen = new Intent(CGMNewVisit.this, ChildDeactivateActivity.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                        nextScreen.putExtra("tblChildInfo", tblChildInfo);
                        startActivity(nextScreen);

                        break;
                    }
                }

            } catch (Exception e) {


                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
        }
    }

    //    16May2021 Bindu set MUAC compl
    private void setMUACcompl() {
        // 15May2021       MUAC risks
        aqChildGM.id(R.id.trcgmmuaccompl).gone();
        aqChildGM.id(R.id.cgmtxtmuaccomplval).text("");
        if(aqChildGM.id(R.id.etcgmmuac).getText().toString().length()>0) {
            double muacval = Double.parseDouble(aqChildGM.id(R.id.etcgmmuac).getText().toString());
            if(muacval <= 11.4) {
                aqChildGM.id(R.id.trcgmmuaccompl).visible();
                aqChildGM.id(R.id.cgmtxtmuaccomplval).text(getResources().getString(R.string.sevacumal));
                aqChildGM.id(R.id.cgmtxtmuaccomplval).textColor(getResources().getColor(R.color.red));
            }
            else if(muacval > 11.4 && muacval <12.5){
                aqChildGM.id(R.id.trcgmmuaccompl).visible();
                aqChildGM.id(R.id.cgmtxtmuaccomplval).text(getResources().getString(R.string.modacumal));
                aqChildGM.id(R.id.cgmtxtmuaccomplval).textColor(getResources().getColor(R.color.darkorange));
            }
            else if(muacval >= 12.5){ // change to equals
                aqChildGM.id(R.id.trcgmmuaccompl).visible();
                aqChildGM.id(R.id.cgmtxtmuaccomplval).text(getResources().getString(R.string.normalacumal));
                aqChildGM.id(R.id.cgmtxtmuaccomplval).textColor(getResources().getColor(R.color.green));
            }
        }
    }


    //    12May2021 Bindu set advice
    private void setAdvise() {
        if (aqChildGM.id(R.id.etcgmweight).getText().toString().length() > 0) {
            int i = childagemonths;
            double chlwt = Double.parseDouble(aqChildGM.id(R.id.etcgmweight).getText().toString());
            aqChildGM.id(R.id.lladvise).gone();
            aqChildGM.id(R.id.tvadvice2).gone();
            aqChildGM.id(R.id.tvadvice3).gone();
            aqChildGM.id(R.id.tvadvice4).gone();

            if (tblChildInfo.getChlGender().equalsIgnoreCase(getResources().getString(R.string.strmale))) {
                double redvalboy = Double.parseDouble(mapChildWeight.get(i).getCgmBoysWtSeconddNegSD());
                double ambvalboy = Double.parseDouble(mapChildWeight.get(i).getCgmBoysWtFirstdNegSD());
                double greenvalboy = Double.parseDouble(mapChildWeight.get(i).getCgmBoysWtMedian());
                if (chlwt <= redvalboy && childagemonths < 37) {// for 0-3 years sev underweight
                    aqChildGM.id(R.id.tvadvice1).text(getResources().getString(R.string.cgm_adv_sev_underwt_0to3y_1));
                    aqChildGM.id(R.id.lladvise).visible();
                    aqChildGM.id(R.id.tvadvice2).visible();
                    aqChildGM.id(R.id.tvadvice3).visible();
                    aqChildGM.id(R.id.tvadvice4).visible();

                    //removed code : mani 2021 June 16

                } else if (chlwt > redvalboy && chlwt <= ambvalboy) {  //for mod weight
                    if (childagemonths > 0 && childagemonths < 7) {// 1 to 6months
                        aqChildGM.id(R.id.lladvise).visible();
                        aqChildGM.id(R.id.tvadvice1).text(getResources().getString(R.string.cgm_adv_mod_underwt_0to6m));
                        aqChildGM.id(R.id.tvadvice2).gone();
                        aqChildGM.id(R.id.tvadvice3).gone();
                        aqChildGM.id(R.id.tvadvice4).gone();

                    } else if (childagemonths > 6 && childagemonths < 37) {// 7m to 3years
                        aqChildGM.id(R.id.lladvise).visible();
                        aqChildGM.id(R.id.tvadvice1).text(getResources().getString(R.string.cgm_adv_mod_underwt_7mto3y));
                        aqChildGM.id(R.id.tvadvice2).gone();
                        aqChildGM.id(R.id.tvadvice3).gone();
                        aqChildGM.id(R.id.tvadvice4).gone();

                    }
                    //removed code : mani 2021 June 16

                } else if (chlwt > ambvalboy && chlwt <= greenvalboy && childagemonths < 37) {
                    aqChildGM.id(R.id.lladvise).visible();
                    aqChildGM.id(R.id.tvadvice1).text(getResources().getString(R.string.cgm_adv_norwt));

                    //removed code : mani 2021 June 16
                }
            }
            if (tblChildInfo.getChlGender().equalsIgnoreCase(getResources().getString(R.string.strfemale))) {
                double redvalgal = Double.parseDouble(mapChildWeight.get(i).getCgmGirlsWtSeconddNegSD());
                double ambvalgal = Double.parseDouble(mapChildWeight.get(i).getCgmGirlsWtFirstdNegSD());
                double greenvalgal = Double.parseDouble(mapChildWeight.get(i).getCgmGirlsWtMedian());
                if (chlwt <= redvalgal && childagemonths < 37) {// for 0-3 years sev underweight
                    aqChildGM.id(R.id.lladvise).visible();
                    aqChildGM.id(R.id.tvadvice1).text(getResources().getString(R.string.cgm_adv_sev_underwt_0to3y_1));
                    aqChildGM.id(R.id.tvadvice2).visible();
                    aqChildGM.id(R.id.tvadvice3).visible();
                    aqChildGM.id(R.id.tvadvice4).visible();

                    //removed code :mani 2021 June 16

                } else if (chlwt > redvalgal && chlwt <= ambvalgal) {  //for mod weight
                    if (childagemonths > 0 && childagemonths < 7) {// 1 to 6months
                        aqChildGM.id(R.id.lladvise).visible();
                        aqChildGM.id(R.id.tvadvice1).text(getResources().getString(R.string.cgm_adv_mod_underwt_0to6m));
                        aqChildGM.id(R.id.tvadvice2).gone();
                        aqChildGM.id(R.id.tvadvice3).gone();
                        aqChildGM.id(R.id.tvadvice4).gone();
                    } else if (childagemonths > 6 && childagemonths < 37) { // 7m to 3years
                        aqChildGM.id(R.id.lladvise).visible();
                        aqChildGM.id(R.id.tvadvice1).text(getResources().getString(R.string.cgm_adv_mod_underwt_7mto3y));
                        aqChildGM.id(R.id.tvadvice2).gone();
                        aqChildGM.id(R.id.tvadvice3).gone();
                        aqChildGM.id(R.id.tvadvice4).gone();
                    }
                    //removed code : mani 2021 June 16

                } else if (chlwt > ambvalgal && chlwt <= greenvalgal && childagemonths < 37) {
                    aqChildGM.id(R.id.lladvise).visible();
                    aqChildGM.id(R.id.tvadvice1).text(getResources().getString(R.string.cgm_adv_norwt));
                    aqChildGM.id(R.id.tvadvice2).gone();
                    aqChildGM.id(R.id.tvadvice3).gone();
                    aqChildGM.id(R.id.tvadvice4).gone();

                    //removed code : mani 2021 June 16
                }
            }
        } else
            aqChildGM.id(R.id.lladvise).gone();
    }

    //   mani 16June2015
    private void setWarningMessage() {
        if (aqChildGM.id(R.id.etcgmweight).getText().toString().length() > 0) {
            int i = childagemonths;
            double chlwt = Double.parseDouble(aqChildGM.id(R.id.etcgmweight).getText().toString());
            // 15May2021 Bindu hide and show compl weight and muac
            aqChildGM.id(R.id.trcgmweightcompl).gone();
            if (tblChildInfo.getChlGender().equalsIgnoreCase(getResources().getString(R.string.strmale))) {
                double redvalboy = Double.parseDouble(mapChildWeight.get(i).getCgmBoysWtSeconddNegSD());
                double ambvalboy = Double.parseDouble(mapChildWeight.get(i).getCgmBoysWtFirstdNegSD());
                double greenvalboy = Double.parseDouble(mapChildWeight.get(i).getCgmBoysWtMedian());
                if (chlwt <= redvalboy && childagemonths < 60) {// for 0-6 years sev underweight
                    aqChildGM.id(R.id.trcgmweightcompl).visible();
                    aqChildGM.id(R.id.cgmtxtweightcomplval).text(getResources().getString(R.string.sevunderweight));
                    aqChildGM.id(R.id.cgmtxtweightcomplval).textColor(getResources().getColor(R.color.red));

                } else if (chlwt > redvalboy && chlwt <= ambvalboy && childagemonths < 60) {  //for mod weight
                    aqChildGM.id(R.id.trcgmweightcompl).visible();
                    aqChildGM.id(R.id.cgmtxtweightcomplval).text(getResources().getString(R.string.modunderweight));
                    aqChildGM.id(R.id.cgmtxtweightcomplval).textColor(getResources().getColor(R.color.darkorange));

                } else if (chlwt > ambvalboy && chlwt <= greenvalboy && childagemonths < 60) { //For Normal weight
                    aqChildGM.id(R.id.trcgmweightcompl).visible();
                    aqChildGM.id(R.id.cgmtxtweightcomplval).text(getResources().getString(R.string.normalweight));
                    aqChildGM.id(R.id.cgmtxtweightcomplval).textColor(getResources().getColor(R.color.green));
                }
            }
            if (tblChildInfo.getChlGender().equalsIgnoreCase(getResources().getString(R.string.strfemale))) {
                double redvalgal = Double.parseDouble(mapChildWeight.get(i).getCgmGirlsWtSeconddNegSD());
                double ambvalgal = Double.parseDouble(mapChildWeight.get(i).getCgmGirlsWtFirstdNegSD());
                double greenvalgal = Double.parseDouble(mapChildWeight.get(i).getCgmGirlsWtMedian());
                if (chlwt <= redvalgal && childagemonths < 60) {// for 0-6 years sev underweight
                    aqChildGM.id(R.id.trcgmweightcompl).visible();
                    aqChildGM.id(R.id.cgmtxtweightcomplval).text(getResources().getString(R.string.sevunderweight));
                    aqChildGM.id(R.id.cgmtxtweightcomplval).textColor(getResources().getColor(R.color.red));

                } else if (chlwt > redvalgal && chlwt <= ambvalgal && childagemonths < 60) {  //for mod weight
                    aqChildGM.id(R.id.trcgmweightcompl).visible();
                    aqChildGM.id(R.id.cgmtxtweightcomplval).text(getResources().getString(R.string.modunderweight));
                    aqChildGM.id(R.id.cgmtxtweightcomplval).textColor(getResources().getColor(R.color.darkorange));

                } else if (chlwt > ambvalgal && chlwt <= greenvalgal && childagemonths < 60) {
                    aqChildGM.id(R.id.trcgmweightcompl).visible();
                    aqChildGM.id(R.id.cgmtxtweightcomplval).text(getResources().getString(R.string.normalweight));
                    aqChildGM.id(R.id.cgmtxtweightcomplval).textColor(getResources().getColor(R.color.green));
                }
            }
        }
    }

    private void sendSMS(TransactionHeaderRepository transRepo, DatabaseHelper databaseHelper) {
        try {

            String p_no = "";
            List<TblContactDetails> values;

            SettingsRepository settingsRepository = new SettingsRepository(databaseHelper);

            values = settingsRepository.getSpecificPhoneNumber("CGM");
            p_no = "";
            for (int i = 0; i < values.size(); i++) {

                String s = values.get(i).getContactNumber();
                p_no = p_no + values.get(i).getContactNumber() + ",";
            }

            String hrp = "";
            String uniqueId = "", chlId= "", chlparentId="", chlcomp ="";
            String smsContent = "";
            if (tblChildInfo.getChildID()!=null && tblChildInfo.getChildID().trim().length() > 0)
            {
                chlId=", C.ID:"+tblChildInfo.getChildID();
            }if (tblChildInfo.getChildRCHID()!=null && tblChildInfo.getChildRCHID().trim().length() > 0)
            {
                uniqueId=", C.MCP No:"+tblChildInfo.getChildRCHID();
            }
            if(tblChildInfo.getWomanId()!=null && tblChildInfo.getWomanId().trim().length() > 0)
            {
                chlparentId=", PID:"+tblChildInfo.getWomanId();
            }else{
                chlparentId=", PID:"+tblChildInfo.getChlParentId();
            }

            chlcomp = ", Compl - " + tblchildgrowth.getChlGMComplication(); //23May2021 Bindu add comma

            FacilityRepository facilityRepo = new FacilityRepository(databaseHelper);
            String hamletname = facilityRepo.getHamletName(tblChildInfo.getChlTribalHamlet());

// 21May2021 Bindu - CGM SMS
            smsContent = "Sentiatend:" + "Ch: "+ tblChildInfo.getChlBabyname() +uniqueId + chlId + chlparentId +", V# "+tblchildgrowth.getChlGMVisitId() +", V.Dt:"+
                    tblchildgrowth.getChlGMVisitdate() +chlcomp +",Wt:"+ tblchildgrowth.getChlGMWeight()+"Kg"+",Age:"+tblchildgrowth.getChlGMAge()  + ", " + hamletname  + ", "+ appState.userType + "- " +user.getUserName()
                    + " ,"+ user.getUserMobileNumber() + "- (CGM)";

            /*smsContent = "Sentiatend:" + tblChildInfo.getChlUserType() +", PID- "+ tblChildInfo.getChlParentId() + ","+ uniqueId +",Ch: "+ tblChildInfo.getChlBabyname() +", V# "+tblchildgrowth.getChlGMVisitId() +", V.Dt:"+
                    tblchildgrowth.getChlGMVisitdate() +",Wt:"+ tblchildgrowth.getChlGMWeight()+"Kg"+",Age:"+tblchildgrowth.getChlGMAge();*/


            if (p_no.length() <= 0) {
                display_messagedialog(transRepo, databaseHelper,smsContent);
            } else
                sending(transRepo, databaseHelper, p_no,smsContent);
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }


    void sending(TransactionHeaderRepository transRepo, DatabaseHelper databaseHelper,
                 String phnNo,String smsCont) throws Exception {
        String smsContent = smsCont;



        InserttblMessageLog(phnNo,
                smsContent, user.getUserId(), transRepo, databaseHelper);
        if (isMessageLogsaved) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.sending_sms),
                    Toast.LENGTH_LONG).show();


            if (isMessageLogsaved) {
                Intent intent = new Intent(CGMNewVisit.this, CGMList.class);
                intent.putExtra("appState", getIntent().getBundleExtra("appState"));
                intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                intent.putExtra("tblChildInfo", tblChildInfo);
                startActivity(intent);
            } else
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.m091), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha

            sendSMSFunction();
        } else
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.m091), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha

    }


    private void sendSMSFunction() throws Exception {
        messageSent = true;
        new SendSMS(this).checkAndSendMsg(databaseHelper);

    }


    public void InserttblMessageLog(String phoneno, String content, String user_id,
                                    TransactionHeaderRepository transactionHeaderRepository, DatabaseHelper databaseHelper) throws Exception {
        final ArrayList<MessageLogPojo> mlpArr = new ArrayList<MessageLogPojo>();

        if (phoneno.length() > 0) {
            String[] phn = phoneno.split("\\,");
            for (int i = 0; i < phn.length; i++) {
                String num = phn[i];
                if (num != null && num.length() > 0) {
                    MessageLogPojo mlp =
                            new MessageLogPojo();
                    mlp.setUserId(user_id);
                    mlp.setMsgPhoneNo(num);
                    mlp.setMsgBody(content);
                    mlp.setWomanId(tblChildInfo.getChildID());
                    mlp.setMsgPriority(1);
                    mlp.setMsgNoOfFailures(0);
                    mlp.setTransId(transId);
                    mlp.setMsgSentDate(DateTimeUtil.getTodaysDate());
                    mlp.setMsgSent(0);
                    mlp.setMsgStage("CGM");
                    mlp.setMsgUserType(appState.userType);
                    mlp.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());

                    mlp.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());//21May2021 Arpitha
                    mlpArr.add(mlp);
                }
            }
        }

        int addMessage = 0;
        SMSRepository smsRepository = new SMSRepository(databaseHelper);
        if (mlpArr != null) {
            for (final MessageLogPojo mlpp : mlpArr) {

                addMessage = smsRepository.insertIntoMessageLog(mlpp, databaseHelper);
            }

            if (addMessage > 0) {
                isMessageLogsaved = true;
                boolean addRegTrans = transactionHeaderRepository.iNewRecordTransNew(user.getUserId(), transId,
                        "tblmessagelog", databaseHelper);
            }



        }
    }

    public void display_messagedialog(final TransactionHeaderRepository
                                              transactionHeaderRepository,
                                      final DatabaseHelper databaseHelper,String smsContent) throws Exception {
        try {
            final Dialog sms_dialog = new Dialog(this);
//            sms_dialog.setTitle(getResources().getString(R.string.plsentervalidphoneno));

            sms_dialog.setContentView(R.layout.sms_dialog);


            sms_dialog.show();

            ImageButton imgsend = (ImageButton) sms_dialog.findViewById(R.id.imgsend);
            ImageButton imgcancel = (ImageButton) sms_dialog.findViewById(R.id.imgcancel);
            etphn = (EditText) sms_dialog.findViewById(R.id.etphno);
            final EditText etmess = (EditText) sms_dialog.findViewById(R.id.etmess);
            TextView txtcontcats = (TextView) sms_dialog.findViewById(R.id.txtcontacts);


//            etmess.setVisibility(View.GONE);
            etmess.setEnabled(false);
            etmess.setText(smsContent);
            txtcontcats.setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    // TODO Auto-generated method stub

                    Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
//					context.startActivity(intent);
                    startActivity(intent);
                    return true;
                }
            });


            //  etphn.setText(p_no);

            imgsend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {

                        sending(transactionHeaderRepository, databaseHelper,
                                etphn.getText().toString(),smsContent);

                    } catch (Exception e) {

                        e.printStackTrace();
                    }

                }
            });


            imgcancel.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    sms_dialog.cancel();
//                    23May2021 Arpitha
                    Intent intent = new Intent(CGMNewVisit.this, CGMList.class);
                    intent.putExtra("appState", getIntent().getBundleExtra("appState"));
                    intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    intent.putExtra("tblChildInfo", tblChildInfo);
                    startActivity(intent);
                }
            });
            sms_dialog.setCancelable(false);
        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    public  int checkSimState() {
        int simState = -1;
        // This is to know the status of the Sim
        TelephonyManager telMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        simState = telMgr.getSimState();

        return simState;
    }
}