package com.sc.stmansi.mainmenu;

public interface NetworkNotificationListener {

    void update(boolean connected);
}
