//27Aug2019 Arpitha - code clean up
package com.sc.stmansi.mainmenu;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import com.sc.stmansi.R;

public class CustomGridViewAdapter extends ArrayAdapter<Item> {
	Context context;
	int layoutResourceId;
	ArrayList<Item> gridMenuData;

	public CustomGridViewAdapter(Context context, int layoutResourceId,
								 ArrayList<Item> data) {
		super(context, layoutResourceId, data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.gridMenuData = data;
	}

	public int getCount() {
		return gridMenuData.size();
	}


	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		RecordHolder holder;

		if (row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);

			holder = new RecordHolder();
			holder.txtTitle = row.findViewById(R.id.item_text);
			holder.imageItem = row.findViewById(R.id.item_image);
			holder.txtBadge = row.findViewById(R.id.txtbadge);
			row.setTag(holder);
		} else {
			holder = (RecordHolder) row.getTag();
		}

		Item item = gridMenuData.get(position);
		holder.txtTitle.setText(item.getTitle());
		holder.imageItem.setImageBitmap(item.getImage());
		if (item.getBadge() != null && item.getBadge() > 0) {

			holder.txtBadge.setText("" + item.getBadge());
		} else {
			holder.txtBadge.setVisibility(View.GONE);
		}
		return row;
	}

	static class RecordHolder {
		TextView txtTitle;
		ImageView imageItem;
		TextView txtBadge;
	}
}