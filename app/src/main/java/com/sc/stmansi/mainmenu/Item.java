//27Aug2019 Arpitha
package com.sc.stmansi.mainmenu;

import android.graphics.Bitmap;

public class Item {
	Bitmap image;
	String title;
	Integer badge;

//	constructor
	public Item(Bitmap image, String spannableStringBuilder, Integer badge) {
		super();
		this.image = image;
		this.title = spannableStringBuilder;
		this.badge = badge;
	}
	public Integer getBadge() {
		return badge;
	}
	public Bitmap getImage() {
		return image;
	}
	public String getTitle() {
		return title;
	}


}
