package com.sc.stmansi.mainmenu;

import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;

import androidx.annotation.NonNull;

import java.util.function.Consumer;

public class MyNetworkCallback extends ConnectivityManager.NetworkCallback {

    private NetworkNotificationListener c;

    public MyNetworkCallback(NetworkNotificationListener c) {
        this.c = c;
    }

    @Override
    public void onAvailable(@NonNull Network network) {
        super.onAvailable(network);
        c.update(true);
    }

    @Override
    public void onLost(@NonNull Network network) {
        super.onLost(network);
        c.update(false);
    }

    @Override
    public void onUnavailable() {
        super.onUnavailable();
        c.update(false);
    }

    @Override
    public void onCapabilitiesChanged(@NonNull Network network, @NonNull NetworkCapabilities networkCapabilities) {
        super.onCapabilitiesChanged(network, networkCapabilities);
    }
}
