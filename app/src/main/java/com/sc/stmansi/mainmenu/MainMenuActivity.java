package com.sc.stmansi.mainmenu;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.misc.TransactionManager;
import com.sc.stmansi.About.AboutActivity;
import com.sc.stmansi.R;
import com.sc.stmansi.Settings.SettingsActivity;
import com.sc.stmansi.adolescent.AdolRegistration;
import com.sc.stmansi.adolescent.AdolescentHome;
import com.sc.stmansi.childregistration.ChildRegistrationActivity;
import com.sc.stmansi.childregistration.RegisteredChildList;
import com.sc.stmansi.childtoadol.ChildtoAdol_List;
import com.sc.stmansi.common.ActivitiesStack;
import com.sc.stmansi.common.CommonDatePickerFragment;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.common.InternetConnectionUtil;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.configuration.MyApplication;
import com.sc.stmansi.configuration.SyncState;
import com.sc.stmansi.database.DBMethods;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.followupCaseMgmt.FollowUpTabsActivity;
import com.sc.stmansi.followupCaseMgmt.MyPeriodicWork;
import com.sc.stmansi.immunization.ChildImmunizationActivity;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.pendingservices.PendingUpcomingServiceActivity;
import com.sc.stmansi.registration.RegistrationActivity;
import com.sc.stmansi.repositories.AuditRepository;
import com.sc.stmansi.repositories.BadgeRepository;
import com.sc.stmansi.repositories.CaseMgmtRepository;
import com.sc.stmansi.repositories.ChildRepository;
import com.sc.stmansi.repositories.ComplicationMgmtRepository;
import com.sc.stmansi.repositories.ImageStoreRepository;
import com.sc.stmansi.repositories.InstitutionRepository;
import com.sc.stmansi.repositories.LoginRepository;
import com.sc.stmansi.repositories.SMSRepository;
import com.sc.stmansi.repositories.SettingsRepository;
import com.sc.stmansi.repositories.TransactionHeaderRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.sms.SMSLog;
import com.sc.stmansi.sms.SendSMS;
import com.sc.stmansi.summary.SummaryTabs;
import com.sc.stmansi.sync.DSSyncFunction;
import com.sc.stmansi.sync.EncDecPassword;
import com.sc.stmansi.sync.SyncFunctions;
import com.sc.stmansi.sync.WebService;
import com.sc.stmansi.tables.AuditPojo;
import com.sc.stmansi.tables.MessageLogPojo;
import com.sc.stmansi.tables.TblChildInfo;
import com.sc.stmansi.tables.TblContactDetails;
import com.sc.stmansi.tables.TblInstitutionDetails;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblAdolReg;
import com.sc.stmansi.tables.tblImageStore;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.womanlist.ANCWomanListActivity;
import com.sc.stmansi.womanlist.PNCWomanListActivity;
import com.sc.stmansi.womanlist.RegisteredWomenActionTabs;
import com.sc.stmansi.womanlist.WomanListActivity;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.sc.stmansi.configuration.MyApplication.getAppContext;

public class MainMenuActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    private TblInstusers user;
    private AQuery aqPMSel;
    RadioGroup radGrpEDDorLmp;
    private int noOfDays;
    private boolean isRegDate;
    private tblregisteredwomen woman;
    private Activity activity;
    private GridView gridView1;
    private CustomGridViewAdapter customGridAdapter1;
    private final ArrayList<Item> gridArray = new ArrayList<Item>();
    private HashMap<String, Integer> badgeCountMap;
    private String screen;
    private final Map<String, Integer> menuItemAndMenuIdMap = new HashMap<String, Integer>();
    private DatabaseHelper databaseHelper;
    private DBMethods dbMethods;
    public static AppState appState;
    private SyncState syncState;
    Boolean wantToCloseDialog = false;
    //	19Nov2019 Arpitha
    Button btnchangepass;
    EditText etoldpass, etretypepass, etnewpass;
    TextView txtdialogtitle;
    boolean isErrorFound = false;

    //23Nov2019 - Bindu
    public static Typeface font = null;
    CountDownTimer dangerSignTimer = null;

    public static final int START_AFTER_SECONDS = 60000; // This is 60 seconds
    //26Nov2019 Arpitha
    static EditText etphn;
    boolean isMessageLogsaved;
    Dialog dialogChangePassword;
    private String hrpcomplmgmtdatelastupdated = "";

    private AsyncCallComplMgmt asyncCallComplMgmt;

    Thread myThread;
    //10Dec2019 Bindu
    private WeakReference<MainMenuActivity> mainMenuActivityWeakReference;
    SharedPreferences.Editor shardPrf;//07Jun2021 Arpitha

    SyncFunctions syncFunctions; //Gururaja 07Jun2021
    WebService webService; //Gururaja 07Jun2021
    private final ArrayList<String> riskDetails = new ArrayList<>();
    private int childtoAdolCount = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainmenu);
        databaseHelper = getHelper();
        Bundle bundle = getIntent().getBundleExtra("globalState");
        appState = bundle.getParcelable("appState");
        MyApplication myApp = (MyApplication) getAppContext();
        syncState = myApp.syncState;
        dbMethods = new DBMethods(MainMenuActivity.this, user, appState, syncState);

        try {
            UserRepository userRepository = new UserRepository(databaseHelper);
            user = userRepository.getOneAuditedUser(appState.ashaId);
            dbMethods.setUser(user); // Required for Sync

            webService = new WebService(dbMethods, user, syncState, appState); //07Jun2021 Gururaja
            syncFunctions = new SyncFunctions(
                    getAndroidId(),
                    dbMethods,
                    syncState,
                    webService); //Gururaja 07Jun2021
        } catch (SQLException e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        activity = MainMenuActivity.this;
        gridView1 = activity.findViewById(R.id.gridView1);
        try {
            AQuery aq = new AQuery(activity);
            aq.id(R.id.gridView1).itemClicked(this, "commonClick");

            getSupportActionBar().setDisplayOptions(
                    getSupportActionBar().DISPLAY_SHOW_CUSTOM |
                            getSupportActionBar().DISPLAY_SHOW_HOME);
            //14Aug2019 - Bindu - set logged in user
            aq.id(R.id.txtloggedinuser).text(getResources().getString(R.string.loggedinas) + appState.userType);

            BadgeRepository badgeRepository = new BadgeRepository(databaseHelper);
            badgeRepository.updateBadgeCountForMenuItems();

            ComplicationMgmtRepository complrepo = new ComplicationMgmtRepository(databaseHelper);
            hrpcomplmgmtdatelastupdated = complrepo.getDateLastUpdated(appState.sessionUserId);
            //callComplMgmtSync();

            mainMenuActivityWeakReference = new WeakReference<>(this); //10Dec2019 - Bindu


            /*beginTimerTask();*/

			/*myThread = null;
			Runnable myRunnableThread = new CountDownRunner();
			myThread = new Thread(myRunnableThread);
			myThread.start();*/


/*//          07Jun2021 Arpitha
            SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
            Date d = new Date();
            String dayOfTheWeek = sdf. format(d);
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

            if(dayOfTheWeek.equalsIgnoreCase("Monday"))
            {
                if(!sharedPreferences.getBoolean("clearlog",false))
                Toast.makeText(getApplicationContext(),dayOfTheWeek,Toast.LENGTH_LONG).show();
                File file = new File(AppState.logDirRef, "sentiatend_mm_sync_log.txt");

                if(file.exists()) {
                    PrintWriter writer = new PrintWriter(file);
                    writer.print("");
                    writer.close();
                shardPrf = sharedPreferences.edit();
                    shardPrf.putBoolean("clearlog",true);
                    shardPrf.commit();
                }
            }else
            {
                if(sharedPreferences.getBoolean("clearlog",false))
                shardPrf = sharedPreferences.edit();
                shardPrf.putBoolean("clearlog",false);
                shardPrf.commit();
            }//07Jun2021 Arpitha*/


            //Ramesh 7-7-2021
            if (user.getUserId() != null && user.getInst_Database() != null && appState.internetOn){
                getCaseManagment();
            }
            appState.internetOn = InternetConnectionUtil.isConnectionAvailable(getApplicationContext(), appState);

//            Toast.makeText(getApplicationContext(),appState.institutionIpAddress+"\n"+appState.institutionDatabaseName+"\n"+appState.webServiceIpAddress+"\n"+appState.webServiceDBbMster,Toast.LENGTH_LONG).show();

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    //	intialize db
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            assignAdaptersToGridViews(MainMenuActivity.this);
            //			13May2021 Arpitha
            String startActivity = getIntent().getStringExtra(Activity.ACTIVITY_SERVICE);
            if (startActivity != null && startActivity.equalsIgnoreCase("LoginActivity")) {
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MainMenuActivity.this);
                if (prefs.getBoolean("autosync", false))
                    if (user.getInst_Database() != null && user.getInst_Database().length() > 0)
                        callSyncMtd();
                    else
                        Toast.makeText(activity, (getResources().getString(R.string.m119)), Toast.LENGTH_LONG)
                                .show();
            }
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    /**
     * convert Images to Bitmap Initiaze GridArrays with the Image, title,
     * badge(count) set gridarrays to CustomGridViewAdapters
     */
    public void assignAdaptersToGridViews(Activity act) {
        childtoAdolCount = 0;
        setNamesAccordingToRes(getResources());

        //28Nov2019 - Bindu - get notification count
        ComplicationMgmtRepository comprepo = new ComplicationMgmtRepository(databaseHelper);
        int notificationcount = comprepo.getBadgeComplMgmtNotification(appState.sessionUserId); //11Dec2019 - Bindu

        // set grid view item
        Bitmap womanReg = BitmapFactory.decodeResource(act.getResources(), R.drawable.registration);
        Bitmap aLLWomanList = BitmapFactory.decodeResource(act.getResources(), R.drawable.reg_women);
        Bitmap adolescentList = BitmapFactory.decodeResource(act.getResources(), R.drawable.teenagers);
        Bitmap hrp = BitmapFactory.decodeResource(act.getResources(), R.drawable.complicated_preg);
        Bitmap homevisit = BitmapFactory.decodeResource(act.getResources(), R.drawable.ic_homevisitmenu);  //29Aug2019 - Bindu - add homevisit and compl
        Bitmap pendingServices = BitmapFactory.decodeResource(act.getResources(), R.drawable.anm_pending_activities);
        Bitmap nearingDel = BitmapFactory.decodeResource(act.getResources(), R.drawable.delwomen);
        Bitmap sync = BitmapFactory.decodeResource(act.getResources(), R.drawable.sync_icon);
        //18Sep2019 - Bindu - Add logout
        Bitmap reglist = BitmapFactory.decodeResource(act.getResources(), R.drawable.general_examination_baby);//05Nov2019 Arpitha


        Bitmap immunization = BitmapFactory.decodeResource(act.getResources(), R.drawable.immunisation);//05Nov2019 Arpitha
//   13Nov2019 Arpitha
        Bitmap anc = BitmapFactory.decodeResource(act.getResources(), R.drawable.anc);

        Bitmap pnc = BitmapFactory.decodeResource(act.getResources(), R.drawable.postnatalcare);
        //22Nov2019 - Bindu - add notification - compl and ref mgmt
        Bitmap complrefmgmt = BitmapFactory.decodeResource(act.getResources(), R.drawable.notification);
//		25Nov2018 Arpitha
        Bitmap womanDeactivationList = BitmapFactory.decodeResource(act.getResources(),
                R.drawable.deactivate);
        Bitmap childDeactivationList = BitmapFactory.decodeResource(act.getResources(),
                R.drawable.deactivate);

        Bitmap childtoAdolList = BitmapFactory.decodeResource(act.getResources(),
                R.drawable.growth);//mani 13Aug2021

        Bitmap logout = BitmapFactory.decodeResource(act.getResources(), R.drawable.ic_logout);//05Dec2019 Arpitha

        gridArray.clear();
        BadgeRepository badgeRepository = new BadgeRepository(databaseHelper);
        badgeCountMap = badgeRepository.getBadgeCountForMenuItems();
        getChildtoAdolBadgeCount(); // Ramesh 8-Sep-2021
        gridArray.add(new Item(womanReg, sp_WReg,
                badgeCountMap.get(sp_WReg)));
        //12Nov2019 - Bindu - change the text to label to get the count
        // All Registered Women
        gridArray.add(new Item(aLLWomanList, sp_allWList,
                badgeCountMap.get(act.getResources().getString(R.string.sp_allWListlbl)))); //change from common cls txt to lbl
        gridArray.add(new Item(anc, sp_anc,
                badgeCountMap.get(act.getResources().getString(R.string.stranc))));
        gridArray.add(new Item(pnc, sp_pnc,
                badgeCountMap.get(act.getResources().getString(R.string.strpnc))));

        gridArray.add(new Item(hrp, sp_CWomans,
                badgeCountMap.get(act.getResources().getString(R.string.hrplbl))));
        //29Aug2019 - Bindu - Add Home visit and Danger sign
        gridArray.add(new Item(homevisit, sp_home,
                badgeCountMap.get(act.getResources().getString(R.string.homevisitlbl))));

        gridArray.add(new Item(nearingDel, sp_ndWomen,
                badgeCountMap.get(act.getResources().getString(R.string.sp_dwomanlbl))));
        gridArray.add(new Item(pendingServices, sp_pending,
                badgeCountMap.get(getResources().getString(R.string.pendingactivitieslbl))));

        gridArray.add(new Item(reglist, sp_RegList,
                badgeCountMap.get(getResources().getString(R.string.regchildlistlbl))));
//        gridArray.add(new Item(adolescentList, sp_adolescent, badgeCountMap.get(getResources().getString(R.string.chldtoadol)))); //31-Aug-2021
        gridArray.add(new Item(adolescentList, sp_adolescent, badgeCountMap.get(getResources().getString(R.string.adollblmainmenu)))); //15May2021 Bindu
        gridArray.add(new Item(childtoAdolList, sp_childtoadol,childtoAdolCount));// Ramesh 2-Sep-2021
        gridArray.add(new Item(immunization, sp_immn,
                badgeCountMap.get(getResources().getString(R.string.sp_immlbl))));//05Nov2019 Arpitha

        //22Nov2019 - Bindu
        gridArray.add(new Item(complrefmgmt, sp_notification,
                badgeCountMap.get(getResources().getString(R.string.sp_notification)))); //11Dec2019 - Bindu

        gridArray.add(new Item(sync, sp_sync,
                badgeCountMap.get(sp_sync)));

//		25Nov2019 Arpitha
        gridArray.add(new Item(womanDeactivationList, sp_WDeact,
                badgeCountMap.get(getResources().getString(R.string.womandeactivationlbl))));
        gridArray.add(new Item(childDeactivationList, sp_CDeact,
                badgeCountMap.get(getResources().getString(R.string.childdeactivationlbl))));

//        gridArray.add(new Item(logout, sp_LOut,
//                badgeCountMap.get(getResources().getString(R.string.action_logoutlbl)))); // Log out - 05Dec2019 - Bindu

        customGridAdapter1 = new CustomGridViewAdapter(act, R.layout.row_grid, gridArray);
        gridView1.setAdapter(customGridAdapter1);
    }

    private void getChildtoAdolBadgeCount() {
        try{
            ChildRepository childRepository = new ChildRepository(databaseHelper);
            List<TblChildInfo> allChild = childRepository.getallActiveChilds("",0);
            if(allChild!=null){
                for (TblChildInfo s:allChild){
                    int ageyear = Integer.parseInt(DateTimeUtil.calculateAgeInYears(s.getChlDateTimeOfBirth()));
                    int agemonth = DateTimeUtil.calculateAgeInMonths(s.getChlDateTimeOfBirth());
                    if (ageyear == 10) {
                        if (agemonth > 0) {
                            childtoAdolCount++;
                        }
                    } else if (ageyear > 10) {
                        childtoAdolCount++;
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    /**
     * Alert Dialog to select Pregnant or Mother Registration
     */
    private void showPregnantOrMotherSelectionDialog() {
        try {
            final AlertDialog.Builder alert = new AlertDialog.Builder(activity,
                    AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
            final View customView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.pregnant_mother_selection,
                    null);

            aqPMSel = new AQuery(customView);
            radGrpEDDorLmp = customView.findViewById(R.id.radEDDorLmp);

            initializeScreen(aqPMSel.id(R.id.llPregOrMothMainLayout).getView());
            setInitialView();


            aqPMSel.id(R.id.imgclearedddate).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    aqPMSel.id(R.id.etlmpdate).text("");
                }
            });
            aqPMSel.id(R.id.imgclearlmp).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    aqPMSel.id(R.id.etMotherAdd).text("");
                    aqPMSel.id(R.id.etlmpdate).text("");
                    aqPMSel.id(R.id.etgest).text("");
                    aqPMSel.id(R.id.imgclearlmp).gone();
                }
            });
            aqPMSel.id(R.id.imgclearregadate).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());
                    Toast.makeText(activity, getResources().getString(R.string.dateresettotodaysdate), Toast.LENGTH_LONG).show();//21Aug2019 Arpitha
                }
            });//   Arpitha - 20Aug2019

            aqPMSel.id(R.id.etMotherAdd).getEditText().setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    try {
                        if (MotionEvent.ACTION_UP == event.getAction()) {
                            isRegDate = false;
                            assignEditTextAndCallDatePicker(aqPMSel.id(R.id.etMotherAdd).getEditText());
                        }
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());

                    }
                    return true;
                }
            });

            aqPMSel.id(R.id.etregdate).getEditText().setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    try {
                        if (MotionEvent.ACTION_UP == event.getAction()) {
                            isRegDate = true;
                            assignEditTextAndCallDatePicker(aqPMSel.id(R.id.etregdate).getEditText());
                        }
                    } catch (Exception e) {

                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }
                    return true;
                }
            });

            aqPMSel.id(R.id.radLMP).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        setLMP();
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }
                }
            });

            aqPMSel.id(R.id.radEDD).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        setEDD();
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }
                }
            });

            alert.setView(customView);

            alert.setCancelable(true)
                    .setTitle(getResources().getString(R.string.registrationHeading))
                    .setPositiveButton(getResources().getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {

                                }
                            })
                    .setNegativeButton(getResources().getString(R.string.cancel), //01Oct2019 - Bindu - Add cancel button
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    dialog.cancel();
                                }
                            });
            final AlertDialog dialog = alert.create();
            if (dialog != null)
                dialog.show();

            // Overriding the handler immediately after show is probably a
            // better
            // approach than OnShowListener as described below
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        validateAndRegister();
                        if (wantToCloseDialog)
                            dialog.dismiss();
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }

                }
            });
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    // prepare bundle for app state
    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        b.putParcelable("syncState", syncState);
        return b;
    }

    //calculate gestation based on LMP
    private void calculateEddGestation(String lmp) {
        try {

            if (lmp.equalsIgnoreCase("lmp")) {
                if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0
                        && aqPMSel.id(R.id.etMotherAdd).getText().length() > 0) {

                    noOfDays = DateTimeUtil
                            .getNumberOfDaysFromLMP(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                    int noOfWeeks = noOfDays / 7;
                    int days = noOfDays % 7;

                    String strdays = "";

                    if (days > 1)
                        strdays = days + " " + activity.getResources().getString(R.string.days);
                    else if (days == 1)
                        strdays = days + " " + activity.getResources().getString(R.string.day);

                    aqPMSel.id(R.id.etgest)
                            .text(noOfWeeks + " " + activity.getResources().getString(R.string.weeks) + " " + strdays);
                }

                String edd = populateEDD(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                aqPMSel.id(R.id.etlmpdate).text(edd);

            } else {

                String strLmp = CalculateLMP(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                aqPMSel.id(R.id.etlmpdate).text(strLmp);

                if (aqPMSel.id(R.id.etlmpdate).getText().length() > 0
                        && aqPMSel.id(R.id.etlmpdate).getText().length() > 0) {

                    noOfDays = DateTimeUtil
                            .getNumberOfDaysFromLMP(aqPMSel.id(R.id.etlmpdate).getText().toString());
                    int noOfWeeks = noOfDays / 7;
                    int days = noOfDays % 7;

                    String strdays = "";

                    if (days > 1)
                        strdays = days + " " + activity.getResources().getString(R.string.days);
                    else if (days == 1)
                        strdays = days + " " + activity.getResources().getString(R.string.day);

                    aqPMSel.id(R.id.etgest)
                            .text(noOfWeeks + " " + activity.getResources().getString(R.string.weeks) + " " + strdays);
                }

            }
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }

    }

    // Calculate EDD and display in EDD Edit text
    private String populateEDD(String xLMP) throws Exception {
        String stredddate = "";
        try {

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());

            Date LmpDate;
            LmpDate = sdf.parse(xLMP);
            Calendar cal = Calendar.getInstance();
            cal.setTime(LmpDate);
            cal.add(Calendar.DAY_OF_MONTH, 280);

            int day = cal.get(Calendar.DAY_OF_MONTH);
            int month = cal.get(Calendar.MONTH) + 1;
            int year = cal.get(Calendar.YEAR);

            stredddate =
                    String.format("%02d", day) + "-" + String.format("%02d", month) + "-" + String.format("%02d", year);

        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        return stredddate;
    }

    //	calculate LMP based on EDD
    private String CalculateLMP(String EDDate) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        Date LMP;
        LMP = sdf.parse(EDDate);
        Calendar cal = Calendar.getInstance();
        cal.setTime(LMP);
        cal.add(Calendar.DAY_OF_MONTH, -280);

        int day = cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH) + 1;
        int year = cal.get(Calendar.YEAR);

        String lmpDate = String.format("%02d", day) + "-" + String.format("%02d", month) + "-" + year;
        return lmpDate;
    }

    /**
     * This method opens the Date Dialog and sets Date to respective EditText
     */
    private void assignEditTextAndCallDatePicker(EditText editText) {
        String currDate = (editText.getText().length() > 0) ? editText.getText().toString() : "";
        CommonDatePickerFragment.getCommonDate(this, currDate);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        try {
            aqPMSel.id(R.id.imgclearlmp).background(R.drawable.brush1);
            if (view.isShown()) {
                String str = String.format("%02d", day) + "-" + String.format("%02d", month + 1) + "-"
                        + year;

                int days = DateTimeUtil.getDaysBetweenDates(DateTimeUtil.getTodaysDate(), str);

                if (!aqPMSel.id(R.id.radEDD).isChecked()) {
                    if (days >= 0) {
                        if (isRegDate) {
                            if (days < 30) {
                                aqPMSel.id(R.id.etregdate).text(str);
                                aqPMSel.id(R.id.imgclearregadate).visible();//20AUg2019 Arpitha
                            } else if (days > 120) {
                                aqPMSel.id(R.id.etregdate).text("");
                                aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                                Toast.makeText(activity, getResources().getString(R.string.date_val_2_toast),
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha

                            } else {
                                aqPMSel.id(R.id.etregdate).text(str);
                                aqPMSel.id(R.id.imgclearregadate).visible();//20AUg2019 Arpitha
                            }

                        } else if (aqPMSel.id(R.id.radLMP).isChecked()) {
                            String lmporadd = str;
                            int daysdiff = 0;
                            String regdate = aqPMSel.id(R.id.etregdate).getText().toString();

                            if (regdate != null && regdate.trim().length() > 0) {
                                daysdiff = DateTimeUtil.getDaysBetweenDates(regdate, lmporadd);

                                int daysdifffromCUrr = DateTimeUtil
                                        .getDaysBetweenDates(DateTimeUtil.getTodaysDate(), lmporadd);

                                if (daysdiff <= 30) {
                                    Toast.makeText(activity,
                                            getResources().getString(R.string.date_val_3_toast),
                                            Toast.LENGTH_LONG).show();
                                    aqPMSel.id(R.id.etlmpdate).text("");
                                    aqPMSel.id(R.id.etgest).text("");
                                    aqPMSel.id(R.id.etMotherAdd).text("");
                                    aqPMSel.id(R.id.imgclearlmp).gone();//20Aug2019 Arpitha
                                } else if (daysdifffromCUrr > 280) {
                                    Toast.makeText(activity,
                                            getResources().getString(R.string.date_val_4_toast),
                                            Toast.LENGTH_LONG).show();
                                    aqPMSel.id(R.id.etlmpdate).text("");
                                    aqPMSel.id(R.id.etgest).text("");
                                    aqPMSel.id(R.id.etMotherAdd).text("");
                                    aqPMSel.id(R.id.imgclearlmp).gone();//20Aug2019 Arpitha
                                } else {
                                    aqPMSel.id(R.id.etMotherAdd).text(str);
                                    calculateEddGestation("lmp");
                                    aqPMSel.id(R.id.imgclearlmp).visible();//20Aug2019 Arpitha
                                }
                            } else
                                Toast.makeText(activity, getResources().getString(R.string.plz_enter_reg_dt),
                                        Toast.LENGTH_LONG).show();
                        } else if (aqPMSel.id(R.id.rdbMother).isChecked()) {

                            int daysdiff = DateTimeUtil
                                    .getDaysBetweenDates(aqPMSel.id(R.id.etregdate).getText().toString(), str);
                            if (daysdiff > 365) {// Arpitha 28Jun2018 - 23Dec2019 Arpitha - allow max 1 year

                                Toast.makeText(activity, (getResources().getString(R.string.datecannotbeafteroneyearfromcurrentdate)), // 11July2018
                                        // Arpitha
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etMotherAdd).text("");
                            } else
                                aqPMSel.id(R.id.etMotherAdd).text(str);
                        } else if (aqPMSel.id(R.id.rdbchild).isChecked()) {

                            int daysdiff = DateTimeUtil
                                    .getDaysBetweenDates(aqPMSel.id(R.id.etregdate).getText().toString(), str);
                            if (daysdiff > 5844) {// Arpitha 28Jun2018

                                Toast.makeText(activity, (getResources().getString(R.string.date_val_7_toast)), // 11July2018
                                        // Arpitha
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etMotherAdd).text("");
                            } else
                                aqPMSel.id(R.id.etMotherAdd).text(str);
                        }
                    } else {
                        if (isRegDate) {
                            aqPMSel.id(R.id.etregdate).text("");
                            aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                            Toast.makeText(activity,
                                    getResources().getString(R.string.reg_date_cannot_greater_current_date),
                                    Toast.LENGTH_LONG).show();
                            aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha
                        } else {
                            aqPMSel.id(R.id.etMotherAdd).text("");
                            aqPMSel.id(R.id.imgclearlmp).gone();//20AUg2019 Arpitha
                            aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                            if (aqPMSel.id(R.id.rdbPreg).isChecked()) {
                                Toast.makeText(activity,
                                        getResources()
                                                .getString(R.string.lmp_date_time_cannot_be_after_curre_datetime), // 11July2018

                                        Toast.LENGTH_LONG).show();

                                aqPMSel.id(R.id.etlmpdate).text("");
                                aqPMSel.id(R.id.etgest).text("");
                            } else {
                                Toast.makeText(activity,
                                        getResources()
                                                .getString(R.string.del_date_cannot_be_after_current_date_time), // 11July2018
                                        Toast.LENGTH_LONG).show();
                            }

                        }
                    }
                } else {
                    if (!isRegDate) {
                        int days1 = 0;
                        if (aqPMSel.id(R.id.etregdate).getText().toString().trim().length() > 0)
                            days1 = DateTimeUtil.getDaysBetweenDates(str,
                                    aqPMSel.id(R.id.etregdate).getText().toString());

                        if (days1 > 0 && days1 < 273) {
                            aqPMSel.id(R.id.etMotherAdd).text(str);
                            calculateEddGestation("edd");
                            aqPMSel.id(R.id.imgclearlmp).visible();//20Aug2019 Arpitha
                        } else {
                            if (days1 > 280) {
                                aqPMSel.id(R.id.etMotherAdd).text("");
                                aqPMSel.id(R.id.imgclearlmp).gone();//21Aug2019 Arpitha
                                aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                                Toast.makeText(activity, getResources().getString(R.string.date_val_6_toast),

                                        Toast.LENGTH_LONG).show();

                                aqPMSel.id(R.id.etlmpdate).text("");
                                aqPMSel.id(R.id.etgest).text("");

                            } else {
                                aqPMSel.id(R.id.etMotherAdd).text("");
                                aqPMSel.id(R.id.imgclearlmp).gone();//21Aug2019 Arpitha
                                aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                                Toast.makeText(activity, getResources().getString(R.string.date_val_5_toast),
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etlmpdate).text("");
                                aqPMSel.id(R.id.etgest).text("");
                            }
                        }
                    } else {
                        days = DateTimeUtil.getDaysBetweenDates(DateTimeUtil.getTodaysDate(), str);
                        if (days >= 0) {

                            if (days > 90) {
                                aqPMSel.id(R.id.etregdate).text("");
                                aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                                Toast.makeText(activity, getResources().getString(R.string.date_val_2_toast),

                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha
                            } else {
                                aqPMSel.id(R.id.etregdate).text(str);
                            }

                        } else {
                            aqPMSel.id(R.id.etregdate).text("");
                            aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                            Toast.makeText(activity, getResources().getString(R.string.reg_date_cannot_greater_current_date),

                                    Toast.LENGTH_LONG).show();
                            aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha
                        }
                    }
                }

            }

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    /* This method invokes when Item clicked in Menu */
    public void commonClick(AdapterView<?> adapter, View v, int position, long arg3) throws Exception {

        gv1ItemActionOnClick(position);
    }

    /**
     * This method calls relevant activity for Item Clicked
     */
    protected void gv1ItemActionOnClick(int position) throws Exception {
        Item selectedItem = gridArray.get(position);
        String selTitle = selectedItem.getTitle();

        if (selTitle.equals(sp_WReg)) {
//			26Nov2019 Arpitha
            if (user.getIsDeactivated() == 1)
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.userdeactivated), Toast.LENGTH_LONG).show();
            else
                showPregnantOrMotherSelectionDialog();
        } else if (selTitle.equals(sp_allWList)) {
            screen = "all";
            Intent wlist = new Intent(MainMenuActivity.this, RegisteredWomenActionTabs.class);
            wlist.putExtra("screen", screen);
            wlist.putExtra("globalState", getIntent().getBundleExtra("globalState"));
            startActivity(wlist);
        } else if (selTitle.equals(sp_adolescent)) {
            Intent wlist = new Intent(MainMenuActivity.this, AdolescentHome.class);
            Bundle lo = getIntent().getBundleExtra("globalState");
            wlist.putExtra("globalState", lo);
            startActivity(wlist);
        } else if (selTitle.equals(sp_CWomans)) {
            screen = "hrp";
            Intent wlist = new Intent(MainMenuActivity.this, WomanListActivity.class);
            wlist.putExtra("globalState", getIntent().getBundleExtra("globalState"));
            wlist.putExtra("screen", screen);
            startActivity(wlist);
        }
        //29Aug2019 - Bindu - Enable Home visit
        else if (selTitle == sp_home) {
            screen = "homevisit";
            Intent wlist = new Intent(MainMenuActivity.this, WomanListActivity.class);
            wlist.putExtra("globalState", getIntent().getBundleExtra("globalState"));
            wlist.putExtra("screen", screen);
            startActivity(wlist);


        } else if (selTitle.equals(sp_ndWomen)) {
            screen = "nearingdel";
            Intent wlist = new Intent(MainMenuActivity.this, WomanListActivity.class);
            wlist.putExtra("globalState", getIntent().getBundleExtra("globalState"));
            wlist.putExtra("screen", screen);
            startActivity(wlist);
        } else if (selTitle.equals(sp_sync)) { //02Aug2019 - Bindu - comment 1 mtd f
//			syncState.syncUptoDate = false;Arpitha 18Nov2019
//			syncState.responseAckCount = 0;Arpitha 18Nov2019
            //callSyncMtd();
            //04Oct2019 - Bindu - User db chk and then call sync mtd
            if (user.getInst_Database() != null && user.getInst_Database().length() > 0)
                callSyncMtd();
            else
                Toast.makeText(activity, (getResources().getString(R.string.m119)), Toast.LENGTH_LONG)
                        .show();

            callImageUpload();
        } else if (selTitle.equals(sp_pending)) {
            screen = "pending";
            Intent wlist = new Intent(MainMenuActivity.this, PendingUpcomingServiceActivity.class);
            wlist.putExtra("screen", screen);
            wlist.putExtra("globalState", getIntent().getBundleExtra("globalState"));
            startActivity(wlist);
        }
        // 18Sep2019 - Bindu - Logout Click action
        else if (selTitle == sp_LOut) {
            Intent goToScreen = new Intent(MainMenuActivity.this, LoginActivity.class);
            displayAlert(getResources().getString(R.string.m111), goToScreen);
        } else if (selTitle == sp_RegList) {//05Nov2019 Arpitha, 05Dec2019 - Bindu - changed from logout to reg
            Intent goToScreen = new Intent(MainMenuActivity.this, RegisteredChildList.class);
            goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
            goToScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
            startActivity(goToScreen);
        } else if (selTitle == sp_immn) {//05Nov2019 Arpitha
            Intent goToScreen = new Intent(MainMenuActivity.this, ChildImmunizationActivity.class);
            goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
            goToScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
            startActivity(goToScreen);
        } else if (selTitle.equals(sp_anc)) {
            Intent wlist = new Intent(MainMenuActivity.this, ANCWomanListActivity.class);
            ANCWomanListActivity.islmpconfirm = false; //03MAy2021 Bindu set false
            wlist.putExtra("globalState", getIntent().getBundleExtra("globalState"));
            wlist.putExtra("screen", screen);
            startActivity(wlist);
        } else if (selTitle.equals(sp_pnc)) {
            Intent wlist = new Intent(MainMenuActivity.this, PNCWomanListActivity.class);
            wlist.putExtra("globalState", getIntent().getBundleExtra("globalState"));
            wlist.putExtra("screen", screen);
            startActivity(wlist);
        } else if (selTitle.equals(sp_notification)) {
//            Intent wlist = new Intent(MainMenuActivity.this, ComplicationMgmtWomanListActivity.class);
            Intent wlist = new Intent(MainMenuActivity.this, FollowUpTabsActivity.class);
            wlist.putExtra("globalState", getIntent().getBundleExtra("globalState"));
            wlist.putExtra("screen", screen);
            startActivity(wlist);
        }
//		25Nov2019 Arpitha
        else if (selTitle.equals(sp_WDeact)) {
            screen = "deact";
            Intent wlist = new Intent(MainMenuActivity.this, WomanListActivity.class);
            wlist.putExtra("globalState", getIntent().getBundleExtra("globalState"));
            wlist.putExtra("screen", screen);
            startActivity(wlist);
        } else if (selTitle.equals(sp_CDeact)) {//25Nov2019 Arpitha
            Intent goToScreen = new Intent(MainMenuActivity.this, RegisteredChildList.class);
            goToScreen.putExtra("cond", "deact");
            goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
            goToScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
            startActivity(goToScreen);
        }else if (selTitle.equals(sp_childtoadol)) {//23Aug2021 mani
            Intent goToScreen = new Intent(MainMenuActivity.this, ChildtoAdol_List.class);
            goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
            goToScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
            startActivity(goToScreen);
        }


    }

    private void callImageUpload() {
        try {
            AsyncImageUpload asyncImageUpload = new AsyncImageUpload();
            asyncImageUpload.execute();
        } catch (Exception e) {
            e.printStackTrace();
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    //	sync method
    public void callSyncMtd() {
        if (syncState==null){
            Bundle bundle = getIntent().getBundleExtra("globalState");
            syncState = bundle.getParcelable("syncState");
        }
        syncState.syncUptoDate = false;
        syncState.responseAckCount = 0;
        updateLastRequest();
        AsyncCall task = new AsyncCall(this);
        task.execute();
    }

    /**
     * Initializes the Action Bar items
     */
    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        super.onCreateOptionsMenu(menu);
        try {
            LayoutInflater mInflater = LayoutInflater.from(this);
            View mCustomView = mInflater.inflate(R.layout.basic_info, null);
            AQuery aqMenu = new AQuery(mCustomView);

            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayUseLogoEnabled(true);  //06Aug2019 - Bindu
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setCustomView(mCustomView);
            getSupportActionBar().setDisplayShowCustomEnabled(true);

            aqMenu.id(R.id.tvAnmFacName).text(user.getUserName());
            aqMenu.id(R.id.tvAnmRepFacName).text(user.getSubDistrict());
            aqMenu.id(R.id.tvWomanName).gone();

//            getMenuInflater().inflate(R.menu.menu, menu); // 2 menu created, one for release , one for dev
            getMenuInflater().inflate(R.menu.menu_dev, menu);
            menu.findItem(R.id.useractivity).setVisible(true); //16Aug2019 - Bindu -hide the summary screen

            menu.findItem(R.id.sms).setVisible(checkSimState() == TelephonyManager.SIM_STATE_READY);


        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
    }

    /**
     * On selecting action bar icons
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.logout: {
                Intent goToScreen = new Intent(MainMenuActivity.this, LoginActivity.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.m111), goToScreen);
                return true;
            }
            case R.id.devops: {
                displayDevOptions();
                return true;
            }
            //30Sep2019 - Bindu
            case R.id.about: {
                Intent goToScreen = new Intent(MainMenuActivity.this, AboutActivity.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }
//			19Nov2019 Arpitha - Change Pasword
            case R.id.changepassword: {
                //displayChangePasswordDialog();
                try {
                    if (user.getIsDeactivated() != 1)
                        displayChangePasswordDialog();
                    else
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.userdeactivated), Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            }
/*//			20Nov2019 Arpitha
			case R.id.summary: {
				Intent goToScreen = new Intent(MainMenuActivity.this, SummaryTabs.class);
				goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
				startActivity(goToScreen);
				return true;
			}*/
            //			22Nov2019 Arpitha
            case R.id.useractivity: {
                Intent goToScreen = new Intent(MainMenuActivity.this, SummaryTabs.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                startActivity(goToScreen);
                return true;
            }
            //10Nob2019 - Bindu - Settings
            case R.id.settings: {
                Intent goToScreen = new Intent(MainMenuActivity.this, SettingsActivity.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }

            //			25Nov2019 Arpitha - Change Pasword
            case R.id.sms: {
                try {
                    if (user.getIsDeactivated() != 1)
                        display_messagedialog(user.getUserId());
                    else
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.userdeactivated), Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            }
//            28Nov2021 Arpitha
            case R.id.smslog:
                Intent smsLog = new Intent(MainMenuActivity.this, SMSLog.class);
                smsLog.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), smsLog);

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void displayDevOptions() {
        try{
            AlertDialog.Builder builder = new AlertDialog.Builder(MainMenuActivity.this);
            LayoutInflater devops = this.getLayoutInflater();
            View viewD = devops.inflate(R.layout.devops,null);
            builder.setView(viewD);
            AlertDialog alert = builder.create();
            alert.show();
            AQuery aqDev = new AQuery(viewD);
            InstitutionRepository institutionRepository = new InstitutionRepository(databaseHelper);
            TblInstitutionDetails institutionData = institutionRepository.getOneInstitution();
            aqDev.id(R.id.etDevOps_Pro_IP).getEditText().setText(appState.webServiceIpAddress);
            aqDev.id(R.id.etDevOps_Inst_IP).getEditText().setText(institutionData.getIpAddress());
            aqDev.id(R.id.etDevOps_DB_Name).getEditText().setText(appState.webServiceDBbMster);
            aqDev.id(R.id.btnsubmit).getButton().setOnClickListener(view -> {
                String instIp = aqDev.id(R.id.etDevOps_Inst_IP).getEditText().getText().toString();
                String instdb = aqDev.id(R.id.etDevOps_DB_Name).getEditText().getText().toString();
                int update = institutionRepository.updateInstIPandDB(instIp);
                if (update>0){
                    appState.institutionIpAddress = instIp;
                    appState.webServiceDBbMster = instdb;
                    updateProperties(instdb);
                    alert.dismiss();
                    Toast.makeText(getApplicationContext(),"Update Success",Toast.LENGTH_LONG).show();

                }else {
                    Toast.makeText(getApplicationContext(),"Update Failed",Toast.LENGTH_LONG).show();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void updateProperties(String dbname) {
        try{
            InputStream in = getApplicationContext().getAssets().open("stend.properties");
            Properties props = new Properties();
            props.load(in);
            in.close();

            FileOutputStream out = new FileOutputStream("stend.properties");
            props.setProperty("serverdbmaster", dbname);
            props.store(out, null);
            out.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Save Confirmation Alert
     *
     * @param goToScreen
     */
    private void displayAlert(String spanText2, final Intent goToScreen) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set dialog message
        alertDialogBuilder.setMessage(spanText2).setCancelable(false).setNegativeButton(
                getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        if (dialogChangePassword != null && dialogChangePassword.isShowing()) {
                            dialogChangePassword.cancel();
                            dialog.cancel();
                        } else {
                            if (goToScreen != null) {
                                try {
                                    startActivity(goToScreen);
                                    if (spanText2.contains(getResources().getString(R.string.m111))) {
                                        LoginRepository loginRepository = new LoginRepository(databaseHelper);
                                        loginRepository.updateLogoutTime(appState.loginAuditId, appState.sessionUserId);
                                    }
                                } catch (Exception e) {
                                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                                    crashlytics.log(e.getMessage());
                                }
                            } else
                                dialog.cancel();
                        }
                    }
                }).setPositiveButton(getResources().getString(R.string.m122),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    /**
     * This method Calls the idle timeout
     */
    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if (appState != null)
            new ActivitiesStack(this).delayedIdle(appState.idleTimeOut);
    }

    //	cal sync method
    public static class AsyncCall extends AsyncTask<String, Void, String> {
        ProgressDialog pDialog;

        private final WeakReference<MainMenuActivity> mainMenuActivityWeakReference; //Gururaja 07Jun2021

        public AsyncCall(MainMenuActivity mainMenuActivity) {
            mainMenuActivityWeakReference = new WeakReference<>(mainMenuActivity);
        }

        @Override
        protected void onPreExecute() {
            MainMenuActivity mainMenuActivity = mainMenuActivityWeakReference.get();
            if (mainMenuActivity != null && mainMenuActivity.isFinishing()) return;

            int syncCnt = (mainMenuActivity.badgeCountMap != null) ? mainMenuActivity.badgeCountMap.get("Sync") : 0; //15Nov2019 - Bindu - static
            if (syncCnt >= 1)
                pDialog = ProgressDialog.show(mainMenuActivity, "", (mainMenuActivity.getResources().getString(R.string.m117)), true,
                        false);
            else {
                AsyncCall.this.cancel(true);
                Toast.makeText(mainMenuActivity, (mainMenuActivity.getResources().getString(R.string.m162)), Toast.LENGTH_LONG).show();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            String serverResult = null;
            if (this.isCancelled()) {
                return serverResult;
            }
            try {
                MainMenuActivity activity = mainMenuActivityWeakReference.get();
                if (activity != null && activity.isFinishing()) return null;

                assert activity != null;
                appState.internetOn = InternetConnectionUtil.isConnectionAvailable(activity, appState);
                if (appState.internetOn) {
                    serverResult = activity.syncFunctions.sync(activity.user.getUserId());
                }
               /* if(serverResult != null && !serverResult.isEmpty()) {
                    mainMenuActivity.syncState.syncUptoDate = true;
                }Arpitha 18Nov2019*/
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
            return serverResult;
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                MainMenuActivity mainMenuActivity = mainMenuActivityWeakReference.get();
                if (mainMenuActivity != null && mainMenuActivity.isFinishing()) return;
                pDialog.dismiss();
                if (!appState.internetOn)
                    Toast.makeText(mainMenuActivity, (mainMenuActivity.getResources().getString(R.string.m114)), Toast.LENGTH_LONG).show();
//                23Aug2021 Arpitha
                else {
                    if (mainMenuActivity.syncState.movedToInputFolder && mainMenuActivity.syncState.responseAckCount <= 10) {//28Apr2021 Arpitha - changed from 5 to 10
                        if (!mainMenuActivity.syncState.syncUptoDate) {

                            AsyncCall task = new AsyncCall(mainMenuActivity);
                            task.execute();
                        } else {
                            AsyncCall.this.cancel(true);
                            BadgeRepository badgeRepository = new BadgeRepository(mainMenuActivity.databaseHelper);
                            badgeRepository.updateBadgeCountForMenuItems();
                            mainMenuActivity.assignAdaptersToGridViews(mainMenuActivity);
                            Toast.makeText(mainMenuActivity, (mainMenuActivity.getResources().getString(R.string.m118)), Toast.LENGTH_LONG)
                                    .show();
                        }
                    } else {
                        AsyncCall.this.cancel(true);
                        if (result.toLowerCase().contains("failed to connect")){
                            Toast.makeText(mainMenuActivity, (mainMenuActivity.getResources().getString(R.string.couldnotconnecttoserver)), Toast.LENGTH_LONG)
                                    .show();
                        }else {
                            Toast.makeText(mainMenuActivity, (mainMenuActivity.getResources().getString(R.string.m119)), Toast.LENGTH_LONG)
                                    .show();
                        }
                    }
                }
            } catch (Exception e) {

                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
        }
    }


    //	main menu title
    private String
            sp_dSigns,
            sp_allWList, sp_adolescent, sp_WReg, sp_pending, sp_ndWomen, sp_CWomans,
            sp_sync, sp_home, sp_LOut, sp_immn, sp_anc, sp_pnc, sp_WDeact, sp_CDeact, sp_notification, sp_RegList, sp_childtoadol;  //05Dec2019 - Bindu - add reg list

    //	set names according to resouce
    public void setNamesAccordingToRes(Resources res) {
        try {
            Locale locale = null;
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

            if (prefs.getBoolean("isEnglish", false)) {
                locale = new Locale("en");
                font = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
            } else if (prefs.getBoolean("isKannada", false)) {  //10Nov2019 - Bindu - Add kannada
                locale = new Locale("kn");
                font = Typeface.createFromAsset(getAssets(), "Lohit-Kannada.ttf");
            } else if (prefs.getBoolean("isTelugu", false)) {  //02Apr2021
                locale = new Locale("te");
                font = Typeface.createFromAsset(getAssets(), "Lohit-Kannada.ttf");
            } else {  //04Feb2021 - Bindu - Add English
                locale = new Locale("en");
                font = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
            }

            Locale.setDefault(locale);
            Configuration config = res.getConfiguration();
            if (Build.VERSION.SDK_INT >= 17) {
                config.setLocale(locale);
            } else
                config.locale = locale;
            res.updateConfiguration(config, res.getDisplayMetrics());


            sp_allWList = (res.getString(R.string.sp_allWList));
            sp_adolescent = (res.getString(R.string.adolescent));
            sp_WReg = (res.getString(R.string.sp_WReg));
            sp_dSigns = (res.getString(R.string.complications));
            sp_CWomans = (res.getString(R.string.hrp));
            sp_pending = (res.getString(R.string.sp_pending));
            sp_ndWomen = (res.getString(R.string.sp_dwoman));
            sp_sync = (res.getString(R.string.sp_sync));
            sp_home = res.getString(R.string.homevisit);
            sp_RegList = res.getString(R.string.regchildlist);//05Nov2019 Arpitha
            sp_immn = res.getString(R.string.sp_imm);//05Nov2019 Arpitha
            sp_anc = res.getString(R.string.anclbl);//13Nov2019 Arpitha //05Dec2019 -bindu - changed llbl for anc and pnc
            sp_pnc = res.getString(R.string.pnclbl);//13Nov2019 Arpitha
//			25Nov2019 Arpitha
            sp_WDeact = res.getString(R.string.womandeactivation);
            sp_CDeact = res.getString(R.string.childdeactivation);
//22Nov2019 - Bindu
            sp_notification = res.getString(R.string.notification);
            sp_LOut = res.getString(R.string.action_logout); //05Dec2019  Bindu - change reg list and add logout
            sp_childtoadol=res.getString(R.string.childtoadol); //mani 23Aug2021

            mapMenuItemAndMenuItemId();
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    //	set ids to menu itmes
    public void mapMenuItemAndMenuItemId() {
        menuItemAndMenuIdMap.put(sp_WReg, 1);
        menuItemAndMenuIdMap.put(sp_allWList, 2);
        menuItemAndMenuIdMap.put(sp_pending, 3);
        menuItemAndMenuIdMap.put(sp_sync, 4);
        menuItemAndMenuIdMap.put(sp_dSigns, 5);
        menuItemAndMenuIdMap.put(sp_CWomans, 6);
        menuItemAndMenuIdMap.put(sp_ndWomen, 7);
        menuItemAndMenuIdMap.put(sp_immn, 8);
        menuItemAndMenuIdMap.put(sp_anc, 9);
        menuItemAndMenuIdMap.put(sp_pnc, 10);
        menuItemAndMenuIdMap.put(sp_RegList, 11); //07dec2019 - Bindu - change logout to reg list
    }

    @Override
    protected void onDestroy() {

//		timerTask.cancel();
        super.onDestroy();
        /*
         * You'll need this in your class to release the helper when done.
         */
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

    // Arpitha 26Aug2019 - pass it to sync class so that context need not be passed
    public String getAndroidId() {
        String androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        return androidId;
    }

    private void validateAndRegister() throws Exception {
        try {
            Intent nextScreen;
            String lmpOrAdd = aqPMSel.id(R.id.etMotherAdd).getText().toString();
            String regDate = aqPMSel.id(R.id.etregdate).getText().toString();

            if ((lmpOrAdd != null && lmpOrAdd.length() > 0) && (regDate != null && regDate.length() > 0)) {

                if (aqPMSel.id(R.id.rdbPreg).isChecked()) {
                    if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0) {
                        int days = DateTimeUtil.getDaysBetweenDates(regDate, lmpOrAdd);
                        int daysdiff = DateTimeUtil
                                .getDaysBetweenDates(DateTimeUtil.getTodaysDate(), lmpOrAdd);
                        if (aqPMSel.id(R.id.radEDD).isChecked()) {
                            if (days >= 0) {
                                wantToCloseDialog = false;
                                Toast.makeText(getApplicationContext(),
                                        getResources().getString(R.string.date_val_5_toast),

                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etMotherAdd).text("");
                            } else if (noOfDays < 30) {
                                wantToCloseDialog = false;
                                Toast.makeText(getApplicationContext(),
                                        getResources().getString(R.string.gestation_must_be_30days),
                                        Toast.LENGTH_LONG).show();
                            } else if (noOfDays > 280) {
                                wantToCloseDialog = false;
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.gest_280),
                                        Toast.LENGTH_LONG).show();
                            } else {
                                wantToCloseDialog = true;
                                woman = new tblregisteredwomen();
                                nextScreen = new Intent(getApplicationContext(), RegistrationActivity.class);
                                nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                woman.setRegPregnantorMother(1);
                                int gravida = 0;
                                if (aqPMSel.id(R.id.chkFirstPreg).isChecked())
                                    gravida = 1;
                                woman.setRegGravida(gravida);
                                woman.setRegRegistrationDate(aqPMSel.id(R.id.etregdate).getText().toString());
                                woman.setRegEDD(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                                nextScreen.putExtra("woman", woman);
                                startActivity(nextScreen);
                            }
                        } else {
                            if (days < 30) {
                                wantToCloseDialog = false;
                                Toast.makeText(getApplicationContext(),
                                        getResources().getString(R.string.date_val_3_toast),

                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etlmpdate).text("");
                                aqPMSel.id(R.id.etMotherAdd).text("");
                            } else if (daysdiff > 280) {

                                wantToCloseDialog = false;
                                Toast.makeText(getApplicationContext(),
                                        activity.getResources().getString(R.string.date_val_4_toast),
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etlmpdate).text("");
                                aqPMSel.id(R.id.etMotherAdd).text("");
                            } else {
                                wantToCloseDialog = true;
                                woman = new tblregisteredwomen();
                                nextScreen = new Intent(activity, RegistrationActivity.class);
                                nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                woman.setRegPregnantorMother(1);
                                int gravida = 0;
                                if (aqPMSel.id(R.id.chkFirstPreg).isChecked())
                                    gravida = 1;
                                woman.setRegGravida(gravida);
                                woman.setRegRegistrationDate(aqPMSel.id(R.id.etregdate).getText().toString());
                                if (aqPMSel.id(R.id.radEDD).isChecked())
                                    woman.setRegEDD(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                                else
                                    woman.setRegLMP(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                                nextScreen.putExtra("woman", woman);
                                startActivity(nextScreen);
                            }
                        }

                    }
                } else if (aqPMSel.id(R.id.rdbMother).isChecked()) {
                    if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0) {
                        int days = DateTimeUtil.getDaysBetweenDates(regDate, lmpOrAdd);
                        if (days < 0) {
                            wantToCloseDialog = false;
                            Toast.makeText(activity, (getResources().getString(R.string.m077)),
                                    Toast.LENGTH_LONG).show();
                            // } else if (days > 549) {
                        } else if (days > 5844) {// Arpitha
                            // 28Jun2018
                            wantToCloseDialog = false;
                            Toast.makeText(activity,
                                    (getResources().getString(R.string.date_val_7_toast)), // 11july2018
                                    // Arpitha
                                    Toast.LENGTH_LONG).show();
                        } else {
                            wantToCloseDialog = true;
                            woman = new tblregisteredwomen();
                            nextScreen = new Intent(activity, RegistrationActivity.class);
                            woman.setRegPregnantorMother(2);
                            woman.setRegADDate(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                            woman.setRegRegistrationDate(aqPMSel.id(R.id.etregdate).getText().toString());
                            nextScreen.putExtra("woman", woman);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(nextScreen);
                        }
                    }
                } else if (aqPMSel.id(R.id.rdbchild).isChecked()) {
                    if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0) {
                        int days = DateTimeUtil.getDaysBetweenDates(regDate, lmpOrAdd);
                        if (days < 0) {
                            wantToCloseDialog = false;
                            Toast.makeText(activity, (getResources().getString(R.string.m077)),
                                    Toast.LENGTH_LONG).show();
                            // } else if (days > 549) {
                        } else if (days > 5844) {// Arpitha
                            // 28Jun2018
                            wantToCloseDialog = false;
                            Toast.makeText(activity,
                                    (getResources().getString(R.string.date_val_7_toast)), // 11july2018
                                    // Arpitha
                                    Toast.LENGTH_LONG).show();
                        } else {
                            wantToCloseDialog = true;
                            woman = new tblregisteredwomen();
                            nextScreen = new Intent(activity, ChildRegistrationActivity.class);
//							woman.setRegPregnantorMother(1);
                            woman.setRegADDate(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                            woman.setRegRegistrationDate(aqPMSel.id(R.id.etregdate).getText().toString());
                            nextScreen.putExtra("woman", woman);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(nextScreen);
                        }
                    }
                } else if (aqPMSel.id(R.id.rdbAdol).isChecked()) { //Ramesh 13-may-2021
                    if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0) {
                        int days = DateTimeUtil.getDaysBetweenDates(regDate, lmpOrAdd);
                        if (days < 0) {
                            wantToCloseDialog = false;
                            Toast.makeText(activity, (getResources().getString(R.string.m077)),
                                    Toast.LENGTH_LONG).show();
                            // } else if (days > 549) {
                        } else if (days > 5844) {// Arpitha
                            // 28Jun2018
                            wantToCloseDialog = false;
                            Toast.makeText(activity,
                                    (getResources().getString(R.string.date_val_7_toast)), // 11july2018
                                    // Arpitha
                                    Toast.LENGTH_LONG).show();
                        } else {
                            wantToCloseDialog = true;
                            tblAdolReg adolReg = new tblAdolReg();
                            nextScreen = new Intent(activity, AdolRegistration.class);
                            adolReg.setRecordCreatedDate(aqPMSel.id(R.id.etregdate).getText().toString());
                            nextScreen.putExtra("AdolReg", adolReg);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(nextScreen);
                        }
                    }
                }
            } else {
                wantToCloseDialog = false;
                if (aqPMSel.id(R.id.rdbPreg).isChecked()) {
                    if (aqPMSel.id(R.id.etMotherAdd).getText().toString().trim().length() <= 0) {
                        aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                        if (aqPMSel.id(R.id.radEDD).isChecked())
                            Toast.makeText(activity,
                                    (activity.getResources().getString(R.string.m170)),
                                    Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(activity,
                                    activity.getResources().getString(R.string.m074),
                                    Toast.LENGTH_LONG).show();
                    } else if (aqPMSel.id(R.id.etregdate).getText().toString().trim().length() <= 0) {
                        aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                        Toast.makeText(getApplicationContext(),
                                (activity.getResources().getString(R.string.m157)),
                                Toast.LENGTH_LONG).show();
                    }
                } else if (aqPMSel.id(R.id.rdbMother).isChecked()) {
                    if (aqPMSel.id(R.id.etMotherAdd).getText().toString().trim().length() <= 0) {
                        aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                        Toast.makeText(activity,
                                (activity.getResources().getString(R.string.m066)),
                                Toast.LENGTH_LONG).show();
                    } else if (aqPMSel.id(R.id.etregdate).getText().toString().trim().length() <= 0) {
                        aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                        Toast.makeText(activity,
                                activity.getResources().getString(R.string.m157),
                                Toast.LENGTH_LONG).show();
                    }
                } else if (aqPMSel.id(R.id.rdbchild).isChecked()) {
                    wantToCloseDialog = true;
//					if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0)
                    {
//						int days = DateTimeUtil.getDaysBetweenDates(regDate, lmpOrAdd);
						/*if (days < 0) {
							wantToCloseDialog = false;
							Toast.makeText(activity, (getResources().getString(R.string.m077)),
									Toast.LENGTH_LONG).show();
							// } else if (days > 549) {
						} else if (days > 5844) {// Arpitha
							// 28Jun2018
							wantToCloseDialog = false;
							Toast.makeText(activity,
									(getResources().getString(R.string.date_val_7_toast)), // 11july2018
									// Arpitha
									Toast.LENGTH_LONG).show();
						} else {*/
                        wantToCloseDialog = true;
                        woman = new tblregisteredwomen();
                        nextScreen = new Intent(activity, ChildRegistrationActivity.class);
                        woman.setRegPregnantorMother(1);
                        woman.setRegADDate(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                        woman.setRegRegistrationDate(aqPMSel.id(R.id.etregdate).getText().toString());
                        nextScreen.putExtra("woman", woman);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        startActivity(nextScreen);
//						}
                    }
                } else if (aqPMSel.id(R.id.rdbAdol).isChecked()) { //Ramesh 13-may-2021

                    wantToCloseDialog = true;
                    tblAdolReg adolReg = new tblAdolReg();
                    nextScreen = new Intent(activity, AdolRegistration.class);
                    adolReg.setRecordCreatedDate(aqPMSel.id(R.id.etregdate).getText().toString());
                    nextScreen.putExtra("AdolReg", adolReg);
                    nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    startActivity(nextScreen);


                }
            }

        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    //	set initial view of the pregnant or mother pop up
    private void setInitialView() {
        aqPMSel.id(R.id.rdbPreg).checked(true);
        aqPMSel.id(R.id.tvMotherAddHeading).text(getResources().getString(R.string.tvPregLmpHeading));
        aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());
        aqPMSel.id(R.id.radLMP).checked(true);
        aqPMSel.id(R.id.etregdate).getEditText().setKeyListener(null);
        aqPMSel.id(R.id.etMotherAdd).getEditText().setKeyListener(null);
        aqPMSel.id(R.id.lllmpdate).visible();
        aqPMSel.id(R.id.llgest).visible();
        aqPMSel.id(R.id.vHLine4).visible();
        aqPMSel.id(R.id.vHLine6).visible();
        aqPMSel.id(R.id.llnote).visible();

        //13Apr2021 Bindu - set here cos not translated from xml
        aqPMSel.id(R.id.anclbl).text(getResources().getString(R.string.anclbl));
        aqPMSel.id(R.id.txtpnclbl).text(getResources().getString(R.string.pnclbl));
        aqPMSel.id(R.id.lmp_date).text(getResources().getString(R.string.tvedd));
        aqPMSel.id(R.id.txtgest).text(getResources().getString(R.string.tvGestationalageL));
        aqPMSel.id(R.id.txtchildreglbl).text(getResources().getString(R.string.childreg));
        aqPMSel.id(R.id.tvFirstPregHeading).text(getResources().getString(R.string.tvFirstPregHeading));
        aqPMSel.id(R.id.reg_date).text(getResources().getString(R.string.reg_date));
        aqPMSel.id(R.id.instructions).text(getResources().getString(R.string.instruction));
        aqPMSel.id(R.id.regdt).text(getResources().getString(R.string.reg_date));
        aqPMSel.id(R.id.txt1).text(getResources().getString(R.string.date_val_0));
        aqPMSel.id(R.id.txt2).text(getResources().getString(R.string.date_val_1));
        aqPMSel.id(R.id.txt3).text(getResources().getString(R.string.date_val_2));
        aqPMSel.id(R.id.txtlmp).text(getResources().getString(R.string.tvPregLmpHeading));
        aqPMSel.id(R.id.txt4).text(getResources().getString(R.string.date_val_3));
        aqPMSel.id(R.id.txt5).text(getResources().getString(R.string.date_val_4));
        aqPMSel.id(R.id.txt6).text(getResources().getString(R.string.date_val_5));
        aqPMSel.id(R.id.txt7).text(getResources().getString(R.string.date_val_6));
        aqPMSel.id(R.id.txt8).text(getResources().getString(R.string.date_val_7));
        aqPMSel.id(R.id.radLMP).text(getResources().getString(R.string.tvlmp));
        aqPMSel.id(R.id.radEDD).text(getResources().getString(R.string.tvedd));
//22May2021 Bindu set adol reg lbl
        aqPMSel.id(R.id.adolreg).text(getResources().getString(R.string.adolescentreg));

    }

    // capture EDD
    private void setEDD() throws Exception {
        aqPMSel.id(R.id.tvMotherAddHeading).text(getResources().getString(R.string.tvedd));
        aqPMSel.id(R.id.etMotherAdd).text("");
        aqPMSel.id(R.id.imgclearlmp).gone();//20AUg2019 Arpitha
        aqPMSel.id(R.id.lmp_date).text(getResources().getString(R.string.tvlmp));
        aqPMSel.id(R.id.etlmpdate).text("");
        aqPMSel.id(R.id.etgest).text("");
        aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());
    }

    // capture LMP
    private void setLMP() throws Exception {
        aqPMSel.id(R.id.tvMotherAddHeading).text(getResources().getString(R.string.tvlmp));
        aqPMSel.id(R.id.etMotherAdd).text("");
        aqPMSel.id(R.id.imgclearlmp).gone();//20AUg2019 Arpitha
        aqPMSel.id(R.id.lmp_date).text(getResources().getString(R.string.tvedd));
        aqPMSel.id(R.id.etlmpdate).text("");
        aqPMSel.id(R.id.etgest).text("");
    }


    /**
     * Common method invokes when any button clicked This method handles
     * Edittext, save clicks. Validate mandatory fields
     */
    public void commonClick(View v) throws Exception {
        switch (v.getId()) {
            case R.id.rdbPreg: {
                aqPMSel.id(R.id.rdbMother).checked(false);
                aqPMSel.id(R.id.tvFirstPregHeading).enabled(true);
                aqPMSel.id(R.id.rdbchild).checked(false);
                aqPMSel.id(R.id.rdbAdol).checked(false);//Ramesh 13-may2021
                aqPMSel.id(R.id.chkFirstPreg).enabled(true);
                aqPMSel.id(R.id.radEDDorLmp).visible();
                //27Nov2018 - Bindu - radiogrp clear chk to reset radiobtn
                aqPMSel.id(R.id.vHLine5).visible();
                aqPMSel.id(R.id.etMotherAdd).text("");
                aqPMSel.id(R.id.radLMP).checked(true);
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.tvPregLmpHeading)));
                // 27Jun2018 Arpitha
                aqPMSel.id(R.id.etregdate).enabled(true);
                aqPMSel.id(R.id.etregdate).textColor(getResources().getColor(R.color.black));
//				aqPMSel.id(R.id.llregdate).background(R.drawable.editext_none);
                aqPMSel.id(R.id.lllmpdate).visible();
                aqPMSel.id(R.id.llgest).visible();
                aqPMSel.id(R.id.vHLine4).visible();
                aqPMSel.id(R.id.vHLine6).visible();

                aqPMSel.id(R.id.llMotherAdd).visible();
                aqPMSel.id(R.id.llFirstPreg).visible();
                aqPMSel.id(R.id.llnote).visible();
                aqPMSel.id(R.id.vHLine5).visible();
//				19Nov2019 Arpitha
                aqPMSel.id(R.id.llancinstruction).visible();
                aqPMSel.id(R.id.llpncinstruction).gone();

                break;
            }
            case R.id.rdbMother: {
                aqPMSel.id(R.id.rdbPreg).checked(false);
                aqPMSel.id(R.id.rdbchild).checked(false);
                aqPMSel.id(R.id.rdbAdol).checked(false);//Ramesh 13-may2021
                //27Nov2018 - Bindu radiogrp clear chk to reset radiobtn
                radGrpEDDorLmp.clearCheck();
                aqPMSel.id(R.id.radLMP).checked(false);
                aqPMSel.id(R.id.radEDD).checked(false);
                aqPMSel.id(R.id.tvFirstPregHeading).enabled(false);
                aqPMSel.id(R.id.chkFirstPreg).enabled(false);
                aqPMSel.id(R.id.chkFirstPreg).checked(false);
                aqPMSel.id(R.id.radEDDorLmp).gone();
                aqPMSel.id(R.id.vHLine5).gone();
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.deldate)));
                aqPMSel.id(R.id.etMotherAdd).text("");
                // 27Jun2018 Arpitha
                aqPMSel.id(R.id.etregdate).enabled(false);
                aqPMSel.id(R.id.etregdate).textColor(R.color.fontcolor_disable);
                //	aqPMSel.id(R.id.llregdate).background(R.drawable.edittext_disable);
                aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());//21March2019 Arpitha
                aqPMSel.id(R.id.lllmpdate).gone();
                aqPMSel.id(R.id.llgest).gone();
                aqPMSel.id(R.id.vHLine4).gone();
                aqPMSel.id(R.id.vHLine6).gone();

                aqPMSel.id(R.id.llMotherAdd).visible();
                aqPMSel.id(R.id.llFirstPreg).visible();
                aqPMSel.id(R.id.llnote).visible();
                aqPMSel.id(R.id.vHLine5).visible();
                //				19Nov2019 Arpitha
                aqPMSel.id(R.id.llancinstruction).gone();
                aqPMSel.id(R.id.llpncinstruction).visible();
                aqPMSel.id(R.id.llFirstPreg).gone();
                aqPMSel.id(R.id.imgclearlmp).visible();
                break;
            }
            case R.id.radEDD: {

                //	aqPMSel.id(R.id.radEDD).checked(true); //27Nov2018 - Bindu- comment the line
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.deldate)));
                aqPMSel.id(R.id.etMotherAdd).text("");
                aqPMSel.id(R.id.lmp_date).text((getResources().getString(R.string.tvlmp)));// 28Jun2018
                // Arpitha
                // 11July2018 Arpitha
                aqPMSel.id(R.id.etlmpdate).text("");
                aqPMSel.id(R.id.etgest).text("");
                break;
            }
            case R.id.rdbchild: {
                aqPMSel.id(R.id.rdbPreg).checked(false);
                aqPMSel.id(R.id.rdbMother).checked(false);
                aqPMSel.id(R.id.rdbAdol).checked(false);//Ramesh 13-may2021

                //27Nov2018 - Bindu radiogrp clear chk to reset radiobtn
                radGrpEDDorLmp.clearCheck();//Arpitha2021
                aqPMSel.id(R.id.radLMP).checked(false);
                aqPMSel.id(R.id.radEDD).checked(false);
                aqPMSel.id(R.id.tvFirstPregHeading).enabled(false);
                aqPMSel.id(R.id.chkFirstPreg).enabled(false).checked(false);
                aqPMSel.id(R.id.radEDDorLmp).gone();
                aqPMSel.id(R.id.vHLine5).gone();
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.deldate)));
                aqPMSel.id(R.id.etMotherAdd).text("");
                aqPMSel.id(R.id.llMotherAdd).gone();
                aqPMSel.id(R.id.llFirstPreg).gone();
                aqPMSel.id(R.id.llnote).gone();
                // 27Jun2018 Arpitha
                aqPMSel.id(R.id.etregdate).enabled(false);
                aqPMSel.id(R.id.etregdate).textColor(R.color.fontcolor_disable);
                //	aqPMSel.id(R.id.llregdate).background(R.drawable.edittext_disable);
                aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());//21March2019 Arpitha
                aqPMSel.id(R.id.lllmpdate).gone();
                aqPMSel.id(R.id.llgest).gone();
                aqPMSel.id(R.id.vHLine4).gone();
                aqPMSel.id(R.id.vHLine6).gone();
                break;
            }
            case R.id.radLMP: {
                //	aqPMSel.id(R.id.radLMP).checked(true); //27Nov2018 - Bindu- comment the line
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.tvlmp)));
                aqPMSel.id(R.id.etMotherAdd).text("");
                aqPMSel.id(R.id.lmp_date).text((getResources().getString(R.string.tvedd)));// 28Jun2018
                // Arpitha

                // 11July2018 Arpitha
                aqPMSel.id(R.id.etlmpdate).text("");
                aqPMSel.id(R.id.etgest).text("");
                break;
            }

            case R.id.gridView1:
                break;
            case R.id.rdbAdol://Ramesh 13-may-2021
                aqPMSel.id(R.id.rdbPreg).checked(false);
                aqPMSel.id(R.id.rdbMother).checked(false);
                aqPMSel.id(R.id.rdbchild).checked(false);

                radGrpEDDorLmp.clearCheck();//Arpitha2021
                aqPMSel.id(R.id.radLMP).checked(false);
                aqPMSel.id(R.id.radEDD).checked(false);
                aqPMSel.id(R.id.tvFirstPregHeading).enabled(false);
                aqPMSel.id(R.id.chkFirstPreg).enabled(false).checked(false);
                aqPMSel.id(R.id.radEDDorLmp).gone();
                aqPMSel.id(R.id.vHLine5).gone();
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.deldate)));
                aqPMSel.id(R.id.etMotherAdd).text("");
                aqPMSel.id(R.id.llMotherAdd).gone();
                aqPMSel.id(R.id.llFirstPreg).gone();
                aqPMSel.id(R.id.llnote).gone();
                aqPMSel.id(R.id.etregdate).enabled(false);
                aqPMSel.id(R.id.etregdate).textColor(R.color.fontcolor_disable);
                aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());//21March2019 Arpitha
                aqPMSel.id(R.id.lllmpdate).gone();
                aqPMSel.id(R.id.llgest).gone();
                aqPMSel.id(R.id.vHLine4).gone();
                aqPMSel.id(R.id.vHLine6).gone();
                break;
            default: {
                break;
            }
        }
    }

    /**
     * This is a Recursive method that traverse through the group and subgroup
     * of view Sets text, assigns clickListners
     *
     * @throws Exception
     */
    private ArrayList<View> initializeScreen(View v) throws Exception {
        if (!(v instanceof ViewGroup)) {
            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);

            // TextView - Assign Labels
            if (v.getClass() == TextView.class || v.getClass() == RadioButton.class) {
                // aqPMSel.id(v.getId()).text(getSS(aqPMSel.id(v.getId()).getText().toString()));
            }

            if (v.getClass() == RadioButton.class) {
                aqPMSel.id(v.getId()).clicked(this, "commonClick");
            }

            return viewArrayList;
        }

        ArrayList<View> result = new ArrayList<View>();

        ViewGroup viewGroup = (ViewGroup) v;
        for (int i = 0; i < viewGroup.getChildCount(); i++) {

            View child = viewGroup.getChildAt(i);

            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);
            viewArrayList.addAll(initializeScreen(child));

            result.addAll(viewArrayList);
        }
        return result;
    }


    // 19Nov2019 Arpitha
    void displayChangePasswordDialog() {
        try {

            dialogChangePassword = new Dialog(MainMenuActivity.this);

            dialogChangePassword.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
            dialogChangePassword.setContentView(R.layout.dialog_changepassword);
            dialogChangePassword.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.dialog_title);
            txtdialogtitle = dialogChangePassword.findViewById(R.id.graph_title);


            txtdialogtitle.setText(Html.fromHtml("<font color='" + getResources().getColor(R.color.colorPrimary) + "'>"
                    + getResources().getString(R.string.change_password) + "</font>"));
            ImageButton imgbtncanceldialog = dialogChangePassword.findViewById(R.id.imgbtncanceldialog);
            imgbtncanceldialog.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    try {

                        displayAlert(getResources().getString(R.string.exit), null);
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                }
            });
            dialogChangePassword.setTitle(Html.fromHtml("<font color='" + getResources()
                    .getColor(R.color.colorPrimary) + "'>"
                    + getResources().getString(R.string.change_password) + "</font>"));

            dialogChangePassword.show();

            btnchangepass = dialogChangePassword.findViewById(R.id.btnchangepass);
            etoldpass = dialogChangePassword.findViewById(R.id.etoldpass);
            etnewpass = dialogChangePassword.findViewById(R.id.etnewpass);
            etretypepass = dialogChangePassword.findViewById(R.id.etreenterpass);

            btnchangepass.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (!ValidateAllFileds()) {


                        try {


                            final TransactionHeaderRepository transactionHeaderRepository = new TransactionHeaderRepository(databaseHelper);
                            final int transId = transactionHeaderRepository.iCreateNewTrans(user.getUserId(), databaseHelper);

                            final String sql = checkForAudit(transId);


                            TransactionManager.callInTransaction(databaseHelper.getConnectionSource(), new Callable<Void>() {
                                public Void call() throws Exception {

                                    UserRepository userRepository = new UserRepository(databaseHelper);
                                    int update = UserRepository.updateNewPassword(appState.sessionUserId,
                                            sql, transId, databaseHelper);

                                    if (update > 0) {

                                        boolean add = TransactionHeaderRepository.iNewRecordTransNew(appState.sessionUserId, transId, "tblaudittrail", databaseHelper);
                                        if (add) {
                                            etnewpass.setText("");
                                            etoldpass.setText("");
                                            etretypepass.setText("");
                                            dialogChangePassword.cancel();
                                            Toast.makeText(getApplicationContext(),
                                                    getResources().getString(R.string.new_pass_updated_successfully), Toast.LENGTH_LONG)
                                                    .show();
                                        } else
                                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.failedtoupdatenewpassword), Toast.LENGTH_LONG).show();

                                    } else
                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.failedtoupdatenewpassword), Toast.LENGTH_LONG).show();

                                    return null;
                                }
                            });


                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.failedtoupdatenewpassword), Toast.LENGTH_LONG).show();


                        }


                    }

                }

            });

            dialogChangePassword.setCancelable(false);// 03April2017 Arpitha

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    boolean ValidateAllFileds() {
        isErrorFound = false;

        if (etoldpass.getText().toString().trim().length() > 0) {

            if (!(EncDecPassword.encrypt(etoldpass.getText().toString()).equals(user.getPassword()))) {

                etoldpass.setError(getResources().getString(R.string.entered_password_does_not_match_stored_password));
                // return false;
                isErrorFound = true;

            }
        } else {
            etoldpass.setError(getResources().getString(R.string.enter_old_password));
            isErrorFound = true;
        }

        if (etnewpass.getText().toString().trim().length() <= 0) {
            etnewpass.setError(getResources().getString(R.string.enter_new_password));
            isErrorFound = true;
        } else {
            if (etoldpass.getText().toString().equals(etnewpass.getText().toString())) {
                etnewpass.setError(getResources().getString(R.string.new_password_is_same_as_old_password));
                isErrorFound = true;
            }

			/*else if (etnewpass.getText().length() < 6) {
				etnewpass.setError(getResources().getString(R.string.new_password_must_contain_atleast_six_characters));
				isErrorFound = true;
			}*/
        }

        if (etretypepass.getText().toString().trim().length() <= 0) {
            etretypepass.setError(getResources().getString(R.string.re_enter_new_password));
            isErrorFound = true;
        } else {
            if (!(etnewpass.getText().toString().equals(etretypepass.getText().toString()))) {
                etretypepass.setError(getResources().getString(R.string.reentered_pass_does_not_match_new_password));
                isErrorFound = true;

            }
            // 03April2017 Arpitha
            if (etoldpass.getText().toString().equals(etretypepass.getText().toString())) {
                etretypepass.setError(getResources().getString(R.string.new_password_is_same_as_old_password));
                isErrorFound = true;
            } // 03April2017 Arpitha
        }
        return isErrorFound;

    }


    String checkForAudit(int transId) throws Exception {
        String addString = "", delSql = "";


        if (addString == "")
            addString = addString + " Password = " + (char) 34 +
                    EncDecPassword.encrypt(etnewpass.getText().toString()) + (char) 34 + "";
        else
            addString = addString + " ,Password = " + (char) 34 +
                    EncDecPassword.encrypt(etnewpass.getText().toString()) + (char) 34 + "";
        InserttblAuditTrail("Password",
                EncDecPassword.encrypt(etnewpass.getText().toString()),
                EncDecPassword.encrypt(etoldpass.getText().toString()),
                transId, "tblinstusers");


        if (addString != null && addString.length() > 0) {
            delSql = "UPDATE tblinstusers SET ";
            delSql = delSql + addString + " WHERE AshaId = '"
                    + appState.ashaId + "' and UserId = '" + appState.sessionUserId + "'";
        }
        return delSql;
    }


    /**
     * Setting AuditTrail pojo, calls - InserttblAuditTrail()
     *
     * @param transId
     */
    private boolean InserttblAuditTrail(String ColumnName, String Old_Value, String
            New_Value, int transId, String strTableName) throws Exception {

        AuditPojo APJ = new AuditPojo();
        APJ.setUserId(user.getUserId());
        APJ.setTblName(strTableName);
        APJ.setDateChanged(DateTimeUtil.getTodaysDate());
        APJ.setColumnName(ColumnName);
        APJ.setOld_Value(Old_Value);
        APJ.setNew_Value(New_Value);
        APJ.setTransId(transId);
        APJ.setRecordcreateddate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());

        AuditRepository auditRepository = new AuditRepository(databaseHelper);
        return AuditRepository.insertTotblAuditTrailNew(APJ, databaseHelper);

    }

    //24Nov2019 - Bindu - ADD complmgmt sync
    private void callComplMgmtSync() {

			/*int interval = 300;

			dangerSignTimer = new CountDownTimer(300, 600) {

				// This is called every interval. (Every 1 minutes in this
				// example)
				public void onTick(long millisUntilFinished) {
//					task1.execute();
					StartAsyncTaskInParallel(task1);
				}

				public void onFinish() {
					Log.d("test", "Timer last tick");
				}
			}.start();*/

        int interval = 30;

        dangerSignTimer = new CountDownTimer(Long.MAX_VALUE, interval * START_AFTER_SECONDS) {

            // This is called every interval. (Every 1 minutes in this
            // example)
            public void onTick(long millisUntilFinished) {

                AsyncCallComplMgmt task1 = new AsyncCallComplMgmt(MainMenuActivity.this);
                task1.execute();
            }

            public void onFinish() {
                Log.d("test", "Timer last tick");
            }
        }.start();

    }

    public static class AsyncCallComplMgmt extends AsyncTask<String, Void, String> {
        ProgressDialog pDialog;

        private final WeakReference<MainMenuActivity> mainMenuActivityWeakReference;
        private SyncState syncState;

        public AsyncCallComplMgmt(MainMenuActivity mainMenuActivity) {
            mainMenuActivityWeakReference = new WeakReference<>(mainMenuActivity);
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... params) {
            String serverResult = null;
            try {
                MainMenuActivity mainMenuActivity = mainMenuActivityWeakReference.get();
                if (mainMenuActivity != null && mainMenuActivity.isFinishing()) return null;

                appState.internetOn = InternetConnectionUtil.isConnectionAvailable(mainMenuActivity, appState);
                if (appState.internetOn) {
                    ComplicationMgmtRepository complrepo = new ComplicationMgmtRepository(mainMenuActivity.databaseHelper);
                    mainMenuActivity.hrpcomplmgmtdatelastupdated = complrepo.getDateLastUpdated(appState.sessionUserId);

                    serverResult = new DSSyncFunction(mainMenuActivity.databaseHelper, appState,
                            mainMenuActivity.syncState, mainMenuActivity.user)
                            .SyncFunctionCompl
                                    (mainMenuActivity.dbMethods, mainMenuActivity.user.getUserId(),
                                            appState.institutionDatabaseName,
                                            appState.webServiceIpAddress + appState.webServicePath,
                                            mainMenuActivity.hrpcomplmgmtdatelastupdated);//30Sep2021 Arpitha - ip from properties
                }

				/*if(serverResult != null && !serverResult.isEmpty()) {
					mainMenuActivity.syncState.syncUptoDateComplmgmt = true;
				}*/
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
            return serverResult;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                MainMenuActivity mainMenuActivity = mainMenuActivityWeakReference.get();
                if (mainMenuActivity != null && mainMenuActivity.isFinishing()) return;
                if (!appState.internetOn)
                    Log.e("Async compl", "No Internet");
                else {
                    if (result != null && !(result.equalsIgnoreCase("Error")))
                        mainMenuActivity.assignAdaptersToGridViews(mainMenuActivity);
					/*AsyncCallComplMgmt task1 = new AsyncCallComplMgmt(mainMenuActivity);
					task1.execute();*/

					/*
					if (mainMenuActivity.syncState.movedToInputFolderComplmgmt && mainMenuActivity.syncState.responseAckCountComplmgmt <= 5) {
						if (!mainMenuActivity.syncState.syncUptoDateComplmgmt) {
							AsyncCallComplMgmt task1 = new AsyncCallComplMgmt(mainMenuActivity);
							task1.execute();
						} else {
							AsyncCallComplMgmt.this.cancel(true);
							mainMenuActivity.assignAdaptersToGridViews(mainMenuActivity);
							Toast.makeText(mainMenuActivity, (mainMenuActivity.getResources().getString(R.string.m118)), Toast.LENGTH_LONG)
									.show();
						}
					} else {
						AsyncCallComplMgmt.this.cancel(true);
						Toast.makeText(mainMenuActivity, (mainMenuActivity.getResources().getString(R.string.m119)), Toast.LENGTH_LONG)
								.show();
					}*/
                }
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
        }
    }


    // 13Oct2016 Arpitha
    public void display_messagedialog(String userid) throws Exception {
        try {
            final Dialog sms_dialog = new Dialog(this);
//            sms_dialog.setTitle(getResources().getString(R.string.send_sms));

            sms_dialog.setContentView(R.layout.sms_dialog);


            sms_dialog.show();

            ImageButton imgsend = sms_dialog.findViewById(R.id.imgsend);
            ImageButton imgcancel = sms_dialog.findViewById(R.id.imgcancel);
            etphn = sms_dialog.findViewById(R.id.etphno);
            final EditText etmess = sms_dialog.findViewById(R.id.etmess);
            TextView txtcontcats = sms_dialog.findViewById(R.id.txtcontacts);

            txtcontcats.setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    // TODO Auto-generated method stub

                    Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                    startActivity(intent);
                    return true;
                }
            });


            String p_no = "";
            List<TblContactDetails> values;

            SettingsRepository settingsRepository = new SettingsRepository(databaseHelper);

            values = settingsRepository.getSpecificPhoneNumber("General");//12May2021 Arpitha
            p_no = "";
            for (int i = 0; i < values.size(); i++) {

                String s = values.get(i).getContactNumber();
                p_no = p_no + values.get(i).getContactNumber() + ",";
            }
            etphn.setText(p_no);

            imgsend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {

                        if (etphn.getText().toString().trim().length() <= 0) {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_valid_phn_no),
                                    Toast.LENGTH_LONG).show();
                        } else if (etmess.getText().toString().trim().length() <= 0) {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_message),
                                    Toast.LENGTH_LONG).show();
                        } else {
                            sms_dialog.cancel();
                            InserttblMessageLog(etphn.getText().toString(),
                                    "Sentiatend MM:- " + etmess.getText().toString() + ", ASHA - "
                                            + user.getUserName()
                                            + " ," + user.getUserMobileNumber(), user.getUserId());
                            if (isMessageLogsaved) {
                                /*Toast.makeText(getApplicationContext(), getResources().getString(R.string.sending_sms),
                                        Toast.LENGTH_LONG).show();06Feb2020 Arpitha*/
                                sendSMSFunction();

                            }
                        }

                    } catch (Exception e) {

                        e.printStackTrace();
                    }

                }
            });


            imgcancel.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    sms_dialog.cancel();
                }
            });
            sms_dialog.setCancelable(false);
        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    /**
     * This method invokes after successfull save of record Sends SMS to the
     * Specified Number
     */// 13Oct2016 Arpitha
    private void sendSMSFunction() throws Exception {
        new SendSMS(this).checkAndSendMsg(databaseHelper); //mani 15 april 2021

    }

    // 13Oct2016 Arpitha
    public void InserttblMessageLog(String phoneno, String content, String user_id) throws Exception {
        final ArrayList<MessageLogPojo> mlpArr = new ArrayList<MessageLogPojo>();

        final TransactionHeaderRepository transactionHeaderRepository = new TransactionHeaderRepository(databaseHelper);
        final int transId = transactionHeaderRepository.iCreateNewTrans(user.getUserId(), databaseHelper);


        if (phoneno.length() > 0) {
            String[] phn = phoneno.split("\\,");
            for (int i = 0; i < phn.length; i++) {
                String num = phn[i];
                if (num != null && num.length() > 0) {
                    MessageLogPojo mlp =
                            new MessageLogPojo();
                    mlp.setUserId(user_id);
                    mlp.setMsgPhoneNo(num);
                    mlp.setMsgBody(content);
                    mlp.setMsgPriority(1);
                    mlp.setMsgNoOfFailures(0);
                    mlp.setTransId(transId);
                    mlp.setMsgSentDate(DateTimeUtil.getTodaysDate());
                    mlp.setMsgSent(0);
                    mlp.setMsgStage("General");
                    mlp.setMsgUserType(appState.userType);
                    mlp.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
                    mlp.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());//21May2021 Arpitha

                    mlpArr.add(mlp);
                }
            }
        }


              /*  dbh.db.beginTransaction();
              //  int transId = dbh.iCreateNewTrans(user_id);// 15Oct2016 Arpitha
                boolean isinserted = dbh.insertToMessageLog(mlpp, transId);// 15Oct2016
                // Arpitha
                if (isinserted) {
                    isMessageLogsaved = true;
                    dbh.iNewRecordTrans(user_id, transId, Partograph_DB.TBL_MESSAGELOG);// 15Oct2016
                    // Arpitha
                    commitTrans();
                } else
                    throw new Exception("InserttblMessageLog(): Failed to Insert Message to tblMessageLog ");
*/


        TransactionManager.callInTransaction(databaseHelper.getConnectionSource(), new Callable<Void>() {
            public Void call() throws Exception {

                int addMessage = 0;
                SMSRepository smsRepository = new SMSRepository(databaseHelper);
                if (mlpArr != null) {
                    for (final MessageLogPojo mlpp : mlpArr) {

                        addMessage = smsRepository.insertIntoMessageLog(mlpp, databaseHelper);
                    }

                    if (addMessage > 0) {
                        isMessageLogsaved = true;
                        boolean addRegTrans = TransactionHeaderRepository.iNewRecordTransNew(user.getUserId(), transId,
                                "tblmessagelog", databaseHelper);
                    }
                }

                return null;
            }
        });


    }

    //  05Dec2019 Arpitha
    public int checkSimState() {
        int simState = -1;
        // This is to know the status of the Sim
        TelephonyManager telMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        simState = telMgr.getSimState();

        return simState;
    }

    //	to execute async class
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void StartAsyncTaskInParallel(AsyncCallComplMgmt task) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            task.execute();
    }

    class CountDownRunner implements Runnable {
        // @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    ComplicationMgmtRepository complrepo = new ComplicationMgmtRepository(databaseHelper);
                    hrpcomplmgmtdatelastupdated = complrepo.getDateLastUpdated(appState.sessionUserId);
                    doWork();
                    Thread.sleep(600000); // Pause for 10 minutes
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                    crashlytics.log(e.getMessage());
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                    crashlytics.log(e.getMessage());
                }
            }
        }
    }

    public void doWork() throws Exception {
        if (getApplicationContext() != null) {
            runOnUiThread(new Runnable() {
                public void run() {
                    try {
						/*AsyncCallComplMgmt task1 = new AsyncCallComplMgmt(MainMenuActivity.this);
						task1.execute();*/
                        String serverResult = "";
                        MainMenuActivity mainMenuActivity = mainMenuActivityWeakReference.get();
                        if (mainMenuActivity != null && mainMenuActivity.isFinishing()) return;

                        appState.internetOn = InternetConnectionUtil.isConnectionAvailable(mainMenuActivity, appState);
                        if (appState.internetOn)
                            serverResult = new DSSyncFunction(mainMenuActivity.databaseHelper, appState, mainMenuActivity.syncState, mainMenuActivity.user)
                                    .SyncFunctionCompl
                                            (mainMenuActivity.dbMethods, mainMenuActivity.user.getUserId(),
                                                    appState.institutionDatabaseName,
                                                    appState.webServiceIpAddress + appState.webServicePath,
                                                    mainMenuActivity.hrpcomplmgmtdatelastupdated);//30Sep2021 Arpitha - ip from properties
                    } catch (Exception e) {
                        e.printStackTrace();
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }
                }
            });
        }
    }
/*
	Timer timer;
	TimerTask timerTask;
	private void beginTimerTask() {
		initializeTimerTask(this);
		timer = new Timer();
		timer.schedule(timerTask, 10000, 60000);
	}*/

	/*private void initializeTimerTask(final MainMenuActivity mainMenuActivity) {
		timerTask = new TimerTask() {
			@Override
			public void run() {
				Log.i("In MainMenuActivity", "executing task");
				asyncCallComplMgmt = new AsyncCallComplMgmt(mainMenuActivity);
				asyncCallComplMgmt.execute();
			}
		};
	}*/


//    void getRiskFromCC()
//    {
//        Data data = new Data.Builder()
//                .putString("webServiceIpAddress", appState.webServiceIpAddress)
//                .putInt("webServicePath", appState.apiPort)
//                .putString("lastServerDate", lastServerDate)
//                .putString("clientDB", user.getInst_Database())
//                .putString("MMId", user.getUserId())
//                .build();
//
//
//        PeriodicWorkRequest periodicWorkRequest = new PeriodicWorkRequest.Builder(MyPeriodicWork.class,15, TimeUnit.MINUTES)
//                .setInputData(data)
//                .build();
//
//
//        WorkManager.getInstance().getWorkInfoByIdLiveData(periodicWorkRequest.getId()).observe(this, workInfo -> {
//            if (workInfo != null) {
//                if (workInfo.getState() == WorkInfo.State.SUCCEEDED) {
//                    String result = MyPeriodicWork.result;
//                    updateCaseMgmt(result);
//                } else if (workInfo.getState() == WorkInfo.State.FAILED) {
//                    if (MyPeriodicWork.result.contains("failed to connect")) {
//                        AlertDialogUtil.displayAlertMessage("Could not connect to Server, Try again later", MainMenuActivity.this);
//                    }
//                }
//            }
//        });
//        WorkManager.getInstance().enqueue(periodicWorkRequest);
//    }

    private void getCaseManagment() {
        try {
            CaseMgmtRepository caseMgmtRepository = new CaseMgmtRepository(databaseHelper);
            String lastServerDate = caseMgmtRepository.getLastServerDate();
            UserRepository userRepo = new UserRepository(databaseHelper);
            TblInstusers user = userRepo.getOneAuditedUser(appState.ashaId);

            Data data = new Data.Builder()
                    .putString("webServiceIpAddress", appState.webServiceIpAddress)
                    .putInt("webServicePath", appState.apiPort)
                    .putString("lastServerDate", lastServerDate)
                    .putString("clientDB", user.getInst_Database())
                    .putString("MMId", user.getUserId())
                    .build();


            OneTimeWorkRequest periodicWorkRequest = new OneTimeWorkRequest.Builder(MyPeriodicWork.class)
                    .setInputData(data)
                    .build();


            WorkManager.getInstance().getWorkInfoByIdLiveData(periodicWorkRequest.getId()).observe(this, workInfo -> {
                if (workInfo != null) {
                    if (workInfo.getState() == WorkInfo.State.SUCCEEDED) {
                        String result = MyPeriodicWork.result;
                        updateCaseMgmt(result);
                    } else if (workInfo.getState() == WorkInfo.State.FAILED) {
                        if (MyPeriodicWork.result!=null ){
                            if (MyPeriodicWork.result.toLowerCase().contains("failed to connect")||MyPeriodicWork.result.toLowerCase().contains("sockettimeout")) {
                                Toast.makeText(getApplicationContext(), "Could not connect to Server, Try again later", Toast.LENGTH_LONG).show();
                            }
                        }else {
                            Toast.makeText(getApplicationContext(), "Could not connect to Server, Try again later", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            });
            WorkManager.getInstance().enqueue(periodicWorkRequest);
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private void updateCaseMgmt(String result) {
        try {
            ArrayList<ContentValues> arrVillage = null;
            SQLiteDatabase db;
            db = databaseHelper.getWritableDatabase();
            if (result.equals("Error") || result.equals("Internal server error")||result.toLowerCase().contains("unknown database")) {
               /* if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }*/
                Toast.makeText(getApplicationContext(), "Data not fetched, Try again later", Toast.LENGTH_LONG).show();
//                AlertDialogUtil.displayAlertMessage("Data not fetched, Try again later", MainMenuActivity.this);
            } else {
                String xml = result;

                xml = xml.replace("<NewDataSet>", "")
                        .replace("</NewDataSet>", "").
                                replace("<NewDataSet/>", "").trim();

                String[] strVal;
                strVal = xml.split("</Table>");
                if (xml.length() != 0) {
                    for (String s : strVal) {
                        s = s + "</Table>";
                        InputStream is = new ByteArrayInputStream(s.getBytes(StandardCharsets.UTF_8));
                        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                        Document doc = dBuilder.parse(is);
                        doc.getDocumentElement().normalize();

                        NodeList nList = doc.getElementsByTagName("Table");

                        arrVillage = new ArrayList<>();

                        if (nList.getLength() > 0) {

                            for (int temp = 0; temp < nList.getLength(); temp++) {

                                Node nNode = nList.item(temp);

                                ContentValues values = new ContentValues();

                                Element eElement = (Element) nNode;

                                if (eElement.getElementsByTagName("regRiskFactorsByCC").item(0) != null) {

                                    String dtemp = "update tblregisteredwomen Set ";

                                    dtemp = dtemp + " regRiskFactorsByCC = '" +
                                            getTagValue(eElement, "regRiskFactorsByCC") + "', ";
                                    dtemp = dtemp + " regOtherRiskByCC = '" +
                                            getTagValue(eElement, "regOtherRiskByCC") + "', ";

                                    dtemp = dtemp + " regAge = '" +
                                            getTagValue(eElement, "regAge") + "', ";
                                    dtemp = dtemp + " regWomanWeight = '" +
                                            getTagValue(eElement, "regWomanWeight") + "', ";
                                    dtemp = dtemp + " regHeight = '" +
                                            getTagValue(eElement, "regHeight") + "', ";
                                    dtemp = dtemp + " regComplicatedpreg = '" +
                                            getTagValue(eElement, "regComplicatedpreg") + "', ";
                                    dtemp = dtemp + " regAmberOrRedColorCode = '" +
                                            getTagValue(eElement, "regAmberOrRedColorCode") + "', ";
                                    dtemp = dtemp + " regrecommendedPlaceOfDelivery = '" +
                                            getTagValue(eElement, "regrecommendedPlaceOfDelivery") + "', ";
                                    dtemp = dtemp + " regBloodGroup = '" +
                                            getTagValue(eElement, "regBloodGroup") + "', ";
                                    dtemp = dtemp + " regheightUnit = '" +
                                            getTagValue(eElement, "regheightUnit") + "', ";

                                    dtemp = dtemp + " regCCConfirmRisk = '" +
                                            getTagValue(eElement, "regCCConfirmRisk") +
                                            "' where  WomanId = '" +
                                            getTagValue(eElement, "WomanId") + "'";


                                    riskDetails.add(dtemp);

                                } else {
                                    values.put("autoId", getTagValue(eElement, "autoId"));
                                    values.put("UserId", getTagValue(eElement, "UserId"));
                                    values.put("MMUserId", getTagValue(eElement, "MMUserId"));
                                    values.put("BeneficiaryType", getTagValue(eElement, "BeneficiaryType"));
                                    values.put("BeneficiaryID", getTagValue(eElement, "BeneficiaryID"));
                                    values.put("chlNo", getTagValue(eElement, "chlNo"));
                                    values.put("BeneficiaryParentID", getTagValue(eElement, "BeneficiaryParentID"));
                                    values.put("VisitType", getTagValue(eElement, "VisitType"));
                                    values.put("VisitMode", getTagValue(eElement, "VisitMode"));
                                    values.put("VisitNum", getTagValue(eElement, "VisitNum"));
                                    values.put("StatusAtVisit", getTagValue(eElement, "StatusAtVisit"));
                                    values.put("VisitDate", getTagValue(eElement, "VisitDate"));
                                    values.put("VisitNumbyMM", getTagValue(eElement, "VisitNumbyMM"));
                                    values.put("VisitDatebyMM", getTagValue(eElement, "VisitDatebyMM"));
                                    values.put("VisitBeneficiaryDangerSigns", getTagValue(eElement, "VisitBeneficiaryDangerSigns"));
                                    values.put("hcmBeneficiaryVisitFac", getTagValue(eElement, "hcmBeneficiaryVisitFac"));
                                    values.put("hcmReasonForNoVisit", getTagValue(eElement, "hcmReasonForNoVisit"));
                                    values.put("hcmReasonOthers", getTagValue(eElement, "hcmReasonOthers"));
                                    values.put("hcmActTakByUserType", getTagValue(eElement, "hcmActTakByUserType"));
                                    values.put("hcmActTakByUserName", getTagValue(eElement, "hcmActTakByUserName"));
                                    values.put("hcmActTakAtFacility", getTagValue(eElement, "hcmActTakAtFacility"));
                                    values.put("hcmActTakFacilityName", getTagValue(eElement, "hcmActTakFacilityName"));
                                    values.put("hcmActTakDate", getTagValue(eElement, "hcmActTakDate"));
                                    values.put("hcmActTakTime", getTagValue(eElement, "hcmActTakTime"));
                                    values.put("hcmActTakForCompl", getTagValue(eElement, "hcmActTakForCompl"));
                                    values.put("hcmActMedicationsPres", getTagValue(eElement, "hcmActMedicationsPres"));
                                    values.put("hcmActTakStatus", getTagValue(eElement, "hcmActTakStatus"));
                                    values.put("hcmActTakStatusDate", getTagValue(eElement, "hcmActTakStatusDate"));
                                    values.put("hcmActTakStatusTime", getTagValue(eElement, "hcmActTakStatusTime"));
                                    values.put("hcmActTakReferredToFacility", getTagValue(eElement, "hcmActTakReferredToFacility"));
                                    values.put("hcmActTakReferredToFacilityName", getTagValue(eElement, "hcmActTakReferredToFacilityName"));
                                    values.put("hcmActTakComments", getTagValue(eElement, "hcmActTakComments"));
                                    values.put("hcmAdviseGiven", getTagValue(eElement, "hcmAdviseGiven"));
                                    values.put("hcmBeneficiaryCondatVisit", getTagValue(eElement, "hcmBeneficiaryCondatVisit"));
                                    values.put("hcmActionToBeTakenClient", getTagValue(eElement, "hcmActionToBeTakenClient"));
                                    values.put("hcmInputToNextLevel", getTagValue(eElement, "hcmInputToNextLevel"));
                                    values.put("hcmIsANMInformedAbtCompl", getTagValue(eElement, "hcmIsANMInformedAbtCompl"));
                                    values.put("hcmANMAwareOfRef", getTagValue(eElement, "hcmANMAwareOfRef"));
                                    values.put("VisitReferredtoFacType", getTagValue(eElement, "VisitReferredtoFacType"));
                                    values.put("VisitReferredtoFacName", getTagValue(eElement, "VisitReferredtoFacName"));
                                    values.put("VisitReferralSlipNumber", getTagValue(eElement, "VisitReferralSlipNumber"));
                                    values.put("hcmCreatedByUserType", getTagValue(eElement, "hcmCreatedByUserType"));
                                    values.put("transId", getTagValue(eElement, "transId"));
                                    values.put("RecordCreatedDate", getTagValue(eElement, "RecordCreatedDate"));
                                    values.put("RecordUpdatedDate", getTagValue(eElement, "RecordUpdatedDate"));
                                    values.put("ServerCreatedDate", getTagValue(eElement, "ServerCreatedDate"));
                                    values.put("ServerUpdatedDate", getTagValue(eElement, "ServerUpdatedDate"));
                                    arrVillage.add(values);
                                }
                            }
                        }

                        if (arrVillage != null && arrVillage.size() > 0) {
                            for (ContentValues values1 : arrVillage) {
                                db.insert("tblCaseMgmt", null, values1);
                            }
                        }

                        if (riskDetails.size() > 0) {
                            for (String str : riskDetails) {
                                db.execSQL(str);
                            }
                        }
                    }
                }
            }

            BadgeRepository badgeRepository = new BadgeRepository(databaseHelper);
            badgeRepository.updateBadgeCountForMenuItems();
            assignAdaptersToGridViews(MainMenuActivity.this);
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    public String getTagValue(Element eElement, String tagName) {
        if (eElement.getElementsByTagName(tagName).item(0) != null)
            return eElement.getElementsByTagName(tagName).item(0).getTextContent();
        else
            return "";
    }

    private class AsyncImageUpload extends AsyncTask<Void, Void, Void> {
        ArrayList<tblImageStore> allImages = new ArrayList<>();

        @Override
        protected void onPreExecute() {
            try {
                ImageStoreRepository imageStoreRepository = new ImageStoreRepository(databaseHelper);
                allImages = imageStoreRepository.getAllImagesNotUploaded();
            } catch (Exception e) {
                FirebaseCrashlytics firebaseCrashlytics = FirebaseCrashlytics.getInstance();
                firebaseCrashlytics.log(e.getMessage());
            }
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            if (allImages.size() != 0) {
                OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                        .connectTimeout(60, TimeUnit.SECONDS)
                        .readTimeout(60, TimeUnit.SECONDS)
                        .writeTimeout(60, TimeUnit.SECONDS)
                        .build();

                for (tblImageStore s : allImages) {
                    String imagepath = AppState.imgDirRef + "/" + s.getImageName() + ".jpeg";
                    File file = new File(imagepath);
                    if (file.exists()) {
                        RequestBody requestBody = new MultipartBody.Builder()
                                .setType(MultipartBody.FORM)
                                .addFormDataPart("filename", s.getImageName())
                                .addFormDataPart(s.getImageName(), s.getImageName(),
                                        RequestBody.create(MediaType.parse("image/jpg"), file))
                                .build();
                        String endpoint = "http://" + appState.webServiceIpAddress + appState.webServicePath + "image";
                        Request request = new Request.Builder()
                                .url(endpoint)
                                .post(requestBody)
                                .build();
                        try (Response r2 = okHttpClient.newCall(request).execute()) {
                            String responseString2 = r2.body().string();
                            Log.i("Image Upload", responseString2);
                            updateImageStatus(s.getImageName());
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.i("Image Upload", e.getMessage());
                        }
                    }
                }
            }
            return null;
        }
    }

    private void updateImageStatus(String imageName) throws Exception {
        ImageStoreRepository imageStoreRepository = new ImageStoreRepository(databaseHelper);
        int update = imageStoreRepository.updateImageStatusof(imageName);
        if (update > 0) {
            Log.e("Image", "Image uploaded");
        } else {
            Log.e("Image", "Image Not uploaded");
        }
    }
    private void updateLastRequest() {
        try{
            int transId = TransactionHeaderRepository.iCreateNewTransNew(user.getUserId(),databaseHelper);
            UserRepository userRepository = new UserRepository(databaseHelper);
            TblInstusers localuser = userRepository.getOneAuditedUser(appState.ashaId);
            String sql = "";
            sql = "UPDATE `tblinstusers` SET `LastRequestNumber` = "+ TblInstusers.getLastRequestNumber()+" WHERE `userId` = '"+localuser.getUserId()+"'";
            TransactionHeaderRepository.iUpdateRecordTransNew(localuser.getUserId(), transId, "tblinstusers", sql, null, null, databaseHelper);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
