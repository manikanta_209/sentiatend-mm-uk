package com.sc.stmansi.configuration;

import android.app.Application;
import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.repositories.HomeVisitRepository;

import java.util.Map;

public class MyApplication extends Application {

    private static Context mContext;
    /*private Locale locale = null;
    public static Typeface font = null;*/
    //25Jul2021 Bindu
    private DatabaseHelper databaseHelper;
    public static Map<String, String> mapSymptoms;
    public static  Map<String, String> mapInterpretations;
    public static  Map<String, String> mapDiagnosis;

    public SyncState syncState = new SyncState();

    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
        //25Jul2021 Bindu
        databaseHelper = getHelper();
        HomeVisitRepository homeRepo = new HomeVisitRepository(databaseHelper);

        try {
            mapSymptoms = homeRepo.getSymptomsNames();
            mapInterpretations = homeRepo.getInterpretationNames();
            mapDiagnosis = homeRepo.getDiagnosisNames();
        }catch (Exception e) {
            e.printStackTrace();
        }

        /*SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

        Configuration config = getBaseContext().getResources().getConfiguration();

       // String lang = preferences.getString(getString(R.string.pref_locale), "en");
        if (prefs.getBoolean("isEnglish", false)) {
            locale = new Locale("en");
            font = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        }else if (prefs.getBoolean("isKannada", false)) {  //10Nov2019 - Bindu - Add kannada
            locale = new Locale("kn");
            font = Typeface.createFromAsset(getAssets(), "Lohit-Kannada.ttf");
        }else if (prefs.getBoolean("isTelugu", false)) {  //02Apr2021
            locale = new Locale("te");
            font = Typeface.createFromAsset(getAssets(), "Lohit-Kannada.ttf");
        }else {  //04Feb2021 - Bindu - Add English
            locale = new Locale("en");
            font = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        }
     //   String systemLocale = getSystemLocale(config).getLanguage();
        Locale.setDefault(locale);
        setSystemLocale(config, locale);
        updateConfiguration(config);*/
    }

        /*@Override
        public void onConfigurationChanged(Configuration newConfig) {
            super.onConfigurationChanged(newConfig);
            if (locale != null) {
                setSystemLocale(newConfig, locale);
                Locale.setDefault(locale);
                updateConfiguration(newConfig);
            }
        }

        @SuppressWarnings("deprecation")
        private static Locale getSystemLocale(Configuration config) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                return config.getLocales().get(0);
            } else {
                return config.locale;
            }
        }

        @SuppressWarnings("deprecation")
        private static void setSystemLocale(Configuration config, Locale locale) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                config.setLocale(locale);
            } else {
                config.locale = locale;
            }
        }

        @SuppressWarnings("deprecation")
        private void updateConfiguration(Configuration config) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                getBaseContext().createConfigurationContext(config);
            } else {
                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
            }
        }
*/

    public static Context getAppContext() {
        return mContext;
    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }
}
