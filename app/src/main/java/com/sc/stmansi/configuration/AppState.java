package com.sc.stmansi.configuration;

import android.os.Environment;
import android.os.Parcel;
import android.os.Parcelable;

public class AppState implements Parcelable {

	public boolean internetOn;
	public String institutionDatabaseName;
	public String institutionIpAddress;
	public boolean isAppInTestMode;
	public int loginAuditId;
	public String userType;
	public String sessionUserId;
	public int idleTimeOut = 5; //18May2021 Bindu change to 5 mins
	public String webServiceIpAddress;
	public String webServiceDBbMster;
	public String webServicePath;
	public String lostDbPath;
	public String masterDb;
	public String selectedWomanId;
	public int selectedVillageCode;
	public String selectedVillageTitle;
	public int selectedVillagePosition;
	public String ashaId;
	//28Nov2019 - Bindu - add visit num and visitdate
	//public int selectedVisitNum;
	//public String selectedVisitDate;
	//Ramesh 30-6-2021
	public String phcname;
	public String scname;
	public String panchayathname;


	public static String mainDir = Environment.getExternalStorageDirectory() + "/Sentiacare/sentiatendAsha";
	public static String dbDirRef = mainDir + "/Database",
			logDirRef = mainDir + "/Logs",
			reportDirRef = mainDir + "/Reports",
			imgDirRef = mainDir + "/Images_Dir";

	public int apiPort;

	public AppState() {
	}


	protected AppState(Parcel in) {
		internetOn = in.readByte() != 0;
		institutionDatabaseName = in.readString();
		institutionIpAddress = in.readString();
		isAppInTestMode = in.readByte() != 0;
		loginAuditId = in.readInt();
		userType = in.readString();
		sessionUserId = in.readString();
		idleTimeOut = in.readInt();
		webServiceIpAddress = in.readString();
		webServiceDBbMster = in.readString();
		webServicePath = in.readString();
		lostDbPath = in.readString();
		masterDb = in.readString();
		selectedWomanId = in.readString();
		selectedVillageCode = in.readInt();
		selectedVillageTitle = in.readString();
		selectedVillagePosition = in.readInt();
		ashaId = in.readString();
		//selectedVisitNum = in.readInt();
		apiPort = in.readInt();

		//Ramesh 30-6-2021
		phcname = in.readString();
		scname = in.readString();
		panchayathname = in.readString();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeByte((byte) (internetOn ? 1 : 0));
		dest.writeString(institutionDatabaseName);
		dest.writeString(institutionIpAddress);
		dest.writeByte((byte) (isAppInTestMode ? 1 : 0));
		dest.writeInt(loginAuditId);
		dest.writeString(userType);
		dest.writeString(sessionUserId);
		dest.writeInt(idleTimeOut);
		dest.writeString(webServiceIpAddress);
		dest.writeString(webServiceDBbMster);
		dest.writeString(webServicePath);
		dest.writeString(lostDbPath);
		dest.writeString(masterDb);
		dest.writeString(selectedWomanId);
		dest.writeInt(selectedVillageCode);
		dest.writeString(selectedVillageTitle);
		dest.writeInt(selectedVillagePosition);
		dest.writeString(ashaId);
		//28Nov2019 - Bindu
		//dest.writeInt(selectedVisitNum);
		dest.writeInt(apiPort);

		//ramesh 30-6-2021
		dest.writeString(phcname);
		dest.writeString(scname);
		dest.writeString(panchayathname);
	}

	@Override
	public int describeContents() {
		return 0;
	}

	public static final Creator<AppState> CREATOR = new Creator<AppState>() {
		@Override
		public AppState createFromParcel(Parcel in) {
			return new AppState(in);
		}

		@Override
		public AppState[] newArray(int size) {
			return new AppState[size];
		}
	};
}
