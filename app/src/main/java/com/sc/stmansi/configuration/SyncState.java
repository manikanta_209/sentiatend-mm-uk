package com.sc.stmansi.configuration;

import android.os.Parcel;
import android.os.Parcelable;

public class SyncState implements Parcelable {

	public boolean serValidUser = false, serTabLost = false, invalidDb = false, readyToDownload = false;
	public String userIdReturned = "", returnFilename = "", returnedInfo = "";
	public boolean syncUptoDate, movedToInputFolder, isRecordExistsForSync;
	public String Messagecnt = null, RequestId = null, xStatus = null, FileStatus = null, DBStatus = null;
	public int responseAckCount;
	//28Nov2019 - Bindu - add complsyncstate
	public boolean syncUptoDateComplmgmt, movedToInputFolderComplmgmt, isRecordExistsForSyncComplmgmt;
	public int responseAckCountComplmgmt;
	public SyncState(){}

	protected SyncState(Parcel in) {
		serValidUser = in.readByte() != 0;
		serTabLost = in.readByte() != 0;
		invalidDb = in.readByte() != 0;
		readyToDownload = in.readByte() != 0;
		userIdReturned = in.readString();
		returnFilename = in.readString();
		returnedInfo = in.readString();
		syncUptoDate = in.readByte() != 0;
		movedToInputFolder = in.readByte() != 0;
		isRecordExistsForSync = in.readByte() != 0;
		Messagecnt = in.readString();
		RequestId = in.readString();
		xStatus = in.readString();
		FileStatus = in.readString();
		DBStatus = in.readString();
		responseAckCount = in.readInt();
		//28Nov2019 - Bindu - add complmgmt sync state
		syncUptoDateComplmgmt = in.readByte() != 0;
		movedToInputFolderComplmgmt = in.readByte() != 0;
		isRecordExistsForSyncComplmgmt = in.readByte() != 0;
		responseAckCountComplmgmt = in.readInt();
	}

	public static final Creator<SyncState> CREATOR = new Creator<SyncState>() {
		@Override
		public SyncState createFromParcel(Parcel in) {
			return new SyncState(in);
		}

		@Override
		public SyncState[] newArray(int size) {
			return new SyncState[size];
		}
	};

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeByte((byte) (serValidUser ? 1 : 0));
		dest.writeByte((byte) (serTabLost ? 1 : 0));
		dest.writeByte((byte) (invalidDb ? 1 : 0));
		dest.writeByte((byte) (readyToDownload ? 1 : 0));
		dest.writeString(userIdReturned);
		dest.writeString(returnFilename);
		dest.writeString(returnedInfo);
		dest.writeByte((byte) (syncUptoDate ? 1 : 0));
		dest.writeByte((byte) (movedToInputFolder ? 1 : 0));
		dest.writeByte((byte) (isRecordExistsForSync ? 1 : 0));
		dest.writeString(Messagecnt);
		dest.writeString(RequestId);
		dest.writeString(xStatus);
		dest.writeString(FileStatus);
		dest.writeString(DBStatus);
		dest.writeInt(responseAckCount);
		//28Nov2019 - Bindu - complmgmtsyncstate
		dest.writeByte((byte) (syncUptoDateComplmgmt ? 1 : 0));
		dest.writeByte((byte) (movedToInputFolderComplmgmt ? 1 : 0));
		dest.writeByte((byte) (isRecordExistsForSyncComplmgmt ? 1 : 0));
		dest.writeInt(responseAckCountComplmgmt);
	}
}
