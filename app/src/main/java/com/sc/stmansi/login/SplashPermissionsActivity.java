//27Aug2019 Arpitha - code clean up
package com.sc.stmansi.login;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.androidquery.AQuery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sc.stmansi.R;

public class SplashPermissionsActivity extends AppCompatActivity {
    String[] PERMISSIONS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.INTERNET,
            //added two more permissions mani 15feb2021
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    AQuery aqSplash;
    private static final int permission_req_code = 1240;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashscreen);
        aqSplash = new AQuery(this);
        if (checkAndRequestPermissions()) {
            initApp();
        }
    }


    //    cal Login activity
    private void initApp() {

        //mani 15feb2021
        if (checkEnableLocation()) {
            Intent login = new Intent(SplashPermissionsActivity.this, LoginActivity.class);
            startActivity(login);
        } else {
            accessLocationDialog();
        }


    }

    //    check for permissions
    private boolean checkAndRequestPermissions() {
        //Chk which permissions are granted
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String permission : PERMISSIONS) {
            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(permission);
            }
        }
        //Ask for non granted permission
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), permission_req_code);
            return false;
        }
        //App has all the permissions
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case permission_req_code: {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                // Initial
                perms.put(Manifest.permission.INTERNET, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                //mani added 2 more permissions
                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.ACCESS_COARSE_LOCATION, PackageManager.PERMISSION_GRANTED);

                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for permissions

                //MANI 15FEB2021 added  permission ACCESS_FINE_LOCATION
                if (perms.get(Manifest.permission.INTERNET) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                        perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                ) {
                    // All Permissions Granted
                    initApp();
                } else {
                    // Permission Denied
                    Toast.makeText(SplashPermissionsActivity.this, "Some Permission is Denied, please allow permission for that the app can work.", Toast.LENGTH_SHORT)
                            .show();
                    aqSplash.id(R.id.txtwelcome).text("Some Permission is Denied, please allow permission for that the app can work");
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    //mani 15feb2021 to check gps enabled or not
    private boolean checkEnableLocation() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (isGPSEnabled && isNetworkEnabled) {
            return true;
        } else {
            return false;
        }
    }

    //mani 15feb2021 Alert dialog to enable the location
    private void accessLocationDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SplashPermissionsActivity.this);
        alertDialogBuilder.setMessage("Your location settings is set to off. Please enable location to use this app")
                .setCancelable(false)
                .setPositiveButton("LOCATION SETTINGS", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setTitle("Enable Location");
        alertDialog.show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (checkEnableLocation()) {
            Intent login = new Intent(SplashPermissionsActivity.this, LoginActivity.class);
            startActivity(login);
        } else {
            accessLocationDialog();
        }
    }
}
