//24Aug2019 Arpitha - removed unused/commented code
package com.sc.stmansi.login;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.androidquery.AQuery;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.sc.stmansi.R;
import com.sc.stmansi.common.ActivitiesStack;
import com.sc.stmansi.common.AlertDialogUtil;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.common.InternetConnectionUtil;
import com.sc.stmansi.complreferralmanagement.NotificationService;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.configuration.LoginData;
import com.sc.stmansi.configuration.MyApplication;
import com.sc.stmansi.configuration.SyncState;
import com.sc.stmansi.database.DBMethods;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.initialization.AppPropertiesLoader;
import com.sc.stmansi.initialization.SchemaCreator;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.repositories.ANMRepository;
import com.sc.stmansi.repositories.AuditRepository;
import com.sc.stmansi.repositories.ComplicationMgmtRepository;
import com.sc.stmansi.repositories.FacilityRepository;
import com.sc.stmansi.repositories.InstitutionRepository;
import com.sc.stmansi.repositories.LoginRepository;
import com.sc.stmansi.repositories.SyncDetailsRepository;
import com.sc.stmansi.repositories.TransactionHeaderRepository;
import com.sc.stmansi.repositories.UserAddResult;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.repositories.VersionRepository;
import com.sc.stmansi.sync.DSSyncFunction;
import com.sc.stmansi.sync.EncDecPassword;
import com.sc.stmansi.sync.SyncFunctions;
import com.sc.stmansi.sync.WebService;
import com.sc.stmansi.tables.AuditPojo;
import com.sc.stmansi.tables.TblInstitutionDetails;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.TblLoginAudit;
import com.sc.stmansi.tables.tblSyncDetails;
import com.sc.stmansi.tables.tblVersionDetails;

import org.jsoup.Jsoup;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.ref.WeakReference;
import java.lang.reflect.Type;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.sc.stmansi.configuration.MyApplication.getAppContext;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    private AQuery aqLogin;
    private ProgressDialog firstLoginPrgsDialog;
    private String wantPermission = Manifest.permission.READ_PHONE_STATE;
    private String TAG = "PhoneActivityTAG";
    private static DatabaseHelper databaseHelper;
    private DBMethods dbMethods;
    private AppState appState;
    private TblInstusers user;
    private LoginData loginData;
    private SyncState syncState;
    //23Nov2019 - Bindu
    public static Typeface font = null;
    CountDownTimer dangerSignTimer = null;
    public static final int START_AFTER_SECONDS = 60000; // This is 60 seconds

    //Mani 31Jan21
    TextView evalMessage;
    FusedLocationProviderClient fusedLocationProviderClient;
    String varLongitude, varLatitude;
    String longitude, latitude;


    //05Feb2021 - Bindu
    SharedPreferences.Editor prefEditor;
    SharedPreferences prefs;
    boolean isbtnRecheckClick = false;
    static private ProgressDialog pDialog;
    private CheckBox checkBox;
    Context context;

    /*
     * Method Name : onCreate Parameters : Bundle Return Value : void
     * Description : Initializes the login screen Sets the login xml to this
     * class Initialize font and database variables Initialize Golobal Variables
     * - Database name and Version getAllFieldsAndInitialize(view) - Apply text
     * to all fields
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        databaseHelper = getHelper();
        dbMethods = new DBMethods(databaseHelper);

        appState = new AppState();
        AppPropertiesLoader.loadProperties(LoginActivity.this, appState);
        MyApplication myApp = (MyApplication) getAppContext();
        syncState = myApp.syncState;
        SchemaCreator.createDirectories(appState);
        /*
         * This method is to be called only from here and Settings activity, Not
         * anywhere else in the project
         */
        try {
            setNamesAccordingToRes(getResources());
            setContentView(R.layout.activity_sentiatendlogin);
            getSupportActionBar();
            getSupportActionBar().setDisplayOptions(getSupportActionBar().DISPLAY_SHOW_CUSTOM | getSupportActionBar().DISPLAY_SHOW_HOME);

            ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
            aqLogin = new AQuery(this);
            context = LoginActivity.this;
            String versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            aqLogin.id(R.id.txtVersion).text(getResources().getString(R.string.app_name) + " - v" + versionName);
            checkBox = findViewById(R.id.cbtablost);
            checkBox.setOnCheckedChangeListener(this);

            //mani 22/march/2021
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getApplicationContext());


            checkForUpdates();//29Sep2021 Arpitha

            initialValues();
            settablostDetails(); //ramesh 20-3-2020

            //05Feb202 Bindu
            if (user.getUserRole().contains("MM")) // 02Apr2021 Bindu change to MM
                aqLogin.id(R.id.txtashaid).text(getResources().getString(R.string.mansimitraid)); // 02Apr2021 Bindu change to MM id
            else if (user.getUserRole().contains("ASHA"))
                aqLogin.id(R.id.txtashaid).text(getResources().getString(R.string.asha));

            //            12May2021 Bindu - to be removed
            //    aqLogin.id(R.id.etpassword).text("123");


            aqLogin.id(R.id.spnusertype).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    setUserType(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            aqLogin.id(R.id.txtchange).getTextView().setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {

                    aqLogin.id(R.id.etphn).enabled(true);
                    aqLogin.id(R.id.etphn).background(R.drawable.edit_text_style_noborder);
                    return true;
                }
            });

            getUserTypeIdOnType();
            addShortcut(); //26Sep2019 - Bindu - call Addshortcut

//            02Sep2020 Arpitha
            if (prefs != null) {
                String userRole = prefs.getString("userrole", "");
                if (userRole != null && userRole.trim().length() > 0) {
                    if (userRole.equalsIgnoreCase("ASHA"))
                        aqLogin.id(R.id.spnusertype).setSelection(1);
                    else if (userRole.equalsIgnoreCase("ANM"))
                        aqLogin.id(R.id.spnusertype).setSelection(2);
                    else if (userRole.equalsIgnoreCase("AWW"))
                        aqLogin.id(R.id.spnusertype).setSelection(3);
                    else if (userRole.equalsIgnoreCase("MM"))
                        aqLogin.id(R.id.spnusertype).setSelection(4);
                }

                String userRoleId = prefs.getString("userroleid", "");

                if (userRoleId != null && userRoleId.trim().length() > 0) {
                    aqLogin.id(R.id.etanmaid).text(userRoleId);
                }
            }

            //Mani01fab2021
            trialPeriodLeft();



            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        if (InternetConnectionUtil.isConnectionAvailable(getApplicationContext(), appState))
                            startTabLost();
                        else {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.m114), Toast.LENGTH_LONG).show();
                            checkBox.setChecked(false);
                        }
                    }
                }
            });
            Button btsyncnow = findViewById(R.id.btnSyncNow);
            btsyncnow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        if (loginData.ashaId != null && aqLogin.id(R.id.etashaid).getEditText().getText().toString().length() > 0) {
                            Toast.makeText(getApplicationContext(), "Syncing Data...", Toast.LENGTH_LONG).show();
                            String ashaID = aqLogin.id(R.id.etashaid).getEditText().getText().toString();
                            AsyncMySQLFetch asyncMySQLFetch = new AsyncMySQLFetch();
                            asyncMySQLFetch.execute(ashaID);
                        } else {
                            aqLogin.id(R.id.etashaid).getEditText().setError("Please enter Asha Id");
                            aqLogin.id(R.id.etashaid).getEditText().requestFocus();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            /*aqLogin.id(R.id.btnrecheck).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        isbtnRecheckClick = true;
                        UserRepository userRepository = new UserRepository(databaseHelper);
                        TblInstusers institutionStaff = userRepository.getAuditedUsers();
                        if(institutionStaff != null) {
                            user = institutionStaff;
                            appState.sessionUserId = user.getUserId();
                            appState.institutionDatabaseName = user.getInst_Database();
                            loginData = new LoginData();
                            loginData.ashaId = user.getAshaId();
                            loginData.password = user.getPassword();
                        }
                        AsyncCallGetUserSettings task = new AsyncCallGetUserSettings(LoginActivity.this);
                        StartAsyncTaskInParallel(task);
                    }catch (Exception e){
                    }
                }
            });*/
            if (user != null && user.getUserId() != null && user.getIsValidated() == 1)
                CheckAppandDBVersions();


        } catch (Exception e) {
            e.printStackTrace();
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private void CheckAppandDBVersions() {
        try {
            PackageInfo info = getApplicationContext().getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), 0);
            SQLiteDatabase tempDB = databaseHelper.getWritableDatabase();
            int appversionCode = info.versionCode;
            int dbVersionCode = tempDB.getVersion();
            VersionRepository versionRepository = new VersionRepository(databaseHelper);
            if (versionRepository.checkRecords()) {
                int transID = 0;
                boolean changeMade = false;
                tblVersionDetails lastRecords = versionRepository.getLastRecord();

                tblVersionDetails versiondetails = new tblVersionDetails();
                versiondetails.setUserId(user.getUserId());
                versiondetails.setPreAppVersion(lastRecords.getCurAppVersion());
                versiondetails.setRecordCreatedDate(lastRecords.getRecordCreatedDate());

                String sql = "";
                int lastAppVersion = lastRecords.getCurAppVersion();
                if (lastAppVersion != appversionCode) {
                    if (transID == 0) {
                        transID = TransactionHeaderRepository.iCreateNewTransNew(user.getUserId(), databaseHelper);
                    }
                    versiondetails.setPreAppVersion(lastAppVersion);
                    versiondetails.setCurAppVersion(appversionCode);
                    changeMade = true;
                    sql = sql + " curAppVersion = " + appversionCode + " ";
                    sql = sql + ", preAppVersion = " + lastAppVersion + " ";
                    InserttblAuditTrail("curAppVersion", String.valueOf(lastAppVersion), String.valueOf(appversionCode), transID);
                    InserttblAuditTrail("preAppVersion", String.valueOf(lastRecords.getPreAppVersion()), String.valueOf(lastAppVersion), transID);
                } else {
                    versiondetails.setCurAppVersion(lastAppVersion);
                }

                int lastDBVersion = lastRecords.getCurDBVersion();
                if (lastDBVersion != dbVersionCode) {
                    if (transID == 0) {
                        transID = TransactionHeaderRepository.iCreateNewTransNew(user.getUserId(), databaseHelper);
                    }
                    changeMade = true;
                    if (sql.length() > 0) {
                        sql = sql + " ,curDBVersion = " + dbVersionCode + " ";
                    } else {
                        sql = sql + " curDBVersion = " + dbVersionCode + " ";
                    }
                    sql = sql + " ,preDBVersion = " + lastDBVersion + " ";
                    versiondetails.setCurDBVersion(dbVersionCode);
                    InserttblAuditTrail("curDBVersion", String.valueOf(lastDBVersion), String.valueOf(dbVersionCode), transID);
                    InserttblAuditTrail("preDBVersion", String.valueOf(lastRecords.getPreDBVersion()), String.valueOf(lastDBVersion), transID);
                } else {
                    versiondetails.setCurDBVersion(lastDBVersion);
                }

                if (changeMade) {
                    String updatedate = ", RecordUpdatedDate = '" + DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime() + "'";
                    InserttblAuditTrail("RecordUpdatedDate", lastRecords.getRecordUpdatedDate(), DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime(), transID);
                    sql = sql + updatedate;
                    String s = " UPDATE tblVersionDetails SET " + sql + " where UserId = '" + user.getUserId() + "' and RecordCreatedDate = '" + lastRecords.getRecordCreatedDate() + "';";
                    versiondetails.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
                    versiondetails.setTransId(transID);
                    int update = versionRepository.updateData(s);
                    if (update > 0) {
                        Log.e("Version", "Updated");
                        TransactionHeaderRepository.iUpdateRecordTransNew(user.getUserId(), transID, "tblVersionDetails", s, null, null, databaseHelper);
                    } else {
                        Log.e("Version", "Not updated");
                    }
                } else {
                    Log.e("Version", "No changes in versions");
                }

            } else {
                int transID = TransactionHeaderRepository.iCreateNewTransNew(user.getUserId(), databaseHelper);
                tblVersionDetails versiondetails = new tblVersionDetails();
                versiondetails.setUserId(user.getUserId());
                versiondetails.setCurAppVersion(appversionCode);
                versiondetails.setCurDBVersion(dbVersionCode);
                versiondetails.setTransId(transID);
                versiondetails.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
                versiondetails.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());

                Dao<tblVersionDetails, Integer> tblVersionDetailsIntegerDao = databaseHelper.getVersionDetailsDao();
                int add = tblVersionDetailsIntegerDao.create(versiondetails);
                if (add > 0) {
                    Log.e("Version", "Inserted");
                    TransactionHeaderRepository.iNewRecordTransNew(user.getUserId(), transID, "tblVersionDetails", databaseHelper);
                } else {
                    Log.e("Version", "Not inserted");
                }
            }

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    // Bindu 05Feb2021
    private void reValidateTrialPeriod() {
        try {
            isbtnRecheckClick = true;
            UserRepository userRepository = new UserRepository(databaseHelper);
            TblInstusers institutionStaff = userRepository.getAuditedUsers();
            if (institutionStaff != null) {
                user = institutionStaff;
                appState.sessionUserId = user.getUserId();
                appState.institutionDatabaseName = user.getInst_Database();
                loginData = new LoginData();
                loginData.ashaId = user.getAshaId();
                loginData.password = user.getPassword();
            }
            AsyncCallGetUserSettings task = new AsyncCallGetUserSettings(LoginActivity.this);
            StartAsyncTaskInParallel(task);
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private void trialPeriodLeft() throws SQLException {

        //Mani010221
        evalMessage = findViewById(R.id.evalEndedMessage);
        DateTimeUtil dateTimeUtil = new DateTimeUtil();
        InstitutionRepository institutionRepository = new InstitutionRepository(databaseHelper);

        if (institutionRepository.getEvalStartEndDate().getIsEvalPeriod().equals("Yes")) {
            if ((dateTimeUtil.getTodaysDate().compareTo(institutionRepository.getEvalStartEndDate().getEvalEndDate()) == 0) ||
                    (dateTimeUtil.getTodaysDate().compareTo(institutionRepository.getEvalStartEndDate().getEvalEndDate()) > 0) ||
                    (dateTimeUtil.getTodaysDate().compareTo(institutionRepository.getEvalStartEndDate().getEvalStartDate()) < 0)) {
                evalMessage.setText("Your trial period has expired, Please contact Sentiacare team");
                //aqLogin.id(R.id.btnlogin).getImageView().setEnabled(false);
                // aqLogin.id(R.id.btnlogin).getImageView().setColorFilter(R.color.lightgray);
                aqLogin.id(R.id.rlLogin).visibility(View.GONE);
                //aqLogin.id(R.id.btnrecheck).visible();

                reValidateTrialPeriod();
            } else {
                evalMessage.setText("Your trial period remaining - " + CalculateEvalDaysLeft() + " day/s");
                //aqLogin.id(R.id.btnrecheck).gone();
            }
        } else { // 05Feb2021 Bindu
            aqLogin.id(R.id.rlLogin).visibility(View.VISIBLE);
            aqLogin.id(R.id.lltrialperiod).gone();
        }
    }


    //Mani04Feb2021
    public static String CalculateEvalDaysLeft() throws SQLException {
        InstitutionRepository institutionRepository = new InstitutionRepository(databaseHelper);
        DateTimeUtil dateTimeUtil = new DateTimeUtil();

        String oldDate = dateTimeUtil.getTodaysDate();
        String newDate = institutionRepository.getEvalStartEndDate().getEvalEndDate();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        Date d1 = new Date(), d2 = new Date();
        try {
            d1 = sdf.parse(oldDate);
            d2 = sdf.parse(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }

        //Comparing dates
        long difference = Math.abs(d1.getTime() - d2.getTime());
        long differenceDates = difference / (24 * 60 * 60 * 1000);
        String dayDifference = Long.toString(differenceDates);
        return dayDifference;
    }

    //		to get suggestions while entering ANM/AWW id
    private void getUserTypeIdOnType() throws SQLException {
        AutoCompleteTextView autoTextView = findViewById(R.id.etanmaid);
        autoTextView.setThreshold(1);//Used to specify minimum number of
        //characters the user has to type in order to display the drop down hint.
        ANMRepository anmRepository = new ANMRepository(this.databaseHelper);
        List<String> auditedUserIds = anmRepository.getAnmIds();
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this,
                android.R.layout.select_dialog_item, auditedUserIds);
        autoTextView.setAdapter(arrayAdapter);//Setting adapter
    }

    //	intialize db object
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    /**
     * This method will get the user profile if exists, and sets the userid
     */
    private void initialValues() {
        try {
            ArrayAdapter adapter = new ArrayAdapter(this, R.layout.simple_spinner_item,
                    getResources().getStringArray(R.array.usertypedispfirsttime));//03Sep2020 Arpitha
            aqLogin.id(R.id.spnusertype).adapter(adapter);//03Sep2020 Arpitha
            aqLogin.id(R.id.btnlogin).getImageView().setOnClickListener(this);
            //     aqLogin.id(R.id.spnusertype).enabled(false);
            getPhoneNumber();
            aqLogin.id(R.id.etashaid).getEditText().setFocusable(true);
            UserRepository userRepository = new UserRepository(this.databaseHelper);
            user = userRepository.getAuditedUsers();
            setLoginDetails();
        } catch (Exception e) {
            e.printStackTrace();
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private void settablostDetails() {
        try {
            if (user == null) {
                aqLogin.id(R.id.cbtablost).getCheckBox().setVisibility(View.VISIBLE); //03May2021 Bindu - hide tab lost //ramesh 19 may2021
                aqLogin.id(R.id.btnSyncNow).getButton().setVisibility(View.GONE);
                aqLogin.id(R.id.txtlastsyncdate).getTextView().setVisibility(View.GONE);
            } else {
                aqLogin.id(R.id.cbtablost).getCheckBox().setVisibility(View.GONE);
                aqLogin.id(R.id.btnSyncNow).getButton().setVisibility(View.GONE); //10Apr2021 bindu hide
                aqLogin.id(R.id.txtlastsyncdate).getTextView().setVisibility(View.GONE);
                /*SyncDetailsRepository syncDetailsRepository = new SyncDetailsRepository(databaseHelper);
                String lastDate = syncDetailsRepository.getLastDate();
                if (lastDate.length() != 0) {
                    aqLogin.id(R.id.txtlastsyncdate).getTextView().setText("The last synced on: " + lastDate);
                }*/ //24Aug2021 Bindu hide sync details
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
     * Method Name : btnLoginClick Parameters : None Return Value : void
     * Description : if Success - Start TabMenuActiviy
     */
    public void btnLoginClick() throws Exception {

        loginData = new LoginData();
        loginData.ashaId = aqLogin.id(R.id.etashaid).getText().toString();
        loginData.password = aqLogin.id(R.id.etpassword).getText().toString();

        appState.ashaId = loginData.ashaId;

        if (validate()) {
            if (aqLogin.id(R.id.spnusertype).getSelectedItem() != null)
                appState.userType = aqLogin.id(R.id.spnusertype).getSelectedItem().toString();

            UserRepository userRepository = new UserRepository(databaseHelper);
            TblInstusers institutionStaff = userRepository.getOneAuditedUser(loginData.ashaId);

            if (institutionStaff != null) {
                user = institutionStaff; // remove
                appState.sessionUserId = user.getUserId();
            }
            if (!(loginData.ashaId.equals(user.getAshaId()))) {
                Toast.makeText(this,
                        (getResources().getString(R.string.enterregisteredphnnu)), Toast.LENGTH_LONG).show();
                aqLogin.id(R.id.etashaid).getEditText().requestFocus();
            /*}
            else if (!(loginData.password.equals(user.getPassword()))) {*/
            } else if (!(EncDecPassword.encrypt(loginData.password).equals(user.getPassword()))) {

                Toast.makeText(this,
                        (getResources().getString(R.string.enter_valid_password)), Toast.LENGTH_LONG).show();
                aqLogin.id(R.id.etpassword).getEditText().requestFocus();
                aqLogin.id(R.id.etpassword).getEditText().setSelection(aqLogin.id(R.id.etashaid).getText().length());
            }
            /* else if (!(EncDecPassword.encrypt(loginData.password).equals(user.getPassword()))) {
                Toast.makeText(this,
                        (getResources().getString(R.string.enter_valid_password)), Toast.LENGTH_LONG).show();
                aqLogin.id(R.id.etpassword).getEditText().requestFocus();
                aqLogin.id(R.id.etpassword).getEditText().setSelection(aqLogin.id(R.id.etashaid).getText().length());
            }Arpitha */
            else {


                Toast.makeText(this, (getResources().getString(R.string.m003)),
                        Toast.LENGTH_LONG).show();

                FirebaseCrashlytics Crashlytics = FirebaseCrashlytics.getInstance(); //Bindu
                Crashlytics.setUserId(user.getUserId());
                /*Crashlytics.setUserIdentifier(user.getUserId());
                Crashlytics.setUserEmail(user.getEmailId());
                Crashlytics.setString("FacilityName", user.getFacilityName());*/

                //Mani 15feb2021 to store latitude and longitude
                accessCurrentLocation();


                setDataBeforeLogin();

                Intent nextScreen = new Intent(getApplicationContext(), MainMenuActivity.class);
                nextScreen.putExtra("globalState", prepareBundle());
                nextScreen.putExtra(Activity.ACTIVITY_SERVICE, getClass().getSimpleName());//13May2021 Arpitha
                startActivity(nextScreen);


                AsyncCallGetUserSettings task = new AsyncCallGetUserSettings(this);
                StartAsyncTaskInParallel(task);
                prefEditor.putString("userrole", aqLogin.id(R.id.spnusertype).getSelectedItem().toString());  //26Sep2019 - Bindu - app name from strings
                prefEditor.putString("userroleid", aqLogin.id(R.id.etanmaid).getText()
                        .toString());

                prefEditor.apply();

                //07Dec2019 - Bindu - Add compl sync
                // callComplMgmtSync();
                //17Dec2019 - Bindu - check if the service is running - if N then start
                /* if(!isMyServiceRunning(NotificationService.class))
                 */
                startService(new Intent(this, NotificationService.class));
            }
        }

    }

    private void startTabLost() {
        try {
            final AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
            View ashaidView = getLayoutInflater().inflate(R.layout.get_ashid, null);
            builder.setView(ashaidView);
            builder.setCancelable(true);
            final AlertDialog alert = builder.create();
            alert.show();
            Button btnFetch = ashaidView.findViewById(R.id.btnfetch);
            final EditText edashaif = ashaidView.findViewById(R.id.etashaid_tablost);

            btnFetch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (validateAshaID()) {
                        String ashaID = edashaif.getText().toString();
                        alert.dismiss();
                        AsyncMySQLFetch asyncMySQLFetch = new AsyncMySQLFetch();
                        asyncMySQLFetch.execute(ashaID);
                        checkBox.setChecked(false);
                    }
                }

                private boolean validateAshaID() {
                    if (edashaif.getText().toString().length() == 0) {
                        edashaif.setError("Please enter Asha ID");
                        edashaif.requestFocus();
                        return false;
                    } else {
                        return true;
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Mani 11feb2021
    @SuppressLint("MissingPermission")
    private void accessCurrentLocation() {

//        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            return;
//        }
//        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
//        if (location == null) {
//            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, LoginActivity.this);
//            location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
//        }
//
//        if (location != null) {
//            varLatitude = String.valueOf(location.getLatitude());
//            varLongitude = String.valueOf(location.getLongitude());
//        }

        //Initialise the location manager
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        //Check condition
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            //when location service is enabled
            // Get last location
            fusedLocationProviderClient.getLastLocation().addOnCompleteListener(task -> {
                //Initialise the location
                Location location = task.getResult();
                if (location != null) {
                    varLatitude = String.valueOf(location.getLatitude());
                    varLongitude = String.valueOf(location.getLongitude());
                } else {
                    //When location is null
                    //Initialize the location request
                    LocationRequest locationRequest = new LocationRequest()
                            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                            .setInterval(10000)
                            .setFastestInterval(1000)
                            .setNumUpdates(1);

                    //Initialise the location call back
                    LocationCallback locationCallback = new LocationCallback() {
                        @Override
                        public void onLocationResult(LocationResult locationResult) {
                            //Initialise the location
                            Location location1 = locationResult.getLastLocation();
                            //set latitude and longitude
                            varLatitude = String.valueOf(location1.getLatitude());
                            varLongitude = String.valueOf(location1.getLongitude());
                        }
                    };
                    //Request location updates
                    fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
                }
                Log.e("latitude In:" + varLatitude, "Longitude:" + varLongitude);
                try {
                    updateLogindate();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            Log.e("latitude out:" + varLatitude, "Longitude:" + varLongitude);
        } else {
            //when location service is not enabled
            //Open Location settings
            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }

    private void updateLogindate() {
        try {
            LoginRepository loginRepository = new LoginRepository(databaseHelper);
            appState.loginAuditId = loginRepository.updateLatandLong(appState.loginAuditId, varLatitude, varLongitude);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void settingsCoordinates() {
        longitude = varLongitude;
        latitude = varLatitude;
    }

    // prepare a bundle
    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        b.putParcelable("syncState", syncState);
        return b;
    }

    // This method Validates if asha id and password are empty
    // Return true when  asha id and password is Valid
    protected boolean validate() {
        if (loginData.ashaId != null && loginData.ashaId.isEmpty()) {
            Toast.makeText(getApplicationContext(), (getResources().getString(R.string.m001)), Toast.LENGTH_LONG)
                    .show();
            aqLogin.id(R.id.etashaid).getEditText().requestFocus();
            return false;
        } else if (loginData.password != null && loginData.password.isEmpty()) {
            if (loginData.password.equals("")) {
                Toast.makeText(getApplicationContext(), (getResources().getString(R.string.m002)),
                        Toast.LENGTH_LONG).show();
                aqLogin.id(R.id.etpassword).getEditText().requestFocus();
                return false;
            }
        }
        if (aqLogin.id(R.id.etphn).getText().toString().trim().length() > 0) {
            Pattern p = Pattern.compile("(0/91)?[7-9][0-9]{9}");
            Matcher matcher = p.matcher(aqLogin.id(R.id.etphn).getText().toString());
            if (!matcher.matches()) {
                Toast.makeText(getApplicationContext(),
                        getResources().getString(R.string.invalidphonenumber),
                        Toast.LENGTH_LONG).show();
                aqLogin.id(R.id.etphn).getEditText().requestFocus();
                return false;
            }
        }

//        03Sep2020 Arpitha
        if (aqLogin.id(R.id.spnusertype).getSelectedItemPosition() == 0) {
            Toast.makeText(getApplicationContext(), (getResources().getString(R.string.selectuserrole)),
                    Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        try {
            btnLoginClick();
        } catch (Exception e) {
            try {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.setCrashlyticsCollectionEnabled(true);
                crashlytics.log(e.getMessage());


                if (e.toString().toLowerCase().contains("can't downgrade database")) {
                }

                Throwable cause = e;
                while (cause.getCause() != null && cause.getCause() != cause) {
                    cause = cause.getCause();
                }
                String exception = cause.toString();

                if (exception.toLowerCase().contains("no such table: tblinstusers")) {
                    // Uncomment the following two lines during Integration
                    firstLoginPrgsDialog = ProgressDialog.show(LoginActivity.this, "",
                            (getResources().getString(R.string.m116)), true, false);

                    AsyncCallUserValid asyncCallUserValid = new AsyncCallUserValid(this);
                    asyncCallUserValid.execute();
                }

            } catch (Exception e1) {
                e.printStackTrace();
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
        }
    }

    //	 check for permissions
    private boolean checkPermission(String permission) {
        if (Build.VERSION.SDK_INT >= 23) {
            int result = ContextCompat.checkSelfPermission(LoginActivity.this, permission);
            return result == PackageManager.PERMISSION_GRANTED;
        } else {
            return true;
        }
    }

    //  request permissions for first time validation
    private void requestPermission(String permission) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(LoginActivity.this, permission)) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.phonestatepermission), Toast.LENGTH_LONG).show();
        }
        int PERMISSION_REQUEST_CODE = 1;
        ActivityCompat.requestPermissions(LoginActivity.this,
                new String[]{permission}, PERMISSION_REQUEST_CODE);
    }

    //10Aug2019 - Bindu
    @Override
    public void onBackPressed() {
        // To kill the complete App OR to refresh the APP
        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    /**
     * This method Calls the idle timeout
     */
    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        new ActivitiesStack(this).delayedIdle(appState.idleTimeOut);
    }

//    //Mani 15feb2021
//    @Override
//    public void onLocationChanged(Location location) {
////        varLongitude= String.valueOf(location.getLatitude());
////        varLatitude= String.valueOf(location.getLongitude());
//    }
//
//    //Mani 15feb2021
//    @Override
//    public void onStatusChanged(String provider, int status, Bundle extras) {
//
//    }
//
//    //Mani 15feb2021
//    @Override
//    public void onProviderEnabled(String provider) {
//
//    }
//
//    //Mani 15feb2021
//    @Override
//    public void onProviderDisabled(String provider) {
//
//    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        try {
            if (b) {
                if (InternetConnectionUtil.isConnectionAvailable(getApplicationContext(), appState))
                    startTabLost();
                else {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.m114), Toast.LENGTH_LONG).show();
                    checkBox.setChecked(false);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //	 validates user for first time
    public static class AsyncCallUserValid extends AsyncTask<String, String, String> {

        private WeakReference<LoginActivity> loginActivityWeakReference;
        private AQuery aq;

        public AsyncCallUserValid(LoginActivity loginActivity) {
            this.loginActivityWeakReference = new WeakReference<>(loginActivity);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            String serverResult = "";
            try {
                LoginActivity loginActivity = loginActivityWeakReference.get();
                aq = new AQuery(loginActivity);
                SyncFunctions syncFunctions = new SyncFunctions(loginActivity.getAndroidId(), loginActivity.dbMethods, loginActivity.syncState);
                boolean internetOn = InternetConnectionUtil.isConnectionAvailable(loginActivity, loginActivity.appState);
                if (internetOn) {
                    String serverDb = loginActivity.appState.webServiceDBbMster;
                    String serviceURL = loginActivity.appState.webServicePath;
//                    String encryptedPwd = loginActivity.loginData.password;//02Dec2019 Arpitha
                    String encryptedPwd = EncDecPassword.encrypt(loginActivity.loginData.password);
                    //mani 23/2/2021 removed appState parameter
                    WebService webService = new WebService(loginActivity.dbMethods, loginActivity.user, loginActivity.syncState, loginActivity.appState);
                    serverResult = webService.invokeLoginOrSettings(loginActivity.loginData.ashaId, encryptedPwd, serverDb, loginActivity.getAndroidId(),
                            "ValidUserForDownLoad", loginActivity.appState.webServiceIpAddress + serviceURL, true, "");

                    syncFunctions.updateResponse(serverResult);
                }

            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
                e.printStackTrace();
            }
            return serverResult;
        }

        @Override
        protected void onPostExecute(String result) {
            final LoginActivity loginActivity = loginActivityWeakReference.get();
            if (!loginActivity.appState.internetOn) {
                if (loginActivity.firstLoginPrgsDialog != null)
                    loginActivity.firstLoginPrgsDialog.dismiss();
                Toast.makeText(loginActivity, (loginActivity.getApplicationContext().getResources().getString(R.string.m114)),
                        Toast.LENGTH_LONG).show();
            } else if (result.equals("Failed to Connect")) { // Ramesh 29-7-2021 handled if could not connect to server
                if (loginActivity.firstLoginPrgsDialog != null)
                    loginActivity.firstLoginPrgsDialog.dismiss();
                Toast.makeText(loginActivity, (loginActivity.getApplicationContext().getResources().getString(R.string.couldnotconnecttoserver)),
                        Toast.LENGTH_LONG).show();
            } else if (!(result == null)) {
                if (loginActivity.syncState.serValidUser && !(loginActivity.syncState.serTabLost)) {
                    try {
                        if (loginActivity.firstLoginPrgsDialog != null)
                            loginActivity.firstLoginPrgsDialog.dismiss();

                        try {
                            new SchemaCreator(loginActivity.databaseHelper.getWritableDatabase()).createSchema(loginActivity.getApplicationContext());
                        } catch (IOException e) {
                            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                            crashlytics.log(e.getMessage());
                            e.printStackTrace();
                        }

                        UserRepository userRepository = new UserRepository(loginActivity.databaseHelper);
                        UserAddResult userAddResult = userRepository.add(loginActivity.loginData.ashaId, loginActivity.syncState.userIdReturned,
                                EncDecPassword.encrypt(loginActivity.loginData.password),
                                aq.id(R.id.etphn).getText().toString());
                        /*UserAddResult userAddResult = userRepository.add(loginActivity.loginData.ashaId, loginActivity.syncState.userIdReturned,
                                loginActivity.loginData.password,
                                aq.id(R.id.etphn).getText().toString());*/
                        if (userAddResult.rowsAffected > 0) {
                            TransactionHeaderRepository transactionHeaderRepository = new TransactionHeaderRepository(loginActivity.databaseHelper);
                            int transactionId = transactionHeaderRepository.iCreateNewTrans(loginActivity.syncState.userIdReturned, databaseHelper);
                            String statement = userRepository.getUpdateStatementForUserPhoneNumber(userAddResult.user);
                            transactionHeaderRepository.update(loginActivity.loginData.ashaId, transactionId, statement.trim(), new Object[]{aq.id(R.id.etphn).getText().toString(), loginActivity.syncState.userIdReturned});

                            FacilityRepository facilityRepository = new FacilityRepository(loginActivity.databaseHelper);
                            facilityRepository.updateUserId(loginActivity.syncState.userIdReturned);

                            InstitutionRepository institutionRepository = new InstitutionRepository(loginActivity.databaseHelper);
                            institutionRepository.deleteInstitutionDetailsAddNew(loginActivity.appState.webServiceIpAddress);

                            loginActivity.btnLoginClick();
                        }
                    } catch (IOException e1) {
                        e1.printStackTrace();
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e1.getMessage());
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e2.getMessage());
                    }
                } else if (!(loginActivity.syncState.serValidUser) && loginActivity.syncState.serTabLost) {
                    if (loginActivity.firstLoginPrgsDialog != null)
                        loginActivity.firstLoginPrgsDialog.dismiss();

                    AsyncCallUserValid.this.cancel(true);

                    if (loginActivity.syncState.returnedInfo != null) {
                        if (loginActivity.syncState.returnedInfo.contains("Lost data is retrieved and creating new database to download")) //17Aug2021 Ramesh for tablost is already exists
                            Toast.makeText(loginActivity, "Tab Lost is already requested, Please contact the Sentia Team",
                                    Toast.LENGTH_LONG).show();
                    }

//                    new Timer().schedule(new TimerTask() {
//                        @Override
//                        public void run() {
//                            // This call is after the files are ready in server
//                            AsyncCallUserValid validateUsr = new AsyncCallUserValid(loginActivity);
//                            validateUsr.execute();
//                        }
//                    }, 10000);

                } else if (!loginActivity.syncState.serValidUser && !loginActivity.syncState.serTabLost) {
                    if (loginActivity.firstLoginPrgsDialog != null)
                        loginActivity.firstLoginPrgsDialog.dismiss();

                    AsyncCallUserValid.this.cancel(true);


                    if (loginActivity.syncState.returnedInfo != null) {
                        if (loginActivity.syncState.returnedInfo.contains("regist"))
                            Toast.makeText(loginActivity, loginActivity.getResources().getString(R.string.m182),
                                    Toast.LENGTH_LONG).show();
                        else if (loginActivity.syncState.returnedInfo.contains("activ"))
                            Toast.makeText(loginActivity, loginActivity.getResources().getString(R.string.m183),
                                    Toast.LENGTH_LONG).show();
                        else if (loginActivity.syncState.returnedInfo.contains("Sign-in not allowed")) //20May2021 Bindu user already validated
                            Toast.makeText(loginActivity, (loginActivity.syncState.returnedInfo.toString()),
                                    Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(loginActivity, (loginActivity.getResources().getString(R.string.m115)),
                                    Toast.LENGTH_LONG).show();
                    }
                } else if (loginActivity.syncState.serTabLost) {
                    if (loginActivity.firstLoginPrgsDialog != null)
                        loginActivity.firstLoginPrgsDialog.dismiss();

                    AsyncCallUserValid.this.cancel(true);
                    if (loginActivity.syncState.returnedInfo != null) {
                        if (loginActivity.syncState.returnedInfo.contains("Lost data is retrieved and creating new database to download")) //17Aug2021 Ramesh for tablost is already exists
                            Toast.makeText(loginActivity, "Tab Lost is already requested, Please contact the Sentia Team",
                                    Toast.LENGTH_LONG).show();
                    }
                }

            }
        }
    }

    // 27Feb2018 Arpitha -  to fetch settings and user data
    public static class AsyncCallGetUserSettings extends AsyncTask<String, Void, String> {
        private WeakReference<LoginActivity> loginActivityWeakReference;

        public AsyncCallGetUserSettings(LoginActivity loginActivity) {
            this.loginActivityWeakReference = new WeakReference<>(loginActivity);
        }

        @Override
        protected void onPreExecute() {
            /*if(isbtnRecheckClick) {
                LoginActivity loginActivity = loginActivityWeakReference.get();
                pDialog = ProgressDialog.show(loginActivity, "",
                        "Please wait", true, false);
            }*/

        }

        @Override
        protected String doInBackground(String... params) {
            String serverResult = null;
            try {
                LoginActivity loginActivity = loginActivityWeakReference.get();
                if (loginActivity != null && loginActivity.isFinishing()) return null;

                loginActivity.appState.internetOn = InternetConnectionUtil.isConnectionAvailable(loginActivity, loginActivity.appState);
                if (loginActivity.appState.internetOn) {
                    DSSyncFunction dsSyncFunction = new DSSyncFunction(databaseHelper, loginActivity.appState, loginActivity.syncState, loginActivity.user);
                    serverResult = dsSyncFunction.SyncFunctionSettings(loginActivity.loginData,
                            loginActivity.appState.webServiceIpAddress + loginActivity.appState.webServicePath,
                            loginActivity.appState.webServiceDBbMster, loginActivity);
                }
            } catch (Exception e) {
                e.printStackTrace();
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
            return serverResult;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                LoginActivity loginActivity = loginActivityWeakReference.get();
                if (loginActivity != null && loginActivity.isFinishing()) return;

                if (!loginActivity.appState.internetOn) {
                    Toast.makeText(loginActivity, loginActivity.getResources().getString(R.string.m114), Toast.LENGTH_LONG)
                            .show();
                }
                UserRepository userRepository = new UserRepository(loginActivity.databaseHelper);
                TblInstusers users = userRepository.getAuditedUsers();
                if (users != null) {
                    loginActivity.user = users;  //12Aug2019 - Bindu get user details
                }

                //05Feb2021 - Bindu - recheck
                if (loginActivity.isbtnRecheckClick) {
                    loginActivity.isbtnRecheckClick = false;
                    loginActivity.trialPeriodLeft();
                    /*if (pDialog != null)
                        pDialog.dismiss();*/
                }

            } catch (Exception e) {
                e.printStackTrace();
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
        }
    }

    //	to execute async class
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void StartAsyncTaskInParallel(AsyncCallGetUserSettings task) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            task.execute();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

    // Arpitha 26Aug2019 - pass it to sync class so that context need not be passed
    public String getAndroidId() {
        String androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        return androidId;
    }

    //	check permissions
    private void getPhoneNumber() throws Exception {

        if (!checkPermission(wantPermission)) {
            requestPermission(wantPermission);
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                List<SubscriptionInfo> subscription = SubscriptionManager.from(getApplicationContext()).getActiveSubscriptionInfoList();
                String phoneNo = "";
                if (subscription != null && subscription.size() > 0) {
                    for (int i = 0; i < subscription.size(); i++) {
                        SubscriptionInfo info = subscription.get(i);
                        Log.d(TAG, "number " + info.getNumber());
                        Log.d(TAG, "network name : " + info.getCarrierName());
                        Log.d(TAG, "country iso " + info.getCountryIso());

                        if (info.getNumber() != null) {//21Ma2021 Arpitha
                            if (phoneNo.trim().length() > 0)//21Ma2021 Arpitha
                                phoneNo = phoneNo + ", " + info.getNumber();
                            else ///21Ma2021 Arpitha
                                phoneNo = info.getNumber();//21Ma2021 Arpitha
                        }
                    }
                }
                aqLogin.id(R.id.etphn).text(phoneNo);
            }
        }
    }

    //	set UI based on user type
    private void setUserType(int position) {
        if (position > 1 && position < 4 &&
                (aqLogin.id(R.id.spnusertype).getSelectedItem().
                        toString().equalsIgnoreCase("ANM")
                        || aqLogin.id(R.id.spnusertype).getSelectedItem().
                        toString().equalsIgnoreCase("AWW"))) {
            aqLogin.id(R.id.tranmid).visible();
            aqLogin.id(R.id.etanmaid).getEditText().requestFocus();
            if (!aqLogin.id(R.id.spnusertype).getSelectedItem().toString()
                    .equalsIgnoreCase(prefs.getString("userrole", "")))
                aqLogin.id(R.id.etanmaid).text("");          //01Oct2019 - Bindu- clear the previous value
            if (position == 2 && aqLogin.id(R.id.spnusertype).getSelectedItem().
                    toString().equalsIgnoreCase("ANM")) {
                aqLogin.id(R.id.txtanmid).text(getResources().getString(R.string.anmid));
                aqLogin.id(R.id.etanmaid).getEditText().setHint(getResources().getString(R.string.anmid));
            } else if (position == 3 && aqLogin.id(R.id.spnusertype).getSelectedItem().
                    toString().equalsIgnoreCase("AWW")) {
                aqLogin.id(R.id.txtanmid).text(getResources().getString(R.string.awwid));
                aqLogin.id(R.id.etanmaid).getEditText().setHint(getResources().getString(R.string.awwid));

            }//25Aug2020 Arpitha
          /*  else if (position == 3) {
                aqLogin.id(R.id.txtanmid).text(getResources().getString(R.string.healthfacilitatorid));
                aqLogin.id(R.id.etanmaid).getEditText().setHint(getResources().getString(R.string.healthfacilitatorid));

            }*/
        } else
            aqLogin.id(R.id.tranmid).gone();


        if (user != null && user.getUserRole() != null && (user.getUserRole().contains("ASHA")) &&
                aqLogin.id(R.id.spnusertype).getSelectedItem().toString()
                        .equalsIgnoreCase("MM")) //2 Apr2021 Bindu change to MM
        {
            aqLogin.id(R.id.tranmid).visible();
            aqLogin.id(R.id.etanmaid).getEditText().requestFocus();
            if (!aqLogin.id(R.id.spnusertype).getSelectedItem().toString()
                    .equalsIgnoreCase(prefs.getString("userrole", "")))
                aqLogin.id(R.id.etanmaid).text("");
            aqLogin.id(R.id.txtanmid).text(getResources().getString(R.string.mansimitraid)); //02Ap2021 Bindu change to MM
            aqLogin.id(R.id.etanmaid).getEditText().setHint(getResources().getString(R.string.mansimitraid)); //02Ap2021 Bindu change to MM

        }

        //02Apr2021 Bindu - change to MM
        if (user != null && user.getUserRole() != null && (user.getUserRole().contains("MM")) &&
                aqLogin.id(R.id.spnusertype).getSelectedItem().toString()
                        .equalsIgnoreCase("ASHA")) {
            aqLogin.id(R.id.tranmid).visible();
            aqLogin.id(R.id.etanmaid).getEditText().requestFocus();
            if (!aqLogin.id(R.id.spnusertype).getSelectedItem().toString()
                    .equalsIgnoreCase(prefs.getString("userrole", "")))
                aqLogin.id(R.id.etanmaid).text("");
            aqLogin.id(R.id.txtanmid).text(getResources().getString(R.string.asha));
            aqLogin.id(R.id.etanmaid).getEditText().setHint(getResources().getString(R.string.asha));

        }
    }

    //    set login details
    private void setLoginDetails() {
        if (user != null) {
            aqLogin.id(R.id.etashaid).text((user.getAshaId()));
            aqLogin.id(R.id.etpassword).text("");
            aqLogin.id(R.id.spnusertype).enabled(true);
            aqLogin.id(R.id.spnusertype).setSelection(0);
            aqLogin.id(R.id.etashaid).getEditText().setFocusable(false);
            aqLogin.id(R.id.etpassword).getEditText().setFocusable(true);
            aqLogin.id(R.id.etphn).text(user.getDevicePhnNumber());
            if (aqLogin.id(R.id.etphn).getText().toString().trim().length() > 0) {
                aqLogin.id(R.id.etphn).enabled(false);
                aqLogin.id(R.id.llchange).visible();
            }
            ArrayAdapter adapter = new ArrayAdapter(this, R.layout.simple_spinner_item,
                    getResources().getStringArray(R.array.usertypedisp));//03Sep2020 Arpitha
            aqLogin.id(R.id.spnusertype).adapter(adapter);//03Sep2020 Arpitha
        } else {
            aqLogin.id(R.id.etashaid).getEditText().setSelection(aqLogin.id(R.id.etashaid).getText().length());
            aqLogin.id(R.id.etpassword).text("");
            aqLogin.id(R.id.llusertype).visible();
            aqLogin.id(R.id.spnusertype).setSelection(0);
            aqLogin.id(R.id.spnusertype).enabled(false);
            aqLogin.id(R.id.etpassword).getEditText().setFocusable(true);
            ArrayAdapter adapter = new ArrayAdapter(this, R.layout.simple_spinner_item,
                    getResources().getStringArray(R.array.usertypedispfirsttime));
            aqLogin.id(R.id.spnusertype).adapter(adapter);
        }
    }

    //    set data before login // TODO appState.institutionDatabaseName must be set from tblinstusers~
    private void setDataBeforeLogin() throws Exception {
        InstitutionRepository institutionRepository = new InstitutionRepository(databaseHelper);
        UserRepository userRepository = new UserRepository(databaseHelper);
        TblInstusers u = userRepository.getUserWithInstitutionDatabaseName(loginData.ashaId);

        TblInstitutionDetails institution = institutionRepository.getOneInstitution();
        appState.institutionDatabaseName = u.getInst_Database();
        appState.institutionIpAddress = institution.getIpAddress();

        TblLoginAudit loginAudit = new TblLoginAudit();
        loginAudit.setLogintime(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
        loginAudit.setUsertype(aqLogin.id(R.id.spnusertype).getSelectedItem().toString());
        //Mani 12feb2021 setting data to two new columns
        loginAudit.setLatitude(varLatitude);
        loginAudit.setLongitude(varLongitude);
        // if (aqLogin.id(R.id.spnusertype).getSelectedItemPosition() >= 1)
        if (aqLogin.id(R.id.spnusertype).getSelectedItem().toString().equalsIgnoreCase("ASHA") || aqLogin.id(R.id.spnusertype).getSelectedItem().toString().equalsIgnoreCase("MM")) // 27Apr2021 Bindu - check for string
            loginAudit.setUserId(aqLogin.id(R.id.etashaid).getText().toString());
        else
            loginAudit.setUserId(aqLogin.id(R.id.etanmaid).getText().toString());
        loginAudit.setCreateddate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
        LoginRepository loginRepository = new LoginRepository(this.databaseHelper);
        TransactionHeaderRepository transactionHeaderRepository = new TransactionHeaderRepository(databaseHelper);
        int transactionId = transactionHeaderRepository.iCreateNewTrans(user.getUserId(), databaseHelper); //26Apr2020 Bindu Add transaction
        loginAudit.setTransId(transactionId); //26Apr2021 Bindu set transid and create record
        appState.loginAuditId = loginRepository.updateLoginAudit(loginAudit);
        boolean added = false;
        if (appState.loginAuditId > 0) {
            RuntimeExceptionDao<TblLoginAudit, Integer> loginDAO = databaseHelper.getLoginAuditRuntimeExceptionDao();
            added = transactionHeaderRepository.iNewRecordTransNew(user.getUserId(), transactionId, loginDAO.getTableName(), databaseHelper);
        }

        if (user != null && !(user.getDevicePhnNumber() != null && user.getDevicePhnNumber().equalsIgnoreCase(aqLogin.id(R.id.etphn).getText().toString()))) { //15May2021 Bindu - check for dev num not null
            userRepository.updateTabletPhoneNumber(user.getAshaId(), aqLogin.id(R.id.etphn).getText().toString());

            //  TransactionHeaderRepository transactionHeaderRepository = new TransactionHeaderRepository(databaseHelper);
            //  int transactionId = transactionHeaderRepository.iCreateNewTrans(user.getUserId(),databaseHelper);
            String updateStatement = userRepository.getUpdateStatementForUserPhoneNumber(user);
            transactionHeaderRepository.update(user.getUserId(), transactionId,

                    updateStatement.trim(), new Object[]{aqLogin.id(R.id.etphn).getText().toString(),
                            user.getUserId()});
        }
    }

    /**
     * Method to Create shortcut for the application on the First Run..
     */
    private void addShortcut() {
        // SharedPreferences prefs = getSharedPreferences("matruksha.prefs", 0);
        // SharedPreferences.Editor prefEditor = prefs.edit();
        prefs = getSharedPreferences("matruksha.prefs", 0);
        prefEditor = prefs.edit();

        // Adding shortcut for MainActivity
        // on Home screen
        boolean isAppInstalled = prefs.getBoolean("isAppInstalled", false);
        String appName = prefs.getString("appName", getResources().getString(R.string.app_name)); //26Sep2019-Bindu - app name from resource

        if (!isAppInstalled) {
            Intent shortcutIntent = new Intent(getApplicationContext(), SplashPermissionsActivity.class);

            shortcutIntent.setAction(Intent.ACTION_MAIN);

            Intent addIntent = new Intent();
            addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
            addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, getResources().getString(R.string.app_name)); //26Sep2019 - bindu - app name from strings
            addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
                    Intent.ShortcutIconResource.fromContext(getApplicationContext(), R.mipmap.ic_sentiatendasha));

            addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
            getApplicationContext().sendBroadcast(addIntent);

            /** Make preference true */
            prefEditor.putBoolean("isAppInstalled", true);
            prefEditor.putString("appName", getResources().getString(R.string.app_name));  //26Sep2019 - Bindu - app name from strings
            prefEditor.apply();
        } else if (!appName.equalsIgnoreCase(getResources().getString(R.string.app_name))) {
            Intent shortcutIntent = new Intent(getApplicationContext(), SplashPermissionsActivity.class);
            shortcutIntent.setAction(Intent.ACTION_MAIN);
            Intent addIntent = new Intent();
            addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
            addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, getResources().getString(R.string.app_name)); //26Sep2019 - Bindu - app name from strings
            addIntent.setAction("com.android.launcher.action.UNINSTALL_SHORTCUT");
            getApplicationContext().sendBroadcast(addIntent);

            /*addIntent = new Intent();
            addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
            addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, "Sentiatend");
            addIntent.setAction("com.android.launcher.action.UNINSTALL_SHORTCUT");
            getApplicationContext().sendBroadcast(addIntent);*/ //26Sep2019 - Bindu - comment the repeated code

            Intent shortcutIntent2 = new Intent(getApplicationContext(), SplashPermissionsActivity.class);
            shortcutIntent2.setAction(Intent.ACTION_MAIN);
            Intent addIntent2 = new Intent();
            addIntent2.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent2);
            addIntent2.putExtra(Intent.EXTRA_SHORTCUT_NAME, getResources().getString(R.string.app_name)); //26Sep2019 - Bindu - app name from strings
            addIntent2.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, Intent.ShortcutIconResource.fromContext(getApplicationContext(), R.mipmap.ic_sentiatend));
            addIntent2.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
            getApplicationContext().sendBroadcast(addIntent2);

            /** Make preference true */
            prefEditor.putBoolean("isAppInstalled", true);
            prefEditor.putString("appName", getResources().getString(R.string.app_name)); //26Sep2019 - Bindu -app name from strings
            prefEditor.apply();
        }
    }

    //	main menu title
    private String
            sp_dSigns,
            sp_allWList, sp_WReg, sp_pending, sp_ndWomen, sp_CWomans,
            sp_sync, sp_home, sp_LOut, sp_immn, sp_anc, sp_pnc, sp_notification;

    //	set names according to resouce
    public void setNamesAccordingToRes(Resources res) {
        try {
            Locale locale = null;
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

            if (prefs.getBoolean("isEnglish", false)) {
                locale = new Locale("en");
                font = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
            } else if (prefs.getBoolean("isKannada", false)) {  //10Nov2019 - Bindu - Add kannada
                locale = new Locale("kx");
                font = Typeface.createFromAsset(getAssets(), "Lohit-Kannada.ttf");
            } else if (prefs.getBoolean("isTelugu", false)) {  //02Apr2021 - Add Telugu
                locale = new Locale("te");
                font = Typeface.createFromAsset(getAssets(), "Lohit-Kannada.ttf");
            } else {
                locale = new Locale("en");
                font = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf"); // 04Feb2021 - Bindu - change default to english
            }

            Configuration config = res.getConfiguration();
            config.locale = locale;
            res.updateConfiguration(config, res.getDisplayMetrics());


            sp_allWList = (res.getString(R.string.sp_allWList));
            sp_WReg = (res.getString(R.string.sp_WReg));
            sp_dSigns = (res.getString(R.string.complications));
            sp_CWomans = (res.getString(R.string.hrp));
            sp_pending = (res.getString(R.string.sp_pending));
            sp_ndWomen = (res.getString(R.string.sp_dwoman));
            sp_sync = (res.getString(R.string.sp_sync));
            sp_home = res.getString(R.string.homevisit);
            sp_LOut = res.getString(R.string.regchildlist);//05Nov2019 Arpitha
            sp_immn = res.getString(R.string.sp_imm);//05Nov2019 Arpitha
            sp_anc = res.getString(R.string.anc);//13Nov2019 Arpitha
            sp_pnc = res.getString(R.string.pnc);//13Nov2019 Arpitha
            //22Nov2019 - Bindu
            sp_notification = res.getString(R.string.notification);

            // mapMenuItemAndMenuItemId();
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            e.printStackTrace();
        }
    }

    private void callComplMgmtSync() {

        int interval = 30;
        dangerSignTimer = new CountDownTimer(Long.MAX_VALUE, interval * START_AFTER_SECONDS) {
            public void onTick(long millisUntilFinished) {
                AsyncCallComplMgmt task1 = new AsyncCallComplMgmt(LoginActivity.this);
                //task1.execute();
                StartAsyncTaskInParallelDS(task1);
            }

            public void onFinish() {
                Log.d("test", "Timer last tick");
            }
        }.start();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void StartAsyncTaskInParallelDS(AsyncCallComplMgmt task1) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            task1.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            task1.execute();
    }

    public static class AsyncCallComplMgmt extends AsyncTask<String, Void, String> {
        private WeakReference<LoginActivity> loginActivityWeakReference;

        public AsyncCallComplMgmt(LoginActivity loginActivity) {
            this.loginActivityWeakReference = new WeakReference<>(loginActivity);
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... params) {
            String serverResult = null;
            try {
                LoginActivity loginActivity = loginActivityWeakReference.get();
                if (loginActivity != null && loginActivity.isFinishing()) return null;

                loginActivity.appState.internetOn = InternetConnectionUtil.isConnectionAvailable(loginActivity, loginActivity.appState);

                if (loginActivity.appState.internetOn) {
                    String hrpcomplmgmtdatelastupdated = "";
                    ComplicationMgmtRepository complrepo = new ComplicationMgmtRepository(databaseHelper);
                    hrpcomplmgmtdatelastupdated = complrepo.getDateLastUpdated(loginActivity.appState.sessionUserId);
                    serverResult = new DSSyncFunction(loginActivity.databaseHelper, loginActivity.appState, loginActivity.syncState, loginActivity.user)
                            .SyncFunctionCompl
                                    (loginActivity.dbMethods, loginActivity.user.getUserId(),
                                            loginActivity.appState.institutionDatabaseName,
                                            loginActivity.appState.webServiceIpAddress + loginActivity.appState.webServicePath,
                                            hrpcomplmgmtdatelastupdated);//30Sep2021 Arpitha - ip from properties
                }
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
            return serverResult;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                LoginActivity loginActivity = loginActivityWeakReference.get();
                if (loginActivity != null && loginActivity.isFinishing()) return;

                Toast.makeText(loginActivity, "Login sync cal", Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                e.printStackTrace();
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
        }
    }


    private String getTimeFromString(String duration) {
        // TODO Auto-generated method stub
        String time = "";
        boolean hourexists = false, minutesexists = false, secondsexists = false;
        if (duration.contains("H"))
            hourexists = true;
        if (duration.contains("M"))
            minutesexists = true;
        if (duration.contains("S"))
            secondsexists = true;
        if (hourexists) {
            String hour = "";
            hour = duration.substring(duration.indexOf("T") + 1,
                    duration.indexOf("H"));
            if (hour.length() == 1)
                hour = "0" + hour;
            time += hour + ":";
        }
        if (minutesexists) {
            String minutes = "";
            if (hourexists)
                minutes = duration.substring(duration.indexOf("H") + 1,
                        duration.indexOf("M"));
            else
                minutes = duration.substring(duration.indexOf("T") + 1,
                        duration.indexOf("M"));
            if (minutes.length() == 1)
                minutes = "0" + minutes;
            time += minutes + ":";
        } else {
            time += "00:";
        }

        return time;
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private class AsyncMySQLFetch extends AsyncTask<String, String, String> {

        ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this);

        @Override
        protected void onPreExecute() {
            progressDialog.setCancelable(false);
            progressDialog.setMessage("This might take a minute, please wait");
            progressDialog.create();
            progressDialog.show();
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... strings) {
            String ID = strings[0];
            OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .build();
            Map<String, String> payload = new HashMap<>();
            payload.put("ASHAID", ID);
            payload.put("masterDB", appState.webServiceDBbMster);
            try {
                SyncDetailsRepository syncDetailsRepo = new SyncDetailsRepository(databaseHelper);
                String lastsyncdate = syncDetailsRepo.getLastDate();
                payload.put("lastSyncDate", lastsyncdate);
            } catch (Exception e) {
                payload.put("lastSyncDate", "");
                if (e.getCause().toString().toLowerCase().contains("no such table")) {
                    createDatebase();
                }
            }

            final MediaType JSON
                    = MediaType.get("application/json; charset=utf-8");

            RequestBody requestBody = RequestBody.create(new Gson().toJson(payload), JSON);

            HttpUrl httpUrl2 = new HttpUrl.Builder()
                    .host(appState.webServiceIpAddress)
                    .port(appState.apiPort)
                    .scheme("http")
                    .addPathSegment("getMysqlData")
                    .build();
            Request request = new Request.Builder()
                    .url(httpUrl2)
                    .post(requestBody)
                    .build();
            try (Response r2 = okHttpClient.newCall(request).execute()) {
                String responseString2 = r2.body().string();
                Log.e("tablost", responseString2);
                if (responseString2.toLowerCase().contains("hikaripool")) {
                    //if hikari got timeout for connection
                    return "Could not Connect to Server, Please try again later";

                } else if (responseString2.toLowerCase().contains("no user found")) {
                    //no user found
                    return "No User found";

                } else if (responseString2.toLowerCase().contains("the user is not validated")) {
                    //validate is not done
                    return "The User is not Validated";

                } else if (responseString2.toLowerCase().contains("please contact sentia team")) {
                    //tab lost validation is 0
                    return "Tab Lost is not requested, Please contact the Sentia Team";

                } else {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    Map<String, ArrayList<ArrayList<String>>> map = new HashMap<>();
                    Type empMapType = new TypeToken<Map<String, ArrayList<ArrayList<String>>>>() {
                    }.getType();
                    map = gson.fromJson(responseString2, empMapType);
                    if (!insertDataintoDB(ID, map)) {
                        return "Something went wrong, Please try again later";
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                if (e.getMessage().toLowerCase().contains("failed to connect")) {
                    return "Could not Connect to Server, Please try again later ";
                }
            }
            return "";
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            if (aVoid.length() == 0) {
                initialValues();
                settablostDetails();
            } else {
                AlertDialogUtil.displayAlertMessage(aVoid, LoginActivity.this);
            }
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();
        }
    }

    private void createDatebase() {
        try {
            new SchemaCreator(LoginActivity.databaseHelper.getWritableDatabase()).createSchema(LoginActivity.this.getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Boolean insertDataintoDB(String Userid, Map<String, ArrayList<ArrayList<String>>> map) {
        try {
            ArrayList<String> keys = new ArrayList<>(map.keySet());
            SQLiteDatabase sqLiteDatabase;
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput("mysqltosqlite.sql",
                    Context.MODE_PRIVATE));
            for (int i = 0; i < keys.size(); i++) {
                String currentTableName = keys.get(i);
                ArrayList<ArrayList<String>> data = map.get(keys.get(i));
                if (data.size() > 1) {// to check data is in the table or not
                    ArrayList<String> columnNames = data.get(0);
                    StringBuilder colNames = new StringBuilder();
                    for (int colNameI = 0; colNameI < columnNames.size() - 1; colNameI++) {
                        colNames.append(columnNames.get(colNameI)).append(",");
                    }
                    colNames.append(columnNames.get(columnNames.size() - 1)); // to get columnnames from table with comma

                    String sql = "insert into " + currentTableName + " (";
                    String values = " ) values (";

                    for (int j = 1; j < data.size(); j++) {
                        ArrayList<String> columnValues = data.get(j);
                        StringBuilder colValues = new StringBuilder();
                        for (int k = 0; k < columnNames.size() - 1; k++) {
                            if (columnValues.get(k) == null) {
                                colValues.append(columnValues.get(k)).append(","); // to get columnvalues from table with comma
                            } else {
                                colValues.append(" '").append(columnValues.get(k)).append("' ").append(","); // to get columnvalues from table with comma
                            }
                        }
                        colValues.append(" '").append(columnValues.get(columnNames.size() - 1)).append("' ");
                        String sqlQuery = sql + colNames + values + colValues + " );";

                        outputStreamWriter.write(sqlQuery + " \n ");
                    }
                }
            }
            outputStreamWriter.close();

            String sqlfileresult = "";
            InputStream inputStream = openFileInput("mysqltosqlite.sql");
            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String temp = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((temp = bufferedReader.readLine()) != null) {
                    stringBuilder.append(temp);
//                    stringBuilder.append(" \n ");
                }

                inputStream.close();
                sqlfileresult = stringBuilder.toString();
                sqLiteDatabase = databaseHelper.getWritableDatabase();
                sqLiteDatabase.beginTransaction();
                String[] lines = sqlfileresult.split(";");

                for (String line : lines) {
                    line = line.trim();
                    if (line.length() > 0) {
                        sqLiteDatabase.execSQL(line);
                    }
                }
                sqLiteDatabase.setTransactionSuccessful();
                sqLiteDatabase.endTransaction();
            }
            SyncDetailsRepository syncDetailsRepo = new SyncDetailsRepository(databaseHelper);
            tblSyncDetails tblsyncdts = new tblSyncDetails();
            UserRepository userRepository = new UserRepository(databaseHelper);
            user = userRepository.getAuditedUsers();

            int transid = TransactionHeaderRepository.iCreateNewTransNew(user.getUserId(), databaseHelper);
            tblsyncdts.setUserId(user.getUserId());
            tblsyncdts.setLastSyncDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
            tblsyncdts.setTransId(transid);
            tblsyncdts.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
            tblsyncdts.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());

            syncDetailsRepo.insertDate(tblsyncdts);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    AlertDialogUtil.displayAlertMessage("Data retrieved Successfully", LoginActivity.this);
                }
            });
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean InserttblAuditTrail(String ColumnName, String Old_Value, String New_Value, int transId) throws Exception {

        AuditPojo APJ = new AuditPojo();
        APJ.setUserId(user.getUserId());
        APJ.setWomanId(user.getUserId());
        APJ.setTblName("tblVersionDetails");
        APJ.setDateChanged(DateTimeUtil.getTodaysDate());
        APJ.setColumnName(ColumnName);
        APJ.setOld_Value(Old_Value);
        APJ.setNew_Value(New_Value);
        APJ.setTransId(transId);
        APJ.setRecordcreateddate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());

        AuditRepository auditRepository = new AuditRepository(databaseHelper);
        return AuditRepository.insertTotblAuditTrailNew(APJ, databaseHelper);

    }

    //    22Sep2021 Arpitha
    void checkForUpdates() {

        GetVersionCode getVersionCode = new GetVersionCode();
        getVersionCode.execute();
    }


    private class GetVersionCode extends AsyncTask<Void, String, String> {
        @Override
        protected String doInBackground(Void... voids) {

            String newVersion = null;
            try {
                newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" +
                        LoginActivity.this.getPackageName() + "&hl=it")
                        //.timeout(10)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select(".hAyfc .htlgb")
                        .get(7)
                        .ownText();
                return newVersion;
            } catch (Exception e) {
                return newVersion;
            }
        }

        @Override
        protected void onPostExecute(String onlineVersion) {
            super.onPostExecute(onlineVersion);
            try {
                PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                String version = pInfo.versionName;
                Log.d("update", "Current version " + "0.5" + "playstore version " + onlineVersion);
                if (onlineVersion != null && !onlineVersion.isEmpty()) {
                    if (Float.valueOf(version) < Float.valueOf(onlineVersion)) {
//                    Toast.makeText(getApplicationContext(),"Upgrade",Toast.LENGTH_LONG).show();
                        displayAlert(getResources().getString(R.string.appupdate));

                    }
                }
            }catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }


    /**
     *  Confirmation Alert - 22Sep2021 Arpitha
     */
    private void displayAlert( String strMessage) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set dialog message
        alertDialogBuilder.setMessage(strMessage).setCancelable(false).setNegativeButton(
                getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                        String appPackageName = null;
                        try {
                            // isUpdateNow = true;
                            appPackageName = getPackageName();
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                    }
                }).setPositiveButton(getResources().getString(R.string.m122),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //isUpdateNow = true;
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }
}