//28Aug2019 Arpitha - code clean up
package com.sc.stmansi.registration;

import android.content.Context;
import android.widget.ArrayAdapter;

import java.util.List;

public class HintAdapter extends ArrayAdapter {

//    constructor
    public HintAdapter(Context context, List<String> objects, int theLayoutResId) {
        super(context, theLayoutResId, objects);
    }

    @Override
    public int getCount() {
        int count = 0;
        try {
            // don't display last item. It is used as hint.
            count = super.getCount();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return count > 0 ? count - 1 : count;
    }
}