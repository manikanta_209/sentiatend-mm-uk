//24Aug2019 Arpitha - removed unused/commented code
package com.sc.stmansi.registration;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.core.content.FileProvider;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.misc.TransactionManager;
import com.sc.stmansi.About.AboutActivity;
import com.sc.stmansi.common.CommonDatePickerFragment;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.delivery.DeliveryInfoActivity;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.repositories.AdolescentRepository;
import com.sc.stmansi.repositories.BankRepository;
import com.sc.stmansi.repositories.FacilityRepository;
import com.sc.stmansi.repositories.LoginRepository;
import com.sc.stmansi.repositories.SMSRepository;
import com.sc.stmansi.repositories.SettingsRepository;
import com.sc.stmansi.repositories.TransactionHeaderRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.repositories.WomanServiceRepository;
import com.sc.stmansi.sms.SendSMS;
import com.sc.stmansi.tables.MessageLogPojo;
import com.sc.stmansi.tables.TblContactDetails;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblAdolReg;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.womanlist.RegisteredWomenActionTabs;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import com.sc.stmansi.R;

import static com.sc.stmansi.sms.SendSMS.checkSimState;

public class RegistrationActivity extends AppCompatActivity implements
        View.OnClickListener, View.OnTouchListener, DatePickerDialog.OnDateSetListener
        , View.OnFocusChangeListener {

    private AQuery aqReg;
    private tblregisteredwomen woman;
    private int redCnt, amberCnt, heightUnit = 0, height, heightD, totPregs, transId,
            regCount, pregOrMother = 1, gestAge;
    private Double weight;
    private ArrayList<String> healthIssuesList, prevDelList, healthIssuesListStore, prevDelListStore;
    private String healthIssuesString, prevDelString, healthIssuesStringStore = "", religion,
            prevDelStringStore = "", uidType = "RCHID", villageName, eddDate, caste, aplBpl;
    private Map<String, Integer> mapVillage;
    private TblInstusers user;
    private File imgfileBeforeResult, imgfileAfterResult;
    private Uri outputFileUri;
    private String lastChildGender = "", path = null, placeOfReg;
    private int BloodGroup, villageCode;
    private EditText focusEditText = null, currentEditTextViewforDate = null;
    private static final int IMAGE_CAPTURE = 0;
    private boolean isdob;
    private boolean isThayiCard;
    private int height_numeric = 0;
    private int height_decimal = 0;
    boolean ismarriagedate = false;
    private String wImage = "";
    private DatabaseHelper databaseHelper;
    private AppState appState;
    private boolean homeReturnDate;
    private String category = "";
    //12Nov2019 - Bindu
    String statuswhilereg = "";
    //26Nov2019 Arpitha
    static EditText etphn;
    boolean isMessageLogsaved;
    Dialog dialogChangePassword;
    boolean messageSent;
    boolean lmp;
    String adolAdharNo = "";//01Sep2021 Arpitha
    tblAdolReg tblAdolReg;//16Sep2021 Arpitha

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            databaseHelper = getHelper();
            Bundle bundle = getIntent().getBundleExtra("globalState");
            appState = bundle.getParcelable("appState");

            UserRepository userRepository = new UserRepository(databaseHelper);
            user = userRepository.getOneAuditedUser(appState.ashaId);

            setContentView(R.layout.activity_registration);

            aqReg = new AQuery(this);

            woman = (tblregisteredwomen) getIntent().getSerializableExtra("woman");


            adolAdharNo = (String) getIntent().getSerializableExtra("adolAadhar");//   01Sep2021 Arpitha

            getSupportActionBar();
            getSupportActionBar().setTitle(getResources().getString(R.string.registrationHeading));

            initializeScreen(aqReg.id(R.id.rlRegDelMainLayout).getView());


            hideAllOptionalViews();
            initialView();
            populateSpinnerValues();

            setOnTextChangeListeners();
            setSpinnerItemListener();
            setFocusChangeListener();
            setOnTouchListener();

//            01Sep2021 Arpitha - Adol ANC reg
            if(adolAdharNo!=null && adolAdharNo.trim().length()>0) {
                 tblAdolReg = new AdolescentRepository(databaseHelper).getAdolDatafromAdarId(adolAdharNo);

                aqReg.id(R.id.etwomenname).text(tblAdolReg.getRegAdolName());

                aqReg.id(R.id.etage).text(tblAdolReg.getRegAdolAge());
                aqReg.id(R.id.etWomanWeight).text(tblAdolReg.getRegAdolWeight());
                aqReg.id(R.id.etaadharcard).text(adolAdharNo);

                if (tblAdolReg.getRegAdolAddress() != null &&
                        tblAdolReg.getRegAdolAddress().trim().length() > 0)
                    {
                        aqReg.id(R.id.etaddr).enabled(false);
                        aqReg.id(R.id.etaddr).text(tblAdolReg.getRegAdolAddress());
                    }
                    if (tblAdolReg.getRegAdolAreaname() != null &&
                            tblAdolReg.getRegAdolAreaname().trim().length() > 0)
                        {
                            aqReg.id(R.id.etareaname).enabled(false);
                            aqReg.id(R.id.etareaname).text(tblAdolReg.getRegAdolAreaname());
                        }
                        if (tblAdolReg.getRegAdolMobile() != null &&
                                tblAdolReg.getRegAdolMobile().trim().length() > 0)
                            {
                                aqReg.id(R.id.etphone).text(tblAdolReg.getRegAdolMobile());
                                aqReg.id(R.id.etphone).enabled(false);
                            }

                            if (tblAdolReg.getRegAdolDoB() != null
                                    && tblAdolReg.getRegAdolDoB().trim().length() > 0) {
                                aqReg.id(R.id.etdob).text(tblAdolReg.getRegAdolDoB());
                                aqReg.id(R.id.etdob).enabled(false);

                            }

                            aqReg.id(R.id.etwomenname).enabled(false);
                            aqReg.id(R.id.etage).enabled(false);
                            aqReg.id(R.id.etWomanWeight).enabled(false);
                            aqReg.id(R.id.etaadharcard).enabled(false);
                            aqReg.id(R.id.spnvillage).enabled(false);
//                        aqReg.id(R.id.etaddr).enabled(false);
//                        aqReg.id(R.id.etareaname).enabled(false);


                aqReg.id(R.id.rd_cm).enabled(false);
                aqReg.id(R.id.rd_feet).enabled(false);
                aqReg.id(R.id.rd_htdontknow).enabled(false);
                aqReg.id(R.id.etheight).enabled(false);
                aqReg.id(R.id.spnHeightNumeric).enabled(false);
                aqReg.id(R.id.spnHeightDecimal).enabled(false);

                         //   heightUnit = tblAdolReg.getRegAdolHeightType();
                            if (tblAdolReg.getRegAdolHeightType() == 1) {
                                aqReg.id(R.id.tr_heightcm).visible();
                                aqReg.id(R.id.rd_cm).checked(true);
                                aqReg.id(R.id.etheight).text(tblAdolReg.getRegAdolHeight());
                                heightUnit = 2;
                            } else if (tblAdolReg.getRegAdolHeightType() == 2) {
                                heightUnit = 1;
                                aqReg.id(R.id.trfeet).visible();
                                aqReg.id(R.id.rd_feet).checked(true);

                                String height = tblAdolReg.getRegAdolHeight();
                                int i = 0;
                                StringBuilder Feet = new StringBuilder();
                                for (i = 0; i < height.length(); i++) {
                                    char c = height.charAt(i);
                                    if (c == '.') {
                                        break;
                                    } else {
                                        Feet.append(c);
                                    }
                                }
                                int selectfeet = Integer.parseInt(Feet.toString()) - 1;
                                aqReg.id(R.id.spnHeightNumeric).getSpinner().setSelection(selectfeet);

                                i++;
                                StringBuilder inches = new StringBuilder();
                                for (; i < height.length(); i++) {
                                    char c = height.charAt(i);

                                    inches.append(c);

                                }
                                int inch = Integer.parseInt(inches.toString().trim());
                                aqReg.id(R.id.spnHeightDecimal).getSpinner().setSelection(inch);

                            } else if (tblAdolReg.getRegAdolHeightType() == 3) {
                                heightUnit = 3;
                                aqReg.id(R.id.rd_htdontknow).checked(true);
                            }

                            calculateBMI();
                            if (tblAdolReg.getRegAdolMobileOf() > 0 &&
                                    tblAdolReg.getRegAdolMobileOf() < 6) {
                                aqReg.id(R.id.spnphn).enabled(false);
                                if (tblAdolReg.getRegAdolMobileOf() == 1)
                                    aqReg.id(R.id.spnphn).setSelection(3);

                                else if (tblAdolReg.getRegAdolMobileOf() == 2)
                                    aqReg.id(R.id.spnphn).setSelection(3);

                                else if (tblAdolReg.getRegAdolMobileOf() == 3)
                                    aqReg.id(R.id.spnphn).setSelection(3);

                                else if (tblAdolReg.getRegAdolMobileOf() == 4)
                                    aqReg.id(R.id.spnphn).setSelection(1);

                                else if (tblAdolReg.getRegAdolMobileOf() == 5)
                                    aqReg.id(R.id.spnphn).setSelection(2);
                            }

                           int villageCode = getKey(mapVillage, "" + tblAdolReg.getRegAdolFacilities());
                            aqReg.id(R.id.spnvillage).setSelection(villageCode);

                  //  }
              //  }






                    //05Aug2019 - Bindu - Sharing file access for version N and above
                    StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                    StrictMode.setVmPolicy(builder.build());


            }

        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void onClick(View v) {

        try {

            switch (v.getId()) {
                case R.id.imgwomenphoto: {
                    CapturingImage();
                    break;
                }
                case R.id.radiomale1: {
                    lastChildGender = "Male";
                    break;
                }
                case R.id.radiofemale1: {
                    lastChildGender = "Female";
                    break;
                }
                case R.id.btnWBD: {
                    aqReg.id(R.id.btnsave).getImageView().setImageResource(R.drawable.ic_arrow_forward); //07Aug2019 - Bindu - change icon
                    aqReg.id(R.id.tvWBIHeading).visible();
                    hideAllOptionalViews();
                    openBasicInfoAndSetFocus();
                    aqReg.id(R.id.ettotlivebirth).getEditText().requestFocus();
                    ColorCount();
                    break;
                }
                case R.id.btnHStatus: {
                    try {
                        if (validateRegFields()) {
                            calculateBMI();
                            aqReg.id(R.id.btnsave).getImageView().setImageResource(R.drawable.ic_arrow_forward); //07Aug2019 - Bindu - change icon
                            hideAllOptionalViews();
                            aqReg.id(R.id.srlhealthhistory).visible();
                            aqReg.id(R.id.btnHStatus).getImageView().setBackgroundColor(getResources().getColor(R.color.gray));
                            ColorCount();
                            aqReg.id(R.id.tvWBIHeading).gone();
                        } else {
                            hideAllOptionalViews();
                            openBasicInfoAndSetFocus();
                        }
                    } catch (Exception e) {

                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }
                    break;
                }
                case R.id.btnSummary: {
                    try {
                        if (validateRegFields()) {
                            if (pregOrMother == 2)
                                aqReg.id(R.id.btnsave).getImageView().setImageResource(R.drawable.delivery_info_reg);
                            else
                                aqReg.id(R.id.btnsave).getImageView().setImageResource(R.drawable.save);
                            hideKeyboard(); //04Oct2019 - Bindu - hide keyboard
                            hideAllOptionalViews();
                            setRegistrationPojo();
                            aqReg.id(R.id.tvWBIHeading).gone();
                            aqReg.id(R.id.srlSummary).visible();
                            aqReg.id(R.id.btnSummary).getImageView().setBackgroundColor(getResources().getColor(R.color.gray));
                            /*if (aqReg.id(R.id.radiomother).isChecked() && aqReg.id(R.id.etadd).getText().length() > 0) {
                                {
                                    hideAllOptionalViews();
                                    aqReg.id(R.id.srlDelInfo).visible();
                                    aqReg.id(R.id.btnsave).getImageView().setImageResource(R.drawable.ic_arrow_forward); //07Aug2019 - Bindu - change icon
                                    aqReg.id(R.id.btnDelInfo).getImageView()
                                            .setBackgroundColor(getResources().getColor(R.color.gray));
                                    if (focusEditText != null)
                                        focusEditText.requestFocus();

                                }
                            }*/
                            ColorCount();
                        } else {
                            hideAllOptionalViews();
                            openBasicInfoAndSetFocus();
                        }
                    } catch (Exception e) {

                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }
                    break;
                }
                case R.id.btnWPAndPPD1: {
                    if (Integer.parseInt(aqReg.id(R.id.ettotpreg).getText().toString()) == 1
                            && aqReg.id(R.id.radiopreg).isChecked()) {
                        Toast.makeText(this, (getResources().getString(R.string.m065)), Toast.LENGTH_LONG).show();
                    } /*else if (aqReg.id(R.id.radiomother).isChecked()) {
                        Toast.makeText(this, (getResources().getString(R.string.m130)), Toast.LENGTH_LONG).show();
                    }*/ else {
                        try {
                            if (validateRegFields()) {
                                aqReg.id(R.id.btnsave).getImageView().setImageResource(R.drawable.ic_arrow_forward); //07Aug2019 - Bindu - change icon
                                hideAllOptionalViews();
                                aqReg.id(R.id.srlWomanPersonalAndPrevPregDetails).visible();
                                aqReg.id(R.id.tvWBIHeading).gone();
                                aqReg.id(R.id.btnWPAndPPD1).getImageView()
                                        .setBackgroundColor(getResources().getColor(R.color.gray));
                                aqReg.id(R.id.ettotlivebirth).getEditText().requestFocus();
                                ColorCount();
                            } else {
                                hideAllOptionalViews();
                                openBasicInfoAndSetFocus();
                            }
                        } catch (Exception e) {

                            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                            crashlytics.log(e.getMessage());
                        }
                    }
                    break;
                }


                case R.id.btnsave: {

                    if (aqReg.id(R.id.srlSummary).getView().getVisibility() == View.VISIBLE) {
                        try {
                            if (validateLmpADDDateSet()) {

                                if (pregOrMother == 2)
                                    aqReg.id(R.id.btnsave).getImageView().setImageResource(R.drawable.delivery_info_reg);
                                else
                                    aqReg.id(R.id.btnsave).getImageView().setImageResource(R.drawable.save);

                                if (aqReg.id(R.id.radiomother).isChecked()) {
                                    if (validateRegFields()) {
                                        calculateBMI();
                                        setRegistrationPojo();
                                        if (woman.getRegADDate().length() > 0) {
                                            calculateBMI();
//                                            confirmAlert();
                                            confimAlertMotherReg();
                                           /* hideAllOptionalViews();
                                            aqReg.id(R.id.srlDelInfo).visible();
                                            aqReg.id(R.id.btnDelInfo).getImageView()
                                                    .setBackgroundColor(getResources().getColor(R.color.gray));
                                            if (focusEditText != null)
                                                focusEditText.requestFocus();*/

                                        } else {
                                            if (validateRegFields()) {
                                                calculateBMI();
                                                confirmAlert();
                                            } else {
                                                hideAllOptionalViews();
                                                openBasicInfoAndSetFocus();
                                            }
                                        }
                                    } else {
                                        hideAllOptionalViews();
                                        openBasicInfoAndSetFocus();
                                    }
                                } else {
                                    if (validateRegFields()) {
                                        calculateBMI();
                                        confirmAlert();
                                    } else {
                                        hideAllOptionalViews();
                                        openBasicInfoAndSetFocus();
                                    }
                                }
                            } else {
                                hideAllOptionalViews();
                                openBasicInfoAndSetFocus();
                            }
                        } catch (Exception e) {

                            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                            crashlytics.log(e.getMessage());
                        }
                    } else if (aqReg.id(R.id.tvWBIHeading).getView().getVisibility() == View.VISIBLE) {

                        try {
                            if (validateRegFields()) {
                                calculateBMI();
                                hideAllOptionalViews();
                                aqReg.id(R.id.tvWBIHeading).gone();
                                aqReg.id(R.id.srlhealthhistory).visible();
                                aqReg.id(R.id.btnHStatus).getImageView().setBackgroundColor(getResources().getColor(R.color.gray));
                                ColorCount();
                            } else {
                                hideAllOptionalViews();
                                openBasicInfoAndSetFocus();
                            }
                        } catch (Exception e) {

                            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                            crashlytics.log(e.getMessage());
                        }


                    } else if (aqReg.id(R.id.srlhealthhistory).getView().getVisibility() == View.VISIBLE) {
                        if (Integer.parseInt(aqReg.id(R.id.ettotpreg).getText().toString()) == 1 && aqReg.id(R.id.radiopreg).isChecked()) {

                            try {
                                if (validateRegFields()) {

                                    calculateBMI();
                                    hideAllOptionalViews();
                                    setRegistrationPojo();
                                    aqReg.id(R.id.srlSummary).visible();
                                    aqReg.id(R.id.btnsave).getImageView().setImageResource(R.drawable.save);
                                    aqReg.id(R.id.btnSummary).getImageView()
                                            .setBackgroundColor(getResources().getColor(R.color.gray));
                                   /* if (aqReg.id(R.id.radiomother).isChecked() && aqReg.id(R.id.etadd).getText().length() > 0) {
                                        {
                                            hideAllOptionalViews();
                                            aqReg.id(R.id.srlDelInfo).visible();
                                            aqReg.id(R.id.btnDelInfo).getImageView()
                                                    .setBackgroundColor(getResources().getColor(R.color.gray));
                                            aqReg.id(R.id.btnsave).getImageView().setImageResource(R.drawable.ic_arrow_forward); //07Aug2019 - Bindu - change icon
                                            if (focusEditText != null)
                                                focusEditText.requestFocus();
                                        }
                                    }*/
                                    ColorCount();
                                } else {
                                    hideAllOptionalViews();
                                    openBasicInfoAndSetFocus();
                                }
                            } catch (Exception e) {

                                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                                crashlytics.log(e.getMessage());
                            }

                        }/* else if (aqReg.id(R.id.radiomother).isChecked()) {

                            try {
                                if (validateRegFields()) {
                                    calculateBMI();
                                    hideAllOptionalViews();
                                    setRegistrationPojo();

                                   *//* if (aqReg.id(R.id.radiomother).isChecked() && aqReg.id(R.id.etadd).getText().length() > 0) {
                                        hideAllOptionalViews();

                                        aqReg.id(R.id.srlDelInfo).visible();
                                        aqReg.id(R.id.btnDelInfo).getImageView()
                                                .setBackgroundColor(getResources().getColor(R.color.gray));
                                        aqReg.id(R.id.btnsave).getImageView().setImageResource(R.drawable.ic_arrow_forward); //07Aug2019 - Bindu - change icon
                                        if (focusEditText != null)
                                            focusEditText.requestFocus();

                                    }*//*
                                    ColorCount();
                                } else {
                                    hideAllOptionalViews();
                                    openBasicInfoAndSetFocus();
                                }
                            } catch (Exception e) {
                                
                                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                            }

                        }*/ else {
                            try {
                                if (validateRegFields()) {
                                    calculateBMI();
                                    hideAllOptionalViews();
                                    aqReg.id(R.id.srlWomanPersonalAndPrevPregDetails).visible();
                                    aqReg.id(R.id.btnWPAndPPD1).getImageView()
                                            .setBackgroundColor(getResources().getColor(R.color.gray));
                                    aqReg.id(R.id.ettotlivebirth).getEditText().requestFocus();
                                    ColorCount();
                                } else {
                                    hideAllOptionalViews();
                                    openBasicInfoAndSetFocus();
                                }
                            } catch (Exception e) {

                                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                                crashlytics.log(e.getMessage());
                            }
                        }

                    } else if (aqReg.id(R.id.srlWomanPersonalAndPrevPregDetails).getView().getVisibility() == View.VISIBLE) {

                        try {
                            if (validateRegFields()) {

                                if (pregOrMother == 2)
                                    aqReg.id(R.id.btnsave).getImageView().setImageResource(R.drawable.delivery_info_reg);
                                else
                                    aqReg.id(R.id.btnsave).getImageView().setImageResource(R.drawable.save);


                                calculateBMI();
                  /*if (aqReg.id(R.id.radiomother).isChecked() && aqReg.id(R.id.etadd).getText().length() > 0) {
                                    {
                                        hideAllOptionalViews();
                                        aqReg.id(R.id.srlDelInfo).visible();
                                        aqReg.id(R.id.btnDelInfo).getImageView()
                                                .setBackgroundColor(getResources().getColor(R.color.gray));
                                        aqReg.id(R.id.btnsave).getImageView().setImageResource(R.drawable.ic_arrow_forward); //07Aug2019 - Bindu - change icon
                                        if (focusEditText != null)
                                            focusEditText.requestFocus();
                                    }
                                }*/
                                hideAllOptionalViews();
                                setRegistrationPojo();
                                aqReg.id(R.id.srlSummary).visible();
//                                aqReg.id(R.id.btnsave).getImageView().setImageResource(R.drawable.save);
                                aqReg.id(R.id.btnSummary).getImageView().setBackgroundColor(getResources().getColor(R.color.gray));

                                ColorCount();
                            } else {
                                hideAllOptionalViews();
                                openBasicInfoAndSetFocus();
                            }
                        } catch (Exception e) {

                            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                            crashlytics.log(e.getMessage());
                        }

                    } /*else if (aqReg.id(R.id.srlDelInfo).getView().getVisibility() == View.VISIBLE) {

                        try {
                            if (validateRegFields()) {
                                calculateBMI();
                                hideAllOptionalViews();
                                setRegistrationPojo();
                                aqReg.id(R.id.srlSummary).visible();
                                aqReg.id(R.id.btnsave).getImageView().setImageResource(R.drawable.save);
                                aqReg.id(R.id.btnSummary).getImageView().setBackgroundColor(getResources().getColor(R.color.gray));
                               *//* if (aqReg.id(R.id.radiomother).isChecked() && aqReg.id(R.id.etadd).getText().length() > 0) {

                                }*//*
                                ColorCount();
                            } else {
                                hideAllOptionalViews();
                                openBasicInfoAndSetFocus();
                            }
                        } catch (Exception e) {
                            
                            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                        }

                    }*/
                    break;
                }
                case R.id.btnclear: {
                    confirmAlertExit();
                    break;
                }
                case R.id.rd_cm: {

                    heightUnit = 2;
                    aqReg.id(R.id.tr_heightcm).visible();

                    aqReg.id(R.id.etheight).getEditText().requestFocus();
                    aqReg.id(R.id.trfeet).gone();

                    calculateBMI(); //09Aug2019 - Bindu - Cal bmi
                    break;

                }
                case R.id.rd_feet: {

                    heightUnit = 1;

                    aqReg.id(R.id.tr_heightcm).gone();

                    aqReg.id(R.id.etheight).text("");
                    aqReg.id(R.id.trfeet).visible();

                    aqReg.id(R.id.spnHeightNumeric).setSelection(4);
                    aqReg.id(R.id.spnHeightDecimal).setSelection(0);
                    calculateBMI();
                    break;

                }
                case R.id.rd_htdontknow: {

                    heightUnit = 3;
                    aqReg.id(R.id.trfeet).gone();
                    aqReg.id(R.id.tr_heightcm).gone();

                    aqReg.id(R.id.etheight).text("");
                    calculateBMI();

                    break;

                }

                case R.id.chkAnemia:
                    ColorCount();
                    break;

                case R.id.chkdiabetes:
                    ColorCount();
                    break;
                case R.id.chkAsthama:
                    ColorCount();
                    break;
                case R.id.chkThyroid:
                    ColorCount();
                    break;
                case R.id.chkHeart:
                    ColorCount();
                    break;
                case R.id.chkMentalIllness:
                    ColorCount();
                    break;
                case R.id.chkTB:
                    ColorCount();
                    break;
                case R.id.chkprevioussurgery:
                    ColorCount();
                    break;
                case R.id.chktwins:
                    ColorCount();
                    break;
                case R.id.chkMentallyRetarded:
                    ColorCount();
                    break;

                case R.id.chkdeficiency:
                    ColorCount();
                    break;
                case R.id.chkdiabetesfamily:
                    ColorCount();
                    break;
                case R.id.chkmarriedinrelation:
                    ColorCount();
                    break;
                case R.id.chkHighBP:
                    ColorCount();
                    break;
                case R.id.chkHsFits:
                    ColorCount();
                    break;
                case R.id.chkbp:
                    ColorCount();
                    break;
                case R.id.rdbpl:
                    ColorCount();
                    break;
                case R.id.chkAnemiaAndBloodTransfusion:
                    ColorCount();
                    break;
                case R.id.chkChildDeathInPregnancy:
                    ColorCount();
                    break;

                case R.id.chkPrevDiabetes:
                    ColorCount();
                    break;
                /*case R.id.chkInsulinForDiabetes:
                    ColorCount();
                    break;*/ //01Oct2019 - Bindu - remove insulin

                case R.id.chkchildlessthan2500gms:
                    ColorCount();
                    break;
                case R.id.chkcesarian:
                    ColorCount();
                    break;
                case R.id.chkPPH:
                    ColorCount();
                    break;
                case R.id.chkInfantDeathBefore28days:
                    ColorCount();
                    break;
                case R.id.chkInfection:
                    ColorCount();
                    break;
                case R.id.chkMentalDisturnabce:
                    ColorCount();
                    break;
                case R.id.chkabortionbefore7months:
                    ColorCount();
                    break;
                /*case R.id.chkabortionbefore6months:
                    ColorCount();
                    break;*/ //01Oct2019 - Bindu - remove insulin
                case R.id.chkabortionlessthan3months:
                    ColorCount();
                    break;
                case R.id.rdhindu:
                    religion = "Hindu";
                    break;
                case R.id.rdmuslim:
                    religion = "Muslim";
                    break;
                case R.id.rdchristian:
                    religion = "Christian";
                    break;
                case R.id.rdothers:
                    religion = "Others";
                    break;
                case R.id.chklowweightinfant:
                    if (Integer.parseInt(aqReg.id(R.id.etweight1).getText().toString()) < 2500
                            || Integer.parseInt(aqReg.id(R.id.etweight2).getText().toString()) < 2500) {
                        Toast.makeText(getApplicationContext(),
                                getResources().getString(R.string.weight_less_please_change_weight), Toast.LENGTH_LONG)
                                .show();
                        aqReg.id(R.id.chklowweightinfant).checked(true);
                    }

                case R.id.rdrchid:
                    uidType = "RCHID";
                    aqReg.id(R.id.tvthaicard).text(getResources().getString(R.string.rchid)); //14Aug2019 - Bindu - set from resource
                    aqReg.id(R.id.llthayi).background(R.drawable.edittext_disable);
                    aqReg.id(R.id.etthaicardregdate).enabled(false);
                    aqReg.id(R.id.imgclearthayidate).gone();
                    aqReg.id(R.id.etthaicard).text("");
                    aqReg.id(R.id.etthaicardregdate).text("");

                    break;
                case R.id.rdthayicard:
                    uidType = "Thayi";
                    aqReg.id(R.id.etthaicardregdate).text("");
                    aqReg.id(R.id.llthayi).background(R.drawable.edittext_disable);
                    aqReg.id(R.id.etthaicardregdate).enabled(false);
                    aqReg.id(R.id.imgclearthayidate).gone();
                    aqReg.id(R.id.tvthaicard).text(getResources().getString(R.string.tvthaicard));
                    aqReg.id(R.id.etthaicard).text("");

                    break;
                case R.id.radiopreg:
                    pregOrMother = 1;
                    break;
                case R.id.radiomother:
                    pregOrMother = 2;
                    break;
//                    20Aug2019 Arpitha
                case R.id.imgcleardob:
                    aqReg.id(R.id.etdob).text("");
                    aqReg.id(R.id.imgcleardob).gone();
                    aqReg.id(R.id.etdob).getEditText().setError(null);
                    aqReg.id(R.id.txtdoberror).gone();
                    break;
                case R.id.imgclearmarriagedate:
                    aqReg.id(R.id.etmarriagrdate).text("");
                    aqReg.id(R.id.imgclearmarriagedate).gone();
                    break;
                case R.id.imgclearthayidate:
                    aqReg.id(R.id.etthaicardregdate).text("");
                    aqReg.id(R.id.imgclearthayidate).gone();
                    break;
                //20Aug2019 Arpitha
                case R.id.chkhiv:
                    ColorCount();
                    break;
                case R.id.chkillness:
                    ColorCount();
                    break;
                case R.id.chkrhneg:
                    ColorCount();
                    break;
                case R.id.chksevanemia:
                    ColorCount();
                    break;
                case R.id.chksyphilis:
                    ColorCount();
                    break;
                case R.id.chktwinmultiple:
                    ColorCount();
                    break;
                case R.id.chkfitsprevioushealth:
                    ColorCount();
                    break;
                case R.id.chkgreaterThan7monthsBleeding:
                    ColorCount();
                    break;
                case R.id.chkChildBirthbefore8months:
                    ColorCount();
                    break;
                case R.id.chkstillorneo:
                    ColorCount();
                    break;
                case R.id.chksponabor:
                    ColorCount();
                    break;
                case R.id.chkbirthweightgt:
                    ColorCount();
                    break;
                case R.id.chkhospitaladm:
                    ColorCount();
                    break;
                case R.id.chksurgeryrep:
                    ColorCount();
                    break;
                case R.id.chkcostlyinj:
                    ColorCount();
                    break;
                //26Sep2019 - Bindu -Add Radiooption - Tribal and non tribal
                case R.id.rdnontribal:
                case R.id.rdtribal:
                    ShowHideTribalDetails();
                    break;
                default:
                    break;
            }
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }

    }

    //    save registration data
    private void saveData() {
        try {
            RuntimeExceptionDao<tblregisteredwomen, Integer> regDAO = databaseHelper.gettblregRuntimeExceptionDao();
            setRegistrationPojo();

//            regDAO.startThreadConnection();
            final TransactionHeaderRepository transactionHeaderRepository = new TransactionHeaderRepository(databaseHelper);
            transId = transactionHeaderRepository.iCreateNewTrans(user.getUserId(), databaseHelper);
            woman.setTransId(transId);

            final UserRepository userRepository = new UserRepository(databaseHelper);
            woman.setWomanId(userRepository.getNewWomanId(user));

            final WomanRepository womanRepository = new WomanRepository(databaseHelper);

            TransactionManager.callInTransaction(databaseHelper.getConnectionSource(), new Callable<Void>() {
                public Void call() throws Exception {//29Aug2019 Arpitha - transaction manager
                    int add;
                    boolean added;
                    String stradd = "";
                    RuntimeExceptionDao<tblregisteredwomen, Integer> regDAO = databaseHelper.gettblregRuntimeExceptionDao();
                    add = regDAO.create(woman);
                    if (add > 0) {
                        added = transactionHeaderRepository.iNewRecordTransNew(user.getUserId(), transId, regDAO.getTableName(), databaseHelper);
                        if (added)
                            stradd = userRepository.UpdateLastWomenNumberNew(user.getUserId(), transId, databaseHelper);
                        /*if (stradd.trim().length() > 0) {
                            WomanServiceRepository womanServiceRepository = new WomanServiceRepository(databaseHelper);
                            added = womanServiceRepository.addServicesNew(woman, transId, appState.userType, databaseHelper, null, null);// 24Oct2019 Arpitha
                        }*/ //23Mar2021 Bindu comment services
                        if (stradd.trim().length() > 0) {
                            if(woman.getRegPregnantorMother() == 2) { //  06Apr2021 Bindu if pregnant then write services
                                WomanServiceRepository womanServiceRepository = new WomanServiceRepository(databaseHelper);
                                added = womanServiceRepository.addServicesNew(woman, transId, appState.userType, databaseHelper, null, null);// 24Oct2019 Arpitha
                            }
                            if (added) {


                                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(RegistrationActivity.this);


                          //mani 15 april 2021 un-commented code
                         /*if (checkSimState() == TelephonyManager.SIM_STATE_READY &&
                                    prefs.getBoolean("sms", false)
                                    && (woman.getRegComplicatedpreg() == 1 ||
                                    DateTimeUtil.getNumberOfDaysFromLMP(woman.getRegLMP()) > 210)
                            )12May2021*/
                                //23MAy2021 Bindu - check for preg only
                                if (checkSimState() == TelephonyManager.SIM_STATE_READY &&
                                        prefs.getBoolean("sms", false)
                                        && (woman.getRegComplicatedpreg() == 1 && woman.getRegPregnantorMother() == 1))
                                {
                                sendSMS(transactionHeaderRepository, databaseHelper);//27Nov2019 Arpitha
                            } else {
                                if (woman.getRegPregnantorMother() == 2) {

                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.regsuccess), Toast.LENGTH_LONG).show(); //Bindu - change msg
                                    Intent intent = new Intent(RegistrationActivity.this, DeliveryInfoActivity.class);
                                    intent.putExtra("appState", appState);
                                    intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                    intent.putExtra("wFilterStr", "");
                                    startActivity(intent);
                                } else {
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.regsuccess), Toast.LENGTH_LONG).show(); //Bindu change static msg
                                    Intent intent = new Intent(RegistrationActivity.this, RegisteredWomenActionTabs.class);
                                    intent.putExtra("appState", appState);
                                    intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                    intent.putExtra("wFilterStr", "");
                                    startActivity(intent);


                                }
                                }
                                //MainMenuActivity.callSyncMtd();  // TODO 12Aug2019 - Bindu - Comment auto sync
                            }
                        }else
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.m091), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha

                    }
                    return null;
                }
            });
        } catch (Exception e) {

            Toast.makeText(getApplicationContext(), getResources().getString(R.string.m091), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }


    //    set data to pojo class
    private void setRegistrationPojo() {

        try {
            ColorCount();
            woman = new tblregisteredwomen();

            woman.setRegWomenImage(wImage);
            woman.setUserId(user.getUserId());
            woman.setRegUserType(appState.userType);


            woman.setRegUIDNo(aqReg.id(R.id.etthaicard).getText().toString());
            woman.setRegUIDType(uidType);
            woman.setRegUIDDate(aqReg.id(R.id.etthaicardregdate).getText().toString());
            woman.setRegRegistrationDate(aqReg.id(R.id.etregdate).getText().toString());
            woman.setRegpregormotheratreg(pregOrMother);
            woman.setRegHusbandName(aqReg.id(R.id.ethusbname).getText().toString());
            woman.setRegDateofBirth(aqReg.id(R.id.etdob).getText().toString());
            woman.setRegAge(aqReg.id(R.id.etage).getText().toString().length() > 0 ?
                    Integer.parseInt(aqReg.id(R.id.etage).getText().toString()) : 0);
            woman.setRegReligion(religion);
            woman.setRegPhoneNumber(aqReg.id(R.id.etphone).getText().toString());
            if(aqReg.id(R.id.etlmp).getText().toString()!=null && aqReg.id(R.id.etlmp).getText().toString().trim().length()>0)

                woman.setRegLMP(aqReg.id(R.id.etlmp).getText().toString());
            else
                woman.setRegLMP(aqReg.id(R.id.etlmpmother).getText().toString());

            if(aqReg.id(R.id.etlmp).getText().toString()!=null && aqReg.id(R.id.etlmp).getText().toString().trim().length()>0)
                woman.setRegEDD(aqReg.id(R.id.tvEddValue).getText().toString());
            else
                woman.setRegEDD(aqReg.id(R.id.eteddmother).getText().toString());

            //  woman.setRegStatusWhileRegistration(aqReg.id(R.id.tvpregstatus).getText().toString());
            woman.setRegStatusWhileRegistration(statuswhilereg); //12Nov2019 - Bindu
            if (pregOrMother == 2)
                woman.setRegADDate(aqReg.id(R.id.tvEddValue).getText().toString());//24Oct2019 Arpitha
            woman.setRegHusbandAge(aqReg.id(R.id.ethusage).getText().toString());
            woman.setRegHusAgeatMarriage(aqReg.id(R.id.ethusageMarriage).getText().toString());
            woman.setRegWifeAgeatMarriage(aqReg.id(R.id.etwifeageMarriage).getText().toString());
            woman.setRegMarriageDate(aqReg.id(R.id.etmarriagrdate).getText().toString());
            woman.setRegEducationHus(aqReg.id(R.id.ethusEducation).getText().toString());
            woman.setRegEducationWife(aqReg.id(R.id.etwifeEducation).getText().toString());
            woman.setRegheightUnit(heightUnit);


            if (heightUnit == 1) {
                String heightval;
                height_numeric = aqReg.id(R.id.spnHeightNumeric).getSelectedItemPosition() + 1;
                height_decimal = aqReg.id(R.id.spnHeightDecimal).getSelectedItemPosition();
                heightval = "" + ((height_numeric) + "." + height_decimal);
                woman.setRegHeight(heightval);

            } else
                woman.setRegHeight(aqReg.id(R.id.etheight).getText().toString());
            woman.setRegWomanWeight(aqReg.id(R.id.etWomanWeight).getText().toString());
            woman.setRegBloodGroup(aqReg.id(R.id.spnbloodgroup).getSelectedItemPosition());
            woman.setRegGravida(aqReg.id(R.id.ettotpreg).getText().toString().length() > 0 ?
                    Integer.parseInt(aqReg.id(R.id.ettotpreg).getText().toString()) : 0);
            woman.setRegPara(aqReg.id(R.id.ettotlivebirth).getText().toString().length() > 0 ?
                    Integer.parseInt(aqReg.id(R.id.ettotlivebirth).getText().toString()) : 0);
            woman.setRegLiveChildren(aqReg.id(R.id.etnooflivechildren).getText()
                    .toString().length() > 0 ?
                    Integer.parseInt(aqReg.id(R.id.etnooflivechildren).getText().toString()) : 0);

            woman.setRegAbortions(aqReg.id(R.id.ettotabort).getText().toString().length() > 0 ?
                    Integer.parseInt(aqReg.id(R.id.ettotabort).getText().toString()) : 0);
            woman.setRegChildMortPreg(aqReg.id(R.id.etDeadDuringPreg).getText().toString().length() > 0 ?
                    Integer.parseInt(aqReg.id(R.id.etDeadDuringPreg).getText().toString()) : 0);
            woman.setRegChildMortDel(aqReg.id(R.id.etDeadAfterDel).getText().toString().length() > 0 ?
                    Integer.parseInt(aqReg.id(R.id.etDeadAfterDel).getText().toString()) : 0);
            woman.setRegNoofHomeDeliveries(aqReg.id(R.id.ethomedel).getText().toString().length() > 0 ?
                    Integer.parseInt(aqReg.id(R.id.ethomedel).getText().toString()) : 0);
            woman.setRegNoofStillBirth(aqReg.id(R.id.etstillbirth).getText().toString().length() > 0 ?
                    Integer.parseInt(aqReg.id(R.id.etstillbirth).getText().toString()) : 0);//13May2021 Arpitha
            woman.setRegLastChildAge(aqReg.id(R.id.etlastchildage).getText().toString()); //05Aug2019 - Bindu - int to String
            woman.setRegLastChildWeight(aqReg.id(R.id.etlastchildWeight).getText().toString().length() > 0 ?
                    Integer.parseInt(aqReg.id(R.id.etlastchildWeight).getText().toString()) : 0);
            woman.setRegAadharCard(aqReg.id(R.id.etaadharcard).getText().toString());
            woman.setRegWomanName(aqReg.id(R.id.etwomenname).getText().toString());
            woman.setRegPlaceOfReg(aqReg.id(R.id.spnPlaceOfReg).getSelectedItem().toString());
            woman.setRegAddress(aqReg.id(R.id.etaddr).getText().toString());


            woman.setRegLastChildGender(lastChildGender);
            woman.setRegPregnantorMother(pregOrMother);

            /*if (aqReg.id(R.id.rdsc).isChecked())
                caste = "SC";
            else if (aqReg.id(R.id.rdst).isChecked())
                caste = "ST";
            else if (aqReg.id(R.id.rdgeneral).isChecked())
                caste = "General";
            else if (aqReg.id(R.id.rdothercaste).isChecked())
                caste = "Other";*/
            //26Sep2019 - Bindu - Add caste
            /*if (aqReg.id(R.id.rdjenukuruba).isChecked())
                caste = getResources().getString(R.string.strjenukurubasave);  //12Nov2019 - Bindu - add res for save separately irrespective of lang for all tribal caste
            else if (aqReg.id(R.id.rdbettakuruba).isChecked())
                caste = getResources().getString(R.string.strbettakurubasave);
            else if (aqReg.id(R.id.rdsoliga).isChecked())
                caste = getResources().getString(R.string.strsoligasave);
            else if (aqReg.id(R.id.rdyarawa).isChecked())
                caste = getResources().getString(R.string.stryarawasave);
            else if (aqReg.id(R.id.rdphaniya).isChecked())
                caste = getResources().getString(R.string.strphaniyasave);
            else
                caste = "";*/

            //08Apr2021 Bindu - Change tribal caste to spinner
            caste = aqReg.id(R.id.spntribalcaste).getSelectedItem().toString();
            if (aqReg.id(R.id.spntribalcaste).getSpinner().getSelectedItemPosition()!=0){
                woman.setRegCaste(caste);
            }else {
                woman.setRegCaste("");
            }


            if (aqReg.id(R.id.rdapl).isChecked())
                // aplbpl = "APL"; //12Nov2019 - Bindu - add res for save separately irrespective of lang for apl/bpl
                aplBpl = getResources().getString(R.string.aplsave);
            else if (aqReg.id(R.id.rdbpl).isChecked())
                //aplbpl = "BPL"; //12Nov2019 - Bindu - add res for save separately irrespective of lang for  apl/bpl
                aplBpl = getResources().getString(R.string.bplsave);
            woman.setRegAPLBPL(aplBpl);

            if (aqReg.id(R.id.spnbankname).getSpinner().getSelectedItem().toString().equals("Select")){
                woman.setRegBankName("");
            }else {
                woman.setRegBankName("" + aqReg.id(R.id.spnbankname).getSelectedItem());
            }
            woman.setRegBranchName(aqReg.id(R.id.etbranchname).getText().toString());
            woman.setRegAccountNo(aqReg.id(R.id.etbankaccount).getText().toString());
            woman.setRegIFSCCode(aqReg.id(R.id.etifsc).getText().toString());

            //12Nov2019 - Bindu - Mobile No of from english array
            String[] phn = getResources().getStringArray(R.array.phnnosave);
            String mobof = "";
            if (aqReg.id(R.id.spnphn).getSelectedItemPosition()!=0){
                mobof = phn[aqReg.id(R.id.spnphn).getSelectedItemPosition()];
            }
            woman.setRegWhoseMobileNo(mobof);
            // woman.setRegWhoseMobileNo(aqReg.id(R.id.spnphn).getSelectedItem().toString());
            woman.setRegComments(aqReg.id(R.id.etcomments).getText().toString());

            /*woman.setRegFacility(TblInstusers.getFacilityName());
            woman.setRegFacilityType(TblInstusers.getFacilityType());*/

            woman.setRegFacility(""); //15Nov2019 - Bindu - remove the user fac type and fac name
            woman.setRegFacilityType("");


            woman.setRegState(user.getState());
            woman.setRegDistrict(user.getDistrict());
            woman.setRegSubDistrict(user.getSubDistrict());
            woman.setRegAreaName(aqReg.id(R.id.etareaname).getText().toString());
            woman.setRegPincode(aqReg.id(R.id.etpincode)
                    .getText().toString());
            woman.setRegFinancialYear(DateTimeUtil.getFinYear());
            woman.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
            woman.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());


            woman.setRegRationCard(aqReg.id(R.id.etrationcard).getText().toString());
            woman.setRegVoterId(aqReg.id(R.id.etvoterid).getText().toString());
            woman.setRegVillage(villageCode);
            woman.setRegCurrHealthRiskFactors(healthIssuesStringStore);
            woman.setRegPrevHealthRiskFactors(prevDelStringStore);

            String familyHistoryRisk = "";

            if (aqReg.id(R.id.chktwins).isChecked())
                familyHistoryRisk = familyHistoryRisk + "Twins";
            if (aqReg.id(R.id.chkMentallyRetarded).isChecked())
                familyHistoryRisk = familyHistoryRisk + ",Mental Retardation";
            if (aqReg.id(R.id.chkdeficiency).isChecked())
                familyHistoryRisk = familyHistoryRisk + ",Physical Deformation";
            if (aqReg.id(R.id.chkdiabetesfamily).isChecked())
                familyHistoryRisk = familyHistoryRisk + ",Diabetes";
            if (aqReg.id(R.id.chkmarriedinrelation).isChecked())
                familyHistoryRisk = familyHistoryRisk + ",Married in Relation";
            if (aqReg.id(R.id.chkBPFamily).isChecked())
                familyHistoryRisk = familyHistoryRisk + ",High BP";

            familyHistoryRisk = familyHistoryRisk.replace(",,", "");

            if (aqReg.id(R.id.etothersfh).getText().toString().trim().length() > 0)
                familyHistoryRisk = familyHistoryRisk + "," + aqReg.id(R.id.etothersfh).getText().toString();
            else
                familyHistoryRisk = familyHistoryRisk;

            woman.setRegFamilyHistoryRiskFactors(familyHistoryRisk);

            woman.setRegOtherhealthissue(aqReg.id(R.id.etothershs).getText().toString());
            woman.setRegOtherfamilyhistory(aqReg.id(R.id.etothersfh).getText().toString());
            woman.setRegOthersprevpreg(aqReg.id(R.id.etothersprev).getText().toString());

            if (redCnt > 3)
                woman.setRegAmberOrRedColorCode(4);
            else if (redCnt > 2)
                woman.setRegAmberOrRedColorCode(3);
            else if (redCnt > 1)
                woman.setRegAmberOrRedColorCode(2);
            else if (redCnt == 1)
                woman.setRegAmberOrRedColorCode(1);
            else if (amberCnt == 2)
                woman.setRegAmberOrRedColorCode(11);
            else if (amberCnt == 1 && redCnt == 0)
                woman.setRegAmberOrRedColorCode(9);
            else
                woman.setRegAmberOrRedColorCode(0);

            int complpreg = 0;
//            int prematureBirth = 0;

            if (aqReg.id(R.id.chkcomplicatedpreg).isChecked())
                complpreg = 1;
            else
                complpreg = 0;


            woman.setRegComplicatedpreg(complpreg);

            if (healthIssuesStringStore.length() > 0 && prevDelStringStore.length() > 0)
                woman.setRegriskFactors(healthIssuesStringStore + "\n" + prevDelStringStore);
            else if (healthIssuesStringStore.length() > 0 && prevDelStringStore.length() <= 0)
                woman.setRegriskFactors(healthIssuesStringStore);
            else if (healthIssuesStringStore.length() <= 0 && prevDelStringStore.length() > 0)
                woman.setRegriskFactors(prevDelStringStore);
                //10Aug2019 - bindu
            else
                woman.setRegriskFactors("");

            woman.setRegCHriskfactors(healthIssuesStringStore);
            woman.setRegPHriskfactors(prevDelStringStore);
            woman.setRegrecommendedPlaceOfDelivery(aqReg.id(R.id.txtReferal).getText().toString());
            woman.setRegComments(aqReg.id(R.id.etcomments).getText().toString());


            woman.setTransId(transId);


            if (aqReg.id(R.id.etthaicard).getText().toString().trim().length() > 0) {
                WomanRepository womanRepository = new WomanRepository(databaseHelper);
                regCount = womanRepository.getUID(aqReg.id(R.id.etthaicard).getText().toString())
                        + 1;
            } else
                regCount = 1;

            woman.setRegCountNo(regCount);
//13Sep2019 - Bindu - set Visit details
            woman.setIsCompl("");
            woman.setLastAncVisitDate("");
            woman.setLastPncVisitDate("");
            woman.setIsReferred("");

            //26Sep2019 - Bindu - Add Eligible couple and Category
            woman.setRegEligibleCoupleNo("" + aqReg.id(R.id.etecno).getText().toString());
            if (aqReg.id(R.id.rdnontribal).isChecked())
//                category = getResources().getString(R.string.strnontribalsave); //12Nov2019 - Bindu - from res save
            category = "Non-tribal"; //Ramesh 17-Aug-2021
            else if (aqReg.id(R.id.rdtribal).isChecked())
//                category = getResources().getString(R.string.strtribalsave); //12Nov2019 - Bindu - from res save
            category = "Tribal"; //Ramesh 17-Aug-2021
            woman.setRegCategory(category);

            //24Mar2021 Bindu
            woman.setIsLMPConfirmed(0);

//            02Sep2021 Arpitha
            if(adolAdharNo!= null && adolAdharNo.trim().length()>0) {
                woman.setRegIsAdolescent("Yes");
                woman.setRegPregnancyCount(new WomanRepository(databaseHelper).getPregnantCount(adolAdharNo));
            }
            else
                woman.setRegIsAdolescent("No");//02Sep2021 Arpitha

            woman.setRegAdolId(tblAdolReg.getAdolID());//16Sep2021 Arpitha

        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }

    }

    /**
     * This method calculate DangerSigns - Generate DangerSigns String Populate
     * summary tab
     */
    private void ColorCount() throws Exception {
        int age = 0;
        redCnt = 0;
        amberCnt = 0;
        healthIssuesList = new ArrayList<String>();
        prevDelList = new ArrayList<String>();

        healthIssuesString = "";
        prevDelString = "";

        healthIssuesStringStore = "";
        prevDelStringStore = "";
        healthIssuesListStore = new ArrayList<String>();
        prevDelListStore = new ArrayList<String>();

        if (heightUnit == 1) {
            height = Integer.parseInt(aqReg.id(R.id.spnHeightNumeric).getSelectedItem().toString());
            heightD = Integer.parseInt(aqReg.id(R.id.spnHeightDecimal).getSelectedItem().toString());
        } else if (heightUnit == 2 && aqReg.id(R.id.etheight).getText().toString().trim().length() > 0) {
            Double heightinfeet = 0.0328084 * (Double.parseDouble(aqReg.id(R.id.etheight).getText().toString()));
            height = Integer.parseInt(heightinfeet.toString().split("\\.")[0]);
            heightD = Integer.parseInt(heightinfeet.toString().split("\\.")[1].substring(0, 1));
        } else {
            height = 0;
            heightD = 0;
        }

        if (height > 0 || heightD > 0) {

            if (height < 5) {
                if (heightD < 8) {
                    redCnt = redCnt + 1;
                    healthIssuesList.add((getResources().getString(R.string.heightab2)));
                    healthIssuesListStore.add(("Short Stature"));

                } else {
                    amberCnt = amberCnt + 1;
                    healthIssuesList.add((getResources().getString(R.string.heightab2)));
                    healthIssuesListStore.add(("Short Stature"));

                }
            } else if (height < 4.8) {
                redCnt = redCnt + 1;
                healthIssuesList.add((getResources().getString(R.string.heightab1)));
                healthIssuesListStore.add(("Highly short Statured"));

            }
        }

        /*if (aqReg.id(R.id.etWomanWeight).getText().toString().length() > 0) {       //13Aug2019 - Bindu - add .tostring
            weight = Double.parseDouble(aqReg.id(R.id.etWomanWeight).getText().toString());
            if ((weight >= 75 && weight <= 80) || weight <= 35) {
                amberCnt = amberCnt + 1;
                healthIssuesList.add((getResources().getString(R.string.weightabno)));
                healthIssuesListStore.add(("Malnutrition"));
            } else if (weight > 80) {
                redCnt = redCnt + 1;
                healthIssuesList.add((getResources().getString(R.string.weightabnormal2)));
                healthIssuesListStore.add(("Overnutrition"));
            }
        }*/ //02Oct2019 - bindu - disable weight cal

        //02Oct2019 - Bindu - change the condition
        if (aqReg.id(R.id.tvBMI).getText().toString().trim().length() > 0) {
            if (Double.parseDouble(aqReg.id(R.id.tvBMI).getText().toString()) < 18.5) {
                redCnt = redCnt + 1;
                healthIssuesList.add((getResources().getString(R.string.undernutrition)));
                healthIssuesListStore.add((getResources().getString(R.string.strundernutrition)));
            } else if (Double.parseDouble(aqReg.id(R.id.tvBMI).getText().toString()) >= 23) {
                redCnt = redCnt + 1;
                healthIssuesList.add((getResources().getString(R.string.overnutrition)));
                healthIssuesListStore.add((getResources().getString(R.string.strovernutrition)));
            }
        }

        if (aqReg.id(R.id.etage).getText().length() > 0) {
            age = Integer.parseInt(aqReg.id(R.id.etage).getText().toString());
            if (age > 40) {
                redCnt = redCnt + 1;
                healthIssuesList.add((getResources().getString(R.string.moreage)));
                healthIssuesListStore.add(("Elderly Gravida"));

            } else if (age >= 35 && age <= 40) {
                amberCnt = amberCnt + 1;
                healthIssuesList.add((getResources().getString(R.string.moreage)));
                healthIssuesListStore.add(("Elderly Gravida"));

            } else if (age < 18 && age > 15) {
                amberCnt = amberCnt + 1;
                healthIssuesList.add((getResources().getString(R.string.lessage)));
                healthIssuesListStore.add(("Very Less Age"));

            } else if (age <= 15) {
                redCnt = redCnt + 1;
                healthIssuesList.add((getResources().getString(R.string.verylessage)));
                healthIssuesListStore.add(("Less Age"));

            }
        }

        int gravida = (aqReg.id(R.id.ettotpreg).getText().toString().length() > 0)
                ? Integer.parseInt(aqReg.id(R.id.ettotpreg).getText().toString()) : 0;
        if (gravida > 3) {
            amberCnt = amberCnt + 1;
            prevDelList.add(("multiple pregnancies(>3)"));
            prevDelListStore.add(("multiple pregnancies(>3)"));
        }


        if (aqReg.id(R.id.chkbp).isChecked()) {
            redCnt = redCnt + 1;
            healthIssuesList.add(getResources().getString(R.string.chkbp));
            healthIssuesListStore.add(("BP"));

        }
        if (aqReg.id(R.id.chkdiabetes).isChecked()) {
            redCnt = redCnt + 1;
            healthIssuesList.add(getResources().getString(R.string.gdm));
            healthIssuesListStore.add(("GDM"));

        }
        if (aqReg.id(R.id.chkAsthama).isChecked()) {
            amberCnt = amberCnt + 1;
            healthIssuesList.add(getResources().getString(R.string.chkAsthama));
            healthIssuesListStore.add(("Asthma"));

        }
        if (aqReg.id(R.id.chkThyroid).isChecked()) {
            amberCnt = amberCnt + 1;
            healthIssuesList.add(getResources().getString(R.string.chkThyroid));
            healthIssuesListStore.add(("Thyroid"));

        }
        if (aqReg.id(R.id.chkHeart).isChecked()) {
            redCnt = redCnt + 1;
            healthIssuesList.add(getResources().getString(R.string.chkHeart));
            healthIssuesListStore.add(("Heart Problems"));

        }
        if (aqReg.id(R.id.chkMentalIllness).isChecked()) {
            amberCnt = amberCnt + 1;
            healthIssuesList.add(getResources().getString(R.string.chkMentalIllness));
            healthIssuesListStore.add(("Mental Illness"));

        }
        if (aqReg.id(R.id.chkTB).isChecked()) {
            amberCnt = amberCnt + 1;
            healthIssuesList.add(getResources().getString(R.string.chkTB));
            healthIssuesListStore.add(("TB"));

        }


        if (aqReg.id(R.id.chkprevioussurgery).isChecked()) {
            amberCnt = amberCnt + 1;
            healthIssuesList.add(getResources().getString(R.string.chkprevioussurgery));
            healthIssuesListStore.add(("Surgeries"));

        }

        if (aqReg.id(R.id.chkAnemia).isChecked()) {
            amberCnt = amberCnt + 1;
            healthIssuesList.add(getResources().getString(R.string.chkAnemia));
            healthIssuesListStore.add(("Anemia"));

        }









        /*if (aqReg.id(R.id.chkillness).isChecked()) {
            amberCnt = amberCnt + 1;
            healthIssuesList.add(getResources().getString(R.string.patientwithhisofsysillness));
            healthIssuesListStore.add(("H/o systemic illness"));

        }*/ // 01Oct2019 - bindu - unchec illness
        if (aqReg.id(R.id.chktwinmultiple).isChecked()) {
            amberCnt = amberCnt + 1;
            healthIssuesList.add(getResources().getString(R.string.multiplepreg));
            healthIssuesListStore.add(("Multiple Pregnancy"));

        }
        if (aqReg.id(R.id.chksevanemia).isChecked()) {
            amberCnt = amberCnt + 1;
            healthIssuesList.add(getResources().getString(R.string.sevanemia));
            healthIssuesListStore.add(("Hb < 7"));

        }
        if (aqReg.id(R.id.chkhiv).isChecked()) {
            redCnt = redCnt + 1;            //11Aug2019 - Bindu - corrected to redcnt from regcount
            healthIssuesList.add(getResources().getString(R.string.hiv));
            healthIssuesListStore.add(("HIV"));

        }
        if (aqReg.id(R.id.chkrhneg).isChecked()) {
            redCnt = redCnt + 1;  //11Aug2019 - Bindu - corrected to redcnt from regcount
            healthIssuesList.add(getResources().getString(R.string.rhnegstatus));
            healthIssuesListStore.add(("Rh-ve status"));

        }
        if (aqReg.id(R.id.chksyphilis).isChecked()) {
            redCnt = redCnt + 1;
            healthIssuesList.add(getResources().getString(R.string.syphilis));
            healthIssuesListStore.add(("Syphilis"));

        }
        if (aqReg.id(R.id.etothershs).getText().toString().trim().length() > 0) {
            amberCnt = amberCnt + 1;
            healthIssuesList.add((aqReg.id(R.id.etothershs).getText().toString()));
            healthIssuesListStore.add((aqReg.id(R.id.etothershs).getText().toString()));
        }
        if (aqReg.id(R.id.chkHsFits).isChecked()) {
            amberCnt = amberCnt + 1;
            healthIssuesList.add(getResources().getString(R.string.convulsions));
            healthIssuesListStore.add(("Convulsions"));
        }

//01Oct2019 - Bindu - change the naming convention and order and set values from resources
        if (aqReg.id(R.id.chkAnemiaAndBloodTransfusion).isChecked()) {
            amberCnt = amberCnt + 1;
            prevDelList.add(getResources().getString(R.string.chkAnemiaAndBloodTransfusion));
            prevDelListStore.add((getResources().getString(R.string.stranemiaandbloodtransfusion)));
        }

        if (aqReg.id(R.id.chkfitsprevioushealth).isChecked()) {
            amberCnt = amberCnt + 1;
            prevDelList.add(getResources().getString(R.string.convulsions));
            prevDelListStore.add((getResources().getString(R.string.strconvulsions)));
        }
        if (aqReg.id(R.id.chkHighBP).isChecked()) {
            redCnt = redCnt + 1;
            prevDelList.add(getResources().getString(R.string.chkHighBP));
            prevDelListStore.add((getResources().getString(R.string.strhighbp)));
        }
        if (aqReg.id(R.id.chkhospitaladm).isChecked()) {
            amberCnt = amberCnt + 1;
            prevDelList.add(getResources().getString(R.string.hospitaladmission));
            prevDelListStore.add((getResources().getString(R.string.strhospitaladmission)));
        }
        if (aqReg.id(R.id.chkPrevDiabetes).isChecked()) {
            amberCnt = amberCnt + 1;
            prevDelList.add(getResources().getString(R.string.gdm));
            prevDelListStore.add((getResources().getString(R.string.strgdm)));
        }
        if (aqReg.id(R.id.chksurgeryrep).isChecked()) {
            amberCnt = amberCnt + 1;
            prevDelList.add(getResources().getString(R.string.surgeryreproductivetract));
            prevDelListStore.add((getResources().getString(R.string.strsurgeryreproductivetract)));
        }
        if (aqReg.id(R.id.chkMentalDisturnabce).isChecked()) {
            amberCnt = amberCnt + 1;
            prevDelList.add(getResources().getString(R.string.antepartum_depression));
            prevDelListStore.add((getResources().getString(R.string.strantepartum_depression)));
        }
        if (aqReg.id(R.id.chkabortionlessthan3months).isChecked()) {
            redCnt = redCnt + 1;
            prevDelList.add(getResources().getString(R.string.chkabortionlessthan3months));
            prevDelListStore.add((getResources().getString(R.string.strabortionlessthan3months)));
        }
        if (aqReg.id(R.id.chkabortionbefore7months).isChecked()) {
            amberCnt = amberCnt + 1;
            prevDelList.add(getResources().getString(R.string.abortionsbftwentyeightwks));
            prevDelListStore.add((getResources().getString(R.string.strabortionsbftwentyeightwks)));
        }
        if (aqReg.id(R.id.chksponabor).isChecked()) {
            amberCnt = amberCnt + 1;
            prevDelList.add(getResources().getString(R.string.spontabor));
            prevDelListStore.add((getResources().getString(R.string.strspontabor)));
        }
        if (aqReg.id(R.id.chkgreaterThan7monthsBleeding).isChecked()) {
            redCnt = redCnt + 1;
            prevDelList.add(getResources().getString(R.string.lowlyingplacenta)); //15Nov2019 - Bindu
            prevDelListStore.add((getResources().getString(R.string.strlowlyingplacenta)));
        }
        if (aqReg.id(R.id.chkChildDeathInPregnancy).isChecked()) {
            redCnt = redCnt + 1;
            prevDelList.add(getResources().getString(R.string.rdbIUD));
            prevDelListStore.add((getResources().getString(R.string.strIUD)));
        }
        if (aqReg.id(R.id.chkChildBirthbefore8months).isChecked()) {
            redCnt = redCnt + 1;
            prevDelList.add(getResources().getString(R.string.pretermdel));
            prevDelListStore.add((getResources().getString(R.string.strpretermdel)));
        }
        if (aqReg.id(R.id.chkcesarian).isChecked()) {
            redCnt = redCnt + 1;
            prevDelList.add(getResources().getString(R.string.chkcesarian));
            prevDelListStore.add((getResources().getString(R.string.strcesarian)));
        }
        if (aqReg.id(R.id.chkstillorneo).isChecked()) {
            amberCnt = amberCnt + 1;
            prevDelList.add(getResources().getString(R.string.stillorneo));
            prevDelListStore.add((getResources().getString(R.string.strstillorneo)));
        }
        if (aqReg.id(R.id.chkchildlessthan2500gms).isChecked()) {
            redCnt = redCnt + 1;
            prevDelList.add(getResources().getString(R.string.childlbw));
            prevDelListStore.add((getResources().getString(R.string.strchildlbw)));
        }
        if (aqReg.id(R.id.chkbirthweightgt).isChecked()) {
            amberCnt = amberCnt + 1;
            prevDelList.add(getResources().getString(R.string.birthweightgt4500)); //15Nov2019 - Bindu
            prevDelListStore.add((getResources().getString(R.string.strbirthweightgt)));// 04Aug2019 - Bindu - change from static
        }
        if (aqReg.id(R.id.chkPPH).isChecked()) {
            amberCnt = amberCnt + 1;
            prevDelList.add(getResources().getString(R.string.chkPPH));
            prevDelListStore.add((getResources().getString(R.string.strPPH)));
        }
        if (aqReg.id(R.id.chkInfection).isChecked()) {
            amberCnt = amberCnt + 1;
            prevDelList.add(getResources().getString(R.string.sepsis));
            prevDelListStore.add((getResources().getString(R.string.strsepsis)));
        }
        if (aqReg.id(R.id.chkcostlyinj).isChecked()) {
            amberCnt = amberCnt + 1;
            prevDelList.add(getResources().getString(R.string.isoimmunization_rhneg));
            prevDelListStore.add((getResources().getString(R.string.strisoimmunization_rhneg)));
        }
        if (aqReg.id(R.id.chkInfantDeathBefore28days).isChecked()) {
            redCnt = redCnt + 1;
            prevDelList.add(getResources().getString(R.string.chkInfantDeathBefore28days));
            prevDelListStore.add((getResources().getString(R.string.strInfantDeathBefore28days)));
        }

        /*if (aqReg.id(R.id.chkInsulinForDiabetes).isChecked()) {
            redCnt = redCnt + 1;
            prevDelList.add(getResources().getString(R.string.chkInsulinForDiabetes));
            prevDelListStore.add(("Insulin For Diabetes"));
        }

        if (aqReg.id(R.id.chkabortionbefore6months).isChecked()) {
            amberCnt = amberCnt + 1;
            prevDelList.add(getResources().getString(R.string.abortionbftwentyfourwks));
            prevDelListStore.add(("Abortion before 24 weeks"));
        }*/ //01Oct2019 - bindu - Remove the symptoms


        if (aqReg.id(R.id.etothersprev).getText().toString().trim().length() > 0) {
            amberCnt = amberCnt + 1;
            prevDelList.add((aqReg.id(R.id.etothersprev).getText().toString()));
            prevDelListStore.add((aqReg.id(R.id.etothersprev).getText().toString()));
        }

        //01Oct2019 - Bindu - change the images icon for stars
        if (amberCnt == 1 && redCnt == 0)
            aqReg.id(R.id.imgStar).getImageView().setImageResource(R.drawable.amberone);
        else if (amberCnt == 2)
            aqReg.id(R.id.imgStar).getImageView().setImageResource(R.drawable.ambertwo);
        else if (amberCnt > 2)
            redCnt = redCnt + 1;
        else
            aqReg.id(R.id.imgStar).getImageView().setImageResource(0);

        if (redCnt == 1)
            aqReg.id(R.id.imgStar).getImageView().setImageResource(R.drawable.redone);
        else if (redCnt == 2)
            aqReg.id(R.id.imgStar).getImageView().setImageResource(R.drawable.redtwo);
        else if (redCnt == 3)
            aqReg.id(R.id.imgStar).getImageView().setImageResource(R.drawable.redthree);
        else if (redCnt > 3)
            aqReg.id(R.id.imgStar).getImageView().setImageResource(R.drawable.redfour);

        for (int i = 0; i < healthIssuesList.size(); i++) {
            if (i == (healthIssuesList.size() - 1)) {
                healthIssuesString = healthIssuesString + healthIssuesList.get(i);
                healthIssuesStringStore = "CH: " + healthIssuesStringStore + healthIssuesListStore.get(i);

            } else {
                healthIssuesString = healthIssuesString + healthIssuesList.get(i) + ",";
                healthIssuesStringStore = healthIssuesStringStore + healthIssuesListStore.get(i) + ",";

            }
        }

        for (int i = 0; i < prevDelList.size(); i++) {
            if (i == (prevDelList.size() - 1)) {
                prevDelString = prevDelString + prevDelList.get(i);
                prevDelStringStore = "PH: " + prevDelStringStore + prevDelListStore.get(i);

            } else {
                prevDelString = prevDelString + prevDelList.get(i) + ",";
                prevDelStringStore = prevDelStringStore + prevDelListStore.get(i) + ",";

            }
        }

        String str = "";
        String spn = null;
        if (aqReg.id(R.id.radiopreg).isChecked()) {
            aqReg.id(R.id.txtLMPorADDL).text((getResources().getString(R.string.tvlmp)));
            str = aqReg.id(R.id.etlmp).getText().toString();
            aqReg.id(R.id.txtLMPorADD).text(str);
            aqReg.id(R.id.txtEDDL).text((getResources().getString(R.string.tvedd)));
            str = eddDate;
            aqReg.id(R.id.txtEDD).text(str);
        } else {
            aqReg.id(R.id.txtLMPorADDL).text((getResources().getString(R.string.tvadd)));
            str = aqReg.id(R.id.tvedd).getText().toString();
            aqReg.id(R.id.txtLMPorADD).text(woman.getRegADDate());
          /*  aqReg.id(R.id.txtEDDL).text((getResources().getString(R.string.tvNoofchildren)));
            str = "" + aqReg.id(R.id.spnnoofchild).getSelectedItem();
            aqReg.id(R.id.txtEDD).text(str);*/


        }

        spn = (aqReg.id(R.id.etwomenname).getText().toString() + "\t");
        aqReg.id(R.id.txtName).text(spn);
        spn = (aqReg.id(R.id.etage).getText().toString() + "\t");
        aqReg.id(R.id.tvWSAgeVal).text(spn);

        str = aqReg.id(R.id.etphone).getText().toString() + " ( " + aqReg.id(R.id.spnphn).getSelectedItem().toString() + " ) "; //04Aug2019 - Bindu Concat mob no of
        aqReg.id(R.id.txtPhNo).text(str);
        str = aqReg.id(R.id.ettotpreg).getText().toString();
        aqReg.id(R.id.txttotPregnancies).text(str);
        if (villageName != null) {
            spn = (villageName + "\t");
            aqReg.id(R.id.txtVillage).text(spn);
        }
        aqReg.id(R.id.txtAmberString).text((prevDelString));
        aqReg.id(R.id.txtRedString).text((healthIssuesString));


        aqReg.id(R.id.imgStar).visible();

        if (aqReg.id(R.id.chkcostlyinj).isChecked() ||
                aqReg.id(R.id.chkcostlyinj).isChecked() ||
                aqReg.id(R.id.chksurgeryrep).isChecked() ||
                aqReg.id(R.id.chkhospitaladm).isChecked() ||
                aqReg.id(R.id.chkbirthweightgt).isChecked() ||
                aqReg.id(R.id.chksponabor).isChecked() ||
                aqReg.id(R.id.chkstillorneo).isChecked() ||
                aqReg.id(R.id.chkMentalDisturnabce).isChecked() ||
                /*aqReg.id(R.id.chkInsulinForDiabetes).isChecked()||*/ //01Oct2019- Bindu - remove insulin
                aqReg.id(R.id.chkInfection).isChecked() ||
                aqReg.id(R.id.chkfitsprevioushealth).isChecked() ||
                aqReg.id(R.id.chkgreaterThan7monthsBleeding).isChecked() ||
                aqReg.id(R.id.chkPrevDiabetes).isChecked() ||
                aqReg.id(R.id.chkChildBirthbefore8months).isChecked() ||
                aqReg.id(R.id.chkchildlessthan2500gms).isChecked() ||
                aqReg.id(R.id.chkChildDeathInPregnancy).isChecked() ||
                aqReg.id(R.id.chkcesarian).isChecked() ||
                aqReg.id(R.id.chkAnemiaAndBloodTransfusion).isChecked() ||
                aqReg.id(R.id.chkabortionbefore7months).isChecked() ||
                /*aqReg.id(R.id.chkabortionbefore6months).isChecked()||
                aqReg.id(R.id.chkillness).isChecked()||*/ //01Oct2019 - bindu - remove illness
                aqReg.id(R.id.chkrhneg).isChecked() ||
                aqReg.id(R.id.chktwinmultiple).isChecked() ||
                aqReg.id(R.id.chksyphilis).isChecked() ||
                aqReg.id(R.id.chkhiv).isChecked() ||
                aqReg.id(R.id.chkThyroid).isChecked() ||
                aqReg.id(R.id.chkprevioussurgery).isChecked() ||
                aqReg.id(R.id.chkMentalIllness).isChecked() ||
                aqReg.id(R.id.chkHeart).isChecked() ||
                aqReg.id(R.id.chkHsFits).isChecked() ||
                aqReg.id(R.id.chkdiabetes).isChecked() ||
                aqReg.id(R.id.chkbp).isChecked() ||
                aqReg.id(R.id.chkAsthama).isChecked() ||
                aqReg.id(R.id.chksevanemia).isChecked() ||
                aqReg.id(R.id.chkPPH).isChecked() ||
                aqReg.id(R.id.etothersprev).getText().toString().trim().length() > 0 ||
                (age > 0 && (age > 40 || age <= 15 || (age >= 35 && age <= 40) || (age < 18 && age > 15))) ||
                (weight > 0 && ((weight >= 75 && weight <= 80) || weight <= 35 || weight > 80)) ||
                (height > 0 && height < 4.8)
        ) {
            aqReg.id(R.id.txtReferal).text(getResources().getString(R.string.strcemoc)); //11May2021 Bindu retrieve cemoc from strings
            aqReg.id(R.id.txtReferal).visible();
            aqReg.id(R.id.txtReferalL).visible();
        } else if ((aqReg.id(R.id.tvBMI).getText().toString().trim().length() > 0 &&
                Double.parseDouble(aqReg.id(R.id.tvBMI).getText().toString()) < 19) ||
                aqReg.id(R.id.chkTB).isChecked() ||
                aqReg.id(R.id.etothershs).getText().toString().trim().length() > 0) {
            aqReg.id(R.id.txtReferal).text(getResources().getString(R.string.strbemoc)); //11May2021 Bindu retrieve bemoc from strings
            aqReg.id(R.id.txtReferal).visible();
            aqReg.id(R.id.txtReferalL).visible();
        } else {
            aqReg.id(R.id.txtReferal).text("");
            aqReg.id(R.id.txtReferal).gone();
            aqReg.id(R.id.txtReferalL).gone();
        }

        if (aqReg.id(R.id.radiomother).isChecked()) {
            aqReg.id(R.id.txtReferal).text("");
            aqReg.id(R.id.txtReferal).gone();
            aqReg.id(R.id.txtReferalL).gone();
        }


        calCompPreg();
    }


    // calcute hrp
    private void calCompPreg() throws Exception {

        if (amberCnt > 0 || redCnt > 0 || totPregs > 3)
            aqReg.id(R.id.chkcomplicatedpreg).checked(true);
        else
            aqReg.id(R.id.chkcomplicatedpreg).checked(false);
    }

    /**
     * This method generates the initial data for screen
     */
    private void initialView() throws Exception {

        aqReg.id(R.id.btnWBD).getImageView()
                .setBackgroundColor(getResources().getColor(R.color.gray));
        aqReg.id(R.id.tvpregstatus).enabled(false);
//        aqReg.id(R.id.tvadd).gone();
//        aqReg.id(R.id.etadd).gone();
        aqReg.id(R.id.radiofemale1).checked(false);
        aqReg.id(R.id.radiomale1).checked(false);

        aqReg.id(R.id.spnHeightNumeric).setSelection(4);
        aqReg.id(R.id.spnHeightDecimal).setSelection(0);


        SimpleDateFormat sdfd = new SimpleDateFormat("dd-MM-yyyy");
        Date runningD = sdfd.parse(DateTimeUtil.getTodaysDate());
        int mon = Integer.parseInt(new SimpleDateFormat("M").format(runningD));

        aqReg.id(R.id.srlWomanBasicInfo).visible();
        aqReg.id(R.id.btnWBD).getImageView().setBackgroundColor(getResources().getColor(R.color.gray));


        aqReg.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());

        FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
        mapVillage = facilityRepository.getPlaceMap();
        ArrayList<String> villageList = new ArrayList<String>();
        villageList.add(getResources().getString(R.string.selectType));
        for (Map.Entry<String, Integer> village : mapVillage.entrySet())
            villageList.add(village.getKey());
        ArrayAdapter<String> VillageAdapter = new ArrayAdapter<>(this,
                R.layout.simple_spinner_dropdown_item, villageList);
        VillageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        aqReg.id(R.id.spnvillage).adapter(VillageAdapter);

        if (woman != null)
            aqReg.id(R.id.etregdate).text(woman.getRegRegistrationDate());

        if (woman != null && woman.getRegPregnantorMother() == 1) {
            aqReg.id(R.id.radiopreg).checked(true);
            aqReg.id(R.id.tvlmp).getTextView().setTypeface(null, Typeface.BOLD);
            aqReg.id(R.id.etlmp).text(woman.getRegLMP());
            aqReg.id(R.id.etlmp).enabled(false);

            pregOrMother = 1;//24Oct2019 Arpitha

            aqReg.id(R.id.regpregormother).text(getResources().getString(R.string.regpreg));//09April2019 Arpitha
            disableMotherDetails();
            if (woman != null &&
                    woman.getRegGravida() == 1) {
                aqReg.id(R.id.ettotlivebirth).text("");
                aqReg.id(R.id.ettotabort).text("");
                aqReg.id(R.id.ethomedel).text("");
                aqReg.id(R.id.etstillbirth).text("");
                aqReg.id(R.id.tvhomedel).text("");
                aqReg.id(R.id.tvDeadDuringPreg).text("");
                aqReg.id(R.id.etDeadAfterDel).text("");

                aqReg.id(R.id.ettotlivebirth).enabled(false).background(R.drawable.edittext_disable);
                aqReg.id(R.id.ettotlivebirth).enabled(false);
                aqReg.id(R.id.tvtotabort).enabled(false);
                aqReg.id(R.id.ettotabort).enabled(false).background(R.drawable.edittext_disable);

                aqReg.id(R.id.ethomedel).enabled(false).background(R.drawable.edittext_disable);
                aqReg.id(R.id.tvhomedel).enabled(false);
                aqReg.id(R.id.etstillbirth).enabled(false).background(R.drawable.edittext_disable);//13May2021 Arpitha
                aqReg.id(R.id.tvstillbirth).enabled(false);//13May2021 Arpitha
                aqReg.id(R.id.tvDeadDuringPreg).enabled(false);
                aqReg.id(R.id.etDeadDuringPreg).enabled(false).background(R.drawable.edittext_disable);
                aqReg.id(R.id.tvDeadAfterDel).enabled(false);
                aqReg.id(R.id.etDeadAfterDel).enabled(false).background(R.drawable.edittext_disable);


                aqReg.id(R.id.ettotpreg).enabled(false).background(R.drawable.edittext_disable);

                calTotPregnancies();

                aqReg.id(R.id.ettotpreg).text("1");


                aqReg.id(R.id.tvnooflivechildren).enabled(false);
                aqReg.id(R.id.etnooflivechildren).enabled(false).background(R.drawable.edittext_disable);

                aqReg.id(R.id.etlastchildWeight).enabled(false).background(R.drawable.edittext_disable);
                aqReg.id(R.id.etlastchildage).enabled(false).background(R.drawable.edittext_disable);
                aqReg.id(R.id.radiofemale1).enabled(false);
                aqReg.id(R.id.radiomale1).enabled(false);


            } else {

                aqReg.id(R.id.ettotpreg).text("2"); // 30oct2018 - bindu - set
                aqReg.id(R.id.ettotpreg).enabled(true);

                calTotPregnancies();
            }
            if (woman != null && (woman.getRegEDD() == null ||
                    woman.getRegEDD().length() == 0) && aqReg.id(R.id.etlmp).getText()
                    .toString() != null && aqReg.id(R.id.etlmp).getText().toString().trim().length() > 0) {
                populateEDD(aqReg.id(R.id.etlmp).getText().toString());

                // 30oct2018 - set para based on gest age
                if (woman != null && woman.getRegGravida() == 1) {
                    if (gestAge >= 24) {
                        aqReg.id(R.id.ettotlivebirth).enabled(true).background(R.drawable.edittext_style); //11Aug2019 - Bindu change bg
                        aqReg.id(R.id.tvtotlivebirth).enabled(true);

                        aqReg.id(R.id.tvtotabort).enabled(true);
                        aqReg.id(R.id.ettotabort).enabled(true).background(R.drawable.edittext_style); //11Aug2019 - Bindu change bg


                    } else {
                        aqReg.id(R.id.ettotlivebirth).text("0");
                    }

                }

            } else {
                if (woman != null && woman.getRegEDD() != null && woman.getRegEDD().trim().length() > 0)
                    CalculateLMP(woman.getRegEDD());
                populateEDD(aqReg.id(R.id.etlmp).getText().toString());
                aqReg.id(R.id.ettotlivebirth).enabled(true);
                aqReg.id(R.id.tvtotlivebirth).enabled(true);

                aqReg.id(R.id.tvtotabort).enabled(true);
                aqReg.id(R.id.ettotabort).enabled(true);

                if (woman.getRegGravida() == 1) {
                    if (gestAge < 24) {
                        aqReg.id(R.id.ettotlivebirth).enabled(false);
                        aqReg.id(R.id.tvtotlivebirth).enabled(false);
                        aqReg.id(R.id.ettotlivebirth).text("0");

                        aqReg.id(R.id.tvtotabort).enabled(false);
                        aqReg.id(R.id.ettotabort).enabled(false);


                        aqReg.id(R.id.ettotlivebirth).background(R.drawable.edittext_disable);
                        aqReg.id(R.id.ettotabort).background(R.drawable.edittext_disable);

                    }
                }
            }
        } else {
            aqReg.id(R.id.tvlmp).getTextView().setTypeface(null, Typeface.NORMAL);
            aqReg.id(R.id.radiomother).checked(true);
            disablePregDetails();
            aqReg.id(R.id.tvedd).text((getResources().getString(R.string.tvadd))).enabled(false);
            String str = "";
            str = woman.getRegADDate();
            aqReg.id(R.id.tvEddValue).text(str);
            aqReg.id(R.id.etdeldate).text(str).enabled(false);
//            aqReg.id(R.id.btnWPAndPPD1).enabled(false).getImageView().setBackgroundResource(R.drawable.disable_button);Arpitha 15Nov2019
            calTotPregnancies();

            aqReg.id(R.id.ettotpreg).text("1");

            pregOrMother = 2;//24Oct2019 Arpitha

            aqReg.id(R.id.tvtotpreg).enabled(true);
            aqReg.id(R.id.ettotpreg).enabled(true);
            aqReg.id(R.id.regpregormother).text(getResources().getString(R.string.regmother));//09April2019 Arpitha
            aqReg.id(R.id.tvlmp).gone();
            aqReg.id(R.id.txtEDDL).gone();

            aqReg.id(R.id.trlmp).visible();
            aqReg.id(R.id.tredd).visible();

            aqReg.id(R.id.ettotlivebirth).enabled(false).background(R.drawable.edittext_disable);
            aqReg.id(R.id.ettotlivebirth).enabled(false);

            aqReg.id(R.id.ethomedel).enabled(false).background(R.drawable.edittext_disable);
            aqReg.id(R.id.tvhomedel).enabled(false);
            aqReg.id(R.id.etstillbirth).enabled(false).background(R.drawable.edittext_disable);//13May2021 Arpitha
            aqReg.id(R.id.tvstillbirth).enabled(false);//13May2021 Arpitha
            aqReg.id(R.id.tvDeadDuringPreg).enabled(false);
            aqReg.id(R.id.etDeadDuringPreg).enabled(false).background(R.drawable.edittext_disable);
            aqReg.id(R.id.tvDeadAfterDel).enabled(false);
            aqReg.id(R.id.etDeadAfterDel).enabled(false).background(R.drawable.edittext_disable);



            calTotPregnancies();



            aqReg.id(R.id.tvnooflivechildren).enabled(false);
            aqReg.id(R.id.etnooflivechildren).enabled(false).background(R.drawable.edittext_disable);

            aqReg.id(R.id.etlastchildWeight).enabled(false).background(R.drawable.edittext_disable);
            aqReg.id(R.id.etlastchildage).enabled(false).background(R.drawable.edittext_disable);
            aqReg.id(R.id.radiofemale1).enabled(false);
            aqReg.id(R.id.radiomale1).enabled(false);
        }

//        aqReg.id(R.id.spnnoofchild).getSpinner().setPrompt((getResources().getString(R.string.noofchilddel)));


        aqReg.id(R.id.tvthaicard).text(getResources().getString(R.string.rchid)); //06Dec2019 - Bindu

        aqReg.id(R.id.etthaicard).getEditText().setSelection(aqReg.id(R.id.etthaicard).getText().toString().trim().length()); // Starting point Cursor

        aqReg.id(R.id.rdrchid).checked(true);


        if (Integer.parseInt(aqReg.id(R.id.ettotpreg).getText().toString()) == 1 && aqReg.id(R.id.radiopreg).isChecked())
            aqReg.id(R.id.btnWPAndPPD1).enabled(false).getImageView().setBackgroundResource(R.drawable.disable_button);

        aqReg.id(R.id.etdob).getEditText().setKeyListener(null);
//        aqReg.id(R.id.ethomereturndate).getEditText().setKeyListener(null);
        aqReg.id(R.id.etthaicardregdate).getEditText().setKeyListener(null);

        aqReg.id(R.id.etmarriagrdate).getEditText().setKeyListener(null);
        aqReg.id(R.id.etthaicardregdate).enabled(false).background(R.drawable.edittext_disable);  //09Aug2019 - Bindu
        aqReg.id(R.id.etbranchname).enabled(false).background(R.drawable.edittext_disable);  //11Aug2019 - Bindu
        aqReg.id(R.id.etbankaccount).enabled(false).background(R.drawable.edittext_disable);  //11Aug2019 - Bindu
        aqReg.id(R.id.etifsc).enabled(false).background(R.drawable.edittext_disable);  //11Aug2019 - Bindu
        //26Sep2019 - bindu - set default place of reg and disable
        aqReg.id(R.id.spnPlaceOfReg).setSelection(1).enabled(false).background(R.drawable.spinner_bg_disabled);
        aqReg.id(R.id.chkcomplicatedpreg).enabled(false);
    }


    /**
     * hide all scrollViews, set gray background to tab buttons
     */
    private void hideAllOptionalViews() {
        aqReg.id(R.id.srlWomanBasicInfo).gone();
        aqReg.id(R.id.srlWomanPersonalAndPrevPregDetails).gone();
        aqReg.id(R.id.srlhealthhistory).gone();
        aqReg.id(R.id.srlSummary).gone();
//        aqReg.id(R.id.srlDelInfo).gone();

        aqReg.id(R.id.btnWBD).getImageView().setBackgroundResource(R.drawable.gray_button);
        aqReg.id(R.id.btnWPAndPPD1).getImageView().setBackgroundResource(R.drawable.gray_button);
//        aqReg.id(R.id.btnDelInfo).getImageView().setBackgroundResource(R.drawable.gray_button);
        aqReg.id(R.id.btnHStatus).getImageView().setBackgroundResource(R.drawable.gray_button);
        aqReg.id(R.id.btnSummary).getImageView().setBackgroundResource(R.drawable.gray_button);
//        aqReg.id(R.id.btnIssueAtDeliveryAcc).getImageView().setImageResource(R.drawable.add);

        if (aqReg.id(R.id.ettotpreg).getText().toString() != null
                && aqReg.id(R.id.ettotpreg).getText().toString().trim().length() > 0
                && Integer.parseInt(aqReg.id(R.id.ettotpreg).getText().toString()) == 1 && aqReg.id(R.id.radiopreg).isChecked())
            aqReg.id(R.id.btnWPAndPPD1).enabled(false)
                    .getImageView().setBackgroundResource(R.drawable.disable_button);

//        aqReg.id(R.id.btnDelInfo).enabled(false).getImageView()
//                .setBackgroundResource(R.drawable.disable_button);
    }


    /**
     * This method sets adapters to spinners
     */
    @SuppressLint("DefaultLocale")
    private void populateSpinnerValues() {
        try {
            ArrayAdapter<String> adapter = null;

            ArrayList<String> blood = new ArrayList<String>();
            blood.add((getResources().getString(R.string.selectType)));
            blood.add((getResources().getString(R.string.apositive)));
            blood.add((getResources().getString(R.string.anegative)));
            blood.add((getResources().getString(R.string.bpositive)));
            blood.add((getResources().getString(R.string.bnegative)));
            blood.add((getResources().getString(R.string.abpositive)));
            blood.add((getResources().getString(R.string.abnegative)));
            blood.add((getResources().getString(R.string.opositive)));
            blood.add((getResources().getString(R.string.onegative)));
            blood.add((getResources().getString(R.string.dontknow)));

            ArrayAdapter<String> adapterblood = new ArrayAdapter<String>(this,
                    R.layout.simple_spinner_dropdown_item, blood);
            adapterblood.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            aqReg.id(R.id.spnbloodgroup).adapter(adapterblood);

            BankRepository bankRepository = new BankRepository(databaseHelper);
            List<String> bankNamesArray = bankRepository.getBankName();

            HintAdapter bankNameAdapter = new HintAdapter(RegistrationActivity.this,
                    bankNamesArray, R.layout.simple_spinner_dropdown_item);

            bankNameAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            aqReg.id(R.id.spnbankname).adapter(bankNameAdapter);

            int pos = bankNamesArray.size() - 1;
            aqReg.id(R.id.spnbankname).setSelection(pos);
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }


    /**
     * calulation for counting total pregnancies
     */
    protected void calTotPregnancies() throws Exception {


        totPregs = (aqReg.id(R.id.ettotpreg).getText().toString().length() > 0)
                ? Integer.parseInt(aqReg.id(R.id.ettotpreg).getText().toString()) : 0;

        calCompPreg();

        if (aqReg.id(R.id.radiopreg).isChecked()) {
            aqReg.id(R.id.tvlastchildage).enabled(true);
            aqReg.id(R.id.etlastchildage).enabled(true).background(R.drawable.edittext_style); //11Aug2019 - Bindu Enable background
            aqReg.id(R.id.tvlastchildWeight).enabled(true);
            aqReg.id(R.id.etlastchildWeight).enabled(true).background(R.drawable.edittext_style); //11Aug2019 - Bindu Enable background
            aqReg.id(R.id.tvchildsex).enabled(true);
            aqReg.id(R.id.radiofemale1).enabled(true);
            aqReg.id(R.id.radiomale1).enabled(true);
        } else {
            // if(Integer.parseInt(aqReg.id(R.id.ettotpreg).getText().toString())<=1)
            if(totPregs <= 1) { //24Dec2019 - Bindu - enable disable last child info
                disableLastChildFields();
                aqReg.id(R.id.etlastchildage).text("");
                aqReg.id(R.id.etlastchildWeight).text("");
                aqReg.id(R.id.radiofemale1).checked(false);
                aqReg.id(R.id.radiomale1).checked(false);
            }
            else {
                aqReg.id(R.id.tvlastchildage).enabled(true);
                aqReg.id(R.id.etlastchildage).enabled(true).background(R.drawable.edittext_style); //11Aug2019 - Bindu Enable background
                aqReg.id(R.id.tvlastchildWeight).enabled(true);
                aqReg.id(R.id.etlastchildWeight).enabled(true).background(R.drawable.edittext_style); //11Aug2019 - Bindu Enable background
                aqReg.id(R.id.tvchildsex).enabled(true);
                aqReg.id(R.id.radiofemale1).enabled(true);
                aqReg.id(R.id.radiomale1).enabled(true);
            }
        }

    }

    //    disables last child fields
    private void disableLastChildFields() throws Exception {

        aqReg.id(R.id.tvlastchildage).enabled(false);
        aqReg.id(R.id.etlastchildage).enabled(false).background(R.drawable.edittext_disable); //11Aug2019 - Bindu - bg
        aqReg.id(R.id.tvlastchildWeight).enabled(false);
        aqReg.id(R.id.etlastchildWeight).enabled(false).background(R.drawable.edittext_disable); //11Aug2019 - Bindu - bg
        aqReg.id(R.id.tvchildsex).enabled(false);
        aqReg.id(R.id.radiofemale1).enabled(false);
        aqReg.id(R.id.radiofemale1).checked(false);
        aqReg.id(R.id.radiomale1).enabled(false);
        aqReg.id(R.id.radiomale1).checked(false);
        aqReg.id(R.id.tvlastchildWeight).enabled(false);
        aqReg.id(R.id.tvchildsex).enabled(false);


        aqReg.id(R.id.etlastchildage).background(R.drawable.edittext_disable);
        aqReg.id(R.id.etlastchildWeight).background(R.drawable.edittext_disable);

    }

    /**
     * On mother radio btn clicked, disabling pregnant fields
     */
    protected void disablePregDetails() throws Exception {

        aqReg.id(R.id.tvEddValue).enabled(false);
//        aqReg.id(R.id.tvadd).enabled(true);
//        aqReg.id(R.id.etadd).enabled(true);
        aqReg.id(R.id.chkcomplicatedpreg).enabled(false);
//        aqReg.id(R.id.tvadd).visible();
//        aqReg.id(R.id.etadd).visible();

        hideAllOptionalViews();
        aqReg.id(R.id.srlWomanBasicInfo).visible();
        ((ScrollView) aqReg.id(R.id.srlWomanBasicInfo).getView()).fullScroll(ScrollView.FOCUS_UP);
        aqReg.id(R.id.btnWBD).getImageView().setBackgroundColor(getResources().getColor(R.color.gray));
        aqReg.id(R.id.btnWBD).getImageView().requestFocus();

        aqReg.id(R.id.tvpregstatus).text((" " + getResources().getString(R.string.statusMother)));
        //12Nov2019 - Bindu - status while reg always eng to store
        statuswhilereg = (" " + getResources().getString(R.string.statusMothersave));

    }

    //  calculate LMP based on EDD
    private void CalculateLMP(String EDDate) throws Exception {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        Date LMP;
        LMP = sdf.parse(EDDate);
        Calendar cal = Calendar.getInstance();
        cal.setTime(LMP);
        cal.add(Calendar.DAY_OF_MONTH, -280);

        int day = cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH) + 1;
        int year = cal.get(Calendar.YEAR);

        String lmpDate = String.format("%02d", day) + "-" + String.format("%02d", month) + "-" + year;
        aqReg.id(R.id.etlmp).text(lmpDate);
    }


    // Calculate EDD and display in EDD Edittext
    private void populateEDD(String xLMP) throws Exception {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        Date LmpDate;
        LmpDate = sdf.parse(xLMP);
        Calendar cal = Calendar.getInstance();
        cal.setTime(LmpDate);
        cal.add(Calendar.DAY_OF_MONTH, 280);

        int day = cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH) + 1;
        int year = cal.get(Calendar.YEAR);

        if (woman.getRegEDD() == null || woman.getRegEDD().length() == 0) {
            eddDate = String.format("%02d", day) + "-" + String.format("%02d", month) + "-" + year;
            //09Apr2021 Bindu - set edd value only if preg
            if(woman.getRegPregnantorMother() == 1)
                aqReg.id(R.id.tvEddValue).text(eddDate);

//            24Dec2019 Arpitha
            if(lmp)
                aqReg.id(R.id.eteddmother).text(eddDate);

            int noOfDays = DateTimeUtil.getNumberOfDaysFromLMP(xLMP);
            int noOfWeeks = noOfDays / 7;
            gestAge = noOfWeeks;
            int days = noOfDays % 7;
            String primi = "", primival = ""; //12Nov2019 - Bindu
            if (Integer.parseInt(aqReg.id(R.id.ettotpreg).getText().toString()) == 1) {
                primi = getResources().getString(R.string.primi);
                primival = getResources().getString(R.string.primisave);
            }
            if (days > 0) {
                if (days == 1) {
                    aqReg.id(R.id.tvpregstatus).text((noOfWeeks + "" + getResources().getString(R.string.weeks) + " "
                            + days + " " + getResources().getString(R.string.day) + " " + primi));
                    statuswhilereg = (noOfWeeks + "" + getResources().getString(R.string.weekssave) + " "
                            + days + " " + getResources().getString(R.string.daysave) + " " + primival); //12Nov2019 - Bindu
                } else {
                    aqReg.id(R.id.tvpregstatus).text((noOfWeeks + "" + getResources().getString(R.string.weeks) + " "
                            + days + " " + getResources().getString(R.string.days) + " " + primi));
                    statuswhilereg = noOfWeeks + "" + getResources().getString(R.string.weekssave) + " "
                            + days + " " + getResources().getString(R.string.dayssave) + " " + primival; //12Nov2019 - Bindu
                }
                /*
                if (days == 1)
                    aqReg.id(R.id.tvpregstatus).text((noOfWeeks + "" + getResources().getString(R.string.weeks) + " "
                            + days + " " + getResources().getString(R.string.day) + " " + primi));
                else
                    aqReg.id(R.id.tvpregstatus).text((noOfWeeks + "" + getResources().getString(R.string.weeks) + " "
                            + days + " " + getResources().getString(R.string.days) + " " + primi));*/
            } else {
                aqReg.id(R.id.tvpregstatus)
                        .text((noOfWeeks + "" + getResources().getString(R.string.weeks) + " " + primi));
                statuswhilereg = noOfWeeks + "" + getResources().getString(R.string.weekssave) + " " + primival;
            }

        } else {

            String primi = "", primival = ""; //12Nov2019 - Bindu
            if (Integer.parseInt(aqReg.id(R.id.ettotpreg).getText().toString()) == 1) {
                primi = getResources().getString(R.string.primi);
                primival = getResources().getString(R.string.primisave);
            }

            aqReg.id(R.id.tvEddValue).text(woman.getRegEDD());
            eddDate = woman.getRegEDD();
            CalculateLMP(woman.getRegEDD());
            int noOfWeeks = DateTimeUtil.getNumberOfWeeksFromLMP(xLMP);
            gestAge = noOfWeeks;
            aqReg.id(R.id.tvpregstatus)
                    .text((noOfWeeks + "" + getResources().getString(R.string.weeks) + " " + primi));
            //12Nov2019 - Bindu
            statuswhilereg = noOfWeeks + "" + getResources().getString(R.string.weekssave) + " " + primi;
        }
    }

    /**
     * On pregnant radio btn clicked, disabling mother fields
     */
    protected void disableMotherDetails() {

//        aqReg.id(R.id.tvadd).enabled(false);
//////        aqReg.id(R.id.etadd).text("");
//////        aqReg.id(R.id.etadd).enabled(false);
////        aqReg.id(R.id.tvadd).gone();
////        aqReg.id(R.id.etadd).gone();

        aqReg.id(R.id.tvEddValue).enabled(true);
        aqReg.id(R.id.tvpregstatus).text(" ");
        ((ScrollView) aqReg.id(R.id.srlWomanBasicInfo).getView()).fullScroll(ScrollView.FOCUS_UP);
        aqReg.id(R.id.etthaicard).getEditText().requestFocus();

        hideAllOptionalViews();
        aqReg.id(R.id.srlWomanBasicInfo).visible();
        ((ScrollView) aqReg.id(R.id.srlWomanBasicInfo).getView()).fullScroll(ScrollView.FOCUS_UP);
        aqReg.id(R.id.btnWBD).getImageView().setBackgroundColor(getResources().getColor(R.color.gray));
        aqReg.id(R.id.btnWBD).getImageView().requestFocus();
    }


    /**
     * This method invokes camera intent
     */
    @SuppressLint("SimpleDateFormat")
    protected void CapturingImage() {

        try {
            // intent to start device camera
            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            Date xDate = new Date();
            String lasmod = new SimpleDateFormat("dd-hh-mm-ss").format(xDate);

            String fileName = "iWomen" + lasmod;

            File imageDir = new File(appState.imgDirRef);

            if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
                if (imageDir != null) {
                    if (!imageDir.mkdirs()) {
                        if (!imageDir.exists()) {
                            Log.d("CameraSample", "failed to create directory");
                        } else {
                            Log.d("CameraSample", "created directory");
                        }
                    }
                }
            } else {
                Log.v(getString(R.string.app_name), "External storage is not mounted READ/WRITE.");
            }

            path = imageDir + "/" + fileName + ".jpg";
            imgfileBeforeResult = new File(path);

            outputFileUri = Uri.fromFile(imgfileBeforeResult);
            outputFileUri = FileProvider.getUriForFile(this,getApplicationContext().getPackageName()
                    + ".provider", imgfileBeforeResult);


            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            startActivityForResult(intent, 0);

        } catch (NullPointerException e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }


    /**
     * Delete the temporarily stored Images
     */
    private void deleteImages() {

        if (imgfileAfterResult != null)
            imgfileAfterResult.delete();
        if (imgfileBeforeResult != null)
            imgfileBeforeResult.delete();
    }

    //    visible basic data
    private void openBasicInfoAndSetFocus() {

        aqReg.id(R.id.srlWomanBasicInfo).visible();
        aqReg.id(R.id.btnWBD).getImageView().setBackgroundColor(getResources().getColor(R.color.gray));
        if (focusEditText != null)
            focusEditText.requestFocus();
    }


    //    Validate registartion mandatory fields
    private boolean validateRegFields() throws Exception {


        focusEditText = null;


        //07Aug2019 - Bindu - place of reg mandatory
        if (aqReg.id(R.id.spnPlaceOfReg).getSelectedItem().toString().equalsIgnoreCase("Select")) {
            //09Aug2019 - Bindu set focus
            if (aqReg.id(R.id.spnPlaceOfReg).getSpinner().getSelectedView().toString() != null) { // 04Aug2019 - Bindu - default mapto service type

                TextView errorText = (TextView) aqReg.id(R.id.spnPlaceOfReg).getSpinner().getSelectedView();
                errorText.setError("");
                errorText.setTextColor(Color.RED);//just to highlight that this is an error
                errorText.setText(getResources().getString(R.string.selectplaceofreg));//changes the selected item text to this
                errorText.setFocusable(true);

                aqReg.id(R.id.etthaicard).getEditText().requestFocus();
                aqReg.id(R.id.etthaicard).getEditText().setFocusableInTouchMode(true); //11Aug2019 - Bindu -set error and focus
            }

            return false;
        }


        // 12Oct2017 Arpitha
        if (aqReg.id(R.id.etthaicard).getText().toString().trim().length() > 0
                && aqReg.id(R.id.etthaicard).getText().toString().trim().length() < 7) {
            Toast.makeText(getApplicationContext(),
                    (getResources().getString(R.string.thayicardno_mustcontain_sevendigits)),
                    Toast.LENGTH_LONG).show();
            focusEditText = aqReg.id(R.id.etthaicard).getEditText();
            return false;
        } else if (aqReg.id(R.id.etthaicard).getText().toString().trim().length() > 0) {
            ArrayList<String> thayicardNo = new ArrayList<String>();
            if (thayicardNo.contains(aqReg.id(R.id.etthaicard).getText().toString())) {
                Toast.makeText(getApplicationContext(),
                        (getResources().getString(R.string.thayicardno_must_be_unique)), Toast.LENGTH_LONG)
                        .show();
                focusEditText = aqReg.id(R.id.etthaicard).getEditText();
                aqReg.id(R.id.etthaicard).text("");
                return false;
            }

        }


        if (aqReg.id(R.id.etwomenname).getText().toString().trim().length() <= 0) {
            focusEditText = aqReg.id(R.id.etwomenname).getEditText();
            Toast.makeText(getApplicationContext(), (getResources().getString(R.string.m073)),
                    Toast.LENGTH_LONG).show();
            return false;
        }


        int age = (aqReg.id(R.id.etage).getText().toString().length() > 0)
                ? Integer.parseInt(aqReg.id(R.id.etage).getText().toString()) : 0;
        if (age <= 0) {
            Toast.makeText(getApplicationContext(), (getResources().getString(R.string.m072)),
                    Toast.LENGTH_LONG).show();
            focusEditText = aqReg.id(R.id.etage).getEditText();
            return false;
        }

        if (aqReg.id(R.id.etWomanWeight).getText().toString().trim().length() > 0) {
            Double weight = Double.parseDouble(aqReg.id(R.id.etWomanWeight).getText().toString());
            if (aqReg.id(R.id.etWomanWeight).getText().toString().length() <= 0 || weight == 0) {
                Toast.makeText(getApplicationContext(), (getResources().getString(R.string.m071)),
                        Toast.LENGTH_LONG).show();
                focusEditText = aqReg.id(R.id.etWomanWeight).getEditText();
                return false;
            } else if (weight == 0) {

                Toast.makeText(getApplicationContext(), (getResources().getString(R.string.m071)),
                        Toast.LENGTH_LONG).show();
                focusEditText = aqReg.id(R.id.etWomanWeight).getEditText();
                return false;
            }
        } else {
            if (aqReg.id(R.id.etWomanWeight).getText().toString().length() <= 0 || weight == 0) {
                Toast.makeText(getApplicationContext(), (getResources().getString(R.string.m071)),
                        Toast.LENGTH_LONG).show();
                focusEditText = aqReg.id(R.id.etWomanWeight).getEditText();
                return false;
            }
        }


        if (!(aqReg.id(R.id.rd_cm).isChecked() || aqReg.id(R.id.rd_feet).isChecked()
                || aqReg.id(R.id.rd_htdontknow).isChecked())) {
            Toast.makeText(getApplicationContext(), (getResources().getString(R.string.enter_height)),
                    Toast.LENGTH_LONG).show();
            focusEditText = aqReg.id(R.id.etWomanWeight).getEditText();

            return false;
        }

        if (heightUnit == 2 && (aqReg.id(R.id.etheight).getText().toString().trim().length() <= 0
                || Double.parseDouble(aqReg.id(R.id.etheight).getText().toString()) == 0)) {
            Toast.makeText(getApplicationContext(), (getResources().getString(R.string.enter_height)),
                    Toast.LENGTH_LONG).show();
            focusEditText = aqReg.id(R.id.etheight).getEditText();
            return false;
        }

        int height = Integer.parseInt(aqReg.id(R.id.spnHeightNumeric).getSelectedItem().toString());
        if (height <= 0) {
            Toast.makeText(getApplicationContext(), (getResources().getString(R.string.m133)),
                    Toast.LENGTH_LONG).show();
            return false;
        }


        if (villageCode == 0) {
            // 04Aug2019 - Bindu - default mapto service type
            TextView errorText = (TextView) aqReg.id(R.id.spnvillage).getSpinner().getSelectedView();
            errorText.setError("");
            errorText.setTextColor(Color.RED);//just to highlight that this is an error
            errorText.setText(getResources().getString(R.string.m184));//changes the selected item text to this
            errorText.setFocusable(true);
            aqReg.id(R.id.etaadharcard).getEditText().requestFocus();
            aqReg.id(R.id.etaadharcard).getEditText().setFocusableInTouchMode(true); //11Aug2019 - Bindu -set error and focus

            Toast.makeText(getApplicationContext(), (getResources().getString(R.string.m184)),
                    Toast.LENGTH_LONG).show();

            return false;
        }


        if (aqReg.id(R.id.etaadharcard).getText().toString().trim().length() > 0) {

            if (aqReg.id(R.id.etaadharcard).getText().toString().trim().length() < 12) {
                Toast.makeText(getApplicationContext(),
                        (getResources().getString(R.string.aadharcardno_mustcontain_sevendigits)),
                        Toast.LENGTH_LONG).show();
                focusEditText = aqReg.id(R.id.etaadharcard).getEditText();
                return false;
            }

        }


        String strPh = aqReg.id(R.id.etphone).getText().toString().trim();
        if (strPh.length() > 0) {
            if (strPh.length() < 10 || strPh.length() > 12) {
                Toast.makeText(getApplicationContext(), (getResources().getString(R.string.m156)),
                        Toast.LENGTH_LONG).show();
                focusEditText = aqReg.id(R.id.etphone).getEditText();
                return false;
            }

            //08Apr2021 Bindu  add only if phone number is entered
            //10Aug2019 - Bindu - reordered the position
            if (aqReg.id(R.id.spnphn).getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.select))) { //14Nov2019 - Bindu

                //09Aug2019 - Bindu set focus
                if (aqReg.id(R.id.spnphn).getSpinner().getSelectedView().toString() != null) { // 04Aug2019 - Bindu - default mapto service type

                    TextView errorText = (TextView) aqReg.id(R.id.spnphn).getSpinner().getSelectedView();
                    errorText.setError("");
                    errorText.setTextColor(Color.RED);//just to highlight that this is an error
                    errorText.setText(getResources().getString(R.string.pleaseselectmobileof));//changes the selected item text to this
                    errorText.setFocusable(true);
                    aqReg.id(R.id.etphone).getEditText().requestFocus();
                    aqReg.id(R.id.etphone).getEditText().setFocusableInTouchMode(true); //11Aug2019 - Bindu -set error and focus
                    return false;
                }

                return false;
            }
        }

        /*if (aqReg.id(R.id.etphone).getText().toString().trim().length() < 10) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.plsentervalidphoneno), Toast.LENGTH_LONG).show(); //Bindu change static msg

            focusEditText = aqReg.id(R.id.etphone).getEditText();
            return false;
        }*/ // 08Apr2021 Bindu remove mandatory

        //08Apr2021 Bindu - set either of them as mandatory
        if (aqReg.id(R.id.etthaicard).getText().toString().trim().length() <= 0 && aqReg.id(R.id.etphone).getText().toString().trim().length() <= 0 && aqReg.id(R.id.etaadharcard).getText().toString().trim().length() <= 0) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.plsenteruniqueid), Toast.LENGTH_LONG).show(); //Bindu change static msg
            focusEditText = aqReg.id(R.id.etphone).getEditText();
            return false;
        }

        if (aqReg.id(R.id.etwifeageMarriage).getText().toString().length() > 0) {
            int ageAtMarriage = Integer.parseInt(aqReg.id(R.id.etwifeageMarriage).getText().toString());
            if (ageAtMarriage > age) {
                Toast.makeText(getApplicationContext(), (getResources().getString(R.string.m163)),
                        Toast.LENGTH_LONG).show();
                focusEditText = aqReg.id(R.id.etwifeageMarriage).getEditText();
                return false;
            } else if (ageAtMarriage == 0) {
                Toast.makeText(getApplicationContext(), (getResources().getString(R.string.womanageatmarriageiscannotbezero)),
                        Toast.LENGTH_LONG).show();
                focusEditText = aqReg.id(R.id.etwifeageMarriage).getEditText();
                return false;
            }
        }

        if (aqReg.id(R.id.radiopreg).isChecked()) {
            if ((aqReg.id(R.id.etlmp).getText().toString().trim().length()) <= 0) {
                Toast.makeText(getApplicationContext(), (getResources().getString(R.string.m074)),
                        Toast.LENGTH_LONG).show();
                focusEditText = aqReg.id(R.id.etlmp).getEditText();
                return false;
            }
        }


        if (aqReg.id(R.id.ettotpreg).getText().toString().trim().length() <= 0) {
            Toast.makeText(getApplicationContext(), (getResources().getString(R.string.plsentergravida)),
                    Toast.LENGTH_LONG).show();

            focusEditText = aqReg.id(R.id.ettotpreg).getEditText();
            return false;
        }

        if ( aqReg.id(R.id.ettotpreg).getText().toString().trim().length()>0 &&
                Integer.parseInt(aqReg.id(R.id.ettotpreg).getText().toString())>1 &&
                aqReg.id(R.id.ettotlivebirth).getText().toString().trim().length() <= 0) {
            Toast.makeText(getApplicationContext(), (getResources().getString(R.string.plsenterpara)),
                    Toast.LENGTH_LONG).show();

            focusEditText = aqReg.id(R.id.ettotlivebirth).getEditText();
            return false;
        }

        int gravida = (aqReg.id(R.id.ettotpreg).getText().toString().length() > 0)
                ? Integer.parseInt(aqReg.id(R.id.ettotpreg).getText().toString()) : 0;
        if (aqReg.id(R.id.ettotpreg).getText().toString().length() <= 0) {
            Toast.makeText(getApplicationContext(), (getResources().getString(R.string.plsentergravida)),
                    Toast.LENGTH_LONG).show();
            focusEditText = aqReg.id(R.id.ettotpreg).getEditText();
            return false;
        } else if (gravida == 0) {
            Toast.makeText(getApplicationContext(), (getResources().getString(R.string.plsentergravida)),
                    Toast.LENGTH_LONG).show();
            focusEditText = aqReg.id(R.id.ettotpreg).getEditText();
            return false;
        }

        if (woman != null && woman.getRegPregnantorMother() == 0) {
            if (!(woman.getRegPregnantorMother() == 0)) {
                if (aqReg.id(R.id.ettotpreg).getText().toString().length() > 0) {
                    if (Integer.parseInt(aqReg.id(R.id.ettotpreg).getText().toString()) < 2) {
                        Toast.makeText(getApplicationContext(),
                                (getResources().getString(R.string.gravidlessthan2)), Toast.LENGTH_LONG)
                                .show();
                        focusEditText = aqReg.id(R.id.ettotpreg).getEditText();
                        return false;
                    }
                }
            }
        } else {
            if (aqReg.id(R.id.ettotpreg).getText().toString().length() > 0) {
                if (Integer.parseInt(aqReg.id(R.id.ettotpreg).getText().toString()) < 1) {
                    Toast.makeText(getApplicationContext(),
                            (getResources().getString(R.string.gravidacannotbezero)), Toast.LENGTH_LONG)
                            .show();
                    focusEditText = aqReg.id(R.id.ettotpreg).getEditText();
                    return false;
                }
            }
        }

        if (aqReg.id(R.id.ethusage).getText().toString().trim().length() > 0) {
            if (Integer.parseInt(aqReg.id(R.id.ethusage).getText().toString()) == 0) {
                Toast.makeText(getApplicationContext(),
                        (getResources().getString(R.string.husbandageiscannotbezero)),
                        Toast.LENGTH_LONG).show();
                focusEditText = aqReg.id(R.id.ethusage).getEditText();
                return false;
            }
        }


        if (aqReg.id(R.id.ethusageMarriage).getText().toString().length() > 0) {
            int ageAtMarriage = Integer.parseInt(aqReg.id(R.id.ethusageMarriage).getText().toString());
            int husage = 0;
            if (aqReg.id(R.id.ethusageMarriage).getText().toString().trim().length() > 0)
                husage = Integer.parseInt(aqReg.id(R.id.ethusage).getText().toString()); //11Aug2019 - Bindu - husb age set ,it was husb marriage age
            if (husage > 0 && ageAtMarriage > husage) {
                Toast.makeText(getApplicationContext(),
                        (getResources().getString(R.string.husband_marriage_age_less_than_current_age)),
                        Toast.LENGTH_LONG).show();
                focusEditText = aqReg.id(R.id.ethusageMarriage).getEditText();
                aqReg.id(R.id.ethusageMarriage).text("");
                return false;
            } else if (husage == 0) {
                Toast.makeText(getApplicationContext(), (getResources().getString(R.string.husbandageatmarriageiscannotbezero)),
                        Toast.LENGTH_LONG).show();
                focusEditText = aqReg.id(R.id.ethusageMarriage).getEditText();
                return false;
            }
        }


        //22Nov2019 - Bindu - bank account no max length to 9 from 12
        if (aqReg.id(R.id.etbankaccount).getText().toString().trim().length() > 0
                && (aqReg.id(R.id.etbankaccount).getText().toString().trim().length() < 9
                || aqReg.id(R.id.etbankaccount).getText().toString().trim().length() > 18)) {
            Toast.makeText(getApplicationContext(),
                    (getResources().getString(R.string.enter_valid_account_no)), Toast.LENGTH_LONG).show();

            aqReg.id(R.id.etbankaccount).getEditText().requestFocus();
            focusEditText = aqReg.id(R.id.etbankaccount).getEditText();
            return false;

        }

        //09Sep2021 Arpitha
       List<tblregisteredwomen> regList =  new WomanRepository(databaseHelper).getExistingAadharIds();
        if(regList!=null && regList.size()>0 && regList.contains(woman.getCurrentWomenStatus())
        && regList.contains( aqReg.id(R.id.etaadharcard).getText().toString()))
        {
            Toast.makeText(getApplicationContext(),
                    (getResources().getString(R.string.ancactive)), Toast.LENGTH_LONG).show();
            aqReg.id(R.id.etaadharcard).getEditText().requestFocus();
            focusEditText = aqReg.id(R.id.etaadharcard).getEditText();
            return false;
        }
        return true;
    }

    /**
     * Validate Lmp and ADD Date Set
     */
    private boolean validateLmpADDDateSet() throws Exception {


        focusEditText = null;

//        if (aqReg.id(R.id.radiopreg).isChecked() && aqReg.id(R.id.etadd).getText().length() == 0) {
        int days = DateTimeUtil.getDaysBetweenDates(aqReg.id(R.id.etregdate).getText().toString(),
                aqReg.id(R.id.etlmp).getText().toString());
        if (woman.getRegPregnantorMother() == 0 && days < 30) {
            Toast.makeText(getApplicationContext(), (getResources().getString(R.string.m075)),
                    Toast.LENGTH_LONG).show();
            aqReg.id(R.id.etlmp).text("");
            aqReg.id(R.id.tvEddValue).text("");
            aqReg.id(R.id.tvpregstatus).text("");
            focusEditText = aqReg.id(R.id.etlmp).getEditText();
            return false;
        }


        int days1 = DateTimeUtil.getDaysBetweenDates(DateTimeUtil.getTodaysDate(),
                aqReg.id(R.id.etlmp).getText().toString());// 28Jun2018 Arpitha
        if (woman.getRegPregnantorMother() == 0 && days1 > 280) {// 28Jun2018 Arpitha
            Toast.makeText(getApplicationContext(), (getResources().getString(R.string.m076new)),
                    Toast.LENGTH_LONG).show();
            focusEditText = aqReg.id(R.id.etlmp).getEditText();
            aqReg.id(R.id.etlmp).text("");
            aqReg.id(R.id.tvpregstatus).text("");
            aqReg.id(R.id.tvEddValue).text("");
            return false;
        }
//        }


        return true;
    }


    /**
     * Confirm Alert on Click save
     */
    private void confirmAlert() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set dialog message
        String mess = "";
        if (regCount > 1)
            mess = (getResources().getString(R.string.duplicateuid)) + " " +
                    (getResources().getString(R.string.regconfirmation));
        else
            mess = getResources().getString(R.string.regconfirmation);
        alertDialogBuilder.setMessage(mess).setCancelable(false)
                .setNegativeButton((getResources().getString(R.string.m121)),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                try {
                                    saveData();
                                } catch (Exception e) {

                                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                                    crashlytics.log(e.getMessage());
                                }
                            }
                        })
                .setPositiveButton((getResources().getString(R.string.m122)),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }


    /**
     * This method invokes when Item clicked in Spinner
     *
     * @throws Exception
     */
    public void onSpinnerClicked(AdapterView<?> adapter, View v, int position, long arg3) throws Exception {

        try {
            switch (adapter.getId()) {
                case R.id.spnvillage: {
                    if (adapter.getSelectedItemPosition() != 0) {
                        villageName = (String) adapter.getSelectedItem();
                        villageCode = mapVillage.get(villageName);

                    } else {
                        villageCode = 0;

                    }
                    break;

                }


                // 08Jan2018 Arpitha
                case R.id.spnHeightNumeric:
                    height_numeric = adapter.getSelectedItemPosition();

                    calculateBMI();

                    checkHeightValue();

                    break;
                case R.id.spnHeightDecimal:
                    height_decimal = adapter.getSelectedItemPosition();
                    calculateBMI();
                    checkHeightValue();
                    break;


                case R.id.spnbankname:
                    //11Aug2019 - Bindu - disable and reset values on change
                    if (aqReg.id(R.id.spnbankname).getSpinner().getSelectedItem() != null
                            && aqReg.id(R.id.spnbankname).getSpinner().getSelectedItem().toString().equalsIgnoreCase("Select")) {
                        aqReg.id(R.id.etbranchname).text("");
                        aqReg.id(R.id.etbankaccount).text("");
                        aqReg.id(R.id.etifsc).text("");
                        aqReg.id(R.id.etbranchname).enabled(false).background(R.drawable.edittext_disable);  //11Aug2019 - Bindu
                        aqReg.id(R.id.etbankaccount).enabled(false).background(R.drawable.edittext_disable);  //11Aug2019 - Bindu
                        aqReg.id(R.id.etifsc).enabled(false).background(R.drawable.edittext_disable);  //11Aug2019 - Bindu
                    } else {
                        aqReg.id(R.id.etbranchname).text("");
                        aqReg.id(R.id.etbankaccount).text("");
                        aqReg.id(R.id.etifsc).text("");
                        aqReg.id(R.id.etbranchname).enabled(true).background(R.drawable.edittext_style);  //11Aug2019 - Bindu
                        aqReg.id(R.id.etbankaccount).enabled(true).background(R.drawable.edittext_style);  //11Aug2019 - Bindu
                        aqReg.id(R.id.etifsc).enabled(true).background(R.drawable.edittext_style);  //11Aug2019 - Bindu
                    }
                    break;

                case R.id.spnbloodgroup: {

                    BloodGroup = adapter.getSelectedItemPosition();
                    //01Oct2019 - Bindu - Blood group neg set Rh-ve checked and disable fields
                    if (adapter != null && adapter.getSelectedItem().toString().contains(getResources().getString(R.string.negative))) {
                        aqReg.id(R.id.chkrhneg).checked(true);
                        aqReg.id(R.id.chkrhneg).enabled(false);
                    } else if (adapter != null && !(adapter.getSelectedItem().toString().contains(getResources().getString(R.string.select)))) {
                        aqReg.id(R.id.chkrhneg).checked(false);
                        aqReg.id(R.id.chkrhneg).enabled(false);
                    } else {
                        aqReg.id(R.id.chkrhneg).checked(false);
                        aqReg.id(R.id.chkrhneg).enabled(true);
                    }
                    break;
                }
                case R.id.spnPlaceOfReg: {

                    placeOfReg = adapter.getSelectedItem().toString();
                    break;
                }
                default:
                    break;
            }
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    // check for height
    private void checkHeightValue() {
        height_numeric = Integer.parseInt(aqReg.id(R.id.spnHeightNumeric).getSelectedItem().toString());
        height_decimal = Integer.parseInt(aqReg.id(R.id.spnHeightDecimal).getSelectedItem().toString());
        if (height_numeric < 5) {
            ((TextView) aqReg.id(R.id.spnHeightNumeric).getSpinner().getSelectedView())
                    .setError("Height Value is " + height_numeric + "." + height_decimal + " feet");
            ((TextView)
                    aqReg.id(R.id.spnHeightDecimal).getSpinner().getSelectedView()).setError("Height Value is " + height_numeric + "." + height_decimal + " feet");
        } else {  //16Aug2019 - Bindu - set else block to remove error
            ((TextView) aqReg.id(R.id.spnHeightNumeric).getSpinner().getSelectedView())
                    .setError(null);
            ((TextView)
                    aqReg.id(R.id.spnHeightDecimal).getSpinner().getSelectedView()).setError(null);

        }

    }


    /**
     * This method calculate age from DOB
     */
    protected int getYearsFromDob(String string) throws ParseException {

        int years = 0;
        String dobtxt = aqReg.id(R.id.etdob).getText().toString();
        Calendar curDt = new GregorianCalendar();
        curDt.setTime(new SimpleDateFormat("dd-MM-yyyy").parse(DateTimeUtil.getTodaysDate()));
        Calendar dob = new GregorianCalendar();
        if (dobtxt.length() > 0) {
            dob.setTime(new SimpleDateFormat("dd-MM-yyyy").parse(dobtxt));
            years = curDt.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
        }
        return years;
    }

    //  text change listner
    TextWatcher watcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            try {

                if (s == aqReg.id(R.id.etage).getEditable()) {

                    //06Aug2019 - Bindu - Change the msg based on cond
                    if (aqReg.id(R.id.etage).getText().toString().length() > 0
                            && Integer.parseInt(aqReg.id(R.id.etage).getText().toString()) < 18) {
                        aqReg.id(R.id.etage).getEditText()
                                .setError(getResources().getString(R.string.agelessthan18));
                    } else if (aqReg.id(R.id.etage).getText().toString().length() > 0
                            && Integer.parseInt(aqReg.id(R.id.etage).getText().toString()) > 40) {
                        aqReg.id(R.id.etage).getEditText()
                                .setError(getResources().getString(R.string.agegreaterthan40));
                    } else if (aqReg.id(R.id.etdob).getText().toString().length() > 0) { //07Aug2019 - Bindu age and dob diff
                        int ageyears = getYearsFromDob(aqReg.id(R.id.etdob).getText().toString());
                        if (aqReg.id(R.id.etage).getText().toString().length() > 0
                                && Integer.parseInt(aqReg.id(R.id.etage).getText().toString()) != ageyears) {
                            aqReg.id(R.id.etage).getEditText()
                                    .setError(getResources().getString(R.string.dobandagenotmatching));
                            aqReg.id(R.id.txtdoberror).visible();//21AUg2019 Arpitha
                            aqReg.id(R.id.txtdoberror).text(getResources().getString(R.string.dobandagenotmatching));//21AUg2019 Arpitha
                        } else {
                            if (ageyears < 0 || !(ageyears > 0 && (ageyears < 18 || ageyears > 40))) {
                                aqReg.id(R.id.txtdoberror).gone();//21AUg2019 Arpitha
                                aqReg.id(R.id.etdob).getEditText().setError(null);
                            }//07Aug2019 - Bindu set dob error
                        }
                    } else {
                        aqReg.id(R.id.etage).getEditText().setError(null);
                        aqReg.id(R.id.etdob).getEditText().setError(null); //07Aug2019 - Bindu set dob error
                    }


                } else if (s == aqReg.id(R.id.etWomanWeight).getEditable()) {

                    if (aqReg.id(R.id.etWomanWeight).getText().toString().length() > 0) {
                        if (Double.parseDouble(aqReg.id(R.id.etWomanWeight).getText().toString()) <= 35
                                || Double.parseDouble(aqReg.id(R.id.etWomanWeight).getText().toString()) > 80) {
                            aqReg.id(R.id.etWomanWeight).getEditText()
                                    // .setError("Weight is " + aqReg.id(R.id.etWomanWeight).getText().toString() + " kgs");
                                    .setError(getResources().getString(R.string.strweight) + aqReg.id(R.id.etWomanWeight).getText().toString() + getResources().getString(R.string.strkg)); //Bindu - change static msg
                        }

                        //09Aug2019 - Bindu - calculate bmi
                        if (heightUnit == 1 || heightUnit == 2)
                            calculateBMI();
                    } else {                        // 09Aug2019 - Bindu - reset bmi val
                        aqReg.id(R.id.tvBMI).text("");
                    }

                } else if (s == aqReg.id(R.id.etheight).getEditable()) {

                    if (aqReg.id(R.id.etheight).getText().toString().length() > 0) {
                        if (Double.parseDouble(aqReg.id(R.id.etheight).getText().toString()) < 153) {
                            aqReg.id(R.id.etheight).getEditText()
                                    // .setError("Height is " + aqReg.id(R.id.etheight).getText().toString() + " cms");
                                    .setError(getResources().getString(R.string.strheight) + aqReg.id(R.id.etheight).getText().toString() + getResources().getString(R.string.strcm)); //Bindu - change static msg

                        }
                        //09Aug2019 - Bindu - calculate bmi
                        calculateBMI();
                    } else {                        // 09Aug2019 - Bindu - reset bmi val
                        aqReg.id(R.id.tvBMI).text("");
                    }

                } else if (s == aqReg.id(R.id.ettotpreg).getEditable()) {
                    try{
                        if(Integer.parseInt(aqReg.id(R.id.ettotpreg).getText().toString())<=1)
                        {
                            disableLastChildFields();
//                         09Aug2021 Arpitha
                            aqReg.id(R.id.ettotlivebirth).text("");
                            aqReg.id(R.id.ettotabort).text("");
                            aqReg.id(R.id.ethomedel).text("");
                            aqReg.id(R.id.etstillbirth).text("");
                           // aqReg.id(R.id.tvhomedel).text("");
                            aqReg.id(R.id.etDeadDuringPreg).text("");
                            aqReg.id(R.id.etDeadAfterDel).text("");
                            aqReg.id(R.id.etnooflivechildren).text("");

                            aqReg.id(R.id.ettotlivebirth).enabled(false).background(R.drawable.edittext_disable);
                            aqReg.id(R.id.ettotlivebirth).enabled(false);
                            aqReg.id(R.id.tvtotabort).enabled(false);
                            aqReg.id(R.id.ettotabort).enabled(false).background(R.drawable.edittext_disable);

                            aqReg.id(R.id.ethomedel).enabled(false).background(R.drawable.edittext_disable);
                            aqReg.id(R.id.tvhomedel).enabled(false);
                            aqReg.id(R.id.etstillbirth).enabled(false).background(R.drawable.edittext_disable);//13May2021 Arpitha
                            aqReg.id(R.id.tvstillbirth).enabled(false);//13May2021 Arpitha
                            aqReg.id(R.id.tvDeadDuringPreg).enabled(false);
                            aqReg.id(R.id.etDeadDuringPreg).enabled(false).background(R.drawable.edittext_disable);
                            aqReg.id(R.id.tvDeadAfterDel).enabled(false);
                            aqReg.id(R.id.etDeadAfterDel).enabled(false).background(R.drawable.edittext_disable);

                            aqReg.id(R.id.tvnooflivechildren).enabled(false);
                            aqReg.id(R.id.etnooflivechildren).enabled(false);
                            aqReg.id(R.id.etnooflivechildren).enabled(false).background(R.drawable.edittext_disable);



                        }

                        else {
                            int para = 0;
                            if (aqReg.id(R.id.ettotlivebirth).getText().toString().length() > 0)
                                para = Integer.parseInt(aqReg.id(R.id.ettotlivebirth).getText().toString());
                            if (aqReg.id(R.id.ettotpreg).getText().toString().length() > 0) {
                                if (para > Integer.parseInt(aqReg.id(R.id.ettotpreg).getText().toString())) {
                                    //aqReg.id(R.id.ettotlivebirth).getEditText().setError("Para cannot be greater than gravida");
                                    aqReg.id(R.id.ettotlivebirth).getEditText().setError(getResources().getString(R.string.paracannotbegreaterthangravida));//14Nov2019 - Bindu-res
                                    aqReg.id(R.id.ettotlivebirth).text("");
                                }
                            }

                            if (woman.getRegPregnantorMother() == 0) {
                                if ((woman.getRegGravida() != 1)) {
                                    if (aqReg.id(R.id.ettotpreg).getText().toString().length() > 0) {
                                        if (Integer.parseInt(aqReg.id(R.id.ettotpreg).getText().toString()) < 2) {
                                            aqReg.id(R.id.ettotpreg).getEditText()
                                                    // .setError("Gravida cannot be less than 2 since its not a primi case");
                                                    .setError(getResources().getString(R.string.gravidlessthan2)); //14Nov2019 - Bindu - res

                                        }
                                    }
                                }
                            } else {
                                if (aqReg.id(R.id.ettotpreg).getText().toString().length() > 0) {
                                    if (Integer.parseInt(aqReg.id(R.id.ettotpreg).getText().toString()) < 1) {
                                        // aqReg.id(R.id.ettotpreg).getEditText().setError("Gravida cannot be 0");
                                        aqReg.id(R.id.ettotpreg).getEditText().setError(getResources().getString(R.string.gravidacannotbezero));//14Nov2019 - Bindu - res
                                    }
                                }
                            }


                            calTotPregnancies();



                            aqReg.id(R.id.ettotlivebirth).enabled(true).background(R.drawable.edittext_style);
                            aqReg.id(R.id.ettotlivebirth).enabled(true);
                            aqReg.id(R.id.tvtotabort).enabled(true);
                            aqReg.id(R.id.ettotabort).enabled(true).background(R.drawable.edittext_style);

                            aqReg.id(R.id.ethomedel).enabled(true).background(R.drawable.edittext_style);
                            aqReg.id(R.id.tvhomedel).enabled(true);
                            aqReg.id(R.id.etstillbirth).enabled(true).background(R.drawable.edittext_disable);//13May2021 Arpitha
                            aqReg.id(R.id.tvstillbirth).enabled(true);//13May2021 Arpitha
                            aqReg.id(R.id.tvDeadDuringPreg).enabled(true);
                            aqReg.id(R.id.etDeadDuringPreg).enabled(true).background(R.drawable.edittext_style);
                            aqReg.id(R.id.tvDeadAfterDel).enabled(true);
                            aqReg.id(R.id.etDeadAfterDel).enabled(true).background(R.drawable.edittext_style);

                            aqReg.id(R.id.tvnooflivechildren).enabled(true);
                            aqReg.id(R.id.etnooflivechildren).enabled(true);
                            aqReg.id(R.id.etnooflivechildren).enabled(true).background(R.drawable.edittext_style);

                        }
                    }catch (Exception e)
                    {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }

                } else if (s == aqReg.id(R.id.ettotlivebirth).getEditable()) {

                    if (woman.getRegGravida() == 1 && gestAge >= 24) {
                        if (aqReg.id(R.id.ettotlivebirth).getText().toString().length() > 0) {
                            if (Integer.parseInt(aqReg.id(R.id.ettotlivebirth).getText().toString()) > 1) {
                                aqReg.id(R.id.ettotlivebirth).getEditText()
                                        //.setError("Para cannot be greater than gravida");
                                        .setError(getResources().getString(R.string.paracannotbegreaterthangravida)); //14Nov2019 - Bindu
                                aqReg.id(R.id.ettotlivebirth).text("");

                            }
                        }
                    } else {
                        int gravida = 0;
                        if (aqReg.id(R.id.ettotpreg).getText().toString().length() > 0)
                            gravida = Integer.parseInt(aqReg.id(R.id.ettotpreg).getText().toString());
                        if (woman.getRegPregnantorMother() == 0) {
                            if (gestAge >= 24) {
                                if (aqReg.id(R.id.ettotlivebirth).getText().toString().length() > 0) {
                                    if (Integer.parseInt(aqReg.id(R.id.ettotlivebirth).getText().toString()) > gravida) {
                                        aqReg.id(R.id.ettotlivebirth).getEditText()
                                                // .setError("Para cannot be greater than gravida");
                                                .setError(getResources().getString(R.string.paracannotbegreaterthangravida)); //14Nov2019 -Bindu -res
                                        aqReg.id(R.id.ettotlivebirth).text("");
                                    }
                                }
                            } else {
                                if (aqReg.id(R.id.ettotlivebirth).getText().toString().length() > 0) {
                                    if (Integer.parseInt(aqReg.id(R.id.ettotlivebirth).getText().toString()) >= gravida) {
                                        aqReg.id(R.id.ettotlivebirth).getEditText()
                                                //   .setError("Para cannot be greater than or equal to gravida");
                                                .setError(getResources().getString(R.string.paracannotbegreaterorequalgravida)); //14Nov2019- Bindu -res
                                        aqReg.id(R.id.ettotlivebirth).text("");
                                    }
                                }
                            }
                        } else {
                            if (aqReg.id(R.id.ettotlivebirth).getText().toString().length() > 0) {
                                /*if (Integer.parseInt(aqReg.id(R.id.ettotlivebirth).getText().toString()) == 0) {
                                    //aqReg.id(R.id.ettotlivebirth).getEditText().setError("Para cannot be 0");
                                    aqReg.id(R.id.ettotlivebirth).getEditText().setError(getResources().getString(R.string.paracannotbezero)); //14Nov2019 - Bindu - res
                                    aqReg.id(R.id.ettotlivebirth).text("");
                                } else */ //21May2021 Bindu allow para to be zero
                                    if (Integer
                                        .parseInt(aqReg.id(R.id.ettotlivebirth).getText().toString()) > gravida) {
                                    aqReg.id(R.id.ettotlivebirth).getEditText()
                                            // .setError("Para cannot be greater than gravida");
                                            .setError(getResources().getString(R.string.paracannotbegreaterthangravida)); //14Nov2019 - Bindu - res
                                    aqReg.id(R.id.ettotlivebirth).text("");
                                }
                            }
                        }
                    }

                    calTotPregnancies();
                } else if (s == aqReg.id(R.id.etwifeageMarriage).getEditable()) {
                    if (count > 0) {
                        if (Integer.parseInt(aqReg.id(R.id.etwifeageMarriage).getText().toString()) == 0)
                            aqReg.id(R.id.etwifeageMarriage).getEditText().setError(getResources().getString(R.string.womanageatmarriageiscannotbezero));
                        else if (Integer.parseInt(aqReg.id(R.id.etwifeageMarriage).getText().toString()) > Integer.parseInt(aqReg.id(R.id.etage).getText().toString())) {
                            aqReg.id(R.id.etwifeageMarriage).getEditText().setError(getResources().getString(R.string.m163));

                        }
                    }
                } else if (s == aqReg.id(R.id.ethusageMarriage).getEditable()) {
                    if (count > 0) {
                        if (Integer.parseInt(aqReg.id(R.id.ethusageMarriage).getText().toString()) == 0)
                            aqReg.id(R.id.ethusageMarriage).getEditText().setError(getResources().getString(R.string.husbandageatmarriageiscannotbezero));
                        else if (Integer.parseInt(aqReg.id(R.id.ethusageMarriage).getText().toString()) > Integer.parseInt(aqReg.id(R.id.ethusage).getText().toString())) {
                            aqReg.id(R.id.ethusageMarriage).getEditText().setError(getResources().getString(R.string.husband_age_lessthan_currrent_age));

                        }
                    }
                } else if (s == aqReg.id(R.id.ethusage).getEditable()) {
                    if (count > 0) {
                        if (Integer.parseInt(aqReg.id(R.id.ethusage).getText().toString()) == 0) {
                            aqReg.id(R.id.ethusage).getEditText().setError(getResources().getString(R.string.husbandageiscannotbezero));

                        } else if (aqReg.id(R.id.ethusage).getText().toString().length() > 0
                                && Integer.parseInt(aqReg.id(R.id.ethusage).getText().toString()) < 18) {
                            aqReg.id(R.id.ethusage).getEditText()
                                    .setError(getResources().getString(R.string.husbagelessthan18));
                        } else
                            aqReg.id(R.id.ethusage).getEditText().setError(null);  // 07Aug2019 - Bindu iCreateNewTrans cond <18
                        //                          22Aug2019 Arpitha
                        if (Integer.parseInt(aqReg.id(R.id.ethusageMarriage).getText().toString()) > Integer.parseInt(aqReg.id(R.id.ethusage).getText().toString()))
                            aqReg.id(R.id.ethusageMarriage).getEditText().setError(getResources().getString(R.string.husband_age_lessthan_currrent_age));
                        else
                            aqReg.id(R.id.ethusageMarriage).getEditText().setError(null);//22Aug2019 Arpitha
                    }

                } else if (s == aqReg.id(R.id.etlastchildage).getEditable()) {
                    if (count > 0) {
                        if (Integer.parseInt(aqReg.id(R.id.etlastchildage).getText()
                                .toString()) == 0)
                            aqReg.id(R.id.etlastchildage).getEditText().
                                    setError(getResources().getString
                                            (R.string.m068));


                        if (aqReg.id(R.id.ettotlivebirth).getText().toString().length() > 0 && Integer.parseInt(aqReg.id(R.id.ettotlivebirth).getText().toString()) > 0) {
                            if (aqReg.id(R.id.etlastchildage).getText().toString().length() > 0) {
                                int lastchildage = (aqReg.id(R.id.etlastchildage).getText().toString().length() > 0 ? Integer.parseInt(aqReg.id(R.id.etlastchildage).getText().toString()) : 0);
                                if (lastchildage <= 0) {

                                    aqReg.id(R.id.etlastchildage).getEditText()
                                            .setError(getResources().getString(R.string.lastchildagezero));
                                } else {
                                    aqReg.id(R.id.etlastchildage).getEditText().setError(null);
                                }
                            }
                        } else {
                            aqReg.id(R.id.etlastchildage).getEditText().setError(null);
                        }

                    }

                } else if (s == aqReg.id(R.id.etthaicard).getEditable()) {
                    if (count > 0) {
                        if (aqReg.id(R.id.etthaicard).getText()
                                .toString().trim().length() < 7)
                            aqReg.id(R.id.etthaicard).getEditText().
                                    setError(getResources().getString
                                            (R.string.thayicardno_mustcontain_sevendigits));

                        if (aqReg.id(R.id.etthaicard).getText().toString().trim().length() > 0) {
                            aqReg.id(R.id.etthaicardregdate).enabled(true).background(R.drawable.edittext_style);  //09Aug2019 - Bindu
                        } else {
                            aqReg.id(R.id.etthaicardregdate).enabled(false).background(R.drawable.edittext_disable);
                            aqReg.id(R.id.etthaicardregdate).text("");  //09Aug2019 Bindu - reset textdate
                        }


                        if (aqReg.id(R.id.etthaicard).getText().toString().trim().length() > 0) {
//                             aqReg.id(R.id.etthaicardregdate).enabled(true);
//                            aqReg.id(R.id.etthaicardregdate).enabled(true).background(R.drawable.editext_none);  //09Aug2019 - Bindu
                            aqReg.id(R.id.etthaicardregdate).enabled(true);
                            aqReg.id(R.id.llthayi).background(R.drawable.edittext_style);  //09Aug2019 - Bindu

                        } else {
                            aqReg.id(R.id.etthaicardregdate).enabled(false);
                            aqReg.id(R.id.etthaicardregdate).text("");  //09Aug2019 Bindu - reset textdate
                            aqReg.id(R.id.llthayi).enabled(true).background(R.drawable.edittext_disable);
                            aqReg.id(R.id.imgclearthayidate).gone();//20Aug2019 Arpitha
                        }

                    }else
                    {
                        aqReg.id(R.id.etthaicardregdate).enabled(false);
                        aqReg.id(R.id.etthaicardregdate).text("");  //09Aug2019 Bindu - reset textdate
                        aqReg.id(R.id.llthayi).enabled(true).background(R.drawable.edittext_disable);
                        aqReg.id(R.id.imgclearthayidate).gone();//20Aug2019 Arpitha
                        aqReg.id(R.id.etthaicardregdate).enabled(false).background(R.drawable.edittext_disable);

                    }

                } else if (s == aqReg.id(R.id.etdob).getEditable()) {
                    int years = getYearsFromDob(aqReg.id(R.id.etdob).getText().toString());
                    //05Aug2019 - Bindu - disable age calculation
                    //07Aug2019 - AGe and DOB val
                    int age = (aqReg.id(R.id.etage).getText().toString().length() > 0 ? Integer.parseInt(aqReg.id(R.id.etage).getText().toString()) : 0);
                    if (years != age) {
                        //04Aug2019 - Bindu - DOB set error
                        aqReg.id(R.id.etdob).getEditText()
                                .setError(getResources().getString(R.string.dobandagenotmatching));
                        aqReg.id(R.id.etage).getEditText()
                                .setError(getResources().getString(R.string.dobandagenotmatching));
                    } else {
                        aqReg.id(R.id.etage).getEditText().setError(null);
                        aqReg.id(R.id.etdob).getEditText().setError(null);
                    }
                } else if (s == aqReg.id(R.id.etlastchildWeight).getEditable()) {
                    if (aqReg.id(R.id.ettotlivebirth).getText().toString().length() > 0 && Integer.parseInt(aqReg.id(R.id.ettotlivebirth).getText().toString()) > 0) {
                        if (aqReg.id(R.id.etlastchildWeight).getText().toString().length() > 0) {
                            int lastchildweight = (aqReg.id(R.id.etlastchildWeight).getText().toString().length() > 0 ? Integer.parseInt(aqReg.id(R.id.etlastchildWeight).getText().toString()) : 0);
                            if (lastchildweight <= 0) {

                                aqReg.id(R.id.etlastchildWeight).getEditText()
                                        .setError(getResources().getString(R.string.lastchildweightzero));
                            } else {
                                aqReg.id(R.id.etlastchildWeight).getEditText().setError(null);

                            }
                        }
                    } else
                        aqReg.id(R.id.etlastchildWeight).getEditText().setError(null);
                }


            } catch (Exception e) {


                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {

        }

    };


    /**
     * This method handle touch listners, calls - displayDatePicker()
     */
    @Override
    public boolean onTouch(View v, MotionEvent event) {

        if (MotionEvent.ACTION_UP == event.getAction()) {
            try {
                switch (v.getId()) {
                    case R.id.etdob: {
                        isdob = true;
                        isThayiCard = false;
                        ismarriagedate = false;
                        lmp = false;
                        assignEditTextAndCallDatePicker(aqReg.id(R.id.etdob).getEditText());
                        break;
                    }
                    case R.id.etthaicardregdate: {
                        isThayiCard = true;
                        ismarriagedate = false;
                        isdob = false;
                        lmp = false;
                        assignEditTextAndCallDatePicker(aqReg.id(R.id.etthaicardregdate).getEditText());
                        break;
                    }
                    // 30Jan2018 Arpitha
                    case R.id.etmarriagrdate: {
                        isThayiCard = false;
                        ismarriagedate = true;
                        isdob = false;
                        lmp = false;
                        assignEditTextAndCallDatePicker(aqReg.id(R.id.etmarriagrdate).getEditText());
                        break;
                    }

                    case R.id.etlmpmother: {
                        isThayiCard = false;
                        ismarriagedate = false;
                        isdob = false;
                        lmp = true;
                        ismarriagedate = false;
                        assignEditTextAndCallDatePicker(aqReg.id(R.id.etlmpmother).getEditText());
                        break;
                    }

                    default:
                        break;
                }
            } catch (Exception e) {

                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
        }
        return true;
    }


    /**
     * This method opens the Date Dialog and sets Date to respective EditText
     */
    private void assignEditTextAndCallDatePicker(EditText editText) {

        currentEditTextViewforDate = editText;
        String currDate = (editText.getText().length() > 0) ? editText.getText().toString() : "";
        CommonDatePickerFragment.getCommonDate(this, currDate);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {

        try {
            if (view.isShown()) {
                String bldSmpldate = null;
                bldSmpldate = String.format("%02d", day) + "-" + String.format("%02d", month + 1) + "-"
                        + year;


                if (isThayiCard) {
                    Date birthdate = null;
                    Date taken = new SimpleDateFormat("dd-MM-yyyy").parse(bldSmpldate);

                    if (aqReg.id(R.id.etthaicardregdate).getText().toString() != null
                            && aqReg.id(R.id.etthaicardregdate).getText().toString()
                            .trim().length() > 0 && aqReg.id(R.id.etdob).getText().toString() != null && aqReg.id(R.id.etdob).getText().toString().trim().length() > 0)
                        birthdate = new SimpleDateFormat("dd-MM-yyyy")
                                .parse(aqReg.id(R.id.etdob).getText().toString());

                    if (birthdate != null && taken.before(birthdate)) {
                        Toast.makeText(getApplicationContext(),
                                (getResources().getString(R.string.thayi_date_cannot_be_before_dob)),
                                Toast.LENGTH_LONG).show();
                        aqReg.id(R.id.etthaicardregdate).text("");
                        aqReg.id(R.id.imgclearthayidate).gone();//20Aug2019 Arpitha
                    } else if (taken.after(new Date())) {
                        Toast.makeText(getApplicationContext(),
                                (getResources().getString(R.string.thayi_date_cannot_after_today)),
                                Toast.LENGTH_LONG).show();
                        aqReg.id(R.id.etthaicardregdate).text("");
                        aqReg.id(R.id.imgclearthayidate).gone();//20Aug2019 Arpitha
                    } else {
                        aqReg.id(R.id.etthaicardregdate).text(bldSmpldate);
                        aqReg.id(R.id.imgclearthayidate).visible();
                        aqReg.id(R.id.llthayi).background(R.drawable.edittext_style);
                        aqReg.id(R.id.etthaicardregdate).background(R.drawable.editext_none);
                    }

                }


                if (ismarriagedate) {
                    Date taken = new SimpleDateFormat("dd-MM-yyyy").parse(bldSmpldate);
                    //05Aug2019 - Bindu - Remove DOB validation

                    if (taken.after(new Date())) {
                        Toast.makeText(getApplicationContext(),
                                (getResources().getString(R.string.date_time_cannot_be_after_curre_datetime)),
                                Toast.LENGTH_LONG).show();
                        aqReg.id(R.id.etmarriagrdate).text("");
                        aqReg.id(R.id.imgclearmarriagedate).gone();//20Aug2019 Arpitha
                    } else {
                        aqReg.id(R.id.etmarriagrdate).text(bldSmpldate);
                        aqReg.id(R.id.imgclearmarriagedate).visible();//20Aug2019 Arpitha
                        aqReg.id(R.id.etmarriagrdate).getEditText().requestFocus();
                    }
                }


                if (isdob) {
                    Date taken = new SimpleDateFormat("dd-MM-yyyy").parse(bldSmpldate);

                    if (taken.after(new Date())) {
                        Toast.makeText(getApplicationContext(),
                                (getResources().getString(R.string.date_time_cannot_be_after_curre_datetime)),
                                Toast.LENGTH_LONG).show();
                        aqReg.id(R.id.etdob).text("");
                        aqReg.id(R.id.imgcleardob).gone();//20Aug2019 Arpitha
                        aqReg.id(R.id.etdob).getEditText().setError(null);//  21Aug2019 Arpitha
                    } else {
                        aqReg.id(R.id.etdob).text(bldSmpldate);
                        aqReg.id(R.id.imgcleardob).visible();//20Aug2019 Arpitha

//                        21Aug2019 Arpitha
                        int age = getYearsFromDob(bldSmpldate);

                        if (age < 18
                                || age > 40) {
                            aqReg.id(R.id.txtdoberror).visible();
                            aqReg.id(R.id.txtdoberror).text("Invalid Date of Birth");

                        } else {
                            aqReg.id(R.id.txtdoberror).gone();//  21Aug2019 Arpitha

                        }
                    }
                }

                if(lmp)
                {
                    Date taken = new SimpleDateFormat("dd-MM-yyyy").parse(bldSmpldate);

                    Date addDate;
                    int diff = 0;
                    if(woman!=null && woman.getRegADDate()!=null && woman.getRegADDate().trim().length()>0)
                    {
                        addDate = new SimpleDateFormat("dd-MM-yyyy").parse(bldSmpldate);

                        diff  =  DateTimeUtil.getDaysBetweenDates(woman.getRegADDate(), bldSmpldate);

                    }


                    if (taken.after(new Date())) {
                        Toast.makeText(getApplicationContext(),
                                (getResources().getString(R.string.date_time_cannot_be_after_curre_datetime)),
                                Toast.LENGTH_LONG).show();
                        aqReg.id(R.id.etlmpmother).text("");
                        aqReg.id(R.id.imgclearmarriagedate).gone();//20Aug2019 Arpitha
                    }
                    else if((diff<210 || diff>270))
                    {
                        Toast.makeText(getApplicationContext(),
                                (getResources().getString(R.string.lmpmustbtwsevenandnine)),
                                Toast.LENGTH_LONG).show();
                        aqReg.id(R.id.etlmpmother).text("");
                        aqReg.id(R.id.imgclearmarriagedate).gone();//20Aug2019 Arpitha
                    }
                    else {
                        aqReg.id(R.id.etlmpmother).text(bldSmpldate);
                        aqReg.id(R.id.imgclearmarriagedate).visible();//20Aug2019 Arpitha
                        aqReg.id(R.id.etlmpmother).getEditText().requestFocus();
                        populateEDD(bldSmpldate);
                        aqReg.id(R.id.etlmpmother).text(bldSmpldate);


                    }
                }
            }
        } catch (ParseException e) {


            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }


    /**
     * This method handles focus change listners Calls - calTotPregnancies(),
     * calculateComplicatedPregnancy(), setLowWeightInfant()
     */
    @Override
    public void onFocusChange(View v, boolean hasFocus) {

        try {
            switch (v.getId()) {

                case R.id.etage: {

                    ColorCount();
                    break;
                }
                case R.id.ettotlivebirth: {
                    setBasedOnPara();
                    break;
                }
                case R.id.ettotabort: {
                    calTotPregnancies();
                    ColorCount();
                    break;
                }
                case R.id.etDeadDuringPreg: {
                    calTotPregnancies();
                    break;
                }
                case R.id.etDeadAfterDel: {
                    calTotPregnancies();
                    break;
                }
                case R.id.etWomanWeight: {
                    calculateBMI();
                    break;
                }

                case R.id.etheight:
                    calculateBMI();
                    break;

                case R.id.ettotpreg:
                    ColorCount();
                    calTotPregnancies();
                    break;

                default:
                    break;
            }
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    /**
     * This method get the result from Camera Intent, gets the image
     */
    @SuppressLint("SimpleDateFormat")
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            wImage = "";
            if (requestCode == IMAGE_CAPTURE) {
                if (resultCode == RESULT_OK) {
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    Bitmap bitmap = BitmapFactory.decodeFile(path);
                    Bitmap resizedBmp = Bitmap.createScaledBitmap(bitmap, 230, 210, true);
                    resizedBmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    aqReg.id(R.id.imgwomenphoto).getImageView().setImageBitmap(resizedBmp);

                    ByteArrayOutputStream outputstream = new ByteArrayOutputStream();
                    resizedBmp.compress(Bitmap.CompressFormat.PNG, 0, outputstream);
                    wImage = Base64.encodeToString(outputstream.toByteArray(), Base64.DEFAULT);
                } else if (resultCode == RESULT_CANCELED) {
                    deleteImages();
                }
            } else {
                Toast.makeText(getApplicationContext(), (getResources().getString(R.string.m100)),
                        Toast.LENGTH_LONG).show();
            }
        } catch (NullPointerException e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }


    @Override
    public void onBackPressed() {

    }


    /**
     * Confirm Alert on Click save
     */
    private void confirmAlertExit() throws Exception {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set dialog message
        alertDialogBuilder.setMessage((getResources().getString(R.string.areyousuretoexit))).setCancelable(false)
                .setNegativeButton((getResources().getString(R.string.m121)),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                try {
                                    Intent exit = new Intent(RegistrationActivity.this,
                                            MainMenuActivity.class);
                                    exit.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                    startActivity(exit);
                                } catch (Exception e) {

                                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                                    crashlytics.log(e.getMessage());
                                }
                            }
                        })
                .setPositiveButton((getResources().getString(R.string.m122)),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }


    /**
     * Initializes the Action Bar items
     */
    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        try {
            getMenuInflater().inflate(R.menu.servicesmenu, menu);

            menu.findItem(R.id.logout).setTitle((getResources().getString(R.string.action_logout)));


        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        return super.onCreateOptionsMenu(menu);
    }


    /**
     * Called whenever an item clicked in actionbar
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        // Handle action buttons
        switch (item.getItemId()) {

            case R.id.home: {
                Intent goToScreen = new Intent(RegistrationActivity.this, MainMenuActivity.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.m110), goToScreen);
                return true;
            }

            case R.id.logout: {
                Intent goToScreen = new Intent(RegistrationActivity.this, LoginActivity.class);
                displayAlert(getResources().getString(R.string.m111), goToScreen);
                return true;
            }


            case R.id.wlist: {
                Intent goToScreen = new Intent(RegistrationActivity.this, RegisteredWomenActionTabs.class);
                displayAlert(getResources().getString(R.string.m110), goToScreen);
                return true;
            }
            //30Sep2019 - Bindu
            case R.id.about: {
                Intent goToScreen = new Intent(RegistrationActivity.this, AboutActivity.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    /**
     * Confirmation Alert
     */
    private void displayAlert(final String spanText2, final Intent goToScreen) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set dialog message
        alertDialogBuilder.setMessage(spanText2).setCancelable(false).setNegativeButton(
                getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (goToScreen != null) {

                            goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(goToScreen);
                            if (spanText2.contains("logout")) {
                                try {
                                    LoginRepository loginRepository = new LoginRepository(databaseHelper);
                                    loginRepository.updateLogoutTime(appState.loginAuditId, appState.sessionUserId);
                                } catch (Exception e) {
                                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                                    crashlytics.log(e.getMessage());
                                }
                            }
                        } else
                            dialog.cancel();
                    }
                }).setPositiveButton(getResources().getString(R.string.m122),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    //    calculate BMI based on height and weight
    private void calculateBMI() throws Exception {

        Double heightInFeet;
        double actHeight, heightInMts = 0;
        if (heightUnit == 2 && aqReg.id(R.id.etheight).getText().toString().trim().length() > 0
                && Double.parseDouble(aqReg.id(R.id.etheight).getText().toString()) > 0) {// Arpitha
            // 08Jan18
            heightInFeet = 0.0328084 * (Double.parseDouble(aqReg.id(R.id.etheight).getText().toString()));

            height = Integer.parseInt(heightInFeet.toString().split("\\.")[0]);
            heightD = Integer.parseInt(heightInFeet.toString().split("\\.")[1].substring(0, 1));

        } else if (heightUnit == 1) {
            height = Integer.parseInt(aqReg.id(R.id.spnHeightNumeric).getSelectedItem().toString());
            heightD = Integer.parseInt(aqReg.id(R.id.spnHeightDecimal).getSelectedItem().toString());
        }else if (heightUnit == 3) {  //05Dec2019 - Bindu - ADD 3 condition
            height = 0;
            heightD = 0;
        }

        actHeight = Double.parseDouble(height + "." + heightD);
        heightInMts = actHeight * 0.3048;

        weight = (aqReg.id(R.id.etWomanWeight).getText().toString().length() > 0)
                ? Double.parseDouble(aqReg.id(R.id.etWomanWeight).getText().toString()) : 0;

        if (heightInMts > 0 && weight > 0) {
            double bmi = weight / (heightInMts * heightInMts);
            aqReg.id(R.id.tvBMI).text("" + bmi);
        } else
            aqReg.id(R.id.tvBMI).text("");
        ColorCount();
    }

    //    enable/disable fields based on para
    private void setBasedOnPara() throws Exception {
        calTotPregnancies();
        int totLiveChild = (aqReg.id(R.id.ettotlivebirth).getText().toString().length() > 0)
                ? Integer.parseInt(aqReg.id(R.id.ettotlivebirth).getText().toString()) : 0;
        if (totLiveChild <= 0) {
            aqReg.id(R.id.etlastchildage).text("");
            aqReg.id(R.id.etlastchildWeight).text("");
            aqReg.id(R.id.radiofemale1).checked(false);
            aqReg.id(R.id.radiomale1).checked(false);
            aqReg.id(R.id.tvlastchildage).enabled(false);
            aqReg.id(R.id.etlastchildage).enabled(false).background(R.drawable.edittext_disable); //11Aug2019 - Bindu - bg
            aqReg.id(R.id.tvlastchildWeight).enabled(false);
            aqReg.id(R.id.etlastchildWeight).enabled(false).background(R.drawable.edittext_disable); //11Aug2019 - Bindu - bg
            aqReg.id(R.id.tvchildsex).enabled(false);
            aqReg.id(R.id.radiofemale1).enabled(false);
            aqReg.id(R.id.radiomale1).enabled(false);
            //09Aug2019 - Bindu
            lastChildGender = "";
        }
    }

    //    text change listner
    private void setOnTextChangeListeners() throws Exception {
        aqReg.id(R.id.etage).getEditText().addTextChangedListener(watcher);
        aqReg.id(R.id.etWomanWeight).getEditText().addTextChangedListener(watcher);
        aqReg.id(R.id.etheight).getEditText().addTextChangedListener(watcher);
        aqReg.id(R.id.ettotpreg).getEditText().addTextChangedListener(watcher);
        aqReg.id(R.id.ettotlivebirth).getEditText().addTextChangedListener(watcher);
        aqReg.id(R.id.etwifeageMarriage).getEditText().addTextChangedListener(watcher);
        aqReg.id(R.id.ethusageMarriage).getEditText().addTextChangedListener(watcher);
        aqReg.id(R.id.ethusage).getEditText().addTextChangedListener(watcher);   //07Aug2019 - Bindu
        aqReg.id(R.id.etlastchildage).getEditText().addTextChangedListener(watcher);
        aqReg.id(R.id.etlastchildWeight).getEditText().addTextChangedListener(watcher); //10Aug2019 - Bindu
        aqReg.id(R.id.etthaicard).getEditText().addTextChangedListener(watcher);
        aqReg.id(R.id.etdob).getEditText().addTextChangedListener(watcher);
        // November 6 2019
        aqReg.id(R.id.etthaicardregdate).enabled(false);  //09Aug2019 - Bindu - 20Aug2019 Arpitha - removed background
     /*   aqReg.id(R.id.etthaicard).getEditText().addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    if (aqReg.id(R.id.etthaicard).getText().toString().trim().length() > 0) {
//                             aqReg.id(R.id.etthaicardregdate).enabled(true);
//                            aqReg.id(R.id.etthaicardregdate).enabled(true).background(R.drawable.editext_none);  //09Aug2019 - Bindu
                        aqReg.id(R.id.etthaicardregdate).enabled(true);
                        aqReg.id(R.id.llthayi).background(R.drawable.edittext_style);  //09Aug2019 - Bindu

                    } else {
                        aqReg.id(R.id.etthaicardregdate).enabled(false);
//                            aqReg.id(R.id.etthaicardregdate).enabled(false).background(R.drawable.edittext_disable);
                        aqReg.id(R.id.etthaicardregdate).text("");  //09Aug2019 Bindu - reset textdate
                        aqReg.id(R.id.llthayi).enabled(true).background(R.drawable.edittext_disable);
                        aqReg.id(R.id.imgclearthayidate).gone();//20Aug2019 Arpitha
                    }
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }

            }
        });*/
    }

    //    spinner item click listner
    private void setSpinnerItemListener() {
        // 01Aug2018 - Bindu - add spinner listener
        aqReg.id(R.id.spnvillage).itemSelected(this, "onSpinnerClicked");
        aqReg.id(R.id.spnbankname).itemSelected(this, "onSpinnerClicked");
        aqReg.id(R.id.spnHeightDecimal).itemSelected(this, "onSpinnerClicked");
        aqReg.id(R.id.spnHeightNumeric).itemSelected(this, "onSpinnerClicked");
    }

    //    on touch  listner
    private void setOnTouchListener() {
        aqReg.id(R.id.etdob).getEditText().setOnTouchListener(this);
        aqReg.id(R.id.etthaicardregdate).getEditText().setOnTouchListener(this);
//        aqReg.id(R.id.ethomereturndate).getEditText().setOnTouchListener(this);

        aqReg.id(R.id.etmarriagrdate).getEditText().setOnTouchListener(this);
        aqReg.id(R.id.etlmpmother).getEditText().setOnTouchListener(this);

    }

    //    on focus change listener
    private void setFocusChangeListener() {
        aqReg.id(R.id.etage).getEditText().setOnFocusChangeListener(this);
        aqReg.id(R.id.ettotlivebirth).getEditText().setOnFocusChangeListener(this);
        aqReg.id(R.id.ettotabort).getEditText().setOnFocusChangeListener(this);
        aqReg.id(R.id.etDeadDuringPreg).getEditText().setOnFocusChangeListener(this);
        aqReg.id(R.id.etDeadAfterDel).getEditText().setOnFocusChangeListener(this);
        aqReg.id(R.id.etWomanWeight).getEditText().setOnFocusChangeListener(this);
        aqReg.id(R.id.etheight).getEditText().setOnFocusChangeListener(this);
        aqReg.id(R.id.ettotpreg).getEditText().setOnFocusChangeListener(this);
    }


    /**
     * This is a Recursive method that traverse through the group and subgroup
     * of view Sets text
     *
     * @throws Exception
     */
    private ArrayList<View> initializeScreen(View v) throws Exception {

        if (!(v instanceof ViewGroup)) {
            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);

            // TextView - Assign Labels
            if (v.getClass() == TextView.class || v.getClass() == CheckBox.class) {
                aqReg.id(v.getId()).text((aqReg.id(v.getId()).getText().toString()));
            }
            if (v.getClass() == AppCompatRadioButton.class) {
                aqReg.id(v.getId()).getButton().setOnClickListener(this);
                aqReg.id(v.getId()).text((aqReg.id(v.getId()).getText().toString()));
            }

            // EditText hint - Assign Labels
            if (v.getClass() == EditText.class) {
                if (aqReg.id(v.getId()).getEditText().getHint() != null)
                    aqReg.id(v.getId()).getEditText().setHint((aqReg.id(v.getId()).getEditText().getHint() + ""));
            }

            // ImageButton/ImageView - OnClickListener commonClick
            if (v.getClass() == AppCompatImageButton.class || v.getClass() == AppCompatImageView.class) {
                aqReg.id(v.getId()).getImageView().setOnClickListener(this);
            }

            // ImageButton/ImageView - OnClickListener commonClick
            if (v.getClass() == CheckBox.class || v.getClass() == CheckBox.class) {
                aqReg.id(v.getId()).getCheckBox().setOnClickListener(this);
            }

            return viewArrayList;
        }
        if (v instanceof Spinner) {
            aqReg.id(v.getId()).itemSelected(this, "onSpinnerClicked");
        }
        if (v instanceof SearchableSpinner) {
            aqReg.id(v.getId()).itemSelected(this, "onSpinnerClicked");
        }

        ArrayList<View> result = new ArrayList<View>();

        ViewGroup viewGroup = (ViewGroup) v;
        for (int i = 0; i < viewGroup.getChildCount(); i++) {

            View child = viewGroup.getChildAt(i);

            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);
            viewArrayList.addAll(initializeScreen(child));

            result.addAll(viewArrayList);
        }
        return result;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

    //    prepare bundle
    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        return b;
    }

    void confimAlertMotherReg() throws SQLException {
        setRegistrationPojo();
        final UserRepository userRepository = new UserRepository(databaseHelper);
        woman.setWomanId(userRepository.getNewWomanId(user));
//                                        Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(RegistrationActivity.this, DeliveryInfoActivity.class);
        intent.putExtra("appState", appState);
        intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
        intent.putExtra("woman", woman);
        startActivity(intent);


        /*AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set dialog message
        String mess = "";
         *//*   if (regCount > 1)
                mess = (getResources().getString(R.string.duplicateuid)) + " " +
                        (getResources().getString(R.string.regconfirmation));
            else
                mess = getResources().getString(R.string.regconfirmation);
            *//*
        mess = "Would you like to Navigate to Delivery Screen, to update Delivery Information?";

        alertDialogBuilder.setMessage(mess).setCancelable(false)
                .setNegativeButton((getResources().getString(R.string.m121)),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                try {
                                    setRegistrationPojo();
                                    final UserRepository userRepository = new UserRepository(databaseHelper);
                                    woman.setWomanId(userRepository.getNewWomanId(user));
//                                        Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_LONG).show();
                                    Intent intent = new Intent(RegistrationActivity.this, DeliveryInfoActivity.class);
                                    intent.putExtra("appState", appState);
                                    intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                    intent.putExtra("woman", woman);
                                    startActivity(intent);
                                } catch (Exception e) {

                                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                                }
                            }
                        })
                .setPositiveButton((getResources().getString(R.string.m122)),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Toast.makeText(getApplicationContext(),
                                        "Please navigate to delivery screen to complete Mother registration..", Toast.LENGTH_LONG).show();

                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();*/

    }

    //04Oct2019 - Bindu - Hide keyboard
    public void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        //13Apr2021 Bindu - Change from currentfocus to getwindow
        //inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        inputMethodManager.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);

    }

    // 26Sep2019 -Bindu - ShowHideTribal or Non tribal
    private void ShowHideTribalDetails() {
        if (aqReg.id(R.id.rdnontribal).isChecked()) {
            /* aqReg.id(R.id.trnontribalcaste).visible();*/
            /*aqReg.id(R.id.trtribalcaste).gone();
            aqReg.id(R.id.rdjenukuruba).checked(false);
            aqReg.id(R.id.rdbettakuruba).checked(false);
            aqReg.id(R.id.rdsoliga).checked(false);
            aqReg.id(R.id.rdyarawa).checked(false);
            aqReg.id(R.id.rdphaniya).checked(false);*/ //09Apr2021 Bindu
            aqReg.id(R.id.trtribalcaste).gone();//26Apr2021 Bindu - show and hide
            aqReg.id(R.id.spntribalcaste).setSelection(0);
        } else if (aqReg.id(R.id.rdtribal).isChecked()) {
            aqReg.id(R.id.trnontribalcaste).gone();
            aqReg.id(R.id.trtribalcaste).visible();
            /*aqReg.id(R.id.rdsc).checked(false);
            aqReg.id(R.id.rdst).checked(false);
            aqReg.id(R.id.rdgeneral).checked(false);
            aqReg.id(R.id.rdothercaste).checked(false);*/
        }
    }


    //    27Nov2019 Arpitha
    private void sendSMS(TransactionHeaderRepository transRepo, DatabaseHelper databaseHelper) {
        try {


           /* p_no = "";
            for (int i = 0; i < values.size(); i++) {

                String s = values.get(i);
                p_no = p_no + values.get(i) + ",";
            }
*/

            String p_no = "";
            List<TblContactDetails> values;

            SettingsRepository settingsRepository = new SettingsRepository(databaseHelper);

            values = settingsRepository.getSpecificPhoneNumber("Registration");
            p_no = "";
            for (int i = 0; i < values.size(); i++) {

                String s = values.get(i).getContactNumber();
                p_no = p_no + values.get(i).getContactNumber() + ",";
            }

            //13May2021 Arpitha
            String uniqueId = "";
            String smsContent="";
            String hrp = "";


            int code = woman.getRegAmberOrRedColorCode();


            String strStar = "-";
            if (code == 9)
                strStar = "1 Amber star";
                //   drawable = R.drawable.amberone;
            else if (code == 1)
                strStar = "1 Red star";

//        drawable = R.drawable.redone;
            else if (code == 2)
                strStar = "2 Red Star"; //16May2021 Bindu - add star

//            drawable = R.drawable.redtwo;
            else if (code == 3)
                strStar = "3 Red Star";
//            drawable = R.drawable.redthree;
            else if (code > 3 && code != 11)
                strStar = "4 Red Star";
//            drawable = R.drawable.redfour;
            else if (code == 11)
                strStar = "2 Amber Star";
//            drawable = R.drawable.ambertwo;

            if (woman.getRegComplicatedpreg() == 1)
                hrp = ",HRP:"+strStar+","+ woman.getRegCurrHealthRiskFactors() + " "
                        + woman.getRegPrevHealthRiskFactors();


            String gestationWks = woman.getRegStatusWhileRegistration();
            gestationWks = gestationWks.replace("Weeks", "W");
            gestationWks = gestationWks.replace("Days", "D");



            if(woman.getRegUIDNo()!=null && woman.getRegUIDNo().trim().length()>0)
            {
                uniqueId = "MCP:"+woman.getRegUIDNo();
            }else if(woman.getRegAadharCard()!=null && woman.getRegAadharCard().trim().length()>0)
            {
                uniqueId = "A:"+woman.getRegAadharCard();
            }else if(woman.getRegPhoneNumber()!=null && woman.getRegPhoneNumber().trim().length()>0)
            {
                uniqueId = "Ph:"+woman.getRegPhoneNumber();
            }

            //23May2021 Bindu - get villagename from eng
            FacilityRepository facilityRepo = new FacilityRepository(databaseHelper);
            String hamletname = facilityRepo.getHamletName(villageCode);

            if(woman.getRegPregnantorMother() ==1 )
        /*smsContent = "Sentiatend:" + appState.userType+", "+uniqueId+", "+woman.getWomanId()+", "
                +"PW-"
                +woman.getRegWomanName()+", "+user.getFacilityName()+", Reg";*/
//        16May2021 Bindu - change sms content
                smsContent = "Sentiatend:-" + woman.getRegWomanName()
                        +", "+uniqueId+", WID - "+woman.getWomanId()+", "
                        + hamletname + "," + gestationWks + ",HRP-"+strStar
                        + "," +appState.userType + "-" +user.getUserName()
                        + " ,"+ user.getUserMobileNumber()+"(REG)";


            if (p_no.length() <= 0) {
                display_messagedialog(transRepo, databaseHelper,smsContent);
            } else
                sending(transRepo, databaseHelper, p_no,smsContent);

           /* String smsContent = "";

            String hrp = "";

            if(woman.getRegComplicatedpreg()==1)
                hrp = " , HRP- "+woman.getRegCurrHealthRiskFactors()+" "+woman.getRegPrevHealthRiskFactors();

            String gestationWks =  woman.getRegStatusWhileRegistration();
            gestationWks = gestationWks.replace("Weeks","W");
            gestationWks = gestationWks.replace("Days","D");

            smsContent = "Matruksha :-"+woman.getRegWomanName() +", "+villageName+" ,"+gestationWks
                    +hrp;
            InserttblMessageLog("8892759870",
                    smsContent, user.getUserId(), transRepo, databaseHelper);
            if (isMessageLogsaved) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.sending_sms),
                        Toast.LENGTH_LONG).show();
                sendSMSFunction();


            }*/
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    /**
     * This method invokes after successfull save of record Sends SMS to the
     * Specified Number
     */// 13Oct2016 Arpitha
    private void sendSMSFunction() throws Exception {
        messageSent = true;
        new SendSMS(this).checkAndSendMsg(databaseHelper);

    }

    // 13Oct2016 Arpitha
    public void InserttblMessageLog(String phoneno, String content, String user_id,
                                    TransactionHeaderRepository transactionHeaderRepository, DatabaseHelper databaseHelper) throws Exception {
        final ArrayList<MessageLogPojo> mlpArr = new ArrayList<MessageLogPojo>();

//        final TransactionHeaderRepository transactionHeaderRepository = new TransactionHeaderRepository(databaseHelper);
//        final int transId = transactionHeaderRepository.iCreateNewTrans(user.getUserId(), databaseHelper);


        if (phoneno.length() > 0) {
            String[] phn = phoneno.split("\\,");
            for (int i = 0; i < phn.length; i++) {
                String num = phn[i];
                if (num != null && num.length() > 0) {
                    MessageLogPojo mlp =
                            new MessageLogPojo();
                    mlp.setUserId(user_id);
                    mlp.setMsgPhoneNo(num);
                    mlp.setMsgBody(content);
                    mlp.setMsgPriority(1);
                    mlp.setMsgNoOfFailures(0);
                    mlp.setTransId(transId);
                    mlp.setWomanId(woman.getWomanId());
                    mlp.setMsgSentDate(DateTimeUtil.getTodaysDate());
                    mlp.setMsgSent(0);
                    mlp.setMsgStage("Registration");//13May2021 Arpitha
                    mlp.setMsgUserType(appState.userType);
                    mlp.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
                    mlp.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());//21May2021 Arpitha

                    mlpArr.add(mlp);
                }
            }
        }


//        TransactionManager.callInTransaction(databaseHelper.getConnectionSource(), new Callable<Void>() {
//            public Void call() throws Exception {

        int addMessage = 0;
        SMSRepository smsRepository = new SMSRepository(databaseHelper);
        if (mlpArr != null) {
            for (final MessageLogPojo mlpp : mlpArr) {

                addMessage = smsRepository.insertIntoMessageLog(mlpp, databaseHelper);
            }

            if (addMessage > 0) {
                isMessageLogsaved = true;
                boolean addRegTrans = transactionHeaderRepository.iNewRecordTransNew(user.getUserId(), transId,
                        "tblmessagelog", databaseHelper);
            }
//                }
//
//                return null;
//            }
//        });


        }
    }

    // 13Oct2016 Arpitha
    public void display_messagedialog(final TransactionHeaderRepository
                                              transactionHeaderRepository, final DatabaseHelper
            databaseHelper,String smsContent) throws Exception {
        try {
            final Dialog sms_dialog = new Dialog(this);
//            sms_dialog.setTitle(getResources().getString(R.string.plsentervalidphoneno));


            sms_dialog.setContentView(R.layout.sms_dialog);


            sms_dialog.show();

            ImageButton imgsend = (ImageButton) sms_dialog.findViewById(R.id.imgsend);
            ImageButton imgcancel = (ImageButton) sms_dialog.findViewById(R.id.imgcancel);
            etphn = (EditText) sms_dialog.findViewById(R.id.etphno);
            final EditText etmess = (EditText) sms_dialog.findViewById(R.id.etmess);
            TextView txtcontcats = (TextView) sms_dialog.findViewById(R.id.txtcontacts);


//            etmess.setVisibility(View.GONE);
            etmess.setText(smsContent);
            etmess.setEnabled(false);
            txtcontcats.setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    // TODO Auto-generated method stub

                    Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
//					context.startActivity(intent);
                    startActivity(intent);
                    return true;
                }
            });


            //  etphn.setText(p_no);

            imgsend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {

                        sending(transactionHeaderRepository, databaseHelper,
                                etphn.getText().toString(),smsContent);

                    } catch (Exception e) {

                        e.printStackTrace();
                    }

                }
            });


            imgcancel.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    sms_dialog.cancel();
//                    18May2021 Arpitha
                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.regsuccess), Toast.LENGTH_LONG).show(); //Bindu change static msg
                    Intent intent = new Intent(RegistrationActivity.this, RegisteredWomenActionTabs.class);
                    intent.putExtra("appState", appState);
                    intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    intent.putExtra("wFilterStr", "");
                    startActivity(intent);
                }
            });
            sms_dialog.setCancelable(false);
        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    void sending(TransactionHeaderRepository transRepo, DatabaseHelper databaseHelper,
                 String phnNo, String smsCont) throws Exception {
        String smsContent = smsCont;

        String hrp = "";


        int code = woman.getRegAmberOrRedColorCode();

        int drawable = 0;
        String strStar = "-";
        if (code == 9)
            strStar = "1 Amber";
            //   drawable = R.drawable.amberone;
        else if (code == 1)
            strStar = "1 Red";

//        drawable = R.drawable.redone;
        else if (code == 2)
            strStar = "2 Red Star"; //16May2021 Bindu - add star

//            drawable = R.drawable.redtwo;
        else if (code == 3)
            strStar = "3 Red Star";
//            drawable = R.drawable.redthree;
        else if (code > 3 && code != 11)
            strStar = "4 Red Star";
//            drawable = R.drawable.redfour;
        else if (code == 11)
            strStar = "2 Amber Star";
//            drawable = R.drawable.ambertwo;

        if (woman.getRegComplicatedpreg() == 1)
            hrp = ",HRP:"+strStar+","+ woman.getRegCurrHealthRiskFactors() + " "
                    + woman.getRegPrevHealthRiskFactors();


        String gestationWks = woman.getRegStatusWhileRegistration();
        gestationWks = gestationWks.replace("Weeks", "W");
        gestationWks = gestationWks.replace("Days", "D");

        /*smsContent = "Matruksha:" + woman.getRegWomanName() + "," + villageName + "," + gestationWks
                + hrp+".. "+", ASHA - " +user.getUserName()
                + " ,"+ user.getUserMobileNumber()+"(REG)";*/



        InserttblMessageLog(phnNo,
                smsContent, user.getUserId(), transRepo, databaseHelper);
        if (isMessageLogsaved) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.sending_sms),
                    Toast.LENGTH_LONG).show();


            if (isMessageLogsaved) {
                if (woman.getRegPregnantorMother() == 2) {

                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.regsuccess), Toast.LENGTH_LONG).show(); //Bindu - change msg
                    Intent intent = new Intent(RegistrationActivity.this, DeliveryInfoActivity.class);
                    intent.putExtra("appState", appState);
                    intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    intent.putExtra("wFilterStr", "");
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.regsuccess), Toast.LENGTH_LONG).show(); //Bindu change static msg
                    Intent intent = new Intent(RegistrationActivity.this, RegisteredWomenActionTabs.class);
                    intent.putExtra("appState", appState);
                    intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    intent.putExtra("wFilterStr", "");
                    startActivity(intent);


                }
            } else
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.m091), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha

            sendSMSFunction();
        } else
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.m091), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha

    }

    //  05Dec2019 Arpitha
    public  int checkSimState() {
        int simState = -1;
        // This is to know the status of the Sim
        TelephonyManager telMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        simState = telMgr.getSimState();

        return simState;
    }

    // return the position of given key in a map
    public static int getKey(Map<String, Integer> mapref, String value) {

        int pos = 0;
        for (Map.Entry<String, Integer> map : mapref.entrySet()) {
            pos++;
            if (map.getValue().toString().equals(value)) {
                return pos;
            }

        }
        return pos;
    }
}





