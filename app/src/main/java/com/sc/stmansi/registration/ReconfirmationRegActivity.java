package com.sc.stmansi.registration;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatRadioButton;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.misc.TransactionManager;
import com.sc.stmansi.About.AboutActivity;
import com.sc.stmansi.common.CommonDatePickerFragment;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.repositories.AuditRepository;
import com.sc.stmansi.repositories.LoginRepository;
import com.sc.stmansi.repositories.TransactionHeaderRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.repositories.WomanServiceRepository;
import com.sc.stmansi.tables.AuditPojo;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.womanlist.RegisteredWomenActionTabs;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Callable;

import com.sc.stmansi.R;

import static com.sc.stmansi.common.DateTimeUtil.getNumberOfDaysFromLMP;

public class ReconfirmationRegActivity extends AppCompatActivity implements
        View.OnClickListener, View.OnFocusChangeListener, DatePickerDialog.OnDateSetListener,
        View.OnTouchListener {

    private tblregisteredwomen regData;
    private AQuery aqVP;
    private Map<String, Integer> facPlaceOfReg = new HashMap<>();
    private Map<String, Integer> phoneNumber = new HashMap<>();
    private Map<String, Integer> village = new HashMap<>();
    private ArrayList<String> villageList = new ArrayList<>();
    private List<String> bankNamesArray;
    private String healthIssuesString = "",
            prevDelString = "", path = null, religion = "", eddDate = "";
    private tblregisteredwomen woman;
    private String villageName;
    private EditText focusEditText = null;
    private int heightUnit = 0, villageCode, redCnt = 0, amberCnt = 0,BloodGroup;// 08Jan2017 Arpitha
    private int heightNumeric = 0;
    private int heightDecimal = 0;
    private int height = 0, heightD = 0;
    private double weight;
    private int daysFromLmp = 0;
    private boolean isFirstTimeMother = false;
    private ArrayList<String> healthIssuesList, prevDelList;
    private String healthIssuesStringStore = "", prevDelStringStore = "", uidType, aplBpl,
            caste;
    private ArrayList<String> healthIssuesListStore, prevDelListStore;
    private static final int IMAGE_CAPTURE = 0;
    private File imgFileBeforeResult;
    private ByteArrayOutputStream baos;
    private Uri outputFileUri;
    private AuditPojo APJ;
    private int gestationAge;
    private String editedValues = "";
    private boolean isDob;
    private boolean isMarriageDate = false;
    private int pregnantOrMother = 1;
    private String wImage;
    private TblInstusers user;
    private DatabaseHelper databaseHelper;
    private AppState appState;
    boolean isRegSpnBanknameset = false;

    //26Sep2019 - Bindu
    private String category = "";
    private boolean isRhNegSet = false;
    private RadioGroup rdgtribalcase;
    private int regType;
    //21Mar2021 - Bindu
    private boolean isLmpDate = false;
    private boolean isEddDate = false;
    private int noOfDays;
    private String lastChildGender = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            Bundle bundle = getIntent().getBundleExtra("globalState");
            appState = bundle.getParcelable("appState");

            databaseHelper = getHelper();

            UserRepository userRepository = new UserRepository(databaseHelper);
            user = userRepository.getOneAuditedUser(appState.ashaId);

            WomanRepository womanRepository = new WomanRepository(databaseHelper);
            regData = womanRepository.getRegistartionDetails(appState.selectedWomanId, appState.sessionUserId);

            setContentView(R.layout.activity_reconfirmregdata);

            aqVP = new AQuery(this);

            initializeScreen(aqVP.id(R.id.rlRegDelMainLayout).getView());

            //04Aug2019 - Bindu - set action bar icon
            getSupportActionBar();
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayOptions(getSupportActionBar().DISPLAY_SHOW_CUSTOM | getSupportActionBar().DISPLAY_SHOW_HOME | getSupportActionBar().DISPLAY_SHOW_TITLE);
            getSupportActionBar().setTitle(getResources().getString(R.string.reconfirmlmp) + " - " + regData.getRegWomanName());


            getData();
            setInitialView();
            setTextChangeListener();
            setFocusChangeListener();
            setOnTouchListener();

            //05Aug2019 - Bindu - Sharing file access for version N and above
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());

            if((regData.getDateDeactivated()!=null &&
                    regData.getDateDeactivated().trim().length()>0)
                    || user.getIsDeactivated()==1) {
                disableScreen();//26Nov2019 Arpitha
            }


        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    //    set touch lisener
    private void setOnTouchListener() throws Exception {
        //21Mar2021 Bindu set touch listener
        aqVP.id(R.id.etlmpmother).getEditText().setOnTouchListener(this);
        aqVP.id(R.id.eteddmother).getEditText().setOnTouchListener(this);
    }

    //	set text change listener to fields
    private void setTextChangeListener() {
        aqVP.id(R.id.ettotpreg).getEditText().addTextChangedListener(watcher);
        aqVP.id(R.id.ettotlivebirth).getEditText().addTextChangedListener(watcher);
        aqVP.id(R.id.etlastchildage).getEditText().addTextChangedListener(watcher);
        aqVP.id(R.id.etlastchildWeight).getEditText().addTextChangedListener(watcher);
    }

    //	set focus change listener to fields
    private void setFocusChangeListener() throws Exception {
        aqVP.id(R.id.ettotlivebirth).getEditText().setOnFocusChangeListener(this);
        aqVP.id(R.id.ettotabort).getEditText().setOnFocusChangeListener(this);
        aqVP.id(R.id.etDeadDuringPreg).getEditText().setOnFocusChangeListener(this);
        aqVP.id(R.id.etDeadAfterDel).getEditText().setOnFocusChangeListener(this);
    }

    //	initialize db object
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    //	set the initial view of screen
    private void setInitialView() {

        try {


            populateEDD(aqVP.id(R.id.etlmp).getText().toString());


            aqVP.id(R.id.etthaicard).enabled(false).background(R.drawable.spinner_bg_disabled); //12Aug2019 - Bindu disabledstate
            aqVP.id(R.id.etthaicardregdate).enabled(false).background(R.drawable.spinner_bg_disabled); //12Aug2019 - Bindu disabledstate
            /*aqVP.id(R.id.ettotabort).enabled(false);
            aqVP.id(R.id.ettotpreg).enabled(false);
            aqVP.id(R.id.ettotlivebirth).enabled(false);
            aqVP.id(R.id.ethomedel).enabled(false);
            aqVP.id(R.id.etnooflivechildren).enabled(false);
            aqVP.id(R.id.etlastchildWeight).enabled(false);
            aqVP.id(R.id.etlastchildage).enabled(false);
            aqVP.id(R.id.radiofemale1).enabled(false);
            aqVP.id(R.id.radiomale1).enabled(false);
            aqVP.id(R.id.ettotpreg).enabled(false);
            aqVP.id(R.id.ettotpreg).background(R.drawable.edittext_disable);
            aqVP.id(R.id.ettotlivebirth).background(R.drawable.edittext_disable);
            aqVP.id(R.id.ettotabort).background(R.drawable.edittext_disable);
            aqVP.id(R.id.ethomedel).background(R.drawable.edittext_disable);
            aqVP.id(R.id.etDeadDuringPreg).background(R.drawable.edittext_disable);
            aqVP.id(R.id.etDeadAfterDel).background(R.drawable.edittext_disable);
            aqVP.id(R.id.etlastchildage).background(R.drawable.edittext_disable);
            aqVP.id(R.id.etlastchildWeight).background(R.drawable.edittext_disable);
            aqVP.id(R.id.etnooflivechildren).background(R.drawable.edittext_disable);
            aqVP.id(R.id.etDeadDuringPreg).enabled(false);
            aqVP.id(R.id.etDeadAfterDel).enabled(false);*/
            /*aqVP.id(R.id.txtheadingreg).text(getResources().getString(R.string.viewprofile));*/
            if (Integer.parseInt(aqVP.id(R.id.ettotpreg).getText().toString()) == 1
                    && aqVP.id(R.id.radiopreg).isChecked())  //15Nov2019 Arpitha)

            if(regData.getRegPregnantorMother()==2)
            {
                aqVP.id(R.id.tvlmp).gone();
                aqVP.id(R.id.etlmp).gone();
                aqVP.id(R.id.tvedd).gone();
                aqVP.id(R.id.tvpregstatus).gone();

                aqVP.id(R.id.trlmp).visible();
                aqVP.id(R.id.tredd).visible();

                aqVP.id(R.id.etlmpmother).enabled(false).background(R.drawable.edittext_disable);
                aqVP.id(R.id.etlmpmother).enabled(false);
                aqVP.id(R.id.eteddmother).enabled(false).background(R.drawable.edittext_disable);
                aqVP.id(R.id.eteddmother).enabled(false);
            } else { //21Mar2021 Bindu - hide/show lmp and edd
                aqVP.id(R.id.trlmp).visible();
                aqVP.id(R.id.tredd).visible();
                aqVP.id(R.id.trgestage).visible();

                //21Mar2021 - Bindu - Check if any services is provided
                WomanServiceRepository serviceRepository = new WomanServiceRepository(databaseHelper);
                boolean isServiceProvided = serviceRepository.getWomanServiceProvided(regData.getWomanId());
                if (isServiceProvided || (regData.getRegADDate() != null && regData.getRegADDate().length() > 0)) {
                    aqVP.id(R.id.etlmpmother).enabled(false).background(R.drawable.edittext_disable);
                    aqVP.id(R.id.etlmpmother).enabled(false);
                    aqVP.id(R.id.eteddmother).enabled(false).background(R.drawable.edittext_disable);
                    aqVP.id(R.id.eteddmother).enabled(false);
                    aqVP.id(R.id.etgest).enabled(false).background(R.drawable.edittext_disable);
                    aqVP.id(R.id.etgest).enabled(false);
                }
            }
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }

    }

    // Calculate EDD and display in EDD Edittext
    private void populateEDD(String xLMP) throws Exception {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        Date LmpDate;
        LmpDate = sdf.parse(xLMP);
        Calendar cal = Calendar.getInstance();
        cal.setTime(LmpDate);
        cal.add(Calendar.DAY_OF_MONTH, 280);

        int day = cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH) + 1;
        int year = cal.get(Calendar.YEAR);

        if (regData.getRegEDD() == null || regData.getRegEDD().length() == 0) {
            eddDate = String.format("%02d", day) + "-" + String.format("%02d", month) + "-" + year;
            aqVP.id(R.id.tvEddValue).text(eddDate);

            int noOfDays = getNumberOfDaysFromLMP(xLMP);
            int noOfWeeks = noOfDays / 7;
            gestationAge = noOfWeeks;
            int days = noOfDays % 7;
            String primi = "";
            if (Integer.parseInt(aqVP.id(R.id.ettotpreg).getText().toString()) == 1) {
                primi = getResources().getString(R.string.primi);

            }
            if (days > 0) { //21Mar2021 Bindu set gest age in edittext
                if (days == 1) {
                    aqVP.id(R.id.tvpregstatus).text((noOfWeeks + "" + getResources().getString(R.string.weeks) + " " + days + " " + getResources().getString(R.string.day) + " " + primi));
                    aqVP.id(R.id.etgest).text((noOfWeeks + "" + getResources().getString(R.string.weeks) + " " + days + " " + getResources().getString(R.string.day)));
                }
                else {
                    aqVP.id(R.id.tvpregstatus).text((noOfWeeks + "" + getResources().getString(R.string.weeks) + " " + days + " " + getResources().getString(R.string.days) + " " + primi));
                    aqVP.id(R.id.etgest).text((noOfWeeks + "" + getResources().getString(R.string.weeks) + " " + days + " " + getResources().getString(R.string.days)));
                }
            } else {
                aqVP.id(R.id.tvpregstatus).text((noOfWeeks + "" + getResources().getString(R.string.weeks) + " " + primi));
                aqVP.id(R.id.etgest).text((noOfWeeks + "" + getResources().getString(R.string.weeks)));
            }
        } else {

            String primi = "";
            if (Integer.parseInt(aqVP.id(R.id.ettotpreg).getText().toString()) == 1) {
                primi = getResources().getString(R.string.primi);
                   disableLastChildFields();  //26Mar2021 Bindu - disable last child info for primi
            }
            aqVP.id(R.id.tvEddValue).text(regData.getRegEDD());
            eddDate = regData.getRegEDD();
            CalculateLMP(regData.getRegEDD());
            //int noOfWeeks = DateTimeUtil.getNumberOfWeeksFromLMP(xLMP);


            //21MAr2021 Bindu set gest age - days
            int noOfDays = getNumberOfDaysFromLMP(xLMP);
            int noOfWeeks = noOfDays / 7;
            gestationAge = noOfWeeks;
            int days = noOfDays % 7;
            aqVP.id(R.id.tvpregstatus)
                    .text((noOfWeeks + "" + getResources().getString(R.string.weeks) + " " + primi));
            if (days > 0) {
                if (days == 1) {
                    aqVP.id(R.id.etgest).text((noOfWeeks + "" + getResources().getString(R.string.weeks) + " " + days + " " + getResources().getString(R.string.day)));
                }
                else {
                    aqVP.id(R.id.etgest).text((noOfWeeks + "" + getResources().getString(R.string.weeks) + " " + days + " " + getResources().getString(R.string.days)));
                }
            }else
                aqVP.id(R.id.etgest).text(noOfWeeks + "" + getResources().getString(R.string.weeks));
        }
    }

    //	calculate LMP
    private void CalculateLMP(String EDDate) throws Exception {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        Date LMP;
        LMP = sdf.parse(EDDate);
        Calendar cal = Calendar.getInstance();
        cal.setTime(LMP);
        cal.add(Calendar.DAY_OF_MONTH, -280);

        int day = cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH) + 1;
        int year = cal.get(Calendar.YEAR);

        String lmpDate = String.format("%02d", day) + "-" + String.format("%02d", month) + "-" + year;
        aqVP.id(R.id.etlmp).text(lmpDate);
    }


    //	get data from database
    void getData() {
        try {
            WomanRepository womanRepository = new WomanRepository(databaseHelper);
            regData = womanRepository.getRegistartionDetails(regData.getWomanId(),
                    user.getUserId());
            if (regData != null) {

                if(regData.getIsLMPConfirmed() == 1) {
                    aqVP.id(R.id.llFooter).gone();
                }else
                    aqVP.id(R.id.llFooter).visible();

                aqVP.id(R.id.ettotpreg).text("" + regData.getRegGravida());
                aqVP.id(R.id.ettotlivebirth).text("" + regData.getRegPara());
                aqVP.id(R.id.etnooflivechildren).text("" + regData.getRegLiveChildren());
                aqVP.id(R.id.ettotabort).text("" + regData.getRegAbortions());
                aqVP.id(R.id.ethomedel).text("" + regData.getRegNoofHomeDeliveries());
                aqVP.id(R.id.etlastchildage).text("" + regData.getRegLastChildAge());
                aqVP.id(R.id.etlastchildWeight).text("" + (regData.getRegLastChildWeight() > 0 ?
                        regData.getRegLastChildWeight() : ""));
                aqVP.id(R.id.etDeadDuringPreg).text("" + regData.getRegChildMortPreg());
                aqVP.id(R.id.etDeadAfterDel).text("" + regData.getRegChildMortDel());

                uidType = regData.getRegUIDType();
                if (regData.getRegUIDType().equalsIgnoreCase(getResources().getString(R.string.strrchid)))
                    //aqVP.id(R.id.tvthaicard).text("RCHID(Unique)");
                    aqVP.id(R.id.tvthaicard).text(getResources().getString(R.string.rchidlbl)); //15Nov2019 -  Bindu
                else
                    aqVP.id(R.id.tvthaicard).text(getResources().getString(R.string.tvthaicard));
                aqVP.id(R.id.etthaicard).text(regData.getRegUIDNo());







                if (regData.getRegLastChildGender() != null && regData.getRegLastChildGender().equalsIgnoreCase("Male"))
                    aqVP.id(R.id.radiomale1).checked(true);
                else if (regData.getRegLastChildGender() != null && regData.getRegLastChildGender().equalsIgnoreCase("Female"))
                    aqVP.id(R.id.radiofemale1).checked(true);



                aqVP.id(R.id.etregdate).text(regData.getRegRegistrationDate());
                if(regData.getRegPregnantorMother()!=2) {//28Nov2019 Arpitha
                    aqVP.id(R.id.etlmp).text(regData.getRegLMP());
                    aqVP.id(R.id.tvEddValue).text(regData.getRegEDD());
                    aqVP.id(R.id.tvmotherorpregnant).text(getResources().getString(R.string.radiopreg));
                    //21Mar2021 Bindu
                    aqVP.id(R.id.etlmpmother).text(regData.getRegLMP());
                    aqVP.id(R.id.eteddmother).text(regData.getRegEDD());
                }else//28Nov2019 Arpitha
                {
                    aqVP.id(R.id.etlmp).text(regData.getRegADDate());
                    aqVP.id(R.id.tvEddValue).gone();
                    aqVP.id(R.id.tvmotherorpregnant).text(getResources().getString(R.string.radiomother)+"("+getResources().getString(R.string.txtADD)+":"+regData.getRegADDate()+")");

                }

                if (regData.getRegpregormotheratreg() == 1)
                    aqVP.id(R.id.regpregormother).text(getResources().getString(R.string.regpreg));
                else
                    aqVP.id(R.id.regpregormother).text(getResources().getString(R.string.regmother));
                aqVP.id(R.id.tvpregstatus).text(regData.getRegStatusWhileRegistration());




                if (regData.getRegWomenImage() != null && regData.getRegWomenImage().trim().length() > 0) {

                    byte[] encodeByte = Base64.decode(regData.getRegWomenImage(), Base64.DEFAULT);
                    Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);


                    if (bitmap!=null){ //Ramesh 3-8-2021 If image is null, setting default image
                        aqVP.id(R.id.imgwomenphoto).getImageView().setImageBitmap(bitmap);
                    }else {
                        aqVP.id(R.id.imgwomenphoto).getImageView().setImageResource(R.drawable.women);
                    }
                }

                aqVP.id(R.id.etlmpmother).text(regData.getRegLMP());
                aqVP.id(R.id.eteddmother).text(regData.getRegEDD());
            }
        } catch (SQLException e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }







    /**
     * This method handles all Click listners, calls - all validation methods
     * and confrimAlert()
     */
    @Override
    public void onClick(View v) {

        try {
            switch (v.getId()) {
                case R.id.imgwomenphoto: {
                    //CapturingImage();
                    break;
                }
                case R.id.radiomale1: {
                    lastChildGender = "Male";
                    break;
                }
                case R.id.radiofemale1: {
                    lastChildGender = "Female";
                    break;
                }
                case R.id.btnsave: {



                        if (regData.getCurrentWomenStatus() != null) {
                            if (regData.getCurrentWomenStatus().equalsIgnoreCase("WT")) {
                                Toast.makeText(getApplicationContext(), (getResources().getString(R.string.m104)),
                                        Toast.LENGTH_LONG).show();
                            } else if (regData.getCurrentWomenStatus().equalsIgnoreCase("WA")) {
                                Toast.makeText(getApplicationContext(), (getResources().getString(R.string.m104)),
                                        Toast.LENGTH_LONG).show();
                            }
                            if (regData.getCurrentWomenStatus().equalsIgnoreCase("WE")) {
                                if (regData.getRegADDate() != null && regData.getRegADDate().length() > 0) {
                                    boolean ischilddead = false;


                                    if (ischilddead)
                                        Toast.makeText(getApplicationContext(),
                                                (getResources().getString(R.string.m104)), Toast.LENGTH_LONG).show();
                                    else
                                        checkEditApplicable();
                                } else
                                    Toast.makeText(getApplicationContext(), (getResources().getString(R.string.m104)),
                                            Toast.LENGTH_LONG).show();
                            }
                        } else {
                            checkEditApplicable();
                        }




                        break;

                    }

                case R.id.btnclear: {
                    confirmAlertExit();
                    break;
                }


                //22Mar2021 Bindu - set click listner for img clear lmp and edd
                //                    20Aug2019 Arpitha
                case R.id.imgclearlmpdate:

                case R.id.imgclearedddate:
                    aqVP.id(R.id.etlmpmother).text("");
                    aqVP.id(R.id.eteddmother).text("");
                    aqVP.id(R.id.etgest).text("");
                    aqVP.id(R.id.imgclearlmpdate).gone();
                    aqVP.id(R.id.imgclearedddate).gone();
                    break;
                default:
                    break;
            }
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }


    /**
     * This method invokes camera intent
     */
    @SuppressLint("SimpleDateFormat")
    protected void CapturingImage() {

        try {
            // intent to start device camera
            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            Date xDate = new Date();
            String lasmod = new SimpleDateFormat("dd-hh-mm-ss").format(xDate);

            String fileName = "iWomen" + lasmod;
            File imageDir = new File(AppState.imgDirRef);

            if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
                if (imageDir != null) {
                    if (!imageDir.mkdirs()) {
                        if (!imageDir.exists()) {
                            Log.d("CameraSample", "failed to create directory");
                        } else {
                            Log.d("CameraSample", "created directory");
                        }
                    }
                }
            } else {
                Log.v(getString(R.string.app_name), "External storage is not mounted READ/WRITE.");
            }

            path = imageDir + "/" + fileName + ".jpg";
            imgFileBeforeResult = new File(path);

            outputFileUri = Uri.fromFile(imgFileBeforeResult);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            startActivityForResult(intent, 0);
        } catch (NullPointerException e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }


    /**
     * This method get the result from Camera Intent, gets the image
     */
    @SuppressLint("SimpleDateFormat")
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            wImage = "";
            if (requestCode == IMAGE_CAPTURE) {
                if (resultCode == RESULT_OK) {


                    baos = new ByteArrayOutputStream();
                    Bitmap bitmap = BitmapFactory.decodeFile(path);
                    Bitmap resizedBmp = Bitmap.createScaledBitmap(bitmap, 230, 210, true);
                    resizedBmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    aqVP.id(R.id.imgwomenphoto).getImageView().setImageBitmap(resizedBmp);


                    ByteArrayOutputStream outputstream = new ByteArrayOutputStream();
                    resizedBmp.compress(Bitmap.CompressFormat.PNG, 0, outputstream);
                    wImage = Base64.encodeToString(outputstream.toByteArray(), Base64.DEFAULT);


                } else if (resultCode == RESULT_CANCELED) {
                    deleteImages();
                }
            } else {
                Toast.makeText(getApplicationContext(), (getResources().getString(R.string.m100)),
                        Toast.LENGTH_LONG).show();
            }
        } catch (NullPointerException e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    /**
     * Delete the temporarily stored Images
     */
    private void deleteImages() {

        File imgfileAfterResult = null;
        if (imgfileAfterResult != null)
            imgfileAfterResult.delete();
        if (imgFileBeforeResult != null)
            imgFileBeforeResult.delete();
    }

    //	disable last child fields
    private void disableLastChildFields() {
        aqVP.id(R.id.tvlastchildage).enabled(false);
        aqVP.id(R.id.etlastchildage).enabled(false);
        aqVP.id(R.id.tvlastchildWeight).enabled(false);
        aqVP.id(R.id.etlastchildWeight).enabled(false);
        aqVP.id(R.id.tvchildsex).enabled(false);
        aqVP.id(R.id.radiofemale1).enabled(false);

        aqVP.id(R.id.radiomale1).enabled(false);

        aqVP.id(R.id.etlastchildage).background(R.drawable.edittext_disable);
        aqVP.id(R.id.etlastchildWeight).background(R.drawable.edittext_disable);

        /*aqVP.id(R.id.etDeadDuringPreg).enabled(false);
        aqVP.id(R.id.etDeadAfterDel).enabled(false);
        aqVP.id(R.id.etDeadDuringPreg).background(R.drawable.edittext_disable);
        aqVP.id(R.id.etDeadAfterDel).background(R.drawable.edittext_disable);*/
    }




    //	validations done for fields
    private boolean validateRegFields() throws Exception {
        focusEditText = null;

        if (aqVP.id(R.id.etlmpmother).getText().toString().trim().length() <= 0 || aqVP.id(R.id.eteddmother).getText().toString().trim().length() <= 0) {
            aqVP.id(R.id.etlmpmother).getEditText().requestFocus();
            Toast.makeText(getApplicationContext(), (getResources().getString(R.string.plsenterlmporedd)), Toast.LENGTH_LONG)
                    .show();
            focusEditText = aqVP.id(R.id.etlmpmother).getEditText();
            return false;
        }
        return true;
    }


    /**
     * This is a Recursive method that traverse through the group and subgroup
     * of view Sets text
     *
     * @throws Exception
     */
    private ArrayList<View> initializeScreen(View v) throws Exception {

        if (!(v instanceof ViewGroup)) {
            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);

            // TextView - Assign Labels
            if (v.getClass() == TextView.class || v.getClass() == CheckBox.class) {
                aqVP.id(v.getId()).text((aqVP.id(v.getId()).getText().toString()));
            }
            if (v.getClass() == AppCompatRadioButton.class) {
                aqVP.id(v.getId()).getButton().setOnClickListener(this);
                aqVP.id(v.getId()).text((aqVP.id(v.getId()).getText().toString()));
            }
            // EditText hint - Assign Labels
            if (v.getClass() == EditText.class) {
                if (aqVP.id(v.getId()).getEditText().getHint() != null)
                    aqVP.id(v.getId()).getEditText().setHint((aqVP.id(v.getId()).getEditText().getHint() + ""));
            }
            // ImageButton/ImageView - OnClickListener commonClick
            if (v.getClass() == AppCompatImageButton.class || v.getClass() == AppCompatImageView.class) {
                aqVP.id(v.getId()).getImageView().setOnClickListener(this);
            }
            // ImageButton/ImageView - OnClickListener commonClick
            if (v.getClass() == AppCompatCheckBox.class || v.getClass() == AppCompatCheckBox.class) {
                aqVP.id(v.getId()).getCheckBox().setOnClickListener(this);
            }

            return viewArrayList;
        }
        if (v instanceof Spinner) {
            aqVP.id(v.getId()).itemSelected(this, "onSpinnerClicked");
        }
        if (v instanceof SearchableSpinner) {
            aqVP.id(v.getId()).itemSelected(this, "onSpinnerClicked");
        }

        ArrayList<View> result = new ArrayList<View>();

        ViewGroup viewGroup = (ViewGroup) v;
        for (int i = 0; i < viewGroup.getChildCount(); i++) {

            View child = viewGroup.getChildAt(i);

            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);
            viewArrayList.addAll(initializeScreen(child));

            result.addAll(viewArrayList);
        }
        return result;
    }

    //	check for edit applicability
    private void checkEditApplicable() throws Exception {

        if (validateRegFields()) {
            confirmAlert();
        }

    }

    /**
     * Confirm Alert on Click save
     */
    private void confirmAlert() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set dialog message
        alertDialogBuilder.setMessage((getResources().getString(R.string.reconfirmlmp))).setCancelable(false)
                .setNegativeButton((getResources().getString(R.string.confirm)),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                try {
                                    btnEditWomanData();
                                } catch (Exception e) {

                                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                                    crashlytics.log(e.getMessage());
                                }
                            }
                        })
                .setPositiveButton((getResources().getString(R.string.discard)),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    //  set to pojo class
    private void setEditProfilePojo() {

        try {
            woman = new tblregisteredwomen();
            woman.setUserId(user.getUserId());
            woman.setWomanId(regData.getWomanId());
            woman.setRegRegistrationDate(regData.getRegRegistrationDate());
            woman.setRegLMP(aqVP.id(R.id.etlmpmother).getText().toString());
            woman.setRegEDD(aqVP.id(R.id.eteddmother).getText().toString());
            woman.setRegADDate(regData.getRegADDate());

            woman.setRegStatusWhileRegistration(aqVP.id(R.id.tvpregstatus).getText().toString());
            woman.setIsLMPConfirmed(1);


            /*woman.setRegGravida(aqVP.id(R.id.ettotpreg).getText().toString().length() > 0 ?
                    Integer.parseInt(aqVP.id(R.id.ettotpreg).getText().toString()) : 0);
            woman.setRegPara(aqVP.id(R.id.ettotlivebirth).getText().toString().length() > 0 ?
                    Integer.parseInt(aqVP.id(R.id.ettotlivebirth).getText().toString()) : 0);
            woman.setRegLiveChildren(aqVP.id(R.id.etnooflivechildren).getText()
                    .toString().length() > 0 ?
                    Integer.parseInt(aqVP.id(R.id.etnooflivechildren).getText().toString()) : 0);
            woman.setRegAbortions(aqVP.id(R.id.ettotabort).getText().toString().length() > 0 ?
                    Integer.parseInt(aqVP.id(R.id.ettotabort).getText().toString()) : 0);
            woman.setRegChildMortPreg(aqVP.id(R.id.etDeadDuringPreg).getText().toString().length() > 0 ?
                    Integer.parseInt(aqVP.id(R.id.etDeadDuringPreg).getText().toString()) : 0);
            woman.setRegChildMortDel(aqVP.id(R.id.etDeadAfterDel).getText().toString().length() > 0 ?
                    Integer.parseInt(aqVP.id(R.id.etDeadAfterDel).getText().toString()) : 0);
            woman.setRegNoofHomeDeliveries(aqVP.id(R.id.ethomedel).getText().toString().length() > 0 ?
                    Integer.parseInt(aqVP.id(R.id.ethomedel).getText().toString()) : 0);*/
            woman.setRegLastChildAge(aqVP.id(R.id.etlastchildage).getText().toString());  //05Aug2019 - Bindu Int to String
            woman.setRegLastChildWeight(aqVP.id(R.id.etlastchildWeight).getText().toString().length() > 0 ?
                    Integer.parseInt(aqVP.id(R.id.etlastchildWeight).getText().toString()) : 0);


            woman.setRegLastChildGender(lastChildGender);



            woman.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());

        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }

    }

    //  update details to table
    void btnEditWomanData() {

        try {

            setEditProfilePojo();

            final TransactionHeaderRepository transRepo = new TransactionHeaderRepository(databaseHelper);
            final int transId = transRepo.iCreateNewTrans(appState.sessionUserId, databaseHelper);

            TransactionManager.callInTransaction(databaseHelper.getConnectionSource(), new Callable<Void>() {
                public Void call() throws Exception {//29Aug2019 Arpitha - transaction manager

                    String upSql = checkForAuditTrailNew(transId);
                    if (upSql != null) {   //10Aug2019 - Bindu - check for update st and then cal update, else exit to reg women
                        transRepo.iNewRecordTransNew(appState.sessionUserId, transId, "tblaudittrail", databaseHelper);

                        WomanRepository womanRepo = new WomanRepository(databaseHelper);
                        int sql = womanRepo.updateWomenRegDataNew(woman.getUserId(), upSql, transId, editedValues, databaseHelper);
                        if (sql > 0) {

                            boolean added;
                                WomanServiceRepository womanServiceRepository = new WomanServiceRepository(databaseHelper);
                                added = womanServiceRepository.addServicesNew(woman, transId, appState.userType, databaseHelper, null, null);// 24Oct2019 Arpitha

                            if(added) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.updatewoman), Toast.LENGTH_LONG).show();

                                // MainMenuActivity.callSyncMtd(); //12Aug2019 - Bindu - Comment autosync
                                Intent exit = new Intent(ReconfirmationRegActivity.this, RegisteredWomenActionTabs.class);
                                exit.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                startActivity(exit);
                            }else {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.m113), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.m113), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.nochangesmadetoedit), Toast.LENGTH_LONG).show();
                        Intent exit = new Intent(ReconfirmationRegActivity.this, RegisteredWomenActionTabs.class);
                        exit.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        startActivity(exit);
                    }
                    return null;
                }
            });

        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.m113), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }




    /**
     * Setting AuditTrail pojo, calls - InserttblAuditTrail()
     *
     * @param transId
     */
    private boolean InserttblAuditTrail(String ColumnName, String Old_Value, String
            New_Value, int transId, byte[] image) throws Exception {

        APJ = new AuditPojo();
        APJ.setUserId(user.getUserId());
        APJ.setWomanId(regData.getWomanId());
        APJ.setTblName("tblregisteredwomen");
        APJ.setDateChanged(DateTimeUtil.getTodaysDate());
        APJ.setColumnName(ColumnName);
        APJ.setOld_Value(Old_Value);
        APJ.setNew_Value(New_Value);
        APJ.setTransId(transId);
//15Aug2019 - Bindu - set recordcreateddate
        APJ.setRecordcreateddate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());

        AuditRepository auditRepository = new AuditRepository(databaseHelper);
        return auditRepository.insertTotblAuditTrailNew(APJ, databaseHelper);

    }

    // insert to audi trail table
    private String checkForAuditTrailNew(int transId) throws Exception {

        String upSql = null;
        String addString = "";
        editedValues = "";

        addString = "isLMPConfirmed = " + (char) 34 + woman.getIsLMPConfirmed() + (char) 34 + "";
        InserttblAuditTrail("isLMPConfirmed",
                "0", ""+woman.getIsLMPConfirmed(),
                transId, null);

        //21Mar2021 Bindu - add LMP and EDD
        if (!regData.getRegLMP().equalsIgnoreCase
                (aqVP.id(R.id.etlmpmother).getText().toString())) {
            addString = addString + " ,regLMP = " + (char) 34 + woman.getRegLMP() + (char) 34 + "";
            InserttblAuditTrail("regLMP",
                    regData.getRegLMP(), woman.getRegLMP(),
                    transId, null);
        }

        if (!regData.getRegEDD().equalsIgnoreCase
                (aqVP.id(R.id.eteddmother).getText().toString())) {
            if (addString == "")
                addString = addString + " regEDD = " + (char) 34 + woman.getRegEDD() + (char) 34 + "";
            else
                addString = addString + " ,regEDD = " + (char) 34 + woman.getRegEDD() + (char) 34 + "";

            InserttblAuditTrail("regEDD",
                    regData.getRegEDD(), woman.getRegEDD(),
                    transId, null);
        }

        if (!regData.getRegLastChildAge().equalsIgnoreCase
                (aqVP.id(R.id.etlastchildage).getText().toString())) {
            if (addString == "")
                addString = addString + " regLastChildAge = " + (char) 34 + woman.getRegLastChildAge() + (char) 34 + "";
            else
                addString = addString + " ,regLastChildAge = " + (char) 34 + woman.getRegLastChildAge() + (char) 34 + "";

            InserttblAuditTrail("regLastChildAge",
                    regData.getRegLastChildAge(), woman.getRegLastChildAge(),
                    transId, null);
        }

        if (regData.getRegLastChildWeight() != woman.getRegLastChildWeight()) {
            if (addString == "")
                addString = addString + " regLastChildWeight = " + (char) 34 + woman.getRegLastChildWeight() + (char) 34 + "";
            else
                addString = addString + " ,regLastChildWeight = " + (char) 34 + woman.getRegLastChildWeight() + (char) 34 + "";

            InserttblAuditTrail("regLastChildWeight",
                    ""+regData.getRegLastChildWeight(), ""+woman.getRegLastChildWeight(),
                    transId, null);
        }

        if (!regData.getRegLastChildGender().equalsIgnoreCase
                (woman.getRegLastChildGender())) {
            if (addString == "")
                addString = addString + " regLastChildGender = " + (char) 34 + woman.getRegLastChildGender() + (char) 34 + "";
            else
                addString = addString + " ,regLastChildGender = " + (char) 34 + woman.getRegLastChildGender() + (char) 34 + "";

            InserttblAuditTrail("regLastChildGender",
                    regData.getRegLastChildGender(), woman.getRegLastChildGender(),
                    transId, null);
        }

        //13Aug2019 - Bindu set recordupdatedate for audit
        if (addString == "")
            addString = addString + " RecordUpdatedDate = " + (char) 34 + woman.getRecordUpdatedDate() + (char) 34 + "";
        else
            addString = addString + " ,RecordUpdatedDate = " + (char) 34 + woman.getRecordUpdatedDate() + (char) 34 + "";
        InserttblAuditTrail("RecordUpdatedDate",
                String.valueOf(regData.getRecordUpdatedDate()),
                String.valueOf(woman.getRecordUpdatedDate()),
                transId, null);

        if (addString != null && addString.length() > 0) {
            upSql = "UPDATE tblregisteredwomen SET ";
            upSql = upSql + addString + " WHERE WomanId = '"
                    + appState.selectedWomanId + "' and UserId = '" + woman.getUserId() + "'";
        }
        return upSql;
    }


    /**
     * This method handle touch listners, calls - displayDatePicker()
     */
    @Override
    public boolean onTouch(View v, MotionEvent event) {

        if (MotionEvent.ACTION_UP == event.getAction()) {
            try {
                switch (v.getId()) {

                    //21Mar2021 Bindu - add lmp and Edd edittext
                    case R.id.etlmpmother: {
                        isLmpDate = true;
                        isDob = false;
                        isMarriageDate = false;
                        isEddDate = false;
                        assignEditTextAndCallDatePicker(aqVP.id(R.id.etlmpmother).getEditText());
                        break;
                    }
                    case R.id.eteddmother: {
                        isEddDate = true;
                        isLmpDate = false;
                        isDob = false;
                        isMarriageDate = false;
                        assignEditTextAndCallDatePicker(aqVP.id(R.id.eteddmother).getEditText());
                        break;
                    }
                    default:
                        break;
                }
            } catch (Exception e) {

                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
        }
        return true;
    }


    /**
     * This method opens the Date Dialog and sets Date to respective EditText
     */
    private void assignEditTextAndCallDatePicker(EditText editText) {
        String currDate = (editText.getText().length() > 0) ? editText.getText().toString() : "";
        CommonDatePickerFragment.getCommonDate(this, currDate);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {

        try {
            if (view.isShown()) {
                String bldSmpldate = null;
                bldSmpldate = String.format("%02d", day) + "-" + String.format("%02d", month + 1) + "-"
                        + year;
                //21Mar2021 Bindu LMP validation
                if (isLmpDate) {
                    String lmporadd = bldSmpldate;
                    int daysdiff = 0;
                    String regdate = aqVP.id(R.id.etregdate).getText().toString();

                    if (regdate != null && regdate.trim().length() > 0) {
                        daysdiff = DateTimeUtil.getDaysBetweenDates(regdate, lmporadd);

                        int daysdifffromCUrr = DateTimeUtil
                                .getDaysBetweenDates(DateTimeUtil.getTodaysDate(), lmporadd);

                        if (daysdiff <= 30) {
                            Toast.makeText(getApplicationContext(),
                                    getResources().getString(R.string.date_val_3_toast),
                                    Toast.LENGTH_LONG).show();
                            aqVP.id(R.id.etlmpmother).text("");
                            aqVP.id(R.id.etgest).text("");
                            aqVP.id(R.id.eteddmother).text("");
                            aqVP.id(R.id.imgclearlmpdate).gone();
                        } else if (daysdifffromCUrr > 280) {
                            Toast.makeText(getApplicationContext(),
                                    getResources().getString(R.string.date_val_4_toast),
                                    Toast.LENGTH_LONG).show();
                            aqVP.id(R.id.etlmpmother).text("");
                            aqVP.id(R.id.etgest).text("");
                            aqVP.id(R.id.eteddmother).text("");
                            aqVP.id(R.id.imgclearlmpdate).gone();
                        } else {
                            aqVP.id(R.id.etlmpmother).text(bldSmpldate);
                            calculateEddGestation("lmp");
                            aqVP.id(R.id.imgclearlmpdate).visible();//20Aug2019 Arpitha
                        }
                    } else
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.plz_enter_reg_dt),
                                Toast.LENGTH_LONG).show();
                }
                if (isEddDate) {
                    int days1 = 0;
                    if (aqVP.id(R.id.etregdate).getText().toString().trim().length() > 0)
                        days1 = DateTimeUtil.getDaysBetweenDates(bldSmpldate,
                                aqVP.id(R.id.etregdate).getText().toString());

                    if (days1 > 0 && days1 < 273) {
                        aqVP.id(R.id.eteddmother).text(bldSmpldate);
                        calculateEddGestation("edd");
                        aqVP.id(R.id.imgclearedddate).visible();
                    } else {
                        if (days1 > 280) {
                            aqVP.id(R.id.eteddmother).text("");
                            aqVP.id(R.id.imgclearedddate).gone();
                            aqVP.id(R.id.eteddmother).getEditText().requestFocus();
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.date_val_6_toast),

                                    Toast.LENGTH_LONG).show();

                            aqVP.id(R.id.etlmpmother).text("");
                            aqVP.id(R.id.etgest).text("");

                        } else {
                            aqVP.id(R.id.eteddmother).text("");
                            aqVP.id(R.id.imgclearedddate).gone();//21Aug2019 Arpitha
                            aqVP.id(R.id.eteddmother).getEditText().requestFocus();
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.date_val_5_toast),
                                    Toast.LENGTH_LONG).show();
                            aqVP.id(R.id.etlmpmother).text("");
                            aqVP.id(R.id.etgest).text("");
                        }
                    }
                }
            }
        } catch (ParseException e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }


    /**
     * This method handles focus change listners Calls - calTotPregnancies(),
     */
    @Override
    public void onFocusChange(View v, boolean hasFocus) {

        try {
            switch (v.getId()) {

               /* case R.id.ettotlivebirth: {
                    calTotPregnancies();
                    int totLiveChild = (aqVP.id(R.id.ettotlivebirth).getText().toString().length() > 0)
                            ? Integer.parseInt(aqVP.id(R.id.ettotlivebirth).getText().toString()) : 0;
                    if (totLiveChild <= 0) {
                        aqVP.id(R.id.etlastchildage).text("");
                        aqVP.id(R.id.etlastchildWeight).text("");
                        aqVP.id(R.id.radiofemale1).checked(false);
                        aqVP.id(R.id.radiomale1).checked(false);
                        aqVP.id(R.id.tvlastchildage).enabled(false);
                        aqVP.id(R.id.etlastchildage).enabled(false);
                        aqVP.id(R.id.tvlastchildWeight).enabled(false);
                        aqVP.id(R.id.etlastchildWeight).enabled(false);
                        aqVP.id(R.id.tvchildsex).enabled(false);
                        aqVP.id(R.id.radiofemale1).enabled(false);
                        aqVP.id(R.id.radiomale1).enabled(false);
                    }
                    break;
                }
                case R.id.ettotabort: {
                    calTotPregnancies();
                    ColorCount();
                    break;
                }
                case R.id.etDeadDuringPreg: {
                    calTotPregnancies();
                    break;
                }*/


                /*case R.id.ettotpreg:
                    ColorCount();
                    break;*/

                default:
                    break;
            }
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }


    @Override
    public void onBackPressed() {

    }


    TextWatcher watcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            try {


                // 31Oct2018 - Bindu
                 /*if (s == aqVP.id(R.id.ettotpreg).getEditable()) {
                    // 07Nov2018 - Bindu - para > gravida chk validation
                    int para = 0;
                    if (aqVP.id(R.id.ettotlivebirth).getText().toString().length() > 0)
                        para = Integer.parseInt(aqVP.id(R.id.ettotlivebirth).getText().toString());
                    if (aqVP.id(R.id.ettotpreg).getText().toString().length() > 0) {
                        if (para > Integer.parseInt(aqVP.id(R.id.ettotpreg).getText().toString())) {
                            aqVP.id(R.id.ettotlivebirth).getEditText().setError("Para cannot be greater than gravida");
                            aqVP.id(R.id.ettotlivebirth).text("");
                        }
                    }*/

                    /*if (woman.getRegPregnantorMother() == 0) {
                        if ((woman.getRegGravida() != 1)) {
                            if (aqVP.id(R.id.ettotpreg).getText().toString().length() > 0) {
                                if (Integer.parseInt(aqVP.id(R.id.ettotpreg).getText().toString()) < 2) {
                                    aqVP.id(R.id.ettotpreg).getEditText()
                                            .setError("Gravida cannot be less than 2 since its not a primi case");

                                }
                            }
                        }
                    } else {
                        if (aqVP.id(R.id.ettotpreg).getText().toString().length() > 0) {
                            if (Integer.parseInt(aqVP.id(R.id.ettotpreg).getText().toString()) < 1) {
                                aqVP.id(R.id.ettotpreg).getEditText().setError("Gravida cannot be 0");
                            }
                        }
                    }

                } else if (s == aqVP.id(R.id.ettotlivebirth).getEditable()) {

                    if (woman.getRegGravida() == 1 && gestationAge >= 24) {
                        if (aqVP.id(R.id.ettotlivebirth).getText().toString().length() > 0) {
                            if (Integer.parseInt(aqVP.id(R.id.ettotlivebirth).getText().toString()) > 1) {
                                aqVP.id(R.id.ettotlivebirth).getEditText()
                                        .setError("Para cannot be greater than gravida");
                                aqVP.id(R.id.ettotlivebirth).text("");

                            }
                        }
                    } else {
                        int gravida = 0;
                        if (aqVP.id(R.id.ettotpreg).getText().toString().length() > 0)
                            gravida = Integer.parseInt(aqVP.id(R.id.ettotpreg).getText().toString());
                        if (woman.getRegPregnantorMother() == 0) {
                            if (gestationAge >= 24) {
                                if (aqVP.id(R.id.ettotlivebirth).getText().toString().length() > 0) {
                                    if (Integer.parseInt(aqVP.id(R.id.ettotlivebirth).getText().toString()) > gravida) {
                                        aqVP.id(R.id.ettotlivebirth).getEditText()
                                                .setError("Para cannot be greater than gravida");
                                        aqVP.id(R.id.ettotlivebirth).text("");
                                    }
                                }
                            } else {
                                if (aqVP.id(R.id.ettotlivebirth).getText().toString().length() > 0) {
                                    if (Integer.parseInt(aqVP.id(R.id.ettotlivebirth).getText().toString()) >= gravida) {
                                        aqVP.id(R.id.ettotlivebirth).getEditText()
                                                .setError("Para cannot be greater than or equal to gravida");
                                        aqVP.id(R.id.ettotlivebirth).text("");
                                    }
                                }
                            }
                        } else {
                            if (aqVP.id(R.id.ettotlivebirth).getText().toString().length() > 0) {
                                if (Integer.parseInt(aqVP.id(R.id.ettotlivebirth).getText().toString()) == 0) {
                                    aqVP.id(R.id.ettotlivebirth).getEditText().setError("Para cannot be 0");
                                    aqVP.id(R.id.ettotlivebirth).text("");
                                } else if (Integer
                                        .parseInt(aqVP.id(R.id.ettotlivebirth).getText().toString()) > gravida) {
                                    aqVP.id(R.id.ettotlivebirth).getEditText()
                                            .setError("Para cannot be greater than gravida");
                                    aqVP.id(R.id.ettotlivebirth).text("");
                                }
                            }
                        }
                    }
                }*/

                int totlivebirth = regData.getRegPara();

                 if (s == aqVP.id(R.id.etlastchildage).getEditable()) {
                     if (count > 0) {
                         if (Integer.parseInt(aqVP.id(R.id.etlastchildage).getText()
                                 .toString()) == 0)
                             aqVP.id(R.id.etlastchildage).getEditText().
                                     setError(getResources().getString
                                             (R.string.m068));


                         if (totlivebirth > 0 ) {
                             if (aqVP.id(R.id.etlastchildage).getText().toString().length() > 0) {
                                 int lastchildage = (aqVP.id(R.id.etlastchildage).getText().toString().length() > 0 ? Integer.parseInt(aqVP.id(R.id.etlastchildage).getText().toString()) : 0);
                                 if (lastchildage <= 0) {

                                     aqVP.id(R.id.etlastchildage).getEditText()
                                             .setError(getResources().getString(R.string.lastchildagezero));
                                 } else {
                                     aqVP.id(R.id.etlastchildage).getEditText().setError(null);
                                 }
                             }
                         } else {
                             aqVP.id(R.id.etlastchildage).getEditText().setError(null);
                         }
                     }
                 }else if (s == aqVP.id(R.id.etlastchildWeight).getEditable()) {
                     if (totlivebirth > 0) {
                         if (aqVP.id(R.id.etlastchildWeight).getText().toString().length() > 0) {
                             int lastchildweight = (aqVP.id(R.id.etlastchildWeight).getText().toString().length() > 0 ? Integer.parseInt(aqVP.id(R.id.etlastchildWeight).getText().toString()) : 0);
                             if (lastchildweight <= 0) {

                                 aqVP.id(R.id.etlastchildWeight).getEditText()
                                         .setError(getResources().getString(R.string.lastchildweightzero));
                             } else {
                                 aqVP.id(R.id.etlastchildWeight).getEditText().setError(null);

                             }
                         }
                     } else
                         aqVP.id(R.id.etlastchildWeight).getEditText().setError(null);
                 }

            } catch (Exception e) {

                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {

        }

    };

    /**
     * This method calculate age from DOB
     */
    protected int getYearsFromDob(String string) throws ParseException {

        int years = 0;
        String dobtxt = aqVP.id(R.id.etdob).getText().toString();
        Calendar curDt = new GregorianCalendar();
        curDt.setTime(new SimpleDateFormat("dd-MM-yyyy").parse(DateTimeUtil.getTodaysDate()));
        Calendar dob = new GregorianCalendar();
        if (dobtxt.length() > 0) {
            dob.setTime(new SimpleDateFormat("dd-MM-yyyy").parse(dobtxt));
            years = curDt.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
        }
        return years;
    }


    /**
     * Confirm Alert
     */
    private void confirmAlertExit() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set dialog message
        alertDialogBuilder.setMessage((getResources().getString(R.string.areyousuretoexit))).setCancelable(false)
                .setNegativeButton((getResources().getString(R.string.m121)),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                try {
                                    Intent exit = new Intent(ReconfirmationRegActivity.this,
                                            MainMenuActivity.class);
                                    exit.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                    startActivity(exit);
                                } catch (Exception e) {

                                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                                    crashlytics.log(e.getMessage());
                                }
                            }
                        })
                .setPositiveButton((getResources().getString(R.string.m122)),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    //	prepare bundle for app state
    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        return b;
    }

    /**
     * Initializes the Action Bar items
     */
    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        try {

            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
            getSupportActionBar().setDisplayShowCustomEnabled(true);

            getMenuInflater().inflate(R.menu.servicesmenu, menu);
            menu.findItem(R.id.logout).setTitle((getResources().getString(R.string.action_logout)));

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        return super.onCreateOptionsMenu(menu);
    }


    /**
     * Called whenever an item clicked in actionbar
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action buttons
        switch (item.getItemId()) {

            case R.id.home: {
                Intent goToScreen = new Intent(ReconfirmationRegActivity.this, MainMenuActivity.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.m110), goToScreen);
                return true;
            }

            case R.id.logout: {
                Intent goToScreen = new Intent(ReconfirmationRegActivity.this, LoginActivity.class);
                displayAlert(getResources().getString(R.string.m111), goToScreen);
                return true;
            }
            case R.id.wlist: {
                Intent goToScreen = new Intent(ReconfirmationRegActivity.this, RegisteredWomenActionTabs.class);
                displayAlert(getResources().getString(R.string.m110), goToScreen);
                return true;
            }
//30Sep2019 - Bindu
            case R.id.about: {
                Intent goToScreen = new Intent(ReconfirmationRegActivity.this, AboutActivity.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * logout Confirmation Alert
     */
    private void displayAlert(final String strMessage, final Intent goToScreen) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set dialog message
        alertDialogBuilder.setMessage(strMessage).setCancelable(false).setNegativeButton(
                getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (goToScreen != null) {
                            goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(goToScreen);
                            if (strMessage.contains("logout")) {
                                try {
                                    LoginRepository loginRepository = new LoginRepository(databaseHelper);
                                    loginRepository.updateLogoutTime(appState.loginAuditId, appState.sessionUserId);
                                } catch (Exception e) {
                                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                                    crashlytics.log(e.getMessage());
                                }
                            }
                        } else
                            dialog.cancel();
                    }
                }).setPositiveButton(getResources().getString(R.string.m122),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        /*
         * You'll need this in your class to release the helper when done.
         */
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

    /**
     * This method invokes when Item clicked in Spinner
     *
     * @throws Exception
     */
    public void onSpinnerClicked(AdapterView<?> adapter, View v, int position, long arg3) throws
            Exception {

        switch (adapter.getId()) {
            default:
                break;
        }
    }





    //04Oct2019 - Bindu - Hide keyboard
    public void hideKeyboard() {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }



    void disableScreen()
    {
        ScrollView srlWomanBasicInfo  = findViewById(R.id.srlWomanBasicInfo);
        aqVP.id(R.id.btnsave).enabled(false);
        disableEnableControls(false,srlWomanBasicInfo);

    }

    //    Arpitha 26Nov2019
    private void disableEnableControls(boolean enable, ViewGroup vg) {
        for (int i = 0; i < vg.getChildCount(); i++) {
            View child = vg.getChildAt(i);
            child.setEnabled(enable);
            if (child instanceof ViewGroup) {
                disableEnableControls(enable, (ViewGroup) child);
            }
        }
    }

    //calculate gestation based on LMP
    private void calculateEddGestation(String lmp) {
        try {

            if (lmp.equalsIgnoreCase("lmp")) {
                if (aqVP.id(R.id.etlmpmother).getText().length() > 0
                        && aqVP.id(R.id.etlmpmother).getText().length() > 0) {

                    noOfDays = DateTimeUtil
                            .getNumberOfDaysFromLMP(aqVP.id(R.id.etlmpmother).getText().toString());
                    int noOfWeeks = noOfDays / 7;
                    int days = noOfDays % 7;

                    String strdays = "";

                    if (days > 1)
                        strdays = days + " " + getResources().getString(R.string.days);
                    else if (days == 1)
                        strdays = days + " " + getResources().getString(R.string.day);

                    aqVP.id(R.id.etgest)
                            .text(noOfWeeks + " " + getResources().getString(R.string.weeks) + " " + strdays);
                }

                String edd = populateEDDStr(aqVP.id(R.id.etlmpmother).getText().toString());
                aqVP.id(R.id.eteddmother).text(edd);

            } else {

                String strLmp = CalculateLMPStr(aqVP.id(R.id.eteddmother).getText().toString());
                aqVP.id(R.id.etlmpmother).text(strLmp);

                if (aqVP.id(R.id.etlmpmother).getText().length() > 0
                        && aqVP.id(R.id.etlmpmother).getText().length() > 0) {

                    noOfDays = DateTimeUtil
                            .getNumberOfDaysFromLMP(aqVP.id(R.id.etlmpmother).getText().toString());
                    int noOfWeeks = noOfDays / 7;
                    int days = noOfDays % 7;

                    String strdays = "";

                    if (days > 1)
                        strdays = days + " " + getResources().getString(R.string.days);
                    else if (days == 1)
                        strdays = days + " " + getResources().getString(R.string.day);

                    aqVP.id(R.id.etgest)
                            .text(noOfWeeks + " " + getResources().getString(R.string.weeks) + " " + strdays);
                }

            }
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }

    }

    // Calculate EDD and display in EDD Edit text
    private String populateEDDStr(String xLMP) throws Exception {
        String stredddate = "";
        try {

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());

            Date LmpDate;
            LmpDate = sdf.parse(xLMP);
            Calendar cal = Calendar.getInstance();
            cal.setTime(LmpDate);
            cal.add(Calendar.DAY_OF_MONTH, 280);

            int day = cal.get(Calendar.DAY_OF_MONTH);
            int month = cal.get(Calendar.MONTH) + 1;
            int year = cal.get(Calendar.YEAR);

            stredddate =
                    String.format("%02d", day) + "-" + String.format("%02d", month) + "-" + String.format("%02d", year);

        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        return stredddate;
    }

    //	calculate LMP based on EDD
    private String CalculateLMPStr(String EDDate) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        Date LMP;
        LMP = sdf.parse(EDDate);
        Calendar cal = Calendar.getInstance();
        cal.setTime(LMP);
        cal.add(Calendar.DAY_OF_MONTH, -280);

        int day = cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH) + 1;
        int year = cal.get(Calendar.YEAR);

        String lmpDate = String.format("%02d", day) + "-" + String.format("%02d", month) + "-" + year;
        return lmpDate;
    }


}


