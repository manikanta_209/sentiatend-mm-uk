package com.sc.stmansi.ListofAdols;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidquery.AQuery;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.adolescent.AdolRegistration;
import com.sc.stmansi.R;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.repositories.AdolescentRepository;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.configuration.SyncState;
import com.sc.stmansi.database.DBMethods;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.quickAction.ActionItem;
import com.sc.stmansi.quickAction.QuickAction;
import com.sc.stmansi.repositories.ChildRepository;
import com.sc.stmansi.tables.tblAdolReg;

import java.util.List;

public class ListofAdols extends AppCompatActivity {

    private DatabaseHelper databaseHelper;
    private List<tblAdolReg> listofAdols;
    private SearchView searchView;
    private QuickAction quickAction;
    private ListofAdolsRecyclerAdapter listofAdolsAdapter;
    private RecyclerView recyclerview;
    private FloatingActionButton floatingbtn;
    private AppState appState;
    private SyncState syncState;
    private RadioButton rb_All;
    private RadioGroup rg_filter;
    private AQuery aq;
    private Button btToolbarHome;
    private int itemPosition;

    @Override
    public void onBackPressed() {
        try {
            Intent intent = new Intent(ListofAdols.this, MainMenuActivity.class);
            intent.putExtra("globalState", prepareBundle());
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listof_adols);
        try {
            Bundle bundle = getIntent().getBundleExtra("globalState");
            appState = bundle.getParcelable("appState");
            Initialize();
            getData();
            setData();

            btToolbarHome.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ListofAdols.this, MainMenuActivity.class);
                    intent.putExtra("globalState", prepareBundle());
                    startActivity(intent);
                }
            });

            rb_All.setChecked(true);
            rg_filter.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (checkedId == R.id.rb_list_all) {
                        listofAdolsAdapter.genderFilter(0);
                        aq.id(R.id.txtAdolCount).getTextView().setText(getResources().getString(R.string.recordcount) + listofAdolsAdapter.getItemCount());
                    } else if (checkedId == R.id.rb_list_male) {
                        listofAdolsAdapter.genderFilter(1);
                        aq.id(R.id.txtAdolCount).getTextView().setText(getResources().getString(R.string.recordcount) + listofAdolsAdapter.getItemCount());
                    } else if (checkedId == R.id.rb_list_female) {
                        listofAdolsAdapter.genderFilter(2);
                        aq.id(R.id.txtAdolCount).getTextView().setText(getResources().getString(R.string.recordcount) + listofAdolsAdapter.getItemCount());
                    }
                }
            });

            recyclerview.addOnItemTouchListener(new RecyclerViewTouchListener(getApplicationContext(), recyclerview, new ClickListener() {
                @Override
                public void onLongClick(View child, int childAdapterPosition) {

                }

                @Override
                public void onClick(View child, int childAdapterPosition) {
                    itemPosition = childAdapterPosition;
                    quickAction.show(child);
                }

                @Override
                public void onClick(View v) {
                }

                @Override
                public boolean onLongClick(View v) {
                    return false;
                }
            }));

            floatingbtn.setOnClickListener(v -> {
                try {
                    Intent intent = new Intent(ListofAdols.this, AdolRegistration.class);
                    intent.putExtra("globalState", prepareBundle());
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });

            try {
                quickAction.setOnActionItemClickListener((source, pos, actionId) -> {
                    ActionItem actionItem = source.getActionItem(pos);
                    try {
                        if (actionItem.getActionId() == 1) {
                            Intent intent = new Intent(ListofAdols.this, AdolRegistration.class);
                            intent.putExtra("globalState", prepareBundle());
                            intent.putExtra("isEdit", true);
                            intent.putExtra("adolID", listofAdols.get(itemPosition).getAdolID());
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @SuppressLint("SetTextI18n")
                @Override
                public boolean onQueryTextChange(String newText) {
                    if (TextUtils.isEmpty(newText)) {
                        listofAdolsAdapter.filter("");
                    } else {
                        listofAdolsAdapter.filter(newText);
                    }
                    aq.id(R.id.txtAdolCount).getTextView().setText(getResources().getString(R.string.recordcount) + listofAdolsAdapter.getItemCount());
                    return true;
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("SetTextI18n")
    private void setData() {
        try {
            recyclerview.setHasFixedSize(true);
            recyclerview.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
            listofAdolsAdapter = new ListofAdolsRecyclerAdapter(listofAdols,
                    getApplicationContext(), quickAction, null,
                    null, new ChildRepository(databaseHelper),databaseHelper);
            recyclerview.setAdapter(listofAdolsAdapter);
            aq.id(R.id.txtAdolCount).getTextView().setText(getResources().getString(R.string.recordcount) +" "+ listofAdolsAdapter.getItemCount());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getData() {
        try {
            AdolescentRepository adolescentRepo = new AdolescentRepository(databaseHelper);
            listofAdols = adolescentRepo.getallAdols();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Initialize() {
        try {
            databaseHelper = getHelper();
            DBMethods dbMethods = new DBMethods(databaseHelper);
            aq = new AQuery(this);

            TextView textViewTitle = findViewById(R.id.toolbartxtTitle);
            textViewTitle.setText(getResources().getString(R.string.list_of_adolescent));
            searchView = findViewById(R.id.svListofAdols);
            recyclerview = findViewById(R.id.rvListofAdol);
            floatingbtn = findViewById(R.id.floatingbuttonAdolReg);
            quickAction = new QuickAction(ListofAdols.this);

            btToolbarHome = findViewById(R.id.toolbarBtnHome);
            btToolbarHome.setVisibility(View.VISIBLE);

            String str = getResources().getString(R.string.edit);
            ActionItem wEdit = new ActionItem(1, str, ContextCompat.getDrawable(ListofAdols.this, R.drawable.ic_edit_icon));
            quickAction.addActionItem(wEdit);

            rb_All = findViewById(R.id.rb_list_all);

            rg_filter = findViewById(R.id.rg_filterList);
            getSupportActionBar().hide();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        b.putParcelable("syncState", syncState);
        return b;
    }


}