package com.sc.stmansi.ListofAdols;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.sc.stmansi.R;


public class ListofAdolsViewHolder extends RecyclerView.ViewHolder {
    TextView txtName, txtAge, txtMobile,txtListofAdolGTS, txtRegDate,txtWarning;
    ImageView imgComplication;
    CardView cardview;
    TextView txtAnc;//02Sep2021 Arpitha
    public ListofAdolsViewHolder(@NonNull View convertView) {
        super(convertView);
        txtName = convertView.findViewById(R.id.txtListofAdolName);
        txtAge = convertView.findViewById(R.id.txtListofAdolAge);
        txtMobile = convertView.findViewById(R.id.txtListofAdolMobile);
        txtRegDate = convertView.findViewById(R.id.txtListofAdolRegDate);
        txtListofAdolGTS = convertView.findViewById(R.id.txtListofAdolGTS);
        imgComplication = convertView.findViewById(R.id.imgComplication);
        cardview = convertView.findViewById(R.id.cardview_ListofAdolItem);
        txtWarning = convertView.findViewById(R.id.txtListofAdolWarning);
        txtAnc = convertView.findViewById(R.id.txtanc);
    }
}
