package com.sc.stmansi.ListofAdols;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.sc.stmansi.R;
import com.sc.stmansi.adolescent.training.ListofAdolsFragment;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.quickAction.QuickAction;
import com.sc.stmansi.repositories.AdolescentRepository;
import com.sc.stmansi.repositories.ChildRepository;
import com.sc.stmansi.tables.tblAdolReg;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class ListofAdolsRecyclerAdapter extends RecyclerView.Adapter<ListofAdolsViewHolder> {
    private final List<tblAdolReg> listofAdols;
    private final Context context;
    private final ArrayList<tblAdolReg> modellist;
    private final QuickAction quickAction;
    private final View view;
    private final ChildRepository childRepository;
    private View.OnClickListener clickListener;
    private DatabaseHelper databaseHelper;//02Sep2021 Arpitha

    public ListofAdolsRecyclerAdapter(List<tblAdolReg> listofAdols, Context applicationContext,
                                      QuickAction quickAction, QuickAction quickActiontemp,
                                      View view, ChildRepository childRepository, DatabaseHelper databaseHelper) {
        this.listofAdols = listofAdols;
        context = applicationContext;
        modellist = new ArrayList<>();
        modellist.addAll(listofAdols);
        this.quickAction = quickAction;
        this.view = view;
        this.childRepository = childRepository;
        this.databaseHelper = databaseHelper;//02Sep2021 Arpitha
    }


    @NonNull
    @Override
    public ListofAdolsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CardView v = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_listofadolitem, parent, false);
        v.setOnClickListener(clickListener);
        return new ListofAdolsViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ListofAdolsViewHolder holder, int position) {
        try {
            holder.txtName.setText(listofAdols.get(position).getRegAdolName());
            holder.txtAge.setText(listofAdols.get(position).getRegAdolAge() + view.getResources().getString(R.string.yearscomma) + (listofAdols.get(position).getRegAdolGender().equals("Male") ? view.getResources().getString(R.string.radiomale1) : view.getResources().getString(R.string.radiofemale1)));
            holder.txtMobile.setText(listofAdols.get(position).getRegAdolMobile());

            String dateTime = listofAdols.get(position).getRecordCreatedDate();
            int index = dateTime.indexOf(" ");
            String date = dateTime.substring(0, index);
            holder.txtRegDate.setText(date);
            if ((listofAdols.get(position).getRegAdolHealthIssues() != null && listofAdols.get(position).getRegAdolHealthIssues().length() > 0) || (listofAdols.get(position).getRegAdolLifeStyle() != null && listofAdols.get(position).getRegAdolLifeStyle().length() > 0) || (listofAdols.get(position).getRegAdolMenstrualPbm() != null && listofAdols.get(position).getRegAdolMenstrualPbm().length() > 0)) { //21May2021 Bindu
                holder.imgComplication.setVisibility(View.VISIBLE);
            } else {
                holder.imgComplication.setVisibility(View.INVISIBLE);
            }

            if (childRepository.isAdolIDExists(listofAdols.get(position).getAdolID())) {
                holder.txtWarning.setVisibility(View.VISIBLE);
            }else {
                holder.txtWarning.setVisibility(View.INVISIBLE);
            }

            holder.txtListofAdolGTS.setText(listofAdols.get(position).getRegAdolGoingtoSchool().equals("Yes") ? view.getResources().getString(R.string.m121) : view.getResources().getString(R.string.m122));

            StringBuilder ComplicationTextHI = new StringBuilder();

            if (listofAdols.get(position).getRegAdolHealthIssues().toString().length() > 0)
                ComplicationTextHI.append(listofAdols.get(position).getRegAdolHealthIssues());

            StringBuilder ComplicationTextL = new StringBuilder();
            if (listofAdols.get(position).getRegAdolLifeStyle().toString().length() > 0)
                ComplicationTextHI.append(listofAdols.get(position).getRegAdolLifeStyle());

            StringBuilder ComplicationTextMP = new StringBuilder();
            if (listofAdols.get(position).getRegAdolMenstrualPbm() != null && listofAdols.get(position).getRegAdolMenstrualPbm().toString().length() > 0)
                ComplicationTextHI.append(listofAdols.get(position).getRegAdolMenstrualPbm());


            if (ComplicationTextHI.length() != 0 || ComplicationTextL.length() != 0 || ComplicationTextMP.length() != 0) {
                holder.imgComplication.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (quickAction != null) {
                            quickAction.dismiss();
                        }
                        if (ListofAdolsFragment.quickActiontemp != null) {
                            ListofAdolsFragment.quickActiontemp.dismiss();
                        }
                        displayAlert(position);
                    }
                });
            }

//            02Sep2021 Arpitha
            String womanId = new AdolescentRepository(databaseHelper).
                    getAdolANCWomanId(listofAdols.get(position).getRegAdolAadharNo(),
                            listofAdols.get(position).getAdolID());
            if (womanId != null && womanId.trim().length() > 0)
                holder.txtAnc.setVisibility(View.VISIBLE);
            else
                holder.txtAnc.setVisibility(View.GONE);

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    @Override
    public int getItemCount() {
        return listofAdols.size();
    }

    public void filter(String charText) {
        try {
            charText = charText.toLowerCase(Locale.getDefault());
            listofAdols.clear();
            if (charText.length() == 0) {
                listofAdols.addAll(modellist);
            } else {
                for (tblAdolReg model : modellist) {
                    if (model.getRegAdolName().toLowerCase().contains(charText.toLowerCase())) {
                        listofAdols.add(model);
                    }
                }
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        notifyDataSetChanged();
    }

    public void genderFilter(int i) {
        try {
            listofAdols.clear();
            if (i == 0) {
                listofAdols.addAll(modellist);
            } else {
                if (i == 1) {
                    for (tblAdolReg model : modellist) {
                        if (model.getRegAdolGender().contains("Male")) {
                            listofAdols.add(model);
                        }
                    }
                } else if (i == 2) {
                    for (tblAdolReg model : modellist) {
                        if (model.getRegAdolGender().contains("Female")) {
                            listofAdols.add(model);
                        }
                    }
                }
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
        notifyDataSetChanged();
    }

    //21May2021 Bindu
    public void displayAlert(int selectedItemPosition) {
        try {
            ArrayList<String> compList = new ArrayList<>();
            String complicationhl = "", complicationmp = "", complicationls = "";
            ArrayList<String> complListDisp = new ArrayList<>();
            ArrayList<String> complListDispls = new ArrayList<>();
            ArrayList<String> complListDispmp = new ArrayList<>();

            if (listofAdols.get(selectedItemPosition).getRegAdolHealthIssues() != null && listofAdols.get(selectedItemPosition).getRegAdolHealthIssues().toString().length() > 0)
                complicationhl = listofAdols.get(selectedItemPosition).getRegAdolHealthIssues();
            if (listofAdols.get(selectedItemPosition).getRegAdolLifeStyle() != null && listofAdols.get(selectedItemPosition).getRegAdolLifeStyle().toString().length() > 0)
                complicationls = listofAdols.get(selectedItemPosition).getRegAdolLifeStyle();
            if (listofAdols.get(selectedItemPosition).getRegAdolMenstrualPbm() != null && listofAdols.get(selectedItemPosition).getRegAdolMenstrualPbm().toString().length() > 0)
                complicationmp = listofAdols.get(selectedItemPosition).getRegAdolMenstrualPbm();
            if (complicationhl != null && complicationhl.length() > 0) {
                compList = new ArrayList<>();
                compList.add(complicationhl.replace(",", ", "));

                for (int i = 0; i < compList.size(); i++) {
                    String compl[] = compList.get(i).split("[,\n]");
                    for (int j = 0; j < compl.length; j++) {
                        String strComp = compl[j];

                        strComp = strComp.replaceAll(" ", "");
                        strComp = strComp.toLowerCase().trim();
                        int identifier = context.getResources().getIdentifier
                                (strComp,
                                        "string", "com.sc.stmansi");
                        if (identifier > 0) {
                            if (strComp.equals("others")) {
                                complListDisp.add(context.getResources().getString(identifier) + listofAdols.get(selectedItemPosition).getRegAdolOtherHealthIssues());
                            } else {
                                complListDisp.add(context.getResources().getString(identifier));
                            }
                        } else {
                            complListDisp.add(compl[j]);
                        }
                    }
                }
            }

            if (complicationls != null && complicationls.length() > 0) {
                compList = new ArrayList<>();
                compList.add(complicationls.replace(",", ", "));

                for (int i = 0; i < compList.size(); i++) {
                    String compl[] = compList.get(i).split("[,\n]");
                    for (int j = 0; j < compl.length; j++) {
                        String strComp = compl[j];

                        strComp = strComp.replaceAll(" ", "");
                        strComp = strComp.toLowerCase().trim();
                        int identifier = context.getResources().getIdentifier
                                (strComp,
                                        "string", "com.sc.stmansi");
                        if (identifier > 0) {
                            complListDispls.add(context.getResources().getString(identifier));
                        } else {
                            complListDispls.add(compl[j]);
                        }
                    }
                }
            }

            //menstrual pbm
            if (complicationmp != null && complicationmp.length() > 0) {
                compList = new ArrayList<>();
                compList.add(complicationmp.replace(",", ", "));

                for (int i = 0; i < compList.size(); i++) {
                    String compl[] = compList.get(i).split("[,\n]");
                    for (int j = 0; j < compl.length; j++) {
                        String strComp = compl[j];

                        strComp = strComp.replaceAll(" ", "");
                        strComp = strComp.toLowerCase().trim();
                        int identifier = context.getResources().getIdentifier
                                (strComp,
                                        "string", "com.sc.stmansi");
                        if (identifier > 0) {
                            complListDispmp.add(context.getResources().getString(identifier));
                        } else {
                            complListDispmp.add(compl[j]);
                        }
                    }
                }
            }


            AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
            View convertView = LayoutInflater.from(context).inflate(R.layout.adolprofile_healthissuedialog, null, false);

            AQuery aq = new AQuery(convertView);
            if (complListDisp != null && complListDisp.size() > 0) {
                StringBuilder healthissue = new StringBuilder();
                for (int i = 0; i < complListDisp.size(); i++) {
                    healthissue.append(complListDisp.get(i)).append(",");
                }
                healthissue.deleteCharAt(healthissue.length() - 1).toString();
                aq.id(R.id.txthealthissueval).text(healthissue.toString());
            } else {
                aq.id(R.id.trhealthissue).gone();
            }

            if (complListDispls != null && complListDispls.size() > 0) {
                StringBuilder healthissue = new StringBuilder();
                for (int i = 0; i < complListDispls.size(); i++) {
                    healthissue.append(complListDispls.get(i)).append(",");
                }
                healthissue.deleteCharAt(healthissue.length() - 1).toString();
                aq.id(R.id.txtlifestyleval).text(healthissue.toString());
            } else {
                aq.id(R.id.trlifestyle).gone();
            }

            if (complListDispmp != null && complListDispmp.size() > 0) {
                StringBuilder healthissue = new StringBuilder();
                for (int i = 0; i < complListDispmp.size(); i++) {
                    healthissue.append(complListDispmp.get(i)).append(",");
                }
                healthissue.deleteCharAt(healthissue.length() - 1).toString();
                aq.id(R.id.txtmpval).text(healthissue.toString());
            } else {
                aq.id(R.id.trmp).gone();
            }

                /*ListView lv = convertView.findViewById(R.id.lvCompliatedReasonslist);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.listitem, l);*/

            alertDialog.setView(convertView).setCancelable(true).setTitle((context.getResources().getString(R.string.complications)))
                    .setPositiveButton((context.getResources().getString(R.string.ok)), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
//                lv.setAdapter(adapter);
            alertDialog.show();

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }
}
