package com.sc.stmansi.ListofAdols;

import android.view.View;

import java.sql.SQLException;

public interface ClickListener extends View.OnClickListener, View.OnLongClickListener {
    void onLongClick(View child, int childAdapterPosition);

    void onClick(View child, int childAdapterPosition) throws SQLException;
}
