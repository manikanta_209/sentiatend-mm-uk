//01Oct2019 Arpitha
package com.sc.stmansi.delivery;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.legacy.app.ActionBarDrawerToggle;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.misc.TransactionManager;
import com.sc.stmansi.About.AboutActivity;
import com.sc.stmansi.HomeVisit.HomeVisit;
import com.sc.stmansi.HomeVisit.HomeVisitListActivity;
import com.sc.stmansi.HomeVisit.PncHomeVisit;
import com.sc.stmansi.common.CommonDatePickerFragment;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.common.MultiSelectionSpinner;
import com.sc.stmansi.common.NavDrawerItem;
import com.sc.stmansi.common.NavDrawerListAdapter;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.deactivate.WomanDeactivateActivity;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.repositories.AuditRepository;
import com.sc.stmansi.repositories.ChildRepository;
import com.sc.stmansi.repositories.DeliveryRepository;
import com.sc.stmansi.repositories.FacilityRepository;
import com.sc.stmansi.repositories.LoginRepository;
import com.sc.stmansi.repositories.TransactionHeaderRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.repositories.WomanServiceRepository;
import com.sc.stmansi.services.ServicesSummaryActivity;
import com.sc.stmansi.services.UnplannedServicesActivity;
import com.sc.stmansi.services.UnplannedServicesListActivity;
import com.sc.stmansi.tables.AuditPojo;
import com.sc.stmansi.tables.TblChildInfo;
import com.sc.stmansi.tables.TblDeliveryInfo;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.viewprofile.ViewProfileActivity;
import com.sc.stmansi.womanlist.RegisteredWomenActionTabs;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import com.sc.stmansi.R;

public class DeliveryInfoActivity extends AppCompatActivity implements
        View.OnTouchListener, DatePickerDialog.OnDateSetListener,
        TimePickerDialog.OnTimeSetListener, View.OnClickListener, AdapterView.OnItemClickListener {

    public static AQuery aqDel;
    private AppState appState;
    private DatabaseHelper databaseHelper;
    private TblInstusers user;
    private tblregisteredwomen woman;
    private TblDeliveryInfo tblDelInfo;
    private List<TblChildInfo> childList;
    private TblChildInfo tblChildInfo;
    private EditText currentEditTextForDate;
    private int rchId = R.id.etrchid, etBabyName = R.id.etbabyname,
            etCommentSchool = R.id.etcommentschl, etDob = R.id.etdob,
            etWeight = R.id.etWeight, rdPhyYesCh = R.id.rdphyYesch,
            rdphyNoCh = R.id.rdphyNoch, etOtherPhy = R.id.etotherPhy, radioMale = R.id.radiomale,
            radioFemale = R.id.radiofemale, radioAmbigious = R.id.radioambi, rdCryYes = R.id.rdcryyes,
            rdCryNo = R.id.rdcryno,
            rdBreastYes = R.id.rdbreastyes, rdBreastNo = R.id.rdbreastno,

    //Mani 10feb2021 added two fields rdskintoskinYes and rdskintoskinNo
    rdskintoskinYes = R.id.rdskintoskinyes,rdskintoskinNo = R.id.rdskintoskinno,
    delResult = R.id.spndelresult,
            rdFresh = R.id.rdfresh, rdMacerated = R.id.rdmacerated,
            etInfantDeathDate = R.id.etinfantdeathdate,
            etTob = R.id.ettob, hour, minute,
            spnDelType = R.id.spndeltype,
            etDelTypeOther = R.id.etDelTypeOther,
            etDelConName = R.id.etdelconductedbyname,
            spnDelBy = R.id.spndelby,
            etDelByOther = R.id.etdelbyother,
            spnDelPlace = R.id.spndelplace,
            spntribalhamletdel = R.id.spntribalhamletdel,
            rdAshaYes = R.id.rdAshaYes,
            rdAshaNo = R.id.rdAshaNo,
            chkLessWeight = R.id.chklessweight,
            etothercomplchl = R.id.etothercomplchl,
            trothercomplchl = R.id.trothercomplchl;
    List<TblDeliveryInfo> deliveryInfoList;
    private AuditPojo APJ;
    String addStringChl = "", ChlSql = "";
    List<TblChildInfo> childListOld;
    public static MultiSelectionSpinner chlCompl1;
    public static MultiSelectionSpinner chlCompl2;
    public static MultiSelectionSpinner chlCompl3;
    public static MultiSelectionSpinner chlCompl4;
    public static MultiSelectionSpinner multiComplications;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private List<tblregisteredwomen> wVisitDetails;
    Map<String, Integer> mapVillage;
    int compl;
    RadioGroup rgiud1, rgiud2, rgiud3, rgiud4;
    RadioGroup rgPhy1, rgCry1, rgBreast1, rgPhy2, rgCry2, rgBreast2, rgPhy3,
            rgCry3, rgBreast3, rgPhy4, rgCry4, rgBreast4,rgSkintoskin1,rgSkintoSkin2,rgSkintoSkin3,rgSkintoSkin4;
    int transId;
   public static boolean mspnMotherCompl, mspnChl1Compl, mspnChl2Compl,mspnChl3Compl,mspnChl4Compl;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newdeliveryinfo);
        try {

             getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(false);


            Bundle bundle = getIntent().getBundleExtra("globalState");
            appState = bundle.getParcelable("appState");
            databaseHelper = getHelper();

            woman = (tblregisteredwomen) getIntent().getSerializableExtra("woman");

            UserRepository userRepository = new UserRepository(databaseHelper);
            user = userRepository.getOneAuditedUser(appState.ashaId);
            aqDel = new AQuery(this);

            rgiud1 = findViewById(R.id.rgiud1);
            rgiud2 = findViewById(R.id.rgiud2);
            rgiud3 = findViewById(R.id.rgiud3);
            rgiud4 = findViewById(R.id.rgiud4);

            rgCry1 = findViewById(R.id.rgcry1);
            rgCry2 = findViewById(R.id.rgcry2);
            rgCry3 = findViewById(R.id.rgcry3);
            rgCry4 = findViewById(R.id.rgcry4);

            rgPhy1 = findViewById(R.id.rgphy1);
            rgPhy2 = findViewById(R.id.rgphy2);
            rgPhy3 = findViewById(R.id.rgphy3);
            rgPhy4 = findViewById(R.id.rgphy4);

            rgBreast1 = findViewById(R.id.rgbreast1);
            rgBreast2 = findViewById(R.id.rgbreast2);
            rgBreast3 = findViewById(R.id.rgbreast3);
            rgBreast4 = findViewById(R.id.rgbreast4);


            //Manikanta 10feb2021 added new field skintoskincontact
            rgSkintoskin1 =findViewById(R.id.rgskintoskin1);
            rgSkintoSkin2 =findViewById(R.id.rgskintoskin2);
            rgSkintoSkin3 =findViewById(R.id.rgskintoskin3);
            rgSkintoSkin4 =findViewById(R.id.rgskintoskin4);

            initiateDrawer();

            if (woman.getRegpregormotheratreg() == 2 &&
                    (deliveryInfoList != null && deliveryInfoList.size() <= 0))
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);


            if (woman.getRegpregormotheratreg() == 1) {
                WomanRepository womanRepository = new WomanRepository(databaseHelper);

                wVisitDetails = womanRepository.getVisitDetailsFrmReg(woman.getWomanId(), appState.sessionUserId);
            }

            multiComplications = findViewById(R.id.spnmothercomplications);
            multiComplications.setItems(getResources().getStringArray(R.array.mothercompl));


            chlCompl1 = findViewById(R.id.spncomplicationschl1);
            chlCompl1.setItems(getResources().getStringArray(R.array.newborncompl));

            chlCompl2 = findViewById(R.id.spncomplicationschl2);
            chlCompl2.setItems(getResources().getStringArray(R.array.newborncompl));

            chlCompl3 = findViewById(R.id.spncomplicationschl3);
            chlCompl3.setItems(getResources().getStringArray(R.array.newborncompl));

            chlCompl4 = findViewById(R.id.spncomplicationschl4);
            chlCompl4.setItems(getResources().getStringArray(R.array.newborncompl));

            getData();
            setOnTextChangeListener();
            setSpinnerClickListner();
            setTouchListener();

            if (woman != null && woman.getRegEDD() != null && woman.getRegEDD().trim().length() > 0) {
                aqDel.id(R.id.txtedd).text(": " + woman.getRegEDD());
                //aqDel.id(R.id.tredd).visible();
            }


            if(woman.getRegADDate()!=null && woman.getRegADDate().trim().length()>0)
            aqDel.id(R.id.txtadd).text(": " + woman.getRegADDate());

            if (user.getIsDeactivated() == 1 ||
                    (woman.getDateDeactivated() != null &&
                            woman.getDateDeactivated().trim().length() > 0)) {
                disableScreen();

                aqDel.id(R.id.llchl1expand).enabled(true);
                aqDel.id(R.id.llchl2expand).enabled(true);
                aqDel.id(R.id.llchl3expand).enabled(true);
                aqDel.id(R.id.llchl4expand).enabled(true);

                Toast.makeText(getApplicationContext(),getResources().getString(R.string.userdeactivated),Toast.LENGTH_LONG).show();

            }

            aqDel.id(R.id.etdelconductedbyname1).getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {

                    if (aqDel.id(R.id.etdelconductedbyname1).getText().toString().trim().length() > 0) {
                        String delCond = aqDel.id(R.id.etdelconductedbyname1).getText().toString();
                        if (aqDel.id(R.id.spnnoofchild).getSelectedItemPosition() > 1) {
                            if (aqDel.id(R.id.etdelconductedbyname2).getText().toString().trim().length() > 0)
                                aqDel.id(R.id.etdelconductedbyname2).text(delCond);
                        }
                        if (aqDel.id(R.id.spnnoofchild).getSelectedItemPosition() > 2) {
                            if (aqDel.id(R.id.etdelconductedbyname3).getText().toString().trim().length() > 0)

                                aqDel.id(R.id.etdelconductedbyname3).text(delCond);
                        }
                        if (aqDel.id(R.id.spnnoofchild).getSelectedItemPosition() > 3) {
                            if (aqDel.id(R.id.etdelconductedbyname4).getText().toString().trim().length() > 0)

                                aqDel.id(R.id.etdelconductedbyname4).text(delCond);
                        }

                    }

                }
            });


            aqDel.id(R.id.etWeight1).getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {

                    if (aqDel.id(R.id.etWeight1).getText().toString().trim().length() > 0 &&
                            Double.parseDouble(aqDel.id(R.id.etWeight1).getText().toString()) <= 999)
                        aqDel.id(R.id.etWeight1).getEditText().setError(getResources().getString(R.string.weightvalis) + ": " + aqDel.id(R.id.etWeight1).getText().toString());

                }
            });

            aqDel.id(R.id.etWeight2).getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {

                    if (aqDel.id(R.id.etWeight2).getText().toString().trim().length() > 0 &&
                            Double.parseDouble(aqDel.id(R.id.etWeight2).getText().toString()) <= 999)
                        aqDel.id(R.id.etWeight2).getEditText().setError(getResources().getString(R.string.weightvalis) + ": " + aqDel.id(R.id.etWeight2).getText().toString());

                }
            });

            aqDel.id(R.id.etWeight3).getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {

                    if (aqDel.id(R.id.etWeight3).getText().toString().trim().length() > 0 &&
                            Double.parseDouble(aqDel.id(R.id.etWeight3).getText().toString()) <= 999)
                        aqDel.id(R.id.etWeight3).getEditText().setError(getResources().getString(R.string.weightvalis) + ": " + aqDel.id(R.id.etWeight3).getText().toString());

                }
            });

            aqDel.id(R.id.etWeight4).getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {

                    if (aqDel.id(R.id.etWeight4).getText().toString().trim().length() > 0 &&
                            Double.parseDouble(aqDel.id(R.id.etWeight1).getText().toString()) <= 999)
                        aqDel.id(R.id.etWeight4).getEditText().setError(getResources().getString(R.string.weightvalis) + ": " + aqDel.id(R.id.etWeight4).getText().toString());

                }
            });




        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private void setOnTextChangeListener() {

        aqDel.id(R.id.etWeight1).getEditText().addTextChangedListener(watcher);
        aqDel.id(R.id.etWeight2).getEditText().addTextChangedListener(watcher);
        aqDel.id(R.id.etWeight3).getEditText().addTextChangedListener(watcher);
        aqDel.id(R.id.etWeight4).getEditText().addTextChangedListener(watcher);
    }

    private void setTouchListener() {

        aqDel.id(R.id.etdob1).getEditText().setOnTouchListener(this);
        aqDel.id(R.id.etdob2).getEditText().setOnTouchListener(this);
        aqDel.id(R.id.etdob3).getEditText().setOnTouchListener(this);
        aqDel.id(R.id.etdob4).getEditText().setOnTouchListener(this);
        aqDel.id(R.id.ettob2).getEditText().setOnTouchListener(this);
        aqDel.id(R.id.ettob3).getEditText().setOnTouchListener(this);
        aqDel.id(R.id.ettob4).getEditText().setOnTouchListener(this);
        aqDel.id(R.id.ettob1).getEditText().setOnTouchListener(this);
        aqDel.id(R.id.etinfantdeathdate1).getEditText().setOnTouchListener(this);
        aqDel.id(R.id.etinfantdeathdate2).getEditText().setOnTouchListener(this);
        aqDel.id(R.id.etinfantdeathdate3).getEditText().setOnTouchListener(this);
        aqDel.id(R.id.etinfantdeathdate4).getEditText().setOnTouchListener(this);

    }

    private void setSpinnerClickListner() {

        aqDel.id(R.id.spnnoofchild).itemSelected(this, "onSpinnerClicked");
        aqDel.id(R.id.spndeltype1).itemSelected(this, "onSpinnerClicked");
        aqDel.id(R.id.spndeltype2).itemSelected(this, "onSpinnerClicked");
        aqDel.id(R.id.spndeltype3).itemSelected(this, "onSpinnerClicked");
        aqDel.id(R.id.spndelplace1).itemSelected(this, "onSpinnerClicked");
        aqDel.id(R.id.spndelplace2).itemSelected(this, "onSpinnerClicked");
        aqDel.id(R.id.spndelplace3).itemSelected(this, "onSpinnerClicked");
        aqDel.id(R.id.spndelplace4).itemSelected(this, "onSpinnerClicked");
        aqDel.id(R.id.spndeltype4).itemSelected(this, "onSpinnerClicked");
        aqDel.id(R.id.spndelby1).itemSelected(this, "onSpinnerClicked");
        aqDel.id(R.id.spndelby2).itemSelected(this, "onSpinnerClicked");
        aqDel.id(R.id.spndelby3).itemSelected(this, "onSpinnerClicked");
        aqDel.id(R.id.spndelby4).itemSelected(this, "onSpinnerClicked");
        aqDel.id(R.id.spnepisotmy).itemSelected(this, "onSpinnerClicked");
        aqDel.id(R.id.spncauseofdeath).itemSelected(this, "onSpinnerClicked");
        aqDel.id(R.id.spndelresult1).itemSelected(this, "onSpinnerClicked");
        aqDel.id(R.id.spndelresult2).itemSelected(this, "onSpinnerClicked");
        aqDel.id(R.id.spndelresult3).itemSelected(this, "onSpinnerClicked");
        aqDel.id(R.id.spndelresult4).itemSelected(this, "onSpinnerClicked");

        aqDel.id(R.id.spntribalhamletdel1).itemSelected(this, "onSpinnerClicked");



    }

    private String updateDelData(int transId) throws Exception {

        tblDelInfo = new TblDeliveryInfo();
      /*  String motherCompl = aqDel.id(R.id.spnmothercomplications).
                getSelectedItem().toString();
        motherCompl = motherCompl.replaceAll(",\\s+", ",");
        tblDelInfo.setDelMotherComplications(motherCompl.replace("Select", ""));*/

        String selIds = "";
        multiComplications.getSelectedIndicies();
        selIds = multiComplications.getSelectedIndicies().toString();

        if (selIds.contains("0,"))
            selIds = selIds.replace("0,", "");
        if (selIds.contains("["))
            selIds = selIds.replace("[", "");
        if (selIds.contains("]"))
            selIds = selIds.replace("]", "");
        tblDelInfo.setDelMotherComplications(selIds);


        tblDelInfo.setDelComments(aqDel.id(R.id.etcommentsdel).getText().toString());
        tblDelInfo.setDelOtherComplications(aqDel.id(R.id.etothercomplmother).getText().toString());
        tblDelInfo.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());


        if (multiComplications.getSelectedItemsAsString() != null &&
                multiComplications.getSelectedItemsAsString().trim().length() > 0) {
            compl = 1;
        } else {
            compl = 0;

        }


        return checkForAudit(transId);
    }

    private String updateChildData(int transId, int i) throws Exception {

        String compl = "";
        tblChildInfo = new TblChildInfo();
    /*    int mspnChlComp = R.id.spncomplicationschl;
        if (aqDel.id(mspnChlComp + i) != null && aqDel.id(mspnChlComp + i).getSelectedItem() != null)
            compl = aqDel.id(mspnChlComp + i).getSelectedItem().toString();
        tblChildInfo.setChlComplications(compl.replaceAll(",\\s+", ","));*/
        tblChildInfo.setChlChildComments(aqDel.id(etCommentSchool + i).getText().toString());

        tblChildInfo.setChlOtherComplications(aqDel.id(etothercomplchl + i).getText().toString());
        tblChildInfo.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());

        String selIds = "";

        if (i == 1)
            selIds = chlCompl1.getSelectedIndicies().toString();
        else if (i == 2)
            selIds = chlCompl2.getSelectedIndicies().toString();
        else if (i == 3)
            selIds = chlCompl3.getSelectedIndicies().toString();
        else if (i == 4)
            selIds = chlCompl4.getSelectedIndicies().toString();

        if (selIds.contains("0,"))
            selIds = selIds.replace("0,", "");
        if (selIds.contains("["))
            selIds = selIds.replace("[", "");
        if (selIds.contains("]"))
            selIds = selIds.replace("]", "");

        tblChildInfo.setChlComplications(selIds);

        tblChildInfo.setChildRCHID(aqDel.id(rchId + i).getText().toString());
        tblChildInfo.setChlBabyname(aqDel.id(etBabyName + i).getText().toString());
        return checkForAuditChild(transId, i);
    }

    String checkForAudit(int transId) throws Exception {
        String addString = "", delSql = "";

        if (addString == "")
            addString = addString + " delMotherComplications = " + (char) 34 + tblDelInfo.getDelMotherComplications() + (char) 34 + "";
        else
            addString = addString + " ,delMotherComplications = " + (char) 34 + tblDelInfo.getDelMotherComplications() + (char) 34 + "";
        InserttblAuditTrail("delMotherComplications",
                deliveryInfoList.get(0).getDelMotherComplications(),
                tblDelInfo.getDelMotherComplications(),
                transId, "tbldeliveryinfo");

        if (addString == "")
            addString = addString + " delOtherComplications = " + (char) 34 + tblDelInfo.getDelOtherComplications() + (char) 34 + "";
        else
            addString = addString + " ,delOtherComplications = " + (char) 34 + tblDelInfo.getDelOtherComplications() + (char) 34 + "";
        InserttblAuditTrail("delOtherComplications",
                deliveryInfoList.get(0).getDelOtherComplications(),
                tblDelInfo.getDelOtherComplications(),
                transId, "tbldeliveryinfo");

        if (addString == "")
            addString = addString + " delComments = " + (char) 34 + tblDelInfo.getDelComments() + (char) 34 + "";
        else
            addString = addString + " ,delComments = " + (char) 34 + tblDelInfo.getDelComments() + (char) 34 + "";
        InserttblAuditTrail("delComments",
                deliveryInfoList.get(0).getDelComments(),
                tblDelInfo.getDelComments(),
                transId, "tbldeliveryinfo");

        if (addString == "")
            addString = addString + " RecordUpdatedDate = " + (char) 34 + tblDelInfo.getRecordUpdatedDate() + (char) 34 + "";
        else
            addString = addString + " ,RecordUpdatedDate = " + (char) 34 + tblDelInfo.getRecordUpdatedDate() + (char) 34 + "";
        InserttblAuditTrail("RecordUpdatedDate",
                deliveryInfoList.get(0).getRecordUpdatedDate(),
                tblDelInfo.getRecordUpdatedDate(),
                transId, "tbldeliveryinfo");

        if (addString != null && addString.length() > 0) {
            delSql = "UPDATE tbldeliveryinfo SET ";
            delSql = delSql + addString + " WHERE WomanId = '"
                    + appState.selectedWomanId + "' and UserId = '" + woman.getUserId() + "'";
        }
        return delSql;
    }

    private String checkForAuditChild(int transId, int pos) throws Exception {

        addStringChl = "";
        ChlSql = "";


        if (addStringChl == "")
            addStringChl = addStringChl + " chlChildname = " + (char) 34 + tblChildInfo.getChlBabyname() + (char) 34 + "";
        else
            addStringChl = addStringChl + " ,chlChildname = " + (char) 34 + tblChildInfo.getChlBabyname() + (char) 34 + "";
        InserttblAuditTrail("chlChildname",
                childListOld.get(pos - 1).getChlBabyname(),
                tblChildInfo.getChlBabyname(),
                transId, "tblchildinfo");

        if (addStringChl == "")
            addStringChl = addStringChl + " chlComplications = " + (char) 34 + tblChildInfo.getChlComplications() + (char) 34 + "";
        else
            addStringChl = addStringChl + " ,chlComplications = " + (char) 34 + tblChildInfo.getChlComplications() + (char) 34 + "";
        InserttblAuditTrail("chlComplications",
                childListOld.get(pos - 1).getChlComplications(),
                tblChildInfo.getChlComplications(),
                transId, "tblchildinfo");

//        01MAy2021 Bindu - Child compl other check for audit
        if (addStringChl == "")
            addStringChl = addStringChl + " chlOtherComplications = " + (char) 34 + tblChildInfo.getChlOtherComplications() + (char) 34 + "";
        else
            addStringChl = addStringChl + " ,chlOtherComplications = " + (char) 34 + tblChildInfo.getChlOtherComplications() + (char) 34 + "";
        InserttblAuditTrail("chlOtherComplications",
                childListOld.get(pos - 1).getChlOtherComplications(),
                tblChildInfo.getChlOtherComplications(),
                transId, "tblchildinfo");

        if (addStringChl == "")
            addStringChl = addStringChl + " chlComments = " + (char) 34 + tblChildInfo.getChlChildComments() + (char) 34 + "";
        else
            addStringChl = addStringChl + " ,chlComments = " + (char) 34 + tblChildInfo.getChlChildComments() + (char) 34 + "";
        InserttblAuditTrail("chlComments",
                childListOld.get(pos - 1).getChlChildComments(),
                tblChildInfo.getChlChildComments(),
                transId, "tblchildinfo");


        if (addStringChl == "")
            addStringChl = addStringChl + " chlRCHID = " + (char) 34 + tblChildInfo.getChildRCHID() + (char) 34 + "";
        else
            addStringChl = addStringChl + " ,chlRCHID = " + (char) 34 + tblChildInfo.getChildRCHID() + (char) 34 + "";
        InserttblAuditTrail("chlRCHID",
                childListOld.get(pos - 1).getChildRCHID(),
                tblChildInfo.getChildRCHID(),
                transId, "tblchildinfo");

        if (addStringChl == "")
            addStringChl = addStringChl + " RecordUpdatedDate = " +
                    (char) 34 + tblChildInfo.getRecordUpdatedDate()
                    + (char) 34 + "";
        else
            addStringChl = addStringChl + " ,RecordUpdatedDate = " +
                    (char) 34 + tblChildInfo.getRecordUpdatedDate() + (char) 34 + "";
        InserttblAuditTrail("RecordUpdatedDate",
                childListOld.get(0).getRecordUpdatedDate(),
                tblChildInfo.getRecordUpdatedDate(),
                transId, "tblchildinfo");

        if (addStringChl != null && addStringChl.length() > 0) {
            ChlSql = "UPDATE tblchildinfo SET ";
            ChlSql = ChlSql + addStringChl + " WHERE WomanId = '"
                    + appState.selectedWomanId + "' and UserId = '" + woman.getUserId() + "' and " +
                    " chlID = '" + childListOld.get(pos - 1).getChildID() + "'";
        }

        return ChlSql;
    }

    private void getData() throws SQLException {

        deliveryInfoList = new ArrayList<>();
        DeliveryRepository deliveryRepository = new DeliveryRepository(databaseHelper);
        deliveryInfoList = deliveryRepository.getDeliveryData(woman.getWomanId(), woman.getUserId());
        if (deliveryInfoList != null && deliveryInfoList.size() > 0) {
            TblDeliveryInfo deliveryInfo = new TblDeliveryInfo();
            deliveryInfo = deliveryInfoList.get(0);
            aqDel.id(R.id.spnnoofchild).setSelection(deliveryInfo.getDelNoOfChildren());

            if (deliveryInfo.getDelMothersDeath() != null && deliveryInfo.getDelMothersDeath().equalsIgnoreCase("Y")) {
                aqDel.id(R.id.rddeathyes).checked(true);
                aqDel.id(R.id.trcasueofdeath).visible();
                int cause = 0;
                if (deliveryInfo.getDelCauseofDeath() != null)
                    cause = Integer.parseInt(deliveryInfo.getDelCauseofDeath());
                aqDel.id(R.id.spncauseofdeath).setSelection(cause);
            } else {
                aqDel.id(R.id.rddeathno).checked(true);
                aqDel.id(R.id.trcasueofdeath).gone();
            }
            aqDel.id(R.id.etothercauseofdeath).text(deliveryInfo.getDelOtherDeathCause());
            aqDel.id(R.id.etcommentsdel).text(deliveryInfo.getDelComments());


            if (deliveryInfo.getDelAshaAccompany() != null
                    && deliveryInfo.getDelAshaAccompany()
                    .equalsIgnoreCase("Y")) {
                aqDel.id(R.id.rdacomyes1).checked(true);
            } else {
                aqDel.id(R.id.rdacomno1).checked(true);
            }

            if (deliveryInfo.getDelMotherComplications() != null && deliveryInfo.getDelMotherComplications().trim().length() > 0) {
                String[] motherCompl = deliveryInfo.getDelMotherComplications().split(",");
                if(deliveryInfo.getDelMotherComplications().contains("5")) //01May2021 Bindu - check mother compl others
                    aqDel.id(R.id.trothercomplmother).visible();

                int[] selectedIds = new int[motherCompl.length];

                if (motherCompl != null && motherCompl.length > 0) {
                    for (int i = 0; i < motherCompl.length; i++) {
                        selectedIds[i] = Integer.parseInt(motherCompl[i].trim());
                    }
                }

                multiComplications.setSelection(selectedIds);
            }


            aqDel.id(R.id.etothercomplmother).text(deliveryInfo.getDelOtherComplications());


            aqDel.id(R.id.txtdelheading1).text(getResources().getString(R.string.deliveryHeading)
                    + " :" + getResources().getString(R.string.edit_mode));


        } else
            aqDel.id(R.id.txtdelheading1).text(getResources().getString(R.string.deliveryHeading)
                    + " :" + getResources().getString(R.string.add_mode));


        ChildRepository childRepository = new ChildRepository(databaseHelper);
        childListOld = childRepository.getChildData(woman.getWomanId());


        setTribalHamlet();

        if (childListOld != null && childListOld.size() > 0) {
            for (int i = 1; i <= childListOld.size(); i++) {
                TblChildInfo tblChildInfo = new TblChildInfo();
                tblChildInfo = childListOld.get(i - 1);

                aqDel.id(etDob + i).text(tblChildInfo.getChlDateTimeOfBirth());
                aqDel.id(etTob + i).text(tblChildInfo.getChlTimeOfBirth());

                aqDel.id(delResult + i).setSelection(tblChildInfo.getChlDeliveryResult());

                aqDel.id(etBabyName + i).text(tblChildInfo.getChlBabyname());
                aqDel.id(rchId + i).text(tblChildInfo.getChildRCHID());
                aqDel.id(etWeight + i).text(tblChildInfo.getDelBabyWeight());
                aqDel.id(etCommentSchool + i).text(tblChildInfo.getChlChildComments());

                if (tblChildInfo.getChlGender() != null && tblChildInfo.getChlGender().
                        equalsIgnoreCase("Female"))
                    aqDel.id(radioFemale + i).checked(true);
                else if (tblChildInfo.getChlGender() != null && tblChildInfo.getChlGender().
                        equalsIgnoreCase("Male")) aqDel.id(radioMale + i).checked(true);
                else if (tblChildInfo.getChlGender() != null && tblChildInfo.getChlGender().
                        equalsIgnoreCase("Ambigious"))
                    aqDel.id(radioAmbigious + i).checked(true);

                if (tblChildInfo.getChlCryAfterBirth() != null && tblChildInfo.getChlCryAfterBirth().
                        equalsIgnoreCase("Y"))
                    aqDel.id(rdCryYes + i).checked(true);
                else if (tblChildInfo.getChlCryAfterBirth() != null && tblChildInfo.getChlCryAfterBirth().
                        equalsIgnoreCase("N"))
                    aqDel.id(rdCryNo + i).checked(true);


                if (tblChildInfo.getChlBreastFeed() != null && tblChildInfo.getChlBreastFeed().
                        equalsIgnoreCase("Y"))
                    aqDel.id(rdBreastYes + i).checked(true);
                else if (tblChildInfo.getChlBreastFeed() != null && tblChildInfo.getChlBreastFeed().
                        equalsIgnoreCase("N"))
                    aqDel.id(rdBreastNo + i).checked(true);

                //Mani 10feb2021
                if (tblChildInfo.getChlSkintoskincontact() != null && tblChildInfo.getChlSkintoskincontact().
                        equalsIgnoreCase("Y"))
                    aqDel.id(rdskintoskinYes + i).checked(true);
                else if (tblChildInfo.getChlSkintoskincontact() != null && tblChildInfo.getChlSkintoskincontact().
                        equalsIgnoreCase("N"))
                    aqDel.id(rdskintoskinNo + i).checked(true);

                if (tblChildInfo.getChlPhysicalDisability() != null && tblChildInfo.getChlPhysicalDisability().
                        equalsIgnoreCase("Y")) {
                    aqDel.id(rdPhyYesCh + i).checked(true);
                    aqDel.id(R.id.trOtherPhy + i).visible();
                    aqDel.id(R.id.etotherPhy + i).text(tblChildInfo.getChlOtherPhysicalDisability());
                } else if (tblChildInfo.getChlPhysicalDisability() != null && tblChildInfo.getChlPhysicalDisability().
                        equalsIgnoreCase("N")) {
                    aqDel.id(rdphyNoCh + i).checked(true);
                    aqDel.id(R.id.trOtherPhy + i).gone();
                }

//27Nov2019 Arpitha
                if (tblChildInfo.getChlIsAshaAcompany() != null
                        && tblChildInfo.getChlIsAshaAcompany().
                        equalsIgnoreCase("Y")) {
                    aqDel.id(rdAshaYes + i).checked(true);
                } else if (tblChildInfo.getChlIsAshaAcompany() != null
                        && tblChildInfo.getChlIsAshaAcompany().
                        equalsIgnoreCase("N")) {
                    aqDel.id(rdAshaNo + i).checked(true);
                }//27Nov2019 Arpitha


                if (tblChildInfo.getChlIUDType() != null && tblChildInfo.getChlIUDType().
                        equalsIgnoreCase("Fresh"))
                    aqDel.id(rdFresh + i).checked(true);
                else if (tblChildInfo.getChlIUDType() != null && tblChildInfo.getChlIUDType().
                        equalsIgnoreCase("Macerated"))
                    aqDel.id(rdMacerated + i).checked(true);

                aqDel.id(etInfantDeathDate + i).text(tblChildInfo.getChlInfantDeathDate());
                aqDel.id(etOtherPhy + i).text(tblChildInfo.getChlOtherPhysicalDisability());


               /* String[] chlCompl = tblChildInfo.getChlComplications().split(",");


                if (i == 1) {
                    chlCompl1.setSelection(chlCompl);
                    *//*ArrayAdapter<String>  simple_adapter = new ArrayAdapter<String>(this,
                            android.R.layout.simple_spinner_item);
                    simple_adapter.addAll(chlCompl);
                    chlCompl1.setAdapter(simple_adapter);*//*

                } else if (i == 2)
                    chlCompl2.setSelection(chlCompl);
                else if (i == 3)
                    chlCompl3.setSelection(chlCompl);
                else if (i == 4)
                    chlCompl4.setSelection(chlCompl);
*/


                if (tblChildInfo.getChlComplications() != null && tblChildInfo.getChlComplications().trim().length() > 0) {
                    String[] chlCompli = tblChildInfo.getChlComplications().split(",");

//                    if( tblChildInfo.getChlComplications().contains("9"))
//                        trothercomplchl.s


                    int[] selectedIds = new int[chlCompli.length];

                    if (chlCompli != null && chlCompli.length > 0) {
                        for (int j = 0; j < chlCompli.length; j++) {
                            selectedIds[j] = Integer.parseInt(chlCompli[j].trim());
                        }
                    }

                    if (i == 1) {
                        chlCompl1.setSelection(selectedIds);

                    } else if (i == 2)
                        chlCompl2.setSelection(selectedIds);
                    else if (i == 3)
                        chlCompl3.setSelection(selectedIds);
                    else if (i == 4)
                        chlCompl4.setSelection(selectedIds);
                }


                aqDel.id(etDelTypeOther + i).text(tblChildInfo.getChlDelTypeOther());
                aqDel.id(etDelConName + i).text(tblChildInfo.getDelDeliveryConductedByName());
                aqDel.id(etDelByOther + i).text(tblChildInfo.getChlDeliveredByOther());

                // aqDel.id(spnDelPlace + i).setSelection(0);Arpitha 31Dec2019
                aqDel.id(spnDelBy + i).setSelection(0);

                if (tblChildInfo.getChlDeliveryType() != null) {
                    if (tblChildInfo.getChlDeliveryType().equalsIgnoreCase("Normal"))
                        aqDel.id(spnDelType + i).setSelection
                                (1);
                    else if (tblChildInfo.getChlDeliveryType().equalsIgnoreCase("Cesarean"))
                        aqDel.id(spnDelType + i).setSelection
                                (2);
                    else if (tblChildInfo.getChlDeliveryType().equalsIgnoreCase("Instrumental"))
                        aqDel.id(spnDelType + i).setSelection
                                (3);
                    else if (tblChildInfo.getChlDeliveryType().equalsIgnoreCase("Other"))
                        aqDel.id(spnDelType + i).setSelection
                                (4);
                    else
                        aqDel.id(spnDelType + i).setSelection
                                (0);

                }


                if (tblChildInfo.getChlDeliveredBy() != null) {
                    if (tblChildInfo.getChlDeliveredBy().equalsIgnoreCase("Gynecologist"))
                        aqDel.id(spnDelBy + i).setSelection
                                (1);
                    else if (tblChildInfo.getChlDeliveredBy().equalsIgnoreCase("Medical Officer"))
                        aqDel.id(spnDelBy + i).setSelection
                                (2);
                    else if (tblChildInfo.getChlDeliveredBy().equalsIgnoreCase("Staff Nurse"))
                        aqDel.id(spnDelBy + i).setSelection
                                (3);
                    else if (tblChildInfo.getChlDeliveredBy().equalsIgnoreCase("ANM"))
                        aqDel.id(spnDelBy + i).setSelection
                                (4);
                    else if (tblChildInfo.getChlDeliveredBy().equalsIgnoreCase("ASHA"))
                        aqDel.id(spnDelBy + i).setSelection
                                (5); //11Apr2021 Bindu - add spn del by
                    else if (tblChildInfo.getChlDeliveredBy().equalsIgnoreCase("Trained Dai"))
                        aqDel.id(spnDelBy + i).setSelection
                                (6);
                    else if (tblChildInfo.getChlDeliveredBy().equalsIgnoreCase("Untrained Dai"))
                        aqDel.id(spnDelBy + i).setSelection
                                (7);
                    else if (tblChildInfo.getChlDeliveredBy().equalsIgnoreCase("Mother/Mother-in-law/Relatives"))
                        aqDel.id(spnDelBy + i).setSelection
                                (8);
                    else if (tblChildInfo.getChlDeliveredBy().equalsIgnoreCase("Others"))
                        aqDel.id(spnDelBy + i).setSelection
                                (9);
                    else
                        aqDel.id(spnDelBy + i).setSelection
                                (0);

                }


//                setTribalHamlet();

                int villageCode = getKey(mapVillage, "" + tblChildInfo.getChlTribalHamlet());

                aqDel.id(spntribalhamletdel + i).setSelection(villageCode);


                if (tblChildInfo.getChlDelPlace() != null) {
                    // 11Apr2021 Bindu change the ordering and compare file

                    aqDel.id(spnDelPlace + i).setSelection(Arrays.asList(getResources().getStringArray(R.array.deliveryplaceedit)).indexOf(tblChildInfo.getChlDelPlace()));

                     /*if (tblChildInfo.getChlDelPlace().equalsIgnoreCase("Home"))
                        aqDel.id(spnDelPlace + i).setSelection
                                (6);
                    else if (tblChildInfo.getChlDelPlace().equalsIgnoreCase("HSC"))
                        aqDel.id(spnDelPlace + i).setSelection
                                (5);
                    else if (tblChildInfo.getChlDelPlace().equalsIgnoreCase("PHC"))
                        aqDel.id(spnDelPlace + i).setSelection
                                (1);
                    else if (tblChildInfo.getChlDelPlace().equalsIgnoreCase("FRU/CHC"))
                        aqDel.id(spnDelPlace + i).setSelection
                                (2);
                    else if (tblChildInfo.getChlDelPlace().equalsIgnoreCase("FRU/TH"))
                        aqDel.id(spnDelPlace + i).setSelection
                                (3);
                    else if (tblChildInfo.getChlDelPlace().equalsIgnoreCase("FRU/DH"))
                        aqDel.id(spnDelPlace + i).setSelection
                                (4);
                    else if (tblChildInfo.getChlDelPlace().equalsIgnoreCase("FRU/Private"))
                        aqDel.id(spnDelPlace + i).setSelection
                                (7);
                    else if (tblChildInfo.getChlDelPlace().equalsIgnoreCase("On the way"))
                        aqDel.id(spnDelPlace + i).setSelection
                                (8); //11pr2021 Bindu add On the way
                    else if (tblChildInfo.getChlDelPlace().equalsIgnoreCase("Others"))
                        aqDel.id(spnDelPlace + i).setSelection
                                (9);
                    else
                        aqDel.id(spnDelPlace + i).setSelection
                                (0);
*/
                }


                if (aqDel.id(etWeight + i).getText().toString().trim().length() > 0) {
                    if (Double.parseDouble(aqDel.id(etWeight + i).getText().toString()) < 2500) {
                        aqDel.id(chkLessWeight + i).checked(true);
                    } else
                        aqDel.id(chkLessWeight + i).checked(false);
                } else
                    aqDel.id(chkLessWeight + i).checked(false);


                aqDel.id(etothercomplchl + i).text(tblChildInfo.getChlOtherComplications());

                if(tblChildInfo.getChlComplications().contains("9"))//09AUg2021 Arpitha
                    aqDel.id(trothercomplchl+i).visible();
                else
                    aqDel.id(trothercomplchl + i).gone();

            }
        }

        ScrollView scrDel = findViewById(R.id.scrdel);
        TableLayout tblChild1 = findViewById(R.id.tblchild1);
        TableLayout tblChild2 = findViewById(R.id.tblchild2);
        TableLayout tblChild3 = findViewById(R.id.tblchild3);
        TableLayout tblChild4 = findViewById(R.id.tblchild4);

        if (childListOld.size() > 0 && deliveryInfoList.size() > 0) {
            disableEnableControls(false, scrDel);
            disableEnableControls(false, tblChild1);
            disableEnableControls(false, tblChild2);
            disableEnableControls(false, tblChild3);
            disableEnableControls(false, tblChild4);


            if (aqDel.id(R.id.etrchid1).getText().toString().trim().length() <= 0) {
                aqDel.id(R.id.etrchid1).enabled(true);
            } else
                aqDel.id(R.id.etrchid1).enabled(false);

            if (aqDel.id(R.id.etrchid2).getText().toString().trim().length() <= 0) {
                aqDel.id(R.id.etrchid2).enabled(true);
            } else
                aqDel.id(R.id.etrchid2).enabled(false);

            if (aqDel.id(R.id.etrchid3).getText().toString().trim().length() <= 0) {
                aqDel.id(R.id.etrchid3).enabled(true);
            } else
                aqDel.id(R.id.etrchid3).enabled(false);

            if (aqDel.id(R.id.etrchid4).getText().toString().trim().length() <= 0) {
                aqDel.id(R.id.etrchid4).enabled(true);
            } else
                aqDel.id(R.id.etrchid4).enabled(false);


            aqDel.id(R.id.etbabyname1).enabled(true);
            aqDel.id(R.id.etbabyname2).enabled(true);
            aqDel.id(R.id.etbabyname3).enabled(true);
            aqDel.id(R.id.etbabyname4).enabled(true);


            if (deliveryInfoList.get(0).getDelMothersDeath() != null &&
                    deliveryInfoList.get(0).getDelMothersDeath()
                            .equalsIgnoreCase("Y")) {
                aqDel.id(R.id.spnmothercomplications).enabled(false);
                aqDel.id(R.id.etothercomplmother).enabled(false);
                aqDel.id(R.id.etcommentsdel).enabled(false);
            } else {
                aqDel.id(R.id.spnmothercomplications).enabled(true);
                aqDel.id(R.id.etothercomplmother).enabled(true);
                aqDel.id(R.id.etcommentsdel).enabled(true);
            }

            if (childListOld.size() >= 1 && childListOld.get(0).getChlDeliveryResult() > 1) {
                aqDel.id(R.id.spncomplicationschl1).enabled(false);
                aqDel.id(R.id.etcommentschl1).enabled(false);
                aqDel.id(R.id.etothercomplchl1).enabled(false);
                aqDel.id(R.id.llchl1expand).enabled(true);


            } else {
                aqDel.id(R.id.spncomplicationschl1).enabled(true);
                aqDel.id(R.id.etcommentschl1).enabled(true);
                aqDel.id(R.id.etothercomplchl1).enabled(true);
                aqDel.id(R.id.llchl1expand).enabled(true);


            }
            if (childListOld.size() >= 2 && childListOld.get(1).getChlDeliveryResult() > 1) {
                aqDel.id(R.id.spncomplicationschl2).enabled(false);
                aqDel.id(R.id.etcommentschl2).enabled(false);
                aqDel.id(R.id.etothercomplchl2).enabled(false);
                aqDel.id(R.id.llchl2expand).enabled(true);


            } else {
                aqDel.id(R.id.spncomplicationschl2).enabled(true);
                aqDel.id(R.id.etcommentschl2).enabled(true);
                aqDel.id(R.id.etothercomplchl2).enabled(true);
                aqDel.id(R.id.llchl2expand).enabled(true);


            }
            if (childListOld.size() >= 3 && childListOld.get(2).getChlDeliveryResult() > 1) {
                aqDel.id(R.id.spncomplicationschl3).enabled(false);
                aqDel.id(R.id.etcommentschl3).enabled(false);
                aqDel.id(R.id.etothercomplchl3).enabled(false);
                aqDel.id(R.id.llchl3expand).enabled(true);


            } else {
                aqDel.id(R.id.spncomplicationschl3).enabled(true);
                aqDel.id(R.id.etcommentschl3).enabled(true);
                aqDel.id(R.id.etothercomplchl3).enabled(true);
                aqDel.id(R.id.llchl3expand).enabled(true);


            }
            if (childListOld.size() >= 4 && childListOld.get(3).getChlDeliveryResult() > 1) {
                aqDel.id(R.id.spncomplicationschl4).enabled(false);
                aqDel.id(R.id.etcommentschl4).enabled(false);
                aqDel.id(R.id.etothercomplchl4).enabled(false);
                aqDel.id(R.id.llchl4expand).enabled(true);


            } else {
                aqDel.id(R.id.spncomplicationschl4).enabled(true);
                aqDel.id(R.id.etcommentschl4).enabled(true);
                aqDel.id(R.id.etothercomplchl4).enabled(true);
                aqDel.id(R.id.llchl4expand).enabled(true);


            }


        }

    }


    /**
     * This method invokes when Item clicked in Spinner
     *
     * @throws Exception
     */
    public void onSpinnerClicked(AdapterView<?> adapter, View v, int position, long arg3) throws Exception {

        try {
            switch (adapter.getId()) {

                case R.id.spnnoofchild:
                    setNoOfChildren(position);
                    break;

                case R.id.spndeltype1:
                    if (aqDel.id(R.id.spndeltype1).getSelectedItemPosition() == 4) {
                        aqDel.id(R.id.trdeltypeother1).visible();
                    } else {
                        aqDel.id(R.id.trdeltypeother1).gone();
                        aqDel.id(R.id.etDelTypeOther1).text(""); //01MAy2021 Bindu reset other del type
                    }

                    int posofDelType = aqDel.id(R.id.spndeltype1).getSelectedItemPosition();
                    if (aqDel.id(R.id.spnnoofchild).getSelectedItemPosition() > 0) {
                        if (aqDel.id(R.id.spndeltype2).getSelectedItemPosition() == 0)
                            aqDel.id(R.id.spndeltype2).setSelection(posofDelType);
                    }
                    if (aqDel.id(R.id.spnnoofchild).getSelectedItemPosition() > 1) {
                        if (aqDel.id(R.id.spndeltype3).getSelectedItemPosition() == 0)
                            aqDel.id(R.id.spndeltype3).setSelection(posofDelType);
                    }
                    if (aqDel.id(R.id.spnnoofchild).getSelectedItemPosition() > 2) {
                        if (aqDel.id(R.id.spndeltype3).getSelectedItemPosition() == 0)
                            aqDel.id(R.id.spndeltype3).setSelection(posofDelType);
                    }
                    if (aqDel.id(R.id.spnnoofchild).getSelectedItemPosition() > 3) {
                        if (aqDel.id(R.id.spndeltype4).getSelectedItemPosition() == 0)
                            aqDel.id(R.id.spndeltype4).setSelection(posofDelType);
                    }


                    if (deliveryInfoList == null || (deliveryInfoList != null && deliveryInfoList.size() <= 0)) {
                    /*if (aqDel.id(R.id.spndeltype1).getSelectedItem().toString().equalsIgnoreCase("Cesarean") ||
                            aqDel.id(R.id.spndeltype1).getSelectedItem().toString().equalsIgnoreCase("Instrumental"))*/

                        if (aqDel.id(R.id.spndeltype1).getSelectedItemPosition() == 2 || aqDel.id(R.id.spndeltype1).getSelectedItemPosition() == 3) {

                            ArrayAdapter delPlaceAdapter = new ArrayAdapter(this, R.layout.simple_spinner_item,
                                    getResources().getStringArray(R.array.delalcecesarean));
                            aqDel.id(R.id.spndelplace1).adapter(delPlaceAdapter);
                        } else {
                            ArrayAdapter delPlaceAdapter = new ArrayAdapter(this, R.layout.simple_spinner_item,
                                    getResources().getStringArray(R.array.delalce));
                            aqDel.id(R.id.spndelplace1).adapter(delPlaceAdapter);
                        }
                    }
                    break;
                case R.id.spndeltype2:
                    if (aqDel.id(R.id.spndeltype2).getSelectedItemPosition() == 4) {
                        aqDel.id(R.id.trdeltypeother2).visible();
                    } else {
                        aqDel.id(R.id.trdeltypeother2).gone();
                        aqDel.id(R.id.etDelTypeOther2).text(""); //01MAy2021 Bindu reset other del type
                    }
                    if (deliveryInfoList == null || (deliveryInfoList != null && deliveryInfoList.size() <= 0)) {
                        if (aqDel.id(R.id.spndeltype2).getSelectedItemPosition() == 2 || aqDel.id(R.id.spndeltype2).getSelectedItemPosition() == 3) {

                            ArrayAdapter delPlaceAdapter = new ArrayAdapter(this, R.layout.simple_spinner_item,
                                    getResources().getStringArray(R.array.delalcecesarean));
                            aqDel.id(R.id.spndelplace2).adapter(delPlaceAdapter);
                        } else {
                            ArrayAdapter delPlaceAdapter = new ArrayAdapter(this, R.layout.simple_spinner_item,
                                    getResources().getStringArray(R.array.delalce));
                            aqDel.id(R.id.spndelplace2).adapter(delPlaceAdapter);
                        }
                    }
                    break;

                case R.id.spndeltype3:
                    if (aqDel.id(R.id.spndeltype3).getSelectedItemPosition() == 4) {
                        aqDel.id(R.id.trdeltypeother3).visible();
                    } else {
                        aqDel.id(R.id.trdeltypeother3).gone();
                        aqDel.id(R.id.etDelTypeOther3).text(""); //01MAy2021 Bindu reset other del type
                    }
                    if (deliveryInfoList == null || (deliveryInfoList != null && deliveryInfoList.size() <= 0)) {
                        if (aqDel.id(R.id.spndeltype3).getSelectedItemPosition() == 2 || aqDel.id(R.id.spndeltype3).getSelectedItemPosition() == 3) {

                            ArrayAdapter delPlaceAdapter = new ArrayAdapter(this, R.layout.simple_spinner_item,
                                    getResources().getStringArray(R.array.delalcecesarean));
                            aqDel.id(R.id.spndelplace3).adapter(delPlaceAdapter);
                        } else {
                            ArrayAdapter delPlaceAdapter = new ArrayAdapter(this, R.layout.simple_spinner_item,
                                    getResources().getStringArray(R.array.delalce));
                            aqDel.id(R.id.spndelplace3).adapter(delPlaceAdapter);
                        }
                    }
                    break;

                case R.id.spndeltype4:
                    if (aqDel.id(R.id.spndeltype4).getSelectedItemPosition() == 4) {
                        aqDel.id(R.id.trdeltypeother4).visible();
                    } else {
                        aqDel.id(R.id.trdeltypeother4).gone();
                        aqDel.id(R.id.etDelTypeOther4).text(""); //01MAy2021 Bindu reset other del type
                    }
                    if (deliveryInfoList == null || (deliveryInfoList != null && deliveryInfoList.size() <= 0)) {
                        if (aqDel.id(R.id.spndeltype4).getSelectedItemPosition() == 2 ||
                                aqDel.id(R.id.spndeltype4).getSelectedItemPosition() == 3) {

                            ArrayAdapter delPlaceAdapter = new ArrayAdapter(this, R.layout.simple_spinner_item,
                                    getResources().getStringArray(R.array.delalcecesarean));
                            aqDel.id(R.id.spndelplace4).adapter(delPlaceAdapter);
                        } else {
                            ArrayAdapter delPlaceAdapter = new ArrayAdapter(this, R.layout.simple_spinner_item,
                                    getResources().getStringArray(R.array.delalce));
                            aqDel.id(R.id.spndelplace4).adapter(delPlaceAdapter);
                        }
                    }

                    break;

                case R.id.spndelby1:
                    if (aqDel.id(R.id.spndelby1).getSelectedItemPosition() == 9) { //01May2021 Bindu - Change pos for others
                        aqDel.id(R.id.trdelbyothers1).visible();
                    } else {
                        aqDel.id(R.id.trdelbyothers1).gone();
                        aqDel.id(R.id.etdelbyother1).text("");//03May2021 Arpitha
                    }

                    int posofDelBy = aqDel.id(R.id.spndelby1).getSelectedItemPosition();
                    if (aqDel.id(R.id.spnnoofchild).getSelectedItemPosition() > 1) {
                        if (aqDel.id(R.id.spndelby2).getSelectedItemPosition() == 0)
                            aqDel.id(R.id.spndelby2).setSelection(posofDelBy);
                    }
                    if (aqDel.id(R.id.spnnoofchild).getSelectedItemPosition() > 2) {
                        if (aqDel.id(R.id.spndelby3).getSelectedItemPosition() == 0)
                            aqDel.id(R.id.spndelby3).setSelection(posofDelBy);
                    }
                    if (aqDel.id(R.id.spnnoofchild).getSelectedItemPosition() > 3) {
                        if (aqDel.id(R.id.spndelby4).getSelectedItemPosition() == 0)
                            aqDel.id(R.id.spndelby4).setSelection(posofDelBy);
                    }
                    break;

                case R.id.spndelby2:
                    if (aqDel.id(R.id.spndelby2).getSelectedItemPosition() == 9) { //01May2021 Bindu - Change pos for others
                        aqDel.id(R.id.trdelbyothers2).visible();
                    } else
                    {
                        aqDel.id(R.id.trdelbyothers2).gone();
                    aqDel.id(R.id.etdelbyother2).text("");//03May2021 Arpitha
            }
                    break;

                case R.id.spndelby3:
                    if (aqDel.id(R.id.spndelby3).getSelectedItemPosition() == 9) { //01May2021 Bindu - Change pos for others
                        aqDel.id(R.id.trdelbyothers3).visible();
                    } else
                    {
                        aqDel.id(R.id.trdelbyothers3).gone();
                    aqDel.id(R.id.etdelbyother3).text("");//03May2021 Arpitha
            }
                    break;

                case R.id.spndelby4:
                    if (aqDel.id(R.id.spndelby4).getSelectedItemPosition() == 9) { //01May2021 Bindu - Change pos for others
                        aqDel.id(R.id.trdelbyothers4).visible();
                    } else
                    {
                        aqDel.id(R.id.trdelbyothers4).gone();
                    aqDel.id(R.id.etdelbyother4).text("");//03May2021 Arpitha
            }
                    break;
               /* case R.id.spnepisotmy:
                    if (aqDel.id(R.id.spnepisotmy).getSelectedItem().toString().equalsIgnoreCase("Other")) {
                        aqDel.id(R.id.trepisotmyother).visible();
                    } else
                        aqDel.id(R.id.trepisotmyother).gone();
                    break;*/

                case R.id.spncauseofdeath:
                    if (aqDel.id(R.id.spncauseofdeath).getSelectedItemPosition() == 5) {
                        aqDel.id(R.id.trothercasue).visible();
                    } else {
                        aqDel.id(R.id.trothercasue).gone();
                        aqDel.id(R.id.etothercauseofdeath).text("");
                    }
                    break;

                case R.id.spndelresult1:
                    if (aqDel.id(R.id.spndelresult1).getSelectedItemPosition() == 3) {
                        aqDel.id(R.id.triudtype1).visible();
                        aqDel.id(R.id.trdeathdate1).visible();
                        if (!(deliveryInfoList != null && deliveryInfoList.size() > 0))
                            aqDel.id(R.id.etinfantdeathdate1).text("");
                        disableFields(1, false);
                    } else if (aqDel.id(R.id.spndelresult1).getSelectedItemPosition() == 2) {
                        aqDel.id(R.id.triudtype1).gone();
                        aqDel.id(R.id.trdeathdate1).visible();
                        if (!(deliveryInfoList != null && deliveryInfoList.size() > 0))
                            aqDel.id(R.id.etinfantdeathdate1).text("");
                        rgiud1.clearCheck();
                        disableFields(1, false);

                    } else {
                        aqDel.id(R.id.triudtype1).gone();
                        aqDel.id(R.id.trdeathdate1).gone();
                        aqDel.id(R.id.etinfantdeathdate1).text("");
                        rgiud1.clearCheck();
                        if (!(deliveryInfoList != null && deliveryInfoList.size() > 0))
                            disableFields(1, true);

                    }
                    break;
                case R.id.spndelresult2:
                    if (aqDel.id(R.id.spndelresult2).getSelectedItemPosition() == 3) {
                        aqDel.id(R.id.triudtype2).visible();
                        aqDel.id(R.id.trdeathdate2).visible();
                        if (!(deliveryInfoList != null && deliveryInfoList.size() > 0))
                            aqDel.id(R.id.etinfantdeathdate2).text("");
                        disableFields(2, false);
                    } else if (aqDel.id(R.id.spndelresult2).getSelectedItemPosition() == 2) {
                        aqDel.id(R.id.triudtype2).gone();
                        aqDel.id(R.id.trdeathdate2).visible();
                        if (!(deliveryInfoList != null && deliveryInfoList.size() > 0))
                            aqDel.id(R.id.etinfantdeathdate2).text("");
                        rgiud2.clearCheck();
                        disableFields(2, false);
                    } else {
                        aqDel.id(R.id.triudtype2).gone();
                        aqDel.id(R.id.trdeathdate2).gone();
                        aqDel.id(R.id.etinfantdeathdate2).text("");
                        rgiud2.clearCheck();
                        if (!(deliveryInfoList != null && deliveryInfoList.size() > 0))
                            disableFields(2, true);

                    }
                    break;

                case R.id.spndelresult3:
                    if (aqDel.id(R.id.spndelresult3).getSelectedItemPosition() == 3) {
                        aqDel.id(R.id.triudtype3).visible();
                        aqDel.id(R.id.trdeathdate3).visible();
                        if (!(deliveryInfoList != null && deliveryInfoList.size() > 0))
                            aqDel.id(R.id.etinfantdeathdate3).text("");
                        disableFields(3, false);
                    } else if (aqDel.id(R.id.spndelresult3).getSelectedItemPosition() == 2) {
                        aqDel.id(R.id.triudtype3).gone();
                        aqDel.id(R.id.trdeathdate3).visible();
                        if (!(deliveryInfoList != null && deliveryInfoList.size() > 0))
                            aqDel.id(R.id.etinfantdeathdate3).text("");
                        rgiud3.clearCheck();
                        disableFields(3, false);
                    } else {
                        aqDel.id(R.id.triudtype3).gone();
                        aqDel.id(R.id.trdeathdate3).gone();
                        aqDel.id(R.id.etinfantdeathdate3).text("");
                        rgiud3.clearCheck();
                        if (!(deliveryInfoList != null && deliveryInfoList.size() > 0))
                            disableFields(3, true);//03May2021 Arpitha - 2 -->3

                    }
                    break;
                case R.id.spndelresult4:
                    if (aqDel.id(R.id.spndelresult4).getSelectedItemPosition() == 3) {
                        aqDel.id(R.id.triudtype4).visible();
                        aqDel.id(R.id.trdeathdate4).visible();
                        if (!(deliveryInfoList != null && deliveryInfoList.size() > 0))
                            aqDel.id(R.id.etinfantdeathdate4).text("");
                        disableFields(4, false);
                    } else if (aqDel.id(R.id.spndelresult4).getSelectedItemPosition() == 2) {
                        aqDel.id(R.id.triudtype4).gone();
                        aqDel.id(R.id.trdeathdate4).visible();
                        if (!(deliveryInfoList != null && deliveryInfoList.size() > 0))
                            aqDel.id(R.id.etinfantdeathdate4).text("");
                        rgiud4.clearCheck();
                        disableFields(4, false);
                    } else {
                        aqDel.id(R.id.triudtype4).gone();
                        aqDel.id(R.id.trdeathdate4).gone();
                        aqDel.id(R.id.etinfantdeathdate4).text("");
                        rgiud4.clearCheck();
                        if (!(deliveryInfoList != null && deliveryInfoList.size() > 0))
                            disableFields(4, true);
                    }
                    break;

                case R.id.spntribalhamletdel1:
                    int posofTribalHamlet = aqDel.id(R.id.spntribalhamletdel1).getSelectedItemPosition();
                    if (aqDel.id(R.id.spnnoofchild).getSelectedItemPosition() > 1) {
                        if (aqDel.id(R.id.spntribalhamletdel2).getSelectedItemPosition() == 0)
                            aqDel.id(R.id.spntribalhamletdel2).setSelection(posofTribalHamlet);
                    }
                    if (aqDel.id(R.id.spnnoofchild).getSelectedItemPosition() > 2) {
                        if (aqDel.id(R.id.spntribalhamletdel3).getSelectedItemPosition() == 0)
                            aqDel.id(R.id.spntribalhamletdel3).setSelection(posofTribalHamlet);
                    }
                    if (aqDel.id(R.id.spnnoofchild).getSelectedItemPosition() > 3) {
                        if (aqDel.id(R.id.spntribalhamletdel4).getSelectedItemPosition() == 0)
                            aqDel.id(R.id.spntribalhamletdel4).setSelection(posofTribalHamlet);
                    }
                    break;


                case R.id.spndelplace1:
                    int posofPlace = aqDel.id(R.id.spndelplace1).getSelectedItemPosition();
                    if (aqDel.id(R.id.spnnoofchild).getSelectedItemPosition() > 1) {
                        if (aqDel.id(R.id.spndelplace2).getSelectedItemPosition() == 0)
                            aqDel.id(R.id.spndelplace2).setSelection(posofPlace);
                    }
                    if (aqDel.id(R.id.spnnoofchild).getSelectedItemPosition() > 2) {
                        if (aqDel.id(R.id.spndelplace3).getSelectedItemPosition() == 0)
                            aqDel.id(R.id.spndelplace3).setSelection(posofPlace);
                    }
                    if (aqDel.id(R.id.spnnoofchild).getSelectedItemPosition() > 3) {
                        if (aqDel.id(R.id.spndelplace4).getSelectedItemPosition() == 0)
                            aqDel.id(R.id.spndelplace4).setSelection(posofPlace);
                    }
                    break;

                default:
                    break;
            }
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    @Override
    public void onBackPressed() {

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        try {
            int noOfWeeks = 0;
//            getSupportActionBar().setIcon(R.drawable.ett);
            if (woman.getRegADDate() != null && woman.getRegADDate().length() > 0) {
                getSupportActionBar().setTitle((woman.getRegWomanName() + "\t"));
            } else {
                noOfWeeks = DateTimeUtil.getNumberOfWeeksFromLMP(woman.getRegLMP());
                getSupportActionBar().setTitle((woman.getRegWomanName() + " (" + noOfWeeks + " "
                        + getResources().getString(R.string.weeks) + ")"));
            }
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer1); //27Sep2019 - Bindu - set drawer and enable true home btn
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);


            aqDel.id(R.id.tvWomanName).text(woman.getRegWomanName() + " (" + (noOfWeeks > 0 ? noOfWeeks : "") + " "
                    + getResources().getString(R.string.weeks) + ")");
//            aqServ.id(R.id.tvInfo1).text(getResources().getString(R.string.tvlmp) + " - " + woman.getRegLMP());
            if (woman.getRegADDate() != null && woman.getRegADDate().length() > 0) {
                aqDel.id(R.id.tvInfo1).text((getResources().getString(R.string.tvadd) + " " + woman.getRegADDate()));
                aqDel.id(R.id.tvWomanName).text(woman.getRegWomanName());
            } else {
                int noOfWeeks1 = DateTimeUtil.getNumberOfWeeksFromLMP(woman.getRegLMP());
                // aqDel.id(R.id.tvInfo1).text((noOfWeeks1 + getResources().getString(R.string.weeks)));
                aqDel.id(R.id.tvInfo1).text((getResources().getString(R.string.edd) + ": " + woman.getRegEDD()));
                aqDel.id(R.id.tvWomanName).text(woman.getRegWomanName() + " (" + (noOfWeeks1 > 0 ? noOfWeeks1 : "") + " "
                        + getResources().getString(R.string.weeks) + ")");
            }
            aqDel.id(R.id.ivWomanImg).gone();

            getMenuInflater().inflate(R.menu.servicesmenu, menu);
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (woman.getRegpregormotheratreg() != 2) {
            if (mDrawerToggle.onOptionsItemSelected(item)) {
                return true;
            }
        }
        switch (item.getItemId()) {
            case R.id.logout: {
                Intent goToScreen = new Intent(DeliveryInfoActivity.this, LoginActivity.class);
                displayAlert(getResources().getString(R.string.m111), goToScreen);
                return true;
            }
            case R.id.home: {
                Intent home = new Intent(DeliveryInfoActivity.this, MainMenuActivity.class);
                displayAlert(getResources().getString(R.string.exit), home);

                return true;
            }
            case R.id.wlist: {
                Intent goToScreen = new Intent(DeliveryInfoActivity.this, RegisteredWomenActionTabs.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;

            }
            case R.id.about: {
                Intent goToScreen = new Intent(DeliveryInfoActivity.this, AboutActivity.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;

            }
            case android.R.id.home: {
                Intent home = new Intent(DeliveryInfoActivity.this, MainMenuActivity.class);
                displayAlert(getResources().getString(R.string.exit), home);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    /**
     * Save Confirmation Alert
     *
     * @param goToScreen
     * @param spanText2
     */
    private void displayAlert(final String spanText2, final Intent goToScreen) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        String strButtonText;
        if (goToScreen != null)
            strButtonText = getResources().getString(R.string.m121);
        else
            strButtonText = getResources().getString(R.string.ok);
        alertDialogBuilder.setMessage(spanText2).setCancelable(false).setNegativeButton(
                strButtonText, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        try {
                            if (goToScreen != null) {

                                if (spanText2.contains(getResources().getString(R.string.save))) {
                                    if (deliveryInfoList.size() <= 0) {
                                        saveData(woman);
                                    } else {
                                        updateData();
                                    }
                                } else {
                                    goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                    startActivity(goToScreen);
                                    if (spanText2.contains("logout")) {
                                        try {
                                            LoginRepository loginRepository = new LoginRepository(databaseHelper);
                                            loginRepository.updateLogoutTime(appState.loginAuditId, appState.sessionUserId);
                                        } catch (Exception e) {
                                            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                                        }
                                    }
                                }

                            } else
                                dialog.cancel();
                        } catch (Exception e) {
                            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                        }
                    }
                });
        if (goToScreen != null) {
            alertDialogBuilder.setPositiveButton(getResources().getString(R.string.m122),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
        }
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }


    //    initialize db object
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }


    //    save registration data
    private void saveData(final tblregisteredwomen woman) {
        try {
            final RuntimeExceptionDao<tblregisteredwomen, Integer> regDAO = databaseHelper.gettblregRuntimeExceptionDao();

            final TransactionHeaderRepository transactionHeaderRepository = new TransactionHeaderRepository(databaseHelper);
            transId = transactionHeaderRepository.iCreateNewTrans(user.getUserId(), databaseHelper);
            woman.setTransId(transId);
            woman.setRegADDate(aqDel.id(R.id.etdob1).getText().toString());

            final UserRepository userRepository = new UserRepository(databaseHelper);


            TransactionManager.callInTransaction(databaseHelper.getConnectionSource(), new Callable<Void>() {
                public Void call() throws Exception {//29Aug2019 Arpitha - transaction manager

                    if (woman.getRegpregormotheratreg() == 2) {
                        final UserRepository userRepository = new UserRepository(databaseHelper);
                        woman.setWomanId(userRepository.getNewWomanId(user));


                        setData();
                        setChildData();
                        tblDelInfo.setTransId(transId);
                        RuntimeExceptionDao<tblregisteredwomen, Integer> regDAO = databaseHelper.gettblregRuntimeExceptionDao();
                        RuntimeExceptionDao<TblDeliveryInfo, Integer> delDAO
                                = databaseHelper.getDelInfoRuntimeExceptionDao();
                        RuntimeExceptionDao<TblChildInfo, Integer> childDAO
                                = databaseHelper.getChildInfoRuntimeExceptionDao();
                        int addReg = regDAO.create(woman);

                        int addChild = 0;

                        if (addReg > 0) {
                            int addDel = delDAO.create(tblDelInfo);
                            if (addDel > 0) {
                                for (int i = 0; i < childList.size(); i++) {
                                    addChild = childDAO.create(childList.get(i));
                                }

                                if (addChild > 0) {
                                    boolean addRegTrans = transactionHeaderRepository.iNewRecordTransNew(user.getUserId(), transId, regDAO.getTableName(), databaseHelper);
                                    if (addRegTrans) {
                                        boolean addDelTrans = transactionHeaderRepository.iNewRecordTransNew(user.getUserId(), transId, delDAO.getTableName(), databaseHelper);
                                        if (addDelTrans) {
                                            boolean addChildTrans = transactionHeaderRepository.iNewRecordTransNew(user.getUserId(), transId, childDAO.getTableName(), databaseHelper);


                                            //  if (added)
                                            if (addChildTrans) {
                                                String addUserUpdateTrans = userRepository.UpdateLastWomenNumberNew(user.getUserId(), transId, databaseHelper);
                                                if (addUserUpdateTrans.trim().length() > 0) {
                                                    WomanServiceRepository womanServiceRepository = new WomanServiceRepository(databaseHelper);
                                                    boolean addServices = womanServiceRepository.addServicesNew(woman, transId, appState.userType, databaseHelper, tblDelInfo, childList);


                                                    if (addServices) {


                                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.motherregsuccess), Toast.LENGTH_LONG).show();
                                                        Intent intent = new Intent(DeliveryInfoActivity.this, RegisteredWomenActionTabs.class);
                                                        intent.putExtra("appState", appState);
                                                        intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                                        intent.putExtra("wFilterStr", "");
                                                        startActivity(intent);

                                                        //MainMenuActivity.callSyncMtd();  // TODO 12Aug2019 - Bindu - Comment auto sync
                                                    } else
                                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.m091), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha
                                                } else
                                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.m091), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha

                                            } else
                                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.m091), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha
                                        } else
                                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.m091), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha
                                    } else
                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.m091), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha

                                } else
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.m091), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha
                            } else
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.m091), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha

                        } else
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.m091), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha

                    } else {
                        int addChildData = 0;
                        setData();
                        setChildData();
                        tblDelInfo.setTransId(transId);
                        RuntimeExceptionDao<TblDeliveryInfo, Integer> delDAO
                                = databaseHelper.getDelInfoRuntimeExceptionDao();
                        RuntimeExceptionDao<TblChildInfo, Integer> childDAO
                                = databaseHelper.getChildInfoRuntimeExceptionDao();
                        int addDelData = delDAO.create(tblDelInfo);
                        if (addDelData > 0) {
                            for (int i = 0; i < childList.size(); i++) {
                                addChildData = childDAO.create(childList.get(i));
                            }


                            if (addChildData > 0) {

                                String sql = "Update tblregisteredwomen SET regPregnantorMother = 2, " +
                                        "regADDate = '" + aqDel.id(R.id.etdob1).getText().toString()
                                        + "' , regInDanger = " + compl + " where WomanId = '" + woman.getWomanId() + "'";
                                updateRegAuditData();//21July2021 Arpitha
                                WomanRepository womanRepository = new WomanRepository(databaseHelper);
                                int updateReg = womanRepository.updateWomenRegDataNew
                                        (user.getUserId(), sql, transId, null, databaseHelper);

                                if (updateReg > 0) {

                                      transactionHeaderRepository.iNewRecordTransNew(user.getUserId(), transId, "tblaudittrail", databaseHelper);

                                    boolean addDelToTrans = transactionHeaderRepository.iNewRecordTransNew(user.getUserId(), transId, delDAO.getTableName(), databaseHelper);
                                    if (addDelToTrans) {
                                        boolean addChlToTrans = transactionHeaderRepository.iNewRecordTransNew(user.getUserId(), transId, childDAO.getTableName(), databaseHelper);


                                        if (addChlToTrans) {
                                            WomanServiceRepository womanServiceRepository = new WomanServiceRepository(databaseHelper);
                                            boolean addService = womanServiceRepository.addServicesNew(woman, transId, appState.userType, databaseHelper, tblDelInfo, childList);


                                            if (addService) {
                                                Toast.makeText(getApplicationContext(),
                                                        getResources().getString(R.string.deldetailsupdatedsuccessfully), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha
                                                Intent intent = new Intent(DeliveryInfoActivity.this, RegisteredWomenActionTabs.class);
                                                intent.putExtra("appState", appState);
                                                intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                                intent.putExtra("wFilterStr", "");
                                                startActivity(intent);
                                            } else
                                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.deliveryinfoupdatefailed), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha

                                        } else
                                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.deliveryinfoupdatefailed), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha

                                    } else
                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.deliveryinfoupdatefailed), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha

                                } else
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.deliveryinfoupdatefailed), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha

                            } else
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.deliveryinfoupdatefailed), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha

                        } else
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.deliveryinfoupdatefailed), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha
                    }
                    return null;
                }
            });
        } catch (Exception e) {


            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }


    void setData() throws Exception {
        tblDelInfo = new TblDeliveryInfo();
        tblDelInfo.setWomanId(woman.getWomanId());
        tblDelInfo.setUserId(woman.getUserId());
        tblDelInfo.setDelDateTimeOfDelivery(aqDel.id(R.id.etdob1).getText().toString() + " " + aqDel.id(R.id.ettob1).getText().toString());
        tblDelInfo.setDelOtherDeathCause(aqDel.id(R.id.etothercauseofdeath).getText().toString());
        tblDelInfo.setDelCauseofDeath("" + aqDel.id(R.id.spncauseofdeath).getSelectedItemPosition());
        tblDelInfo.setDelNoOfChildren(aqDel.id(R.id.spnnoofchild).getSelectedItemPosition());
        tblDelInfo.setDelComments(aqDel.id(R.id.etcommentsdel).getText().toString());

       /* String motherCompl = aqDel.id(R.id.spnmothercomplications).
                getSelectedItem().toString();
        motherCompl = motherCompl.replaceAll(",\\s+", ",");
        tblDelInfo.setDelMotherComplications(motherCompl.replace("Select", ""));

*/

        String selIds = "";
        multiComplications.getSelectedIndicies();
        selIds = multiComplications.getSelectedIndicies().toString();

        if (selIds.contains("0,"))
            selIds = selIds.replace("0,", "");
        if (selIds.contains("["))
            selIds = selIds.replace("[", "");
        if (selIds.contains("]"))
            selIds = selIds.replace("]", "");


        tblDelInfo.setDelMotherComplications(selIds);


        if (aqDel.id(R.id.rddeathyes).isChecked())
            tblDelInfo.setDelMothersDeath("Y");
        else if (aqDel.id(R.id.rddeathno).isChecked())
            tblDelInfo.setDelMothersDeath("N");
        tblDelInfo.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
        tblDelInfo.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());


        tblDelInfo.setDelOtherComplications(aqDel.id(R.id.etothercomplmother).getText().toString());

        tblDelInfo.setDelUserType(appState.userType);

        if (multiComplications.getSelectedItemsAsString() != null &&
                multiComplications.getSelectedItemsAsString().trim().length() > 0) {
            compl = 1;
        } else {
            compl = 0;

        }

        woman.setRegInDanger(compl);

        if (aqDel.id(R.id.rdacomno1).isChecked())
            tblDelInfo.setDelAshaAccompany("N");
        else if (aqDel.id(R.id.rdacomyes1).isChecked())
            tblDelInfo.setDelAshaAccompany("Y");

    }


    private void setChildData() throws Exception {

        childList = new ArrayList<>();
        for (int i = 1; i <= aqDel.id(R.id.spnnoofchild).getSelectedItemPosition(); i++) {

            //mani 10feb2021 Added new field strskintoskin
            String strPhyDis = "", strGender = "", strBreast = "", strCry = "", strIudType = "", strSkintoskin= "";
            tblChildInfo = new TblChildInfo();


            tblChildInfo.setChlRegDate(DateTimeUtil.getTodaysDate());
            tblChildInfo.setTransId(transId);
            tblChildInfo.setWomanId(woman.getWomanId());
            tblChildInfo.setUserId(user.getUserId());
            tblChildInfo.setChildNo(i);
            tblChildInfo.setChildID(woman.getWomanId() + "000" + i);
            tblChildInfo.setChildRCHID(aqDel.id(rchId + i).getText().toString());
            tblChildInfo.setChlBabyname(aqDel.id(etBabyName + i).getText().toString());
            tblChildInfo.setChlChildComments(aqDel.id(etCommentSchool + i).getText().toString());
            tblChildInfo.setChlDateTimeOfBirth(aqDel.id(etDob + i).getText().toString());
            tblChildInfo.setChlTimeOfBirth(aqDel.id(R.id.ettob+i).getText().toString());
            tblChildInfo.setDelBabyWeight(aqDel.id(etWeight + i).getText().toString());

            if (aqDel.id(rdPhyYesCh + i).isChecked())
                strPhyDis = "Y";
            else if (aqDel.id(rdphyNoCh + i).isChecked())
                strPhyDis = "N";

            tblChildInfo.setChlPhysicalDisability(strPhyDis);
            tblChildInfo.setChlOtherPhysicalDisability(aqDel.id(etOtherPhy + i).getText().toString());
            tblChildInfo.setChlOtherComplications(aqDel.id(etothercomplchl + i).getText().toString());

            int spnId = R.id.spncomplicationschl;
            String compl = "";


           /* String[] complType = getResources().getStringArray(R.array.newborncomplsave);
            String Compl = "";
            String[] selectedItems = aqDel.id(spnId+i).getSelectedItem().toString().split(",");

            int b = aqDel.id(spnId+i).getSelectedItemPosition();
            for(int j=0;j<selectedItems.length;j++) {
                Compl = Compl +""+ complType[j]+",";
            }*/


          /*  if (aqDel.id(spnId + i) != null && aqDel.id(spnId + i).getSelectedItem() != null)
                compl = aqDel.id(spnId + i).getSelectedItem().toString();
            tblChildInfo.setChlComplications(compl.replaceAll
                    (",\\s+", ",").replace("Select",
                    ""));*/


            //  chlCompl1.getSelectedIndicies();
            String selIds = "";

            if (i == 1)
                selIds = chlCompl1.getSelectedIndicies().toString();
            else if (i == 2)
                selIds = chlCompl2.getSelectedIndicies().toString();
            else if (i == 3)
                selIds = chlCompl3.getSelectedIndicies().toString();
            else if (i == 4)
                selIds = chlCompl4.getSelectedIndicies().toString();

            if (selIds.contains("0,"))
                selIds = selIds.replace("0,", "");
            if (selIds.contains("["))
                selIds = selIds.replace("[", "");
            if (selIds.contains("]"))
                selIds = selIds.replace("]", "");

            tblChildInfo.setChlComplications(selIds);


            if (aqDel.id(radioMale + i).isChecked())
                strGender = "Male";
            else if (aqDel.id(radioFemale + i).isChecked())
                strGender = "Female";
            else if (aqDel.id(radioAmbigious + i).isChecked())
                strGender = "Ambigious";

            tblChildInfo.setChlGender(strGender);
            tblChildInfo.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
            tblChildInfo.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime()); //17-aug-2021 Ramesh

            tblChildInfo.setChlDeliveryResult(aqDel.id(delResult + i).getSelectedItemPosition());

            if (aqDel.id(rdCryYes + i).isChecked())
                strCry = "Y";
            else if (aqDel.id(rdCryNo + i).isChecked())
                strCry = "N";

            if (aqDel.id(rdBreastYes + i).isChecked())
                strBreast = "Y";
            else if (aqDel.id(rdBreastNo + i).isChecked())
                strBreast = "N";

            if (aqDel.id(rdFresh + i).isChecked())
                strIudType = "Fresh";
            else if (aqDel.id(rdMacerated + i).isChecked())
                strIudType = "Macerated";

            //Mani 10feb2021
            if (aqDel.id(rdskintoskinYes + i).isChecked())
                strSkintoskin = "Y";
            else if (aqDel.id(rdskintoskinNo + i).isChecked())
                strSkintoskin = "N";

            tblChildInfo.setChlCryAfterBirth(strCry);
            tblChildInfo.setChlBreastFeed(strBreast);
            tblChildInfo.setChlIUDType(strIudType);
            tblChildInfo.setChlInfantDeathDate(aqDel.id(etInfantDeathDate + i).getText().toString());
            tblChildInfo.setChlUserType(appState.userType);
            //Mani 10feb2021 inserting skintoskincontact info
            tblChildInfo.setChlSkintoskincontact(strSkintoskin);


            String[] delTypeby = getResources().getStringArray(R.array.deltypesave);
            String posofDeltype = "";
            posofDeltype = delTypeby[aqDel.id(spnDelType + i).getSelectedItemPosition()];

            tblChildInfo.setChlDeliveryType(posofDeltype);
            tblChildInfo.setChlDelPlace(aqDel.id(spnDelPlace + i).getSelectedItem().toString());


            String[] delby = getResources().getStringArray(R.array.DeliveredBysave);
            String posof = "";
            posof = delby[aqDel.id(spnDelBy + i).getSelectedItemPosition()];

            tblChildInfo.setChlDeliveredBy(posof);

            tblChildInfo.setChlDelTypeOther(aqDel.id(etDelTypeOther + i).getText().toString());
            tblChildInfo.setChlDeliveredByOther(aqDel.id(etDelByOther + i).getText().toString());
            tblChildInfo.setDelDeliveryConductedByName(aqDel.id(etDelConName + i).getText().toString());

            String villageName = (String) aqDel.id(spntribalhamletdel + i).getSelectedItem();
            int villageCode = mapVillage.get(villageName);

            tblChildInfo.setChlTribalHamlet(villageCode);

//            27Nov2019 Arpitha
            if (aqDel.id(rdAshaYes + i).isChecked())
                tblChildInfo.setChlIsAshaAcompany("Y");
            else if (aqDel.id(rdAshaNo + i).isChecked())
                tblChildInfo.setChlIsAshaAcompany("N");
            childList.add(tblChildInfo);
        }


    }

    private void disableEnableControls(boolean enable, ViewGroup vg) {
        for (int i = 0; i < vg.getChildCount(); i++) {
            View child = vg.getChildAt(i);
            child.setEnabled(enable);
            if (child instanceof ViewGroup) {
                disableEnableControls(enable, (ViewGroup) child);
            }
        }
    }


    /**
     * This method handle touch listners, calls - displayDatePicker()
     */
    @Override
    public boolean onTouch(View v, MotionEvent event) {

        if (MotionEvent.ACTION_UP == event.getAction()) {
            try {
                switch (v.getId()) {
                    case R.id.etdob1: {

                        if (event.getRawX() >= (aqDel.id(R.id.etdob1).getEditText().getRight()
                                - aqDel.id(R.id.etdob1).getEditText().getCompoundDrawables()
                                [2].getBounds().width())) {

                            aqDel.id(R.id.etdob1).text(DateTimeUtil.getTodaysDate());
                        } else
                            assignEditTextAndCallDatePicker(aqDel.id(R.id.etdob1).getEditText());
                        break;
                    }
                    case R.id.etdob2: {
                        if (event.getRawX() >= (aqDel.id(R.id.etdob2).getEditText().getRight()
                                - aqDel.id(R.id.etdob2).getEditText().getCompoundDrawables()
                                [2].getBounds().width())) {

                            aqDel.id(R.id.etdob2).text(DateTimeUtil.getTodaysDate());
                        } else
                            assignEditTextAndCallDatePicker(aqDel.id(R.id.etdob2).getEditText());
                        break;
                    }
                    case R.id.etdob3: {
                        if (event.getRawX() >= (aqDel.id(R.id.etdob3).getEditText().getRight()
                                - aqDel.id(R.id.etdob3).getEditText().getCompoundDrawables()
                                [2].getBounds().width())) {

                            aqDel.id(R.id.etdob3).text(DateTimeUtil.getTodaysDate());
                        } else
                            assignEditTextAndCallDatePicker(aqDel.id(R.id.etdob3).getEditText());
                        break;
                    }
                    case R.id.etdob4: {
                        if (event.getRawX() >= (aqDel.id(R.id.etdob4).getEditText().getRight()
                                - aqDel.id(R.id.etdob4).getEditText().getCompoundDrawables()
                                [2].getBounds().width())) {

                            aqDel.id(R.id.etdob4).text(DateTimeUtil.getTodaysDate());
                        } else
                            assignEditTextAndCallDatePicker(aqDel.id(R.id.etdob4).getEditText());
                        break;
                    }

                    case R.id.ettob1: {
                        if (event.getRawX() >= (aqDel.id(R.id.ettob1).getEditText().getRight()
                                - aqDel.id(R.id.ettob1).getEditText().getCompoundDrawables()
                                [2].getBounds().width())) {

                            aqDel.id(R.id.ettob1).text(DateTimeUtil.getTimeIn12HourFormat(DateTimeUtil.getCurrentTime()));
                        } else
                            assignEditTextAndCallTimePicker(aqDel.id(R.id.ettob1).getEditText());
                        break;
                    }
                    case R.id.ettob2: {
                        if (event.getRawX() >= (aqDel.id(R.id.ettob2).getEditText().getRight()
                                - aqDel.id(R.id.ettob2).getEditText().getCompoundDrawables()
                                [2].getBounds().width())) {

                            aqDel.id(R.id.ettob2).text(DateTimeUtil.getTimeIn12HourFormat(DateTimeUtil.getCurrentTime()));
                        } else
                            assignEditTextAndCallTimePicker(aqDel.id(R.id.ettob2).getEditText());
                        break;
                    }
                    case R.id.ettob3: {
                        if (event.getRawX() >= (aqDel.id(R.id.ettob3).getEditText().getRight()
                                - aqDel.id(R.id.ettob3).getEditText().getCompoundDrawables()
                                [2].getBounds().width())) {

                            aqDel.id(R.id.ettob3).text(DateTimeUtil.getTimeIn12HourFormat(DateTimeUtil.getCurrentTime()));
                        } else
                            assignEditTextAndCallTimePicker(aqDel.id(R.id.ettob3).getEditText());
                        break;
                    }
                    case R.id.ettob4: {
                        if (event.getRawX() >= (aqDel.id(R.id.ettob4).getEditText().getRight()
                                - aqDel.id(R.id.ettob4).getEditText().getCompoundDrawables()
                                [2].getBounds().width())) {

                            aqDel.id(R.id.ettob4).text(DateTimeUtil.getTimeIn12HourFormat(DateTimeUtil.getCurrentTime()));
                        } else
                            assignEditTextAndCallTimePicker(aqDel.id(R.id.ettob4).getEditText());
                        break;
                    }

                    case R.id.etinfantdeathdate1: {
                        if (event.getRawX() >= (aqDel.id(R.id.etinfantdeathdate1).getEditText().getRight()
                                - aqDel.id(R.id.etinfantdeathdate1).getEditText().getCompoundDrawables()
                                [2].getBounds().width())) {

                            aqDel.id(R.id.etinfantdeathdate1).text("");
                        } else
                            assignEditTextAndCallDatePicker(aqDel.id(R.id.etinfantdeathdate1).getEditText());
                        break;
                    }

                    case R.id.etinfantdeathdate2: {
                        if (event.getRawX() >= (aqDel.id(R.id.etinfantdeathdate2).getEditText().getRight()
                                - aqDel.id(R.id.etinfantdeathdate2).getEditText().getCompoundDrawables()
                                [2].getBounds().width())) {

                            aqDel.id(R.id.etinfantdeathdate2).text("");
                        } else
                            assignEditTextAndCallDatePicker(aqDel.id(R.id.etinfantdeathdate2).getEditText());
                        break;
                    }

                    case R.id.etinfantdeathdate3: {
                        if (event.getRawX() >= (aqDel.id(R.id.etinfantdeathdate3).getEditText().getRight()
                                - aqDel.id(R.id.etinfantdeathdate3).getEditText().getCompoundDrawables()
                                [2].getBounds().width())) {

                            aqDel.id(R.id.etinfantdeathdate3).text("");
                        } else
                            assignEditTextAndCallDatePicker(aqDel.id(R.id.etinfantdeathdate3).getEditText());
                        break;
                    }
                    case R.id.etinfantdeathdate4: {
                        if (event.getRawX() >= (aqDel.id(R.id.etinfantdeathdate4).getEditText().getRight()
                                - aqDel.id(R.id.etinfantdeathdate4).getEditText().getCompoundDrawables()
                                [2].getBounds().width())) {

                            aqDel.id(R.id.etinfantdeathdate4).text("");
                        } else
                            assignEditTextAndCallDatePicker(aqDel.id(R.id.etinfantdeathdate4).getEditText());
                        break;
                    }
                    default:
                        break;
                }
            } catch (Exception e) {

                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            }
        }
        return true;
    }


    /**
     * This method opens the Date Dialog and sets Date to respective EditText
     */
    private void assignEditTextAndCallDatePicker(EditText editText) {
        currentEditTextForDate = editText;
        String currDate = (editText.getText().length() > 0) ? editText.getText().toString() : "";
        CommonDatePickerFragment.getCommonDate(this, currDate);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {

        try {
            if (view.isShown()) {
                String bldSmpldate = null;
                bldSmpldate = String.format("%02d", day) + "-" + String.format("%02d", month + 1) + "-"
                        + year;

                Date selectedDate = new SimpleDateFormat("dd-MM-yyyy").parse(bldSmpldate);

                Date regDate = null;

                if (aqDel.id(R.id.etdob1).getText().toString() != null && aqDel.id(R.id.etdob1).getText().toString().trim().length() > 0)
                    regDate = new SimpleDateFormat("dd-MM-yyyy").parse(aqDel.id(R.id.etdob1).getText().toString());


                int daysdiff = DateTimeUtil
                        .getDaysBetweenDates(DateTimeUtil.getTodaysDate(), bldSmpldate);


                if (currentEditTextForDate == aqDel.id(R.id.etdob1).getEditText()) {


                    if (selectedDate.after(new Date())) {
                        displayAlert(getResources().getString(R.string.date_cannot_af_currentdate), null);
                        aqDel.id(R.id.etdob1).text(DateTimeUtil.getTodaysDate());
                    } else if (daysdiff > 365) {// Arpitha 28Jun2018 - 23Dec2019 Arpitha - allow max 1 year

                        displayAlert(getResources().getString(R.string.datecannotbeafteroneyearfromcurrentdate), null);

                        aqDel.id(R.id.etMotherAdd).text("");
                    }

                    /*else if (regDate != null && selectedDate.before(regDate)) {
                        displayAlert(getResources().getString(R.string.date_cannot_be_before_del_date), null);
                        aqDel.id(R.id.etdob1).text(DateTimeUtil.getTodaysDate());
                    } */
                    else {
                        aqDel.id(R.id.etdob1).text(bldSmpldate);
                    } aqDel.id(R.id.txtadd).text(": " + bldSmpldate);
                } else if (currentEditTextForDate == aqDel.id(R.id.etdob2).getEditText()) {


                    if (selectedDate.after(new Date())) {
                        displayAlert(getResources().getString(R.string.date_cannot_af_currentdate), null);
                        aqDel.id(R.id.etdob2).text(DateTimeUtil.getTodaysDate());
                    } else if (regDate != null && selectedDate.before(regDate)) {
                        displayAlert(getResources().getString(R.string.date_cannot_be_before_del_date), null);
                        aqDel.id(R.id.etdob2).text(DateTimeUtil.getTodaysDate());
                    } else
                        aqDel.id(R.id.etdob2).text(bldSmpldate);
                } else if (currentEditTextForDate == aqDel.id(R.id.etdob3).getEditText()) {


                    if (selectedDate.after(new Date())) {
                        displayAlert(getResources().getString(R.string.date_cannot_af_currentdate), null);
                        aqDel.id(R.id.etdob3).text(DateTimeUtil.getTodaysDate());
                    } else if (regDate != null && selectedDate.before(regDate)) {
                        displayAlert(getResources().getString(R.string.date_cannot_be_before_del_date), null);
                        aqDel.id(R.id.etdob3).text(DateTimeUtil.getTodaysDate());
                    } else
                        aqDel.id(R.id.etdob3).text(bldSmpldate);
                } else if (currentEditTextForDate == aqDel.id(R.id.etdob4).getEditText()) {


                    if (selectedDate.after(new Date())) {
                        displayAlert(getResources().getString(R.string.date_cannot_af_currentdate), null);
                        aqDel.id(R.id.etdob4).text(DateTimeUtil.getTodaysDate());
                    } else if (regDate != null && selectedDate.before(regDate)) {
                        displayAlert(getResources().getString(R.string.date_cannot_be_before_del_date), null);
                        aqDel.id(R.id.etdob4).text(DateTimeUtil.getTodaysDate());
                    } else
                        aqDel.id(R.id.etdob4).text(bldSmpldate);
                } else if (currentEditTextForDate == aqDel.id(R.id.etinfantdeathdate1).getEditText()) {


                    if (selectedDate.after(new Date())) {
                        displayAlert(getResources().getString(R.string.date_cannot_af_currentdate), null);
                        aqDel.id(R.id.etinfantdeathdate1).text("");
                    } /*else if (regDate != null && selectedDate.before(regDate)) {
                        displayAlert(getResources().getString(R.string.infantdeathdate_cannot_before_regdate), null);
                        aqDel.id(R.id.etinfantdeathdate1).text("");
                    } TODO*/ else
                        aqDel.id(R.id.etinfantdeathdate1).text(bldSmpldate);
                } else if (currentEditTextForDate == aqDel.id(R.id.etinfantdeathdate2).getEditText()) {


                    if (selectedDate.after(new Date())) {
                        displayAlert(getResources().getString(R.string.date_cannot_af_currentdate), null);
                        aqDel.id(R.id.etinfantdeathdate2).text("");
                    } /*else if (regDate != null && selectedDate.before(regDate)) {
                        displayAlert(getResources().getString(R.string.infantdeathdate_cannot_before_regdate), null);
                        aqDel.id(R.id.etinfantdeathdate2).text("");
                    }TODO*/ else
                        aqDel.id(R.id.etinfantdeathdate2).text(bldSmpldate);
                } else if (currentEditTextForDate == aqDel.id(R.id.etinfantdeathdate3).getEditText()) {


                    if (selectedDate.after(new Date())) {
                        displayAlert(getResources().getString(R.string.date_cannot_af_currentdate), null);
                        aqDel.id(R.id.etinfantdeathdate3).text("");
                    } /*else if (regDate != null && selectedDate.before(regDate)) {
                        displayAlert(getResources().getString(R.string.infantdeathdate_cannot_before_regdate), null);
                        aqDel.id(R.id.etinfantdeathdate3).text("");
                    } TODO*/ else
                        aqDel.id(R.id.etinfantdeathdate3).text(bldSmpldate);
                } else if (currentEditTextForDate == aqDel.id(R.id.etinfantdeathdate4).getEditText()) {


                    if (selectedDate.after(new Date())) {
                        displayAlert(getResources().getString(R.string.date_cannot_af_currentdate), null);
                        aqDel.id(R.id.etinfantdeathdate4).text("");
                    } /*else if (regDate != null && selectedDate.before(regDate)) {
                        displayAlert(getResources().getString(R.string.infantdeathdate_cannot_before_regdate), null);
                        aqDel.id(R.id.etinfantdeathdate4).text("");
                    } TODO*/ else
                        aqDel.id(R.id.etinfantdeathdate4).text(bldSmpldate);
                }

            }
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }


    boolean validateFields() {
        if (aqDel.id(R.id.spnnoofchild).getSelectedItemPosition() == 0) {
            displayAlert(getResources().getString(R.string.selectnoofbirths), null);

            TextView errorText = (TextView) aqDel.id(R.id.spnnoofchild).getSpinner().getSelectedView();
            if (errorText != null) {
                errorText.setError("");
                errorText.setTextColor(Color.RED);//just to highlight that this is an error
                errorText.setText(getResources().getString(R.string.selectnoofbirths));//changes the selected item text to this
                errorText.setFocusable(true);
            }

            return false;
        } else if (!(aqDel.id(R.id.rddeathno).isChecked() || aqDel.id(R.id.rddeathyes).isChecked())) {
            displayAlert(getResources().getString(R.string.selectmotherdeath), null);

            return false;
        } else if (aqDel.id(R.id.spncauseofdeath).getSelectedItemPosition() == 0
                && aqDel.id(R.id.rddeathyes).isChecked()) {
            displayAlert(getResources().getString(R.string.selectcauseofdeath), null);
            TextView errorText = (TextView) aqDel.id(R.id.spncauseofdeath).getSpinner().getSelectedView();
            if (errorText != null) {
                errorText.setError("");
                errorText.setTextColor(Color.RED);//just to highlight that this is an error
                errorText.setText(getResources().getString(R.string.selectcauseofdeath));//changes the selected item text to this
                errorText.setFocusable(true);
            }

            return false;
        } else if (aqDel.id(R.id.spncauseofdeath).getSelectedItemPosition() == 5
                && aqDel.id(R.id.etothercauseofdeath).getText().toString().trim().length() <= 0) {
            displayAlert(getResources().getString(R.string.entercauseofdeath), null);
            aqDel.id(R.id.etothercauseofdeath).getEditText().requestFocus();

            return false;
        }

        if (aqDel.id(R.id.spnnoofchild).getSelectedItemPosition() > 0) {

            aqDel.id(R.id.tblchild2).gone();
            aqDel.id(R.id.imgchl2expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
            aqDel.id(R.id.tblchild3).gone();
            aqDel.id(R.id.imgchl3expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
            aqDel.id(R.id.tblchild1).visible();
            aqDel.id(R.id.imgchl1expand).getImageView().setBackground(getResources().getDrawable(R.drawable.expand));

            aqDel.id(R.id.tblchild4).gone();
            aqDel.id(R.id.imgchl4expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));


            if (aqDel.id(R.id.etdob1).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterdob), null);
                aqDel.id(R.id.etdob1).getEditText().requestFocus();
                return false;
            } else if (aqDel.id(R.id.ettob1).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.entertob), null);
                aqDel.id(R.id.ettob1).getEditText().requestFocus();
                return false;
            } else if (aqDel.id(R.id.spndelresult1).getSelectedItemPosition() == 0) {
                displayAlert(getResources().getString(R.string.selctdelresult), null);

                TextView errorText = (TextView) aqDel.id(R.id.spndelresult1).getSpinner().getSelectedView();

                if (errorText != null) {
                    errorText.setError("");
                    errorText.setTextColor(Color.RED);//just to highlight that this is an error
                    errorText.setText(getResources().getString(R.string.selctdelresult));//changes the selected item text to this
                    errorText.setFocusable(true);
                }
                return false;
            } else if (aqDel.id(R.id.spndelresult1).getSelectedItemPosition() > 1 &&
                    aqDel.id(R.id.etinfantdeathdate1).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterdeathdate), null);
                aqDel.id(R.id.etinfantdeathdate1).getEditText().requestFocus();
                return false;
            } else if (aqDel.id(R.id.spndelresult1).getSelectedItemPosition() == 3 &&
                    !(aqDel.id(R.id.rdfresh1).isChecked() || aqDel.id(R.id.rdmacerated1).isChecked())) {
                displayAlert(getResources().getString(R.string.selctiudtype), null);
                return false;
            } else if (aqDel.id(R.id.etbabyname1).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enternewbornname), null);
                aqDel.id(R.id.etbabyname1).getEditText().requestFocus();
                return false;
            } else if (aqDel.id(R.id.etWeight1).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterbirthweight), null);
                aqDel.id(R.id.etWeight1).getEditText().requestFocus();
                return false;
            } else if ((!(deliveryInfoList != null && deliveryInfoList.size() > 0)) &&
                    aqDel.id(R.id.spntribalhamletdel1).getSelectedItemPosition() == 0) {
                displayAlert(getResources().getString(R.string.m184), null);

                TextView errorText = (TextView) aqDel.id(R.id.spntribalhamletdel1).getSpinner().getSelectedView();
                if (errorText != null) {
                    errorText.setError("");
                    errorText.setTextColor(Color.RED);//just to highlight that this is an error
                    errorText.setText(getResources().getString(R.string.m184));//changes the selected item text to this
                    errorText.setFocusable(true);
                }
                return false;
            } else if (aqDel.id(R.id.spndeltype1).getSelectedItemPosition() == 0) {
                displayAlert(getResources().getString(R.string.selecttypeofdel), null);

                TextView errorText = (TextView) aqDel.id(R.id.spndeltype1).getSpinner().getSelectedView();
                if (errorText != null) {
                    errorText.setError("");
                    errorText.setTextColor(Color.RED);//just to highlight that this is an error
                    errorText.setText(getResources().getString(R.string.selecttypeofdel));//changes the selected item text to this
                    errorText.setFocusable(true);
                }

                return false;
            } else if (aqDel.id(R.id.spndeltype1).getSelectedItemPosition() == 4
                    && aqDel.id(R.id.etDelTypeOther1).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterothertypeofdel), null);
                aqDel.id(R.id.etDelTypeOther1).getEditText().requestFocus();
                return false;
            } else if (aqDel.id(R.id.spndelplace1).getSelectedItemPosition() == 0) {
                displayAlert(getResources().getString(R.string.selectplaceofdel), null);


                TextView errorText = (TextView) aqDel.id(R.id.spndelplace1).getSpinner().getSelectedView();
                if (errorText != null) {
                    errorText.setError("");
                    errorText.setTextColor(Color.RED);//just to highlight that this is an error
                    errorText.setText(getResources().getString(R.string.selectplaceofdel));//changes the selected item text to this
                    errorText.setFocusable(true);
                }
                return false;
            } else if (aqDel.id(R.id.spndelby1).getSelectedItemPosition() == 0) {
                displayAlert(getResources().getString(R.string.selectdelby), null);

                TextView errorText = (TextView) aqDel.id(R.id.spndelby1).getSpinner().getSelectedView();
                if (errorText != null) {
                    errorText.setError("");
                    errorText.setTextColor(Color.RED);//just to highlight that this is an error
                    errorText.setText(getResources().getString(R.string.selectdelby));//changes the selected item text to this
                    errorText.setFocusable(true);
                }

                return false;
            } else if (aqDel.id(R.id.spndelby1).getSelectedItemPosition() == 9 &&
                    aqDel.id(R.id.etdelbyother1).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterotherdelby), null);
                aqDel.id(R.id.etdelbyother1).getEditText().requestFocus();

                return false;
            } else if (aqDel.id(R.id.etdelconductedbyname1).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterdelcondbyname), null);
                aqDel.id(R.id.etdelconductedbyname1).getEditText().requestFocus();

                return false;
            } else if (!(aqDel.id(R.id.radiofemale1).isChecked() ||
                    aqDel.id(R.id.radiomale1).isChecked() ||
                    aqDel.id(R.id.radioambi1).isChecked())) {
                displayAlert(getResources().getString(R.string.selectgender), null);
                return false;
            } else if (aqDel.id(R.id.spndelresult1).getSelectedItemPosition() == 1
                    && !(aqDel.id(R.id.rdcryno1).isChecked() ||
                    aqDel.id(R.id.rdcryyes1).isChecked())) {
                displayAlert(getResources().getString(R.string.selectcry), null);
                return false;
            } else if (aqDel.id(R.id.spndelresult1).getSelectedItemPosition() == 1 && !(aqDel.id(R.id.rdbreastno1).isChecked() ||
                    aqDel.id(R.id.rdbreastyes1).isChecked())) {
                displayAlert(getResources().getString(R.string.selectbreastfeed), null);
                return false;
                //Mani 10feb2021 validation of  new field skintoskincontact
            } else if (aqDel.id(R.id.spndelresult1).getSelectedItemPosition() == 1 && !(aqDel.id(R.id.rdskintoskinyes1).isChecked() ||
                    aqDel.id(R.id.rdskintoskinno1).isChecked())) {
                displayAlert(getResources().getString(R.string.selectskintoskin), null);
                return false;
            } else if (aqDel.id(R.id.spndelresult1).getSelectedItemPosition() == 1 && !(aqDel.id(R.id.rdphyYesch1).isChecked() ||
                    aqDel.id(R.id.rdphyNoch1).isChecked())) {
                displayAlert(getResources().getString(R.string.selectphydisability), null);
                return false;
            } else if (aqDel.id(R.id.rdphyYesch1).isChecked() && aqDel.id(R.id.etotherPhy1).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterdescription), null);
                aqDel.id(R.id.etotherPhy1).getEditText().requestFocus();
                return false;
            }

            if(aqDel.id(R.id.etWeight1).getText().toString().trim().length()>0
                    && Double.parseDouble(aqDel.id(R.id.etWeight1).getText().toString())<100)
            {
                displayAlert(getResources().getString(R.string.weightisabnormal), null);
                aqDel.id(R.id.etWeight1).getEditText().requestFocus();
                return false;
            }


        }

        if (aqDel.id(R.id.spnnoofchild).getSelectedItemPosition() > 1) {

            aqDel.id(R.id.tblchild2).visible();
            aqDel.id(R.id.imgchl2expand).getImageView().setBackground(getResources().getDrawable(R.drawable.expand));
            aqDel.id(R.id.tblchild3).gone();
            aqDel.id(R.id.imgchl3expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
            aqDel.id(R.id.tblchild1).gone();
            aqDel.id(R.id.imgchl1expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));

            aqDel.id(R.id.tblchild4).gone();
            aqDel.id(R.id.imgchl4expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));


            if (aqDel.id(R.id.etdob2).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterdob), null);
                aqDel.id(R.id.etdob2).getEditText().requestFocus();
                return false;
            } else if (aqDel.id(R.id.ettob2).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.entertob), null);
                aqDel.id(R.id.ettob2).getEditText().requestFocus();
                return false;
            } else if (aqDel.id(R.id.spndelresult2).getSelectedItemPosition() == 0) {
                displayAlert(getResources().getString(R.string.selctdelresult), null);
                TextView errorText = (TextView) aqDel.id(R.id.spndelresult2).getSpinner().getSelectedView();
                if (errorText != null) {
                    errorText.setError("");
                    errorText.setTextColor(Color.RED);//just to highlight that this is an error
                    errorText.setText(getResources().getString(R.string.selctdelresult));//changes the selected item text to this
                    errorText.setFocusable(true);
                }
                return false;
            } else if (aqDel.id(R.id.spndelresult2).getSelectedItemPosition() > 1 &&
                    aqDel.id(R.id.etinfantdeathdate2).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterdeathdate), null);
                aqDel.id(R.id.etinfantdeathdate2).getEditText().requestFocus();
                return false;
            } else if (aqDel.id(R.id.spndelresult2).getSelectedItemPosition() == 3 &&
                    !(aqDel.id(R.id.rdfresh2).isChecked() || aqDel.id(R.id.rdmacerated2).isChecked())) {
                displayAlert(getResources().getString(R.string.selctiudtype), null);
                return false;
            } else if (aqDel.id(R.id.etbabyname2).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enternewbornname), null);
                aqDel.id(R.id.etbabyname2).getEditText().requestFocus();
                return false;
            } else if (aqDel.id(R.id.etWeight2).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterbirthweight), null);
                aqDel.id(R.id.etWeight2).getEditText().requestFocus();
                return false;
            } else if (aqDel.id(R.id.spntribalhamletdel2).getSelectedItemPosition() == 0) {
                displayAlert(getResources().getString(R.string.m184), null);

                TextView errorText = (TextView) aqDel.id(R.id.spntribalhamletdel2).getSpinner().getSelectedView();

                if (errorText != null) {
                    errorText.setError("");

                    errorText.setTextColor(Color.RED);//just to highlight that this is an error
                    errorText.setText(getResources().getString(R.string.m184));//changes the selected item text to this
                    errorText.setFocusable(true);
                }
                return false;
            } else if (aqDel.id(R.id.spndeltype2).getSelectedItemPosition() == 0) {
                displayAlert(getResources().getString(R.string.selecttypeofdel), null);
                TextView errorText = (TextView) aqDel.id(R.id.spndeltype2).getSpinner().getSelectedView();
                if (errorText != null) {
                    errorText.setError("");
                    errorText.setTextColor(Color.RED);//just to highlight that this is an error
                    errorText.setText(getResources().getString(R.string.selecttypeofdel));//changes the selected item text to this
                    errorText.setFocusable(true);
                }
                return false;
            } else if (aqDel.id(R.id.spndeltype2).getSelectedItemPosition() == 4
                    && aqDel.id(R.id.etDelTypeOther2).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterothertypeofdel), null);
                aqDel.id(R.id.etDelTypeOther2).getEditText().requestFocus();
                return false;
            } else if (aqDel.id(R.id.spndelplace2).getSelectedItemPosition() == 0) {
                displayAlert(getResources().getString(R.string.selectplaceofdel), null);

                TextView errorText = (TextView) aqDel.id(R.id.spndelplace2).getSpinner().getSelectedView();
                if (errorText != null) {
                    errorText.setError("");
                    errorText.setTextColor(Color.RED);//just to highlight that this is an error
                    errorText.setText(getResources().getString(R.string.selectplaceofdel));//changes the selected item text to this
                    errorText.setFocusable(true);
                }
                return false;
            } else if (aqDel.id(R.id.spndelby2).getSelectedItemPosition() == 0) {
                displayAlert(getResources().getString(R.string.selectdelby), null);

                TextView errorText = (TextView) aqDel.id(R.id.spndelby2).getSpinner().getSelectedView();

                if (errorText != null) {
                    errorText.setError("");
                    errorText.setTextColor(Color.RED);//just to highlight that this is an error
                    errorText.setText(getResources().getString(R.string.selectdelby));//changes the selected item text to this
                    errorText.setFocusable(true);
                }

                return false;
            } else if (aqDel.id(R.id.spndelby2).getSelectedItemPosition() == 9 &&
                    aqDel.id(R.id.etdelbyother2).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterotherdelby), null);
                aqDel.id(R.id.etdelbyother2).getEditText().requestFocus();

                return false;
            } else if (aqDel.id(R.id.etdelconductedbyname2).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterdelcondbyname), null);
                aqDel.id(R.id.etdelconductedbyname2).getEditText().requestFocus();

                return false;
            } else if (!(aqDel.id(R.id.radiofemale2).isChecked() ||
                    aqDel.id(R.id.radiomale2).isChecked() ||
                    aqDel.id(R.id.radioambi2).isChecked())) {
                displayAlert(getResources().getString(R.string.selectgender), null);
                return false;
            } else if (aqDel.id(R.id.spndelresult2).getSelectedItemPosition() == 1 && !(aqDel.id(R.id.rdcryno2).isChecked() ||
                    aqDel.id(R.id.rdcryyes2).isChecked())) {
                displayAlert(getResources().getString(R.string.selectcry), null);
                return false;
            } else if (aqDel.id(R.id.spndelresult2).getSelectedItemPosition() == 1 && !(aqDel.id(R.id.rdbreastno2).isChecked() ||
                    aqDel.id(R.id.rdbreastyes2).isChecked())) {
                displayAlert(getResources().getString(R.string.selectbreastfeed), null);
                return false;
                //Mani 10feb2021 validation of  new field skintoskincontact
            } else if (aqDel.id(R.id.spndelresult2).getSelectedItemPosition() == 1 && !(aqDel.id(R.id.rdskintoskinyes2).isChecked() ||
                    aqDel.id(R.id.rdskintoskinno2).isChecked())) {
                displayAlert(getResources().getString(R.string.selectskintoskin), null);
                return false;
            } else if (aqDel.id(R.id.spndelresult2).getSelectedItemPosition() == 1 && !(aqDel.id(R.id.rdphyYesch2).isChecked() ||
                    aqDel.id(R.id.rdphyNoch2).isChecked())) {
                displayAlert(getResources().getString(R.string.selectphydisability), null);
                return false;
            } else if (aqDel.id(R.id.rdphyYesch2).isChecked() && aqDel.id(R.id.etotherPhy2).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterdescription), null);
                aqDel.id(R.id.etotherPhy2).getEditText().requestFocus();
                return false;
            }

            if(aqDel.id(R.id.etWeight1).getText().toString().trim().length()>0
                    && Double.parseDouble(aqDel.id(R.id.etWeight1).getText().toString())<100)
            {
                displayAlert(getResources().getString(R.string.weightisabnormal), null);
                aqDel.id(R.id.etWeight1).getEditText().requestFocus();
                return false;
            }
            else  if(aqDel.id(R.id.etWeight2).getText().toString().trim().length()>0
                    && Double.parseDouble(aqDel.id(R.id.etWeight2).getText().toString())<100)
            {
                displayAlert(getResources().getString(R.string.weightisabnormal), null);
                aqDel.id(R.id.etWeight2).getEditText().requestFocus();
                return false;
            }


        }


        if (aqDel.id(R.id.spnnoofchild).getSelectedItemPosition() > 2) {


            aqDel.id(R.id.tblchild2).gone();
            aqDel.id(R.id.imgchl2expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
            aqDel.id(R.id.tblchild3).visible();
            aqDel.id(R.id.imgchl3expand).getImageView().setBackground(getResources().getDrawable(R.drawable.expand));
            aqDel.id(R.id.tblchild1).gone();
            aqDel.id(R.id.imgchl1expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));

            aqDel.id(R.id.tblchild4).gone();
            aqDel.id(R.id.imgchl4expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));


            if (aqDel.id(R.id.etdob3).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterdob), null);
                aqDel.id(R.id.etdob3).getEditText().requestFocus();
                return false;
            } else if (aqDel.id(R.id.ettob3).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.entertob), null);
                aqDel.id(R.id.ettob3).getEditText().requestFocus();
                return false;
            } else if (aqDel.id(R.id.spndelresult3).getSelectedItemPosition() == 0) {
                displayAlert(getResources().getString(R.string.selctdelresult), null);

                TextView errorText = (TextView) aqDel.id(R.id.spndelresult3).getSpinner().getSelectedView();
                if (errorText != null) {
                    errorText.setError("");
                    errorText.setTextColor(Color.RED);//just to highlight that this is an error
                    errorText.setText(getResources().getString(R.string.selctdelresult));//changes the selected item text to this
                    errorText.setFocusable(true);
                }


                return false;
            } else if (aqDel.id(R.id.spndelresult3).getSelectedItemPosition() > 1 &&
                    aqDel.id(R.id.etinfantdeathdate3).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterdeathdate), null);
                aqDel.id(R.id.etinfantdeathdate3).getEditText().requestFocus();
                return false;
            } else if (aqDel.id(R.id.spndelresult3).getSelectedItemPosition() == 3 &&
                    !(aqDel.id(R.id.rdfresh3).isChecked() || aqDel.id(R.id.rdmacerated3).isChecked())) {
                displayAlert(getResources().getString(R.string.selctiudtype), null);
                return false;
            } else if (aqDel.id(R.id.etbabyname3).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enternewbornname), null);
                aqDel.id(R.id.etbabyname3).getEditText().requestFocus();
                return false;
            } else if (aqDel.id(R.id.etWeight3).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterbirthweight), null);
                aqDel.id(R.id.etWeight3).getEditText().requestFocus();
                return false;
            } else if (aqDel.id(R.id.spntribalhamletdel3).getSelectedItemPosition() == 0) {
                displayAlert(getResources().getString(R.string.m184), null);

                TextView errorText = (TextView) aqDel.id(R.id.spntribalhamletdel3).getSpinner().getSelectedView();
                if (errorText != null) {
                    errorText.setError("");
                    errorText.setTextColor(Color.RED);//just to highlight that this is an error
                    errorText.setText(getResources().getString(R.string.m184));//changes the selected item text to this
                    errorText.setFocusable(true);
                }

                return false;
            } else if (aqDel.id(R.id.spndeltype3).getSelectedItemPosition() == 0) {
                displayAlert(getResources().getString(R.string.selecttypeofdel), null);

                TextView errorText = (TextView) aqDel.id(R.id.spndeltype3).getSpinner().getSelectedView();

                if (errorText != null) {
                    errorText.setError("");
                    errorText.setTextColor(Color.RED);//just to highlight that this is an error
                    errorText.setText(getResources().getString(R.string.selecttypeofdel));//changes the selected item text to this
                    errorText.setFocusable(true);
                }

                return false;
            } else if (aqDel.id(R.id.spndeltype3).getSelectedItemPosition() == 4
                    && aqDel.id(R.id.etDelTypeOther3).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterothertypeofdel), null);
                aqDel.id(R.id.etDelTypeOther3).getEditText().requestFocus();
                return false;
            } else if (aqDel.id(R.id.spndelplace3).getSelectedItemPosition() == 0) {
                displayAlert(getResources().getString(R.string.selectplaceofdel), null);

                TextView errorText = (TextView) aqDel.id(R.id.spndelplace3).getSpinner().getSelectedView();

                if (errorText != null) {
                    errorText.setError("");
                    errorText.setTextColor(Color.RED);//just to highlight that this is an error
                    errorText.setText(getResources().getString(R.string.selectplaceofdel));//changes the selected item text to this
                    errorText.setFocusable(true);
                }
                return false;
            } else if (aqDel.id(R.id.spndelby3).getSelectedItemPosition() == 0) {
                displayAlert(getResources().getString(R.string.selectdelby), null);

                TextView errorText = (TextView) aqDel.id(R.id.spndelby3).getSpinner().getSelectedView();
                if (errorText != null) {
                    errorText.setError("");
                    errorText.setTextColor(Color.RED);//just to highlight that this is an error
                    errorText.setText(getResources().getString(R.string.selectdelby));//changes the selected item text to this
                    errorText.setFocusable(true);
                }

                return false;
            } else if (aqDel.id(R.id.spndelby3).getSelectedItemPosition() == 9 &&
                    aqDel.id(R.id.etdelbyother3).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterotherdelby), null);
                aqDel.id(R.id.etdelbyother3).getEditText().requestFocus();

                return false;
            } else if (aqDel.id(R.id.etdelconductedbyname3).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterdelcondbyname), null);
                aqDel.id(R.id.etdelconductedbyname3).getEditText().requestFocus();

                return false;
            } else if (!(aqDel.id(R.id.radiofemale3).isChecked() ||
                    aqDel.id(R.id.radiomale3).isChecked() ||
                    aqDel.id(R.id.radioambi3).isChecked())) {
                displayAlert(getResources().getString(R.string.selectgender), null);
                return false;
            } else if (aqDel.id(R.id.spndelresult3).getSelectedItemPosition() == 1 && !(aqDel.id(R.id.rdcryno3).isChecked() ||
                    aqDel.id(R.id.rdcryyes3).isChecked())) {
                displayAlert(getResources().getString(R.string.selectcry), null);
                return false;
            } else if (aqDel.id(R.id.spndelresult3).getSelectedItemPosition() == 1 && !(aqDel.id(R.id.rdbreastno3).isChecked() ||
                    aqDel.id(R.id.rdbreastyes3).isChecked())) {
                displayAlert(getResources().getString(R.string.selectbreastfeed), null);
                return false;
                //Mani 10feb2021 validation of  new field skintoskincontact
            } else if (aqDel.id(R.id.spndelresult3).getSelectedItemPosition() == 1 && !(aqDel.id(R.id.rdskintoskinyes3).isChecked() ||
                    aqDel.id(R.id.rdskintoskinno3).isChecked())) {
                displayAlert(getResources().getString(R.string.selectskintoskin), null);
                return false;
            } else if (aqDel.id(R.id.spndelresult3).getSelectedItemPosition() == 1 && !(aqDel.id(R.id.rdphyYesch3).isChecked() ||
                    aqDel.id(R.id.rdphyNoch3).isChecked())) {
                displayAlert(getResources().getString(R.string.selectphydisability), null);
                return false;
            } else if (aqDel.id(R.id.rdphyYesch3).isChecked() && aqDel.id(R.id.etotherPhy3).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterdescription), null);
                aqDel.id(R.id.etotherPhy3).getEditText().requestFocus();
                return false;
            }

            if(aqDel.id(R.id.etWeight1).getText().toString().trim().length()>0
                    && Double.parseDouble(aqDel.id(R.id.etWeight1).getText().toString())<100)
            {
                displayAlert(getResources().getString(R.string.weightisabnormal), null);
                aqDel.id(R.id.etWeight1).getEditText().requestFocus();
                return false;
            }
            else  if(aqDel.id(R.id.etWeight2).getText().toString().trim().length()>0
                    && Double.parseDouble(aqDel.id(R.id.etWeight2).getText().toString())<100)
            {
                displayAlert(getResources().getString(R.string.weightisabnormal), null);
                aqDel.id(R.id.etWeight2).getEditText().requestFocus();
                return false;
            }
            else  if(aqDel.id(R.id.etWeight3).getText().toString().trim().length()>0
                    && Double.parseDouble(aqDel.id(R.id.etWeight3).getText().toString())<100)
            {
                displayAlert(getResources().getString(R.string.weightisabnormal), null);
                aqDel.id(R.id.etWeight3).getEditText().requestFocus();
                return false;
            }

        }


        if (aqDel.id(R.id.spnnoofchild).getSelectedItemPosition() > 3) {


            aqDel.id(R.id.tblchild2).gone();
            aqDel.id(R.id.imgchl2expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
            aqDel.id(R.id.tblchild3).gone();
            aqDel.id(R.id.imgchl3expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
            aqDel.id(R.id.tblchild1).gone();
            aqDel.id(R.id.imgchl1expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));

            aqDel.id(R.id.tblchild4).visible();
            aqDel.id(R.id.imgchl4expand).getImageView().setBackground(getResources().getDrawable(R.drawable.expand));


            if (aqDel.id(R.id.etdob4).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterdob), null);
                aqDel.id(R.id.etdob4).getEditText().requestFocus();
                return false;
            } else if (aqDel.id(R.id.ettob4).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.entertob), null);
                aqDel.id(R.id.ettob4).getEditText().requestFocus();
                return false;
            } else if (aqDel.id(R.id.spndelresult4).getSelectedItemPosition() == 0) {
                displayAlert(getResources().getString(R.string.selctdelresult), null);

                TextView errorText = (TextView) aqDel.id(R.id.spndelresult4).getSpinner().getSelectedView();
                if (errorText != null) {
                    errorText.setError("");
                    errorText.setTextColor(Color.RED);//just to highlight that this is an error
                    errorText.setText(getResources().getString(R.string.selctdelresult));//changes the selected item text to this
                    errorText.setFocusable(true);
                }


                return false;
            } else if (aqDel.id(R.id.spndelresult4).getSelectedItemPosition() > 1 &&
                    aqDel.id(R.id.etinfantdeathdate4).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterdeathdate), null);
                aqDel.id(R.id.etinfantdeathdate4).getEditText().requestFocus();
                return false;
            } else if (aqDel.id(R.id.spndelresult4).getSelectedItemPosition() == 3 &&
                    !(aqDel.id(R.id.rdfresh4).isChecked() || aqDel.id(R.id.rdmacerated4).isChecked())) {
                displayAlert(getResources().getString(R.string.selctiudtype), null);
                return false;
            } else if (aqDel.id(R.id.etbabyname4).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enternewbornname), null);
                aqDel.id(R.id.etbabyname4).getEditText().requestFocus();
                return false;
            } else if (aqDel.id(R.id.etWeight4).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterbirthweight), null);
                aqDel.id(R.id.etWeight4).getEditText().requestFocus();
                return false;
            } else if (aqDel.id(R.id.spntribalhamletdel4).getSelectedItemPosition() == 0) {
                displayAlert(getResources().getString(R.string.m184), null);

                TextView errorText = (TextView) aqDel.id(R.id.spntribalhamletdel4).getSpinner().getSelectedView();
                if (errorText != null) {
                    errorText.setError("");
                    errorText.setTextColor(Color.RED);//just to highlight that this is an error
                    errorText.setText(getResources().getString(R.string.m184));//changes the selected item text to this
                    errorText.setFocusable(true);
                }


                return false;
            } else if (aqDel.id(R.id.spndeltype4).getSelectedItemPosition() == 0) {
                displayAlert(getResources().getString(R.string.selecttypeofdel), null);

                TextView errorText = (TextView) aqDel.id(R.id.spndeltype4).getSpinner().getSelectedView();
                if (errorText != null) {
                    errorText.setError("");
                    errorText.setTextColor(Color.RED);//just to highlight that this is an error
                    errorText.setText(getResources().getString(R.string.selecttypeofdel));//changes the selected item text to this
                    errorText.setFocusable(true);
                }


                return false;
            } else if (aqDel.id(R.id.spndeltype4).getSelectedItemPosition() == 4
                    && aqDel.id(R.id.etDelTypeOther4).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterothertypeofdel), null);
                aqDel.id(R.id.etDelTypeOther4).getEditText().requestFocus();
                return false;
            } else if (aqDel.id(R.id.spndelplace4).getSelectedItemPosition() == 0) {
                displayAlert(getResources().getString(R.string.selectplaceofdel), null);

                TextView errorText = (TextView) aqDel.id(R.id.spndelplace4).getSpinner().getSelectedView();
                if (errorText != null) {
                    errorText.setError("");
                    errorText.setTextColor(Color.RED);//just to highlight that this is an error
                    errorText.setText(getResources().getString(R.string.selectplaceofdel));//changes the selected item text to this
                    errorText.setFocusable(true);
                }

                return false;
            } else if (aqDel.id(R.id.spndelby4).getSelectedItemPosition() == 0) {
                displayAlert(getResources().getString(R.string.selectdelby), null);

                TextView errorText = (TextView) aqDel.id(R.id.spndelby4).getSpinner().getSelectedView();
                if (errorText != null) {
                    errorText.setError("");
                    errorText.setTextColor(Color.RED);//just to highlight that this is an error
                    errorText.setText(getResources().getString(R.string.selectdelby));//changes the selected item text to this
                    errorText.setFocusable(true);
                }

                return false;
            } else if (aqDel.id(R.id.spndelby4).getSelectedItemPosition() == 9 &&
                    aqDel.id(R.id.etdelbyother4).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterotherdelby), null);
                aqDel.id(R.id.etdelbyother4).getEditText().requestFocus();

                return false;
            } else if (aqDel.id(R.id.etdelconductedbyname4).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterdelcondbyname), null);
                aqDel.id(R.id.etdelconductedbyname4).getEditText().requestFocus();

                return false;
            } else if (!(aqDel.id(R.id.radiofemale4).isChecked() ||
                    aqDel.id(R.id.radiomale4).isChecked() ||
                    aqDel.id(R.id.radioambi4).isChecked())) {
                displayAlert(getResources().getString(R.string.selectgender), null);
                return false;
            } else if (aqDel.id(R.id.spndelresult4).getSelectedItemPosition() == 1 && !(aqDel.id(R.id.rdcryno4).isChecked() ||
                    aqDel.id(R.id.rdcryyes4).isChecked())) {
                displayAlert(getResources().getString(R.string.selectcry), null);
                return false;
            } else if (aqDel.id(R.id.spndelresult4).getSelectedItemPosition() == 1 && !(aqDel.id(R.id.rdbreastno4).isChecked() ||
                    aqDel.id(R.id.rdbreastyes4).isChecked())) {
                displayAlert(getResources().getString(R.string.selectbreastfeed), null);
                return false;
                //Mani 10feb2021 validation of  new field skintoskincontact
            } else if (aqDel.id(R.id.spndelresult4).getSelectedItemPosition() == 1 && !(aqDel.id(R.id.rdskintoskinyes4).isChecked() ||
                    aqDel.id(R.id.rdskintoskinno4).isChecked())) {
                displayAlert(getResources().getString(R.string.selectskintoskin), null);
                return false;
            } else if (aqDel.id(R.id.spndelresult4).getSelectedItemPosition() == 1 && !(aqDel.id(R.id.rdphyYesch4).isChecked() ||
                    aqDel.id(R.id.rdphyNoch4).isChecked())) {
                displayAlert(getResources().getString(R.string.selectphydisability), null);
                return false;
            } else if (aqDel.id(R.id.rdphyYesch4).isChecked() && aqDel.id(R.id.etotherPhy4).getText().toString().trim().length() <= 0) {
                displayAlert(getResources().getString(R.string.enterdescription), null);
                aqDel.id(R.id.etotherPhy4).getEditText().requestFocus();
                return false;
            }

              if(aqDel.id(R.id.etWeight1).getText().toString().trim().length()>0
                    && Double.parseDouble(aqDel.id(R.id.etWeight1).getText().toString())<100)
            {
                displayAlert(getResources().getString(R.string.weightisabnormal), null);
                aqDel.id(R.id.etWeight1).getEditText().requestFocus();
                return false;
            }
            else  if(aqDel.id(R.id.etWeight2).getText().toString().trim().length()>0
                    && Double.parseDouble(aqDel.id(R.id.etWeight2).getText().toString())<100)
            {
                displayAlert(getResources().getString(R.string.weightisabnormal), null);
                aqDel.id(R.id.etWeight2).getEditText().requestFocus();
                return false;
            }
            else  if(aqDel.id(R.id.etWeight3).getText().toString().trim().length()>0
                    && Double.parseDouble(aqDel.id(R.id.etWeight3).getText().toString())<100)
            {
                displayAlert(getResources().getString(R.string.weightisabnormal), null);
                aqDel.id(R.id.etWeight3).getEditText().requestFocus();
                return false;
            }
            else  if(aqDel.id(R.id.etWeight4).getText().toString().trim().length()>0
            && Double.parseDouble(aqDel.id(R.id.etWeight4).getText().toString())<100)
            {

                displayAlert(getResources().getString(R.string.weightisabnormal), null);
                aqDel.id(R.id.etWeight4).getEditText().requestFocus();
                return false;
            }
        }
        return true;
    }


    /**
     * This method opens the Time Dialog and sets Time to respective EditText
     * @return
     */
    private void assignEditTextAndCallTimePicker(EditText editText) {
        currentEditTextForDate = editText;
        Calendar c = Calendar.getInstance();
        hour = c.get(Calendar.HOUR_OF_DAY);
        minute = c.get(Calendar.MINUTE);
        new TimePickerDialog(this, AlertDialog.THEME_HOLO_LIGHT, this, hour, minute,
				DateFormat.is24HourFormat(this)).show();
       // CommonTimePickerFragment.getCommonTime(DeliveryInfoActivity.this, hour, minute);
    }

    @Override
    public void onTimeSet(TimePicker view, int selectedHour, int selectedMinute) {
        hour = selectedHour;
        minute = selectedMinute;
        try {

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
            String delDateTime = aqDel.id(R.id.etdob1).getText().toString() + " " + aqDel.id(R.id.ettob1).getText().toString();
            Date del = null;
            if (delDateTime != null && delDateTime.trim().length() > 0)
                del = sdf.parse(delDateTime);

            final Calendar c = Calendar.getInstance();
            hour = c.get(Calendar.HOUR_OF_DAY);
            minute = c.get(Calendar.MINUTE);


            if (currentEditTextForDate == aqDel.id(R.id.ettob1).getEditText()) {

                String strSelDateTime1 = aqDel.id(R.id.etdob1).getText().toString() + " " + ("" + selectedHour) + ":"
                        + ("" + selectedMinute);

                Date selDateTime1 = null;
                if (strSelDateTime1 != null && strSelDateTime1.trim().length() > 0)
                    selDateTime1 = sdf.parse(strSelDateTime1);
                if (selDateTime1.after(new Date())) {
                    displayAlert(getResources().getString(R.string.time_cannot_be_after_curre_time), null);
                    aqDel.id(R.id.ettob1).text(DateTimeUtil.getTimeIn12HourFormat(DateTimeUtil.getCurrentTime()));
                } else {
                   /* aqDel.id(R.id.ettob1).getEditText().setText(new StringBuilder().append(padding_str(selectedHour))
                            .append(":").append(padding_str(selectedMinute)));*/
                    String time = DateTimeUtil.getTimeIn12HourFormat(selectedHour + ":" + selectedMinute);
                    aqDel.id(R.id.ettob1).getEditText().setText(time);
                }
            } else if (currentEditTextForDate == aqDel.id(R.id.ettob2).getEditText()) {
                String strSelDateTime2 = aqDel.id(R.id.etdob2).getText().toString() + " " + ("" + selectedHour) + ":"
                        + ("" + selectedMinute);

                Date selDateTime2 = null;
                if (strSelDateTime2 != null && strSelDateTime2.trim().length() > 0)
                    selDateTime2 = sdf.parse(strSelDateTime2);

                Date now = new Date();
                if (selDateTime2.after(now)) {
                    displayAlert(getResources().getString(R.string.time_cannot_be_after_curre_time), null);
                    aqDel.id(R.id.ettob2).text(DateTimeUtil.getTimeIn12HourFormat(DateTimeUtil.getCurrentTime()));
                } else if (delDateTime != null && selDateTime2.before(del)) {
                    displayAlert(getResources().getString(R.string.date_cannot_be_before_del_date), null);
                    aqDel.id(R.id.ettob2).text(DateTimeUtil.getTimeIn12HourFormat(DateTimeUtil.getCurrentTime()));
                } else {
                    /*aqDel.id(R.id.ettob2).getEditText().setText(new StringBuilder().append(padding_str(selectedHour))
                            .append(":").append(padding_str(selectedMinute)));*/
                    String time = DateTimeUtil.getTimeIn12HourFormat(selectedHour + ":" + selectedMinute);
                    aqDel.id(R.id.ettob2).getEditText().setText(time);
                }
            } else if (currentEditTextForDate == aqDel.id(R.id.ettob3).getEditText()) {

                String strSelDateTime3 = aqDel.id(R.id.etdob3).getText().toString() + " " + ("" + selectedHour) + ":"
                        + ("" + selectedMinute);

                Date selDateTime3 = null;
                if (strSelDateTime3 != null && strSelDateTime3.trim().length() > 0)
                    selDateTime3 = sdf.parse(strSelDateTime3);

                if (selDateTime3.after(new Date())) {
                    displayAlert(getResources().getString(R.string.time_cannot_be_after_curre_time), null);
                    aqDel.id(R.id.ettob3).text(DateTimeUtil.getTimeIn12HourFormat(DateTimeUtil.getCurrentTime()));
                } else if (delDateTime != null && selDateTime3.before(del)) {
                    displayAlert(getResources().getString(R.string.date_cannot_be_before_del_date), null);
                    aqDel.id(R.id.ettob3).text(DateTimeUtil.getTimeIn12HourFormat(DateTimeUtil.getCurrentTime()));
                } else {
                    /*aqDel.id(R.id.ettob3).getEditText().setText(new StringBuilder().append(padding_str(selectedHour))
                            .append(":").append(padding_str(selectedMinute)));*/
                    String time = DateTimeUtil.getTimeIn12HourFormat(selectedHour + ":" + selectedMinute);
                    aqDel.id(R.id.ettob3).getEditText().setText(time);
                }
            } else if (currentEditTextForDate == aqDel.id(R.id.ettob4).getEditText()) {

                String strSelDateTime4 = aqDel.id(R.id.etdob4).getText().toString() + " " + ("" + selectedHour) + ":"
                        + ("" + selectedMinute);

                Date selDateTime4 = null;
                if (strSelDateTime4 != null && strSelDateTime4.trim().length() > 0)
                    selDateTime4 = sdf.parse(strSelDateTime4);

                if (selDateTime4.after(new Date())) {
                    displayAlert(getResources().getString(R.string.time_cannot_be_after_curre_time), null);
                    aqDel.id(R.id.ettob4).text(DateTimeUtil.getTimeIn12HourFormat(DateTimeUtil.getCurrentTime()));
                } else if (delDateTime != null && selDateTime4.before(del)) {
                    displayAlert(getResources().getString(R.string.date_cannot_be_before_del_date), null);
                    aqDel.id(R.id.ettob4).text(DateTimeUtil.getTimeIn12HourFormat(DateTimeUtil.getCurrentTime()));
                } else {
                    /*aqDel.id(R.id.ettob4).getEditText().setText(new StringBuilder().append(padding_str(selectedHour))
                            .append(":").append(padding_str(selectedMinute)));*/
                    String time = DateTimeUtil.getTimeIn12HourFormat(selectedHour + ":" + selectedMinute);
                    aqDel.id(R.id.ettob4).getEditText().setText(time);

                }
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private static String padding_str(int c) {
        if (c >= 10)
            return String.valueOf(c);
        else
            return "0" + c;
    }


    //  text change listner
    TextWatcher watcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            try {

                if (s == aqDel.id(R.id.etWeight1).getEditable()) {

                    if (aqDel.id(R.id.etWeight1).getText().toString().trim().length() > 0) {
                        if (Double.parseDouble(aqDel.id(R.id.etWeight1).getText().toString()) < 2500) {
                            aqDel.id(R.id.chklessweight1).checked(true);
                        } else
                            aqDel.id(R.id.chklessweight1).checked(false);
                    } else
                        aqDel.id(R.id.chklessweight1).checked(false);

                } else if (s == aqDel.id(R.id.etWeight2).getEditable()) {

                    if (aqDel.id(R.id.etWeight2).getText().toString().trim().length() > 0) {
                        if (Double.parseDouble(aqDel.id(R.id.etWeight2).getText().toString()) < 2500) {
                            aqDel.id(R.id.chklessweight2).checked(true);
                        } else
                            aqDel.id(R.id.chklessweight2).checked(false);
                    } else
                        aqDel.id(R.id.chklessweight2).checked(false);

                } else if (s == aqDel.id(R.id.etWeight3).getEditable()) {

                    if (aqDel.id(R.id.etWeight3).getText().toString().trim().length() > 0) {
                        if (Double.parseDouble(aqDel.id(R.id.etWeight3).getText().toString()) < 2500) {
                            aqDel.id(R.id.chklessweight3).checked(true);
                        } else
                            aqDel.id(R.id.chklessweight3).checked(false);
                    } else
                        aqDel.id(R.id.chklessweight3).checked(false);

                } else if (s == aqDel.id(R.id.etWeight4).getEditable()) {

                    if (aqDel.id(R.id.etWeight4).getText().toString().trim().length() > 0) {
                        if (Double.parseDouble(aqDel.id(R.id.etWeight4).getText().toString()) < 2500) {
                            aqDel.id(R.id.chklessweight4).checked(true);
                        } else
                            aqDel.id(R.id.chklessweight4).checked(false);
                    } else
                        aqDel.id(R.id.chklessweight4).checked(false);

                }


            } catch (Exception e) {


                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            }

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {

        }

    };

    /**
     * Setting AuditTrail pojo, calls - InserttblAuditTrail()
     *
     * @param transId
     */
    private boolean InserttblAuditTrail(String ColumnName, String Old_Value, String
            New_Value, int transId, String strTableName) throws Exception {

        APJ = new AuditPojo();
        APJ.setUserId(user.getUserId());
        if(strTableName.equalsIgnoreCase("tblchildinfo"))//09Aug2021 Arpitha
            APJ.setWomanId(tblChildInfo.getChildID());
        else
        APJ.setWomanId(tblDelInfo.getWomanId());
        APJ.setTblName(strTableName);
        APJ.setDateChanged(DateTimeUtil.getTodaysDate());
        APJ.setColumnName(ColumnName);
        APJ.setOld_Value(Old_Value);
        APJ.setNew_Value(New_Value);
        APJ.setTransId(transId);
        APJ.setRecordcreateddate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());

        AuditRepository auditRepository = new AuditRepository(databaseHelper);
        return auditRepository.insertTotblAuditTrailNew(APJ, databaseHelper);

    }


    //  update details to table
    void updateData() {

        try {

            final TransactionHeaderRepository transRepo = new TransactionHeaderRepository(databaseHelper);
            final int transId = transRepo.iCreateNewTrans(appState.sessionUserId, databaseHelper);

            TransactionManager.callInTransaction(databaseHelper.getConnectionSource(), new Callable<Void>() {
                public Void call() throws Exception {//29Aug2019 Arpitha - transaction manager

                    String upSqlDel = updateDelData(transId);
                    String upSqlChild = "";
                    int sqlChild = 0;

                    if (upSqlDel != null && upSqlChild != null) {   //10Aug2019 - Bindu - check for update st and then cal update, else exit to reg women
                        transRepo.iNewRecordTransNew(appState.sessionUserId, transId, "tblaudittrail", databaseHelper);


                        DeliveryRepository womanRepo = new DeliveryRepository(databaseHelper);
                        ChildRepository childRepo = new ChildRepository(databaseHelper);
                        int sql = womanRepo.updateDelData(woman.getUserId(), upSqlDel, transId, databaseHelper);
                        for (int i = 1; i <= aqDel.id(R.id.spnnoofchild).getSelectedItemPosition(); i++) {
                            upSqlChild = updateChildData(transId, i);
                            sqlChild = childRepo.updateChildData(woman.getUserId(), upSqlChild, transId, databaseHelper);
                        }

                        if (sql > 0 && sqlChild > 0) {

                            String sql1 = "Update tblregisteredwomen SET regPregnantorMother = 2, " +
                                    "regADDate = '" + aqDel.id(R.id.etdob1).getText().toString()
                                    + "' , regInDanger = " + compl + " where WomanId = '" + woman.getWomanId() + "'";
                            WomanRepository womanRepository = new WomanRepository(databaseHelper);
                            int updateReg = womanRepository.updateWomenRegDataNew
                                    (woman.getUserId(), sql1, transId, null, databaseHelper);

                            if (updateReg > 0) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.deldetailsupdatedsuccessfully), Toast.LENGTH_LONG).show();

                                // MainMenuActivity.callSyncMtd(); //12Aug2019 - Bindu - Comment autosync
                                Intent exit = new Intent(DeliveryInfoActivity.this, RegisteredWomenActionTabs.class);
                                exit.putExtra("appState", appState);
                                exit.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                exit.putExtra("wFilterStr", "");
                                startActivity(exit);
                            } else {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.deliveryinfoupdatefailed), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha

                            }
                        } else {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.deliveryinfoupdatefailed), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha

                        }
                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.nochangesmadetoedit), Toast.LENGTH_LONG).show();
                        Intent exit = new Intent(DeliveryInfoActivity.this, RegisteredWomenActionTabs.class);
                        exit.putExtra("appState", appState);
                        exit.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        exit.putExtra("wFilterStr", "");
                        startActivity(exit);

                    }
                    return null;
                }
            });


        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.deliveryinfoupdatefailed), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private void initiateDrawer() throws Exception {
        mDrawerLayout = findViewById(R.id.ett_drawer_layout);
        mDrawerList = findViewById(R.id.ett_left_drawer);


        ArrayList<NavDrawerItem> navDrawerItems = new ArrayList<>();
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.sp_WRegEdit), R.drawable.registration));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.services), R.drawable.anm_pending_activities));//01Oct2019 Arpitha

        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.addunplannedservices), R.drawable.anm_pending_activities));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.unplannedserviceslist), R.drawable.anm_pending_activities));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.homevisitadd),
                R.drawable.prenatalhomevisit));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.womanhomevisithistory),
                R.drawable.prenatalhomevisit));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deactivate),
                R.drawable.deactivate));//28Nov2019 Arpitha


        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        mDrawerList.setAdapter(new NavDrawerListAdapter(getApplicationContext(), navDrawerItems));

//        if (woman.getRegpregormotheratreg() != 2) {//27Nov2019 Arpitha
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer,
                R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerClosed(View view) {
                /*invalidateOptionsMenu(); // creates call to
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                getSupportActionBar().setDisplayShowTitleEnabled(false);
                getSupportActionBar().setDisplayShowCustomEnabled(false);
*/

                invalidateOptionsMenu(); // creates call to
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                getSupportActionBar().setDisplayShowTitleEnabled(false);
                getSupportActionBar().setDisplayShowCustomEnabled(false);

                int noOfWeeks;
                try {
                    if (woman.getRegADDate() != null && woman.getRegADDate().length() > 0) {
                        getSupportActionBar().setTitle((woman.getRegWomanName() + "\t"));
                    } else {
                        noOfWeeks = DateTimeUtil.getNumberOfWeeksFromLMP(woman.getRegLMP());
                        getSupportActionBar().setTitle((woman.getRegWomanName() + " (" + noOfWeeks + " "
                                + getResources().getString(R.string.weeks) + ")"));
                    }
                } catch (ParseException e) {


                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                try {
                   /* invalidateOptionsMenu(); // creates call to
                    getSupportActionBar().setDisplayShowHomeEnabled(false);
                    getSupportActionBar().setDisplayShowTitleEnabled(false);
                    getSupportActionBar().setDisplayShowCustomEnabled(true);
                    getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);
*/
                    invalidateOptionsMenu();
                    getSupportActionBar().setDisplayShowHomeEnabled(false);
                    getSupportActionBar().setDisplayShowTitleEnabled(false);
                    getSupportActionBar().setDisplayShowCustomEnabled(true);
                    getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);

                    aqDel.id(R.id.ivWomanImg).getImageView().setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            mDrawerLayout.closeDrawer(mDrawerList);
                        }
                    });

                    aqDel.id(R.id.tvWomanName).text((woman.getRegWomanName() + "\t"));
                    if (woman.getRegADDate() != null && woman.getRegADDate().length() > 0) {
                        aqDel.id(R.id.tvInfo1).text((getResources().getString(R.string.tvadd) + " " + woman.getRegADDate()));
                    } else {
                        int noOfWeeks = DateTimeUtil.getNumberOfWeeksFromLMP(woman.getRegLMP());
//                        aqDel.id(R.id.tvInfo1).text((noOfWeeks + getResources().getString(R.string.weeks)));
                        aqDel.id(R.id.tvInfo1).text((woman.getRegEDD() + getResources().getString(R.string.weeks)));
                    }
                } catch (Exception e) {


                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }
            }
        };
        // }
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    /**
     * This method is to sync drawerToggle and to replace up caret icon to menu
     * icon in Action Bar
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        mspnMotherCompl = true;
    }


    /**
     * The click listener for ListView in the navigation drawer
     */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            mDrawerLayout.closeDrawer(mDrawerList);
            try {

                switch (position) {
                    case 0:
                        Intent viewProfile = new Intent(DeliveryInfoActivity.this, ViewProfileActivity.class);
                        viewProfile.putExtra("woman", woman);
                        displayAlert(getResources().getString(R.string.exit), viewProfile);

                        break;
                    case 1:
                        Intent services = new Intent(DeliveryInfoActivity.this, ServicesSummaryActivity.class);
                        services.putExtra("woman", woman);
                        displayAlert(getResources().getString(R.string.exit), services);

                        break;

                    case 2:
                        Intent unplanned = new Intent(DeliveryInfoActivity.this, UnplannedServicesActivity.class);
                        unplanned.putExtra("woman", woman);
                        displayAlert(getResources().getString(R.string.exit), unplanned);

                        break;
                    case 3:
                        Intent unPlannedList = new Intent(DeliveryInfoActivity.this, UnplannedServicesListActivity.class);
                        unPlannedList.putExtra("woman", woman);
                        displayAlert(getResources().getString(R.string.exit), unPlannedList);

                        break;
                    //29Aug2019 - Bindu - Add Home visit
                    case 4:
                        /*int daysDiff = DateTimeUtil.getDaysBetweenDates(DateTimeUtil.getTodaysDate(), woman.getRegLMP());
                        if (daysDiff > 210) {
                            displayNearingDelAlertMessage(woman);
                        } else {
                            Intent homeVisit = new Intent(DeliveryInfoActivity.this, HomeVisit.class);
                            displayAlert(getResources().getString(R.string.exit),homeVisit);

                        }*/
                        if (woman != null && woman.getRegPregnantorMother() == 2) { //02Dec2019 - Bindu - add condition - PNC Home visit only
                            Intent homeVisit = new Intent(DeliveryInfoActivity.this, PncHomeVisit.class);
                            displayAlert(getResources().getString(R.string.exit), homeVisit);
                        }
                        break;
                    case 5:
//                        if (wVisitDetails != null && wVisitDetails.get(0).getLastAncVisitDate().length() > 0 || wVisitDetails.get(0).getLastPncVisitDate().length() > 0) {
                            Intent homeVisitHistory = new Intent(DeliveryInfoActivity.this, HomeVisitListActivity.class);
                            displayAlert(getResources().getString(R.string.exit), homeVisitHistory);

                       /* } else {
                            Toast.makeText(DeliveryInfoActivity.this, getResources().getString(R.string.norecords), Toast.LENGTH_SHORT).show();
                        }*/
                        break;
//                  28Nov2019 Arpitha
                    case 6:
                        Intent deact = new Intent(DeliveryInfoActivity.this, WomanDeactivateActivity.class);
                        deact.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        deact.putExtra("woman", woman);
                        startActivity(deact);
                        break;

                }
            } catch (Exception e) {


                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            }
        }
    }


    public void displayNearingDelAlertMessage(tblregisteredwomen regwoman) throws Exception {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        String alertmsg = regwoman.getRegWomanName() + ", " + " EDD : " + regwoman.getRegEDD();
        String msg = "<font color='#CC3333'> Nearing Delivery - </font>" + alertmsg;
        alertDialogBuilder.setMessage(getResources().getText(R.string.alerthomevisitnearingdelmsg)).setCancelable(false).setTitle(Html.fromHtml(msg)).setNegativeButton(
                getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent nextScreen = new Intent(DeliveryInfoActivity.this, HomeVisit.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        nextScreen.putExtra("woman", woman);
                        startActivity(nextScreen);
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    void setTribalHamlet() throws SQLException {
        FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
        mapVillage = facilityRepository.getPlaceMap();
        ArrayList<String> villageList = new ArrayList<String>();
        villageList.add(getResources().getString(R.string.selectType));
        for (Map.Entry<String, Integer> village : mapVillage.entrySet())
            villageList.add(village.getKey());
        ArrayAdapter<String> VillageAdapter = new ArrayAdapter<>(this,
                R.layout.simple_spinner_dropdown_item, villageList);
        VillageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        for (int i = 0; i <= aqDel.id(R.id.spnnoofchild).getSelectedItemPosition(); i++) {
            aqDel.id(spntribalhamletdel + i).adapter(VillageAdapter);
        }
    }

    // return the position of given key in a map
    public static int getKey(Map<String, Integer> mapref, String value) {

        int pos = 0;
        for (Map.Entry<String, Integer> map : mapref.entrySet()) {
            pos++;
            if (map.getValue().toString().equals(value)) {
                return pos;
            }

        }
        return pos;
    }


    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.llchl1expand:
                setFirstChildUI();
                break;
            case R.id.llchl2expand:
                secondChildUI();
                break;
            case R.id.llchl3expand:
                setThirdChildUI();
                break;
            case R.id.llchl4expand:
                setFourthChildUI();
                break;
            case R.id.btnsave:
                if (validateFields()) {
                    Intent intent = new Intent(DeliveryInfoActivity.this, RegisteredWomenActionTabs.class);
                    displayAlert(getResources().getString(R.string.m099), intent);
                }
                break;
            case R.id.rddeathyes:
                aqDel.id(R.id.trcasueofdeath).visible();
                aqDel.id(R.id.spncauseofdeath).setSelection(0);
                break;
            case R.id.rddeathno:
                aqDel.id(R.id.trcasueofdeath).gone();
                aqDel.id(R.id.spncauseofdeath).setSelection(0);
                aqDel.id(R.id.trothercasue).gone();
                aqDel.id(R.id.etothercauseofdeath).text("");
                break;
            case R.id.btncancel:
                Intent exit = new Intent(DeliveryInfoActivity.this, MainMenuActivity.class);
                displayAlert(getResources().getString(R.string.exit), exit);
               /* exit.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                startActivity(exit);*/
                break;
            case R.id.rdphyYesch1:
                aqDel.id(R.id.trOtherPhy1).visible();
                break;
            case R.id.rdphyNoch1:
                aqDel.id(R.id.trOtherPhy1).gone();
                aqDel.id(R.id.etotherPhy1).text(""); //01May2021 Bindu reset phy others
                break;
            case R.id.rdphyYesch2:
                aqDel.id(R.id.trOtherPhy2).visible();
                break;
            case R.id.rdphyNoch2:
                aqDel.id(R.id.trOtherPhy2).gone();
                aqDel.id(R.id.etotherPhy2).text(""); //01May2021 Bindu reset phy others
                break;
            case R.id.rdphyYesch3:
                aqDel.id(R.id.trOtherPhy3).visible();
                break;
            case R.id.rdphyNoch3:
                aqDel.id(R.id.trOtherPhy3).gone();
                aqDel.id(R.id.etotherPhy3).text(""); //01May2021 Bindu reset phy others
                break;

            case R.id.rdphyYesch4:
                aqDel.id(R.id.trOtherPhy4).visible();
                break;
            case R.id.rdphyNoch4:
                aqDel.id(R.id.trOtherPhy4).gone();
                aqDel.id(R.id.etotherPhy4).text(""); //01May2021 Bindu reset phy others
                break;

            default:
                break;


        }

    }

    void setFirstChildUI() {
        if (aqDel.id(R.id.tblchild1).getView().getVisibility() == View.VISIBLE) {
            aqDel.id(R.id.tblchild1).gone();
            aqDel.id(R.id.imgchl1expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
        } else {
            aqDel.id(R.id.tblchild1).visible();
            aqDel.id(R.id.imgchl1expand).getImageView().setBackground(getResources().getDrawable(R.drawable.expand));

            aqDel.id(R.id.tblchild2).gone();
            aqDel.id(R.id.imgchl2expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
            aqDel.id(R.id.tblchild3).gone();
            aqDel.id(R.id.imgchl3expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
            aqDel.id(R.id.tblchild4).gone();
            aqDel.id(R.id.imgchl4expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));


        }
    }

    void secondChildUI() {
        if (aqDel.id(R.id.tblchild2).getView().getVisibility() == View.VISIBLE) {
            aqDel.id(R.id.tblchild2).gone();
            aqDel.id(R.id.imgchl2expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
        } else {
            aqDel.id(R.id.tblchild2).visible();
            aqDel.id(R.id.imgchl2expand).getImageView().setBackground(getResources().getDrawable(R.drawable.expand));

            aqDel.id(R.id.tblchild1).gone();
            aqDel.id(R.id.imgchl1expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
            aqDel.id(R.id.tblchild3).gone();
            aqDel.id(R.id.imgchl3expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
            aqDel.id(R.id.tblchild4).gone();
            aqDel.id(R.id.imgchl4expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));


        }
    }

    void setThirdChildUI() {
        if (aqDel.id(R.id.tblchild3).getView().getVisibility() == View.VISIBLE) {
            aqDel.id(R.id.tblchild3).gone();
            aqDel.id(R.id.imgchl3expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
        } else {
            aqDel.id(R.id.tblchild3).visible();
            aqDel.id(R.id.imgchl3expand).getImageView().setBackground(getResources().getDrawable(R.drawable.expand));

            aqDel.id(R.id.tblchild2).gone();
            aqDel.id(R.id.imgchl2expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
            aqDel.id(R.id.tblchild1).gone();
            aqDel.id(R.id.imgchl1expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
            aqDel.id(R.id.tblchild4).gone();
            aqDel.id(R.id.imgchl4expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));

        }
    }

    void setFourthChildUI() {
        if (aqDel.id(R.id.tblchild4).getView().getVisibility() == View.VISIBLE) {
            aqDel.id(R.id.tblchild4).gone();
            aqDel.id(R.id.imgchl4expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
        } else {

            aqDel.id(R.id.tblchild2).gone();
            aqDel.id(R.id.imgchl2expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
            aqDel.id(R.id.tblchild3).gone();
            aqDel.id(R.id.imgchl3expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));
            aqDel.id(R.id.tblchild1).gone();
            aqDel.id(R.id.imgchl1expand).getImageView().setBackground(getResources().getDrawable(R.drawable.collapse));

            aqDel.id(R.id.tblchild4).visible();
            aqDel.id(R.id.imgchl4expand).getImageView().setBackground(getResources().getDrawable(R.drawable.expand));

        }
    }
//}

    private void setNoOfChildren(int position) throws Exception {

        if (deliveryInfoList == null || (deliveryInfoList != null && deliveryInfoList.size() <= 0)) {
            TableLayout tblchl1 = findViewById(R.id.tblchild1);
            clearText(tblchl1);
            TableLayout tblchl2 = findViewById(R.id.tblchild2);
            clearText(tblchl2);
            TableLayout tblchl3 = findViewById(R.id.tblchild3);
            clearText(tblchl3);
            TableLayout tblchl4 = findViewById(R.id.tblchild4);
            clearText(tblchl4);
        }

        if (position == 1) {


            aqDel.id(R.id.llchl1).visible();
            aqDel.id(R.id.llchl2).gone();
            aqDel.id(R.id.llchl3).gone();
            aqDel.id(R.id.llchl4).gone();


            if (woman.getRegpregormotheratreg() == 2
                    && woman.getRegADDate() != null
                    && woman.getRegADDate().trim().length()
                    > 0) {
                if (aqDel.id(R.id.etdob1).getText().toString().trim().length() <= 0)
                    aqDel.id(R.id.etdob1).text(woman.getRegADDate());
            } else {
                if (aqDel.id(R.id.etdob1).getText().toString().trim().length() <= 0)
                    aqDel.id(R.id.etdob1).text(DateTimeUtil.getTodaysDate());
                aqDel.id(R.id.txtadd).text(": " + DateTimeUtil.getTodaysDate());

            }
            if (deliveryInfoList== null || (deliveryInfoList!=null)
            && deliveryInfoList.size()<=0)
                aqDel.id(R.id.ettob1).text(DateTimeUtil.getTimeIn12HourFormat(DateTimeUtil.getCurrentTime()));
        } else if (position == 2) {


            aqDel.id(R.id.llchl1).visible();
            aqDel.id(R.id.llchl2).visible();
            aqDel.id(R.id.llchl3).gone();
            aqDel.id(R.id.llchl4).gone();

            if (woman.getRegpregormotheratreg() == 2
                    && woman.getRegADDate() != null
                    && woman.getRegADDate().trim().length()
                    > 0) {
                aqDel.id(R.id.etdob1).text(woman.getRegADDate());
                aqDel.id(R.id.etdob2).text(woman.getRegADDate());
            }
            else if(deliveryInfoList==null || (deliveryInfoList!=null && deliveryInfoList.size()<=0)){
                aqDel.id(R.id.etdob1).text(DateTimeUtil.getTodaysDate());
                aqDel.id(R.id.etdob2).text(DateTimeUtil.getTodaysDate());
                aqDel.id(R.id.txtadd).text(": " + DateTimeUtil.getTodaysDate());
            }
            if (deliveryInfoList== null || (deliveryInfoList!=null)
                    && deliveryInfoList.size()<=0) {
                aqDel.id(R.id.ettob1).text(DateTimeUtil.getTimeIn12HourFormat(DateTimeUtil.getCurrentTime()));
                aqDel.id(R.id.ettob2).text(DateTimeUtil.getTimeIn12HourFormat(DateTimeUtil.getCurrentTime()));
            }
        } else if (position == 3) {
            aqDel.id(R.id.llchl1).visible();
            aqDel.id(R.id.llchl2).visible();
            aqDel.id(R.id.llchl3).visible();
            aqDel.id(R.id.llchl4).gone();

            if (woman.getRegpregormotheratreg() == 2
                    && woman.getRegADDate() != null
                    && woman.getRegADDate().trim().length()
                    > 0) {
                aqDel.id(R.id.etdob1).text(woman.getRegADDate());
                aqDel.id(R.id.etdob2).text(woman.getRegADDate());

                aqDel.id(R.id.etdob3).text(woman.getRegADDate());
            }
            else {
                aqDel.id(R.id.etdob1).text(DateTimeUtil.getTodaysDate());
                aqDel.id(R.id.etdob3).text(DateTimeUtil.getTodaysDate());
                aqDel.id(R.id.etdob2).text(DateTimeUtil.getTodaysDate());

                aqDel.id(R.id.txtadd).text(": " + DateTimeUtil.getTodaysDate());
            }

            if (deliveryInfoList== null || (deliveryInfoList!=null)
                    && deliveryInfoList.size()<=0) {
                aqDel.id(R.id.ettob1).text(DateTimeUtil.getTimeIn12HourFormat(DateTimeUtil.getCurrentTime()));
                aqDel.id(R.id.ettob2).text(DateTimeUtil.getTimeIn12HourFormat(DateTimeUtil.getCurrentTime()));
                aqDel.id(R.id.ettob3).text(DateTimeUtil.getTimeIn12HourFormat(DateTimeUtil.getCurrentTime()));

            }
        } else if (position == 4) {
            aqDel.id(R.id.llchl1).visible();
            aqDel.id(R.id.llchl2).visible();
            aqDel.id(R.id.llchl3).visible();
            aqDel.id(R.id.llchl4).visible();

            if (woman.getRegpregormotheratreg() == 2
                    && woman.getRegADDate() != null
                    && woman.getRegADDate().trim().length()
                    > 0) {
                aqDel.id(R.id.etdob1).text(woman.getRegADDate());
                aqDel.id(R.id.etdob2).text(woman.getRegADDate());
                aqDel.id(R.id.etdob3).text(woman.getRegADDate());
                aqDel.id(R.id.etdob4).text(woman.getRegADDate());
            } else {
                aqDel.id(R.id.etdob1).text(DateTimeUtil.getTodaysDate());
                aqDel.id(R.id.etdob2).text(DateTimeUtil.getTodaysDate());
                aqDel.id(R.id.etdob3).text(DateTimeUtil.getTodaysDate());
                aqDel.id(R.id.etdob4).text(DateTimeUtil.getTodaysDate());
                aqDel.id(R.id.txtadd).text(": " + DateTimeUtil.getTodaysDate());

            }
            if (deliveryInfoList== null || (deliveryInfoList!=null)
                    && deliveryInfoList.size()<=0) {
                aqDel.id(R.id.ettob1).text(DateTimeUtil.getTimeIn12HourFormat(DateTimeUtil.getCurrentTime()));
                aqDel.id(R.id.ettob2).text(DateTimeUtil.getTimeIn12HourFormat(DateTimeUtil.getCurrentTime()));
                aqDel.id(R.id.ettob3).text(DateTimeUtil.getTimeIn12HourFormat(DateTimeUtil.getCurrentTime()));
                aqDel.id(R.id.ettob4).text(DateTimeUtil.getTimeIn12HourFormat(DateTimeUtil.getCurrentTime()));
            }
        } else {
            aqDel.id(R.id.llchl1).gone();
            aqDel.id(R.id.llchl2).gone();
            aqDel.id(R.id.llchl3).gone();
            aqDel.id(R.id.llchl4).gone();

        }

        if (!(deliveryInfoList != null && deliveryInfoList.size() > 0))
            setTribalHamlet();
    }

    //    Arpitha 26Nov2019
    void disableScreen() {
        ScrollView scrdel = findViewById(R.id.scrdel);
        aqDel.id(R.id.btnsave).enabled(false);
        disableEnableControls(false, scrdel);

    }

    void disableFields(int pos, boolean setTrue) {
        aqDel.id(rdPhyYesCh + pos).enabled(setTrue);
        aqDel.id(rdphyNoCh + pos).enabled(setTrue);
        aqDel.id(rdBreastYes + pos).enabled(setTrue);
        aqDel.id(rdBreastNo + pos).enabled(setTrue);
        aqDel.id(rdCryNo + pos).enabled(setTrue);
        aqDel.id(rdCryYes + pos).enabled(setTrue);
        //Manikanta 10feb2021
        aqDel.id(rdskintoskinYes + pos).enabled(setTrue);
        aqDel.id(rdskintoskinNo + pos).enabled(setTrue);

      /*  aqDel.id(R.id.trOtherPhy2).gone();
        aqDel.id(R.id.etotherPhy2).text("");*/

        if (pos == 1) {
            rgPhy1.clearCheck();
            rgCry1.clearCheck();
            rgBreast1.clearCheck();
            //Mani 10feb2021 added rgskintoskin
            rgSkintoskin1.clearCheck();
            aqDel.id(R.id.trOtherPhy1).gone();
            aqDel.id(R.id.etotherPhy1).text("");
        } else if (pos == 2) {
            rgPhy2.clearCheck();
            rgCry2.clearCheck();
            rgBreast2.clearCheck();
            //Mani 10feb2021 added rgskintoskin
            rgSkintoSkin2.clearCheck();
            aqDel.id(R.id.trOtherPhy2).gone();
            aqDel.id(R.id.etotherPhy2).text("");
        } else if (pos == 3) {
            rgPhy3.clearCheck();
            rgCry3.clearCheck();
            rgBreast3.clearCheck();
            //Mani 10feb2021 added rgskintoskin
            rgSkintoSkin3.clearCheck();
            aqDel.id(R.id.trOtherPhy3).gone();
            aqDel.id(R.id.etotherPhy3).text("");
        } else if (pos == 4) {
            rgPhy4.clearCheck();
            rgCry4.clearCheck();
            rgBreast4.clearCheck();
            //Mani 10feb2021 added rgskintoskin
            rgSkintoSkin4.clearCheck();
            aqDel.id(R.id.trOtherPhy4).gone();
            aqDel.id(R.id.etotherPhy4).text("");
        }


    }


    void clearText(ViewGroup view) {
        for (int i = 0; i < view.getChildCount(); i++) {
            View child = view.getChildAt(i);

            if (child instanceof EditText) {

                EditText et = (EditText) child;

                et.setText("");
            }
            if (child instanceof Spinner) {

                Spinner spn = (Spinner) child;

                spn.setSelection(0);
            }
            if (child instanceof RadioGroup) {

                RadioGroup rg = (RadioGroup) child;

                rg.clearCheck();
            }
            if (child instanceof MultiSelectionSpinner) {

                MultiSelectionSpinner spnMulti = (MultiSelectionSpinner) child;

                spnMulti.setSelection(new String[0]);
            }
            if (child instanceof ViewGroup) {
                clearText((ViewGroup) child);
            }
        }
    }


    /**
     * Setting AuditTrail pojo, calls - InserttblAuditTrail()
     *
     * @param transId
     */
    private boolean InserttblAuditTrail(String ColumnName, String Old_Value, String
            New_Value, int transId) throws Exception {

        APJ = new AuditPojo();
        APJ.setUserId(user.getUserId());
        APJ.setWomanId(woman.getWomanId());
        APJ.setTblName("tblregisteredwomen");
        APJ.setDateChanged(DateTimeUtil.getTodaysDate());
        APJ.setColumnName(ColumnName);
        APJ.setOld_Value(Old_Value);
        APJ.setNew_Value(New_Value);
        APJ.setTransId(transId);
//15Aug2019 - Bindu - set recordcreateddate
        APJ.setRecordcreateddate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());

        AuditRepository auditRepository = new AuditRepository(databaseHelper);
        return auditRepository.insertTotblAuditTrailNew(APJ, databaseHelper);

    }

    void updateRegAuditData() throws Exception {

        InserttblAuditTrail("regADDate",
                woman.getRegADDate(),
                aqDel.id(R.id.etdob1).getText().toString(),transId);

        InserttblAuditTrail("regPregnantorMother",
                ""+1,""+2,transId);

        InserttblAuditTrail("regInDanger",
                woman.getIsCompl(),""+compl,transId);
    }
}
