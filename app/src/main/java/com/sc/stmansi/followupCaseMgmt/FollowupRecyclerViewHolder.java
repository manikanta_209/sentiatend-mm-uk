package com.sc.stmansi.followupCaseMgmt;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sc.stmansi.R;

public class FollowupRecyclerViewHolder extends RecyclerView.ViewHolder {
    TextView Name,VisitCount,VisitDate,txtStatus,txtReferredToFacname,txtvisitmode, txtvisitusertype;
    ImageView Ref,Comp,View,imgWomanStatusHv;
    public FollowupRecyclerViewHolder(@NonNull View itemView) {
        super(itemView);
        Name = itemView.findViewById(R.id.txt_BeneName);
        VisitCount = itemView.findViewById(R.id.txt_VisitCount);
        VisitDate = itemView.findViewById(R.id.txt_VisitDate);
        Ref = itemView.findViewById(R.id.imgReferred);
        Comp = itemView.findViewById(R.id.imgComplication);
        View = itemView.findViewById(R.id.imgviewVisit);
        txtStatus = itemView.findViewById(R.id.txtStatusatvisit);
        txtReferredToFacname = itemView.findViewById(R.id.txtreferredtofacname);
        imgWomanStatusHv = itemView.findViewById(R.id.imgWstatus);
        txtvisitmode = itemView.findViewById(R.id.txtvisitmode); //13Aug2021 Bindu
        //21Aug2021 Bindu
        txtvisitusertype = itemView.findViewById(R.id.txtvisitusertype);
    }
}
