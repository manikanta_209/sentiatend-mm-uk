package com.sc.stmansi.followupCaseMgmt;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.google.gson.Gson;
import com.sc.stmansi.configuration.AppState;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MyPeriodicWork extends Worker {
    public static String result;
    private AppState appState;

    public MyPeriodicWork(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        appState = new AppState();
        appState.webServiceIpAddress = getInputData().getString("webServiceIpAddress");
//        appState.webServicePath = getInputData().getString("webServicePath");
        appState.apiPort = getInputData().getInt("webServicePath",0);
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .build();
        Map<String, String> payload = new HashMap<>();
        payload.put("MMId",getInputData().getString("MMId"));
        payload.put("clientDB",getInputData().getString("clientDB"));
        payload.put("lastServerDate", getInputData().getString("lastServerDate"));

        final MediaType PLAIN
                = MediaType.get("text/plain; charset=utf-8");
        RequestBody requestBody = RequestBody.create(new Gson().toJson(payload), PLAIN);
        HttpUrl url = new HttpUrl.Builder()
                .scheme("http")
                .host(appState.webServiceIpAddress)
                .port(appState.apiPort)
                .addPathSegment("fetchcasemgmt")
                .build();
//        String serverIp = "http://"+appState.webServiceIpAddress+appState.webServicePath;
//        String endpoint = serverIp+"fetchcasemgmt";
         Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        try (Response response = okHttpClient.newCall(request).execute()) {
            result = response.body().string();
            response.close();
            Log.e("Fetch Case Mgmt",result);
            return Result.success();
        } catch (Exception e) {
            result = e.getMessage();
            Log.e("Fetch Case Mgmt",e.getMessage());
            return Result.failure();
        }
    }
}
