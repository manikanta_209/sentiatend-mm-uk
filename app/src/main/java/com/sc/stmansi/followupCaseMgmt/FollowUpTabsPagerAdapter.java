package com.sc.stmansi.followupCaseMgmt;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.google.firebase.crashlytics.FirebaseCrashlytics;

public class FollowUpTabsPagerAdapter  extends FragmentPagerAdapter {


        FragmentManager fm;
        public FollowUpTabsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int index) {
            try {
                switch (index) {
                    case 0:
                        WomanFollowUp f1 = new WomanFollowUp();
                        Bundle b1 = new Bundle();
                        b1.remove("womenCondition");
                        b1.putString("womenCondition", "all");
                        f1.setArguments(b1);
                        return f1;
                    case 1:
                        AdolescentFollowUp f2 = new AdolescentFollowUp();
                        Bundle b2 = new Bundle();
                        b2.remove("womenCondition");
                        b2.putString("womenCondition", "preg");
                        f2.setArguments(b2);
                        return f2;
                    case 2:
                        ChildFollowUp f3 = new ChildFollowUp();
                        Bundle b3 = new Bundle();
                        b3.remove("womenCondition");
                        b3.putString("womenCondition", "Mother");
                        f3.setArguments(b3);
                        return f3;
                }
            } catch (Exception e) {

                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }

            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }

    }


