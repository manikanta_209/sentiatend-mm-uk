package com.sc.stmansi.followupCaseMgmt;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.About.AboutActivity;
import com.sc.stmansi.R;
import com.sc.stmansi.childtoadol.ChildtoAdol_List;
import com.sc.stmansi.common.AlertDialogUtil;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.common.InternetConnectionUtil;
import com.sc.stmansi.common.NavDrawerItem;
import com.sc.stmansi.common.NavDrawerListAdapter;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.pendingservices.IndicatorViewClickListener;
import com.sc.stmansi.recyclerview.ClickListener;
import com.sc.stmansi.recyclerview.RecyclerViewUpdateListener;
import com.sc.stmansi.repositories.CaseMgmtRepository;
import com.sc.stmansi.repositories.FacilityRepository;
import com.sc.stmansi.repositories.LoginRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblregisteredwomen;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class FollowUpTabsActivity extends AppCompatActivity implements ActionBar.OnNavigationListener, AdapterView.OnItemSelectedListener, IndicatorViewClickListener, ClickListener {

    AQuery aq;
    AppState appState;
    DatabaseHelper databaseHelper;
    Activity activity;
    private ViewPager viewPager;
    private FollowUpTabsPagerAdapter mAdapter;
    public String wFilterStr = "";
    // Tab titles
    private Integer[] tabs = {R.drawable.reg_women_small,
            R.drawable.ic_adolall,
            R.drawable.general_examination_baby};

    private Spinner villageSpinner;
    private ArrayAdapter<CharSequence> villageSpinnerAdapter;
    private Map<String, Integer> villageCodeForName;
    private int currentTab = 0;
    private List<Fragment> callbacks = new ArrayList<>(3);
    ;
    static String strFromDate = "";
    static String strToDate = "";
    static boolean fromDate;
    private ArrayList<String> riskDetails = new ArrayList<>();
    private ProgressDialog progressDialog;
    private DrawerLayout drawer;
    private ListView drawerList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_followuptabs);
        //  AllRegisteredWomanListFrag.islmpconfirm = false;

        try {
            aq = new AQuery(this);

            Bundle bundle = getIntent().getBundleExtra("globalState");
            appState = bundle.getParcelable("appState");

            getIntent().putExtra("wFilterStr", wFilterStr);


            databaseHelper = getHelper();

            activity = FollowUpTabsActivity.this;


            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setNavigationMode(androidx.appcompat.app.ActionBar.NAVIGATION_MODE_TABS);

            getSupportActionBar().setCustomView(R.layout.ejanani_village_filter);
            getSupportActionBar().setDisplayShowCustomEnabled(true);

//            InitalizeDrawer();

            aq.id(R.id.imgresetfilter).getImageView().setBackgroundResource(R.drawable.ic_reset_gray);//04Dec2019 Arpitha
            aq.id(R.id.imgresetfilter).enabled(false);//03May2021 Arpitha

            viewPager = findViewById(R.id.followuppager);
            viewPager.setOffscreenPageLimit(3);

            mAdapter = new com.sc.stmansi.followupCaseMgmt.FollowUpTabsPagerAdapter(getSupportFragmentManager());
            viewPager.setAdapter(mAdapter);
            viewPager.addOnPageChangeListener(new WomenViewPagerListener());

            for (Integer tab_name : tabs) {
                getSupportActionBar().addTab(getSupportActionBar().newTab()
                        .setIcon(tab_name).setTabListener(new TabSelectionListener()));
            }

            aq.id(R.id.imgresetfilter).getImageView().setOnClickListener(new FilterResetListener());


            villageSpinner = aq.id(R.id.spVillageFilter).getSpinner();
            villageSpinner.setOnItemSelectedListener(this);
            populateSpinnerVillage();

            villageSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    try {
                        CharSequence villageTitle = villageSpinnerAdapter.getItem(i);
                        if (!villageTitle.equals(getResources().getString(R.string.all)))
                            appState.selectedVillageCode = villageCodeForName.get(villageTitle);
                        appState.selectedVillageTitle = villageTitle.toString();
                        mAdapter = new FollowUpTabsPagerAdapter(getSupportFragmentManager());
                        for (Fragment activeFragment : callbacks) {
                            RecyclerViewUpdateListener f = (RecyclerViewUpdateListener) activeFragment;
                            f.updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr, false);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });


            EditText filterTextView = aq.id(R.id.etFollowUpFilter).getEditText();
            filterTextView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (v.getText() != null) {
                        aq.id(R.id.imgresetfilter).enabled(true);//03May2021 Arpitha
                        aq.id(R.id.imgresetfilter).getImageView().setBackgroundResource(R.drawable.ic_reset);
                        wFilterStr = v.getText().toString();
                        for (Fragment activeFragment : callbacks) {
                            RecyclerViewUpdateListener f = (RecyclerViewUpdateListener) activeFragment;
                            f.updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr, false);
                        }
                    }
                    return true;
                }
            });
            aq.id(R.id.btnok).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (aq.id(R.id.spndatefilter).getSelectedItemPosition() == 2
                            && (strFromDate.length() <= 0 || strToDate.length() <= 0)) {
                        displayAlert(getResources().getString(R.string.selectdate), null);
                    } else {
                        wFilterStr = filterTextView.getText().toString();
                        for (Fragment activeFragment : callbacks) {
                            RecyclerViewUpdateListener f = (RecyclerViewUpdateListener) activeFragment;
                            f.updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr, false);
                        }
                    }


                }
            });


            aq.id(R.id.txtfromdate).getTextView().setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (MotionEvent.ACTION_UP == event.getAction()) {
                        fromDate = true;
                        DialogFragment newFragment = new DatePickerFragment(aq.id(R.id.txtfromdate));
                        newFragment.show(getSupportFragmentManager(), "datePicker");

                    }
                    return true;
                }
            });

            aq.id(R.id.txttodate).getTextView().setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (MotionEvent.ACTION_UP == event.getAction()) {
                        fromDate = false;
                        DialogFragment newFragment = new DatePickerFragment(aq.id(R.id.txttodate));
                        newFragment.show(getSupportFragmentManager(), "datePicker");

                    }
                    return true;
                }
            });

            aq.id(R.id.btnok).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (aq.id(R.id.spndatefilter).getSelectedItemPosition() == 2
                            && (strFromDate.length() <= 0 || strToDate.length() <= 0)) {
                        displayAlert(getResources().getString(R.string.selectdate), null);
                    } else {
                        wFilterStr = filterTextView.getText().toString();
                        for (Fragment activeFragment : callbacks) {
                            RecyclerViewUpdateListener f = (RecyclerViewUpdateListener) activeFragment;
                            f.updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr, false);
                        }
                    }


                }
            });


            //25Mar2021 - Bindu - Oncheck Change listener

           /* CheckBox chk = findViewById(R.id.chklmpnotconfirmed);
            chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    for (Fragment activeFragment : callbacks) {
                        RecyclerViewUpdateListener f = (RecyclerViewUpdateListener) activeFragment;
                        f.updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr,aq.id(R.id.chklmpnotconfirmed).isChecked());
                    }
                }
            });*/


            aq.id(R.id.updateNotify).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    appState.internetOn = InternetConnectionUtil.isConnectionAvailable(getApplicationContext(), appState);
                    if (appState.internetOn) {
                        progressDialog = new ProgressDialog(FollowUpTabsActivity.this);
                        progressDialog.setCancelable(false);
                        progressDialog.setMessage(getResources().getString(R.string.m176));
                        progressDialog.create();
                        progressDialog.show();
                        progressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialogInterface) {
                                try {
                                    mAdapter = new FollowUpTabsPagerAdapter(getSupportFragmentManager());
                                    for (Fragment activeFragment : callbacks) {
                                        RecyclerViewUpdateListener f = (RecyclerViewUpdateListener) activeFragment;
                                        f.updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr, false);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                        getCaseManagment();
                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.m114), Toast.LENGTH_LONG).show();
                        appState.internetOn = InternetConnectionUtil.isConnectionAvailable(getApplicationContext(), appState);
                    }
                }
            });

        } catch (Exception e) {

            e.printStackTrace();

        }
    }

    private void InitalizeDrawer() {
        try {
            //drawer = findViewById(R.id.drawer_NotiftyFollowup);
            //drawerList = findViewById(R.id.lvNav_NotifyFollowup);

            ArrayList<NavDrawerItem> navDrawerItems = new ArrayList<>();
            //navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.casemgmt), R.drawable.ic_homevisit));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.childtoadol), R.drawable.ic_adolall));
            drawer.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
            drawerList.setAdapter(new NavDrawerListAdapter(getApplicationContext(), navDrawerItems));
            drawerList.setOnItemClickListener(new DrawerItemClickListener());
            Toolbar toolbar = findViewById(R.id.toolbar_List_SanNap);
            ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close);
            drawer.setDrawerListener(drawerToggle);
            drawerToggle.syncState();
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private void getCaseManagment() {
        try {
            CaseMgmtRepository caseMgmtRepository = new CaseMgmtRepository(databaseHelper);
            String lastServerDate = caseMgmtRepository.getLastServerDate();
            UserRepository userRepo = new UserRepository(databaseHelper);
            TblInstusers user = userRepo.getOneAuditedUser(appState.ashaId);

            Data data = new Data.Builder()
                    .putString("webServiceIpAddress", appState.webServiceIpAddress)
                    .putInt("webServicePath", appState.apiPort)
                    .putString("lastServerDate", lastServerDate)
                    .putString("clientDB", user.getInst_Database())
                    .putString("MMId", user.getUserId())
                    .build();


            OneTimeWorkRequest periodicWorkRequest = new OneTimeWorkRequest.Builder(MyPeriodicWork.class)
                    .setInputData(data)
                    .build();


            WorkManager.getInstance().getWorkInfoByIdLiveData(periodicWorkRequest.getId()).observe(this, workInfo -> {
                if (workInfo != null) {
                    if (workInfo.getState() == WorkInfo.State.SUCCEEDED) {
                        String result = MyPeriodicWork.result;
                        updateCaseMgmt(result);
                    } else if (workInfo.getState() == WorkInfo.State.FAILED) {
                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                        if (MyPeriodicWork.result.toLowerCase().contains("failed to connect") || MyPeriodicWork.result.toLowerCase().contains("sockettimeout")) {
                            AlertDialogUtil.displayAlertMessage("Could not connect to Server, Try again later", FollowUpTabsActivity.this);
                        }
                    }
                }
            });
            WorkManager.getInstance().enqueue(periodicWorkRequest);
        } catch (Exception e) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private void updateCaseMgmt(String result) {
        try {
            ArrayList<ContentValues> arrVillage = null;
            SQLiteDatabase db;
            db = databaseHelper.getWritableDatabase();
            if (result.equals("Error") || result.equals("Internal server error") || result.toLowerCase().contains("unknown database")) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                AlertDialogUtil.displayAlertMessage("Data not fetched, Try again later", FollowUpTabsActivity.this);
            } else {
                String xml = result;

                xml = xml.replace("<NewDataSet>", "")
                        .replace("</NewDataSet>", "").
                                replace("<NewDataSet/>", "").trim();

                String[] strVal;
                strVal = xml.split("</Table>");
                if (xml.length() != 0) {
                    for (String s : strVal) {
                        s = s + "</Table>";
                        InputStream is = new ByteArrayInputStream(s.getBytes(StandardCharsets.UTF_8));
                        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                        Document doc = dBuilder.parse(is);
                        doc.getDocumentElement().normalize();

                        NodeList nList = doc.getElementsByTagName("Table");

                        arrVillage = new ArrayList<>();

                        if (nList.getLength() > 0) {

                            for (int temp = 0; temp < nList.getLength(); temp++) {

                                Node nNode = nList.item(temp);

                                ContentValues values = new ContentValues();

                                Element eElement = (Element) nNode;

                                if (eElement.getElementsByTagName("regRiskFactorsByCC").item(0) != null) {

                                    String dtemp = "update tblregisteredwomen Set ";

                                    dtemp = dtemp + " regRiskFactorsByCC = '" +
                                            getTagValue(eElement, "regRiskFactorsByCC") + "', ";
                                    dtemp = dtemp + " regOtherRiskByCC = '" +
                                            getTagValue(eElement, "regOtherRiskByCC") + "', ";

                                    dtemp = dtemp + " regAge = '" +
                                            getTagValue(eElement, "regAge") + "', ";
                                    dtemp = dtemp + " regWomanWeight = '" +
                                            getTagValue(eElement, "regWomanWeight") + "', ";
                                    dtemp = dtemp + " regHeight = '" +
                                            getTagValue(eElement, "regHeight") + "', ";
                                    dtemp = dtemp + " regComplicatedpreg = '" +
                                            getTagValue(eElement, "regComplicatedpreg") + "', ";
                                    dtemp = dtemp + " regAmberOrRedColorCode = '" +
                                            getTagValue(eElement, "regAmberOrRedColorCode") + "', ";
                                    dtemp = dtemp + " regrecommendedPlaceOfDelivery = '" +
                                            getTagValue(eElement, "regrecommendedPlaceOfDelivery") + "', ";
                                    dtemp = dtemp + " regBloodGroup = '" +
                                            getTagValue(eElement, "regBloodGroup") + "', ";
                                    dtemp = dtemp + " regheightUnit = '" +
                                            getTagValue(eElement, "regheightUnit") + "', ";

                                    dtemp = dtemp + " regCCConfirmRisk = '" +
                                            getTagValue(eElement, "regCCConfirmRisk") +
                                            "' where  WomanId = '" +
                                            getTagValue(eElement, "WomanId") + "'";


                                    riskDetails.add(dtemp);

                                } else {
                                    values.put("autoId", getTagValue(eElement, "autoId"));
                                    values.put("UserId", getTagValue(eElement, "UserId"));
                                    values.put("MMUserId", getTagValue(eElement, "MMUserId"));
                                    values.put("BeneficiaryType", getTagValue(eElement, "BeneficiaryType"));
                                    values.put("BeneficiaryID", getTagValue(eElement, "BeneficiaryID"));
                                    values.put("chlNo", getTagValue(eElement, "chlNo"));
                                    values.put("BeneficiaryParentID", getTagValue(eElement, "BeneficiaryParentID"));
                                    values.put("VisitType", getTagValue(eElement, "VisitType"));
                                    values.put("VisitMode", getTagValue(eElement, "VisitMode"));
                                    values.put("VisitNum", getTagValue(eElement, "VisitNum"));
                                    values.put("StatusAtVisit", getTagValue(eElement, "StatusAtVisit"));
                                    values.put("VisitDate", getTagValue(eElement, "VisitDate"));
                                    values.put("VisitNumbyMM", getTagValue(eElement, "VisitNumbyMM"));
                                    values.put("VisitDatebyMM", getTagValue(eElement, "VisitDatebyMM"));
                                    values.put("VisitBeneficiaryDangerSigns", getTagValue(eElement, "VisitBeneficiaryDangerSigns"));
                                    values.put("hcmBeneficiaryVisitFac", getTagValue(eElement, "hcmBeneficiaryVisitFac"));
                                    values.put("hcmReasonForNoVisit", getTagValue(eElement, "hcmReasonForNoVisit"));
                                    values.put("hcmReasonOthers", getTagValue(eElement, "hcmReasonOthers"));
                                    values.put("hcmActTakByUserType", getTagValue(eElement, "hcmActTakByUserType"));
                                    values.put("hcmActTakByUserName", getTagValue(eElement, "hcmActTakByUserName"));
                                    values.put("hcmActTakAtFacility", getTagValue(eElement, "hcmActTakAtFacility"));
                                    values.put("hcmActTakFacilityName", getTagValue(eElement, "hcmActTakFacilityName"));
                                    values.put("hcmActTakDate", getTagValue(eElement, "hcmActTakDate"));
                                    values.put("hcmActTakTime", getTagValue(eElement, "hcmActTakTime"));
                                    values.put("hcmActTakForCompl", getTagValue(eElement, "hcmActTakForCompl"));
                                    values.put("hcmActMedicationsPres", getTagValue(eElement, "hcmActMedicationsPres"));
                                    values.put("hcmActTakStatus", getTagValue(eElement, "hcmActTakStatus"));
                                    values.put("hcmActTakStatusDate", getTagValue(eElement, "hcmActTakStatusDate"));
                                    values.put("hcmActTakStatusTime", getTagValue(eElement, "hcmActTakStatusTime"));
                                    values.put("hcmActTakReferredToFacility", getTagValue(eElement, "hcmActTakReferredToFacility"));
                                    values.put("hcmActTakReferredToFacilityName", getTagValue(eElement, "hcmActTakReferredToFacilityName"));
                                    values.put("hcmActTakComments", getTagValue(eElement, "hcmActTakComments"));
                                    values.put("hcmAdviseGiven", getTagValue(eElement, "hcmAdviseGiven"));
                                    values.put("hcmBeneficiaryCondatVisit", getTagValue(eElement, "hcmBeneficiaryCondatVisit"));
                                    values.put("hcmActionToBeTakenClient", getTagValue(eElement, "hcmActionToBeTakenClient"));
                                    values.put("hcmInputToNextLevel", getTagValue(eElement, "hcmInputToNextLevel"));
                                    values.put("hcmIsANMInformedAbtCompl", getTagValue(eElement, "hcmIsANMInformedAbtCompl"));
                                    values.put("hcmANMAwareOfRef", getTagValue(eElement, "hcmANMAwareOfRef"));
                                    values.put("VisitReferredtoFacType", getTagValue(eElement, "VisitReferredtoFacType"));
                                    values.put("VisitReferredtoFacName", getTagValue(eElement, "VisitReferredtoFacName"));
                                    values.put("VisitReferralSlipNumber", getTagValue(eElement, "VisitReferralSlipNumber"));
                                    values.put("hcmCreatedByUserType", getTagValue(eElement, "hcmCreatedByUserType"));
                                    values.put("transId", getTagValue(eElement, "transId"));
                                    values.put("RecordCreatedDate", getTagValue(eElement, "RecordCreatedDate"));
                                    values.put("RecordUpdatedDate", getTagValue(eElement, "RecordUpdatedDate"));
                                    values.put("ServerCreatedDate", getTagValue(eElement, "ServerCreatedDate"));
                                    values.put("ServerUpdatedDate", getTagValue(eElement, "ServerUpdatedDate"));
                                    arrVillage.add(values);
                                }

                            }
                        }

                        if (arrVillage != null && arrVillage.size() > 0) {
                            for (ContentValues values1 : arrVillage) {
                                db.insert("tblCaseMgmt", null, values1);
                            }
                        }

                        if (riskDetails.size() > 0) {
                            for (String str : riskDetails) {
                                db.execSQL(str);
                            }
                        }
                    }
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    AlertDialogUtil.displayAlertMessage("Updated", FollowUpTabsActivity.this);
                } else {
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    AlertDialogUtil.displayAlertMessage("Up to Date", FollowUpTabsActivity.this);
                }
                mAdapter = new FollowUpTabsPagerAdapter(getSupportFragmentManager());
                for (Fragment activeFragment : callbacks) {
                    RecyclerViewUpdateListener f = (RecyclerViewUpdateListener) activeFragment;
                    f.updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr, false);
                }
            }
        } catch (Exception e) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    //    initialize db object
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public boolean onLongClick(View view) {
        return false;
    }


    //    view pager class
    class WomenViewPagerListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageSelected(int position) {
            currentTab = position;
            getSupportActionBar().setSelectedNavigationItem(position);
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {
        }
    }

    /* @Override
     public boolean onNavigationItemSelected(int i, long l) {
         return false;
     }
 */
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    //    tab listener class
    class TabSelectionListener implements androidx.appcompat.app.ActionBar.TabListener {

        @Override
        public void onTabSelected(androidx.appcompat.app.ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
            currentTab = tab.getPosition();
            viewPager.setCurrentItem(tab.getPosition());
            if (currentTab == 2) //08Apr2021 Bindu - hide display lmp not confirmed for PNC tab
                aq.id(R.id.chklmpnotconfirmed).gone();
            else
                aq.id(R.id.chklmpnotconfirmed).visible();
        }

        @Override
        public void onTabUnselected(androidx.appcompat.app.ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

        }

        @Override
        public void onTabReselected(androidx.appcompat.app.ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * populate Village to Spinner
     */
    private void populateSpinnerVillage() throws Exception {
        FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
        villageCodeForName = facilityRepository.getPlaceMap();

        villageSpinnerAdapter = new ArrayAdapter<>(this, R.layout.actionbar_spinner_list_item);
//        villageSpinnerAdapter.add("All Women");06May2021 Arpitha
        villageSpinnerAdapter.add(getResources().getString(R.string.all));//06May2021 Arpitha
        for (Map.Entry<String, Integer> village : villageCodeForName.entrySet()) {
            villageSpinnerAdapter.add(village.getKey());
        }
        villageSpinnerAdapter.setDropDownViewResource(R.layout.actionbar_spinner_layout_view);
        villageSpinner.setAdapter(villageSpinnerAdapter);

    }

    /**
     * /** Actionbar navigation item select listener
     */
    @Override
    public boolean onNavigationItemSelected(int itemPosition, long itemId) {
        try {
            CharSequence villageTitle = villageSpinnerAdapter.getItem(itemPosition);
            appState.selectedVillageCode = villageCodeForName.get(villageTitle);
            mAdapter = new FollowUpTabsPagerAdapter(getSupportFragmentManager());
            invalidateOptionsMenu();
            for (Fragment activeFragment : callbacks) {
                RecyclerViewUpdateListener f = (RecyclerViewUpdateListener) activeFragment;
                f.updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr, false);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            return false;
        }
    }

    /**
     * on back click send to tabmenu activity
     */
    @Override
    public void onBackPressed() {
    }

    /**
     * Initializes the Action Bar items
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.regwomenmenu, menu);
        menu.findItem(R.id.newregmenu).setVisible(false);

        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Called whenever an item clicked in actionbar
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action buttons
        switch (item.getItemId()) {
            case R.id.home: {
                Intent goToScreen = new Intent(com.sc.stmansi.followupCaseMgmt.FollowUpTabsActivity.this, MainMenuActivity.class);
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                return true;
            }
            case android.R.id.home: {
                Intent goToScreen = new Intent(com.sc.stmansi.followupCaseMgmt.FollowUpTabsActivity.this, MainMenuActivity.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                startActivity(goToScreen);
                return true;
            }
            case R.id.logout: {
                Intent goToScreen = new Intent(com.sc.stmansi.followupCaseMgmt.FollowUpTabsActivity.this, LoginActivity.class);
                displayAlert(getResources().getString(R.string.m111), goToScreen);
                return true;
            }
            //30Sep2019 - Bindu
            case R.id.about: {
                Intent goToScreen = new Intent(com.sc.stmansi.followupCaseMgmt.FollowUpTabsActivity.this, AboutActivity.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }


            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //    prepare bundle
    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        return b;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        return super.onPrepareOptionsMenu(menu);
    }

    /**
     * Save Confirmation Alert
     *
     * @param goToScreen
     * @param spanText2
     */
    private void displayAlert(final String spanText2, final Intent goToScreen) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set dialog message
        alertDialogBuilder
                .setMessage(spanText2)
                .setCancelable(false)
                .setNegativeButton((getResources().getString(R.string.m121)), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (goToScreen != null) {
                            startActivity(goToScreen);
                            if (spanText2.contains("logout")) {
                                try {
                                    LoginRepository loginRepository = new LoginRepository(databaseHelper);
                                    loginRepository.updateLogoutTime(appState.loginAuditId, appState.sessionUserId);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                                    crashlytics.log(e.getMessage());
                                }
                            }
                        } else
                            dialog.cancel();
                    }
                })
                .setPositiveButton((getResources().getString(R.string.m122)), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    /**
     * This method Calls the idle timeout
     */ //09Aug2019 - Bindu
    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        //ActivitiesStack.delayedIdle(Integer.parseInt(CommonClass.properties.getProperty("idleTimeOut")));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        /*
         * You'll need this in your class to release the helper when done.
         */
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

    public void addFragment(Fragment f) {
        if (this.callbacks == null) {
            this.callbacks = new ArrayList<>(3);
        }
        this.callbacks.add(f);
    }

    //    filter classt
    class FilterResetListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            try {//03May2021 Arpitha
                aq.id(R.id.imgresetfilter).enabled(false);//03May2021 Arpitha
                aq.id(R.id.imgresetfilter).getImageView().setBackgroundResource(R.drawable.ic_reset_gray);
                aq.id(R.id.etFollowUpFilter).text("");
                wFilterStr = "";
                for (Fragment activeFragment : callbacks) {
                    RecyclerViewUpdateListener f = (RecyclerViewUpdateListener) activeFragment;
                    f.updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr, false);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    @SuppressLint("SimpleDateFormat")
    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        private AQuery aqPMSel;
        private tblregisteredwomen woman;

        public DatePickerFragment() {
        }

        public DatePickerFragment(AQuery aQuery) {
            aqPMSel = aQuery;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            int year = 0, month = 0, day = 0;
            try {

                Calendar c = Calendar.getInstance();
//                String defaultDate = DateTimeUtil.getTodaysDate(); //09Aug2019 - Cal set date of Provided date
                String defaultDate = aqPMSel.getText().toString(); //09Aug2019 - Cal set date of Provided date

                defaultDate = DateTimeUtil.getTodaysDate();
                if (defaultDate.length() > 0) {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    Date sendDate = sdf.parse(defaultDate);
                    c.setTime(sendDate);
                    year = c.get(Calendar.YEAR);
                    month = c.get(Calendar.MONTH);
                    day = c.get(Calendar.DAY_OF_MONTH);
                } else {
                    c.setTime(new Date());
                    year = c.get(Calendar.YEAR);
                    month = c.get(Calendar.MONTH);
                    day = c.get(Calendar.DAY_OF_MONTH);
                }
            } catch (Exception e) {

                e.printStackTrace();
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }

            // Create a new instance of DatePickerDialog and return it
            DatePickerDialog dpDialog;
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                // Do something for lollipop and above versions
                dpDialog = new DatePickerDialog(getActivity(), R.style.DialogTheme, this, year, month, day);
            } else {
                // do something for phones running an SDK before lollipop
                dpDialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, this, year, month, day);
                dpDialog.setTitle("Set a Date");
                dpDialog.getDatePicker().setCalendarViewShown(false);
            }
            return dpDialog;
        }

        @Override
        public void onDateSet(DatePicker view, int year, int month, int day) {
            if (view.isShown()) {
                try {
                    String SelectedDate = String.format("%02d", day) + "-" +
                            String.format("%02d", month + 1) + "-"
                            + year;
//                    Date lmpDate = null;
                    SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                    Date seldate = format.parse(SelectedDate);
                    Date fromdate = null;

                    if (strFromDate != null && strFromDate.trim().length() > 0)
                        fromdate = format.parse(strFromDate);
//
                    if (fromDate) {
                        if (seldate.after(new Date())) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.date_cannot_af_currentdate), Toast.LENGTH_LONG).show();
                            strFromDate = "";
                            aqPMSel.id(R.id.txtfromdate).text(getResources().getString(R.string.fromdate));
                        } else {
                            strFromDate = SelectedDate;
                            aqPMSel.id(R.id.txtfromdate).text(strFromDate);
                        }
                    } else {

                        if (seldate.after(new Date())) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.date_cannot_af_currentdate), Toast.LENGTH_LONG).show();
                            strToDate = "";
                            aqPMSel.id(R.id.txttodate).text(getResources().getString(R.string.todate));
                        } else if (fromdate != null && seldate.before(fromdate)) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.date_cannot_af_currentdate), Toast.LENGTH_LONG).show();
                            strToDate = "";
                            aqPMSel.id(R.id.txttodate).text(getResources().getString(R.string.todate));
                        } else {
                            strToDate = SelectedDate;
                            aqPMSel.id(R.id.txttodate).text(strToDate);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                    crashlytics.log(e.getMessage());

                }
            }
        }
    }

    @Override
    public void displayAlert(int selectedItemPosition) {

    }

    @Override
    public void displayHomeVisitList(int selectedItemPosition) {

    }

    @Override
    public void displayDelComplicationAlert(int selectedItemPosition) {

    }

    @Override
    public void displayLMPConfirmation(int selectedItemPosition) {

    }

    public String getTagValue(Element eElement, String tagName) {
        if (eElement.getElementsByTagName(tagName).item(0) != null)
            return eElement.getElementsByTagName(tagName).item(0).getTextContent();
        else
            return "";
    }

    public void RefreshData() {
        for (Fragment activeFragment : callbacks) {
            RecyclerViewUpdateListener f = (RecyclerViewUpdateListener) activeFragment;
            f.updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr, false);
        }
    }

    private class DrawerItemClickListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            drawer.closeDrawer(drawerList);
            try{
                switch (i){
                    case 0:
                        drawer.closeDrawer(drawerList);
                        break;
                    case 1:
                        Intent intent = new Intent(FollowUpTabsActivity.this, ChildtoAdol_List.class);
                        intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        startActivity(intent);
                        break;
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
