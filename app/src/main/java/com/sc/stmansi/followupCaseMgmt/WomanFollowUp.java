package com.sc.stmansi.followupCaseMgmt;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.HomeVisit.HomeVisit;
import com.sc.stmansi.R;
import com.sc.stmansi.common.WrapContentLinearLayoutManager;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.quickAction.ActionItem;
import com.sc.stmansi.quickAction.QuickAction;
import com.sc.stmansi.recyclerview.ClickListener;
import com.sc.stmansi.recyclerview.RecyclerViewUpdateListener;
import com.sc.stmansi.repositories.CaseMgmtRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblCaseMgmt;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.viewprofile.ViewProfileActivity;
import com.sc.stmansi.womanlist.QueryParams;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class WomanFollowUp extends Fragment implements ClickListener,
        RecyclerViewUpdateListener {


    //31Aug2019 - Bindu
    private static final int ID_WEDIT = 1;
    private static final int ID_HOMEVISITADD = 2;
    private static final int ID_DEL = 3;//31Oct2019 Arpitha
    private static final int ID_CHILD = 4;//16Nov2019 Arpitha
    private static final int ID_DEACT = 5;//19Nov2019 Arpitha
    QuickAction mQuickAction = null;
    tblCaseMgmt woman;
    private TblInstusers user;
    private String wFilterStr;
    private TextView allCountView, noDataLabelView;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter registeredWomanAdapter;
    private List<tblCaseMgmt> filteredRowItems;
    private boolean isUpdating;
    private DatabaseHelper databaseHelper;
    private AppState appState;
    private WomanRepository womanRepository;
    private int nextItemPosition;
    private String womenCondition;
    private long womenCount;
    public static boolean islmpconfirm = false; //02May2021 Bindu
    private View parentView;
    private WrapContentLinearLayoutManager wrapManager;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        FollowUpTabsActivity r = (FollowUpTabsActivity) context;
        r.addFragment(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        parentView = inflater.inflate(R.layout.fragment_womanfollowup, container, false);
        return parentView;
    }

    /**
     * Initialize font, database, UsersPojo variables and village spinner
     * calls - populateWomenList
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        AQuery aQuery = new AQuery(getActivity());
        try {
            databaseHelper = getHelper();
            islmpconfirm = false;
            Bundle bundle = getActivity().getIntent().getParcelableExtra("globalState");
            appState = bundle.getParcelable("appState");

            UserRepository userRepository = new UserRepository(databaseHelper);
            user = userRepository.getOneAuditedUser(appState.ashaId);

            womanRepository = new WomanRepository(databaseHelper);

            wFilterStr = getActivity().getIntent().getStringExtra("wFilterStr");

            final Bundle b = getArguments();
            womenCondition = b.getString("womenCondition");

            noDataLabelView = aQuery.id(R.id.tvWomanListLblAllWomen).getTextView();
            allCountView = aQuery.id(R.id.txtcountwfollowup).getTextView();
            // 30April2021 Guru
//            womenCount = getTotalWomenCount(user.getUserId(),
//                    0, womenCondition, "", false, womanRepository, false);
            womenCount = getCaseRecordsCount(new CaseMgmtRepository(databaseHelper), 0, "", "Woman");
            allCountView.setText(getResources().getString(R.string.recordcount, womenCount));

            recyclerView = getView().findViewById(R.id.all_women_recycler_view);
            recyclerView.setVisibility(View.VISIBLE);
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            recyclerView.setHasFixedSize(true);

            // use a linear layout manager
//            layoutManager = new LinearLayoutManager(getActivity());
//            recyclerView.setLayoutManager(layoutManager);
//            linearLayoutManager = (LinearLayoutManager) layoutManager;
            filteredRowItems = new ArrayList<>();
            registeredWomanAdapter = new FollowupRecyclerAdapter(filteredRowItems, parentView.getContext(), databaseHelper, appState, getActivity());
            recyclerView.setAdapter(registeredWomanAdapter);
            wrapManager = new WrapContentLinearLayoutManager(parentView.getContext(), LinearLayout.VERTICAL, true);
            recyclerView.setLayoutManager(wrapManager);

            QueryParams params = new QueryParams();
            params.textViewFilter = "";
            params.selectedVillageCode = 0;
            params.nextItemPosition = 0;
//            new LoadPregnantList(this, params, true).execute();
            isUpdating = true;

            recyclerView.addOnScrollListener(new OnRecyclerScrollListener());

            //31Aug2019 - Bindu - ADD items for quick action

            //    Mani Jun 1 2021 Removed UnplannedServicesListActivity & ServicesSummaryActivity
            String str = getActivity().getResources().getString(R.string.viewprofile);
            ActionItem wEdit = new ActionItem(ID_WEDIT, str, ContextCompat.getDrawable(getActivity(), R.drawable.ic_edit_60));

            str = getActivity().getResources().getString(R.string.homevisit);
            ActionItem wHomeVisitAdd = new ActionItem(ID_HOMEVISITADD, str, ContextCompat.getDrawable(getActivity(), R.drawable.ic_homevisit));

            str = getActivity().getResources().getString(R.string.deliveryHeading);
            ActionItem wDel = new ActionItem(ID_DEL, str, ContextCompat.getDrawable(getActivity(), R.drawable.delivery_info));

//          16Nov2019 Arpitha
            str = getActivity().getResources().getString(R.string.viewchild);
            ActionItem wChild = new ActionItem(ID_CHILD, str, ContextCompat.getDrawable(getActivity(), R.drawable.general_examination_baby));

            //          19Nov2019 Arpitha
            str = getActivity().getResources().getString(R.string.deactivate);
            ActionItem wDeact = new ActionItem(ID_DEACT, str, ContextCompat.getDrawable(getActivity(), R.drawable.deactivate));


            mQuickAction = new QuickAction(getActivity());

            mQuickAction.addActionItem(wEdit);
            mQuickAction.addActionItem(wHomeVisitAdd);
            mQuickAction.addActionItem(wDel);
            mQuickAction.addActionItem(wChild);
            mQuickAction.addActionItem(wDeact);//19Nov2019 Arpitha

            //setup the action item click listener
//            mQuickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
//                @Override
//                public void onItemClick(QuickAction quickAction, int pos, int actionId) {
//                    ActionItem actionItem = quickAction.getActionItem(pos);
//                    try {
//                        switch (actionItem.getActionId()) {
//                            case ID_WEDIT: {
//                                Intent nextScreen = new Intent(getActivity(), ViewProfileActivity.class);
//                                nextScreen.putExtra("globalState", getActivity().getIntent().getBundleExtra("globalState"));
//                                getActivity().startActivity(nextScreen);
//                                mQuickAction.dismiss();
//                                break;
//                            }
//                            //    Mani Jun 1 2021 Removed UnplannedServicesListActivity & ServicesSummaryActivity
//                            case ID_HOMEVISITADD: {
//                                //18Oct2019 - Bindu - Chk nearing del message
//                                WomanRepository womanRepository = new WomanRepository(databaseHelper); //11Nov2019 - Bindu - Add Woman repo and get woman details
//                                woman = womanRepository.getRegistartionDetails(appState.selectedWomanId, appState.sessionUserId);
//
//                                if (woman.getRegPregnantorMother() == 2) {
//                                    Intent nextScreen = new Intent(getActivity(), PncHomeVisit.class);
//                                    nextScreen.putExtra("globalState", getActivity().getIntent().getBundleExtra("globalState"));
//                                    getActivity().startActivity(nextScreen);
//                                } else {
//                                    int daysDiff = DateTimeUtil.getDaysBetweenDates(DateTimeUtil.getTodaysDate(), woman.getRegLMP());
//                                    if (daysDiff > 210 && woman.getRegPregnantorMother() == 1) { //11Nov2019 - Bindu - add only for pregnant woman
//                                        displayNearingDelAlertMessage(woman);
//                                    } else {
//                                        Intent nextScreen = new Intent(getActivity(), HomeVisit.class);
//                                        nextScreen.putExtra("globalState", getActivity().getIntent().getBundleExtra("globalState"));
//                                        getActivity().startActivity(nextScreen);
//                                    }
//                                }
//                                mQuickAction.dismiss();
//                                break;
//                            }
//                            case ID_DEL: {
//                                WomanRepository womanRepository = new WomanRepository(databaseHelper);
//                                woman = womanRepository.getRegistartionDetails(appState.selectedWomanId, appState.sessionUserId);
//
//                                if (woman.getRegPregnantorMother() == 2 || DateTimeUtil.getNumberOfDaysFromLMP(woman.getRegLMP()) > 210) {
//
//                                    Intent nextScreen = new Intent(getActivity(), DeliveryInfoActivity.class);
//                                    nextScreen.putExtra("globalState", getActivity().getIntent().getBundleExtra("globalState"));
//                                    nextScreen.putExtra("woman", woman);
//                                    getActivity().startActivity(nextScreen);
//                                    mQuickAction.dismiss();
//                                } else {
//                                    if (woman.getDateDeactivated() != null && woman.getDateDeactivated().trim().length() > 0)
//                                        Toast.makeText(getActivity(), getResources().getString(R.string.userdeactivated), Toast.LENGTH_LONG).show();
//
//                                    else
//                                        Toast.makeText(getActivity(), getResources().getString(R.string.m132), Toast.LENGTH_LONG).show();
//
//                                }
//                                break;
//                            }
////                          16Nov2019 Arpitha
//                            case ID_CHILD: {
//                                WomanRepository womanRepository = new WomanRepository(databaseHelper);
//                                woman = womanRepository.getRegistartionDetails(appState.selectedWomanId, appState.sessionUserId);
//
//                                if (woman.getRegPregnantorMother() == 2 && woman.getRegADDate() != null && woman.getRegADDate().trim().length() > 0) {
//
//
//                                    Intent nextScreen = new Intent(getActivity(), RegisteredChildList.class);
//                                    nextScreen.putExtra("globalState", getActivity().getIntent().getBundleExtra("globalState"));
//                                    nextScreen.putExtra("womanId", woman.getWomanId());
//
//                                    getActivity().startActivity(nextScreen);
//                                } else
//                                    Toast.makeText(getActivity(), getResources().getString(R.string.ntapplicable), Toast.LENGTH_LONG).show();
//
//                                mQuickAction.dismiss();
//                                break;
//                            }
////                          19Nov2019 Arpitha
//                            case ID_DEACT:
//                                WomanRepository womanRepository = new WomanRepository(databaseHelper);
//                                woman = womanRepository.getRegistartionDetails(appState.selectedWomanId, appState.sessionUserId);
//
//                                Intent deact = new Intent(getActivity(), WomanDeactivateActivity.class);
//                                deact.putExtra("globalState", getActivity().getIntent().getBundleExtra("globalState"));
//                                deact.putExtra("woman", woman);
//                                startActivity(deact);
//
//                                mQuickAction.dismiss();
//                                break;
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            });

            mQuickAction.setOnDismissListener(new QuickAction.OnDismissListener() {
                @Override
                public void onDismiss() {
//				Toast.makeText(activity, "Ups..dismissed", Toast.LENGTH_SHORT).show();
                }
            });

//            womenCount = filteredRowItems.size();
//            allCountView.setText(getResources().getString(R.string.recordcount, womenCount));
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    //    initialize db object
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(getActivity(), DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void updateList(String villageTitle, Map<String, Integer> villageNameForCode, String textViewFilter, boolean isLmpNotconfirmed) {
        try {
            QueryParams queryParams = new QueryParams();
            queryParams.nextItemPosition = 0;
            queryParams.selectedVillageTitle = villageTitle;
            queryParams.textViewFilter = textViewFilter;
            queryParams.isLMPNotConfirmed = isLmpNotconfirmed; //25Mar2021 Bindu
            islmpconfirm = isLmpNotconfirmed; //02May2021 Bindu - set LMP to retrieve in onscroll
            this.wFilterStr = textViewFilter;

            new LoadPregnantList(this, queryParams, true, villageNameForCode).execute();


        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    //    update labels based on data
    private void updateLabels() {
        if (filteredRowItems.size() == 0) {
            recyclerView.setVisibility(View.GONE);

            noDataLabelView.setText(getResources().getString(R.string.no_data));
            noDataLabelView.setVisibility(View.VISIBLE);
            return;
        } else {
            noDataLabelView.setVisibility(View.GONE);
        }
    }

    /**
     * get the Display pojo list from the given womanIds
     *
     * @param limitStart
     * @param villageCode
     * @param filter
     * @param isLMPNotConfirmed
     */
    private List<tblCaseMgmt> prepareRowItemsForDisplay(int limitStart, int villageCode,
                                                        String filter,
                                                        boolean isLMPNotConfirmed, String fromDate, String toDate) {

        try {
            return new CaseMgmtRepository(databaseHelper).getCaseRecords(villageCode,
                    filter, "Woman", limitStart);
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    @Override
    public void onClick(View v) {
        setSelectedWomanId(v);
        mQuickAction.show(v);
    }

    @Override
    public boolean onLongClick(View v) {
        setSelectedWomanId(v);

        Intent intent = new Intent(getActivity(), ViewProfileActivity.class);
        intent.putExtra("globalState", getActivity().getIntent().getBundleExtra("globalState"));
        startActivity(intent);
        return true;
    }

    //    set selected woman id
    private void setSelectedWomanId(View v) {
        int itemPosition = recyclerView.getChildLayoutPosition(v);
        woman = filteredRowItems.get(itemPosition);

        appState.selectedWomanId = woman.getBeneficiaryID();
    }

    //    prepare bundle
    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        return b;
    }

//    @Override
//    public void displayAlert(int selectedItemPosition) {
//        try {
//            ArrayList<String> compList = new ArrayList<>();
//            String complication = filteredRowItems.get(selectedItemPosition).getVisitBeneficiaryDangerSigns();
//            if (complication != null && complication.length() > 0) {
//                compList = new ArrayList<>();
//                compList.add(complication.replace(",", ", ").replace("PH:", "\nPH:"));
//
//
//                ArrayList<String> complListDisp = new ArrayList<>();
//                for (int i = 0; i < compList.size(); i++) {
//                    String compl[] = compList.get(i).split("[,\n]");
//                    for (int j = 0; j < compl.length; j++) {
//                        String strComp = compl[j];
//                        if (strComp.contains("CH:"))
//                            strComp = strComp.replaceAll("CH:", "");
//                        if (strComp.contains("PH:"))
//                            strComp = strComp.replaceAll("PH:", "");
//
//                        strComp = strComp.replaceAll(" ", "");
//                        if (strComp.contains("ormoreconsecutivespontaneousabortions"))
//                            strComp = strComp.split("[<>0123456789/(]")[1];
//                        else
//                            strComp = strComp.split("[<>0123456789/(]")[0];
//                        strComp = strComp.toLowerCase().trim();
//                        int identifier = getResources().getIdentifier
//                                (strComp,
//                                        "string", "com.sc.stmansi");
//                        if (identifier > 0) {
//                            if (compl[j].contains("CH:"))
//                                complListDisp.add("CH: " + getResources().getString(identifier));
//                            else if (compl[j].contains("PH:"))
//                                complListDisp.add("PH: " + getResources().getString(identifier));
//                            else
//                                complListDisp.add(getResources().getString(identifier));
//
//                        } else {
//                           /* if(compl[j].contains("CH:"))
//                            complListDisp.add("CH: "+compl[j]);
//                           else if(compl[j].contains("PH:"))
//                                complListDisp.add("PH: "+compl[j]);
//                            else*/
//                            complListDisp.add(compl[j]);
//
//                        }
//                    }
//
//                }
//
//                ArrayList<String> complications = new ArrayList<>();
//                String strStrings = "";
//                for (int i = 0; i < complListDisp.size(); i++) {
//                    if (i != 0)
//                        strStrings = strStrings + ", " + complListDisp.get(i);
//                    else
//                        strStrings = complListDisp.get(i);
//                }
//
//                String[] list = strStrings.split("PH:");
//
//                ArrayList<String> l = new ArrayList<>();
//                for (int i = 0; i < list.length; i++) {
//                    if (i == 0)
//                        l.add(list[i]);
//                    else
//                        l.add("\nPH: " + list[i]);
//                }
//
//
//                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
//                View convertView = LayoutInflater.from(getActivity()).inflate(R.layout.complicated_reasons_list, null, false);
//
//                ListView lv = convertView.findViewById(R.id.lvCompliatedReasonslist);
//                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.listitem, l);
//
//                alertDialog.setView(convertView).setCancelable(true).setTitle((this.getResources().getString(R.string.compl_reasons)))
//                        .setPositiveButton((this.getResources().getString(R.string.ok)), new DialogInterface.OnClickListener() {
//
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                dialog.cancel();
//                            }
//                        });
//                lv.setAdapter(adapter);
//                alertDialog.show();
//            }
//        } catch (Exception e) {
//            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
//            crashlytics.log(e.getMessage());
//        }
//    }
//
//    @Override
//    public void displayHomeVisitList(int selectedItemPosition) {
//        tblCaseMgmt woman = filteredRowItems.get(selectedItemPosition);
//        Intent homevisitlist = new Intent(getActivity(), HomeVisitListActivity.class);
//        appState.selectedWomanId = woman.getBeneficiaryID();
//        homevisitlist.putExtra("globalState", prepareBundle());
//        startActivity(homevisitlist);
//    }

//    @Override
//    public void displayDelComplicationAlert(int selectedItemPosition) {
//        try {
//            ArrayList<String> complL = new ArrayList<>();
//            if (filteredRowItems.get(selectedItemPosition).getVisitBeneficiaryDangerSigns().length()!=0) {
//                DeliveryRepository deliveryRepository = new DeliveryRepository(databaseHelper);
//                List<TblDeliveryInfo> delData = deliveryRepository.getDeliveryData(filteredRowItems.get(selectedItemPosition).getBeneficiaryID(), filteredRowItems.get(selectedItemPosition).getMMUserId());
//
//                if (delData != null && delData.size() > 0 && delData.get(0)
//                        .getDelMotherComplications() != null && delData.get(0)
//                        .getDelMotherComplications().trim().length() > 0) {
//
//                    String[] chlCompl;
//
//
//                    chlCompl = getResources().getStringArray(R.array.mothercompl);
//
//                    String[] selectedCompl = delData.get(0)
//                            .getDelMotherComplications().split(",");
//
//                    complL = new ArrayList<>();
//
//                    for (int i = 1; i <= selectedCompl.length; i++) {
//                        if (selectedCompl[i - 1] != null && selectedCompl[i - 1].trim().length() > 0)
//                            complL.add(chlCompl[Integer.parseInt(selectedCompl[i - 1].trim())]);
//                    }
//                }
//
//
//                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
//                View convertView = LayoutInflater.from(getActivity()).inflate(R.layout.complicated_reasons_list, null, false);
//
//                ListView lv = convertView.findViewById(R.id.lvCompliatedReasonslist);
//                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.listitem, complL);
//
//                alertDialog.setView(convertView).setCancelable(true).setTitle((this.getResources().getString(R.string.complication_reasons)))
//                        .setPositiveButton((this.getResources().getString(R.string.ok)), new DialogInterface.OnClickListener() {
//
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                dialog.cancel();
//                            }
//                        });
//                lv.setAdapter(adapter);
//                alertDialog.show();
//            }
//        } catch (Exception e) {
//            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
//            crashlytics.log(e.getMessage());
//        }
//    }

    //18Oct2019 - Bindu - Alert message nearing del
    public void displayNearingDelAlertMessage(tblregisteredwomen woman) throws Exception {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

        String alertmsg = woman.getRegWomanName() + ", " + " EDD : " + woman.getRegEDD();
        //alertDialogBuilder.setMessage(message).setCancelable(false).setTitle(Html.fromHtml("<font color='#FF7F27'>Alert -- </font><br><br><p>"+alertmsg + "</p>")).setNegativeButton(
        String msg = "<font color='#CC3333'> Nearing Delivery - </font>" + alertmsg;
        alertDialogBuilder.setMessage(getResources().getText(R.string.alerthomevisitnearingdelmsg)).setCancelable(false).setTitle(Html.fromHtml(msg)).setNegativeButton(
                getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent nextScreen = new Intent(getActivity(), HomeVisit.class);
                        nextScreen.putExtra("globalState", getActivity().getIntent().getBundleExtra("globalState"));
                        nextScreen.putExtra("woman", woman);
                        getActivity().startActivity(nextScreen);
                        mQuickAction.dismiss();
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
//
//    @Override
//    public void displayLMPConfirmation(int selectedItemPosition) {
//        tblCaseMgmt woman = filteredRowItems.get(selectedItemPosition);
//        Intent nextScreen = new Intent(getActivity(), ReconfirmationRegActivity.class);
//        appState.selectedWomanId = woman.getBeneficiaryID();
//        nextScreen.putExtra("globalState", getActivity().getIntent().getBundleExtra("globalState"));
//        getActivity().startActivity(nextScreen);
//    }


    //    load data in a list
    static class LoadPregnantList extends AsyncTask<Void, Void, Void> {

        private WomanFollowUp viewsUpdateCallback;
        private QueryParams queryParams;
        private boolean firstLoad;
        private Map<String, Integer> villageCodeForName;

        LoadPregnantList(WomanFollowUp viewsUpdateCallback, QueryParams queryParams, boolean firstLoad, Map<String, Integer> villageCodeForName) {
            this.viewsUpdateCallback = viewsUpdateCallback;
            this.queryParams = queryParams;
            this.firstLoad = firstLoad;
            this.villageCodeForName = villageCodeForName;
        }

        LoadPregnantList(WomanFollowUp viewsUpdateCallback, QueryParams queryParams, boolean firstLoad) {
            this.viewsUpdateCallback = viewsUpdateCallback;
            this.queryParams = queryParams;
            this.firstLoad = firstLoad;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            int selectedVillageCode;
            // This means second constructor was used (called by scrollListener)
            if (villageCodeForName == null) selectedVillageCode = queryParams.selectedVillageCode;
            else {
                String selectedVillageTitle = queryParams.selectedVillageTitle;
                if (selectedVillageTitle == null)
                    selectedVillageCode = 0;
                else {
                    Integer code = villageCodeForName.get(selectedVillageTitle);
                    selectedVillageCode = (code == null) ? 0 : code;
                }
            }
            viewsUpdateCallback.appState.selectedVillageCode = selectedVillageCode; //10Dec2019 - Bindu
            try {
//                viewsUpdateCallback.womenCount = viewsUpdateCallback.getWomanFollowUpCount(0,
//                        viewsUpdateCallback.womenCondition,viewsUpdateCallback.wFilterStr,new FollowUpRepository(viewsUpdateCallback.databaseHelper),
//                        queryParams.strFromDate,queryParams.strToDate);
                viewsUpdateCallback.womenCount = viewsUpdateCallback.getCaseRecordsCount(new CaseMgmtRepository(viewsUpdateCallback.databaseHelper),
                        selectedVillageCode, queryParams.textViewFilter, "Woman"); // 30April2021 Guru
                List<tblCaseMgmt> rows = viewsUpdateCallback.prepareRowItemsForDisplay(queryParams.nextItemPosition,
                        selectedVillageCode, queryParams.textViewFilter, queryParams.isLMPNotConfirmed, queryParams.strFromDate, queryParams.strToDate); //25Mar2021 Bindu
                if (firstLoad) {
                    int prevSize = viewsUpdateCallback.filteredRowItems.size();
                    viewsUpdateCallback.filteredRowItems.clear();
                    viewsUpdateCallback.registeredWomanAdapter.notifyItemRangeRemoved(0, prevSize);
                    firstLoad = false;
                }
                if (rows.size() > 0) {
//                    viewsUpdateCallback.registeredWomanAdapter.notifyDataSetChanged();
                    viewsUpdateCallback.filteredRowItems.addAll(rows);

                    viewsUpdateCallback.nextItemPosition = viewsUpdateCallback.filteredRowItems.size();
                    viewsUpdateCallback.registeredWomanAdapter.notifyItemRangeInserted(firstLoad ? 0 : viewsUpdateCallback.nextItemPosition,
                            viewsUpdateCallback.filteredRowItems.size());
                }
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
                Log.e("recycler", e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            try {
                viewsUpdateCallback.updateLabels();
                viewsUpdateCallback.registeredWomanAdapter.notifyDataSetChanged();
                // 30April2021 Guru
                if (viewsUpdateCallback.filteredRowItems.size() > 0) {
                    viewsUpdateCallback.recyclerView.setVisibility(View.VISIBLE);
                    if (!firstLoad) {
                        viewsUpdateCallback.wrapManager.scrollToPosition(viewsUpdateCallback.registeredWomanAdapter.getItemCount());
                    }
                    viewsUpdateCallback.isUpdating = false;
                } // 30April2021 Guru
                viewsUpdateCallback.allCountView.setText(viewsUpdateCallback.getResources().
                        getString(R.string.recordcount,
                                viewsUpdateCallback.womenCount)); // 30April2021 Guru
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
        }
    }

    //    recycler view scroll
    public class OnRecyclerScrollListener extends RecyclerView.OnScrollListener {
        int ydy = 0;

        @Override
        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            // 30April2021 Guru
            if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                int lastCompletelyVisibleItemPosition = wrapManager.findLastCompletelyVisibleItemPosition();
                boolean shouldPullUpRefresh = lastCompletelyVisibleItemPosition == filteredRowItems.size() - 1
                        && recyclerView.getScrollState() == RecyclerView.SCROLL_STATE_DRAGGING;
                if (shouldPullUpRefresh) {
                    QueryParams queryParams = new QueryParams();
                    queryParams.nextItemPosition = nextItemPosition;
                    queryParams.selectedVillageCode = appState.selectedVillageCode;
                    queryParams.textViewFilter = wFilterStr;
                    queryParams.isLMPNotConfirmed = islmpconfirm; //02MAy2021 Bindu set isLMPconfirmation
                    if (!isUpdating) {
                        isUpdating = true;
                        new LoadPregnantList(WomanFollowUp.this, queryParams, false).execute();
                    }
                }
            } // 30April2021 Guru
        }

        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            ydy = dy;

            int lastCompletelyVisibleItemPosition = wrapManager.findLastCompletelyVisibleItemPosition();
            boolean shouldPullUpRefresh = lastCompletelyVisibleItemPosition == filteredRowItems.size() - 1
                    && recyclerView.getScrollState() == RecyclerView.SCROLL_STATE_DRAGGING;
            if (shouldPullUpRefresh) {
                QueryParams queryParams = new QueryParams();
                queryParams.nextItemPosition = nextItemPosition;
                queryParams.selectedVillageCode = appState.selectedVillageCode;
                queryParams.textViewFilter = wFilterStr;
                queryParams.isLMPNotConfirmed = islmpconfirm; //02MAy2021 Bindu - set isLMPconfirmation
                if (!isUpdating) {
                    isUpdating = true;
                    new LoadPregnantList(WomanFollowUp.this, queryParams, false).execute();
                }
            }
        }
    }
}

