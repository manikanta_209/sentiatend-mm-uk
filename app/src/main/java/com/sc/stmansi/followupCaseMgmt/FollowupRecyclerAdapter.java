package com.sc.stmansi.followupCaseMgmt;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.sc.stmansi.HomeVisit.HomeVisitActivityByCc;
import com.sc.stmansi.HomeVisit.HomeVisitActivityByMC;
import com.sc.stmansi.R;
import com.sc.stmansi.adolescent.homevisit.AdolVisitViewCC;
import com.sc.stmansi.childhomevisit.ChildHomeVisitView;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.repositories.AdolescentRepository;
import com.sc.stmansi.repositories.AuditRepository;
import com.sc.stmansi.repositories.CaseMgmtRepository;
import com.sc.stmansi.repositories.ChildRepository;
import com.sc.stmansi.repositories.HomeVisitRepository;
import com.sc.stmansi.repositories.TransactionHeaderRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.tables.AuditPojo;
import com.sc.stmansi.tables.TblCaseMgmt_ANCPNCHv;
import com.sc.stmansi.tables.TblChildInfo;
import com.sc.stmansi.tables.TblSymptomMaster;
import com.sc.stmansi.tables.tblCaseMgmt;
import com.sc.stmansi.tables.tblregisteredwomen;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FollowupRecyclerAdapter extends RecyclerView.Adapter<FollowupRecyclerViewHolder> {
    private final List<tblCaseMgmt> caseDetails;
    private final Context act;
    private final DatabaseHelper databaseHelper;
    private final AppState appState;
    private final FollowUpTabsActivity frag;
    private CaseMgmtRepository caseMgmtRepository;
    private int transId;

    public FollowupRecyclerAdapter(List<tblCaseMgmt> filteredRowItems, Context activity, DatabaseHelper databaseHelper, AppState appState, FragmentActivity fragmentActivity) {
        caseDetails = filteredRowItems;
        act = activity;
        this.databaseHelper = databaseHelper;
        this.appState = appState;
        caseMgmtRepository = new CaseMgmtRepository(databaseHelper);
        frag = (FollowUpTabsActivity) fragmentActivity;
    }

    @NonNull
    @Override
    public FollowupRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CardView v = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_followupvisithistory, parent, false);
        return new FollowupRecyclerViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull FollowupRecyclerViewHolder holder, int position) {
        try {
            holder.VisitCount.setText("#" + caseDetails.get(position).getVisitNum() + "");
            holder.VisitDate.setText(caseDetails.get(position).getVisitDate() + "");
//            21Aug2021 Bindu user type display
            holder.txtvisitusertype.setText(act.getResources().getString(R.string.by) + " - " + caseDetails.get(position).getHcmCreatedByUserType());

            if (caseDetails.get(position).getBeneficiaryType().equals("Adol")) {
                AdolescentRepository adolescentRepository = new AdolescentRepository(databaseHelper);
                holder.Name.setText(adolescentRepository.getAdolDatafromAdolID(caseDetails.get(position).getBeneficiaryID()).getRegAdolName());
            } else if (caseDetails.get(position).getBeneficiaryType().equals("Woman")) {
                WomanRepository womanRepository = new WomanRepository(databaseHelper);
                tblregisteredwomen woman = womanRepository.getRegistartionDetails(caseDetails.get(position).getBeneficiaryID(), caseDetails.get(position).getMMUserId());
                holder.Name.setText(woman.getRegWomanName());
                if (caseDetails.get(position).getVisitMode() != null) {
                    if (caseDetails.get(position).getVisitMode().equalsIgnoreCase("ANC")) {
                        holder.imgWomanStatusHv.setVisibility(View.VISIBLE);
                        holder.txtStatus.setVisibility(View.VISIBLE);
                        holder.imgWomanStatusHv.setImageResource(R.drawable.ic_pregnant);
                        holder.txtStatus.setText(act.getResources().getString(R.string.statusatvisit) + " : " + caseDetails.get(position).getStatusAtVisit());
                    } else if (caseDetails.get(position).getVisitMode().equalsIgnoreCase("PNC")) {
                        holder.imgWomanStatusHv.setVisibility(View.VISIBLE);
                        holder.txtStatus.setVisibility(View.VISIBLE);
                        holder.imgWomanStatusHv.setImageResource(R.drawable.ic_mother);
                        holder.txtStatus.setText(act.getResources().getString(R.string.txtDaysSinceDeliveryName) + " : " + caseDetails.get(position).getStatusAtVisit());
                    }
                } else {
                    holder.imgWomanStatusHv.setVisibility(View.GONE);
                    holder.txtStatus.setVisibility(View.GONE);
                }
            } else if (caseDetails.get(position).getBeneficiaryType().equals("Child")) {
                ChildRepository childRepository = new ChildRepository(databaseHelper);
                String childName = childRepository.getChildbyChilID(caseDetails.get(position).getBeneficiaryID()).getChlBabyname();
                holder.Name.setText(childName);
                if (caseDetails.get(position).getVisitMode() != null) {
                    if (caseDetails.get(position).getVisitMode().equalsIgnoreCase("CGM")) {
                        holder.txtStatus.setVisibility(View.VISIBLE);
                        holder.txtStatus.setText(act.getResources().getString(R.string.statusatvisit) + " : " + caseDetails.get(position).getStatusAtVisit());
                    }
                } else {
                    holder.imgWomanStatusHv.setVisibility(View.GONE);
                    holder.txtStatus.setVisibility(View.GONE);
                }
                holder.txtvisitmode.setText("@ "+   caseDetails.get(position).getVisitMode()); //13Aug2021 Bindu
            }


            if (caseDetails.get(position).getBeneficiaryType().equals("Adol")){
                String t = act.getResources().getString(R.string.healthissuetrim)+" @ ";
                if (caseDetails.get(position).getVisitBeneficiaryDangerSigns().length()!=0 && !(caseDetails.get(position).getVisitBeneficiaryDangerSigns().equals(t))){
                    holder.Comp.setVisibility(View.VISIBLE);
                } else {
                    holder.Comp.setVisibility(View.INVISIBLE);
                }
            }else {
                if (caseDetails.get(position).getVisitBeneficiaryDangerSigns().trim().length() != 0) {
                    holder.Comp.setVisibility(View.VISIBLE);
                } else {
                    holder.Comp.setVisibility(View.INVISIBLE);
                }
            }

            /*if (caseDetails.get(position).getVisitReferredtoFacType().length() != 0) {
                holder.Ref.setVisibility(View.VISIBLE);
            } else {
                holder.Ref.setVisibility(View.INVISIBLE);
            }*/ //22Sep2021 Bindu - null pointer exception so added in next cond

            if ((caseDetails.get(position).getVisitReferredtoFacType() != null && caseDetails.get(position).getVisitReferredtoFacType().length() > 0) || (caseDetails.get(position).getVisitReferredtoFacName() != null && caseDetails.get(position).getVisitReferredtoFacName().length() > 0)) {
                //04Jul2021 Bindu - check null
                holder.txtReferredToFacname.setText(act.getResources().getString(R.string.reffacilityname) + caseDetails.get(position).getVisitReferredtoFacType() + " - " + caseDetails.get(position).getVisitReferredtoFacName());
                holder.txtReferredToFacname.setVisibility(View.VISIBLE);
                holder.Ref.setVisibility(View.VISIBLE); //22Sep2021 Bindu add here
            } else {
                holder.txtReferredToFacname.setText("");
                holder.Ref.setVisibility(View.GONE); //22Sep2021 Bindu
            }


            if (caseDetails.get(position).getBeneficiaryType().equals("Woman")) {
                holder.Comp.setOnClickListener(new View.OnClickListener() {
                    private TblCaseMgmt_ANCPNCHv currentWoman;

                    @Override
                    public void onClick(View view) {
                        try {
                            ArrayList<TblCaseMgmt_ANCPNCHv> listofwoman = caseMgmtRepository.getCaseMgmtWomanANCPNCHV_CC(caseDetails.get(position).getBeneficiaryID(), caseDetails.get(position).getMMUserId(), caseDetails.get(position).getHcmCreatedByUserType());
                            for (TblCaseMgmt_ANCPNCHv ss : listofwoman)
                                if (ss.getVisitNum() == caseDetails.get(position).getVisitNum()) {
                                    currentWoman = ss;
                                    break;
                                }
                            if ((caseDetails.get(position).getVisitBeneficiaryDangerSigns() != null && caseDetails.get(position).getVisitBeneficiaryDangerSigns().trim().length() > 0)) {
                                String title = act.getResources().getString(R.string.symptoms);
                                boolean isPnc = false;
                                if (caseDetails.get(position).getVisitMode().equalsIgnoreCase(act.getResources().getString(R.string.strpnc))) {
                                    title = act.getResources().getString(R.string.dangersigns);
                                    isPnc = true;
                                }


                                ArrayList<String> compList = new ArrayList<>();
                                if (caseDetails.get(position).getVisitBeneficiaryDangerSigns() != null && caseDetails.get(position).getVisitBeneficiaryDangerSigns().length() > 0) {
                                    compList.add(setWomaCompl(position));
                                }
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(act);
                                View convertView = LayoutInflater.from(act).inflate(R.layout.custom_hv_dialog, null, false);
                                TextView txtmotherdangersigns = convertView.findViewById(R.id.txtmotherdangersignval);
                                TextView txtnbdangersigns = convertView.findViewById(R.id.txtnbdangersignval);
                                LinearLayout llnbdangersigns = convertView.findViewById(R.id.llnbds);
                                LinearLayout llwomandangersigns = convertView.findViewById(R.id.llmotherds);

                                if (caseDetails.get(position).getVisitBeneficiaryDangerSigns() != null && caseDetails.get(position).getVisitBeneficiaryDangerSigns().length() > 0) {
                                    txtmotherdangersigns.setText(setWomaCompl(position));//15May2021 Arpitha
                                    llwomandangersigns.setVisibility(View.VISIBLE);
                                } else {
                                    llwomandangersigns.setVisibility(View.GONE);
                                }

                                if (isPnc) {

                                    String babyDangerSign = "";
                                    //20Jul2021 Bindu
                                    if(currentWoman.getChildDangersigns() != null && currentWoman.getChildDangersigns().length() > 0) {
                                        babyDangerSign = act.getResources().getString(R.string.baby1)+ " - " + setChildCompl(currentWoman.getChildDangersigns());
                                    }
                                    if(currentWoman.getChildDangersignsSec() != null && currentWoman.getChildDangersignsSec().length() > 0) {
                                        babyDangerSign = babyDangerSign + "\n" + act.getResources().getString(R.string.baby2)+ " - " + setChildCompl(currentWoman.getChildDangersignsSec());

                                    }
                                    if(currentWoman.getChildDangersignsThird() != null && currentWoman.getChildDangersignsThird().length() > 0) {
                                        babyDangerSign = babyDangerSign + "\n" +act.getResources().getString(R.string.baby3)+ " - " + setChildCompl(currentWoman.getChildDangersignsThird());

                                    }
                                    if(currentWoman.getChildDangersignsFourth() != null && currentWoman.getChildDangersignsFourth().length() > 0) {
                                        babyDangerSign = babyDangerSign + "\n" +act.getResources().getString(R.string.baby4)+ " - " + setChildCompl(currentWoman.getChildDangersignsFourth());

                                    }
                                    if(currentWoman.getChildDangersignsOthers() != null && currentWoman.getChildDangersignsOthers().length() > 0) {
                                        babyDangerSign = babyDangerSign + "\n" + currentWoman.getChildDangersignsOthers();
                                    }
                                    txtnbdangersigns.setText(babyDangerSign);
                                    llnbdangersigns.setVisibility(View.VISIBLE);
                                } else {
                                    llnbdangersigns.setVisibility(View.GONE);
                                }

                                alertDialog.setView(convertView).setCancelable(true).setTitle(title)
                                        .setPositiveButton((act.getResources().getString(R.string.ok)), new DialogInterface.OnClickListener() {

                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                            }
                                        });
                                alertDialog.show();
                            }
                        } catch (Exception e) {
                            FirebaseCrashlytics firebaseCrashlytics = FirebaseCrashlytics.getInstance();
                            firebaseCrashlytics.log(e.getMessage());
                            e.printStackTrace();
                        }
                    }
                });
            } else if (caseDetails.get(position).getBeneficiaryType().equals("Adol")) {
                holder.Comp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(act);
                        View complicationView = View.inflate(act, R.layout.customdialogavh_complication, null);
                        builder.setView(complicationView);
                        builder.setCancelable(false);
                        builder.setPositiveButton(act.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).create().show();

                        TextView txtCHIHead = complicationView.findViewById(R.id.txtcomplicationHealthIssueHead);
                        TextView txtCHIBody = complicationView.findViewById(R.id.txtcomplicationHealthIssueBody);
                        TextView txtCMPHead = complicationView.findViewById(R.id.txtcomplicationMenstrualproblemsHead);
                        TextView txtCMPBody = complicationView.findViewById(R.id.txtcomplicationMenstrualproblemsBody);
                        TextView txtHBHead = complicationView.findViewById(R.id.txtcomplicationHBHead);
                        TextView txtHBBody = complicationView.findViewById(R.id.txtcomplicationHBBody);
                        TextView txtWeightHead = complicationView.findViewById(R.id.txtcomplicationWeightHead);
                        TextView txtWeightBody = complicationView.findViewById(R.id.txtcomplicationWeightBody);
                        TextView txtHeightHead = complicationView.findViewById(R.id.txtcomplicationHeightHead);
                        TextView txtHeightBody = complicationView.findViewById(R.id.txtcomplicationHeightBody);

                        String complications = caseDetails.get(position).getVisitBeneficiaryDangerSigns();
                        String[] listofcomplications = complications.split("@");
                        for (String s : listofcomplications) {
                            if (s.contains("Health")) {
                                txtCHIHead.setVisibility(View.VISIBLE);
                                String hit = act.getResources().getString(R.string.healthissuetrim);
                                String hi = s.replace(hit, "");
                                String[] hidata = hi.split("  ");
                                StringBuilder hiBuilder = new StringBuilder();
                                for (String word : hidata) {
                                    word = word.replace("Anemia", act.getResources().getString(R.string.anemia));
                                    hiBuilder.append(word).append(", ");
                                }
                                if (hiBuilder.toString().contains(",")){
                                    txtCHIBody.setText(hiBuilder.deleteCharAt(hiBuilder.lastIndexOf(",")));
                                }else {
                                    txtCHIBody.setText(hiBuilder.toString());
                                }
                                txtCHIBody.setVisibility(View.VISIBLE);
                            }
                            if (s.contains("hb")) {
                                txtHBHead.setVisibility(View.VISIBLE);
                                String hbtemp = s.replace("hb is ", "");
                                txtHBBody.setText(s.replace("hb is ", "") + " " + act.getResources().getString(R.string.hemoglobincount) + "( " + getHGCategory(Float.parseFloat(hbtemp)) + ")");
                                txtHBBody.setVisibility(View.VISIBLE);
                            }
                            if (s.contains("Weight")) {
                                txtWeightHead.setVisibility(View.VISIBLE);
                                txtWeightBody.setText(s.replace("Weight is", ""));
                                txtWeightBody.setVisibility(View.VISIBLE);
                            }
                            if (s.contains("Height")) {
                                txtHeightHead.setVisibility(View.VISIBLE);
                                txtHeightBody.setText(s.replace("Height is ", ""));
                                txtHeightBody.setVisibility(View.VISIBLE);
                            }
                            if (s.contains("Menstrual")) {
                                txtCMPHead.setVisibility(View.VISIBLE);
                                String hi = s.trim().replace("Menstrualpbm ", "");
                                String[] hidata = hi.split("  ");
                                StringBuilder hiBuilder = new StringBuilder();
                                String[] mensuproblems = act.getResources().getStringArray(R.array.menustralproblems);
                                for (String word : hidata) {
                                    try{
                                        int intWord = Integer.parseInt(word.trim());
                                        hiBuilder.append(mensuproblems[intWord]).append(", ");
                                    }catch (Exception e){
                                        hiBuilder.append(word).append(",");
                                    }
                                }
                                if (hiBuilder.toString().contains(",")){
                                    txtCMPBody.setText(hiBuilder.deleteCharAt(hiBuilder.lastIndexOf(",")));
                                }else {
                                    txtCMPBody.setText(hiBuilder.toString());
                                }
                                txtCMPBody.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                });
            } else if (caseDetails.get(position).getBeneficiaryType().equals("Child")) {
                holder.Comp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            if ((caseDetails.get(position).getVisitBeneficiaryDangerSigns() != null && caseDetails.get(position).getVisitBeneficiaryDangerSigns().length() > 0)) {
                                String title = act.getResources().getString(R.string.complications);

                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(act);
                                View convertView = LayoutInflater.from(act).inflate(R.layout.customchildhv_dialog, null, false);
                                TextView txtchilddangersigns = convertView.findViewById(R.id.tvdialogchlhv);

                                txtchilddangersigns.setText(caseDetails.get(position).getVisitBeneficiaryDangerSigns());

                                alertDialog.setView(convertView).setCancelable(true).setTitle(title)
                                        .setPositiveButton((act.getResources().getString(R.string.ok)), (dialog, which) -> dialog.cancel());
                                // lv.setAdapter(adapter);
                                alertDialog.show();
                            }
                        } catch (Exception e) {
                            FirebaseCrashlytics firebaseCrashlytics = FirebaseCrashlytics.getInstance();
                            firebaseCrashlytics.log(e.getMessage());
                        }
                    }
                });
            }
            holder.View.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {

                        Intent intent = null;
                        if (caseDetails.get(position).getBeneficiaryType().equals("Adol")) {
                            intent = new Intent(act, AdolVisitViewCC.class);
                            intent.putExtra("globalState", prepareBundle());
                            intent.putExtra("VisitCount", caseDetails.get(position).getVisitNum() + "");
                            intent.putExtra("adolID", caseDetails.get(position).getBeneficiaryID());
                            intent.putExtra("fromNotification", true);
                            intent.putExtra("userTypeHV", caseDetails.get(position).getHcmCreatedByUserType());
                            AsyncUpdateViewedDate asyncUpdateViewedDate = new AsyncUpdateViewedDate();
                            asyncUpdateViewedDate.execute(caseDetails.get(position));
                            act.startActivity(intent);
                        } else if (caseDetails.get(position).getBeneficiaryType().equals("Woman")) {
                            appState.selectedWomanId = caseDetails.get(position).getBeneficiaryID();
                            ArrayList<TblCaseMgmt_ANCPNCHv> listofwoman = caseMgmtRepository.getCaseMgmtWomanANCPNCHV_CC(caseDetails.get(position).getBeneficiaryID(), caseDetails.get(position).getMMUserId(), caseDetails.get(position).getHcmCreatedByUserType());
                            for (int i = 0; i < listofwoman.size(); i++) {
                                if ((listofwoman.get(i).getBeneficiaryID().equals(caseDetails.get(position).getBeneficiaryID()) || listofwoman.get(i).getBeneficiaryParentID().equals(caseDetails.get(position).getBeneficiaryID())) &&
                                        listofwoman.get(i).getVisitNum() == caseDetails.get(position).getVisitNum()) {
                                    if(caseDetails.get(position).getHcmCreatedByUserType().equalsIgnoreCase(act.getResources().getString(R.string.ccusertype)))
                                        intent = new Intent(act, HomeVisitActivityByCc.class);
                                    else if(caseDetails.get(position).getHcmCreatedByUserType().equalsIgnoreCase(act.getResources().getString(R.string.mcusertype)))
                                        intent = new Intent(act, HomeVisitActivityByMC.class);
                                    intent.putExtra("globalState", prepareBundle());
                                    intent.putExtra("pos", i);
                                    intent.putExtra("fromNotification", true);
                                    AsyncUpdateViewedDate asyncUpdateViewedDate = new AsyncUpdateViewedDate();
                                    asyncUpdateViewedDate.execute(caseDetails.get(position));
                                    act.startActivity(intent);
                                }
                            }
                        } else if (caseDetails.get(position).getBeneficiaryType().equals("Child")) {
                            ChildRepository childRepository = new ChildRepository(databaseHelper);
                            caseMgmtRepository = new CaseMgmtRepository(databaseHelper);
                            List<tblCaseMgmt> listofchild = caseMgmtRepository.getChildHomeVisitHistory(caseDetails.get(position).getBeneficiaryID(), caseDetails.get(position).getHcmCreatedByUserType()); // do not change usertype - retrieved based on table data
                            for (int i = 0; i < listofchild.size(); i++) {
                                if (listofchild.get(i).getBeneficiaryID().equals(caseDetails.get(position).getBeneficiaryID()) &&
                                        (listofchild.get(i).getVisitNum()) == caseDetails.get(position).getVisitNum()) {
                                    intent = new Intent(act, ChildHomeVisitView.class);
                                    intent.putExtra("globalState", prepareBundle());
                                    intent.putExtra("pos", i);
                                    intent.putExtra("childId", caseDetails.get(position).getBeneficiaryID());
                                    TblChildInfo tblChildinfoo = childRepository.getChildbyChilID(caseDetails.get(position).getBeneficiaryID());
                                    intent.putExtra("tblChildInfo", tblChildinfoo);
                                    intent.putExtra("usertype", caseDetails.get(position).getHcmCreatedByUserType());
                                    intent.putExtra("fromNotification", true);
                                    AsyncUpdateViewedDate asyncUpdateViewedDate = new AsyncUpdateViewedDate();
                                    asyncUpdateViewedDate.execute(caseDetails.get(position));
                                    act.startActivity(intent);
                                }
                            }
                        }

                    } catch (Exception e) {
                        FirebaseCrashlytics firebaseCrashlytics = FirebaseCrashlytics.getInstance();
                        firebaseCrashlytics.log(e.getMessage());
                    }
                }
            });
        } catch (Exception e) {
            FirebaseCrashlytics firebaseCrashlytics = FirebaseCrashlytics.getInstance();
            firebaseCrashlytics.log(e.getMessage());
        }
    }

    @Override
    public int getItemCount() {
        return caseDetails.size();
    }

    private String setWomaCompl(int pos) throws SQLException {
        HomeVisitRepository homeVisitRepository = new HomeVisitRepository(databaseHelper);

        ArrayList<String> list = new ArrayList<>();
        String symptom = "";
        if (caseDetails.get(pos).getVisitBeneficiaryDangerSigns() != null && caseDetails.get(pos).getVisitBeneficiaryDangerSigns().length() > 0) {
            list = new ArrayList<String>(Arrays.asList(caseDetails.get(pos).getVisitBeneficiaryDangerSigns().split("\\,")));

            if (list != null && list.size() > 0) {

                for (int i = 0; i < list.size(); i++) {
                    String strSy = list.get(i);
                    int identifier = act.getResources().getIdentifier
                            (strSy,
                                    "string", act.getPackageName()); //25Jun2021 Bindu get package name from app
                    if (identifier > 0) {
                        if (i == list.size() - 1)
                            symptom = symptom + act.getResources().getString(identifier);
                        else
                            symptom = symptom + act.getResources().getString(identifier) + ",";
                    } else {
                        if (i == list.size() - 1)
                            symptom = symptom + strSy;
                        else
                            symptom = symptom + strSy + ",";
                    }

                }
            }
        }
        return symptom;
    }

    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        return b;
    }

    private class AsyncUpdateViewedDate extends AsyncTask<tblCaseMgmt, Void, Void> {

        @Override
        protected Void doInBackground(tblCaseMgmt... tblCaseMgmts) {
            try {
                tblCaseMgmt caseMgmt = tblCaseMgmts[0];
                transId = TransactionHeaderRepository.iCreateNewTransNew(caseMgmt.getMMUserId(), databaseHelper);
                String currentDate = DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime();
                CaseMgmtRepository caseMgmtRepository = new CaseMgmtRepository(databaseHelper);
                String sql = " update tblCaseMgmt set RecordViewedDate = '" + currentDate + "' , RecordUpdatedDate = '" + DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime() + "' where  UserId ='" + caseMgmt.getUserId() + "' and MMUserId = '" + caseMgmt.getMMUserId() + "' and BeneficiaryID = '" + caseMgmt.getBeneficiaryID() + "' and VisitMode = '" + caseMgmt.getVisitMode() + "' and VisitNum = " + caseMgmt.getVisitNum() + "";
                if (caseMgmtRepository.updateViewDate(sql) > 0) {
                    TransactionHeaderRepository.iUpdateRecordTransNew(caseMgmt.getUserId(), transId, "tblCaseMgmt", sql, null, null, databaseHelper);
                    InserttblAuditTrail("RecordViewedDate", null, currentDate, transId, caseMgmt);
                    InserttblAuditTrail("RecordUpdatedDate", caseMgmt.getRecordUpdatedDate(), DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime(), transId, caseMgmt);
                    Log.e("RecordViewedDate", "Updated " + caseMgmt.getAutoId() + " " + currentDate);
                } else {
                    Log.e("RecordViewedDate", "Not Updated " + caseMgmt.getAutoId() + " " + currentDate);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            frag.RefreshData();
        }
    }

    private void InserttblAuditTrail(String ColumnName, String Old_Value, String
            New_Value, int transId, tblCaseMgmt casemgmt) throws Exception {

        AuditPojo tblAudit = new AuditPojo();
        tblAudit.setWomanId(casemgmt.getBeneficiaryID());
        tblAudit.setUserId(casemgmt.getMMUserId());
        tblAudit.setTblName("tblCaseMgmt");
        tblAudit.setColumnName(ColumnName);
        tblAudit.setDateChanged(DateTimeUtil.getTodaysDate());
        tblAudit.setOld_Value(Old_Value);
        tblAudit.setNew_Value(New_Value);
        tblAudit.setTransId(transId);
        tblAudit.setRecordcreateddate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
        AuditRepository auditRepository = new AuditRepository(databaseHelper);
        auditRepository.insertTotblAuditTrail(tblAudit);
    }

    private String getHGCategory(float hbCount) {
        try {
//            float hbCount = Float.parseFloat(aqAdolVisit.id(R.id.etAdolVisitHbCount).getEditText().getText().toString());
            if (hbCount < 8.0) {
                //severe
                return act.getResources().getString(R.string.severe);
            } else if (hbCount >= 8 && hbCount <= 9.9) {
                //moderate
                return act.getResources().getString(R.string.moderate);
            } else if (hbCount >= 10 && hbCount <= 11.9) {
                //mild
                return act.getResources().getString(R.string.mild);
            } else if (hbCount >= 11.9 && hbCount <= 24) {
                //normal
                return act.getResources().getString(R.string.normal);
            } else if (hbCount > 24) {
                return "NA";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
    private String setChildCompl(String dangersign) throws SQLException {

        ArrayList<String> symptoms = new ArrayList<>();
        ArrayList<String> list = new ArrayList<>();
        String symptom = "";
        if (dangersign != null && dangersign.length() > 0) {
            list = new ArrayList<String>(Arrays.asList(dangersign.split(",")));

            if (list != null && list.size() > 0) {

                for (int i = 0; i < list.size(); i++) {
                    String strSy = list.get(i);

                    List<TblSymptomMaster> symptomMasterList = databaseHelper.getTblSymptomMasterDao().queryBuilder()
                            .selectColumns("symXmlStringName").where()
                            .eq("symSymptomName", strSy).query();

                    if (symptomMasterList != null && symptomMasterList.size() > 0) {
                        symptoms.add(symptomMasterList.get(0).getSymXmlStringName());
                    } else {
                        if (strSy.toLowerCase().trim().contains("grunting")) //21Jul2021 Bindu manually add
                            symptoms.add("nbfastbreathing");
                        if (strSy.toLowerCase().trim().contains("diarrhea")) //21Jul2021 Bindu manually add
                            symptoms.add("nbdiarrhoea");
                    }

                }

                if(symptoms != null) {
                    String strSy = "";
                    for(int i=0; i< symptoms.size();i++) {
                        strSy = symptoms.get(i);
                        strSy = strSy.replaceAll(" ", "");
                        strSy = strSy.toLowerCase().trim();
                        int identifier = act.getResources().getIdentifier
                                (strSy,
                                        "string", act.getPackageName()); //25Jun2021 Bindu get package name from app
                        if (identifier > 0) {
                            if (i == list.size() - 1)
                                symptom = symptom + act.getResources().getString(identifier);
                            else
                                symptom = symptom + act.getResources().getString(identifier) + ",";
                        } else {
                            if (i == list.size() - 1)
                                symptom = symptom + strSy;
                            else
                                symptom = symptom + strSy + ",";
                        }
                    }
                }
            }
        }return symptom;
    }
}
