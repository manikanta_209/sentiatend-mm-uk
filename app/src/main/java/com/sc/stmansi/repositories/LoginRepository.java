package com.sc.stmansi.repositories;

import com.j256.ormlite.stmt.UpdateBuilder;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.tables.TblLoginAudit;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LoginRepository {

	private DatabaseHelper databaseHelper;

	public LoginRepository(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	public void updateLogoutTime(int loginAuditId, String userId) throws Exception {
		TblLoginAudit loginAudit = new TblLoginAudit();
		loginAudit.setLogouttime(DateTimeUtil.getTodaysDate()
				+ " " + DateTimeUtil.getCurrentTime());
		loginAudit.setUpdateddate(DateTimeUtil.getTodaysDate()
				+ " " + DateTimeUtil.getCurrentTime());

		UpdateBuilder<TblLoginAudit, Integer> updateBuilder = this
				.databaseHelper.getLoginAuditRuntimeExceptionDao().updateBuilder();
		updateBuilder.updateColumnValue("logouttime",
				loginAudit.getLogouttime())
				.updateColumnValue("updateddate", loginAudit.getUpdateddate()).where()
				.eq("autoId", loginAuditId);

		updateBuilder.update();

		String upSql = updateBuilder.prepare().toString().replace("MappedStatement: ","");

		TransactionHeaderRepository transactionHeaderRepository = new TransactionHeaderRepository(databaseHelper);


		int transId = transactionHeaderRepository.iCreateNewTrans(userId,databaseHelper);


		transactionHeaderRepository.updateNew(userId, transId,
				databaseHelper.getLoginAuditRuntimeExceptionDao().getTableName(),
				upSql,null, null,databaseHelper);


	}

	public List<String> getAnmIds() throws SQLException {
		List<TblLoginAudit> log = this.databaseHelper.getLoginAuditRuntimeExceptionDao().queryBuilder().distinct()
				.selectColumns("userId").query();

		List<String> list = new ArrayList<>();
		for (TblLoginAudit login : log) {
			list.add(login.getUserId());
		}
		return list;
	}

	public int updateLoginAudit(TblLoginAudit tblloginAudit) throws Exception {
		int insert = this.databaseHelper.getLoginAuditRuntimeExceptionDao().create(tblloginAudit);
		if (insert > 0) {
			List<TblLoginAudit> loginAuditList = this.databaseHelper.getLoginAuditRuntimeExceptionDao().queryBuilder()
					.selectColumns("autoId")
					.orderBy("autoId", false)
					.query();
			return loginAuditList.get(0).getAutoId();
		}
		return 0;
	}

	public int updateLatandLong(int loginAuditId, String varLatitude, String varLongitude) throws  Exception{
		UpdateBuilder<TblLoginAudit, Integer> updatebuilder = this.databaseHelper.getLoginAuditRuntimeExceptionDao().updateBuilder();
		updatebuilder.updateColumnValue("latitude",varLatitude).updateColumnValue("longitude",varLongitude)
				.where().eq("autoId",loginAuditId);
		int update = updatebuilder.update();
		return update;
	}
}
