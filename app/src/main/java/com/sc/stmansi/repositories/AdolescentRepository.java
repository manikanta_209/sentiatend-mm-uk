package com.sc.stmansi.repositories;

import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.tables.tblAdolReg;
import com.sc.stmansi.tables.tblAdolVisitHeader;
import com.sc.stmansi.tables.tblCampDetails;
import com.sc.stmansi.tables.tblregisteredwomen;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AdolescentRepository {
    private DatabaseHelper databaseHelper;
    String[] columns = {"AdolID",
            "regAdolName",
            "regAdolAge",
            "regAdolGender",
            "regAdolDoB",
            "regAdolWeight",
            "regAdolHeightType",
            "regAdolHeight",
            "regAdolMobile",
            "regAdolMobileOf",
            "regAdolFather",
            "regAdolFatherDesc",
            "regAdolMother",
            "regAdolMotherDesc",
            "regAdolGuardian",
            "regAdolFamilytype",
            "regAdolPregnant",
            "regAdolAddress",
            "regAdolAreaname",
            "regAdolFacilities",
            "regAdolPincode",
            "regAdolRelationship",
            "regAdolPartnerName",
            "regAdolPartnerOccupa",
            "regAdolMarriageType",
            "regAdolGoingtoSchool",
            "regAdolNoofChild",
            "regAdolOtherHealthIssues",
            "regAdolSuggestionGiven",
            "regAdolComments",
            "LastVisitCount",
            "regAdolAadharNo",
            "regAdolisPeriod",
            "regAdolAgeatMenstruated",
            "regAdolEducation",
            "RecordCreatedDate",
            "regAdolDeactivateDate",
            "regAdolDeactivateReasons",
            "regAdolDeactivateReasonOthers",
            "regAdolDeactivateComments",
            "RecordUpdatedDate",
            "regAdolHealthIssues",
            "regAdolLifeStyle",
            "regAdolMenstrualPbm",
            "regAdolOtherMenspbm",
            "regAdolPhysicalActivity"

    };

    public AdolescentRepository(DatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }


    public int insertData(tblAdolReg tblAdolReg) throws Exception {

        return this.databaseHelper.gettblAdolregRuntimeExceptionDao().create(tblAdolReg);
    }

    public List<tblAdolReg> getallAdols() throws Exception {
        QueryBuilder<tblAdolReg, Integer> queryBuilder = databaseHelper.gettblAdolregRuntimeExceptionDao().queryBuilder().
                selectColumns(columns).orderBy("RecordCreatedDate", false);
        Where<tblAdolReg, Integer> where = queryBuilder.where().eq("regAdolDeactivateDate", "");
        return where.query();
    }

    public tblAdolReg getAdolDatafromAdolID(String adolID) throws Exception {
        List<tblAdolReg> queryBuilder = databaseHelper.gettblAdolregRuntimeExceptionDao().queryBuilder().
                selectColumns(columns).where().eq("AdolID", adolID).query();
        return queryBuilder.get(0);
    }

    public String getnewAdolId() throws Exception {
        RuntimeExceptionDao<tblAdolReg, Integer> usersDAO = databaseHelper.gettblAdolregRuntimeExceptionDao();
        String sql = "select MAX(AdolID) from tblAdolReg";
        GenericRawResults<String[]> rawResults = databaseHelper.gettblAdolregRuntimeExceptionDao().queryRaw(sql);
        List<String[]> results = rawResults.getResults();
        String[] resultArray = results.get(0);
        String finalResult = resultArray[0];
        if (finalResult != null) {
            int lastParentno = Integer.parseInt(finalResult);
            lastParentno = lastParentno + 1;

            String returnValue;
            if (lastParentno <= 9) {
                returnValue = "000" + lastParentno;
            } else if (lastParentno <= 99) {
                returnValue = "00" + lastParentno;
            } else if (lastParentno <= 999) {
                returnValue = "0" + lastParentno;
            } else {
                returnValue = String.valueOf(lastParentno);
            }
            return returnValue;
        } else {
            return "0001";
        }
    }


    public int updateData(String sql) {
        try {
            RuntimeExceptionDao<tblAdolReg, Integer> adolDAO = databaseHelper.gettblAdolregRuntimeExceptionDao();
            return adolDAO.updateRaw(sql);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public List<tblAdolReg> getAdolsbyFacility(String tblCampFacilities) {
        try {
            if (tblCampFacilities.contains(",")) {
                String[] facilitiesIndexs = tblCampFacilities.split(",");
                List<tblAdolReg> list = new ArrayList<>();
                for (String s : facilitiesIndexs) {
                    s = s.trim();
                    List<tblAdolReg> queryBuilder = databaseHelper.gettblAdolregRuntimeExceptionDao().queryBuilder().
                            selectColumns(columns).orderBy("RecordCreatedDate", false).where().eq("regAdolFacilities", Integer.parseInt(s))
                            .and().eq("regAdolDeactivateDate", "").query();
                    list.addAll(queryBuilder);
                }
                return list;
            } else {
                return databaseHelper.gettblAdolregRuntimeExceptionDao().queryBuilder().
                        selectColumns(columns).orderBy("RecordCreatedDate", false).where().eq("regAdolFacilities", Integer.parseInt(tblCampFacilities))
                        .and().eq("regAdolDeactivateDate", "").query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<tblAdolReg> getDeactivatedAdolsbyFacility(String tblCampFacilities) {
        try {
            if (tblCampFacilities.contains(",")) {
                String[] facilitiesIndexs = tblCampFacilities.split(",");
                List<tblAdolReg> list = new ArrayList<>();
                for (String s : facilitiesIndexs) {
                    s = s.trim();
                    List<tblAdolReg> queryBuilder = databaseHelper.gettblAdolregRuntimeExceptionDao().queryBuilder().
                            selectColumns(columns).orderBy("RecordCreatedDate", false).where().eq("regAdolFacilities", Integer.parseInt(s))
                            .and().not().eq("regAdolDeactivateDate", "").query();
                    list.addAll(queryBuilder);
                }
                return list;
            } else {
                return databaseHelper.gettblAdolregRuntimeExceptionDao().queryBuilder().
                        selectColumns(columns).orderBy("RecordCreatedDate", false).where().eq("regAdolFacilities", Integer.parseInt(tblCampFacilities))
                        .and().not().eq("regAdolDeactivateDate", "").query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public int updateVisitCount(String sql) {

        RuntimeExceptionDao<tblAdolReg, Integer> adolDAO = databaseHelper.gettblAdolregRuntimeExceptionDao();
        return adolDAO.updateRaw(sql);
    }

    public boolean checkMobileNoExists(String mobileNo, String AdolId) { //ramesh 11-may 2021
        try {
            List<tblAdolReg> queryBuilder = databaseHelper.gettblAdolregRuntimeExceptionDao().queryBuilder().
                    selectColumns(columns).where().eq("regAdolMobile", mobileNo).and().eq("regAdolDeactivateDate", "")
                    .and().not().eq("AdolID", AdolId).query();
            if (queryBuilder.size() != 0) {
                return true;   //if mobile exists
            } else {
                return false; //if nmobile no exists
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public List<tblAdolReg> getallDeactivaedAdols() throws SQLException {
        QueryBuilder<tblAdolReg, Integer> queryBuilder = databaseHelper.gettblAdolregRuntimeExceptionDao().queryBuilder().
                selectColumns(columns).orderBy("RecordCreatedDate", false);
        Where<tblAdolReg, Integer> where = queryBuilder.where().not().eq("regAdolDeactivateDate", "");
        return where.query();
    }

    //    19May2021 Arpitha
    public List<tblAdolReg> getRegisteredAdols(int villageCode, long limitStart, boolean all, String filter,String search) throws SQLException {

        long limit = 10;
        boolean includeVillageCode = villageCode != 0;

        QueryBuilder<tblAdolReg, Integer> queryBuilder = databaseHelper.gettblAdolregRuntimeExceptionDao()
                .queryBuilder()
                .orderBy("transId", false)
                .offset(limitStart)
                .limit(limit);

        Where<tblAdolReg, Integer> where = queryBuilder.where();


        if (includeVillageCode) {
            where = where.eq("regAdolFacilities", villageCode);
        }

        if (includeVillageCode) {
            if (all)
                where = where.and().eq("regAdolDeactivateDate", "");
            else
                where = where.and().ne("regAdolDeactivateDate", "");
        } else {
            if (all)
                where = where.eq("regAdolDeactivateDate", "");
            else
                where = where.ne("regAdolDeactivateDate", "");
        }


        if (filter.trim().length() > 0) {
            if (filter.equalsIgnoreCase("Male"))
                where = where.and().eq("regAdolGender", filter);
            else if (filter.equalsIgnoreCase("Female"))
                where = where.and().eq("regAdolGender", filter);

        }

        //ramesh 28/5/2021 added search by name in the query
        if (search.trim().length() > 0) {
            where = where.and (where,where.like("regAdolName", "%" + search + "%"));
        }
        String st  = queryBuilder.prepare().toString();

        queryBuilder.setWhere(where);
        List<tblAdolReg> allAdols = queryBuilder.query();
        AdolVisitHeaderRepository adolVisitHeaderRepository = new AdolVisitHeaderRepository(databaseHelper);
        for (tblAdolReg s: allAdols){
            if (s.getLastVisitCount()!=0){
                tblAdolVisitHeader localAdolVisitHeader = adolVisitHeaderRepository.getVisitbyID(String.valueOf(s.getLastVisitCount()), s.getAdolID());
                s.setRegAdolGoingtoSchool(localAdolVisitHeader.getAdolvisHGTS());
            }
        }
        return allAdols;
    }

    //    19May2021 Arpitha
    public long getRegisteredAdolsCount(int villageCode, boolean all, String filter,String search) throws SQLException {

        long limit = 10;
        boolean includeVillageCode = villageCode != 0;

        QueryBuilder<tblAdolReg, Integer> queryBuilder = databaseHelper.gettblAdolregRuntimeExceptionDao()
                .queryBuilder()
                .orderBy("transId", false);

        Where<tblAdolReg, Integer> where = queryBuilder.where();


        if (includeVillageCode) {
            where = where.eq("regAdolFacilities", villageCode);
        }

        if (includeVillageCode) {
            if (all)
                where = where.and().eq("regAdolDeactivateDate", "");
            else
                where = where.and().ne("regAdolDeactivateDate", "");
        } else {
            if (all)
                where = where.eq("regAdolDeactivateDate", "");
            else
                where = where.ne("regAdolDeactivateDate", "");
        }

        if (filter.trim().length() > 0) {
            if (filter.equalsIgnoreCase("Male"))
                where = where.and().eq("regAdolGender", filter);
            else if (filter.equalsIgnoreCase("Female"))
                where = where.and().eq("regAdolGender", filter);
        }

        if (search.trim().length() > 0) {
            where = where.and (where,where.like("regAdolName", "%" + search + "%"));
        }

        queryBuilder.setWhere(where);
        // String s = queryBuilder.prepare().toString();
        return queryBuilder.countOf();

    }


    public boolean checkAadharNoExists(String toString) throws SQLException{
        List<tblAdolReg> queryBuilder = databaseHelper.gettblAdolregRuntimeExceptionDao().queryBuilder().
                selectColumns(columns).where().eq("regAdolAadharNo", toString).and().eq("regAdolDeactivateDate", "").query();
        if (queryBuilder.size() != 0) {
            return true;   //if mobile exists
        } else {
            return false; //if nmobile no exists
        }
    }

//  01Sep2021 Arpitha - get adol details based on adhar number
    public tblAdolReg getAdolDatafromAdarId(String adharID) throws Exception {
        String[] columns = {"regAdolName","regAdolAge","regAdolDoB","regAdolWeight","regAdolHeightType",
                "regAdolHeight"	,
                "regAdolMobile"	,
                "regAdolMobileOf","regAdolAddress"	,
                "regAdolAreaname","regAdolFacilities"};
        List<tblAdolReg> queryBuilder = databaseHelper.gettblAdolregRuntimeExceptionDao()
                .queryBuilder().
                selectColumns(columns).where().eq("regAdolAadharNo", adharID).query();
        return queryBuilder.get(0);
    }

//    02Sep2021 Arpitha
    public boolean isAdolANC(String adharNo, String adolId) throws SQLException {
        boolean isAdolANC = false;
      long regList =  databaseHelper.gettblregRuntimeExceptionDao().queryBuilder()
                .where().eq("regAadharCard",adharNo).and().isNull("DateDeactivated")
              .and().eq("regIsAdolescent","Yes")
              .and().eq("regAdolId",adolId).countOf();
      if(regList > 0)
          isAdolANC = true;

      return isAdolANC;

    }
    //    02Sep2021 Arpitha
    public String getAdolANCWomanId(String adharNo, String adolId) throws SQLException {
        String adolANCWomanId = "";
        List<tblregisteredwomen> regList =  databaseHelper.gettblregRuntimeExceptionDao().queryBuilder()
                .where().eq("regAadharCard",adharNo).and().isNull("DateDeactivated")
                .and().eq("regAdolId",adolId).query();
        if(regList!=null && regList.size() > 0)
            adolANCWomanId = regList.get(0).getWomanId();

        return adolANCWomanId;

    }
    //    04Sep2021 Arpitha
    public String getAdolId(String adharNo) throws SQLException {
        String adolANCWomanId = "";
        List<tblAdolReg> regList =  databaseHelper.gettblAdolregRuntimeExceptionDao().queryBuilder()
                .where().eq("regAdolAadharNo",adharNo).query();
        if(regList!=null && regList.size() > 0)
            adolANCWomanId = regList.get(0).getAdolID();

        return adolANCWomanId;

    }

//    08Sep2021 Arpitha
    public  boolean isAdolPreganant(String adolId) throws SQLException {
        boolean isPregnant = false;
      List<tblAdolVisitHeader> adolRegList =   databaseHelper.gettblAdolVisitHeaderRuntimeExceptionDao().queryBuilder()
                .selectColumns("adolvisHIsPregnantatReg").where()
                .eq("AdolId",adolId).query();

      if(adolRegList!=null && adolRegList.size()>0)
      {
          for(int i=0;i<adolRegList.size();i++)
          {
              if(adolRegList.get(i).getAdolvisHIsPregnantatReg()!=null &&
              adolRegList.get(i).getAdolvisHIsPregnantatReg().trim().length()>0 &&
              adolRegList.get(i).getAdolvisHIsPregnantatReg().equalsIgnoreCase("Yes"))
                  isPregnant = true;
          }

      }
      return isPregnant;
    }

    //    02Sep2021 Arpitha
    public boolean isAdolANCHistory(String adharNo, String adolId) throws SQLException {
        boolean isAdolANC = false;
        long regList =  databaseHelper.gettblregRuntimeExceptionDao().queryBuilder()
                .where().eq("regAadharCard",adharNo)
                .and().isNotNull("DateDeactivated")
                .and().eq("regAdolId",adolId).countOf();
        if(regList > 0)
            isAdolANC = true;

        return isAdolANC;

    }

//    12Sep2021 Arpitha
public List<tblAdolReg> getExistingAadharId() throws SQLException {
       return databaseHelper.gettblAdolregRuntimeExceptionDao().queryBuilder()
                .selectColumns("regAdolAadharNo").query();
    }

}
