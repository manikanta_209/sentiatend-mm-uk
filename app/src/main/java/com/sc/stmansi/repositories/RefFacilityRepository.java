package com.sc.stmansi.repositories;

import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.tables.tblreferralFacility;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class RefFacilityRepository {

	private DatabaseHelper databaseHelper;

	public RefFacilityRepository(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	public Map<String, String> getRefFacilityType() throws SQLException {
		Map<String, String> factype = new LinkedHashMap<>();
		RuntimeExceptionDao<tblreferralFacility, Integer> FacTypeDao = databaseHelper.gettblreferralFacilityRuntimeExceptionDao();
		List<tblreferralFacility> listreffactype = FacTypeDao.queryBuilder().distinct()
				.selectColumns("FacilityType")
				//.orderBy("AutoId", false)
				.orderBy("FacilityType", true)
				.query();
		for (tblreferralFacility tbl : listreffactype) {

			factype.put(tbl.getFacilityType(), tbl.getFacilityType());
		}
		return factype;
	}

	public Map<String, String> getRefFacilityName(String strfactype) throws SQLException {
		Map<String, String> factype = new LinkedHashMap<>();
		RuntimeExceptionDao<tblreferralFacility, Integer> FacTypeDao = databaseHelper.gettblreferralFacilityRuntimeExceptionDao();
		List<tblreferralFacility> listreffactype = FacTypeDao.queryBuilder().distinct()
				.selectColumns("FacilityName")
				.where().eq("FacilityType", strfactype)
				//.orderBy("FacilityType", true)
				.query();
		for (tblreferralFacility tbl : listreffactype) {

			factype.put(tbl.getFacilityName(), tbl.getFacilityName());
		}
		return factype;
	}

	//08Apr2021 - Bindu - get Ref Fac names
	public String getRefFacilityNames() throws SQLException {
		String villages ;
		RuntimeExceptionDao<tblreferralFacility, Integer> tblreferralFacilities = databaseHelper.gettblreferralFacilityRuntimeExceptionDao();

		String strqry = "select FacilityName from tblreferralFacility";

		GenericRawResults<String[]> rawResults = tblreferralFacilities.queryRaw(strqry);

		List<String[]> results = rawResults.getResults();
		String[] resultArray = results.get(0);
		if (resultArray[0] != null)
			villages = resultArray[0];
		else
			villages = "";

		return villages;
	}
}
