package com.sc.stmansi.repositories;

import com.j256.ormlite.stmt.QueryBuilder;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.tables.tblSanNap;

import java.util.List;

public class SanNapRepository {
    private DatabaseHelper databaseHelper;

    public SanNapRepository(DatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }

    public tblSanNap getDetailsofKindofNkins(String adolId, int transId) throws Exception{
        QueryBuilder<tblSanNap, Integer> queryBuilder = this.databaseHelper.getTblSanNapDao().queryBuilder();
        return queryBuilder.where().eq("AdolId",adolId).and().eq("transId",transId).and().eq("SanNapId","402").query().get(0);
    }

    public List<tblSanNap> getSanNapDetails(String adolId, int transId) throws Exception {
        QueryBuilder<tblSanNap, Integer> queryBuilder = this.databaseHelper.getTblSanNapDao().queryBuilder();
        return queryBuilder.where().eq("AdolId",adolId).and().eq("transId",transId).query();
    }
}
