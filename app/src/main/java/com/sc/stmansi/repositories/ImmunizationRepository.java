package com.sc.stmansi.repositories;

import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.dao.RawRowMapper;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.tables.ChildImmunization;
import com.sc.stmansi.tables.TblChildImmunization;
import com.sc.stmansi.tables.TblChildInfo;
import com.sc.stmansi.tables.tblservicestypemaster;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ImmunizationRepository {

    private static DatabaseHelper databaseHelper;

    public ImmunizationRepository(DatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }

    public List<tblservicestypemaster> getImmunizations() throws SQLException {

        /*tblservicestypemaster tblservicestypemaster = new tblservicestypemaster();
        String strVal = "Y";
        tblservicestypemaster.setIsImmunisation("Y");*/
      return  databaseHelper.getServicesTypeRuntimeExceptionDao().queryBuilder()
              .where().eq("isImmunisation","Y").query();

    }

    // 21Dec2017 Arpitha - method that will return the min and max date of not
    // applicable services
    public static List<tblservicestypemaster> getServiceMinMaxDates(String serviceType, int serviceNumber, String womenId) throws SQLException {
        List<tblservicestypemaster> arrVal = new ArrayList<tblservicestypemaster>();

        String sql;
        if (serviceType.equalsIgnoreCase("TT") || serviceType.equalsIgnoreCase("ANC")
                || serviceType.equalsIgnoreCase("TT") || serviceType.equalsIgnoreCase("WomanVisit")
                || serviceType.equalsIgnoreCase("IFA") || serviceType.equalsIgnoreCase("Calcium")
                || serviceType.equalsIgnoreCase("HomeVisit") || serviceType.equalsIgnoreCase("EDD")
                || serviceType.equalsIgnoreCase("TTBooster")) {
            sql = "Select CASE When tblservicestypemaster.taskreference is 'LMP' then "
                    + " Date(substr(tblregisteredwomen.regLMP , 7) || '-' || substr(tblregisteredwomen.regLMP ,4,2)  || '-' "
                    + "  || substr(tblregisteredwomen.regLMP , 1,2), '+'||tblservicestypemaster.DaysDiffStart||' days') "
                    + " else Date(substr(tblregisteredwomen.regADDate , 7) || '-' ||"
                    + " substr(tblregisteredwomen.regADDate ,4,2)  || '-'"
                    + " || substr(tblregisteredwomen.regADDate , 1,2), '+'||tblservicestypemaster.DaysDiffStart||' days') END AS STARTDATE , "
                    + " CASE When tblservicestypemaster.taskreference is 'LMP' then Date(substr(tblregisteredwomen.regLMP , 7) || '-' ||  substr(tblregisteredwomen.regLMP ,4,2)  || '-'"
                    + " || substr(tblregisteredwomen.regLMP , 1,2), '+'||tblservicestypemaster.DaysDiffEnd||' days') else Date(substr(tblregisteredwomen.regADDate , 7) || '-' ||"
                    + " substr(tblregisteredwomen.regADDate ,4,2)  || '-'"
                    + " || substr(tblregisteredwomen.regADDate , 1,2), '+'||tblservicestypemaster.DaysDiffStart||' days') END AS ENDDATE"
                    + " from tblregisteredwomen INNER JOIN tblservicestypemaster where servicetype = '" + serviceType
                    + "' " + " and serviceid= " + serviceNumber + " and womanid='" + womenId + "'";
        } else {
            sql = "Select CASE When tblservicestypemaster.taskreference is 'ADD' then "
                    + " Date(substr(tblregisteredwomen.regADDate , 7) || '-' || substr(tblregisteredwomen.regADDate ,4,2)  || '-' "
                    + "  || substr(tblregisteredwomen.regADDate , 1,2), '+'||tblservicestypemaster.DaysDiffStart||' days') "
                    + " else Date(substr(tblregisteredwomen.regADDate , 7) || '-' ||"
                    + " substr(tblregisteredwomen.regADDate ,4,2)  || '-'"
                    + " || substr(tblregisteredwomen.regADDate , 1,2), '+'||tblservicestypemaster.DaysDiffStart||' days') END AS STARTDATE , "
                    + " CASE When tblservicestypemaster.taskreference is 'ADD' then Date(substr(tblregisteredwomen.regADDate , 7) || '-' ||  substr(tblregisteredwomen.regADDate ,4,2)  || '-'"
                    + " || substr(tblregisteredwomen.regADDate , 1,2), '+'||tblservicestypemaster.DaysDiffEnd||' days') else Date(substr(tblregisteredwomen.regADDate , 7) || '-' ||"
                    + " substr(tblregisteredwomen.regADDate ,4,2)  || '-'"
                    + " || substr(tblregisteredwomen.regADDate , 1,2), '+'||tblservicestypemaster.DaysDiffStart||' days') END AS ENDDATE"
                    + " from tblregisteredwomen INNER JOIN tblservicestypemaster where servicetype = '" + serviceType
                    + "' " + " and serviceid= " + serviceNumber + " and womanid='" + womenId + "'";
        }

       /* Cursor cur = EjananiAppContext.getDb().rawQuery(sql, null);
        if (cur.getCount() > 0) {
            cur.moveToFirst();
            WomanPendingPojo wpojo = new WomanPendingPojo();
            wpojo.setDueDateMin(cur.getString(0));
            wpojo.setDueDateMax(cur.getString(1));
            arrVal.add(wpojo);
        }*/

        databaseHelper.getServicesTypeRuntimeExceptionDao();
        GenericRawResults<String[]> raw  = databaseHelper.getServicesTypeRuntimeExceptionDao().queryRaw(sql);
//        arrVal.add(raw.getResults());
        return arrVal;

    }

    public List<TblChildInfo> getChildPendingForImmunization(String userId, int serviceId) throws SQLException {

        List<TblChildInfo> arrVal = new ArrayList<>();
        String sql = "Select chlId,chlChildName,chlRCHID,chlGender,chlDateOfBirth,WomanId from tblchildinfo join tblservicestypemaster where" +
                " tblservicestypemaster.ServiceId=" + serviceId +
                " and date((Date(substr(chlDateOfBirth , 7) || '-' || \n" +
                "substr(chlDateOfBirth ,4,2) || '-' || \n" +
                "substr(chlDateOfBirth , 1,2))), '+'||DaysDiffStart ||' day')<=Date('now') and UserId='" + userId + "' and chlID NOT IN(Select immChildId from tblchildimmunization  where immId=="+serviceId+")";

        GenericRawResults<TblChildInfo> tbls = databaseHelper.
                getChildInfoRuntimeExceptionDao().
                queryRaw(sql,
                        new RawRowMapper<TblChildInfo>() {
                            @Override
                            public TblChildInfo mapRow(String[] strings, String[] strings1) {
                                TblChildInfo servicesPojo = new TblChildInfo();
                                String s = strings[0];
                                servicesPojo.setCount(Integer.parseInt(strings1[0]));
                                servicesPojo.setServiceType(strings1[1]);
                                return servicesPojo;
                            }
                        });

        for (TblChildInfo foo : tbls) {
            arrVal.add(foo);

        }
        return tbls.getResults();
    }

    public Map<Integer,String> getImmunizationNames() throws SQLException {

        Map<Integer,String> mapList = new LinkedHashMap<>();
        List<tblservicestypemaster> listServices =   databaseHelper.getServicesTypeRuntimeExceptionDao().queryBuilder()
                .selectColumns("ServiceType","ServiceId").orderBy("DaysDiffStart",true)
                .where().eq("isImmunisation","Y").query();

        for(tblservicestypemaster listServices1: listServices)
        {
            mapList.put(listServices1.getServiceId(),listServices1.getServiceType());
        }

        return mapList;
    }


   public int addImmunization(DatabaseHelper dbHelper, TblChildImmunization tblChildImmunization) throws SQLException {
        return dbHelper.getImmunizationRuntimeExceptionDao().create(tblChildImmunization);

    }

    public List<ChildImmunization> getChildImmunizationStatus(String userId, String chlId, int filter, String dob) throws SQLException {

        List<ChildImmunization> arrVal = new ArrayList<>();
        String sql;
        if(filter==1)
            sql = " select servicetype, immActualDateOfAction, chlDateOfBirth, daysDiffStart," +
                    " daysDiffEnd, ServiceId\n" +
                    " from  tblservicestypemaster   left join tblchildimmunization " +
                    " on  tblservicestypemaster.ServiceType = tblchildimmunization.immType  " +
                    " and immchildId = '"+chlId+"'  left join tblchildinfo on " +
                    " tblchildinfo.chlID  = tblchildimmunization.immChildId  where  " +
                    " tblservicestypemaster.isImmunisation='Y' and " +
                    " (Date(substr('"+dob+"' , 7) || '-' || substr('"+dob+"' ,4,2)" +
                    "                             || '-' || substr('"+dob+"' , 1,2),'+' || " +
                    " daysDiffEnd ||' day') < Date('now')) and immActualDateofAction is null  order by daysdiffStart";
        else if(filter==2)
            sql = "select\t servicetype, immActualDateOfAction, chlDateOfBirth, daysDiffStart," +
                    " daysDiffEnd , ServiceId \n" +
                    " from  tblservicestypemaster   left join tblchildimmunization\n" +
                    "on  tblservicestypemaster.ServiceType = tblchildimmunization.immType \n" +
                    "and immchildId = '"+chlId+"'  left join tblchildinfo on \n" +
                    " tblchildinfo.chlID  = tblchildimmunization.immChildId  where  \n" +
                    "tblservicestypemaster.isImmunisation='Y' and \n" +
                    "(Date(substr('"+dob+"' , 7) || '-' || substr('"+dob+"' ,4,2)\n" +
                    "                             || '-' || substr('"+dob+"' , 1,2),'+' || \n" +
                    " daysDiffStart ||' day') <= Date('now'))  and \n" +
                    " (Date(substr('"+dob+"' , 7) || '-' || substr('"+dob+"' ,4,2)\n" +
                    "    || '-' || substr('"+dob+"' , 1,2),'+' || \n" +
                    " daysDiffEnd ||' day') >= Date('now'))   and immActualDateofAction is null  order by daysdiffStart ";
        else if(filter==3)
            sql = " select servicetype, immActualDateOfAction, chlDateOfBirth, daysDiffStart," +
                    " daysDiffEnd , ServiceId \n" +
                    " from  tblservicestypemaster   left join tblchildimmunization " +
                    " on  tblservicestypemaster.ServiceType = tblchildimmunization.immType  " +
                    " and immchildId = '"+chlId+"'  left join tblchildinfo on " +
                    " tblchildinfo.chlID  = tblchildimmunization.immChildId  where  " +
                    " tblservicestypemaster.isImmunisation='Y' and " +
                    " (Date(substr('"+dob+"' , 7) || '-' || substr('"+dob+"' ,4,2)" +
                    "                             || '-' || substr('"+dob+"' , 1,2),'+' || " +
                    " daysDiffStart ||' day') > Date('now'))   and immActualDateofAction is  null   order by daysdiffStart ";
        else if(filter==4)
            sql = " select servicetype, immActualDateOfAction, chlDateOfBirth, daysDiffStart," +
                    " daysDiffEnd , ServiceId \n" +
                    " from  tblservicestypemaster   left join tblchildimmunization " +
                    " on  tblservicestypemaster.ServiceType = tblchildimmunization.immType  " +
                    " and immchildId = '"+chlId+"'  left join tblchildinfo on " +
                    " tblchildinfo.chlID  = tblchildimmunization.immChildId  where  " +
                    " tblservicestypemaster.isImmunisation='Y' and  " +
                    "immActualDateofAction is not null  order by daysdiffStart ";
        else
            sql = " select servicetype, immActualDateOfAction, chlDateOfBirth, daysDiffStart," +
                    " daysDiffEnd , ServiceId \n" +
                    " from  tblservicestypemaster left join tblchildimmunization " +
                    " on  tblservicestypemaster.ServiceType = tblchildimmunization.immType  " +
                    " and immchildId = '"+chlId+"'  left join tblchildinfo on " +
                    " tblchildinfo.chlID  = tblchildimmunization.immChildId  where  " +
                    " tblservicestypemaster.isImmunisation='Y'  order by daysdiffStart " ;


        GenericRawResults<ChildImmunization> tbls = databaseHelper.
                getServicesTypeRuntimeExceptionDao().
                queryRaw(sql,
                        new RawRowMapper<ChildImmunization>() {
                            @Override
                            public ChildImmunization mapRow(String[] strings, String[] strings1) {
                                ChildImmunization servicesPojo = new ChildImmunization();
                                String s = strings[0];
                                servicesPojo.setServiceType(strings1[0]);
                                servicesPojo.setActualDateOfAction(strings1[1]);
                                servicesPojo.setDateTimeOfBirth(strings1[2]);
                                servicesPojo.setDaysDiffStart(Integer.parseInt(strings1[3]));
                                servicesPojo.setDaysDiffEnd(Integer.parseInt(strings1[4]));
                                servicesPojo.setServiceId(Integer.parseInt(strings1[5]));


                                return servicesPojo;
                            }
                        });

        for (ChildImmunization foo : tbls) {
            arrVal.add(foo);

        }
        return arrVal;
    }

    public List<TblChildImmunization> getImmunizationData(String strImmType, String chlId) throws SQLException {

       return databaseHelper.getImmunizationRuntimeExceptionDao().queryBuilder().where().
                eq("immType",strImmType).and()
               .eq("immChildId",chlId)
               .query();
    }




    public List<String[]> getPendingChildCountForImmunization(String userId,
                                                                int villageCode, String fromDate , String toDate) throws SQLException {

        List<TblChildInfo> arrVal = new ArrayList<>();

      String sql;

      if(villageCode!=0)
          sql = "Select " +
                  " servicetype , count(chlId) from tblservicestypemaster " +
                  " left join tblchildinfo on " +
                  " date((Date(substr(chlDateOfBirth , 7) || '-' || " +
                  " substr(chlDateOfBirth ,4,2) || '-' || " +
                  " substr(chlDateOfBirth , 1,2))), '+'||DaysDiffStart ||' day')  " +
                  " between Date(substr('"+fromDate+"' , 7) || '-'" +
                  " || substr('"+fromDate+"' ,4,2)" +
                  " || '-' || substr('"+fromDate+"' , 1,2)) and Date(substr('"+toDate+"' , 7)" +
                  " || '-'" +
                  "  || substr('"+toDate+"' ,4,2)" +
                  " || '-' || substr('"+toDate+"' , 1,2))" +
                  " and chlTribalHamlet= "+villageCode + " and  " +
                  " UserId='"+userId+"'  and chlDeactDate is null and chldeliveryresult<=1 and chlID NOT IN(Select immChildId from" +
                  " tblchildimmunization" +
                  " where tblservicestypemaster.ServiceType = tblchildimmunization.immtype)  where isImmunisation='Y'  "+
                  " group by servicetype  order by DaysDiffStart";
      else
      sql = "Select " +
              " servicetype , count(chlId) from tblservicestypemaster " +
              " left join tblchildinfo on " +
              " date((Date(substr(chlDateOfBirth , 7) || '-' || " +
              " substr(chlDateOfBirth ,4,2) || '-' || " +
              " substr(chlDateOfBirth , 1,2))), '+'||DaysDiffStart ||' day')  " +
              " between Date(substr('"+fromDate+"' , 7) || '-'" +
              " || substr('"+fromDate+"' ,4,2)" +
              " || '-' || substr('"+fromDate+"' , 1,2)) and Date(substr('"+toDate+"' , 7)" +
              " || '-'" +
              "  || substr('"+toDate+"' ,4,2)" +
              " || '-' || substr('"+toDate+"' , 1,2))" +
              " and  " +
              " UserId='"+userId+"' and chlDeactDate is null and chldeliveryresult<=1 and chlID NOT IN(Select immChildId from" +
              " tblchildimmunization" +
              " where tblservicestypemaster.ServiceType = tblchildimmunization.immtype)  where isImmunisation='Y'  " +
              " group by servicetype  order by DaysDiffStart";


        GenericRawResults<String[]> gen = databaseHelper.getChildInfoRuntimeExceptionDao().queryRaw(sql);

        return gen.getResults();
    }


    public String getEarliestDate() throws SQLException {




        GenericRawResults<String[]> list = databaseHelper.getChildInfoRuntimeExceptionDao().queryRaw
                (" SELECT `chlDateOfBirth` FROM `tblchildinfo` " +
                        "  ORDER BY Date(substr(chlDateOfBirth , 7) || '-' || " +
                        "               substr(chlDateOfBirth ,4,2) || '-' || " +
                        "               substr(chlDateOfBirth , 1,2))  Limit 1");

       // if (list!=null && list.getResults() != null && list.getResults().size() > 0) {  // 11Dec2019 - Bindu -check for size

       List<String[]> m =  list.getResults();
       if(m!=null && m.size()>0)
       {
//            String s = list.getResults().get(0)[0];
           String s  = "";

           String[] strArray = m.get(0);

           if(strArray!=null && strArray.length>0)
                s = strArray[0];

           if (s != null && s.trim().length() > 0)
                return s;
            else
                return "";
        } else return "";
    }

    //11Feb2021 - Manikanta - add parent id  and userid

    public List<ChildImmunization> getPendingChild(String userId,
                                                   int villageCode,
                                                   String immuType,
                                                   String fromDate,
                                                   String toDate) throws SQLException {

        List<ChildImmunization> arrVal = new ArrayList<>();
        String sql = null;
        if(villageCode!=0)
            sql  = "Select  chlReg, chlID, chlChildname, "+
                    "                chlDateOfBirth, chlGender, " +
                    "chlComplications, DaysDiffStart, DaysDiffEnd," +
                    " ServiceId, WomanId, chlDeactDate,chlReg, chlWeight,chlParentId,UserId from tblchildinfo inner join tblservicestypemaster " +
                    "where  date((Date(substr(chlDateOfBirth , 7) || '-' || " +
                    " substr(chlDateOfBirth ,4,2) || '-' || " +
                    " substr(chlDateOfBirth , 1,2))), '+'||DaysDiffStart ||' day')  " +
                    " between Date(substr('"+fromDate+"' , 7) || '-' || substr('"+fromDate+"'" +
                    " ,4,2) || \n" +
                    " '-' || substr('"+fromDate+"' , 1,2)) and Date(substr('"+toDate+"' , 7)" +
                    " || '-' \n" +
                    " || substr('"+toDate+"' ,4,2) || '-' || substr('"+toDate+"' , 1,2))  " +
                    " and   \n" +
                    " UserId='"+userId+"' and chlID NOT IN(Select immChildId from " +
                    "tblchildimmunization\n" +
                    " where  tblchildimmunization.immtype = '"+immuType+"')" +
                    " and servicetype='"+immuType+"' and chlTribalHamlet= "+villageCode +" and chldeliveryresult<=1  and chlDeactDate is null ";
        else
            sql  = "Select  chlReg, chlID, chlChildname, "+
                    "  chlDateOfBirth, chlGender, chlComplications ," +
                    " DaysDiffStart, DaysDiffEnd, ServiceId, WomanId, chlDeactDate,chlReg,chlWeight,chlParentId,UserId from tblchildinfo inner join tblservicestypemaster " +
                    "where  date((Date(substr(chlDateOfBirth , 7) || '-' || " +
                    " substr(chlDateOfBirth ,4,2) || '-' || " +
                    " substr(chlDateOfBirth , 1,2))), '+'||DaysDiffStart ||' day')  " +
                    " between Date(substr('"+fromDate+"' , 7) || '-' || substr('"+fromDate+"'" +
                    " ,4,2) || \n" +
                    " '-' || substr('"+fromDate+"' , 1,2)) and Date(substr('"+toDate+"' , 7)" +
                    " || '-' \n" +
                    " || substr('"+toDate+"' ,4,2) || '-' || substr('"+toDate+"' , 1,2))  " +
                    " and   \n" +
                    " UserId='"+userId+"' and chlID NOT IN(Select immChildId from " +
                    "tblchildimmunization\n" +
                    " where  tblchildimmunization.immtype = '"+immuType+"')" +
                    " and servicetype='"+immuType+"' and chldeliveryresult<=1  and chlDeactDate is null ";


        GenericRawResults<ChildImmunization> tbls = databaseHelper.
                getServicesTypeRuntimeExceptionDao().
                queryRaw(sql,
                        new RawRowMapper<ChildImmunization>() {
                            @Override
                            public ChildImmunization mapRow(String[] strings, String[] strings1) {
                                ChildImmunization servicesPojo = new ChildImmunization();
                                String s = strings[0];
                                servicesPojo.setChlId(strings1[1]);
                                servicesPojo.setChlName(strings1[2]);
                                servicesPojo.setChlGender(strings1[4]);
                                servicesPojo.setChlComplications(strings1[5]);
                                servicesPojo.setChlDateofBirth(strings1[3]);
                                servicesPojo.setDaysDiffStart(Integer.parseInt(strings1[6]));
                                servicesPojo.setDaysDiffEnd(Integer.parseInt(strings1[7]));
                                servicesPojo.setWomanId(strings1[9]);
                                servicesPojo.setServiceId(Integer.parseInt(strings1[8]));
                                servicesPojo.setChlDeactDate(strings1[10]);
                                servicesPojo.setChlReg(strings1[11]);
                                servicesPojo.setWeight(strings1[12]);
//                    11FebMani set Parent id and userid
                                servicesPojo.setChlParentId(strings1[13]);
                                servicesPojo.setUserId(strings1[14]);


                                return servicesPojo;
                            }
                        });

        for (ChildImmunization foo : tbls) {
            arrVal.add(foo);

        }
        return arrVal;
    }

    public List<String[]> getCompletedImmuCount(String userId, int villageCode) throws SQLException {



       String sql;


       sql  = "SELECT tblservicestypemaster.ServiceType,\n" +
               "  count(tblchildimmunization.immActualdateofaction) \n" +
               "  FROM tblservicestypemaster\n" +
               "  left JOIN tblchildimmunization ON tblservicestypemaster.ServiceId =\n" +
               "    tblchildimmunization.immId AND tblservicestypemaster.ServiceType =\n" +
               "    tblchildimmunization.immType where tblservicestypemaster.isimmunisation ='Y'  " +
               "group by tblservicestypemaster.ServiceId,tblservicestypemaster.ServiceType order by daysdiffstart";

        if(villageCode!=0)
       sql = "Select sm.servicetype, t3.cnt from tblservicestypemaster sm left join\n" +
               "                  (Select sum(cast(t2.wid as decimal)) as cnt, servicetype from(\n" +
               "                Select  \n" +
               "                 count(distinct(chlID)) as wid, immtype as servicetype\n" +
               "                  from tblchildimmunization sl inner join tblchildinfo reg on\n" +
               "                 sl.immChildId = reg.chlID where   immactualdateofaction\n" +
               "                 is not null    and reg.chlTribalHamlet= "+villageCode+" \n" +
               "                  and sl.UserId='"+userId+"' \n" +
               "              group by servicetype,womenid ) \n" +
               "                  as t2 group by t2.servicetype) as t3 \n" +
               "                 on sm.servicetype = t3.servicetype where isImmunisation = 'Y'\n" +
               "                     group by sm.servicetype order by daysdiffstart";
        else
            sql = "Select sm.servicetype, t3.cnt from tblservicestypemaster sm left join\n" +
                    "                  (Select sum(cast(t2.wid as decimal)) as cnt, servicetype from(\n" +
                    "                Select  \n" +
                    "                 count(distinct(chlID)) as wid, immtype as servicetype\n" +
                    "                  from tblchildimmunization sl inner join tblchildinfo reg on\n" +
                    "                 sl.immChildId = reg.chlID where   immactualdateofaction\n" +
                    "                 is not null     \n" +
                    "                  and sl.UserId='"+userId+"' \n" +
                    "              group by servicetype,womenid ) \n" +
                    "                  as t2 group by t2.servicetype) as t3 \n" +
                    "                 on sm.servicetype = t3.servicetype where isImmunisation = 'Y'\n" +
                    "                     group by sm.servicetype order by daysdiffstart";


        GenericRawResults<String[]> gen = databaseHelper.getImmunizationRuntimeExceptionDao().queryRaw(sql);

        return gen.getResults();
    }

    //11Feb2021 - Manikanta - add parent id and userid
    public List<ChildImmunization> getCompletedChild(String userId,
                                                     int villageCode,
                                                     String immuType,
                                                     String fromDate,
                                                     String toDate) throws SQLException {

        List<ChildImmunization> arrVal = new ArrayList<>();
        String sql = null;
        if(villageCode!=0)
            sql  = "Select  chlReg, chlID, chlChildname, "+
                    "                chlDateOfBirth, chlGender, " +
                    "chlComplications, DaysDiffStart, DaysDiffEnd," +
                    " ServiceId, WomanId, chlDeactDate,chlReg,chlWeight,chlParentId,UserId from tblchildinfo inner join tblservicestypemaster " +
                    "where  " +
                   /* "date((Date(substr(chlDateOfBirth , 7) || '-' || " +
                    " substr(chlDateOfBirth ,4,2) || '-' || " +
                    " substr(chlDateOfBirth , 1,2))), '+'||DaysDiffStart ||' day')  " +
                    " between Date(substr('"+fromDate+"' , 7) || '-' || substr('"+fromDate+"'" +
                    " ,4,2) || \n" +
                    " '-' || substr('"+fromDate+"' , 1,2)) and Date(substr('"+toDate+"' , 7)" +
                    " || '-' \n" +
                    " || substr('"+toDate+"' ,4,2) || '-' || substr('"+toDate+"' , 1,2))  " +
                    " and   \n" +*/
                    " UserId='"+userId+"' and chlID  IN(Select immChildId from " +
                    "tblchildimmunization\n" +
                    " where  tblchildimmunization.immtype = '"+immuType+"')" +
                    " and servicetype='"+immuType+"' and chlTribalHamlet= "+villageCode +" ";
        else
            sql  = "Select  chlReg, chlID, chlChildname, "+
                    "  chlDateOfBirth, chlGender, chlComplications ," +
                    " DaysDiffStart, DaysDiffEnd, ServiceId, WomanId, chlDeactDate,chlReg,chlWeight,chlParentId,UserId from tblchildinfo inner join tblservicestypemaster " +
                    "where " +
                   /* " date((Date(substr(chlDateOfBirth , 7) || '-' || " +
                    " substr(chlDateOfBirth ,4,2) || '-' || " +
                    " substr(chlDateOfBirth , 1,2))), '+'||DaysDiffStart ||' day')  " +
                    " between Date(substr('"+fromDate+"' , 7) || '-' || substr('"+fromDate+"'" +
                    " ,4,2) || \n" +
                    " '-' || substr('"+fromDate+"' , 1,2)) and Date(substr('"+toDate+"' , 7)" +
                    " || '-' \n" +
                    " || substr('"+toDate+"' ,4,2) || '-' || substr('"+toDate+"' , 1,2))  " +
                    " and   \n" +*/
                    " UserId='"+userId+"' and chlID  IN(Select immChildId from " +
                    "tblchildimmunization\n" +
                    " where  tblchildimmunization.immtype = '"+immuType+"')" +
                    " and servicetype='"+immuType+"' ";


        GenericRawResults<ChildImmunization> tbls = databaseHelper.
                getServicesTypeRuntimeExceptionDao().
                queryRaw(sql,
                        new RawRowMapper<ChildImmunization>() {
                            @Override
                            public ChildImmunization mapRow(String[] strings, String[] strings1) {
                                ChildImmunization servicesPojo = new ChildImmunization();
                                String s = strings[0];
                                servicesPojo.setChlId(strings1[1]);
                                servicesPojo.setChlName(strings1[2]);
                                servicesPojo.setChlGender(strings1[4]);
                                servicesPojo.setChlComplications(strings1[5]);
                                servicesPojo.setChlDateofBirth(strings1[3]);
                                servicesPojo.setDaysDiffStart(Integer.parseInt(strings1[6]));
                                servicesPojo.setDaysDiffEnd(Integer.parseInt(strings1[7]));
                                servicesPojo.setWomanId(strings1[9]);
                                servicesPojo.setServiceId(Integer.parseInt(strings1[8]));
                                servicesPojo.setChlDeactDate(strings1[10]);
                                servicesPojo.setChlReg(strings1[11]);
                                servicesPojo.setWeight(strings1[12]);
                                //11Feb2021 - Manikanta - add parent id and userid
                                servicesPojo.setChlParentId(strings1[13]);
                                servicesPojo.setUserId(strings1[14]);


                                return servicesPojo;
                            }
                        });

        for (ChildImmunization foo : tbls) {
            arrVal.add(foo);

        }
        return arrVal;
    }
}
