package com.sc.stmansi.repositories;

import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.tables.AuditPojo;

public class AuditRepository {

	private DatabaseHelper databaseHelper;

	public AuditRepository(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	public boolean insertTotblAuditTrail(AuditPojo apj) throws Exception {
		databaseHelper.getAuditRuntimeExceptionDao().create(apj);
		return true;
	}

	public static boolean insertTotblAuditTrailNew(AuditPojo apj, DatabaseHelper newDb1) throws Exception {

		newDb1.getAuditRuntimeExceptionDao().create(apj);

		return true;
	}
}
