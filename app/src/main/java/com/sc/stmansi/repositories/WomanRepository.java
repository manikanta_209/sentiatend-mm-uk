package com.sc.stmansi.repositories;

import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.dao.RawRowMapper;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.womanlist.CareType;
import com.sc.stmansi.womanlist.QueryParams;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class WomanRepository {

    private DatabaseHelper databaseHelper;

    public WomanRepository(DatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }

    public int getUID(String strThayicard) {
        int count = 0;
        try {
            RuntimeExceptionDao<tblregisteredwomen, Integer> regDAO = databaseHelper.gettblregRuntimeExceptionDao();
            count = (int) regDAO.queryBuilder().where().eq("regUIDNo",
                    strThayicard).countOf();
        } catch (SQLException e) {
            // TODO add logging framework

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }

        return count;
    }

    public List<tblregisteredwomen> getDisplayWomanList(String userId,
                                                        int villageCode,
                                                        String cond,
                                                        String filter,
                                                        long limitStart, boolean isLMPNotConfirmed) throws SQLException {
        boolean includeVillageCode = villageCode != 0;
        boolean includeUserFilter = filter != null && filter.trim().length() > 0;
        boolean includePatientStatus = cond != null && !cond.equalsIgnoreCase("all");

        long limit = 10;

        String[] selectColumns = {"regWomanName", "regRegistrationDate", "regStatusWhileRegistration",
                "regComplicatedpreg", "regriskFactors", "regAmberOrRedColorCode",
                "regrecommendedPlaceOfDelivery", "UserId", "WomanId", "regVillage", "regLMP", "IsCompl",
                "LastAncVisitDate", "LastPncVisitDate", "IsReferred", "regADDate","DateDeactivated","regWomenImage", "isLMPConfirmed", "regRiskFactorsByCC", "regOtherRiskByCC", "regCCConfirmRisk","regIsAdolescent"}; // 07Apr2021 Bindu add islmpconfirmed

        QueryBuilder<tblregisteredwomen, Integer> queryBuilder = databaseHelper.gettblregRuntimeExceptionDao().queryBuilder()
                .selectColumns(selectColumns)
                .orderBy("transId", false)
                .offset(limitStart)
                .limit(limit);

        Where<tblregisteredwomen, Integer> where = queryBuilder.where()
                .eq("userId", userId)
                .and().isNull("DateDeactivated");

        if (includeVillageCode) {
            where = where.and().eq("regvillage", villageCode);
        }
        if (includePatientStatus) {
            int code = 0;
            if (cond.equalsIgnoreCase("preg")) code = 1;
            else if (cond.equalsIgnoreCase("mother")) code = 2;
            where = where.and().eq("regpregnantorMother", code);
        }
        //25Mar2021 Bindu add lmp not confirmed
        if (isLMPNotConfirmed)
            where = where.and().eq("regpregnantorMother", 1).and().eq("isLMPConfirmed", 0);

        if (includeUserFilter) {
            where = where.and().like("regwomanName", "%" + filter + "%").or()
                    .like("regphonenumber", "%" + filter + "%").or()
                    .like("reguidNo", "%" + filter + "%");
        }
        queryBuilder.setWhere(where);
        return queryBuilder.query();
    }

    public tblregisteredwomen getRegistartionDetails(String womanId, String userId) throws SQLException {
        RuntimeExceptionDao<tblregisteredwomen, Integer> regDAO = databaseHelper.gettblregRuntimeExceptionDao();
        return regDAO.queryBuilder().where().eq("WomanId", womanId).
                and().eq("userId", userId).query().get(0);
    }


    //  get womanid list
    public List<tblregisteredwomen> getHRPList(QueryParams queryParams) throws Exception {
        boolean includeVillageCode = queryParams.selectedVillageCode != 0;
        boolean includeUserFilter = queryParams.textViewFilter != null && !queryParams.textViewFilter.trim().isEmpty();

        long limit = 10;

        String[] selectColumns = {"regWomanName", "regRegistrationDate", "regStatusWhileRegistration",
                "regComplicatedpreg", "regriskFactors", "regAmberOrRedColorCode",
                "regrecommendedPlaceOfDelivery", "UserId", "WomanId", "regVillage", "regLMP", "IsCompl",
                "LastAncVisitDate", "LastPncVisitDate", "IsReferred", "regADDate","DateDeactivated","regWomenImage","isLMPConfirmed", "regRiskFactorsByCC", "regOtherRiskByCC", "regCCConfirmRisk","regIsAdolescent"};//15Nov2019 Arpitha //08Apr2021 Bindu add lmpconfirmed

        QueryBuilder<tblregisteredwomen, Integer> queryBuilder = databaseHelper.gettblregRuntimeExceptionDao().queryBuilder()
                .selectColumns(selectColumns)
                .orderBy("transId", false);

        Where<tblregisteredwomen, Integer> where = queryBuilder.where();
        where.eq("userId", queryParams.userId).and().isNull("DateDeactivated")
                .and(where.eq("regComplicatedpreg", 1), where.or().eq("iscompl", 'Y'));

        if (includeVillageCode) {
            where = where.and().eq("regvillage", queryParams.selectedVillageCode);
        }
        if (includeUserFilter) {
            where = where.and(where, where.or(where.like("regwomanName", "%" + queryParams.textViewFilter + "%"),
                    where.like("regphonenumber", "%" + queryParams.textViewFilter + "%"),
                    where.like("reguidNo", "%" + queryParams.textViewFilter + "%")));
        }
        queryBuilder.setWhere(where);
        String s = queryBuilder.prepare().toString();
        return queryBuilder.query();
    }

    public List<tblregisteredwomen> getNearingDeliveryList(QueryParams queryParams) throws SQLException {
        boolean includeUserFilter = queryParams.textViewFilter != null && !queryParams.textViewFilter.trim().isEmpty();
        boolean includeVillageCode = queryParams.selectedVillageCode != 0;

        long limit = 10;

        String[] selectColumns = {"regWomanName", "regRegistrationDate", "regStatusWhileRegistration",
                "regComplicatedpreg", "regriskFactors", "regAmberOrRedColorCode",
                "regrecommendedPlaceOfDelivery", "UserId", "WomanId", "regVillage", "regLMP", "IsCompl",
                "LastAncVisitDate", "LastPncVisitDate", "IsReferred", "regADDate","DateDeactivated","regWomenImage","isLMPConfirmed", "regRiskFactorsByCC", "regOtherRiskByCC", "regCCConfirmRisk","regIsAdolescent"};//15Nov2019 Arpitha //08Apr2021 Bindu add lmp confirmation

        QueryBuilder<tblregisteredwomen, Integer> qb = databaseHelper.gettblregRuntimeExceptionDao()
                .queryBuilder()
                .selectColumns(selectColumns)
                .orderBy("transid", false);

        Where<tblregisteredwomen, Integer> where = qb.where();
        where.eq("userId", queryParams.userId).and().isNull("DateDeactivated")
                .and(where.isNull("CurrentWomenStatus"), where.or().eq("CurrentWomenStatus", ""))
                .and(where.isNull("regADDate"), where.or().eq("regADDate", ""));

        if (includeUserFilter) {
            where = where.and(where, where.or(where.like("regwomanName", "%" + queryParams.textViewFilter + "%"),
                    where.like("regphonenumber", "%" + queryParams.textViewFilter + "%"),
                    where.like("reguidNo", "%" + queryParams.textViewFilter + "%")));
        }
        if (includeVillageCode) {
            where = where.and().eq("regvillage", queryParams.selectedVillageCode);
        }

        where = where.and()
                .raw(" julianday('now') - julianday(substr(regLMP , 7) || '-' || " +
                        " substr(regLMP ,4,2)  || '-' " +
                        "|| substr(regLMP , 1,2)) > 210");
        qb.setWhere(where);
        return qb.query();
    }


    public static int updateWomenRegDataNew(String userId,
                                            String upSql, int transId,
                                            String editedValues, DatabaseHelper newDb) throws SQLException {

        int update = 0;
        update = newDb.gettblregRuntimeExceptionDao().updateRaw(upSql);


        TransactionHeaderRepository transactionHeaderRepository = new TransactionHeaderRepository(newDb);
        transactionHeaderRepository.updateNew(userId,
                transId, newDb.gettblregRuntimeExceptionDao().getTableName(),
                upSql, null, null, newDb);

        return update;
    }

    //    add DateDeactivated in where cond Arpitha 29NOv2019
    public long getRegisteredWomenCount(String womanCondition, String sessionUserId) throws SQLException {
        boolean includePatientStatus = womanCondition != null && !womanCondition.equalsIgnoreCase("all");
        QueryBuilder<tblregisteredwomen, Integer> queryBuilder = databaseHelper.gettblregRuntimeExceptionDao().queryBuilder();
        Where<tblregisteredwomen, Integer> where = queryBuilder.where()
                .eq("UserId", sessionUserId).and().isNull("DateDeactivated");

        if (includePatientStatus) {
            int code = 0;
            if (womanCondition.equalsIgnoreCase("preg")) code = 1;
            else if (womanCondition.equalsIgnoreCase("mother")) code = 2;
            where = where.and().eq("regpregnantorMother", code);
        }
        queryBuilder.setWhere(where);
        String s = queryBuilder.prepare().toString();
        return queryBuilder.countOf();
    }

    public List<tblregisteredwomen> getVisitDetailsFrmReg(String womanId, String sessionUserId) throws SQLException {
        String[] selectColumns = {"IsCompl", "LastAncVisitDate", "LastPncVisitDate", "IsReferred"};
        return databaseHelper.gettblregRuntimeExceptionDao().queryBuilder()
                .selectColumns(selectColumns)
                .where().eq("userId", sessionUserId).and().eq("WomanId", womanId).query();
    }

    //  get womanid list
    public List<tblregisteredwomen> getHomeVisitMenuist(String userId,
                                                        int villageCode, String filter, long limitStart, boolean noVisitDone) throws Exception {
        boolean includeVillageCode = villageCode != 0;
        boolean includeUserFilter = filter != null && !filter.trim().isEmpty();

        long limit = 10;

        String[] selectColumns = {"regWomanName", "regRegistrationDate", "regStatusWhileRegistration",
                "regComplicatedpreg", "regriskFactors", "regAmberOrRedColorCode",
                "regrecommendedPlaceOfDelivery", "UserId", "WomanId", "regVillage", "regLMP", "IsCompl",
                "LastAncVisitDate", "LastPncVisitDate", "IsReferred"};

        QueryBuilder<tblregisteredwomen, Integer> queryBuilder = databaseHelper.gettblregRuntimeExceptionDao().queryBuilder()
                .selectColumns(selectColumns)
                .orderBy("transId", false);

        Where<tblregisteredwomen, Integer> where = queryBuilder.where().eq("userId", userId)
                .and().isNull("DateDeactivated");

        if (includeVillageCode) {
            where = where.and().eq("regvillage", villageCode);
        }
        if (includeUserFilter) {
            String strfilter = "(regwomanName LIKE '%" + filter + "%'  OR regphonenumber LIKE " +
                    "'%" + filter + "%'  OR `reguidNo` LIKE '%" + filter + "%')";
            where = where.and().raw(strfilter);
           /* where = where.and().like("regwomanName", "%" + filter + "%").or()
                    .like("regphonenumber", "%" + filter + "%").or()
                    .like("reguidNo", "%" + filter + "%");*/
        }
        queryBuilder.setWhere(where);
        return queryBuilder.query();
    }

    //    20Ot2019 Arpitha
    public List<tblregisteredwomen> getDisplayMotherList(String userId) throws SQLException {
        QueryBuilder<tblregisteredwomen, Integer> queryBuilder =
                databaseHelper.gettblregRuntimeExceptionDao().queryBuilder();

        return queryBuilder.where().eq("UserId", userId).and().
                raw(" regPregnantorMother == 2 or (regPregnantorMother==1 and regGravida>1 )").query();
                /*eq("regPregnantorMother", 2)
                .or()

                .eq("regPregnantorMother",1)
                .and().gt("regGravida",1)
                .query();*/

    }

    public List<tblregisteredwomen> getWomenForCareType(QueryParams queryParams) throws SQLException {
        String noVisitClause = ""; String strvillagecode = "", strLMPConfirm = "";
        QueryBuilder<tblregisteredwomen,
                Integer> qb = databaseHelper.gettblregRuntimeExceptionDao().queryBuilder();

        //11Dec2019 -Bindu - Add village code
        boolean includeVillageCode = queryParams.selectedVillageCode != 0;

        if (queryParams.careType == CareType.ANC) {
            qb.orderBy("LastAncVisitDate", true);
            noVisitClause = " and regPregnantorMother = 1 "; // Where status is anc and no single visit done during ANC

            if (queryParams.noVisitSelected)
                noVisitClause = noVisitClause + " and (LastAncVisitDate = '' or LastAncVisitDate is null) ";

        } else if (queryParams.careType == CareType.PNC) {
            qb.orderBy("LastPncVisitDate", true);
            noVisitClause = " and regPregnantorMother = 2 "; // Where status is pnc and no single visit done during PNC
            if (queryParams.noVisitSelected)
                noVisitClause = noVisitClause + " and (LastPncVisitDate = '' or LastPncVisitDate is null) ";
        } else {
            qb.orderBy("transid", false);
        }

        //11Dec2019 - Bind where cond - add village
        if (includeVillageCode) {
            strvillagecode = " and regvillage = '"+ queryParams.selectedVillageCode +"'";
        }

        if(queryParams.isLMPNotConfirmed == true) //08Apr2021 Bindu - add lmp not confirmed
            strLMPConfirm = " and regpregnantorMother = 1 and isLMPConfirmed = 0 ";

        String strExcludeChilMort;
        strExcludeChilMort = " and ((regpregnantormother==1 and DateDeactivated is null) or (regpregnantormother ==2 and ((DateDeactivated is null and womanid   In (Select womanid from tbldeliveryinfo where delMothersdeath='N')) or " +

                "womanid   In (Select womanid from tblchildinfo where (chlDeliveryResult==1 and chlDeactDate is  null))))) " ;


//        strExcludeChilMort = " and womanid  in (select womanid from tblchildinfo where chlreg=0 and (chlDeliveryResult == 1 or chlDeactDate is null or chlDeactDate == '' ))";


        List<tblregisteredwomen> tblreg;
        if (queryParams.textViewFilter.trim().length() > 0) {
            qb.where().raw(
                    "  UserId = '" + queryParams.userId + "'  "  + strvillagecode + strLMPConfirm + noVisitClause + //08Apr2021 Bindu add strlmpconfirm
                    strExcludeChilMort +
                    //queryParams.textViewFilter + // 07Dec2019 - Bindu - comment this line
                    " and " +
                    " (`regwomanName` LIKE '%" + queryParams.textViewFilter + "%' OR `regphonenumber` LIKE '%" + queryParams.textViewFilter + "%' " +
                    " OR `regphonenumber` LIKE '%" + queryParams.textViewFilter + "%' )");
            /*qb.where().raw("(CurrentWomenStatus is null OR CurrentWomenStatus = '') " +
                    " and UserId = '" + queryParams.userId + "'  and DateDeactivated is null "  + strvillagecode + noVisitClause +
                    strExcludeChilMort +
                    //queryParams.textViewFilter + // 07Dec2019 - Bindu - comment this line
                    " and " +
                    " (`regwomanName` LIKE '%" + queryParams.textViewFilter + "%' OR `regphonenumber` LIKE '%" + queryParams.textViewFilter + "%' " +
                    " OR `regphonenumber` LIKE '%" + queryParams.textViewFilter + "%' )");*/
            tblreg = qb.query();
        } else {
            qb.where().raw(
                    " UserId = '" + queryParams.userId + "'   " +
                     strExcludeChilMort +
                    strvillagecode + strLMPConfirm + noVisitClause + //08Apr2021 Bindu add strlmpconfirm
                    queryParams.textViewFilter);



            tblreg = qb.query();

            String s = qb.prepare().toString();
            String ss = s;
        }
        return tblreg;
    }


    public List<tblregisteredwomen> getSummary(int i, String month,
                                               String strUserId, String year, int tribalHamlet) {

        List<tblregisteredwomen> arrVal = new ArrayList<>();
        String sql = "";

        String yearFilter = "";
        String monthFilter = "";
        String tribalHamletFilter = "";
        String chlYearFilter = "";
        String chlMonthFilter = "";
        String chlTribalHamletFilter = "";

        if (year != null && year.trim().length() > 0 && !(year.equalsIgnoreCase("All")))
            yearFilter = " and subStr(regRegistrationDate,7) ='" + year + "'";

        if (month != null && Integer.parseInt(month) > 0) {
            if (Integer.parseInt(month) < 10)
                monthFilter = " and subStr(regRegistrationDate,4,2) ='0" + month + "'";
            else
                monthFilter = " and subStr(regRegistrationDate,4,2) ='" + month + "'";

        }

        if (tribalHamlet > 0)
            tribalHamletFilter = " and regVillage = " + tribalHamlet;


        if (year != null && year.trim().length() > 0 && !(year.equalsIgnoreCase("All")))
            chlYearFilter = " and subStr(chlDateOfBirth,7) ='" + year + "'";

        if (month != null && Integer.parseInt(month) > 0) {
            if (Integer.parseInt(month) < 10)
                chlMonthFilter = " and subStr(chlDateOfBirth,4,2) ='0" + month + "'";
            else
                chlMonthFilter = " and subStr(chlDateOfBirth,4,2) ='" + month + "'";
        }

        if (tribalHamlet > 0)
            chlTribalHamletFilter = " and chlTribalHamlet = " + tribalHamlet;

        if (i == 1)
            sql = "select count(*) from tblregisteredwomen " +
                    " where  UserId = '" + strUserId + "' " + tribalHamletFilter + yearFilter + monthFilter;
        else if (i == 2)
            sql = "select count(*) from tblregisteredwomen where regPregormotheratreg!=2 " +
                    " and UserId = '" + strUserId + "' " + tribalHamletFilter + yearFilter + monthFilter;
        else if (i == 3)
            sql = "select count(*) from tblregisteredwomen where regPregormotheratreg==2 "
                    + " and UserId = '" + strUserId + "' " + tribalHamletFilter + yearFilter + monthFilter;

        else if (i == 4)
            sql = "select count(*) from tblregisteredwomen where regPregnantormother!=2  and " +
                    "UserId = '" + strUserId + "' and datedeactivated is null   " + tribalHamletFilter + yearFilter + monthFilter;
        else if (i == 5)
            sql = "select count(*) from tblregisteredwomen where regPregnantormother==2 and " +
                    "UserId = '" + strUserId + "'  and datedeactivated is null  " + tribalHamletFilter + yearFilter + monthFilter;
        else if (i == 6)
            sql = "select count(*) from tblregisteredwomen where " +
                    " (regComplicatedpreg==1 or isCompl = 'Y' ) "
                    + " and UserId = '" + strUserId + "'" +
                    "  and datedeactivated is null  " + tribalHamletFilter + yearFilter + monthFilter;

        else if (i == 7)
            sql = "select count(*) from tblregisteredwomen where DateDeactivated is not null"
                    + " and UserId = '" + strUserId + "' " + tribalHamletFilter + yearFilter + monthFilter;

        else if (i == 8)
            sql = "select count(*) from tblChildInfo"
                    + " where UserId = '" + strUserId + "' "
                    + chlTribalHamletFilter + chlYearFilter + chlMonthFilter;


        else if (i == 9)
            sql = "select count(*) from tblChildInfo  where chlreg=1"
                    + " and UserId = '" + strUserId + "' " + chlTribalHamletFilter + chlYearFilter + chlMonthFilter;


        else if (i == 10)
            sql = "select count(*) from tblChildInfo  where chlreg=0"
                    + " and UserId = '" + strUserId + "' " + chlTribalHamletFilter + chlYearFilter + chlMonthFilter;

        else if (i == 11)
            sql = "select count(*) from tblChildInfo  where chlDeactDate is not null"
                    + " and UserId = '" + strUserId + "' " + chlTribalHamletFilter + chlYearFilter + chlMonthFilter;


        GenericRawResults<tblregisteredwomen> tbls = databaseHelper.
                gettblregRuntimeExceptionDao().
                queryRaw(sql,
                        new RawRowMapper<tblregisteredwomen>() {
                            @Override
                            public tblregisteredwomen mapRow(String[] strings, String[] strings1) {
                                tblregisteredwomen servicesPojo = new tblregisteredwomen();
                                String s = strings[0];
                                servicesPojo.setRegRegistrationDate(strings1[0]);
                                return servicesPojo;
                            }
                        });

        for (tblregisteredwomen foo : tbls) {
            arrVal.add(foo);

        }
        return arrVal;
    }

    //  get deactivated woman list
    public List<tblregisteredwomen> getDeactivatedList(QueryParams queryParams) throws Exception {
        boolean includeVillageCode = queryParams.selectedVillageCode != 0;
        boolean includeUserFilter = queryParams.textViewFilter != null && !queryParams.textViewFilter.trim().isEmpty();

        long limit = 10;

        String[] selectColumns = {"regWomanName", "regRegistrationDate", "regStatusWhileRegistration",
                "regComplicatedpreg", "regriskFactors", "regAmberOrRedColorCode",
                "regrecommendedPlaceOfDelivery", "UserId", "WomanId", "regVillage", "regLMP", "IsCompl",
                "LastAncVisitDate", "LastPncVisitDate", "IsReferred", "regADDate", "DateDeactivated","regWomenImage","isLMPConfirmed", "regRiskFactorsByCC", "regOtherRiskByCC", "regCCConfirmRisk","regIsAdolescent"};//15Nov2019 Arpitha , 03May2021 Arpitha - add lmpnotconfirmed

        QueryBuilder<tblregisteredwomen, Integer> queryBuilder = databaseHelper.gettblregRuntimeExceptionDao().queryBuilder()
                .selectColumns(selectColumns)
                .orderBy("transId", false);

        Where<tblregisteredwomen, Integer> where = queryBuilder.where();
        where.eq("userId", queryParams.userId).and().isNotNull("DateDeactivated");
        // .and(where.eq("regComplicatedpreg", 1), where.or()
        //     .eq("iscompl", 'Y'));

        if (includeVillageCode) {
            where = where.and().eq("regvillage", queryParams.selectedVillageCode);
        }
        if (includeUserFilter) {
            where = where.and(where, where.or(where.like("regwomanName", "%" + queryParams.textViewFilter + "%"),
                    where.like("regphonenumber", "%" + queryParams.textViewFilter + "%"),
                    where.like("reguidNo", "%" + queryParams.textViewFilter + "%")));
        }
        queryBuilder.setWhere(where);
        return queryBuilder.query();
    }

    public List<tblregisteredwomen> getDeactivationData(String strWomanId, String userId) throws SQLException {


        String[] selectColumns = {"UserId", "WomanId", "DateDeactivated", "ReasonforDeactivation",
                "OtherDeactivationReason", "DeacComments", "DateTransferred",
                "DateExpired",
                "DateAborted"};


        return databaseHelper.gettblregRuntimeExceptionDao().queryBuilder()
                .selectColumns(selectColumns).where()
                .eq("WomanId", strWomanId).and().eq("UserId", userId)
                .and().isNotNull("DateDeactivated").query();
    }

    //24Nov2019 - Bindu - Complication mgmt list
    public List<tblregisteredwomen> getComplicationMgmtList(String userId,
                                                            int villageCode,
                                                            String filter,
                                                            long limitStart) throws SQLException {
        boolean includeVillageCode = villageCode != 0;
        boolean includeUserFilter = filter != null && filter.trim().length() > 0;


        long limit = 10;

        String[] selectColumns = {"regWomanName", "regRegistrationDate", "regStatusWhileRegistration",
                "regComplicatedpreg", "regriskFactors", "regAmberOrRedColorCode",
                "regrecommendedPlaceOfDelivery", "UserId", "WomanId", "regVillage", "regLMP", "IsCompl",
                "LastAncVisitDate", "LastPncVisitDate", "IsReferred", "regADDate"};

        QueryBuilder<tblregisteredwomen, Integer> queryBuilder = databaseHelper.gettblregRuntimeExceptionDao().queryBuilder()
                .selectColumns(selectColumns)
                .orderBy("transId", false)
                .offset(limitStart)
                .limit(limit);

        Where<tblregisteredwomen, Integer> where = queryBuilder.where()
                .eq("userId", userId)
                .and().isNull("DateDeactivated");

        if (includeVillageCode) {
            where = where.and().eq("regvillage", villageCode);
        }

        if (includeUserFilter) {
            where = where.and().like("regwomanName", "%" + filter + "%").or()
                    .like("regphonenumber", "%" + filter + "%").or()
                    .like("reguidNo", "%" + filter + "%");
        }
        queryBuilder.setWhere(where);
        return queryBuilder.query();
    }

    //    27Nov2019 Arpitha
    public List<String> getYears() throws SQLException {

        String sql = " select strftime('%Y',substr(chlDateOfBirth , 7) || '-' || " +
                " substr(chlDateOfBirth ,4,2)  || '-' " +
                "                      || substr(chlDateOfBirth , 1,2)) from tblchildinfo " +
                " union " +
                "  select strftime('%Y',substr(regregistrationdate , 7) || '-' || " +
                " substr(regregistrationdate ,4,2)  || '-' " +
                "                      || substr(regregistrationdate , 1,2)) from tblregisteredwomen ";

        GenericRawResults<String> result = databaseHelper.gettblregRuntimeExceptionDao().queryRaw(sql, new RawRowMapper<String>() {
            @Override
            public String mapRow(String[] strings, String[] strings1) {
                String[] servicesPojo;
                String s = strings[0];
                // servicesPojo.setRegRegistrationDate(strings1[0]);
                return strings1[0];
            }
        });

        return result.getResults();
    }


    public List<tblregisteredwomen> getDisplayMotherList(String userId,
                                                         int villageCode,
                                                         String cond,
                                                         String filter,
                                                         long limitStart, boolean all, boolean lmpnotconfirmed) throws SQLException {
        boolean includeVillageCode = villageCode != 0;
        boolean includeUserFilter = filter != null && filter.trim().length() > 0;
        boolean includePatientStatus = cond != null && cond.trim().length() > 0;


        String strvillageCode = "", strStatus = "", strFilter = "", strLMPConfirm = "";

        if (includeVillageCode) {
            strvillageCode = " and regvillage = " + villageCode;
        }

        if (includeVillageCode) {
            strvillageCode = " and regvillage = " + villageCode;
        }

        if(lmpnotconfirmed) {
            strLMPConfirm = " and regpregnantorMother = 1 and isLMPConfirmed = 0 ";
        }


        if (includePatientStatus) {
            if (cond.equalsIgnoreCase("preg")) {
                strStatus = " and regpregnantorMother = 1 and DateDeactivated is null ";
            } else if (cond.equalsIgnoreCase("pnc")) {
//                strStatus = " and regpregnantorMother = 2 and (DateDeactivated is null or (chlDeactDate is null and chlReg=0) ) "; //10Dec2019 - Check for del child
                strStatus = " and regpregnantorMother = 2 and ((DateDeactivated is null and delMothersDeath!='Y') " +
                        "or (chlReg=0 and (chlDeactDate is null   and chlDeliveryResult==1) ) ) "; //10Dec2019 - Check for del child


         /* strStatus   =  "and regpregnantorMother = 2" +

                   " and ((DateDeactivated is null and delMothersDeath!='Y') or ((chlDeactDate is null and " +
                   "chlDeliveryResult==1 )and chlReg=0) )";*/
            }
            else if (cond.equalsIgnoreCase("mother")) {
//                strStatus = " and regpregnantorMother = 2 and (DateDeactivated is null or (chlDeactDate is null and chlReg=0) ) "; //10Dec2019 - Check for del child
              /*  strStatus = " and regpregnantorMother = 2 and ((DateDeactivated is null and delMothersDeath!='Y') " +
                        "or (chlReg=0 and (chlDeactDate is null   and chlDeliveryResult==1) ) ) "; //10Dec2019 - Check for del child
*/

                strStatus = "  and (regpregnantorMother = 2 and " +
                        "((DateDeactivated is null ) or " +
                        "(chlDeactDate is null and chlReg=0 ) )) " ; //10Dec2019 - Check for del child
                      //  "or  (regpregnantorMother = 1 and (DateDeactivated is null))) ";


         /*strStatus   =  "and regpregnantorMother = 2" +

                   " and ((DateDeactivated is null and delMothersDeath!='Y') or ((chlDeactDate is null and " +
                   "chlDeliveryResult==1 )and chlReg=0) )";*/
            }
            else

              /*  strStatus   =  "and regpregnantorMother = 2" +

                        " and (((DateDeactivated is null and delMothersDeath!='Y') or ((chlDeactDate is null and " +
                        "chlDeliveryResult==1 )and chlReg=0) ) or (regpregnantorMother = 1 and (DateDeactivated is null))) ";
*/
                strStatus = "  and ((regpregnantorMother = 2 and " +
                        "((DateDeactivated is null ) or " +
                        "(chlDeactDate is null and chlReg=0 ) )) " + //10Dec2019 - Check for del child
                        "or  (regpregnantorMother = 1 and (DateDeactivated is null))) ";
               /* strStatus = "  and ((regpregnantorMother = 2 and (DateDeactivated is null or (chlDeactDate is null and chlReg=0) )) " + //10Dec2019 - Check for del child
                        "or  (regpregnantorMother = 1 and (DateDeactivated is null))) ";*/


        }

        if (includeUserFilter) {
            strFilter = " and regWomanName like '%" + filter + "%' or regphonenumber like " +
                    "'%" + filter + "%' or reguidNo like '%" + filter + "%'";
        }

        String sql;

        if (all)
            sql = " Select distinct regWomanName, regRegistrationDate, " +
                    "regStatusWhileRegistration, " +
                    " regComplicatedpreg, regAmberOrRedColorCode," +
                    " regrecommendedPlaceOfDelivery, tblregisteredwomen.UserId," +
                    "tblregisteredwomen.WomanId, regVillage, regLMP, IsCompl, " +
                    " LastAncVisitDate, LastPncVisitDate, IsReferred, regADDate,  " +
                    "regriskFactors, regPregnantorMother, regInDanger, DateDeactivated, regWomenImage , isLMPConfirmed, regRiskFactorsByCC, regOtherRiskByCC, regCCConfirmRisk ,regIsAdolescent" + //07Apr2021 Bindu add isLMPconfirmed
                    " from tblregisteredwomen left " +
                    "join tblchildinfo on tblregisteredwomen.WomanId = tblchildinfo.WomanId " +
                    " left join tbldeliveryinfo on tblregisteredwomen.WomanId = tbldeliveryinfo.WomanId "+
                    "where  tblregisteredwomen.UserId = '" + userId + "' "
                    + strvillageCode +
                    // strFilter + strStatus +"  order by tblregisteredwomen.transId desc " + " LIMIT 10 OFFSET " + (limitStart);
                    strLMPConfirm + strStatus + strFilter + "  order by tblregisteredwomen.transId desc "; //25Mar2021 Bindu add LMP confirmation
//        + " LIMIT 10 OFFSET " + (limitStart);

        else

            sql = " Select distinct regWomanName, regRegistrationDate, " +
                    "regStatusWhileRegistration, " +
                    " regComplicatedpreg, regAmberOrRedColorCode," +
                    " regrecommendedPlaceOfDelivery, tblregisteredwomen.UserId," +
                    "tblregisteredwomen.WomanId, regVillage, regLMP, IsCompl, " +
                    " LastAncVisitDate, LastPncVisitDate, IsReferred, regADDate, " +
                    " regriskFactors, regPregnantorMother , regInDanger, DateDeactivated, regWomenImage, isLMPConfirmed, regRiskFactorsByCC, regOtherRiskByCC, regCCConfirmRisk ,regIsAdolescent" + //07Apr2021 Bindu add isLMPconfirmed
                    " from tblregisteredwomen left " +
                    "join tblchildinfo on tblregisteredwomen.WomanId = tblchildinfo.WomanId " +
                    " left join tbldeliveryinfo on tblregisteredwomen.WomanId = tbldeliveryinfo.WomanId "+
                    "where  tblregisteredwomen.UserId = '" + userId + "' "
                    + strvillageCode +
                    // strFilter + strStatus +"  order by tblregisteredwomen.transId desc " + " LIMIT 10 OFFSET " + (limitStart);
                    strLMPConfirm + strStatus + strFilter + "  order by tblregisteredwomen.transId desc " + " LIMIT 10 OFFSET " + (limitStart); //25MAr2021 Bindu add lmp validation

        GenericRawResults<tblregisteredwomen> tbls =
                databaseHelper.gettblregRuntimeExceptionDao().

                        queryRaw(sql,
                                new RawRowMapper<tblregisteredwomen>() {
                                    @Override
                                    public tblregisteredwomen mapRow(String[] strings, String[] strings1) {
                                        tblregisteredwomen tblregisteredwomen = new tblregisteredwomen();
                                        tblregisteredwomen.setRegWomanName(strings1[0]);
                                        tblregisteredwomen.setRegRegistrationDate(strings1[1]);
                                        tblregisteredwomen.setRegStatusWhileRegistration(strings1[2]);
                                        if(strings1[3]!=null && strings1[3].trim().length()>0)//07Sep2021 Arpitha
                                            tblregisteredwomen.setRegComplicatedpreg(Integer.parseInt(strings1[3]));
                                        if(strings1[4]!=null && strings1[4].trim().length()>0)//07Sep2021 Arpitha
                                            tblregisteredwomen.setRegAmberOrRedColorCode(Integer.parseInt(strings1[4]));
                                        tblregisteredwomen.setRegrecommendedPlaceOfDelivery(strings1[5]);
                                        tblregisteredwomen.setUserId(strings1[6]);
                                        tblregisteredwomen.setWomanId(strings1[7]);
                                        if(strings1[8]!=null && strings1[8].trim().length()>0)//07Sep2021 Arpitha
                                            tblregisteredwomen.setRegVillage(Integer.parseInt(strings1[8]));
                                        tblregisteredwomen.setRegLMP(strings1[9]);
                                        tblregisteredwomen.setIsCompl(strings1[10]);
                                        tblregisteredwomen.setLastAncVisitDate(strings1[11]);
                                        tblregisteredwomen.setLastPncVisitDate(strings1[12]);
                                        tblregisteredwomen.setIsReferred(strings1[13]);
                                        tblregisteredwomen.setRegADDate(strings1[14]);
                                        tblregisteredwomen.setRegriskFactors(strings1[15]);
                                            if(strings1[16]!=null && strings1[16].trim().length()>0)//07Sep2021 Arpitha
                                            tblregisteredwomen.setRegPregnantorMother(Integer.parseInt(strings1[16]));

                                        if(strings1[17]!=null && strings1[17].trim().length()>0)//07Sep2021 Arpitha
                                            tblregisteredwomen.setRegInDanger(Integer.parseInt(strings1[17]));
                                        tblregisteredwomen.setDateDeactivated(strings1[18]);
                                        tblregisteredwomen.setRegWomenImage(strings1[19]);
                                        if(strings1[20]!=null && strings1[20].trim().length()>0)//07Sep2021 Arpitha
                                            tblregisteredwomen.setIsLMPConfirmed(Integer.parseInt(strings1[20])); //07Apr2021 Bindu set lmp confirmed
                                        tblregisteredwomen.setRegRiskFactorsByCC(strings1[21]);
                                        tblregisteredwomen.setRegOtherRiskByCC(strings1[22]);
                                        tblregisteredwomen.setRegCCConfirmRisk(strings1[23]);

                                        tblregisteredwomen.setRegIsAdolescent(strings1[24]);
                                        return tblregisteredwomen;
                                    }
                                });

        List<tblregisteredwomen> regList = new ArrayList<>();

        regList.addAll(tbls.getResults());

        return regList;

    }

    //02Apr2021 Bindu - Get is  isLMP confirmed - 1 then true , else false
    public boolean isWomanLMPConfirmed(String womanId) throws SQLException {
        boolean recordexists = false;
        if(databaseHelper.gettblregRuntimeExceptionDao().queryBuilder()
                .where().eq("WomanId",womanId).and().eq("isLMPConfirmed",1).countOf() == 1)
            recordexists = true;
        else
            recordexists = false;

        return recordexists;
    }

    public long getTotalWomenCount(String userId,
                                   int villageCode,
                                   String cond,
                                   String filter,
                                   boolean lmpnotconfirmed) {
        long c = 0;
        boolean includeVillageCode = villageCode != 0;
        boolean includeUserFilter = filter != null && filter.trim().length() > 0;
        boolean includePatientStatus = cond != null && !cond.equalsIgnoreCase("all");

        StringBuilder selectionArgsBuilder = new StringBuilder();
        selectionArgsBuilder.append("userId = ?");

        List<String> selectionArgsList = new ArrayList<>();
        selectionArgsList.add(userId);

        if (includeVillageCode) {
            selectionArgsBuilder.append(" AND ")
            .append("regvillage = ?");
            selectionArgsList.add(String.valueOf(villageCode));
        }
        if (includePatientStatus) {
            int code = 0;
            if (cond.equalsIgnoreCase("preg")) code = 1;
            else if (cond.equalsIgnoreCase("mother")) code = 2;

            selectionArgsBuilder.append(" AND ")
                    .append("regpregnantorMother = ?");
            selectionArgsList.add(String.valueOf(code));
        }
        //25Mar2021 Bindu add lmp not confirmed
        if (lmpnotconfirmed) {
            selectionArgsBuilder.append(" AND ")
                    .append("isLMPConfirmed = ?");
            selectionArgsList.add(String.valueOf(0));
        }

        if (includeUserFilter) {
            selectionArgsBuilder.append(" AND ")
                    .append(" (regwomanName LIKE ?")
                    .append(" OR ")
                    .append("regphonenumber LIKE ?")
                    .append(" OR ")
                    .append("reguidNo LIKE ?")
                    .append(")");
            selectionArgsList.add("%" + filter + "%");
            selectionArgsList.add("%" + filter + "%");
            selectionArgsList.add("%" + filter + "%");
        }
        String selection = selectionArgsBuilder.toString();

        String[] selectionArgs = new String[selectionArgsList.size()];
        selectionArgs = selectionArgsList.toArray(selectionArgs);

        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        c = DatabaseUtils.queryNumEntries(db, "tblregisteredwomen",
                selection,
                selectionArgs);
        return c;
    }




















    public long getWomanListCount(String userId, int villageCode,
                                                         String cond,
                                                         String filter,
                                                          boolean all, boolean lmpnotconfirmed) throws SQLException {
        boolean includeVillageCode = villageCode != 0;
        boolean includeUserFilter = filter != null && filter.trim().length() > 0;
        boolean includePatientStatus = cond != null && cond.trim().length() > 0;


        String strvillageCode = "", strStatus = "", strFilter = "", strLMPConfirm = "";

        if (includeVillageCode) {
            strvillageCode = " and regvillage = " + villageCode;
        }

        if(lmpnotconfirmed) {
            strLMPConfirm = " and regpregnantorMother = 1 and isLMPConfirmed = 0 ";
        }


        if (includePatientStatus) {
            if (cond.equalsIgnoreCase("preg")) {
                strStatus = " and regpregnantorMother = 1 and DateDeactivated is null ";
            } else if (cond.equalsIgnoreCase("pnc")) {
//                strStatus = " and regpregnantorMother = 2 and (DateDeactivated is null or (chlDeactDate is null and chlReg=0) ) "; //10Dec2019 - Check for del child
                strStatus = " and regpregnantorMother = 2 and ((DateDeactivated is null and delMothersDeath!='Y') " +
                        "or (chlReg=0 and (chlDeactDate is null   and chlDeliveryResult==1) ) ) "; //10Dec2019 - Check for del child


         /* strStatus   =  "and regpregnantorMother = 2" +

                   " and ((DateDeactivated is null and delMothersDeath!='Y') or ((chlDeactDate is null and " +
                   "chlDeliveryResult==1 )and chlReg=0) )";*/
            }
            else if (cond.equalsIgnoreCase("mother")) {
//                strStatus = " and regpregnantorMother = 2 and (DateDeactivated is null or (chlDeactDate is null and chlReg=0) ) "; //10Dec2019 - Check for del child
              /*  strStatus = " and regpregnantorMother = 2 and ((DateDeactivated is null and delMothersDeath!='Y') " +
                        "or (chlReg=0 and (chlDeactDate is null   and chlDeliveryResult==1) ) ) "; //10Dec2019 - Check for del child
*/

                strStatus = "  and (regpregnantorMother = 2 and " +
                        "((DateDeactivated is null ) or " +
                        "(chlDeactDate is null and chlReg=0 ) )) " ; //10Dec2019 - Check for del child
                //  "or  (regpregnantorMother = 1 and (DateDeactivated is null))) ";


         /*strStatus   =  "and regpregnantorMother = 2" +

                   " and ((DateDeactivated is null and delMothersDeath!='Y') or ((chlDeactDate is null and " +
                   "chlDeliveryResult==1 )and chlReg=0) )";*/
            }
            else

              /*  strStatus   =  "and regpregnantorMother = 2" +

                        " and (((DateDeactivated is null and delMothersDeath!='Y') or ((chlDeactDate is null and " +
                        "chlDeliveryResult==1 )and chlReg=0) ) or (regpregnantorMother = 1 and (DateDeactivated is null))) ";
*/
                strStatus = "  and ((regpregnantorMother = 2 and " +
                        "((DateDeactivated is null ) or " +
                        "(chlDeactDate is null and chlReg=0 ) )) " + //10Dec2019 - Check for del child
                        "or  (regpregnantorMother = 1 and (DateDeactivated is null))) ";
               /* strStatus = "  and ((regpregnantorMother = 2 and (DateDeactivated is null or (chlDeactDate is null and chlReg=0) )) " + //10Dec2019 - Check for del child
                        "or  (regpregnantorMother = 1 and (DateDeactivated is null))) ";*/


        }

        if (includeUserFilter) {
            strFilter = " and regWomanName like '%" + filter + "%' or regphonenumber like " +
                    "'%" + filter + "%' or reguidNo like '%" + filter + "%'";
        }

        String sql;

        if (all)
            sql = " Select distinct regWomanName, regRegistrationDate, " +
                    "regStatusWhileRegistration, " +
                    " regComplicatedpreg, regAmberOrRedColorCode," +
                    " regrecommendedPlaceOfDelivery, tblregisteredwomen.UserId," +
                    "tblregisteredwomen.WomanId, regVillage, regLMP, IsCompl, " +
                    " LastAncVisitDate, LastPncVisitDate, IsReferred, regADDate,  " +
                    "regriskFactors, regPregnantorMother, regInDanger, DateDeactivated, regWomenImage , isLMPConfirmed" + //07Apr2021 Bindu add isLMPconfirmed
                    " from tblregisteredwomen left " +
                    "join tblchildinfo on tblregisteredwomen.WomanId = tblchildinfo.WomanId " +
                    " left join tbldeliveryinfo on tblregisteredwomen.WomanId = tbldeliveryinfo.WomanId "+
                    "where  tblregisteredwomen.UserId = '" + userId + "' "
                    + strvillageCode +
                    // strFilter + strStatus +"  order by tblregisteredwomen.transId desc " + " LIMIT 10 OFFSET " + (limitStart);
                    strLMPConfirm + strStatus + strFilter + "  order by tblregisteredwomen.transId desc "; //25Mar2021 Bindu add LMP confirmation
//        + " LIMIT 10 OFFSET " + (limitStart);

        else

            sql = " Select distinct regWomanName, regRegistrationDate, " +
                    "regStatusWhileRegistration, " +
                    " regComplicatedpreg, regAmberOrRedColorCode," +
                    " regrecommendedPlaceOfDelivery, tblregisteredwomen.UserId," +
                    "tblregisteredwomen.WomanId, regVillage, regLMP, IsCompl, " +
                    " LastAncVisitDate, LastPncVisitDate, IsReferred, regADDate, " +
                    " regriskFactors, regPregnantorMother , regInDanger, DateDeactivated, regWomenImage, isLMPConfirmed" + //07Apr2021 Bindu add isLMPconfirmed
                    " from tblregisteredwomen left " +
                    "join tblchildinfo on tblregisteredwomen.WomanId = tblchildinfo.WomanId " +
                    " left join tbldeliveryinfo on tblregisteredwomen.WomanId = tbldeliveryinfo.WomanId "+
                    "where  tblregisteredwomen.UserId = '" + userId + "' "
                    + strvillageCode +
                    // strFilter + strStatus +"  order by tblregisteredwomen.transId desc " + " LIMIT 10 OFFSET " + (limitStart);
                    strLMPConfirm + strStatus + strFilter + "  order by tblregisteredwomen.transId desc " ;//+ " LIMIT 10 OFFSET " + (limitStart); //25MAr2021 Bindu add lmp validation


        /*enericRawResults<String[]> val = databaseHelper.gettblregRuntimeExceptionDao().

                queryRaw(sql, null);


        return  val.getResults().size();*/
        GenericRawResults<tblregisteredwomen> tbls =
                databaseHelper.gettblregRuntimeExceptionDao().

                        queryRaw(sql,
                                new RawRowMapper<tblregisteredwomen>() {
                                    @Override
                                    public tblregisteredwomen mapRow(String[] strings, String[] strings1) {
                                        tblregisteredwomen tblregisteredwomen = new tblregisteredwomen();
                                        tblregisteredwomen.setRegWomanName(strings1[0]);
                                        tblregisteredwomen.setRegRegistrationDate(strings1[1]);
                                        tblregisteredwomen.setRegStatusWhileRegistration(strings1[2]);
                                        if(strings1[3]!=null && strings1[3].trim().length()>0)//07Sep2021 Arpitha
                                        tblregisteredwomen.setRegComplicatedpreg(Integer.parseInt(strings1[3]));
                                        if(strings1[4]!=null && strings1[4].trim().length()>0)//07Sep2021 Arpitha
                                            tblregisteredwomen.setRegAmberOrRedColorCode(Integer.parseInt(strings1[4]));
                                        tblregisteredwomen.setRegrecommendedPlaceOfDelivery(strings1[5]);
                                        tblregisteredwomen.setUserId(strings1[6]);
                                        tblregisteredwomen.setWomanId(strings1[7]);
                                        if(strings1[8]!=null && strings1[8].trim().length()>0)//07Sep2021 Arpitha
                                            tblregisteredwomen.setRegVillage(Integer.parseInt(strings1[8]));
                                        tblregisteredwomen.setRegLMP(strings1[9]);
                                        tblregisteredwomen.setIsCompl(strings1[10]);
                                        tblregisteredwomen.setLastAncVisitDate(strings1[11]);
                                        tblregisteredwomen.setLastPncVisitDate(strings1[12]);
                                        tblregisteredwomen.setIsReferred(strings1[13]);
                                        tblregisteredwomen.setRegADDate(strings1[14]);
                                        tblregisteredwomen.setRegriskFactors(strings1[15]);
                                        if(strings1[16]!=null && strings1[16].trim().length()>0)//07Sep2021 Arpitha
                                            tblregisteredwomen.setRegPregnantorMother(Integer.parseInt(strings1[16]));
                                        if(strings1[17]!=null && strings1[17].trim().length()>0)//07Sep2021 Arpitha
                                        tblregisteredwomen.setRegInDanger(Integer.parseInt(strings1[17]));
                                        tblregisteredwomen.setDateDeactivated(strings1[18]);
                                        tblregisteredwomen.setRegWomenImage(strings1[19]);
                                        if(strings1[20]!=null && strings1[20].trim().length()>0)//07Sep2021 Arpitha
                                            tblregisteredwomen.setIsLMPConfirmed(Integer.parseInt(strings1[20])); //07Apr2021 Bindu set lmp confirmed
                                        return tblregisteredwomen;
                                    }
                                });

        List<tblregisteredwomen> regList = new ArrayList<>();

        regList.addAll(tbls.getResults());

        return regList.size();

    }

//    02Sep2021 Arpitha
public int getPregnantCount(String strAdharNo) {
    int count = 0;
    try {
        RuntimeExceptionDao<tblregisteredwomen, Integer> regDAO = databaseHelper.gettblregRuntimeExceptionDao();
        count = (int) regDAO.queryBuilder().where().eq("regAadharCard",
                strAdharNo).countOf();
    } catch (SQLException e) {
        // TODO add logging framework

        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
        crashlytics.log(e.getMessage());
    }

    return count;
}

//11Sep2021 Arpitha - get preg history list
    public List<tblregisteredwomen> getPregList(String adharId,int limit) throws SQLException {
       return databaseHelper.gettblregRuntimeExceptionDao().queryBuilder().limit(10L).offset((long) limit).where().
               eq("regAadharCard",
                adharId).and().isNotNull("DateDeactivated")
                .query();

    }

//    09Sep2021 Arpitha
    public  List<tblregisteredwomen> getExistingAadharIds() throws SQLException {
          return databaseHelper.gettblregRuntimeExceptionDao().queryBuilder()
                  .selectColumns("regAadharCard","regStatusWhileRegistration").where()
                  .isNull("DateDeactivated").query();
    }
}