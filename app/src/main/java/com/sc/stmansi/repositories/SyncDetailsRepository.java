package com.sc.stmansi.repositories;

import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.tables.tblSyncDetails;

import java.sql.SQLException;
import java.util.List;


public class SyncDetailsRepository {
    private DatabaseHelper databaseHelper;

    public SyncDetailsRepository(DatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }


    public void insertDate(tblSyncDetails tblSyncDetails) {
        int inserted = this.databaseHelper.getSyncDetailsRuntimeExceptionDao().create(tblSyncDetails);
        if (inserted>0){
            // data inserted
        }else {
            //not inserted
        }
    }

    public String getLastDate() throws SQLException {
            List<tblSyncDetails> data = this.databaseHelper.getSyncDetailsRuntimeExceptionDao().queryBuilder().distinct().orderBy("autoID", false).query();
            if (data.size()>0){
                return data.get(0).getLastSyncDate() ;
            }
            return "";
    }
}
