package com.sc.stmansi.repositories;

import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.tables.tblImageStore;

import java.util.ArrayList;

public class ImageStoreRepository {
    DatabaseHelper databaseHelper;

    public ImageStoreRepository(DatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }

    public ArrayList<tblImageStore> getAllImagesofIDandHomeVisit(String str) throws Exception{
        QueryBuilder<tblImageStore, Integer> querybuilder = this.databaseHelper.getTblImageStoreDao().queryBuilder();
        return (ArrayList<tblImageStore>) querybuilder.where().like("ImageName","%"+str+"%").query();
    }
    public int updateImageStatusof(String imageName) throws Exception{
        UpdateBuilder<tblImageStore, Integer> updatebuilder = this.databaseHelper.getTblImageStoreDao().updateBuilder();
        updatebuilder.updateColumnValue("ImagePath","Uploaded").where().eq("ImageName",imageName);
        return updatebuilder.update();
    }

    public ArrayList<tblImageStore> getAllImagesNotUploaded() throws Exception{
        QueryBuilder<tblImageStore, Integer> querybuilder = this.databaseHelper.getTblImageStoreDao().queryBuilder();
        return (ArrayList<tblImageStore>) querybuilder.where().eq("ImagePath","").query();
    }
}
