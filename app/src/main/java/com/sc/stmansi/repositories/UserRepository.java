package com.sc.stmansi.repositories;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.tables.TblInstusers;

import java.sql.SQLException;
import java.util.List;

public class UserRepository {

	private DatabaseHelper databaseHelper;

	public UserRepository(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	public UserAddResult add(String emailId, String userIdReturned,
                             String encrypt, String tabPhn) throws SQLException {

		TblInstusers user = new TblInstusers();
		user.setAshaId(emailId);
		user.setUserId(userIdReturned);
		user.setPassword(encrypt);
		user.setDevicePhnNumber(tabPhn);

		RuntimeExceptionDao<TblInstusers, Integer> usersDAO = this.databaseHelper.getUsersRuntimeExceptionDao();
		int add = usersDAO.create(user);

		UserAddResult userAddResult = new UserAddResult();
		userAddResult.rowsAffected = add;
		userAddResult.user = user;

		return userAddResult;
	}

	public TblInstusers getAuditedUsers() throws SQLException {
		return this.databaseHelper.getUsersRuntimeExceptionDao().queryBuilder().query().get(0);
	}

	//	add where condition,
	//	Arpitha 26Nov2019 add isDeactivated and inst id and als set iser deactivated if inst is deactivated
	public TblInstusers getOneAuditedUser(String ashaId) throws SQLException { //24Nov2019 - Bindu - added - state and district //Ramesh 29-6-2021 added userrole
		PreparedQuery<TblInstusers> preparedQuery = this.databaseHelper.getUsersRuntimeExceptionDao()
				.queryBuilder().selectColumns("UserId", "AshaId", "Password","UserRole", "DevicePhnNumber", "LastTransNumber",
						"LastWomennumber","LastAdolNumber", "LastRequestNumber", "UserName","State","District","SubDistrict",
						"EmailId", "FacilityName", "Inst_Database","isDeactivated","InstituteId","UserMobileNumber") //04Dec2019 - Bindu - added user mob no
				.where().eq("AshaId", ashaId).prepare();
		TblInstusers user = this.databaseHelper.getUsersRuntimeExceptionDao().query(preparedQuery).get(0);

		if(user!=null && user.getInstituteId()!=null) {
			int deact = databaseHelper.getInstDetailsRuntimeExceptionDao().queryBuilder()
					.selectColumns("isDeactivated").where()
					.eq("InstituteId", user.getInstituteId())
					.query().get(0).getIsDeactivated();
			if (deact == 1)
				user.setIsDeactivated(deact);
		}
		return user;
	}

	public String getUpdateStatementForUserPhoneNumber(TblInstusers user) throws SQLException {
		return this.databaseHelper.getUsersRuntimeExceptionDao().updateBuilder().updateColumnValue
				("DevicePhnNumber", user.getDevicePhnNumber())
				.where().eq("AshaId", user.getAshaId()).prepare()
				.toString().replace("MappedStatement:", "");
	}

	public void updateTabletPhoneNumber(String ashaId, String phoneNumber) throws SQLException {
		RuntimeExceptionDao<TblInstusers, Integer> usersDAO = this.databaseHelper.getUsersRuntimeExceptionDao();
		UpdateBuilder<TblInstusers, Integer> updateBuilder =
				usersDAO.updateBuilder();
		updateBuilder.updateColumnValue("DevicePhnNumber",
				phoneNumber)
				.where()
				.eq("AshaId", ashaId);
		updateBuilder.update();
	}

	// Get LastWomenNumber, Add 1 and Generate the New WomenId
	public String getNewWomanId(TblInstusers user) throws SQLException {
		String returnValue;

		RuntimeExceptionDao<TblInstusers, Integer> usersDAO = databaseHelper.getUsersRuntimeExceptionDao();

		List<TblInstusers> list = usersDAO.queryBuilder().selectColumns("LastWomenNumber")
				.where().eq("userId", user.getUserId()).query();
		int lastwomenno = 0;
		if (list != null && list.size() > 0)
			lastwomenno = user.getLastWomennumber();

		lastwomenno = lastwomenno + 1;

		//05Aug2019 - Bindu - change to  from  ,  and also WID - UID+0000 + upto 9999 users canbe created
		if (lastwomenno <= 9) {
			returnValue = user.getUserId() + "000" + lastwomenno;
		} else if (lastwomenno <= 99) {
			returnValue = user.getUserId() + "00" + lastwomenno;  //05Aug2019 - Bindu - change to 4 from ,
		} else if (lastwomenno <= 999) {
			returnValue = user.getUserId() + "0" + lastwomenno;  //05Aug2019 - Bindu - change to 4 from ,
		} else {
			returnValue = user.getUserId() + lastwomenno;
		}
		return returnValue;
	}

	public String getNewAdolId(TblInstusers user) throws SQLException {
		String returnValue;

		RuntimeExceptionDao<TblInstusers, Integer> usersDAO = databaseHelper.getUsersRuntimeExceptionDao();

		List<TblInstusers> list = usersDAO.queryBuilder().selectColumns("LastAdolNumber")
				.where().eq("userId", user.getUserId()).query();
		int lastwomenno = 0;
		if (list != null && list.size() > 0)
			lastwomenno = user.getLastAdolNumber();

		lastwomenno = lastwomenno + 1;

		//05Aug2019 - Bindu - change to  from  ,  and also WID - UID+0000 + upto 9999 users canbe created
		if (lastwomenno <= 9) {
			returnValue = user.getUserId() + "A" + "000" + lastwomenno; //22MAy2021 Bindu add A to Adol ID
		} else if (lastwomenno <= 99) {
			returnValue = user.getUserId() + "A" + "00" + lastwomenno;  //05Aug2019 - Bindu - change to 4 from ,
		} else if (lastwomenno <= 999) {
			returnValue = user.getUserId() + "A" + "0" + lastwomenno;  //05Aug2019 - Bindu - change to 4 from ,
		} else {
			returnValue = user.getUserId() + "A" + lastwomenno;
		}
		return returnValue;
	}

	public String UpdateLastAdolNumberNew(String anmId, int transId, DatabaseHelper newdb1) {
		try {

			TblInstusers users = new TblInstusers();

			int LastWomanNumber = getIntUsersValueNew(anmId, "LastAdolNumber", newdb1);
			int WomanNum = LastWomanNumber + 1;

			RuntimeExceptionDao<TblInstusers, Integer> usersDAO = this.databaseHelper.getUsersRuntimeExceptionDao();
			UpdateBuilder<TblInstusers, Integer> updateBuilder = usersDAO.updateBuilder();


			updateBuilder.updateColumnValue("LastAdolNumber", WomanNum)
					.where().eq("userId", anmId);
			updateBuilder.update();
			String abc = usersDAO.updateBuilder().updateColumnValue("LastAdolNumber", WomanNum)
					.where().eq("userId", anmId).prepare().toString().replace("MappedStatement:", "");
		/*	iUpdateRecordTransSram(anmId, transId, "tblinstusers", abc.trim(),
					new String[]{anmId},
					new Object[]{WomanNum, anmId}, newdb1);
*/
			TransactionHeaderRepository transactionHeaderRepository = new TransactionHeaderRepository(databaseHelper);
			transactionHeaderRepository.updateNew(anmId, transId, "tblinstusers",
					abc.trim(), new String[]{anmId},
					new Object[]{WomanNum, anmId}, newdb1);

			return abc;
		} catch (Exception e) {

			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
			crashlytics.log(e.getMessage());
			return "";
		}

	}


	//    update last women number with transaction
	public String UpdateLastWomenNumberNew(String anmId, int transId, DatabaseHelper newdb1) {
		try {

			TblInstusers users = new TblInstusers();

			int LastWomanNumber = getIntUsersValueNew(anmId, "LastWomenNumber", newdb1);
			int WomanNum = LastWomanNumber + 1;

			RuntimeExceptionDao<TblInstusers, Integer> usersDAO = this.databaseHelper.getUsersRuntimeExceptionDao();
			UpdateBuilder<TblInstusers, Integer> updateBuilder = usersDAO.updateBuilder();


			updateBuilder.updateColumnValue("LastWomenNumber", WomanNum)
					.where().eq("userId", anmId);
			updateBuilder.update();
			String abc = usersDAO.updateBuilder().updateColumnValue("LastWomenNumber", WomanNum)
					.where().eq("userId", anmId).prepare().toString().replace("MappedStatement:", "");
		/*	iUpdateRecordTransSram(anmId, transId, "tblinstusers", abc.trim(),
					new String[]{anmId},
					new Object[]{WomanNum, anmId}, newdb1);
*/
			TransactionHeaderRepository transactionHeaderRepository = new TransactionHeaderRepository(databaseHelper);
			transactionHeaderRepository.updateNew(anmId, transId, "tblinstusers",
					abc.trim(), new String[]{anmId},
					new Object[]{WomanNum, anmId}, newdb1);

			return abc;
		} catch (Exception e) {

			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
			return "";
		}

	}

	// get A value from users Table - with transaction
	public int getIntUsersValueNew(String UserId, String ColumnName, DatabaseHelper newdb1) throws Exception {

		RuntimeExceptionDao<TblInstusers, Integer> usersDAO = this.databaseHelper.getUsersRuntimeExceptionDao();
		usersDAO = newdb1.getUsersRuntimeExceptionDao();
		int returnval = 0;
		List<TblInstusers> numRows = usersDAO.queryForAll();
		if (numRows != null && numRows.size() > 0) {
			if (ColumnName.equalsIgnoreCase("LastTransNumber"))
				returnval = TblInstusers.getLastTransNumber();
			else if (ColumnName.equalsIgnoreCase("LastWomennumber"))
				returnval = TblInstusers.getLastWomennumber();
			else if (ColumnName.equalsIgnoreCase("LastRequestNumber"))
				returnval = TblInstusers.getLastRequestNumber();
			else if (ColumnName.equalsIgnoreCase("LastParentNumber"))
				returnval = TblInstusers.getLastParentNumber();
			else if (ColumnName.equalsIgnoreCase("LastAdolNumber"))
				returnval = TblInstusers.getLastAdolNumber();
		}
		return returnval;
	}

	public TblInstusers getUserWithInstitutionDatabaseName(String ashaId) throws SQLException {
		return this.databaseHelper.getUsersRuntimeExceptionDao()
				.queryBuilder()
				.selectColumns("Inst_Database")
				.where()
				.eq("AshaId", ashaId)
				.queryForFirst();
	}

	public TblInstusers getUserWithUserId(String userId) throws SQLException {
		PreparedQuery<TblInstusers> preparedQuery = this.databaseHelper.getUsersRuntimeExceptionDao()
				.queryBuilder().selectColumns("UserId", "AshaId", "Password", "DevicePhnNumber", "LastTransNumber",
						"LastWomennumber", "LastRequestNumber", "UserName", "SubDistrict",
						"EmailId", "FacilityName", "Inst_Database")
				.where().eq("UserId", userId).prepare();
		return this.databaseHelper.getUsersRuntimeExceptionDao().queryForFirst(preparedQuery);
	}


	// Get LastChildNumber, Add 1 and Generate the New ChildId
	public String getNewChildParentId(String userId) throws SQLException {
		String returnValue;

		/*  Dao<TblChlParentDetails, Integer> childDAO =*/
		/*             databaseHelper.getChlParentRuntimeExceptionDao();*/


		RuntimeExceptionDao<TblInstusers, Integer> usersDAO = databaseHelper.getUsersRuntimeExceptionDao();

		List<TblInstusers> list = usersDAO.queryBuilder().selectColumns("LastParentNumber")
				.where().eq("userId", userId).query();
		int lastParentno = 0;
		if (list != null && list.size() > 0)
			lastParentno = list.get(0).getLastParentNumber();



      /*  List<TblChlParentDetails> list = childDAO.queryBuilder().selectColumns("chlParentId").query();
        int lastChildNo = 0;
        if (list != null && list.size() > 0)
            lastChildNo = list.size();*/

		lastParentno = lastParentno + 1;

		if (lastParentno <= 9) {
			returnValue = userId + "P" + "000" + lastParentno;
		} else if (lastParentno <= 99) {
			returnValue = userId + "P" + "00" + lastParentno;
		} else if (lastParentno <= 999) {
			returnValue = userId + "P" + "0" + lastParentno;
		} else {
			returnValue = userId + "P" + lastParentno;
		}
		return returnValue;
	}



	//    update last women number with transaction
	public String UpdateLastParentNumberNew(String anmId, int transId, DatabaseHelper newdb1) {
		try {

			TblInstusers users = new TblInstusers();

			int LastWomanNumber = getIntUsersValueNew(anmId, "LastParentNumber", newdb1);
			int WomanNum = LastWomanNumber + 1;

			RuntimeExceptionDao<TblInstusers, Integer> usersDAO = this.databaseHelper.getUsersRuntimeExceptionDao();
			UpdateBuilder<TblInstusers, Integer> updateBuilder = usersDAO.updateBuilder();


			updateBuilder.updateColumnValue("LastParentNumber", WomanNum)
					.where().eq("userId", anmId);
			updateBuilder.update();
			String abc = usersDAO.updateBuilder().updateColumnValue("LastParentNumber", WomanNum)
					.where().eq("userId", anmId).prepare().toString().replace("MappedStatement:", "");
		/*	iUpdateRecordTransSram(anmId, transId, "tblinstusers", abc.trim(),
					new String[]{anmId},
					new Object[]{WomanNum, anmId}, newdb1);
*/
			TransactionHeaderRepository transactionHeaderRepository = new TransactionHeaderRepository(databaseHelper);
			transactionHeaderRepository.updateNew(anmId, transId, "tblinstusers",
					abc.trim(), new String[]{anmId},
					new Object[]{WomanNum, anmId}, newdb1);

			return abc;
		} catch (Exception e) {

			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
			return "";
		}

	}


	public static int updateNewPassword(String userId,
											String upSql,
										int transId,

										DatabaseHelper newDb) throws SQLException {

		int update = 0;
		update = newDb.getUsersRuntimeExceptionDao().updateRaw(upSql);


		TransactionHeaderRepository transactionHeaderRepository = new TransactionHeaderRepository(newDb);
		transactionHeaderRepository.updateNew(userId, transId,
				newDb.getUsersRuntimeExceptionDao().getTableName(),
				upSql, null,null,newDb);

		return update;
	}

}