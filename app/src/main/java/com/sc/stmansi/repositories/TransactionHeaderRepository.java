package com.sc.stmansi.repositories;

import android.annotation.SuppressLint;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tbltransHeader;
import com.sc.stmansi.tables.tbltransdetail;

import java.sql.SQLException;
import java.util.List;

public class TransactionHeaderRepository {

	private DatabaseHelper databaseHelper;

	public TransactionHeaderRepository(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	public int iCreateNewTrans(String userId, DatabaseHelper dbHelper) {
		try {
			int LastTransNumber;
			int NewTransNumber;
			String TodaysDateString = DateTimeUtil.getTodaysDate();

			LastTransNumber = getIntUsersValue(userId, "LastTransNumber");
			NewTransNumber = LastTransNumber + 1;

			tbltransHeader transHeader = new tbltransHeader();
			transHeader.setRequestId("0");
			transHeader.setTransDate(TodaysDateString);
			transHeader.setTransId("" + NewTransNumber);
			transHeader.setTransStatus("N");
			dbHelper.getTransHeaderRuntimeExceptionDao().create(transHeader);

			Object[] arr2 = {NewTransNumber, userId};

			RuntimeExceptionDao<TblInstusers, Integer> usersDAO = dbHelper.getUsersRuntimeExceptionDao();

			UpdateBuilder<TblInstusers, Integer> updateBuilder = usersDAO.updateBuilder();
			updateBuilder.updateColumnValue("LastTransNumber", NewTransNumber).where()
					.eq("userId", userId);
			updateBuilder.update();

			String abc = usersDAO.updateBuilder().updateColumnValue("LastTransNumber", NewTransNumber).where()
					.eq("userId", userId).prepare().toString().replace("MappedStatement:", "");

			updateNew(userId, NewTransNumber, "tblinstusers", abc.trim(),null, arr2,dbHelper);

			return NewTransNumber;
		} catch (Exception e) {
			//TODO add logging framework

			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
			return 0;
		}
	}

	public int getIntUsersValue(String userId, String ColumnName) throws SQLException {
		UserRepository userRepository = new UserRepository(databaseHelper);
		TblInstusers user = userRepository.getUserWithUserId(userId);
		int returnval = 0;

		if (user != null) {
			if (ColumnName.equalsIgnoreCase("LastTransNumber"))
				returnval = user.getLastTransNumber();
			else if (ColumnName.equalsIgnoreCase("LastWomennumber"))
				returnval = user.getLastWomennumber();
			else if (ColumnName.equalsIgnoreCase("LastRequestNumber"))
				returnval = user.getLastRequestNumber();
		}
		return returnval;
	}

	// Update transaction table
	@SuppressLint("DefaultLocale")
	public boolean update(String userId, int transId,
						  String SQLStatement,
						  Object[] arr3) {
		try {
			String WHERECondition;
			String SQLStr = null;

			int idx = SQLStatement.toLowerCase().indexOf("WHERE".toLowerCase());
			if (idx < 0) {
				SQLStatement = SQLStatement;
			} else {
				WHERECondition = SQLStatement.substring(idx);
			}


			RuntimeExceptionDao<TblInstusers, Integer> usersDAO = this.databaseHelper.getUsersRuntimeExceptionDao();
			int NoofRecords = (int) usersDAO.queryBuilder().countOf();

			if (NoofRecords > 0) {
				String str = SQLStatement;
				String stemp = "";
				int i = 0;

				if (str.contains("?")) {
					while (str.length() > 2) {
						if (arr3 != null && arr3.length > 0 && i < arr3.length) {
							stemp = stemp + str.substring(0, str.indexOf("?") + 1);
							Object obj = arr3[i++];
							if (obj != null) {
								if (obj instanceof String)
									stemp = stemp.replace("?", "'" + obj.toString() + "'");
								else
									stemp = stemp.replace("?", " " + obj.toString() + " ");
								str = str.substring(str.indexOf("?") + 1);
							} else return false;
						} else {
//                            return false;
							stemp = SQLStatement;
							break;
						}
					}
				} else {
					stemp = SQLStatement;
				}
				System.out.println(str + "\n");
				System.out.println(stemp);

				RuntimeExceptionDao<tbltransdetail, Integer> transdetailDAO = this.databaseHelper.getTransDetailsRuntimeExceptionDao();

				tbltransdetail transdetail = new tbltransdetail();
				transdetail.setAction("U");
				transdetail.setSQLStatement(SQLStatement);
				transdetail.setTableName("tblinstusers");
				transdetail.setTransId("" + transId);
				transdetail.setUserId(userId);
				transdetailDAO.create(transdetail);

				return true;
			}
		} catch (Exception e) {
			// TODO add logging framework

			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
			return false;
		}
		return true;
	}

	public boolean iNewRecordTrans(String userId, int transId, String tableName, DatabaseHelper dbHelper) {
		try {
			RuntimeExceptionDao<tbltransdetail, Integer> transdetailDAO = dbHelper.getTransDetailsRuntimeExceptionDao();

			tbltransdetail transdetail = new tbltransdetail();
			transdetail.setAction("I");
			transdetail.setSQLStatement("");
			transdetail.setTableName(tableName);
			transdetail.setTransId("" + transId);
			transdetail.setUserId(userId);
			transdetailDAO.create(transdetail);

			return true;

		} catch (Exception E) {
			// TODO add logging framework
			return false;
		}
	}

	// Update transaction table - with transaction
	@SuppressLint("DefaultLocale")
	public  boolean updateNew(String userId, int transId,
												 String tableName, String SQLStatement,
												 String[] arr2,
												 Object[] arr3, DatabaseHelper newDb) {
		try {

			String WHERECondition;
			String SQLStr = null;

			int idx = SQLStatement.toLowerCase().indexOf("WHERE".toLowerCase());
			if (idx < 0) {

//                return false;
				SQLStatement = SQLStatement;
			} else
				WHERECondition = SQLStatement.substring(idx);


			 RuntimeExceptionDao<TblInstusers, Integer>  usersDAO = newDb.getUsersRuntimeExceptionDao();
			int NoofRecords = (int) usersDAO.queryBuilder().countOf();

			if (NoofRecords > 0) {
				String str = SQLStatement;
				String stemp = "";
				int i = 0;

				if (str.contains("?")) {
					while (str.length() > 2) {
						if (arr3 != null && arr3.length > 0 && i < arr3.length) {
							stemp = stemp + str.substring(0, str.indexOf("?") + 1);
							Object obj = arr3[i++];
							if (obj != null) {
								if (obj instanceof String)
									stemp = stemp.replace("?", "'" + obj.toString() + "'");
								else
									stemp = stemp.replace("?", " " + obj.toString() + " ");
								str = str.substring(str.indexOf("?") + 1);
							} else return false;
						} else {
//                            return false;
							stemp = SQLStatement;
							break;
						}
					}
				} else {
					stemp = SQLStatement;
				}
				System.out.println(str + "\n");
				System.out.println(stemp);

				RuntimeExceptionDao<tbltransdetail, Integer> transdetailDAO = newDb.getTransDetailsRuntimeExceptionDao();

				tbltransdetail transdetail = new tbltransdetail();
				transdetail.setAction("U");
				transdetail.setSQLStatement(SQLStatement);
				transdetail.setTableName(tableName);
				transdetail.setTransId("" + transId);
				transdetail.setUserId(userId);
				transdetailDAO.create(transdetail);

				return true;
			}
		} catch (Exception e) {

			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
			return false;
		}
		return true;
	}


	// Add new Record to Trans Table - with transaction
	public static boolean iNewRecordTransNew(String userId, int transId, String tableName, DatabaseHelper newdb1) {

		try {

			RuntimeExceptionDao<tbltransdetail, Integer>  transdetailDAO = newdb1.getTransDetailsRuntimeExceptionDao();

			tbltransdetail transdetail = new tbltransdetail();
			transdetail.setAction("I");
			transdetail.setSQLStatement("");
			transdetail.setTableName(tableName);
			transdetail.setTransId("" + transId);
			transdetail.setUserId(userId);
			transdetailDAO.create(transdetail);

			return true;

		} catch (Exception e) {
			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
			return false;
		}

	}

	// Add new Transaction - with trasaction - 18Sep2019 Arpitha
	public static int iCreateNewTransNew(String userId, DatabaseHelper newdbConnection) {


		try {
			int LastTransNumber = 0;
			int NewTransNumber = 0;
			String TodaysDateString = DateTimeUtil.getTodaysDate();

			LastTransNumber = getIntUsersValueNew(userId, "LastTransNumber", newdbConnection);
			NewTransNumber = LastTransNumber + 1;


			RuntimeExceptionDao<tbltransHeader, Integer> transheaderDAO = newdbConnection.getTransHeaderRuntimeExceptionDao();
			tbltransHeader transHeader = new tbltransHeader();
			transHeader.setRequestId("0");
			transHeader.setTransDate(TodaysDateString);
			transHeader.setTransId("" + NewTransNumber);
			transHeader.setTransStatus("N");
			transheaderDAO.create(transHeader);


			Object[] arr2 = {NewTransNumber, userId};


			RuntimeExceptionDao<TblInstusers, Integer>  usersDAO = newdbConnection.getUsersRuntimeExceptionDao();

			TblInstusers users = new TblInstusers();

			UpdateBuilder<TblInstusers, Integer> updateBuilder = usersDAO.updateBuilder();
			updateBuilder.updateColumnValue("LastTransNumber", NewTransNumber).where()
					.eq("userId", userId);
			updateBuilder.update();

			String abc = usersDAO.updateBuilder().updateColumnValue("LastTransNumber", NewTransNumber).where()
					.eq("userId", userId).prepare().toString().replace("MappedStatement:", "");

			iUpdateRecordTransNew(userId, NewTransNumber, "tblinstusers", abc.trim(),
					new String[]{userId},
					arr2, newdbConnection);


			return NewTransNumber;
		} catch (Exception E) {
			return 0;
		}
	}

	// get A value from users Table
	public static int getIntUsersValueNew(String UserId, String ColumnName, DatabaseHelper newdb1) throws Exception {

		RuntimeExceptionDao<TblInstusers, Integer> usersDAO = newdb1.getUsersRuntimeExceptionDao();
		int returnval = 0;
		List<TblInstusers> numRows = usersDAO.queryForAll();
		if (numRows != null && numRows.size() > 0) {
			if (ColumnName.equalsIgnoreCase("LastTransNumber"))
				returnval = TblInstusers.getLastTransNumber();
			else if (ColumnName.equalsIgnoreCase("LastWomennumber"))
				returnval = TblInstusers.getLastWomennumber();
			else if (ColumnName.equalsIgnoreCase("LastRequestNumber"))
				returnval = TblInstusers.getLastRequestNumber();
		}
		return returnval;
	}

	// Update transaction table
	@SuppressLint("DefaultLocale")
	public static boolean iUpdateRecordTransNew(String userId, int transId,
												String tableName, String SQLStatement,
												String[] arr2,
												Object[] arr3, DatabaseHelper newdb1) {
		try {

			String WHERECondition;
			String SQLStr = null;

			int idx = SQLStatement.toLowerCase().indexOf("WHERE".toLowerCase());
			if (idx < 0) {

//                return false;
				SQLStatement = SQLStatement;
			} else
				WHERECondition = SQLStatement.substring(idx);


			RuntimeExceptionDao<TblInstusers, Integer> usersDAO = newdb1.getUsersRuntimeExceptionDao();
			int NoofRecords = (int) usersDAO.queryBuilder().countOf();

			if (NoofRecords > 0) {
				String str = SQLStatement;
				String stemp = "";
				int i = 0;

				if (str.contains("?")) {
					while (str.length() > 2) {
						if (arr3 != null && arr3.length > 0 && i < arr3.length) {
							stemp = stemp + str.substring(0, str.indexOf("?") + 1);
							Object obj = arr3[i++];
							if (obj != null) {
								if (obj instanceof String)
									stemp = stemp.replace("?", "'" + obj.toString() + "'");
								else
									stemp = stemp.replace("?", " " + obj.toString() + " ");
								str = str.substring(str.indexOf("?") + 1);
							} else return false;
						} else {
//                            return false;
							stemp = SQLStatement;
							break;
						}
					}
				} else {
					stemp = SQLStatement;
				}
				System.out.println(str + "\n");
				System.out.println(stemp);

				RuntimeExceptionDao<tbltransdetail, Integer> transdetailDAO = newdb1.getTransDetailsRuntimeExceptionDao();

				tbltransdetail transdetail = new tbltransdetail();
				transdetail.setAction("U");
				transdetail.setSQLStatement(SQLStatement);
				transdetail.setTableName(tableName);
				transdetail.setTransId("" + transId);
				transdetail.setUserId(userId);
				transdetailDAO.create(transdetail);

				return true;
			}
		} catch (Exception e) {
			return false;
		}
		return true;
	}
}
