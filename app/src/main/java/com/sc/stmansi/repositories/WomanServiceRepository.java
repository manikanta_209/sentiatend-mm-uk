//27Aug2019 Arpitha - remove unused code
package com.sc.stmansi.repositories;

import android.annotation.SuppressLint;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.dao.RawRowMapper;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.tables.ServiceslistPojo;
import com.sc.stmansi.tables.TblAncTests;
import com.sc.stmansi.tables.TblAncTestsMaster;
import com.sc.stmansi.tables.TblChildInfo;
import com.sc.stmansi.tables.TblDeliveryInfo;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.tables.tblserviceslist;
import com.sc.stmansi.tables.tblservicestypemaster;
import com.sc.stmansi.tables.tbltransdetail;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class WomanServiceRepository {

    private DatabaseHelper databaseHelper;

    public WomanServiceRepository(DatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }

    @SuppressLint("SimpleDateFormat")
//      get dates between dates
    public int getDaysBetweenDates(String regdate, String lmpdate) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Date getregdate = null, getlmpdate = null;
        if (regdate.length() != 0 && lmpdate.length() != 0) {
            getregdate = sdf.parse(regdate);
            getlmpdate = sdf.parse(lmpdate);
            return (int) ((getregdate.getTime() - getlmpdate.getTime()) / (1000 * 60 * 60 * 24));
        }
        return 0;
    }

    //   get Next date
    public String getNextDate(String srcDate, int interval, tblregisteredwomen td, boolean isMaxDt)
            throws Exception {


        SimpleDateFormat xSimpleDate = new SimpleDateFormat("dd-MM-yyyy");
        String nxtDt = null;

        if (srcDate.length() != 0) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(xSimpleDate.parse(srcDate));
            cal.add(Calendar.DATE, interval);
            Date xDt = cal.getTime();

            if ((td != null) && (td.getRegADDate() == null || td.getRegADDate().length() == 0) && isMaxDt) {
                Date eddDt = xSimpleDate.parse(td.getRegEDD());
                int i = xDt.compareTo(eddDt);
                if (i > 0)
                    nxtDt = xSimpleDate.format(eddDt);
                else
                    nxtDt = xSimpleDate.format(xDt);
            } else {
                nxtDt = xSimpleDate.format(xDt);
            }
            return nxtDt;
        }
        return nxtDt;
    }

    //	update planned services data
    public void updateServicesListData(ServiceslistPojo slu, DatabaseHelper dbHelper, int transId) throws SQLException {
        RuntimeExceptionDao<tblserviceslist, Integer> tblservicesDAO = dbHelper.getServicesRuntimeExceptionDao();
        UpdateBuilder<tblserviceslist, Integer> updateBuilder = tblservicesDAO.updateBuilder();

        updateBuilder.updateColumnValue("ServiceActualDateofAction", slu.getActualDateofAction())
                .updateColumnValue("ServiceProvidedFacName", slu.getFacilityName())
                .updateColumnValue("ServiceProvidedFacType", slu.getFacType())
                .updateColumnValue("ServiceComments", slu.getComments())
                .updateColumnValue("ServiceUserType", slu.getUsertype())
                .updateColumnValue("RecordUpdatedDate", slu.getRecordupdateddate()) //26July2019 - Bindu iCreateNewTrans - recordupdateddate
                .updateColumnValue("ServiceDetail1", slu.getServiceDetail1())//09May2021 Arpitha
                .updateColumnValue("ServiceDetail2", slu.getServiceDetail2())//09May2021 Arpitha
                .where().eq("WomenId", slu.getWomanId()).and()
                .eq("ServiceId", slu.getServiceId());

        int update = updateBuilder.update();


        if (update > 0) {


            TransactionHeaderRepository transactionHeaderRepository = new TransactionHeaderRepository(dbHelper);


            //16Aug2019 - Bindu - set ttlogic for planned service - if booster is given, disabe tt1 and tt2 and viceversa
            UpdateBuilder<tblserviceslist, Integer> updatettlogic = tblservicesDAO.updateBuilder();
            if (slu.getServiceId() == 3) {
                updatettlogic.updateColumnValue("ServicePlanned", 5)
                        .updateColumnValue("ServiceComments", "Planned")
                        .updateColumnValue("ServiceUserType", slu.getUsertype())
                        // .updateColumnValue("transid", slu.getTransId()) //18Aug2019 - Bindu
                        .updateColumnValue("RecordUpdatedDate", slu.getRecordupdateddate()) //26July2019 - Bindu add - recordupdateddate
                        .where().eq("WomenId", slu.getWomanId()).and()
                        .eq("ServicePlanned", 1).and()
                        .in("ServiceId", 1, 2);
                updatettlogic.update();
                transactionHeaderRepository.updateNew(slu.getUserid(), transId, "tblserviceslist"
                        , updatettlogic.prepare().toString().replace("MappedStatement:", "").trim(),
                        null, null, dbHelper);

            } else if (slu.getServiceId() == 1 || slu.getServiceId() == 2) {
                updatettlogic.updateColumnValue("ServicePlanned", 5)
                        .updateColumnValue("ServiceComments", "Planned")
                        .updateColumnValue("ServiceUserType", slu.getUsertype())
                        //  .updateColumnValue("transid", slu.getTransId())  //18Aug2019 - Bindu
                        .updateColumnValue("RecordUpdatedDate", slu.getRecordupdateddate()) //26July2019 - Bindu add - recordupdateddate
                        .where().eq("WomenId", slu.getWomanId()).and()
                        .eq("ServicePlanned", 1).and()
                        .eq("ServiceId", 3);
                updatettlogic.update();
                transactionHeaderRepository.updateNew(slu.getUserid(), transId, "tblserviceslist"
                        , updatettlogic.prepare().toString().replace("MappedStatement:", "").trim(),
                        null, null, dbHelper); //11Dec2019 - Bindu - add tt logic tblserviceslist - update in transtable
            }
            //11Dec2019 - Bindu - add to trans table
            transactionHeaderRepository.updateNew(slu.getUserid(), transId, "tblserviceslist",
                        updateBuilder.prepare().toString().replace("MappedStatement:", "").trim(), null, null, dbHelper);

        } else {
            tblserviceslist tblservlits = new tblserviceslist();
            tblservlits.setUserId(slu.getUserid());
            tblservlits.setWomenId(slu.getWomanId());
            String servicetype = slu.getServicetype();
            if (servicetype.contains("1"))
                servicetype = servicetype.replace("1", "");
            else if (servicetype.contains("2"))
                servicetype = servicetype.replace("2", "");
            else if (servicetype.contains("3"))
                servicetype = servicetype.replace("3", "");
            tblservlits.setServiceType(servicetype);
            tblservlits.setServiceId(slu.getServiceId());
            tblservlits.setServiceComments(slu.getComments());
            tblservlits.setServiceProvidedFacType(slu.getFacType());
            tblservlits.setServiceActualDateofAction(slu.getActualDateofAction());
            tblservlits.setServiceProvidedFacName(slu.getFacilityName());
            tblservlits.setServicePlanned(2);
            tblservlits.setServiceNumber(slu.getServiceNumber());
            //updateUnplannedServices(tblservlits);18Sep2019 Arpitha
        }
    }

    //	update unplanned services in table - 15May2021 ARpitha also insrt anc tests
    public boolean updateUnplannedServices(tblserviceslist SLU, DatabaseHelper dbHelper, ArrayList<TblAncTests> ancTestsList) throws SQLException {
        RuntimeExceptionDao<tblserviceslist, Integer> tblservicesDAO = databaseHelper.getServicesRuntimeExceptionDao();
        RuntimeExceptionDao<tblservicestypemaster, Integer> tblservicestypeDAO = databaseHelper.getServicesTypeRuntimeExceptionDao();
        boolean sucess = false;
        int addServ;

        if (SLU.getServiceNumber() <= 0) {

            //27Sep2019 - Bindu - Folicacid - service check
            final List<tblserviceslist> query; //30Sep2019 - Bindu -Change to tblserviceslist
            if (SLU.getServiceType().equalsIgnoreCase("Folic Acid")) {
                SLU.setServiceType("FolicAcid");
            }


            String strqry = "SELECT `ServiceId`, Max(`ServiceNumber`) FROM `tblserviceslist` WHERE  WomenId = '" + SLU.getWomenId() + "' and ServiceType = '" + SLU.getServiceType() + "'";
            GenericRawResults<String[]> rawResults = tblservicesDAO.queryRaw(strqry);

            List<String[]> results = rawResults.getResults();
            // This will select the first result (the first and maybe only row returned)
            String[] resultArray = results.get(0);
            //This will select the first field in the result which should be the ID
            if (resultArray[0] != null) {
                SLU.setServiceId(Integer.parseInt(resultArray[0]));
                SLU.setServiceNumber(Integer.parseInt(resultArray[1]) + 1);
            }


        }

        TransactionHeaderRepository transRepo = new TransactionHeaderRepository(databaseHelper);
        int transId = transRepo.iCreateNewTransNew(SLU.getUserId(), databaseHelper);
        SLU.setTransId(transId);

        addServ = tblservicesDAO.create(SLU);

        if (addServ > 0) {
            sucess = iNewRecordTransNew(SLU.getUserId(), transId, "tblserviceslist", databaseHelper);
//15May2021 Arpitha
            long added =0;
            for (int i = 0; i < ancTestsList.size(); i++) {
                TblAncTests tblAncTests = ancTestsList.get(i);
                tblAncTests.setServiceNumber(SLU.getServiceNumber());
                tblAncTests.setTransId(transId);
                ancTestsList.set(i, tblAncTests);
                added = databaseHelper.getTblAncTestsDao().create(tblAncTests);
            }
            if (added > 0) {
                iNewRecordTransNew(ancTestsList.get(0).getUserId(), transId, databaseHelper.getTblAncTestsDao().getTableName(), databaseHelper);

                sucess = true;
            }

        }


        return sucess;
    }

    public tblserviceslist getService(String womanId, int ServiceId, int serviceNum) throws SQLException {
        List<tblserviceslist> services = databaseHelper.getServicesRuntimeExceptionDao()
                .queryBuilder()
                .where().eq("WomenId", womanId)
                .and()
                .eq("ServiceId", ServiceId)
                .and()
                .eq("ServiceNumber", serviceNum)  //18Oct2019 - Bindu - add servicenumber param
                .and()
                .isNotNull("ServiceActualDateofAction")
                .and()
                .ne("ServicePlanned", 3)
                .query();
        return services.isEmpty() ? null : services.get(0);
    }

    //	check TT is provided
    public boolean isTTprovided(String wid) {
        boolean isttprovided = false;
        try {
            String strqry = "Select * from tblserviceslist where servicetype = 'TT' and serviceactualdateofaction is not null and womenid= '" + wid + "'";

            GenericRawResults<String[]> gen = databaseHelper.getServicesRuntimeExceptionDao().queryRaw(strqry);
            List<String[]> list = gen.getResults();
            if (list.size() > 0) {
                isttprovided = true;
            }
        } catch (Exception e) {
            // TODO add logging framework
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        return isttprovided;
    }

    //	check TTBooster is provided
    public boolean isTTBoosterprovided(String wid) {
        boolean isttboosterprovided = false;
        try {
            String strqry = "Select * from tblserviceslist where servicetype = 'TTBooster' and serviceactualdateofaction is not null and womenid= '" + wid + "'";

            GenericRawResults<String[]> gen = databaseHelper.getServicesRuntimeExceptionDao().queryRaw(strqry);
            List<String[]> list = gen.getResults();
            if (list.size() > 0) {
                isttboosterprovided = true;
            }
        } catch (Exception e) {
            // TODO add logging framework
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        return isttboosterprovided;
    }

    // 11Aug2019 - Bindu order by serv id
    // 09Aug2019 - Bindu - Retain onl pending
    // 05Aug2019 - Bindu chk for ttbooster applicable
    public List<tblserviceslist> getServices(final tblregisteredwomen woman, int numofdays) throws Exception {
        long limit = 10;
        String[] selectColumns = {"ServiceType", "ServiceActualDateofAction", "ServiceDetail1",
                "ServiceDueDateMin", "ServiceDueDateMax", "ServiceNumber", "ServiceId", "ServiceUserType"};
        QueryBuilder<tblserviceslist, Integer> queryBuilder = databaseHelper.getServicesRuntimeExceptionDao()
                .queryBuilder()
                .selectColumns(selectColumns)
                .orderBy("serviceid", true);
        Where<tblserviceslist, Integer> where = queryBuilder.where()
                .eq("womenId", woman.getWomanId())
                .and().not().eq("ServicePlanned", 3);

        int ttBoosterApplicability = 0;
        if (woman.getRegLastChildAge() != null && !woman.getRegLastChildAge().isEmpty() &&
                woman.getRegLastChildAge().trim().length() > 0) {
            int lastChildAge = Integer.parseInt(woman.getRegLastChildAge());
            if (lastChildAge > 0 && lastChildAge < 36)
                ttBoosterApplicability = 1; //booster applicable
            else if (lastChildAge > 36)
                ttBoosterApplicability = 2;  //booster not applicable
        } else {  //11Aug2019 - Bindu - Chk if TTBooster is provided
            if (isTTBoosterprovided(woman.getWomanId()))
                ttBoosterApplicability = 1;
            else if (isTTprovided(woman.getWomanId()))
                ttBoosterApplicability = 2;
        }
        if (ttBoosterApplicability == 1) {
            where = where.and().not().eq("serviceid", 1)
                    .and().not().eq("serviceid", 2);
        } else if (ttBoosterApplicability == 2) {
            where = where.and().not().eq("serviceid", 3);
        }

        // The following is for Spinner.onItemSelected
        String today = DateTimeUtil.getDateYYYYMMDD(DateTimeUtil.getTodaysDate(), "yyyy-MM-dd");
        if (numofdays == 0) {
            // do nothing
        } else if (numofdays == 1) {
            where = where.and().isNull("ServiceActualDateofAction").and().lt("serviceid", 15)
                    .and().raw("(Date(substr(serviceduedatemax , 7) || '-' || substr(serviceduedatemax ,4,2)" +
                            " || '-' || substr(serviceduedatemax , 1,2)) < Date('" + today + "'))");
        } else if (numofdays == 2) {
            where = where.and().isNull("serviceactualDateofaction")
                    .and().raw("(Date('" + today + "') between Date(substr(serviceduedatemin , 7) || '-'" +
                            " || substr(serviceduedatemin ,4,2) || '-' || substr(serviceduedatemin , 1,2))" +
                            " and Date(substr(serviceduedatemax , 7) || '-' || substr(serviceduedatemax ,4,2)" +
                            " || '-' || substr(serviceduedatemax , 1,2)))");
        } else if (numofdays == 3) {  //09Aug2019 - Bindu - change position
            where = where.and().isNull("serviceactualDateofaction")
                    .and().raw("Date(substr(serviceduedatemin , 7) || '-'" +
                            " || substr(serviceduedatemin ,4,2)" +
                            " || '-' || substr(serviceduedatemin , 1,2)) > Date('" + today + "')");
        } else if (numofdays == 4) { //09Aug2019 - Bindu - change position
            where = where.and().isNotNull("serviceactualDateofaction");
        }
        queryBuilder.setWhere(where);
        String s = queryBuilder.prepare().toString();
        return queryBuilder.query();
    }

    public List<tblserviceslist> getFilteredServicesForWoman(final tblregisteredwomen woman) throws Exception {
        String[] selectColumns = {"ServiceType", "ServiceActualDateofAction", "ServiceDetail1",
                "ServiceNumber", "ServiceId", "ServiceActualDateofAction", "WomenId"};
        return databaseHelper.getServicesRuntimeExceptionDao()
                .queryBuilder()
                .selectColumns(selectColumns)
                .orderBy("ServiceActualDateofAction", false)
                .where()
                .eq("womenId", woman.getWomanId()).and().eq("ServicePlanned", 3).query();
    }

    // Get all service list along with the upcoming woman count
    public List<String[]> getPendingServicesAndCount(int villageCode, String userId)
            throws Exception {

        String sql;
        String villageCond = "";

        String selDtae = DateTimeUtil.getDateYYYYMMDD
                (DateTimeUtil.getTodaysDate(), "yyyy-MM-dd");

        if (villageCode > 0)
            villageCond = " and reg.regvillage="
                    + villageCode;

        sql = "Select sm.servicetype, t3.cnt from tblservicestypemaster sm left join\n" +
                "   (Select sum(cast(t2.wid as decimal)) as cnt, servicetype from(\n" +
                "  Select \n" +
                "   count(distinct(womenid)) as wid, servicetype as servicetype\n" +
                "   from tblserviceslist sl inner join tblregisteredwomen reg on\n" +
                "   sl.womenid = reg.womanid where   serviceactualdateofaction\n" +
                "     is null and serviceplanned=1   " + villageCond +        //12Aug2019 - Bindu - iCreateNewTrans cond service planned !=2 as all the services are written
                "      and serviceactualdateofaction is null  and sl.UserId='" + userId + "'  and \n" +
                "\t\t\t\t \n" +
                "\t\t\t\t Date(substr(serviceduedatemin , 7) || '-'\n" +
                "  || substr(serviceduedatemin ,4,2)\n" +
                "  || '-' || substr(serviceduedatemin , 1,2))<=('" + selDtae + "')  and datedeactivated is null\n" +
                "   \n" +
                "   and (( womanid \n" +
                " IN (select womanid from tblchildinfo where chlDeactDate\n" +
                " is null) and regpregnantormother=2) or regpregnantormother=1) " +
                "\t\t\t\t \n" +
                "\t\t\t\t\n" +
                "\t\t\t\tgroup by servicetype,womenid )\n" +
                "  as t2 group by t2.servicetype) as t3\n" +
                "    on sm.servicetype = t3.servicetype where isImmunisation = 'N'\n" +
                "     group by sm.servicetype order by serviceid";

        GenericRawResults<String[]> gen = databaseHelper.gettblregRuntimeExceptionDao().queryRaw(sql);

        List<String[]> list = gen.getResults();
        return list;

    }

    //	 get all service list along with the pending woman count
    public List<String[]> getUpcomingServicesAndCount(int villageCode, String userId)
            throws Exception {

        String sql;
        String villageCond = "";
        String selDtae = DateTimeUtil.getDateYYYYMMDD
                (DateTimeUtil.getTodaysDate(), "yyyy-MM-dd");


        if (villageCode > 0)
            villageCond = " and reg.regvillage="
                    + villageCode;

        sql = "Select sm.servicetype, t3.cnt from tblservicestypemaster sm left join\n" +
                "   (Select sum(cast(t2.wid as decimal)) as cnt, servicetype from(\n" +
                "   Select \n" +
                " count(distinct(womenid)) as wid, servicetype as servicetype\n" +
                "   from tblserviceslist sl inner join tblregisteredwomen reg on\n" +
                "    sl.womenid = reg.womanid where   serviceactualdateofaction\n" +
                " is null and serviceplanned!=3    and serviceplanned != 2  " + villageCond +    //12Aug2019 - Bindu - iCreateNewTrans cond service planned !=2 as all the services are written
                "  and serviceactualdateofaction is null and sl.UserId='" + userId + "' and \n" +
                "\t\t\t\t \n" +
                "\t\t\t\t Date(substr(serviceduedatemin , 7) || '-'\n" +
                "  || substr(serviceduedatemin ,4,2)\n" +
                "   || '-' || substr(serviceduedatemin , 1,2))>('" + selDtae + "')   and datedeactivated is null \n" +
                "   \n" +
                "   and (( womanid \n" +
                " IN (select womanid from tblchildinfo where chlDeactDate\n " +
                " is null) and regpregnantormother=2) or regpregnantormother=1)" +
                "\t\t\t\t \n" +
                "\t\t\t\t\n" +
                "\t\t\t\tgroup by servicetype,womenid )\n" +
                "   as t2 group by t2.servicetype) as t3\n" +
                "  on sm.servicetype = t3.servicetype where isImmunisation = 'N'\n" +
                "     group by sm.servicetype order by serviceid";
        GenericRawResults<String[]> gen = databaseHelper.gettblregRuntimeExceptionDao().queryRaw(sql);

        List<String[]> list = gen.getResults();
        return list;
    }

    public List<String[]> getWomanPendingList(String sType,
                                              int villageCode,
                                              String status) throws Exception {
        String sql;
        String strStatus = "";

        String todayDate = DateTimeUtil.getDateYYYYMMDD
                (DateTimeUtil.getTodaysDate(), "yyyy-MM-dd");//Arpitha - 30Apr2021



        if (status.equalsIgnoreCase("Upcoming"))
            strStatus = " and " +
                    " Date(substr(serviceduedatemin , 7) || '-' || " +
                    "   substr(serviceduedatemin ,4,2)  || '-' || " +
                    " substr(serviceduedatemin , 1,2))>Date('now')";
        else if (status.equalsIgnoreCase("Pending"))
            strStatus = " and " +
                    " Date(substr(serviceduedatemin , 7) || '-' || " +
                    "   substr(serviceduedatemin ,4,2)  || '-' || " +
                    " substr(serviceduedatemin , 1,2))<=Date('"+todayDate+"')";//Arpitha 30Apr2021 - it was nt fetching current date records

        if (villageCode == 0)
            sql = "Select distinct(womanid),regwomanname, ServiceDueDateMin, " +
                    "ServiceDueDateMax, reglmp,regpregnantormother,\n" +
                    "regADDate, regComplicatedpreg, regriskfactors,regAmberorredcolorcode," +
                    "regwomenImage ,servicenumber,serviceid, tblregisteredwomen.UserId,servicetype from tblserviceslist inner \n" +
                    "join tblregisteredwomen on tblserviceslist.womenid =\n" +
                    "tblregisteredwomen.womanid where servicetype='" + sType + "' and " +
                    "serviceactualdateofaction is null" + " and serviceplanned=1 " +               //12Aug2019 -Bindu - Check not Not applicable
                    strStatus +
                    "  and datedeactivated is null\n" +
                    "   \n" +
                    "   and (( womanid \n" +
                    "IN (select womanid from tblchildinfo where chlDeactDate\n" +
                    " is null) and regpregnantormother=2) or regpregnantormother=1) group by womanid" +
                    "  order by Date(substr(serviceduedatemin , 7) || '-' || \n" +
                    "substr(serviceduedatemin ,4,2)  || '-' || \n" +
                    " substr(serviceduedatemin , 1,2))";

        else
            sql = "Select distinct(womanid),regwomanname, ServiceDueDateMin, ServiceDueDateMax, reglmp,regpregnantormother,\n" +
                    "regADDate, regComplicatedpreg, regriskfactors,regAmberorredcolorcode , " +
                    "regwomenImage ,servicenumber,serviceid, tblregisteredwomen.UserId,servicetype from tblserviceslist inner \n" +
                    "join tblregisteredwomen on tblserviceslist.womenid =\n" +
                    "tblregisteredwomen.womanid where servicetype='" + sType + "'" +
                    " and serviceactualdateofaction is null" + " and serviceplanned=1 " +               //12Aug2019 -Bindu - Check not Not applicable
                    " and regvillage=" + villageCode + " " +
                    strStatus +
                    " and datedeactivated is null\n" +
                    "   \n" +
                    "   and (( womanid \n" +
                    "IN (select womanid from tblchildinfo where chlDeactDate\n" +
                    " is null) and regpregnantormother=2) or regpregnantormother=1)  group by womanid order by Date(substr(serviceduedatemin , 7) || '-' || \n" +
                    "substr(serviceduedatemin ,4,2)  || '-' || \n" +
                    "                substr(serviceduedatemin , 1,2))";


        GenericRawResults<String[]> gen = databaseHelper.getServicesRuntimeExceptionDao().queryRaw(sql);

        List<String[]> list = gen.getResults();
        return list;

    }

    public List<ServiceslistPojo> getServiceTypeData(final String womanId, String stype, String sessionUserId) throws SQLException {

        List<ServiceslistPojo> listServcies = new ArrayList<>();

        int qryttboosterapp = 0;
        WomanRepository womanRepository = new WomanRepository(databaseHelper);
        tblregisteredwomen woman = womanRepository.getRegistartionDetails(womanId, sessionUserId);
        if (woman.getRegLastChildAge() != null && woman.getRegLastChildAge() != "" && woman.getRegLastChildAge().trim().length() > 0) {
            int lastchildage = Integer.parseInt(woman.getRegLastChildAge());
            if (lastchildage > 0 && lastchildage < 36)
                // qryttboosterapp = "  tblservicestypemaster.serviceid != 1 and tblservicestypemaster.serviceid != 2 ";
                qryttboosterapp = 1;
            else if (lastchildage > 0 && lastchildage > 36)
                qryttboosterapp = 2;


        }

        String query = "Select tblservicestypemaster.ServiceType, " +
                "tblServiceslist. " +
                "  ServiceActualDateofAction,\n" +
                "  tblServicestypemaster.ServiceNumber" +
                "  from " +
                " tblservicestypemaster left join tblserviceslist\n" +
                "   on tblservicestypemaster.ServiceId = " +
                "tblserviceslist.ServiceId \n" +
                "     and tblserviceslist.ServicePlanned!= 3 " +
                " and womenId='" + womanId + "'\n" +
                " where" +
                " tblservicestypemaster.ServiceType='" + stype + "' and ServiceActualDateofAction is not null";
        if (qryttboosterapp == 1)
            query = query + " and tblservicestypemaster.serviceid != 1 and tblservicestypemaster.serviceid != 2 ";
        else if (qryttboosterapp == 2)
            query = query + " and tblservicestypemaster.serviceid != 3 ";

        query = query + " order by tblservicestypemaster.serviceid";


        GenericRawResults<ServiceslistPojo> tbls = databaseHelper.getServicesTypeRuntimeExceptionDao().
                queryRaw(query,
                        new RawRowMapper<ServiceslistPojo>() {
                            @Override
                            public ServiceslistPojo mapRow(String[] strings, String[] strings1) {
                                ServiceslistPojo servicesPojo = new ServiceslistPojo();
                                String s = strings[0];
                                servicesPojo.setServicetype(strings1[0]);
                                if (strings1[1] != null && strings1[1].trim().length() > 0)
                                    servicesPojo.setActualDateofAction(strings1[1]);
                                else
                                    servicesPojo.setActualDateofAction("");
                                servicesPojo.setServiceNumber(Integer.parseInt(strings1[2]));
                                servicesPojo.setWomanId(womanId);

                                return servicesPojo;
                            }
                        });

        for (ServiceslistPojo foo : tbls) {
            listServcies.add(foo);
        }
        return listServcies;
    }

    public List<String[]> getPendingServiceslistData(String womanId, String serviceType)
            throws SQLException {
        String sql = "SELECT * FROM `tblserviceslist` WHERE `servicetype` = " +
                "'" + serviceType + "' AND" +
                " `ServiceActualDateofAction` " +
                "IS NULL  AND Date(substr(serviceduedatemin , 7) || '-' " +
                "  || substr(serviceduedatemin ,4,2) " +
                "|| '-' || substr(serviceduedatemin , 1,2)) <= Date('now') AND " +
                "`womenid` = '" + womanId + "' and serviceplanned != 2";    //12Aug2019 - Bindu - Remove Expired/Unplanned2 service

        GenericRawResults<String[]> gen = databaseHelper.getServicesRuntimeExceptionDao().queryRaw(sql);
        return gen.getResults();
    }


    // Structures and Insert records to tblServicesList
    // 24Oct2019 Arpitha - add 2 parameters delinfo and child info
    public boolean addServicesNew(tblregisteredwomen td, int transId,
                                  String userType, DatabaseHelper newdb1,
                                  TblDeliveryInfo tblDeliveryInfo, List<TblChildInfo> tblChildInfo)
            throws Exception {

        String selectservicesSQL;
        int noOfDaysDiff;
        boolean returnValue = false;
        boolean temp1 = false, temp2 = false, temp3 = false;
        String ReferenceDate;


        TransactionHeaderRepository transactionHeaderRepository = new TransactionHeaderRepository(databaseHelper);


        if (td.getRegADDate() == null || (td.getRegADDate() != null && td.getRegADDate().length() <= 0)) { // Pregnant Woman
            ReferenceDate = td.getRegLMP();
            noOfDaysDiff = getDaysBetweenDates(td.getRegRegistrationDate(), ReferenceDate);

            //11Aug2019 - Bindu - First insert TT service and write all the services
            selectservicesSQL = insertTTOrBoosterNew(td, noOfDaysDiff, newdb1);
            //returnValue = WriteServices(selectservicesSQL, td, ReferenceDate, transId);
            returnValue = WriteServicesAllNew(selectservicesSQL, noOfDaysDiff, td,
                    ReferenceDate, transId, newdb1, userType);
            temp2 = returnValue;

            String SQL95 = "SELECT * from tblservicestypemaster where "
                    + "ServiceType <> 'TT' and ServiceType <> 'TTBooster' "
                    + "and ServiceType <> 'EDD' and TaskReference = 'LMP' ";


            returnValue = WriteServicesAllNew(SQL95, noOfDaysDiff, td, ReferenceDate, transId, newdb1, userType);
            temp1 = returnValue;

            String SQL96 = "SELECT * from tblservicestypemaster "
                    + "where TaskReference = 'LMP' and ServiceType='EDD' ";
            returnValue = WriteServiceNew(SQL96, td, td.getRegEDD(), transId, newdb1, userType, tblDeliveryInfo, tblChildInfo);  //Retain same for EDD
            temp3 = returnValue;


            if (temp1 && temp3)
                transactionHeaderRepository.iNewRecordTransNew(td.getUserId(), transId, "tblserviceslist", newdb1);

        } else {
            ReferenceDate = td.getRegADDate();
            noOfDaysDiff = getDaysBetweenDates(td.getRegRegistrationDate(), ReferenceDate);

            String SQL97 = "select * from tblservicestypemaster " + "where TaskReference = 'ADD' and ServiceType = 'PNC'";


           /* returnValue = WriteServiceNew(SQL97 + " and DaysDiffEnd > " + noOfDaysDiff, td,
                    ReferenceDate, transId, newdb1, userType, tblDeliveryInfo, tblChildInfo);*/

            returnValue = WriteServiceNew(SQL97, td,
                    ReferenceDate, transId, newdb1, userType, tblDeliveryInfo, tblChildInfo);

            if (returnValue)
                transactionHeaderRepository.iNewRecordTransNew(td.getUserId(), transId, "tblserviceslist", newdb1);
        }

        int LastServiceListId = LastServiceIdforWomanNew(td.getUserId(), td.getWomanId(), newdb1);


        RuntimeExceptionDao regDAO = newdb1.gettblregRuntimeExceptionDao();

        UpdateBuilder<tblregisteredwomen, Integer> updateBuilder =
                regDAO.updateBuilder();


        updateBuilder.updateColumnValue("LastServiceListNumber", "" + LastServiceListId).
                where().eq("womanId", td.getWomanId());
        updateBuilder.update();

        transactionHeaderRepository.updateNew(td.getUserId(), transId, regDAO.getTableName(),
                updateBuilder.prepare()
                        .toString().replace("MappedStatement:", ""), null,
                new Object[]{LastServiceListId, td.getWomanId()}, newdb1);

        return true;

    }


    // Insert either TT or TTBooster
    public static String insertTTOrBoosterNew(tblregisteredwomen td, int noOfDaysDiff, DatabaseHelper newdb1) throws Exception {
        String returnSql = null;


        RuntimeExceptionDao regDAO = newdb1.gettblregRuntimeExceptionDao();

        List<tblregisteredwomen> listtblreg = regDAO.queryBuilder()
                .selectColumns("regLastChildAge").where()
                .eq("WomanId", td.getWomanId()).query();


        RuntimeExceptionDao tblservicestypeDAO = newdb1.getServicesTypeRuntimeExceptionDao();

        // 05Aug2019 - Bindu
        String lastchildage = listtblreg.get(0).getRegLastChildAge();
        //02Oct2019 - Bindu - for primi - disable booster and enter only TT
        if (td.getRegStatusWhileRegistration() != null && td.getRegStatusWhileRegistration().contains("Primi")) {
            returnSql = tblservicestypeDAO.queryBuilder().where()
                    .eq("ServiceType", "TT")
                    .and()
                    .eq("TaskReference", "LMP")
                    .prepare().toString();
        } else if (lastchildage != null && lastchildage != "" && lastchildage.trim().length() > 0) {
            if (Integer.parseInt(listtblreg.get(0).getRegLastChildAge()) > 0 &&
                    Integer.parseInt(listtblreg.get(0).getRegLastChildAge()) < 36) {
                returnSql = tblservicestypeDAO.queryBuilder().where().
                        eq("ServiceType", "TTBooster").and()
                        .eq("TaskReference", "LMP").prepare().toString();
                /*  .and().gt("DaysDiffEnd", noOfDaysDiff).prepare().toString();*/
            } else {                                                                              //Write TT if > 36
                returnSql = tblservicestypeDAO.queryBuilder().where()
                        .eq("ServiceType", "TT")
                        .and()
                        .eq("TaskReference", "LMP")
                        .prepare().toString();
            }
        } else {                                                                                 //Write all if last child age not written
            returnSql = tblservicestypeDAO.queryBuilder().where()
                    .eq("ServiceType", "TT")
                    .or()
                    .eq("ServiceType", "TTBooster")
                    .and()
                    .eq("TaskReference", "LMP")
                    .prepare().toString();
                               /*.and().gt("DaysDiffEnd",noOfDaysDiff)
                  .prepare().toString();*/
        }


        return returnSql.replace("MappedStatement:", "");
    }


    // Write List of Services in tblServicesList - 12Aug2019 - Bindu
    public boolean WriteServicesAllNew(String strSql, int noofdays, tblregisteredwomen td,
                                       String ReferenceDate, int transId, DatabaseHelper newdb1, String userType) throws Exception {

        boolean isAdded = false;
        RuntimeExceptionDao tblservicestypeDAO = newdb1.getServicesTypeRuntimeExceptionDao();
        GenericRawResults<String[]> ss = tblservicestypeDAO.queryRaw(strSql);

        List<String[]> vv = ss.getResults();

        if (vv != null && vv.size() > 0) {

            for (int i = 0; i < vv.size(); i++) {
                if (vv.get(i)[1].equalsIgnoreCase("PNC")) {

                } else {

                    if (td.getRegADDate() != null && td.getRegADDate().trim().length() > 0) {

                    } else
                        isAdded = writeServicesNNew(td, ReferenceDate, transId, vv.get(i), noofdays, newdb1, userType); //12Aug2019 - Bindu send no of days


                }
            }
        }


        return isAdded;
    }


    //     write services to tblserviceslist
    private boolean writeServicesNew(tblregisteredwomen td, String referenceDate,
                                     int transId, String[] vv, DatabaseHelper newdb1, String userType) throws Exception {

        boolean isAdded = false;
        int id = 0;

        RuntimeExceptionDao tblservicesDAO = newdb1.getServicesRuntimeExceptionDao();
        tblserviceslist serv = new tblserviceslist();
        serv.setUserId(td.getUserId());
        serv.setWomenId(td.getWomanId());
        serv.setServiceUserType(userType);
        serv.setServiceId(Integer.parseInt(vv[0]));
        serv.setServiceType(vv[1]);
        serv.setServiceNumber(Integer.parseInt(vv[2]));
        serv.setServicePlanned(1);
        if (vv[1].equalsIgnoreCase("EDD")) {
            serv.setServiceDueDateMin(referenceDate);
            serv.setServiceDueDateMax(referenceDate);
        } else {
            serv.setServiceDueDateMin(getNextDate(referenceDate, Integer.parseInt(vv[5]), td, false));
            serv.setServiceDueDateMax(getNextDate(referenceDate, Integer.parseInt(vv[6]), td, false));
        }
        serv.setTransId(transId);
        serv.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
        serv.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());  //15Aug2019 - Bindu
        id = tblservicesDAO.create(serv);


        if (id > 0)
            isAdded = true;

        return isAdded;
    }

    // Write List of Services in tblServicesList
    // 24Oct2019 Arpitha - add 2 parameters tblelinfo and childinfo
    public boolean WriteServiceNew(String strSql, tblregisteredwomen td,
                                   String ReferenceDate, int transId,
                                   DatabaseHelper newdb1,
                                   String userType, TblDeliveryInfo tblDeliveryInfo, List<TblChildInfo> tblchilinfo) throws Exception {

        boolean isAdded = false;
        RuntimeExceptionDao tblservicestypeDAO = newdb1.getServicesTypeRuntimeExceptionDao();
        GenericRawResults<String[]> ss = tblservicestypeDAO.queryRaw(strSql);

        List<String[]> vv = ss.getResults();

        boolean isChildDead = false, firstChildDead = false, secondChildDead = false, thirdChildDead = false, fourthChildDead = false;

        if (tblchilinfo != null) {
            for (int i = 0; i < tblchilinfo.size(); i++) {

                if (i == 0 && tblchilinfo.get(i).getChlDeliveryResult() > 1)
                    firstChildDead = true;

                if (i == 1 && tblchilinfo.get(i).getChlDeliveryResult() > 1)
                    secondChildDead = true;

                if (i == 2 && tblchilinfo.get(i).getChlDeliveryResult() > 1)
                    thirdChildDead = true;

                if (i == 3 && tblchilinfo.get(i).getChlDeliveryResult() > 1)
                    fourthChildDead = true;
            }

            if (tblchilinfo.size()==1 && firstChildDead )
                isChildDead = true;

            if (tblchilinfo.size()==2 && firstChildDead && secondChildDead)
                isChildDead = true;

            if (tblchilinfo.size()==3 && firstChildDead && secondChildDead && thirdChildDead)
                isChildDead = true;

            if (tblchilinfo.size()==4 && firstChildDead && secondChildDead && thirdChildDead && fourthChildDead)
                isChildDead = true;
        }

        if (vv != null && vv.size() > 0) {

            for (int i = 0; i < vv.size(); i++) {
                if (vv.get(i)[1].equalsIgnoreCase("PNC") &&

                        (!(tblDeliveryInfo.getDelMothersDeath() != null &&
                                tblDeliveryInfo.getDelMothersDeath().equalsIgnoreCase("Y"))) ||
                        !(isChildDead)) {
                    // 24Oct2019 Arpitha
                    isAdded = getPncSNoForWoman(td, tblDeliveryInfo, tblchilinfo,
                            ReferenceDate, transId, vv.get(i), newdb1, userType);// 24Oct2019 Arpitha
                } else {

                    if (td.getRegADDate() != null && td.getRegADDate().trim().length() > 0) {

                    } else
                        isAdded = writeServicesNew(td, ReferenceDate, transId, vv.get(i), newdb1, userType);


                }
            }
        }


        return isAdded;
    }

    //     write services to tblserviceslist
    private boolean writeServicesNNew(tblregisteredwomen td, String referenceDate,
                                      int transId, String[] vv, int noofdays, DatabaseHelper newdb1,
                                      String userType) throws Exception {

        boolean isAdded = false;
        int id = 0;

        RuntimeExceptionDao tblservicesDAO = newdb1.getServicesRuntimeExceptionDao();

        //  newtblservicesDAO = newdb.getServicesDao();

        tblserviceslist serv = new tblserviceslist();
        serv.setUserId(td.getUserId());
        serv.setWomenId(td.getWomanId());
        serv.setServiceUserType(userType);
        serv.setServiceId(Integer.parseInt(vv[0]));
        serv.setServiceType(vv[1]);
        serv.setServiceNumber(Integer.parseInt(vv[2]));
        if (Integer.parseInt(vv[6]) >= noofdays) //12Aug2019 - Bindu - compare n of days and set as Planned or unplanned
            serv.setServicePlanned(1);
        else
            serv.setServicePlanned(2);
        if (vv[1].equalsIgnoreCase("EDD")) {
            serv.setServiceDueDateMin(referenceDate);
            serv.setServiceDueDateMax(referenceDate);
        } else {
            serv.setServiceDueDateMin(getNextDate(referenceDate, Integer.parseInt(vv[5]), td, false));
            serv.setServiceDueDateMax(getNextDate(referenceDate, Integer.parseInt(vv[6]), td, false));
        }
        serv.setTransId(transId);
        serv.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
        serv.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
        id = tblservicesDAO.create(serv);
        //  id = newtblservicesDAO.create(serv);  //13Aug2019 - Bindu Dao fromRuntimedao


        if (id > 0)
            isAdded = true;

        return isAdded;
    }

    // get The last service Id
    public static int LastServiceIdforWomanNew(String userId, String womanId, DatabaseHelper newdb1) throws Exception {


        RuntimeExceptionDao tblservicesDAO = newdb1.getServicesRuntimeExceptionDao();
        List<tblserviceslist> sevlist = tblservicesDAO.queryBuilder()
                .selectColumns("ServiceId").where()
                .eq("ServicePlanned", "1")
                .and().eq("userId", userId).and()
                .eq("womenId", womanId).query();

        int serviceId = 0;
        if (sevlist != null && sevlist.size() > 0)
            serviceId = sevlist.get(sevlist.size() - 1).getServiceId();
        return serviceId;

    }

//    15May2021 Arpitha - fetch based on service number
    public tblserviceslist getUnplannedService(String womanId, int ServiceId, int servNum) throws SQLException {
        List<tblserviceslist> services = databaseHelper.getServicesRuntimeExceptionDao()
                .queryBuilder()
                .where().eq("WomenId", womanId)
                .and()
                .eq("ServiceId", ServiceId)
                .and()
                .isNotNull("ServiceActualDateofAction")
                .and()
                .eq("ServicePlanned", 3)
                .and()
                .eq("ServiceNumber", servNum)
                .query();
        return services.isEmpty() ? null : services.get(0);
    }

    // Add new Record to Trans Table
    public static boolean iNewRecordTransNew(String userId, int transId, String tableName, DatabaseHelper newdb1) {

        try {

            RuntimeExceptionDao<tbltransdetail, Integer> transdetailDAO = newdb1.getTransDetailsRuntimeExceptionDao();

            tbltransdetail transdetail = new tbltransdetail();
            transdetail.setAction("I");
            transdetail.setSQLStatement("");
            transdetail.setTableName(tableName);
            transdetail.setTransId("" + transId);
            transdetail.setUserId(userId);
            transdetailDAO.create(transdetail);

            return true;

        } catch (Exception E) {
            return false;
        }

    }

    // 24Oct2019 Arpitha
    private boolean getPncSNoForWoman(tblregisteredwomen td, TblDeliveryInfo did,
                                      List<TblChildInfo> tblChildInfo, String referenceDate,
                                      int transId,
                                      String[] list, DatabaseHelper newdb1, String userTYpe) throws Exception {

        boolean isAdded = false;

        // int noOfChild = did.getDelNoOfChildren();
//        int homeDeliveryId = Integer.parseInt(did.getDelPlace());
        boolean isUnderWeight = false;

       /* if (noOfChild == 1) {
            if (tblChildInfo.getDelBabyWeight()!=null && Double.parseDouble(tblChildInfo.getDelBabyWeight()) < 2500)
                isUnderWeight = true;
        } else if (noOfChild == 2) {
            if (tblChildInfo.getDelBabyWeight()!=null && Double.parseDouble(tblChildInfo.getDelBabyWeight()) < 2500 || Double.parseDouble(tblChildInfo.getDelBabyWeight()) < 2500)
                isUnderWeight = true;
        }*/

        int servNo = 0;
/*
        if (isUnderWeight) {
            if (homeDeliveryId == 8 || homeDeliveryId == 6) {
                servNo = Integer.parseInt( list[2]);
            } else {
                if (Integer.parseInt( list[2]) != 1)
                    servNo = Integer.parseInt( list[2]);
            }
        } else {
            if (homeDeliveryId == 8 || homeDeliveryId == 6) {
                if (Integer.parseInt( list[2]) == 1 || Integer.parseInt( list[2]) == 2 || Integer.parseInt( list[2]) == 3 || Integer.parseInt( list[2]) == 7)
                    servNo = Integer.parseInt( list[2]);
            } else {
                if (Integer.parseInt( list[2]) == 2 || Integer.parseInt( list[2]) == 3 || Integer.parseInt( list[2]) == 7)
                    servNo = Integer.parseInt( list[2]);
            }
        }*/

        servNo = Integer.parseInt(list[2]);
        if (servNo != 0)
            isAdded = writeServicesNew(td, referenceDate, transId, list, newdb1, userTYpe);

        return isAdded;
    }

    public int getDaysforImmunization(String columnName, String serviceType) throws SQLException {

        int days = 0;
        tblservicestypemaster tblservicestypemaster = databaseHelper.
                getServicesTypeRuntimeExceptionDao().queryBuilder()
                .selectColumns(columnName)
                .where().eq("ServiceType", serviceType).query().get(0);

        String sql = databaseHelper.
                getServicesTypeRuntimeExceptionDao().queryBuilder()
                .selectColumns(columnName)
                .where().eq("ServiceType", serviceType).prepare().toString();

        if (tblservicestypemaster != null) {
            if (columnName.equalsIgnoreCase("DaysDiffStart"))
                days = tblservicestypemaster.getDaysDiffStart();
            else
                days = tblservicestypemaster.getDaysDiffEnd();
        }

        return days;
    }


    public List<tblservicestypemaster> getServiceName() throws SQLException {

        return databaseHelper.getServicesTypeRuntimeExceptionDao().queryBuilder()
                .selectColumns("ServiceType").orderBy("serviceId", true).groupBy("ServiceType")

                .query();
    }

    public long getCount(String servType) throws SQLException {
        return databaseHelper.getServicesRuntimeExceptionDao().queryBuilder()
                .where().isNotNull("serviceactualDateofaction").
                        and().eq("servicetype", servType).countOf();
    }

    public long getImmunozationCount(String immType) throws SQLException {
        return databaseHelper.getImmunizationRuntimeExceptionDao().queryBuilder()
                .where().eq("immType", immType).countOf();
    }

    public long getExpiredServiceCount(String servType, int villageCode) throws SQLException {

        String strvillageCode = "";

        if (villageCode > 0)
            strvillageCode = " and  regVillage ==" + villageCode;

        String sql = " SELECT count(distinct womanId) FROM `tblserviceslist`  join tblregisteredwomen\n" +
                " on    tblserviceslist.WomenId =  tblregisteredwomen.WomanId " + strvillageCode +
                " where ((`serviceactualDateofaction` IS NULL AND \n" +
                " `servicetype` = '" + servType + "' ) AND (Date(substr(serviceduedatemax , 7) || '-' || \n" +
                " substr(serviceduedatemax ,4,2) || '-'\n" +
                " || substr(serviceduedatemax , 1,2)) < Date('now')) ) ";


        return databaseHelper.
                getServicesTypeRuntimeExceptionDao().
                queryRaw(sql).getResults().size();


       /* String s = databaseHelper.getServicesRuntimeExceptionDao().queryBuilder()
                .where().isNull("serviceactualDateofaction").
                        and().eq("servicetype",servType)
                .and().
                        raw("(Date(substr(serviceduedatemax , 7) || '-' || substr(serviceduedatemax ,4,2) || '-'\n" +
                                " || substr(serviceduedatemax , 1,2)) < Date('now'))").prepare().toString();
         return  databaseHelper.getServicesRuntimeExceptionDao().queryBuilder()
                .where().isNull("serviceactualDateofaction").
                        and().eq("servicetype",servType)
                .and().
                        raw("(Date(substr(serviceduedatemax , 7) || '-' || substr(serviceduedatemax ,4,2) || '-'\n" +
                                " || substr(serviceduedatemax , 1,2)) < Date('now'))").countOf();
   */
    }

    public long getUpcomingServiceCount(String servType, int villageCode) throws SQLException {
       /* return  databaseHelper.getServicesRuntimeExceptionDao().queryBuilder()
                .where().isNull("serviceactualDateofaction").
                        and().eq("servicetype",servType)
                .and().
                        raw("(Date(substr(serviceduedatemax , 7) || '-' || substr(serviceduedatemax ,4,2) || '-'\n" +
                                " || substr(serviceduedatemax , 1,2)) > Date('now'))").countOf();
   */
        String strvillageCode = "";

        if (villageCode > 0)
            strvillageCode = " and  regVillage ==" + villageCode;

        String sql = " SELECT count(distinct womanId) FROM `tblserviceslist`  join tblregisteredwomen\n" +
                " on   tblserviceslist.WomenId =  tblregisteredwomen.WomanId " + strvillageCode +
                " where ((`serviceactualDateofaction` IS NULL AND \n" +
                " `servicetype` = '" + servType + "' ) AND (Date(substr(serviceduedatemin , 7) || '-' || \n" +
                " substr(serviceduedatemin ,4,2) || '-'\n" +
                " || substr(serviceduedatemin , 1,2)) > Date('now')) ) ";


        return databaseHelper.
                getServicesTypeRuntimeExceptionDao().
                queryRaw(sql).getResults().size();
    }


    public long getPlannedServiceCount(String servType, int villageCode) throws SQLException {
        // if(villageCode==0)
//        return  databaseHelper.getServicesRuntimeExceptionDao().queryBuilder()
//                .where().isNull("serviceactualDateofaction").
//                        and().eq("servicetype",servType)
//                .and().
//                        raw(" Date('now') between Date(substr(serviceduedatemin , 7) || '-' ||" +
//                                " substr(serviceduedatemin ,4,2) || '-' " +
//                                " || substr(serviceduedatemin , 1,2)) and  Date(substr(serviceduedatemax , " +
//                                " 7) || '-' || substr(serviceduedatemax ,4,2) || '-' " +
//                                " || substr(serviceduedatemax , 1,2)) ")
//                .countOf();
//        else
//            return  databaseHelper.getServicesRuntimeExceptionDao().queryBuilder()
//                    .where().isNull("serviceactualDateofaction").
//                            and().eq("servicetype",servType)
//                    .and().
//                            raw(" Date('now') between Date(substr(serviceduedatemin , 7) || '-' ||" +
//                                    " substr(serviceduedatemin ,4,2) || '-' " +
//                                    " || substr(serviceduedatemin , 1,2)) and  Date(substr(serviceduedatemax , " +
//                                    " 7) || '-' || substr(serviceduedatemax ,4,2) || '-' " +
//                                    " || substr(serviceduedatemax , 1,2))  ")
//                    .countOf();
        String strvillageCode = "";

        if (villageCode > 0)
            strvillageCode = " and  regVillage ==" + villageCode;

        String sql = " SELECT count(distinct womanId) FROM `tblserviceslist`  join tblregisteredwomen\n" +
                " on  tblserviceslist.WomenId =  tblregisteredwomen.WomanId " + strvillageCode +
                " where ((`serviceactualDateofaction` IS NULL AND \n" +
                " `servicetype` = '" + servType + "' ) AND " +
                " Date('now') between Date(substr(serviceduedatemin , 7) || '-' ||" +
                " substr(serviceduedatemin ,4,2) || '-' " +
                " || substr(serviceduedatemin , 1,2)) and  Date(substr(serviceduedatemax , " +
                " 7) || '-' || substr(serviceduedatemax ,4,2) || '-' " +
                " || substr(serviceduedatemax , 1,2)))  ";
                /*" (Date(substr(serviceduedatemin , 7) || '-' || \n" +
                " substr(serviceduedatemin ,4,2) || '-'\n" +
                " || substr(serviceduedatemin , 1,2)) > Date('now')) ) ";*/


        return databaseHelper.
                getServicesTypeRuntimeExceptionDao().
                queryRaw(sql).getResults().size();


    }


    public int getExpiredImmuCount(String immuType, int villageCode) throws SQLException {

        String sql;
        if (villageCode == 0)
            sql = " select servicetype, immActualDateOfAction, chlDateOfBirth, daysDiffStart," +
                    " daysDiffEnd\n" +
                    " from  tblservicestypemaster   left join tblchildimmunization " +
                    " on  tblservicestypemaster.ServiceType = tblchildimmunization.immType  " +
                    "   left join tblchildinfo  " +
                    "  where  " +
                    " tblservicestypemaster.isImmunisation='Y' and " +
                    " (Date(substr(chlDateOfBirth , 7) || '-' || substr(chlDateOfBirth ,4,2)" +
                    "                             || '-' || substr(chlDateOfBirth , 1,2),'+' || " +
                    " daysDiffEnd ||' day') < Date('now')) and immActualDateofAction is null  and " +
                    "Servicetype='" + immuType + "' order by daysdiffStart";

        else
            sql = " select servicetype, immActualDateOfAction, chlDateOfBirth, daysDiffStart," +
                    " daysDiffEnd\n" +
                    " from  tblservicestypemaster   left join tblchildimmunization " +
                    " on  tblservicestypemaster.ServiceType = tblchildimmunization.immType  " +
                    "   left join tblchildinfo  " +
                    "  where  " +
                    " tblservicestypemaster.isImmunisation='Y' and " +
                    " (Date(substr(chlDateOfBirth , 7) || '-' || substr(chlDateOfBirth ,4,2)" +
                    "                             || '-' || substr(chlDateOfBirth , 1,2),'+' || " +
                    " daysDiffEnd ||' day') < Date('now')) and immActualDateofAction is null  and " +
                    "Servicetype='" + immuType + "' and chltribalhamlet = " + villageCode + " order by daysdiffStart";

        return databaseHelper.
                getServicesTypeRuntimeExceptionDao().
                queryRaw(sql).getResults().size();

    }


    public int getUpcomingImmuCount(String immuType, int villageCode) throws SQLException {

        String sql;
        if (villageCode == 0)
            sql = " select servicetype, immActualDateOfAction, chlDateOfBirth, daysDiffStart," +
                    " daysDiffEnd\n" +
                    " from  tblservicestypemaster   left join tblchildimmunization " +
                    " on  tblservicestypemaster.ServiceType = tblchildimmunization.immType  " +
                    "   left join tblchildinfo  " +
                    "  where  " +
                    " tblservicestypemaster.isImmunisation='Y' and " +
                    " (Date(substr(chlDateOfBirth , 7) || '-' || substr(chlDateOfBirth ,4,2)" +
                    "                             || '-' || substr(chlDateOfBirth , 1,2),'+' || " +
                    " daysDiffStart ||' day') > Date('now')) and immActualDateofAction is null  and " +
                    "Servicetype='" + immuType + "' order by daysdiffStart";
        else
            sql = " select servicetype, immActualDateOfAction, chlDateOfBirth, daysDiffStart," +
                    " daysDiffEnd\n" +
                    " from  tblservicestypemaster   left join tblchildimmunization " +
                    " on  tblservicestypemaster.ServiceType = tblchildimmunization.immType  " +
                    "   left join tblchildinfo  " +
                    "  where  " +
                    " tblservicestypemaster.isImmunisation='Y' and " +
                    " (Date(substr(chlDateOfBirth , 7) || '-' || substr(chlDateOfBirth ,4,2)" +
                    "                             || '-' || substr(chlDateOfBirth , 1,2),'+' || " +
                    " daysDiffStart ||' day') > Date('now')) and immActualDateofAction is null  and " +
                    "Servicetype='" + immuType + "' and chlTribalHamlet = " + villageCode + " order by daysdiffStart";

        return databaseHelper.
                getServicesTypeRuntimeExceptionDao().
                queryRaw(sql).getResults().size();

    }

    public String getPlannedImmuCount(String immuType, int villageCode) throws SQLException {

        String sql;
        if (villageCode == 0)
            sql = " select count(*)\n" +
                    " from  tblservicestypemaster   left join tblchildimmunization  on  tblservicestypemaster.ServiceType\n" +
                    " = tblchildimmunization.immType     left join tblchildinfo    where  \n" +
                    " tblservicestypemaster.isImmunisation='Y' and  \n" +
                    " \n" +
                    "  (Date(substr(chlDateOfBirth , 7) || '-' || substr(chlDateOfBirth ,4,2)\n" +
                    "  || '-' || substr(chlDateOfBirth , 1,2),'+' || \n" +
                    "  daysDiffStart ||' day') <= Date('now'))  and \n" +
                    "  (Date(substr(chlDateOfBirth , 7) || '-' || substr(chlDateOfBirth ,4,2)\n" +
                    "  || '-' || substr(chlDateOfBirth , 1,2),'+' || \n" +
                    "  daysDiffEnd ||' day') >= Date('now'))\n" +
                    " \n" +
                    " \n" +
                    "and Servicetype='" + immuType + "' order by daysdiffStart";

        else
            sql = " select count(*)\n" +
                    " from  tblservicestypemaster   left join tblchildimmunization  on  tblservicestypemaster.ServiceType\n" +
                    " = tblchildimmunization.immType     left join tblchildinfo    where  \n" +
                    " tblservicestypemaster.isImmunisation='Y' and  \n" +
                    " \n" +
                    "  (Date(substr(chlDateOfBirth , 7) || '-' || substr(chlDateOfBirth ,4,2)\n" +
                    "  || '-' || substr(chlDateOfBirth , 1,2),'+' || \n" +
                    "  daysDiffStart ||' day') <= Date('now'))  and \n" +
                    "  (Date(substr(chlDateOfBirth , 7) || '-' || substr(chlDateOfBirth ,4,2)\n" +
                    "  || '-' || substr(chlDateOfBirth , 1,2),'+' || \n" +
                    "  daysDiffEnd ||' day') >= Date('now'))\n" +
                    " \n" +
                    " \n" +
                    "and Servicetype='" + immuType + "' and chlTribalHamlet = " + villageCode + " order by daysdiffStart";


        List<String[]> result = databaseHelper.
                getServicesTypeRuntimeExceptionDao().
                queryRaw(sql).getResults();

        String s = result.get(0)[0];

        return result.get(0)[0];

    }

    //    05Dec2019 Arpitha
    public static int updateEDDService(String userId,
                                       String upSql, int transId,
                                       String editedValues, DatabaseHelper newDb) throws SQLException {

        int update = 0;
        update = newDb.getServicesRuntimeExceptionDao().updateRaw(upSql);


        TransactionHeaderRepository transactionHeaderRepository = new TransactionHeaderRepository(newDb);
        transactionHeaderRepository.updateNew(userId, transId,
                newDb.getServicesRuntimeExceptionDao().getTableName(), upSql, null, null, newDb);

        return update;
    }

    // 09Aug2019 - Bindu - Retain onl pending
    // 05Aug2019 - Bindu chk for ttbooster applicable
    public List<tblserviceslist> getServicesForMother(final tblregisteredwomen woman, int numofdays) throws Exception {
        long limit = 10;
        String[] selectColumns = {"ServiceType", "ServiceActualDateofAction", "ServiceDetail1",
                "ServiceDueDateMin", "ServiceDueDateMax", "ServiceNumber", "ServiceId", "ServiceUserType"};
        QueryBuilder<tblserviceslist, Integer> queryBuilder = databaseHelper.getServicesRuntimeExceptionDao()
                .queryBuilder()
                .selectColumns(selectColumns)
                .orderBy("serviceid", true);
        Where<tblserviceslist, Integer> where = queryBuilder.where()
                .eq("womenId", woman.getWomanId())
                .and().not().eq("ServicePlanned", 3);

        int ttBoosterApplicability = 0;
        if (woman.getRegLastChildAge() != null && !woman.getRegLastChildAge().isEmpty() &&
                woman.getRegLastChildAge().trim().length() > 0) {
            int lastChildAge = Integer.parseInt(woman.getRegLastChildAge());
            if (lastChildAge > 0 && lastChildAge < 36)
                ttBoosterApplicability = 1; //booster applicable
            else if (lastChildAge > 36)
                ttBoosterApplicability = 2;  //booster not applicable
        } else {  //11Aug2019 - Bindu - Chk if TTBooster is provided
            if (isTTBoosterprovided(woman.getWomanId()))
                ttBoosterApplicability = 1;
            else if (isTTprovided(woman.getWomanId()))
                ttBoosterApplicability = 2;
        }
        if (ttBoosterApplicability == 1) {
            where = where.and().not().eq("serviceid", 1)
                    .and().not().eq("serviceid", 2);
        } else if (ttBoosterApplicability == 2) {
            where = where.and().not().eq("serviceid", 3);
        }

        // The following is for Spinner.onItemSelected
        String today = DateTimeUtil.getDateYYYYMMDD(DateTimeUtil.getTodaysDate(), "yyyy-MM-dd");
        if (numofdays == 0) {
            // do nothing
        } else if (numofdays == 1) {
            where = where.and().isNull("ServiceActualDateofAction")
                    //.and().lt("serviceid", 15)
                    .and().raw("((Date(substr(serviceduedatemax , 7) || '-'" +
                            " || substr(serviceduedatemax ,4,2)" +
                            " || '-' || substr(serviceduedatemax , 1,2)) < Date('" + today + "')) or serviceid<15)");
            // .or().lt("serviceid",15);
        } else if (numofdays == 2) {
            where = where.and().isNull("serviceactualDateofaction")
                    .and().raw("(Date('" + today + "') between Date(substr(serviceduedatemin , 7) || '-'" +
                            " || substr(serviceduedatemin ,4,2) || '-' || substr(serviceduedatemin , 1,2))" +
                            " and Date(substr(serviceduedatemax , 7) || '-' || substr(serviceduedatemax ,4,2)" +
                            " || '-' || substr(serviceduedatemax , 1,2))) and serviceid>15 ");
        } else if (numofdays == 3) {  //09Aug2019 - Bindu - change position
            where = where.and().isNull("serviceactualDateofaction")
                    .and().raw("Date(substr(serviceduedatemin , 7) || '-'" +
                            " || substr(serviceduedatemin ,4,2)" +
                            " || '-' || substr(serviceduedatemin , 1,2)) > Date('" + today + "') and serviceid>16 ");
        } else if (numofdays == 4) { //09Aug2019 - Bindu - change position
//            where = where.and().isNotNull("serviceactualDateofaction").or().eq("ServiceType","EDD");
            where = where.and().raw(" (serviceactualDateofaction is not null or ServiceType='EDD' )");
            // ".or().eq("ServiceType","EDD");


        }
        queryBuilder.setWhere(where);
        String s = queryBuilder.prepare().toString();
        return queryBuilder.query();
    }

    //21Mar2021 Bindu - Get is  service provided count = 15 then true , else false
    public boolean getWomanServiceProvided(String womanId) throws SQLException {
        boolean recordexists = false;
        if(databaseHelper.getServicesRuntimeExceptionDao().queryBuilder()
                .where().eq("WomenId",womanId).and().isNull("ServiceActualDateofAction").countOf() < 15)
            recordexists = true;
        else
            recordexists = false;

        return recordexists;
    }

//    10May2021 Arpitha
    public List<TblAncTestsMaster> getAncTests() throws SQLException {
        return  databaseHelper.getTblAncTestsMasterDao().queryForAll();
    }

//    0May2021 Arpitha
    public long insertANcTests(List<TblAncTests> ancTestsList, DatabaseHelper databaseHelper, int transId) throws SQLException {
   
        long added = 0;
        for(int i=0;i<ancTestsList.size();i++) {
            added = databaseHelper.getTblAncTestsDao().create(ancTestsList.get(i));
        }
        if(added>0)
            iNewRecordTransNew(ancTestsList.get(0).getUserId(), transId, databaseHelper.getTblAncTestsDao().getTableName(), databaseHelper);

        return  added;
    }

    public List<TblAncTests> getAncTestsToDisplay(String userId, String womanId, int servNo) throws SQLException {
      return  databaseHelper.getTblAncTestsDao().queryBuilder().where()
                .eq("UserId",userId).and().eq("WomenId",womanId)
                .and().eq("ServiceNumber",servNo).query();

    }
}
