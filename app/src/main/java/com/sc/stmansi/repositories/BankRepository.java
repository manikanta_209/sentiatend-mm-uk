package com.sc.stmansi.repositories;

import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.tables.tblbankdetails;

import java.util.ArrayList;
import java.util.List;

public class BankRepository {

	private DatabaseHelper databaseHelper;

	public BankRepository(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	public ArrayList<String> getBankName() throws Exception {
		ArrayList<String> bankNames = new ArrayList<>();
		RuntimeExceptionDao<tblbankdetails, Integer> bankDetailsDAO = databaseHelper.getBankDetailsRuntimeExceptionDao();
		List<tblbankdetails> bank = bankDetailsDAO.queryBuilder().selectColumns("bankDetails").query();
		for (int i = 0; i < bank.size(); i++) {
			bankNames.add(bank.get(i).getBankDetails());
		}
		return bankNames;
	}
}
