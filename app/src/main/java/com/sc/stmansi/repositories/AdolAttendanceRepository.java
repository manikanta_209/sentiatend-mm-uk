package com.sc.stmansi.repositories;

import com.j256.ormlite.stmt.Where;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.tables.tblAdolAttendanceSavings;

import java.sql.SQLException;
import java.util.List;

public class AdolAttendanceRepository {
    DatabaseHelper databaseHelper;

    String[] columns = {
            "userID",
            "AdolID",
            "CampID",
            "tblAttSavAttendance",
            "transId",
            "DateCreated",
            "DateUpdated"
    };

    public AdolAttendanceRepository(DatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }


    public int insert(tblAdolAttendanceSavings single) {
        return this.databaseHelper.getAttendanceRuntimeExceptionDao().create(single);
    }

    public List<tblAdolAttendanceSavings> getAllAttendanceofAdolID(String adolId) throws SQLException {
        Where<tblAdolAttendanceSavings, Integer> queryBuilder = databaseHelper.getAttendanceRuntimeExceptionDao().queryBuilder().
                selectColumns(columns).where().eq("AdolID",adolId);
        return queryBuilder.query();
    }

    public tblAdolAttendanceSavings getAttendanceofAdolIDandcampId(String adolId,String campId) {
        try{
            Where<tblAdolAttendanceSavings, Integer> queryBuilder = databaseHelper.getAttendanceRuntimeExceptionDao().queryBuilder().
                    selectColumns(columns).where().eq("AdolID",adolId).and().eq("CampID",campId);
            return queryBuilder.query().get(0);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

}
