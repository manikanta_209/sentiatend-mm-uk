package com.sc.stmansi.repositories;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.dao.RawRowMapper;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.tables.TblRegComplMgmt;
import com.sc.stmansi.tables.tblHRPComplicationMgmt;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ComplicationMgmtRepository {

	private DatabaseHelper databaseHelper;

	public ComplicationMgmtRepository(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	public List<TblRegComplMgmt> getComplMgmtNotificationData(String userId, int villageCode, String filter, long limit) throws SQLException {

		List<TblRegComplMgmt> arrVal = new ArrayList<>();
		String sql;
		boolean includeVillageCode = villageCode != 0;
		boolean includeUserFilter = filter != null && filter.trim().length() > 0;
		 limit = 10;

		sql = "SELECT tblregisteredwomen.WomanId,tblregisteredwomen.regWomanName,\n" +
				"  tblregisteredwomen.regLMP,\n" +
				"  tblregisteredwomen.regADDate,\n" +
				"  tblregisteredwomen.regPregnantOrMother,\n" +
				"  tblregisteredwomen.CurrentWomenStatus,\n" +
				"  tblhrpcomplicationmgmt.*\n" +
				"FROM tblhrpcomplicationmgmt\n" +
				"  INNER JOIN tblregisteredwomen ON tblregisteredwomen.WomanId =\n" +
				"    tblhrpcomplicationmgmt.WomanId";

		sql = sql + " where tblregisteredwomen.userId = '"+userId+"' and (tblhrpcomplicationmgmt.hcmClientSeenDate is null or tblhrpcomplicationmgmt.hcmClientSeenDate='')";
		if (includeVillageCode) {
			sql = sql + " and  tblregisteredwomen.regvillage= '"+villageCode+"'";
		}

		if (includeUserFilter) {
			sql = sql + " and (tblregisteredwomen.regwomanName LIKE '%" + filter + "%'  OR tblregisteredwomen.regphonenumber LIKE " +
					"'%" + filter + "%'  OR tblregisteredwomen.reguidNo LIKE '%" + filter + "%')";
		}
		GenericRawResults<TblRegComplMgmt> tbls = databaseHelper.
				gettblRegComplicationMgmtDao().
				queryRaw(sql,
						new RawRowMapper<TblRegComplMgmt>() {
							@Override
							public TblRegComplMgmt mapRow(String[] strings, String[] strings1) {
								TblRegComplMgmt cdatapojo = new TblRegComplMgmt();
								String s = strings[0];
								cdatapojo.setWomanId(strings1[0]);
								cdatapojo.setRegWomanName(strings1[1]);
								cdatapojo.setRegLMP(strings1[2]);
								cdatapojo.setRegADD(strings1[3]);
								cdatapojo.setRegPregnantOrMother(Integer.parseInt(strings1[4]));
								cdatapojo.setCurrentWomenStatus(strings1[5]);
								cdatapojo.setVisitType(strings1[9]);
								cdatapojo.setVisitNum(Integer.parseInt(strings1[10]));
								cdatapojo.setVisitDate(strings1[11]);
								cdatapojo.setAutoId(Integer.parseInt(strings1[6]));
								cdatapojo.setHcmActTakStatus(strings1[22]);
								cdatapojo.setHcmActionToBeTakenClient(strings1[29]);

								return cdatapojo;
							}
						});

		for (TblRegComplMgmt foo : tbls) {
			arrVal.add(foo);

		}
		return arrVal;
	}

	public List<tblHRPComplicationMgmt> getComplMgmtVisitDetails(String womanId, String sessionUserId, int VisitNum, String visitdate) throws SQLException {
		return databaseHelper.gettblComplicationMgmtDao().queryBuilder()
				.where().eq("userId", sessionUserId).and().eq("WomanId",womanId)
				.and().eq("VisitNum", VisitNum)
				//.and().eq("VisitDate",visitdate)
				.query();
	}

	// Get last updated from tblhrpcomplmgmt
	public String getDateLastUpdated(String sessionUserId) throws SQLException{
		String datelastupdated = null;
		Dao<TblRegComplMgmt, Integer> tblcomplDao = databaseHelper.gettblRegComplicationMgmtDao();

		String strqry = "select MAX(ServerUpdatedDate) from tblhrpcomplicationmgmt where UserId = '" + sessionUserId + "'";

		GenericRawResults<String[]> rawResults = tblcomplDao.queryRaw(strqry);

		List<String[]> results = rawResults.getResults();
		// This will select the first result (the first and maybe only row returned)
		String[] resultArray = results.get(0);
		//This will select the first field in the result which should be the ID
		if (resultArray[0] != null)
			datelastupdated = resultArray[0];
		else
			datelastupdated = "";

		return datelastupdated;
	}

	public boolean getComplMgmtVisitDetailsWoman(String womanId, int visitNum) throws SQLException {
		boolean recordexists = false;
		if(databaseHelper.gettblComplicationMgmtDao().queryBuilder()
				.where().eq("WomanId",womanId).and().eq("VisitNum", visitNum).countOf() > 0)
			recordexists = true;
		else
			recordexists = false;

		return recordexists;
	}

	public void updateClientAction(tblHRPComplicationMgmt regcompl, DatabaseHelper databaseHelper, int transId) throws SQLException {
		Dao<tblHRPComplicationMgmt, Integer> tblcomplmgmt = databaseHelper.gettblComplicationMgmtDao();
		UpdateBuilder<tblHRPComplicationMgmt, Integer> updateBuilder = tblcomplmgmt.updateBuilder();

		updateBuilder.updateColumnValue("hcmCommentsClient", regcompl.getHcmCommentsClient())
				.updateColumnValue("hcmClientReceivedDate", regcompl.getHcmClientReceivedDate())
				//.updateColumnValue("hcmClientSeenDate", regcompl.getHcmClientSeenDate())
				.updateColumnValue("hcmActionDateTimeClient", regcompl.getHcmActionDateTimeClient())
				.updateColumnValue("UserType", regcompl.getUserType())
				.updateColumnValue("transid", regcompl.getTranid())
				.where().eq("WomanId", regcompl.getWomanId()).and()
				.eq("AutoId", regcompl.getAutoId());

		int update = updateBuilder.update();
		if (update > 0) {
			TransactionHeaderRepository transactionHeaderRepository = new TransactionHeaderRepository(databaseHelper);
			transactionHeaderRepository.updateNew(regcompl.getUserId(), transId, "tblhrpcomplicationmgmt"
					, updateBuilder.prepare().toString().replace("MappedStatement:", "").trim(),
					null, null, databaseHelper);
		}
	}

	// Update User Seen Date
	public void updateUserSeenDate(String selectedWomanId, int complAutoId, String userId, int transId) {
		try {
			Dao<tblHRPComplicationMgmt, Integer> tblcomplmgmt = databaseHelper.gettblComplicationMgmtDao();
			UpdateBuilder<tblHRPComplicationMgmt, Integer> updateBuilder = tblcomplmgmt.updateBuilder();

			updateBuilder
					.updateColumnValue("hcmClientSeenDate", DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime())
					.where().eq("WomanId", selectedWomanId).and()
					.eq("AutoId", complAutoId);

			int update = updateBuilder.update();
			if (update > 0) {
				TransactionHeaderRepository transactionHeaderRepository = new TransactionHeaderRepository(databaseHelper);
				transactionHeaderRepository.updateNew(userId, transId, "tblhrpcomplicationmgmt"
						, updateBuilder.prepare().toString().replace("MappedStatement:", "").trim(),
						null, null, databaseHelper);
			}
		}catch (Exception e) {
			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}
	}

	public int getBadgeComplMgmtNotification(String sessionUserId) {
		int count = 0;
		try {
			Dao<tblHRPComplicationMgmt, Integer> tblcomplmgmtDao = databaseHelper.gettblComplicationMgmtDao();
			count = (int) tblcomplmgmtDao.queryBuilder().where().isNull("hcmClientSeenDate").or().eq("hcmClientSeenDate","").countOf();
		} catch (SQLException e) {
			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}
		return count;
	}

	public String getVisitASHAComments(int VisitAutoId) throws SQLException{
		String strComments = null;
		Dao<TblRegComplMgmt, Integer> tblcomplDao = databaseHelper.gettblRegComplicationMgmtDao();

		String strqry = "select hcmCommentsClient from tblhrpcomplicationmgmt where AutoId = '" + VisitAutoId + "'";

		GenericRawResults<String[]> rawResults = tblcomplDao.queryRaw(strqry);

		List<String[]> results = rawResults.getResults();
		// This will select the first result (the first and maybe only row returned)
		String[] resultArray = results.get(0);
		//This will select the first field in the result which should be the ID
		if (resultArray[0] != null)
			strComments = resultArray[0];
		else
			strComments = "";

		return strComments;
	}
}
