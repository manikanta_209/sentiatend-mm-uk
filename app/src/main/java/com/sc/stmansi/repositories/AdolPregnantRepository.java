package com.sc.stmansi.repositories;

import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.tables.tblAdolPregnant;

import java.util.List;

public class AdolPregnantRepository {
    private DatabaseHelper databaseHelper;
    String[] columns = {
            "UserId",
            "AdolId",
            "VisitId",
            "pregnantCount",
            "tblAdolPreOutcome",
            "tblAdolPreDoD",
            "tblAdolPreDeliverWhere",
            "tblAdolPreWhoConductDelivery",
            "tblAdolPreTransport",
            "tblAdolPreDeliveryType",
            "tblAdolPreDeliveryStatus",
            "tblAdolPreChildSex",
            "tblAdolPreChildAge",
            "transId",
    };

    public AdolPregnantRepository(DatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }

    public tblAdolPregnant getPregnantbyIDs(String visitCount, String adolId) throws Exception {
        List<tblAdolPregnant> queryBuilder = databaseHelper.gettblAdolPregnantRuntimeExceptionDao().queryBuilder().
                selectColumns(columns).where().eq("AdolId", adolId).and().eq("VisitId", visitCount).query();
        return queryBuilder.get(0);
    }
    public List<tblAdolPregnant> getPregnantsbyIDs( String adolId) throws Exception {
        return databaseHelper.gettblAdolPregnantRuntimeExceptionDao().queryBuilder().
                selectColumns(columns).where().eq("AdolId", adolId).query();
    }

    public List<tblAdolPregnant> checkisBornexist(String adolId, String pregnantCount) throws Exception{
        return databaseHelper.gettblAdolPregnantRuntimeExceptionDao().queryBuilder().selectColumns(columns).where().eq("AdolId",adolId).and().eq("pregnantCount",pregnantCount).query();
    }

    public List<tblAdolPregnant> checkisBornChilds(String adolId) throws Exception{
        return databaseHelper.gettblAdolPregnantRuntimeExceptionDao().queryBuilder().selectColumns(columns).where().eq("AdolId",adolId).query();
    }
}
