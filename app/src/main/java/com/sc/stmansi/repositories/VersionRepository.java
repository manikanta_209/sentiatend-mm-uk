package com.sc.stmansi.repositories;

import com.j256.ormlite.stmt.QueryBuilder;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.tables.tblVersionDetails;

import java.util.List;

public class VersionRepository {
    private DatabaseHelper databaseHelper;

    public VersionRepository(DatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }


    public boolean checkRecords() throws Exception{
        List<tblVersionDetails> list = this.databaseHelper.getVersionDetailsDao().queryForAll();
        if (list.size()==0){
            return false;
        }else {
            return true;
        }
    }

    public tblVersionDetails getLastRecord() throws Exception{
        QueryBuilder<tblVersionDetails, Integer> querybuilder = this.databaseHelper.getVersionDetailsDao().queryBuilder();
        querybuilder.orderBy("transId",false);
        return  querybuilder.query().get(0);
    }

    public int updateData(String sql) throws Exception{
        return this.databaseHelper.getVersionDetailsDao().updateRaw(sql);
    }
}
