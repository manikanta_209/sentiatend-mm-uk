package com.sc.stmansi.repositories;

import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.tables.tblAdolFollowUpMaster;
import com.sc.stmansi.tables.tblAdolVisitDetails;

import java.util.List;

public class AdolFollowupMaterRepository {
    private DatabaseHelper databaseHelper;
    String[] columns = {
            "SlNo" ,"followupGrpID" , "followupGirlorBoy" ,"followupID" , "followupName" , "followupXmlStringName"
    };
    public AdolFollowupMaterRepository(DatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }

    public tblAdolFollowUpMaster getDatabyIds(String adolvisDFollowUps) throws Exception{
        List<tblAdolFollowUpMaster> queryBuilder = databaseHelper.gettblAdolFollowUpRuntimeExceptionDao().queryBuilder().
                selectColumns(columns).where().eq("followupID", adolvisDFollowUps).query();
        return queryBuilder.get(0);
    }
}
