package com.sc.stmansi.repositories;

import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.tables.TblLoginAudit;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ANMRepository {

	private DatabaseHelper databaseHelper;

	public ANMRepository(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	// TODO move this to LoginRepository
	public List<String> getAnmIds() throws SQLException {
		List<TblLoginAudit> log = this.databaseHelper.getLoginAuditRuntimeExceptionDao().queryBuilder().distinct()
				.selectColumns("userId").query();
		List<String> list = new ArrayList<>();
		for (TblLoginAudit login : log) {
			list.add(login.getUserId());
		}
		return list;
	}
}
