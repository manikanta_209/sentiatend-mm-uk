package com.sc.stmansi.repositories;

import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.tables.tblAdolVisitHeader;

import java.util.List;

public class AdolVisitHeaderRepository {
    private DatabaseHelper databaseHelper;
    private String[] columns = {
            "UserId" ,
            "AdolId" ,
            "VisitId" ,
            "adolvisHVisitDate" ,
            "adolvisHGTS" ,
            "adolvisHEducation" ,
            "adolvisHIsPeriodstarted" ,
            "adolvisHAgeMenstural" ,
            "adolvisHUsedforPeriods" ,
            "adolvisHUsedforPeriodsOthers" ,
            "adolvisHMenstrualProblem" ,
            "adolvisHMenstrualProblemOthers" ,
            "adolvisHMaritalStatus" ,
            "adolvisHPartnerName" ,
            "adolvisHPartnerOccup" ,
            "adolvisHAgeatMarriage" ,
            "adolvisHIsPregnantatReg" ,
            "adolvisHHb" ,
            "adolvisHHbDate",
            "adolvisHHbCategory" ,
            "adolvisHHeightType" ,
            "adolvisHHeight" ,
            "adolvisHWeight" ,
            "adolvisHBMI" ,
            "adolvisHIsIFATaken",
            "adolvisHIFADaily" ,
            "adolvisHIFATabletsCount" ,
            "adolvisHIFANotTakenReason" ,
            "adolvisHIFAGivenBy" ,
            "adolvisHDewormingtab"  ,
            "adolvisHHealthIssues"  ,
            "adolvisHHealthIssuesOthers"  ,
            "adolvisHTreatedAt" ,
            "adolvisHGeneralTreatment" ,
            "adolvisHGeneralTreatmentOthers" ,
            "adolvisHContraceptionAware" ,
            "adolvisHContraceptionNeed" ,
            "adolvisHContraceptionMethod" ,
            "adolvisHContraceptionMethodOthers" ,
            "adolvisHComplications",
            "RecordCreatedDate" ,
            "RecordUpdatedDate",
            "adolvisHAshaAvailable"
    };

    public AdolVisitHeaderRepository(DatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }


    public String generateVisitID(int visit_id, String adolID) {
        String returnValue;
        if (visit_id <= 9) {
            returnValue = "000" + visit_id;
        } else if (visit_id <= 99) {
            returnValue = "00" + visit_id;
        } else if (visit_id <= 999) {
            returnValue = "0" + visit_id;
        } else {
            returnValue = String.valueOf(visit_id);
        }
        return adolID + returnValue;
    }

    public List<tblAdolVisitHeader> getAllVisits(String adolId) throws Exception {
        return databaseHelper.gettblAdolVisitHeaderRuntimeExceptionDao().queryBuilder().
                selectColumns(columns).where().eq("AdolID", adolId).query();
    }

    public tblAdolVisitHeader getVisitbyID(String visitCount, String adolId) {
        try{
            List<tblAdolVisitHeader> queryBuilder = databaseHelper.gettblAdolVisitHeaderRuntimeExceptionDao().queryBuilder().
                    selectColumns(columns).where().eq("AdolId", adolId).and().eq("VisitId", visitCount).query();
            return queryBuilder.get(0);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
