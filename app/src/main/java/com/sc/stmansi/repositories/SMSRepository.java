package com.sc.stmansi.repositories;

import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.tables.MessageLogPojo;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SMSRepository {

    private DatabaseHelper databaseHelper;

    public SMSRepository(DatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }

    public void updateMessageLog(MessageLogPojo mlp, boolean isSuccess,
                                 DatabaseHelper databaseHelper, int transId) throws Exception {

        String sql;
        if (isSuccess) {
           /* Object[] arr = new Object[]{DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime(),
                    mlp.getUserId(),
                    mlp.getWomanId(), mlp.getMsgId()};
*/

            sql = "Update tblmessagelog Set msgSentDate = '" + DateTimeUtil.getTodaysDate()
                    + " " + DateTimeUtil.getCurrentTime() + "', msgSent = 1 "
                    + " Where UserId = '" + mlp.getUserId() + "' "
                    + " and msgId = " + mlp.getMsgId();
        } else {

//            Object[] arr = new Object[]{mlp.getUserId(), mlp.getWomanId(), mlp.getMsgId()};

            sql = "Update tblmessagelog Set msgNoOfFailures = (msgNoOfFailures + 1) "
                    + " Where UserId = '" + mlp.getUserId() + "'  "
                    + " and msgId = " + mlp.getMsgId();
        }
        databaseHelper.getMessageLogDao().updateRaw(sql);


        TransactionHeaderRepository transactionHeaderRepository = new TransactionHeaderRepository(databaseHelper);
        transactionHeaderRepository.updateNew(mlp.getUserId(), transId, databaseHelper.getMessageLogDao().getTableName(),
                sql, null, null, databaseHelper);


    }

    /**
     * Get Failed Sms OR which is not Sent
     *
     * @throws Exception
     */
    public List<MessageLogPojo> getFailedSmsOrNotSent() throws Exception {


//        String s = databaseHelper.getMessageLogDao()
//                .queryBuilder().where()
//                .eq("msgSent",0)
//                .and().lt("msgNoOfFailures",1).prepare().toString();

     /* List<MessageLogPojo> messageLogPojoList =  databaseHelper.getMessageLogDao()
              .queryBuilder().where()
                .eq("msgSent",0)
              .and().lt("msgNoOfFailures",1).query();12May2021 Arpitha*/

        String s = databaseHelper.getMessageLogDao()
                .queryBuilder().where()
                //  .eq("msgSent",0)
                .raw("  transid =  (Select max(transId) from tblmessagelog)").prepare().toString();

        List<MessageLogPojo> messageLogPojoList = databaseHelper.getMessageLogDao()
                .queryBuilder().where()
//                .eq("msgSent",0)
                .raw(" transid = ( Select max(transId) from tblmessagelog)").query();

        if (messageLogPojoList != null && messageLogPojoList.size() > 0)
            return messageLogPojoList;

        else
            return null;
    }

//    26Nov2019 Arpitha

    public int insertIntoMessageLog(MessageLogPojo messageLogPojo, DatabaseHelper databaseHelper) throws SQLException {
        return databaseHelper.getMessageLogDao().create(messageLogPojo);
    }

    //    28May2021 Arpitha
    public List<MessageLogPojo> getMessageLogData(String userId) throws SQLException {
        List<MessageLogPojo> messageLogPojoList = new ArrayList<>();
        messageLogPojoList = databaseHelper.getMessageLogDao().queryBuilder().limit((long) 15).orderBy("transId", false)
                .where()
                .eq("UserId", userId).query();

        return messageLogPojoList;
    }


    //    30May2021 Arpitha
    public int updateIsSMSRecvdByCC(MessageLogPojo mlp,
                                     DatabaseHelper databaseHelper) throws Exception {

        String sql;
        int update;


        sql = "Update tblmessagelog Set msgRecvdByCC = 'Yes' "
                + " Where UserId = '" + mlp.getUserId() + "'  "
                + " and msgId = " + mlp.getMsgId();

         update  = databaseHelper.getMessageLogDao().updateRaw(sql);


         if(update > 0) {
             final TransactionHeaderRepository transactionHeaderRepository = new TransactionHeaderRepository(databaseHelper);
             int transId = transactionHeaderRepository.iCreateNewTrans(mlp.getUserId(), databaseHelper);

             transactionHeaderRepository.updateNew(mlp.getUserId(), transId, databaseHelper.getMessageLogDao().getTableName(),
                     sql, null, null, databaseHelper);
         }

        return   update;

    }

}
