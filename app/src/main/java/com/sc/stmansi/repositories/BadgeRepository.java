package com.sc.stmansi.repositories;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.tables.TblBadgeCount;
import com.sc.stmansi.tables.TblChildInfo;
import com.sc.stmansi.tables.tblAdolReg;
import com.sc.stmansi.tables.tblCaseMgmt;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.tables.tblserviceslist;
import com.sc.stmansi.tables.tbltransHeader;

import java.util.HashMap;
import java.util.List;

public class BadgeRepository {

	private DatabaseHelper databaseHelper;

	public BadgeRepository(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	public HashMap<String, Integer> getBadgeCountForMenuItems() {
		HashMap<String, Integer> badgeMap = new HashMap<>();
		try {

			RuntimeExceptionDao<TblBadgeCount, Integer> badgeDAO = databaseHelper.getBadgeRuntimeExceptionDao();
			List<TblBadgeCount> listBadge = badgeDAO.queryForAll();

			for (int i = 0; i < listBadge.size(); i++) {
				badgeMap.put(listBadge.get(i).getMenuItemName(), listBadge.get(i).getBadgeCount());
			}
		} catch (Exception e) {
			// TODO add logging framework

			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}
		return badgeMap;
	}


	public void updateBadgeCountForMenuItems() {
		try {
			RuntimeExceptionDao<tblregisteredwomen, Integer> regDAO = databaseHelper.gettblregRuntimeExceptionDao();
			RuntimeExceptionDao<TblBadgeCount, Integer> badgeDAO = databaseHelper.getBadgeRuntimeExceptionDao();
			RuntimeExceptionDao<tbltransHeader, Integer> transheaderDAO = databaseHelper.getTransHeaderRuntimeExceptionDao();
			RuntimeExceptionDao<tblserviceslist, Integer> tblservicesDAO = databaseHelper.getServicesRuntimeExceptionDao();
			UpdateBuilder<TblBadgeCount, Integer> update = badgeDAO.updateBuilder();
			RuntimeExceptionDao<TblChildInfo, Integer> tblChildDAO = databaseHelper.getChildInfoRuntimeExceptionDao();
			RuntimeExceptionDao<tblregisteredwomen, Integer> ancDAO = databaseHelper.gettblregRuntimeExceptionDao();
			RuntimeExceptionDao<tblregisteredwomen, Integer> pncDAO = databaseHelper.gettblregRuntimeExceptionDao();
//15MAy2021 Bindu add adol
			RuntimeExceptionDao<tblAdolReg, Integer> tblAdolDAO = databaseHelper.gettblAdolregRuntimeExceptionDao();

			Dao<tblCaseMgmt, Integer> tblCaseMgmtDAO = databaseHelper.getTblCaseMgmtDao();

			/*long reglistCount = regDAO.queryBuilder().where()
					.isNull("DateDeactivated").countOf();
			update.updateColumnValue("BadgeCount", reglistCount)
					.where().eq("MenuItemName", "Registered Women");
			update.update();
*/


			String sqlAllReg = " Select count (distinct tblregisteredwomen.WomanId)  " +
					"from tblregisteredwomen left join tblchildinfo on tblregisteredwomen.WomanId = tblchildinfo.WomanId where " +
					"((regpregnantorMother = 2 and (DateDeactivated is null or chlDeactDate is null ))" +
					/*" (regpregnantorMother = 2" +

					" and ((DateDeactivated is null and delMothersDeath!='Y') or ((chlDeactDate is null and " +
					"chlDeliveryResult==1 )and chlReg=0) )"+*/
					" or  (regpregnantorMother = 1 and (DateDeactivated is null)) )  order by tblregisteredwomen.transId desc ";
			GenericRawResults<String[]> genReg = regDAO.queryRaw(sqlAllReg);

			List<String[]> listAllReg = genReg.getResults();


			int regCnt = Integer.parseInt(listAllReg.get(0)[0]);
			update.updateColumnValue("BadgeCount", regCnt)
					.where().eq("MenuItemName", "Registered Women");
			update.update();


			long hrpCount = regDAO.queryBuilder().where()
					.eq("regComplicatedpreg", 1)
					.or().eq("isCompl", "Y")
					.and().isNull("DateDeactivated")
					.countOf();
			update.updateColumnValue("BadgeCount", hrpCount)
					.where().eq("MenuItemName", "HRP");
			update.update();


			String sqlHv = " SELECT count(*) FROM `tblregisteredwomen` WHERE   ((regpregnantormother==1 and DateDeactivated is null) or (regpregnantormother ==2 and ((DateDeactivated is null and womanid   In (Select womanid from tbldeliveryinfo where delMothersdeath='N')) or womanid   In (Select womanid from tblchildinfo where (chlDeliveryResult==1 and chlDeactDate is  null)))))  ORDER BY `transid` DESC ";
			GenericRawResults<String[]> genHV = regDAO.queryRaw(sqlHv);

			List<String[]> listHV = genHV.getResults();


			int hvCnt = Integer.parseInt(listHV.get(0)[0]);

			/*long homevisitCnt = regDAO.queryBuilder().where()
					.isNull("DateDeactivated").countOf();*/
			update.updateColumnValue("BadgeCount", hvCnt)
					.where().eq("MenuItemName", "Home Visit");
			update.update();


			long complcnt = regDAO.queryBuilder().where().
					eq("regInDanger", 1).
					and().isNull("DateDeactivated").countOf();
			update.updateColumnValue("BadgeCount", complcnt)
					.where().eq("MenuItemName", "Complications");
			update.update();


			/*String sql = "Select  distinct womenid, servicetype as aa from tblserviceslist where serviceactualdateofaction is null\n" +
					"and \n" +
					" Date(substr(ServiceDueDateMin , 7) || '-' ||\n" +
					"                     substr(ServiceDueDateMin ,4,2)\n" +
					"                     || '-' || substr(ServiceDueDateMin , 1,2))<=Date('now')  and serviceplanned=1 \n" +
					"\t\t\t\t\t\t\"\\t\\t\\t\\t\\t UNION ALL\\n\" +\n\t\t\t\t \n" +
					"\t\t\t\t\t\n" +
					"\t\t\t\t\n" +
					"Select  distinct womenid, servicetype as bb from tblserviceslist where serviceactualdateofaction is null\n" +
					"and \n" +
					" Date(substr(ServiceDueDateMin , 7) || '-' ||\n" +
					"                     substr(ServiceDueDateMin ,4,2)\n" +
					"                     || '-' || substr(ServiceDueDateMin , 1,2))>Date('now')";*/

			/*String sql = "select sum(cnt) from (\n" +
					"Select sm.servicetype, t3.cnt from tblservicestypemaster sm left join\n" +
					"                   (Select sum(cast(t2.wid as decimal)) as cnt, servicetype from(\n" +
					"                Select \n" +
					"               count(distinct(womenid)) as wid, servicetype as servicetype\n" +
					"                   from tblserviceslist sl inner join tblregisteredwomen reg on\n" +
					"                    sl.womenid = reg.womanid where   serviceactualdateofaction\n" +
					"                 is null and serviceplanned!=3    and serviceplanned != 2   \n" +
					"                  and serviceactualdateofaction is null and \n" +
					"               \n" +
					"                Date(substr(serviceduedatemin , 7) || '-'\n" +
					"                 || substr(serviceduedatemin ,4,2)\n" +
					"                  || '-' || substr(serviceduedatemin , 1,2))>('2020-01-20')   and datedeactivated is null \n" +
					"              \n" +
					"                   and (( womanid \n" +
					"                 IN (select womanid from tblchildinfo where chlDeactDate\n" +
					"                 is null) and regpregnantormother=2) or regpregnantormother=1)\n" +
					"            \n" +
					"                group by servicetype,womenid )\n" +
					"                   as t2 group by t2.servicetype) as t3\n" +
					"                  on sm.servicetype = t3.servicetype where isImmunisation = 'N'\n" +
					"                     group by sm.servicetype UNION ALL Select sm.servicetype, t3.cnt from tblservicestypemaster sm left join\n" +
					"                  (Select sum(cast(t2.wid as decimal)) as cnt, servicetype from(\n" +
					"                 Select \n" +
					"                  count(distinct(womenid)) as wid, servicetype as servicetype\n" +
					"                  from tblserviceslist sl inner join tblregisteredwomen reg on\n" +
					"                 sl.womenid = reg.womanid where   serviceactualdateofaction\n" +
					"                    is null and serviceplanned=1   \n" +
					"                     and serviceactualdateofaction is null  and \n" +
					"                Date(substr(serviceduedatemin , 7) || '-'\n" +
					"                  || substr(serviceduedatemin ,4,2)\n" +
					"                  || '-' || substr(serviceduedatemin , 1,2))<=('now')  and datedeactivated is null\n" +
					"                  and (( womanid \n" +
					"                IN (select womanid from tblchildinfo where chlDeactDate\n" +
					"                 is null) and regpregnantormother=2) or regpregnantormother=1) \n" +
					"               \n" +
					"                group by servicetype,womenid )\n" +
					"                  as t2 group by t2.servicetype) as t3\n" +
					"                    on sm.servicetype = t3.servicetype where isImmunisation = 'N'\n" +
					"                     group by sm.servicetype)";*/


			String selDtae = DateTimeUtil.getDateYYYYMMDD
					(DateTimeUtil.getTodaysDate(), "yyyy-MM-dd");
			String sql  = "Select   sum(cnt) from (Select sm.servicetype, t3.cnt from tblservicestypemaster sm left join" +
					"                  (Select sum(cast(t2.wid as decimal)) as cnt, servicetype from(" +
					"                  Select " +
					"                   count(distinct(womenid)) as wid, servicetype as servicetype" +
					"                   from tblserviceslist sl inner join tblregisteredwomen reg on" +
					"                   sl.womenid = reg.womanid where   serviceactualdateofaction" +
					"                   is null and serviceplanned=1      " +
					"                  and serviceactualdateofaction is null  and " +
					"              " +
					"               Date(substr(serviceduedatemin , 7) || '-'" +
					"                  || substr(serviceduedatemin ,4,2)" +
					"                  || '-' || substr(serviceduedatemin , 1,2))<=('"+selDtae+"')  and datedeactivated is null" +
					"                   " +
					"                   and (( womanid " +
					"                 IN (select womanid from tblchildinfo where chlDeactDate" +
					"                 is null) and regpregnantormother=2) or regpregnantormother=1) " +
					"              " +
					"                group by servicetype,womenid )" +
					"                 as t2 group by t2.servicetype) as t3" +
					"                   on sm.servicetype = t3.servicetype where isImmunisation = 'N'" +
					"                     group by sm.servicetype  UNION ALL Select sm.servicetype, t3.cnt from tblservicestypemaster sm left join\n" +
					"                   (Select sum(cast(t2.wid as decimal)) as cnt, servicetype from(" +
					"                   Select " +
					"                 count(distinct(womenid)) as wid, servicetype as servicetype" +
					"                   from tblserviceslist sl inner join tblregisteredwomen reg on" +
					"                    sl.womenid = reg.womanid where   serviceactualdateofaction" +
					"                 is null and serviceplanned!=3    and serviceplanned != 2  " +
					"                  and serviceactualdateofaction is null and " +
					"               " +
					"                 Date(substr(serviceduedatemin , 7) || '-'" +
					"                  || substr(serviceduedatemin ,4,2)" +
					"                  || '-' || substr(serviceduedatemin , 1,2))>('"+selDtae+"')   and datedeactivated is null" +
					"               " +
					"                   and (( womanid " +
					"                 IN (select womanid from tblchildinfo where chlDeactDate" +
					"                 is null) and regpregnantormother=2) or regpregnantormother=1)" +
					"               " +
					"               group by servicetype,womenid )" +
					"                   as t2 group by t2.servicetype) as t3" +
					"                  on sm.servicetype = t3.servicetype where isImmunisation = 'N'" +
					"                     group by sm.servicetype )";
			GenericRawResults<String[]> gen = tblservicesDAO.queryRaw(sql);

			List<String[]> list = gen.getResults();
			//15Apr2021 Bindu - Check null
			int pendingServCnt = 0;
					String listsize = list.get(0)[0];

			 pendingServCnt = listsize != null ? Integer.parseInt(list.get(0)[0]) : 0;

			update.updateColumnValue("BadgeCount", pendingServCnt)
					.where().eq("MenuItemName", "Pending Activities");
			update.update();


			QueryBuilder<tblregisteredwomen,
					Integer> qb = regDAO.queryBuilder();
			qb.where().raw("(CurrentWomenStatus is null OR CurrentWomenStatus = '') \n" +
					"\t\t\t and DateDeactivated is null " +
					"\t\t\t and julianday('now') - julianday(substr(regLMP , 7) || '-' ||\n" +
					"\t\t\t substr(regLMP ,4,2)  || '-' " +
					"|| substr(regLMP , 1,2)) > 210 and (regADDate is null or regADDate ='')");
			long nearingDel = qb.countOf();
			update.updateColumnValue("BadgeCount", nearingDel)
					.where().eq("MenuItemName", "Nearing Delivery");
			update.update();


			long deactivateCnt = regDAO.queryBuilder().where()
					.isNotNull("DateDeactivated").countOf();
			update.updateColumnValue("BadgeCount", deactivateCnt)
					.where().eq("MenuItemName", "Deactivate");
			update.update();


			long syncCnt = transheaderDAO.queryBuilder().where()
					.eq("TransStatus", "N").or()
					.eq("TransStatus", "A").or()   //05Dec2019 - Bindu - Add A status
					.eq("TransStatus", "S").countOf();
			update.updateColumnValue("BadgeCount", syncCnt)
					.where().eq("MenuItemName", "Sync");
			update.update();

//09Nov2019 Arpitha
			long childCnt = tblChildDAO.queryBuilder().where().isNull("chlDeactDate").countOf();
			update.updateColumnValue("BadgeCount", childCnt)
					.where().eq("MenuItemName", "Registered Child List");
			update.update();

			ImmunizationRepository childRepository = new ImmunizationRepository(databaseHelper);


			String strFromDate =  childRepository.getEarliestDate();

			String sqlImmu = "Select count(tblchildinfo.chlId)  from  tblservicestypemaster  \n" +
					"\n" +
					" left join tblchildinfo on \n" +
					"                  date((Date(substr(chlDateOfBirth , 7) || '-' ||  \n" +
					" substr(chlDateOfBirth ,4,2) || '-' ||  substr(chlDateOfBirth , 1,2))), '+'||DaysDiffStart ||' day')  \n" +
					" between Date(substr('"+strFromDate+"' , 7) \n" +
					"|| '-' || substr('"+strFromDate+"' ,4,2) || '-' || substr('"+strFromDate+"' , 1,2))" +
					" and Date(substr('"+ DateTimeUtil.getTodaysDate() +"' , 7) || '-'  || substr('"+ DateTimeUtil.getTodaysDate() +"' ,4,2)\n" +
					" || '-' || substr('"+ DateTimeUtil.getTodaysDate() +"' , 1,2))" +
//					"                   substr(chlDateOfBirth ,4,2) || '-' || \n" +
//					"                   substr(chlDateOfBirth , 1,2))), '+'||DaysDiffStart ||' day')  \n" +
//					"                   <=   Date('now') " +
					" and\n" +
					" tblchildinfo.chlId not in (select immChildId from tblchildimmunization  \n" +
					" where tblservicestypemaster.ServiceType = tblchildimmunization.immtype) \n" +
					" and   chldeliveryresult<=1 and chlid not null\n" +
					" where  isImmunisation='Y' and chlDeactDate is null ";
			GenericRawResults<String[]> gen1 = tblservicesDAO.queryRaw(sqlImmu);

			List<String[]> listImmu = gen1.getResults();


			int immCnt = Integer.parseInt(listImmu.get(0)[0]);
			update.updateColumnValue("BadgeCount", immCnt)
					.where().eq("MenuItemName", "Immunization");
			update.update();

			//15Nov2019 Arpitha
			long ancCount = ancDAO.queryBuilder().where()
					.eq("regPregnantorMother",1).and().isNull("DateDeactivated").countOf();
			update.updateColumnValue("BadgeCount", ancCount)
					.where().eq("MenuItemName", "ANC");
			update.update();



			String sqlPncReg = "  Select count (distinct tblregisteredwomen.WomanId)  " +
					" from tblregisteredwomen left join tblchildinfo " +
					" on tblregisteredwomen.WomanId = tblchildinfo.WomanId left join" +
					" tbldeliveryinfo on tblregisteredwomen.WomanId = tbldeliveryinfo.WomanId where " +
					" regpregnantorMother = 2 and ((DateDeactivated is null and delMothersDeath!='Y') or ((chlDeactDate is null and " +
					" chlDeliveryResult==1 )and chlReg=0) )   order by tblregisteredwomen.transId desc " ;
					//"  order by tblregisteredwomen.transId desc  ";

		/*String sqlPncReg = " Select distinct regWomanName, regRegistrationDate, regStatusWhileRegistration, " +
				" regComplicatedpreg, regAmberOrRedColorCode, regrecommendedPlaceOfDelivery, " +
				"tblregisteredwomen.UserId,tblregisteredwomen.WomanId, regVillage, regLMP, IsCompl, " +
				" LastAncVisitDate, LastPncVisitDate, IsReferred, regADDate,  regriskFactors, regPregnantorMother , " +
				"regInDanger, DateDeactivated, regWomenImage from tblregisteredwomen left join tblchildinfo on" +
				" tblregisteredwomen.WomanId = tblchildinfo.WomanId\n" +
				"\n" +
				"left join tbldeliveryinfo on tblregisteredwomen.WomanId = tbldeliveryinfo.WomanId\n" +
				"\n" +
				" where  tblregisteredwomen.UserId = '10024'  and regpregnantorMother = 2\n" +
				"\n" +
				"\n" +
				" and ((DateDeactivated is null and delMothersDeath!='Y') or ((chlDeactDate is null and \n" +
				"chlDeliveryResult==1 )and chlReg=0) )   order by tblregisteredwomen.transId desc  LIMIT 10 OFFSET 0";
	*/		GenericRawResults<String[]> genPNC = regDAO.queryRaw(sqlPncReg);

			List<String[]> listPNCReg = genPNC.getResults();


			int pncCnt = Integer.parseInt(listPNCReg.get(0)[0]);
			update.updateColumnValue("BadgeCount", pncCnt)
					.where().eq("MenuItemName", "PNC");
			update.update();


			//30Nov2019 Arpitha
			long wDeactCount = databaseHelper.gettblregRuntimeExceptionDao().queryBuilder()
					.where().isNotNull("DateDeactivated").countOf();
			update.updateColumnValue("BadgeCount", wDeactCount)
					.where().eq("MenuItemName", "Woman Deactivation");
			update.update();


			//30Nov2019 Arpitha
			long cDeactCount = databaseHelper.getChildInfoRuntimeExceptionDao().queryBuilder()
					.where().isNotNull("chlDeactDate").countOf();
			update.updateColumnValue("BadgeCount", cDeactCount)
					.where().eq("MenuItemName", "Child Deactivation");
			update.update();

//			15May2021 Bindu add Adol badge count
			long adolCnt = tblAdolDAO.queryBuilder().where().eq("regAdolDeactivateDate", "").countOf();
			update.updateColumnValue("BadgeCount", adolCnt)
					.where().eq("MenuItemName", "Adolescent");
			update.update();

//			Ramesh 22-jul-2021

			String strcasemgmt = "  select count(*) from tblCaseMgmt where RecordViewedDate is NULL and BeneficiaryId != 'All';" ;

			GenericRawResults<String[]> gencasemgmt = tblCaseMgmtDAO.queryRaw(strcasemgmt);

			List<String[]> listcasemgmt = gencasemgmt.getResults();


			int casecnt = Integer.parseInt(listcasemgmt.get(0)[0]);
			update.updateColumnValue("BadgeCount", casecnt)
					.where().eq("MenuItemName", "Notification");
			update.update();


		} catch (Exception e) {
			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}

	}
}
