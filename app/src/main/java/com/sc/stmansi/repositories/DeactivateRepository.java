package com.sc.stmansi.repositories;

import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.tables.TblDeactivate;

import java.sql.SQLException;

public class DeactivateRepository {

    private DatabaseHelper databaseHelper;

    public DeactivateRepository(DatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }

    public int insertIntoTblDeactivate(TblDeactivate tblDeactivate) throws SQLException {
        int added = 0;
        databaseHelper.getDeactivateRuntimeExceptionDao().create(tblDeactivate);
        return added;
    }
}
