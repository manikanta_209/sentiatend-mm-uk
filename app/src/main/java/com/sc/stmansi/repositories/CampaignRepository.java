package com.sc.stmansi.repositories;

import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.dao.RawRowMapper;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.tables.tblCampDetails;

import java.sql.SQLException;
import java.util.List;

public class CampaignRepository {
    private DatabaseHelper databaseHelper;

    String[] columns = {"userId",
            "tblCampID",
            "tblCampName",
            "tblCampDateTime",
            "tblCampDesc",
            "tblCampPresent",
            "tblCampAbsent",
            "tblCampSchemaType",
            "tblCampCreatedBy",
            "tblCampisActive",
            "tblCampSummary",
            "tblCampEndTime",
            "tblCampFacilities",
            "tblCampOutcome",
            "tblCampCCAccompany",
            "tblCampProObjective",
            "tblCampProObjOthers"
    };

    public CampaignRepository(DatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }


    public String getlastCampID() throws Exception {
        String sql = "select MAX(tblCampID) from tblCampDetails";
        GenericRawResults<String[]> rawResults = databaseHelper.gettblregRuntimeExceptionDao().queryRaw(sql);
        List<String[]> results = rawResults.getResults();
        String[] resultArray = results.get(0);
        String finalResult = resultArray[0];
        if (finalResult != null) {
            int lastParentno = Integer.parseInt(finalResult);
            lastParentno = lastParentno + 1;

            String returnValue;
            if (lastParentno <= 9) {
                returnValue = "000" + lastParentno;
            } else if (lastParentno <= 99) {
                returnValue = "00" + lastParentno;
            } else if (lastParentno <= 999) {
                returnValue = "0" + lastParentno;
            } else {
                returnValue = String.valueOf(lastParentno);
            }
            return returnValue;
        } else {
            return "0001";
        }
    }

    public int createCampaign(tblCampDetails tblCampDetails) throws Exception {
        return this.databaseHelper.getCampaignRuntimeExceptionDao().create(tblCampDetails);
    }

    public tblCampDetails getCampDetailsbyID(String campID) throws Exception {
        List<tblCampDetails> q = databaseHelper.getCampaignRuntimeExceptionDao().queryBuilder().selectColumns(columns).where().eq("tblCampID", campID).query();
        return q.get(0);
    }

    public List<tblCampDetails> getAllCampaign() throws Exception {
        QueryBuilder<tblCampDetails, Integer> query = this.databaseHelper.getCampaignRuntimeExceptionDao().queryBuilder().selectColumns(columns).orderBy("tblCampID", false);
        return query.query();
    }

    public int updateCampDetails(tblCampDetails currentCampDetails, String campID) throws Exception {
        UpdateBuilder<tblCampDetails, Integer> updateBuilder = this.databaseHelper.getCampaignRuntimeExceptionDao().updateBuilder();
        updateBuilder.updateColumnValue("tblCampPresent", currentCampDetails.getTblCampPresent())
                .updateColumnValue("tblCampAbsent", currentCampDetails.getTblCampAbsent())
                .updateColumnValue("tblCampSummary", currentCampDetails.getTblCampSummary())
                .updateColumnValue("tblCampEndTime", currentCampDetails.getTblCampEndTime())
                .updateColumnValue("transId", currentCampDetails.getTransId())
                .updateColumnValue("tblCampisActive", currentCampDetails.getTblCampisActive()).where().eq("tblCampID", campID);
        return updateBuilder.update();
    }

    public int updaterawSql(String sql) {
        RuntimeExceptionDao<tblCampDetails, Integer> dao = this.databaseHelper.getCampaignRuntimeExceptionDao();
        return dao.updateRaw(sql);
    }


    public List<tblCampDetails> getCampaignsbyFacility(String facilityindex) throws Exception {
        if (facilityindex.equals("0")) {
            return this.getAllCampaign();
        } else {
            return databaseHelper.getCampaignRuntimeExceptionDao().queryBuilder().selectColumns(columns).where().eq("tblCampFacilities", facilityindex).query();
        }
    }

    public List<tblCampDetails> getCampaignsbyMonthandYear(String year, String month) throws Exception {
        String sql = "select * from tblCampDetails where Substr(tblCampDateTime,4,2)=='04' and Substr(tblCampDateTime,7,4)=='2021';";
        if (year.equals("All")) {
            if (month.equals("00")) {
                return this.getAllCampaign();
            } else if (!(month.equals("00"))) {
                sql = "select * from tblCampDetails where Substr(tblCampDateTime,4,2)=='" + month + "';";
            }
        } else if (!(year.equals("All"))) {
            if (month.equals("00")) {
                sql = "select * from tblCampDetails where Substr(tblCampDateTime,7,4)=='" + year + "';";
            } else if (!(month.equals("00"))) {
                sql = "select * from tblCampDetails where Substr(tblCampDateTime,4,2)=='" + month + "' and Substr(tblCampDateTime,7,4)=='" + year + "';";
            }
        }

        GenericRawResults<tblCampDetails> result = this.databaseHelper.getCampaignRuntimeExceptionDao().queryRaw(sql, new RawRowMapper<tblCampDetails>() {
            @Override
            public tblCampDetails mapRow(String[] columnNames, String[] resultColumns) throws SQLException {
                tblCampDetails tblCampDetails = new tblCampDetails();
                tblCampDetails.setUserId(resultColumns[0]);
                tblCampDetails.setTblCampID(resultColumns[1]);
                tblCampDetails.setTblCampName(resultColumns[2]);
                tblCampDetails.setTblCampDateTime(resultColumns[3]);
                tblCampDetails.setTblCampDesc(resultColumns[4]);
                tblCampDetails.setTblCampPresent(resultColumns[5]);
                tblCampDetails.setTblCampAbsent(resultColumns[6]);
                tblCampDetails.setTblCampSchemaType(Integer.parseInt(resultColumns[7]));
                tblCampDetails.setTblCampFacilities(resultColumns[8]);
                tblCampDetails.setTblCampCreatedBy(resultColumns[9]);
                tblCampDetails.setTblCampOutcome(resultColumns[10]);
                tblCampDetails.setTblCampisActive(Integer.parseInt(resultColumns[11]));
                tblCampDetails.setTblCampSummary(resultColumns[12]);
                tblCampDetails.setTblCampEndTime(resultColumns[13]);
                return tblCampDetails;
            }
        });
        return result.getResults();
    }

    public String getLastCampDate() throws Exception {
        String sql = "select tblCampDateTime from tblCampDetails";
        GenericRawResults<tblCampDetails> rawResults = databaseHelper.gettblregRuntimeExceptionDao().queryRaw(sql, new RawRowMapper<tblCampDetails>() {
            @Override
            public tblCampDetails mapRow(String[] columnNames, String[] resultColumns) throws SQLException {
                tblCampDetails tblCampDetails = new tblCampDetails();
                tblCampDetails.setTblCampDateTime(resultColumns[0]);
                return tblCampDetails;
            }
        });
        List<tblCampDetails> results = rawResults.getResults();
        if (results.size() != 0) {
            return results.get(results.size() - 1).getTblCampDateTime();
        } else {
            return "No Data";
        }
    }

    //20May2021 Arpitha
    public List<tblCampDetails> getCampaigns(String year, String month, long limitStart, int villageCode, int search) throws Exception {

        long limit = 10;
        boolean includeVillageCode = villageCode != 0;


        String strWhere = "Where ";


        if (includeVillageCode) {
            strWhere = strWhere + " tblCampFacilities = " + villageCode;
        }

        if (!year.equalsIgnoreCase("All")) {
            if (includeVillageCode)
                strWhere = strWhere + " and Substr(tblCampDateTime,7,4) = '" + year + "'";
            else
                strWhere = strWhere + "  Substr(tblCampDateTime,7,4) = '" + year + "'";

        }

        if (!month.equalsIgnoreCase("00")) {
//            25May2021 Arpitha
            if (!year.equalsIgnoreCase("All"))
                strWhere = strWhere + " and  Substr(tblCampDateTime,4,2) = '" + month + "'";
            else
                strWhere = strWhere + "   Substr(tblCampDateTime,4,2) = '" + month + "'";
        }


        //28/5/2021 Ramesh added search by training name in the query
        if (search > 0) {
            boolean all = includeVillageCode || (!year.equalsIgnoreCase("All")) || (!month.equalsIgnoreCase("00"));
            if (search < 12) {
                if (all){
                    strWhere = strWhere + " and tblCampName = '" + search + "'";
                }else {
                    strWhere = strWhere + " tblCampName = '" + search + "'";
                }
            } else {
                if (all){
                    strWhere = strWhere + " and tblCampName not like '%1%'" +
                            " and tblCampName not like '%2%'" +
                            " and tblCampName not like '%3%'" +
                            " and tblCampName not like '%4%'" +
                            " and tblCampName not like '%5%'" +
                            " and tblCampName not like '%6%'" +
                            " and tblCampName not like '%7%'" +
                            " and tblCampName not like '%8%'" +
                            " and tblCampName not like '%9%'" +
                            " and tblCampName not like '%10%'" +
                            " and tblCampName not like '%11%'";
                }else {
                    strWhere = strWhere + " tblCampName not like '%1%'" +
                            " and tblCampName not like '%2%'" +
                            " and tblCampName not like '%3%'" +
                            " and tblCampName not like '%4%'" +
                            " and tblCampName not like '%5%'" +
                            " and tblCampName not like '%6%'" +
                            " and tblCampName not like '%7%'" +
                            " and tblCampName not like '%8%'" +
                            " and tblCampName not like '%9%'" +
                            " and tblCampName not like '%10%'" +
                            " and tblCampName not like '%11%'";
                }
            }
        }


        String sql = "Select * from tblCampDetails ";

        if (includeVillageCode || !year.equalsIgnoreCase("All") || !month.equalsIgnoreCase("00") || search != 0)
            sql = sql + strWhere;

        sql = sql + " order by transid DESC LIMIT 10 OFFSET  " + limitStart;

        GenericRawResults<tblCampDetails> result = this.databaseHelper.getCampaignRuntimeExceptionDao().queryRaw(sql, new RawRowMapper<tblCampDetails>() {
            @Override
            public tblCampDetails mapRow(String[] columnNames, String[] resultColumns) throws SQLException {
                tblCampDetails tblCampDetails = new tblCampDetails();
                tblCampDetails.setUserId(resultColumns[0]);
                tblCampDetails.setTblCampID(resultColumns[1]);
                tblCampDetails.setTblCampName(resultColumns[2]);
                tblCampDetails.setTblCampDateTime(resultColumns[3]);
                tblCampDetails.setTblCampDesc(resultColumns[4]);
                tblCampDetails.setTblCampPresent(resultColumns[5]);
                tblCampDetails.setTblCampAbsent(resultColumns[6]);
                tblCampDetails.setTblCampSchemaType(Integer.parseInt(resultColumns[7]));
                tblCampDetails.setTblCampFacilities(resultColumns[8]);
                tblCampDetails.setTblCampCreatedBy(resultColumns[9]);
                tblCampDetails.setTblCampOutcome(resultColumns[10]);
                tblCampDetails.setTblCampisActive(Integer.parseInt(resultColumns[11]));
                tblCampDetails.setTblCampSummary(resultColumns[12]);
                tblCampDetails.setTblCampEndTime(resultColumns[13]);
                return tblCampDetails;
            }
        });

        return result.getResults();


    }

    //20May2021 Arpitha
    public long getCampaignsCount(String year, String month, int villageCode,int search) throws Exception {

        long limit = 10;
        boolean includeVillageCode = villageCode != 0;


        String strWhere = "Where ";


        if (includeVillageCode) {
            strWhere = strWhere + " tblCampFacilities = " + villageCode;
        }

        if (!year.equalsIgnoreCase("All")) {
            if (includeVillageCode)
                strWhere = strWhere + " and Substr(tblCampDateTime,7,4) = '" + year + "'";
            else
                strWhere = strWhere + "  Substr(tblCampDateTime,7,4) = '" + year + "'";

        }

        if (!month.equalsIgnoreCase("00")) {
            if (!year.equalsIgnoreCase("All"))
                strWhere = strWhere + " and  Substr(tblCampDateTime,4,2) = '" + month + "'";

            else

                strWhere = strWhere + "   Substr(tblCampDateTime,4,2) = '" + month + "'";
        }

        if (search > 0) {
            boolean all = includeVillageCode || (!year.equalsIgnoreCase("All")) || (!month.equalsIgnoreCase("00"));
            if (search < 12) {
                if (all){
                    strWhere = strWhere + " and tblCampName = '" + search + "'";
                }else {
                    strWhere = strWhere + " tblCampName = '" + search + "'";
                }
            } else {
                if (all){
                    strWhere = strWhere + " and tblCampName not like '%1%'" +
                            " and tblCampName not like '%2%'" +
                            " and tblCampName not like '%3%'" +
                            " and tblCampName not like '%4%'" +
                            " and tblCampName not like '%5%'" +
                            " and tblCampName not like '%6%'" +
                            " and tblCampName not like '%7%'" +
                            " and tblCampName not like '%8%'" +
                            " and tblCampName not like '%9%'" +
                            " and tblCampName not like '%10%'" +
                            " and tblCampName not like '%11%'";
                }else {
                    strWhere = strWhere + " tblCampName not like '%1%'" +
                            " and tblCampName not like '%2%'" +
                            " and tblCampName not like '%3%'" +
                            " and tblCampName not like '%4%'" +
                            " and tblCampName not like '%5%'" +
                            " and tblCampName not like '%6%'" +
                            " and tblCampName not like '%7%'" +
                            " and tblCampName not like '%8%'" +
                            " and tblCampName not like '%9%'" +
                            " and tblCampName not like '%10%'" +
                            " and tblCampName not like '%11%'";
                }
            }
        }

        String sql = "Select * from tblCampDetails ";

        if (includeVillageCode || !year.equalsIgnoreCase("All") || !month.equalsIgnoreCase("00") || search!=0)
            sql = sql + strWhere;

        //  sql = sql+" LIMIT 10 OFFSET  "+limitStart;

        GenericRawResults<tblCampDetails> result = this.databaseHelper.getCampaignRuntimeExceptionDao().queryRaw(sql, new RawRowMapper<tblCampDetails>() {
            @Override
            public tblCampDetails mapRow(String[] columnNames, String[] resultColumns) throws SQLException {
                tblCampDetails tblCampDetails = new tblCampDetails();
                tblCampDetails.setUserId(resultColumns[0]);
                tblCampDetails.setTblCampID(resultColumns[1]);
                tblCampDetails.setTblCampName(resultColumns[2]);
                tblCampDetails.setTblCampDateTime(resultColumns[3]);
                tblCampDetails.setTblCampDesc(resultColumns[4]);
                tblCampDetails.setTblCampPresent(resultColumns[5]);
                tblCampDetails.setTblCampAbsent(resultColumns[6]);
                tblCampDetails.setTblCampSchemaType(Integer.parseInt(resultColumns[7]));
                tblCampDetails.setTblCampFacilities(resultColumns[8]);
                tblCampDetails.setTblCampCreatedBy(resultColumns[9]);
                tblCampDetails.setTblCampOutcome(resultColumns[10]);
                tblCampDetails.setTblCampisActive(Integer.parseInt(resultColumns[11]));
                tblCampDetails.setTblCampSummary(resultColumns[12]);
                tblCampDetails.setTblCampEndTime(resultColumns[13]);
                return tblCampDetails;
            }
        });

        return result.getResults().size();
    }

    public List<String> getYears() throws SQLException {

        //23MAy2021 Bindu - add distinct
        String sql = " select distinct strftime('%Y',substr(tblCampDateTime , 7,4) || '-' || " +
                " substr(tblCampDateTime ,4,2)  || '-' " +
                "|| substr(tblCampDateTime , 1,2)) from tblCampDetails order by strftime('%Y',substr(tblCampDateTime , 7,4) || '-' ||" +
                "substr(tblCampDateTime ,4,2)  || '-'" +
                "|| substr(tblCampDateTime , 1,2)) desc";

        GenericRawResults<String> result = databaseHelper.gettblregRuntimeExceptionDao().queryRaw(sql, new RawRowMapper<String>() {
            @Override
            public String mapRow(String[] strings, String[] strings1) {
                String[] servicesPojo;
                String s = strings[0];
                // servicesPojo.setRegRegistrationDate(strings1[0]);
                return strings1[0];
            }
        });

        return result.getResults();
    }
}
