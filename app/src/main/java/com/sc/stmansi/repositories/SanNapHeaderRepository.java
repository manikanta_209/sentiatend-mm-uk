package com.sc.stmansi.repositories;

import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.dao.RawRowMapper;
import com.j256.ormlite.stmt.QueryBuilder;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.tables.tblSanNapHeader;

import java.sql.SQLException;
import java.util.List;

public class SanNapHeaderRepository {
    private DatabaseHelper databaseHelper;

    public SanNapHeaderRepository(DatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }

    public List<tblSanNapHeader> getAllHeadersofAdol(String adolID) throws Exception {
        QueryBuilder<tblSanNapHeader, Integer> queryBuilder = this.databaseHelper.getTblSanNapHeaderDao().queryBuilder().groupBy("transId").orderBy("transId", true);
        return queryBuilder.where().eq("AdolId", adolID).query();
    }

    public tblSanNapHeader getHeaderbasedonAdolIDandTransID(String adolId, int transId) throws Exception {
        return this.databaseHelper.getTblSanNapHeaderDao().queryBuilder().where().eq("AdolId", adolId).and().eq("transId", transId).queryForFirst();
    }

    public List<String> getYears() throws SQLException {
        String sql = " select distinct strftime('%Y',substr(tblSNDateofFeedback , 7,4) || '-' || " +
                " substr(tblSNDateofFeedback ,4,2)  || '-' " +
                "|| substr(tblSNDateofFeedback , 1,2)) from tblSanNapHeader order by strftime('%Y',substr(tblSNDateofFeedback , 7,4) || '-' ||" +
                "substr(tblSNDateofFeedback ,4,2)  || '-'" +
                "|| substr(tblSNDateofFeedback , 1,2)) desc";

        GenericRawResults<String> result = databaseHelper.getTblSanNapHeaderDao().queryRaw(sql, new RawRowMapper<String>() {
            @Override
            public String mapRow(String[] strings, String[] strings1) {
                return strings1[0];
            }
        });

        return result.getResults();
    }

    public List<tblSanNapHeader> getAllHeadersofAdolwithFilter(String adolId, String month, String year) throws Exception {

        String strWhere = "where AdolId = '" + adolId + "' ";

        if (!year.equalsIgnoreCase("All")) {
            strWhere = strWhere + " and Substr(tblSNDateofFeedback,7,4) = '" + year + "'";
        }

        if (!month.equalsIgnoreCase("00")) {
            if (!year.equalsIgnoreCase("All"))
                strWhere = strWhere + " and  Substr(tblSNDateofFeedback,4,2) = '" + month + "'";
            else
                strWhere = strWhere + "   Substr(tblSNDateofFeedback,4,2) = '" + month + "'";
        }
        String sql ="Select * from tblSanNapHeader ";
        sql = sql + strWhere;

        GenericRawResults<tblSanNapHeader> result = databaseHelper.getTblSanNapHeaderDao().queryRaw(sql, new RawRowMapper<tblSanNapHeader>() {
            @Override
            public tblSanNapHeader mapRow(String[] strings, String[] resultColumns) {
                tblSanNapHeader s = new tblSanNapHeader();
                s.setUserId(resultColumns[1]);
                s.setAdolId(resultColumns[2]);
                s.setTblSNBlockName(resultColumns[3]);
                s.setTblSNVillage(resultColumns[4]);
                s.setTblSNAdolName(resultColumns[5]);
                s.setTblSNAdolAge(resultColumns[6]);
                s.setTblSNAdolGTS(resultColumns[7]);
                s.setTblSNDateofSNProvided(resultColumns[8]);
                s.setTblSNDateofFeedback(resultColumns[9]);
                s.setTblSNFieldStaff(resultColumns[10]);
                s.setTransId(Integer.parseInt(resultColumns[11]));
                s.setRecordCreatedDate(resultColumns[12]);
                s.setRecordUpdatedDate(resultColumns[13]);
                return s;
            }
        });
        return result.getResults();
    }
}
