package com.sc.stmansi.repositories;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.sc.stmansi.configuration.MyApplication;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.tables.TblFacilityDetails;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class FacilityRepository {

	private DatabaseHelper databaseHelper;

	public FacilityRepository(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	public void updateUserId(String userId) throws SQLException {
		UpdateBuilder<TblFacilityDetails, Integer> updateBuilder = this.databaseHelper.getVillageRuntimeExceptionDao().updateBuilder();
		updateBuilder.updateColumnValue("UserId", userId);
		updateBuilder.update();
	}

	public Map<String, Integer> getPlaceMap() throws SQLException {
		Map<String, Integer> villages = new LinkedHashMap<>();
		RuntimeExceptionDao<TblFacilityDetails, Integer> villagesDAO = databaseHelper.getVillageRuntimeExceptionDao();

		//04Apr2021 Bindu get local names based on language
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MyApplication.getAppContext());
		String strcolumname = "FacilityName";

		if (prefs.getBoolean("isTelugu", false || prefs.getBoolean("isisKannada", false))) {
			 strcolumname = "LocalName";
		}else  {
			 strcolumname = "FacilityName";
		}

		List<TblFacilityDetails> listVillages = villagesDAO.queryBuilder().distinct()
				.selectColumns(strcolumname).selectColumns("AutoId")
				//.orderBy("AutoId", false)
				.orderBy(strcolumname, true)
				.query();
		for (TblFacilityDetails tbl : listVillages) {

			villages.put((strcolumname.equalsIgnoreCase("FacilityName")? tbl.getFacilityName() : tbl.getLocalName()), tbl.getAutoId());
		}
		return villages;
	}

	//25Jul2019 - Bindu - Get Facility namelist
	public Map<String, String> getFacilityNames(String facilitytype, String userId) throws SQLException {
		Map<String, String> villages = new HashMap<>();
		RuntimeExceptionDao<TblFacilityDetails, Integer> villagesDAO = databaseHelper.getVillageRuntimeExceptionDao();
		List<TblFacilityDetails> listVillages = null;

		if (facilitytype.toLowerCase().contains("phc") || facilitytype.toLowerCase().contains("chc") || facilitytype.toLowerCase().contains("gh")) { //15Apr2021 Bindu Check for CHC
			listVillages = villagesDAO.queryBuilder().distinct().selectColumns("BEmOCName")
					.where().eq("UserId", userId).and().eq("BEmOCType", facilitytype).query();

			String query = villagesDAO.queryBuilder().distinct().selectColumns("BEmOCName")
					.where().eq("UserId", userId).and().eq("BEmOCType", facilitytype).prepare().toString();

			if (listVillages != null) {
				for (TblFacilityDetails tbl : listVillages) {
					villages.put(tbl.getBEmOCName(), tbl.getBEmOCName());
				}
			}
		} else if (facilitytype.toLowerCase().contains("fru") || facilitytype.toLowerCase().contains("th") || facilitytype.toLowerCase().contains("dh") || facilitytype.toLowerCase().contains("private")) {
			//03Oct2019 - fac type compare
			String strfac = "";
			if(facilitytype.toLowerCase().contains("th"))
				strfac = "TH";
			else if(facilitytype.toLowerCase().contains("dh"))
				strfac = "DH";
			else if(facilitytype.toLowerCase().contains("private"))
				strfac = "Pvt Hospital";
			//15Apr2021 Bindu check for CHC
			else if(facilitytype.toLowerCase().contains("chc"))
				strfac = "chc";


			listVillages = villagesDAO.queryBuilder().distinct().selectColumns("CEmOCName")
					.where().eq("UserId", userId).and().eq("CEmOCType",strfac).query();

			String query = villagesDAO.queryBuilder().distinct().selectColumns("CEmOCName")
					.where().eq("UserId", userId).and().eq("CEmOCType",strfac).prepare().toString();


			if (listVillages != null) {
				for (TblFacilityDetails tbl : listVillages) {
					villages.put(tbl.getCEmOCName(), tbl.getCEmOCName());
				}
			}
		}
		return villages;
	}

	//17Sep2019 - Bindu - Get Referral Facility namelist
	public Map<String, String> getReferralFacilityNames(String facilitytype, String userId) throws SQLException {


		Map<String, String> villages = new HashMap<>();
		RuntimeExceptionDao<TblFacilityDetails, Integer> villagesDAO = databaseHelper.getVillageRuntimeExceptionDao();
		List<TblFacilityDetails> listVillages = null;

		if(facilitytype.toLowerCase().contains("cemoc")) {
			listVillages = villagesDAO.queryBuilder().distinct().selectColumns("CEmOCName")
					.where().eq("UserId", userId).query();

			String query = villagesDAO.queryBuilder().distinct().selectColumns("CEmOCName")
					.where().eq("UserId", userId).prepare().toString();


			if(listVillages != null) {
				for (TblFacilityDetails tbl : listVillages) {
					villages.put("CEmOC", tbl.getCEmOCName());
				}
			}
		}
		else  if(facilitytype.toLowerCase().contains("phc")){
			listVillages = villagesDAO.queryBuilder().distinct().selectColumns("BEmOCName")
					.where().eq("UserId", userId).query();

			String query = villagesDAO.queryBuilder().distinct().selectColumns("BEmOCName")
					.where().eq("UserId", userId).prepare().toString();

			if(listVillages != null) {
				for (TblFacilityDetails tbl : listVillages) {
					villages.put("BEmOC", tbl.getBEmOCName());
				}
			}
		}else {
			listVillages = villagesDAO.queryBuilder().distinct().selectColumns("CEmOCName")
					.where().eq("UserId", userId).query();

			if(listVillages != null) {
				for (TblFacilityDetails tbl : listVillages) {
					villages.put("CEmOC", tbl.getCEmOCName());
				}
			}

			listVillages = villagesDAO.queryBuilder().distinct().selectColumns("BEmOCName")
					.where().eq("UserId", userId).query();

			if(listVillages != null) {
				for (TblFacilityDetails tbl : listVillages) {
					villages.put("BEmOC", tbl.getBEmOCName());
				}
			}
		}
		return villages;
	}

	//11Dec2019 - Bindu - get Hamlet names
	public String getHamletName(int villagecode) throws SQLException {
		String villages ;
		RuntimeExceptionDao<TblFacilityDetails, Integer> villagesDAO = databaseHelper.getVillageRuntimeExceptionDao();

		String strqry = "select FacilityName from tblfacilitydetails where Autoid ='" + villagecode + "'"; //16May2021 Bindu change col name - eng fac

		GenericRawResults<String[]> rawResults = villagesDAO.queryRaw(strqry);

		List<String[]> results = rawResults.getResults();
		String[] resultArray = results.get(0);
		if (resultArray[0] != null)
			villages = resultArray[0];
		else
			villages = "";

		return villages;
	}
}
