package com.sc.stmansi.repositories;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.dao.RawRowMapper;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.tables.TblChildInfo;
import com.sc.stmansi.tables.TblChlParentDetails;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.womanlist.QueryParams;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ChildRepository {


    private DatabaseHelper databaseHelper;

    public ChildRepository(DatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }


    public List<TblChildInfo> getChildData()
            throws SQLException {
        RuntimeExceptionDao<TblChildInfo, Integer> childDetailsDAO = databaseHelper.getChildInfoRuntimeExceptionDao();
        return childDetailsDAO.queryBuilder().orderBy("transId", false).query();

    }

    public List<TblChildInfo> getChildData(String womanId)
            throws SQLException {
        RuntimeExceptionDao<TblChildInfo, Integer> childDetailsDAO = databaseHelper.getChildInfoRuntimeExceptionDao();
        return childDetailsDAO.queryBuilder().where()
                .eq("WomanId", womanId).and().eq("chlReg",0).query(); //10Dec2019 - Bindu - add reg 0

    }

    public TblChildInfo getChildbyChilID(String chlid) //Ramesh 8-7-2021
            throws SQLException {
        RuntimeExceptionDao<TblChildInfo, Integer> childDetailsDAO = databaseHelper.getChildInfoRuntimeExceptionDao();
        return childDetailsDAO.queryBuilder().where()
                .eq("chlID", chlid).query().get(0); //10Dec2019 - Bindu - add reg 0

    }

    // Get LastChildNumber, Add 1 and Generate the New ChildId
    public String getNewChildId(String userId, int i, String parentId) throws SQLException {
        String returnValue;

        String strParentId = "";

        /*

        Dao<TblChlParentDetails, Integer> parentDAO = databaseHelper.getChlParentRuntimeExceptionDao();

        List<TblChlParentDetails> parentlist = parentDAO.queryBuilder().selectColumns("chlParentId").query();

        if(parentlist!=null && parentlist.size()>0)
            strParentId = parentlist.get(parentlist.size()-1).getChlParentId();
*/

        strParentId = parentId;
        RuntimeExceptionDao<TblChildInfo, Integer> childDAO = databaseHelper.getChildInfoRuntimeExceptionDao();

        List<TblChildInfo> list = childDAO.queryBuilder().selectColumns("chlID").query();
        int lastChildNo = 0;
        if (list != null && list.size() > 0)
            lastChildNo = list.size();

        lastChildNo = lastChildNo + 1 + (i - 1);

        if (lastChildNo <= 9) {
            returnValue = strParentId + "000" + lastChildNo;
        } else if (lastChildNo <= 99) {
            returnValue = strParentId + "00" + lastChildNo;
        } else if (lastChildNo <= 999) {
            returnValue = strParentId + "0" + lastChildNo;
        } else {
            returnValue = strParentId + lastChildNo;
        }
        return returnValue;
    }


    public int updateChildData(String userId, String upSqlDel, int transId,
                               DatabaseHelper databaseHelper) {

        int update = 0;
        update = databaseHelper.getChildInfoRuntimeExceptionDao().updateRaw(upSqlDel);


        TransactionHeaderRepository transactionHeaderRepository = new TransactionHeaderRepository(databaseHelper);
        transactionHeaderRepository.updateNew(userId, transId,
                databaseHelper.getChildInfoRuntimeExceptionDao().getTableName(),
                upSqlDel, null,null, databaseHelper);

        return update;
    }


    // Get LastChildNumber, Add 1 and Generate the New ChildId
    public String getNewChildParentId(String userId) throws SQLException {
        String returnValue;

        Dao<TblChlParentDetails, Integer> childDAO =
                databaseHelper.getChlParentRuntimeExceptionDao();


        RuntimeExceptionDao<TblInstusers, Integer> usersDAO = databaseHelper.getUsersRuntimeExceptionDao();

        List<TblInstusers> list = usersDAO.queryBuilder().selectColumns("LastWomenNumber")
                .where().eq("userId", userId).query();
        int lastParentno = 0;
        if (list != null && list.size() > 0)
            lastParentno = list.get(0).getLastParentNumber();



      /*  List<TblChlParentDetails> list = childDAO.queryBuilder().selectColumns("chlParentId").query();
        int lastChildNo = 0;
        if (list != null && list.size() > 0)
            lastChildNo = list.size();*/

        lastParentno = lastParentno + 1;

        if (lastParentno <= 9) {
            returnValue = userId + "P" + "000" + lastParentno;
        } else if (lastParentno <= 99) {
            returnValue = userId + "P" + "00" + lastParentno;
        } else if (lastParentno <= 999) {
            returnValue = userId + "P" + "0" + lastParentno;
        } else {
            returnValue = userId + "P" + lastParentno;
        }
        return returnValue;
    }

    public List<TblChlParentDetails> getParentDetails(String chlParentId) throws SQLException {

        Dao<TblChlParentDetails, Integer> childDetailsDAO
                = databaseHelper.getChlParentRuntimeExceptionDao();
        return childDetailsDAO.queryBuilder().where()
                .eq("chlParentId", chlParentId).query();


    }

    public List<TblChlParentDetails> getParentDetail() throws SQLException {

        Dao<TblChlParentDetails, Integer> childDetailsDAO = databaseHelper.getChlParentRuntimeExceptionDao();
        return childDetailsDAO.queryBuilder().query();
    }


    //  get womanid list
    public List<TblChildInfo> getChildList(QueryParams queryParams) throws Exception {
        boolean includeVillageCode = queryParams.selectedVillageCode != 0;
        boolean includeUserFilter = queryParams.textViewFilter != null && !queryParams.textViewFilter.trim().isEmpty();
        boolean includeDateFilter = queryParams.selectedDateFilter != 0;
        boolean includeWomanFilter = queryParams.womanId != null && !queryParams.womanId.trim().isEmpty();
        ;
        boolean includeDeactivateFilter = queryParams.isDeactivated == true;


        long limit = 10;

        QueryBuilder<TblChildInfo, Integer> queryBuilder = databaseHelper.getChildInfoRuntimeExceptionDao().queryBuilder()
                .orderBy("transId", false);

        Where<TblChildInfo, Integer> where = queryBuilder.where();
        where.eq("UserId", queryParams.userId);

        if (includeVillageCode) {
            where = where.and().eq("chlTribalHamlet", queryParams.selectedVillageCode);
        }
        if (includeUserFilter) {
            where = where.and(where, where.or(where.like("chlChildname", "%" + queryParams.textViewFilter + "%"),
                    where.like("chlRCHID", "%" + queryParams.textViewFilter + "%")));
        }

        if (includeDateFilter && queryParams.selectedDateFilter > 0) {

            if (queryParams.selectedDateFilter == 1)
                where = where.and().eq("chlDateOfBirth", DateTimeUtil.getTodaysDate());
            else if (queryParams.selectedDateFilter >= 2 &&
                    queryParams.fromDate != null && queryParams.fromDate.trim().length() > 0
                    && queryParams.toDate != null && queryParams.toDate.trim().length() > 0) {


                if (queryParams.selectedDateFilter == 2) {
                    String strWhere = "";

                    if (includeDeactivateFilter) {
                        strWhere = "  Date(substr(chlDeactDate, " +
                                " 7) || '-' || substr(chlDeactDate ,4,2)  || '-'  " +
                                " ||"
                                + " substr(chlDeactDate, 1,2))  between " +
                                "Date(substr('" + queryParams.fromDate + "' , " + "7) || '-' || substr('" + queryParams.fromDate + "' ,4,2)  || '-'  " +
                                " || substr('" + queryParams.fromDate + "' , 1,2)) and  Date(substr('" + queryParams.toDate + "' , " +
                                "7) || '-' || substr('" + queryParams.toDate + "' ,4,2)  || '-'  " +
                                " || substr('" + queryParams.toDate + "' , 1,2))";
                    } else
                        strWhere = "  Date(substr(chlDateOfBirth, " +
                                " 7) || '-' || substr(chlDateOfBirth ,4,2)  || '-'  " +
                                " ||"
                                + " substr(chlDateOfBirth, 1,2))  between " +
                                "Date(substr('" + queryParams.fromDate + "' , " + "7) || '-' || substr('" + queryParams.fromDate + "' ,4,2)  || '-'  " +
                                " || substr('" + queryParams.fromDate + "' , 1,2)) and  Date(substr('" + queryParams.toDate + "' , " +
                                "7) || '-' || substr('" + queryParams.toDate + "' ,4,2)  || '-'  " +
                                " || substr('" + queryParams.toDate + "' , 1,2))";
                    where = where.and().raw(strWhere);

                }
            }else
            {
                // if (includeDateFilter && queryParams.selectedDateFilter > 1) {
                String strWhere1 = "";

                if(queryParams.selectedDateFilter == 3)
                {

                    strWhere1 = "julianday('now') - julianday(substr(chlDateOfBirth , 7) || '-' || \n" +
                            "                         substr(chlDateOfBirth ,4,2)  || '-' \n" +
                            "                        || substr(chlDateOfBirth , 1,2))  < 294 ";
                }else  if (queryParams.selectedDateFilter == 4)
                {

                    strWhere1 = "( julianday('now') - julianday(substr(chlDateOfBirth , 7) || '-' || \n" +
                            "                         substr(chlDateOfBirth ,4,2)  || '-' \n" +
                            "                        || substr(chlDateOfBirth , 1,2))  <730 and   julianday('now') - julianday(substr(chlDateOfBirth , 7) || '-' || \n" +
                            "                         substr(chlDateOfBirth ,4,2)  || '-' \n" +
                            "                        || substr(chlDateOfBirth , 1,2)) > 180 )";
                }else if(queryParams.selectedDateFilter == 5)
                {
                    strWhere1  = " julianday('now') - julianday(substr(chlDateOfBirth , 7) || '-' || " +
                            "                         substr(chlDateOfBirth ,4,2)  || '-' " +
                            "                        || substr(chlDateOfBirth , 1,2))  >=730 ";

                }

                where = where.and().raw(strWhere1);


            //}
            }

        } else if (includeWomanFilter) {
            where = where.and().eq("WomanId", queryParams.womanId);
        }

        if (includeDeactivateFilter) {
            where = where.and().isNotNull("chlDeactDate");

        } else if(!includeWomanFilter)
            where = where.and().isNull("chlDeactDate");

        queryBuilder.setWhere(where);
        String s = queryBuilder.prepare().toString();
        return queryBuilder.query();
    }


    //20Nov2019 - Bindu - Get child mortality data
    public List<TblChildInfo> getChildMortalityData(String womanId)
            throws SQLException {
        String[] selectColumns = {"chlNo", "chlDeliveryResult"};

        QueryBuilder<TblChildInfo, Integer> queryBuilder = databaseHelper.getChildInfoRuntimeExceptionDao().queryBuilder()
                .selectColumns(selectColumns);

        Where<TblChildInfo, Integer> where = queryBuilder.where()
                .eq("WomanId", womanId)
                .and().ne("chlDeliveryResult", 1).and().eq("chlReg", 0);
        queryBuilder.setWhere(where);
        return queryBuilder.query();

    }


    public List<TblChildInfo> getYears() throws SQLException {

        List<TblChildInfo> arrVal = new ArrayList<>();
        String sql = "Select substr(chlDateOfBirth, 7) from tblchildinfo";
        GenericRawResults<TblChildInfo> tbls = databaseHelper.
                getChildInfoRuntimeExceptionDao().
                queryRaw(sql,
                        new RawRowMapper<TblChildInfo>() {
                            @Override
                            public TblChildInfo mapRow(String[] strings, String[] strings1) {
                                TblChildInfo servicesPojo = new TblChildInfo();
                                String s = strings[0];
                                servicesPojo.setChlDateTimeOfBirth(strings1[0]);
                                return servicesPojo;
                            }
                        });

        for (TblChildInfo foo : tbls) {
            arrVal.add(foo);

        }
        return arrVal;
    }

    //    25Nov2019 Arpitha
    public List<TblChildInfo> getChildDeactivationData(String strChildId, String userId)
            throws SQLException {
        RuntimeExceptionDao<TblChildInfo, Integer> childDetailsDAO = databaseHelper.getChildInfoRuntimeExceptionDao();
        return childDetailsDAO.queryBuilder()
                .orderBy("transId", false)
                .where().eq("chlID",strChildId)
                .and().eq("UserId",userId)
                .and().isNotNull("chlDeactDate")
                .query();

    }


    public List<TblChildInfo> getChildForMother(String womanId, String userId) throws SQLException {

      return   databaseHelper.getChildInfoRuntimeExceptionDao().queryBuilder()
              .where().eq("WomanId",womanId).and()
                .eq("UserId", userId).and().isNull("chlDeactDate")
                .query();
    }

    // 29Nov2019 - Bindu - get child id alive - if multiple then return 0 else child no -
    // to set default baby val when symtom is checked
    public int getChildAliveID(String womanId) throws SQLException{
        int returnChlNo;
        RuntimeExceptionDao<TblChildInfo, Integer> childDAO = databaseHelper.getChildInfoRuntimeExceptionDao();
        List<TblChildInfo> list = childDAO.queryBuilder().selectColumns("chlNo")
                .where().eq("WomanId", womanId).and().eq("chlDeliveryResult",1).and().eq("chlReg",0).query(); //10Dec2019 - Bindu
        if (list != null && list.size() > 1)
            returnChlNo = 0;
        else if(list != null && list.size()== 0) //11Dec2019 - Bindu
            returnChlNo = 0;
        else
            returnChlNo = list.get(0).getChildNo();
        return returnChlNo;
    }

    //01Dec019 - Bindu - Get child mortality data
    public List<TblChildInfo> getChildDeactivatedData(String womanId)
            throws SQLException {
        String[] selectColumns = {"chlNo", "chlDeactDate"};

        QueryBuilder<TblChildInfo, Integer> queryBuilder = databaseHelper.getChildInfoRuntimeExceptionDao().queryBuilder()
                .selectColumns(selectColumns);

        Where<TblChildInfo, Integer> where = queryBuilder.where()
                .eq("WomanId", womanId)
                .and().isNotNull("chlDeactDate").and().eq("chlReg", 0);
        queryBuilder.setWhere(where);
        return queryBuilder.query();

    }
    // 01Dec2019 - Bindu - get child id active - if multiple then return 0 else child no
    // to set default baby val when symtom is checked
    public int getChildActiveID(String womanId) throws SQLException{
        int returnChlNo;
        RuntimeExceptionDao<TblChildInfo, Integer> childDAO = databaseHelper.getChildInfoRuntimeExceptionDao();
        List<TblChildInfo> list = childDAO.queryBuilder().selectColumns("chlNo")
                .where().eq("WomanId", womanId).and().eq("chlDeliveryResult",1).and().isNull("chlDeactDate").and().eq("chlReg", 0).query();
        if (list != null && list.size() > 1)
            returnChlNo = 0;
        else if(list != null && list.size()== 0)
            returnChlNo = 0;
        else
            returnChlNo = list.get(0).getChildNo();
        return returnChlNo;
    }

    public long getNoOfChildrenOfParent(String strParentId) throws SQLException {

        return  databaseHelper.getChildInfoRuntimeExceptionDao().queryBuilder()
                .where().eq("chlParentId", strParentId).countOf();
    }

    public long getNoOfChildrenOfWoman(String womanId) throws SQLException {
        return  databaseHelper.getChildInfoRuntimeExceptionDao().queryBuilder()
                .where().eq("WomanId", womanId).countOf();
    }


    public boolean checkIsChildAlive(String womanId) throws SQLException {

        boolean childAlive = false;
        List<TblChildInfo> childInfoList =    databaseHelper.getChildInfoRuntimeExceptionDao().queryBuilder()
                .selectColumns("chlDeliveryResult")
                .where()
                .eq("WomanId",womanId).query();

        for(int i = 0; i<childInfoList.size();i++)
        {
            if(childInfoList.get(i).getChlDeliveryResult()==1)
                childAlive = true;
        }
        return  childAlive;

    }

    public boolean checkSelectedChildAlive(String chlId) throws SQLException {

        boolean childAlive = false;
        List<TblChildInfo> childInfoList =    databaseHelper.getChildInfoRuntimeExceptionDao().queryBuilder()
                .selectColumns("chlDeliveryResult")
                .where()
                .eq("chlID",chlId).query();

        for(int i = 0; i<childInfoList.size();i++)
        {
            if(childInfoList.get(i).getChlDeliveryResult()==1)
                childAlive = true;
        }
        return  childAlive;

    }

    public boolean checkIsChildActive(String womanId) throws SQLException {

        boolean childActive = false;
        List<TblChildInfo> childInfoList =    databaseHelper.getChildInfoRuntimeExceptionDao().queryBuilder()
                .selectColumns("chlDeactDate")
                .where()
                .eq("WomanId",womanId).query();

        for(int i = 0; i<childInfoList.size();i++)
        {
            if(childInfoList.get(i).getChlDeactDate()==null)
                childActive = true;
        }
        return  childActive;

    }

    //mani 23Aug2021
    public List<TblChildInfo> getChildToAdolList(String UserId) throws Exception {
        RuntimeExceptionDao<TblChildInfo,Integer> tblchildgrowtMonitorDao=databaseHelper.getChildInfoRuntimeExceptionDao();
        return tblchildgrowtMonitorDao.queryBuilder()
                .where().eq("UserId", UserId).query();
    }

    public int updateChildDeact(String sql) {

        return this.databaseHelper.getChildInfoRuntimeExceptionDao().updateRaw(sql);
    }

    public boolean isAdolIDExists(String adolID) throws Exception{
        List<TblChildInfo> list = this.databaseHelper.getChildInfoRuntimeExceptionDao().queryBuilder().where().eq("childtoadolID", adolID).query();
        if (list!=null){
            if (list.size()!=0){
                //yes
                return true;
            }else {
                list.size();
                //no
                return false;
            }
        }else {
            //no
            return false;
        }
    }

    public List<TblChildInfo> getallActiveChilds(String filter,int villagecode) throws Exception{
        boolean includefilter = filter!=null && filter.length()>0;
        boolean includeVillage = villagecode!=0;


        QueryBuilder<TblChildInfo, Integer> qBuilder = this.databaseHelper.getChildInfoRuntimeExceptionDao().queryBuilder().orderBy("transId", false);
        Where<TblChildInfo, Integer> wBuilder = qBuilder.where().isNull("chlDeactDate");
        if (includefilter){
            wBuilder.and().like("chlChildname","%"+filter+"%");
        }
        if (includeVillage){
            wBuilder.and().eq("chlTribalHamlet",villagecode);
        }
        String s = qBuilder.prepareStatementString();
        return qBuilder.query();
    }

    public TblChildInfo getChildBasedonAdol(String adolID) throws Exception {
        return this.databaseHelper.getChildInfoRuntimeExceptionDao().queryBuilder().orderBy("transId",false).where().eq("childtoadolID",adolID).queryForFirst();
    }
}
