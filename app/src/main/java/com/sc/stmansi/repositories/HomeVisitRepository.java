package com.sc.stmansi.repositories;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.preference.PreferenceManager;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.sc.stmansi.HomeVisit.HomeVisitViewActivity;
import com.sc.stmansi.R;
import com.sc.stmansi.configuration.MyApplication;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.recyclerview.HomeVisitListAdapter;
import com.sc.stmansi.tables.TblChildImmunization;
import com.sc.stmansi.tables.TblDiagnosisMaster;
import com.sc.stmansi.tables.TblFacilityDetails;
import com.sc.stmansi.tables.TblSignsMaster;
import com.sc.stmansi.tables.TblSymptomMaster;
import com.sc.stmansi.tables.TblVisitChildHeader;
import com.sc.stmansi.tables.TblVisitDetails;
import com.sc.stmansi.tables.TblVisitDetailsPojo;
import com.sc.stmansi.tables.TblVisitHeader;
import com.sc.stmansi.tables.Tblinterpretationmaster;
import com.sc.stmansi.tables.tblChildVisitDetails;
import com.sc.stmansi.tables.tblCovidVaccineDetails;
import com.sc.stmansi.tables.tblregisteredwomen;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class HomeVisitRepository {

    private DatabaseHelper databaseHelper;

    public HomeVisitRepository(DatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }

    public int getLastHomeVisitNoWoman(String userId, String womanId) throws Exception {
        String returnValue = null;
        Dao<TblVisitHeader, Integer> tblVisitHeaderDao = databaseHelper.getTblVisitHeaderDao();

        int lastVisitNo = 0;
        String result = "";
        String strqry = "select MAX(visitNum) from tblVisitHeader where WomanId = '" + womanId + "' and userId = '" + userId + "'";

        GenericRawResults<String[]> rawResults = tblVisitHeaderDao.queryRaw(strqry);

        List<String[]> results = rawResults.getResults();
        // This will select the first result (the first and maybe only row returned)
        String[] resultArray = results.get(0);
        //This will select the first field in the result which should be the ID
        if (resultArray[0] != null)
            lastVisitNo = Integer.parseInt(resultArray[0]) + 1;
        else
            lastVisitNo = lastVisitNo + 1;

        return lastVisitNo;
    }

    public int updateVisitDetailsWomanInReg(String womanId, String userId, tblregisteredwomen reg, int transId, DatabaseHelper db) throws  Exception{

        int update = 0;
        RuntimeExceptionDao<tblregisteredwomen, Integer> regDAO = db.gettblregRuntimeExceptionDao();
        UpdateBuilder<tblregisteredwomen, Integer> updateBuilder = regDAO.updateBuilder();

        updateBuilder.updateColumnValue("LastAncVisitDate", reg.getLastAncVisitDate())
                .updateColumnValue("LastPncVisitDate", reg.getLastPncVisitDate())
                .updateColumnValue("isCompl", reg.getIsCompl())
                .updateColumnValue("isReferred", reg.getIsReferred())
                .updateColumnValue("regrecommendedPlaceOfDelivery", reg.getRegrecommendedPlaceOfDelivery())
                .updateColumnValue("regUserType", reg.getRegUserType())
                .updateColumnValue("RecordUpdatedDate",reg.getRecordUpdatedDate())
                .where().eq("WomanId", womanId).and()
                .eq("userId", userId);

        String strupdate = updateBuilder.prepare().toString();
        update = updateBuilder.update();

        if(update > 0) {
            TransactionHeaderRepository transactionRepo = new TransactionHeaderRepository(databaseHelper);
            transactionRepo.updateNew(userId, transId, "tblregisteredwomen"
                    , updateBuilder.prepare().toString().replace("MappedStatement:", "").trim(),
                    null, null,databaseHelper);
        }

        return update;
    }

    //    TblVisitHeader table DAO object creation
    public Dao<TblVisitDetails, Integer> getTblVisitDetailDao(){
        Dao<TblVisitDetails, Integer> tblVisitDetailDao = null;
        try {
            tblVisitDetailDao = databaseHelper.getTblVisitDetailDao();
            if (tblVisitDetailDao == null) {
                tblVisitDetailDao = databaseHelper.getDao(TblVisitDetails.class);
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return tblVisitDetailDao;

    }

    public List<TblVisitHeader> getVisitDetailsForwoman(String womanId, String sessionUserId) throws SQLException {
        return databaseHelper.getTblVisitHeaderDao().queryBuilder()
                .where().eq("userId", sessionUserId).and().eq("WomanId",womanId).query();
    }

    public List<tblregisteredwomen> getVisitDetailsFrmReg(String womanId, String sessionUserId) throws SQLException {
        String[] selectColumns = { "IsCompl", "LastAncVisitDate","LastPncVisitDate","IsReferred"};
        return databaseHelper.gettblregRuntimeExceptionDao().queryBuilder()
                .selectColumns(selectColumns)
                .where().eq("userId", sessionUserId).and().eq("WomanId",womanId).query();
    }

    //01Dec2019- Bindu - Home visit PNC update reg details
    public int updateVisitDetailsWomanInRegPNC(String womanId, String userId, tblregisteredwomen reg, int transId, DatabaseHelper db) throws  Exception{

        int update = 0;
        RuntimeExceptionDao<tblregisteredwomen, Integer> regDAO = db.gettblregRuntimeExceptionDao();
        UpdateBuilder<tblregisteredwomen, Integer> updateBuilder = regDAO.updateBuilder();

        updateBuilder
                .updateColumnValue("LastPncVisitDate", reg.getLastPncVisitDate())
                .updateColumnValue("isCompl", reg.getIsCompl())
                .updateColumnValue("isReferred", reg.getIsReferred())
                .updateColumnValue("regUserType", reg.getRegUserType())
                .updateColumnValue("RecordUpdatedDate",reg.getRecordUpdatedDate())
                .where().eq("WomanId", womanId).and()
                .eq("userId", userId);

        String strupdate = updateBuilder.prepare().toString();
        update = updateBuilder.update();

        if(update > 0) {
            TransactionHeaderRepository transactionRepo = new TransactionHeaderRepository(databaseHelper);
            transactionRepo.updateNew(userId, transId, "tblregisteredwomen"
                    , updateBuilder.prepare().toString().replace("MappedStatement:", "").trim(),
                    null, null,databaseHelper);
        }

        return update;
    }

    //01Dec2019 - Bindu - Get Child visit detailsfrom header
    public List<TblVisitChildHeader> getVisitDetailsForChild(String selectedWomanId, String sessionUserId) throws SQLException{
        return databaseHelper.getTblVisitChildHeaderDao().queryBuilder()
                .where().eq("userId", sessionUserId).and().eq("WomanId",selectedWomanId).query();
    }

//    10May2021 Arpitha
    public  ArrayList<String>  getHomeVisitSymptoms(String userId, String womanId, int visitNum)
            throws SQLException {
        ArrayList<String> symptoms = new ArrayList<>();
      List<TblVisitDetails> visitDetailsList =  databaseHelper.getTblVisitDetailDao().queryBuilder()
              .selectColumns("visDSymptoms","visDSignsInvestigations","visDSignsInvestInterpretation","visDDiagnosis").where()
               .eq("UserId",userId).and().eq("WomanId",womanId).
               and().eq("VisitNum",visitNum).query();
      if(visitDetailsList!=null && visitDetailsList.size()>0)
      {
          List<TblDiagnosisMaster> diagnosisMastersList = new ArrayList<>();
          List<Tblinterpretationmaster> interpretationMastersList = new ArrayList<>();
          List<TblSignsMaster> signsMastersList = new ArrayList<>();
          for (int i=0;i<visitDetailsList.size();i++) {
              diagnosisMastersList = new ArrayList<>();
              interpretationMastersList = new ArrayList<>();
              signsMastersList = new ArrayList<>();
            List<TblSymptomMaster>  symptomMasterList =  databaseHelper.getTblSymptomMasterDao().queryBuilder()
                      .selectColumns("symXmlStringName").where()
                      .eq("symSymptomId", visitDetailsList.get(i)
                              .getVisDSymptoms()).query();
            if(visitDetailsList.get(i).getVisDDiagnosis()!=null &&
                    visitDetailsList.get(i).getVisDDiagnosis().trim().length()>0)
             diagnosisMastersList = databaseHelper.getTblDiagnosisMasterDao().queryBuilder().
                    selectColumns("diagnosisXmlStringName").where().eq("diagnosisId",
                    visitDetailsList.get(i).getVisDDiagnosis()).query();

              if(visitDetailsList.get(i).getVisDSignsInvestInterpretation()!=null && visitDetailsList.get(i).getVisDSignsInvestInterpretation().trim().length()>0)
              interpretationMastersList = databaseHelper.getTblInterpretationMasterDao().queryBuilder().
                      selectColumns("interpretationXmlStringName").where().eq("interpretationId",
                      visitDetailsList.get(i).getVisDSignsInvestInterpretation()).query();

              /*if(visitDetailsList.get(i).getVisDSignsInvestigations()!=null && visitDetailsList.get(i).getVisDSignsInvestigations().trim().length()>0) {

                  String[] dSigns = visitDetailsList.get(i).getVisDSignsInvestigations().split(",");
                  for(int l=0;l<dSigns.length;l++) {
                      List<TblSignsMaster> sinsList = databaseHelper.getTblSignsMasterDao().queryBuilder().
                              selectColumns("signXmlStringName").where().eq("signId",
                              dSigns[l].trim()).query();
                      signsMastersList.addAll(sinsList);
                  }
              }*/

            if(symptomMasterList!=null && symptomMasterList.size()>0)
            {
                String dia = "",interp = "", signs = "";
                if(diagnosisMastersList!=null && diagnosisMastersList.size()>0)
                    dia  = diagnosisMastersList.get(0).getDiagnosisXmlStringName();
                if(interpretationMastersList!=null && interpretationMastersList.size()>0)
                    interp  = interpretationMastersList.get(0).getInterpretationXmlStringName();
                /*if(signsMastersList!=null && signsMastersList.size()>0) {
                    for (int m = 0;m<signsMastersList.size();m++) {
                        signs = signs+":"+signsMastersList.get(m).getSignXmlStringName();
                    }
                }*/
                symptoms.add(symptomMasterList.get(0).getSymXmlStringName()+"-"+interp+"-"+dia);
            }

          }
      }
      return symptoms;


    }
//    15May2021 Arpitha
public  ArrayList<String>  getHomeVisitSymptomsForChild(String userId, String womanId, int visitNum)
        throws SQLException {
    ArrayList<String> symptoms = new ArrayList<>();
    ArrayList<String> symptomsComp = new ArrayList<>();
    String[] selectColumns = { "visDSymptoms","visDOtherSymptoms","ChildNo"}; //26Jul2021 Bindu add other col
    List<tblChildVisitDetails> visitDetailsList =
            databaseHelper.getTblChildVisitDetailDao().queryBuilder().distinct().selectColumns(selectColumns).where()
            .eq("UserId",userId).and().eq("WomanId",womanId).
                    and().eq("VisitNum",visitNum).query();
    String s =   databaseHelper.getTblChildVisitDetailDao().queryBuilder().distinct().selectColumns(selectColumns).where()
            .eq("UserId",userId).and().eq("WomanId",womanId).
                    and().eq("VisitNum",visitNum).prepare().toString();
    String b = s;
    if(visitDetailsList!=null && visitDetailsList.size()>0)
    {
        for (int i=0;i<visitDetailsList.size();i++) {
            List<TblSymptomMaster>  symptomMasterList =  databaseHelper.getTblSymptomMasterDao().queryBuilder()
                    .selectColumns("symXmlStringName").where()
                    .eq("symSymptomId", visitDetailsList.get(i)
                            .getVisDSymptoms()).query();

            if(symptomMasterList!=null && symptomMasterList.size()>0)
            {
                String strBabyNo = "";

                if(!visitDetailsList.get(i).getVisDSymptoms().equals("323")) { // 26Jul2021 Bindu chec for others
                    if (visitDetailsList.get(i).getChildNo() == 1)
                        strBabyNo = "Baby1";
                    else if (visitDetailsList.get(i).getChildNo() == 2)
                        strBabyNo = "Baby2";
                    else if (visitDetailsList.get(i).getChildNo() == 3)
                        strBabyNo = "Baby3";
                    else if (visitDetailsList.get(i).getChildNo() == 4)
                        strBabyNo = "Baby4";
                }

                 if(visitDetailsList.get(i).getVisDSymptoms().equals("323")) {//26Jul2021 Bindu add other
                     //strBabyNo = "All";
                     symptoms.add(symptomMasterList.get(0).getSymXmlStringName() + " - " +  visitDetailsList.get(i).getVisDOtherSymptoms() + "-babyAll-");
                 } else
                    symptoms.add(symptomMasterList.get(0).getSymXmlStringName() + " - " + strBabyNo);
                }
            }

        }
    //}
    return symptoms;


}


//17May2021 Bindu update covid vaccine details
public int updateCovidVaccineDetails(String womanId, String userId, tblCovidVaccineDetails reg, int transId, DatabaseHelper db) throws  Exception{

        int update = 0;
      /*  Dao<tblCovidVaccineDetails, Integer> regDAO = db.getTblCovidVaccineDao();
        UpdateBuilder<tblCovidVaccineDetails, Integer> updateBuilder = regDAO.updateBuilder();

        updateBuilder.updateColumnValue("CovidVaccinated", reg.getCovidVaccinated())
                .updateColumnValue("CovidFirstDoseDate", reg.getCovidFirstDoseDate())
                .updateColumnValue("CovidSecondDoseDate", reg.getCovidSecondDoseDate())
                .updateColumnValue("RecordUpdatedDate", reg.getRecordUpdatedDate())
                .where().eq("WomanId", womanId).and()
                .eq("userId", userId);

        String strupdate = updateBuilder.prepare().toString();
        update = updateBuilder.update();

        if(update > 0) {
            TransactionHeaderRepository transactionRepo = new TransactionHeaderRepository(databaseHelper);
            transactionRepo.updateNew(userId, transId, "tblCovidVaccineDetails"
                    , updateBuilder.prepare().toString().replace("MappedStatement:", "").trim(),
                    null, null,databaseHelper);
        }
*/ //20May2021 Bindu
        return update;
    }

    //25Jul2021 Bindu
    public Map<String, String> getSymptomsNames() throws SQLException {
        Map<String, String> symptoms = new HashMap<>();
        Dao<TblSymptomMaster, Integer> symptomMasterDao = databaseHelper.getTblSymptomMasterDao();
        String[] selectColumns = {"symSymptomId","symXmlStringName"};

        List<TblSymptomMaster> listSymptoms = null;
        listSymptoms = symptomMasterDao.queryBuilder().distinct().selectColumns(selectColumns)
                    .query();
            if (listSymptoms != null) {
                for (TblSymptomMaster tbl : listSymptoms) {
                    symptoms.put(tbl.getSymSymptomId(), tbl.getSymXmlStringName());
                }
            }
        return symptoms;
    }

    public Map<String, String> getInterpretationNames() throws SQLException {
        Map<String, String> symptoms = new HashMap<>();
        Dao<Tblinterpretationmaster, Integer> symptomMasterDao = databaseHelper.getTblInterpretationMasterDao();
        List<Tblinterpretationmaster> listSymptoms = null;
        String[] selectColumns = {"interpretationId","interpretationXmlStringName"};
        listSymptoms = symptomMasterDao.queryBuilder().distinct().selectColumns(selectColumns)
                .query();
        if (listSymptoms != null) {
            for (Tblinterpretationmaster tbl : listSymptoms) {
                symptoms.put(tbl.getInterpretationId(), tbl.getInterpretationXmlStringName());
            }
        }
        return symptoms;
    }

    public Map<String, String> getDiagnosisNames() throws SQLException {
        Map<String, String> symptoms = new HashMap<>();
        Dao<TblDiagnosisMaster, Integer> symptomMasterDao = databaseHelper.getTblDiagnosisMasterDao();
        List<TblDiagnosisMaster> listSymptoms = null;
        String[] selectColumns = {"diagnosisId","diagnosisXmlStringName"};
        listSymptoms = symptomMasterDao.queryBuilder().distinct().selectColumns(selectColumns)
                .query();
        if (listSymptoms != null) {
            for (TblDiagnosisMaster tbl : listSymptoms) {
                symptoms.put(tbl.getDiagnosisId(), tbl.getDiagnosisXmlStringName());
            }
        }
        return symptoms;
    }


    //    25Jul2021 Bindu
    public  ArrayList<String>  getHomeVisitSymptomsNew(String userId, String womanId, int visitNum, Context ctx)
            throws SQLException {

        Locale locale = null;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        Resources res = ctx.getResources();
        if (prefs.getBoolean("isEnglish", false)) {
            locale = new Locale("en");
        } else if (prefs.getBoolean("isKannada", false)) {  //10Nov2019 - Bindu - Add kannada
            locale = new Locale("kn");
        } else if (prefs.getBoolean("isTelugu", false)) {  //02Apr2021 - Add Telugu
            locale = new Locale("te");
        } else {
            locale = new Locale("en");
        }

        Configuration config = res.getConfiguration();
        config.locale = locale;
        res.updateConfiguration(config, res.getDisplayMetrics());
        
        Map<String, ArrayList<TblVisitDetailsPojo>> listMap = new LinkedHashMap<>();
        Map<String, String> interlistMap = new LinkedHashMap<>();
        Map<String, String> diaglistMap = new LinkedHashMap<>();

        ArrayList<TblVisitDetailsPojo> visitDetailsList= null;
        Dao<TblVisitDetailsPojo, Integer> detailPojoDao = databaseHelper.getTblVisitDetailPojoDao();
        String strSqlRec = "SELECT tblsymptomsmaster.symGroupId, " +
                "                  tblVisitDetails.visDSymptoms, " +
                "    `tblvisitdetails`.`visDOtherSymptoms`," +
                "    `tblvisitdetails`.`visDSignsInvestigations`," +
                "    `tblvisitdetails`.`visDSignsInvestInterpretation`," +
                "    `tblvisitdetails`.`visDDiagnosis` " +
                "                FROM tblsymptomsmaster " +
                "                 INNER JOIN tblVisitDetails ON tblsymptomsmaster.symSymptomId = " +
                "                    tblVisitDetails.visDSymptoms where tblVisitDetails.UserId = '"+userId+"' and tblVisitDetails.WomanId = '"+womanId+"' and tblVisitDetails.VisitNum = '"+visitNum+"'  group by tblVisitDetails.transid,tblVisitDetails.visDSymptoms order by tblsymptomsmaster.symGroupId asc";
        GenericRawResults<String[]> ss1 = detailPojoDao.queryRaw(strSqlRec);
        List<String[]> vv1 = ss1.getResults();
        if (vv1 != null && vv1.size() > 0) {
            visitDetailsList = new ArrayList<>();
            for (int i = 0; i < vv1.size(); i++) {
                String[] str = vv1.get(i);
                TblVisitDetailsPojo visDet = new TblVisitDetailsPojo();

                String[] str1 = new String[5];
                if(i > 0) {
//                     str = vv1.get(i);
                     str1 = vv1.get(i - 1);
                    if (!str1[0].equals(str[0])) {
                        visitDetailsList = new ArrayList<>();
                        str1[0] = str[0];
                    }
                }
                else
                    str1[0] = str[0];

                visDet.setSymGroupId(str[0]);
                visDet.setVisDSymptoms(str[1]);
                visDet.setVisDOtherSymptoms(str[2]);
                visDet.setVisDSignsInvestigations(str[3]);
                 /*visDet.setVisDSignsInvestInterpretation(str[4]);
                visDet.setVisDDiagnosis(str[5]);*/

                visitDetailsList.add(visDet);

                listMap.put(str[0],visitDetailsList);

                if((str[0].equals("23") || str[0].equals("22"))) {
                    interlistMap.put(str[0],str[4]);
                    diaglistMap.put(str[0],str[5]);
                } else {
                    if (!interlistMap.containsKey(str[0]) && !(str[0].equals("23") || str[0].equals("22")))
                        interlistMap.put(str[0], str[4]);
                    if (!diaglistMap.containsKey(str[0]) && !(str[0].equals("23") || str[0].equals("22")))
                        diaglistMap.put(str[0], str[5]);
                }
            }
        }

        ArrayList<String> symptoms = new ArrayList<>();
        if(listMap != null && !listMap.isEmpty()) {
            for (Map.Entry<String, ArrayList<TblVisitDetailsPojo>> entry : listMap.entrySet()) {
                String symp = "", inter = "", diag = "";
                ArrayList<String> symparr = new ArrayList<String>();
                for (TblVisitDetailsPojo symname : entry.getValue()) {
                    //symp = symname.getVisDSymptoms();
                    symp = MyApplication.mapSymptoms.get(symname.getVisDSymptoms());
                    int identifier = ctx.getResources().getIdentifier
                            (symp,
                                    "string", ctx.getPackageName());
                    if (identifier > 0) {
                        symp = ctx.getResources().getString(identifier);
                    }

                    //26Jul2021 Bindu
                    if(symname.getVisDSymptoms().equals("111") && (symname.getVisDSignsInvestigations() != null && symname.getVisDSignsInvestigations().length() > 0)) {
                        symp = ctx.getResources().getString(R.string.constipationnotrelieved);
                    }
                    if (symname.getVisDSymptoms().equals("141") || symname.getVisDSymptoms().equals("149"))
                        symparr.add(symname.getVisDOtherSymptoms() != null && symname.getVisDOtherSymptoms().length() > 0 ? symp + " - " + symname.getVisDOtherSymptoms() : "");
                    else
                        symparr.add(symp);
                }

                if (interlistMap != null && interlistMap.containsKey(entry.getKey())) {
                    inter = MyApplication.mapInterpretations.get(interlistMap.get(entry.getKey()));
                    if (inter != null && inter.length() > 0) {
//                    inter = HomeVisitViewActivity.mapInterpretations.get(interlistMap.get(entry.getKey()));
                        int identifier1 = ctx.getResources().getIdentifier
                                (inter,
                                        "string", ctx.getPackageName());
                        if (identifier1 > 0) {
                            inter = ctx.getResources().getString(identifier1);
                        }
                    }
                }

                if (diaglistMap != null && diaglistMap.containsKey(entry.getKey())) {
                    diag = MyApplication.mapDiagnosis.get(diaglistMap.get(entry.getKey()));
                    if (diag != null && diag.length() > 0) {
//                    diag = HomeVisitViewActivity.mapDiagnosis.get(diaglistMap.get(entry.getKey()));

                        int identifier2 = ctx.getResources().getIdentifier
                                (diag,
                                        "string", ctx.getPackageName());
                        if (identifier2 > 0) {
                            diag = ctx.getResources().getString(identifier2);
                        }
                    }
                }

                // create object of StringBuilder class
                StringBuilder sb = new StringBuilder();

                // Appends characters one by one
                for (String ch : symparr) {
                    sb.append(ch).append(",");
                }
                sb = sb.deleteCharAt(sb.lastIndexOf(","));

                // convert in string
                String sym = sb.toString();

                symptoms.add(sym + (inter != null && inter.length() > 0 ? " - " + inter : "") + (diag != null && diag.length() > 0 ? " - " + diag : ""));
            }
        }
        return symptoms;
    }

    //    25Jul2021 Bindu
    public  ArrayList<String>  getHomeVisitSymptomsPNC(String userId, String womanId, int visitNum, Context ctx)
            throws SQLException {
        Locale locale = null;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        Resources res = ctx.getResources();
        if (prefs.getBoolean("isEnglish", false)) {
            locale = new Locale("en");
        } else if (prefs.getBoolean("isKannada", false)) {  //10Nov2019 - Bindu - Add kannada
            locale = new Locale("kn");
        } else if (prefs.getBoolean("isTelugu", false)) {  //02Apr2021 - Add Telugu
            locale = new Locale("te");
        } else {
            locale = new Locale("en");
        }

        Configuration config = res.getConfiguration();
        config.locale = locale;
        res.updateConfiguration(config, res.getDisplayMetrics());

        ArrayList<String> symptoms = new ArrayList<>();
        List<TblVisitDetails> visitDetailsList =  databaseHelper.getTblVisitDetailDao().queryBuilder()
                .selectColumns("visDSymptoms","visDOtherSymptoms","visDSignsInvestigations","visDSignsInvestInterpretation","visDDiagnosis").where()
                .eq("UserId",userId).and().eq("WomanId",womanId).
                        and().eq("VisitNum",visitNum).query();
        if(visitDetailsList!=null && visitDetailsList.size()>0)
        {
            String symp = "";
            for (int i=0;i<visitDetailsList.size();i++) {
                symp = "";

                if(!(visitDetailsList.get(i).getVisDSymptoms().equalsIgnoreCase("168") )) {
                    symp = MyApplication.mapSymptoms.get(visitDetailsList.get(i).getVisDSymptoms());
                    int identifier = ctx.getResources().getIdentifier
                            (symp,
                                    "string", ctx.getPackageName());
                    if (identifier > 0) {
                        symp = ctx.getResources().getString(identifier);
                    }
                    symptoms.add(symp);
                } else {
                    symp = MyApplication.mapSymptoms.get(visitDetailsList.get(i).getVisDSymptoms());
                    int identifier = ctx.getResources().getIdentifier
                            (symp,
                                    "string", ctx.getPackageName());
                    if (identifier > 0) {
                        symp = ctx.getResources().getString(identifier);
                    }
                    symptoms.add(symp + " : " + visitDetailsList.get(i).getVisDOtherSymptoms());
                }
            }
        }
        return symptoms;
    }

    //    Bindu 18Jul2021 Bindu
    public int getLastHomeVisitNoChild(String userId, String chilId) throws Exception {
        String returnValue = null;
        Dao<TblVisitChildHeader, Integer> tblVisitHeaderDao = databaseHelper.getTblVisitChildHeaderDao();

        int lastVisitNo = 0;
        String result = "";
        String strqry = "select MAX(visitNum) from tblVisitChildHeader where ChildId = '" + chilId + "' and userId = '" + userId + "'";

        GenericRawResults<String[]> rawResults = tblVisitHeaderDao.queryRaw(strqry);

        List<String[]> results = rawResults.getResults();
        // This will select the first result (the first and maybe only row returned)
        String[] resultArray = results.get(0);
        //This will select the first field in the result which should be the ID
        if (resultArray[0] != null)
            lastVisitNo = Integer.parseInt(resultArray[0]) + 1;
        else
            lastVisitNo = lastVisitNo + 1;

        return lastVisitNo;
    }

    //    21Jul2021 Bindu
    public List<TblVisitChildHeader> getVisitHeaderDetailsForChild(String chlid, String sessionUserId) throws SQLException {
        return databaseHelper.getTblVisitChildHeaderDao().queryBuilder()
                .where().eq("userId", sessionUserId).and().eq("ChildId",chlid).query();
    }

    //    21Jul2021 Bindu - Specific child
    public  ArrayList<String> getHomeVisitSymptomsChild(String userId, String chlid, int visitNum) throws SQLException {
        ArrayList<String> symptoms = new ArrayList<>();
        List<tblChildVisitDetails> visitDetailsList =
                databaseHelper.getTblChildVisitDetailDao().queryBuilder()
                        .selectColumns("visDSymptoms")
                        .selectColumns("visDOtherSymptoms")
                        .where()
                        .eq("UserId",userId).and().eq("ChildId",chlid).
                        and().eq("VisitNum",visitNum).query();
        if(visitDetailsList!=null && visitDetailsList.size()>0)
        {
            for (int i=0;i<visitDetailsList.size();i++) {
                List<TblSymptomMaster>  symptomMasterList =  databaseHelper.getTblSymptomMasterDao().queryBuilder()
                        .selectColumns("symXmlStringName").where()
                        .eq("symSymptomId", visitDetailsList.get(i)
                                .getVisDSymptoms()).query();

                if(symptomMasterList!=null && symptomMasterList.size()>0)
                {
                    if(visitDetailsList.get(i).getVisDSymptoms().equals("323")) {
                        symptoms.add(symptomMasterList.get(0).getSymXmlStringName() + " - " +  visitDetailsList.get(i).getVisDOtherSymptoms());
                    } else
                        symptoms.add(symptomMasterList.get(0).getSymXmlStringName());
                }


            }
        }
        return symptoms;
    }
}
