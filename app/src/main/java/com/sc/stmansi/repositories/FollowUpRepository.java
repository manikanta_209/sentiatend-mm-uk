package com.sc.stmansi.repositories;

import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.dao.RawRowMapper;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.tables.TblChildInfo;
import com.sc.stmansi.tables.tblAdolReg;
import com.sc.stmansi.tables.tblregisteredwomen;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FollowUpRepository {

    private DatabaseHelper databaseHelper;

    public FollowUpRepository(DatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }

    public List<tblAdolReg> getAdolFollowUpDueList(int villageCode, String fromDate,
                                                   String toDate, String filter, int limitStart,String phc,
                                                   String sc,String panchayath)
            throws SQLException {


        boolean includeVillageCode = villageCode!=0,
                dateFilter = fromDate!=null && toDate!=null && fromDate.trim().length()>0 &&
                        toDate.trim().length()>0,
                includeUserFilter = filter!=null && filter.trim().length()>0;
        String strvillageCode = "", strFilter = "";

        if (includeVillageCode) {
            strvillageCode = " and tblAdolReg.regAdolFacilities = " + villageCode;
            /*strvillageCode = "and tblfacilitydetails.PHCName= '"+phc+"' and " +
                    "  tblfacilitydetails.SCName= '"+sc+"' and " +
                    " tblfacilitydetails.PanchayathName= '"+panchayath+"'  " +
                    " and  tblAdolReg.regAdolFacilities= '"+villageCode+"'";*/
        }


        String strDateFilter =  "";


        if(dateFilter)
        /*strDateFilter = " AND (Date(substr(Recordcreateddate,  7) || '-' || substr(Recordcreateddate ,4,2)  || '-'   ||   substr(Recordcreateddate, 1,2)) " +
                " between   Date(substr('"+fromDate+"' ,  + 7) || '-' || substr('"+fromDate+"' ,4,2)  || '-'    || substr('"+fromDate+"' , 1,2)) " +
                " and  Date(substr('"+toDate+"' ,    7) || '-' || substr('"+toDate+"' ,4,2)  || '-'   || substr('"+toDate+"' , 1,2))) ";
*/
        strDateFilter = " AND ( ((Date(substr(tblAdolVisitHeader.adolvisHVisitDate, 7,4) || '-' || substr(tblAdolVisitHeader.adolvisHVisitDate ,4,2)  || '-'   ||   substr(tblAdolVisitHeader.adolvisHVisitDate, 1,2)) \n" +
                "                between   Date(substr('"+fromDate+"' ,   7,4) || '-' || substr('"+fromDate+"' ,4,2)  || '-'    || substr('"+fromDate+"' , 1,2)) \n" +
                "                 and  Date(substr('"+toDate+"' ,    7,4) || '-' || substr('"+toDate+"' ,4,2)  || '-'   || substr('"+toDate+"' , 1,2))))\n" +


                " OR (Date(substr(tblAdolReg.Recordcreateddate, 7,4) || '-' || substr(tblAdolReg.Recordcreateddate ,4,2)  || '-'   ||   substr(tblAdolReg.Recordcreateddate, 1,2)) \n" +
                "                between   Date(substr('"+fromDate+"' ,   7,4) || '-' || substr('"+fromDate+"' ,4,2)  || '-'    || substr('"+fromDate+"' , 1,2)) \n" +
                "                 and  Date(substr('"+toDate+"' ,    7,4) || '-' || substr('"+toDate+"' ,4,2)  || '-'   || substr('"+toDate+"' , 1,2))))\n" ;


        if (includeUserFilter) {
            strFilter = " and regAdolName like '%" + filter + "%' or regAdolMobile like " +
                    "'%" + filter + "%' or regAdolAadharNo like '%" + filter + "%'";
        }

     /*   String sql = "Select AdolId,regAdolName, regAdolAge,regAdolGender,regAdolMobile, " +
                "regAdolGoingtoSchool,regAdolMenstrualPbm,regAdolHealthIssues,Recordcreateddate " +
                "from tblAdolReg where " +
                "length(regAdolMenstrualPbm)>0 OR AdolId IN (SELECT AdolId from " +
                "tblAdolVisitHeader where " +
                "length(adolvisHMenstrualProblem))  OR regAdolHealthIssues like '%Anemia%' and regAdolDeactivateDate=''";
*/

        String sql = "Select AdolId,regAdolName, regAdolAge,regAdolGender,regAdolMobile," +
                "   regAdolGoingtoSchool,regAdolMenstrualPbm,regAdolHealthIssues,Recordcreateddate " +
                "  from tblAdolReg " +
                "  INNER JOIN tblfacilitydetails ON tblAdolReg.regAdolFacilities = " +
                "    tblfacilitydetails.AutoId " +
                "where " +
                "   (length(regAdolMenstrualPbm)>0 OR AdolId IN (SELECT AdolId from " +
                " tblAdolVisitHeader where " +
                "  (length(adolvisHMenstrualProblem)>0  or adolvisHHb<9)   and adolvisHFollowUpbyCC!='Yes' )  OR regAdolHealthIssues like '%Anemia%') " +
                " and regAdolDeactivateDate = ''   "
                +"   and tblfacilitydetails.PHCName= '"+phc+"' and   tblfacilitydetails.SCName= '"+sc+"' " +
                " and  tblfacilitydetails.PanchayathName= '"+panchayath+"'  "
                + strvillageCode + strDateFilter + strFilter
                + " order by transId desc  LIMIT 10 OFFSET "+limitStart;


        GenericRawResults<tblAdolReg> adolList = databaseHelper.gettblAdolregRuntimeExceptionDao()
                .queryRaw(sql, new RawRowMapper<tblAdolReg>() {
                    @Override
                    public tblAdolReg mapRow(String[] columnNames, String[] resultColumns) throws SQLException {
                       tblAdolReg tblAdolReg = new tblAdolReg();
                       tblAdolReg.setAdolID(resultColumns[0]);
                       tblAdolReg.setRegAdolName(resultColumns[1]);
                       tblAdolReg.setRegAdolAge(resultColumns[2]);
                       tblAdolReg.setRegAdolGender(resultColumns[3]);
                       tblAdolReg.setRegAdolMobile(resultColumns[4]);
                       tblAdolReg.setRegAdolGoingtoSchool(resultColumns[5]);
                       tblAdolReg.setRegAdolMenstrualPbm(resultColumns[6]);
                        tblAdolReg.setRegAdolHealthIssues(resultColumns[7]);
                        tblAdolReg.setRecordCreatedDate(resultColumns[8]);
                        return tblAdolReg;
                    }
                });

        List<tblAdolReg> adolRegList  = new ArrayList<>();
        adolRegList.addAll(adolList.getResults());
        return adolRegList;
    }

   /* public long getAdolFollowUpDueCount() throws SQLException {

        String sql = "Select count(AdolId) from tblAdolReg where " +
                "length(regAdolMenstrualPbm)>0 OR AdolId IN (SELECT AdolId from " +
                "tblAdolVisitHeader where " +
                "length(adolvisHMenstrualProblem))  OR regAdolHealthIssues like '%Anemia%'";

        GenericRawResults<Integer> adolList = databaseHelper.gettblAdolregRuntimeExceptionDao()
                .queryRaw(sql, new RawRowMapper<Integer>() {
                    @Override
                    public Integer mapRow(String[] columnNames, String[] resultColumns) throws SQLException {
                      *//*  tblAdolReg tblAdolReg = new tblAdolReg();
                        tblAdolReg.setAdolID(resultColumns[0]);
                        tblAdolReg.setRegAdolName(resultColumns[1]);
                        tblAdolReg.setRegAdolAge(resultColumns[2]);
                        tblAdolReg.setRegAdolGender(resultColumns[3]);
                        tblAdolReg.setRegAdolMobile(resultColumns[4]);
                        tblAdolReg.setRegAdolGoingtoSchool(resultColumns[5]);
                        tblAdolReg.setRegAdolMenstrualPbm(resultColumns[6]);
                        tblAdolReg.setRegAdolHealthIssues(resultColumns[7]);
                        tblAdolReg.setRecordCreatedDate(resultColumns[8]);*//*


                        return resultColumns.length;
                    }
                });


        return adolList.getResults().size();
    }*/

    public List<TblChildInfo> getChildFollowUpDueList(int villageCode, String toDate,String fromDate,
                                                      String filter,
                                                      int limitStart, String phc, String sc, String panchayath) throws SQLException {

        boolean includeVillageCode = villageCode!=0,
                dateFilter = fromDate!=null && toDate!=null && fromDate.trim().length()>0 &&
                        toDate.trim().length()>0,
                includeUserFilter = filter!=null && filter.trim().length()>0;
        String strvillageCode = "", strFilter = "";

        if (includeVillageCode) {
            strvillageCode = " and chlTribalHamlet = " + villageCode +" ";
        }

        String strDateFilter =  "";


        if(dateFilter)
            strDateFilter = "AND  ((Date(substr(chlDateOfBirth,  7) || '-' || substr(chlDateOfBirth ,4,2)  || '-'   ||   substr(chlDateOfBirth, 1,2)) \n" +
                    "                    between   Date(substr('"+fromDate+"' ,  + 7) || '-' || substr('"+fromDate+"' ,4,2)  || '-'    || substr('"+fromDate+"' , 1,2)) \n" +
                    "                     and  Date(substr('"+toDate+"' ,    7) || '-' || substr('"+toDate+"' ,4,2)  || '-'   || substr('"+toDate+"' , 1,2))) or\n" +
                    "\t\t\t\t\t \n" +
                    "\t\t\t\t\t(Date(substr(chlGMVisitdate,  7) || '-' || substr(chlGMVisitdate ,4,2)  || '-'   ||   substr(chlGMVisitdate, 1,2)) \n" +
                    "                    between   Date(substr('"+fromDate+"' ,  + 7) || '-' || substr('"+fromDate+"' ,4,2)  || '-'    || substr('"+fromDate+"' , 1,2)) \n" +
                    "                     and  Date(substr('"+toDate+"' ,    7) || '-' || substr('"+toDate+"' ,4,2)  || '-'   || substr('"+toDate+"' , 1,2))) )";
          /*  strDateFilter = " AND (Date(substr(chlDateOfBirth,  7) || '-' || substr(chlDateOfBirth ,4,2)  || '-'   ||   substr(chlDateOfBirth, 1,2)) " +
                    " between   Date(substr('"+fromDate+"' ,  + 7) || '-' || substr('"+fromDate+"' ,4,2)  || '-'    || substr('"+fromDate+"' , 1,2)) " +
                    " and  Date(substr('"+toDate+"' ,    7) || '-' || substr('"+toDate+"' ,4,2)  || '-'   || substr('"+toDate+"' , 1,2))) ";
*/


        if (includeUserFilter) {
            strFilter = " and chlChildName like '%" + filter + "%' or chlRCHID like " +
                    "'%" ;
            //+ filter + "%' or chlRCHID like '%" + filter + "%'";
        }
        String sql = "Select distinct tblchildinfo.\"UserId\"\t,\n" +
                "\t\"WomanId\"\t,\n" +
                "\t\"chlReg\"\t,\n" +
                "\t\"chlNo\"\t,\n" +
                "tblchildinfo.chlID," +
                "\t\"chlParentId\"\t,\n" +
                "\t\"chlRCHID\"\t,\n" +
                "\t\"chlRegDate\"\t,\n" +
                "\t\"chlChildname\"\t,\n" +
                "\t\"chlDateOfBirth\"\t,\n" +
                "\t\"chlTimeOfBirth\"\t,\n" +
                "\t\"chlDeliveryType\"\t,\n" +
                "\t\"chlDelTypeOther\"\t,\n" +
                "\t\"chlDeliveryResult\"\t,\n" +
                "\t\"chlIUDType\"\t,\n" +
                "\t\"chlDeathDate\"\t,\n" +
                "\t\"chlDelPlace\"\t,\n" +
                "\t\"chlDeliveredBy\"\t,\n" +
                "\t\"chlDeliveredByOther\"\t,\n" +
                "\t\"chlDeliveryConductedByName\"\t,\n" +
                "\t\"chlGender\",\n" +
                "\t\"chlWeight\"\t,\n" +
                "\t\"chlCryAfterBirth\"\t,\n" +
                "\t\"chlBreastFeed\"\t,\n" +
                "\t\"chlComplications\"\t,\n" +
                "\t\"chlOtherComplications\"\t,\n" +
                "\t\"chlTribalHamlet\"\t,\n" +
                "\t\"chlGoingToSchool\"\t,\n" +
                "\t\"chlPhysicalDisability\"\t,\n" +
                "\t\"chlMentalDisability\"\t,\n" +
                "\t\"chlOtherPhysicalDisability\"\t,\n" +
                "\t\"chlOtherMentalDisability\"\t,\n" +
                "\t\"chlIsAshaAcompany\"\t,\n" +
                "\t\"chlComments\"\t,\n" +
                "\t\"chlDeactDate\"\t,\n" +
                "\t\"chlDeactReason\"\t,\n" +
                "\t\"chlDeactOtherReason\"\t,\n" +
                "\t\"chlDeactComments\"\t,\n" +
                "\t\"chlDeactRelocatedDate\"\t,\n" +
                "\t\"chlDeactMortalityDate\"\t,\n" +
                "\t\"chlUserType\"\t,\n" +
                "\t\n" +
                "\t\"chlSkintoskincontact\"  " +
                "from tblchildinfo"
                +" LEFT join tblchildgrowth on tblchildinfo.chlId = tblchildgrowth.chlId "
                +"INNER JOIN tblfacilitydetails ON tblchildinfo.chlTribalHamlet = tblfacilitydetails.AutoId "
               + " where" +
                " ( tblchildinfo.chlId IN (SELECT tblchildgrowth.chlId from tblchildgrowth where chlGMMuac<=11.4 or (chlGMMuac>11.4" +
                " and chlGMMuac < 12.5))    or chlWeight <2500)   "
                +"   and tblfacilitydetails.PHCName= '"+phc+"' and   tblfacilitydetails.SCName= '"+sc+"' " +
                "   and  tblfacilitydetails.PanchayathName= '"+panchayath+"' "
                +strvillageCode
                +strDateFilter
                +strFilter+" order by tblchildinfo.transId desc  LIMIT 10 OFFSET "+limitStart;

        GenericRawResults<TblChildInfo> adolList = databaseHelper.getChildInfoRuntimeExceptionDao()
                .queryRaw(sql, new RawRowMapper<TblChildInfo>() {
                    @Override
                    public TblChildInfo mapRow(String[] columnNames, String[] resultColumns) throws SQLException {
                        TblChildInfo tblChildInfo = new TblChildInfo();
                        tblChildInfo.setUserId(resultColumns[0]);
                        tblChildInfo.setWomanId(resultColumns[1]);
                        if(resultColumns[2]!=null && resultColumns[2].trim().length()>0)
                        tblChildInfo.setChlReg(Integer.parseInt(resultColumns[2]));
                        if(resultColumns[3]!=null && resultColumns[3].trim().length()>0)
                        tblChildInfo.setChildNo(Integer.parseInt(resultColumns[3]));
                        tblChildInfo.setChildID(resultColumns[4]);
                        tblChildInfo.setChlParentId(resultColumns[5]);
                        tblChildInfo.setChildRCHID(resultColumns[6]);
                        tblChildInfo.setChlRegDate(resultColumns[7]);
                        tblChildInfo.setChlBabyname(resultColumns[8]);
                        tblChildInfo.setChlDateTimeOfBirth(resultColumns[9]+" "+resultColumns[10]);
                        tblChildInfo.setChlDeliveryType(resultColumns[11]);
                        tblChildInfo.setChlDelTypeOther(resultColumns[12]);
                        if(resultColumns[13]!=null && resultColumns[13].trim().length()>0)
                        tblChildInfo.setChlDeliveryResult(Integer.parseInt(resultColumns[13]));
                        tblChildInfo.setChlIUDType(resultColumns[14]);
                        tblChildInfo.setChlInfantDeathDate(resultColumns[15]);
                        tblChildInfo.setChlDelPlace(resultColumns[16]);
                        tblChildInfo.setChlDeliveredBy(resultColumns[17]);
                        tblChildInfo.setChlDeliveredByOther(resultColumns[18]);
                        tblChildInfo.setDelDeliveryConductedByName(resultColumns[19]);
                        tblChildInfo.setChlGender(resultColumns[20]);
                        tblChildInfo.setDelBabyWeight(resultColumns[21]);
                        tblChildInfo.setChlCryAfterBirth(resultColumns[22]);
                        tblChildInfo.setChlBreastFeed(resultColumns[23]);
                        tblChildInfo.setChlComplications(resultColumns[24]);
                        tblChildInfo.setChlOtherComplications(resultColumns[25]);
                        if(resultColumns[26]!=null && resultColumns[26].trim().length()>0)
                        tblChildInfo.setChlTribalHamlet(Integer.parseInt(resultColumns[26]));
                        tblChildInfo.setChlSchool(resultColumns[27]);
                        tblChildInfo.setChlPhysicalDisability(resultColumns[28]);
                        tblChildInfo.setChlMentalDisability(resultColumns[29]);
                        tblChildInfo.setChlOtherPhysicalDisability(resultColumns[30]);

                        tblChildInfo.setChlOtherMentalDisability(resultColumns[31]);
                        tblChildInfo.setChlIsAshaAcompany(resultColumns[32]);
                        tblChildInfo.setChlChildComments(resultColumns[33]);
                        tblChildInfo.setChlDeactDate(resultColumns[34]);
                        tblChildInfo.setChlDeactReason(resultColumns[35]);
                        tblChildInfo.setChlDeactOtherReason(resultColumns[36]);
                        tblChildInfo.setChlDeactComments(resultColumns[37]);
                        tblChildInfo.setChlDeactRelocatedDate(resultColumns[38]);
                        tblChildInfo.setChlDeactMortalityDate(resultColumns[39]);
                        tblChildInfo.setChlUserType(resultColumns[40]);
                        tblChildInfo.setChlSkintoskincontact(resultColumns[41]);



                        return tblChildInfo;
                    }
                });

        List<TblChildInfo> adolRegList  = new ArrayList<>();
        adolRegList.addAll(adolList.getResults());
        return adolRegList;
    }

    public long getChildFollowUpDueCount() throws SQLException {

        String sql = "Select chlId,chlChildName, chlDateOfBirth,chlGender from tblchildinfo where chlId IN (SELECT chlId from tblchildgrowth where chlGMMuac<=11.4 or (chlGMMuac>11.4 and chlGMMuac < 12.5))";

        GenericRawResults<Integer> adolList = databaseHelper.gettblAdolregRuntimeExceptionDao()
                .queryRaw(sql, new RawRowMapper<Integer>() {
                    @Override
                    public Integer mapRow(String[] columnNames, String[] resultColumns) throws SQLException {
                      /*  tblAdolReg tblAdolReg = new tblAdolReg();
                        tblAdolReg.setAdolID(resultColumns[0]);
                        tblAdolReg.setRegAdolName(resultColumns[1]);
                        tblAdolReg.setRegAdolAge(resultColumns[2]);
                        tblAdolReg.setRegAdolGender(resultColumns[3]);
                        tblAdolReg.setRegAdolMobile(resultColumns[4]);
                        tblAdolReg.setRegAdolGoingtoSchool(resultColumns[5]);
                        tblAdolReg.setRegAdolMenstrualPbm(resultColumns[6]);
                        tblAdolReg.setRegAdolHealthIssues(resultColumns[7]);
                        tblAdolReg.setRecordCreatedDate(resultColumns[8]);*/


                        return resultColumns.length;
                    }
                });


        return adolList.getResults().size();
    }

    public List<tblregisteredwomen> getWomanFollowUpDueListold() throws SQLException {
        String sql = "Select WomanId, regWomanName,regRegistrationDate,regStatusWhileRegistration,regADDate,regriskFactors,regPregnantorMother from tblregisteredwomen where WomanId IN (SELECT WomanId from tblVisitHeader)";

        GenericRawResults<tblregisteredwomen> adolList = databaseHelper.gettblregRuntimeExceptionDao()
                .queryRaw(sql, new RawRowMapper<tblregisteredwomen>() {
                    @Override
                    public tblregisteredwomen mapRow(String[] columnNames, String[] resultColumns) throws SQLException {
                        tblregisteredwomen tblChildInfo = new tblregisteredwomen();
                        tblChildInfo.setWomanId(resultColumns[0]);
                        tblChildInfo.setRegWomanName(resultColumns[1]);
                        tblChildInfo.setRegRegistrationDate(resultColumns[2]);

                        tblChildInfo.setCurrentWomenStatus(resultColumns[3]);
                        tblChildInfo.setRegADDate(resultColumns[4]);
                        tblChildInfo.setRegriskFactors(resultColumns[5]);
                        tblChildInfo.setRegPregnantorMother(Integer.parseInt(resultColumns[6]));
                        return tblChildInfo;
                    }
                });

        List<tblregisteredwomen> adolRegList  = new ArrayList<>();
        adolRegList.addAll(adolList.getResults());
        return adolRegList;
    }



    //    19Jun2021 Arpitha - get woman
    public List<tblregisteredwomen> getWomanFollowUpDueList(int villageCode,
                                                 String cond,
                                                 String filter,
                                                 long limitStart, String fromDate, String toDate,String phc, String sc, String panchayath) throws Exception {
        boolean includeVillageCode = villageCode != 0;
        boolean includeUserFilter = filter != null && filter.trim().length() > 0;
        boolean includePatientStatus = cond != null && cond.trim().length() > 0;
        boolean includeDateFilter = fromDate != null && toDate != null && fromDate.trim().length() >0 && toDate.trim().length() > 0;


        String strvillageCode = "", strStatus = "", strFilter = "", strLMPConfirm = "", strDateFilter = "";



      /*  if (includeVillageCode) {
            strvillageCode = " and regvillage = " + villageCode;
        }*/

        if(includeVillageCode) {
            /*strvillageCode = "  and tblfacilitydetails.PHCName= 'Ananthagiri' and " +
                    "   tblfacilitydetails.SCName= 'Tokuru' and " +
                    "   tblfacilitydetails.PanchayathName= 'Tokuru'  " +
                    "            and  tblregisteredwomen.regvillage= '" + villageCode + "' ";
*/
            /*strvillageCode = "and tblfacilitydetails.PHCName= '"+phc+"' and " +
                    "  tblfacilitydetails.SCName= '"+sc+"' and " +
                    " tblfacilitydetails.PanchayathName= '"+panchayath+"'  " +
                    " and  tblregisteredwomen.regvillage= '"+villageCode+"'";*/
            strvillageCode = " and  tblregisteredwomen.regvillage= '"+villageCode+"'";
        }




        if (includePatientStatus) {
            if (cond.equalsIgnoreCase("preg")) {
                strStatus = " and regpregnantorMother = 1 and DateDeactivated is null ";
            } else if (cond.equalsIgnoreCase("pnc")) {
                strStatus = "  regpregnantorMother = 2 and ((DateDeactivated is null and delMothersDeath!='Y') " +
                        "or (chlReg=0 and (chlDeactDate is null   and chlDeliveryResult==1) ) ) "; //10Dec2019 - Check for del child



            }
            else if (cond.equalsIgnoreCase("mother")) {


                strStatus = "  and    (regpregnantorMother = 2 and " +
                        "((DateDeactivated is null ) or " +
                        "(chlDeactDate is null and chlReg=0 ) )) " ;


            }
            else


                strStatus = "  and   ((regpregnantorMother = 2 and " +
                        "((DateDeactivated is null ) or " +
                        "(chlDeactDate is null and chlReg=0 ) )) " +
                        "or  (regpregnantorMother = 1 and (DateDeactivated is null))) ";



        }

        if (includeUserFilter) {
            strFilter = " and regWomanName like '%" + filter + "%' or regphonenumber like " +
                    "'%" + filter + "%' or reguidNo like '%" + filter + "%'";
        }

        if(includeDateFilter)
            strDateFilter = "  (Date(substr(visHVisitDate, \n" +
                    "                     7) || '-' || substr(visHVisitDate ,4,2)  || '-'  \n" +
                    "                     || \n" +
                    "                      substr(visHVisitDate, 1,2))  between \n" +
                    "                     Date(substr('"+fromDate+"' ,  + 7) || '-' || substr('"+fromDate+"' ,4,2)  || '-'  \n" +
                    "                 \n" +
                    "                      || substr('"+fromDate+"' , 1,2)) and  Date(substr('"+toDate+"' , \n" +
                    "                       7) || '-' || substr('"+toDate+"' ,4,2)  || '-' \n" +
                    "                      || substr('"+toDate+"' , 1,2))) \n" ;

          //  if(cond.equalsIgnoreCase("preg"))
         /*   strDateFilter = "and  ((Date(substr(LastAncVisitDate, " +
                    " 7) || '-' || substr(LastAncVisitDate ,4,2)  || '-'  " +
                    " || " +
                    "  substr(LastAncVisitDate, 1,2))  between " +
                    "  Date(substr('"+fromDate+"' ,  + 7) || '-' || substr('"+fromDate+"' ,4,2)  || '-'  " +
                   // "\t\t\t\t\t\t\t\n" +
                  //  "\t\t\t\t\n" +
                    "  || substr('"+fromDate+"' , 1,2)) and  Date(substr('"+toDate+"' , " +
                    "   7) || '-' || substr('"+toDate+"' ,4,2)  || '-' " +
                    "  || substr('"+toDate+"' , 1,2)) and regpregnantorMother = 1) "+
                    " OR "+

                    "  (Date(substr(LastPncVisitDate, " +
                    " 7) || '-' || substr(LastPncVisitDate ,4,2)  || '-'  " +
                    " || " +
                    "  substr(LastPncVisitDate, 1,2))  between " +
                    "  Date(substr('"+fromDate+"' ,  + 7) || '-' || substr('"+fromDate+"' ,4,2)  || '-'  " +
                    // "\t\t\t\t\t\t\t\n" +
                    //  "\t\t\t\t\n" +
                    "  || substr('"+fromDate+"' , 1,2)) and  Date(substr('"+toDate+"' , " +
                    "   7) || '-' || substr('"+toDate+"' ,4,2)  || '-' " +
                    "  || substr('"+toDate+"' , 1,2)) and regpregnantorMother = 2)    OR\n" +

                    "(Date(substr(visHVisitDate, \n" +
                    "                     7) || '-' || substr(visHVisitDate ,4,2)  || '-'  \n" +
                    "                     || \n" +
                    "                      substr(visHVisitDate, 1,2))  between \n" +
                    "                     Date(substr('"+fromDate+"' ,  + 7) || '-' || substr('"+fromDate+"' ,4,2)  || '-'  \n" +
                    "                 \n" +
                    "                      || substr('"+fromDate+"' , 1,2)) and  Date(substr('"+toDate+"' , \n" +
                    "                       7) || '-' || substr('"+toDate+"' ,4,2)  || '-' \n" +
                    "                      || substr('"+toDate+"' , 1,2))) ";
*/





        String sql;



            sql = " Select distinct regWomanName, regRegistrationDate, " +
                    "regStatusWhileRegistration, " +
                    " regComplicatedpreg, regAmberOrRedColorCode," +
                    " regrecommendedPlaceOfDelivery, tblregisteredwomen.UserId," +
                    "tblregisteredwomen.WomanId, regVillage, regLMP, IsCompl, " +
                    " LastAncVisitDate, LastPncVisitDate, IsReferred, regADDate, " +
                    " regriskFactors, regPregnantorMother , regInDanger, DateDeactivated, regWomenImage, isLMPConfirmed" + //07Apr2021 Bindu add isLMPconfirmed
                    " from tblregisteredwomen left " +
                    "join tblchildinfo on tblregisteredwomen.WomanId = tblchildinfo.WomanId " +
                    " left join tbldeliveryinfo on tblregisteredwomen.WomanId = tbldeliveryinfo.WomanId "+
                    "  INNER JOIN tblfacilitydetails ON tblregisteredwomen.regVillage = " +
                    "                     tblfacilitydetails.AutoId " +
                    "  INNER JOIN tblVisitHeader  ON tblregisteredwomen.WomanId = tblVisitHeader.WomanId" +


                    " where  "
       + "   tblregisteredwomen.WomanId IN (SELECT WomanId from tblVisitHeader) and  tblregisteredwomen.WomanId NOT In (SELECT BeneficiaryID from tblCaseMgmt) "
                   + "and tblfacilitydetails.PHCName= '"+phc+"' and " +
                    "  tblfacilitydetails.SCName= '"+sc+"' and " +
                    " tblfacilitydetails.PanchayathName= '"+panchayath+"'  "
                    + strStatus +
                    strvillageCode +

                    strLMPConfirm + strFilter + strDateFilter +

                    "  order by tblregisteredwomen.transId desc " + " LIMIT 10 OFFSET " + (limitStart); //25MAr2021 Bindu add lmp validation

        GenericRawResults<tblregisteredwomen> tbls =
                databaseHelper.gettblregRuntimeExceptionDao().

                        queryRaw(sql,
                                new RawRowMapper<tblregisteredwomen>() {
                                    @Override
                                    public tblregisteredwomen mapRow(String[] strings, String[] strings1) {
                                        tblregisteredwomen tblregisteredwomen = new tblregisteredwomen();
                                        tblregisteredwomen.setRegWomanName(strings1[0]);
                                        tblregisteredwomen.setRegRegistrationDate(strings1[1]);
                                        tblregisteredwomen.setRegStatusWhileRegistration(strings1[2]);
                                        tblregisteredwomen.setRegComplicatedpreg(Integer.parseInt(strings1[3]));
                                        tblregisteredwomen.setRegAmberOrRedColorCode(Integer.parseInt(strings1[4]));
                                        tblregisteredwomen.setRegrecommendedPlaceOfDelivery(strings1[5]);
                                        tblregisteredwomen.setUserId(strings1[6]);
                                        tblregisteredwomen.setWomanId(strings1[7]);
                                        tblregisteredwomen.setRegVillage(Integer.parseInt(strings1[8]));
                                        tblregisteredwomen.setRegLMP(strings1[9]);
                                        tblregisteredwomen.setIsCompl(strings1[10]);
                                        tblregisteredwomen.setLastAncVisitDate(strings1[11]);
                                        tblregisteredwomen.setLastPncVisitDate(strings1[12]);
                                        tblregisteredwomen.setIsReferred(strings1[13]);
                                        tblregisteredwomen.setRegADDate(strings1[14]);
                                        tblregisteredwomen.setRegriskFactors(strings1[15]);
                                        tblregisteredwomen.setRegPregnantorMother(Integer.parseInt(strings1[16]));

                                        tblregisteredwomen.setRegInDanger(Integer.parseInt(strings1[17]));
                                        tblregisteredwomen.setDateDeactivated(strings1[18]);
                                        tblregisteredwomen.setRegWomenImage(strings1[19]);
                                        tblregisteredwomen.setIsLMPConfirmed(Integer.parseInt(strings1[20])); //07Apr2021 Bindu set lmp confirmed
                                        return tblregisteredwomen;
                                    }
                                });

        List<tblregisteredwomen> regList = new ArrayList<>();

        regList.addAll(tbls.getResults());

        return regList;

    }

    //    19Jun2021 Arpitha - get woman
    public int getWomanFollowUpDueCount(int villageCode,
                                                            String cond,
                                                            String filter,
                                                           String fromDate, String toDate) throws SQLException {
        boolean includeVillageCode = villageCode != 0;
        boolean includeUserFilter = filter != null && filter.trim().length() > 0;
        boolean includePatientStatus = cond != null && cond.trim().length() > 0;
        boolean includeDateFilter = fromDate != null && toDate != null &&
                fromDate.trim().length() >0 && toDate.trim().length() > 0;


        String strvillageCode = "", strStatus = "", strFilter = "", strLMPConfirm = "", strDateFilter = "";



        if(includeVillageCode) {
            strvillageCode = "  and tblfacilitydetails.PHCName= 'Ananthagiri' and " +
                    "   tblfacilitydetails.SCName= 'Tokuru' and " +
                    "   tblfacilitydetails.PanchayathName= 'Tokuru'  " +
                    "            and  tblregisteredwomen.regvillage= '" + villageCode + "' ";
        }





        if (includePatientStatus) {
            if (cond.equalsIgnoreCase("preg")) {
                strStatus = " and regpregnantorMother = 1 and DateDeactivated is null ";
            } else if (cond.equalsIgnoreCase("pnc")) {
                strStatus = "  regpregnantorMother = 2 and ((DateDeactivated is null and delMothersDeath!='Y') " +
                        "or (chlReg=0 and (chlDeactDate is null   and chlDeliveryResult==1) ) ) "; //10Dec2019 - Check for del child



            }
            else if (cond.equalsIgnoreCase("mother")) {


                strStatus = "  and    (regpregnantorMother = 2 and " +
                        "((DateDeactivated is null ) or " +
                        "(chlDeactDate is null and chlReg=0 ) )) " ;


            }
            else


                strStatus = "  and   ((regpregnantorMother = 2 and " +
                        "((DateDeactivated is null ) or " +
                        "(chlDeactDate is null and chlReg=0 ) )) " +
                        "or  (regpregnantorMother = 1 and (DateDeactivated is null))) ";



        }

        if (includeUserFilter) {
            strFilter = " and regWomanName like '%" + filter + "%' or regphonenumber like " +
                    "'%" + filter + "%' or reguidNo like '%" + filter + "%'";
        }

        if(includeDateFilter)
          //  if(cond.equalsIgnoreCase("preg"))
                strDateFilter = "and  (Date(substr(LastAncVisitDate, " +
                        " 7) || '-' || substr(LastAncVisitDate ,4,2)  || '-'  " +
                        " || " +
                        "  substr(LastAncVisitDate, 1,2))  between " +
                        "  Date(substr('"+fromDate+"' ,  + 7) || '-' || substr('"+fromDate+"' ,4,2)  || '-'  " +
                        // "\t\t\t\t\t\t\t\n" +
                        //  "\t\t\t\t\n" +
                        "  || substr('"+fromDate+"' , 1,2)) and  Date(substr('"+toDate+"' , " +
                        "   7) || '-' || substr('"+toDate+"' ,4,2)  || '-' " +
                        "  || substr('"+toDate+"' , 1,2)) and regpregnantorMother = 1) "+
                        " OR "+

                        "  (Date(substr(LastPncVisitDate, " +
                        " 7) || '-' || substr(LastPncVisitDate ,4,2)  || '-'  " +
                        " || " +
                        "  substr(LastPncVisitDate, 1,2))  between " +
                        "  Date(substr('"+fromDate+"' ,  + 7) || '-' || substr('"+fromDate+"' ,4,2)  || '-'  " +
                        // "\t\t\t\t\t\t\t\n" +
                        //  "\t\t\t\t\n" +
                        "  || substr('"+fromDate+"' , 1,2)) and  Date(substr('"+toDate+"' , " +
                        "   7) || '-' || substr('"+toDate+"' ,4,2)  || '-' " +
                        "  || substr('"+toDate+"' , 1,2)) and regpregnantorMother = 2) ";






        String sql;



        sql = " Select distinct regWomanName, regRegistrationDate, " +
                "regStatusWhileRegistration, " +
                " regComplicatedpreg, regAmberOrRedColorCode," +
                " regrecommendedPlaceOfDelivery, tblregisteredwomen.UserId," +
                "tblregisteredwomen.WomanId, regVillage, regLMP, IsCompl, " +
                " LastAncVisitDate, LastPncVisitDate, IsReferred, regADDate, " +
                " regriskFactors, regPregnantorMother , regInDanger, DateDeactivated, regWomenImage, isLMPConfirmed" + //07Apr2021 Bindu add isLMPconfirmed
                " from tblregisteredwomen left " +
                "join tblchildinfo on tblregisteredwomen.WomanId = tblchildinfo.WomanId " +
                " left join tbldeliveryinfo on tblregisteredwomen.WomanId = tbldeliveryinfo.WomanId "+
                "  INNER JOIN tblfacilitydetails ON tblregisteredwomen.regVillage = " +
                "                     tblfacilitydetails.AutoId " +
                "  INNER JOIN tblVisitHeader  ON tblregisteredwomen.WomanId = tblVisitHeader.WomanId" +

                " where  "
                + "   tblregisteredwomen.WomanId IN (SELECT WomanId from tblVisitHeader)  and  tblregisteredwomen.WomanId NOT In (SELECT BeneficiaryID from tblCaseMgmt) "
                + strStatus +
                strvillageCode +

                strLMPConfirm + strFilter + strDateFilter +

                "  order by tblregisteredwomen.transId desc " ;
        //+ " LIMIT 10 OFFSET " + (limitStart); //25MAr2021 Bindu add lmp validation

        GenericRawResults<tblregisteredwomen> tbls =
                databaseHelper.gettblregRuntimeExceptionDao().

                        queryRaw(sql,
                                new RawRowMapper<tblregisteredwomen>() {
                                    @Override
                                    public tblregisteredwomen mapRow(String[] strings, String[] strings1) {
                                        tblregisteredwomen tblregisteredwomen = new tblregisteredwomen();
                                        tblregisteredwomen.setRegWomanName(strings1[0]);
                                        tblregisteredwomen.setRegRegistrationDate(strings1[1]);
                                        tblregisteredwomen.setRegStatusWhileRegistration(strings1[2]);
                                        tblregisteredwomen.setRegComplicatedpreg(Integer.parseInt(strings1[3]));
                                        tblregisteredwomen.setRegAmberOrRedColorCode(Integer.parseInt(strings1[4]));
                                        tblregisteredwomen.setRegrecommendedPlaceOfDelivery(strings1[5]);
                                        tblregisteredwomen.setUserId(strings1[6]);
                                        tblregisteredwomen.setWomanId(strings1[7]);
                                        tblregisteredwomen.setRegVillage(Integer.parseInt(strings1[8]));
                                        tblregisteredwomen.setRegLMP(strings1[9]);
                                        tblregisteredwomen.setIsCompl(strings1[10]);
                                        tblregisteredwomen.setLastAncVisitDate(strings1[11]);
                                        tblregisteredwomen.setLastPncVisitDate(strings1[12]);
                                        tblregisteredwomen.setIsReferred(strings1[13]);
                                        tblregisteredwomen.setRegADDate(strings1[14]);
                                        tblregisteredwomen.setRegriskFactors(strings1[15]);
                                        tblregisteredwomen.setRegPregnantorMother(Integer.parseInt(strings1[16]));

                                        tblregisteredwomen.setRegInDanger(Integer.parseInt(strings1[17]));
                                        tblregisteredwomen.setDateDeactivated(strings1[18]);
                                        tblregisteredwomen.setRegWomenImage(strings1[19]);
                                        tblregisteredwomen.setIsLMPConfirmed(Integer.parseInt(strings1[20])); //07Apr2021 Bindu set lmp confirmed
                                        return tblregisteredwomen;
                                    }
                                });

        List<tblregisteredwomen> regList = new ArrayList<>();

        regList.addAll(tbls.getResults());

        return regList.size();

    }

    public int getAdolFollowUpDueCount(int villageCode, String fromDate,
                                                   String toDate, String filter, int limitStart) throws SQLException {

        boolean includeVillageCode = villageCode!=0,
                dateFilter = fromDate!=null && toDate!=null && fromDate.trim().length()>0 &&
                        toDate.trim().length()>0,
                includeUserFilter = filter!=null && filter.trim().length()>0;
        String strvillageCode = "", strFilter = "";

        if (includeVillageCode) {
            strvillageCode = " and regAdolFacilities = " + villageCode;
        }

        String strDateFilter =  "";


        if(dateFilter)
            strDateFilter = " AND (Date(substr(Recordcreateddate,  7) || '-' || substr(Recordcreateddate ,4,2)  || '-'   ||   substr(Recordcreateddate, 1,2)) " +
                    " between   Date(substr('"+fromDate+"' ,  + 7) || '-' || substr('"+fromDate+"' ,4,2)  || '-'    || substr('"+fromDate+"' , 1,2)) " +
                    " and  Date(substr('"+toDate+"' ,    7) || '-' || substr('"+toDate+"' ,4,2)  || '-'   || substr('"+toDate+"' , 1,2))) ";



        if (includeUserFilter) {
            strFilter = " and regAdolName like '%" + filter + "%' or regAdolMobile like " +
                    "'%" + filter + "%' or regAdolAadharNo like '%" + filter + "%'";
        }

     /*   String sql = "Select AdolId,regAdolName, regAdolAge,regAdolGender,regAdolMobile, " +
                "regAdolGoingtoSchool,regAdolMenstrualPbm,regAdolHealthIssues,Recordcreateddate " +
                "from tblAdolReg where " +
                "length(regAdolMenstrualPbm)>0 OR AdolId IN (SELECT AdolId from " +
                "tblAdolVisitHeader where " +
                "length(adolvisHMenstrualProblem))  OR regAdolHealthIssues like '%Anemia%' and regAdolDeactivateDate=''";
*/

        String sql = "Select AdolId,regAdolName, regAdolAge,regAdolGender,regAdolMobile," +
                "   regAdolGoingtoSchool,regAdolMenstrualPbm,regAdolHealthIssues,Recordcreateddate " +
                "  from tblAdolReg where " +
                "   length(regAdolMenstrualPbm)>0 OR AdolId IN (SELECT AdolId from " +
                " tblAdolVisitHeader where " +
                "  length(adolvisHMenstrualProblem))  OR regAdolHealthIssues like '%Anemia%' " +
                " and regAdolDeactivateDate = ''   " + strvillageCode + strDateFilter + strFilter
                + " order by transId desc  LIMIT 10 OFFSET "+limitStart;


        GenericRawResults<tblAdolReg> adolList = databaseHelper.gettblAdolregRuntimeExceptionDao()
                .queryRaw(sql, new RawRowMapper<tblAdolReg>() {
                    @Override
                    public tblAdolReg mapRow(String[] columnNames, String[] resultColumns) throws SQLException {
                        tblAdolReg tblAdolReg = new tblAdolReg();
                        tblAdolReg.setAdolID(resultColumns[0]);
                        tblAdolReg.setRegAdolName(resultColumns[1]);
                        tblAdolReg.setRegAdolAge(resultColumns[2]);
                        tblAdolReg.setRegAdolGender(resultColumns[3]);
                        tblAdolReg.setRegAdolMobile(resultColumns[4]);
                        tblAdolReg.setRegAdolGoingtoSchool(resultColumns[5]);
                        tblAdolReg.setRegAdolMenstrualPbm(resultColumns[6]);
                        tblAdolReg.setRegAdolHealthIssues(resultColumns[7]);
                        tblAdolReg.setRecordCreatedDate(resultColumns[8]);
                        return tblAdolReg;
                    }
                });

        List<tblAdolReg> adolRegList  = new ArrayList<>();
        adolRegList.addAll(adolList.getResults());
        return adolRegList.size();
    }


}
