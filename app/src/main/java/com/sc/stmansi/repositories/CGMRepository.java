package com.sc.stmansi.repositories;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.tables.TblChildInfo;
import com.sc.stmansi.tables.TblFacilityDetails;
import com.sc.stmansi.tables.tblbmiforage;
import com.sc.stmansi.tables.tblchildgrowth;
import com.sc.stmansi.tables.tblheightforage;
import com.sc.stmansi.tables.tblmuacforage;
import com.sc.stmansi.tables.tblweightforage;


import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class CGMRepository {
    private DatabaseHelper databaseHelper;

    public CGMRepository(DatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }

    public int getLastChildGMVisit(String childId) throws Exception {
        Dao<tblchildgrowth, Integer> tblchildgrowthIntegerDao = databaseHelper.getChildGrowthExceptionDao();

        int lastVisitNo = 0;
        String strqry = "select MAX(chlGMVisitId) from tblchildgrowth where chlId = '" + childId + "'";

        GenericRawResults<String[]> rawResults = tblchildgrowthIntegerDao.queryRaw(strqry);

        List<String[]> results = rawResults.getResults();
        // This will select the first result (the first and maybe only row returned)
        String[] resultArray = results.get(0);
        //This will select the first field in the result which should be the ID
        if (resultArray[0] != null)
            lastVisitNo = Integer.parseInt(resultArray[0]) + 1;
        else
            lastVisitNo = lastVisitNo + 1;

        return lastVisitNo;
    }

    public List<tblchildgrowth> getChildGrowthData(String childid) throws SQLException{
        RuntimeExceptionDao<tblchildgrowth,Integer> tblchildgrowtMonitorDao=databaseHelper.getChildGrowthExceptionDao();
        return tblchildgrowtMonitorDao.queryBuilder()
                .orderBy("chlGMVisitId", false) //13May2021 Bindu change order
                .where().eq("chlId", childid).query();
    }

    public List<TblChildInfo> getChildData(String childId)
            throws SQLException {
        RuntimeExceptionDao<TblChildInfo, Integer> childDetail = databaseHelper.getChildInfoRuntimeExceptionDao();
        return childDetail.queryBuilder()
                .where().eq("chlID", childId)
                .query();
    }

    public List<tblweightforage> getweightforages() throws SQLException{
        RuntimeExceptionDao<tblweightforage, Integer> weightforages=databaseHelper.getTblweightforageDao();
        return weightforages.queryBuilder().query();
    }

    public  List<tblheightforage> getheightforages() throws SQLException {
        RuntimeExceptionDao<tblheightforage, Integer> heightforages=databaseHelper.getTblheightforageDao();
        return  heightforages.queryBuilder().query();
    }

    public List<tblbmiforage> getbmiforages() throws SQLException{
        RuntimeExceptionDao<tblbmiforage, Integer> bmiforages=databaseHelper.gettblbmiforageDao();
        return  bmiforages.queryBuilder().query();
    }

    public List<tblmuacforage> getheadcfforages() throws SQLException{
        RuntimeExceptionDao<tblmuacforage, Integer> headcfforages=databaseHelper.gettblmuacforageDao();
        return  headcfforages.queryBuilder().query();
    }

    public List<tblchildgrowth> getChildGrowthWeightHeight(String childid) throws SQLException{
        RuntimeExceptionDao<tblchildgrowth,Integer> tblchildgrowtMonitorDao=databaseHelper.getChildGrowthExceptionDao();
        return tblchildgrowtMonitorDao.queryBuilder()
                .selectColumns("chlGMAgeInMonths")
                .selectColumns("chlGMAge")
                .selectColumns("chlGMHeight")
                .selectColumns("chlGMWeight")
                .where()
                .eq("chlId", childid).query();
    }

    public String getLastChildLastHeight(String childId, int visitNum) throws Exception {
        Dao<tblchildgrowth, Integer> tblchildgrowthIntegerDao = databaseHelper.getChildGrowthExceptionDao();

        String lastVisitNo = null; String strqry="";
        if(visitNum != 1) { //17May2021 Bindu - get last visit height
            strqry = "select chlGMHeight from tblchildgrowth where chlId = '" + childId + "' and chlGMVisitId = " + (visitNum - 1);

            GenericRawResults<String[]> rawResults = tblchildgrowthIntegerDao.queryRaw(strqry);

            List<String[]> results = rawResults.getResults();
            // This will select the first result (the first and maybe only row returned)
            String[] resultArray = results.get(0);
            //This will select the first field in the result which should be the ID
            if (resultArray[0] != null)
                lastVisitNo = resultArray[0];
        }
        return lastVisitNo;
    }


    public List<tblweightforage> getWeightBasedOnAge(Integer age) throws SQLException{
        RuntimeExceptionDao<tblweightforage, Integer> weightforages=databaseHelper.getTblweightforageDao();
        return weightforages.queryBuilder().where()
                .eq("Xaxis", age).query();
    }

//    12May2021 Bindu - get all the weight for age data
    public Map<Integer, tblweightforage> getChildWeightforAgeMasterData() throws SQLException{
        Map<Integer, tblweightforage> chlWtforAge = new LinkedHashMap<>();
        RuntimeExceptionDao<tblweightforage, Integer> weightforagesDao= databaseHelper.getTblweightforageDao();
        String[] selectColumns = {"Xaxis", "cgmBoysWtMedian", "cgmBoysWtFirstdNegSD",
                "cgmBoysWtSeconddNegSD", "cgmGirlsWtMedian", "cgmGirlsWtFirstdNegSD",
                "cgmGirlsWtSeconddNegSD"};

        List<tblweightforage> tblweightforageList = weightforagesDao.queryBuilder().distinct()
                .selectColumns(selectColumns)
                .orderBy("Xaxis", true)
                .query();

        tblweightforage tblwt ;
        for (tblweightforage tbl : tblweightforageList) {
            tblwt = new tblweightforage();
            //tblwt.setXaxis(tbl.getXaxis());
            tblwt.setCgmBoysWtFirstdNegSD(tbl.getCgmBoysWtFirstdNegSD());
            tblwt.setCgmBoysWtMedian(tbl.getCgmBoysWtMedian());
            tblwt.setCgmBoysWtSeconddNegSD(tbl.getCgmBoysWtSeconddNegSD());
            tblwt.setCgmGirlsWtFirstdNegSD(tbl.getCgmGirlsWtFirstdNegSD());
            tblwt.setCgmGirlsWtMedian(tbl.getCgmGirlsWtMedian());
            tblwt.setCgmGirlsWtSeconddNegSD(tbl.getCgmGirlsWtSeconddNegSD());

            chlWtforAge.put(tbl.getXaxis(), tblwt);
        }
        return chlWtforAge;
    }
}
