package com.sc.stmansi.repositories;

import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.tables.TblInstitutionDetails;

import java.sql.SQLException;

public class InstitutionRepository {

	private DatabaseHelper databaseHelper;

	public InstitutionRepository(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	public TblInstitutionDetails getOneInstitution() throws SQLException {
		return this.databaseHelper.getInstDetailsRuntimeExceptionDao().queryBuilder().selectColumns("Databasename", "ipAddress").queryForFirst();
	}


	//Added New method 29-jan-21
	public TblInstitutionDetails getEvalStartEndDate() throws SQLException {
		return this.databaseHelper.getInstDetailsRuntimeExceptionDao().queryBuilder().selectColumns("IsEvalPeriod", "EvalStartDate", "EvalEndDate").queryForFirst();
	}

	public void deleteInstitutionDetailsAddNew(String ipAddress) throws SQLException {
		RuntimeExceptionDao<TblInstitutionDetails, Integer> instDetailsDAO = this.databaseHelper.getInstDetailsRuntimeExceptionDao();
		instDetailsDAO.deleteBuilder().delete();

		TblInstitutionDetails tblinstDetails = new TblInstitutionDetails();
		tblinstDetails.setIpAddress(ipAddress);
		instDetailsDAO.create(tblinstDetails);
	}

    public int updateInstIPandDB(String instIp ) {
		return this.databaseHelper.getInstDetailsRuntimeExceptionDao().updateRaw(" UPDATE tblinstitutiondetails SET ipAddress = '"+instIp+"'; ");
    }
}
