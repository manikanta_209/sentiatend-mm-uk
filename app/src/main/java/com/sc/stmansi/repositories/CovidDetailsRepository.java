package com.sc.stmansi.repositories;

import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.tables.tblCovidTestDetails;
import com.sc.stmansi.tables.tblCovidVaccineDetails;

import java.sql.SQLException;
import java.util.List;

public class CovidDetailsRepository {

	private DatabaseHelper databaseHelper;

	public CovidDetailsRepository(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	//get Covid health details
	public List<tblCovidTestDetails> getVisitCovidTestDetails(String visitno, String womanid) throws Exception {
		return databaseHelper.getTblCovidTestDao().queryBuilder().
				where().eq("WomenId", womanid).and().eq("VisitNum", visitno).query();

	}

	//get Covid vaccine details
	public List<tblCovidVaccineDetails> getVisitCovidVaccineDetails(String womanid) throws Exception {
		return databaseHelper.getTblCovidVaccineDao().queryBuilder().
				where().eq("BeneficiaryId", womanid).query(); //20May2021 Bindu

	}

	public static int updateCovidVaccine(String userId,
											String upSql, int transId,
											 DatabaseHelper newDb) throws SQLException {

		int update = 0;
		update = newDb.getTblCovidVaccineDao().updateRaw(upSql);


		TransactionHeaderRepository transactionHeaderRepository = new TransactionHeaderRepository(newDb);
		transactionHeaderRepository.updateNew(userId,
				transId, newDb.getTblCovidVaccineDao().getTableName(),
				upSql, null, null, newDb);

		return update;
	}

	public List<tblCovidVaccineDetails> getAdolVisitCovidVaccinDetails(String adolId) throws SQLException{
		return databaseHelper.getTblCovidVaccineDao().queryBuilder().
				where().eq("BeneficiaryId", adolId).and().eq("BeneficiaryType","Adol").query(); //21May2021 Ramesh
	}
	public List<tblCovidTestDetails> getAdolVisitCovidTestDetails(String visitno, String adolid) throws Exception {
		return databaseHelper.getTblCovidTestDao().queryBuilder().
				where().eq("WomenId", adolid).and().eq("VisitNum", visitno).and().eq("beneficiaryType","Adol").query();

	}

}
