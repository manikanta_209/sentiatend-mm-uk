package com.sc.stmansi.repositories;

import android.util.Log;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.dao.RawRowMapper;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.tables.TblCaseMgmt_ANCPNCHv;
import com.sc.stmansi.tables.tblCaseMgmt;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CaseMgmtRepository {
    private DatabaseHelper databaseHelper;

    private String[] columns = {
            "autoId",
            "UserId",
            "BeneficiaryType",
            "BeneficiaryID",
            "BeneficiaryParentID",
            "VisitType",
            "VisitNum",
            "VisitDate",
            "VisitNumbyMM",
            "VisitDatebyMM",
            "VisitBeneficiaryDangerSigns",
            "hcmBeneficiaryVisitFac",
            "hcmReasonForNoVisit",
            "hcmReasonOthers",
            "hcmActTakByUserType",
            "hcmActTakByUserName",
            "hcmActTakAtFacility",
            "hcmActTakFacilityName",
            "hcmActTakDate",
            "hcmActTakTime",
            "hcmActTakForCompl",
            "hcmActMedicationsPres",
            "hcmActTakStatus",
            "hcmActTakStatusDate",
            "hcmActTakStatusTime",
            "hcmActTakReferredToFacility",
            "hcmActTakReferredToFacilityName",
            "hcmActTakComments",
            "hcmAdviseGiven",
            "hcmBeneficiaryCondatVisit",
            "hcmActionToBeTakenClient",
            "hcmInputToNextLevel",
            "hcmIsANMInformedAbtCompl",
            "hcmANMAwareOfRef",
            "VisitReferredtoFacType",
            "VisitReferredtoFacName",
            "VisitReferralSlipNumber",
            "hcmCreatedByUserType",
            "transId",
            "RecordCreatedDate",
            "RecordUpdatedDate",
            "StatusAtVisit",
            "ChlNo",
            "VisitMode",
            "ServerCreatedDate",
            "ServerUpdatedDate"
    };

    public CaseMgmtRepository(DatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }

    public List<tblCaseMgmt> getAllCases(String adolId, String userId,String userType) throws Exception {
        return databaseHelper.getTblCaseMgmtDao().queryBuilder().
                selectColumns(columns).where().eq("BeneficiaryID", adolId).and().eq("hcmCreatedByUserType",userType).query();
    }

    public tblCaseMgmt getCaseDetails(String adolId, String visitCount ,String userType) throws Exception {
        List<tblCaseMgmt> queryBuilder = databaseHelper.getTblCaseMgmtDao().queryBuilder().
                selectColumns(columns).where().eq("BeneficiaryID", adolId).and().eq("VisitNum", visitCount).and().eq("hcmCreatedByUserType",userType).query();
        if (queryBuilder.size() != 0)
            return queryBuilder.get(0);
        else
            return null;
    }

    public tblCaseMgmt getCaseDetailsforCC(String adolId, String visitCount, String userId) throws Exception {
        List<tblCaseMgmt> queryBuilder = databaseHelper.getTblCaseMgmtDao().queryBuilder().
                selectColumns(columns).where().eq("BeneficiaryID", adolId).and().eq("VisitNum", visitCount).and().not().eq("UserId",userId).query();
        if (queryBuilder.size() != 0)
            return queryBuilder.get(0);
        else
            return null;
    }

    public tblCaseMgmt getLastVisitCount(String adolId, String userId,String userType) throws Exception {
        List<tblCaseMgmt> local = databaseHelper.getTblCaseMgmtDao().queryBuilder().
                selectColumns(columns).orderBy("transId", false).limit(Long.valueOf(1)).where().eq("BeneficiaryID", adolId).and().eq("hcmCreatedByUserType",userType).query();
        if (local.size()!=0){
            return local.get(0);
        }else {
            return null;
        }
    }
    public List<tblCaseMgmt> getChildHomeVisitHistory(String childId,String userType) throws Exception {
        return databaseHelper.getTblCaseMgmtDao().queryBuilder()
                .selectColumns(columns)
                .where()
                .eq("BeneficiaryID", childId).and().eq("hcmCreatedByUserType",userType).query();
    }

    //mani 24june2021
    public int getLastChildHomeVisit(String childId) throws Exception {
        Dao<tblCaseMgmt, Integer> tblCaseMgmtDao = databaseHelper.getTblCaseMgmtDao();

        int lastVisitNo = 0;
        String strqry = "select MAX(VisitNum) from tblCaseMgmt where BeneficiaryID = '" + childId + "'";

        GenericRawResults<String[]> rawResults = tblCaseMgmtDao.queryRaw(strqry);

        List<String[]> results = rawResults.getResults();
        // This will select the first result (the first and maybe only row returned)
        String[] resultArray = results.get(0);
        //This will select the first field in the result which should be the ID
        if (resultArray[0] != null)
            lastVisitNo = Integer.parseInt(resultArray[0]) + 1;
        else
            lastVisitNo = lastVisitNo + 1;

        return lastVisitNo;
    }

    public String getLastServerDate() throws Exception{
        List<tblCaseMgmt> local = databaseHelper.getTblCaseMgmtDao().queryBuilder().
                selectColumns(columns).orderBy("ServerCreatedDate", false).limit(Long.valueOf(1))
                .query();
        if (local.size()!=0){
            return local.get(0).getServerCreatedDate();
        }else {
            return "";
        }
    }

    public long getCaseRecordsCount(int villageCode, String filter, String BeneType , int i) {
        String sql ="";
        try{
            boolean includeVillageCode = villageCode!=0;
            if(BeneType.equals("Adol")){
                sql = "select distinct tblCaseMgmt.VisitNum,tblCaseMgmt.VisitMode,tblCaseMgmt.VisitType,tblCaseMgmt.VisitDate,tblCaseMgmt.BeneficiaryID,tblCaseMgmt.BeneficiaryType," +
                        "tblCaseMgmt.VisitBeneficiaryDangerSigns, tblCaseMgmt.VisitReferredtoFacType,tblCaseMgmt.hcmActTakStatus,tblCaseMgmt.VisitReferredtoFacName " +
                        " from tblCaseMgmt INNER JOIN tblAdolReg on tblAdolReg.AdolID = tblCaseMgmt.BeneficiaryID " +
                        "where tblAdolReg.regAdolName Like '%"+filter+"%' and tblCaseMgmt.RecordViewedDate is null ";
            }else if (BeneType.equals("Woman")){
                sql = "select distinct tblCaseMgmt.VisitNum,tblCaseMgmt.VisitMode,tblCaseMgmt.VisitType,tblCaseMgmt.VisitDate,tblCaseMgmt.BeneficiaryID," +
                        "tblCaseMgmt.BeneficiaryType,tblCaseMgmt.VisitBeneficiaryDangerSigns, tblCaseMgmt.VisitReferredtoFacType,tblCaseMgmt.hcmActTakStatus,tblCaseMgmt.VisitReferredtoFacName " +
                        "from tblCaseMgmt INNER JOIN tblregisteredwomen on tblregisteredwomen.WomanId = tblCaseMgmt.BeneficiaryID " +
                        "where tblregisteredwomen.regWomanName Like '%"+filter+"%' and tblCaseMgmt.RecordViewedDate is null ";
            }else if (BeneType.equals("Child")){
                sql = "select distinct tblCaseMgmt.VisitNum,tblCaseMgmt.VisitMode,tblCaseMgmt.VisitType,tblCaseMgmt.VisitDate,tblCaseMgmt.BeneficiaryID," +
                        "tblCaseMgmt.BeneficiaryType,tblCaseMgmt.VisitBeneficiaryDangerSigns, tblCaseMgmt.VisitReferredtoFacType,tblCaseMgmt.hcmActTakStatus,tblCaseMgmt.VisitReferredtoFacName " +
                        "from tblCaseMgmt INNER JOIN tblchildinfo on tblchildinfo.chlId = tblCaseMgmt.BeneficiaryID " +
                        "where tblchildinfo.chlChildname Like '%"+filter+"%' and tblCaseMgmt.RecordViewedDate is null ";
            }

            String wh = "";
            if (BeneType.length()>0){
                wh = wh + "and tblCaseMgmt.BeneficiaryType = '"+BeneType+"'";
            }

            if (includeVillageCode){
                if(BeneType.equals("Adol")){
                    wh = wh + " and tblAdolReg.regAdolFacilities = "+ villageCode;
                }else if (BeneType.equals("Woman")){
                    wh = wh + " and tblregisteredwomen.regVillage = "+ villageCode;
                }else if (BeneType.equals("Child")){
                    wh = wh + " and tblchildinfo.chlTribalHamlet = "+ villageCode;
                }
            }
            sql = sql + wh;

            GenericRawResults<tblCaseMgmt> caseList = databaseHelper.getTblCaseMgmtDao()
                    .queryRaw(sql, new RawRowMapper<tblCaseMgmt>() {
                        @Override
                        public tblCaseMgmt mapRow(String[] columnNames, String[] resultColumns) throws SQLException {
                            tblCaseMgmt tblCaseMgmt = new tblCaseMgmt();
                            tblCaseMgmt.setVisitNum(Integer.parseInt(resultColumns[0]));
                            tblCaseMgmt.setVisitMode(resultColumns[1]);
                            tblCaseMgmt.setVisitType(resultColumns[2]);
                            tblCaseMgmt.setVisitDate(resultColumns[3]);
                            tblCaseMgmt.setBeneficiaryID(resultColumns[4]);
                            tblCaseMgmt.setBeneficiaryType(resultColumns[5]);
                            tblCaseMgmt.setVisitBeneficiaryDangerSigns(resultColumns[6]);
                            tblCaseMgmt.setVisitReferredtoFacType(resultColumns[7]);
                            tblCaseMgmt.setHcmActTakStatus(resultColumns[8]);
                            tblCaseMgmt.setVisitReferredtoFacName(resultColumns[9]);
                            return tblCaseMgmt;
                        }
                    });

            return caseList.getResults().size();
        }catch (Exception e){
            e.printStackTrace();
            Log.e("Case Fetch",e.getMessage()+sql);
        }
        return 0;
    }

    public List<tblCaseMgmt> getCaseRecords(int villageCode, String filter, String BeneType , int limit) {
        String sql ="";
        try{
            boolean includeVillageCode = villageCode!=0;
            if(BeneType.equals("Adol")){
                sql = "select distinct tblCaseMgmt.VisitNum,tblCaseMgmt.VisitMode,tblCaseMgmt.VisitType,tblCaseMgmt.VisitDate,tblCaseMgmt.BeneficiaryID,tblCaseMgmt.BeneficiaryType," +
                        "tblCaseMgmt.VisitBeneficiaryDangerSigns, tblCaseMgmt.VisitReferredtoFacType,tblCaseMgmt.hcmActTakStatus," +
                        "tblCaseMgmt.VisitReferredtoFacName,tblCaseMgmt.MMUserId,tblCaseMgmt.StatusAtVisit,tblCaseMgmt.userId," +
                        "tblCaseMgmt.hcmCreatedByUserType,tblCaseMgmt.autoId,tblCaseMgmt.VisitMode,tblCaseMgmt.VisitNum,tblCaseMgmt.RecordUpdatedDate " +
                        " from tblCaseMgmt INNER JOIN tblAdolReg " +
                        "on tblAdolReg.AdolID = tblCaseMgmt.BeneficiaryID where tblAdolReg.regAdolName Like '%"+filter+"%' and tblCaseMgmt.RecordViewedDate is null ";
            }else if (BeneType.equals("Woman")){
                sql = "select distinct tblCaseMgmt.VisitNum,tblCaseMgmt.VisitMode,tblCaseMgmt.VisitType,tblCaseMgmt.VisitDate,tblCaseMgmt.BeneficiaryID," +
                        "tblCaseMgmt.BeneficiaryType,tblCaseMgmt.VisitBeneficiaryDangerSigns, tblCaseMgmt.VisitReferredtoFacType," +
                        "tblCaseMgmt.hcmActTakStatus,tblCaseMgmt.VisitReferredtoFacName,tblCaseMgmt.MMUserId,tblCaseMgmt.StatusAtVisit," +
                        "tblCaseMgmt.userId,tblCaseMgmt.hcmCreatedByUserType,tblCaseMgmt.autoId,tblCaseMgmt.VisitMode,tblCaseMgmt.VisitNum,tblCaseMgmt.RecordUpdatedDate " +
                        "from tblCaseMgmt INNER JOIN tblregisteredwomen on tblregisteredwomen.WomanId = tblCaseMgmt.BeneficiaryID " +
                        "where tblregisteredwomen.regWomanName Like '%"+filter+"%' and tblCaseMgmt.RecordViewedDate is null ";
            }else if (BeneType.equals("Child")){
                sql = "select distinct tblCaseMgmt.VisitNum,tblCaseMgmt.VisitMode,tblCaseMgmt.VisitType,tblCaseMgmt.VisitDate,tblCaseMgmt.BeneficiaryID," +
                        "tblCaseMgmt.BeneficiaryType,tblCaseMgmt.VisitBeneficiaryDangerSigns, tblCaseMgmt.VisitReferredtoFacType," +
                        "tblCaseMgmt.hcmActTakStatus,tblCaseMgmt.VisitReferredtoFacName,tblCaseMgmt.MMUserId,tblCaseMgmt.StatusAtVisit,tblCaseMgmt.userId, " +
                        "tblCaseMgmt.hcmCreatedByUserType,tblCaseMgmt.autoId,tblCaseMgmt.VisitMode,tblCaseMgmt.VisitNum,tblCaseMgmt.RecordUpdatedDate " +
                        "from tblCaseMgmt INNER JOIN tblchildinfo on tblchildinfo.chlId = tblCaseMgmt.BeneficiaryID " +
                        "where tblchildinfo.chlChildname Like '%"+filter+"%' and tblCaseMgmt.RecordViewedDate is null ";
            }

            String wh = "";
            if (BeneType.length()>0){
                wh = wh + "and tblCaseMgmt.BeneficiaryType = '"+BeneType+"'";
            }

            if (includeVillageCode){
                if(BeneType.equals("Adol")){
                   wh = wh + " and tblAdolReg.regAdolFacilities = "+ villageCode;
                }else if (BeneType.equals("Woman")){
                    wh = wh + " and tblregisteredwomen.regVillage = "+ villageCode;
                }else if (BeneType.equals("Child")){
                    wh = wh + " and tblchildinfo.chlTribalHamlet = "+ villageCode;
                }
            }
            sql = sql + wh;
            sql = sql + " order by tblCaseMgmt.transId asc LIMIT 10 OFFSET "+limit;


            GenericRawResults<tblCaseMgmt> caseList = databaseHelper.getTblCaseMgmtDao()
                    .queryRaw(sql, new RawRowMapper<tblCaseMgmt>() {
                        @Override
                        public tblCaseMgmt mapRow(String[] columnNames, String[] resultColumns) throws SQLException {
                            tblCaseMgmt tblCaseMgmt = new tblCaseMgmt();
                            tblCaseMgmt.setVisitNum(Integer.parseInt(resultColumns[0]));
                            tblCaseMgmt.setVisitMode(resultColumns[1]);
                            tblCaseMgmt.setVisitType(resultColumns[2]);
                            tblCaseMgmt.setVisitDate(resultColumns[3]);
                            tblCaseMgmt.setBeneficiaryID(resultColumns[4]);
                            tblCaseMgmt.setBeneficiaryType(resultColumns[5]);
                            tblCaseMgmt.setVisitBeneficiaryDangerSigns(resultColumns[6]);
                            tblCaseMgmt.setVisitReferredtoFacType(resultColumns[7]);
                            tblCaseMgmt.setHcmActTakStatus(resultColumns[8]);
                            tblCaseMgmt.setVisitReferredtoFacName(resultColumns[9]);
                            tblCaseMgmt.setMMUserId(resultColumns[10]);
                            tblCaseMgmt.setStatusAtVisit(resultColumns[11]);
                            tblCaseMgmt.setUserId(resultColumns[12]);
                            tblCaseMgmt.setHcmCreatedByUserType(resultColumns[13]);
                            tblCaseMgmt.setAutoId(Integer.parseInt(resultColumns[14]));
                            tblCaseMgmt.setVisitMode(resultColumns[15]);
                            tblCaseMgmt.setVisitNum(Integer.parseInt(resultColumns[16]));
                            tblCaseMgmt.setRecordUpdatedDate(resultColumns[17]);
                            return tblCaseMgmt;
                        }
                    });

            return caseList.getResults();
        }catch (Exception e){
            e.printStackTrace();
            Log.e("Case Fetch",e.getMessage()+sql);
        }
        return null;
    }

    //    04Jul2021 Bindu //24Jul2021 Bindu raw query
    public boolean getComplMgmtVisitDetailsWoman(String womanId, int visitNum) throws SQLException {
        boolean recordexists = false;
        /*if(databaseHelper.getTblCaseMgmtDao().queryBuilder()
                .where().eq("BeneficiaryID",womanId).or().eq("BeneficiaryParentID",womanId).and().eq("VisitNumbyMM", visitNum).and().eq("VisitMode" ,"ANC").or().eq("VisitMode" ,"PNC").countOf() > 0)
            recordexists = true;
        else
            recordexists = false;*/

        Dao<tblCaseMgmt, Integer> tblCaseMgmtDao = databaseHelper.getTblCaseMgmtDao();
        String strqry = "";
        strqry = "select distinct VisitNumbyMM from tblCaseMgmt where (BeneficiaryId='"+womanId+"' or BeneficiaryParentID='"+womanId+"') and visitmode in ('ANC','PNC')  and VisitNumbyMM = '"+visitNum+"'";

        GenericRawResults<String[]> rawResults = tblCaseMgmtDao.queryRaw(strqry);

        List<String[]> vv = rawResults.getResults();

        if(vv != null && vv.size() > 0) {
            recordexists = true;
        }

        return recordexists;
    }
    // Get MM HV case mgmt status - Bindu - 22Jul2021 Add ben id //24Jul2021 Bindu add visitmode
    public tblCaseMgmt getCaseMgmtDetailsWoman(String benfid, String visitnum, String userId) throws Exception {
        List<tblCaseMgmt> queryBuilder = databaseHelper.getTblCaseMgmtDao().queryBuilder().
                selectColumns(columns).where().eq("BeneficiaryID", benfid).or().eq("BeneficiaryParentID",benfid).and().eq("VisitNumbyMM", visitnum).query(); // Do not add user id here - cos case managed by CC user
        if (queryBuilder.size() != 0)
            return queryBuilder.get(0);
        else
            return null;
    }

    // Get MM HV case mgmt status - Bindu //24Jul2021 use raw query
    public tblCaseMgmt getCaseMgmtDetailsWomanNew(String benfid, String visitnum, String userId, String visHVisitDate) throws Exception {
        //24Jul2021 Bindu - Change to custom qry
        ArrayList<tblCaseMgmt> tblCaseMgmtList = null;
        tblCaseMgmtList = new ArrayList<>();

        Dao<tblCaseMgmt, Integer> tblCaseMgmtDao = databaseHelper.getTblCaseMgmtDao();
        String strqry = "";
        strqry = "select distinct VisitNumbyMM from tblCaseMgmt where (BeneficiaryId='"+benfid+"' or BeneficiaryParentID='"+benfid+"') and visitmode in ('ANC','PNC')  and VisitNumbyMM = '"+visitnum+"' and VisitDatebyMM='"+visHVisitDate+"'"; // Do not add user id here - cos case managed by CC user

        GenericRawResults<String[]> rawResults = tblCaseMgmtDao.queryRaw(strqry);

        List<String[]> vv = rawResults.getResults();

        tblCaseMgmt caseMgmt = new tblCaseMgmt();
        if(vv != null && vv.size() > 0) {
            String[] s = vv.get(0);
            caseMgmt.setVisitNumbyMM(Integer.parseInt(s[0]));
            tblCaseMgmtList.add(caseMgmt);
        }

        if(tblCaseMgmtList!=null && tblCaseMgmtList.size() > 0)
            return tblCaseMgmtList.get(0);
        else
            return null;

    }

    // Bindu get Woman HV details
    public List<tblCaseMgmt> getAllCasesWomanHV(String benId, String userId, String usertype) throws Exception {
        return databaseHelper.getTblCaseMgmtDao().queryBuilder().
                selectColumns(columns).orderBy("VisitNum", true).groupBy("transId").where().eq("BeneficiaryID", benId).or().eq("BeneficiaryParentID",benId).and().eq("hcmCreatedByUserType",usertype).query();
    }

    public ArrayList<TblCaseMgmt_ANCPNCHv> getCaseMgmtWomanANCPNCHV_CC(String benid, String userId, String usertype) throws Exception {
        ArrayList<TblCaseMgmt_ANCPNCHv> tblCaseMgmtList = null;
        tblCaseMgmtList = new ArrayList<>();

//        String strSql = "Select distinct transid from tblCaseMgmt where (beneficiaryID = '"+benid+"' or BeneficiaryParentID = '"+benid+"')  and VisitMode in ('ANC','PNC') and hcmCreatedByUserType = '"+usertype+"' order by transid asc";
        String strSql = "Select distinct transid from tblCaseMgmt where (beneficiaryID = '"+benid+"' or BeneficiaryParentID = '"+benid+"') and MMUserId = '"+userId+"' and VisitMode in ('ANC','PNC') and hcmCreatedByUserType = '"+usertype+"' order by transid asc";
        Dao<tblCaseMgmt, Integer> tblCaseMgmtDao = databaseHelper.getTblCaseMgmtDao();
        GenericRawResults<String[]> ss = tblCaseMgmtDao.queryRaw(strSql);
        List<String[]> vv = ss.getResults();
        if (vv != null && vv.size() > 0) {
            for (int i = 0; i < vv.size(); i++) {
                String[] strtransid = vv.get(i);
                String strSqlRec = "Select * from tblCaseMgmt where transid = '"+strtransid[0]+"'";
                GenericRawResults<String[]> ss1 = tblCaseMgmtDao.queryRaw(strSqlRec);
                List<String[]> vv1 = ss1.getResults();
                if(vv1 != null && vv1.size() > 0) {
                    TblCaseMgmt_ANCPNCHv caseMgmt = new TblCaseMgmt_ANCPNCHv();
                    int j=0;
                    do{
                        String[] str = vv1.get(j);

                        caseMgmt.setUserId(str[1]);
                        caseMgmt.setMMUserId(str[2]);
                        caseMgmt.setBeneficiaryType(str[3]);
                        caseMgmt.setChlNo(str[4]); //
                        if(str[4] != null && str[4].equalsIgnoreCase("1"))
                            caseMgmt.setChildDangersigns(str[14]);
                        else if(str[4] != null && str[4].equalsIgnoreCase("2"))
                            caseMgmt.setChildDangersignsSec(str[14]);
                        else if(str[4] != null && str[4].equalsIgnoreCase("3"))
                            caseMgmt.setChildDangersignsThird(str[14]);
                        else if(str[4] != null && str[4].equalsIgnoreCase("4"))
                            caseMgmt.setChildDangersignsFourth(str[14]);
                        else if(str[4] != null && str[4].equalsIgnoreCase("All"))
                            caseMgmt.setChildDangersignsOthers(str[14]);
                        else
                            caseMgmt.setVisitBeneficiaryDangerSigns(str[14]);

                        caseMgmt.setBeneficiaryID(str[5]);
                        caseMgmt.setBeneficiaryParentID(str[6]);
                        caseMgmt.setVisitMode(str[7]);
                        caseMgmt.setVisitType(str[8]);
                        caseMgmt.setVisitNum(Integer.parseInt(str[9]));
                        caseMgmt.setVisitDate(str[10]);
                        caseMgmt.setStatusAtVisit(str[11]);
                        caseMgmt.setVisitNumbyMM(Integer.parseInt(str[12]));
                        caseMgmt.setVisitDatebyMM(str[13]);
//                        caseMgmt.setVisitBeneficiaryDangerSigns(str[14]);
                        caseMgmt.setHcmBeneficiaryVisitFac(str[15]);
                        caseMgmt.setHcmReasonForNoVisit(str[16]);
                        caseMgmt.setHcmReasonOthers(str[17]);
                        caseMgmt.setHcmActTakByUserType(str[18]);
                        caseMgmt.setHcmActTakByUserName(str[19]);
                        caseMgmt.setHcmActTakAtFacility(str[20]);
                        caseMgmt.setHcmActTakFacilityName(str[21]);
                        caseMgmt.setHcmActTakDate(str[22]);
                        caseMgmt.setHcmActTakTime(str[23]);
                        caseMgmt.setHcmActTakForCompl(str[24]);
                        caseMgmt.setHcmActMedicationsPres(str[25]);
                        caseMgmt.setHcmActTakStatus(str[26]);
                        caseMgmt.setHcmActTakStatusDate(str[27]);
                        caseMgmt.setHcmActTakStatusTime(str[28]);
                        caseMgmt.setHcmActTakReferredToFacility(str[29]);
                        caseMgmt.setHcmActTakReferredToFacilityName(str[30]);
                        caseMgmt.setHcmActTakComments(str[31]);
                        caseMgmt.setHcmAdviseGiven(str[32]);
                        caseMgmt.setHcmBeneficiaryCondatVisit(str[33]);
                        caseMgmt.setHcmActionToBeTakenClient(str[34]);
                        caseMgmt.setHcmInputToNextLevel(str[35]);
                        caseMgmt.setHcmIsANMInformedAbtCompl(str[36]);
                        caseMgmt.setHcmANMAwareOfRef(str[37]);
                        caseMgmt.setVisitReferredtoFacType(str[38]);
                        caseMgmt.setVisitReferredtoFacName(str[39]);
                        caseMgmt.setVisitReferralSlipNumber(str[40]);

                        j++;
                    }while(j< vv1.size());
                    tblCaseMgmtList.add(caseMgmt);

                }

            }
        }
        return tblCaseMgmtList;
    }

    public int updateViewDate(String sql) throws Exception{
        return this.databaseHelper.getTblCaseMgmtDao().updateRaw(sql);
    }

    public List<tblCaseMgmt> getAllCasesForMC(String adolId, String userId) throws Exception {
        return databaseHelper.getTblCaseMgmtDao().queryBuilder().
//                selectColumns(columns).where().eq("BeneficiaryID", adolId).and().eq("UserId",userId).and().eq("hcmCreatedByUserType",userType).query();
        selectColumns(columns).where().eq("BeneficiaryID", adolId).and().eq("hcmCreatedByUserType","MC").query();
    }

    //Bindu Get Adol last visit count by user type
    public tblCaseMgmt getLastVisitCountbyUserType(String adolId, String userType) throws Exception {
        List<tblCaseMgmt> local = databaseHelper.getTblCaseMgmtDao().queryBuilder().
                selectColumns(columns).orderBy("transId", false).limit(Long.valueOf(1)).where().eq("BeneficiaryID", adolId).and().eq("hcmCreatedByUserType",userType).query();
        if (local.size()!=0){
            return local.get(0);
        }else {
            return null;
        }
    }

    //21Aug2021 Bindu - get adol case details by usertype
    public tblCaseMgmt getAdolCaseDetailsbyUserType(String adolId, String visitCount, String usertype) throws Exception {
        List<tblCaseMgmt> queryBuilder = databaseHelper.getTblCaseMgmtDao().queryBuilder().
                selectColumns(columns).where().eq("BeneficiaryID", adolId).and().eq("VisitNum", visitCount).and().eq("hcmCreatedByUserType",usertype).query();
        if (queryBuilder.size() != 0)
            return queryBuilder.get(0);
        else
            return null;
    }

    public ArrayList<TblCaseMgmt_ANCPNCHv> getCaseMgmtWomanANCPNCHV_MC(String benid, String usertype) throws Exception {
        ArrayList<TblCaseMgmt_ANCPNCHv> tblCaseMgmtList = null;
        tblCaseMgmtList = new ArrayList<>();

        String strSql = "Select distinct transid from tblCaseMgmt where (beneficiaryID = '"+benid+"' or BeneficiaryParentID = '"+benid+"') and hcmCreatedByUserType = '"+usertype+"' and VisitMode in ('ANC','PNC') order by transid asc";
        Dao<tblCaseMgmt, Integer> tblCaseMgmtDao = databaseHelper.getTblCaseMgmtDao();
        GenericRawResults<String[]> ss = tblCaseMgmtDao.queryRaw(strSql);
        List<String[]> vv = ss.getResults();
        if (vv != null && vv.size() > 0) {
            for (int i = 0; i < vv.size(); i++) {
                String[] strtransid = vv.get(i);
                String strSqlRec = "Select * from tblCaseMgmt where transid = '"+strtransid[0]+"'";
                GenericRawResults<String[]> ss1 = tblCaseMgmtDao.queryRaw(strSqlRec);
                List<String[]> vv1 = ss1.getResults();
                if(vv1 != null && vv1.size() > 0) {
                    TblCaseMgmt_ANCPNCHv caseMgmt = new TblCaseMgmt_ANCPNCHv();
                    int j=0;
                    do{
                        String[] str = vv1.get(j);

                        caseMgmt.setUserId(str[1]);
                        caseMgmt.setMMUserId(str[2]);
                        caseMgmt.setBeneficiaryType(str[3]);
                        caseMgmt.setChlNo(str[4]); //
                        if(str[4] != null && str[4].equalsIgnoreCase("1"))
                            caseMgmt.setChildDangersigns(str[14]);
                        else if(str[4] != null && str[4].equalsIgnoreCase("2"))
                            caseMgmt.setChildDangersignsSec(str[14]);
                        else if(str[4] != null && str[4].equalsIgnoreCase("3"))
                            caseMgmt.setChildDangersignsThird(str[14]);
                        else if(str[4] != null && str[4].equalsIgnoreCase("4"))
                            caseMgmt.setChildDangersignsFourth(str[14]);
                        else if(str[4] != null && str[4].equalsIgnoreCase("All"))
                            caseMgmt.setChildDangersignsOthers(str[14]);
                        else
                            caseMgmt.setVisitBeneficiaryDangerSigns(str[14]);

                        caseMgmt.setBeneficiaryID(str[5]);
                        caseMgmt.setBeneficiaryParentID(str[6]);
                        caseMgmt.setVisitMode(str[7]);
                        caseMgmt.setVisitType(str[8]);
                        caseMgmt.setVisitNum(Integer.parseInt(str[9]));
                        caseMgmt.setVisitDate(str[10]);
                        caseMgmt.setStatusAtVisit(str[11]);
                        caseMgmt.setVisitNumbyMM(Integer.parseInt(str[12]));
                        caseMgmt.setVisitDatebyMM(str[13]);
//                        caseMgmt.setVisitBeneficiaryDangerSigns(str[14]);
                        caseMgmt.setHcmBeneficiaryVisitFac(str[15]);
                        caseMgmt.setHcmReasonForNoVisit(str[16]);
                        caseMgmt.setHcmReasonOthers(str[17]);
                        caseMgmt.setHcmActTakByUserType(str[18]);
                        caseMgmt.setHcmActTakByUserName(str[19]);
                        caseMgmt.setHcmActTakAtFacility(str[20]);
                        caseMgmt.setHcmActTakFacilityName(str[21]);
                        caseMgmt.setHcmActTakDate(str[22]);
                        caseMgmt.setHcmActTakTime(str[23]);
                        caseMgmt.setHcmActTakForCompl(str[24]);
                        caseMgmt.setHcmActMedicationsPres(str[25]);
                        caseMgmt.setHcmActTakStatus(str[26]);
                        caseMgmt.setHcmActTakStatusDate(str[27]);
                        caseMgmt.setHcmActTakStatusTime(str[28]);
                        caseMgmt.setHcmActTakReferredToFacility(str[29]);
                        caseMgmt.setHcmActTakReferredToFacilityName(str[30]);
                        caseMgmt.setHcmActTakComments(str[31]);
                        caseMgmt.setHcmAdviseGiven(str[32]);
                        caseMgmt.setHcmBeneficiaryCondatVisit(str[33]);
                        caseMgmt.setHcmActionToBeTakenClient(str[34]);
                        caseMgmt.setHcmInputToNextLevel(str[35]);
                        caseMgmt.setHcmIsANMInformedAbtCompl(str[36]);
                        caseMgmt.setHcmANMAwareOfRef(str[37]);
                        caseMgmt.setVisitReferredtoFacType(str[38]);
                        caseMgmt.setVisitReferredtoFacName(str[39]);
                        caseMgmt.setVisitReferralSlipNumber(str[40]);

                        j++;
                    }while(j< vv1.size());
                    tblCaseMgmtList.add(caseMgmt);
                }
            }
        }
        return tblCaseMgmtList;
    }

    public int getAutoIDforCC() throws SQLException{
        List<tblCaseMgmt> list = this.databaseHelper.getTblCaseMgmtDao().queryBuilder().
                selectColumns(columns).where().eq("hcmCreatedByUserType", "CC").query();
        return list.size()+1;
    }

    //16SEp2021 Bindu
    // Get MM HV case mgmt status - Bindu //24Jul2021 use raw query
    public tblCaseMgmt getCaseMgmtDetailsWomanbyUserType(String benfid, String visitnum, String usertype, String visHVisitDate) throws Exception {
        //24Jul2021 Bindu - Change to custom qry
        ArrayList<tblCaseMgmt> tblCaseMgmtList = null;
        tblCaseMgmtList = new ArrayList<>();

        Dao<tblCaseMgmt, Integer> tblCaseMgmtDao = databaseHelper.getTblCaseMgmtDao();
        String strqry = "";
        strqry = "select distinct VisitNumbyMM from tblCaseMgmt where (BeneficiaryId='" + benfid + "' or BeneficiaryParentID='" + benfid + "') and visitmode in ('ANC','PNC')  and VisitNumbyMM = '" + visitnum + "' and hcmCreatedByUserType = '" + usertype + "' and VisitDatebyMM='" + visHVisitDate + "'"; //13Aug2021 Bindu  add visit date

        GenericRawResults<String[]> rawResults = tblCaseMgmtDao.queryRaw(strqry);

        List<String[]> vv = rawResults.getResults();

        tblCaseMgmt caseMgmt = new tblCaseMgmt();
        if (vv != null && vv.size() > 0) {
            String[] s = vv.get(0);
            caseMgmt.setVisitNumbyMM(Integer.parseInt(s[0]));
            tblCaseMgmtList.add(caseMgmt);
        }

        if (tblCaseMgmtList != null && tblCaseMgmtList.size() > 0)
            return tblCaseMgmtList.get(0);
        else
            return null;

    }



    // Get MM HV case mgmt status - Mani 28sep2021
    public tblCaseMgmt getCaseMgmtDetailsChildNew(String benfid, String visitnum, String userId, String visHVisitDate) throws Exception {
        //24Jul2021 Bindu - Change to custom qry
        ArrayList<tblCaseMgmt> tblCaseMgmtList = null;
        tblCaseMgmtList = new ArrayList<>();

        Dao<tblCaseMgmt, Integer> tblCaseMgmtDao = databaseHelper.getTblCaseMgmtDao();
        String strqry = "";
        strqry = "select distinct VisitNumbyMM from tblCaseMgmt where (BeneficiaryId='"+benfid+"' or BeneficiaryParentID='"+benfid+"') and VisitNumbyMM = '"+visitnum+"' and UserId = '"+userId+"' and VisitDatebyMM='"+visHVisitDate+"'";

        GenericRawResults<String[]> rawResults = tblCaseMgmtDao.queryRaw(strqry);

        List<String[]> vv = rawResults.getResults();

        tblCaseMgmt caseMgmt = new tblCaseMgmt();
        if(vv != null && vv.size() > 0) {
            String[] s = vv.get(0);
            caseMgmt.setVisitNumbyMM(Integer.parseInt(s[0]));
            tblCaseMgmtList.add(caseMgmt);
        }

        if(tblCaseMgmtList!=null && tblCaseMgmtList.size() > 0)
            return tblCaseMgmtList.get(0);
        else
            return null;

    }

    // Get MM HV case mgmt status - Mani 28sep2021
    public tblCaseMgmt getCaseMgmtDetailsChildNewCC(String benfid, String visitnum, String visHVisitDate, String usertype) throws Exception {
        //24Jul2021 Bindu - Change to custom qry
        ArrayList<tblCaseMgmt> tblCaseMgmtList = null;
        tblCaseMgmtList = new ArrayList<>();

        Dao<tblCaseMgmt, Integer> tblCaseMgmtDao = databaseHelper.getTblCaseMgmtDao();
        String strqry = "";
        strqry = "select distinct VisitNumbyMM from tblCaseMgmt where (BeneficiaryId='"+benfid+"' or BeneficiaryParentID='"+benfid+"') and VisitNumbyMM = '"+visitnum+"' and hcmCreatedByUserType = '"+usertype+"' and VisitDatebyMM='"+visHVisitDate+"'";

        GenericRawResults<String[]> rawResults = tblCaseMgmtDao.queryRaw(strqry);

        List<String[]> vv = rawResults.getResults();

        tblCaseMgmt caseMgmt = new tblCaseMgmt();
        if(vv != null && vv.size() > 0) {
            String[] s = vv.get(0);
            caseMgmt.setVisitNumbyMM(Integer.parseInt(s[0]));
            tblCaseMgmtList.add(caseMgmt);
        }

        if(tblCaseMgmtList!=null && tblCaseMgmtList.size() > 0)
            return tblCaseMgmtList.get(0);
        else
            return null;

    }
}
