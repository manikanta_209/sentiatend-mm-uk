package com.sc.stmansi.repositories;

import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.tables.tblAdolVisitDetails;

import java.util.List;

public class AdolVisitDetailsRepository {
    private DatabaseHelper databaseHelper;
    String[] columns = {
            "UserId", "AdolId", "VisitId", "adolvisDVisitDate", "adolvisDFollowUps", "transId", "RecordCreatedDate", "RecordUpdatedDate"
    };

    public AdolVisitDetailsRepository(DatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }

    public List<tblAdolVisitDetails> getFollowUps(String visitCount, String adolId) throws Exception {
        List<tblAdolVisitDetails> queryBuilder = databaseHelper.gettblAdolVisitDetailsRuntimeExceptionDao().queryBuilder().
                selectColumns(columns).where().eq("AdolId", adolId).and().eq("VisitId", visitCount).query();
        return queryBuilder;
    }
}
