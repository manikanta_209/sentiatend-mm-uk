package com.sc.stmansi.repositories;

import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.tables.TblDeliveryInfo;

import java.sql.SQLException;
import java.util.List;

public class DeliveryRepository {

    private DatabaseHelper databaseHelper;

    public DeliveryRepository(DatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }


    public List<TblDeliveryInfo> getDeliveryData(String womanId, String userId) throws SQLException {
        RuntimeExceptionDao<TblDeliveryInfo, Integer> deliveryDetailsDAO = databaseHelper.getDelInfoRuntimeExceptionDao();
       return deliveryDetailsDAO.queryBuilder().where().eq("WomanId",womanId).and().eq("UserId",userId).query();

    }

    public int updateDelData(String userId, String sql,int transId, DatabaseHelper dbhelper){

        int update =0 ;
        update =  dbhelper.gettblregRuntimeExceptionDao().updateRaw(sql);


        TransactionHeaderRepository transactionHeaderRepository = new TransactionHeaderRepository(dbhelper);
        transactionHeaderRepository.updateNew(userId, transId,
                dbhelper.gettblregRuntimeExceptionDao().getTableName(),
                sql, null, null,dbhelper);

        return update;
    }


//    19Dec2019 Arpitha
    public boolean checkIsMotherAlive(String womanId) throws SQLException {

        boolean motherAlive = false;
        List<TblDeliveryInfo> childInfoList =    databaseHelper.getDelInfoRuntimeExceptionDao().queryBuilder()
                .selectColumns("delMothersDeath")
                .where()
                .eq("WomanId",womanId).query();

        for(int i = 0; i<childInfoList.size();i++)
        {
            if(childInfoList.get(i).getDelMothersDeath().equalsIgnoreCase("Y"))
                motherAlive = true;
        }
        return  motherAlive;

    }

}
