package com.sc.stmansi.repositories;

import com.j256.ormlite.dao.GenericRawResults;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.tables.TblContactDetails;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SettingsRepository {

    private DatabaseHelper databaseHelper;

    public SettingsRepository(DatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }

    public List<TblContactDetails> getContactDetails(String userId) throws SQLException {
        String s = databaseHelper.getContactDetailsDao().queryBuilder().where().eq("UserId",userId)
                .and().ne("contactDeleted",1).prepare().toString();
       return databaseHelper.getContactDetailsDao().queryBuilder().where().eq("UserId",userId)
               .and().ne("contactDeleted",1).query();
    }

    public int updateSettingsData(String userId, String upSql, DatabaseHelper databaseHelper, int transId) {

        int update = 0;
        update = databaseHelper.gettblregRuntimeExceptionDao().updateRaw(upSql);


        TransactionHeaderRepository transactionHeaderRepository = new TransactionHeaderRepository(databaseHelper);
        transactionHeaderRepository.updateNew(userId, transId, databaseHelper.gettblregRuntimeExceptionDao().getTableName(),
                upSql, null,null, databaseHelper);

        return update;
    }

    public List<TblContactDetails> getphonenumber() throws SQLException {

        return databaseHelper.getContactDetailsDao().queryBuilder()
                .where().ne("contactDeleted",1)
                .query();
    }
//    12May2021 Arpitha
public List<TblContactDetails> getSpecificPhoneNumber(String screen) throws SQLException {

    return databaseHelper.getContactDetailsDao().queryBuilder()
            .where().ne("contactDeleted",1)
            .and().eq("contactselectedoption",screen)
            .query();
}

//13May2021 Arpitha
public List<String[]> getContactDetailsCount(String userId) throws SQLException {
        String sql = "Select contactselectedoption, count(*) from tblcontactdetails where userId='"+userId+"'  group by " +
                "contactselectedoption";
    GenericRawResults<String[]> rawResults = databaseHelper.getContactDetailsDao().queryRaw(sql);

    List<String[]> results = new ArrayList<>();

    if(rawResults!=null)
     results = rawResults.getResults();

    return results;

}
}
