package com.sc.stmansi.ImageStore;

import android.graphics.Bitmap;

import java.io.Serializable;

public class ImagePojo implements Serializable {

    private byte[] Image;

    private String ImagePath;

    private Bitmap ImageBitmap;

    public Bitmap getImageBitmap() {
        return ImageBitmap;
    }

    public void setImageBitmap(Bitmap imageBitmap) {
        ImageBitmap = imageBitmap;
    }

    public byte[] getImage() {
        return Image;
    }

    public void setImage(byte[] image) {
        Image = image;
    }

    public String getImagePath() {
        return ImagePath;
    }

    public void setImagePath(String imagePath) {
        ImagePath = imagePath;
    }
}
