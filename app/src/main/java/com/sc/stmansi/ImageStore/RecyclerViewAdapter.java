package com.sc.stmansi.ImageStore;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.androidquery.AQuery;
import com.github.chrisbanes.photoview.PhotoView;
import com.github.chrisbanes.photoview.PhotoViewAttacher;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.sc.stmansi.R;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.tables.tblImageStore;

import java.io.File;
import java.util.ArrayList;
import java.util.Objects;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {

    private Activity act;
    private AQuery aq;
    private ArrayList<ImagePojo> imagePojo = new ArrayList<>();
    private ArrayList<tblImageStore> array = new ArrayList<>();
    private DatabaseHelper databaseHelper;

    public RecyclerViewAdapter(Activity act, ArrayList<tblImageStore> imagesArray, DatabaseHelper databaseHelper, ArrayList<ImagePojo> allImagePojo, AQuery aq) {
        this.act = act;
        array = imagesArray;
        this.databaseHelper = databaseHelper;
        imagePojo = allImagePojo;
        this.aq = aq;
    }

    public RecyclerViewAdapter(Activity homeVisitViewActivity, ArrayList<tblImageStore> allImages, DatabaseHelper databaseHelper) {
        act = homeVisitViewActivity;
        array = allImages;
        this.databaseHelper = databaseHelper;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        try {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.gallery_singleimage_view, parent, false);
            return new MyViewHolder(view);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        try{
            if (imagePojo!=null && imagePojo.size() != 0) {
                Bitmap bm = null;
                ImageLoader.getInstance().init(UniversalLoader.initImageLoader(act).build());
                for (ImagePojo s : imagePojo) {
                    String imagepath = AppState.imgDirRef +"/"+ array.get(position).getImageName() + ".jpeg";
                    if (imagepath.equals(s.getImagePath())) {
                        bm = s.getImageBitmap();
                        break;
                    }
                }
                holder.imageView.setImageBitmap(bm);
//        ImageLoader.getInstance().displayImage(String.valueOf(uri), holder.imageView);

                Bitmap finalBm = bm;
                holder.imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        show(holder.imageView, finalBm);
                    }
                });
                holder.imageView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(act);
                        builder.setMessage(act.getResources().getString(R.string.m127));
                        builder.setNegativeButton(act.getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                deleteImage(position);
                            }
                        }).setPositiveButton(act.getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).create().show();
                        return false;
                    }
                });
            } else {
                String imagepath = AppState.imgDirRef +"/"+ array.get(position).getImageName() + ".jpeg";
                final Uri uri = Uri.fromFile(new File(imagepath));
                ImageLoader.getInstance().init(UniversalLoader.initImageLoader(act).build());
                ImageLoader.getInstance().displayImage(String.valueOf(uri), holder.imageView);
                holder.imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        show(holder.imageView, uri);
                    }
                });
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void deleteImage(int pos) {
        try {
            array.remove(pos);
            if (array.size() == 0) {
                aq.id(R.id.lladdphotos).gone();
            } else {
                aq.id(R.id.lladdphotos).visible();
            }
            notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return array.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.img);
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    public void show(ImageView source, Bitmap bm) {
        try {
            BitmapDrawable background = ImagePreviewerUtils.getBlurredScreenDrawable(act, source.getRootView());

            @SuppressLint("InflateParams") View dialogView = LayoutInflater.from(act).inflate(R.layout.view_image_previewer, null);
            PhotoView imageView = dialogView.findViewById(R.id.previewer_image);
            ImageView imageViewCancel = dialogView.findViewById(R.id.imgCancel);
            imageView.setImageBitmap(bm);
            PhotoViewAttacher photoViewAttacher = new PhotoViewAttacher(imageView);
            photoViewAttacher.update();

            final Dialog dialog = new Dialog(act, R.style.ImagePreviewerTheme);
            dialog.getWindow().getAttributes().windowAnimations = R.style.ImagePreviewerTheme;
            Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(background);
            dialog.setContentView(dialogView);
            dialog.show();
            imageViewCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public void show(ImageView source, Uri uri) {
        try {
            BitmapDrawable background = ImagePreviewerUtils.getBlurredScreenDrawable(act, source.getRootView());

            @SuppressLint("InflateParams") View dialogView = LayoutInflater.from(act).inflate(R.layout.view_image_previewer, null);
            PhotoView imageView = dialogView.findViewById(R.id.previewer_image);
            ImageView imageViewCancel = dialogView.findViewById(R.id.imgCancel);
            imageView.setImageURI(uri);
            PhotoViewAttacher photoViewAttacher = new PhotoViewAttacher(imageView);
            photoViewAttacher.update();

            final Dialog dialog = new Dialog(act, R.style.ImagePreviewerTheme);
            dialog.getWindow().getAttributes().windowAnimations = R.style.ImagePreviewerTheme;
            Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(background);
            dialog.setContentView(dialogView);
            dialog.show();
            imageViewCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
