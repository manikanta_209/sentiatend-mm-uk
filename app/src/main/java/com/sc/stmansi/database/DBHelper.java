package com.sc.stmansi.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.tables.AuditPojo;
import com.sc.stmansi.tables.TblBadgeCount;
import com.sc.stmansi.tables.TblFacilityDetails;
import com.sc.stmansi.tables.TblInstitutionDetails;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.TblLoginAudit;
import com.sc.stmansi.tables.TblSyncMaster;
import com.sc.stmansi.tables.TblTransHeaderDS;
import com.sc.stmansi.tables.tblbankdetails;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.tables.tblserviceslist;
import com.sc.stmansi.tables.tblservicestypemaster;
import com.sc.stmansi.tables.tbltransHeader;
import com.sc.stmansi.tables.tbltransdetail;

import java.sql.SQLException;

public class DBHelper extends OrmLiteSqliteOpenHelper {

	private static final String DATABASE_NAME = AppState.dbDirRef + "/stenddb.db";
	private static final int DATABASE_VERSION = 7;

	private RuntimeExceptionDao<tblregisteredwomen, Integer> tblregRuntimeDAO;
	private RuntimeExceptionDao<tbltransHeader, Integer> transheaderDAO;
	private RuntimeExceptionDao<tbltransdetail, Integer> transdetailDAO;
	private RuntimeExceptionDao<TblInstusers, Integer> usersDAO;
	private RuntimeExceptionDao<tblserviceslist, Integer> servicesDAO;
	private RuntimeExceptionDao<tblservicestypemaster, Integer> servicestypeDAO;
	private RuntimeExceptionDao<TblFacilityDetails, Integer> villagesDAO;
	private RuntimeExceptionDao<tblbankdetails, Integer> bankdetailsDAO;
	private RuntimeExceptionDao<AuditPojo, Integer> auditsDAO;
	private RuntimeExceptionDao<TblBadgeCount, Integer> badgeDAO;
	private RuntimeExceptionDao<TblSyncMaster, Integer> syncMasterDAO;
	private RuntimeExceptionDao<TblTransHeaderDS, Integer> transheaderDSDAO;
	private RuntimeExceptionDao<TblInstitutionDetails, Integer> instDeatilsSDAO;
	private RuntimeExceptionDao<TblLoginAudit, Integer> loginAuditDAO;

	//    object creation
	public DBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
		try {
			SQLiteDatabase db = sqLiteDatabase;
		} catch (Exception e) {
			
			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}

	}

	@Override
	public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int i, int i2) {
		try {
			TableUtils.dropTable(connectionSource, tblregisteredwomen.class, true);
			onCreate(sqLiteDatabase, connectionSource);
		} catch (SQLException e) {
			

			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}

	}

	//    tblreg table DAO object creation
	public RuntimeExceptionDao<tblregisteredwomen, Integer> gettblregRuntimeExceptionDao() {
		try {


			if (tblregRuntimeDAO == null) {
				tblregRuntimeDAO = getRuntimeExceptionDao(tblregisteredwomen.class);
			}
		} catch (Exception e) {
			

			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}
		return tblregRuntimeDAO;

	}

	//    transheader table DAO object creation
	public RuntimeExceptionDao<tbltransHeader, Integer> getTransHeaderRuntimeExceptionDao() {
		try {


			if (transheaderDAO == null) {
				transheaderDAO = getRuntimeExceptionDao(tbltransHeader.class);
			}
		} catch (Exception e) {
			

			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}
		return transheaderDAO;

	}

	//    transdetails table DAO object creation
	public RuntimeExceptionDao<tbltransdetail, Integer> getTransDetailsRuntimeExceptionDao() {
		try {


			if (transdetailDAO == null) {
				transdetailDAO = getRuntimeExceptionDao(tbltransdetail.class);
			}
		} catch (Exception e) {
			

			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}
		return transdetailDAO;

	}

	//    tblusers table DAO object creation
	public RuntimeExceptionDao<TblInstusers, Integer> getUsersRuntimeExceptionDao() {
		try {


			if (usersDAO == null) {
				usersDAO = getRuntimeExceptionDao(TblInstusers.class);
			}
		} catch (Exception e) {
			

			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}
		return usersDAO;

	}

	//    tblserviceslist table DAO object creation
	public RuntimeExceptionDao<tblserviceslist, Integer> getServicesRuntimeExceptionDao() {
		try {


			if (servicesDAO == null) {
				servicesDAO = getRuntimeExceptionDao(tblserviceslist.class);
			}
		} catch (Exception e) {
			

			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}
		return servicesDAO;

	}

	//    Servicetype table DAO object creation
	public RuntimeExceptionDao<tblservicestypemaster, Integer> getServicesTypeRuntimeExceptionDao() {
		try {


			if (servicestypeDAO == null) {
				servicestypeDAO = getRuntimeExceptionDao(tblservicestypemaster.class);
			}
		} catch (Exception e) {
			

			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}
		return servicestypeDAO;

	}

	//    villages table DAO object creation
	public RuntimeExceptionDao<TblFacilityDetails, Integer> getVillageRuntimeExceptionDao() {
		try {


			if (villagesDAO == null) {
				villagesDAO = getRuntimeExceptionDao(TblFacilityDetails.class);
			}
		} catch (Exception e) {
			

			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}
		return villagesDAO;

	}


	//    bank details table DAO object creation
	public RuntimeExceptionDao<tblbankdetails, Integer> getBankDetailsRuntimeExceptionDao() {
		try {


			if (bankdetailsDAO == null) {
				bankdetailsDAO = getRuntimeExceptionDao(tblbankdetails.class);
			}
		} catch (Exception e) {
			

			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}
		return bankdetailsDAO;

	}


	//    audit trail table DAO object creation
	public RuntimeExceptionDao<AuditPojo, Integer> getAuditRuntimeExceptionDao() {
		try {


			if (auditsDAO == null) {
				auditsDAO = getRuntimeExceptionDao(AuditPojo.class);
			}
		} catch (Exception e) {
			

			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}
		return auditsDAO;

	}

	// get badge count object
	public RuntimeExceptionDao<TblBadgeCount, Integer> getBadgeRuntimeExceptionDao() {
		try {


			if (badgeDAO == null) {
				badgeDAO = getRuntimeExceptionDao(TblBadgeCount.class);
			}
		} catch (Exception e) {
			

			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}
		return badgeDAO;

	}

	// get sync masterobject
	public RuntimeExceptionDao<TblSyncMaster, Integer> getSyncMasterRuntimeExceptionDao() {
		try {


			if (syncMasterDAO == null) {
				syncMasterDAO = getRuntimeExceptionDao(TblSyncMaster.class);
			}
		} catch (Exception e) {
			

			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}
		return syncMasterDAO;

	}

	// get sync masterobject
	public RuntimeExceptionDao<TblTransHeaderDS, Integer> getTransHeaderDSRuntimeExceptionDao() {
		try {


			if (transheaderDSDAO == null) {
				transheaderDSDAO = getRuntimeExceptionDao(TblTransHeaderDS.class);
			}
		} catch (Exception e) {
			

			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}
		return transheaderDSDAO;

	}


	// get tb inst details object
	public RuntimeExceptionDao<TblInstitutionDetails, Integer> getInstDetailsRuntimeExceptionDao() {
		try {


			if (instDeatilsSDAO == null) {
				instDeatilsSDAO = getRuntimeExceptionDao(TblInstitutionDetails.class);
			}
		} catch (Exception e) {
			

			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}
		return instDeatilsSDAO;

	}


	// login audit object
	public RuntimeExceptionDao<TblLoginAudit, Integer>
	getLoginAuditRuntimeExceptionDao() {
		try {


			if (loginAuditDAO == null) {
				loginAuditDAO = getRuntimeExceptionDao(TblLoginAudit.class);
			}
		} catch (Exception e) {
			

			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
		}
		return loginAuditDAO;

	}

	/*Must do this, found it here -
		https://github.com/j256/ormlite-examples/blob/master/android/HelloAndroid/src/com/example/helloandroid/DatabaseHelper.java
	*/
	@Override
	public void close() {
		super.close();
		tblregRuntimeDAO = null;
		transheaderDAO = null;
		transdetailDAO = null;
		usersDAO = null;
		servicesDAO = null;
		servicestypeDAO = null;
		villagesDAO = null;
		bankdetailsDAO = null;
		auditsDAO = null;
		badgeDAO = null;
		syncMasterDAO = null;
		transheaderDSDAO = null;
		instDeatilsSDAO = null;
		loginAuditDAO = null;
	}
}
