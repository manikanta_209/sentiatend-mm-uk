package com.sc.stmansi.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.dao.RawRowMapper;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.configuration.SyncState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.sync.PrepareSyncXML;
import com.sc.stmansi.tables.ServiceslistPojo;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.TblSyncMaster;
import com.sc.stmansi.tables.TblTransHeaderDS;
import com.sc.stmansi.tables.TblVisitDetails;
import com.sc.stmansi.tables.TblVisitHeader;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.tables.tbltransHeader;
import com.sc.stmansi.tables.tbltransdetail;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DBMethods {

    private RuntimeExceptionDao<tbltransHeader, Integer> transheaderDAO;
    private RuntimeExceptionDao<TblInstusers, Integer> usersDAO;
    private RuntimeExceptionDao<tbltransdetail, Integer> transdetailDAO;
    private RuntimeExceptionDao<TblSyncMaster, Integer> syncMasterDAO;
    private RuntimeExceptionDao<TblTransHeaderDS, Integer> transHeaderDSDAO;

    private AppState appState;
    private SyncState syncState;
    private DatabaseHelper databaseHelper;
    private TblInstusers user;

    //11Sep2019 Bindu - create DAo for tblVisitHeader and tblVisitDetails
    private static Dao<TblVisitHeader, Integer> tblVisitHeaderDao = null;
    private static Dao<TblVisitDetails, Integer> tblVisitDetailDao = null;

    public DBMethods(DatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }

    public DBMethods(Context context, TblInstusers user, AppState appState, SyncState syncState) {
        this.databaseHelper = new DatabaseHelper(context);
        this.user = user;
        this.appState = appState;
        this.syncState = syncState;
    }

    // get A value from users Table
    public int getIntUsersValue(String UserId, String ColumnName) throws SQLException {
        usersDAO = databaseHelper.getUsersRuntimeExceptionDao();
        UserRepository userRepository = new UserRepository(databaseHelper);
        TblInstusers user = userRepository.getOneAuditedUser(appState.ashaId);
        int returnval = 0;
        List<TblInstusers> numRows = usersDAO.queryForAll();
        if (numRows != null && numRows.size() > 0) {
            if (ColumnName.equalsIgnoreCase("LastTransNumber"))
                returnval = user.getLastTransNumber();
            else if (ColumnName.equalsIgnoreCase("LastWomennumber"))
                returnval = user.getLastWomennumber();
            else if (ColumnName.equalsIgnoreCase("LastRequestNumber"))
                returnval = user.getLastRequestNumber();
        }
        return returnval;
    }

    public void UpdateDSTransStatus(String requestId, String Status) throws SQLException {
        syncMasterDAO = databaseHelper.getSyncMasterRuntimeExceptionDao();
        UpdateBuilder<TblSyncMaster, Integer> updateSyncMas
                = syncMasterDAO.updateBuilder();
        updateSyncMas.updateColumnValue
                ("RequestStatus", Status).where()
                .eq("RequestId", requestId);
        updateSyncMas.update();

        // If Request is rolled back in Server, change the status to 'N' in
        // tblTransHeader
        if (Status.equals("R"))
            Status = "N";


        transHeaderDSDAO = databaseHelper.getTransHeaderDSRuntimeExceptionDao();
        UpdateBuilder<TblTransHeaderDS, Integer> updateTransHeader
                = transHeaderDSDAO.updateBuilder();
        updateTransHeader.updateColumnValue
                ("TransHeaderDS", Status).where()
                .eq("ReqId", requestId);
        updateTransHeader.update();
    }


    public String callResponseXML() {
        String returnValue = null;

        try {
            PrepareSyncXML xExportXML;
            xExportXML = new PrepareSyncXML(databaseHelper, syncState, user);
            returnValue = xExportXML.WriteResponseXML();
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        return returnValue;
    }

    public String callXMLExport(String androidId, String userId) {
        String returnValue = null;
        try {
            PrepareSyncXML xExportXML;

            xExportXML = new PrepareSyncXML(this);
            returnValue = xExportXML.WriteSyncXML(androidId, userId);
            xExportXML.writeTxtFile(returnValue);
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        return returnValue;
    }

    // This method is related to syncronisation
    public void UpdateTransStatus(String RequestId, String Status) throws Exception {
        syncMasterDAO = databaseHelper.getSyncMasterRuntimeExceptionDao();//Arpitha 09Sep2019
        transheaderDAO = databaseHelper.getTransHeaderRuntimeExceptionDao();//Arpitha 09Sep2019
        UpdateBuilder<TblSyncMaster, Integer> updateBuilder = syncMasterDAO.updateBuilder();
        updateBuilder.updateColumnValue
                ("RequestStatus", Status).where()
                .eq("RequestId", RequestId);
        updateBuilder.update();

        // If Request is rolled back in Server, change the status to 'N' in
        // tblTransHeader
        if (Status.equals("R"))
            Status = "N";

        UpdateBuilder<tbltransHeader, Integer> updateBuildertrasn
                = transheaderDAO.updateBuilder();
        updateBuildertrasn.updateColumnValue
                ("TransStatus", Status).where()
                .eq("RequestId", RequestId);
        updateBuildertrasn.update();
    }

    public String getRequestId() throws SQLException {
        String reqid = "";
        syncMasterDAO = databaseHelper.getSyncMasterRuntimeExceptionDao();
        List<TblSyncMaster> syncMasterlist = syncMasterDAO.queryBuilder().selectColumns("RequestId").where()
                .eq("RequestStatus", "A").or().
                        eq("RequestStatus", "S").query();
        if (syncMasterlist != null && syncMasterlist.size() > 0)
            reqid = syncMasterlist.get(0).getRequestId();
        return reqid;
    }

    public String getTransDate() throws SQLException {
        String reqid = "";
        transheaderDAO = databaseHelper.getTransHeaderRuntimeExceptionDao();
        List<tbltransHeader> transheaderlist = transheaderDAO.queryBuilder()
                .selectColumns("transdate").where()
                .eq("TransStatus", "N").query();
        if (transheaderlist != null && transheaderlist.size() > 0)
            reqid = transheaderlist.get(0).getTransDate();
        return reqid;
    }

    public void insertSyncMaster(TblSyncMaster syncMaster) {
        syncMasterDAO = databaseHelper.getSyncMasterRuntimeExceptionDao();
        syncMasterDAO.create(syncMaster);
    }

    public void updateLastRequestNo(int lastReqNo) throws SQLException {
        usersDAO = databaseHelper.getUsersRuntimeExceptionDao();
        UpdateBuilder<TblInstusers, Integer> updateBuilder = usersDAO.updateBuilder();
        UserRepository userRepository = new UserRepository(databaseHelper);
        TblInstusers user = userRepository.getOneAuditedUser(appState.ashaId);
        updateBuilder.updateColumnValue("LastRequestNumber", lastReqNo).where()
                .eq("userId", user.getUserId());
        updateBuilder.update();
    }

    public List<tbltransHeader> getTransId(String xFirstDate) throws SQLException {
        transheaderDAO = databaseHelper.getTransHeaderRuntimeExceptionDao();
        List<tbltransHeader> transIdList = transheaderDAO.queryBuilder().selectColumns("TransId").where().
                eq("TransStatus", "N")
                .and().eq("TransDate", xFirstDate).query();
        return transIdList;
    }

    public List<tbltransdetail> getTransDetailsData(String xTranId) throws SQLException {
        transdetailDAO = databaseHelper.getTransDetailsRuntimeExceptionDao();

        List<tbltransdetail> transDetail = transdetailDAO.queryBuilder().where().eq("TransId", xTranId).query();
        return transDetail;
    }

    public void updateTransHeader(String reqId, String xFirstDate) throws SQLException {
        transheaderDAO = databaseHelper.getTransHeaderRuntimeExceptionDao();
        UpdateBuilder<tbltransHeader, Integer> updateBuilder = transheaderDAO.updateBuilder();
        updateBuilder.updateColumnValue("RequestId", reqId).where()
                .eq("TransStatus", "N")
                .and().eq("TransDate", xFirstDate);
        updateBuilder.update();
    }

    public String getLastupdatedDatetimeSettings(String userId) {
        return null;
    }

    public List<String[]> getWomanPendingList
            (String sType, int villageCode, String status) throws Exception {
        String sql;
        String strStatus = "";


        if (status.equalsIgnoreCase("Upcoming"))
            strStatus = " and " +
                    " Date(substr(serviceduedatemin , 7) || '-' || " +
                    "   substr(serviceduedatemin ,4,2)  || '-' || " +
                    " substr(serviceduedatemin , 1,2))>Date('now')";
        else if (status.equalsIgnoreCase("Pending"))
            strStatus = " and " +
                    " Date(substr(serviceduedatemin , 7) || '-' || " +
                    "   substr(serviceduedatemin ,4,2)  || '-' || " +
                    " substr(serviceduedatemin , 1,2))<=Date('now')";

        if (villageCode == 0)
            sql = "Select distinct(womanid),regwomanname, ServiceDueDateMin, " +
                    "ServiceDueDateMax, reglmp,regpregnantormother,\n" +
                    "regADDate, regComplicatedpreg, regriskfactors,regAmberorredcolorcode," +
                    "regwomenImage ,servicenumber,serviceid from tblserviceslist inner \n" +
                    "join tblregisteredwomen on tblserviceslist.womenid =\n" +
                    "tblregisteredwomen.womanid where servicetype='" + sType + "' and " +
                    "serviceactualdateofaction is null" + " and serviceplanned!=2 " +               //12Aug2019 -Bindu - Check not Not applicable
                    strStatus +
                    " group by womanid" +
                    "  order by Date(substr(serviceduedatemin , 7) || '-' || \n" +
                    "substr(serviceduedatemin ,4,2)  || '-' || \n" +
                    " substr(serviceduedatemin , 1,2))";

        else
            sql = "Select distinct(womanid),regwomanname, ServiceDueDateMin, ServiceDueDateMax, reglmp,regpregnantormother,\n" +
                    "regADDate, regComplicatedpreg, regriskfactors,regAmberorredcolorcode , " +
                    "regwomenImage ,servicenumber,serviceid from tblserviceslist inner \n" +
                    "join tblregisteredwomen on tblserviceslist.womenid =\n" +
                    "tblregisteredwomen.womanid where servicetype='" + sType + "'" +
                    " and serviceactualdateofaction is null" + " and serviceplanned!=2 " +               //12Aug2019 -Bindu - Check not Not applicable
                    " and regvillage=" + villageCode + " " +
                    strStatus +
                    " group by womanid order by Date(substr(serviceduedatemin , 7) || '-' || \n" +
                    "substr(serviceduedatemin ,4,2)  || '-' || \n" +
                    "                substr(serviceduedatemin , 1,2))";


        GenericRawResults<String[]> gen = databaseHelper.getServicesRuntimeExceptionDao().queryRaw(sql);

        List<String[]> list = gen.getResults();
        return list;

    }


    public List<String[]> getPendingServiceslistData(String womanId, String serviceType)
            throws SQLException {
        String sql = "SELECT * FROM `tblserviceslist` WHERE `servicetype` = " +
                "'" + serviceType + "' AND" +
                " `ServiceActualDateofAction` " +
                "IS NULL  AND Date(substr(serviceduedatemin , 7) || '-' " +
                "  || substr(serviceduedatemin ,4,2) " +
                "|| '-' || substr(serviceduedatemin , 1,2)) <= Date('now') AND " +
                "`womenid` = '" + womanId + "' and serviceplanned != 2";    //12Aug2019 - Bindu - Remove Expired/Unplanned2 service

        GenericRawResults<String[]> gen = databaseHelper.getServicesRuntimeExceptionDao().queryRaw(sql);

        List<String[]> list = gen.getResults();
        return list;
    }

    public List<ServiceslistPojo> getServiceTypeData(final String womanId, String stype) throws SQLException {

        List<ServiceslistPojo> listServcies = new ArrayList<>();

        int qryttboosterapp = 0;
        WomanRepository womanRepository = new WomanRepository(databaseHelper);
        tblregisteredwomen woman = womanRepository.getRegistartionDetails(womanId, appState.sessionUserId);
        if (woman.getRegLastChildAge() != null && woman.getRegLastChildAge() != "" && woman.getRegLastChildAge().trim().length() > 0) {
            int lastchildage = Integer.parseInt(woman.getRegLastChildAge());
            if (lastchildage > 0 && lastchildage < 36)
                // qryttboosterapp = "  tblservicestypemaster.serviceid != 1 and tblservicestypemaster.serviceid != 2 ";
                qryttboosterapp = 1;
            else if (lastchildage > 0 && lastchildage > 36)
                qryttboosterapp = 2;


        }

        String query = "Select tblservicestypemaster.ServiceType, " +
                "tblServiceslist. " +
                "  ServiceActualDateofAction,\n" +
                "  tblServicestypemaster.ServiceNumber" +
                "  from " +
                " tblservicestypemaster left join tblserviceslist\n" +
                "   on tblservicestypemaster.ServiceId = " +
                "tblserviceslist.ServiceId \n" +
                "     and tblserviceslist.ServicePlanned!= 3 " +
                " and womenId='" + womanId + "'\n" +
                " where" +
                " tblservicestypemaster.ServiceType='" + stype + "' and ServiceActualDateofAction is not null";
        if (qryttboosterapp == 1)
            query = query + " and serviceid != 1 and serviceid != 2 ";
        else if (qryttboosterapp == 2)
            query = query + " and serviceid != 3 ";

        query = query + " order by tblservicestypemaster.serviceid";


        GenericRawResults<ServiceslistPojo> tbls = databaseHelper.getServicesTypeRuntimeExceptionDao().
                queryRaw(query,
                        new RawRowMapper<ServiceslistPojo>() {
                            @Override
                            public ServiceslistPojo mapRow(String[] strings, String[] strings1
                            ) throws SQLException {
                                ServiceslistPojo servicesPojo = new ServiceslistPojo();
                                String s = strings[0];
                                servicesPojo.setServicetype(strings1[0]);
                                if (strings1[1] != null && strings1[1].trim().length() > 0)
                                    servicesPojo.setActualDateofAction(strings1[1]);
                                else
                                    servicesPojo.setActualDateofAction("");
                                servicesPojo.setServiceNumber(Integer.parseInt(strings1[2]));
                                servicesPojo.setWomanId(womanId);

                                return servicesPojo;
                            }
                        });

        for (ServiceslistPojo foo : tbls) {
            listServcies.add(foo);
        }
        return listServcies;
    }

    public TblInstusers getUser() {
        return this.user;
    }

    public void setUser(TblInstusers user) {
        this.user = user;
    }

    public SyncState getSyncState() {
        return this.syncState;
    }

    public SQLiteDatabase getSQLiteDatabase() {
        return this.databaseHelper.getWritableDatabase();
    }

    public String getReferralLastupdatedDatetime() throws SQLException {
        String refDateTime = "";

        String sql = " Select dateupdated from tblreferralFacility order by Date(dateupdated), time(dateupdated) desc  limit 1 ";
        GenericRawResults<String[]> gen = databaseHelper.gettblreferralFacilityRuntimeExceptionDao().queryRaw(sql);

        List<String[]> date  = gen.getResults();



        if(date!=null && date.size()>0)
        {
            refDateTime = date.get(0)[0];
        }
        return refDateTime;
    }
}