package com.sc.stmansi.SanNap;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sc.stmansi.R;

public class ListofSNViewHolder extends RecyclerView.ViewHolder {
    TextView txt_Count,txt_FeedbackDate, txt_KindofNkins, txt_FieldStaff;
    ImageView img_view;

    public ListofSNViewHolder(@NonNull View itemView) {
        super(itemView);
        txt_Count = itemView.findViewById(R.id.txt_sncount);
        txt_FeedbackDate = itemView.findViewById(R.id.txt_snfeedback_date);
        txt_KindofNkins = itemView.findViewById(R.id.txt_sn_kindofNkins);
        txt_FieldStaff = itemView.findViewById(R.id.txt_sn_FieldStaff);
        img_view = itemView.findViewById(R.id.imgviewSN);
    }
}
