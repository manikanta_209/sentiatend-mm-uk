package com.sc.stmansi.SanNap;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.sc.stmansi.R;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.repositories.SanNapRepository;
import com.sc.stmansi.tables.tblSanNap;
import com.sc.stmansi.tables.tblSanNapHeader;

import java.util.List;

public class ListofSNAdapter extends RecyclerView.Adapter<ListofSNViewHolder> {

    private final List<tblSanNapHeader> allSanNapHeaders;
    private final Context context;
    private final DatabaseHelper databaseHelper;
    private final Bundle bundle;

    public ListofSNAdapter(Context applicationContext, List<tblSanNapHeader> allSanNapHeaders, DatabaseHelper databaseHelper, Bundle bundle) {
        context = applicationContext;
        this.allSanNapHeaders = allSanNapHeaders;
        this.databaseHelper = databaseHelper;
        this.bundle = bundle;
    }

    @NonNull
    @Override
    public ListofSNViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CardView v = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_listofsannapitem, parent, false);
        return new ListofSNViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ListofSNViewHolder holder, int position) {
        try {
            int count = position + 1;
            holder.txt_Count.setText("#" + count);
            holder.txt_FeedbackDate.setText(allSanNapHeaders.get(position).getTblSNDateofFeedback());
            SanNapRepository sanNapRepository = new SanNapRepository(databaseHelper);
            tblSanNap sanNapDetails = sanNapRepository.getDetailsofKindofNkins(allSanNapHeaders.get(position).getAdolId(), allSanNapHeaders.get(position).getTransId());

            holder.txt_KindofNkins.setText(sanNapDetails.getSanNapValue() + context.getResources().getString(R.string.napkins));
            if (allSanNapHeaders.get(position).getTblSNFieldStaff().length()>0)
                holder.txt_FieldStaff.setText(context.getResources().getString(R.string.recordedby) + allSanNapHeaders.get(position).getTblSNFieldStaff());

            holder.img_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, View_SanNap.class);
                    intent.putExtra("adolID", allSanNapHeaders.get(position).getAdolId());
                    intent.putExtra("transId", allSanNapHeaders.get(position).getTransId());
                    intent.putExtra("globalState", bundle);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return allSanNapHeaders.size();
    }
}
