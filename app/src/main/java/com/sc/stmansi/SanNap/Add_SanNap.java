package com.sc.stmansi.SanNap;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.sc.stmansi.HomeVisit.HomeVisit;
import com.sc.stmansi.R;
import com.sc.stmansi.adolescent.AdolDeactivation;
import com.sc.stmansi.adolescent.AdolParticipatedTrainings;
import com.sc.stmansi.adolescent.AdolRegistration;
import com.sc.stmansi.adolescent.AdolVisitHistory;
import com.sc.stmansi.adolescent.AdolVisit_New;
import com.sc.stmansi.adolescent.AdolescentHome;
import com.sc.stmansi.childregistration.ChildRegistrationActivity;
import com.sc.stmansi.common.AlertDialogUtil;
import com.sc.stmansi.common.CommonDatePickerFragment;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.common.NavDrawerItem;
import com.sc.stmansi.common.NavDrawerListAdapter;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.configuration.SyncState;
import com.sc.stmansi.database.DBMethods;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.registration.RegistrationActivity;
import com.sc.stmansi.repositories.AdolVisitHeaderRepository;
import com.sc.stmansi.repositories.AdolescentRepository;
import com.sc.stmansi.repositories.FacilityRepository;
import com.sc.stmansi.repositories.SanNapHeaderRepository;
import com.sc.stmansi.repositories.TransactionHeaderRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblAdolReg;
import com.sc.stmansi.tables.tblAdolVisitHeader;
import com.sc.stmansi.tables.tblSanNap;
import com.sc.stmansi.tables.tblSanNapHeader;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.viewprofile.ViewProfileActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Callable;

public class Add_SanNap extends AppCompatActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener {
    private AppState appState;
    private DatabaseHelper databaseHelper;
    public AQuery aq;
    private SyncState syncState;
    private String AdolId;
    private tblAdolReg currentAdolDetails;
    private TblInstusers user;
    private Map<String, Integer> facilities;
    private LinkedHashMap<String, String[]> InsertSNAnswer = new LinkedHashMap<>();
    private tblSanNapHeader tblSNHeader;
    private Dao<tblSanNapHeader, Integer> SanNapHeaderDao;
    private int transId = 0;
    private Dao<tblSanNap, Integer> SanNapDetailsDao;
    private EditText currentEditText;
    private DrawerLayout drawer;
    private ListView drawerList;
    private String womanId;
    private Bundle bundle;
    private AQuery aqPMSel;
    private Activity activity;
    RadioGroup radGrpEDDorLmp;
    private boolean isRegDate;
    Boolean wantToCloseDialog = false;
    private int noOfDays;
    private tblregisteredwomen woman;

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Add_SanNap.this, List_SanNap.class);
        intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
        intent.putExtra("adolID", AdolId);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_sannap);
        try {
            bundle = getIntent().getBundleExtra("globalState");
            if (bundle != null) {
                appState = bundle.getParcelable("appState");
                syncState = bundle.getParcelable("syncState");
            } else {
                appState = new AppState();
            }
            AdolId = getIntent().getStringExtra("adolID");
            Initialze();
            getSupportActionBar().hide();
            getData();
            InitializeDrawer();
            UIFunctions();

            aq.id(R.id.btncancel).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Add_SanNap.this, List_SanNap.class);
                    intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    intent.putExtra("adolID", AdolId);
                    startActivity(intent);
                }
            });

            aq.id(R.id.btnsave).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (validationProvidedDate() && validationFeedbackDate() && validationKindofNkins())
                        setData();
                }
            });
            aq.id(R.id.toolbarBtnHome).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        displayDialogExit();
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean validationKindofNkins() {
        try {
            if ((!aq.id(R.id.rbSN_KindofProvidedtoYouReusable).isChecked()) && (!aq.id(R.id.rbSN_KindofProvidedtoYouDisposable).isChecked())) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.pleaseselectwhatkindofsanitarynkins), Toast.LENGTH_SHORT).show();
                opencloseAccordion(aq.id(R.id.ll_SN_InfoProvidedNapkins_Body).getView(), aq.id(R.id.txt_sn_infoprovidednapkins).getTextView());
                aq.id(R.id.ll_SN_Intro_Body).backgroundColor(getResources().getColor(R.color.lightgray));
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean validationFeedbackDate() {
        try {
            if (aq.id(R.id.etSN_FeedbackDate).getEditText().getText().toString().length() == 0) {
                aq.id(R.id.etSN_FeedbackDate).getEditText().setError(getResources().getString(R.string.pleaseenterfeedbackdate));
                aq.id(R.id.etSN_FeedbackDate).getEditText().requestFocus();
                return false;
            } else {
                aq.id(R.id.etSN_FeedbackDate).getEditText().setError(null);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean validationProvidedDate() {
        try {
            if (aq.id(R.id.etSN_ProvidedDate).getEditText().getText().toString().length() == 0) {
                aq.id(R.id.etSN_ProvidedDate).getEditText().setError(getResources().getString(R.string.pleaseenterprovideddate));
                aq.id(R.id.etSN_ProvidedDate).getEditText().requestFocus();
                return false;
            } else {
                aq.id(R.id.etSN_ProvidedDate).getEditText().setError(null);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private void setData() {
        try {
            InsertSNAnswer = new LinkedHashMap<>();
            tblSNHeader = new tblSanNapHeader();
            tblSNHeader.setUserId(user.getUserId());
            tblSNHeader.setAdolId(currentAdolDetails.getAdolID());
            tblSNHeader.setTblSNAdolAge(currentAdolDetails.getRegAdolAge());
            tblSNHeader.setTblSNAdolName(currentAdolDetails.getRegAdolName());

            tblSNHeader.setTblSNBlockName(aq.id(R.id.etSN_NameofBlock).getEditText().getText().toString());

            tblSNHeader.setTblSNVillage(String.valueOf(currentAdolDetails.getRegAdolFacilities()));

            tblSNHeader.setTblSNAdolGTS(aq.id(R.id.rbSN_GotoSchoolYes).isChecked() ? "Yes" : "No");

            tblSNHeader.setTblSNDateofSNProvided(aq.id(R.id.etSN_ProvidedDate).getEditText().getText().toString());
            tblSNHeader.setTblSNDateofFeedback(aq.id(R.id.etSN_FeedbackDate).getEditText().getText().toString());

            tblSNHeader.setTblSNFieldStaff(aq.id(R.id.rbSN_NameofStaff_ACHF).isChecked() ? "ACHF" : aq.id(R.id.rbSN_NameofStaff_AHF).isChecked() ? "AHF" : "");
            tblSNHeader.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
            tblSNHeader.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
            getNapkinProvidedDetails();
            if (aq.id(R.id.rbSN_KindofProvidedtoYouReusable).isChecked()) {
                getResusableNapkinsInformations();
            } else if (aq.id(R.id.rbSN_KindofProvidedtoYouDisposable).isChecked()) {
                getDisposableNapkinsInformations();
            } else {
                Toast.makeText(getApplicationContext(), "select what kind of Sanitary napkins were provided", Toast.LENGTH_SHORT).show();
            }
            InsertSNAnswer.size();
            ShowAlert();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ShowAlert() {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(Add_SanNap.this);
            builder.setMessage(getResources().getString(R.string.doyouwanttosave)).setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            SaveData();
                        }
                    }).setNegativeButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            }).create().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void SaveData() {
        try {
            transId = TransactionHeaderRepository.iCreateNewTransNew(user.getUserId(), databaseHelper);
            TransactionManager.callInTransaction(databaseHelper.getConnectionSource(), new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    SanNapHeaderDao = databaseHelper.getTblSanNapHeaderDao();
                    tblSNHeader.setTransId(transId);
                    int add = SanNapHeaderDao.create(tblSNHeader);
                    if (add > 0) {
                        boolean added = TransactionHeaderRepository.iNewRecordTransNew(appState.sessionUserId, transId,
                                SanNapHeaderDao.getTableName(), databaseHelper);
                        if (added) {
                            tblSanNap tblSanNapDetail = PrepareSanNapDetailsData();
                            SanNapDetailsDao = databaseHelper.getTblSanNapDao();
                            if (tblSanNapDetail != null) {
                                for (LinkedHashMap.Entry<String, String[]> data : InsertSNAnswer.entrySet()) {
                                    tblSanNapDetail.setSanNapId(data.getKey());
                                    tblSanNapDetail.setSanNapValue(data.getValue()[0]);
                                    tblSanNapDetail.setSanNapComments(data.getValue()[1]);
                                    add = SanNapDetailsDao.create(tblSanNapDetail);
                                }
                                if (add > 0) {
                                    added = TransactionHeaderRepository.iNewRecordTransNew(appState.sessionUserId, transId,
                                            SanNapDetailsDao.getTableName(), databaseHelper);
                                }
                                if (added) {
                                    Intent intent = new Intent(Add_SanNap.this, List_SanNap.class);
                                    intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                    intent.putExtra("adolID", AdolId);
                                    startActivity(intent);
                                } else {
                                    Toast.makeText(getApplicationContext(), "Not Saved", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), "Not Saved", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Not Saved", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Not Saved", Toast.LENGTH_SHORT).show();
                    }
                    return null;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private tblSanNap PrepareSanNapDetailsData() {
        try {
            tblSanNap tblSNDetails = new tblSanNap();
            tblSNDetails.setAdolId(currentAdolDetails.getAdolID());
            tblSNDetails.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
            tblSNDetails.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
            tblSNDetails.setUserId(user.getUserId());
            tblSNDetails.setTransId(transId);

            return tblSNDetails;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void getDisposableNapkinsInformations() {
        try {
            String[] str = new String[2];
            //quality of Nkins
            if (aq.id(R.id.rbSN_D_QofNapkins_Good).isChecked()) {
                str[0] = "1";
            } else if (aq.id(R.id.rbSN_D_QofNapkins_Fair).isChecked()) {
                str[0] = "2";
            } else if (aq.id(R.id.rbSN_D_QofNapkins_Poor).isChecked()) {
                str[0] = "3";
            } else {
                str[0] = "";
            }
            InsertSNAnswer.put("422", str);


            //while giving, were you informed how to dispose
            str = new String[2];
            if (aq.id(R.id.rbSN_WereyouInformedHowtoDisposal_Yes).isChecked()) {
                str[0] = "Yes";
            } else if (aq.id(R.id.rbSN_WereyouInformedHowtoDisposal_No).isChecked()) {
                str[0] = "No";
            }
            InsertSNAnswer.put("423", str);

            //feel about using
            str = new String[2];
            if (aq.id(R.id.rbSN_D_FeelaboutNapkins_Easy).isChecked()) {
                str[0] = "Easy";
            } else if (aq.id(R.id.rbSN_D_FeelaboutNapkins_Diffculty).isChecked()) {
                str[0] = "Diffcult";
            } else {
                str[0] = "";
            }
            InsertSNAnswer.put("424", str);

            //using all 5 pads
            str = new String[2];
            if (aq.id(R.id.rbSN_D_UsingAllthePads_Yes).isChecked()) {
                str[0] = "Yes";
                InsertSNAnswer.put("425", str);
            } else if (aq.id(R.id.rbSN_D_UsingAllthePads_No).isChecked()) {
                str[0] = "No";
                InsertSNAnswer.put("425", str);


                //how many nkins used
                str = new String[2];
                if (aq.id(R.id.rbSN_D_HowManyNkinsused_Perday).isChecked()) {
                    str[0] = "Per Day";
                } else if (aq.id(R.id.rbSN_D_HowManyNkinsused_WholeMensCycle).isChecked()) {
                    str[0] = "Whole Menstrual Cycler";
                } else {
                    str[0] = "";
                }
                InsertSNAnswer.put("426", str);
            }

            //sharing nkins with family
            str = new String[2];
            if (aq.id(R.id.rbSN_D_SharingwithFamilyMem_Yes).isChecked()) {
                str[0] = "Yes";
            } else if (aq.id(R.id.rbSN_D_SharingwithFamilyMem_No).isChecked()) {
                str[0] = "No";
            }
            InsertSNAnswer.put("427", str);

            //rashes or allergies
            str = new String[2];
            if (aq.id(R.id.rbSN_D_RashesorAllergiesAfterUsage_Yes).isChecked()) {
                str[0] = "Yes";
            } else if (aq.id(R.id.rbSN_D_RashesorAllergiesAfterUsage_No).isChecked()) {
                str[0] = "No";
            }
            InsertSNAnswer.put("428", str);

            //ablsorb bleeding
            str = new String[2];
            if (aq.id(R.id.rbSN_D_AbletoAbsorbtheBlood_Yes).isChecked()) {
                str[0] = "Yes";
            } else if (aq.id(R.id.rbSN_D_AbletoAbsorbtheBlood_No).isChecked()) {
                str[0] = "No";
            }
            InsertSNAnswer.put("429", str);

            //what do with used nkins
            str = new String[2];
            str[0] = String.valueOf(aq.id(R.id.spn_Sn_D_WhatyouwillDowithUsedNapkins).getSpinner().getSelectedItemPosition());
            if (aq.id(R.id.spn_Sn_D_WhatyouwillDowithUsedNapkins).getSpinner().getSelectedItemPosition() == 5) {
                str[1] = aq.id(R.id.etSN_D_WhatyouwillDowithUsedNapkinsOthers).getEditText().getText().toString();
            }
            InsertSNAnswer.put("430", str);

            //face problmes when dispose
            str = new String[2];
            if (aq.id(R.id.rbSN_FacingAnyProblemwhenDisposal_Yes).isChecked()) {
                str[0] = "Yes";
                InsertSNAnswer.put("431", str);

                //what kind of problems
                str = new String[2];
                str[0] = String.valueOf(aq.id(R.id.spn_Sn_WhatKindofProblemwhenDisposal).getSpinner().getSelectedItemPosition());
                if (aq.id(R.id.spn_Sn_WhatKindofProblemwhenDisposal).getSpinner().getSelectedItemPosition() == 3) {
                    str[1] = aq.id(R.id.etSN_D_WhatKindofProblemwhenDisposalOthers).getEditText().getText().toString();
                }
                InsertSNAnswer.put("432", str);
            } else if (aq.id(R.id.rbSN_FacingAnyProblemwhenDisposal_No).isChecked()) {
                str[0] = "No";
                InsertSNAnswer.put("431", str);
            }

            //similar nkins
            str = new String[2];
            if (aq.id(R.id.rbSN_D_UseSameNkinsinFuture_Yes).isChecked()) {
                str[0] = "Yes";
                InsertSNAnswer.put("433", str);
            } else if (aq.id(R.id.rbSN_D_UseSameNkinsinFuture_No).isChecked()) {
                str[0] = "No";
                InsertSNAnswer.put("433", str);

                //why wouldnot use disposalbe nkins
                str = new String[2];
                str[0] = String.valueOf(aq.id(R.id.spn_Sn_WhynotReuseDisposalNkins).getSpinner().getSelectedItemPosition());
                if (aq.id(R.id.spn_Sn_WhynotReuseDisposalNkins).getSpinner().getSelectedItemPosition() == 7) {
                    str[1] = aq.id(R.id.etSN_WhynotReuseDisposalNkinsOthers).getEditText().getText().toString();
                }
                InsertSNAnswer.put("434", str);
            }

            //recomment to friends
            str = new String[2];
            if (aq.id(R.id.rbSN_D_RecommendtoyourFriends_Yes).isChecked()) {
                str[0] = "Yes";
            } else if (aq.id(R.id.rbSN_D_RecommendtoyourFriends_No).isChecked()) {
                str[0] = "No";
            }
            InsertSNAnswer.put("435", str);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getResusableNapkinsInformations() {
        try {
            String[] str = new String[2];
            //quality of Nkins
            if (aq.id(R.id.rbSN_QofNapkins_Good).isChecked()) {
                str[0] = "1";
            } else if (aq.id(R.id.rbSN_QofNapkins_Fair).isChecked()) {
                str[0] = "2";
            } else if (aq.id(R.id.rbSN_QofNapkins_Poor).isChecked()) {
                str[0] = "3";
            } else {
                str[0] = "";
            }
            InsertSNAnswer.put("405", str);

            //while giving, were you informed how to use
            str = new String[2];
            if (aq.id(R.id.rbSN_WereyouInformedHowtoUse_Yes).isChecked()) {
                str[0] = "Yes";
            } else if (aq.id(R.id.rbSN_WereyouInformedHowtoUse_No).isChecked()) {
                str[0] = "No";
            }
            if (str[0] != null)
                InsertSNAnswer.put("406", str);

            //feel about using Nkins
            str = new String[2];
            if (aq.id(R.id.rbSN_FeelaboutNapkins_Easy).isChecked()) {
                str[0] = "Easy";
            } else if (aq.id(R.id.rbSN_FeelaboutNapkins_Diffculty).isChecked()) {
                str[0] = "Diffcult";
            } else {
                str[0] = "";
            }
            if (str[0] != null)
                InsertSNAnswer.put("407", str);

            //using all 5 pads
            str = new String[2];
            if (aq.id(R.id.rbSN_UsingAllthePads_Yes).isChecked()) {
                str[0] = "Yes";
                InsertSNAnswer.put("408", str);
            } else if (aq.id(R.id.rbSN_UsingAllthePads_No).isChecked()) {
                str[0] = "No";
                InsertSNAnswer.put("408", str);

                //how many nkins used
                str = new String[2];
                str[0] = aq.id(R.id.etSN_HowManySNDoYouUse).getEditText().getText().toString();
                InsertSNAnswer.put("409", str);
            }

            //sharing nkins with family
            str = new String[2];
            if (aq.id(R.id.rbSN_SharingwithFamilyMem_Yes).isChecked()) {
                str[0] = "Yes";
            } else if (aq.id(R.id.rbSN_SharingwithFamilyMem_No).isChecked()) {
                str[0] = "No";
            }
            if (str[0] != null)
                InsertSNAnswer.put("410", str);

            //geting rashes or allergies
            str = new String[2];
            if (aq.id(R.id.rbSN_RashesorAllergiesAfterUsage_Yes).isChecked()) {
                str[0] = "Yes";
            } else if (aq.id(R.id.rbSN_RashesorAllergiesAfterUsage_No).isChecked()) {
                str[0] = "No";
            }
            if (str[0] != null)
                InsertSNAnswer.put("411", str);

            //absorb blood
            str = new String[2];
            if (aq.id(R.id.rbSN_AbletoAbsorbtheBlood_Yes).isChecked()) {
                str[0] = "Yes";
            } else if (aq.id(R.id.rbSN_AbletoAbsorbtheBlood_No).isChecked()) {
                str[0] = "No";
            }
            if (str[0] != null)
                InsertSNAnswer.put("412", str);

            //what to do with used Nkins
            str = new String[2];
            str[0] = String.valueOf(aq.id(R.id.spn_Sn_WhatyouwillDowithUsedNapkins).getSpinner().getSelectedItemPosition());
            InsertSNAnswer.put("413", str);

            //face problem when washing and drying
            str = new String[2];
            if (aq.id(R.id.rbSN_FacingAnyProblemwhenWashorDry_Yes).isChecked()) {
                str[0] = "Yes";
                InsertSNAnswer.put("414", str);

                //what kind of problems
                str = new String[2];
                str[0] = String.valueOf(aq.id(R.id.spn_Sn_WhatKindofProblemwhenWashadnDry).getSpinner().getSelectedItemPosition());
                if (aq.id(R.id.spn_Sn_WhatKindofProblemwhenWashadnDry).getSpinner().getSelectedItemPosition() == 4) {
                    str[1] = aq.id(R.id.etSN_WhatKindofProblemwhenWashadnDryOthers).getEditText().getText().toString();
                }
                InsertSNAnswer.put("415", str);
            } else if (aq.id(R.id.rbSN_FacingAnyProblemwhenWashorDry_No).isChecked()) {
                str[0] = "No";
                InsertSNAnswer.put("414", str);
            }

            //do u use after washing
            str = new String[2];
            if (aq.id(R.id.rbSN_UsingafterWashorDry_Yes).isChecked()) {
                str[0] = "Yes";
            } else if (aq.id(R.id.rbSN_UsingafterWashorDry_No).isChecked()) {
                str[0] = "No";
            }
            if (str[0] != null)
                InsertSNAnswer.put("416", str);

            //washing make quality impacy
            str = new String[2];
            if (aq.id(R.id.rbSN_ImpactonQualityofNapkins_Yes).isChecked()) {
                str[0] = "Yes";
            } else if (aq.id(R.id.rbSN_ImpactonQualityofNapkins_No).isChecked()) {
                str[0] = "No";
            }
            if (str[0] != null)
                InsertSNAnswer.put("417", str);

            //what type of impact
            str = new String[2];
            str[0] = String.valueOf(aq.id(R.id.spn_Sn_ImpactTypeonQofNkins).getSpinner().getSelectedItemPosition());
            if (aq.id(R.id.spn_Sn_ImpactTypeonQofNkins).getSpinner().getSelectedItemPosition() == 5) {
                str[1] = aq.id(R.id.etSN_ImpactTypeonQofNkinsOthers).getEditText().getText().toString();
            }
            if (str[0] != null)
                InsertSNAnswer.put("418", str);

            //similar napkins
            str = new String[2];
            if (aq.id(R.id.rbSN_UseSameNkinsinFuture_Yes).isChecked()) {
                str[0] = "Yes";
                InsertSNAnswer.put("419", str);
            } else if (aq.id(R.id.rbSN_UseSameNkinsinFuture_No).isChecked()) {
                str[0] = "No";
                InsertSNAnswer.put("419", str);

                //why wouldnot use resualb nkins
                str = new String[2];
                str[0] = String.valueOf(aq.id(R.id.spn_Sn_WhynotReuseNkins).getSpinner().getSelectedItemPosition());
                if (aq.id(R.id.spn_Sn_WhynotReuseNkins).getSpinner().getSelectedItemPosition() == 5) {
                    str[1] = aq.id(R.id.etSN_WhynotReuseNkinsOthers).getEditText().getText().toString();
                }
                InsertSNAnswer.put("420", str);
            }

            //recomment to friends
            str = new String[2];
            if (aq.id(R.id.rbSN_RecommendtoyourFriends_Yes).isChecked()) {
                str[0] = "Yes";
            } else if (aq.id(R.id.rbSN_RecommendtoyourFriends_No).isChecked()) {
                str[0] = "No";
            }
            if (str[0] != null)
                InsertSNAnswer.put("421", str);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getNapkinProvidedDetails() {
        try {
            String[] str = new String[2];
            //provided by mansi
            if (aq.id(R.id.rbSN_ProvidedbyProgramYes).isChecked()) {
                str = new String[2];
                str[0] = "Yes";
                InsertSNAnswer.put("400", str);
            } else if (aq.id(R.id.rbSN_ProvidedbyProgramNo).isChecked()) {
                str = new String[2];
                str[0] = "No";
                InsertSNAnswer.put("400", str);
                //reason for
                str = new String[2];
                str[0] = String.valueOf(aq.id(R.id.spn_Sn_ProvidedbyProgramReason).getSpinner().getSelectedItemPosition());
                if (aq.id(R.id.spn_Sn_ProvidedbyProgramReason).getSpinner().getSelectedItemPosition() == 5) {
                    str[1] = aq.id(R.id.etSN_ProvidedbyProgramReasonOthers).getEditText().getText().toString();
                }
                InsertSNAnswer.put("401", str);
            }

            // what kind of snkins provided
            str = new String[2];
            if (aq.id(R.id.rbSN_KindofProvidedtoYouReusable).isChecked()) {
                str[0] = "Reusable";
                InsertSNAnswer.put("402", str);
            } else if (aq.id(R.id.rbSN_KindofProvidedtoYouDisposable).isChecked()) {
                str[0] = "Disposable";
                InsertSNAnswer.put("402", str);
            }

            //how many nkins povided
            str = new String[2];
            str[0] = aq.id(R.id.etSN_HowManyProvided).getEditText().getText().toString();
            InsertSNAnswer.put("403", str);

            //what you used before
            str = new String[2];
            str[0] = String.valueOf(aq.id(R.id.spn_Sn_usedtomanger_beforeSN).getSpinner().getSelectedItemPosition());
            if (aq.id(R.id.spn_Sn_usedtomanger_beforeSN).getSpinner().getSelectedItemPosition() == 5) {
                str[1] = aq.id(R.id.etSN_usedtomanger_beforeSNOthers).getEditText().getText().toString();
            }
            InsertSNAnswer.put("404", str);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void UIFunctions() {
        try {
            aq.id(R.id.rbSN_ProvidedbyProgramYes).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    aq.id(R.id.tr_Sn_ProvidedbyProgramReason).getView().setVisibility(View.GONE);
                    aq.id(R.id.spn_Sn_ProvidedbyProgramReason).getSpinner().setSelection(0);
                    aq.id(R.id.etSN_ProvidedbyProgramReasonOthers).getEditText().setText("");
                }
            });
            aq.id(R.id.rbSN_ProvidedbyProgramNo).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    aq.id(R.id.tr_Sn_ProvidedbyProgramReason).getView().setVisibility(View.VISIBLE);
                }
            });

            //resusable and disposable
            aq.id(R.id.rbSN_KindofProvidedtoYouReusable).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    aq.id(R.id.ll_SN_InfoandFeedbackofResuable).getView().setVisibility(View.VISIBLE);
                    aq.id(R.id.ll_SN_InfoandFeedbackofDisposable).getView().setVisibility(View.GONE);
                }
            });
            aq.id(R.id.rbSN_KindofProvidedtoYouDisposable).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    aq.id(R.id.ll_SN_InfoandFeedbackofResuable).getView().setVisibility(View.GONE);
                    aq.id(R.id.ll_SN_InfoandFeedbackofDisposable).getView().setVisibility(View.VISIBLE);
                }
            });

            //Resusable , all 5 pads
            aq.id(R.id.rbSN_UsingAllthePads_Yes).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    aq.id(R.id.tr_Sn_HowManySNDoYouUse).getView().setVisibility(View.GONE);
                    aq.id(R.id.etSN_HowManySNDoYouUse).getEditText().setText("");
                }
            });
            aq.id(R.id.rbSN_UsingAllthePads_No).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    aq.id(R.id.tr_Sn_HowManySNDoYouUse).getView().setVisibility(View.VISIBLE);
                }
            });

            aq.id(R.id.rbSN_FacingAnyProblemwhenWashorDry_Yes).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    aq.id(R.id.tr_Sn_WhatKindofProblemwhenWashadnDry).getView().setVisibility(View.VISIBLE);
                }
            });
            aq.id(R.id.rbSN_FacingAnyProblemwhenWashorDry_No).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    aq.id(R.id.tr_Sn_WhatKindofProblemwhenWashadnDry).getView().setVisibility(View.GONE);
                    aq.id(R.id.spn_Sn_WhatKindofProblemwhenWashadnDry).getSpinner().setSelection(0);
                    aq.id(R.id.etSN_WhatKindofProblemwhenWashadnDryOthers).getEditText().setText("");
                }
            });
            aq.id(R.id.rbSN_UseSameNkinsinFuture_Yes).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    aq.id(R.id.tr_sn_WhynotReuseNkins).getView().setVisibility(View.GONE);
                    aq.id(R.id.spn_Sn_WhynotReuseNkins).getSpinner().setSelection(0);
                    aq.id(R.id.etSN_WhynotReuseNkinsOthers).getEditText().setText("");
                }
            });
            aq.id(R.id.rbSN_UseSameNkinsinFuture_No).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    aq.id(R.id.tr_sn_WhynotReuseNkins).getView().setVisibility(View.VISIBLE);
                }
            });

            //Disposable , all 5 pads
            aq.id(R.id.rbSN_D_UsingAllthePads_Yes).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    aq.id(R.id.tr_SN_D_howmanyNKinsUsed).getView().setVisibility(View.GONE);
                }
            });
            aq.id(R.id.rbSN_D_UsingAllthePads_No).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    aq.id(R.id.tr_SN_D_howmanyNKinsUsed).getView().setVisibility(View.VISIBLE);
                    RadioGroup rg_HomeManyNkinsUsed = findViewById(R.id.rgSN_D_HowManyNkinsused);
                    rg_HomeManyNkinsUsed.clearCheck();
                }
            });


            aq.id(R.id.spn_Sn_ProvidedbyProgramReason).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (i == 5) {
                        aq.id(R.id.tr_Sn_ProvidedbyProgramReasonOthers).getView().setVisibility(View.VISIBLE);
                    } else {
                        aq.id(R.id.tr_Sn_ProvidedbyProgramReasonOthers).getView().setVisibility(View.GONE);
                        aq.id(R.id.etSN_ProvidedbyProgramReasonOthers).getEditText().setText("");
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            aq.id(R.id.spn_Sn_usedtomanger_beforeSN).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (i == 5) {
                        aq.id(R.id.tr_Sn_usedtomanger_beforeSNOthers).getView().setVisibility(View.VISIBLE);
                    } else {
                        aq.id(R.id.tr_Sn_usedtomanger_beforeSNOthers).getView().setVisibility(View.GONE);
                        aq.id(R.id.etSN_usedtomanger_beforeSNOthers).getEditText().setText("");
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            aq.id(R.id.spn_Sn_WhatyouwillDowithUsedNapkins).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (i == 5) {
                        aq.id(R.id.tr_Sn_WhatyouwillDowithUsedNapkinsOthers).getView().setVisibility(View.VISIBLE);
                    } else {
                        aq.id(R.id.tr_Sn_WhatyouwillDowithUsedNapkinsOthers).getView().setVisibility(View.GONE);
                        aq.id(R.id.etSN_WhatyouwillDowithUsedNapkinsOthers).getEditText().setText("");
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            aq.id(R.id.spn_Sn_WhatKindofProblemwhenWashadnDry).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (i == 4) {
                        aq.id(R.id.tr_Sn_WhatKindofProblemwhenWashadnDryOthers).getView().setVisibility(View.VISIBLE);
                    } else {
                        aq.id(R.id.tr_Sn_WhatKindofProblemwhenWashadnDryOthers).getView().setVisibility(View.GONE);
                        aq.id(R.id.etSN_WhatKindofProblemwhenWashadnDryOthers).getEditText().setText("");
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            aq.id(R.id.spn_Sn_ImpactTypeonQofNkins).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (i == 5) {
                        aq.id(R.id.tr_Sn_ImpactTypeonQofNkinsOthers).getView().setVisibility(View.VISIBLE);
                    } else {
                        aq.id(R.id.tr_Sn_ImpactTypeonQofNkinsOthers).getView().setVisibility(View.GONE);
                        aq.id(R.id.etSN_ImpactTypeonQofNkinsOthers).getEditText().setText("");
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            aq.id(R.id.spn_Sn_WhynotReuseNkins).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (i == 5) {
                        aq.id(R.id.tr_Sn_WhynotReuseNkinsOthers).getView().setVisibility(View.VISIBLE);
                    } else {
                        aq.id(R.id.tr_Sn_WhynotReuseNkinsOthers).getView().setVisibility(View.GONE);
                        aq.id(R.id.etSN_WhynotReuseNkinsOthers).getEditText().setText("");
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            aq.id(R.id.etSN_HowManyProvided).getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (aq.id(R.id.etSN_HowManyProvided).getEditText().length() > 0) {
                        int charInt = Integer.parseInt(charSequence.toString());
                        if (charInt > 7) {
                            aq.id(R.id.etSN_HowManyProvided).getEditText().setError(getResources().getString(R.string.valuecannotbegreaterthan6));
//                            aq.id(R.id.etSN_HowManyProvided).getEditText().setText("6");
                            aq.id(R.id.etSN_HowManyProvided).getEditText().setSelection(1);
                        } else {
                            aq.id(R.id.etSN_HowManyProvided).getEditText().setError(null);
                        }
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
            aq.id(R.id.etSN_HowManySNDoYouUse).getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (aq.id(R.id.etSN_HowManySNDoYouUse).getEditText().length() > 0) {
                        int charInt = Integer.parseInt(charSequence.toString());
                        if (charInt > 7) {
                            aq.id(R.id.etSN_HowManySNDoYouUse).getEditText().setError(getResources().getString(R.string.valuecannotbegreaterthan6));
//                            aq.id(R.id.etSN_HowManyProvided).getEditText().setText("6");
                            aq.id(R.id.etSN_HowManySNDoYouUse).getEditText().setSelection(1);
                        } else {
                            aq.id(R.id.etSN_HowManySNDoYouUse).getEditText().setError(null);
                        }
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

            //disposal what did you di with used nkins
            aq.id(R.id.spn_Sn_D_WhatyouwillDowithUsedNapkins).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (i == 5) {
                        aq.id(R.id.tr_Sn_D_WhatyouwillDowithUsedNapkinsOthers).getView().setVisibility(View.VISIBLE);
                    } else {
                        aq.id(R.id.tr_Sn_D_WhatyouwillDowithUsedNapkinsOthers).getView().setVisibility(View.GONE);
                        aq.id(R.id.etSN_D_WhatyouwillDowithUsedNapkinsOthers).getEditText().setText("");
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            aq.id(R.id.rbSN_FacingAnyProblemwhenDisposal_Yes).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    aq.id(R.id.tr_SN_WhatKindofProblemwhenDisposal).getView().setVisibility(View.VISIBLE);
                }
            });
            aq.id(R.id.rbSN_FacingAnyProblemwhenDisposal_No).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    aq.id(R.id.tr_SN_WhatKindofProblemwhenDisposal).getView().setVisibility(View.GONE);
                    aq.id(R.id.spn_Sn_WhatKindofProblemwhenDisposal).getSpinner().setSelection(0);
                    aq.id(R.id.etSN_D_WhatKindofProblemwhenDisposalOthers).getEditText().setText("");

                }
            });

            aq.id(R.id.spn_Sn_WhatKindofProblemwhenDisposal).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (i == 3) {
                        aq.id(R.id.tr_Sn_D_WhatKindofProblemwhenDisposalOthers).getView().setVisibility(View.VISIBLE);
                    } else {
                        aq.id(R.id.tr_Sn_D_WhatKindofProblemwhenDisposalOthers).getView().setVisibility(View.GONE);
                        aq.id(R.id.etSN_D_WhatKindofProblemwhenDisposalOthers).getEditText().setText("");
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            aq.id(R.id.rbSN_D_UseSameNkinsinFuture_Yes).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    aq.id(R.id.tr_SN_WhynotReasonuseDisposalNkins).getView().setVisibility(View.GONE);
                    aq.id(R.id.spn_Sn_WhynotReuseDisposalNkins).getSpinner().setSelection(0);
                    aq.id(R.id.etSN_WhynotReuseDisposalNkinsOthers).getEditText().setText("");
                }
            });
            aq.id(R.id.rbSN_D_UseSameNkinsinFuture_No).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    aq.id(R.id.tr_SN_WhynotReasonuseDisposalNkins).getView().setVisibility(View.VISIBLE);
                }
            });

            aq.id(R.id.spn_Sn_WhynotReuseDisposalNkins).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (i == 7) {
                        aq.id(R.id.tr_Sn_WhynotReuseDisposalNkinsOthers).getView().setVisibility(View.VISIBLE);
                    } else {
                        aq.id(R.id.tr_Sn_WhynotReuseDisposalNkinsOthers).getView().setVisibility(View.GONE);
                        aq.id(R.id.etSN_WhynotReuseDisposalNkinsOthers).getEditText().setText("");
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("SetTextI18n")
    private void getData() {
        try {
            UserRepository userRepo = new UserRepository(databaseHelper);
            user = userRepo.getOneAuditedUser(appState.ashaId);

            aq.id(R.id.toolbartxtTitle).getTextView().setText(getResources().getString(R.string.mhmform));
            aq.id(R.id.etSN_FeedbackDate).getTextView().setText(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
            aq.id(R.id.etSN_ProvidedDate).getTextView().setText(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());

            AdolescentRepository adolescentRepository = new AdolescentRepository(databaseHelper);
            currentAdolDetails = adolescentRepository.getAdolDatafromAdolID(AdolId);

            FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
            facilities = facilityRepository.getPlaceMap();
            ArrayList<String> villageList = new ArrayList<String>();
            villageList.add(getResources().getString(R.string.selectType));
            for (Map.Entry<String, Integer> village : facilities.entrySet())
                villageList.add(village.getKey());
            ArrayAdapter<String> VillageAdapter = new ArrayAdapter<>(this,
                    R.layout.simple_spinner_dropdown_item, villageList);
            VillageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            aq.id(R.id.spn_SN_VillageName).adapter(VillageAdapter);

            aq.id(R.id.txtSN_AdolName).getTextView().setText(currentAdolDetails.getRegAdolName());
            aq.id(R.id.txtSN_AdolAge).getTextView().setText(currentAdolDetails.getRegAdolAge() + getResources().getString(R.string.yearstxt));

            int villagecode = getKey(facilities, String.valueOf(currentAdolDetails.getRegAdolFacilities()));
            aq.id(R.id.spn_SN_VillageName).getSpinner().setSelection(villagecode);
            aq.id(R.id.txtSN_AdolVillageName).getTextView().setText(aq.id(R.id.spn_SN_VillageName).getSpinner().getSelectedItem().toString());

            AdolVisitHeaderRepository adolVisitHeaderRepository = new AdolVisitHeaderRepository(databaseHelper);
            if (currentAdolDetails.getLastVisitCount() != 0) {
                tblAdolVisitHeader localAdolVisitHeader = adolVisitHeaderRepository.getVisitbyID(String.valueOf(currentAdolDetails.getLastVisitCount()), currentAdolDetails.getAdolID());
                currentAdolDetails.setRegAdolGoingtoSchool(localAdolVisitHeader.getAdolvisHGTS());
            }


            if (currentAdolDetails.getRegAdolGoingtoSchool().equals("Yes")) {
                aq.id(R.id.rbSN_GotoSchoolYes).checked(true);
            } else {
                aq.id(R.id.rbSN_GotoSchoolNo).checked(false);
            }

            SanNapHeaderRepository sanNapHeaderRepository = new SanNapHeaderRepository(databaseHelper);
            List<tblSanNapHeader> listofHeaders = sanNapHeaderRepository.getAllHeadersofAdol(AdolId);
            if (listofHeaders != null && listofHeaders.size() > 0) {
                for (tblSanNapHeader s : listofHeaders) {
                    if (s.getTblSNDateofFeedback().equals(DateTimeUtil.getTodaysDate())) {
                        aq.id(R.id.txt_SNMonthWarning).getTextView().setVisibility(View.VISIBLE);
                        break;
                    }
                }
            }

            opencloseAccordion(aq.id(R.id.ll_SN_Intro_Body).getView(), aq.id(R.id.txt_sn_intro).getTextView());//to opeb intro
            aq.id(R.id.ll_SN_Intro_Body).backgroundColor(getResources().getColor(R.color.lightgray));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Initialze() {
        databaseHelper = getHelper();
        DBMethods dbMethods = new DBMethods(databaseHelper);
        aq = new AQuery(this);
    }

    public void opencloseAccordion(View v, TextView txt) {
        try {
            if (v.getVisibility() == View.VISIBLE) {
                v.setVisibility(View.GONE);
                if (txt.getCurrentTextColor() == getResources().getColor(R.color.red))
                    txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.collapse, 0);
                else
                    txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
            } else {
                v.setVisibility(View.VISIBLE);
                txt.setTextColor(getResources().getColor(R.color.red));
                txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);

            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    @Override
    public void onClick(View view) {
        try {
            if (view.getId() == R.id.ll_SN_Intro_Header || view.getId() == R.id.txt_sn_intro) {
                opencloseAccordion(aq.id(R.id.ll_SN_Intro_Body).getView(), aq.id(R.id.txt_sn_intro).getTextView());
                aq.id(R.id.ll_SN_Intro_Body).backgroundColor(getResources().getColor(R.color.lightgray));
            } else if (view.getId() == R.id.ll_SN_InfoProvidedNapkins_Head || view.getId() == R.id.txt_sn_infoprovidednapkins) {
                opencloseAccordion(aq.id(R.id.ll_SN_InfoProvidedNapkins_Body).getView(), aq.id(R.id.txt_sn_infoprovidednapkins).getTextView());
                aq.id(R.id.ll_SN_Intro_Body).backgroundColor(getResources().getColor(R.color.lightgray));
            } else if (view.getId() == R.id.ll_SN_InfoandFeedbackofResuable_Head || view.getId() == R.id.txt_sn_InfoandFeedbackofResuable) {
                opencloseAccordion(aq.id(R.id.ll_SN_InfoandFeedbackofResuable_Body).getView(), aq.id(R.id.txt_sn_InfoandFeedbackofResuable).getTextView());
                aq.id(R.id.ll_SN_Intro_Body).backgroundColor(getResources().getColor(R.color.lightgray));
            } else if (view.getId() == R.id.ll_SN_InfoandFeedbackofDisposable_Head || view.getId() == R.id.txt_sn_InfoandFeedbackofDisposable) {
                opencloseAccordion(aq.id(R.id.ll_SN_InfoandFeedbackofDisposable_Body).getView(), aq.id(R.id.txt_sn_InfoandFeedbackofDisposable).getTextView());
                aq.id(R.id.ll_SN_Intro_Body).backgroundColor(getResources().getColor(R.color.lightgray));
            } else if (view.getId() == R.id.etSN_ProvidedDate) {
                assignEditTextAndCallDatePicker(aq.id(R.id.etSN_ProvidedDate).getEditText());
            } else if (view.getId() == R.id.etSN_FeedbackDate) {
                assignEditTextAndCallDatePicker(aq.id(R.id.etSN_FeedbackDate).getEditText());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(getApplicationContext(), DatabaseHelper.class);
        }
        return databaseHelper;
    }

    private void displayDialogExit() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(Add_SanNap.this);
        builder.setMessage(getResources().getString(R.string.m110));
        builder.setCancelable(false);
        builder.setNegativeButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Add_SanNap.this, AdolescentHome.class);
                intent.putExtra("tabItem", 0);
                intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                startActivity(intent);
            }
        }).setPositiveButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    public static int getKey(Map<String, Integer> mapref, String value) {

        int pos = 0;
        for (Map.Entry<String, Integer> map : mapref.entrySet()) {
            pos++;
            if (map.getValue().toString().equals(value)) {
                return pos;
            }

        }
        return pos;
    }

    private void assignEditTextAndCallDatePicker(EditText editText) {
        currentEditText = editText;
        String currDate = (editText.getText().length() > 0) ? editText.getText().toString() : "";
        CommonDatePickerFragment.getCommonDate(this, currDate);
    }

    //    @SuppressLint("DefaultLocale")
//    @Override
//    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
//        try {
//            if (datePicker.isShown()) {
//                String strseldate = null;
//                strseldate = String.format("%02d", day) + "-" + String.format("%02d", month + 1) + "-" + year;
//
//                @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
//
//                Date adolDate =  format.parse(currentAdolDetails.getRecordCreatedDate());
//                Date seldate = format.parse(strseldate);
//
//                if (currentEditText == aq.id(R.id.etSN_ProvidedDate).getEditText()) {
//                    if (seldate != null && seldate.after(new Date())) {
//                        AlertDialogUtil.displayAlertMessage(
//                                getResources().getString(R.string.date_cannot_af_currentdate)
//                                , this);
//                        aq.id(R.id.etSN_ProvidedDate).text("");
//                    } else if (seldate!= null && seldate.before(adolDate)) {
//                        AlertDialogUtil.displayAlertMessage(
//                                getResources().getString(R.string.date_cannot_bf_regdate)
//                                , this);
//                        aq.id(R.id.etSN_ProvidedDate).text("");
//                    } else {
//                        aq.id(R.id.etSN_ProvidedDate).text(strseldate);
//                    }
//                } else if (currentEditText == aq.id(R.id.etSN_FeedbackDate).getEditText()) {
//                    if (seldate != null && seldate.after(new Date())) {
//                        AlertDialogUtil.displayAlertMessage(
//                                getResources().getString(R.string.date_cannot_af_currentdate)
//                                , this);
//                        aq.id(R.id.etSN_FeedbackDate).text("");
//                    } else {
//                        aq.id(R.id.etSN_FeedbackDate).text(strseldate);
//                    }
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
    private void InitializeDrawer() {
        try {
            drawer = findViewById(R.id.drawer_adolAdd_Sannap);
            drawerList = findViewById(R.id.lvNav_adolAdd_Sannap);

            ArrayList<NavDrawerItem> navDrawerItems = new ArrayList<>();
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.editadolescent), R.drawable.ic_edit_icon));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.homevisitadd), R.drawable.ic_homevisit));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.adolvisithistory), R.drawable.anm_pending_activities));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.trainingparticipated), R.drawable.presentation));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.mhmform), R.drawable.form_icon));

            String strAddPregnancy = "";
            if (new AdolescentRepository(databaseHelper).
                    isAdolANC(currentAdolDetails.getRegAdolAadharNo(),currentAdolDetails.getAdolID()))
                strAddPregnancy = getResources().getString(R.string.viewpregancny);
            else
                strAddPregnancy = getResources().getString(R.string.addpregancny);
            navDrawerItems.add(new NavDrawerItem(strAddPregnancy, R.drawable.anc));
            womanId = new AdolescentRepository(databaseHelper).getAdolANCWomanId
                    (currentAdolDetails.getRegAdolAadharNo(),currentAdolDetails.getAdolID());
            navDrawerItems.add(new NavDrawerItem(getResources()
                    .getString(R.string.anchomevisit), R.drawable.ic_homevisit));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deactivate), R.drawable.deactivate));
            drawer.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
            drawerList.setAdapter(new NavDrawerListAdapter(getApplicationContext(), navDrawerItems));
            drawerList.setOnItemClickListener(new DrawerItemClickListener());
            Toolbar toolbar = findViewById(R.id.toolbar_SanNap);
            ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close);
            drawer.setDrawerListener(drawerToggle);
            drawerToggle.syncState();
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private class DrawerItemClickListener implements android.widget.AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            drawer.closeDrawer(drawerList);
            try {
                switch (i) {
                    case 0:
                        Intent intent = new Intent(Add_SanNap.this, AdolRegistration.class);
                        intent.putExtra("globalState", bundle);
                        intent.putExtra("isEdit", true);
                        intent.putExtra("adolID", AdolId);
                        startActivity(intent);
                        break;
                    case 1:
                        if (currentAdolDetails.getRegAdolDeactivateDate().length() != 0) {
                            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.adolescentdeactivated), Add_SanNap.this);
                            drawer.closeDrawer(GravityCompat.END);
                        } else {
                            intent = new Intent(Add_SanNap.this, AdolVisit_New.class);
                            intent.putExtra("globalState", bundle);
                            intent.putExtra("adolID", AdolId);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                        break;
                    case 2:
                        intent = new Intent(Add_SanNap.this, AdolVisitHistory.class);
                        intent.putExtra("globalState", bundle);
                        intent.putExtra("adolID", AdolId);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        break;
                    case 3:
                        intent = new Intent(Add_SanNap.this, AdolParticipatedTrainings.class);
                        intent.putExtra("globalState", bundle);
                        intent.putExtra("adolID", AdolId);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        break;
                    case 4:
                        intent = new Intent(Add_SanNap.this, List_SanNap.class);
                        intent.putExtra("globalState", bundle);
                        intent.putExtra("adolID", AdolId);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        break;
                    // 01Sep2021 Arpitha
                    case 5:

                        if (currentAdolDetails.getRegAdolPregnant() != null && currentAdolDetails.getRegAdolPregnant().trim().length() > 0
                                && currentAdolDetails.getRegAdolPregnant().equalsIgnoreCase("Yes")) {
                            if (new AdolescentRepository(databaseHelper)
                                    .isAdolANC(currentAdolDetails.getRegAdolAadharNo(),currentAdolDetails.getAdolID())) {
                                appState.selectedWomanId = womanId;
                                Intent intent1 = new Intent(Add_SanNap.this, ViewProfileActivity.class);
                                intent1.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                startActivity(intent1);
                            } else
                                showPregnantOrMotherSelectionDialog();

                        } else
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.ntapplicable),
                                    Toast.LENGTH_LONG).show();
                        break;

                    case 6:
                        if (new AdolescentRepository(databaseHelper)
                                .isAdolANC(currentAdolDetails.getRegAdolAadharNo(),currentAdolDetails.getAdolID())) {
                            appState.selectedWomanId = womanId;
                            Intent hv = new Intent(Add_SanNap.this, HomeVisit.class);
                            hv.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(hv);
                        } else
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.ntapplicable),
                                    Toast.LENGTH_LONG).show();
                        break;
                    case 7:
                        intent = new Intent(Add_SanNap.this, AdolDeactivation.class);
                        intent.putExtra("globalState", bundle);
                        if (currentAdolDetails.getRegAdolDeactivateDate().length() != 0) {
                            intent.putExtra("isView", true);
                        }
                        intent.putExtra("adolID", AdolId);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        break;
                }
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());

            }
        }
    }

    private void showPregnantOrMotherSelectionDialog() {
        try {
            final android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(activity,
                    android.app.AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
            final View customView = LayoutInflater.from(this).inflate(R.layout.pregnant_mother_selection,
                    null);

            aqPMSel = new AQuery(customView);
            radGrpEDDorLmp = customView.findViewById(R.id.radEDDorLmp);

            initializeScreen(aqPMSel.id(R.id.llPregOrMothMainLayout).getView());
            setInitialView();

            aqPMSel.id(R.id.llmother).gone();
            aqPMSel.id(R.id.llchild).gone();
            aqPMSel.id(R.id.llAdol).gone();


            aqPMSel.id(R.id.imgclearedddate).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    aqPMSel.id(R.id.etlmpdate).text("");
                }
            });
            aqPMSel.id(R.id.imgclearlmp).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    aqPMSel.id(R.id.etMotherAdd).text("");
                    aqPMSel.id(R.id.etlmpdate).text("");
                    aqPMSel.id(R.id.etgest).text("");
                    aqPMSel.id(R.id.imgclearlmp).gone();
                }
            });
            aqPMSel.id(R.id.imgclearregadate).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());
                    Toast.makeText(activity, getResources().getString(R.string.dateresettotodaysdate), Toast.LENGTH_LONG).show();//21Aug2019 Arpitha
                }
            });//   Arpitha - 20Aug2019

//            aqPMSel.id(R.id.etMotherAdd).text("20-05-2021");
            aqPMSel.id(R.id.eteddmother).enabled(false);

            aqPMSel.id(R.id.etMotherAdd).getEditText().setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    try {
                        if (MotionEvent.ACTION_UP == event.getAction()) {
                            isRegDate = false;
                            assignEditTextAndCallDatePicker(aqPMSel.id(R.id.etMotherAdd).getEditText());
                        }
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());

                    }
                    return true;
                }
            });

            aqPMSel.id(R.id.etregdate).getEditText().setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    try {
                        if (MotionEvent.ACTION_UP == event.getAction()) {
                            isRegDate = true;
                            assignEditTextAndCallDatePicker(aqPMSel.id(R.id.etregdate).getEditText());
                        }
                    } catch (Exception e) {

                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }
                    return true;
                }
            });

            aqPMSel.id(R.id.radLMP).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        setLMP();
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }
                }
            });

            aqPMSel.id(R.id.radEDD).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        setEDD();
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }
                }
            });

            alert.setView(customView);

            alert.setCancelable(true)
                    .setTitle(getResources().getString(R.string.registrationHeading))
                    .setPositiveButton(getResources().getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {

                                }
                            })
                    .setNegativeButton(getResources().getString(R.string.cancel), //01Oct2019 - Bindu - Add cancel button
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    dialog.cancel();
                                }
                            });
            final android.app.AlertDialog dialog = alert.create();
            if (dialog != null)
                dialog.show();

            // Overriding the handler immediately after show is probably a
            // better
            // approach than OnShowListener as described below
            dialog.getButton(android.app.AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        validateAndRegister();
                        if (wantToCloseDialog)
                            dialog.dismiss();
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }

                }
            });
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }


    //calculate gestation based on LMP
    private void calculateEddGestation(String lmp) {
        try {

            if (lmp.equalsIgnoreCase("lmp")) {
                if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0
                        && aqPMSel.id(R.id.etMotherAdd).getText().length() > 0) {

                    noOfDays = DateTimeUtil
                            .getNumberOfDaysFromLMP(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                    int noOfWeeks = noOfDays / 7;
                    int days = noOfDays % 7;

                    String strdays = "";

                    if (days > 1)
                        strdays = days + " " + activity.getResources().getString(R.string.days);
                    else if (days == 1)
                        strdays = days + " " + activity.getResources().getString(R.string.day);

                    aqPMSel.id(R.id.etgest)
                            .text(noOfWeeks + " " + activity.getResources().getString(R.string.weeks) + " " + strdays);
                }

                String edd = populateEDD(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                aqPMSel.id(R.id.etlmpdate).text(edd);

            } else {

                String strLmp = CalculateLMP(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                aqPMSel.id(R.id.etlmpdate).text(strLmp);

                if (aqPMSel.id(R.id.etlmpdate).getText().length() > 0
                        && aqPMSel.id(R.id.etlmpdate).getText().length() > 0) {

                    noOfDays = DateTimeUtil
                            .getNumberOfDaysFromLMP(aqPMSel.id(R.id.etlmpdate).getText().toString());
                    int noOfWeeks = noOfDays / 7;
                    int days = noOfDays % 7;

                    String strdays = "";

                    if (days > 1)
                        strdays = days + " " + activity.getResources().getString(R.string.days);
                    else if (days == 1)
                        strdays = days + " " + activity.getResources().getString(R.string.day);

                    aqPMSel.id(R.id.etgest)
                            .text(noOfWeeks + " " + activity.getResources().getString(R.string.weeks) + " " + strdays);
                }

            }
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }

    }

    // Calculate EDD and display in EDD Edit text
    private String populateEDD(String xLMP) throws Exception {
        String stredddate = "";
        try {

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());

            Date LmpDate;
            LmpDate = sdf.parse(xLMP);
            Calendar cal = Calendar.getInstance();
            cal.setTime(LmpDate);
            cal.add(Calendar.DAY_OF_MONTH, 280);

            int day = cal.get(Calendar.DAY_OF_MONTH);
            int month = cal.get(Calendar.MONTH) + 1;
            int year = cal.get(Calendar.YEAR);

            stredddate =
                    String.format("%02d", day) + "-" + String.format("%02d", month) + "-" + String.format("%02d", year);

        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        return stredddate;
    }

    //	calculate LMP based on EDD
    private String CalculateLMP(String EDDate) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        Date LMP;
        LMP = sdf.parse(EDDate);
        Calendar cal = Calendar.getInstance();
        cal.setTime(LMP);
        cal.add(Calendar.DAY_OF_MONTH, -280);

        int day = cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH) + 1;
        int year = cal.get(Calendar.YEAR);

        String lmpDate = String.format("%02d", day) + "-" + String.format("%02d", month) + "-" + year;
        return lmpDate;
    }

    /**
     * This method opens the Date Dialog and sets Date to respective EditText
     */

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        try {
            if (view.isShown()) {
                String str = String.format("%02d", day) + "-" + String.format("%02d", month + 1) + "-"
                        + year;

                String strseldate = null;
                strseldate = String.format("%02d", day) + "-" + String.format("%02d", month + 1) + "-" + year;
                @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                int days = DateTimeUtil.getDaysBetweenDates(DateTimeUtil.getTodaysDate(), str);

                Date adolDate = format.parse(currentAdolDetails.getRecordCreatedDate());
                Date seldate = format.parse(strseldate);

                if (currentEditText == aq.id(R.id.etSN_ProvidedDate).getEditText()) {
                    if (seldate != null && seldate.after(new Date())) {
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.date_cannot_af_currentdate)
                                , this);
                        aq.id(R.id.etSN_ProvidedDate).text("");
                    } else if (seldate != null && seldate.before(adolDate)) {
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.date_cannot_bf_regdate)
                                , this);
                        aq.id(R.id.etSN_ProvidedDate).text("");
                    } else {
                        aq.id(R.id.etSN_ProvidedDate).text(strseldate);
                    }
                } else if (currentEditText == aq.id(R.id.etSN_FeedbackDate).getEditText()) {
                    if (seldate != null && seldate.after(new Date())) {
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.date_cannot_af_currentdate)
                                , this);
                        aq.id(R.id.etSN_FeedbackDate).text("");
                    } else {
                        aq.id(R.id.etSN_FeedbackDate).text(strseldate);
                    }
                }
                if(aqPMSel!=null)
                {
                aqPMSel.id(R.id.imgclearlmp).background(R.drawable.brush1);
                if (!aqPMSel.id(R.id.radEDD).isChecked()) {
                    if (days >= 0) {
                        if (isRegDate) {
                            if (days < 30) {
                                aqPMSel.id(R.id.etregdate).text(str);
                                aqPMSel.id(R.id.imgclearregadate).visible();//20AUg2019 Arpitha
                            } else if (days > 120) {
                                aqPMSel.id(R.id.etregdate).text("");
                                aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                                Toast.makeText(activity, getResources().getString(R.string.date_val_2_toast),
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha

                            } else {
                                aqPMSel.id(R.id.etregdate).text(str);
                                aqPMSel.id(R.id.imgclearregadate).visible();//20AUg2019 Arpitha
                            }

                        } else if (aqPMSel.id(R.id.radLMP).isChecked()) {
                            String lmporadd = str;
                            int daysdiff = 0;
                            String regdate = aqPMSel.id(R.id.etregdate).getText().toString();

                            if (regdate != null && regdate.trim().length() > 0) {
                                daysdiff = DateTimeUtil.getDaysBetweenDates(regdate, lmporadd);

                                int daysdifffromCUrr = DateTimeUtil
                                        .getDaysBetweenDates(DateTimeUtil.getTodaysDate(), lmporadd);

                                if (daysdiff <= 30) {
                                    Toast.makeText(activity,
                                            getResources().getString(R.string.date_val_3_toast),
                                            Toast.LENGTH_LONG).show();
                                    aqPMSel.id(R.id.etlmpdate).text("");
                                    aqPMSel.id(R.id.etgest).text("");
                                    aqPMSel.id(R.id.etMotherAdd).text("");
                                    aqPMSel.id(R.id.imgclearlmp).gone();//20Aug2019 Arpitha
                                } else if (daysdifffromCUrr > 280) {
                                    Toast.makeText(activity,
                                            getResources().getString(R.string.date_val_4_toast),
                                            Toast.LENGTH_LONG).show();
                                    aqPMSel.id(R.id.etlmpdate).text("");
                                    aqPMSel.id(R.id.etgest).text("");
                                    aqPMSel.id(R.id.etMotherAdd).text("");
                                    aqPMSel.id(R.id.imgclearlmp).gone();//20Aug2019 Arpitha
                                } else {
                                    aqPMSel.id(R.id.etMotherAdd).text(str);
                                    calculateEddGestation("lmp");
                                    aqPMSel.id(R.id.imgclearlmp).visible();//20Aug2019 Arpitha
                                }
                            } else
                                Toast.makeText(activity, getResources().getString(R.string.plz_enter_reg_dt),
                                        Toast.LENGTH_LONG).show();
                        } else if (aqPMSel.id(R.id.rdbMother).isChecked()) {

                            int daysdiff = DateTimeUtil
                                    .getDaysBetweenDates(aqPMSel.id(R.id.etregdate).getText().toString(), str);
                            if (daysdiff > 365) {// Arpitha 28Jun2018 - 23Dec2019 Arpitha - allow max 1 year

                                Toast.makeText(activity, (getResources().getString(R.string.datecannotbeafteroneyearfromcurrentdate)), // 11July2018
                                        // Arpitha
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etMotherAdd).text("");
                            } else
                                aqPMSel.id(R.id.etMotherAdd).text(str);
                        } else if (aqPMSel.id(R.id.rdbchild).isChecked()) {

                            int daysdiff = DateTimeUtil
                                    .getDaysBetweenDates(aqPMSel.id(R.id.etregdate).getText().toString(), str);
                            if (daysdiff > 5844) {// Arpitha 28Jun2018

                                Toast.makeText(activity, (getResources().getString(R.string.date_val_7_toast)), // 11July2018
                                        // Arpitha
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etMotherAdd).text("");
                            } else
                                aqPMSel.id(R.id.etMotherAdd).text(str);
                        }
                    } else {
                        if (isRegDate) {
                            aqPMSel.id(R.id.etregdate).text("");
                            aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                            Toast.makeText(activity,
                                    getResources().getString(R.string.reg_date_cannot_greater_current_date),
                                    Toast.LENGTH_LONG).show();
                            aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha
                        } else {
                            aqPMSel.id(R.id.etMotherAdd).text("");
                            aqPMSel.id(R.id.imgclearlmp).gone();//20AUg2019 Arpitha
                            aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                            if (aqPMSel.id(R.id.rdbPreg).isChecked()) {
                                Toast.makeText(activity,
                                        getResources()
                                                .getString(R.string.lmp_date_time_cannot_be_after_curre_datetime), // 11July2018

                                        Toast.LENGTH_LONG).show();

                                aqPMSel.id(R.id.etlmpdate).text("");
                                aqPMSel.id(R.id.etgest).text("");
                            } else {
                                Toast.makeText(activity,
                                        getResources()
                                                .getString(R.string.del_date_cannot_be_after_current_date_time), // 11July2018
                                        Toast.LENGTH_LONG).show();
                            }

                        }
                    }
                } else {
                    if (!isRegDate) {
                        int days1 = 0;
                        if (aqPMSel.id(R.id.etregdate).getText().toString().trim().length() > 0)
                            days1 = DateTimeUtil.getDaysBetweenDates(str,
                                    aqPMSel.id(R.id.etregdate).getText().toString());

                        if (days1 > 0 && days1 < 273) {
                            aqPMSel.id(R.id.etMotherAdd).text(str);
                            calculateEddGestation("edd");
                            aqPMSel.id(R.id.imgclearlmp).visible();//20Aug2019 Arpitha
                        } else {
                            if (days1 > 280) {
                                aqPMSel.id(R.id.etMotherAdd).text("");
                                aqPMSel.id(R.id.imgclearlmp).gone();//21Aug2019 Arpitha
                                aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                                Toast.makeText(activity, getResources().getString(R.string.date_val_6_toast),

                                        Toast.LENGTH_LONG).show();

                                aqPMSel.id(R.id.etlmpdate).text("");
                                aqPMSel.id(R.id.etgest).text("");

                            } else {
                                aqPMSel.id(R.id.etMotherAdd).text("");
                                aqPMSel.id(R.id.imgclearlmp).gone();//21Aug2019 Arpitha
                                aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                                Toast.makeText(activity, getResources().getString(R.string.date_val_5_toast),
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etlmpdate).text("");
                                aqPMSel.id(R.id.etgest).text("");
                            }
                        }
                    } else {
                        days = DateTimeUtil.getDaysBetweenDates(DateTimeUtil.getTodaysDate(), str);
                        if (days >= 0) {

                            if (days > 90) {
                                aqPMSel.id(R.id.etregdate).text("");
                                aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                                Toast.makeText(activity, getResources().getString(R.string.date_val_2_toast),

                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha
                            } else {
                                aqPMSel.id(R.id.etregdate).text(str);
                            }

                        } else {
                            aqPMSel.id(R.id.etregdate).text("");
                            aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                            Toast.makeText(activity, getResources().getString(R.string.reg_date_cannot_greater_current_date),

                                    Toast.LENGTH_LONG).show();
                            aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha
                        }
                    }
                }
                }

            }

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private void validateAndRegister() throws Exception {
        try {
            Intent nextScreen;
            String lmpOrAdd = aqPMSel.id(R.id.etMotherAdd).getText().toString();
            String regDate = aqPMSel.id(R.id.etregdate).getText().toString();

            if ((lmpOrAdd != null && lmpOrAdd.length() > 0) && (regDate != null && regDate.length() > 0)) {

                if (aqPMSel.id(R.id.rdbPreg).isChecked()) {
                    if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0) {
                        int days = DateTimeUtil.getDaysBetweenDates(regDate, lmpOrAdd);
                        int daysdiff = DateTimeUtil
                                .getDaysBetweenDates(DateTimeUtil.getTodaysDate(), lmpOrAdd);
                        if (aqPMSel.id(R.id.radEDD).isChecked()) {
                            if (days >= 0) {
                                wantToCloseDialog = false;
                                Toast.makeText(getApplicationContext(),
                                        getResources().getString(R.string.date_val_5_toast),

                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etMotherAdd).text("");
                            } else if (noOfDays < 30) {
                                wantToCloseDialog = false;
                                Toast.makeText(getApplicationContext(),
                                        getResources().getString(R.string.gestation_must_be_30days),
                                        Toast.LENGTH_LONG).show();
                            } else if (noOfDays > 280) {
                                wantToCloseDialog = false;
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.gest_280),
                                        Toast.LENGTH_LONG).show();
                            } else {
                                wantToCloseDialog = true;
                                woman = new tblregisteredwomen();
                                nextScreen = new Intent(getApplicationContext(), RegistrationActivity.class);
                                nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                nextScreen.putExtra("adolAadhar", currentAdolDetails.getRegAdolAadharNo());//01Sep2021 Arpitha
                                woman.setRegPregnantorMother(1);
                                int gravida = 0;
                                if (aqPMSel.id(R.id.chkFirstPreg).isChecked())
                                    gravida = 1;
                                woman.setRegGravida(gravida);
                                woman.setRegRegistrationDate(aqPMSel.id(R.id.etregdate).getText().toString());
                                woman.setRegEDD(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                                nextScreen.putExtra("woman", woman);
                                startActivity(nextScreen);
                            }
                        } else {
                            if (days < 30) {
                                wantToCloseDialog = false;
                                Toast.makeText(getApplicationContext(),
                                        getResources().getString(R.string.date_val_3_toast),

                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etlmpdate).text("");
                                aqPMSel.id(R.id.etMotherAdd).text("");
                            } else if (daysdiff > 280) {

                                wantToCloseDialog = false;
                                Toast.makeText(getApplicationContext(),
                                        activity.getResources().getString(R.string.date_val_4_toast),
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etlmpdate).text("");
                                aqPMSel.id(R.id.etMotherAdd).text("");
                            } else {
                                wantToCloseDialog = true;
                                woman = new tblregisteredwomen();
                                nextScreen = new Intent(activity, RegistrationActivity.class);
                                nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                nextScreen.putExtra("adolAadhar", currentAdolDetails.getRegAdolAadharNo());//01Sep2021 Arpitha


                                woman.setRegPregnantorMother(1);
                                int gravida = 0;
                                if (aqPMSel.id(R.id.chkFirstPreg).isChecked())
                                    gravida = 1;
                                woman.setRegGravida(gravida);
                                woman.setRegRegistrationDate(aqPMSel.id(R.id.etregdate).getText().toString());
                                if (aqPMSel.id(R.id.radEDD).isChecked())
                                    woman.setRegEDD(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                                else
                                    woman.setRegLMP(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                                nextScreen.putExtra("woman", woman);
                                startActivity(nextScreen);
                            }
                        }

                    }
                } else if (aqPMSel.id(R.id.rdbMother).isChecked()) {
                    if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0) {
                        int days = DateTimeUtil.getDaysBetweenDates(regDate, lmpOrAdd);
                        if (days < 0) {
                            wantToCloseDialog = false;
                            Toast.makeText(activity, (getResources().getString(R.string.m077)),
                                    Toast.LENGTH_LONG).show();
                            // } else if (days > 549) {
                        } else if (days > 5844) {// Arpitha
                            // 28Jun2018
                            wantToCloseDialog = false;
                            Toast.makeText(activity,
                                    (getResources().getString(R.string.date_val_7_toast)), // 11july2018
                                    // Arpitha
                                    Toast.LENGTH_LONG).show();
                        } else {
                            wantToCloseDialog = true;
                            woman = new tblregisteredwomen();
                            nextScreen = new Intent(activity, RegistrationActivity.class);
                            nextScreen.putExtra("adolAadhar", currentAdolDetails.getRegAdolAadharNo());//01Sep2021 Arpitha
                            woman.setRegPregnantorMother(2);
                            woman.setRegADDate(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                            woman.setRegRegistrationDate(aqPMSel.id(R.id.etregdate).getText().toString());
                            nextScreen.putExtra("woman", woman);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(nextScreen);
                        }
                    }
                } else if (aqPMSel.id(R.id.rdbchild).isChecked()) {
                    if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0) {
                        int days = DateTimeUtil.getDaysBetweenDates(regDate, lmpOrAdd);
                        if (days < 0) {
                            wantToCloseDialog = false;
                            Toast.makeText(activity, (getResources().getString(R.string.m077)),
                                    Toast.LENGTH_LONG).show();
                            // } else if (days > 549) {
                        } else if (days > 5844) {// Arpitha
                            // 28Jun2018
                            wantToCloseDialog = false;
                            Toast.makeText(activity,
                                    (getResources().getString(R.string.date_val_7_toast)), // 11july2018
                                    // Arpitha
                                    Toast.LENGTH_LONG).show();
                        } else {
                            wantToCloseDialog = true;
                            woman = new tblregisteredwomen();
                            nextScreen = new Intent(activity, ChildRegistrationActivity.class);
//							woman.setRegPregnantorMother(1);
                            woman.setRegADDate(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                            woman.setRegRegistrationDate(aqPMSel.id(R.id.etregdate).getText().toString());
                            nextScreen.putExtra("woman", woman);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(nextScreen);
                        }
                    }
                } else if (aqPMSel.id(R.id.rdbAdol).isChecked()) { //Ramesh 13-may-2021
                    if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0) {
                        int days = DateTimeUtil.getDaysBetweenDates(regDate, lmpOrAdd);
                        if (days < 0) {
                            wantToCloseDialog = false;
                            Toast.makeText(activity, (getResources().getString(R.string.m077)),
                                    Toast.LENGTH_LONG).show();
                            // } else if (days > 549) {
                        } else if (days > 5844) {// Arpitha
                            // 28Jun2018
                            wantToCloseDialog = false;
                            Toast.makeText(activity,
                                    (getResources().getString(R.string.date_val_7_toast)), // 11july2018
                                    // Arpitha
                                    Toast.LENGTH_LONG).show();
                        } else {
                            wantToCloseDialog = true;
                            tblAdolReg adolReg = new tblAdolReg();
                            nextScreen = new Intent(activity, AdolRegistration.class);
                            adolReg.setRecordCreatedDate(aqPMSel.id(R.id.etregdate).getText().toString());
                            nextScreen.putExtra("AdolReg", adolReg);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(nextScreen);
                        }
                    }
                }
            } else {
                wantToCloseDialog = false;
                if (aqPMSel.id(R.id.rdbPreg).isChecked()) {
                    if (aqPMSel.id(R.id.etMotherAdd).getText().toString().trim().length() <= 0) {
                        aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                        if (aqPMSel.id(R.id.radEDD).isChecked())
                            Toast.makeText(activity,
                                    (activity.getResources().getString(R.string.m170)),
                                    Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(activity,
                                    activity.getResources().getString(R.string.m074),
                                    Toast.LENGTH_LONG).show();
                    } else if (aqPMSel.id(R.id.etregdate).getText().toString().trim().length() <= 0) {
                        aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                        Toast.makeText(getApplicationContext(),
                                (activity.getResources().getString(R.string.m157)),
                                Toast.LENGTH_LONG).show();
                    }
                } else if (aqPMSel.id(R.id.rdbMother).isChecked()) {
                    if (aqPMSel.id(R.id.etMotherAdd).getText().toString().trim().length() <= 0) {
                        aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                        Toast.makeText(activity,
                                (activity.getResources().getString(R.string.m066)),
                                Toast.LENGTH_LONG).show();
                    } else if (aqPMSel.id(R.id.etregdate).getText().toString().trim().length() <= 0) {
                        aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                        Toast.makeText(activity,
                                activity.getResources().getString(R.string.m157),
                                Toast.LENGTH_LONG).show();
                    }
                } else if (aqPMSel.id(R.id.rdbchild).isChecked()) {
                    wantToCloseDialog = true;
//					if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0)
                    {
//						int days = DateTimeUtil.getDaysBetweenDates(regDate, lmpOrAdd);
						/*if (days < 0) {
							wantToCloseDialog = false;
							Toast.makeText(activity, (getResources().getString(R.string.m077)),
									Toast.LENGTH_LONG).show();
							// } else if (days > 549) {
						} else if (days > 5844) {// Arpitha
							// 28Jun2018
							wantToCloseDialog = false;
							Toast.makeText(activity,
									(getResources().getString(R.string.date_val_7_toast)), // 11july2018
									// Arpitha
									Toast.LENGTH_LONG).show();
						} else {*/
                        wantToCloseDialog = true;
                        woman = new tblregisteredwomen();
                        nextScreen = new Intent(activity, ChildRegistrationActivity.class);
                        woman.setRegPregnantorMother(1);
                        woman.setRegADDate(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                        woman.setRegRegistrationDate(aqPMSel.id(R.id.etregdate).getText().toString());
                        nextScreen.putExtra("woman", woman);
                        nextScreen.putExtra("globalState", activity.getIntent().getBundleExtra("globalState"));
                        startActivity(nextScreen);
//						}
                    }
                } else if (aqPMSel.id(R.id.rdbAdol).isChecked()) { //Ramesh 13-may-2021

                    wantToCloseDialog = true;
                    tblAdolReg adolReg = new tblAdolReg();
                    nextScreen = new Intent(activity, AdolRegistration.class);
                    adolReg.setRecordCreatedDate(aqPMSel.id(R.id.etregdate).getText().toString());
                    nextScreen.putExtra("AdolReg", adolReg);
                    nextScreen.putExtra("globalState", activity.getIntent().getBundleExtra("globalState"));
                    startActivity(nextScreen);


                }
            }

        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private ArrayList<View> initializeScreen(View v) throws Exception {
        if (!(v instanceof ViewGroup)) {
            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);

            // TextView - Assign Labels
            if (v.getClass() == TextView.class || v.getClass() == RadioButton.class) {
                // aqPMSel.id(v.getId()).text(getSS(aqPMSel.id(v.getId()).getText().toString()));
            }

            if (v.getClass() == RadioButton.class) {
                aqPMSel.id(v.getId()).clicked(this, "commonClick");
            }

            return viewArrayList;
        }

        ArrayList<View> result = new ArrayList<View>();

        ViewGroup viewGroup = (ViewGroup) v;
        for (int i = 0; i < viewGroup.getChildCount(); i++) {

            View child = viewGroup.getChildAt(i);

            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);
            viewArrayList.addAll(initializeScreen(child));

            result.addAll(viewArrayList);
        }
        return result;
    }


    //	set initial view of the pregnant or mother pop up
    private void setInitialView() {
        aqPMSel.id(R.id.rdbPreg).checked(true);
        aqPMSel.id(R.id.tvMotherAddHeading).text(getResources().getString(R.string.tvPregLmpHeading));
        aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());
        aqPMSel.id(R.id.radLMP).checked(true);
        aqPMSel.id(R.id.etregdate).getEditText().setKeyListener(null);
        aqPMSel.id(R.id.etMotherAdd).getEditText().setKeyListener(null);
        aqPMSel.id(R.id.lllmpdate).visible();
        aqPMSel.id(R.id.llgest).visible();
        aqPMSel.id(R.id.vHLine4).visible();
        aqPMSel.id(R.id.vHLine6).visible();
        aqPMSel.id(R.id.llnote).visible();

        //13Apr2021 Bindu - set here cos not translated from xml
        aqPMSel.id(R.id.anclbl).text(getResources().getString(R.string.anclbl));
        aqPMSel.id(R.id.txtpnclbl).text(getResources().getString(R.string.pnclbl));
        aqPMSel.id(R.id.lmp_date).text(getResources().getString(R.string.tvedd));
        aqPMSel.id(R.id.txtgest).text(getResources().getString(R.string.tvGestationalageL));
        aqPMSel.id(R.id.txtchildreglbl).text(getResources().getString(R.string.childreg));
        aqPMSel.id(R.id.tvFirstPregHeading).text(getResources().getString(R.string.tvFirstPregHeading));
        aqPMSel.id(R.id.reg_date).text(getResources().getString(R.string.reg_date));
        aqPMSel.id(R.id.instructions).text(getResources().getString(R.string.instruction));
        aqPMSel.id(R.id.regdt).text(getResources().getString(R.string.reg_date));
        aqPMSel.id(R.id.txt1).text(getResources().getString(R.string.date_val_0));
        aqPMSel.id(R.id.txt2).text(getResources().getString(R.string.date_val_1));
        aqPMSel.id(R.id.txt3).text(getResources().getString(R.string.date_val_2));
        aqPMSel.id(R.id.txtlmp).text(getResources().getString(R.string.tvPregLmpHeading));
        aqPMSel.id(R.id.txt4).text(getResources().getString(R.string.date_val_3));
        aqPMSel.id(R.id.txt5).text(getResources().getString(R.string.date_val_4));
        aqPMSel.id(R.id.txt6).text(getResources().getString(R.string.date_val_5));
        aqPMSel.id(R.id.txt7).text(getResources().getString(R.string.date_val_6));
        aqPMSel.id(R.id.txt8).text(getResources().getString(R.string.date_val_7));
        aqPMSel.id(R.id.radLMP).text(getResources().getString(R.string.tvlmp));
        aqPMSel.id(R.id.radEDD).text(getResources().getString(R.string.tvedd));
//22May2021 Bindu set adol reg lbl
        aqPMSel.id(R.id.adolreg).text(getResources().getString(R.string.adolescentreg));

    }

    private void setEDD() throws Exception {
        aqPMSel.id(R.id.tvMotherAddHeading).text(getResources().getString(R.string.tvedd));
        aqPMSel.id(R.id.etMotherAdd).text("");
        aqPMSel.id(R.id.imgclearlmp).gone();//20AUg2019 Arpitha
        aqPMSel.id(R.id.lmp_date).text(getResources().getString(R.string.tvlmp));
        aqPMSel.id(R.id.etlmpdate).text("");
        aqPMSel.id(R.id.etgest).text("");
        aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());
    }

    // capture LMP
    private void setLMP() throws Exception {
        aqPMSel.id(R.id.tvMotherAddHeading).text(getResources().getString(R.string.tvlmp));
        aqPMSel.id(R.id.etMotherAdd).text("");
        aqPMSel.id(R.id.imgclearlmp).gone();//20AUg2019 Arpitha
        aqPMSel.id(R.id.lmp_date).text(getResources().getString(R.string.tvedd));
        aqPMSel.id(R.id.etlmpdate).text("");
        aqPMSel.id(R.id.etgest).text("");
    }

    public void commonClick(View v) throws Exception {
        switch (v.getId()) {
            case R.id.rdbPreg: {
                aqPMSel.id(R.id.rdbMother).checked(false);
                aqPMSel.id(R.id.tvFirstPregHeading).enabled(true);
                aqPMSel.id(R.id.rdbchild).checked(false);
                aqPMSel.id(R.id.rdbAdol).checked(false);//Ramesh 13-may2021
                aqPMSel.id(R.id.chkFirstPreg).enabled(true);
                aqPMSel.id(R.id.radEDDorLmp).visible();
                //27Nov2018 - Bindu - radiogrp clear chk to reset radiobtn
                aqPMSel.id(R.id.vHLine5).visible();
                aqPMSel.id(R.id.etMotherAdd).text("");
                aqPMSel.id(R.id.radLMP).checked(true);
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.tvPregLmpHeading)));
                // 27Jun2018 Arpitha
                aqPMSel.id(R.id.etregdate).enabled(true);
                aqPMSel.id(R.id.etregdate).textColor(getResources().getColor(R.color.black));
//				aqPMSel.id(R.id.llregdate).background(R.drawable.editext_none);
                aqPMSel.id(R.id.lllmpdate).visible();
                aqPMSel.id(R.id.llgest).visible();
                aqPMSel.id(R.id.vHLine4).visible();
                aqPMSel.id(R.id.vHLine6).visible();

                aqPMSel.id(R.id.llMotherAdd).visible();
                aqPMSel.id(R.id.llFirstPreg).visible();
                aqPMSel.id(R.id.llnote).visible();
                aqPMSel.id(R.id.vHLine5).visible();
//				19Nov2019 Arpitha
                aqPMSel.id(R.id.llancinstruction).visible();
                aqPMSel.id(R.id.llpncinstruction).gone();

                break;
            }
            case R.id.rdbMother: {
                aqPMSel.id(R.id.rdbPreg).checked(false);
                aqPMSel.id(R.id.rdbchild).checked(false);
                aqPMSel.id(R.id.rdbAdol).checked(false);//Ramesh 13-may2021
                //27Nov2018 - Bindu radiogrp clear chk to reset radiobtn
                radGrpEDDorLmp.clearCheck();
                aqPMSel.id(R.id.radLMP).checked(false);
                aqPMSel.id(R.id.radEDD).checked(false);
                aqPMSel.id(R.id.tvFirstPregHeading).enabled(false);
                aqPMSel.id(R.id.chkFirstPreg).enabled(false);
                aqPMSel.id(R.id.chkFirstPreg).checked(false);
                aqPMSel.id(R.id.radEDDorLmp).gone();
                aqPMSel.id(R.id.vHLine5).gone();
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.deldate)));
                aqPMSel.id(R.id.etMotherAdd).text("");
                // 27Jun2018 Arpitha
                aqPMSel.id(R.id.etregdate).enabled(false);
                aqPMSel.id(R.id.etregdate).textColor(R.color.fontcolor_disable);
                //	aqPMSel.id(R.id.llregdate).background(R.drawable.edittext_disable);
                aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());//21March2019 Arpitha
                aqPMSel.id(R.id.lllmpdate).gone();
                aqPMSel.id(R.id.llgest).gone();
                aqPMSel.id(R.id.vHLine4).gone();
                aqPMSel.id(R.id.vHLine6).gone();

                aqPMSel.id(R.id.llMotherAdd).visible();
                aqPMSel.id(R.id.llFirstPreg).visible();
                aqPMSel.id(R.id.llnote).visible();
                aqPMSel.id(R.id.vHLine5).visible();
                //				19Nov2019 Arpitha
                aqPMSel.id(R.id.llancinstruction).gone();
                aqPMSel.id(R.id.llpncinstruction).visible();
                aqPMSel.id(R.id.llFirstPreg).gone();
                aqPMSel.id(R.id.imgclearlmp).visible();
                break;
            }
            case R.id.radEDD: {

                //	aqPMSel.id(R.id.radEDD).checked(true); //27Nov2018 - Bindu- comment the line
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.deldate)));
                aqPMSel.id(R.id.etMotherAdd).text("");
                aqPMSel.id(R.id.lmp_date).text((getResources().getString(R.string.tvlmp)));// 28Jun2018
                // Arpitha
                // 11July2018 Arpitha
                aqPMSel.id(R.id.etlmpdate).text("");
                aqPMSel.id(R.id.etgest).text("");
                break;
            }
            case R.id.rdbchild: {
                aqPMSel.id(R.id.rdbPreg).checked(false);
                aqPMSel.id(R.id.rdbMother).checked(false);
                aqPMSel.id(R.id.rdbAdol).checked(false);//Ramesh 13-may2021

                //27Nov2018 - Bindu radiogrp clear chk to reset radiobtn
                radGrpEDDorLmp.clearCheck();//Arpitha2021
                aqPMSel.id(R.id.radLMP).checked(false);
                aqPMSel.id(R.id.radEDD).checked(false);
                aqPMSel.id(R.id.tvFirstPregHeading).enabled(false);
                aqPMSel.id(R.id.chkFirstPreg).enabled(false).checked(false);
                aqPMSel.id(R.id.radEDDorLmp).gone();
                aqPMSel.id(R.id.vHLine5).gone();
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.deldate)));
                aqPMSel.id(R.id.etMotherAdd).text("");
                aqPMSel.id(R.id.llMotherAdd).gone();
                aqPMSel.id(R.id.llFirstPreg).gone();
                aqPMSel.id(R.id.llnote).gone();
                // 27Jun2018 Arpitha
                aqPMSel.id(R.id.etregdate).enabled(false);
                aqPMSel.id(R.id.etregdate).textColor(R.color.fontcolor_disable);
                //	aqPMSel.id(R.id.llregdate).background(R.drawable.edittext_disable);
                aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());//21March2019 Arpitha
                aqPMSel.id(R.id.lllmpdate).gone();
                aqPMSel.id(R.id.llgest).gone();
                aqPMSel.id(R.id.vHLine4).gone();
                aqPMSel.id(R.id.vHLine6).gone();
                break;
            }
            case R.id.radLMP: {
                //	aqPMSel.id(R.id.radLMP).checked(true); //27Nov2018 - Bindu- comment the line
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.tvlmp)));
                aqPMSel.id(R.id.etMotherAdd).text("");
                aqPMSel.id(R.id.lmp_date).text((getResources().getString(R.string.tvedd)));// 28Jun2018
                // Arpitha

                // 11July2018 Arpitha
                aqPMSel.id(R.id.etlmpdate).text("");
                aqPMSel.id(R.id.etgest).text("");
                break;
            }

            case R.id.gridView1:
                break;
            case R.id.rdbAdol://Ramesh 13-may-2021
                aqPMSel.id(R.id.rdbPreg).checked(false);
                aqPMSel.id(R.id.rdbMother).checked(false);
                aqPMSel.id(R.id.rdbchild).checked(false);

                radGrpEDDorLmp.clearCheck();//Arpitha2021
                aqPMSel.id(R.id.radLMP).checked(false);
                aqPMSel.id(R.id.radEDD).checked(false);
                aqPMSel.id(R.id.tvFirstPregHeading).enabled(false);
                aqPMSel.id(R.id.chkFirstPreg).enabled(false).checked(false);
                aqPMSel.id(R.id.radEDDorLmp).gone();
                aqPMSel.id(R.id.vHLine5).gone();
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.deldate)));
                aqPMSel.id(R.id.etMotherAdd).text("");
                aqPMSel.id(R.id.llMotherAdd).gone();
                aqPMSel.id(R.id.llFirstPreg).gone();
                aqPMSel.id(R.id.llnote).gone();
                aqPMSel.id(R.id.etregdate).enabled(false);
                aqPMSel.id(R.id.etregdate).textColor(R.color.fontcolor_disable);
                aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());//21March2019 Arpitha
                aqPMSel.id(R.id.lllmpdate).gone();
                aqPMSel.id(R.id.llgest).gone();
                aqPMSel.id(R.id.vHLine4).gone();
                aqPMSel.id(R.id.vHLine6).gone();
                break;
            default: {
                break;
            }
        }
    }
}