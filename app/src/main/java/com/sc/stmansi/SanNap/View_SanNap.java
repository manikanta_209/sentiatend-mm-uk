package com.sc.stmansi.SanNap;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.R;
import com.sc.stmansi.adolescent.AdolDeactivation;
import com.sc.stmansi.adolescent.AdolParticipatedTrainings;
import com.sc.stmansi.adolescent.AdolRegistration;
import com.sc.stmansi.adolescent.AdolVisitHistory;
import com.sc.stmansi.adolescent.AdolVisit_New;
import com.sc.stmansi.common.AlertDialogUtil;
import com.sc.stmansi.common.NavDrawerItem;
import com.sc.stmansi.common.NavDrawerListAdapter;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.configuration.SyncState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.repositories.AdolescentRepository;
import com.sc.stmansi.repositories.FacilityRepository;
import com.sc.stmansi.repositories.SanNapHeaderRepository;
import com.sc.stmansi.repositories.SanNapRepository;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblAdolReg;
import com.sc.stmansi.tables.tblSanNap;
import com.sc.stmansi.tables.tblSanNapHeader;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class View_SanNap extends AppCompatActivity {

    private AppState appState;
    private SyncState syncState;
    private String AdolId;
    private AQuery aq;
    private DatabaseHelper databaseHelper;
    private TblInstusers user;
    private tblAdolReg currentAdolDetails;
    private int transId;
    private tblSanNapHeader currentSanNapHeader;
    private List<tblSanNap> currentSanNaps;
    private String KindofNKins = "";

    private DrawerLayout drawer;
    private ListView drawerList;
    private Bundle bundle;

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(View_SanNap.this, List_SanNap.class);
        intent.putExtra("adolID", AdolId);
        intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_sannap);
        try {
            bundle = getIntent().getBundleExtra("globalState");
            if (bundle != null) {
                appState = bundle.getParcelable("appState");
                syncState = bundle.getParcelable("syncState");
            } else {
                appState = new AppState();
            }
            AdolId = getIntent().getStringExtra("adolID");
            transId = getIntent().getIntExtra("transId", 0);
            Initialze();
            InitializeDrawer();
            getData();
            UpdateUI();
            aq.id(R.id.toolbarBtnHome).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        displayDialogExit();
                    } catch (Exception e) {
                        e.printStackTrace();
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }
                }
            });
            aq.id(R.id.btncancel).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        displayDialogExit();
                    } catch (Exception e) {
                        e.printStackTrace();
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("SetTextI18n")
    private void UpdateUI() {
        try {
            aq.id(R.id.txtSN_View_AdolName).getTextView().setText(currentAdolDetails.getRegAdolName());
            aq.id(R.id.txtSN_View_AdolAge).getTextView().setText(currentAdolDetails.getRegAdolAge() + getResources().getString(R.string.yearstxt));

            FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
            Map<String, Integer> facilities = facilityRepository.getPlaceMap();
            ArrayList<String> villageList = new ArrayList<String>();
            villageList.add(getResources().getString(R.string.selectType));
            for (Map.Entry<String, Integer> village : facilities.entrySet())
                villageList.add(village.getKey());
            ArrayAdapter<String> VillageAdapter = new ArrayAdapter<>(this,
                    R.layout.simple_spinner_dropdown_item, villageList);
            VillageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            int villagecode = getKey(facilities, String.valueOf(currentAdolDetails.getRegAdolFacilities()));

            aq.id(R.id.txtSN_View_AdolVillageName).getTextView().setText(VillageAdapter.getItem(villagecode));
            aq.id(R.id.txtSN_View_NameofBlock).getTextView().setText(currentSanNapHeader.getTblSNBlockName()!=null && currentSanNapHeader.getTblSNBlockName().length()>0?currentSanNapHeader.getTblSNBlockName():"-");
            aq.id(R.id.txtSN_View_GTS).getTextView().setText(currentSanNapHeader.getTblSNAdolGTS().equals("Yes") ? getResources().getString(R.string.m121) : getResources().getString(R.string.m122));

            aq.id(R.id.txtSN_View_providedDate).getTextView().setText(currentSanNapHeader.getTblSNDateofSNProvided());
            aq.id(R.id.txtSN_View_FeedbackDate).getTextView().setText(currentSanNapHeader.getTblSNDateofFeedback());
            aq.id(R.id.txtSN_View_FieldStaff).getTextView().setText(currentSanNapHeader.getTblSNFieldStaff().length() > 0 ? currentSanNapHeader.getTblSNFieldStaff() : "-");

            for (tblSanNap s : currentSanNaps) {
                if (s.getSanNapId().equals("400")) {
                    String temp = s.getSanNapValue();
                    if (temp.equals("Yes"))
                        aq.id(R.id.txtSN_View_ProvidedbyProgram).getTextView().setText(getResources().getString(R.string.m121));
                    else {
                        aq.id(R.id.txtSN_View_ProvidedbyProgram).getTextView().setText(getResources().getString(R.string.m122));
                    }
                } else if (s.getSanNapId().equals("401")) {
                    aq.id(R.id.tr_Sn_ProvidedbyProgramReason).getView().setVisibility(View.VISIBLE);
                    String[] providedproblemreason = getResources().getStringArray(R.array.providedbyprogramreason);
                    if (!s.getSanNapValue().equals("0")) {
                        if (!s.getSanNapValue().equals("5")) {
                            aq.id(R.id.txtSN_View_ProvidedbyProgram_Reason).getTextView().setText(providedproblemreason[Integer.parseInt(s.getSanNapValue())]);
                        } else {
                            aq.id(R.id.txtSN_View_ProvidedbyProgram_Reason).getTextView().setText(getResources().getString(R.string.others)+s.getSanNapComments());
                        }
                    } else {
                        aq.id(R.id.txtSN_View_ProvidedbyProgram_Reason).getTextView().setText("-");
                    }
                } else if (s.getSanNapId().equals("402")) {
                    if (s.getSanNapValue().equals("Reusable")) {
                        aq.id(R.id.txtSN_View_KindofProvidedNkins).getTextView().setText(getResources().getString(R.string.reusable));
                        KindofNKins = "Reusable";
                    } else {
                        aq.id(R.id.txtSN_View_KindofProvidedNkins).getTextView().setText(getResources().getString(R.string.disposable));
                        KindofNKins = "Disposable";
                    }
                } else if (s.getSanNapId().equals("403")) {
                    if (s.getSanNapValue().length() > 0) {
                        aq.id(R.id.txtSN_View_HowManyProvided).getTextView().setText(s.getSanNapValue());
                    } else {
                        aq.id(R.id.txtSN_View_HowManyProvided).getTextView().setText("-");
                    }
                } else if (s.getSanNapId().equals("404")) {
                    String[] beforeNkins = getResources().getStringArray(R.array.usedtomangerbeforesn);
                    if (!s.getSanNapValue().equals("0")) {
                        if (!s.getSanNapValue().equals("5"))
                            aq.id(R.id.txtSN_View_UsedtoManagerBeforeSN).getTextView().setText(beforeNkins[Integer.parseInt(s.getSanNapValue())]);
                        else
                            aq.id(R.id.txtSN_View_UsedtoManagerBeforeSN).getTextView().setText(getResources().getString(R.string.others)+s.getSanNapComments());
                    } else {
                        aq.id(R.id.txtSN_View_UsedtoManagerBeforeSN).getTextView().setText("-");
                    }
                }
                if (KindofNKins.equals("Reusable")) {
                    aq.id(R.id.ll_SanNap_View_Reusable).getView().setVisibility(View.VISIBLE);
                    if (s.getSanNapId().equals("405")) {
                        if (s.getSanNapValue() != null) {
                            switch (s.getSanNapValue()) {
                                case "1":
                                    aq.id(R.id.txtSN_View_Reuse_QofNkins).getTextView().setText(getResources().getString(R.string.good));
                                    break;
                                case "2":
                                    aq.id(R.id.txtSN_View_Reuse_QofNkins).getTextView().setText(getResources().getString(R.string.fair));
                                    break;
                                case "3":
                                    aq.id(R.id.txtSN_View_Reuse_QofNkins).getTextView().setText(getResources().getString(R.string.poor));
                                    break;
                            }
                        } else {
                            aq.id(R.id.txtSN_View_Reuse_QofNkins).getTextView().setText("-");
                        }
                    } else if (s.getSanNapId().equals("406")) {
                        aq.id(R.id.txtSN_View_Informedhowtouseandwash).getTextView().setText(s.getSanNapValue().equals("Yes") ? getResources().getString(R.string.m121) : getResources().getString(R.string.m122));
                    } else if (s.getSanNapId().equals("407")) {
                        if (s.getSanNapValue() != null)
                            aq.id(R.id.txtSN_View_FeelusingAboutNkins).getTextView().setText(s.getSanNapValue().equals("Easy") ? getResources().getString(R.string.easy) :s.getSanNapValue().equals("Diffcult")?getResources().getString(R.string.diffcult):"-");
                        else
                            aq.id(R.id.txtSN_View_FeelusingAboutNkins).getTextView().setText("-");
                    } else if (s.getSanNapId().equals("408")) {
                        aq.id(R.id.txtSN_View_Usingall5Pads).getTextView().setText(s.getSanNapValue().equals("Yes") ? getResources().getString(R.string.m121) : getResources().getString(R.string.m122));
                    } else if (s.getSanNapId().equals("409")) {
                        aq.id(R.id.tr_Sn_HowManySNDoYouUse).getView().setVisibility(View.VISIBLE);
                        if (s.getSanNapValue().length() > 0) {
                            aq.id(R.id.txtSN_View_HowmanyUsingall5Pads).getTextView().setText(s.getSanNapValue());
                        } else {
                            aq.id(R.id.txtSN_View_HowmanyUsingall5Pads).getTextView().setText("-");
                        }
                    } else if (s.getSanNapId().equals("410")) {
                        aq.id(R.id.txtSN_View_SharingwithFamily).getTextView().setText(s.getSanNapValue().equals("Yes") ? getResources().getString(R.string.m121) : getResources().getString(R.string.m122));
                    } else if (s.getSanNapId().equals("411")) {
                        aq.id(R.id.txtSN_View_getRashesorAllergies).getTextView().setText(s.getSanNapValue().equals("Yes") ? getResources().getString(R.string.m121) : getResources().getString(R.string.m122));
                    } else if (s.getSanNapId().equals("412")) {
                        aq.id(R.id.txtSN_View_AbletoAbsorbBlood).getTextView().setText(s.getSanNapValue().equals("Yes") ? getResources().getString(R.string.m121) : getResources().getString(R.string.m122));
                    } else if (s.getSanNapId().equals("413")) {
                        if (!s.getSanNapValue().equals("0")) {
                            String[] usednapkins = getResources().getStringArray(R.array.usednapkins);
                            aq.id(R.id.txtSN_View_ActionwithUsedNkins).getTextView().setText(usednapkins[Integer.parseInt(s.getSanNapValue())]);
                        } else {
                            aq.id(R.id.txtSN_View_ActionwithUsedNkins).getTextView().setText("-");
                        }
                    } else if (s.getSanNapId().equals("414")) {
                        aq.id(R.id.txtSN_View_ProblemswhileWashingorDrying).getTextView().setText(s.getSanNapValue().equals("Yes") ? getResources().getString(R.string.m121) : getResources().getString(R.string.m122));
                    } else if (s.getSanNapId().equals("415")) {
                        aq.id(R.id.tr_Sn_WhatKindofProblemwhenWashadnDry).getView().setVisibility(View.VISIBLE);
                        String[] kindofproblems = getResources().getStringArray(R.array.kindofproblemwhenwashanddry);
                        if (!s.getSanNapValue().equals("4")) {
                            aq.id(R.id.txtSN_View_KindofProblemswhenWashorDry).getTextView().setText(kindofproblems[Integer.parseInt(s.getSanNapValue())]);
                        } else {
                            aq.id(R.id.txtSN_View_KindofProblemswhenWashorDry).getTextView().setText(getResources().getString(R.string.others)+s.getSanNapComments());
                        }
                    } else if (s.getSanNapId().equals("416")) {
                        aq.id(R.id.txtSN_View_UsedNkinsafterWashorDry).getTextView().setText(s.getSanNapValue().equals("Yes") ? getResources().getString(R.string.m121) : getResources().getString(R.string.m122));
                    } else if (s.getSanNapId().equals("417")) {
                        aq.id(R.id.txtSN_View_ImpactonQuality).getTextView().setText(s.getSanNapValue().equals("Yes") ? getResources().getString(R.string.m121) : getResources().getString(R.string.m122));
                    } else if (s.getSanNapId().equals("418")) {
                        String[] impactkins = getResources().getStringArray(R.array.impacttypeonqofnkins);
                        if (!s.getSanNapValue().equals("0")) {
                            if (!s.getSanNapValue().equals("5"))
                                aq.id(R.id.txtSN_View_TypeofImpactonQuality).getTextView().setText(impactkins[Integer.parseInt(s.getSanNapValue())]);
                            else
                                aq.id(R.id.txtSN_View_TypeofImpactonQuality).getTextView().setText(getResources().getString(R.string.others)+s.getSanNapComments());
                        } else {
                            aq.id(R.id.txtSN_View_TypeofImpactonQuality).getTextView().setText("-");
                        }
                    } else if (s.getSanNapId().equals("419")) {
                        aq.id(R.id.txtSN_View_UseSameNkinsinFuture).getTextView().setText(s.getSanNapValue().equals("Yes") ? getResources().getString(R.string.m121) : getResources().getString(R.string.m122));
                    } else if (s.getSanNapId().equals("420")) {
                        aq.id(R.id.tr_sn_WhynotReuseNkins).getView().setVisibility(View.VISIBLE);
                        String[] reasonnotReuse = getResources().getStringArray(R.array.reasonnotresusingnkins);
                        if (!s.getSanNapValue().equals("5"))
                            aq.id(R.id.txtSN_View_WhynotReusedKins).getTextView().setText(reasonnotReuse[Integer.parseInt(s.getSanNapValue())]);
                        else
                            aq.id(R.id.txtSN_View_WhynotReusedKins).getTextView().setText(getResources().getString(R.string.others)+s.getSanNapComments());
                    } else if (s.getSanNapId().equals("421")) {
                        aq.id(R.id.txtSN_View_RecommendtoyourFriend).getTextView().setText(s.getSanNapValue().equals("Yes") ? getResources().getString(R.string.m121) : getResources().getString(R.string.m122));
                    }
                } else if (KindofNKins.equals("Disposable")) {
                    aq.id(R.id.ll_SanNap_View_Disposal).getView().setVisibility(View.VISIBLE);
                    if (s.getSanNapId().equals("422")) {
                        if (s.getSanNapValue() != null) {
                            switch (s.getSanNapValue()) {
                                case "1":
                                    aq.id(R.id.txtSN_View_Dispose_QofNkins).getTextView().setText(getResources().getString(R.string.good));
                                    break;
                                case "2":
                                    aq.id(R.id.txtSN_View_Dispose_QofNkins).getTextView().setText(getResources().getString(R.string.fair));
                                    break;
                                case "3":
                                    aq.id(R.id.txtSN_View_Dispose_QofNkins).getTextView().setText(getResources().getString(R.string.poor));
                                    break;
                            }
                        } else {
                            aq.id(R.id.txtSN_View_Dispose_QofNkins).getTextView().setText("-");
                        }
                    } else if (s.getSanNapId().equals("423")) {
                        aq.id(R.id.txtSN_View_Informedhowtouseanddispose).getTextView().setText(s.getSanNapValue().equals("Yes") ? getResources().getString(R.string.m121) : getResources().getString(R.string.m122));
                    } else if (s.getSanNapId().equals("424")) {
                        if (s.getSanNapValue() != null) {
                            aq.id(R.id.txtSN_View_dispose_FeelusingAboutNkins).getTextView().setText(s.getSanNapValue().equals("Easy") ? getResources().getString(R.string.easy) : getResources().getString(R.string.diffcult));
                        } else {
                            aq.id(R.id.txtSN_View_dispose_FeelusingAboutNkins).getTextView().setText("-");
                        }
                    } else if (s.getSanNapId().equals("425")) {
                        aq.id(R.id.txtSN_View_dispose_Usingall5Pads).getTextView().setText(s.getSanNapValue().equals("Yes") ? getResources().getString(R.string.m121) : getResources().getString(R.string.m122));
                    } else if (s.getSanNapId().equals("426")) {
                        aq.id(R.id.tr_Sn_HowManySNDoYouUse).getView().setVisibility(View.VISIBLE);
                        aq.id(R.id.txtSN_View_dispose_HowmanyUsingall5Pads).getTextView().setText(s.getSanNapValue().equals("per Day") ? getResources().getString(R.string.perday) : getResources().getString(R.string.wholemenscycle));
                    } else if (s.getSanNapId().equals("427")) {
                        aq.id(R.id.txtSN_View_dispose_SharingwithFamily).getTextView().setText(s.getSanNapValue().equals("Yes") ? getResources().getString(R.string.m121) : getResources().getString(R.string.m122));
                    } else if (s.getSanNapId().equals("428")) {
                        aq.id(R.id.txtSN_View_dispose_getRashesorAllergies).getTextView().setText(s.getSanNapValue().equals("Yes") ? getResources().getString(R.string.m121) : getResources().getString(R.string.m122));
                    } else if (s.getSanNapId().equals("429")) {
                        aq.id(R.id.txtSN_View_dispose_AbletoAbsorbBlood).getTextView().setText(s.getSanNapValue().equals("Yes") ? getResources().getString(R.string.m121) : getResources().getString(R.string.m122));
                    } else if (s.getSanNapId().equals("430")) {
                        String[] usednapkins_disposal = getResources().getStringArray(R.array.usednapkins_disposal);
                        if (!s.getSanNapValue().equals("0")) {
                            if (!s.getSanNapValue().equals("5")) {
                                aq.id(R.id.txtSN_View_dispose_ActionwithUsedNkins).getTextView().setText(usednapkins_disposal[Integer.parseInt(s.getSanNapValue())]);
                            } else {
                                aq.id(R.id.txtSN_View_dispose_ActionwithUsedNkins).getTextView().setText(getResources().getString(R.string.others)+s.getSanNapComments());
                            }
                        } else {
                            aq.id(R.id.txtSN_View_dispose_ActionwithUsedNkins).getTextView().setText("-");
                        }
                    } else if (s.getSanNapId().equals("431")) {
                        aq.id(R.id.txtSN_View_dispose_ProblemswhileWashingorDrying).getTextView().setText(s.getSanNapValue().equals("Yes") ? getResources().getString(R.string.m121) : getResources().getString(R.string.m122));
                    } else if (s.getSanNapId().equals("432")) {
                        aq.id(R.id.tr_Sn_WhatKindofProblemwhenWashadnDry).getView().setVisibility(View.VISIBLE);
                        String[] kindofproblemwhenwashanddry = getResources().getStringArray(R.array.kindofproblemwhenwashanddry);
                        if (!s.getSanNapValue().equals("4")) {
                            aq.id(R.id.txtSN_View_dispose_KindofProblemswhenWashorDry).getTextView().setText(kindofproblemwhenwashanddry[Integer.parseInt(s.getSanNapValue())]);
                        } else {
                            aq.id(R.id.txtSN_View_dispose_KindofProblemswhenWashorDry).getTextView().setText(getResources().getString(R.string.others)+s.getSanNapComments());
                        }
                    } else if (s.getSanNapId().equals("433")) {
                        aq.id(R.id.txtSN_View_dispose_UseSameNkinsinFuture).getTextView().setText(s.getSanNapValue().equals("Yes") ? getResources().getString(R.string.m121) : getResources().getString(R.string.m122));
                    } else if (s.getSanNapId().equals("434")) {
                        aq.id(R.id.tr_sn_disposal_WhynotReuseNkins).getView().setVisibility(View.VISIBLE);
                        String[] reasonnotdisposalnkins = getResources().getStringArray(R.array.reasonnotdisposalnkins);
                        if (!s.getSanNapValue().equals("0")) {
                            if (!s.getSanNapValue().equals("7")) {
                                aq.id(R.id.txtSN_View_dispose_WhynotReusedKins).getTextView().setText(reasonnotdisposalnkins[Integer.parseInt(s.getSanNapValue())]);
                            } else {
                                aq.id(R.id.txtSN_View_dispose_WhynotReusedKins).getTextView().setText(getResources().getString(R.string.others)+s.getSanNapComments());
                            }
                        } else {
                            aq.id(R.id.txtSN_View_dispose_WhynotReusedKins).getTextView().setText("-");
                        }
                    } else if (s.getSanNapId().equals("435")) {
                        aq.id(R.id.txtSN_View_dispose_RecommendtoyourFriend).getTextView().setText(s.getSanNapValue().equals("Yes") ? getResources().getString(R.string.m121) : getResources().getString(R.string.m122));
                    }

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private void getData() {
        try {
            aq.id(R.id.toolbartxtTitle).getTextView().setText(getResources().getString(R.string.feedbackfromsummary));

            AdolescentRepository adolescentRepository = new AdolescentRepository(databaseHelper);
            currentAdolDetails = adolescentRepository.getAdolDatafromAdolID(AdolId);

            SanNapHeaderRepository sanNapHeaderRepository = new SanNapHeaderRepository(databaseHelper);
            currentSanNapHeader = sanNapHeaderRepository.getHeaderbasedonAdolIDandTransID(AdolId, transId);

            SanNapRepository sanNapRepository = new SanNapRepository(databaseHelper);
            currentSanNaps = sanNapRepository.getSanNapDetails(AdolId, transId);

        } catch (Exception e) {
            e.printStackTrace();
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private void Initialze() {
        databaseHelper = getHelper();
        aq = new AQuery(this);
        getSupportActionBar().hide();
    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(getApplicationContext(), DatabaseHelper.class);
        }
        return databaseHelper;
    }

    private void displayDialogExit() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(View_SanNap.this);
        builder.setMessage(getResources().getString(R.string.exit));
        builder.setCancelable(false);
        builder.setNegativeButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(View_SanNap.this, List_SanNap.class);
                intent.putExtra("adolID", AdolId);
                intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                startActivity(intent);
            }
        }).setPositiveButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    public static int getKey(Map<String, Integer> mapref, String value) {

        int pos = 0;
        for (Map.Entry<String, Integer> map : mapref.entrySet()) {
            pos++;
            if (map.getValue().toString().equals(value)) {
                return pos;
            }

        }
        return pos;
    }

    private void InitializeDrawer() {
        try {
            drawer = findViewById(R.id.drawer_adolView_Sannap);
            drawerList = findViewById(R.id.lvNav_adolView_Sannap);

            ArrayList<NavDrawerItem> navDrawerItems = new ArrayList<>();
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.editadolescent), R.drawable.ic_edit_icon));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.homevisitadd), R.drawable.ic_homevisit));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.adolvisithistory), R.drawable.anm_pending_activities));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.trainingparticipated), R.drawable.presentation));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.mhmformlist), R.drawable.form_icon));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.mhmformadd), R.drawable.form_icon));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deactivate), R.drawable.deactivate));
            drawer.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
            drawerList.setAdapter(new NavDrawerListAdapter(getApplicationContext(), navDrawerItems));
            drawerList.setOnItemClickListener(new DrawerItemClickListener());
            Toolbar toolbar = findViewById(R.id.toolbar_View_Sannap);
            ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close);
            drawer.setDrawerListener(drawerToggle);
            drawerToggle.syncState();
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

    private class DrawerItemClickListener implements android.widget.AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            drawer.closeDrawer(drawerList);
            try {
                switch (i) {
                    case 0:
                        Intent intent = new Intent(View_SanNap.this, AdolRegistration.class);
                        intent.putExtra("globalState", bundle);
                        intent.putExtra("isEdit", true);
                        intent.putExtra("adolID", AdolId);
                        startActivity(intent);
                        break;
                    case 1:
                        if (currentAdolDetails.getRegAdolDeactivateDate().length() != 0) {
                            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.adolescentdeactivated), View_SanNap.this);
                            drawer.closeDrawer(GravityCompat.END);
                        } else {
                            intent = new Intent(View_SanNap.this, AdolVisit_New.class);
                            intent.putExtra("globalState", bundle);
                            intent.putExtra("adolID", AdolId);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                        break;
                    case 2:
                        intent = new Intent(View_SanNap.this, AdolVisitHistory.class);
                        intent.putExtra("globalState", bundle);
                        intent.putExtra("adolID", AdolId);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        break;
                    case 3:
                        intent = new Intent(View_SanNap.this, AdolParticipatedTrainings.class);
                        intent.putExtra("globalState", bundle);
                        intent.putExtra("adolID", AdolId);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        break;
                    case 4:
                        intent = new Intent(view.getContext(), List_SanNap.class);
                        intent.putExtra("globalState", bundle);
                        intent.putExtra("adolID", AdolId);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        break;
                    case 5:
                        intent = new Intent(view.getContext(), Add_SanNap.class);
                        intent.putExtra("globalState", bundle);
                        intent.putExtra("adolID", AdolId);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        break;
                    case 6:
                        intent = new Intent(View_SanNap.this, AdolDeactivation.class);
                        intent.putExtra("globalState", bundle);
                        if (currentAdolDetails.getRegAdolDeactivateDate().length() != 0) {
                            intent.putExtra("isView", true);
                        }
                        intent.putExtra("adolID", AdolId);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        break;
                }
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());

            }
        }
    }
}