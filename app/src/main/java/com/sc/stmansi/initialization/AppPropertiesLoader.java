package com.sc.stmansi.initialization;

import android.content.Context;

import com.sc.stmansi.configuration.AppState;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class AppPropertiesLoader {

	public static void loadProperties(Context context, AppState appState) {
		try (InputStream in = context.getAssets().open("stend.properties")){
			Properties p = new Properties();
			p.load(in);

			appState.idleTimeOut = Integer.parseInt(p.getProperty("idleTimeOut"));
			appState.webServiceIpAddress = p.getProperty("ipAddress");
			appState.webServiceDBbMster = p.getProperty("serverdbmaster");
			appState.webServicePath = p.getProperty("serverURL");
			appState.apiPort = Integer.parseInt(p.getProperty("apiPort"));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
