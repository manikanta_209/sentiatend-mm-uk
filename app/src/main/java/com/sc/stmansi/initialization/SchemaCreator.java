package com.sc.stmansi.initialization;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.sc.stmansi.configuration.AppState;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class SchemaCreator {

	private SQLiteDatabase sqLiteDatabase;

	public SchemaCreator(SQLiteDatabase sqLiteDatabase) {
		this.sqLiteDatabase = sqLiteDatabase;
	}

	public static void createDirectories(AppState appState) {
		new File(appState.dbDirRef).mkdirs();
		new File(appState.logDirRef).mkdirs();
		new File(appState.reportDirRef).mkdirs();
		new File(appState.imgDirRef).mkdirs();
	}

	public void createSchema(Context context) throws IOException {
		int count = 0;

		try (InputStream in = context.getResources().getAssets().open("sentiatend1.sql");
			 ByteArrayOutputStream out = new ByteArrayOutputStream()) {
			byte[] buffer = new byte[4096];
			while (true) {
				int read = in.read(buffer);
				if (read == -1) {
					break;
				}
				out.write(buffer, 0, read);
			}

			byte[] bytes = out.toByteArray();
			String sql = new String(bytes, StandardCharsets.UTF_8);
			String[] lines = sql.split(";(\\s)*[\n\r]");

			sqLiteDatabase.beginTransaction();

			for (String line : lines) {
				line = line.trim();
				if (line.length() > 0) {
					sqLiteDatabase.execSQL(line);
					count++;
				}
			}
			sqLiteDatabase.setTransactionSuccessful();
			sqLiteDatabase.endTransaction();
		}
		System.out.println("Executed " + count + " statements from SQL script eJananiCreateStats_Client.txt ");
		System.out.println("statements excuted are: " + count);
	}
}
