package com.sc.stmansi.pendingservices;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sc.stmansi.R;

public class WomenAwaitingViewHolder extends RecyclerView.ViewHolder {

    ImageView photoView, starView;
    TextView description, dueDateMin;

    public WomenAwaitingViewHolder(@NonNull View itemView) {
        super(itemView);
        photoView = itemView.findViewById(R.id.imgWomanphoto);
        starView = itemView.findViewById(R.id.ivStar);
        description = itemView.findViewById(R.id.Woman);
        dueDateMin = itemView.findViewById(R.id.duedatemin);

    }
}
