package com.sc.stmansi.pendingservices;

import com.sc.stmansi.womanlist.ActionBarVillageSpinnerItem;

import java.util.List;

public interface VillageChangedListener {

    void onVillageChanged(List<ActionBarVillageSpinnerItem> spinnerItems, int selectedItemPosition);
}
