//28Aug2019 Arpitha - code clean up
package com.sc.stmansi.pendingservices;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class PendingUpcomingTabsAdapter extends FragmentPagerAdapter {

    PendingUpcomingTabsAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int index) {
        try {
            switch (index) {
                case 0:
                    return new PendingFragment();
                case 1:
                    return new UpcomingFragment();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

}
