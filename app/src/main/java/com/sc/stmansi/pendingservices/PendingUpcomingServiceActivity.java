//28Aug2019 Arpitha - code clean up
package com.sc.stmansi.pendingservices;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.About.AboutActivity;
import com.sc.stmansi.common.ActivitiesStack;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.repositories.FacilityRepository;
import com.sc.stmansi.repositories.LoginRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.womanlist.ActionBarVillageSpinnerItem;
import com.sc.stmansi.womanlist.CustomVillageSpinnerAdapter;
import com.sc.stmansi.womanlist.RegisteredWomenActionTabs;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.sc.stmansi.R;

public class PendingUpcomingServiceActivity extends AppCompatActivity implements
        androidx.appcompat.app.ActionBar.TabListener, ActionBar.OnNavigationListener {

    private ViewPager viewPager;
    private PendingUpcomingTabsAdapter mAdapter;

    private AQuery aq;
    private ArrayList<ActionBarVillageSpinnerItem> vItems;
    private boolean spinnerSelectionChanged;

    private TblInstusers user;
    private DatabaseHelper databaseHelper;
    private AppState appState;

    @Override
    public void onTabSelected(androidx.appcompat.app.ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        viewPager.setCurrentItem(tab.getPosition());

    }

    @Override
    public void onTabUnselected(androidx.appcompat.app.ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabReselected(androidx.appcompat.app.ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigation_tabs);
        try {
            databaseHelper = getHelper();
            Bundle bundle = getIntent().getBundleExtra("globalState");
            appState = bundle.getParcelable("appState");

            UserRepository userRepository = new UserRepository(databaseHelper);
            user = userRepository.getOneAuditedUser(appState.ashaId);
            aq = new AQuery(this);

            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
            getSupportActionBar().setCustomView(R.layout.ejanani_village_filter);
            getSupportActionBar().setDisplayShowCustomEnabled(true);

            // Initialization
            viewPager = findViewById(R.id.pager);
            viewPager.setOffscreenPageLimit(3);
            aq.id(R.id.llWomanFilter).gone();
            mAdapter = new PendingUpcomingTabsAdapter(getSupportFragmentManager());
            viewPager.setAdapter(mAdapter);

            String[] tabs = {getResources().getString(R.string.pending), getResources().getString(R.string.upcoming)};

            // Adding Tabs
            for (String tab_name : tabs) {
                getSupportActionBar().addTab(getSupportActionBar().newTab().setText(tab_name).setTabListener(this)
                );
            }
            /**
             * on swiping the viewpager make respective tab selected
             * */
            viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                @Override
                public void onPageSelected(int position) {
                    // on changing the page
                    // make respected tab selected
                    getSupportActionBar().setSelectedNavigationItem(position);
                }

                @Override
                public void onPageScrolled(int arg0, float arg1, int arg2) {
                }

                @Override
                public void onPageScrollStateChanged(int arg0) {
                }
            });
            populateSpinnerVillage();
            aq.id(R.id.spVillageFilter).itemSelected(this, "selectVillage");
        } catch (SQLException e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * populate Village to Spinner
     */
    private void populateSpinnerVillage() throws SQLException {
        FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
        Map<String, Integer> MapVillage = facilityRepository.getPlaceMap();
        vItems = new ArrayList<>();
        vItems.add(new ActionBarVillageSpinnerItem(getResources().getString(R.string.all_Womans), 0));
        for (Map.Entry<String, Integer> village : MapVillage.entrySet()) {
            vItems.add(new ActionBarVillageSpinnerItem(village.getKey(), village.getValue()));
        }
        CustomVillageSpinnerAdapter villageAdapter = new CustomVillageSpinnerAdapter(getApplicationContext(), R.layout.actionbar_spinner_list_item, vItems);
        aq.id(R.id.spVillageFilter).adapter(villageAdapter);
    }

    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        return b;
    }

    /**
     * This method invokes on Spinner Clicks
     */
    public void selectVillage(AdapterView<?> adapter, View v, int position, long arg3) {
        appState.selectedVillagePosition = position;
        if(spinnerSelectionChanged) {
            List<Fragment> fragments = getSupportFragmentManager().getFragments();
            for (Fragment f : fragments) {
                VillageChangedListener listener = (VillageChangedListener) f;
                listener.onVillageChanged(vItems, position);
            }
        }
        spinnerSelectionChanged = true;
    }

    /**
     * Actionbar navigation item select listener
     */
    @Override
    public boolean onNavigationItemSelected(int itemPosition, long itemId) {
        try {
            ActionBarVillageSpinnerItem selectedVillage = vItems.get(itemPosition);
            appState.selectedVillageCode = selectedVillage.getVillageCode();

            mAdapter = new PendingUpcomingTabsAdapter(getSupportFragmentManager());
            invalidateOptionsMenu();
            String[] tabs = {getResources().getString(R.string.pending),
                    getResources().getString(R.string.upcoming)};
            for (String tab_name : tabs) {
                getSupportActionBar().addTab(getSupportActionBar().newTab().setText(tab_name)
                        .setTabListener(this));
            }
            return true;
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            return false;
        }
    }

    /**
     * Initializes the Action Bar items
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.servicesmenu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Called whenever an item clicked in actionbar
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action buttons
        switch (item.getItemId()) {
            case R.id.home: {
                Intent goToScreen = new Intent(PendingUpcomingServiceActivity.this, MainMenuActivity.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }
            case android.R.id.home: {
                Intent goToScreen = new Intent(PendingUpcomingServiceActivity.this, MainMenuActivity.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                startActivity(goToScreen);
                return true;
            }
            case R.id.logout: {
                Intent goToScreen = new Intent(PendingUpcomingServiceActivity.this, LoginActivity.class);
                displayAlert(getResources().getString(R.string.m111), goToScreen);
                return true;
            }
            case R.id.wlist: {
                Intent goToScreen = new Intent(PendingUpcomingServiceActivity.this, RegisteredWomenActionTabs.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }
            //30Sep2019 - Bindu
            case R.id.about: {
                Intent goToScreen = new Intent(PendingUpcomingServiceActivity.this, AboutActivity.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * alert message
     */
    private void displayAlert(final String spanText2, final Intent goToScreen) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set dialog message
        alertDialogBuilder
                .setMessage(spanText2)
                .setCancelable(false)
                .setNegativeButton((getResources().getString(R.string.m121)), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (goToScreen != null) {
                            startActivity(goToScreen);
                            if (spanText2.contains("logout")) {
                                try {
                                    LoginRepository loginRepository = new LoginRepository(databaseHelper);
                                    loginRepository.updateLogoutTime(appState.loginAuditId, appState.sessionUserId);
                                } catch (Exception e) {
                                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                                }
                            }
                        } else
                            dialog.cancel();
                    }
                })
                .setPositiveButton((getResources().getString(R.string.m122)), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    /**
     * This method Calls the idle timeout
     */ //09Aug2019 - Bindu
    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        new ActivitiesStack(this).delayedIdle(appState.idleTimeOut);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        /*
         * You'll need this in your class to release the helper when done.
         */
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }
}
