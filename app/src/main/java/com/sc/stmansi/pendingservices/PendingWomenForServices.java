//28Aug2019 Arpitha - code clean up
package com.sc.stmansi.pendingservices;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.About.AboutActivity;
import com.sc.stmansi.HomeVisit.HomeVisitListActivity;
import com.sc.stmansi.common.ActivitiesStack;
import com.sc.stmansi.common.AlertDialogUtil;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.delivery.DeliveryInfoActivity;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.recyclerview.ClickListener;
import com.sc.stmansi.repositories.ChildRepository;
import com.sc.stmansi.repositories.FacilityRepository;
import com.sc.stmansi.repositories.LoginRepository;
import com.sc.stmansi.repositories.TransactionHeaderRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.repositories.WomanServiceRepository;
import com.sc.stmansi.tables.ServiceslistPojo;
import com.sc.stmansi.tables.TblAncTests;
import com.sc.stmansi.tables.TblAncTestsMaster;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.tables.tblserviceslist;
import com.sc.stmansi.womanlist.ActionBarVillageSpinnerItem;
import com.sc.stmansi.womanlist.CustomVillageSpinnerAdapter;
import com.sc.stmansi.womanlist.RegisteredWomenActionTabs;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.sc.stmansi.R;

@SuppressLint({"ClickableViewAccessibility", "InflateParams"})
public class PendingWomenForServices extends AppCompatActivity implements ClickListener, IndicatorViewClickListener {

    private ArrayList<ActionBarVillageSpinnerItem> vItems;
    private TblInstusers user;
    private AQuery aqPendWoman;
    private int tribalHamlet;
    private AlertDialog alertDialog = null;
    private AQuery aqPMSel;
    private tblserviceslist arrVal;
    private Map<String, String> mapFacilityName;
    private ArrayList<String> facilityNameList;
    private int serviceNum;
    private String status;
    private List<String[]> serviceList;
    private String serviceMinDate;
    private DatabaseHelper databaseHelper;
    private AppState appState;
    private static tblregisteredwomen woman;//28Apr2021 Arpitha - made it static to access in ondateset
    private String sType = null;
    private int sType2;
    private RecyclerView recyclerView;
    private List<String[]> services;
    private RecyclerView.Adapter adapter;
    private boolean spinnerSelectionChanged;
    //    10May2021 Arpitha
    Map<Integer, TblAncTests> anctestVal;
    ArrayList<TblAncTests> ancTestsListAdd;
    int rdtestYes = R.id.rdanctestYes;
    int rdtestNo = R.id.rdanctestNo;
    int ettestval = R.id.etanctestval;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ejanani_women_list);

        Bundle bundle = getIntent().getBundleExtra("globalState");
        appState = bundle.getParcelable("appState");

        try {
            databaseHelper = getHelper();

            UserRepository userRepository = new UserRepository(databaseHelper);
            user = userRepository.getOneAuditedUser(appState.ashaId);

            aqPendWoman = new AQuery(this);

            tribalHamlet = getIntent().getIntExtra("tribalHamlet", 0);
            status = getIntent().getStringExtra("status");

            sType = getIntent().getStringExtra("sType");
            sType2 = getIntent().getIntExtra("sType2", 0);

            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setCustomView(R.layout.ejanani_village_filter);
            getSupportActionBar().setDisplayShowCustomEnabled(true);

            aqPendWoman.id(R.id.spVillageFilter).itemSelected(this, "selectVillage");

            if (status.equalsIgnoreCase("Upcoming")) {
               // aqPendWoman.id(R.id.txtfilterdate).text(status + " Service - " + sType);
                int identifier = getResources().getIdentifier(sType.toLowerCase(),
                        "string", "com.sc.stmansi");
                aqPendWoman.id(R.id.txtfilterdate).text(getResources().getString(R.string.upcoming) + " "+ getResources().getString(R.string.service) + " - " + getResources().getString(identifier)); //14Nov2019- Bindu

                aqPendWoman.id(R.id.txtnote).visible();
            } else {
               // aqPendWoman.id(R.id.txtfilterdate).text(status + " Service - " + sType);
                int identifier = getResources().getIdentifier(sType.toLowerCase(),
                        "string", "com.sc.stmansi");
                aqPendWoman.id(R.id.txtfilterdate).text(getResources().getString(R.string.strpending) + " "+ getResources().getString(R.string.service) + " - " + getResources().getString(identifier)); //14Nov2019 - Bindu
                aqPendWoman.id(R.id.txtnote).gone();
            }

            aqPendWoman.id(R.id.imgresetfilter).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        aqPendWoman.id(R.id.spVillageFilter).setSelection(0);
                        tribalHamlet = 0;
                        new AwaitingWomenLoader(PendingWomenForServices.this).execute();

                    } catch (Exception e) {

                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                    }
                }
            });

            populateSpinnerVillage();

            recyclerView = this.findViewById(R.id.women_awaiting_recycler_view);
            recyclerView.setVisibility(View.VISIBLE);
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            recyclerView.setHasFixedSize(true);

            // use a linear layout manager
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(layoutManager);

            services = new ArrayList<>();
            adapter = new PendingWomenServicesAdapter(this, services, this);
            recyclerView.setAdapter(adapter);
            
            new AwaitingWomenLoader(this).execute();


        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }

    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    /**
     * This method invokes on Spinner Clicks
     */
    public void selectVillage(AdapterView<?> adapter, View v, int position, long arg3) {
        switch (adapter.getId()) {
            case R.id.spVillageFilter: {
                try {
                    ActionBarVillageSpinnerItem selectedVillage = vItems.get(position);
                    appState.selectedVillageCode = selectedVillage.getVillageCode();
                    tribalHamlet = appState.selectedVillageCode;
                    sType2 = tribalHamlet;
                    if(spinnerSelectionChanged) {
                        new AwaitingWomenLoader(this).execute();
                    }
                    spinnerSelectionChanged = true;

                    if(position==0) {
                        aqPendWoman.id(R.id.imgresetfilter).enabled(false);
                        aqPendWoman.id(R.id.imgresetfilter).background(R.drawable.ic_reset_gray);
                    }
                    else {
                        aqPendWoman.id(R.id.imgresetfilter).enabled(true);
                        aqPendWoman.id(R.id.imgresetfilter).background(R.drawable.ic_reset);
                    }
                } catch (Exception e) {
                    
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

                }
                break;
            }
            default:
                break;
        }
    }

    /**
     * Initializes the Action Bar items
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        try {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.servicesmenu, menu);
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * populate Village to Spinner
     */
    private void populateSpinnerVillage() {
        try {
            FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
            Map<String, Integer> MapVillage = facilityRepository.getPlaceMap();

            vItems = new ArrayList<>();
            vItems.add(new ActionBarVillageSpinnerItem((getResources().getString(R.string.all_Womans)), 0));
            for (Entry<String, Integer> village : MapVillage.entrySet()) {
                vItems.add(new ActionBarVillageSpinnerItem(village.getKey(),
                        village.getValue()));
            }
            CustomVillageSpinnerAdapter villageAdapter = new CustomVillageSpinnerAdapter(getApplicationContext(), R.layout.actionbar_spinner_list_item, vItems);
            aqPendWoman.id(R.id.spVillageFilter).adapter(villageAdapter);

            aqPendWoman.id(R.id.spVillageFilter).setSelection(appState.selectedVillagePosition);

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    /**
     * This method invokes when item clicked in serviceList View
     */
    public void commonClick(AdapterView<?> adapter, View v, int position, long arg3) {
        try {

          /*  if(user.getIsDeactivated()==1 ||
                    (woman.getDateDeactivated()!=null &&
                            woman.getDateDeactivated().trim().length()>0)) {//26Nov2019 Arpitha

                Toast.makeText(getApplicationContext(),getResources().getString(R.string.userdeactivated),Toast.LENGTH_LONG);

            }else{*/
                if (status.equalsIgnoreCase("Pending") &&
                        sType.equalsIgnoreCase("EDD")) {

                    WomanRepository womanRepository = new WomanRepository(databaseHelper);
                    woman = womanRepository.getRegistartionDetails(services.get(position)[0],appState.sessionUserId);
                    {
                        if (woman!=null && woman.getRegPregnantorMother() == 2 || DateTimeUtil.getNumberOfDaysFromLMP(woman.getRegLMP()) > 210) {

                            Intent nextScreen = new Intent(PendingWomenForServices.this, DeliveryInfoActivity.class);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            nextScreen.putExtra("woman", woman);
                            startActivity(nextScreen);
                        } else
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.m132), Toast.LENGTH_LONG).show();

                    }
                } else {
                    if (status.equalsIgnoreCase("Pending")) {

                        tblregisteredwomen tblreg = new tblregisteredwomen();

                        String[] woman = services.get(position);
                        tblreg.setWomanId(woman[0]);

                        WomanRepository womanRepo = new WomanRepository(databaseHelper);
                        this.woman = womanRepo.getRegistartionDetails(woman[0], appState.sessionUserId);


                        boolean isChildAlive;

                        ChildRepository childRepository = new ChildRepository(databaseHelper);

                        isChildAlive = childRepository.checkIsChildAlive(this.woman.getWomanId());

                        if(user.getIsDeactivated()==1 ||
                                (this.woman.getDateDeactivated()!=null &&
                                        this.woman.getDateDeactivated().trim().length()>0 && !isChildAlive)) {//26Nov2019 Arpitha

                            Toast.makeText(getApplicationContext(),
                                    getResources().getString(R.string.userdeactivated),Toast.LENGTH_LONG);

                        }else{
//                        tblregisteredwomen tblreg = new tblregisteredwomen();
                       /* String[] woman = services.get(position);
                        tblreg.setWomanId(woman[0]);

                        WomanRepository womanRepo = new WomanRepository(databaseHelper);
                        this.woman = womanRepo.getRegistartionDetails(woman[0], appState.sessionUserId);
*/
                        tblreg.setRegWomanName(woman[1]);
                        tblreg.setRegPregnantorMother(Integer.parseInt(woman[5]));
                        tblreg.setRegLMP(woman[4]);
                        tblreg.setRegADDate(woman[6]);
                        tblreg.setRegriskFactors(woman[8]);
                        tblreg.setRegComplicatedpreg(Integer.parseInt(woman[7]));
                        tblreg.setRegAmberOrRedColorCode(Integer.parseInt(woman[9]));

                        tblreg.setUserId(woman[13]);        //18Aug2019 - Bindu - set userid
                        appState.selectedWomanId = tblreg.getWomanId();

                        int service = Integer.parseInt(woman[12]);
                        serviceNum = Integer.parseInt(woman[11]);
                        serviceMinDate = woman[2];
                        String serviceMaxDate = woman[3]; // 04Aug2019 - Bindu -- iCreateNewTrans max date

                        addServicesData(PendingWomenForServices.this,
                                woman[0], sType,
                                service, serviceNum, serviceMinDate, serviceMaxDate);
                    }
                }
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }


    /**
     * Save Confirmation Alert
     *
     * @param goToScreen
     */
    private void displayAlert(final String spanText2, final Intent goToScreen) {
        try {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

            // set dialog message
            alertDialogBuilder
                    .setMessage(spanText2)
                    .setCancelable(false)
                    .setNegativeButton((getResources().getString(R.string.m121)), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            if (goToScreen != null) {
                                startActivity(goToScreen);
                                if (spanText2.contains("logout")) {
                                    try {
                                        LoginRepository loginRepository = new LoginRepository(databaseHelper);
                                        loginRepository.updateLogoutTime(appState.loginAuditId, appState.sessionUserId);
                                    } catch (Exception e) {

                                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                                    }
                                }
                            } else
                                dialog.cancel();
                        }
                    })
                    .setPositiveButton((getResources().getString(R.string.m122)), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alertDialog1 = alertDialogBuilder.create();
            alertDialog1.show();
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }


    /**
     * This method Calls the idle timeout
     */
    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        new ActivitiesStack(this).delayedIdle(appState.idleTimeOut);
    }


    /**
     * Called whenever an item clicked in actionbar
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        try {


            // Handle action buttons
            switch (item.getItemId()) {

                case R.id.home: {
                    Intent goToScreen = new Intent(PendingWomenForServices.this, MainMenuActivity.class);
                    goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    displayAlert(getResources().getString(R.string.exit), goToScreen);
                    return true;
                }

                case android.R.id.home: {
                    Intent goToScreen = new Intent(PendingWomenForServices.this, PendingUpcomingServiceActivity.class);
                    goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    startActivity(goToScreen);

                    return true;
                }

                case R.id.logout: {
                    Intent goToScreen = new Intent(PendingWomenForServices.this, LoginActivity.class);
                    displayAlert(getResources().getString(R.string.m111), goToScreen);
                    return true;
                }


                case R.id.wlist: {
                    Intent goToScreen = new Intent(PendingWomenForServices.this, RegisteredWomenActionTabs.class);
                    goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    displayAlert(getResources().getString(R.string.exit), goToScreen);
                    return true;
                }
                //30Sep2019 - Bindu
                case R.id.about: {
                    Intent goToScreen = new Intent(PendingWomenForServices.this, AboutActivity.class);
                    goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    displayAlert(getResources().getString(R.string.exit), goToScreen);
                    return true;
                }
                default:
                    return super.onOptionsItemSelected(item);
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            return false;
        }
    }


    //    pop up which accepts services data
    public void addServicesData(final Context context,
                                final String womanId, final String serviceType,
                                final int sId, final int sNumber,
                                String minDate, String maxDate) throws Exception {

        AlertDialog.Builder alert = new AlertDialog.Builder(context, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        final View customView = LayoutInflater.from(context).inflate(R.layout.add_services_info, null);

        aqPMSel = new AQuery(customView);
        initializeScreen(aqPMSel.id(R.id.lladdservices).getView(), context);

        //        06May2021 Arpitha
        String strServType= "";
        int identifier = context.getResources().getIdentifier(serviceType.toLowerCase(),
                "string", "com.sc.stmansi");//06May2021 Arpitha
        strServType = (context.getResources().getString(identifier));//06May2021 Arpitha

        aqPMSel.id(R.id.txtheading).text(getResources()
                .getString(R.string.addplannedservice) + ": " + strServType + " " + serviceNum);
        aqPMSel.id(R.id.etdate).text(DateTimeUtil.getTodaysDate());
        aqPMSel.id(R.id.txtNameval).text(woman.getRegWomanName());
//        aqPMSel.id(R.id.txtlmpval).text(woman.getRegLMP());
        if(sId<=15)//15Nov2019 Arpitha
            aqPMSel.id(R.id.txtlmpval).text(woman.getRegLMP());
        else {
            aqPMSel.id(R.id.txtlmpval).text(woman.getRegADDate());
            aqPMSel.id(R.id.tvlmp).text(getResources().getString(R.string.tvadd));
        }

        aqPMSel.id(R.id.trspnfacname).gone();
        aqPMSel.id(R.id.trduration).visible(); //04Aug2019 -Bindu

        aqPMSel.id(R.id.txtservicedeatils).text(sType + " " + getResources().getString(R.string.details));

        aqPMSel.id(R.id.tvdurationvalmin).text(minDate); // set Min and max date duration

        aqPMSel.id(R.id.tvdurationvalmax).text(maxDate);



        //        07May2021 Arpitha - set fields based on service type
        if (strServType.contains(getResources().getString(R.string.tt))) {
            aqPMSel.id(R.id.trnooftabgiven).gone();
            aqPMSel.id(R.id.trnooftabconsumed).gone();
            aqPMSel.id(R.id.llanctests).gone();
        } else if (strServType.contains(getResources().getString(R.string.ifa)) ||
                strServType.contains
                        (getResources().getString(R.string.folicacid)) ||
                strServType.contains(getResources().getString(R.string.calcium))) {
            aqPMSel.id(R.id.trnooftabgiven).visible();
            aqPMSel.id(R.id.trnooftabconsumed).visible();
            aqPMSel.id(R.id.llanctests).gone();
        } else if (strServType.contains(getResources().getString(R.string.anc))) {
            aqPMSel.id(R.id.trnooftabgiven).gone();
            aqPMSel.id(R.id.trnooftabconsumed).gone();
            aqPMSel.id(R.id.llanctests).visible();

            if(sNumber == 1 )
            {

                aqPMSel.id(R.id.trpregtest).visible();
                aqPMSel.id(R.id.trbabygrowthscan).gone();
                aqPMSel.id(R.id.trbreastheart).gone();
            }else
            {
                aqPMSel.id(R.id.trpregtest).gone();
                aqPMSel.id(R.id.trbabygrowthscan).visible();
                aqPMSel.id(R.id.trbreastheart).visible();
            }
        }
        setOnClickListner();//11May2021 Arpitha



        //20Aug2019 Arpitha
        aqPMSel.id(R.id.imgcleardate).getImageView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aqPMSel.id(R.id.etdate).text(DateTimeUtil.getTodaysDate());
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.dateresettotodaysdate), Toast.LENGTH_LONG).show();//21Aug2019 Arpitha
            }
        });

        //25Jul2019 - Bindu

        aqPMSel.id(R.id.spnfactype).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    String selected = adapterView.getItemAtPosition(i).toString();
                    //                    15May2021 Bindu - add select, hmevisit and other from eng compare
                    if (!(selected.equalsIgnoreCase(getResources().getString(R.string.selecteng)) || selected.equalsIgnoreCase(getResources().getString(R.string.bhomeeng)) || selected.equalsIgnoreCase(getResources().getString(R.string.bsc)) ||
                            selected.equalsIgnoreCase(getResources().getString(R.string.botherseng)))) {

                        if (arrVal == null) {
                            aqPMSel.id(R.id.trspnfacname).visible();
                            aqPMSel.id(R.id.tvfacname).text(getResources().getString(R.string.facilitynameothers));
                        }
                        facilityNameList = new ArrayList<>();
                        FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
                        mapFacilityName = facilityRepository.getFacilityNames(aqPMSel.id(R.id.spnfactype).getSelectedItem().toString(), user.getUserId());
                        //facilityNameList.add(getResources().getString(R.string.selectfacilityname));
                        facilityNameList.add(getResources().getString(R.string.strselect)); //15Nov2019 - Bindu selectfacilityname  to strselect
                        for (Entry<String, String> village : mapFacilityName.entrySet())
                            facilityNameList.add(village.getKey());
                        //facilityNameList.add(getResources().getString(R.string.other));
                        facilityNameList.add(getResources().getString(R.string.strother)); //15Nov2019 - Bindu other  to strother
                        ArrayAdapter<String> FacNameAdapter = new ArrayAdapter<>(PendingWomenForServices.this,
                                R.layout.simple_spinner_dropdown_item, facilityNameList);
                        FacNameAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        aqPMSel.id(R.id.spnfacname).adapter(FacNameAdapter);
                    } else {
                        aqPMSel.id(R.id.trspnfacname).gone();
                        aqPMSel.id(R.id.tretfacname).visible();
                        aqPMSel.id(R.id.tvfacname).text(getResources().getString(R.string.facilityname));
                    }
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        // Facility name on select listener
        aqPMSel.id(R.id.spnfacname).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    String selected = adapterView.getItemAtPosition(i).toString();
                    //if (selected.equalsIgnoreCase(getResources().getString(R.string.other))) {
                    if (selected.equalsIgnoreCase(getResources().getString(R.string.strother))) {  //15Nov2019 - Bindu other  to strother
                        aqPMSel.id(R.id.tretfacname).visible();
                        aqPMSel.id(R.id.etfacname).text("");
                    } else {
                        aqPMSel.id(R.id.tretfacname).gone();
                        aqPMSel.id(R.id.etfacname).text("");
                    }
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        WomanServiceRepository serviceRepo = new WomanServiceRepository(databaseHelper);
        serviceList = serviceRepo.getPendingServiceslistData(womanId, serviceType);

        ArrayList<String> serviceArr = new ArrayList<>();
        serviceArr.add("Select");
        if (serviceList != null && serviceList.size() > 1) {
            for (String[] sList : serviceList) {


                serviceArr.add(sList[5] + sList[4]);

            }
            ArrayAdapter<String> servicesAdapter = new ArrayAdapter<>(this,
                    R.layout.simple_spinner_dropdown_item, serviceArr);
            servicesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            aqPMSel.id(R.id.spnservtype).adapter(servicesAdapter);
            aqPMSel.id(R.id.trservtype).visible();
        } else {
            aqPMSel.id(R.id.trservtype).gone();

        }

        List<ServiceslistPojo> sPojo = serviceRepo.getServiceTypeData(womanId,
                sType, appState.sessionUserId);

        if (sPojo != null && sPojo.size() > 0) {

            aqPMSel.id(R.id.tblservicesdetails).visible();
            aqPMSel.id(R.id.txtservicedeatils).visible();

            if (sPojo.size() >= 1) {
                aqPMSel.id(R.id.trservice1).visible();
                aqPMSel.id(R.id.txtprevSer1).text(sPojo.get(0).getServicetype()
                        + sPojo.get(0).getServiceNumber()+ "   " + sPojo.get(0).getActualDateofAction());
                aqPMSel.id(R.id.txtprovideddate1).text(sPojo.get(0).getActualDateofAction());
                if (sPojo.get(0).getActualDateofAction() != null
                        && sPojo.get(0).getActualDateofAction().trim().length() > 0)
                    aqPMSel.id(R.id.tick1).background(R.drawable.tick);
            }


            if (sPojo.size() >= 2) {
                aqPMSel.id(R.id.trservice2).visible();
                aqPMSel.id(R.id.txtprevSer2).text(sPojo.get(1).getServicetype()
                        + sPojo.get(1).getServiceNumber()+ "   " + sPojo.get(1).getActualDateofAction());
                aqPMSel.id(R.id.txtprovideddate2).text(sPojo.get(1).getActualDateofAction());
                if (sPojo.get(1).getActualDateofAction() != null
                        && sPojo.get(1).getActualDateofAction().trim().length() > 0)
                    aqPMSel.id(R.id.tick2).background(R.drawable.tick);
            }


            if (sPojo.size() >= 3) {
                aqPMSel.id(R.id.trserv3).visible();
                aqPMSel.id(R.id.txtprevSer3).text(sPojo.get(2).getServicetype()
                        + sPojo.get(2).getServiceNumber() + "   " + sPojo.get(2).getActualDateofAction());
                aqPMSel.id(R.id.txtprovideddate3).text(sPojo.get(2).getActualDateofAction());

                if (sPojo.get(2).getActualDateofAction() != null
                        && sPojo.get(2).getActualDateofAction().trim().length() > 0)
                    aqPMSel.id(R.id.tick3).background(R.drawable.tick);
            }


            if (sPojo.size() >= 4) {
                aqPMSel.id(R.id.trserv4).visible();
                aqPMSel.id(R.id.txtprevSer4).text(sPojo.get(3).getServicetype()
                        + sPojo.get(3).getServiceNumber() + "   " + sPojo.get(3).getActualDateofAction());
                aqPMSel.id(R.id.txtprovideddate4).text(sPojo.get(3).getActualDateofAction());

                if (sPojo.get(3).getActualDateofAction() != null
                        && sPojo.get(3).getActualDateofAction().trim().length() > 0)
                    aqPMSel.id(R.id.tick4).background(R.drawable.tick);
            }

            if (sPojo.size() >= 5) {
                aqPMSel.id(R.id.trserv5).visible();
                aqPMSel.id(R.id.txtprevSer5).text(sPojo.get(4).getServicetype()
                        + sPojo.get(4).getServiceNumber() + "   " + sPojo.get(4).getActualDateofAction());
                aqPMSel.id(R.id.txtprovideddate5).text(sPojo.get(4).getActualDateofAction());

                if (sPojo.get(4).getActualDateofAction() != null
                        && sPojo.get(4).getActualDateofAction().trim().length() > 0)
                    aqPMSel.id(R.id.tick5).background(R.drawable.tick);
            }

            if (sPojo.size() >= 6) {
                aqPMSel.id(R.id.trserv6).visible();
                aqPMSel.id(R.id.txtprevSer6).text(sPojo.get(5).getServicetype()
                        + sPojo.get(5).getServiceNumber() + "   " + sPojo.get(5).getActualDateofAction());
                aqPMSel.id(R.id.txtprovideddate6).text(sPojo.get(5).getActualDateofAction());

                if (sPojo.get(5).getActualDateofAction() != null
                        && sPojo.get(5).getActualDateofAction().trim().length() > 0)
                    aqPMSel.id(R.id.tick6).background(R.drawable.tick);
            }

            if (sPojo.size() >= 7) {
                aqPMSel.id(R.id.trserv7).visible();
                aqPMSel.id(R.id.txtprevSer7).text(sPojo.get(6).getServicetype()
                        + sPojo.get(6).getServiceNumber() + "   " + sPojo.get(6).getActualDateofAction());
                aqPMSel.id(R.id.txtprovideddate4).text(sPojo.get(6).getActualDateofAction());

                if (sPojo.get(6).getActualDateofAction() != null
                        && sPojo.get(6).getActualDateofAction().trim().length() > 0)
                    aqPMSel.id(R.id.tick4).background(R.drawable.tick);
            }
        }


        aqPMSel.id(R.id.spnservtype).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (aqPMSel.id(R.id.spnservtype).getSpinner().getSelectedView().toString() != null) { // 04Aug2019 - Bindu - default mapto service type
                    if (position == 0) {
                        TextView errorText = (TextView) aqPMSel.id(R.id.spnservtype).getSpinner().getSelectedView();
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        //errorText.setText(getResources().getString(R.string.selectservtype));//changes the selected item text to this
                        errorText.setText(getResources().getString(R.string.selservicetoprovide));//changes the selected item text to this //15Nov2019 - Bindu
                    }
                }

                if (position > 0) {
                    String strMin = serviceList.get(position - 1)[7] + "  -  " +
                            serviceList.get(position - 1)[8];
                    aqPMSel.id(R.id.tvdurationvalmin).text(strMin);
                    aqPMSel.id(R.id.trduration).visible();
                    serviceMinDate = serviceList.get(position - 1)[7];
                } else
                    aqPMSel.id(R.id.trduration).gone(); //04Au2019 -Bindu
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        aqPMSel.id(R.id.btnsave).getImageView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (serviceList != null && serviceList.size() > 1) {
                        if (aqPMSel.id(R.id.spnservtype)
                                .getSelectedItemPosition() > 0) {
                            int servNum = Integer.parseInt(serviceList.get(aqPMSel.id(R.id.spnservtype)
                                    .getSelectedItemPosition() - 1)[4]);
                            int servId = Integer.parseInt(serviceList.get(aqPMSel.id(R.id.spnservtype)
                                    .getSelectedItemPosition() - 1)[3]);
                            if (ValidateServiceFields(serviceType))
                                confirmAlert(serviceType, servId,
                                        servNum);
                        } else {
                            if (ValidateServiceFields(serviceType))
                                confirmAlert(serviceType, sId,
                                        sNumber);
                        }
                    } else {
                        if (ValidateServiceFields(serviceType))
                            confirmAlert(serviceType, sId,
                                    sNumber);
                    }
                } catch (Exception e) {


                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }

            }
        });

        aqPMSel.id(R.id.btncancel).getImageView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    confirmAlert(null, 0, 0);
                } catch (Exception e) {

                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }

            }
        });

        aqPMSel.id(R.id.etdate).getEditText().setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEvent.ACTION_UP == event.getAction()) {
                    DatePickerFragment newFragment = DatePickerFragment.getInstance();
                    newFragment.setAQuery(aqPMSel);
                    Bundle bundle = new Bundle();
                    bundle.putString("regLMP", woman.getRegLMP());
                    bundle.putString("serviceMinimumDate", serviceMinDate);
                    newFragment.setArguments(bundle);
                    newFragment.show(getSupportFragmentManager(), "datePicker");

                }
                return true;
            }
        });

        alert.setView(customView).setTitle("").setPositiveButton(
                "", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        alertDialog.setCancelable(false);
                        alertDialog.show();
                        try {
                            if (serviceList != null && serviceList.size() > 1) {
                                int servNum = Integer.parseInt(serviceList.get(aqPMSel.id(R.id.spnservtype)
                                        .getSelectedItemPosition() - 1)[4]);
                                int servId = Integer.parseInt(serviceList.get(aqPMSel.id(R.id.spnservtype)
                                        .getSelectedItemPosition() - 1)[3]);

                                confirmAlert(serviceType, servId, servNum);
                            } else {
                                confirmAlert(serviceType, sId, sNumber);
                            }
                        } catch (Exception e) {

                            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                        }

                    }
                });
        alertDialog = alert.create();
        alertDialog.show();
        alert.setCancelable(false);
        alertDialog.setTitle(context.getResources().getString(R.string.ok));
        alert.setCancelable(true);
        alertDialog.show();
        alertDialog.setCancelable(false);

    }


    //    update services data to table
    void addServicesData(String serviceType, int serviceId, int servNum) throws Exception {

        TransactionHeaderRepository transRepo = new TransactionHeaderRepository(databaseHelper);
        int transId = transRepo.iCreateNewTrans(woman.getUserId(),databaseHelper);
        final ServiceslistPojo SLU = new ServiceslistPojo();
        SLU.setUserid(woman.getUserId());
        SLU.setWomanId(woman.getWomanId());
        String servicetype = serviceType;
        if (servicetype.contains("1"))
            servicetype = servicetype.replace("1", "");
        else if (servicetype.contains("2"))
            servicetype = servicetype.replace("2", "");
        else if (servicetype.contains("3"))
            servicetype = servicetype.replace("3", "");
        SLU.setServicetype(servicetype);
        SLU.setServiceId(serviceId);
        SLU.setServiceNumber(servNum);
        SLU.setComments(aqPMSel.id(R.id.etcomments).getText().toString());
        SLU.setFacType(aqPMSel.id(R.id.spnfactype).getSelectedItem().toString());  //12Aug2019 - Bindu - change from positin to direct val
        SLU.setActualDateofAction(aqPMSel.id(R.id.etdate).getText().toString());
        SLU.setUsertype(appState.userType);

        // 26Jul2019 - Bindu  set fac name based on fac type cond
        String sel = "";
        if (aqPMSel.id(R.id.spnfacname).getSelectedItem() != null)
            sel = aqPMSel.id(R.id.spnfacname).getSelectedItem().toString();

        //15Nov2019 - Bindu - set res english
        if(sel.equalsIgnoreCase(getResources().getString(R.string.select)))
            sel = getResources().getString(R.string.strselect);
        else if(sel.equalsIgnoreCase(getResources().getString(R.string.other)))
            sel = getResources().getString(R.string.strother);


        if (sel.trim().length() > 0 && !(sel.equalsIgnoreCase("select") || sel.equalsIgnoreCase("other")))

            SLU.setFacilityName(sel);
        else if (sel.equalsIgnoreCase("other")
                && aqPMSel.id(R.id.etfacname).getText().toString().trim().length() <= 0)
            SLU.setFacilityName("Other");
        else
            SLU.setFacilityName(aqPMSel.id(R.id.etfacname).getText().toString());
        SLU.setTransId(transId);
        SLU.setRecordupdateddate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime()); //26Jul2019 - Bindu
        SLU.setUsertype(appState.userType);

        //            07May2021 Arpitha
        SLU.setServiceDetail1(aqPMSel.id(R.id.etnooftabgiven).getText().toString());
        SLU.setServiceDetail2(aqPMSel.id(R.id.etnooftabconsumed).getText().toString());

        setAncTestData(transId, servNum);//10May2021 Arpitha



        WomanServiceRepository womanServiceRepository = new WomanServiceRepository(databaseHelper);
        womanServiceRepository.updateServicesListData(SLU,databaseHelper,transId);
        womanServiceRepository.insertANcTests(ancTestsListAdd, databaseHelper, transId);
        Toast.makeText(PendingWomenForServices.this, getResources().getString(R.string.serviceupdatesuccess), Toast.LENGTH_LONG).show();
        alertDialog.cancel();
    }


    //    validates mandatory fields
    boolean ValidateServiceFields( String serviceType ) {
        if (aqPMSel.id(R.id.etdate).getText().toString().trim().length() <= 0) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.enterservicecompdate), Toast.LENGTH_LONG).show();
            return false;
        } else if (serviceList != null && serviceList.size() > 1 && aqPMSel.id(R.id.spnservtype)
                .getSelectedItemPosition() <= 0) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.selectservtype)
                    , Toast.LENGTH_LONG).show();
            return false;
        } else if (aqPMSel.id(R.id.spnfactype).getSelectedItemPosition() == 0) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.selserviceprovfactype), Toast.LENGTH_LONG).show();
            return false;
        }
//        15May2021 Arpitha
        else if( serviceType.contains("ANC") && !(aqPMSel.id(R.id.rdanctestYes1).isChecked() || aqPMSel.id(R.id.rdanctestNo1).isChecked()))
        {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.enterbp), Toast.LENGTH_LONG).show();

            return false;
        }
        //        11May2021 Arpitha
        else  if( serviceType.contains("ANC") &&  aqPMSel.id(R.id.rdanctestYes1).isChecked() &&
                (aqPMSel.id(R.id.etbpsystolic)
                        .getText().toString().trim().length()
                        <= 0 || aqPMSel.id(R.id.etbpdiastolic).getText().toString()
                        .trim().length() <= 0))
        {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.enterbp), Toast.LENGTH_LONG).show();

            aqPMSel.id(R.id.etbpsystolic).getEditText().requestFocus();//22Aug2019 Arpitha
            return false;
        }
        //        11May2021 Arpitha
        else if( serviceType.contains("ANC") && aqPMSel.id(R.id.etanctestval15).getText().toString().trim().length()<=0)
        {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.enterweightvalue), Toast.LENGTH_LONG).show();

            aqPMSel.id(R.id.etanctestval15).getEditText().requestFocus();//22Aug2019 Arpitha
            return false;
        }
        return true;
    }

    /**
     * This is a Recursive method that traverse through the group and subgroup
     * of view Sets text, assigns clickListners
     * <p>
     * //@param aqPMSel
     *
     * @throws Exception
     */
    private ArrayList<View> initializeScreen(View v, Context context) throws Exception {

        if (!(v instanceof ViewGroup)) {
            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);

            // TextView - Assign Labels
            if (v.getClass() == TextView.class || v.getClass() == RadioButton.class) {
            }

            if (v.getClass() == RadioButton.class) {
                aqPMSel.id(v.getId()).clicked(context, "commonClick");
            }

            return viewArrayList;
        }

        if (v instanceof Spinner) {
        }

        ArrayList<View> result = new ArrayList<View>();

        ViewGroup viewGroup = (ViewGroup) v;
        for (int i = 0; i < viewGroup.getChildCount(); i++) {

            View child = viewGroup.getChildAt(i);

            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);
            viewArrayList.addAll(initializeScreen(child, context));

            result.addAll(viewArrayList);
        }
        return result;
    }

    /**
     * Confirm Alert on Click save
     */
    private void confirmAlert(final String serviceType, final int serviceId, final int servNum) throws Exception {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        String mess;

        if (serviceType != null || serviceId != 0)
            mess = getResources().getString(R.string.m099);
        else {
            if (arrVal != null)
                mess = getResources().getString(R.string.areyousuretoexit);
            else
                mess = getResources().getString(R.string.m110);
        }

        alertDialogBuilder.setMessage(mess).setCancelable(false)
                .setNegativeButton(getResources().getString(R.string.m121),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                try {
                                    if (serviceType != null || serviceId != 0) {
                                        addServicesData(serviceType, serviceId, servNum);
                                        new AwaitingWomenLoader(PendingWomenForServices.this).execute();
                                    } else
                                        alertDialog.cancel();

                                } catch (Exception e) {
                                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                                }
                            }
                        })
                .setPositiveButton((getResources().getString(R.string.m122)),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    @Override
    public void onClick(View v) {
        commonClick(null, v, recyclerView.getChildLayoutPosition(v), 0);
    }


    @Override
    public boolean onLongClick(View v) {
        return false;
    }

    @Override
    public void displayAlert(int selectedItemPosition) {
        try {
            ArrayList<String> compList = new ArrayList<>();
            String complication = services.get(selectedItemPosition)[8];
            if (complication != null && complication.length() > 0) {
                compList = new ArrayList<>();
                compList.add(complication.replace(",", ", ").replace("PH:", "\nPH:"));


                ArrayList<String> complListDisp = new ArrayList<>();
                for (int i = 0; i < compList.size(); i++) {
                    String compl[] = compList.get(i).split("[,\n]");
                    for (int j = 0; j < compl.length; j++) {
                        String strComp = compl[j];
                        if (strComp.contains("CH:"))
                            strComp = strComp.replaceAll("CH:", "");
                        if (strComp.contains("PH:"))
                            strComp = strComp.replaceAll("PH:", "");

                        strComp = strComp.replaceAll(" ", "");
                        if (strComp.contains("ormoreconsecutivespontaneousabortions"))
                            strComp = strComp.split("[<>0123456789/(]")[1];
                        else
                            strComp = strComp.split("[<>0123456789/(]")[0];
                        strComp = strComp.toLowerCase().trim();
                        int identifier = getResources().getIdentifier
                                (strComp,
                                        "string", "com.sc.stmansi");
                        if (identifier > 0) {
                            if (compl[j].contains("CH:"))
                                complListDisp.add("CH: " + getResources().getString(identifier));
                            else if (compl[j].contains("PH:"))
                                complListDisp.add("PH: " + getResources().getString(identifier));
                            else
                                complListDisp.add(getResources().getString(identifier));

                        } else {
                           /* if(compl[j].contains("CH:"))
                            complListDisp.add("CH: "+compl[j]);
                           else if(compl[j].contains("PH:"))
                                complListDisp.add("PH: "+compl[j]);
                            else*/
                            complListDisp.add(compl[j]);

                        }
                    }

                }

                ArrayList<String> complications = new ArrayList<>();
                String strStrings = "";
                for (int i = 0; i < complListDisp.size(); i++) {
                    if (i != 0)
                        strStrings = strStrings + ", " + complListDisp.get(i);
                    else
                        strStrings = complListDisp.get(i);
                }

                String[] list = strStrings.split("PH:");

                ArrayList<String> l = new ArrayList<>();
                for (int i = 0; i < list.length; i++) {
                    if (i == 0)
                        l.add(list[i]);
                    else
                        l.add("\nPH: " + list[i]);
                }


                AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                View convertView = LayoutInflater.from(this).inflate(R.layout.complicated_reasons_list, null, false);

                ListView lv = convertView.findViewById(R.id.lvCompliatedReasonslist);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.listitem, l);

                alertDialog.setView(convertView).setCancelable(true).setTitle((this.getResources().getString(R.string.compl_reasons)))
                        .setPositiveButton((this.getResources().getString(R.string.ok)), new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                lv.setAdapter(adapter);
                alertDialog.show();
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    @Override
    public void displayHomeVisitList(int selectedItemPosition) {
        Intent homevisitlist = new Intent(this, HomeVisitListActivity.class);
        appState.selectedWomanId = services.get(selectedItemPosition)[0];
        homevisitlist.putExtra("globalState", prepareBundle());
        startActivity(homevisitlist);
    }

    @Override
    public void displayDelComplicationAlert(int selectedItemPosition) {

    }


    @SuppressLint("SimpleDateFormat")
    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

       private AQuery aQuery;

        static DatePickerFragment getInstance() {
            return new DatePickerFragment();
        }

        public void setAQuery(AQuery aQuery) {
            this.aQuery = aQuery;
        }
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            int year = 0, month = 0, day = 0;
            try {
                Calendar c = Calendar.getInstance();
                String defaultDate = aQuery.id(R.id.etdate).getText().toString(); //09Aug2019 - Cal set date of Provided date
                if (defaultDate != null && defaultDate.length() > 0) {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    Date sendDate = sdf.parse(defaultDate);
                    c.setTime(sendDate);
                    year = c.get(Calendar.YEAR);
                    month = c.get(Calendar.MONTH);
                    day = c.get(Calendar.DAY_OF_MONTH);
                } else {
                    c.setTime(new Date());
                    year = c.get(Calendar.YEAR);
                    month = c.get(Calendar.MONTH);
                    day = c.get(Calendar.DAY_OF_MONTH);
                }

            } catch (Exception e) {

                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            }

            // Create a new instance of DatePickerDialog and return it
            DatePickerDialog dpDialog;
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                // Do something for lollipop and above versions
                dpDialog = new DatePickerDialog(getActivity(), R.style.DialogTheme, this, year, month, day);
            } else {
                // do something for phones running an SDK before lollipop
                dpDialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, this, year, month, day);
                dpDialog.setTitle("Set a Date");
                dpDialog.getDatePicker().setCalendarViewShown(false);
            }
            return dpDialog;
        }

        @Override
        public void onDateSet(DatePicker view, int year, int month, int day) {

            if (view.isShown()) {
                try {
                    String strSelectedDate = String.format("%02d", day) + "-"
                            + String.format("%02d", month + 1) + "-"
                            + year;

                    Date selectedDate = null, lmpDate = null, minDate = null;

                    String registeredLMP = getArguments().getString("regLMP");
                    if (registeredLMP != null && registeredLMP.trim().length() > 0) {
                        lmpDate = new SimpleDateFormat("dd-MM-yyyy").parse(registeredLMP);
                    }

                    String servMinDate = getArguments().getString("serviceMinimumDate");
                    if (servMinDate != null && servMinDate.trim().length() > 0)
                        minDate = new SimpleDateFormat("dd-MM-yyyy").
                                parse(servMinDate);

                    //                    28Apr2021 Arpitha
                    Date delDate = null;
                    if (woman.getRegADDate() != null && woman.getRegADDate().trim().length() > 0)
                        delDate = new SimpleDateFormat("dd-MM-yyyy").parse(woman.getRegADDate());//28Apr2021 Arpitha



                    if (strSelectedDate != null)
                        selectedDate = new SimpleDateFormat("dd-MM-yyyy").parse(strSelectedDate);// 07Feb2018

                    if (selectedDate.after(new Date())) {
                        Toast.makeText(getActivity(),
                                getResources().getString(R.string.date_cannot_af_currentdate),
                                Toast.LENGTH_LONG).show();
                    } else if (lmpDate != null && selectedDate.before(lmpDate)) {

                        Toast.makeText(getActivity(),
                                getResources().getString(R.string.date_cannot_bf_lmp),
                                Toast.LENGTH_LONG).show();
                        aQuery.id(R.id.etdate).text(DateTimeUtil.getTodaysDate());
                    }
                    //add delDate condition - 28Apr2021 Arpitha
                    else if (delDate == null && minDate != null && selectedDate.before(minDate)) {

                        Toast.makeText(getActivity(),
                                getResources().getString(R.string.datecannotbebfmindate),
                                Toast.LENGTH_LONG).show();
                        aQuery.id(R.id.etdate).text(DateTimeUtil.getTodaysDate());
                    } else if (delDate != null && (selectedDate.before(delDate) )) {   // changed on 28Apr2021
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.date_cannot_bf_deldate)
                                , getActivity());
                        aQuery.id(R.id.etdate).text(DateTimeUtil.getTodaysDate());
                    }
                    else {

                        aQuery.id(R.id.etdate).text(String.format("%02d", day) + "-" + String.format("%02d", month + 1)
                                + "-" + year);
                        aQuery.id(R.id.imgcleardate).visible();//20Aug2019 Arpitha

                    }
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }

            }

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        /*
         * You'll need this in your class to release the helper when done.
         */
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

    static class AwaitingWomenLoader extends AsyncTask<Void, Void, Void> {

        private WeakReference<PendingWomenForServices> weakRef;
        private AQuery aQuery;

        AwaitingWomenLoader(PendingWomenForServices activity) {
            weakRef = new WeakReference<>(activity);
            aQuery = new AQuery(activity);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            PendingWomenForServices activity = weakRef.get();
            WomanServiceRepository womanServiceRepository = new WomanServiceRepository(activity.databaseHelper);

            try {
                List<String[]> pendingWomen = womanServiceRepository.getWomanPendingList(activity.sType,
                        activity.sType2, activity.status);

                int previousSize = activity.services.size();
                activity.services.clear();
                activity.adapter.notifyItemRangeRemoved(0, previousSize);
                if(!pendingWomen.isEmpty()) {
                    activity.services.addAll(pendingWomen);
                    activity.adapter.notifyItemRangeInserted(0, activity.services.size());
                }
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            PendingWomenForServices activity = weakRef.get();
            activity.recyclerView.setVisibility(View.GONE);
            if (activity.services.isEmpty()) {
                aQuery.id(R.id.tvWomanListLbl).visible();
                aQuery.id(R.id.tvWomanListLbl).text("No Records");

            } else {
                aQuery.id(R.id.tvWomanListLbl).gone();
                aQuery.id(R.id.lvEjananiList).visible();
                activity.recyclerView.setVisibility(View.VISIBLE);
            }
            aQuery.id(R.id.txtcountwl).text(activity.getResources()
                    .getString(R.string.recordcount , activity.services.size()));
        }
    }

    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        return b;
    }

    @Override
    public void displayLMPConfirmation(int selectedItemPosition) {

    }

    //    10May2021 Arpitha
    private void setAncTestData(int transId, int servNo) throws Exception {

        ancTestsListAdd = new ArrayList<>();
        List<TblAncTestsMaster> ancTestsMasterList = new ArrayList<>();

        WomanServiceRepository womanServiceRepository = new WomanServiceRepository(databaseHelper);
        ancTestsMasterList = womanServiceRepository.getAncTests();

        for (int i = 1; i <= ancTestsMasterList.size(); i++) {
            TblAncTests tblAncTests = new TblAncTests();
            tblAncTests.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
            tblAncTests.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());//21May2021 Arpitha


            tblAncTests.setServiceNumber(servNo);
            /*if (i <=13 || i == 16) {
                if (aqPMSel.id(rdtestYes + i).isChecked()) {
                    tblAncTests.setTestDone("Yes");

                } else if (aqPMSel.id(rdtestNo + i).isChecked())
                    tblAncTests.setTestDone("No");
            }*/


            if (i == 1) {
//                if(aqPMSel.id(R.id.etbpsystolic).getText().toString().trim().length()>0 || aqPMSel.id(R.id.etbpdiastolic).getText().toString().trim().length()>0)

                if (aqPMSel.id(R.id.rdanctestYes1).isChecked()) {
                    tblAncTests.setTestDone("Yes");
                    tblAncTests.setTestValue(aqPMSel.id(R.id.etbpsystolic).getText().toString()
                            +"/"+aqPMSel.id(R.id.etbpdiastolic).getText().toString());
                }
                else if (aqPMSel.id(R.id.rdanctestNo1).isChecked())
                    tblAncTests.setTestDone("No");
            } else if (i == 2) {
                tblAncTests.setTestValue(aqPMSel.id(R.id.etanctestval2).getText().toString());
                if (aqPMSel.id(R.id.rdanctestYes2).isChecked())
                    tblAncTests.setTestDone("Yes");
                else if (aqPMSel.id(R.id.rdanctestNo2).isChecked())
                    tblAncTests.setTestDone("No");
            } else if (i == 3) {
                tblAncTests.setTestValue(aqPMSel.id(R.id.etanctestval3).getText().toString());
                if (aqPMSel.id(R.id.rdanctestYes3).isChecked())
                    tblAncTests.setTestDone("Yes");
                else if (aqPMSel.id(R.id.rdanctestNo3).isChecked())
                    tblAncTests.setTestDone("No");
            } else if (i == 4) {
                tblAncTests.setTestValue(aqPMSel.id(R.id.etanctestval4).getText().toString());
                if (aqPMSel.id(R.id.rdanctestYes4).isChecked())
                    tblAncTests.setTestDone("Yes");
                else if (aqPMSel.id(R.id.rdanctestNo4).isChecked())
                    tblAncTests.setTestDone("No");
            } else if (i == 5) {
                tblAncTests.setTestValue(aqPMSel.id(R.id.etanctestval5).getText().toString());
                if (aqPMSel.id(R.id.rdanctestYes5).isChecked())
                    tblAncTests.setTestDone("Yes");
                else if (aqPMSel.id(R.id.rdanctestNo5).isChecked())
                    tblAncTests.setTestDone("No");

            } else if (i == 6) {
                tblAncTests.setTestValue(aqPMSel.id(R.id.etanctestval6).getText().toString());
                if (aqPMSel.id(R.id.rdanctestYes6).isChecked())
                    tblAncTests.setTestDone("Yes");
                else if (aqPMSel.id(R.id.rdanctestNo6).isChecked())
                    tblAncTests.setTestDone("No");
            } else if (i == 7) {
                tblAncTests.setTestValue(aqPMSel.id(R.id.etanctestval7).getText().toString());
                if (aqPMSel.id(R.id.rdanctestYes7).isChecked())
                    tblAncTests.setTestDone("Yes");
                else if (aqPMSel.id(R.id.rdanctestNo7).isChecked())
                    tblAncTests.setTestDone("No");

            } else if (i == 8) {
                tblAncTests.setTestValue(aqPMSel.id(R.id.etanctestval8).getText().toString());
                if (aqPMSel.id(R.id.rdanctestYes8).isChecked())
                    tblAncTests.setTestDone("Yes");
                else if (aqPMSel.id(R.id.rdanctestNo8).isChecked())
                    tblAncTests.setTestDone("No");
            } else if (i == 9) {

                tblAncTests.setTestValue(aqPMSel.id(R.id.etanctestval9).getText().toString());
                if (aqPMSel.id(R.id.rdanctestYes9).isChecked())
                    tblAncTests.setTestDone("Yes");
                else if (aqPMSel.id(R.id.rdanctestNo9).isChecked())
                    tblAncTests.setTestDone("No");
            } else if (i == 10) {
                tblAncTests.setTestValue(aqPMSel.id(R.id.etanctestval10).getText().toString());
                if (aqPMSel.id(R.id.rdanctestYes10).isChecked())
                    tblAncTests.setTestDone("Yes");
                else if (aqPMSel.id(R.id.rdanctestNo10).isChecked())
                    tblAncTests.setTestDone("No");
            } else if (i == 11) {
                tblAncTests.setTestValue(aqPMSel.id(R.id.etanctestval11).getText().toString());
                if (aqPMSel.id(R.id.rdanctestYes11).isChecked())
                    tblAncTests.setTestDone("Yes");
                else if (aqPMSel.id(R.id.rdanctestNo11).isChecked())
                    tblAncTests.setTestDone("No");
            } else if (i == 12) {
                tblAncTests.setTestValue(aqPMSel.id(R.id.etanctestval12).getText().toString());
                if (aqPMSel.id(R.id.rdanctestYes12).isChecked())
                    tblAncTests.setTestDone("Yes");
                else if (aqPMSel.id(R.id.rdanctestNo12).isChecked())
                    tblAncTests.setTestDone("No");
            } else if (i == 13) {
                tblAncTests.setTestValue(aqPMSel.id(R.id.etanctestval13).getText().toString());
                if (aqPMSel.id(R.id.rdanctestYes13).isChecked())
                    tblAncTests.setTestDone("Yes");
                else if (aqPMSel.id(R.id.rdanctestNo13).isChecked())
                    tblAncTests.setTestDone("No");
            } else if (i == 14) {
                tblAncTests.setTestValue(aqPMSel.id(R.id.etanctestval14).getText().toString());
            } else if (i == 15) {
                tblAncTests.setTestValue(aqPMSel.id(R.id.etanctestval15).getText().toString());
            } else if (i == 16) {
                tblAncTests.setTestValue(aqPMSel.id(R.id.etanctestval16).getText().toString());
                if (aqPMSel.id(R.id.rdanctestYes16).isChecked())
                    tblAncTests.setTestDone("Yes");
                else if (aqPMSel.id(R.id.rdanctestNo16).isChecked())
                    tblAncTests.setTestDone("No");
            }

            tblAncTests.setTestId(ancTestsMasterList.get(i - 1).getTestId());
            tblAncTests.setUserId(woman.getUserId());
            tblAncTests.setWomenId(woman.getWomanId());
            tblAncTests.setTransId(transId);
            if (tblAncTests.getTestDone() != null ||
                    (tblAncTests.getTestValue() != null && tblAncTests.getTestValue().trim().length() > 0))
                ancTestsListAdd.add(tblAncTests);
        }
    }


    //    11May2021 Arpitha
    private void setOnClickListner() {


        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo1).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etbpdiastolic).gone();
                aqPMSel.id(R.id.etbpsystolic).gone();
                aqPMSel.id(R.id.etbpdiastolic).text("");
                aqPMSel.id(R.id.etbpsystolic).text("");
                aqPMSel.id(R.id.trancbptest).gone();

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes1).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etbpdiastolic).visible();
                aqPMSel.id(R.id.etbpsystolic).visible();
                aqPMSel.id(R.id.trancbptest).visible();

            }
        });

        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo2).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval2).gone();
                aqPMSel.id(R.id.etanctestval2).text("");

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes2).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval2).visible();

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo3).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval3).gone();
                aqPMSel.id(R.id.etanctestval3).text("");

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes3).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval3).visible();

            }
        });//        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo4).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval4).gone();
                aqPMSel.id(R.id.etanctestval4).text("");

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes4).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval4).visible();

            }
        });//        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo5).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval5).gone();
                aqPMSel.id(R.id.etanctestval5).text("");

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes5).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval5).visible();

            }
        });//        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo6).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval6).gone();
                aqPMSel.id(R.id.etanctestval6).text("");

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes6).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval6).visible();

            }
        });//        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo7).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval7).gone();
                aqPMSel.id(R.id.etanctestval7).text("");

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes7).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval7).visible();

            }
        });//        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo8).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval8).gone();
                aqPMSel.id(R.id.etanctestval8).text("");

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes8).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval8).visible();

            }
        });//        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo9).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval9).gone();
                aqPMSel.id(R.id.etanctestval9).text("");

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes9).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval9).visible();

            }
        });//        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo10).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval10).gone();
                aqPMSel.id(R.id.etanctestval10).text("");

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes10).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval10).visible();

            }
        });//        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo11).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval11).gone();
                aqPMSel.id(R.id.etanctestval11).text("");

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes11).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval11).visible();

            }
        });//        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo12).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval12).gone();
                aqPMSel.id(R.id.etanctestval12).text("");

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes12).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval12).visible();

            }
        });//        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo13).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval13).gone();
                aqPMSel.id(R.id.etanctestval13).text("");

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes13).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval13).visible();

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestNo16).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval16).gone();
                aqPMSel.id(R.id.etanctestval16).text("");

            }
        });
        //        10May2021 Arpitha
        aqPMSel.id(R.id.rdanctestYes16).getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                aqPMSel.id(R.id.etanctestval16).visible();

            }
        });

        aqPMSel.id(R.id.trtests).getView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (aqPMSel.id(R.id.lltest).getView().getVisibility() == View.VISIBLE) {
                    aqPMSel.id(R.id.lltest).gone();
                    aqPMSel.id(R.id.ivtests).background(R.drawable.ic_add_box);

                } else {
                    aqPMSel.id(R.id.lltest).visible();
                    aqPMSel.id(R.id.ivtests).background(R.drawable.ic_indeterminate_check_box);

                }
            }
        });

        aqPMSel.id(R.id.trlabtests).getView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (aqPMSel.id(R.id.lllabtest).getView().getVisibility() == View.VISIBLE) {
                    aqPMSel.id(R.id.lllabtest).gone();
                    aqPMSel.id(R.id.ivlabtest).background(R.drawable.ic_add_box);

                } else {
                    aqPMSel.id(R.id.lllabtest).visible();
                    aqPMSel.id(R.id.ivlabtest).background(R.drawable.ic_indeterminate_check_box);

                }
            }
        });
    }
}
