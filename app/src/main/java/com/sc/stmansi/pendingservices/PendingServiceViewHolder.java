package com.sc.stmansi.pendingservices;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sc.stmansi.R;

public class PendingServiceViewHolder extends RecyclerView.ViewHolder {

    public ImageView serviceImageView;
    public TextView serviceNameView, serviceCountView;

    public PendingServiceViewHolder(@NonNull View itemView) {
        super(itemView);
        serviceImageView = itemView.findViewById(R.id.imgservice);
        serviceNameView = itemView.findViewById(R.id.txtServiceName);
        serviceCountView = itemView.findViewById(R.id.txtServiceCount);
    }
}
