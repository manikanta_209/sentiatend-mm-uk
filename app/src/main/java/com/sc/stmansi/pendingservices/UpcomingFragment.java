//28Aug2019 Arpitha - code clean up
package com.sc.stmansi.pendingservices;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.recyclerview.ClickListener;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.repositories.WomanServiceRepository;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.womanlist.ActionBarVillageSpinnerItem;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import com.sc.stmansi.R;

public class UpcomingFragment extends Fragment implements VillageChangedListener, ClickListener {

    private RecyclerView recyclerView;
    private List<String[]> services;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private DatabaseHelper databaseHelper;
    private AppState appState;
    private TblInstusers user;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_upcomingservices, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
            Bundle bundle = getActivity().getIntent().getBundleExtra("globalState");

            appState = bundle.getParcelable("appState");

            databaseHelper = getHelper();
            UserRepository userRepository = new UserRepository(databaseHelper);
            user = userRepository.getOneAuditedUser(appState.ashaId);

            recyclerView = getView().findViewById(R.id.upcoming_services_recycler_view);
            recyclerView.setVisibility(View.VISIBLE);
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            recyclerView.setHasFixedSize(true);

            // use a linear layout manager
            layoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(layoutManager);

            services = new ArrayList<>();
            adapter = new PendingServicesAdapter(getActivity(), services, this);
            recyclerView.setAdapter(adapter);

            List<ActionBarVillageSpinnerItem> items = new ArrayList<>();
            items.add(new ActionBarVillageSpinnerItem("All Women", 0));
            new UpcomingServicesLoader(this, items, 0).execute();
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(getActivity(), DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void onClick(View v) {
        int selectedItemPosition = recyclerView.getChildLayoutPosition(v);
        String[] service = services.get(selectedItemPosition);
       /* if (!(service[0].equalsIgnoreCase("EDD")))
        {*/
            String serviceCount = service[1];
            int count = Integer.parseInt(serviceCount == null ? "0" : serviceCount);
            if (count > 0) {
                Intent goToScreen = new Intent(getActivity(), PendingWomenForServices.class);
                goToScreen.putExtra("sType", service[0]);
                goToScreen.putExtra("sType2", appState.selectedVillageCode);
                goToScreen.putExtra("servList", service[0]);
                goToScreen.putExtra("tribalHamlet", appState.selectedVillageCode);
                goToScreen.putExtra("status", "Upcoming");
                goToScreen.putExtra("pos", getActivity().getIntent().getIntExtra("pos", 0));
                goToScreen.putExtra("globalState", prepareBundle());
                startActivity(goToScreen);
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.nowomanavailtothisservice), Toast.LENGTH_LONG).show();
            }
       /* } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.eddnotanabled), Toast.LENGTH_LONG).show();
        }*/
    }

    @Override
    public boolean onLongClick(View v) {
        return false;
    }

    @Override
    public void onVillageChanged(List<ActionBarVillageSpinnerItem> spinnerItems, int selectedItemPosition) {
        new UpcomingServicesLoader(this, spinnerItems, selectedItemPosition).execute();
    }

    static class UpcomingServicesLoader extends AsyncTask<Void, Void, Integer> {

        private WeakReference<UpcomingFragment> weakReference;
        private List<ActionBarVillageSpinnerItem> spinnerItems;
        private int selectedItemPosition;
        UpcomingServicesLoader(UpcomingFragment fragment, List<ActionBarVillageSpinnerItem> spinnerItems, int selectedItemPosition) {
            weakReference = new WeakReference<>(fragment);
            this.spinnerItems = spinnerItems;
            this.selectedItemPosition = selectedItemPosition;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            UpcomingFragment fragment = weakReference.get();
            fragment.appState.selectedVillageCode = spinnerItems.get(selectedItemPosition).getVillageCode();
            WomanServiceRepository repository = new WomanServiceRepository(fragment.databaseHelper);
            try {
                List<String[]> rows = repository.getUpcomingServicesAndCount(fragment.appState.selectedVillageCode, fragment.user.getUserId());
                if (rows.size() > 0) {
                    int previousSize = fragment.services.size();
                    fragment.services.clear(); // clear must be called only for firstLoad, it is fine here for now until pagination is introduced
                    fragment.adapter.notifyItemRangeRemoved(0, previousSize);
                    fragment.services.addAll(rows);
                    fragment.adapter.notifyItemRangeInserted(0, fragment.services.size());
                }
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            }
            return fragment.services.size();
        }
    }

    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        return b;
    }
}
