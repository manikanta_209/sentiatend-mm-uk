//28Aug2019 Arpitha - code clean up
package com.sc.stmansi.pendingservices;


import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sc.stmansi.recyclerview.ClickListener;

import java.util.List;

import com.sc.stmansi.R;

@SuppressLint({"DefaultLocale", "InflateParams"})
public class PendingServicesAdapter extends RecyclerView.Adapter<PendingServiceViewHolder> {

    private Context context;
    private List<String[]> services;
    private ClickListener viewClickListener;

    public PendingServicesAdapter(Context context, List<String[]> services, ClickListener viewClickListener) {
        this.context = context;
        this.services = services;
        this.viewClickListener = viewClickListener;
    }

    @NonNull
    @Override
    public PendingServiceViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RelativeLayout layout = (RelativeLayout) LayoutInflater.from(context)
                .inflate(R.layout.ejanani_pendingservices_group_item, viewGroup, false);
        layout.setOnClickListener(viewClickListener);
        return new PendingServiceViewHolder(layout);
    }

    @Override
    public void onBindViewHolder(@NonNull PendingServiceViewHolder vh, int i) {
        String[] service = services.get(i);
        String serviceName = service[0];
        String serviceCount = service[1];

        if (serviceName.equalsIgnoreCase("TT")
                || serviceName.equalsIgnoreCase("TTBooster"))
            vh.serviceImageView.setImageResource(R.drawable.tt);
        else if (serviceName.equalsIgnoreCase("IFA"))
            vh.serviceImageView.setImageResource(R.drawable.ifa);
        else if (serviceName.equalsIgnoreCase("ANC"))
            vh.serviceImageView.setImageResource(R.drawable.anc);
        else if (serviceName.equalsIgnoreCase("HomeVisit"))
            vh.serviceImageView.setImageResource(R.drawable.prenatalhomevisit);
        else if (serviceName.equalsIgnoreCase("EDD"))
            vh.serviceImageView.setImageResource(R.drawable.delivery_info);
        else if (serviceName.equalsIgnoreCase("PNC"))
            vh.serviceImageView.setImageResource(R.drawable.postnatalcare);
        else if (serviceName.equalsIgnoreCase("Calcium"))
            vh.serviceImageView.setImageResource(R.drawable.calcium);
        else if (serviceName.equalsIgnoreCase("FolicAcid"))
            vh.serviceImageView.setImageResource(R.drawable.folicacid_final);
        else
            vh.serviceImageView.setImageResource(R.drawable.immunisation);

        //04Aug2019 - Bindu - get names from string files
        int identifier = context.getResources().getIdentifier(serviceName.toLowerCase(), "string", "com.sc.stmansi");
        vh.serviceNameView.setTypeface(null, Typeface.BOLD);
        vh.serviceNameView.setText((context.getResources().getString(identifier)));

        vh.serviceCountView.setText(serviceCount == null ? "0" : serviceCount);
    }

    @Override
    public int getItemCount() {
        return services.size();
    }
}
