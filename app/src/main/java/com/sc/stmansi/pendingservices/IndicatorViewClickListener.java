package com.sc.stmansi.pendingservices;

public interface IndicatorViewClickListener {

    void displayAlert(int selectedItemPosition);

    void displayHomeVisitList(int selectedItemPosition);

    void displayDelComplicationAlert(int selectedItemPosition);

    //07Apr2021 Bindu
    void displayLMPConfirmation(int selectedItemPosition);

}
