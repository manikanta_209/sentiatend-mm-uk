//28Aug2019 Arpitha - code clean up
package com.sc.stmansi.pendingservices;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sc.stmansi.recyclerview.ClickListener;

import java.util.List;

import com.sc.stmansi.R;


@SuppressLint({"DefaultLocale", "InflateParams"})
public class PendingWomenServicesAdapter extends RecyclerView.Adapter<WomenAwaitingViewHolder> {

    Context context;
    private List<String[]> serviceList;

    private ClickListener viewClickListener;

    public PendingWomenServicesAdapter(Context context, List<String[]> womenWithServiceCount,
                                       ClickListener viewClickListener) {
        this.context = context;
        this.serviceList = womenWithServiceCount;
        this.viewClickListener = viewClickListener;
    }

    @NonNull
    @Override
    public WomenAwaitingViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LinearLayout layout = (LinearLayout) LayoutInflater.from(context)
                .inflate(R.layout.ejanani_pendingservices_child_item, viewGroup, false);
        layout.setOnClickListener(viewClickListener);
        return new WomenAwaitingViewHolder(layout);
    }

    @Override
    public void onBindViewHolder(@NonNull WomenAwaitingViewHolder vh, int i) {
        if(serviceList.isEmpty())
            return;

        final int itemPosition = i;
        String[] woman = serviceList.get(i);
        vh.description.setText(woman[1]);

//        06May2021 Arpitha
        String strServType= "";
        int identifier = context.getResources().getIdentifier(woman[14].toLowerCase(),
                "string", "com.sc.stmansi");//06May2021 Arpitha
        strServType = (context.getResources().getString(identifier));//06May2021 Arpitha

        vh.dueDateMin.setText(strServType + ": " + woman[2] + "  -  " + woman[3]);

        if (Integer.parseInt(woman[7]) == 1 && woman[8] != null && woman[8].length() > 0) {
            int code = Integer.parseInt(woman[9]);

            //07Dec2019 - bindu - change the star icon
            if (code == 9)
                vh.starView.setImageResource(R.drawable.amberone);
            else if (code == 1)
                vh.starView.setImageResource(R.drawable.redone);
            else if (code == 2)
                vh.starView.setImageResource(R.drawable.redtwo);
            else if (code == 3)
                vh.starView.setImageResource(R.drawable.redthree);
            else if (code > 3 && code != 11)
                vh.starView.setImageResource(R.drawable.redfour);
            else if (code == 11)
                vh.starView.setImageResource(R.drawable.ambertwo);

            vh.starView.setVisibility(View.VISIBLE);
            vh.starView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((IndicatorViewClickListener) context).displayAlert(itemPosition);
                }
            });
        }else
            vh.starView.setVisibility(View.GONE);

        if (woman[10] != null && woman[10].trim().length() > 0) {
            byte[] encodeByte = Base64.decode(woman[10], Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            vh.photoView.setImageBitmap(bitmap);
        }
    }

    @Override
    public int getItemCount() {
        return serviceList.size();
    }
}
