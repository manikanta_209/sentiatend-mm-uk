package com.sc.stmansi.HomeVisit;

import java.util.ArrayList;
import java.util.List;

public class PNCPojoNb {

    private String nbSymptomsname;
    private List<String> nbBabyIds;

    public PNCPojoNb(String symptomName) {
        this.nbBabyIds = new ArrayList<>(4);
        this.nbSymptomsname = symptomName;
    }

    public void addBabyId(String babyId) {
        this.nbBabyIds.add(babyId);
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append(this.nbSymptomsname).append(" - ");
        for(String id : this.nbBabyIds) {
            s.append(id).append(",").append(" ");
        }
        String str = s.toString().trim();
        return str.substring(0, str.length() - 1);
    }
}
