package com.sc.stmansi.HomeVisit;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.core.content.FileProvider;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.legacy.app.ActionBarDrawerToggle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.sc.stmansi.About.AboutActivity;
import com.sc.stmansi.ImageStore.FileUtils;
import com.sc.stmansi.ImageStore.ImagePojo;
import com.sc.stmansi.ImageStore.RecyclerViewAdapter;
import com.sc.stmansi.R;
import com.sc.stmansi.common.AlertDialogUtil;
import com.sc.stmansi.common.CommonDatePickerFragment;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.common.MultiSelectionSpinner;
import com.sc.stmansi.common.NavDrawerItem;
import com.sc.stmansi.common.NavDrawerListAdapter;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.configuration.SyncState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.deactivate.WomanDeactivateActivity;
import com.sc.stmansi.delivery.DeliveryInfoActivity;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.repositories.AuditRepository;
import com.sc.stmansi.repositories.ChildRepository;
import com.sc.stmansi.repositories.CovidDetailsRepository;
import com.sc.stmansi.repositories.DeliveryRepository;
import com.sc.stmansi.repositories.FacilityRepository;
import com.sc.stmansi.repositories.HomeVisitRepository;
import com.sc.stmansi.repositories.ImageStoreRepository;
import com.sc.stmansi.repositories.LoginRepository;
import com.sc.stmansi.repositories.RefFacilityRepository;
import com.sc.stmansi.repositories.SMSRepository;
import com.sc.stmansi.repositories.SettingsRepository;
import com.sc.stmansi.repositories.TransactionHeaderRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.services.ServicesSummaryActivity;
import com.sc.stmansi.services.UnplannedServicesListActivity;
import com.sc.stmansi.sms.SendSMS;
import com.sc.stmansi.tables.AuditPojo;
import com.sc.stmansi.tables.MessageLogPojo;
import com.sc.stmansi.tables.TblChildInfo;
import com.sc.stmansi.tables.TblContactDetails;
import com.sc.stmansi.tables.TblDeliveryInfo;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.TblVisitChildHeader;
import com.sc.stmansi.tables.TblVisitDetails;
import com.sc.stmansi.tables.TblVisitHeader;
import com.sc.stmansi.tables.tblChildVisitDetails;
import com.sc.stmansi.tables.tblCovidTestDetails;
import com.sc.stmansi.tables.tblCovidVaccineDetails;
import com.sc.stmansi.tables.tblImageStore;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.viewprofile.ViewProfileActivity;
import com.sc.stmansi.womanlist.RegisteredWomenActionTabs;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class PncHomeVisit extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener, DatePickerDialog.OnDateSetListener {
    private AppState appState;
    private SyncState syncState;
    private DatabaseHelper databaseHelper;
    public static AQuery aq;
    private tblregisteredwomen woman;
    private TblInstusers user;
    private Activity act;
    private int cemocCnt = 0, bemocCnt = 0, visitNum = 0, transId , childVisitNum=0;//mani 12Sep2021
    static int week;
    String strstatus;
    private boolean isBtnSummaryClicked = false;
    int delnoofchildren = 0;
    List<TblDeliveryInfo> deliveryInfoList;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private ArrayList<NavDrawerItem> navDrawerItems;
    private ListView mDrawerList;
    private EditText currentEditTextViewforDate;
    private ImageView currentImageView;
    boolean isVisitDate = false;
    ArrayList arrLSymptomsMother, arrLSymptomsMotherSave;
    List<String> arrLNbSymptoms, arrLNbIds;
    Map<String, VisitDetailsPojo> nbdangersignsMap;
    private List<PNCPojoNb> pncNbs;
    private ArrayList<String> facilitynamelist = new ArrayList<String>();
    private Map<String, String> mapFacilityname;
    ArrayList<Integer> childMortality;
    List<TblChildInfo> childListMortID;
    boolean isMotherMortality = false, isAnyChildMortality = false, isAllChildMortality = false, isAllChildDeactivated = false;
    TblVisitHeader tblvisH;
    tblregisteredwomen hmvisitregdetails;
    TblVisitDetails tblvisD;
    tblChildVisitDetails tblCvisD;
    List<Integer> allchildlist = Arrays.asList(1, 2, 3, 4);
    List<Integer> unionchildlist = new ArrayList<Integer>(allchildlist);
    List<Integer> alivechildlist = new ArrayList<Integer>(allchildlist);
    List<TblChildInfo> childListIds;
    Map<Integer, String> myChildIDs = new HashMap<Integer, String>();
    LinkedHashMap<String, VisitDetailsPojo> insertchildMap = null;
    private Dao<TblVisitHeader, Integer> tblVisHeaderDao;
    private Dao<TblVisitDetails, Integer> tblVisDetailDao;
    private Dao<tblChildVisitDetails, Integer> tblChildVisDetailDao;
    private LinkedHashMap<String, VisitDetailsPojo> insertMotherSignsInvestVal;
    private int nbsmallbaby = R.id.chknbsmallbabylbw,
            nbnomeconium = R.id.chknbnomeconiumpassed, nbnourinepassed = R.id.chknbnourinepassed,
            nbinabilitytostuck = R.id.chknbinabilitytosuck, nbfastbreathing = R.id.chknbfastbreathing,
            nbinabilitytopassurineandstool = R.id.chknbinabilitytopassurineandstool, nbumbilicalstump = R.id.chknbumbilicalstumpredorpus,
            nbpustules = R.id.chknbpustules, nbjaundice = R.id.chknbjaundice, nbfever = R.id.chknbfever,
            nbdiarrhoea = R.id.chknbdiarrhoea, nbbloodinstools = R.id.chknbbloodinstools, nblethargic = R.id.chknblethargicbaby;
    //Manikanta 10feb2021 Added 2 fields : nbfeelswarm and nbfeelstoowarm
    //09Dec2019 -Bindu
    private int nbfits = R.id.chknbfitsbaby, nbeyesarered = R.id.chknbeyesareredbaby, nbpallorofpalmsorsoles = R.id.chknbpallorofpalmsbaby, nbyellowpalms = R.id.chknbyellowpalmsbaby, nbbluepalms = R.id.chknbbluepalmsbaby, nbfeelscoldorhot = R.id.chknbfeelscoldorhotbaby, nbbleeding = R.id.chknbbleedingbaby, nbabdominaldistension = R.id.chknbabdominaldistensionbaby, nbanybirthdefect = R.id.chknbanybirthdefectbaby, nbfeelswarm = R.id.chknbfeelswarmbaby, nbfeelstoowarm = R.id.chknbfeelstoowarmbaby;
    //29Nov2019 - Bindu
    private int singleChildAliveId = 0;
    private int nbbabyexamination = R.id.tblexaminationschild1;

    //mani 14Sep2021
    private int nbbabytabletdetails =R.id.llchiltablet1;
    TblVisitChildHeader tblvisCH;
    LinkedHashMap<String, VisitDetailsPojo> insertchildHeaderMap = null;
    private Dao<TblVisitChildHeader, Integer> tblVisChildHeaderDao;
    boolean isWomanDeactivated = false;
    private int nbtblchildregdetails = R.id.tblregatgovtfacchild1;
    /*private int nbbabyweight = R.id.etweightbaby1, nbbabyweightdate = R.id.etweightdatebaby1,nbimgclearwt = R.id.imgcleardateweightbaby1,
            nbbabytemp = R.id.etTempbaby1, nbbabytempdate = R.id.ettempdonedatebaby1, nbimgcleartemp = R.id.imgcleardatetempbaby1,
            nbbabyresprate = R.id.etrespratebaby1, nbbabyrespratedate = R.id.etrespratebaby1, nbimgclearresprate =  R.id.imgcleardaterespratebaby1,
            nbchlregid = R.id.etchildregid1, nbchlregdate = R.id.etchildregdate1, nbimgclearchlregdate= R.id.imgclearchildregdate1;
*/
    List<TblChildInfo> childListDeactivateID;
    ArrayList<Integer> childDeactivated;
    private int singleChildActiveId = 0;
    LinkedHashMap<String, VisitDetailsPojo> insertchildMap2 = null;
    LinkedHashMap<String, VisitDetailsPojo> insertchildMap3 = null;
    LinkedHashMap<String, VisitDetailsPojo> insertchildMap4 = null;
    //04Dec2019 - Bindu
    EditText etphn;
    boolean isMessageLogsaved;
    boolean messageSent;
    private List<PNCPojoNb> pncNbssave;
    //08Apr2021 Bindu
    private ArrayList<String> reffacilitynamelist = new ArrayList<String>();
    private ArrayList<String> reffacilitytypelist = new ArrayList<String>();
    private Map<String, String> mapRefFacilityType;
    private Map<String, String> mapRefFacilityname;
    //    11May2021 Bindu add sunkenfont , vomiting and sepsis for new born
    private int nbsunkenfont = R.id.chknbsunkenfont, nbvomiting = R.id.chknbvomiting, nbsepsis = R.id.chknbsepsis;
    //    14May2021 Bindu add pneumonia
    private int nbpneumonia = R.id.chknbpneumonia;
    //    16May2021 Bindu
    boolean isCoviddetDt = false;
    private tblCovidTestDetails tblcovtest;
    private tblCovidVaccineDetails tblcovvac;
    private Dao<tblCovidTestDetails, Integer> tblCovidTestDao;
    private Dao<tblCovidVaccineDetails, Integer> tblCovidVaccineDao;
    //    16MAy2021 Bindu
    public static MultiSelectionSpinner spnHealthProblemshv;
    static RadioGroup rgCovidResult, rgCovidTest;
    private List<tblCovidVaccineDetails> covVacItems;
    private boolean isvaccinated = false;
    //20May2021 Bindu
    private LinkedHashMap<String, String> insertCovidVaccineVal;

    //MaNI 12Sep2021
    private int nbhypothermia = R.id.chknbhypothermiababy;
    private int nbrespiratorydistress = R.id.chknbraspiratorydistressbaby;
    private int nbcynosis = R.id.chknbcyanosisbaby;
    private int nbexcessiveweightloss = R.id.chknbexcessivewtlossbaby;
    private int nbconvulsion = R.id.chknbconvulsionbaby;
    private int nbpreterm = R.id.chknbpretermbaby;
    private int spnChlWhoGaveIFATablets = R.id.spnChlWhoGaveIFATablets, rd_IFAYes=R.id.rd_IFAYes, rd_ConsumingIFAYes=R.id.rd_ConsumingIFAYes, etIFANoofTabletsConsumes=R.id.etIFANoofTabletsConsumes,
            rd_dewormingtabletsYes=R.id.rd_chldewormingtabletsYes, rd_dewormingtabletsNo=R.id.rd_chldewormingtabletsNo, rd_dewormingtabletsDono=R.id.rd_chldewormingtabletsDono, spnIFAReasons=R.id.spnIFAReasons;
    String ifaTaken="",ifaConsumed="",ifaTabletCount="",ifaNotGivenReason="",dewormingtab="";
    Integer  ifaAdviceGiven;


    //28-6-2021
    private static final int REQUEST_IMAGE_CAPTURE = 2;
    private RecyclerView recyclerViewImage;
    private String mCurrentPhotoPath;
    private ArrayList<tblImageStore> AllImagesBeforeSave = new ArrayList<>();
    private ArrayList<ImagePojo> AllImagePojo = new ArrayList<>();
    private Dao<tblImageStore, Integer> tblImageStoreDao;
    TblChildInfo tblChildInfo;

    //mani 14Sep2021
    public static MultiSelectionSpinner spnIFAReasons1,spnIFAReasons2,spnIFAReasons3,spnIFAReasons4;
    private RadioGroup  rgIFA, rgConsumeIFA , rgIFA2, rgConsumeIFA2,rgIFA3, rgConsumeIFA3,rgIFA4, rgConsumeIFA4;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pnchomevisit);
        try {
            databaseHelper = getHelper();
            Bundle bundle = getIntent().getBundleExtra("globalState");
            appState = bundle.getParcelable("appState");
            syncState = bundle.getParcelable("syncState");

            aq = new AQuery(this);

            WomanRepository womanRepo = new WomanRepository(databaseHelper);
            woman = womanRepo.getRegistartionDetails(appState.selectedWomanId, appState.sessionUserId);




            UserRepository userRepo = new UserRepository(databaseHelper);
            user = userRepo.getOneAuditedUser(appState.ashaId);
            act = this;
            initiateDrawer();
            initializeScreen(aq.id(R.id.rlhomevisit).getView());
            initialView();


            /*aq.id(R.id.etvisitdate).getEditText().setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (MotionEvent.ACTION_UP == event.getAction()) {
                        HomeVisit.DatePickerFragment newFragment = HomeVisit.DatePickerFragment.getInstance();
                        newFragment.setWoman(woman);
                        newFragment.show(getSupportFragmentManager(), "datePicker");
                    }
                    return true;
                }
            });*/

            aq.id(R.id.etvisitdate).getEditText().setOnTouchListener(this);
            aq.id(R.id.etbpdonedate).getEditText().setOnTouchListener(this);
            aq.id(R.id.etpulsedate).getEditText().setOnTouchListener(this);
            aq.id(R.id.ethbdonedate).getEditText().setOnTouchListener(this);
            aq.id(R.id.ettempdonedate).getEditText().setOnTouchListener(this);

            aq.id(R.id.etTemp).getEditText().addTextChangedListener(watcher);
            aq.id(R.id.etbpsystolicsumm).getEditText().addTextChangedListener(watcher);
            aq.id(R.id.etbpdiastolicsumm).getEditText().addTextChangedListener(watcher);
            aq.id(R.id.ethb).getEditText().addTextChangedListener(watcher);
            aq.id(R.id.etpulse).getEditText().addTextChangedListener(watcher);

            // baby 1 date features
            aq.id(R.id.etweightdatebaby1).getEditText().setOnTouchListener(this);
            aq.id(R.id.ettempdonedatebaby1).getEditText().setOnTouchListener(this);
            aq.id(R.id.etrespratedonedatebaby1).getEditText().setOnTouchListener(this);
            aq.id(R.id.etchildregdate1).getEditText().setOnTouchListener(this);

            // baby 2 date features
            aq.id(R.id.etweightdatebaby2).getEditText().setOnTouchListener(this);
            aq.id(R.id.ettempdonedatebaby2).getEditText().setOnTouchListener(this);
            aq.id(R.id.etrespratedonedatebaby2).getEditText().setOnTouchListener(this);
            aq.id(R.id.etchildregdate2).getEditText().setOnTouchListener(this);

            // baby 3 date features
            aq.id(R.id.etweightdatebaby3).getEditText().setOnTouchListener(this);
            aq.id(R.id.ettempdonedatebaby3).getEditText().setOnTouchListener(this);
            aq.id(R.id.etrespratedonedatebaby3).getEditText().setOnTouchListener(this);
            aq.id(R.id.etchildregdate3).getEditText().setOnTouchListener(this);

            // baby 4 date features
            aq.id(R.id.etweightdatebaby4).getEditText().setOnTouchListener(this);
            aq.id(R.id.ettempdonedatebaby4).getEditText().setOnTouchListener(this);
            aq.id(R.id.etrespratedonedatebaby4).getEditText().setOnTouchListener(this);
            aq.id(R.id.etchildregdate4).getEditText().setOnTouchListener(this);



            setBabyDetailsTextChangeListener(aq.id(R.id.etweightbaby1).getEditText(), aq.id(R.id.etTempbaby1).getEditText(), aq.id(R.id.etrespratebaby1).getEditText(), aq.id(R.id.etchildregid1).getEditText());
            setBabyDetailsTextChangeListener(aq.id(R.id.etweightbaby2).getEditText(), aq.id(R.id.etTempbaby2).getEditText(), aq.id(R.id.etrespratebaby2).getEditText(), aq.id(R.id.etchildregid2).getEditText());
            setBabyDetailsTextChangeListener(aq.id(R.id.etweightbaby3).getEditText(), aq.id(R.id.etTempbaby3).getEditText(), aq.id(R.id.etrespratebaby3).getEditText(), aq.id(R.id.etchildregid3).getEditText());
            setBabyDetailsTextChangeListener(aq.id(R.id.etweightbaby4).getEditText(), aq.id(R.id.etTempbaby4).getEditText(), aq.id(R.id.etrespratebaby4).getEditText(), aq.id(R.id.etchildregid4).getEditText());


            aq.id(R.id.imgcleardate).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    aq.id(R.id.etvisitdate).text(DateTimeUtil.getTodaysDate());
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.dateresettotodaysdate), Toast.LENGTH_LONG).show();//21Aug2019 Arpitha
                }
            });


// Facility name on select listener
            aq.id(R.id.spnfacnameHv).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    try {
                        String selected = adapterView.getItemAtPosition(i).toString();
                        if (selected.equalsIgnoreCase(getResources().getString(R.string.other))) {
                            aq.id(R.id.tretfacname).visible();
                        } else {
                            aq.id(R.id.tretfacname).gone();
                            aq.id(R.id.etreffacilityname).text("");
                        }
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            //08Apr2021 Bindu add ref fac type listener
            aq.id(R.id.spnfactype).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    try {
                        String selected = adapterView.getItemAtPosition(i).toString();
                        if (selected.equalsIgnoreCase(getResources().getString(R.string.selectfacilityname))) {
                            aq.id(R.id.trspnfacname).gone();
                            aq.id(R.id.spnfacname).setSelection(0);
                        } else {
                            aq.id(R.id.trspnfacname).visible();
                            setRefFacilityName(selected);
                        }
                    } catch (Exception e) {
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

//            16Jan2020 Arpitha & Bindu
            if (aq.id(R.id.txtmeconium).getText().toString().length() > 0)
                aq.id(R.id.txtmeconium).visible();
            else
                aq.id(R.id.txtmeconium).gone();

            //16May2021 Bindu
            aq.id(R.id.etVisitCovidTestDate).getEditText().setOnTouchListener(this);
            aq.id(R.id.etVisitCovidFirstDoseDate).getEditText().setOnTouchListener(this);
            aq.id(R.id.etVisitCovidSecondDoseDate).getEditText().setOnTouchListener(this);
            aq.id(R.id.tr_CovidTest).gone();

            rgCovidResult = findViewById(R.id.rgCovidResult);
            rgCovidTest = findViewById(R.id.rgCovidTest); //21May2021 Bindu
            spnHealthProblemshv = findViewById(R.id.spnVisitHealthProblems);
            String[] healthproblems = getResources().getStringArray(R.array.healthproblemsuffer);
            spnHealthProblemshv.setItems(healthproblems);

            if (spnHealthProblemshv.getSelectedItemsAsString().contains("Others")) {
                aq.id(R.id.trHealthProblemYesOthers).visible();
            } else {
                aq.id(R.id.trHealthProblemYesOthers).gone();
            }

            spnHealthProblemshv.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (spnHealthProblemshv.getSelectedIndicies().size() != 0) {
                        aq.id(R.id.llCovidQs).getView().setVisibility(View.VISIBLE);
                    } else {
                        aq.id(R.id.llCovidQs).getView().setVisibility(View.GONE);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            aq.id(R.id.spnVisitVaccineDose).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (!isvaccinated) {
                        aq.id(R.id.etVisitCovidFirstDoseDate).text("");
                        aq.id(R.id.etVisitCovidSecondDoseDate).text("");
                        aq.id(R.id.trCovidFirstDoseDate).getView().setVisibility(View.GONE);
                        aq.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.GONE);
                    /*if (i == 0) {
                        aq.id(R.id.trCovidFirstDoseDate).getView().setVisibility(View.GONE);
                        aq.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.GONE);
                    } else*/

                        if (i == 1) {
                            aq.id(R.id.trCovidFirstDoseDate).getView().setVisibility(View.VISIBLE);
                            aq.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.GONE);
                        } else if (i == 2) {
                            aq.id(R.id.trCovidFirstDoseDate).getView().setVisibility(View.VISIBLE);
                            aq.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.VISIBLE);
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });




        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    // Set baby text change listner
    private void setBabyDetailsTextChangeListener(EditText wt, EditText temp, EditText rrate, EditText chregid) {
        aq.id(wt).getEditText().addTextChangedListener(watcher);
        aq.id(temp).getEditText().addTextChangedListener(watcher);
        aq.id(rrate).getEditText().addTextChangedListener(watcher);
        aq.id(chregid).getEditText().addTextChangedListener(watcher);
    }

    private void displayPNCTabs() {
        if (isMotherMortality || isWomanDeactivated) {  //30Nov2019 - Bindu - add deactivation condition
            aq.id(R.id.btnmotherdangersigns).gone();
            aq.id(R.id.scrmotherdangersigns).gone();
            aq.id(R.id.txtselectedbtnheading).text(getResources().getString(R.string.newborndangersigns));
            aq.id(R.id.btnnewborndangersigns).getImageView().setBackgroundColor(getResources().getColor(R.color.gray));
            //30Nov2019 - Bindu - if mortality then hide mther examinations
            aq.id(R.id.txtexaminationsmother).gone();
            aq.id(R.id.tblexaminationsmother).gone();

            //21May2021 Bindu hide covid vaccine for mortality or deactivated
            aq.id(R.id.llcovidvaccine).gone();

        } else {
            aq.id(R.id.btnmotherdangersigns).getImageView().setBackgroundColor(getResources().getColor(R.color.gray));
            aq.id(R.id.txtselectedbtnheading).text(getResources().getString(R.string.motherdangersigns));
            aq.id(R.id.scrmotherdangersigns).visible();
            //30Nov2019 - Bindu - if mortality then hide mther examinations
            aq.id(R.id.txtexaminationsmother).visible();
            aq.id(R.id.tblexaminationsmother).visible();
        }

        int mortsize = childListMortID != null ? childListMortID.size() : 0;
        //01Dec2019 - bindu
        int childdeactsize = childListDeactivateID != null ? childListDeactivateID.size() : 0;

        if (delnoofchildren == mortsize || delnoofchildren == childdeactsize) { //01dec2019 - add deactivate size
            isAllChildMortality = true;
            isAllChildDeactivated = true; //01Dec2019- Bindu
            aq.id(R.id.btnnewborndangersigns).gone();
            aq.id(R.id.scrnewborndangersigns).gone();
            //30Nov2019 - Bindu - if mortality then hide baby examinations
            aq.id(R.id.tblexaminationschild1).gone();
            aq.id(R.id.tblexaminationschild2).gone();
            aq.id(R.id.tblexaminationschild3).gone();
            aq.id(R.id.tblexaminationschild4).gone();
        } else {
            aq.id(R.id.scrnewborndangersigns).visible();
            displayChildDetails(findViewById(R.id.llnbsmallbabylbwsumm).getId());
            displayChildDetails(findViewById(R.id.llnbnomeconiumpassedsumm).getId());
            displayChildDetails(findViewById(R.id.llnbnourinepassedsumm).getId());
            displayChildDetails(findViewById(R.id.llnbinabilitytosucksumm).getId());
            displayChildDetails(findViewById(R.id.llnbfastbreathingsumm).getId());
            displayChildDetails(findViewById(R.id.llnbinabilitytopassurineandstoolsumm).getId());
            displayChildDetails(findViewById(R.id.llnbumbilicalstumpredorpussumm).getId());
            displayChildDetails(findViewById(R.id.llnbpustulessumm).getId());
            displayChildDetails(findViewById(R.id.llnbjaundicesumm).getId());
            displayChildDetails(findViewById(R.id.llnbfeversumm).getId());
            displayChildDetails(findViewById(R.id.llnbdiarrhoeasumm).getId());
            displayChildDetails(findViewById(R.id.llnbbloodinstoolssumm).getId());
            displayChildDetails(findViewById(R.id.llnblethargicbabysumm).getId());
            //09dec2019 -Bindu
            displayChildDetails(findViewById(R.id.llnbfitsbabysumm).getId());
            displayChildDetails(findViewById(R.id.llnbeyesareredbabysumm).getId());
            displayChildDetails(findViewById(R.id.llnbpallorofpalmsbabysumm).getId());
            displayChildDetails(findViewById(R.id.llnbyellowpalmsbabysumm).getId());
            displayChildDetails(findViewById(R.id.llnbbluepalmsbabysumm).getId());
            displayChildDetails(findViewById(R.id.llnbfeelscoldorhotbabysumm).getId());
            displayChildDetails(findViewById(R.id.llnbbleedingbabysumm).getId());
            displayChildDetails(findViewById(R.id.llnbabdominaldistensionbabysumm).getId());
            displayChildDetails(findViewById(R.id.llnbanybirthdefectbabysumm).getId());
            //Manikanta 10feb2021
            displayChildDetails(findViewById(R.id.llnbfeelswarmbabysumm).getId());
            displayChildDetails(findViewById(R.id.llnbfeelstoowarmbabysumm).getId());


//            11May2021 Bindu add sunken, vomiting and sepsis
            displayChildDetails(findViewById(R.id.llnbsunkenfontsumm).getId());
            displayChildDetails(findViewById(R.id.llnbvomitingsumm).getId());
            displayChildDetails(findViewById(R.id.llnbsepsissumm).getId());
//            14May2021 Bindu add pneumonia
            displayChildDetails(findViewById(R.id.llnbpneumoniasumm).getId());

            //Mani 12Sep2021
            displayChildDetails(findViewById(R.id.llnbhypothermiababysumm).getId());
            displayChildDetails(findViewById(R.id.llnbraspiratorydistressbabysumm).getId());
            displayChildDetails(findViewById(R.id.llnbcyanosisbabysumm).getId());
            displayChildDetails(findViewById(R.id.llnbexcessivewtlossbabysumm).getId());
            displayChildDetails(findViewById(R.id.llnbconvulsionbabysumm).getId());
            displayChildDetails(findViewById(R.id.llnbpretermsumm).getId());

            //30Nov2019 - Bindu - Show or hide baby examinations
            displayBabyExaminations();
        }

        if ((isMotherMortality || isWomanDeactivated) && (isAllChildMortality || isAllChildDeactivated)) { //04Dec2019 - Bindu  add deactivated
            aq.id(R.id.btnmotherdangersigns).gone();
            aq.id(R.id.scrmotherdangersigns).gone();
            aq.id(R.id.btnnewborndangersigns).gone();
            aq.id(R.id.scrnewborndangersigns).gone();
            aq.id(R.id.btnSummary).gone();
            aq.id(R.id.scrhomevisitsummary).gone();
            aq.id(R.id.imgSummaryAddPhoto).gone();
            aq.id(R.id.txtselectedbtnheading).gone();
            aq.id(R.id.txtmortality).visible();
            aq.id(R.id.txtmortality).text(getResources().getString(R.string.servicenotapplicable));
            aq.id(R.id.txtdetailsnote).gone();
            aq.id(R.id.llsave).gone();
            aq.id(R.id.llhmdate).gone();

        }
    }

    //30Nov2019 - Bindu - Display baby examination
    private void displayBabyExaminations() {
        int count = delnoofchildren;
        for (int i = 0; i < count; i++) {
            //chk for child mortality and set
            if (childMortality != null && childMortality.size() > 0) {
                if (childMortality.contains(i + 1)) {
                    aq.id(nbbabyexamination + (i)).gone();
                    aq.id(nbtblchildregdetails + (i)).gone();

                    aq.id(nbbabytabletdetails + (i)).gone();

                } else {
                    aq.id(nbbabyexamination + (i)).visible();
                    aq.id(nbtblchildregdetails + (i)).visible();
                    aq.id(nbbabytabletdetails + (i)).visible();

                }
            } else if (childDeactivated != null && childDeactivated.size() > 0) { //01Dec2019 - Bindu - Chk deactivate
                if (childDeactivated.contains(i + 1)) {
                    aq.id(nbbabyexamination + (i)).gone();
                    aq.id(nbtblchildregdetails + (i)).gone();
                    aq.id(nbbabytabletdetails + (i)).gone();

                } else {
                    aq.id(nbbabyexamination + (i)).visible();
                    aq.id(nbtblchildregdetails + (i)).visible();
                    aq.id(nbbabytabletdetails + (i)).visible();

                }
            } else {
                aq.id(nbbabyexamination + (i)).visible();
                aq.id(nbtblchildregdetails + (i)).visible();
                aq.id(nbbabytabletdetails + (i)).visible();

            }
        }
    }

    // Initial view
    private void initialView() throws Exception {
        aq.id(R.id.scrhomevisitsummary).gone();
        aq.id(R.id.imgSummaryAddPhoto).gone();
        HomeVisitRepository homeRepo = new HomeVisitRepository(databaseHelper);
        visitNum = homeRepo.getLastHomeVisitNoWoman(appState.sessionUserId, woman.getWomanId());



        aq.id(R.id.txtheading).text(getResources().getString(R.string.pnchomevisit) + " - " + " # " + visitNum);
        aq.id(R.id.etvisitdate).text(DateTimeUtil.getTodaysDate());
        calculateDaysfromDel(DateTimeUtil.getTodaysDate());

        deliveryInfoList = new ArrayList<>();
        DeliveryRepository deliveryRepository = new DeliveryRepository(databaseHelper);
        deliveryInfoList = deliveryRepository.getDeliveryData(woman.getWomanId(), appState.sessionUserId);

        //30Nov2019 - Bindu - add deactivate status
        if ((woman.getDateDeactivated() != null && woman.getDateDeactivated().trim().length() > 0) || user.getIsDeactivated() == 1)
            isWomanDeactivated = true;

        if (deliveryInfoList != null && deliveryInfoList.size() > 0) {
            delnoofchildren = deliveryInfoList.get(0).getDelNoOfChildren();
            //Check for mother mortality and hide mother details
            if (deliveryInfoList.get(0).getDelMothersDeath() != null && deliveryInfoList.get(0).getDelMothersDeath().equalsIgnoreCase("Y")) {
                isMotherMortality = true;
            }

            ChildRepository childRepository = new ChildRepository(databaseHelper);

            //29Nov2019 - Bindu - get childid alive
            singleChildAliveId = childRepository.getChildAliveID(woman.getWomanId());
            //01Dec2019 - Bindu - get child id active
            singleChildActiveId = childRepository.getChildActiveID(woman.getWomanId());

            childListIds = childRepository.getChildData(woman.getWomanId());
            for (int i = 1; i <= childListIds.size(); i++) {
                myChildIDs.put(i, childListIds.get(i - 1).getChildID());
            }
            childListMortID = childRepository.getChildMortalityData(woman.getWomanId());
            if (childListMortID != null && childListMortID.size() > 0) {
                isAnyChildMortality = true;
                childMortality = new ArrayList<>(4);
                for (int i = 0; i < childListMortID.size(); i++) {
                    childMortality.add(childListMortID.get(i).getChildNo());
                }

                unionchildlist.addAll(childMortality);
                alivechildlist.retainAll(childMortality);
                unionchildlist.removeAll(alivechildlist);
            }

            //01Dec2019 -  Bindu - check for deactivate
            childListDeactivateID = childRepository.getChildDeactivatedData(woman.getWomanId());
            if (childListDeactivateID != null && childListDeactivateID.size() > 0) {
                childDeactivated = new ArrayList<>(4);
                for (int i = 0; i < childListDeactivateID.size(); i++) {
                    childDeactivated.add(childListDeactivateID.get(i).getChildNo());
                }
                unionchildlist.addAll(childDeactivated);
                alivechildlist.retainAll(childDeactivated);
                unionchildlist.removeAll(alivechildlist);
            }

        }

        /*if(childListMortID != null && childListMortID.size() <= 0) {
            aq.id(R.id.btnnewborndangersigns).visible();
            //display no of child

        }else {
            aq.id(R.id.btnnewborndangersigns).gone();
        }*/

        //mani 14Sep2021
        rgIFA = findViewById(R.id.rgChildVisitIFA1);
        rgConsumeIFA = findViewById(R.id.rg_consumingIFA1);
        rgIFA2 = findViewById(R.id.rgChildVisitIFA2);
        rgConsumeIFA2 = findViewById(R.id.rg_consumingIFA2);
        rgIFA3 = findViewById(R.id.rgChildVisitIFA3);
        rgConsumeIFA3 = findViewById(R.id.rg_consumingIFA3);
        rgIFA4 = findViewById(R.id.rgChildVisitIFA3);
        rgConsumeIFA4 = findViewById(R.id.rg_consumingIFA3);

        spnIFAReasons1 = findViewById(R.id.spnChlIFAReasons1);
        spnIFAReasons2 = findViewById(R.id.spnChlIFAReasons2);
        spnIFAReasons3 = findViewById(R.id.spnChlIFAReasons3);
        spnIFAReasons4 = findViewById(R.id.spnChlIFAReasons4);


        displayPNCTabs();
        aq.id(R.id.etbpdonedate).enabled(false).background(R.drawable.editextdisable_none);
        aq.id(R.id.etpulsedate).enabled(false).background(R.drawable.editextdisable_none);
        aq.id(R.id.ettempdonedate).enabled(false).background(R.drawable.editextdisable_none);
        aq.id(R.id.ethbdonedate).enabled(false).background(R.drawable.editextdisable_none);

        //30Nov2019 - Bindu - Baby1 details date disable

        DisableBabyDateFields(aq.id(R.id.etweightdatebaby1).getEditText(), aq.id(R.id.ettempdonedatebaby1).getEditText(), aq.id(R.id.etrespratedonedatebaby1).getEditText(), aq.id(R.id.etchildregdate1).getEditText());
        DisableBabyDateFields(aq.id(R.id.etweightdatebaby2).getEditText(), aq.id(R.id.ettempdonedatebaby2).getEditText(), aq.id(R.id.etrespratedonedatebaby2).getEditText(), aq.id(R.id.etchildregdate2).getEditText());
        DisableBabyDateFields(aq.id(R.id.etweightdatebaby3).getEditText(), aq.id(R.id.ettempdonedatebaby3).getEditText(), aq.id(R.id.etrespratedonedatebaby3).getEditText(), aq.id(R.id.etchildregdate3).getEditText());
        DisableBabyDateFields(aq.id(R.id.etweightdatebaby4).getEditText(), aq.id(R.id.ettempdonedatebaby4).getEditText(), aq.id(R.id.etrespratedonedatebaby4).getEditText(), aq.id(R.id.etchildregdate4).getEditText());

        //        16May2021 Bindu hide covid result
        aq.id(R.id.trCovidResult).gone();
        //        17May2021 Bindu
        getCovidVaccineDetails();
    }

    //    get covid vaccine details
   /* private void getCovidVaccineDetails() throws Exception{
        CovidDetailsRepository covRepo = new CovidDetailsRepository(databaseHelper);
        covVacItems = covRepo.getVisitCovidVaccineDetails(appState.selectedWomanId);
        if(covVacItems != null && covVacItems.size() > 0) {
            for(int i=0; i < covVacItems.size(); i++) {
                if(covVacItems.get(i).getCovidVaccinated().equalsIgnoreCase(getResources().getString(R.string.yesshortform))) {
                    isvaccinated = true;
                    opencloseAccordion(aq.id(R.id.llcovidvaccinedetails).getView(), aq.id(R.id.txtcovidvaccine).getTextView());
                    aq.id(R.id.llcovidvaccinedetails).backgroundColor(getResources().getColor(R.color.lightgray));
                    aq.id(R.id.trCovidVaccinDose).gone();
                    aq.id(R.id.rd_CovidVaccineYes).enabled(false);
                    aq.id(R.id.rd_CovidVaccineNo).enabled(false);
                    aq.id(R.id.rd_CovidVaccineYes).checked(true);
                    if((covVacItems.get(i).getCovidFirstDoseDate().toString().length() > 0) && covVacItems.get(i).getCovidSecondDoseDate().toString().length() > 0){
                        aq.id(R.id.trCovidFirstDoseDate).getView().setVisibility(View.VISIBLE);
                        aq.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.VISIBLE);
                        aq.id(R.id.etVisitCovidFirstDoseDate).text(covVacItems.get(i).getCovidFirstDoseDate());
                        aq.id(R.id.etVisitCovidFirstDoseDate).enabled(false).background(R.drawable.edittext_disable);
                        aq.id(R.id.etVisitCovidSecondDoseDate).text(covVacItems.get(i).getCovidSecondDoseDate());
                        aq.id(R.id.etVisitCovidSecondDoseDate).enabled(false).background(R.drawable.edittext_disable);
                        aq.id(R.id.imgclearfirstdosedate).gone();
                        aq.id(R.id.imgclearsecdosedate).gone();
                    }
                    else if(covVacItems.get(i).getCovidFirstDoseDate().toString().length() > 0 && covVacItems.get(i).getCovidSecondDoseDate().toString().length() <= 0) {
                        aq.id(R.id.etVisitCovidFirstDoseDate).text(covVacItems.get(i).getCovidFirstDoseDate());
                        aq.id(R.id.etVisitCovidFirstDoseDate).enabled(false).background(R.drawable.edittext_disable);
                        aq.id(R.id.trCovidFirstDoseDate).getView().setVisibility(View.VISIBLE);
                        aq.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.VISIBLE);
                        aq.id(R.id.imgclearfirstdosedate).gone();
                    }
                    *//*else if(covVacItems.get(i).getCovidSecondDoseDate().length() > 0) {
                        aq.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.VISIBLE);
                        aq.id(R.id.etVisitCovidSecondDoseDate).text(covVacItems.get(i).getCovidSecondDoseDate());
                        aq.id(R.id.etVisitCovidSecondDoseDate).enabled(false).background(R.drawable.edittext_disable);
                    }*//*
                }
            }
        }
    }*/

    //    get covid vaccine details
    private void getCovidVaccineDetails() throws Exception {
        CovidDetailsRepository covRepo = new CovidDetailsRepository(databaseHelper);
        covVacItems = covRepo.getVisitCovidVaccineDetails(appState.selectedWomanId);
        if (covVacItems != null && covVacItems.size() > 0) {
            if (covVacItems.size() > 0) { // if both the doses are completed
                opencloseAccordion(aq.id(R.id.llcovidvaccinedetails).getView(), aq.id(R.id.txtcovidvaccine).getTextView());
                aq.id(R.id.llcovidvaccinedetails).backgroundColor(getResources().getColor(R.color.lightgray));
                aq.id(R.id.trCovidVaccinDose).gone();
                aq.id(R.id.rd_CovidVaccineYes).enabled(false);
                aq.id(R.id.rd_CovidVaccineNo).enabled(false);
                aq.id(R.id.rd_CovidVaccineYes).checked(true);
            }
            for (int i = 0; i < covVacItems.size(); i++) {
                isvaccinated = true;
//                if(covVacItems.get(i).getCovidVaccinated().equalsIgnoreCase(getResources().getString(R.string.yesshortform))) {
//                 if(covVacItems.get(i).getCovidVaccinatedNo().equalsIgnoreCase("1")) {

                //isvaccinated = true;

//                    if((covVacItems.get(i).getCovidFirstDoseDate().toString().length() > 0) && covVacItems.get(i).getCovidSecondDoseDate().toString().length() > 0){
//                if(covVacItems.size() == 2) {
                if (covVacItems.get(i).getCovidVaccinatedNo().equalsIgnoreCase("1")) {
                    aq.id(R.id.trCovidFirstDoseDate).getView().setVisibility(View.VISIBLE);
                    aq.id(R.id.etVisitCovidFirstDoseDate).text(covVacItems.get(i).getCovidVaccinatedDate());
                    aq.id(R.id.etVisitCovidFirstDoseDate).enabled(false).background(R.drawable.edittext_disable);
                    aq.id(R.id.imgclearfirstdosedate).gone();
                    aq.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.VISIBLE);
                }
                if (covVacItems.get(i).getCovidVaccinatedNo().equalsIgnoreCase("2")) {
                    aq.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.VISIBLE);
                    aq.id(R.id.etVisitCovidSecondDoseDate).text(covVacItems.get(i).getCovidVaccinatedDate());
                    aq.id(R.id.etVisitCovidSecondDoseDate).enabled(false).background(R.drawable.edittext_disable);
                    aq.id(R.id.imgclearsecdosedate).gone();
                }
//                }
                    /*else if(covVacItems.get(i).getCovidFirstDoseDate().toString().length() > 0 && covVacItems.get(i).getCovidSecondDoseDate().toString().length() <= 0) {
                        aq.id(R.id.etVisitCovidFirstDoseDate).text(covVacItems.get(i).getCovidFirstDoseDate());
                        aq.id(R.id.etVisitCovidFirstDoseDate).enabled(false).background(R.drawable.edittext_disable);
                        aq.id(R.id.trCovidFirstDoseDate).getView().setVisibility(View.VISIBLE);
                        aq.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.VISIBLE);
                        aq.id(R.id.imgclearfirstdosedate).gone();
                    }*/
                    /*else if(covVacItems.get(i).getCovidSecondDoseDate().length() > 0) {
                        aq.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.VISIBLE);
                        aq.id(R.id.etVisitCovidSecondDoseDate).text(covVacItems.get(i).getCovidSecondDoseDate());
                        aq.id(R.id.etVisitCovidSecondDoseDate).enabled(false).background(R.drawable.edittext_disable);
                    }*/
//                }
            }
        }
    }

    private void DisableBabyDateFields(EditText wtdt, EditText tempdt, EditText rratedt, EditText cregdatedt) {
        aq.id(wtdt).enabled(false).background(R.drawable.editextdisable_none);

        /*wtdt.setEnabled(false).(R.drawable.editextdisable_none); */
        aq.id(tempdt).enabled(false).background(R.drawable.editextdisable_none);
        aq.id(rratedt).enabled(false).background(R.drawable.editextdisable_none);
        aq.id(cregdatedt).enabled(false).background(R.drawable.editextdisable_none);
    }

    private void displayChildDetails(int view) {
        LinearLayout ll = findViewById(view);
        int count = delnoofchildren;
        for (int i = 0; i < count; i++) {
            View v = ll.getChildAt(i);
            if (v instanceof CheckBox) {
                //chk for child mortality and set
                if (childMortality != null && childMortality.size() > 0) {
                    if (childMortality.contains(i + 1)) {
                        (v).setVisibility(View.GONE);
                    } else
                        (v).setVisibility(View.VISIBLE);
                } else if (childDeactivated != null && childDeactivated.size() > 0) { //01Dec2019 -Bindu - add deactivation
                    if (childDeactivated.contains(i + 1)) {
                        (v).setVisibility(View.GONE);
                    } else
                        (v).setVisibility(View.VISIBLE);
                } else {
                    (v).setVisibility(View.VISIBLE);
                }
            }
        }
    }


    // Calculate diff of days from del
    private void calculateDaysfromDel(String selectedDate) {
        try {
            int noOfDays = DateTimeUtil.getNumberOfDaysFromLMPVisitDate(woman.getRegADDate(), selectedDate);
            aq.id(R.id.txtgestage).text(act.getResources().getString(R.string.txtdeldiffdaysatvisit) + "- " + noOfDays + "D");
            strstatus = noOfDays + "D";
            /*week = noOfDays / 7;
            int days = noOfDays % 7;
            if (days > 0) {
                *//*aq.id(R.id.txtgestage).text(act.getResources().getString(R.string.txtdeldiffdaysatvisit) + "- " + week + "W " + days + "D");
                strstatus = week + "W " + days + "D";*//*
                aq.id(R.id.txtgestage).text(act.getResources().getString(R.string.txtdeldiffdaysatvisit) + "- " + noOfDays + "D");
                strstatus = days + "D";
            }else
                strstatus = days + "D";*/
            /*else {
                aq.id(R.id.txtgestage).text(act.getResources().getString(R.string.txtdeldiffdaysatvisit) + "- " + week + "W ");
                strstatus = week + "W " ;
            }*/
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    /**
     * This is a Recursive method that traverse through the group and subgroup
     * of view Sets text
     *
     * @throws Exception
     */
    private ArrayList<View> initializeScreen(View v) throws Exception {
        if (!(v instanceof ViewGroup)) {
            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);

            // TextView - Assign Labels
            if (v.getClass() == TextView.class) {
                aq.id(v.getId()).text(getSS(aq.id(v.getId()).getText().toString()));
            }
            if (v.getClass() == RadioButton.class || v.getClass() == AppCompatRadioButton.class) {
                aq.id(v.getId()).getButton().setOnClickListener(this);
                aq.id(v.getId()).text(getSS(aq.id(v.getId()).getText().toString()));
            }

            // EditText hint - Assign Labels
            if (v.getClass() == EditText.class) {
                if (aq.id(v.getId()).getEditText().getHint() != null)
                    aq.id(v.getId()).getEditText().setHint(getSS(aq.id(v.getId()).getEditText().getHint() + ""));
            }

            // ImageButton/ImageView - OnClickListener commonClick
            if (v.getClass() == ImageButton.class || v.getClass() == ImageView.class || v.getClass() == AppCompatImageButton.class || v.getClass() == AppCompatImageView.class) {
                aq.id(v.getId()).getImageView().setOnClickListener(this);
            }

            // Checkbox - OnClickListener commonClick
            if (v.getClass() == CheckBox.class || v.getClass() == AppCompatCheckBox.class) {
                aq.id(v.getId()).getCheckBox().setOnClickListener(this);
            }
            return viewArrayList;
        }

        ArrayList<View> result = new ArrayList<View>();
        ViewGroup viewGroup = (ViewGroup) v;
        for (int i = 0; i < viewGroup.getChildCount(); i++) {

            View child = viewGroup.getChildAt(i);

            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);
            viewArrayList.addAll(initializeScreen(child));

            result.addAll(viewArrayList);
        }
        return result;
    }

    /**
     * Returns SpannableStringBuilder for a given String
     */
    public SpannableStringBuilder getSS(String srcText) {
        return getSSC(srcText);
    }

    // Returns SpannableStringBuilder for a given String
    public static SpannableStringBuilder getSSC(String msg) {
        SpannableStringBuilder SS;
        SS = new SpannableStringBuilder(msg);
        return SS;
    }

    @Override
    public void onClick(View view) {
        try {
            switch (view.getId()) {
                //                16May2021 Bindu
                case R.id.imgclearcovidresultdate: {
                    clearDate(aq.id(R.id.etVisitCovidTestDate).getEditText(), aq.id(R.id.imgclearcovidresultdate).getImageView());
                    break;
                }
                case R.id.imgclearfirstdosedate: {
                    clearDate(aq.id(R.id.etVisitCovidFirstDoseDate).getEditText(), aq.id(R.id.imgclearfirstdosedate).getImageView());
                    break;
                }
                case R.id.imgclearsecdosedate: {
                    clearDate(aq.id(R.id.etVisitCovidSecondDoseDate).getEditText(), aq.id(R.id.imgclearsecdosedate).getImageView());
                    break;
                }
                case R.id.btnmotherdangersigns: {
                    aq.id(R.id.txtselectedbtnheading).text(getResources().getString(R.string.motherdangersigns));
                    hidetabs();
                    aq.id(R.id.btnmotherdangersigns).getImageView().setBackgroundColor(getResources().getColor(R.color.gray));
                    aq.id(R.id.scrmotherdangersigns).visible();
                    aq.id(R.id.scrhomevisitsummary).gone();
                    aq.id(R.id.imgSummaryAddPhoto).gone();
                    aq.id(R.id.scrnewborndangersigns).gone();
                    aq.id(R.id.btnsavehomevisit).getImageView().setImageResource(R.drawable.ic_arrow_forward);
                    isBtnSummaryClicked = false;
                    break;
                }
                case R.id.btnnewborndangersigns: {
                    aq.id(R.id.txtselectedbtnheading).text(getResources().getString(R.string.newborndangersigns));
                    hidetabs();
                    aq.id(R.id.btnnewborndangersigns).getImageView().setBackgroundColor(getResources().getColor(R.color.gray));
                    aq.id(R.id.scrnewborndangersigns).visible();
                    aq.id(R.id.scrhomevisitsummary).gone();
                    aq.id(R.id.imgSummaryAddPhoto).gone();
                    aq.id(R.id.scrmotherdangersigns).gone();
                    aq.id(R.id.btnsavehomevisit).getImageView().setImageResource(R.drawable.ic_arrow_forward);
                    isBtnSummaryClicked = false;
                    break;
                }
                case R.id.btnSummary: {
                    if (validatecovidhealthissue()) { //17May2021 Bindu
                        if (validateChildCheck())
                            displayBtnSummaryClickedAction();
                    }

                    break;
                }
                case R.id.btnsavehomevisit: {
                    if (isBtnSummaryClicked) {
                        if (validatePNCHomevisit())
                            confirmAlert();
                    } else {
                        /*if(validatecovidhealthissue()) { //17May2021 Bindu*/
                        if (validateChildCheck()) // 26Apr2021 Bindu - check child cond check
                            displayBtnSummaryClickedAction();
                        /*}*/
                    }
                    break;
                }
                //29Nov2019 - Bindu - add view click listener
                case R.id.btnviewhomevisit: {
                   // if (visitNum > 1) {
                        Intent homevisitlist = new Intent(PncHomeVisit.this, HomeVisitListActivity.class);
                        homevisitlist.putExtra("woman", woman);
                        homevisitlist.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        displayAlert(getResources().getString(R.string.exit), homevisitlist);
                   /* } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.norecords), Toast.LENGTH_SHORT).show();
                    }*/
                    break;
                }
                case R.id.chkisreferred: {
                    showreferaldetails();
                    break;
                }
                //Mother danger signs
                case R.id.chktearinperineum: {
                    opencloseAccordionCheckbox(aq.id(R.id.chktearinperineum).getCheckBox());
                    break;
                }
                case R.id.chkinabilitytopassurine: {
                    opencloseAccordionCheckbox(aq.id(R.id.chkinabilitytopassurine).getCheckBox());
                    break;
                }
                case R.id.chkburninginurination: {
                    opencloseAccordionCheckbox(aq.id(R.id.chkburninginurination).getCheckBox());
                    break;
                }
                case R.id.chkdifficultyinbreastfeeding: {
                    opencloseAccordionCheckbox(aq.id(R.id.chkdifficultyinbreastfeeding).getCheckBox());
                    break;
                }
                case R.id.chkpaininlowerabdomen: {
                    opencloseAccordionCheckbox(aq.id(R.id.chkpaininlowerabdomen).getCheckBox());
                    break;
                }
                case R.id.chkpusformation: {
                    opencloseAccordionCheckbox(aq.id(R.id.chkpusformation).getCheckBox());
                    break;
                }
                case R.id.chkexcessivevaginalbleeding: {
                    opencloseAccordionCheckbox(aq.id(R.id.chkexcessivevaginalbleeding).getCheckBox());
                    break;
                }
                case R.id.chkinabilitytocontroldefecationorurine: {
                    opencloseAccordionCheckbox(aq.id(R.id.chkinabilitytocontroldefecationorurine).getCheckBox());
                    break;
                }
                case R.id.chkfoulsmellingvaginaldischarge: {
                    opencloseAccordionCheckbox(aq.id(R.id.chkfoulsmellingvaginaldischarge).getCheckBox());
                    break;
                }
                case R.id.chkbreastsproblems: {
                    opencloseAccordionCheckbox(aq.id(R.id.chkbreastsproblems).getCheckBox());
                    break;
                }
                case R.id.chkdiffcultyinbreathing: {
                    opencloseAccordionCheckbox(aq.id(R.id.chkdiffcultyinbreathing).getCheckBox());
                    break;
                }
                case R.id.chkswollenhands: {
                    opencloseAccordionCheckbox(aq.id(R.id.chkswollenhands).getCheckBox());
                    break;
                }
                case R.id.chkfainting: {
                    opencloseAccordionCheckbox(aq.id(R.id.chkfainting).getCheckBox());
                    break;
                }
                //09Dec2019 - Bindu
                case R.id.chkmotherfits: {
                    opencloseAccordionCheckbox(aq.id(R.id.chkmotherfits).getCheckBox());
                    break;
                }
                case R.id.chkmotherfever: {
                    opencloseAccordionCheckbox(aq.id(R.id.chkmotherfever).getCheckBox());
                    break;
                }
                case R.id.chkothersymptomsmother: {
                    if (aq.id(R.id.chkothersymptomsmother).isChecked()) {
                        aq.id(R.id.etothersymptomsmother).visible();
                    } else {
                        aq.id(R.id.etothersymptomsmother).gone();
                        aq.id(R.id.etothersymptomsmother).text("");
                    }
                    break;
                }
                // New born danger signs
                case R.id.chkothersymptomsnb: {
                    if (aq.id(R.id.chkothersymptomsnb).isChecked()) {
                        aq.id(R.id.etothersymptomsnb).visible();
                    } else {
                        aq.id(R.id.etothersymptomsnb).gone();
                        aq.id(R.id.etothersymptomsnb).text("");
                    }
                    break;
                }
                case R.id.chknbsmallbabylbw:
                    opencloseAccordion(aq.id(R.id.llnbsmallbabylbwdetails).getView(), aq.id(R.id.chknbsmallbabylbw).getCheckBox(), aq.id(R.id.chknbsmallbabylbw1).getCheckBox(), aq.id(R.id.chknbsmallbabylbw2).getCheckBox(), aq.id(R.id.chknbsmallbabylbw3).getCheckBox(), aq.id(R.id.chknbsmallbabylbw4).getCheckBox());
                    break;
                case R.id.chknbsmallbabylbw1:
                case R.id.chknbsmallbabylbw2:
                case R.id.chknbsmallbabylbw3:
                case R.id.chknbsmallbabylbw4:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbsmallbabylbw).getCheckBox(), aq.id(R.id.chknbsmallbabylbw1).getCheckBox(), aq.id(R.id.chknbsmallbabylbw2).getCheckBox(), aq.id(R.id.chknbsmallbabylbw3).getCheckBox(), aq.id(R.id.chknbsmallbabylbw4).getCheckBox());
                    break;

                //no meconium
                case R.id.chknbnomeconiumpassed:
                    opencloseAccordion(aq.id(R.id.llnbnomeconiumpasseddetails).getView(), aq.id(R.id.chknbnomeconiumpassed).getCheckBox(), aq.id(R.id.chknbnomeconiumpassed1).getCheckBox(), aq.id(R.id.chknbnomeconiumpassed2).getCheckBox(), aq.id(R.id.chknbnomeconiumpassed3).getCheckBox(), aq.id(R.id.chknbnomeconiumpassed4).getCheckBox());
                    break;
                case R.id.chknbnomeconiumpassed1:
                case R.id.chknbnomeconiumpassed2:
                case R.id.chknbnomeconiumpassed3:
                case R.id.chknbnomeconiumpassed4:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbnomeconiumpassed).getCheckBox(), aq.id(R.id.chknbnomeconiumpassed1).getCheckBox(), aq.id(R.id.chknbnomeconiumpassed2).getCheckBox(), aq.id(R.id.chknbnomeconiumpassed3).getCheckBox(), aq.id(R.id.chknbnomeconiumpassed4).getCheckBox());
                    break;

                // no urine passed
                case R.id.chknbnourinepassed:
                    opencloseAccordion(aq.id(R.id.llnbnourinepasseddetails).getView(), aq.id(R.id.chknbnourinepassed).getCheckBox(), aq.id(R.id.chknbnourinepassed1).getCheckBox(), aq.id(R.id.chknbnourinepassed2).getCheckBox(), aq.id(R.id.chknbnourinepassed3).getCheckBox(), aq.id(R.id.chknbnourinepassed4).getCheckBox());
                    break;
                case R.id.chknbnourinepassed1:
                case R.id.chknbnourinepassed2:
                case R.id.chknbnourinepassed3:
                case R.id.chknbnourinepassed4:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbnourinepassed).getCheckBox(), aq.id(R.id.chknbnourinepassed1).getCheckBox(), aq.id(R.id.chknbnourinepassed2).getCheckBox(), aq.id(R.id.chknbnourinepassed3).getCheckBox(), aq.id(R.id.chknbnourinepassed4).getCheckBox());
                    break;

                //inability to suck
                case R.id.chknbinabilitytosuck:
                    opencloseAccordion(aq.id(R.id.llnbinabilitytosuckdetails).getView(), aq.id(R.id.chknbinabilitytosuck).getCheckBox(), aq.id(R.id.chknbinabilitytosuck1).getCheckBox(), aq.id(R.id.chknbinabilitytosuck2).getCheckBox(), aq.id(R.id.chknbinabilitytosuck3).getCheckBox(), aq.id(R.id.chknbinabilitytosuck4).getCheckBox());
                    break;
                case R.id.chknbinabilitytosuck1:
                case R.id.chknbinabilitytosuck2:
                case R.id.chknbinabilitytosuck3:
                case R.id.chknbinabilitytosuck4:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbinabilitytosuck).getCheckBox(), aq.id(R.id.chknbinabilitytosuck1).getCheckBox(), aq.id(R.id.chknbinabilitytosuck2).getCheckBox(), aq.id(R.id.chknbinabilitytosuck3).getCheckBox(), aq.id(R.id.chknbinabilitytosuck4).getCheckBox());
                    break;

                //nb fast breathing
                case R.id.chknbfastbreathing:
                    opencloseAccordion(aq.id(R.id.llnbfastbreathingdetails).getView(), aq.id(R.id.chknbfastbreathing).getCheckBox(), aq.id(R.id.chknbfastbreathing1).getCheckBox(), aq.id(R.id.chknbfastbreathing2).getCheckBox(), aq.id(R.id.chknbfastbreathing3).getCheckBox(), aq.id(R.id.chknbfastbreathing4).getCheckBox());
                    break;
                case R.id.chknbfastbreathing1:
                case R.id.chknbfastbreathing2:
                case R.id.chknbfastbreathing3:
                case R.id.chknbfastbreathing4:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbfastbreathing).getCheckBox(), aq.id(R.id.chknbfastbreathing1).getCheckBox(), aq.id(R.id.chknbfastbreathing2).getCheckBox(), aq.id(R.id.chknbfastbreathing3).getCheckBox(), aq.id(R.id.chknbfastbreathing4).getCheckBox());
                    break;

                //nb inability to suck
                case R.id.chknbinabilitytopassurineandstool:
                    opencloseAccordion(aq.id(R.id.llnbinabilitytopassurineandstooldetails).getView(), aq.id(R.id.chknbinabilitytopassurineandstool).getCheckBox(), aq.id(R.id.chknbinabilitytopassurineandstool1).getCheckBox(), aq.id(R.id.chknbinabilitytopassurineandstool2).getCheckBox(), aq.id(R.id.chknbinabilitytopassurineandstool3).getCheckBox(), aq.id(R.id.chknbinabilitytopassurineandstool4).getCheckBox());
                    break;
                case R.id.chknbinabilitytopassurineandstool1:
                case R.id.chknbinabilitytopassurineandstool2:
                case R.id.chknbinabilitytopassurineandstool3:
                case R.id.chknbinabilitytopassurineandstool4:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbinabilitytopassurineandstool).getCheckBox(), aq.id(R.id.chknbinabilitytopassurineandstool1).getCheckBox(), aq.id(R.id.chknbinabilitytopassurineandstool2).getCheckBox(), aq.id(R.id.chknbinabilitytopassurineandstool3).getCheckBox(), aq.id(R.id.chknbinabilitytopassurineandstool4).getCheckBox());
                    break;

                //nb umbilical stump
                case R.id.chknbumbilicalstumpredorpus:
                    opencloseAccordion(aq.id(R.id.llnbumbilicalstumpredorpusdetails).getView(), aq.id(R.id.chknbumbilicalstumpredorpus).getCheckBox(), aq.id(R.id.chknbumbilicalstumpredorpus1).getCheckBox(), aq.id(R.id.chknbumbilicalstumpredorpus2).getCheckBox(), aq.id(R.id.chknbumbilicalstumpredorpus3).getCheckBox(), aq.id(R.id.chknbumbilicalstumpredorpus4).getCheckBox());
                    break;
                case R.id.chknbumbilicalstumpredorpus1:
                case R.id.chknbumbilicalstumpredorpus2:
                case R.id.chknbumbilicalstumpredorpus3:
                case R.id.chknbumbilicalstumpredorpus4:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbumbilicalstumpredorpus).getCheckBox(), aq.id(R.id.chknbumbilicalstumpredorpus1).getCheckBox(), aq.id(R.id.chknbumbilicalstumpredorpus2).getCheckBox(), aq.id(R.id.chknbumbilicalstumpredorpus3).getCheckBox(), aq.id(R.id.chknbumbilicalstumpredorpus4).getCheckBox());
                    break;

                //nb pustules
                case R.id.chknbpustules:
                    opencloseAccordion(aq.id(R.id.llnbpustulesdetails).getView(), aq.id(R.id.chknbpustules).getCheckBox(), aq.id(R.id.chknbpustules1).getCheckBox(), aq.id(R.id.chknbpustules2).getCheckBox(), aq.id(R.id.chknbpustules3).getCheckBox(), aq.id(R.id.chknbpustules4).getCheckBox());
                    break;
                case R.id.chknbpustules1:
                case R.id.chknbpustules2:
                case R.id.chknbpustules3:
                case R.id.chknbpustules4:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbpustules).getCheckBox(), aq.id(R.id.chknbpustules1).getCheckBox(), aq.id(R.id.chknbpustules2).getCheckBox(), aq.id(R.id.chknbpustules3).getCheckBox(), aq.id(R.id.chknbpustules4).getCheckBox());
                    break;

                //nb jaundice
                case R.id.chknbjaundice:
                    opencloseAccordion(aq.id(R.id.llnbjaundicedetails).getView(), aq.id(R.id.chknbjaundice).getCheckBox(), aq.id(R.id.chknbjaundice1).getCheckBox(), aq.id(R.id.chknbjaundice2).getCheckBox(), aq.id(R.id.chknbjaundice3).getCheckBox(), aq.id(R.id.chknbjaundice4).getCheckBox());
                    break;
                case R.id.chknbjaundice1:
                case R.id.chknbjaundice2:
                case R.id.chknbjaundice3:
                case R.id.chknbjaundice4:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbjaundice).getCheckBox(), aq.id(R.id.chknbjaundice1).getCheckBox(), aq.id(R.id.chknbjaundice2).getCheckBox(), aq.id(R.id.chknbjaundice3).getCheckBox(), aq.id(R.id.chknbjaundice4).getCheckBox());
                    break;

                //nb fever
                case R.id.chknbfever:
                    opencloseAccordion(aq.id(R.id.llnbfeverdetails).getView(), aq.id(R.id.chknbfever).getCheckBox(), aq.id(R.id.chknbfever1).getCheckBox(), aq.id(R.id.chknbfever2).getCheckBox(), aq.id(R.id.chknbfever3).getCheckBox(), aq.id(R.id.chknbfever4).getCheckBox());
                    break;
                case R.id.chknbfever1:
                case R.id.chknbfever2:
                case R.id.chknbfever3:
                case R.id.chknbfever4:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbfever).getCheckBox(), aq.id(R.id.chknbfever1).getCheckBox(), aq.id(R.id.chknbfever2).getCheckBox(), aq.id(R.id.chknbfever3).getCheckBox(), aq.id(R.id.chknbfever4).getCheckBox());
                    break;

//                    11May2021 Bindu add Sunken font, vomiting and sepsis
                //nb sunken font
                case R.id.chknbsunkenfont:
                    opencloseAccordion(aq.id(R.id.llnbsunkenfontdetails).getView(), aq.id(R.id.chknbsunkenfont).getCheckBox(), aq.id(R.id.chknbsunkenfont1).getCheckBox(), aq.id(R.id.chknbsunkenfont2).getCheckBox(), aq.id(R.id.chknbsunkenfont3).getCheckBox(), aq.id(R.id.chknbsunkenfont4).getCheckBox());
                    break;
                case R.id.chknbsunkenfont1:
                case R.id.chknbsunkenfont2:
                case R.id.chknbsunkenfont3:
                case R.id.chknbsunkenfont4:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbsunkenfont).getCheckBox(), aq.id(R.id.chknbsunkenfont1).getCheckBox(), aq.id(R.id.chknbsunkenfont2).getCheckBox(), aq.id(R.id.chknbsunkenfont3).getCheckBox(), aq.id(R.id.chknbsunkenfont4).getCheckBox());
                    break;
//                   nb vomiting
                case R.id.chknbvomiting:
                    opencloseAccordion(aq.id(R.id.llnbvomitingdetails).getView(), aq.id(R.id.chknbvomiting).getCheckBox(), aq.id(R.id.chknbvomiting1).getCheckBox(), aq.id(R.id.chknbvomiting2).getCheckBox(), aq.id(R.id.chknbvomiting3).getCheckBox(), aq.id(R.id.chknbvomiting4).getCheckBox());
                    break;
                case R.id.chknbvomiting1:
                case R.id.chknbvomiting2:
                case R.id.chknbvomiting3:
                case R.id.chknbvomiting4:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbvomiting).getCheckBox(), aq.id(R.id.chknbvomiting1).getCheckBox(), aq.id(R.id.chknbvomiting2).getCheckBox(), aq.id(R.id.chknbvomiting3).getCheckBox(), aq.id(R.id.chknbvomiting4).getCheckBox());
                    break;
//                    nb sepsis
                case R.id.chknbsepsis:
                    opencloseAccordion(aq.id(R.id.llnbsepsisdetails).getView(), aq.id(R.id.chknbsepsis).getCheckBox(), aq.id(R.id.chknbsepsis1).getCheckBox(), aq.id(R.id.chknbsepsis2).getCheckBox(), aq.id(R.id.chknbsepsis3).getCheckBox(), aq.id(R.id.chknbsepsis4).getCheckBox());
                    break;
                case R.id.chknbsepsis1:
                case R.id.chknbsepsis2:
                case R.id.chknbsepsis3:
                case R.id.chknbsepsis4:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbsepsis).getCheckBox(), aq.id(R.id.chknbsepsis1).getCheckBox(), aq.id(R.id.chknbsepsis2).getCheckBox(), aq.id(R.id.chknbsepsis3).getCheckBox(), aq.id(R.id.chknbsepsis4).getCheckBox());
                    break;


                //nb diarroea
                case R.id.chknbdiarrhoea:
                    opencloseAccordion(aq.id(R.id.llnbdiarrhoeadetails).getView(), aq.id(R.id.chknbdiarrhoea).getCheckBox(), aq.id(R.id.chknbdiarrhoea1).getCheckBox(), aq.id(R.id.chknbdiarrhoea2).getCheckBox(), aq.id(R.id.chknbdiarrhoea3).getCheckBox(), aq.id(R.id.chknbdiarrhoea4).getCheckBox());
                    break;
                case R.id.chknbdiarrhoea1:
                case R.id.chknbdiarrhoea2:
                case R.id.chknbdiarrhoea3:
                case R.id.chknbdiarrhoea4:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbdiarrhoea).getCheckBox(), aq.id(R.id.chknbdiarrhoea1).getCheckBox(), aq.id(R.id.chknbdiarrhoea2).getCheckBox(), aq.id(R.id.chknbdiarrhoea3).getCheckBox(), aq.id(R.id.chknbdiarrhoea4).getCheckBox());
                    break;
                //  14May2021 Bindu add Pneumonia
                //nb  Pneumonia
                case R.id.chknbpneumonia:
                    opencloseAccordion(aq.id(R.id.llnbpneumoniadetails).getView(), aq.id(R.id.chknbpneumonia).getCheckBox(), aq.id(R.id.chknbpneumonia1).getCheckBox(), aq.id(R.id.chknbpneumonia2).getCheckBox(), aq.id(R.id.chknbpneumonia3).getCheckBox(), aq.id(R.id.chknbpneumonia4).getCheckBox());
                    break;
                case R.id.chknbpneumonia1:
                case R.id.chknbpneumonia2:
                case R.id.chknbpneumonia3:
                case R.id.chknbpneumonia4:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbpneumonia).getCheckBox(), aq.id(R.id.chknbpneumonia1).getCheckBox(), aq.id(R.id.chknbpneumonia2).getCheckBox(), aq.id(R.id.chknbpneumonia3).getCheckBox(), aq.id(R.id.chknbpneumonia4).getCheckBox());
                    break;

                //nb blood in stools
                case R.id.chknbbloodinstools:
                    opencloseAccordion(aq.id(R.id.llnbbloodinstoolsdetails).getView(), aq.id(R.id.chknbbloodinstools).getCheckBox(), aq.id(R.id.chknbbloodinstools1).getCheckBox(), aq.id(R.id.chknbbloodinstools2).getCheckBox(), aq.id(R.id.chknbbloodinstools3).getCheckBox(), aq.id(R.id.chknbbloodinstools4).getCheckBox());
                    break;
                case R.id.chknbbloodinstools1:
                case R.id.chknbbloodinstools2:
                case R.id.chknbbloodinstools3:
                case R.id.chknbbloodinstools4:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbbloodinstools).getCheckBox(), aq.id(R.id.chknbbloodinstools1).getCheckBox(), aq.id(R.id.chknbbloodinstools2).getCheckBox(), aq.id(R.id.chknbbloodinstools3).getCheckBox(), aq.id(R.id.chknbbloodinstools4).getCheckBox());
                    break;
                //nb lethargic
                case R.id.chknblethargicbaby:
                    opencloseAccordion(aq.id(R.id.llnblethargicbabydetails).getView(), aq.id(R.id.chknblethargicbaby).getCheckBox(), aq.id(R.id.chknblethargicbaby1).getCheckBox(), aq.id(R.id.chknblethargicbaby2).getCheckBox(), aq.id(R.id.chknblethargicbaby3).getCheckBox(), aq.id(R.id.chknblethargicbaby4).getCheckBox());
                    break;
                case R.id.chknblethargicbaby1:
                case R.id.chknblethargicbaby2:
                case R.id.chknblethargicbaby3:
                case R.id.chknblethargicbaby4:
                    setSelectedSymptomTextColor(aq.id(R.id.chknblethargicbaby).getCheckBox(), aq.id(R.id.chknblethargicbaby1).getCheckBox(), aq.id(R.id.chknblethargicbaby2).getCheckBox(), aq.id(R.id.chknblethargicbaby3).getCheckBox(), aq.id(R.id.chknblethargicbaby4).getCheckBox());
                    break;
                //Fits
                case R.id.chknbfitsbaby:
                    opencloseAccordion(aq.id(R.id.llnbfitsbabydetails).getView(), aq.id(R.id.chknbfitsbaby).getCheckBox(), aq.id(R.id.chknbfitsbaby1).getCheckBox(), aq.id(R.id.chknbfitsbaby2).getCheckBox(), aq.id(R.id.chknbfitsbaby3).getCheckBox(), aq.id(R.id.chknbfitsbaby4).getCheckBox());
                    break;
                case R.id.chknbfitsbaby1:
                case R.id.chknbfitsbaby2:
                case R.id.chknbfitsbaby3:
                case R.id.chknbfitsbaby4:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbfitsbaby).getCheckBox(), aq.id(R.id.chknbfitsbaby1).getCheckBox(), aq.id(R.id.chknbfitsbaby2).getCheckBox(), aq.id(R.id.chknbfitsbaby3).getCheckBox(), aq.id(R.id.chknbfitsbaby4).getCheckBox());
                    break;
                //Eyes are red
                case R.id.chknbeyesareredbaby:
                    opencloseAccordion(aq.id(R.id.llnbeyesareredbabydetails).getView(), aq.id(R.id.chknbeyesareredbaby).getCheckBox(), aq.id(R.id.chknbeyesareredbaby1).getCheckBox(), aq.id(R.id.chknbeyesareredbaby2).getCheckBox(), aq.id(R.id.chknbeyesareredbaby3).getCheckBox(), aq.id(R.id.chknbeyesareredbaby4).getCheckBox());
                    break;
                case R.id.chknbeyesareredbaby1:
                case R.id.chknbeyesareredbaby2:
                case R.id.chknbeyesareredbaby3:
                case R.id.chknbeyesareredbaby4:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbeyesareredbaby).getCheckBox(), aq.id(R.id.chknbeyesareredbaby1).getCheckBox(), aq.id(R.id.chknbeyesareredbaby2).getCheckBox(), aq.id(R.id.chknbeyesareredbaby3).getCheckBox(), aq.id(R.id.chknbeyesareredbaby4).getCheckBox());
                    break;
                //Pallor of palms
                case R.id.chknbpallorofpalmsbaby:
                    opencloseAccordion(aq.id(R.id.llnbpallorofpalmsbabydetails).getView(), aq.id(R.id.chknbpallorofpalmsbaby).getCheckBox(), aq.id(R.id.chknbpallorofpalmsbaby1).getCheckBox(), aq.id(R.id.chknbpallorofpalmsbaby2).getCheckBox(), aq.id(R.id.chknbpallorofpalmsbaby3).getCheckBox(), aq.id(R.id.chknbpallorofpalmsbaby4).getCheckBox());
                    break;
                case R.id.chknbpallorofpalmsbaby1:
                case R.id.chknbpallorofpalmsbaby2:
                case R.id.chknbpallorofpalmsbaby3:
                case R.id.chknbpallorofpalmsbaby4:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbpallorofpalmsbaby).getCheckBox(), aq.id(R.id.chknbpallorofpalmsbaby1).getCheckBox(), aq.id(R.id.chknbpallorofpalmsbaby2).getCheckBox(), aq.id(R.id.chknbpallorofpalmsbaby3).getCheckBox(), aq.id(R.id.chknbpallorofpalmsbaby4).getCheckBox());
                    break;
                //yellow  palms
                case R.id.chknbyellowpalmsbaby:
                    opencloseAccordion(aq.id(R.id.llnbyellowpalmsbabydetails).getView(), aq.id(R.id.chknbyellowpalmsbaby).getCheckBox(), aq.id(R.id.chknbyellowpalmsbaby1).getCheckBox(), aq.id(R.id.chknbyellowpalmsbaby2).getCheckBox(), aq.id(R.id.chknbyellowpalmsbaby3).getCheckBox(), aq.id(R.id.chknbyellowpalmsbaby4).getCheckBox());
                    break;
                case R.id.chknbyellowpalmsbaby1:
                case R.id.chknbyellowpalmsbaby2:
                case R.id.chknbyellowpalmsbaby3:
                case R.id.chknbyellowpalmsbaby4:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbyellowpalmsbaby).getCheckBox(), aq.id(R.id.chknbyellowpalmsbaby1).getCheckBox(), aq.id(R.id.chknbyellowpalmsbaby2).getCheckBox(), aq.id(R.id.chknbyellowpalmsbaby3).getCheckBox(), aq.id(R.id.chknbyellowpalmsbaby4).getCheckBox());
                    break;
                //blue palms
                case R.id.chknbbluepalmsbaby:
                    opencloseAccordion(aq.id(R.id.llnbbluepalmsbabydetails).getView(), aq.id(R.id.chknbbluepalmsbaby).getCheckBox(), aq.id(R.id.chknbbluepalmsbaby1).getCheckBox(), aq.id(R.id.chknbbluepalmsbaby2).getCheckBox(), aq.id(R.id.chknbbluepalmsbaby3).getCheckBox(), aq.id(R.id.chknbbluepalmsbaby4).getCheckBox());
                    break;
                case R.id.chknbbluepalmsbaby1:
                case R.id.chknbbluepalmsbaby2:
                case R.id.chknbbluepalmsbaby3:
                case R.id.chknbbluepalmsbaby4:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbbluepalmsbaby).getCheckBox(), aq.id(R.id.chknbbluepalmsbaby1).getCheckBox(), aq.id(R.id.chknbbluepalmsbaby2).getCheckBox(), aq.id(R.id.chknbbluepalmsbaby3).getCheckBox(), aq.id(R.id.chknbbluepalmsbaby4).getCheckBox());
                    break;

                //Manikanta 10feb2021 added new method uncheckOtherFirstChild1
                //Feels cold to touch
                case R.id.chknbfeelscoldorhotbaby:
                    opencloseAccordion(aq.id(R.id.llnbfeelscoldorhotbabydetails).getView(), aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby1).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby2).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby3).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby4).getCheckBox());
                    uncheckOtherFirstChild1(aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby1).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox());
                    break;
                case R.id.chknbfeelscoldorhotbaby1:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby1).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby2).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby3).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby4).getCheckBox());
                    uncheckOtherDetails1(1, aq.id(R.id.chknbfeelswarmbaby1).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby2).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby3).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby4).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby2).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby3).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby4).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby1).getCheckBox());
                    break;
                case R.id.chknbfeelscoldorhotbaby2:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby1).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby2).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby3).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby4).getCheckBox());
                    uncheckOtherDetails1(2, aq.id(R.id.chknbfeelswarmbaby1).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby2).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby3).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby4).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby2).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby3).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby4).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby1).getCheckBox());
                    break;
                case R.id.chknbfeelscoldorhotbaby3:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby1).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby2).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby3).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby4).getCheckBox());
                    uncheckOtherDetails1(3, aq.id(R.id.chknbfeelswarmbaby1).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby2).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby3).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby4).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby2).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby3).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby4).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby1).getCheckBox());
                    break;
                case R.id.chknbfeelscoldorhotbaby4:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby1).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby2).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby3).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby4).getCheckBox());
                    uncheckOtherDetails1(4, aq.id(R.id.chknbfeelswarmbaby1).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby2).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby3).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby4).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby2).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby3).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby4).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby1).getCheckBox());
                    break;

                //Manikanta 10feb2021 added new field chknbfeelswarmbaby
                //Feels warm to touch
                case R.id.chknbfeelswarmbaby:
                    opencloseAccordion(aq.id(R.id.llnbfeelswarmbabydetails).getView(), aq.id(R.id.chknbfeelswarmbaby).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby1).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby2).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby3).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby4).getCheckBox());
                    uncheckOtherFirstChild2(aq.id(R.id.chknbfeelswarmbaby).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby1).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox());
                    break;
                case R.id.chknbfeelswarmbaby1:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbfeelswarmbaby).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby1).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby2).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby3).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby4).getCheckBox());
                    uncheckOtherDetails2(1, aq.id(R.id.chknbfeelscoldorhotbaby1).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby2).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby3).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby4).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby2).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby3).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby4).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby1).getCheckBox());
                    break;
                case R.id.chknbfeelswarmbaby2:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbfeelswarmbaby).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby1).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby2).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby3).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby4).getCheckBox());
                    uncheckOtherDetails2(2, aq.id(R.id.chknbfeelscoldorhotbaby1).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby2).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby3).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby4).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby2).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby3).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby4).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby1).getCheckBox());
                    break;
                case R.id.chknbfeelswarmbaby3:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbfeelswarmbaby).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby1).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby2).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby3).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby4).getCheckBox());
                    uncheckOtherDetails2(3, aq.id(R.id.chknbfeelscoldorhotbaby1).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby2).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby3).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby4).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby2).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby3).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby4).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby1).getCheckBox());
                    break;
                case R.id.chknbfeelswarmbaby4:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbfeelswarmbaby).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby1).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby2).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby3).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby4).getCheckBox());
                    uncheckOtherDetails2(4, aq.id(R.id.chknbfeelscoldorhotbaby1).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby2).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby3).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby4).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby2).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby3).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby4).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby1).getCheckBox());
                    break;

                //Manikanta 10feb2021 added to category
                //Feels too warm to touch
                case R.id.chknbfeelstoowarmbaby:
                    opencloseAccordion(aq.id(R.id.llnbfeelstoowarmbabydetails).getView(), aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby1).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby2).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby3).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby4).getCheckBox());
                    uncheckOtherFirstChild3(aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox());
                    break;
                case R.id.chknbfeelstoowarmbaby1:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby1).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby2).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby3).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby4).getCheckBox());
                    uncheckOtherDetails3(1, aq.id(R.id.chknbfeelscoldorhotbaby1).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby2).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby3).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby4).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby2).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby3).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby4).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby1).getCheckBox());
                    break;
                case R.id.chknbfeelstoowarmbaby2:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby1).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby2).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby3).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby4).getCheckBox());
                    uncheckOtherDetails3(2, aq.id(R.id.chknbfeelscoldorhotbaby1).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby2).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby3).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby4).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby2).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby3).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby4).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby1).getCheckBox());
                    break;
                case R.id.chknbfeelstoowarmbaby3:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby1).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby2).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby3).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby4).getCheckBox());
                    uncheckOtherDetails3(3, aq.id(R.id.chknbfeelscoldorhotbaby1).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby2).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby3).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby4).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby2).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby3).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby4).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby1).getCheckBox());
                    break;
                case R.id.chknbfeelstoowarmbaby4:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby1).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby2).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby3).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby4).getCheckBox());
                    uncheckOtherDetails3(4, aq.id(R.id.chknbfeelscoldorhotbaby1).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby2).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby3).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby4).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby2).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby3).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby4).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby1).getCheckBox());
                    break;
                //Bleeding from any site
                case R.id.chknbbleedingbaby:
                    opencloseAccordion(aq.id(R.id.llnbbleedingbabydetails).getView(), aq.id(R.id.chknbbleedingbaby).getCheckBox(), aq.id(R.id.chknbbleedingbaby1).getCheckBox(), aq.id(R.id.chknbbleedingbaby2).getCheckBox(), aq.id(R.id.chknbbleedingbaby3).getCheckBox(), aq.id(R.id.chknbbleedingbaby4).getCheckBox());
                    break;
                case R.id.chknbbleedingbaby1:
                case R.id.chknbbleedingbaby2:
                case R.id.chknbbleedingbaby3:
                case R.id.chknbbleedingbaby4:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbbleedingbaby).getCheckBox(), aq.id(R.id.chknbbleedingbaby1).getCheckBox(), aq.id(R.id.chknbbleedingbaby2).getCheckBox(), aq.id(R.id.chknbbleedingbaby3).getCheckBox(), aq.id(R.id.chknbbleedingbaby4).getCheckBox());
                    break;
                //Abdominal distension
                case R.id.chknbabdominaldistensionbaby:
                    opencloseAccordion(aq.id(R.id.llnbabdominaldistensionbabydetails).getView(), aq.id(R.id.chknbabdominaldistensionbaby).getCheckBox(), aq.id(R.id.chknbabdominaldistensionbaby1).getCheckBox(), aq.id(R.id.chknbabdominaldistensionbaby2).getCheckBox(), aq.id(R.id.chknbabdominaldistensionbaby3).getCheckBox(), aq.id(R.id.chknbabdominaldistensionbaby4).getCheckBox());
                    break;
                case R.id.chknbabdominaldistensionbaby1:
                case R.id.chknbabdominaldistensionbaby2:
                case R.id.chknbabdominaldistensionbaby3:
                case R.id.chknbabdominaldistensionbaby4:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbabdominaldistensionbaby).getCheckBox(), aq.id(R.id.chknbabdominaldistensionbaby1).getCheckBox(), aq.id(R.id.chknbabdominaldistensionbaby2).getCheckBox(), aq.id(R.id.chknbabdominaldistensionbaby3).getCheckBox(), aq.id(R.id.chknbabdominaldistensionbaby4).getCheckBox());
                    break;


                //Mani 12Sep2021 -Hypothermia
                case R.id.chknbhypothermiababy:
                    opencloseAccordion(aq.id(R.id.llnbhypothermiababydetails).getView(), aq.id(R.id.chknbhypothermiababy).getCheckBox(), aq.id(R.id.chknbhypothermiababy1).getCheckBox(), aq.id(R.id.chknbhypothermiababy2).getCheckBox(), aq.id(R.id.chknbhypothermiababy3).getCheckBox(), aq.id(R.id.chknbhypothermiababy4).getCheckBox());
                    break;
                case R.id.chknbhypothermiababy1:
                case R.id.chknbhypothermiababy2:
                case R.id.chknbhypothermiababy3:
                case R.id.chknbhypothermiababy4:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbhypothermiababy).getCheckBox(), aq.id(R.id.chknbhypothermiababy1).getCheckBox(), aq.id(R.id.chknbhypothermiababy2).getCheckBox(), aq.id(R.id.chknbhypothermiababy3).getCheckBox(), aq.id(R.id.chknbhypothermiababy4).getCheckBox());
                    break;


                //Mani 12Sep2021 - Cyanosis
                case R.id.chknbcyanosisbaby:
                    opencloseAccordion(aq.id(R.id.llnbcyanosisdetails).getView(), aq.id(R.id.chknbcyanosisbaby).getCheckBox(), aq.id(R.id.chknbcyanosisbaby1).getCheckBox(), aq.id(R.id.chknbcyanosisbaby2).getCheckBox(), aq.id(R.id.chknbcyanosisbaby3).getCheckBox(), aq.id(R.id.chknbcyanosisbaby4).getCheckBox());
                    break;
                case R.id.chknbcyanosisbaby1:
                case R.id.chknbcyanosisbaby2:
                case R.id.chknbcyanosisbaby3:
                case R.id.chknbcyanosisbaby4:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbcyanosisbaby).getCheckBox(), aq.id(R.id.chknbcyanosisbaby1).getCheckBox(), aq.id(R.id.chknbcyanosisbaby2).getCheckBox(), aq.id(R.id.chknbcyanosisbaby3).getCheckBox(), aq.id(R.id.chknbcyanosisbaby4).getCheckBox());
                    break;


                //Mani 12Sep2021 - Raspiratory distress
                case R.id.chknbraspiratorydistressbaby:
                    opencloseAccordion(aq.id(R.id.llnbraspiratorydistressdetails).getView(), aq.id(R.id.chknbraspiratorydistressbaby).getCheckBox(), aq.id(R.id.chknbraspiratorydistressbaby1).getCheckBox(), aq.id(R.id.chknbraspiratorydistressbaby2).getCheckBox(), aq.id(R.id.chknbraspiratorydistressbaby3).getCheckBox(), aq.id(R.id.chknbraspiratorydistressbaby4).getCheckBox());
                    break;
                case R.id.chknbraspiratorydistressbaby1:
                case R.id.chknbraspiratorydistressbaby2:
                case R.id.chknbraspiratorydistressbaby3:
                case R.id.chknbraspiratorydistressbaby4:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbraspiratorydistressbaby).getCheckBox(), aq.id(R.id.chknbraspiratorydistressbaby1).getCheckBox(), aq.id(R.id.chknbraspiratorydistressbaby2).getCheckBox(), aq.id(R.id.chknbraspiratorydistressbaby3).getCheckBox(), aq.id(R.id.chknbraspiratorydistressbaby4).getCheckBox());
                    break;


                //Mani 12Sep2021 - Excessive weight loss
                case R.id.chknbexcessivewtlossbaby:
                    opencloseAccordion(aq.id(R.id.llnbexcessivewtlossdetails).getView(), aq.id(R.id.chknbexcessivewtlossbaby).getCheckBox(), aq.id(R.id.chknbexcessivewtlossbaby1).getCheckBox(), aq.id(R.id.chknbexcessivewtlossbaby2).getCheckBox(), aq.id(R.id.chknbexcessivewtlossbaby3).getCheckBox(), aq.id(R.id.chknbexcessivewtlossbaby4).getCheckBox());
                    break;
                case R.id.chknbexcessivewtlossbaby1:
                case R.id.chknbexcessivewtlossbaby2:
                case R.id.chknbexcessivewtlossbaby3:
                case R.id.chknbexcessivewtlossbaby4:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbexcessivewtlossbaby).getCheckBox(), aq.id(R.id.chknbexcessivewtlossbaby1).getCheckBox(), aq.id(R.id.chknbexcessivewtlossbaby2).getCheckBox(), aq.id(R.id.chknbexcessivewtlossbaby3).getCheckBox(), aq.id(R.id.chknbexcessivewtlossbaby4).getCheckBox());
                    break;


                //Mani 12Sep2021 - Convulsion
                case R.id.chknbconvulsionbaby:
                    opencloseAccordion(aq.id(R.id.llnbconvulsiondetails).getView(), aq.id(R.id.chknbconvulsionbaby).getCheckBox(), aq.id(R.id.chknbconvulsionbaby1).getCheckBox(), aq.id(R.id.chknbconvulsionbaby2).getCheckBox(), aq.id(R.id.chknbconvulsionbaby3).getCheckBox(), aq.id(R.id.chknbconvulsionbaby4).getCheckBox());
                    break;
                case R.id.chknbconvulsionbaby1:
                case R.id.chknbconvulsionbaby2:
                case R.id.chknbconvulsionbaby3:
                case R.id.chknbconvulsionbaby4:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbconvulsionbaby).getCheckBox(), aq.id(R.id.chknbconvulsionbaby1).getCheckBox(), aq.id(R.id.chknbconvulsionbaby2).getCheckBox(), aq.id(R.id.chknbconvulsionbaby3).getCheckBox(), aq.id(R.id.chknbconvulsionbaby4).getCheckBox());
                    break;

                //Mani 12Sep2021 - preterm
                case R.id.chknbpretermbaby:
                    opencloseAccordion(aq.id(R.id.llnbpretermdetails).getView(), aq.id(R.id.chknbpretermbaby).getCheckBox(), aq.id(R.id.chknbpretermbaby1).getCheckBox(), aq.id(R.id.chknbpretermbaby2).getCheckBox(), aq.id(R.id.chknbpretermbaby3).getCheckBox(), aq.id(R.id.chknbpretermbaby4).getCheckBox());
                    break;
                case R.id.chknbpretermbaby1:
                case R.id.chknbpretermbaby2:
                case R.id.chknbpretermbaby3:
                case R.id.chknbpretermbaby4:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbpretermbaby).getCheckBox(), aq.id(R.id.chknbpretermbaby1).getCheckBox(), aq.id(R.id.chknbpretermbaby2).getCheckBox(), aq.id(R.id.chknbpretermbaby3).getCheckBox(), aq.id(R.id.chknbpretermbaby4).getCheckBox());
                    break;

                //Any birth defects
                case R.id.chknbanybirthdefectbaby:
                    opencloseAccordion(aq.id(R.id.llnbanybirthdefectbabydetails).getView(), aq.id(R.id.chknbanybirthdefectbaby).getCheckBox(), aq.id(R.id.chknbanybirthdefectbaby1).getCheckBox(), aq.id(R.id.chknbanybirthdefectbaby2).getCheckBox(), aq.id(R.id.chknbanybirthdefectbaby3).getCheckBox(), aq.id(R.id.chknbanybirthdefectbaby4).getCheckBox());
                    break;
                case R.id.chknbanybirthdefectbaby1:
                case R.id.chknbanybirthdefectbaby2:
                case R.id.chknbanybirthdefectbaby3:
                case R.id.chknbanybirthdefectbaby4:
                    setSelectedSymptomTextColor(aq.id(R.id.chknbanybirthdefectbaby).getCheckBox(), aq.id(R.id.chknbanybirthdefectbaby1).getCheckBox(), aq.id(R.id.chknbanybirthdefectbaby2).getCheckBox(), aq.id(R.id.chknbanybirthdefectbaby3).getCheckBox(), aq.id(R.id.chknbanybirthdefectbaby4).getCheckBox());
                    break;

                case R.id.imgcleardatebp: {
                    clearDate(aq.id(R.id.etbpdonedate).getEditText(), aq.id(R.id.imgcleardatebp).getImageView());
                    break;
                }
                case R.id.imgcleardatepulse: {
                    clearDate(aq.id(R.id.etpulsedate).getEditText(), aq.id(R.id.imgcleardatepulse).getImageView());
                    break;
                }
                case R.id.imgcleardatehb: {
                    clearDate(aq.id(R.id.ethbdonedate).getEditText(), aq.id(R.id.imgcleardatehb).getImageView());
                    break;
                }
                case R.id.imgcleardatetemp: {
                    clearDate(aq.id(R.id.ettempdonedate).getEditText(), aq.id(R.id.imgcleardatetemp).getImageView());
                    break;
                }
                case R.id.imgcleardateweightbaby1: {
                    clearDate(aq.id(R.id.etweightdatebaby1).getEditText(), aq.id(R.id.imgcleardateweightbaby1).getImageView());
                    break;
                }
                case R.id.imgcleardatetempbaby1: {
                    clearDate(aq.id(R.id.ettempdonedatebaby1).getEditText(), aq.id(R.id.imgcleardatetempbaby1).getImageView());
                    break;
                }
                case R.id.imgcleardaterespratebaby1: {
                    clearDate(aq.id(R.id.etrespratedonedatebaby1).getEditText(), aq.id(R.id.imgcleardaterespratebaby1).getImageView());
                    break;
                }
                case R.id.imgclearchildregdate1: {
                    clearDate(aq.id(R.id.etchildregdate1).getEditText(), aq.id(R.id.imgclearchildregdate1).getImageView());
                    break;
                }
                //Baby 2
                case R.id.imgcleardateweightbaby2: {
                    clearDate(aq.id(R.id.etweightdatebaby2).getEditText(), aq.id(R.id.imgcleardateweightbaby2).getImageView());
                    break;
                }
                case R.id.imgcleardatetempbaby2: {
                    clearDate(aq.id(R.id.ettempdonedatebaby2).getEditText(), aq.id(R.id.imgcleardatetempbaby2).getImageView());
                    break;
                }
                case R.id.imgcleardaterespratebaby2: {
                    clearDate(aq.id(R.id.etrespratedonedatebaby2).getEditText(), aq.id(R.id.imgcleardaterespratebaby2).getImageView());
                    break;
                }
                case R.id.imgclearchildregdate2: {
                    clearDate(aq.id(R.id.etchildregdate2).getEditText(), aq.id(R.id.imgclearchildregdate2).getImageView());
                    break;
                }
                //Baby 3
                case R.id.imgcleardateweightbaby3: {
                    clearDate(aq.id(R.id.etweightdatebaby3).getEditText(), aq.id(R.id.imgcleardateweightbaby3).getImageView());
                    break;
                }
                case R.id.imgcleardatetempbaby3: {
                    clearDate(aq.id(R.id.ettempdonedatebaby3).getEditText(), aq.id(R.id.imgcleardatetempbaby3).getImageView());
                    break;
                }
                case R.id.imgcleardaterespratebaby3: {
                    clearDate(aq.id(R.id.etrespratedonedatebaby3).getEditText(), aq.id(R.id.imgcleardaterespratebaby3).getImageView());
                    break;
                }
                case R.id.imgclearchildregdate3: {
                    clearDate(aq.id(R.id.etchildregdate3).getEditText(), aq.id(R.id.imgclearchildregdate3).getImageView());
                    break;
                }
                //Baby 4
                case R.id.imgcleardateweightbaby4: {
                    clearDate(aq.id(R.id.etweightdatebaby4).getEditText(), aq.id(R.id.imgcleardateweightbaby4).getImageView());
                    break;
                }
                case R.id.imgcleardatetempbaby4: {
                    clearDate(aq.id(R.id.ettempdonedatebaby4).getEditText(), aq.id(R.id.imgcleardatetempbaby4).getImageView());
                    break;
                }
                case R.id.imgcleardaterespratebaby4: {
                    clearDate(aq.id(R.id.etrespratedonedatebaby4).getEditText(), aq.id(R.id.imgcleardaterespratebaby4).getImageView());
                    break;
                }
                case R.id.imgclearchildregdate4: {
                    clearDate(aq.id(R.id.etchildregdate4).getEditText(), aq.id(R.id.imgclearchildregdate4).getImageView());
                    break;
                }
                case R.id.chkischildregatfacchild1: {
                    unCheckChildRegDetails(aq.id(R.id.chkischildregatfacchild1).getCheckBox(), aq.id(R.id.trchildregid1).getView(), aq.id(R.id.trchildregdate1).getView(), aq.id(R.id.etchildregid1).getEditText(), aq.id(R.id.etchildregdate1).getEditText(), aq.id(R.id.imgclearchildregdate1).getImageView());
                    break;
                }
                case R.id.chkischildregatfacchild2: {
                    unCheckChildRegDetails(aq.id(R.id.chkischildregatfacchild2).getCheckBox(), aq.id(R.id.trchildregid2).getView(), aq.id(R.id.trchildregdate2).getView(), aq.id(R.id.etchildregid2).getEditText(), aq.id(R.id.etchildregdate2).getEditText(), aq.id(R.id.imgclearchildregdate2).getImageView());
                    break;
                }
                case R.id.chkischildregatfacchild3: {
                    unCheckChildRegDetails(aq.id(R.id.chkischildregatfacchild3).getCheckBox(), aq.id(R.id.trchildregid3).getView(), aq.id(R.id.trchildregdate3).getView(), aq.id(R.id.etchildregid3).getEditText(), aq.id(R.id.etchildregdate3).getEditText(), aq.id(R.id.imgclearchildregdate3).getImageView());
                    break;
                }
                case R.id.chkischildregatfacchild4: {
                    unCheckChildRegDetails(aq.id(R.id.chkischildregatfacchild4).getCheckBox(), aq.id(R.id.trchildregid4).getView(), aq.id(R.id.trchildregdate4).getView(), aq.id(R.id.etchildregid4).getEditText(), aq.id(R.id.etchildregdate4).getEditText(), aq.id(R.id.imgclearchildregdate4).getImageView());
                    break;
                }
                //               16May2021 Bindu  add recent health and covid vaccine
                case R.id.llhealthdetid:
                    opencloseAccordion(aq.id(R.id.llhealthdetails).getView(), aq.id(R.id.txthealthdet).getTextView());
                    aq.id(R.id.llhealthdetails).backgroundColor(getResources().getColor(R.color.lightgray));
                    break;
                //               16May2021 Bindu
                case R.id.llcovidvaccineid:
                    opencloseAccordion(aq.id(R.id.llcovidvaccinedetails).getView(), aq.id(R.id.txtcovidvaccine).getTextView());
                    aq.id(R.id.llcovidvaccinedetails).backgroundColor(getResources().getColor(R.color.lightgray));
                    break;
//                    16May2021 Bindu add covid health and covid vaccine
                case R.id.rd_healthissuesYes:
                    hideshowdetailsanyhealthissuesymtpoms(aq.id(R.id.txthealthdet).getTextView());
                    break;
                case R.id.rd_healthissuesNo:
                    hideshowdetailsanyhealthissuesymtpoms(aq.id(R.id.txthealthdet).getTextView());
                    break;
                case R.id.rd_CovidTestYes:
                    hideshowcovidTestdetails();
                    break;
                case R.id.rd_CovidTestNo:
                    hideshowcovidTestdetails();
                    break;
                case R.id.rd_CovidVaccineYes:
                    hideshowcovidvaccinedetails(aq.id(R.id.txtcovidvaccine).getTextView());
                    break;
                case R.id.rd_CovidVaccineNo:
                    hideshowcovidvaccinedetails(aq.id(R.id.txtcovidvaccine).getTextView());
                    break;
                //29-6-2021 Ramesh
                case R.id.lladdphotosid:
                    opencloseAccordion(aq.id(R.id.lladdphotosdetails).getView(), aq.id(R.id.txtaddphotos).getTextView());
                    aq.id(R.id.lladdphotosdetails).backgroundColor(getResources().getColor(R.color.lightgray));
                    if (aq.id(R.id.lladdphotosdetails).getView().getVisibility() == View.VISIBLE) {
                        AsyncImage asyncImage = new AsyncImage();
                        asyncImage.execute();
                    }
                    break;
                case R.id.imgSummaryAddPhoto:
                    if (AllImagesBeforeSave!=null && AllImagesBeforeSave.size()<2){
                        callCamera();
                    }else {
                        AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.maxlimitreached),PncHomeVisit.this);
                    }
                    break;


                    //mani 14Sep2021
                case R.id.rd_IFAYes1:
                    childtabletconsumingyes();
                    break;

                case R.id.rd_IFANo1:
                    childtabletconsumingyes();
                break;

                case R.id.rd_IFAYes2:
                    childtabletconsumingyes2();
                    break;

                case R.id.rd_IFANo2:
                    childtabletconsumingyes2();
                    break;

                case R.id.rd_IFAYes3:
                    childtabletconsumingyes3();
                    break;

                case R.id.rd_IFANo3:
                    childtabletconsumingyes3();
                    break;
                case R.id.rd_IFAYes4:
                    childtabletconsumingyes4();
                    break;

                case R.id.rd_IFANo4:
                    childtabletconsumingyes4();
                    break;

                case R.id.rd_ConsumingIFAYes1:
                    childifatabletcount();
                    break;
                case R.id.rd_ConsumingIFANo1:
                    childifatabletcount();
                    break;
                case R.id.rd_ConsumingIFAYes2:
                    childifatabletcount2();
                    break;
                case R.id.rd_ConsumingIFANo2:
                    childifatabletcount2();
                    break;
                case R.id.rd_ConsumingIFAYes3:
                    childifatabletcount3();
                    break;
                case R.id.rd_ConsumingIFANo3:
                    childifatabletcount3();
                    break;
                case R.id.rd_ConsumingIFAYes4:
                    childifatabletcount4();
                    break;
                case R.id.rd_ConsumingIFANo4:
                    childifatabletcount4();
                    break;

            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private void childifatabletcount() {
        if (aq.id(R.id.rd_ConsumingIFAYes1).isChecked()) {
            aq.id(R.id.trChildVisitIFANooftabletsperDay1).visible();
            aq.id(R.id.trIFAReasons1).gone();
            spnIFAReasons1.setItems(getResources().getStringArray(R.array.ifareasons));
        } else if (aq.id(R.id.rd_ConsumingIFANo1).isChecked()) {
            aq.id(R.id.trChildVisitIFANooftabletsperDay1).gone();
            aq.id(R.id.etIFANoofTabletsConsumes1).text("");
            aq.id(R.id.trIFAReasons1).getView().setVisibility(View.VISIBLE);
        }
    }

    private void childifatabletcount2() {
        if (aq.id(R.id.rd_ConsumingIFAYes2).isChecked()) {
            aq.id(R.id.trChildVisitIFANooftabletsperDay2).visible();
            aq.id(R.id.trIFAReasons2).gone();
            spnIFAReasons2.setItems(getResources().getStringArray(R.array.ifareasons));
        } else if (aq.id(R.id.rd_ConsumingIFANo2).isChecked()) {
            aq.id(R.id.trChildVisitIFANooftabletsperDay2).gone();
            aq.id(R.id.etIFANoofTabletsConsumes2).text("");
            aq.id(R.id.trIFAReasons2).getView().setVisibility(View.VISIBLE);
        }
    }

    private void childifatabletcount3() {
        if (aq.id(R.id.rd_ConsumingIFAYes3).isChecked()) {
            aq.id(R.id.trChildVisitIFANooftabletsperDay3).visible();
            aq.id(R.id.trIFAReasons3).gone();
            spnIFAReasons3.setItems(getResources().getStringArray(R.array.ifareasons));
        } else if (aq.id(R.id.rd_ConsumingIFANo3).isChecked()) {
            aq.id(R.id.trChildVisitIFANooftabletsperDay3).gone();
            aq.id(R.id.etIFANoofTabletsConsumes3).text("");
            aq.id(R.id.trIFAReasons3).getView().setVisibility(View.VISIBLE);
        }
    }
    private void childifatabletcount4() {
        if (aq.id(R.id.rd_ConsumingIFAYes4).isChecked()) {
            aq.id(R.id.trChildVisitIFANooftabletsperDay4).visible();
            aq.id(R.id.trIFAReasons4).gone();
            spnIFAReasons4.setItems(getResources().getStringArray(R.array.ifareasons));
        } else if (aq.id(R.id.rd_ConsumingIFANo4).isChecked()) {
            aq.id(R.id.trChildVisitIFANooftabletsperDay4).gone();
            aq.id(R.id.etIFANoofTabletsConsumes4).text("");
            aq.id(R.id.trIFAReasons4).getView().setVisibility(View.VISIBLE);
        }
    }
    private void childtabletconsumingyes() {
        if (aq.id(R.id.rd_IFAYes1).isChecked()){
            aq.id(R.id.ll_AV_IFA1).getView().setVisibility(View.VISIBLE);
      spnIFAReasons1.setItems(getResources().getStringArray(R.array.ifareasons));
        }else if (aq.id(R.id.rd_IFANo1).isChecked()) {
            rgConsumeIFA.clearCheck();
            aq.id(R.id.rd_ConsumingIFANo1).checked(false);
            aq.id(R.id.rd_ConsumingIFAYes1).checked(false);
            aq.id(R.id.etIFANoofTabletsConsumes1).text("");
            spnIFAReasons1.setItems(getResources().getStringArray(R.array.ifareasons));
            aq.id(R.id.spnChlWhoGaveIFATablets1).getSpinner().setSelection(0);
            aq.id(R.id.ll_AV_IFA1).getView().setVisibility(View.GONE);
            aq.id(R.id.trChildVisitIFANooftabletsperDay1).getView().setVisibility(View.GONE);
            aq.id(R.id.trIFAReasons1).getView().setVisibility(View.GONE);
        }
    }

    private void childtabletconsumingyes2() {
        if (aq.id(R.id.rd_IFAYes2).isChecked()){
            aq.id(R.id.ll_AV_IFA2).getView().setVisibility(View.VISIBLE);
      spnIFAReasons2.setItems(getResources().getStringArray(R.array.ifareasons));
        }else if (aq.id(R.id.rd_IFANo2).isChecked()) {
            rgConsumeIFA2.clearCheck();
            aq.id(R.id.rd_ConsumingIFANo2).checked(false);
            aq.id(R.id.rd_ConsumingIFAYes2).checked(false);
            aq.id(R.id.etIFANoofTabletsConsumes2).text("");
            spnIFAReasons2.setItems(getResources().getStringArray(R.array.ifareasons));
            aq.id(R.id.spnChlWhoGaveIFATablets2).getSpinner().setSelection(0);
            aq.id(R.id.ll_AV_IFA2).getView().setVisibility(View.GONE);
            aq.id(R.id.trChildVisitIFANooftabletsperDay2).getView().setVisibility(View.GONE);
            aq.id(R.id.trIFAReasons2).getView().setVisibility(View.GONE);
        }
    }

    private void childtabletconsumingyes3() {
        if (aq.id(R.id.rd_IFAYes3).isChecked()){
            aq.id(R.id.ll_AV_IFA3).getView().setVisibility(View.VISIBLE);
      spnIFAReasons3.setItems(getResources().getStringArray(R.array.ifareasons));
        }else if (aq.id(R.id.rd_IFANo3).isChecked()) {
            rgConsumeIFA3.clearCheck();
            aq.id(R.id.rd_ConsumingIFANo3).checked(false);
            aq.id(R.id.rd_ConsumingIFAYes3).checked(false);
            aq.id(R.id.etIFANoofTabletsConsumes3).text("");
            spnIFAReasons3.setItems(getResources().getStringArray(R.array.ifareasons));
            aq.id(R.id.spnChlWhoGaveIFATablets3).getSpinner().setSelection(0);
            aq.id(R.id.ll_AV_IFA3).getView().setVisibility(View.GONE);
            aq.id(R.id.trChildVisitIFANooftabletsperDay3).getView().setVisibility(View.GONE);
            aq.id(R.id.trIFAReasons3).getView().setVisibility(View.GONE);
        }
    }

    private void childtabletconsumingyes4() {
        if (aq.id(R.id.rd_IFAYes4).isChecked()){
            aq.id(R.id.ll_AV_IFA4).getView().setVisibility(View.VISIBLE);
      spnIFAReasons4.setItems(getResources().getStringArray(R.array.ifareasons));
        }else if (aq.id(R.id.rd_IFANo4).isChecked()) {
            rgConsumeIFA4.clearCheck();
            aq.id(R.id.rd_ConsumingIFANo4).checked(false);
            aq.id(R.id.rd_ConsumingIFAYes4).checked(false);
            aq.id(R.id.etIFANoofTabletsConsumes4).text("");
            spnIFAReasons4.setItems(getResources().getStringArray(R.array.ifareasons));
            aq.id(R.id.spnChlWhoGaveIFATablets4).getSpinner().setSelection(0);
            aq.id(R.id.ll_AV_IFA4).getView().setVisibility(View.GONE);
            aq.id(R.id.trChildVisitIFANooftabletsperDay4).getView().setVisibility(View.GONE);
            aq.id(R.id.trIFAReasons4).getView().setVisibility(View.GONE);
        }
    }

    private void callCamera() {//28-6-2021 Ramesh
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            try {
                File photoFile = null;
                photoFile = createImagePath();
                // Continue only if the File was successfully created
                if (photoFile != null) {
                    Uri photoURI = FileProvider.getUriForFile(
                            getApplicationContext(),
                            getApplicationContext()
                                    .getPackageName() + ".provider", photoFile);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private File createImagePath() {
        try {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = "JPEG_" + timeStamp + "_";
            File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            File image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );
            mCurrentPhotoPath = image.getAbsolutePath();
            return image;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //Mani 15feb21 uncheck the other fields
    private void uncheckOtherDetails3(int i, CheckBox ch, CheckBox ch1, CheckBox ch2, CheckBox ch3, CheckBox ch4, CheckBox ch5, CheckBox ch6, CheckBox ch7) {
        switch (i) {
            case 1:
                aq.id(R.id.chknbfeelswarmbaby1).checked(false);
                aq.id(R.id.chknbfeelscoldorhotbaby1).checked(false);
                if ((!ch.isChecked() && !ch1.isChecked() && !ch2.isChecked() && !ch3.isChecked())
                        && (!ch4.isChecked() && !ch5.isChecked() && !ch6.isChecked() && !ch7.isChecked())) {
                    aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                    aq.id(R.id.chknbfeelswarmbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelswarmbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                } else if (!ch1.isChecked() && !ch2.isChecked() && !ch3.isChecked()) {
                    aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                } else {
                    aq.id(R.id.chknbfeelswarmbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelswarmbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                }
                break;
            case 2:
                aq.id(R.id.chknbfeelswarmbaby2).checked(false);
                aq.id(R.id.chknbfeelscoldorhotbaby2).checked(false);
                if ((!ch.isChecked() && !ch1.isChecked() && !ch2.isChecked() && !ch3.isChecked())
                        && (!ch4.isChecked() && !ch5.isChecked() && !ch6.isChecked() && !ch7.isChecked())) {
                    aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                    aq.id(R.id.chknbfeelswarmbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelswarmbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                } else if (!ch1.isChecked() && !ch2.isChecked() && !ch3.isChecked()) {
                    aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                } else {
                    aq.id(R.id.chknbfeelswarmbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelswarmbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                }
                break;
            case 3:
                aq.id(R.id.chknbfeelswarmbaby3).checked(false);
                aq.id(R.id.chknbfeelscoldorhotbaby3).checked(false);
                if ((!ch.isChecked() && !ch1.isChecked() && !ch2.isChecked() && !ch3.isChecked())
                        && (!ch4.isChecked() && !ch5.isChecked() && !ch6.isChecked() && !ch7.isChecked())) {
                    aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                    aq.id(R.id.chknbfeelswarmbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelswarmbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                } else if (!ch1.isChecked() && !ch2.isChecked() && !ch3.isChecked()) {
                    aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                } else {
                    aq.id(R.id.chknbfeelswarmbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelswarmbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                }
                break;
            case 4:
                aq.id(R.id.chknbfeelswarmbaby4).checked(false);
                aq.id(R.id.chknbfeelscoldorhotbaby4).checked(false);
                if ((!ch.isChecked() && !ch1.isChecked() && !ch2.isChecked() && !ch3.isChecked())
                        && (!ch4.isChecked() && !ch5.isChecked() && !ch6.isChecked() && !ch7.isChecked())) {
                    aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                    aq.id(R.id.chknbfeelswarmbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelswarmbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                } else if (!ch1.isChecked() && !ch2.isChecked() && !ch3.isChecked()) {
                    aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                } else {
                    aq.id(R.id.chknbfeelswarmbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelswarmbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                }
                break;
        }
    }


    //Mani 15feb21 uncheck the other fields
    private void uncheckOtherDetails2(int i, CheckBox ch, CheckBox ch1, CheckBox ch2, CheckBox ch3, CheckBox ch4, CheckBox ch5, CheckBox ch6, CheckBox ch7) {
        switch (i) {
            case 1:
                aq.id(R.id.chknbfeelstoowarmbaby1).checked(false);
                aq.id(R.id.chknbfeelscoldorhotbaby1).checked(false);
                if ((!ch.isChecked() && !ch1.isChecked() && !ch2.isChecked() && !ch3.isChecked())
                        && (!ch4.isChecked() && !ch5.isChecked() && !ch6.isChecked() && !ch7.isChecked())) {
                    aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                    aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                } else if (!ch1.isChecked() && !ch2.isChecked() && !ch3.isChecked()) {
                    aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                } else {
                    aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                }
                break;
            case 2:
                aq.id(R.id.chknbfeelstoowarmbaby2).checked(false);
                aq.id(R.id.chknbfeelscoldorhotbaby2).checked(false);
                if ((!ch.isChecked() && !ch1.isChecked() && !ch2.isChecked() && !ch3.isChecked())
                        && (!ch4.isChecked() && !ch5.isChecked() && !ch6.isChecked() && !ch7.isChecked())) {
                    aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                    aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                } else if (!ch1.isChecked() && !ch2.isChecked() && !ch3.isChecked()) {
                    aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                } else {
                    aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                }
                break;
            case 3:
                aq.id(R.id.chknbfeelstoowarmbaby3).checked(false);
                aq.id(R.id.chknbfeelscoldorhotbaby3).checked(false);
                if ((!ch.isChecked() && !ch1.isChecked() && !ch2.isChecked() && !ch3.isChecked())
                        && (!ch4.isChecked() && !ch5.isChecked() && !ch6.isChecked() && !ch7.isChecked())) {
                    aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                    aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                } else if (!ch1.isChecked() && !ch2.isChecked() && !ch3.isChecked()) {
                    aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                } else {
                    aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                }
                break;
            case 4:
                aq.id(R.id.chknbfeelstoowarmbaby4).checked(false);
                aq.id(R.id.chknbfeelscoldorhotbaby4).checked(false);
                if ((!ch.isChecked() && !ch1.isChecked() && !ch2.isChecked() && !ch3.isChecked())
                        && (!ch4.isChecked() && !ch5.isChecked() && !ch6.isChecked() && !ch7.isChecked())) {
                    aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                    aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                } else if (!ch1.isChecked() && !ch2.isChecked() && !ch3.isChecked()) {
                    aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                } else {
                    aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                }
                break;
        }
    }

    //Mani 15feb21 uncheck the other fields
    public void uncheckOtherFirstChild3(CheckBox checkBox, CheckBox checkBox1, CheckBox checkBox2, CheckBox checkBox3) {
        if (singleChildAliveId == 1 || singleChildActiveId == 1) {
            if (checkBox.isChecked() && checkBox1.isChecked()) {
                aq.id(R.id.chknbfeelscoldorhotbaby1).checked(false);
                aq.id(R.id.chknbfeelswarmbaby1).checked(false);
                aq.id(R.id.chknbfeelscoldorhotbaby).checked(false);
                aq.id(R.id.chknbfeelswarmbaby).checked(false);
                checkBox2.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                checkBox2.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                checkBox3.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                checkBox3.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
            }
        }
    }

    //Mani 15feb21 uncheck the other fields
    public void uncheckOtherFirstChild1(CheckBox checkBox, CheckBox checkBox1, CheckBox checkBox2, CheckBox checkBox3) {
        if (singleChildAliveId == 1 || singleChildActiveId == 1) {
            if (checkBox.isChecked() && checkBox1.isChecked()) {
                aq.id(R.id.chknbfeelswarmbaby1).checked(false);
                aq.id(R.id.chknbfeelstoowarmbaby1).checked(false);
                aq.id(R.id.chknbfeelstoowarmbaby).checked(false);
                aq.id(R.id.chknbfeelswarmbaby).checked(false);
                checkBox2.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                checkBox2.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                checkBox3.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                checkBox3.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
            }
        }
    }

    //Mani 15feb21 uncheck the other fields
    public void uncheckOtherFirstChild2(CheckBox checkBox, CheckBox checkBox1, CheckBox checkBox2, CheckBox checkBox3) {
        if (singleChildAliveId == 1 || singleChildActiveId == 1) {
            if (checkBox.isChecked() && checkBox1.isChecked()) {
                aq.id(R.id.chknbfeelscoldorhotbaby1).checked(false);
                aq.id(R.id.chknbfeelstoowarmbaby1).checked(false);
                aq.id(R.id.chknbfeelstoowarmbaby).checked(false);
                aq.id(R.id.chknbfeelscoldorhotbaby).checked(false);
                checkBox2.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                checkBox2.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                checkBox3.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                checkBox3.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
            }
        }

    }

    //Mani 15feb21 uncheck the other fields
    public void uncheckOtherDetails1(int i, CheckBox ch, CheckBox ch1, CheckBox ch2, CheckBox ch3, CheckBox ch4, CheckBox ch5, CheckBox ch6, CheckBox ch7) {
        switch (i) {
            case 1:
                aq.id(R.id.chknbfeelswarmbaby1).checked(false);
                aq.id(R.id.chknbfeelstoowarmbaby1).checked(false);
                if ((!ch.isChecked() && !ch1.isChecked() && !ch2.isChecked() && !ch3.isChecked())
                        && (!ch4.isChecked() && !ch5.isChecked() && !ch6.isChecked() && !ch7.isChecked())) {
                    aq.id(R.id.chknbfeelswarmbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelswarmbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                    aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                } else if (!ch1.isChecked() && !ch2.isChecked() && !ch3.isChecked()) {
                    aq.id(R.id.chknbfeelswarmbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelswarmbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                } else {
                    aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                }
                break;
            case 2:
                aq.id(R.id.chknbfeelswarmbaby2).checked(false);
                aq.id(R.id.chknbfeelstoowarmbaby2).checked(false);
                if ((!ch.isChecked() && !ch1.isChecked() && !ch2.isChecked() && !ch3.isChecked())
                        && (!ch4.isChecked() && !ch5.isChecked() && !ch6.isChecked() && !ch7.isChecked())) {
                    aq.id(R.id.chknbfeelswarmbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelswarmbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                    aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                } else if (!ch1.isChecked() && !ch2.isChecked() && !ch3.isChecked()) {
                    aq.id(R.id.chknbfeelswarmbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelswarmbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                } else {
                    aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                }
                break;
            case 3:
                aq.id(R.id.chknbfeelswarmbaby3).checked(false);
                aq.id(R.id.chknbfeelswarmbaby3).checked(false);
                if ((!ch.isChecked() && !ch1.isChecked() && !ch2.isChecked() && !ch3.isChecked())
                        && (!ch4.isChecked() && !ch5.isChecked() && !ch6.isChecked() && !ch7.isChecked())) {
                    aq.id(R.id.chknbfeelswarmbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelswarmbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                    aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                } else if (!ch1.isChecked() && !ch2.isChecked() && !ch3.isChecked()) {
                    aq.id(R.id.chknbfeelswarmbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelswarmbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                } else {
                    aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                }
                break;
            case 4:
                aq.id(R.id.chknbfeelswarmbaby4).checked(false);
                aq.id(R.id.chknbfeelswarmbaby4).checked(false);
                if ((!ch.isChecked() && !ch1.isChecked() && !ch2.isChecked() && !ch3.isChecked())
                        && (!ch4.isChecked() && !ch5.isChecked() && !ch6.isChecked() && !ch7.isChecked())) {
                    aq.id(R.id.chknbfeelswarmbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelswarmbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                    aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                } else if (!ch1.isChecked() && !ch2.isChecked() && !ch3.isChecked()) {
                    aq.id(R.id.chknbfeelswarmbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelswarmbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                } else {
                    aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox().setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
                }
                break;
        }

    }

    // Uncheck and Clear Data for Child reg details
    public void unCheckChildRegDetails(CheckBox chk, View trchildregid, View trchildregdate, EditText etchildregid, EditText etchildregdate, ImageView imgclear) {

        if (chk.isChecked()) {
            trchildregid.setVisibility(View.VISIBLE);
            trchildregdate.setVisibility(View.VISIBLE);
        } else {
            trchildregid.setVisibility(View.GONE);
            trchildregdate.setVisibility(View.GONE);
            etchildregid.setText("");
            etchildregdate.setText("");
            imgclear.setVisibility(View.GONE);
        }

    }

    // Save data
    private void savedata() {
        try {
            tblVisHeaderDao = databaseHelper.getTblVisitHeaderDao();
            tblVisDetailDao = databaseHelper.getTblVisitDetailDao();
            tblChildVisDetailDao = databaseHelper.getTblChildVisitDetailDao();
            tblVisHeaderDao = databaseHelper.getTblVisitHeaderDao();
            //30Nov2019 - Bind
            tblVisChildHeaderDao = databaseHelper.getTblVisitChildHeaderDao();
            //28-6-2021
            tblImageStoreDao = databaseHelper.getTblImageStoreDao();
            prepareVisitHeaderData();
            prepareMotherVisitDetailsData();
            prepareNbVisitDetailsData();
            prepareChildVisitHeaderData();

            //            16May2021 Bindu covid data
            tblCovidTestDao = databaseHelper.getTblCovidTestDao();
            tblCovidVaccineDao = databaseHelper.getTblCovidVaccineDao();
            //            16May2021 Bindu
            if (aq.id(R.id.rd_CovidTestYes).isChecked() || aq.id(R.id.rd_CovidTestNo).isChecked()) //23May2021 Bindu put cond
                prepareCovidTestdata();
            prepareCovidVacinedata();

            final TransactionHeaderRepository transRepo = new TransactionHeaderRepository(databaseHelper);
            transId = transRepo.iCreateNewTrans(appState.sessionUserId, databaseHelper);
            tblvisH.setTransid(transId);
            tblvisD.setTransid(transId);
            tblCvisD.setTransid(transId);
            tblvisCH.setTransid(transId);

//16May2021 Bindu
            if (tblcovtest != null) //23May2021 Bindu
                tblcovtest.setTransId(transId);
            tblcovvac.setTransId(transId);

            prepareAuditTrail();

            TransactionManager.callInTransaction(databaseHelper.getConnectionSource(), new Callable<Void>() {
                public Void call() throws Exception {
                    int add;
                    boolean added;
                    boolean isMotherdatainserted = true, isNbdatainseted = true;
                    add = tblVisHeaderDao.create(tblvisH);

                    if (add > 0) {
                        added = transRepo.iNewRecordTransNew(appState.sessionUserId, transId,
                                tblVisHeaderDao.getTableName(), databaseHelper);

                        if (added) {

                            if (!(isMotherMortality || isWomanDeactivated)) {
                                if (insertMotherSignsInvestVal != null && insertMotherSignsInvestVal.size() > 0) {
                                    for (Map.Entry<String, VisitDetailsPojo> entry : insertMotherSignsInvestVal.entrySet()) {
                                        tblvisD.setVisDSymptoms(entry.getKey());
                                        tblvisD.setVisDReferredToFacilityType(entry.getValue().getRefTo());
                                        if (entry.getKey().equals("168") && aq.id(R.id.etothersymptomsmother).getText().toString().length() > 0) {
                                            tblvisD.setVisDOtherSymptoms(aq.id(R.id.etothersymptomsmother).getText().toString());
                                        } else
                                            tblvisD.setVisDOtherSymptoms("");
                                        add = tblVisDetailDao.create(tblvisD);

                                    }
                                    if (add > 0) {
                                        added = transRepo.iNewRecordTransNew(appState.sessionUserId, transId,
                                                tblVisDetailDao.getTableName(), databaseHelper);
                                        isMotherdatainserted = true;
                                    } else
                                        isMotherdatainserted = false;
                                }


                                //17May2021 Bindu - for Mother covid details capture
                                if (tblcovtest != null) {//23May2021 Bindu
                                    add = tblCovidTestDao.create(tblcovtest);
                                    if (add > 0) {
                                        added = transRepo.iNewRecordTransNew(appState.sessionUserId, transId,
                                                tblCovidTestDao.getTableName(), databaseHelper);
                                    }
                                }

                                if (added) {
//                                        if (!isvaccinated) { //20MAy2021 Bindu
                                    if (insertCovidVaccineVal != null && insertCovidVaccineVal.size() > 0) {
                                        for (Map.Entry<String, String> entry : insertCovidVaccineVal.entrySet()) {
                                            tblcovvac.setCovidVaccinatedNo(entry.getKey());
                                            tblcovvac.setCovidVaccinatedDate(entry.getValue());
                                            add = tblCovidVaccineDao.create(tblcovvac);
                                        }
                                        if (add > 0) {
                                            added = transRepo.iNewRecordTransNew(appState.sessionUserId, transId,
                                                    tblCovidVaccineDao.getTableName(), databaseHelper);
                                        }

                                    } /*else {
                                            String upSql = checkForAuditTrail(transId);
                                            if (upSql != null) {
                                                transRepo.iNewRecordTransNew(appState.sessionUserId, transId, "tblaudittrail", databaseHelper);

                                                CovidDetailsRepository covrepo = new CovidDetailsRepository(databaseHelper);
                                                add = covrepo.updateCovidVaccine(woman.getUserId(), upSql, transId, databaseHelper);
                                            }
                                        }*/
                                    if (added) {
                                        isMotherdatainserted = true;
                                    } else
                                        isMotherdatainserted = false;
                                }
                                //}
                            }
// New born
                            if (isMotherdatainserted && !(isAllChildMortality || isAllChildDeactivated)) {  // Either mortality or deactivated

                                if (insertchildHeaderMap != null && insertchildHeaderMap.size() > 0) {
                                    for (Map.Entry<String, VisitDetailsPojo> entry : insertchildHeaderMap.entrySet()) {
                                        tblvisCH.setChildId(entry.getKey());
                                        tblvisCH.setVisCHTemperature(entry.getValue().getTemp());
                                        tblvisCH.setVisCHTemperatureDate(entry.getValue().getTempdate());
                                        tblvisCH.setVisCHWeight(entry.getValue().getWeight());
                                        tblvisCH.setVisCHWeightDate(entry.getValue().getWeightdate());
                                        tblvisCH.setVisCHResprate(entry.getValue().getResprate());
                                        tblvisCH.setVisCHResprateDate(entry.getValue().getRespratedate());
                                        tblvisCH.setVisCHRegAtGovtFac(entry.getValue().isChildregatgovtfac());
                                        tblvisCH.setVisCHRegGovtFacId(entry.getValue().getChildregid());
                                        tblvisCH.setVisCHRegAtGovtFacDate(entry.getValue().getChildregdate());
                                        tblvisCH.setChildNo(entry.getValue().getChildNo());
                                        tblvisCH.setVisitNum(entry.getValue().getChildVisitNumber());
                                        tblvisCH.setChildvisHIsIFATaken(entry.getValue().getIfataken());
                                        tblvisCH.setChildvisHIFADaily(entry.getValue().getIfaconsume());
                                        tblvisCH.setChildvisHIFATabletsCount(entry.getValue().getIfacount());
                                        tblvisCH.setChildvisHIFANotTakenReason(entry.getValue().getIfanotgivenreason());
                                        tblvisCH.setChildvisHIFAGivenBy(entry.getValue().getIfagivenby());
                                        tblvisCH.setChildvisHDewormingtab(entry.getValue().getDewormingtablet());
                                        add = tblVisChildHeaderDao.create(tblvisCH);

                                    }

                                    if (add > 0) {
                                        added = transRepo.iNewRecordTransNew(appState.sessionUserId, transId,
                                                tblVisChildHeaderDao.getTableName(), databaseHelper);
                                        isNbdatainseted = true;
                                    } else
                                        isNbdatainseted = false;
                                }

                                if (isNbdatainseted && insertchildMap != null && insertchildMap.size() > 0) {
                                    for (Map.Entry<String, VisitDetailsPojo> entry : insertchildMap.entrySet()) {
                                        tblCvisD.setVisDSymptoms(entry.getValue().getSympid());
                                        //tblCvisD.setChildId(entry.getKey());
                                        tblCvisD.setChildId(entry.getValue().getChildId());
                                        tblCvisD.setChildNo(1);
                                        tblCvisD.setVisDReferredToFacilityType(entry.getValue().getRefTo());
                                        if (entry.getKey().equals("323") && aq.id(R.id.etothersymptomsnb).getText().toString().length() > 0) {
                                            tblCvisD.setVisDOtherSymptoms(aq.id(R.id.etothersymptomsnb).getText().toString());
                                        } else
                                            tblCvisD.setVisDOtherSymptoms("");
                                        add = tblChildVisDetailDao.create(tblCvisD);

                                    }
                                    if (add > 0) {
                                        isNbdatainseted = true;
                                    } else
                                        isNbdatainseted = false;
                                }

                                //Second child
                                if (isNbdatainseted && insertchildMap2 != null && insertchildMap2.size() > 0) {
                                    for (Map.Entry<String, VisitDetailsPojo> entry : insertchildMap2.entrySet()) {
                                        tblCvisD.setVisDSymptoms(entry.getValue().getSympid());
                                        //tblCvisD.setChildId(entry.getKey());
                                        tblCvisD.setChildId(entry.getValue().getChildId());  //04Dec2019 - Bindu - set childid
                                        tblCvisD.setChildNo(2);
                                        tblCvisD.setVisDReferredToFacilityType(entry.getValue().getRefTo());
                                        /*if (entry.getKey().equals("314") && aq.id(R.id.etothersymptomsnb).getText().toString().length() > 0) {
                                            tblCvisD.setVisDOtherSymptoms(aq.id(R.id.etothersymptomsnb).getText().toString());
                                        } else
                                            tblCvisD.setVisDOtherSymptoms("");*/
                                        add = tblChildVisDetailDao.create(tblCvisD);

                                    }
                                    if (add > 0) {
                                        /*added = transRepo.iNewRecordTransNew(appState.sessionUserId, transId,
                                                tblChildVisDetailDao.getTableName(), databaseHelper);*/
                                        isNbdatainseted = true;
                                    } else
                                        isNbdatainseted = false;
                                }
                                // Third child
                                if (isNbdatainseted && insertchildMap3 != null && insertchildMap3.size() > 0) {
                                    for (Map.Entry<String, VisitDetailsPojo> entry : insertchildMap3.entrySet()) {
                                        tblCvisD.setVisDSymptoms(entry.getValue().getSympid());
                                        //tblCvisD.setChildId(entry.getKey());
                                        tblCvisD.setChildId(entry.getValue().getChildId());
                                        tblCvisD.setChildNo(3);
                                        tblCvisD.setVisDReferredToFacilityType(entry.getValue().getRefTo());
                                        /*if (entry.getKey().equals("314") && aq.id(R.id.etothersymptomsnb).getText().toString().length() > 0) {
                                            tblCvisD.setVisDOtherSymptoms(aq.id(R.id.etothersymptomsnb).getText().toString());
                                        } else
                                            tblCvisD.setVisDOtherSymptoms("");*/
                                        add = tblChildVisDetailDao.create(tblCvisD);

                                    }
                                    if (add > 0) {
                                        /*added = transRepo.iNewRecordTransNew(appState.sessionUserId, transId,
                                                tblChildVisDetailDao.getTableName(), databaseHelper);*/
                                        isNbdatainseted = true;
                                    } else
                                        isNbdatainseted = false;
                                }

                                //Fourth child
                                if (isNbdatainseted && insertchildMap4 != null && insertchildMap4.size() > 0) {
                                    for (Map.Entry<String, VisitDetailsPojo> entry : insertchildMap4.entrySet()) {
                                        tblCvisD.setVisDSymptoms(entry.getValue().getSympid());
                                        // tblCvisD.setChildId(entry.getKey());
                                        tblCvisD.setChildId(entry.getValue().getChildId());
                                        tblCvisD.setChildNo(4);
                                        tblCvisD.setVisDReferredToFacilityType(entry.getValue().getRefTo());
                                        /*if (entry.getKey().equals("314") && aq.id(R.id.etothersymptomsnb).getText().toString().length() > 0) {
                                            tblCvisD.setVisDOtherSymptoms(aq.id(R.id.etothersymptomsnb).getText().toString());
                                        } else
                                            tblCvisD.setVisDOtherSymptoms("");*/
                                        add = tblChildVisDetailDao.create(tblCvisD);

                                    }
                                    if (add > 0) {
                                        /*added = transRepo.iNewRecordTransNew(appState.sessionUserId, transId,
                                                tblChildVisDetailDao.getTableName(), databaseHelper);*/
                                        isNbdatainseted = true;
                                    } else
                                        isNbdatainseted = false;
                                }

                                if (isNbdatainseted) {
                                    added = transRepo.iNewRecordTransNew(appState.sessionUserId, transId,
                                            tblChildVisDetailDao.getTableName(), databaseHelper);
                                }
                            }


                        }

                        if (AllImagesBeforeSave.size()!=0 && AllImagePojo.size()!=0){
                            for (tblImageStore s: AllImagesBeforeSave){
                                s.setTransId(transId);
                                s.setImagePath("");
                                add = tblImageStoreDao.create(s);
                            }
                            if (add>0){
                                added = transRepo.iNewRecordTransNew(appState.sessionUserId, transId,
                                        tblImageStoreDao.getTableName(), databaseHelper);
                            }
                            for (ImagePojo s:AllImagePojo){
                                fileUpload();
                                FileOutputStream fileOutputStream = new FileOutputStream(s.getImagePath());
                                fileOutputStream.write(s.getImage());
                            }
                        }

                        if (added && isMotherdatainserted && isNbdatainseted) {
                            HomeVisitRepository homeRepo = new HomeVisitRepository(databaseHelper);
                            int update = homeRepo.updateVisitDetailsWomanInRegPNC(woman.getWomanId(), woman.getUserId(), hmvisitregdetails, transId, databaseHelper);
                            if (update > 0) {
                                added = transRepo.iNewRecordTransNew(appState.sessionUserId, transId,
                                        "tblaudittrail", databaseHelper);//Arpitha 24July2021

                                if (tblvisH.getVisHCompl().equalsIgnoreCase("Y") || tblvisCH.getVisCHCompl().equalsIgnoreCase("Y")) { //If compl, then send sms //16May2021 Bindu - check for newborn compl as well
                                    //04Dec2019 - Bindu
                                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(PncHomeVisit.this);
                                    //mani 15 april 2021 uncommented lines
                                    if (checkSimState() == TelephonyManager.SIM_STATE_READY &&
                                            prefs.getBoolean("sms", false)) {
                                        sendSMS(transRepo, databaseHelper, tblvisH);
                                    } else//21Ma2021 Arpitha
                                    {

                                            Intent intent = new Intent(PncHomeVisit.this, HomeVisitListActivity.class);
                                            intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                            intent.putExtra("woman", woman);
                                            startActivity(intent);

                                    }
                                } else//21Ma2021 Arpitha
                                {
                                    //mani 30Aug2021 added childhv condition
                                        Intent intent = new Intent(PncHomeVisit.this, HomeVisitListActivity.class);
                                        intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                        intent.putExtra("woman", woman);
                                        startActivity(intent);

                                }
                            } else {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.failedtoaddhomevisit), Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                    return null;
                }
            });
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            e.printStackTrace();
        }
    }

    // Create data for Child visit header tbl
    private void prepareChildVisitHeaderData() {
        try {
            tblvisCH = new TblVisitChildHeader();

            tblvisCH.setUserId(user.getUserId());
            tblvisCH.setWomanId(woman.getWomanId());
//            tblvisCH.setVisitNum(visitNum);
            tblvisCH.setVisHVisitDate(aq.id(R.id.etvisitdate).getText().toString());
            tblvisCH.setVisHVisitIsReferred(aq.id(R.id.chkisreferred).isChecked() ? getResources().getString(R.string.yesshortform) : getResources().getString(R.string.noshortform));

            String sel = "";
            if (aq.id(R.id.spnfacnameHv).getSelectedItem() != null)
                sel = aq.id(R.id.spnfacnameHv).getSelectedItem().toString();
            if (sel.trim().length() > 0 && !(sel.equalsIgnoreCase(getResources().getString(R.string.select)) || sel.equalsIgnoreCase(getResources().getString(R.string.other)))) {
                tblvisCH.setVisHVisitReferredToFacilityName(aq.id(R.id.spnfacnameHv).getSelectedItem().toString());
                tblvisCH.setVisHVisitReferredToFacilityType(aq.id(R.id.spnfactype).getSelectedItem().toString()); //15Apr2021 - Bindu - set facility type
                /*for (Map.Entry<String, String> village : mapRefFacilityname.entrySet()) {
                    if (aq.id(R.id.spnfacnameHv).getSelectedItem().toString().equalsIgnoreCase(village.getValue()))
                        tblvisCH.setVisHVisitReferredToFacilityType(village.getKey());
                }*/
            } else if (sel.equalsIgnoreCase(getResources().getString(R.string.other))
                    && aq.id(R.id.etreffacilityname).getText().toString().trim().length() <= 0) {
                tblvisCH.setVisHVisitReferredToFacilityName(aq.id(R.id.etreffacilityname).getText().toString());
                tblvisCH.setVisHVisitReferredToFacilityType(getResources().getString(R.string.other));
            } else {
                tblvisCH.setVisHVisitReferredToFacilityName(aq.id(R.id.etreffacilityname).getText().toString());
                if (aq.id(R.id.etreffacilityname).getText().toString().length() > 0)
                    tblvisCH.setVisHVisitReferredToFacilityType(getResources().getString(R.string.other));
                else
                    tblvisCH.setVisHVisitReferredToFacilityType("");
            }

            tblvisCH.setVisHVisitComments(aq.id(R.id.etvisitcomments).getText().toString());
            // if (arrLNbSymptoms.size() > 0)
            if (pncNbs.size() > 0)  //26Apr2021 Bindu - NB danger signs size and set
                tblvisCH.setVisCHCompl(getResources().getString(R.string.yesshortform));
            else
                tblvisCH.setVisCHCompl(getResources().getString(R.string.noshortform));
            tblvisCH.setVisCHAdvise(aq.id(R.id.txtactionsumm).getText().toString());
            tblvisCH.setVisHChildSymptoms(aq.id(R.id.txtDangSignsNbE).getText().toString()); //pending
            tblvisCH.setVisHUserType(appState.userType);
            tblvisCH.setRecordcreateddate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
            tblvisCH.setRecordupdateddate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
            tblvisCH.setVisHReferralSlipNumber("" + aq.id(R.id.etrefslipnumber).getText().toString()); //26Apr2021 Bindu add referal slip number

            //mani 18June2021
            String ashaavailabilitychild="";
            if (aq.id(R.id.ashavailableYes).isChecked())
                ashaavailabilitychild="Y";
            else if (aq.id(R.id.ashavailableNo).isChecked())
                ashaavailabilitychild="N";
            else if (aq.id(R.id.ashavailableDontKnow).isChecked())
                ashaavailabilitychild="Dont Know";
            tblvisCH.setVisCHAshaAvailable(ashaavailabilitychild);

            insertchildHeaderMap = new LinkedHashMap<>();

            for (Integer n : unionchildlist) {
                if (myChildIDs.get(n) != null) {
                    if (n == 1) {
                        // visi define // step 2 - vsitnum add in constructor
                        HomeVisitRepository homeRepo = new HomeVisitRepository(databaseHelper);
                        childVisitNum = homeRepo.getLastHomeVisitNoChild(appState.sessionUserId,myChildIDs.get(n));
                        setchltabletdetails(1);
                        insertchildHeaderMap.put(myChildIDs.get(n), new VisitDetailsPojo(myChildIDs.get(n),
                                aq.id(R.id.etweightbaby1).getText().toString(), aq.id(R.id.etweightdatebaby1).getText().toString(),
                                aq.id(R.id.etTempbaby1).getText().toString(), aq.id(R.id.ettempdonedatebaby1).getText().toString(),
                                aq.id(R.id.etrespratebaby1).getText().toString(), aq.id(R.id.etrespratedonedatebaby1).getText().toString(),
                                aq.id(R.id.chkischildregatfacchild1).isChecked(), aq.id(R.id.etchildregid1).getText().toString(), aq.id(R.id.etchildregdate1).getText().toString(), 1,childVisitNum,ifaTaken,ifaConsumed,ifaTabletCount,ifaNotGivenReason,ifaAdviceGiven,dewormingtab));
                    } else if (n == 2) {
                        HomeVisitRepository homeRepo = new HomeVisitRepository(databaseHelper);
                        childVisitNum = homeRepo.getLastHomeVisitNoChild(appState.sessionUserId,myChildIDs.get(n));
                        setchltabletdetails(2);
                        insertchildHeaderMap.put(myChildIDs.get(n), new VisitDetailsPojo(myChildIDs.get(n),
                                aq.id(R.id.etweightbaby2).getText().toString(), aq.id(R.id.etweightdatebaby2).getText().toString(),
                                aq.id(R.id.etTempbaby2).getText().toString(), aq.id(R.id.ettempdonedatebaby2).getText().toString(),
                                aq.id(R.id.etrespratebaby2).getText().toString(), aq.id(R.id.etrespratedonedatebaby2).getText().toString(),
                                aq.id(R.id.chkischildregatfacchild2).isChecked(), aq.id(R.id.etchildregid2).getText().toString(), aq.id(R.id.etchildregdate2).getText().toString(), 2,childVisitNum,ifaTaken,ifaConsumed,ifaTabletCount,ifaNotGivenReason,ifaAdviceGiven,dewormingtab));
                    } else if (n == 3) {
                        HomeVisitRepository homeRepo = new HomeVisitRepository(databaseHelper);
                        childVisitNum = homeRepo.getLastHomeVisitNoChild(appState.sessionUserId,myChildIDs.get(n));
                        setchltabletdetails(3);
                        insertchildHeaderMap.put(myChildIDs.get(n), new VisitDetailsPojo(myChildIDs.get(n),
                                aq.id(R.id.etweightbaby3).getText().toString(), aq.id(R.id.etweightdatebaby3).getText().toString(),
                                aq.id(R.id.etTempbaby3).getText().toString(), aq.id(R.id.ettempdonedatebaby3).getText().toString(),
                                aq.id(R.id.etrespratebaby3).getText().toString(), aq.id(R.id.etrespratedonedatebaby3).getText().toString(),
                                aq.id(R.id.chkischildregatfacchild3).isChecked(), aq.id(R.id.etchildregid3).getText().toString(), aq.id(R.id.etchildregdate3).getText().toString(), 3,childVisitNum,ifaTaken,ifaConsumed,ifaTabletCount,ifaNotGivenReason,ifaAdviceGiven,dewormingtab));
                    } else if (n == 4) {
                        HomeVisitRepository homeRepo = new HomeVisitRepository(databaseHelper);
                        childVisitNum = homeRepo.getLastHomeVisitNoChild(appState.sessionUserId,myChildIDs.get(n));
                        setchltabletdetails(4);
                        insertchildHeaderMap.put(myChildIDs.get(n), new VisitDetailsPojo(myChildIDs.get(n),
                                aq.id(R.id.etweightbaby4).getText().toString(), aq.id(R.id.etweightdatebaby4).getText().toString(),
                                aq.id(R.id.etTempbaby4).getText().toString(), aq.id(R.id.ettempdonedatebaby4).getText().toString(),
                                aq.id(R.id.etrespratebaby4).getText().toString(), aq.id(R.id.etrespratedonedatebaby4).getText().toString(),
                                aq.id(R.id.chkischildregatfacchild4).isChecked(), aq.id(R.id.etchildregid4).getText().toString(), aq.id(R.id.etchildregdate4).getText().toString(), 4,childVisitNum,ifaTaken,ifaConsumed,ifaTabletCount,ifaNotGivenReason,ifaAdviceGiven,dewormingtab));
                    }
                }
            }


        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private void setchltabletdetails(int i) {
        // Tablet details
        if (aq.id(rd_IFAYes + i).isChecked()) {
            ifaTaken="Yes";
            if (aq.id(rd_ConsumingIFAYes + i).isChecked()) {
                ifaConsumed="Yes";
                ifaTabletCount=aq.id(etIFANoofTabletsConsumes + i).getEditText().getText().toString();
                ifaNotGivenReason="";

            } else {
                ifaConsumed="No";
                String reasons="";
                if (i>0) {
                    if (i == 1)
                        reasons = spnIFAReasons1.getSelectedIndicies().toString();
                    else if (i == 2)
                        reasons = spnIFAReasons2.getSelectedIndicies().toString();
                    else if (i == 3)
                        reasons = spnIFAReasons3.getSelectedIndicies().toString();
                    else if (i == 4)
                        reasons = spnIFAReasons4.getSelectedIndicies().toString();
                }
                reasons = reasons.replace("[", "");
                reasons = reasons.replace("]", "");
                ifaNotGivenReason=reasons;
                ifaTabletCount="";
            }
            ifaAdviceGiven=aq.id(spnChlWhoGaveIFATablets + i).getSpinner().getSelectedItemPosition();
        } else {

            ifaTaken="No";
            ifaConsumed="";
            ifaTabletCount="";
            ifaNotGivenReason="";
            ifaAdviceGiven=0;
        }

        if (aq.id(rd_dewormingtabletsYes + i ).isChecked()) {
            dewormingtab="Yes";
        } else if (aq.id(rd_dewormingtabletsNo + i).isChecked()) {
            dewormingtab="No";
        } else if (aq.id(rd_dewormingtabletsDono + i).isChecked()) {
            dewormingtab="Dont Know";
        }
    }

    // Hide tabs and set the proper selected tab
    private void hidetabs() {
        aq.id(R.id.btnSummary).getImageView().setBackgroundResource(R.drawable.gray_button);
        aq.id(R.id.btnmotherdangersigns).getImageView().setBackgroundResource(R.drawable.gray_button);
        aq.id(R.id.btnnewborndangersigns).getImageView().setBackgroundResource(R.drawable.gray_button);
    }

    // Button summary click action
    private void displayBtnSummaryClickedAction() {
        try {

            if (aq.id(R.id.scrmotherdangersigns).getView().getVisibility() == View.VISIBLE && !(isAllChildMortality || isAllChildDeactivated)) { // 04Dec2019 - Bindu - set new brn danger signs click
                aq.id(R.id.txtselectedbtnheading).text(getResources().getString(R.string.newborndangersigns));
                hidetabs();
                aq.id(R.id.btnnewborndangersigns).getImageView().setBackgroundColor(getResources().getColor(R.color.gray));
                aq.id(R.id.scrmotherdangersigns).gone();
                aq.id(R.id.scrnewborndangersigns).visible();
                aq.id(R.id.scrhomevisitsummary).gone();
                aq.id(R.id.imgSummaryAddPhoto).gone();
            } else {

                aq.id(R.id.txtselectedbtnheading).text(getResources().getString(R.string.homevisitsummary));
                hidetabs();
                aq.id(R.id.btnSummary).getImageView().setBackgroundColor(getResources().getColor(R.color.gray));
                aq.id(R.id.scrmotherdangersigns).gone();
                aq.id(R.id.scrnewborndangersigns).gone();
                aq.id(R.id.scrhomevisitsummary).visible();
                aq.id(R.id.imgSummaryAddPhoto).visible();
                aq.id(R.id.btnsavehomevisit).getImageView().setImageResource(R.drawable.save);
                isBtnSummaryClicked = true;

                setSymptomsSummary();
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    public void opencloseAccordion(View v, CheckBox chk, CheckBox chkbaby1, CheckBox chkbaby2, CheckBox chkbaby3, CheckBox chkbaby4) {
        try {
            if (chk.isChecked()) {
                v.setVisibility(View.VISIBLE);
                /*chk.setTextColor(getResources().getColor(R.color.red));
                chk.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);*/

                //29Nov2019 - Bindu - check if single alive child then default check the checkbox
                if (singleChildAliveId != 0 || singleChildActiveId != 0) {
                    chk.setTextColor(getResources().getColor(R.color.red));
                    chk.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
                    if (singleChildAliveId == 1 || singleChildActiveId == 1)
                        chkbaby1.setChecked(true);
                    else if (singleChildAliveId == 2 || singleChildActiveId == 2) {
                        chkbaby2.setChecked(true);
                    } else if (singleChildAliveId == 3 || singleChildActiveId == 3) {
                        chkbaby3.setChecked(true);
                    } else if (singleChildAliveId == 4 || singleChildActiveId == 4) {
                        chkbaby4.setChecked(true);
                    }
                }
                /* if(childListIds != null && childListIds.size() == 1) {
                    chk.setTextColor(getResources().getColor(R.color.red));
                    chk.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
                    for(int i=0;i<childListIds.size(); i++) {
                        if(childListIds.get(i).getChildNo() == 1) {
                            chkbaby1.setChecked(true);
                        }
                        else if(childListIds.get(i).getChildNo() == 2) {
                            chkbaby2.setChecked(true);
                        }
                        else if(childListIds.get(i).getChildNo() == 3) {
                            chkbaby3.setChecked(true);
                        }
                        else if(childListIds.get(i).getChildNo() == 4) {
                            chkbaby4.setChecked(true);
                        }
                    }
                }*/
            } else {
                v.setVisibility(View.GONE);
                chk.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                chk.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);

                chkbaby1.setChecked(false);
                chkbaby2.setChecked(false);
                chkbaby3.setChecked(false);
                chkbaby4.setChecked(false);

            }

            //setSelectedSymptomTextColor(chk,chkbaby1,chkbaby2,chkbaby3,chkbaby4);
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    public void setSelectedSymptomTextColor(CheckBox chk, CheckBox chkbaby1, CheckBox chkbaby2, CheckBox chkbaby3, CheckBox chkbaby4) {
        try {
            if (chk.isChecked() && (chkbaby1.isChecked() || chkbaby2.isChecked() || chkbaby3.isChecked() || chkbaby4.isChecked())) {
                chk.setTextColor(getResources().getColor(R.color.red));
                chk.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
                //26Apr2021 Bindu set error
                chk.setError(null);
            } else {
                chk.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                chk.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    public void opencloseAccordionCheckbox(CheckBox chk) {
        try {
            if (chk.isChecked()) {
                chk.setTextColor(getResources().getColor(R.color.red));
                chk.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, 0, 0);
            } else {
                chk.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                chk.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    /**
     * Initiates the Navigation drawer
     */
    private void initiateDrawer() {
        try {
            mDrawerLayout = findViewById(R.id.ett_drawer_layout);
            mDrawerList = findViewById(R.id.ett_left_drawer);
          /*  mDrawerToggle = new ActionBarDrawerToggle
                    (
                            this,
                            mDrawerLayout,
                            R.string.drawer_open,
                            R.string.drawer_close
                    )
            {
            };
            mDrawerLayout.setDrawerListener(mDrawerToggle);
            mDrawerToggle.syncState(); */

            navDrawerItems = new ArrayList<NavDrawerItem>();

            // adding nav drawer items to array
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.sp_WRegEdit),
                    R.drawable.registration));

            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.services),
                    R.drawable.anm_pending_activities));


            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.unplannedserviceslist),
                    R.drawable.anm_pending_activities));
            //14Sep2019 - Bindu - Add Homevisit
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.womanhomevisithistory),
                    R.drawable.prenatalhomevisit));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deliveryHeading), R.drawable.delivery_info));//31Oct2019 Arpitha

            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deactivate),
                    R.drawable.deactivate));//28Nov2019 Arpitha
            // set a custom shadow that overlays the main content when the drawer
            // opens
            mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
            mDrawerList.setAdapter(new NavDrawerListAdapter(getApplicationContext(), navDrawerItems));
            mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

            // ActionBarDrawerToggle ties together the the proper interactions
            // between the sliding drawer and the action bar app icon
            mDrawerToggle = new ActionBarDrawerToggle(this, /* host Activity */
                    mDrawerLayout, /* DrawerLayout object */
                    R.drawable.ic_drawer, /*
             * nav drawer image to replace 'Up'
             * caret
             */
                    R.string.drawer_open, /*
             * "open drawer" description for
             * accessibility
             */
                    R.string.drawer_close /*
             * "close drawer" description for
             * accessibility
             */
            ) {
                public void onDrawerClosed(View view) {
                    /*invalidateOptionsMenu(); // creates call to
                    getSupportActionBar().setDisplayShowHomeEnabled(false);
                    getSupportActionBar().setDisplayShowTitleEnabled(false);
                    getSupportActionBar().setDisplayShowCustomEnabled(false);
                    int noOfWeeks;
                    try {
                        if (woman.getRegADDate() != null && woman.getRegADDate().length() > 0) {
                            getSupportActionBar().setTitle((woman.getRegWomanName() + "\t"));
                        } else {
                            noOfWeeks = CommonClass.getNumberOfWeeksFromLMP(woman.getRegLMP());
                            getSupportActionBar().setTitle((woman.getRegWomanName() + " (" + noOfWeeks + " "
                                    + getResources().getString(R.string.weeks) + ")"));
                        }*/
                    try {
                        displayDrawerItems();
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }
                }

                public void onDrawerOpened(View drawerView) {
                    try {

                        displayDrawerItems();
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }
                }
            };
            mDrawerLayout.addDrawerListener(mDrawerToggle);
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    // Display drawer open close items
    private void displayDrawerItems() throws Exception {

        invalidateOptionsMenu(); // creates call to
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer1);
        getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);


        aq.id(R.id.ivWomanImg).getImageView().setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                mDrawerLayout.closeDrawer(mDrawerList);
            }
        });

        aq.id(R.id.tvWomanName).text((woman.getRegWomanName() + "\t"));
        if (woman.getRegADDate() != null && woman.getRegADDate().length() > 0) {
            aq.id(R.id.tvInfo1).text(woman.getRegADDate());
        } else {
            int noOfWeeks = DateTimeUtil.getNumberOfWeeksFromLMP(woman.getRegLMP());
            aq.id(R.id.tvInfo1).text(getResources().getString(R.string.tvlmp) + woman.getRegLMP() + "  , GA : " + noOfWeeks + "W");
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle != null && mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        try {
            switch (item.getItemId()) {
                case android.R.id.home:
                    Intent home = new Intent(PncHomeVisit.this, MainMenuActivity.class);
                    home.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    startActivity(home);
                    break;

                case R.id.wlist:
                    Intent wlist = new Intent(PncHomeVisit.this, RegisteredWomenActionTabs.class);
                    wlist.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    /*startActivity(wlist);*/
                    displayAlert(getResources().getString(R.string.exit), wlist);
                    break;

                case R.id.logout:
                    Intent logout = new Intent(PncHomeVisit.this, LoginActivity.class);
                    displayAlert(getResources().getString(R.string.m111), logout);
                    /*startActivity(logout);
                    CommonClass.updateLogoutTime();*/
                    break;

                case R.id.home:
                    Intent intent = new Intent(PncHomeVisit.this, MainMenuActivity.class);
                    intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    startActivity(intent);
                    break;
                //30Sep2019 - Bindu
                case R.id.about: {
                    Intent goToScreen = new Intent(PncHomeVisit.this, AboutActivity.class);
                    goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    displayAlert(getResources().getString(R.string.exit), goToScreen);
                    return true;
                }
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        return super.onOptionsItemSelected(item);
    }


    /**
     * Initializes the Action Bar items
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        try {
            //16Sep2019 - Bindu
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer1);
            getSupportActionBar().setDisplayShowCustomEnabled(true);

            getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);

            aq.id(R.id.tvWomanName).text(woman.getRegWomanName());
            if (woman.getRegADDate() != null && woman.getRegADDate().length() > 0) {
                aq.id(R.id.tvInfo1).text(getResources().getString(R.string.tvadd) + " - " + woman.getRegADDate());
            } else {
                int noOfWeeks = DateTimeUtil.getNumberOfWeeksFromLMP(woman.getRegLMP());
                aq.id(R.id.tvInfo1).text(getResources().getString(R.string.tvlmp) + woman.getRegLMP() + "  , GA : " + noOfWeeks + "W");
            }

            aq.id(R.id.ivWomanImg).gone();

            getMenuInflater().inflate(R.menu.servicesmenu, menu);

            menu.findItem(R.id.logout).setTitle((getResources().getString(R.string.action_logout)));


        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        // mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    /**
     * The click listner for ListView in the navigation drawer
     */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            mDrawerLayout.closeDrawer(mDrawerList);
            //   if (woman.getRegADDate() == null || woman.getRegADDate().length() == 0) {
            try {
                if (position == 0) {
                    Intent viewprof = new Intent(PncHomeVisit.this, ViewProfileActivity.class);
                    viewprof.putExtra("woman", woman); //26Jul2019 - Bindu
                    viewprof.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    startActivity(viewprof);
                } else if (position == 1) {
                    Intent unplanned = new Intent(PncHomeVisit.this, ServicesSummaryActivity.class);
                    unplanned.putExtra("woman", woman); //26Jul2019 - Bindu
                    unplanned.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    startActivity(unplanned);
                } else if (position == 2) {
                    Intent unplanned = new Intent(PncHomeVisit.this,
                            UnplannedServicesListActivity.class); //07Dec2019 - Bindu - add screen to list
                    unplanned.putExtra("woman", woman); //26Jul2019 - Bindu
                    unplanned.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    startActivity(unplanned);
                } else if (position == 3) {

                        Intent intent = new Intent(PncHomeVisit.this, HomeVisitListActivity.class);
                        intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        intent.putExtra("woman", woman);
                        startActivity(intent);

                }//01Oct2019 Arpitha
                else if (position == 4) {
                    if (woman.getRegPregnantorMother() == 2 || DateTimeUtil.getNumberOfDaysFromLMP(woman.getRegLMP()) > 210) {
                        Intent del = new Intent(PncHomeVisit.this, DeliveryInfoActivity.class);
                        del.putExtra("woman", woman);
                        del.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        startActivity(del);
                    } else
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.m132), Toast.LENGTH_LONG).show();

                }
                //                  28Nov2019 Arpitha
                else if (position == 5) {
                    Intent deact = new Intent(PncHomeVisit.this, WomanDeactivateActivity.class);
                    deact.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    deact.putExtra("woman", woman);
                    startActivity(deact);
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
           /* } else {
            }*/
        }
    }

    private void displayAlert(final String spanText2, final Intent goToScreen) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set dialog message
        alertDialogBuilder.setMessage(spanText2).setCancelable(false).setNegativeButton(
                (getResources().getString(R.string.m121)), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (goToScreen != null) {
                            try {
                                startActivity(goToScreen);
                                if (spanText2.contains("logout")) {
                                    try {
                                        LoginRepository loginRepo = new LoginRepository(databaseHelper);
                                        loginRepo.updateLogoutTime(appState.loginAuditId, appState.sessionUserId);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                                        crashlytics.log(e.getMessage());
                                    }
                                }
                            } catch (Exception e) {
                                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                                crashlytics.log(e.getMessage());
                            }
                        } else {
                            dialog.cancel();
                        }
                    }
                }).setPositiveButton((getResources().getString(R.string.m122)),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    /**
     * This method handle touch listners, calls - displayDatePicker()
     */
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (MotionEvent.ACTION_UP == event.getAction()) {
            try {
                switch (v.getId()) {
                    //                    16MAy2021 Bindu - add covid details
                    case R.id.etVisitCovidTestDate:
                        isVisitDate = false;
                        isCoviddetDt = true;
                        assignEditTextAndCallDatePicker(aq.id(R.id.etVisitCovidTestDate).getEditText(), aq.id(R.id.imgclearcovidresultdate).getImageView());
                        break;
                    case R.id.etVisitCovidFirstDoseDate:
                        isVisitDate = false;
                        isCoviddetDt = true;
                        assignEditTextAndCallDatePicker(aq.id(R.id.etVisitCovidFirstDoseDate).getEditText(), aq.id(R.id.imgclearfirstdosedate).getImageView());
                        break;
                    case R.id.etVisitCovidSecondDoseDate:
                        isVisitDate = false;
                        isCoviddetDt = true;
                        assignEditTextAndCallDatePicker(aq.id(R.id.etVisitCovidSecondDoseDate).getEditText(), aq.id(R.id.imgclearsecdosedate).getImageView());
                        break;

                    case R.id.etvisitdate: {
                        isVisitDate = true;
                        isCoviddetDt = false;
                        assignEditTextAndCallDatePicker(aq.id(R.id.etvisitdate).getEditText(), aq.id(R.id.imgcleardate).getImageView());
                        break;
                    }
                    case R.id.etbpdonedate: {
                        isVisitDate = false;
                        isCoviddetDt = false;
                        assignEditTextAndCallDatePicker(aq.id(R.id.etbpdonedate).getEditText(), aq.id(R.id.imgcleardatebp).getImageView());
                        break;
                    }
                    case R.id.etpulsedate: {
                        isVisitDate = false;
                        isCoviddetDt = false;
                        assignEditTextAndCallDatePicker(aq.id(R.id.etpulsedate).getEditText(), aq.id(R.id.imgcleardatepulse).getImageView());
                        break;
                    }
                    case R.id.ethbdonedate: {
                        isVisitDate = false;
                        isCoviddetDt = false;
                        assignEditTextAndCallDatePicker(aq.id(R.id.ethbdonedate).getEditText(), aq.id(R.id.imgcleardatehb).getImageView());
                        break;
                    }
                    case R.id.ettempdonedate: {
                        isVisitDate = false;
                        isCoviddetDt = false;
                        assignEditTextAndCallDatePicker(aq.id(R.id.ettempdonedate).getEditText(), aq.id(R.id.imgcleardatetemp).getImageView());
                        break;
                    }
                    //Baby 1
                    case R.id.etweightdatebaby1: {
                        isVisitDate = false;
                        isCoviddetDt = false;
                        assignEditTextAndCallDatePicker(aq.id(R.id.etweightdatebaby1).getEditText(), aq.id(R.id.imgcleardateweightbaby1).getImageView());
                        break;
                    }
                    case R.id.ettempdonedatebaby1: {
                        isVisitDate = false;
                        isCoviddetDt = false;
                        assignEditTextAndCallDatePicker(aq.id(R.id.ettempdonedatebaby1).getEditText(), aq.id(R.id.imgcleardatetempbaby1).getImageView());
                        break;
                    }
                    case R.id.etrespratedonedatebaby1: {
                        isVisitDate = false;
                        isCoviddetDt = false;
                        assignEditTextAndCallDatePicker(aq.id(R.id.etrespratedonedatebaby1).getEditText(), aq.id(R.id.imgcleardaterespratebaby1).getImageView());
                        break;
                    }
                    case R.id.etchildregdate1: {
                        isVisitDate = false;
                        isCoviddetDt = false;
                        assignEditTextAndCallDatePicker(aq.id(R.id.etchildregdate1).getEditText(), aq.id(R.id.imgclearchildregdate1).getImageView());
                        break;
                    }
                    //Baby 2
                    case R.id.etweightdatebaby2: {
                        isVisitDate = false;
                        isCoviddetDt = false;
                        assignEditTextAndCallDatePicker(aq.id(R.id.etweightdatebaby2).getEditText(), aq.id(R.id.imgcleardateweightbaby2).getImageView());
                        break;
                    }
                    case R.id.ettempdonedatebaby2: {
                        isVisitDate = false;
                        isCoviddetDt = false;
                        assignEditTextAndCallDatePicker(aq.id(R.id.ettempdonedatebaby2).getEditText(), aq.id(R.id.imgcleardatetempbaby2).getImageView());
                        break;
                    }
                    case R.id.etrespratedonedatebaby2: {
                        isVisitDate = false;
                        isCoviddetDt = false;
                        assignEditTextAndCallDatePicker(aq.id(R.id.etrespratedonedatebaby2).getEditText(), aq.id(R.id.imgcleardaterespratebaby2).getImageView());
                        break;
                    }
                    case R.id.etchildregdate2: {
                        isVisitDate = false;
                        isCoviddetDt = false;
                        assignEditTextAndCallDatePicker(aq.id(R.id.etchildregdate2).getEditText(), aq.id(R.id.imgclearchildregdate2).getImageView());
                        break;
                    }
                    //Baby 3
                    case R.id.etweightdatebaby3: {
                        isVisitDate = false;
                        isCoviddetDt = false;
                        assignEditTextAndCallDatePicker(aq.id(R.id.etweightdatebaby3).getEditText(), aq.id(R.id.imgcleardateweightbaby3).getImageView());
                        break;
                    }
                    case R.id.ettempdonedatebaby3: {
                        isVisitDate = false;
                        isCoviddetDt = false;
                        assignEditTextAndCallDatePicker(aq.id(R.id.ettempdonedatebaby3).getEditText(), aq.id(R.id.imgcleardatetempbaby3).getImageView());
                        break;
                    }
                    case R.id.etrespratedonedatebaby3: {
                        isVisitDate = false;
                        isCoviddetDt = false;
                        assignEditTextAndCallDatePicker(aq.id(R.id.etrespratedonedatebaby3).getEditText(), aq.id(R.id.imgcleardaterespratebaby3).getImageView());
                        break;
                    }
                    case R.id.etchildregdate3: {
                        isVisitDate = false;
                        isCoviddetDt = false;
                        assignEditTextAndCallDatePicker(aq.id(R.id.etchildregdate3).getEditText(), aq.id(R.id.imgclearchildregdate3).getImageView());
                        break;
                    }
                    //Baby 4
                    case R.id.etweightdatebaby4: {
                        isVisitDate = false;
                        isCoviddetDt = false;
                        assignEditTextAndCallDatePicker(aq.id(R.id.etweightdatebaby4).getEditText(), aq.id(R.id.imgcleardateweightbaby4).getImageView());
                        break;
                    }
                    case R.id.ettempdonedatebaby4: {
                        isVisitDate = false;
                        isCoviddetDt = false;
                        assignEditTextAndCallDatePicker(aq.id(R.id.ettempdonedatebaby4).getEditText(), aq.id(R.id.imgcleardatetempbaby4).getImageView());
                        break;
                    }
                    case R.id.etrespratedonedatebaby4: {
                        isVisitDate = false;
                        isCoviddetDt = false;
                        assignEditTextAndCallDatePicker(aq.id(R.id.etrespratedonedatebaby4).getEditText(), aq.id(R.id.imgcleardaterespratebaby4).getImageView());
                        break;
                    }
                    case R.id.etchildregdate4: {
                        isVisitDate = false;
                        isCoviddetDt = false;
                        assignEditTextAndCallDatePicker(aq.id(R.id.etchildregdate4).getEditText(), aq.id(R.id.imgclearchildregdate4).getImageView());
                        break;
                    }
                }
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
        }
        return true;
    }

    /**
     * This method opens the Date Dialog and sets Date to respective EditText
     */
    private void assignEditTextAndCallDatePicker(EditText editText, ImageView imageView) {
        currentEditTextViewforDate = editText;
        currentImageView = imageView;
        String currDate = (editText.getText().length() > 0) ? editText.getText().toString() : "";
        CommonDatePickerFragment.getCommonDate(PncHomeVisit.this, currDate);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        if (view.isShown()) {
            try {

                String SelectedDate = String.format("%02d", day) + "-" + String.format("%02d", month + 1) + "-" + year;

                Date adddate = null;
                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                Date seldate = format.parse(SelectedDate);

                if (woman.getRegADDate() != null && woman.getRegADDate().trim().length() > 0)
                    adddate = format.parse(woman.getRegADDate());

                //                16May2021 Bindu add first and sec dose date
                Date firstdosedate = null, secdosedate = null;
                if (aq.id(R.id.etVisitCovidFirstDoseDate).getText().toString().length() > 0)
                    firstdosedate = format.parse(aq.id(R.id.etVisitCovidFirstDoseDate).getText().toString());

                if (aq.id(R.id.etVisitCovidSecondDoseDate).getText().toString().length() > 0)
                    secdosedate = format.parse(aq.id(R.id.etVisitCovidSecondDoseDate).getText().toString());


                if (isVisitDate) {
                    if (seldate.after(new Date())) {
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.date_cannot_af_currentdate), this);
                        aq.id(R.id.etvisitdate).text(DateTimeUtil.getTodaysDate());
                    } else if (adddate != null && seldate.before(adddate)) {

                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.date_cannot_bf_deldate)
                                , this);
                        aq.id(R.id.etvisitdate).text(DateTimeUtil.getTodaysDate());
                    } else {
                        aq.id(R.id.etvisitdate).text(SelectedDate);
                        aq.id(R.id.imgcleardate).visible();
                        calculateDaysfromDel(SelectedDate);
                    }
                } else {
                    if (seldate.after(new Date())) {
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.date_cannot_af_currentdate), this);
                        currentEditTextViewforDate.setText("");
                        currentEditTextViewforDate.requestFocus();
                        currentEditTextViewforDate.setError("Error");
                        aq.id(currentImageView).gone();
                    } else if (currentEditTextViewforDate == aq.id(R.id.etVisitCovidFirstDoseDate).getEditText()) {
                        if (secdosedate != null && seldate.after(secdosedate)) { // first dose cant be after sec dose
                            AlertDialogUtil.displayAlertMessage(
                                    getResources().getString(R.string.datecantbeaftersecdosedate)
                                    , this);
                            currentEditTextViewforDate.setText("");
                            currentEditTextViewforDate.requestFocus();
                            currentEditTextViewforDate.setError("Error");
                            aq.id(currentImageView).gone();
                        } else {
                            currentEditTextViewforDate.setText(SelectedDate);
                            aq.id(currentImageView).visible();
                            currentEditTextViewforDate.setError(null);
                        }
                    } else if (currentEditTextViewforDate == aq.id(R.id.etVisitCovidSecondDoseDate).getEditText()) {
                        if (firstdosedate != null && seldate.before(firstdosedate)) { // sec dose cant be bef first dose
                            AlertDialogUtil.displayAlertMessage(
                                    getResources().getString(R.string.datecantbebefrefirstdosedate)
                                    , this);
                            currentEditTextViewforDate.setText("");
                            currentEditTextViewforDate.requestFocus();
                            currentEditTextViewforDate.setError("Error");
                            aq.id(currentImageView).gone();
                        } else if (firstdosedate != null && seldate.equals(firstdosedate)) { // sec dose cant be equals first dose
                            AlertDialogUtil.displayAlertMessage(
                                    getResources().getString(R.string.seconddosedatecannotbeequaltofirstdose)
                                    , this);
                            currentEditTextViewforDate.setText("");
                            currentEditTextViewforDate.requestFocus();
                            currentEditTextViewforDate.setError("Error");
                            aq.id(currentImageView).gone();
                        } else {
                            currentEditTextViewforDate.setText(SelectedDate);
                            aq.id(currentImageView).visible();
                            currentEditTextViewforDate.setError(null);
                        }
                    } else if (adddate != null && seldate.before(adddate) && !isCoviddetDt) { //17May2021 Bindu set covid test date
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.date_cannot_bf_deldate)
                                , this);
                        currentEditTextViewforDate.setText("");
                        currentEditTextViewforDate.requestFocus();
                        currentEditTextViewforDate.setError("Error");
                        aq.id(currentImageView).gone();
                    } else {
                        currentEditTextViewforDate.setText(SelectedDate);
                        aq.id(currentImageView).visible();
                        currentEditTextViewforDate.setError(null);
                    }
                }
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
        }
    }

    private boolean validatePNCHomevisit() {

        //        16May2021 Bindu check for covid vaccine
        if (!(isMotherMortality || isWomanDeactivated)) { //21May2021 Bindu check active

                //mani 23Aug2021
            if (!(aq.id(R.id.ashavailableYes).isChecked() || aq.id(R.id.ashavailableNo).isChecked() ||  aq.id(R.id.ashavailableDontKnow).isChecked())) {
                AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plselectavailabilityasha), act);
                return false;
            }

            if (!(aq.id(R.id.rd_CovidVaccineYes).isChecked() || aq.id(R.id.rd_CovidVaccineNo).isChecked())) {
                AlertDialogUtil.displayAlertMessage(
                        getResources().getString(R.string.plsentercovidvaccinedetails)
                        , act);
                opencloseAccordion(aq.id(R.id.llcovidvaccinedetails).getView(), aq.id(R.id.txtcovidvaccine).getTextView());
                aq.id(R.id.llcovidvaccinedetails).backgroundColor(getResources().getColor(R.color.lightgray));
                return false;
            }

            if (aq.id(R.id.rd_CovidVaccineYes).isChecked()) {
                if (aq.id(R.id.spnVisitVaccineDose).getSelectedItemPosition() == 0 && !isvaccinated) {
                    AlertDialogUtil.displayAlertMessage(
                            getResources().getString(R.string.plsselectdosecompleted)
                            , act);
                    return false;
                } else if (aq.id(R.id.spnVisitVaccineDose).getSelectedItemPosition() == 1) {
                    if (aq.id(R.id.etVisitCovidFirstDoseDate).getText().toString().trim().length() <= 0) {
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.plsselectfirstdosedate)
                                , act);
                        return false;
                    }
                } else if (aq.id(R.id.spnVisitVaccineDose).getSelectedItemPosition() == 2) {
                    if (aq.id(R.id.etVisitCovidFirstDoseDate).getText().toString().trim().length() <= 0) {
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.plsselectfirstdosedate)
                                , act);
                        return false;
                    }
                    if (aq.id(R.id.etVisitCovidSecondDoseDate).getText().toString().trim().length() <= 0) {
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.plsselectsecdosedate)
                                , act);
                        return false;
                    }
                }
            }
        }

        if (aq.id(R.id.etbpsystolicsumm).getText().toString().trim().length() > 0 && aq.id(R.id.etbpdiastolicsumm).getText().toString().trim().length() <= 0) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.enter_diastolic), act);
            aq.id(R.id.etbpdiastolicsumm).getEditText().requestFocus();
            return false;
        } else if (aq.id(R.id.etbpsystolicsumm).getText().toString().trim().length() <= 0 && aq.id(R.id.etbpdiastolicsumm).getText().toString().trim().length() > 0) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.enter_systolic), act);
            aq.id(R.id.etbpsystolicsumm).getEditText().requestFocus();
            return false;
        }

        /*//mother danger signs
        if(aq.id(R.id.chkothersymptomsmother).isChecked()) {
            if(aq.id(R.id.etothersymptomsmother).getText().toString().trim().length() <= 0) {
                AlertDialogUtil.displayAlertMessage(
                        getResources().getString(R.string.plsenterother), act);
                aq.id(R.id.etothersymptomsmother).getEditText().requestFocus();
                aq.id(R.id.txtselectedbtnheading).text(getResources().getString(R.string.motherdangersigns));
                aq.id(R.id.btnmotherdangersigns).getImageView().setBackgroundColor(getResources().getColor(R.color.gray));
                aq.id(R.id.btnnewborndangersigns).getImageView().setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                aq.id(R.id.scrmotherdangersigns).visible();
                aq.id(R.id.scrhomevisitsummary).gone();
                aq.id(R.id.scrnewborndangersigns).gone();
                return false;
            }
        }*/

       /* //New born danger signs
        if(!setBabySelectionChecked(aq.id(R.id.chknbsmallbabylbw).getCheckBox(), aq.id(R.id.chknbsmallbabylbw1).getCheckBox(),aq.id(R.id.chknbsmallbabylbw2).getCheckBox(),aq.id(R.id.chknbsmallbabylbw3).getCheckBox(),aq.id(R.id.chknbsmallbabylbw4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            //aq.id(R.id.chknbsmallbabylbw).getCheckBox().setError(getResources().getString(R.string.plsselectbaby));
            return false;
        }

        if(!setBabySelectionChecked(aq.id(R.id.chknbnomeconiumpassed).getCheckBox(), aq.id(R.id.chknbnomeconiumpassed1).getCheckBox(),aq.id(R.id.chknbnomeconiumpassed2).getCheckBox(),aq.id(R.id.chknbnomeconiumpassed3).getCheckBox(),aq.id(R.id.chknbnomeconiumpassed4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        if(!setBabySelectionChecked(aq.id(R.id.chknbnourinepassed).getCheckBox(), aq.id(R.id.chknbnourinepassed1).getCheckBox(),aq.id(R.id.chknbnourinepassed2).getCheckBox(),aq.id(R.id.chknbnourinepassed3).getCheckBox(),aq.id(R.id.chknbnourinepassed4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        if(!setBabySelectionChecked(aq.id(R.id.chknbinabilitytosuck).getCheckBox(), aq.id(R.id.chknbinabilitytosuck1).getCheckBox(),aq.id(R.id.chknbinabilitytosuck2).getCheckBox(),aq.id(R.id.chknbinabilitytosuck3).getCheckBox(),aq.id(R.id.chknbinabilitytosuck4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        if(!setBabySelectionChecked(aq.id(R.id.chknbfastbreathing).getCheckBox(), aq.id(R.id.chknbfastbreathing1).getCheckBox(),aq.id(R.id.chknbfastbreathing2).getCheckBox(),aq.id(R.id.chknbfastbreathing3).getCheckBox(),aq.id(R.id.chknbfastbreathing4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        if(!setBabySelectionChecked(aq.id(R.id.chknbinabilitytopassurineandstool).getCheckBox(), aq.id(R.id.chknbinabilitytopassurineandstool1).getCheckBox(),aq.id(R.id.chknbinabilitytopassurineandstool2).getCheckBox(),aq.id(R.id.chknbinabilitytopassurineandstool3).getCheckBox(),aq.id(R.id.chknbinabilitytopassurineandstool4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        if(!setBabySelectionChecked(aq.id(R.id.chknbumbilicalstumpredorpus).getCheckBox(), aq.id(R.id.chknbumbilicalstumpredorpus1).getCheckBox(),aq.id(R.id.chknbumbilicalstumpredorpus2).getCheckBox(),aq.id(R.id.chknbumbilicalstumpredorpus3).getCheckBox(),aq.id(R.id.chknbumbilicalstumpredorpus4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        if(!setBabySelectionChecked(aq.id(R.id.chknbpustules).getCheckBox(), aq.id(R.id.chknbpustules1).getCheckBox(),aq.id(R.id.chknbpustules2).getCheckBox(),aq.id(R.id.chknbpustules3).getCheckBox(),aq.id(R.id.chknbpustules4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        if(!setBabySelectionChecked(aq.id(R.id.chknbjaundice).getCheckBox(), aq.id(R.id.chknbjaundice1).getCheckBox(),aq.id(R.id.chknbjaundice2).getCheckBox(),aq.id(R.id.chknbjaundice3).getCheckBox(),aq.id(R.id.chknbjaundice4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        if(!setBabySelectionChecked(aq.id(R.id.chknbfever).getCheckBox(), aq.id(R.id.chknbfever1).getCheckBox(),aq.id(R.id.chknbfever2).getCheckBox(),aq.id(R.id.chknbfever3).getCheckBox(),aq.id(R.id.chknbfever4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        if(!setBabySelectionChecked(aq.id(R.id.chknbdiarrhoea).getCheckBox(), aq.id(R.id.chknbdiarrhoea1).getCheckBox(),aq.id(R.id.chknbdiarrhoea2).getCheckBox(),aq.id(R.id.chknbdiarrhoea3).getCheckBox(),aq.id(R.id.chknbdiarrhoea4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        if(!setBabySelectionChecked(aq.id(R.id.chknbbloodinstools).getCheckBox(), aq.id(R.id.chknbbloodinstools1).getCheckBox(),aq.id(R.id.chknbbloodinstools2).getCheckBox(),aq.id(R.id.chknbbloodinstools3).getCheckBox(),aq.id(R.id.chknbbloodinstools4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        if(!setBabySelectionChecked(aq.id(R.id.chknblethargicbaby).getCheckBox(), aq.id(R.id.chknblethargicbaby1).getCheckBox(),aq.id(R.id.chknblethargicbaby2).getCheckBox(),aq.id(R.id.chknblethargicbaby3).getCheckBox(),aq.id(R.id.chknblethargicbaby4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        //09Dec2019 - Bindu
        if(!setBabySelectionChecked(aq.id(R.id.chknbfitsbaby).getCheckBox(), aq.id(R.id.chknbfitsbaby1).getCheckBox(),aq.id(R.id.chknbfitsbaby2).getCheckBox(),aq.id(R.id.chknbfitsbaby3).getCheckBox(),aq.id(R.id.chknbfitsbaby4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        if(!setBabySelectionChecked(aq.id(R.id.chknbeyesareredbaby).getCheckBox(), aq.id(R.id.chknbeyesareredbaby1).getCheckBox(),aq.id(R.id.chknbeyesareredbaby2).getCheckBox(),aq.id(R.id.chknbeyesareredbaby3).getCheckBox(),aq.id(R.id.chknbeyesareredbaby4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        if(!setBabySelectionChecked(aq.id(R.id.chknbpallorofpalmsbaby).getCheckBox(), aq.id(R.id.chknbpallorofpalmsbaby1).getCheckBox(),aq.id(R.id.chknbpallorofpalmsbaby2).getCheckBox(),aq.id(R.id.chknbpallorofpalmsbaby3).getCheckBox(),aq.id(R.id.chknbpallorofpalmsbaby4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        if(!setBabySelectionChecked(aq.id(R.id.chknbyellowpalmsbaby).getCheckBox(), aq.id(R.id.chknbyellowpalmsbaby1).getCheckBox(),aq.id(R.id.chknbyellowpalmsbaby2).getCheckBox(),aq.id(R.id.chknbyellowpalmsbaby3).getCheckBox(),aq.id(R.id.chknbyellowpalmsbaby4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        if(!setBabySelectionChecked(aq.id(R.id.chknbbluepalmsbaby).getCheckBox(), aq.id(R.id.chknbbluepalmsbaby1).getCheckBox(),aq.id(R.id.chknbbluepalmsbaby2).getCheckBox(),aq.id(R.id.chknbbluepalmsbaby3).getCheckBox(),aq.id(R.id.chknbbluepalmsbaby4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        if(!setBabySelectionChecked(aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby1).getCheckBox(),aq.id(R.id.chknbfeelscoldorhotbaby2).getCheckBox(),aq.id(R.id.chknbfeelscoldorhotbaby3).getCheckBox(),aq.id(R.id.chknbfeelscoldorhotbaby4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        if(!setBabySelectionChecked(aq.id(R.id.chknbbleedingbaby).getCheckBox(), aq.id(R.id.chknbbleedingbaby1).getCheckBox(),aq.id(R.id.chknbbleedingbaby2).getCheckBox(),aq.id(R.id.chknbbleedingbaby3).getCheckBox(),aq.id(R.id.chknbbleedingbaby4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        if(!setBabySelectionChecked(aq.id(R.id.chknbabdominaldistensionbaby).getCheckBox(), aq.id(R.id.chknbabdominaldistensionbaby1).getCheckBox(),aq.id(R.id.chknbabdominaldistensionbaby2).getCheckBox(),aq.id(R.id.chknbabdominaldistensionbaby3).getCheckBox(),aq.id(R.id.chknbabdominaldistensionbaby4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        if(!setBabySelectionChecked(aq.id(R.id.chknbanybirthdefectbaby).getCheckBox(), aq.id(R.id.chknbanybirthdefectbaby1).getCheckBox(),aq.id(R.id.chknbanybirthdefectbaby2).getCheckBox(),aq.id(R.id.chknbanybirthdefectbaby3).getCheckBox(),aq.id(R.id.chknbanybirthdefectbaby4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }


        if(aq.id(R.id.chkothersymptomsnb).isChecked()) {
            if(aq.id(R.id.etothersymptomsnb).getText().toString().trim().length() <= 0) {
                AlertDialogUtil.displayAlertMessage(
                        getResources().getString(R.string.plsenterother), act);
                aq.id(R.id.etothersymptomsnb).getEditText().requestFocus();
                aq.id(R.id.txtselectedbtnheading).text(getResources().getString(R.string.newborndangersigns));
                aq.id(R.id.btnmotherdangersigns).getImageView().setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                aq.id(R.id.btnnewborndangersigns).getImageView().setBackgroundColor(getResources().getColor(R.color.gray));
                aq.id(R.id.scrmotherdangersigns).gone();
                aq.id(R.id.scrhomevisitsummary).gone();
                aq.id(R.id.scrnewborndangersigns).visible();
                return false;
            }
        }*/

        if (aq.id(R.id.chkisreferred).isChecked()) {
            //08Apr2021 Bindu add ref fac type and ref slip num validation
            if (aq.id(R.id.spnfactype).getSelectedItemPosition() == 0) {
                AlertDialogUtil.displayAlertMessage(
                        getResources().getString(R.string.plsselreffactype)
                        , act);
                TextView errorText = (TextView) aq.id(R.id.spnfactype).getSpinner().getSelectedView();
                errorText.setError("");
                errorText.setTextColor(Color.RED);
                errorText.setText(getResources().getString(R.string.plsselreffactype));
                errorText.setFocusable(true);
                return false;
            }

            if (aq.id(R.id.spnfacnameHv).getSelectedItemPosition() == 0) {
                AlertDialogUtil.displayAlertMessage(
                        getResources().getString(R.string.plsselreffac)
                        , act);
                TextView errorText = (TextView) aq.id(R.id.spnfacnameHv).getSpinner().getSelectedView();
                errorText.setError("");
                errorText.setTextColor(Color.RED);
                errorText.setText(getResources().getString(R.string.plsselreffac));
                errorText.setFocusable(true);
                return false;
            }

            if (aq.id(R.id.etrefslipnumber).getText().toString().trim().length() <= 0) {
                AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsenterrefslipnum), act);
                aq.id(R.id.etrefslipnumber).getEditText().requestFocus();
                return false;
            }
        }

        //Baby reg date at gov
        if (aq.id(R.id.chkischildregatfacchild1).isChecked()) {
            if (aq.id(R.id.etchildregid1).getText().toString().trim().length() <= 0) {
                AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsenterchildregid), act);
                aq.id(R.id.etchildregid1).getEditText().requestFocus();
                return false;
            } else if (aq.id(R.id.etchildregdate1).getText().toString().trim().length() <= 0) {
                AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsenterchildregdate), act);
                aq.id(R.id.etchildregid1).getEditText().requestFocus();
                aq.id(R.id.etchildregdate1).getEditText().requestFocus();
                return false;
            }
        }

        if (aq.id(R.id.chkischildregatfacchild2).isChecked()) {
            if (aq.id(R.id.etchildregid2).getText().toString().trim().length() <= 0) {
                AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsenterchildregid), act);
                aq.id(R.id.etchildregid2).getEditText().requestFocus();
                return false;
            } else if (aq.id(R.id.etchildregdate2).getText().toString().trim().length() <= 0) {
                AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsenterchildregdate), act);
                aq.id(R.id.etchildregid2).getEditText().requestFocus();
                aq.id(R.id.etchildregdate2).getEditText().requestFocus();
                return false;
            }
        }

        if (aq.id(R.id.chkischildregatfacchild3).isChecked()) {
            if (aq.id(R.id.etchildregid3).getText().toString().trim().length() <= 0) {
                AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsenterchildregid), act);
                aq.id(R.id.etchildregid3).getEditText().requestFocus();
                return false;
            } else if (aq.id(R.id.etchildregdate3).getText().toString().trim().length() <= 0) {
                AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsenterchildregdate), act);
                aq.id(R.id.etchildregid3).getEditText().requestFocus();
                aq.id(R.id.etchildregdate3).getEditText().requestFocus();
                return false;
            }
        }
        if (aq.id(R.id.chkischildregatfacchild4).isChecked()) {
            if (aq.id(R.id.etchildregid4).getText().toString().trim().length() <= 0) {
                AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsenterchildregid), act);
                aq.id(R.id.etchildregid4).getEditText().requestFocus();
                return false;
            } else if (aq.id(R.id.etchildregdate4).getText().toString().trim().length() <= 0) {
                AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsenterchildregdate), act);
                aq.id(R.id.etchildregid4).getEditText().requestFocus();
                aq.id(R.id.etchildregdate4).getEditText().requestFocus();
                return false;
            }
        }

        return true;
    }

    //10Dec2019 - Bindu- Validate Child check
    private boolean validateChildCheck() {

        //mother danger signs
        if (aq.id(R.id.chkothersymptomsmother).isChecked()) {
            if (aq.id(R.id.etothersymptomsmother).getText().toString().trim().length() <= 0) {
                AlertDialogUtil.displayAlertMessage(
                        getResources().getString(R.string.plsenterother), act);
                aq.id(R.id.etothersymptomsmother).getEditText().requestFocus();
                aq.id(R.id.txtselectedbtnheading).text(getResources().getString(R.string.motherdangersigns));
                aq.id(R.id.btnmotherdangersigns).getImageView().setBackgroundColor(getResources().getColor(R.color.gray));
                aq.id(R.id.btnnewborndangersigns).getImageView().setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                aq.id(R.id.scrmotherdangersigns).visible();
                aq.id(R.id.scrhomevisitsummary).gone();
                aq.id(R.id.imgSummaryAddPhoto).gone();
                aq.id(R.id.scrnewborndangersigns).gone();
                return false;
            }
        }

        //New born danger signs
        if (!setBabySelectionChecked(aq.id(R.id.chknbsmallbabylbw).getCheckBox(), aq.id(R.id.chknbsmallbabylbw1).getCheckBox(), aq.id(R.id.chknbsmallbabylbw2).getCheckBox(), aq.id(R.id.chknbsmallbabylbw3).getCheckBox(), aq.id(R.id.chknbsmallbabylbw4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            //aq.id(R.id.chknbsmallbabylbw).getCheckBox().setError(getResources().getString(R.string.plsselectbaby));
            return false;
        }

        if (!setBabySelectionChecked(aq.id(R.id.chknbnomeconiumpassed).getCheckBox(), aq.id(R.id.chknbnomeconiumpassed1).getCheckBox(), aq.id(R.id.chknbnomeconiumpassed2).getCheckBox(), aq.id(R.id.chknbnomeconiumpassed3).getCheckBox(), aq.id(R.id.chknbnomeconiumpassed4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        if (!setBabySelectionChecked(aq.id(R.id.chknbnourinepassed).getCheckBox(), aq.id(R.id.chknbnourinepassed1).getCheckBox(), aq.id(R.id.chknbnourinepassed2).getCheckBox(), aq.id(R.id.chknbnourinepassed3).getCheckBox(), aq.id(R.id.chknbnourinepassed4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        if (!setBabySelectionChecked(aq.id(R.id.chknbinabilitytosuck).getCheckBox(), aq.id(R.id.chknbinabilitytosuck1).getCheckBox(), aq.id(R.id.chknbinabilitytosuck2).getCheckBox(), aq.id(R.id.chknbinabilitytosuck3).getCheckBox(), aq.id(R.id.chknbinabilitytosuck4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        if (!setBabySelectionChecked(aq.id(R.id.chknbfastbreathing).getCheckBox(), aq.id(R.id.chknbfastbreathing1).getCheckBox(), aq.id(R.id.chknbfastbreathing2).getCheckBox(), aq.id(R.id.chknbfastbreathing3).getCheckBox(), aq.id(R.id.chknbfastbreathing4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        if (!setBabySelectionChecked(aq.id(R.id.chknbinabilitytopassurineandstool).getCheckBox(), aq.id(R.id.chknbinabilitytopassurineandstool1).getCheckBox(), aq.id(R.id.chknbinabilitytopassurineandstool2).getCheckBox(), aq.id(R.id.chknbinabilitytopassurineandstool3).getCheckBox(), aq.id(R.id.chknbinabilitytopassurineandstool4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        if (!setBabySelectionChecked(aq.id(R.id.chknbumbilicalstumpredorpus).getCheckBox(), aq.id(R.id.chknbumbilicalstumpredorpus1).getCheckBox(), aq.id(R.id.chknbumbilicalstumpredorpus2).getCheckBox(), aq.id(R.id.chknbumbilicalstumpredorpus3).getCheckBox(), aq.id(R.id.chknbumbilicalstumpredorpus4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        if (!setBabySelectionChecked(aq.id(R.id.chknbpustules).getCheckBox(), aq.id(R.id.chknbpustules1).getCheckBox(), aq.id(R.id.chknbpustules2).getCheckBox(), aq.id(R.id.chknbpustules3).getCheckBox(), aq.id(R.id.chknbpustules4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        if (!setBabySelectionChecked(aq.id(R.id.chknbjaundice).getCheckBox(), aq.id(R.id.chknbjaundice1).getCheckBox(), aq.id(R.id.chknbjaundice2).getCheckBox(), aq.id(R.id.chknbjaundice3).getCheckBox(), aq.id(R.id.chknbjaundice4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        if (!setBabySelectionChecked(aq.id(R.id.chknbfever).getCheckBox(), aq.id(R.id.chknbfever1).getCheckBox(), aq.id(R.id.chknbfever2).getCheckBox(), aq.id(R.id.chknbfever3).getCheckBox(), aq.id(R.id.chknbfever4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

//        11May2021 Bindu add sunkenfont, vomiting and sepsis
        if (!setBabySelectionChecked(aq.id(R.id.chknbsunkenfont).getCheckBox(), aq.id(R.id.chknbsunkenfont1).getCheckBox(), aq.id(R.id.chknbsunkenfont2).getCheckBox(), aq.id(R.id.chknbsunkenfont3).getCheckBox(), aq.id(R.id.chknbsunkenfont4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }
        if (!setBabySelectionChecked(aq.id(R.id.chknbvomiting).getCheckBox(), aq.id(R.id.chknbvomiting1).getCheckBox(), aq.id(R.id.chknbvomiting2).getCheckBox(), aq.id(R.id.chknbvomiting3).getCheckBox(), aq.id(R.id.chknbvomiting4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }
        if (!setBabySelectionChecked(aq.id(R.id.chknbsepsis).getCheckBox(), aq.id(R.id.chknbsepsis1).getCheckBox(), aq.id(R.id.chknbsepsis2).getCheckBox(), aq.id(R.id.chknbsepsis3).getCheckBox(), aq.id(R.id.chknbsepsis4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        if (!setBabySelectionChecked(aq.id(R.id.chknbdiarrhoea).getCheckBox(), aq.id(R.id.chknbdiarrhoea1).getCheckBox(), aq.id(R.id.chknbdiarrhoea2).getCheckBox(), aq.id(R.id.chknbdiarrhoea3).getCheckBox(), aq.id(R.id.chknbdiarrhoea4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        //        14May2021 Bindu add pneumonia
        if (!setBabySelectionChecked(aq.id(R.id.chknbpneumonia).getCheckBox(), aq.id(R.id.chknbpneumonia1).getCheckBox(), aq.id(R.id.chknbpneumonia2).getCheckBox(), aq.id(R.id.chknbpneumonia3).getCheckBox(), aq.id(R.id.chknbpneumonia4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        if (!setBabySelectionChecked(aq.id(R.id.chknbbloodinstools).getCheckBox(), aq.id(R.id.chknbbloodinstools1).getCheckBox(), aq.id(R.id.chknbbloodinstools2).getCheckBox(), aq.id(R.id.chknbbloodinstools3).getCheckBox(), aq.id(R.id.chknbbloodinstools4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        if (!setBabySelectionChecked(aq.id(R.id.chknblethargicbaby).getCheckBox(), aq.id(R.id.chknblethargicbaby1).getCheckBox(), aq.id(R.id.chknblethargicbaby2).getCheckBox(), aq.id(R.id.chknblethargicbaby3).getCheckBox(), aq.id(R.id.chknblethargicbaby4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        //09Dec2019 - Bindu
        if (!setBabySelectionChecked(aq.id(R.id.chknbfitsbaby).getCheckBox(), aq.id(R.id.chknbfitsbaby1).getCheckBox(), aq.id(R.id.chknbfitsbaby2).getCheckBox(), aq.id(R.id.chknbfitsbaby3).getCheckBox(), aq.id(R.id.chknbfitsbaby4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        if (!setBabySelectionChecked(aq.id(R.id.chknbeyesareredbaby).getCheckBox(), aq.id(R.id.chknbeyesareredbaby1).getCheckBox(), aq.id(R.id.chknbeyesareredbaby2).getCheckBox(), aq.id(R.id.chknbeyesareredbaby3).getCheckBox(), aq.id(R.id.chknbeyesareredbaby4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        if (!setBabySelectionChecked(aq.id(R.id.chknbpallorofpalmsbaby).getCheckBox(), aq.id(R.id.chknbpallorofpalmsbaby1).getCheckBox(), aq.id(R.id.chknbpallorofpalmsbaby2).getCheckBox(), aq.id(R.id.chknbpallorofpalmsbaby3).getCheckBox(), aq.id(R.id.chknbpallorofpalmsbaby4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        if (!setBabySelectionChecked(aq.id(R.id.chknbyellowpalmsbaby).getCheckBox(), aq.id(R.id.chknbyellowpalmsbaby1).getCheckBox(), aq.id(R.id.chknbyellowpalmsbaby2).getCheckBox(), aq.id(R.id.chknbyellowpalmsbaby3).getCheckBox(), aq.id(R.id.chknbyellowpalmsbaby4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        if (!setBabySelectionChecked(aq.id(R.id.chknbbluepalmsbaby).getCheckBox(), aq.id(R.id.chknbbluepalmsbaby1).getCheckBox(), aq.id(R.id.chknbbluepalmsbaby2).getCheckBox(), aq.id(R.id.chknbbluepalmsbaby3).getCheckBox(), aq.id(R.id.chknbbluepalmsbaby4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        if (!setBabySelectionChecked(aq.id(R.id.chknbfeelscoldorhotbaby).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby1).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby2).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby3).getCheckBox(), aq.id(R.id.chknbfeelscoldorhotbaby4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

//        Manikanta10feb2021 added new category
        if (!setBabySelectionChecked(aq.id(R.id.chknbfeelswarmbaby).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby1).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby2).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby3).getCheckBox(), aq.id(R.id.chknbfeelswarmbaby4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

//        Manikanta10feb2021 added new category
        if (!setBabySelectionChecked(aq.id(R.id.chknbfeelstoowarmbaby).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby1).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby2).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby3).getCheckBox(), aq.id(R.id.chknbfeelstoowarmbaby4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        if (!setBabySelectionChecked(aq.id(R.id.chknbbleedingbaby).getCheckBox(), aq.id(R.id.chknbbleedingbaby1).getCheckBox(), aq.id(R.id.chknbbleedingbaby2).getCheckBox(), aq.id(R.id.chknbbleedingbaby3).getCheckBox(), aq.id(R.id.chknbbleedingbaby4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        if (!setBabySelectionChecked(aq.id(R.id.chknbabdominaldistensionbaby).getCheckBox(), aq.id(R.id.chknbabdominaldistensionbaby1).getCheckBox(), aq.id(R.id.chknbabdominaldistensionbaby2).getCheckBox(), aq.id(R.id.chknbabdominaldistensionbaby3).getCheckBox(), aq.id(R.id.chknbabdominaldistensionbaby4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }

        if (!setBabySelectionChecked(aq.id(R.id.chknbanybirthdefectbaby).getCheckBox(), aq.id(R.id.chknbanybirthdefectbaby1).getCheckBox(), aq.id(R.id.chknbanybirthdefectbaby2).getCheckBox(), aq.id(R.id.chknbanybirthdefectbaby3).getCheckBox(), aq.id(R.id.chknbanybirthdefectbaby4).getCheckBox())) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectbaby), act);
            return false;
        }


        if (aq.id(R.id.chkothersymptomsnb).isChecked()) {
            if (aq.id(R.id.etothersymptomsnb).getText().toString().trim().length() <= 0) {
                AlertDialogUtil.displayAlertMessage(
                        getResources().getString(R.string.plsenterother), act);
                aq.id(R.id.etothersymptomsnb).getEditText().requestFocus();
                aq.id(R.id.txtselectedbtnheading).text(getResources().getString(R.string.newborndangersigns));
                aq.id(R.id.btnmotherdangersigns).getImageView().setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                aq.id(R.id.btnnewborndangersigns).getImageView().setBackgroundColor(getResources().getColor(R.color.gray));
                aq.id(R.id.scrmotherdangersigns).gone();
                aq.id(R.id.scrhomevisitsummary).gone();
                aq.id(R.id.imgSummaryAddPhoto).gone();
                aq.id(R.id.scrnewborndangersigns).visible();
                return false;
            }
        }
        return true;
    }

    private boolean setBabySelectionChecked(CheckBox chk, CheckBox chkbaby1, CheckBox chkbaby2, CheckBox chkbaby3, CheckBox chkbaby4) {
        if (chk.isChecked()) {
            if (!(chkbaby1.isChecked() || chkbaby2.isChecked() || chkbaby3.isChecked() || chkbaby4.isChecked())) {
                chk.setError(getResources().getString(R.string.plsselectbaby));
                return false;
            } else
                chk.setError(null);
        } else
            chk.setError(null); //26Apr2021 Bindu set null
        return true;
    }

    private void setSymptomsSummary() throws Exception {
        cemocCnt = 0;
        bemocCnt = 0;
        setMotherSymptoms();
        setNbSymptoms();

        aq.id(R.id.txtDangSigns).text("");
        aq.id(R.id.txtDangSignsE).text(""); //05Dec2019 - Bindu
        if (isMotherMortality || isWomanDeactivated) {   //01Dec2019 - Bindu - check for mother mortality -pending - deactivate
            aq.id(R.id.txtexaminationsmother).gone();
            aq.id(R.id.tblexaminationsmother).gone();
        } else {
            if (arrLSymptomsMother.size() > 0) {
                aq.id(R.id.llsummsymtoms).visible();
                aq.id(R.id.txtDangSigns).visible();
                aq.id(R.id.txtmotherdangersignsL).visible();
                String strMSymptoms[] = (String[]) arrLSymptomsMother.toArray(new String[arrLSymptomsMother.size()]);
                for (int j = 0; j < strMSymptoms.length; j++) {
                    if (j == 0)
                        aq.id(R.id.txtDangSigns).getTextView().append(" * " + strMSymptoms[j]);
                    else
                        aq.id(R.id.txtDangSigns).getTextView().append(strMSymptoms[j]);
                    if (j + 1 != arrLSymptomsMother.size())
                        aq.id(R.id.txtDangSigns).getTextView().append("\n * ");

                }
                //05Dec2019 - Bindu
                String strMSymptomssave[] = (String[]) arrLSymptomsMotherSave.toArray(new String[arrLSymptomsMotherSave.size()]);
                for (int j = 0; j < strMSymptomssave.length; j++) {
                    if (j == 0)
                        aq.id(R.id.txtDangSignsE).getTextView().append(" * " + strMSymptomssave[j]);
                    else
                        aq.id(R.id.txtDangSignsE).getTextView().append(strMSymptomssave[j]);
                    if (j + 1 != arrLSymptomsMotherSave.size())
                        aq.id(R.id.txtDangSignsE).getTextView().append("\n * ");

                }
            } else {
                if (pncNbs.isEmpty())
                    aq.id(R.id.llsummsymtoms).gone();
                aq.id(R.id.txtDangSigns).gone();
                aq.id(R.id.txtmotherdangersignsL).gone();
            }
        }


        //New born danger signs
        aq.id(R.id.txtDangSignsNb).text("");
        aq.id(R.id.txtDangSignsNbE).text(""); //05Dec2019 - Bindu
        if (!pncNbs.isEmpty()) {
            aq.id(R.id.llsummsymtoms).visible();
            aq.id(R.id.txtDangSignsNb).visible();
            aq.id(R.id.txtnbdangersignsL).visible();
            StringBuilder s = new StringBuilder();
            for (PNCPojoNb p : pncNbs) {
                s.append(p).append("\n ");
            }
            //            13Aug2021 Arpitha
            String sp = s.toString();
            sp = sp.replace(getResources().getString(R.string.nbanybirthdefects),getResources().getString(R.string.nbanybirthdefectsdisp));
            aq.id(R.id.txtDangSignsNb).getTextView().append(sp.toString().trim());

            //05Dec2019 - Bindu
            StringBuilder s1 = new StringBuilder();
            for (PNCPojoNb p1 : pncNbssave) {
                s1.append(p1).append("\n ");
            }
            aq.id(R.id.txtDangSignsNbE).getTextView().append(s1.toString().trim());

        } else {
            if (arrLSymptomsMother.size() <= 0)
                aq.id(R.id.llsummsymtoms).gone();
            aq.id(R.id.txtDangSignsNb).gone();
            aq.id(R.id.txtnbdangersignsL).gone();
        }

        /*if (cemocCnt > 0) {
            aq.id(R.id.txtapprefferalcentre).text(getResources().getString(R.string.refertocemoc));
            aq.id(R.id.txtapprefferalcentre).visible();
            startAnimation();
            setFacilityNames(getResources().getString(R.string.strcemoc));
        } else if (bemocCnt > 0) {
            aq.id(R.id.txtapprefferalcentre).text(getResources().getString(R.string.refertobemoc));
            aq.id(R.id.txtapprefferalcentre).visible();
            startAnimation();
            setFacilityNames("phc");
        } */ //08Apr2021 Bindu -
        if (cemocCnt > 0 || bemocCnt > 0) {
            aq.id(R.id.txtapprefferalcentre).text(getResources().getString(R.string.refer));
            aq.id(R.id.txtapprefferalcentre).visible();
            startAnimation();
            setFacilityType();
        } else {
            aq.id(R.id.txtapprefferalcentre).text("");
            aq.id(R.id.txtapprefferalcentre).gone();
            aq.id(R.id.chkisreferred).checked(false);
            aq.id(R.id.spnfacnameHv).setSelection(0);
            aq.id(R.id.etfacname).text("");
            aq.id(R.id.trspnfacname).gone();
            aq.id(R.id.tretfacname).gone();
            // setFacilityNames("");
            //08Apr2021 Bindu
            aq.id(R.id.spnfactype).setSelection(0);
            aq.id(R.id.trspnfactype).gone();
            aq.id(R.id.trrefslipno).gone();
            aq.id(R.id.etrefslipnumber).text("");

            setRefFacilityName("");
        }
        setAdvise();
    }

    private void setAdvise() {
        if (cemocCnt > 0) {
            aq.id(R.id.llsummadvise).visible();
            aq.id(R.id.txtactionsumm).visible();
            aq.id(R.id.llreferralinfo).visible();
            aq.id(R.id.txtactionsumm).text("ACTION -  " + getResources().getString(R.string.refertocemoc));
        } else if (bemocCnt > 0) {
            aq.id(R.id.llsummadvise).visible();
            aq.id(R.id.txtactionsumm).visible();
            aq.id(R.id.llreferralinfo).visible();
            aq.id(R.id.txtactionsumm).text("ACTION -  " + getResources().getString(R.string.refertobemoc));
        } else {
            aq.id(R.id.llsummadvise).gone();
            aq.id(R.id.txtactionsumm).text("");
            aq.id(R.id.txtactionsumm).gone();
            aq.id(R.id.llreferralinfo).gone();

        }
    }

    private void setNbSymptoms() {
        try {


            pncNbs = new ArrayList<>(10);
            insertchildMap = new LinkedHashMap<>();
            //01Dec2019 - Bindu
            insertchildMap2 = new LinkedHashMap<>();
            insertchildMap3 = new LinkedHashMap<>();
            insertchildMap4 = new LinkedHashMap<>();

            arrLNbSymptoms = new ArrayList<>();
            arrLNbIds = new ArrayList<>();
            nbdangersignsMap = new HashMap<>();

            //05Dec2019 - Bindu
            pncNbssave = new ArrayList<>(10);

            if (aq.id(R.id.chknbsmallbabylbw).isChecked()) {
                PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbsmallbabylbw).getText().toString());
                PNCPojoNb psave = new PNCPojoNb(getResources().getString(R.string.nbsmallbabylbwsave));  //05Dec2019 - Bindu - for saving in eng
                for (Integer n : unionchildlist) {
                    if (aq.id(nbsmallbaby + n).isChecked()) {
                        cemocCnt++;
                        p.addBabyId(getResources().getString(R.string.baby) + n);
                        psave.addBabyId(getResources().getString(R.string.babysave) + n); //05Dec2019 - Bindu
                        if (n == 1 && myChildIDs.get(n) != null)
                            insertchildMap.put("301", new VisitDetailsPojo("301", getResources().getString(R.string.nbsmallbabylbw), getResources().getString(R.string.strcemoc), myChildIDs.get(n))); //11May2021 Bindu - thru out change cemoc to dh/ah - 109 occurences
                        else if (n == 2 && myChildIDs.get(n) != null)
                            insertchildMap2.put("301", new VisitDetailsPojo("301", getResources().getString(R.string.nbsmallbabylbw), getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 3 && myChildIDs.get(n) != null)
                            insertchildMap3.put("301", new VisitDetailsPojo("301", getResources().getString(R.string.nbsmallbabylbw), getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 4 && myChildIDs.get(n) != null)
                            insertchildMap4.put("301", new VisitDetailsPojo("301", getResources().getString(R.string.nbsmallbabylbw), getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                    }
                }
                pncNbs.add(p);
                pncNbssave.add(psave);
            }

            if (aq.id(R.id.chknbnomeconiumpassed).isChecked()) {
                PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbnomeconiumpassed).getText().toString());
                PNCPojoNb psave = new PNCPojoNb(getResources().getString(R.string.nbnomeconiumpassedsave));  //05Dec2019 - Bindu - for saving in eng
                for (Integer n : unionchildlist) {
                    if (aq.id(nbnomeconium + n).isChecked()) {
                        cemocCnt++;
                        p.addBabyId(getResources().getString(R.string.baby) + n);
                        psave.addBabyId(getResources().getString(R.string.babysave) + n); //05Dec2019 - Bindu
                        if (n == 1 && myChildIDs.get(n) != null)
                            insertchildMap.put("302", new VisitDetailsPojo("302", getResources().getString(R.string.nbnomeconiumpassed), getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 2 && myChildIDs.get(n) != null)
                            insertchildMap2.put("302", new VisitDetailsPojo("302", getResources().getString(R.string.nbnomeconiumpassed), getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 3 && myChildIDs.get(n) != null)
                            insertchildMap3.put("302", new VisitDetailsPojo("302", getResources().getString(R.string.nbnomeconiumpassed), getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 4 && myChildIDs.get(n) != null)
                            insertchildMap4.put("302", new VisitDetailsPojo("302", getResources().getString(R.string.nbnomeconiumpassed), getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                    }
                }
                pncNbs.add(p);
                pncNbssave.add(psave); //05Dec2019 - Bindu
            }

            if (aq.id(R.id.chknbnourinepassed).isChecked()) {
                PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbnourinepassed).getText().toString());
                PNCPojoNb psave = new PNCPojoNb(getResources().getString(R.string.nbnourinepassedsave));  //05Dec2019 - Bindu - for saving in eng
                for (Integer n : unionchildlist) {
                    if (aq.id(nbnourinepassed + n).isChecked()) {
                        cemocCnt++;
                        p.addBabyId(getResources().getString(R.string.baby) + n);
                        psave.addBabyId(getResources().getString(R.string.babysave) + n); //05Dec2019 - Bindu
                        if (n == 1 && myChildIDs.get(n) != null)
                            insertchildMap.put("303", new VisitDetailsPojo("303", getResources().getString(R.string.nbnourinepassed), getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 2 && myChildIDs.get(n) != null)
                            insertchildMap2.put("303", new VisitDetailsPojo("303", getResources().getString(R.string.nbnourinepassed), getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 3 && myChildIDs.get(n) != null)
                            insertchildMap3.put("303", new VisitDetailsPojo("303", getResources().getString(R.string.nbnourinepassed), getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 4 && myChildIDs.get(n) != null)
                            insertchildMap4.put("303", new VisitDetailsPojo("303", getResources().getString(R.string.nbnourinepassed), getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                    }
                }
                pncNbs.add(p);
                pncNbssave.add(psave); //05Dec2019 - Bindu
            }

            //Inability to suck
            if (aq.id(R.id.chknbinabilitytosuck).isChecked()) {
                PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbinabilitytosuck).getText().toString());
                PNCPojoNb psave = new PNCPojoNb(getResources().getString(R.string.nbinabilitytosucksave));  //05Dec2019 - Bindu - for saving in eng
                for (Integer n : unionchildlist) {
                    if (aq.id(nbinabilitytostuck + n).isChecked()) {
                        cemocCnt++;
                        p.addBabyId(getResources().getString(R.string.baby) + n);
                        psave.addBabyId(getResources().getString(R.string.babysave) + n); //05Dec2019 - Bindu
                        if (n == 1 && myChildIDs.get(n) != null)
                            insertchildMap.put("304", new VisitDetailsPojo("304", getResources().getString(R.string.nbinabilitytosuck), getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 2 && myChildIDs.get(n) != null)
                            insertchildMap2.put("304", new VisitDetailsPojo("304", getResources().getString(R.string.nbinabilitytosuck), getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 3 && myChildIDs.get(n) != null)
                            insertchildMap3.put("304", new VisitDetailsPojo("304", getResources().getString(R.string.nbinabilitytosuck), getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 4 && myChildIDs.get(n) != null)
                            insertchildMap4.put("304", new VisitDetailsPojo("304", getResources().getString(R.string.nbinabilitytosuck), getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                    }
                }
                pncNbs.add(p);
                pncNbssave.add(psave); //05Dec2019 - Bindu
            }

            //fast breathing
            if (aq.id(R.id.chknbfastbreathing).isChecked()) {
                PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbfastbreathing).getText().toString());
                PNCPojoNb psave = new PNCPojoNb(getResources().getString(R.string.nbfastbreathingsave));  //05Dec2019 - Bindu - for saving in eng
                for (Integer n : unionchildlist) {
                    if (aq.id(nbfastbreathing + n).isChecked()) {
                        cemocCnt++;
                        p.addBabyId(getResources().getString(R.string.baby) + n);
                        psave.addBabyId(getResources().getString(R.string.babysave) + n); //05Dec2019 - Bindu
                        if (n == 1 && myChildIDs.get(n) != null)
                            insertchildMap.put("305", new VisitDetailsPojo("305", getResources().getString(R.string.nbfastbreathing), getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 2 && myChildIDs.get(n) != null)
                            insertchildMap2.put("305", new VisitDetailsPojo("305", getResources().getString(R.string.nbfastbreathing), getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 3 && myChildIDs.get(n) != null)
                            insertchildMap3.put("305", new VisitDetailsPojo("305", getResources().getString(R.string.nbfastbreathing), getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 4 && myChildIDs.get(n) != null)
                            insertchildMap4.put("305", new VisitDetailsPojo("305", getResources().getString(R.string.nbfastbreathing), getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                    }
                }
                pncNbs.add(p);
                pncNbssave.add(psave); //05Dec2019 - Bindu
            }

            //inability to pass urine and stools
            if (aq.id(R.id.chknbinabilitytopassurineandstool).isChecked()) {
                PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbinabilitytopassurineandstool).getText().toString());
                PNCPojoNb psave = new PNCPojoNb(getResources().getString(R.string.nbinabilitytopassurineandstoolsave));  //05Dec2019 - Bindu - for saving in eng
                for (Integer n : unionchildlist) {
                    if (aq.id(nbinabilitytopassurineandstool + n).isChecked()) {
                        cemocCnt++;
                        p.addBabyId(getResources().getString(R.string.baby) + n);
                        psave.addBabyId(getResources().getString(R.string.babysave) + n); //05Dec2019 - Bindu
                        if (n == 1 && myChildIDs.get(n) != null)
                            insertchildMap.put("306", new VisitDetailsPojo("306", getResources().getString(R.string.nbinabilitytopassurineandstool), getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 2 && myChildIDs.get(n) != null)
                            insertchildMap2.put("306", new VisitDetailsPojo("306", getResources().getString(R.string.nbinabilitytopassurineandstool), getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 3 && myChildIDs.get(n) != null)
                            insertchildMap3.put("306", new VisitDetailsPojo("306", getResources().getString(R.string.nbinabilitytopassurineandstool), getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 4 && myChildIDs.get(n) != null)
                            insertchildMap4.put("306", new VisitDetailsPojo("306", getResources().getString(R.string.nbinabilitytopassurineandstool), getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                    }
                }
                pncNbs.add(p);
                pncNbssave.add(psave); //05Dec2019 - Bindu
            }

            //umbilical stump red or pus
            if (aq.id(R.id.chknbumbilicalstumpredorpus).isChecked()) {
                PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbumbilicalstumpredorpus).getText().toString());
                PNCPojoNb psave = new PNCPojoNb(getResources().getString(R.string.nbumbilicalstumpredorpussave));  //05Dec2019 - Bindu - for saving in eng
                for (Integer n : unionchildlist) {
                    if (aq.id(nbumbilicalstump + n).isChecked()) {//12Aug2021 Arpitha - wrong id was checked
                        cemocCnt++;
                        p.addBabyId(getResources().getString(R.string.baby) + n);
                        psave.addBabyId(getResources().getString(R.string.babysave) + n); //05Dec2019 - Bindu
                        if (n == 1 && myChildIDs.get(n) != null)
                            insertchildMap.put("307", new VisitDetailsPojo("307", getResources().getString(R.string.nbumbilicalstumpredorpus), getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 2 && myChildIDs.get(n) != null)
                            insertchildMap2.put("307", new VisitDetailsPojo("307", getResources().getString(R.string.nbumbilicalstumpredorpus), getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 3 && myChildIDs.get(n) != null)
                            insertchildMap3.put("307", new VisitDetailsPojo("307", getResources().getString(R.string.nbumbilicalstumpredorpus), getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 4 && myChildIDs.get(n) != null)
                            insertchildMap4.put("307", new VisitDetailsPojo("307", getResources().getString(R.string.nbumbilicalstumpredorpus), getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                    }
                }
                pncNbs.add(p);
                pncNbssave.add(psave); //05Dec2019 - Bindu
            }

            //pustules
            if (aq.id(R.id.chknbpustules).isChecked()) {
                PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbpustules).getText().toString());
                PNCPojoNb psave = new PNCPojoNb(getResources().getString(R.string.nbpustulessave));  //05Dec2019 - Bindu - for saving in eng
                for (Integer n : unionchildlist) {
                    if (aq.id(nbpustules + n).isChecked()) {
                        cemocCnt++;
                        p.addBabyId(getResources().getString(R.string.baby) + n);
                        psave.addBabyId(getResources().getString(R.string.babysave) + n); //05Dec2019 - Bindu
                        if (n == 1 && myChildIDs.get(n) != null)
                            insertchildMap.put("308", new VisitDetailsPojo("308", getResources().getString(R.string.nbpustules), getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 2 && myChildIDs.get(n) != null)
                            insertchildMap2.put("308", new VisitDetailsPojo("308", getResources().getString(R.string.nbpustules), getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 3 && myChildIDs.get(n) != null)
                            insertchildMap3.put("308", new VisitDetailsPojo("308", getResources().getString(R.string.nbpustules), getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 4 && myChildIDs.get(n) != null)
                            insertchildMap4.put("308", new VisitDetailsPojo("308", getResources().getString(R.string.nbpustules), getResources().getString(R.string.strcemoc), myChildIDs.get(n)));

                    }
                }
                pncNbs.add(p);
                pncNbssave.add(psave); //05Dec2019 - Bindu
            }

            //jaundice
            if (aq.id(R.id.chknbjaundice).isChecked()) {
                PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbjaundice).getText().toString());
                PNCPojoNb psave = new PNCPojoNb(getResources().getString(R.string.nbjaundicesave));  //05Dec2019 - Bindu - for saving in eng
                for (Integer n : unionchildlist) {
                    if (aq.id(nbjaundice + n).isChecked()) {
                        cemocCnt++;
                        p.addBabyId(getResources().getString(R.string.baby) + n);
                        psave.addBabyId(getResources().getString(R.string.babysave) + n); //05Dec2019 - Bindu
                        if (n == 1 && myChildIDs.get(n) != null)
                            insertchildMap.put("309", new VisitDetailsPojo("309", getResources().getString(R.string.nbjaundice), getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 2 && myChildIDs.get(n) != null)
                            insertchildMap2.put("309", new VisitDetailsPojo("309", getResources().getString(R.string.nbjaundice), getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 3 && myChildIDs.get(n) != null)
                            insertchildMap3.put("309", new VisitDetailsPojo("309", getResources().getString(R.string.nbjaundice), getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 4 && myChildIDs.get(n) != null)
                            insertchildMap4.put("309", new VisitDetailsPojo("309", getResources().getString(R.string.nbjaundice), getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                    }
                }
                pncNbs.add(p);
                pncNbssave.add(psave); //05Dec2019 - Bindu
            }

            //fever
            if (aq.id(R.id.chknbfever).isChecked()) {
                PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbfever).getText().toString());
                PNCPojoNb psave = new PNCPojoNb(getResources().getString(R.string.nbfeversave));  //05Dec2019 - Bindu - for saving in eng
                for (Integer n : unionchildlist) {
                    if (aq.id(nbfever + n).isChecked()) {
                        cemocCnt++;
                        p.addBabyId(getResources().getString(R.string.baby) + n);
                        psave.addBabyId(getResources().getString(R.string.babysave) + n); //05Dec2019 - Bindu
                        if (n == 1 && myChildIDs.get(n) != null)
                            insertchildMap.put("310", new VisitDetailsPojo("310", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 2 && myChildIDs.get(n) != null)
                            insertchildMap2.put("310", new VisitDetailsPojo("310", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 3 && myChildIDs.get(n) != null)
                            insertchildMap3.put("310", new VisitDetailsPojo("310", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 4 && myChildIDs.get(n) != null)
                            insertchildMap4.put("310", new VisitDetailsPojo("310", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                    }
                }
                pncNbs.add(p);
                pncNbssave.add(psave); //05Dec2019 - Bindu
            }

            //diarrhoea
            if (aq.id(R.id.chknbdiarrhoea).isChecked()) {
                PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbdiarrhoea).getText().toString());
                PNCPojoNb psave = new PNCPojoNb(getResources().getString(R.string.nbdiarrhoeasave));  //05Dec2019 - Bindu - for saving in eng
                for (Integer n : unionchildlist) {
                    if (aq.id(nbdiarrhoea + n).isChecked()) {
                        cemocCnt++;
                        p.addBabyId(getResources().getString(R.string.baby) + n);
                        psave.addBabyId(getResources().getString(R.string.babysave) + n); //05Dec2019 - Bindu
                        if (n == 1 && myChildIDs.get(n) != null)
                            insertchildMap.put("311", new VisitDetailsPojo("311", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 2 && myChildIDs.get(n) != null)
                            insertchildMap2.put("311", new VisitDetailsPojo("311", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 3 && myChildIDs.get(n) != null)
                            insertchildMap3.put("311", new VisitDetailsPojo("311", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 4 && myChildIDs.get(n) != null)
                            insertchildMap4.put("311", new VisitDetailsPojo("311", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                    }
                }
                pncNbs.add(p);
                pncNbssave.add(psave); //05Dec2019 - Bindu
            }

            //blood in stools
            if (aq.id(R.id.chknbbloodinstools).isChecked()) {
                PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbbloodinstools).getText().toString());
                PNCPojoNb psave = new PNCPojoNb(getResources().getString(R.string.nbbloodinstoolssave));  //05Dec2019 - Bindu - for saving in eng
                for (Integer n : unionchildlist) {
                    if (aq.id(nbbloodinstools + n).isChecked()) {
                        cemocCnt++;
                        p.addBabyId(getResources().getString(R.string.baby) + n);
                        psave.addBabyId(getResources().getString(R.string.babysave) + n); //05Dec2019 - Bindu
                        if (n == 1 && myChildIDs.get(n) != null)
                            insertchildMap.put("312", new VisitDetailsPojo("312", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 2 && myChildIDs.get(n) != null)
                            insertchildMap2.put("312", new VisitDetailsPojo("312", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 3 && myChildIDs.get(n) != null)
                            insertchildMap3.put("312", new VisitDetailsPojo("312", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 4 && myChildIDs.get(n) != null)
                            insertchildMap4.put("312", new VisitDetailsPojo("312", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                    }
                }
                pncNbs.add(p);
                pncNbssave.add(psave); //05Dec2019 - Bindu
            }

            //dull and lethargic
            if (aq.id(R.id.chknblethargicbaby).isChecked()) {
                PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknblethargicbaby).getText().toString());
                PNCPojoNb psave = new PNCPojoNb(getResources().getString(R.string.nblethargicbabysave));  //05Dec2019 - Bindu - for saving in eng
                for (Integer n : unionchildlist) {
                    if (aq.id(nblethargic + n).isChecked()) {
                        cemocCnt++;
                        p.addBabyId(getResources().getString(R.string.baby) + n);
                        psave.addBabyId(getResources().getString(R.string.babysave) + n); //05Dec2019 - Bindu
                        //insertchildMap.put(myChildIDs.get(n), new VisitDetailsPojo("313", getResources().getString(R.string.nblethargicbaby),getResources().getString(R.string.strcemoc)));
                        if (n == 1 && myChildIDs.get(n) != null)
                            insertchildMap.put("313", new VisitDetailsPojo("313", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 2 && myChildIDs.get(n) != null)
                            insertchildMap2.put("313", new VisitDetailsPojo("313", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 3 && myChildIDs.get(n) != null)
                            insertchildMap3.put("313", new VisitDetailsPojo("313", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 4 && myChildIDs.get(n) != null)
                            insertchildMap4.put("313", new VisitDetailsPojo("313", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                    }
                }
                pncNbs.add(p);
                pncNbssave.add(psave); //05Dec2019 - Bindu
            }

            //09Dec2019 - Bindu
            //nb fits
            if (aq.id(R.id.chknbfitsbaby).isChecked()) {
                PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbfitsbaby).getText().toString());
                PNCPojoNb psave = new PNCPojoNb(getResources().getString(R.string.nbfitssave));  //05Dec2019 - Bindu - for saving in eng
                for (Integer n : unionchildlist) {
                    if (aq.id(nbfits + n).isChecked()) {
                        cemocCnt++;
                        p.addBabyId(getResources().getString(R.string.baby) + n);
                        psave.addBabyId(getResources().getString(R.string.babysave) + n);
                        if (n == 1 && myChildIDs.get(n) != null)
                            insertchildMap.put("314", new VisitDetailsPojo("314", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 2 && myChildIDs.get(n) != null)
                            insertchildMap2.put("314", new VisitDetailsPojo("314", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 3 && myChildIDs.get(n) != null)
                            insertchildMap3.put("314", new VisitDetailsPojo("314", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 4 && myChildIDs.get(n) != null)
                            insertchildMap4.put("314", new VisitDetailsPojo("314", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                    }
                }
                pncNbs.add(p);
                pncNbssave.add(psave);
            }

            //eyes are red
            if (aq.id(R.id.chknbeyesareredbaby).isChecked()) {
                PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbeyesareredbaby).getText().toString());
                PNCPojoNb psave = new PNCPojoNb(getResources().getString(R.string.nbeyesinfectedsave));  //05Dec2019 - Bindu - for saving in eng
                for (Integer n : unionchildlist) {
                    if (aq.id(nbeyesarered + n).isChecked()) {
                        cemocCnt++;
                        p.addBabyId(getResources().getString(R.string.baby) + n);
                        psave.addBabyId(getResources().getString(R.string.babysave) + n);
                        if (n == 1 && myChildIDs.get(n) != null)
                            insertchildMap.put("315", new VisitDetailsPojo("315", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 2 && myChildIDs.get(n) != null)
                            insertchildMap2.put("315", new VisitDetailsPojo("315", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 3 && myChildIDs.get(n) != null)
                            insertchildMap3.put("315", new VisitDetailsPojo("315", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 4 && myChildIDs.get(n) != null)
                            insertchildMap4.put("315", new VisitDetailsPojo("315", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                    }
                }
                pncNbs.add(p);
                pncNbssave.add(psave);
            }

            //pallor of palms/soles
            if (aq.id(R.id.chknbpallorofpalmsbaby).isChecked()) {
                PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbpallorofpalmsbaby).getText().toString());
                PNCPojoNb psave = new PNCPojoNb(getResources().getString(R.string.nbpallororsolessave));  //05Dec2019 - Bindu - for saving in eng
                for (Integer n : unionchildlist) {
                    if (aq.id(nbpallorofpalmsorsoles + n).isChecked()) {
                        cemocCnt++;
                        p.addBabyId(getResources().getString(R.string.baby) + n);
                        psave.addBabyId(getResources().getString(R.string.babysave) + n);
                        if (n == 1 && myChildIDs.get(n) != null)
                            insertchildMap.put("316", new VisitDetailsPojo("316", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 2 && myChildIDs.get(n) != null)
                            insertchildMap2.put("316", new VisitDetailsPojo("316", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 3 && myChildIDs.get(n) != null)
                            insertchildMap3.put("316", new VisitDetailsPojo("316", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 4 && myChildIDs.get(n) != null)
                            insertchildMap4.put("316", new VisitDetailsPojo("316", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                    }
                }
                pncNbs.add(p);
                pncNbssave.add(psave);
            }

            //yellow of palms
            if (aq.id(R.id.chknbyellowpalmsbaby).isChecked()) {
                PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbyellowpalmsbaby).getText().toString());
                PNCPojoNb psave = new PNCPojoNb(getResources().getString(R.string.nbyellowpalmsandsolessave));  //05Dec2019 - Bindu - for saving in eng
                for (Integer n : unionchildlist) {
                    if (aq.id(nbyellowpalms + n).isChecked()) {
                        cemocCnt++;
                        p.addBabyId(getResources().getString(R.string.baby) + n);
                        psave.addBabyId(getResources().getString(R.string.babysave) + n);
                        if (n == 1 && myChildIDs.get(n) != null)
                            insertchildMap.put("317", new VisitDetailsPojo("317", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 2 && myChildIDs.get(n) != null)
                            insertchildMap2.put("317", new VisitDetailsPojo("317", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 3 && myChildIDs.get(n) != null)
                            insertchildMap3.put("317", new VisitDetailsPojo("317", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 4 && myChildIDs.get(n) != null)
                            insertchildMap4.put("317", new VisitDetailsPojo("317", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                    }
                }
                pncNbs.add(p);
                pncNbssave.add(psave);
            }
            //Blue palms
            if (aq.id(R.id.chknbbluepalmsbaby).isChecked()) {
                PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbbluepalmsbaby).getText().toString());
                PNCPojoNb psave = new PNCPojoNb(getResources().getString(R.string.nbbluepalmsandsolessave));  //05Dec2019 - Bindu - for saving in eng
                for (Integer n : unionchildlist) {
                    if (aq.id(nbbluepalms + n).isChecked()) {
                        cemocCnt++;
                        p.addBabyId(getResources().getString(R.string.baby) + n);
                        psave.addBabyId(getResources().getString(R.string.babysave) + n);
                        if (n == 1 && myChildIDs.get(n) != null)
                            insertchildMap.put("318", new VisitDetailsPojo("318", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 2 && myChildIDs.get(n) != null)
                            insertchildMap2.put("318", new VisitDetailsPojo("318", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 3 && myChildIDs.get(n) != null)
                            insertchildMap3.put("318", new VisitDetailsPojo("318", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 4 && myChildIDs.get(n) != null)
                            insertchildMap4.put("318", new VisitDetailsPojo("318", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                    }
                }
                pncNbs.add(p);
                pncNbssave.add(psave);
            }

            //Feels cold
            if (aq.id(R.id.chknbfeelscoldorhotbaby).isChecked()) {
                PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbfeelscoldorhotbaby).getText().toString());
                PNCPojoNb psave = new PNCPojoNb(getResources().getString(R.string.nbfeelscoldorhottotouchsave));  //05Dec2019 - Bindu - for saving in eng
                for (Integer n : unionchildlist) {
                    if (aq.id(nbfeelscoldorhot + n).isChecked()) {
                        cemocCnt++;
                        p.addBabyId(getResources().getString(R.string.baby) + n);
                        psave.addBabyId(getResources().getString(R.string.babysave) + n);
                        if (n == 1 && myChildIDs.get(n) != null)
                            insertchildMap.put("319", new VisitDetailsPojo("319", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 2 && myChildIDs.get(n) != null)
                            insertchildMap2.put("319", new VisitDetailsPojo("319", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 3 && myChildIDs.get(n) != null)
                            insertchildMap3.put("319", new VisitDetailsPojo("319", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 4 && myChildIDs.get(n) != null)
                            insertchildMap4.put("319", new VisitDetailsPojo("319", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                    }
                }
                pncNbs.add(p);
                pncNbssave.add(psave);
            }

            //Bleeding from any site
            if (aq.id(R.id.chknbbleedingbaby).isChecked()) {
                PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbbleedingbaby).getText().toString());
                PNCPojoNb psave = new PNCPojoNb(getResources().getString(R.string.nbbleedingfromanysitesave));  //05Dec2019 - Bindu - for saving in eng
                for (Integer n : unionchildlist) {
                    if (aq.id(nbbleeding + n).isChecked()) {
                        cemocCnt++;
                        p.addBabyId(getResources().getString(R.string.baby) + n);
                        psave.addBabyId(getResources().getString(R.string.babysave) + n);
                        if (n == 1 && myChildIDs.get(n) != null)
                            insertchildMap.put("320", new VisitDetailsPojo("320", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 2 && myChildIDs.get(n) != null)
                            insertchildMap2.put("320", new VisitDetailsPojo("320", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 3 && myChildIDs.get(n) != null)
                            insertchildMap3.put("320", new VisitDetailsPojo("320", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 4 && myChildIDs.get(n) != null)
                            insertchildMap4.put("320", new VisitDetailsPojo("320", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                    }
                }
                pncNbs.add(p);
                pncNbssave.add(psave);
            }

            //Abdominal distension
            if (aq.id(R.id.chknbabdominaldistensionbaby).isChecked()) {
                PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbabdominaldistensionbaby).getText().toString());
                PNCPojoNb psave = new PNCPojoNb(getResources().getString(R.string.nbabdominaldistensionsave));  //05Dec2019 - Bindu - for saving in eng
                for (Integer n : unionchildlist) {
                    if (aq.id(nbabdominaldistension + n).isChecked()) {
                        cemocCnt++;
                        p.addBabyId(getResources().getString(R.string.baby) + n);
                        psave.addBabyId(getResources().getString(R.string.babysave) + n);
                        if (n == 1 && myChildIDs.get(n) != null)
                            insertchildMap.put("321", new VisitDetailsPojo("321", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 2 && myChildIDs.get(n) != null)
                            insertchildMap2.put("321", new VisitDetailsPojo("321", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 3 && myChildIDs.get(n) != null)
                            insertchildMap3.put("321", new VisitDetailsPojo("321", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 4 && myChildIDs.get(n) != null)
                            insertchildMap4.put("321", new VisitDetailsPojo("321", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                    }
                }
                pncNbs.add(p);
                pncNbssave.add(psave);
            }

            //Any birth defect
            if (aq.id(R.id.chknbanybirthdefectbaby).isChecked()) {
                PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbanybirthdefectbaby).getText().toString());
                PNCPojoNb psave = new PNCPojoNb(getResources().getString(R.string.nbanybirthdefectssave));  //05Dec2019 - Bindu - for saving in eng
                for (Integer n : unionchildlist) {
                    if (aq.id(nbanybirthdefect + n).isChecked()) {
                        cemocCnt++;
                        p.addBabyId(getResources().getString(R.string.baby) + n);
                        psave.addBabyId(getResources().getString(R.string.babysave) + n);
                        if (n == 1 && myChildIDs.get(n) != null)
                            insertchildMap.put("322", new VisitDetailsPojo("322", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 2 && myChildIDs.get(n) != null)
                            insertchildMap2.put("322", new VisitDetailsPojo("322", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 3 && myChildIDs.get(n) != null)
                            insertchildMap3.put("322", new VisitDetailsPojo("322", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 4 && myChildIDs.get(n) != null)
                            insertchildMap4.put("322", new VisitDetailsPojo("322", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                    }
                }
                pncNbs.add(p);
                pncNbssave.add(psave);
            }

            //manikanta10feb2021
            //Feels warm
            if (aq.id(R.id.chknbfeelswarmbaby).isChecked()) {
                PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbfeelswarmbaby).getText().toString());
                PNCPojoNb psave = new PNCPojoNb(getResources().getString(R.string.nbfeelswarmtotouchsave));
                for (Integer n : unionchildlist) {
                    if (aq.id(nbfeelswarm + n).isChecked()) {
                        cemocCnt++;
                        p.addBabyId(getResources().getString(R.string.baby) + n);
                        psave.addBabyId(getResources().getString(R.string.babysave) + n);
                        if (n == 1 && myChildIDs.get(n) != null)
                            insertchildMap.put("324", new VisitDetailsPojo("324", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 2 && myChildIDs.get(n) != null)
                            insertchildMap2.put("324", new VisitDetailsPojo("324", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 3 && myChildIDs.get(n) != null)
                            insertchildMap3.put("324", new VisitDetailsPojo("324", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 4 && myChildIDs.get(n) != null)
                            insertchildMap4.put("324", new VisitDetailsPojo("324", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                    }
                }
                pncNbs.add(p);
                pncNbssave.add(psave);
            }

            //manikanta10feb2021
            //Feels too warm
            if (aq.id(R.id.chknbfeelstoowarmbaby).isChecked()) {
                PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbfeelstoowarmbaby).getText().toString());
                PNCPojoNb psave = new PNCPojoNb(getResources().getString(R.string.nbfeelstoowarmtouchsave));
                for (Integer n : unionchildlist) {
                    if (aq.id(nbfeelstoowarm + n).isChecked()) {
                        cemocCnt++;
                        p.addBabyId(getResources().getString(R.string.baby) + n);
                        psave.addBabyId(getResources().getString(R.string.babysave) + n);
                        if (n == 1 && myChildIDs.get(n) != null)
                            insertchildMap.put("325", new VisitDetailsPojo("325", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 2 && myChildIDs.get(n) != null)
                            insertchildMap2.put("325", new VisitDetailsPojo("325", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 3 && myChildIDs.get(n) != null)
                            insertchildMap3.put("325", new VisitDetailsPojo("325", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 4 && myChildIDs.get(n) != null)
                            insertchildMap4.put("325", new VisitDetailsPojo("325", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                    }
                }
                pncNbs.add(p);
                pncNbssave.add(psave);
            }

            //11May2021 Bindu add sunkenfont , vomiting and sepsis
            //sunken fontanelle
            if (aq.id(R.id.chknbsunkenfont).isChecked()) {
                PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbsunkenfont).getText().toString());
                PNCPojoNb psave = new PNCPojoNb(getResources().getString(R.string.nbsunkenfontnellesave));
                for (Integer n : unionchildlist) {
                    if (aq.id(nbsunkenfont + n).isChecked()) {
                        cemocCnt++;
                        p.addBabyId(getResources().getString(R.string.baby) + n);
                        psave.addBabyId(getResources().getString(R.string.babysave) + n); //05Dec2019 - Bindu
                        if (n == 1 && myChildIDs.get(n) != null)
                            insertchildMap.put("326", new VisitDetailsPojo("326", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 2 && myChildIDs.get(n) != null)
                            insertchildMap2.put("326", new VisitDetailsPojo("326", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 3 && myChildIDs.get(n) != null)
                            insertchildMap3.put("326", new VisitDetailsPojo("326", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 4 && myChildIDs.get(n) != null)
                            insertchildMap4.put("326", new VisitDetailsPojo("326", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                    }
                }
                pncNbs.add(p);
                pncNbssave.add(psave); //05Dec2019 - Bindu
            }
            //nb vomiting
            if (aq.id(R.id.chknbvomiting).isChecked()) {
                PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbvomiting).getText().toString());
                PNCPojoNb psave = new PNCPojoNb(getResources().getString(R.string.nbvomitingsave));
                for (Integer n : unionchildlist) {
                    if (aq.id(nbvomiting + n).isChecked()) {
                        cemocCnt++;
                        p.addBabyId(getResources().getString(R.string.baby) + n);
                        psave.addBabyId(getResources().getString(R.string.babysave) + n); //05Dec2019 - Bindu
                        if (n == 1 && myChildIDs.get(n) != null)
                            insertchildMap.put("327", new VisitDetailsPojo("327", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 2 && myChildIDs.get(n) != null)
                            insertchildMap2.put("327", new VisitDetailsPojo("327", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 3 && myChildIDs.get(n) != null)
                            insertchildMap3.put("327", new VisitDetailsPojo("327", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 4 && myChildIDs.get(n) != null)
                            insertchildMap4.put("327", new VisitDetailsPojo("327", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                    }
                }
                pncNbs.add(p);
                pncNbssave.add(psave); //05Dec2019 - Bindu
            }
            //nbsepsis
            if (aq.id(R.id.chknbsepsis).isChecked()) {
                PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbsepsis).getText().toString());
                PNCPojoNb psave = new PNCPojoNb(getResources().getString(R.string.nbsepsissave));
                for (Integer n : unionchildlist) {
                    if (aq.id(nbsepsis + n).isChecked()) {
                        cemocCnt++;
                        p.addBabyId(getResources().getString(R.string.baby) + n);
                        psave.addBabyId(getResources().getString(R.string.babysave) + n); //05Dec2019 - Bindu
                        if (n == 1 && myChildIDs.get(n) != null)
                            insertchildMap.put("328", new VisitDetailsPojo("328", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 2 && myChildIDs.get(n) != null)
                            insertchildMap2.put("328", new VisitDetailsPojo("328", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 3 && myChildIDs.get(n) != null)
                            insertchildMap3.put("328", new VisitDetailsPojo("328", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 4 && myChildIDs.get(n) != null)
                            insertchildMap4.put("328", new VisitDetailsPojo("328", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                    }
                }
                pncNbs.add(p);
                pncNbssave.add(psave); //05Dec2019 - Bindu
            }

            // 14May2021 Bindu pneumonia
            if (aq.id(R.id.chknbpneumonia).isChecked()) {
                PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbpneumonia).getText().toString());
                PNCPojoNb psave = new PNCPojoNb(getResources().getString(R.string.nbpneumoniasave));
                for (Integer n : unionchildlist) {
                    if (aq.id(nbpneumonia + n).isChecked()) {
                        cemocCnt++;
                        p.addBabyId(getResources().getString(R.string.baby) + n);
                        psave.addBabyId(getResources().getString(R.string.babysave) + n); //05Dec2019 - Bindu
                        if (n == 1 && myChildIDs.get(n) != null)
                            insertchildMap.put("329", new VisitDetailsPojo("329", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 2 && myChildIDs.get(n) != null)
                            insertchildMap2.put("329", new VisitDetailsPojo("329", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 3 && myChildIDs.get(n) != null)
                            insertchildMap3.put("329", new VisitDetailsPojo("329", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 4 && myChildIDs.get(n) != null)
                            insertchildMap4.put("329", new VisitDetailsPojo("329", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                    }
                }
                pncNbs.add(p);
                pncNbssave.add(psave); //05Dec2019 - Bindu
            }

            // Mani 12Sep2021 Hypothermia
            if (aq.id(R.id.chknbhypothermiababy).isChecked()) {
                PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbhypothermiababy).getText().toString());
                PNCPojoNb psave = new PNCPojoNb(getResources().getString(R.string.nbhypothermia));
                for (Integer n : unionchildlist) {
                    if (aq.id(nbhypothermia + n).isChecked()) {
                        cemocCnt++;
                        p.addBabyId(getResources().getString(R.string.baby) + n);
                        psave.addBabyId(getResources().getString(R.string.babysave) + n); //05Dec2019 - Bindu
                        if (n == 1 && myChildIDs.get(n) != null)
                            insertchildMap.put("330", new VisitDetailsPojo("330", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 2 && myChildIDs.get(n) != null)
                            insertchildMap2.put("330", new VisitDetailsPojo("330", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 3 && myChildIDs.get(n) != null)
                            insertchildMap3.put("330", new VisitDetailsPojo("330", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 4 && myChildIDs.get(n) != null)
                            insertchildMap4.put("330", new VisitDetailsPojo("330", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                    }
                }
                pncNbs.add(p);
                pncNbssave.add(psave);
            }

            // Mani 12Sep2021  Raspiratory distress
            if (aq.id(R.id.chknbraspiratorydistressbaby).isChecked()) {
                PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbraspiratorydistressbaby).getText().toString());
                PNCPojoNb psave = new PNCPojoNb(getResources().getString(R.string.nbrespiratorydistress));
                for (Integer n : unionchildlist) {
                    if (aq.id(nbrespiratorydistress + n).isChecked()) {
                        cemocCnt++;
                        p.addBabyId(getResources().getString(R.string.baby) + n);
                        psave.addBabyId(getResources().getString(R.string.babysave) + n); //05Dec2019 - Bindu
                        if (n == 1 && myChildIDs.get(n) != null)
                            insertchildMap.put("331", new VisitDetailsPojo("331", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 2 && myChildIDs.get(n) != null)
                            insertchildMap2.put("331", new VisitDetailsPojo("331", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 3 && myChildIDs.get(n) != null)
                            insertchildMap3.put("331", new VisitDetailsPojo("331", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 4 && myChildIDs.get(n) != null)
                            insertchildMap4.put("331", new VisitDetailsPojo("331", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                    }
                }
                pncNbs.add(p);
                pncNbssave.add(psave);
            }

            // Mani 12Sep2021  Cyanosis
            if (aq.id(R.id.chknbcyanosisbaby).isChecked()) {
                PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbcyanosisbaby).getText().toString());
                PNCPojoNb psave = new PNCPojoNb(getResources().getString(R.string.nbcyanosis));
                for (Integer n : unionchildlist) {
                    if (aq.id(nbcynosis + n).isChecked()) {
                        cemocCnt++;
                        p.addBabyId(getResources().getString(R.string.baby) + n);
                        psave.addBabyId(getResources().getString(R.string.babysave) + n); //05Dec2019 - Bindu
                        if (n == 1 && myChildIDs.get(n) != null)
                            insertchildMap.put("332", new VisitDetailsPojo("332", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 2 && myChildIDs.get(n) != null)
                            insertchildMap2.put("332", new VisitDetailsPojo("332", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 3 && myChildIDs.get(n) != null)
                            insertchildMap3.put("332", new VisitDetailsPojo("332", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 4 && myChildIDs.get(n) != null)
                            insertchildMap4.put("332", new VisitDetailsPojo("332", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                    }
                }
                pncNbs.add(p);
                pncNbssave.add(psave);
            }

            // Mani 12Sep2021  Excessive weight loss
            if (aq.id(R.id.chknbexcessivewtlossbaby).isChecked()) {
                PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbexcessivewtlossbaby).getText().toString());
                PNCPojoNb psave = new PNCPojoNb(getResources().getString(R.string.nbexcessiveweightloss));
                for (Integer n : unionchildlist) {
                    if (aq.id(nbexcessiveweightloss + n).isChecked()) {
                        cemocCnt++;
                        p.addBabyId(getResources().getString(R.string.baby) + n);
                        psave.addBabyId(getResources().getString(R.string.babysave) + n); //05Dec2019 - Bindu
                        if (n == 1 && myChildIDs.get(n) != null)
                            insertchildMap.put("333", new VisitDetailsPojo("333", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 2 && myChildIDs.get(n) != null)
                            insertchildMap2.put("333", new VisitDetailsPojo("333", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 3 && myChildIDs.get(n) != null)
                            insertchildMap3.put("333", new VisitDetailsPojo("333", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 4 && myChildIDs.get(n) != null)
                            insertchildMap4.put("333", new VisitDetailsPojo("333", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                    }
                }
                pncNbs.add(p);
                pncNbssave.add(psave);
            }


            // Mani 12Sep2021  Convulsion
            if (aq.id(R.id.chknbconvulsionbaby).isChecked()) {
                PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbconvulsionbaby).getText().toString());
                PNCPojoNb psave = new PNCPojoNb(getResources().getString(R.string.nbconvulsion));
                for (Integer n : unionchildlist) {
                    if (aq.id(nbconvulsion + n).isChecked()) {
                        cemocCnt++;
                        p.addBabyId(getResources().getString(R.string.baby) + n);
                        psave.addBabyId(getResources().getString(R.string.babysave) + n); //05Dec2019 - Bindu
                        if (n == 1 && myChildIDs.get(n) != null)
                            insertchildMap.put("334", new VisitDetailsPojo("334", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 2 && myChildIDs.get(n) != null)
                            insertchildMap2.put("334", new VisitDetailsPojo("334", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 3 && myChildIDs.get(n) != null)
                            insertchildMap3.put("334", new VisitDetailsPojo("334", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 4 && myChildIDs.get(n) != null)
                            insertchildMap4.put("334", new VisitDetailsPojo("334", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                    }
                }
                pncNbs.add(p);
                pncNbssave.add(psave);
            }

            // Mani 12Sep2021  preterm
            if (aq.id(R.id.chknbpretermbaby).isChecked()) {
                PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chknbpretermbaby).getText().toString());
                PNCPojoNb psave = new PNCPojoNb(getResources().getString(R.string.nbpreterm));
                for (Integer n : unionchildlist) {
                    if (aq.id(nbpreterm + n).isChecked()) {
                        cemocCnt++;
                        p.addBabyId(getResources().getString(R.string.baby) + n);
                        psave.addBabyId(getResources().getString(R.string.babysave) + n); //05Dec2019 - Bindu
                        if (n == 1 && myChildIDs.get(n) != null)
                            insertchildMap.put("335", new VisitDetailsPojo("335", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 2 && myChildIDs.get(n) != null)
                            insertchildMap2.put("335", new VisitDetailsPojo("335", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 3 && myChildIDs.get(n) != null)
                            insertchildMap3.put("335", new VisitDetailsPojo("335", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                        else if (n == 4 && myChildIDs.get(n) != null)
                            insertchildMap4.put("335", new VisitDetailsPojo("335", "", getResources().getString(R.string.strcemoc), myChildIDs.get(n)));
                    }
                }
                pncNbs.add(p);
                pncNbssave.add(psave);
            }
            // New born other danger signs //11May2021 Bindu add others to the last
            if (aq.id(R.id.chkothersymptomsnb).isChecked()) {
                if (aq.id(R.id.chkothersymptomsnb).getText().toString().trim().length() > 0) {
                    insertchildMap.put("323", new VisitDetailsPojo("323", "", getResources().getString(R.string.strcemoc), "All"));
                    cemocCnt++;
                    PNCPojoNb p = new PNCPojoNb(aq.id(R.id.chkothersymptomsnb).getText().toString() + " :  " + aq.id(R.id.etothersymptomsnb).getText().toString());
                    PNCPojoNb psave = new PNCPojoNb(getResources().getString(R.string.othersave) + " :  " + aq.id(R.id.etothersymptomsnb).getText().toString());  //05Dec2019 - Bindu - for saving in eng
                    pncNbs.add(p);
                    pncNbssave.add(psave); //05Dec2019 - Bindu
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setMotherSymptoms() {
        arrLSymptomsMother = new ArrayList<>();
        arrLSymptomsMotherSave = new ArrayList<>();

        insertMotherSignsInvestVal = new LinkedHashMap<>();

        if (aq.id(R.id.chktearinperineum).isChecked()) {
            arrLSymptomsMother.add(aq.id(R.id.chktearinperineum).getText().toString());
            arrLSymptomsMotherSave.add(getResources().getString(R.string.tearinperineumsave));
            insertMotherSignsInvestVal.put("153", new VisitDetailsPojo(getResources().getString(R.string.tearinperineum), "BEmOC"));
            bemocCnt++;
        }
        if (aq.id(R.id.chkinabilitytopassurine).isChecked()) {
            arrLSymptomsMother.add(aq.id(R.id.chkinabilitytopassurine).getText().toString());
            arrLSymptomsMotherSave.add(getResources().getString(R.string.inabilitytopassurinesave));
            insertMotherSignsInvestVal.put("154", new VisitDetailsPojo(getResources().getString(R.string.inabilitytopassurine), "BEmOC"));
            bemocCnt++;
        }
        if (aq.id(R.id.chkburninginurination).isChecked()) {
            arrLSymptomsMother.add(aq.id(R.id.chkburninginurination).getText().toString());
            arrLSymptomsMotherSave.add(getResources().getString(R.string.burninginurinationsave));
            insertMotherSignsInvestVal.put("155", new VisitDetailsPojo(getResources().getString(R.string.burninginurination), "BEmOC"));
            bemocCnt++;
        }
        if (aq.id(R.id.chkdifficultyinbreastfeeding).isChecked()) {
            arrLSymptomsMother.add(aq.id(R.id.chkdifficultyinbreastfeeding).getText().toString());
            arrLSymptomsMotherSave.add(getResources().getString(R.string.difficultyinbreastfeedingsave));
            insertMotherSignsInvestVal.put("156", new VisitDetailsPojo(getResources().getString(R.string.difficultyinbreastfeeding), "BEmOC"));
            bemocCnt++;
        }
        if (aq.id(R.id.chkpaininlowerabdomen).isChecked()) {
            arrLSymptomsMother.add(aq.id(R.id.chkpaininlowerabdomen).getText().toString());
            arrLSymptomsMotherSave.add(getResources().getString(R.string.paininlowerabdomensave));
            insertMotherSignsInvestVal.put("157", new VisitDetailsPojo(getResources().getString(R.string.paininlowerabdomen), "BEmOC"));
            bemocCnt++;
        }
        if (aq.id(R.id.chkpusformation).isChecked()) {
            arrLSymptomsMother.add(aq.id(R.id.chkpusformation).getText().toString());
            arrLSymptomsMotherSave.add(getResources().getString(R.string.pusformationsave));
            insertMotherSignsInvestVal.put("158", new VisitDetailsPojo(getResources().getString(R.string.pusformation), "BEmOC"));
            bemocCnt++;
        }
        if (aq.id(R.id.chkexcessivevaginalbleeding).isChecked()) {
            arrLSymptomsMother.add(aq.id(R.id.chkexcessivevaginalbleeding).getText().toString());
            arrLSymptomsMotherSave.add(getResources().getString(R.string.excessivevaginalbleedingsave));
            insertMotherSignsInvestVal.put("159", new VisitDetailsPojo(getResources().getString(R.string.excessivevaginalbleeding), getResources().getString(R.string.strcemoc)));
            cemocCnt++;
        }
        if (aq.id(R.id.chkinabilitytocontroldefecationorurine).isChecked()) {
            arrLSymptomsMother.add(aq.id(R.id.chkinabilitytocontroldefecationorurine).getText().toString());
            arrLSymptomsMotherSave.add(getResources().getString(R.string.inabilitytocontroldefecationorurinesave));
            insertMotherSignsInvestVal.put("160", new VisitDetailsPojo(getResources().getString(R.string.inabilitytocontroldefecationorurine), getResources().getString(R.string.strcemoc)));
            cemocCnt++;
        }
        if (aq.id(R.id.chkfoulsmellingvaginaldischarge).isChecked()) {
            arrLSymptomsMother.add(aq.id(R.id.chkfoulsmellingvaginaldischarge).getText().toString());
            arrLSymptomsMotherSave.add(getResources().getString(R.string.foulsmellingvaginaldischargesave));
            insertMotherSignsInvestVal.put("161", new VisitDetailsPojo(getResources().getString(R.string.foulsmellingvaginaldischarge), getResources().getString(R.string.strcemoc)));
            cemocCnt++;
        }
        if (aq.id(R.id.chkbreastsproblems).isChecked()) {
            arrLSymptomsMother.add(aq.id(R.id.chkbreastsproblems).getText().toString());
            arrLSymptomsMotherSave.add(getResources().getString(R.string.breastsproblemssave));
            insertMotherSignsInvestVal.put("162", new VisitDetailsPojo(getResources().getString(R.string.breastsproblems), getResources().getString(R.string.strcemoc)));
            cemocCnt++;
        }
        if (aq.id(R.id.chkdiffcultyinbreathing).isChecked()) {
            arrLSymptomsMother.add(aq.id(R.id.chkdiffcultyinbreathing).getText().toString());
            arrLSymptomsMotherSave.add(getResources().getString(R.string.diffcultyinbreathingsave));
            insertMotherSignsInvestVal.put("163", new VisitDetailsPojo(getResources().getString(R.string.difficult_in_breathing), getResources().getString(R.string.strcemoc)));
            cemocCnt++;
        }
        if (aq.id(R.id.chkswollenhands).isChecked()) {
            arrLSymptomsMother.add(aq.id(R.id.chkswollenhands).getText().toString());
            arrLSymptomsMotherSave.add(getResources().getString(R.string.swollenhandssave));
            insertMotherSignsInvestVal.put("164", new VisitDetailsPojo(getResources().getString(R.string.swollenhands), getResources().getString(R.string.strcemoc)));
            cemocCnt++;
        }

        //09Dec2019 - Bindu - fits and fever
        if (aq.id(R.id.chkmotherfits).isChecked()) {
            arrLSymptomsMother.add(aq.id(R.id.chkmotherfits).getText().toString());
            arrLSymptomsMotherSave.add(getResources().getString(R.string.fitssave));
            insertMotherSignsInvestVal.put("165", new VisitDetailsPojo(getResources().getString(R.string.fits), getResources().getString(R.string.strcemoc)));
            cemocCnt++;
        }

        if (aq.id(R.id.chkmotherfever).isChecked()) {
            arrLSymptomsMother.add(aq.id(R.id.chkmotherfever).getText().toString());
            arrLSymptomsMotherSave.add(getResources().getString(R.string.feversave));
            insertMotherSignsInvestVal.put("166", new VisitDetailsPojo(getResources().getString(R.string.fever), getResources().getString(R.string.strcemoc)));
            cemocCnt++;
        }

        if (aq.id(R.id.chkfainting).isChecked()) {
            arrLSymptomsMother.add(aq.id(R.id.chkfainting).getText().toString());
            arrLSymptomsMotherSave.add(getResources().getString(R.string.faintingsave));
            insertMotherSignsInvestVal.put("167", new VisitDetailsPojo(getResources().getString(R.string.fainting), getResources().getString(R.string.strcemoc)));
            cemocCnt++;
        }

        if (aq.id(R.id.chkothersymptomsmother).isChecked()) {
            arrLSymptomsMother.add(aq.id(R.id.chkothersymptomsmother).getText().toString() + " : " + aq.id(R.id.etothersymptomsmother).getText().toString());
            arrLSymptomsMotherSave.add(getResources().getString(R.string.otherssave) + " : " + aq.id(R.id.etothersymptomsmother).getText().toString());
            insertMotherSignsInvestVal.put("168", new VisitDetailsPojo(aq.id(R.id.etothersymptomsmother).getText().toString(), getResources().getString(R.string.strcemoc)));
            cemocCnt++;
        }
    }

    // Blink the textview
    private void startAnimation() {
        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(500); // manage the blinking time with this parameter
        anim.setStartOffset(20);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        aq.id(R.id.txtapprefferalcentre).getTextView().startAnimation(anim);
    }

    private void setFacilityNames(String factype) throws Exception {
        facilitynamelist = new ArrayList<>();
        FacilityRepository facilityRepo = new FacilityRepository(databaseHelper);
        mapFacilityname = facilityRepo.getReferralFacilityNames(factype, appState.sessionUserId);
        facilitynamelist.add(getResources().getString(R.string.selectfacilityname));
        for (Map.Entry<String, String> village : mapFacilityname.entrySet())
            //facilitynamelist.add(village.getKey());
            facilitynamelist.add(village.getValue());
        facilitynamelist.add(getResources().getString(R.string.other));
        ArrayAdapter<String> FacNameAdapter = new ArrayAdapter<String>(PncHomeVisit.this,
                R.layout.simple_spinner_dropdown_item, facilitynamelist);
        FacNameAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        aq.id(R.id.spnfacnameHv).adapter(FacNameAdapter);
    }

    //Prepare data for Visit Header
    private void prepareVisitHeaderData() {
        try {
            tblvisH = new TblVisitHeader();
            hmvisitregdetails = new tblregisteredwomen();

            tblvisH.setUserId(user.getUserId());
            tblvisH.setWomanId(woman.getWomanId());
            tblvisH.setVisitNum(visitNum);
            tblvisH.setVisHVisitType(woman.getRegPregnantorMother() == 1 ? getResources().getString(R.string.stranc) : getResources().getString(R.string.strpnc));
            tblvisH.setVisHVisitDate(aq.id(R.id.etvisitdate).getText().toString());
            tblvisH.setVisHStatusAtVisit(strstatus);
            tblvisH.setVisHVisitIsAdviseGiven(aq.id(R.id.chkisAdviceGiven).isChecked() ? getResources().getString(R.string.yesshortform) : getResources().getString(R.string.noshortform));
            tblvisH.setVisHVisitIsReferred(aq.id(R.id.chkisreferred).isChecked() ? getResources().getString(R.string.yesshortform) : getResources().getString(R.string.noshortform));

            String sel = "";
            if (aq.id(R.id.spnfacnameHv).getSelectedItem() != null)
                sel = aq.id(R.id.spnfacnameHv).getSelectedItem().toString();
            if (sel.trim().length() > 0 && !(sel.equalsIgnoreCase(getResources().getString(R.string.select)) || sel.equalsIgnoreCase(getResources().getString(R.string.other)))) {
                tblvisH.setVisHVisitReferredToFacilityName(aq.id(R.id.spnfacnameHv).getSelectedItem().toString());
                tblvisH.setVisHVisitReferredToFacilityType(aq.id(R.id.spnfactype).getSelectedItem().toString()); //08Apr2021
                /*for (Map.Entry<String, String> village : mapFacilityname.entrySet()) {
                    if (aq.id(R.id.spnfacnameHv).getSelectedItem().toString().equalsIgnoreCase(village.getValue()))
                        tblvisH.setVisHVisitReferredToFacilityType(village.getKey());
                }*/ //08Apr2021 Bindu
            } else if (sel.equalsIgnoreCase(getResources().getString(R.string.other))
                    && aq.id(R.id.etreffacilityname).getText().toString().trim().length() <= 0) {
                tblvisH.setVisHVisitReferredToFacilityName(aq.id(R.id.etreffacilityname).getText().toString());
                tblvisH.setVisHVisitReferredToFacilityType(getResources().getString(R.string.other));
            } else {
                tblvisH.setVisHVisitReferredToFacilityName(aq.id(R.id.etreffacilityname).getText().toString());
                if (aq.id(R.id.etreffacilityname).getText().toString().length() > 0)
                    tblvisH.setVisHVisitReferredToFacilityType(getResources().getString(R.string.other));
                else
                    tblvisH.setVisHVisitReferredToFacilityType("");
            }

            String bpsys = aq.id(R.id.etbpsystolicsumm).getText().toString();
            String bpdias = aq.id(R.id.etbpdiastolicsumm).getText().toString();

            if (bpsys.trim().length() > 0 && bpdias.trim().length() > 0) {
                tblvisH.setVisHBP(bpsys + "/" + bpdias);
                tblvisH.setVisHBPDate(aq.id(R.id.etbpdonedate).getText().toString());
            }
            tblvisH.setVisHTemperature(aq.id(R.id.etTemp).getText().toString());
            tblvisH.setVisHTemperatureDate(aq.id(R.id.ettempdonedate).getText().toString());
            tblvisH.setVisHHb(aq.id(R.id.ethb).getText().toString());
            tblvisH.setVisHHbDate(aq.id(R.id.ethbdonedate).getText().toString());
            tblvisH.setVisHPulse(aq.id(R.id.etpulse).getText().toString());
            tblvisH.setVisHPulseDate(aq.id(R.id.etpulsedate).getText().toString());

            tblvisH.setVisHVisitComments(aq.id(R.id.etvisitcomments).getText().toString());
            String childsymp = "";
            childsymp = aq.id(R.id.txtDangSignsNbE).getText().toString();
            if (arrLSymptomsMotherSave.size() > 0 || (childsymp!= null && childsymp.trim().length() > 0)) //22Jul2021 Bindu - PNC HV COMPL - both mother and child check
                tblvisH.setVisHCompl(getResources().getString(R.string.yesshortform));
            else
                tblvisH.setVisHCompl(getResources().getString(R.string.noshortform));
            tblvisH.setVisHResult(aq.id(R.id.txtDangSignsE).getText().toString()); //05Dec2019 - Bindu
            tblvisH.setVisHAdvise(aq.id(R.id.txtactionsumm).getText().toString());
            tblvisH.setVisHChildSymptoms(aq.id(R.id.txtDangSignsNbE).getText().toString()); //05Dec2019 - Bindu
            tblvisH.setVisHUserType(appState.userType);
            tblvisH.setRecordcreateddate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
            tblvisH.setRecordupdateddate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
            tblvisH.setVisHReferralSlipNumber("" + aq.id(R.id.etrefslipnumber).getText().toString()); //08Apr2021 Bindu add referal slip number

            // Update tblregisteredwomen details related to home visit date
            if (woman.getRegPregnantorMother() == 2) {
                hmvisitregdetails.setLastPncVisitDate(aq.id(R.id.etvisitdate).getText().toString());
            }

            if (woman.getIsCompl() != null && woman.getIsCompl().equalsIgnoreCase(getResources().getString(R.string.yesshortform))) {
                hmvisitregdetails.setIsCompl(getResources().getString(R.string.yesshortform));
            } else {
                if (arrLSymptomsMotherSave.size() > 0)
                    hmvisitregdetails.setIsCompl(getResources().getString(R.string.yesshortform));
                else {
                    hmvisitregdetails.setIsCompl(getResources().getString(R.string.noshortform));
                }
            }

            if (woman.getIsReferred() != null && woman.getIsReferred().equalsIgnoreCase(getResources().getString(R.string.yesshortform)))
                hmvisitregdetails.setIsReferred(getResources().getString(R.string.yesshortform));
            else {
                if (aq.id(R.id.chkisreferred).isChecked())
                    hmvisitregdetails.setIsReferred(getResources().getString(R.string.yesshortform));
                else {
                    hmvisitregdetails.setIsReferred(getResources().getString(R.string.noshortform));
                }
            }

            //mani 18June2021
            String ashaavailability="";
            if (aq.id(R.id.ashavailableYes).isChecked())
                ashaavailability="Y";
            else if (aq.id(R.id.ashavailableNo).isChecked())
                ashaavailability="N";
            else if (aq.id(R.id.ashavailableDontKnow).isChecked())
                ashaavailability="Dont Know";
            tblvisH.setVisHAshaAvailable(ashaavailability);

            hmvisitregdetails.setRegUserType(appState.userType);
            hmvisitregdetails.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    //Prepare data for Mother Visit Details
    private void prepareMotherVisitDetailsData() {
        try {
            tblvisD = new TblVisitDetails();
            tblvisD.setUserId(user.getUserId());
            tblvisD.setWomanId(woman.getWomanId());
            tblvisD.setVisitNum(visitNum);
            tblvisD.setVisDVisitType(woman.getRegPregnantorMother() == 1 ? getResources().getString(R.string.stranc) : getResources().getString(R.string.strpnc));
            tblvisD.setVisDVisitDate(aq.id(R.id.etvisitdate).getText().toString());

            tblvisD.setVisDReferredToFacilityType(""); //To be corrected

            tblvisD.setVisHUserType(appState.userType);
            tblvisD.setRecordcreateddate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
            tblvisD.setRecordupdateddate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private void prepareNbVisitDetailsData() {
        try {
            tblCvisD = new tblChildVisitDetails();
            tblCvisD.setUserId(user.getUserId());
            tblCvisD.setWomanId(woman.getWomanId());
            tblCvisD.setVisitNum(visitNum);
            tblCvisD.setVisDVisitDate(aq.id(R.id.etvisitdate).getText().toString());

            tblCvisD.setVisDReferredToFacilityType(""); //To be corrected

            tblCvisD.setVisHUserType(appState.userType);
            tblCvisD.setRecordcreateddate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
            tblCvisD.setRecordupdateddate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    // Show Referral details
    private void showreferaldetails() throws Exception {
        if (aq.id(R.id.chkisreferred).isChecked()) {
            aq.id(R.id.trspnfacname).visible();
            aq.id(R.id.tretfacname).gone();
            //08Apr2021 Bindu
            aq.id(R.id.trspnfactype).visible();
            aq.id(R.id.trrefslipno).visible();
            setFacilityType();
        } else {
            aq.id(R.id.spnfacnameHv).setSelection(0);
            aq.id(R.id.trspnfacname).gone();
            aq.id(R.id.tretfacname).gone();
            aq.id(R.id.etreffacilityname).text("");
            //08Apr2021 Bindu
            aq.id(R.id.spnfactype).setSelection(0);
            aq.id(R.id.trspnfactype).gone();
            aq.id(R.id.trrefslipno).gone();
            aq.id(R.id.etrefslipnumber).text("");
        }
    }

    // 20Sep2019 - Bindu - Clear Date for specfic Edittext and hide or display brush
    private void clearDate(EditText editText, ImageView imageView) {
        try {
            editText.setText("");
            aq.id(imageView).gone();
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    // Text watcher
    TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            try {
                if (s == aq.id(R.id.etTemp).getEditable()) {
                    enabledisabledatefields(aq.id(R.id.etTemp).getEditText(), aq.id(R.id.ettempdonedate).getEditText(), aq.id(R.id.imgcleardatetemp).getImageView());
                    if (aq.id(R.id.etTemp).getText().toString().length() > 0
                            && Double.parseDouble(aq.id(R.id.etTemp).getText().toString()) <= 0) {
                        aq.id(R.id.etTemp).getEditText().setError(getResources().getString(R.string.incorrectinput));
                    } else {
                        aq.id(R.id.etTemp).getEditText().setError(null);
                    }
                } else if (s == aq.id(R.id.etpulse).getEditable()) {      //04Oct2019 - Bindu  - add pulse
                    enabledisabledatefields(aq.id(R.id.etpulse).getEditText(), aq.id(R.id.etpulsedate).getEditText(), aq.id(R.id.imgcleardatepulse).getImageView());
                    if (aq.id(R.id.etpulse).getText().toString().length() > 0
                            && Integer.parseInt(aq.id(R.id.etpulse).getText().toString()) <= 0) {
                        aq.id(R.id.etpulse).getEditText().setError(getResources().getString(R.string.incorrectinput));
                    } else {
                        aq.id(R.id.etpulse).getEditText().setError(null);
                    }
                } else if (s == aq.id(R.id.etbpsystolicsumm).getEditable()) {
                    enabledisabledatefields(aq.id(R.id.etbpsystolicsumm).getEditText(), aq.id(R.id.etbpdonedate).getEditText(), aq.id(R.id.imgcleardatebp).getImageView());
                    if (aq.id(R.id.etbpsystolicsumm).getText().toString().length() > 0
                            && Integer.parseInt(aq.id(R.id.etbpsystolicsumm).getText().toString()) <= 0) {
                        aq.id(R.id.etbpsystolicsumm).getEditText().setError(getResources().getString(R.string.incorrectinput));
                    } else {
                        aq.id(R.id.etbpsystolicsumm).getEditText().setError(null);
                    }
                } else if (s == aq.id(R.id.etbpdiastolicsumm).getEditable()) {
                    enabledisabledatefields(aq.id(R.id.etbpdiastolicsumm).getEditText(), aq.id(R.id.etbpdonedate).getEditText(), aq.id(R.id.imgcleardatebp).getImageView());
                    if (aq.id(R.id.etbpdiastolicsumm).getText().toString().length() > 0
                            && Integer.parseInt(aq.id(R.id.etbpdiastolicsumm).getText().toString()) <= 0) {
                        aq.id(R.id.etbpdiastolicsumm).getEditText().setError(getResources().getString(R.string.incorrectinput));
                    } else {
                        aq.id(R.id.etbpdiastolicsumm).getEditText().setError(null);
                    }
                } else if (s == aq.id(R.id.ethb).getEditable()) {
                    enabledisabledatefields(aq.id(R.id.ethb).getEditText(), aq.id(R.id.ethbdonedate).getEditText(), aq.id(R.id.imgcleardatehb).getImageView());
                    if (aq.id(R.id.ethb).getText().toString().length() > 0
                            && Double.parseDouble(aq.id(R.id.ethb).getText().toString()) <= 0) {
                        aq.id(R.id.ethb).getEditText().setError(getResources().getString(R.string.incorrectinput));
                    } else {
                        aq.id(R.id.ethb).getEditText().setError(null);
                    }
                }
                //Baby weight
                else if (s == aq.id(R.id.etweightbaby1).getEditable()) {
                    enabledisabledatefieldsBaby(aq.id(R.id.etweightbaby1).getEditText(), aq.id(R.id.etweightdatebaby1).getEditText(), aq.id(R.id.imgcleardateweightbaby1).getImageView());
                    CheckValidationOnExamination(aq.id(R.id.etweightbaby1).getEditText());

                    //24Dec2019 - Bindu - weight validation
                    if (aq.id(R.id.etweightbaby1).getText().toString().length() > 0
                            && Double.parseDouble(aq.id(R.id.etweightbaby1).getText().toString()) <= 999) {
                        aq.id(R.id.etweightbaby1).getEditText()
                                .setError(getResources().getString(R.string.strweight) + aq.id(R.id.etweightbaby1).getText().toString() + getResources().getString(R.string.etlastchildWeight));
                    }

                } else if (s == aq.id(R.id.etweightbaby2).getEditable()) {
                    enabledisabledatefieldsBaby(aq.id(R.id.etweightbaby2).getEditText(), aq.id(R.id.etweightdatebaby2).getEditText(), aq.id(R.id.imgcleardateweightbaby2).getImageView());
                    CheckValidationOnExamination(aq.id(R.id.etweightbaby2).getEditText());
                    //24Dec2019 - Bindu - weight validation
                    if (aq.id(R.id.etweightbaby2).getText().toString().length() > 0
                            && Double.parseDouble(aq.id(R.id.etweightbaby2).getText().toString()) <= 999) {
                        aq.id(R.id.etweightbaby2).getEditText()
                                .setError(getResources().getString(R.string.strweight) + aq.id(R.id.etweightbaby2).getText().toString() + getResources().getString(R.string.etlastchildWeight));
                    }
                } else if (s == aq.id(R.id.etweightbaby3).getEditable()) {
                    enabledisabledatefieldsBaby(aq.id(R.id.etweightbaby3).getEditText(), aq.id(R.id.etweightdatebaby3).getEditText(), aq.id(R.id.imgcleardateweightbaby3).getImageView());
                    CheckValidationOnExamination(aq.id(R.id.etweightbaby3).getEditText());
                    //24Dec2019 - Bindu - weight validation
                    if (aq.id(R.id.etweightbaby3).getText().toString().length() > 0
                            && Double.parseDouble(aq.id(R.id.etweightbaby3).getText().toString()) <= 999) {
                        aq.id(R.id.etweightbaby3).getEditText()
                                .setError(getResources().getString(R.string.strweight) + aq.id(R.id.etweightbaby3).getText().toString() + getResources().getString(R.string.etlastchildWeight));
                    }
                } else if (s == aq.id(R.id.etweightbaby4).getEditable()) {
                    enabledisabledatefieldsBaby(aq.id(R.id.etweightbaby4).getEditText(), aq.id(R.id.etweightdatebaby4).getEditText(), aq.id(R.id.imgcleardateweightbaby4).getImageView());
                    CheckValidationOnExamination(aq.id(R.id.etweightbaby4).getEditText());
                    //24Dec2019 - Bindu - weight validation
                    if (aq.id(R.id.etweightbaby4).getText().toString().length() > 0
                            && Double.parseDouble(aq.id(R.id.etweightbaby4).getText().toString()) <= 999) {
                        aq.id(R.id.etweightbaby4).getEditText()
                                .setError(getResources().getString(R.string.strweight) + aq.id(R.id.etweightbaby4).getText().toString() + getResources().getString(R.string.etlastchildWeight));
                    }
                }
                //Baby temp
                else if (s == aq.id(R.id.etTempbaby1).getEditable()) {
                    enabledisabledatefieldsBaby(aq.id(R.id.etTempbaby1).getEditText(), aq.id(R.id.ettempdonedatebaby1).getEditText(), aq.id(R.id.imgcleardatetempbaby1).getImageView());
                    CheckValidationOnExamination(aq.id(R.id.etTempbaby1).getEditText());
                } else if (s == aq.id(R.id.etTempbaby2).getEditable()) {
                    enabledisabledatefieldsBaby(aq.id(R.id.etTempbaby2).getEditText(), aq.id(R.id.ettempdonedatebaby2).getEditText(), aq.id(R.id.imgcleardatetempbaby2).getImageView());
                    CheckValidationOnExamination(aq.id(R.id.etTempbaby2).getEditText());
                } else if (s == aq.id(R.id.etTempbaby3).getEditable()) {
                    enabledisabledatefieldsBaby(aq.id(R.id.etTempbaby3).getEditText(), aq.id(R.id.ettempdonedatebaby3).getEditText(), aq.id(R.id.imgcleardatetempbaby3).getImageView());
                    CheckValidationOnExamination(aq.id(R.id.etTempbaby3).getEditText());
                } else if (s == aq.id(R.id.etTempbaby4).getEditable()) {
                    enabledisabledatefieldsBaby(aq.id(R.id.etTempbaby4).getEditText(), aq.id(R.id.ettempdonedatebaby4).getEditText(), aq.id(R.id.imgcleardatetempbaby4).getImageView());
                    CheckValidationOnExamination(aq.id(R.id.etTempbaby4).getEditText());
                }
                //Baby Resp rate
                else if (s == aq.id(R.id.etrespratebaby1).getEditable()) {
                    enabledisabledatefieldsBaby(aq.id(R.id.etrespratebaby1).getEditText(), aq.id(R.id.etrespratedonedatebaby1).getEditText(), aq.id(R.id.imgcleardaterespratebaby1).getImageView());
                    CheckValidationOnExamination(aq.id(R.id.etrespratebaby1).getEditText());
                } else if (s == aq.id(R.id.etrespratebaby2).getEditable()) {
                    enabledisabledatefieldsBaby(aq.id(R.id.etrespratebaby2).getEditText(), aq.id(R.id.etrespratedonedatebaby2).getEditText(), aq.id(R.id.imgcleardaterespratebaby2).getImageView());
                    CheckValidationOnExamination(aq.id(R.id.etrespratebaby2).getEditText());
                } else if (s == aq.id(R.id.etrespratebaby3).getEditable()) {
                    enabledisabledatefieldsBaby(aq.id(R.id.etrespratebaby3).getEditText(), aq.id(R.id.etrespratedonedatebaby3).getEditText(), aq.id(R.id.imgcleardaterespratebaby3).getImageView());
                    CheckValidationOnExamination(aq.id(R.id.etrespratebaby3).getEditText());
                } else if (s == aq.id(R.id.etrespratebaby4).getEditable()) {
                    enabledisabledatefieldsBaby(aq.id(R.id.etrespratebaby4).getEditText(), aq.id(R.id.etrespratedonedatebaby4).getEditText(), aq.id(R.id.imgcleardaterespratebaby4).getImageView());
                    CheckValidationOnExamination(aq.id(R.id.etrespratebaby4).getEditText());
                }
                //child reg id
                else if (s == aq.id(R.id.etchildregid1).getEditable()) {
                    enabledisabledatefieldsBaby(aq.id(R.id.etchildregid1).getEditText(), aq.id(R.id.etchildregdate1).getEditText(), aq.id(R.id.imgclearchildregdate1).getImageView());
                    CheckValidationOnExamination(aq.id(R.id.etchildregid1).getEditText());
                } else if (s == aq.id(R.id.etchildregid2).getEditable()) {
                    enabledisabledatefieldsBaby(aq.id(R.id.etchildregid2).getEditText(), aq.id(R.id.etchildregdate2).getEditText(), aq.id(R.id.imgclearchildregdate2).getImageView());
                    CheckValidationOnExamination(aq.id(R.id.etchildregid2).getEditText());
                } else if (s == aq.id(R.id.etchildregid3).getEditable()) {
                    enabledisabledatefieldsBaby(aq.id(R.id.etchildregid3).getEditText(), aq.id(R.id.etchildregdate3).getEditText(), aq.id(R.id.imgclearchildregdate3).getImageView());
                    CheckValidationOnExamination(aq.id(R.id.etchildregid3).getEditText());
                } else if (s == aq.id(R.id.etchildregid4).getEditable()) {
                    enabledisabledatefieldsBaby(aq.id(R.id.etchildregid4).getEditText(), aq.id(R.id.etchildregdate4).getEditText(), aq.id(R.id.imgclearchildregdate4).getImageView());
                    CheckValidationOnExamination(aq.id(R.id.etchildregid4).getEditText());
                }
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            try {

            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
        }
    };

    //Bindu - enable disable date fields
    private void enabledisabledatefields(EditText etval, EditText etvaldate, ImageView imgcleardate) {
        if (etval.getText().toString().trim().length() > 0) {
            etvaldate.setEnabled(true);
            etvaldate.setBackgroundResource(R.drawable.editext_none);
        } else {
            etvaldate.setEnabled(false);
            etvaldate.setText("");
            imgcleardate.setVisibility(View.GONE);
            etvaldate.setBackgroundResource(R.drawable.editextdisable_none);
        }
    }

    private void enabledisabledatefieldsBaby(EditText etval, EditText etvaldate, ImageView imgcleardate) {

        if (etval.getText().toString().trim().length() > 0) {
            etvaldate.setEnabled(true);
            etvaldate.setBackgroundResource(R.drawable.editext_none);
        } else {
            etvaldate.setEnabled(false);
            etvaldate.setText("");
            imgcleardate.setVisibility(View.GONE);
            etvaldate.setBackgroundResource(R.drawable.editextdisable_none);

        }

    }

    private void CheckValidationOnExamination(EditText nbtestname) {

        if (nbtestname.getText().toString().length() > 0
                && Double.parseDouble(nbtestname.getText().toString()) <= 0) {
            nbtestname.setError(getResources().getString(R.string.incorrectinput));
        } else {
            nbtestname.setError(null);
        }

    }

    @Override
    public void onBackPressed() {

    }

    /**
     * Confirm Alert on Click save
     */
    private void confirmAlert() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set dialog message
        String mess = getResources().getString(R.string.m099);
        alertDialogBuilder.setMessage(mess).setCancelable(false)
                .setNegativeButton((getResources().getString(R.string.m121)),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                try {
                                    savedata();
                                } catch (Exception e) {
                                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                                    crashlytics.log(e.getMessage());
                                }
                            }
                        })
                .setPositiveButton((getResources().getString(R.string.m122)),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    //04Dec2019 - Bindu
    private void sendSMS(TransactionHeaderRepository transRepo, DatabaseHelper databaseHelper, TblVisitHeader tblvisH) {
        try {
            String p_no = "";
            List<TblContactDetails> values;

            SettingsRepository settingsRepository = new SettingsRepository(databaseHelper);

            values = settingsRepository.getSpecificPhoneNumber("PNC Home Visit");//13May2021 Arpitha
            p_no = "";
            for (int i = 0; i < values.size(); i++) {

                String s = values.get(i).getContactNumber();
                p_no = p_no + values.get(i).getContactNumber() + ",";
            }

            // 11Dec2019 - Bindu  Get Hamlet name
            FacilityRepository facilityRepo = new FacilityRepository(databaseHelper);
            String hamletname = facilityRepo.getHamletName(woman.getRegVillage());

            String iscompl = "", isref = "";
            //  iscompl = " , C - " + tblvisH.getVisHCompl();
            if (tblvisH.getVisHResult() != null && tblvisH.getVisHResult().trim().length() > 0)
                iscompl = ", Mo.C - " + "Y";
            if (tblvisH.getVisHChildSymptoms() != null && tblvisH.getVisHChildSymptoms().trim().length() > 0)
                iscompl = iscompl + ", Nb.C - " + "Y";

            if (aq.id(R.id.chkisreferred).isChecked())
                isref = ", Ref - " + tblvisH.getVisHVisitIsReferred() + " , To - " + tblvisH.getVisHVisitReferredToFacilityName();

        /*if(arrLSymptomsMotherSave.size() > 0)
            iscompl = " , C - " + "Y" ;

        if (aq.id(R.id.chkisreferred).isChecked())
            isref = " , Ref - " + "Y" + ", To = " + aq.id(R.id.spnfacnameHv).getSelectedItem().toString();*/

//        smsContent = "Matruksha :-" + woman.getRegWomanName() + ", " + hamletname + ", " + iscompl + isref + ", ASHA - " + user.getUserName() + " ," + user.getUserMobileNumber(); //24Dec2019 - bindu - add asha name

            //13May2021 Arpitha
            String uniqueId = "";
            if (woman.getRegUIDNo() != null && woman.getRegUIDNo().trim().length() > 0) {
                uniqueId = "MCP:" + woman.getRegUIDNo();
            } else if (woman.getRegAadharCard() != null && woman.getRegAadharCard().trim().length() > 0) {
                uniqueId = "A:" + woman.getRegAadharCard();
            } else if (woman.getRegPhoneNumber() != null && woman.getRegPhoneNumber().trim().length() > 0) {
                uniqueId = "Ph:" + woman.getRegPhoneNumber();
            }

            String smsContent = "";
//        String ref = aq.id(R.id.chkisreferred).isChecked() ? getResources().getString(R.string.yesshortform) : getResources().getString(R.string.noshortform);
//        if (woman.getRegPregnantorMother() == 1)
            /*smsContent = "Sentiatend:" + appState.userType + ", " + uniqueId
                    + ", " + woman.getWomanId() + ", " + "PW-"
                    + woman.getRegWomanName()+", V # :" +visitNum+", V Dt:"
                    +aq.id(R.id.etvisitdate).getText().toString()
                    +", Ref:"+ref
                    + ", " + user.getFacilityName() + ", HV";*/
            //16May2021 Bindu change sms content
            smsContent = "Sentiatend :-" + woman.getRegWomanName() + ", " + uniqueId +
                    ", WID- " + woman.getWomanId() + ", V# :" + visitNum + ", V Dt: " + aq.id(R.id.etvisitdate).getText().toString()
                    + ", LM "
                    + ", " + hamletname + iscompl + isref + ", " + appState.userType + "- " + user.getUserName()
                    + " ," + user.getUserMobileNumber() + "- (HV)";

            if (p_no.length() <= 0) {
                display_messagedialog(transRepo, databaseHelper, tblvisH, smsContent);
            } else
                sending(transRepo, databaseHelper, p_no, tblvisH, smsContent);

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    // 13Oct2016 Arpitha
    public void display_messagedialog(final TransactionHeaderRepository
                                              transactionHeaderRepository,
                                      final DatabaseHelper databaseHelper,
                                      final TblVisitHeader tblvisH, String smsContent) throws Exception {
        try {
            final Dialog sms_dialog = new Dialog(this);
//            sms_dialog.setTitle(getResources().getString(R.string.plsentervalidphoneno));
            sms_dialog.setContentView(R.layout.sms_dialog);
            sms_dialog.show();

            ImageButton imgsend = (ImageButton) sms_dialog.findViewById(R.id.imgsend);
            ImageButton imgcancel = (ImageButton) sms_dialog.findViewById(R.id.imgcancel);
            etphn = (EditText) sms_dialog.findViewById(R.id.etphno);
            final EditText etmess = (EditText) sms_dialog.findViewById(R.id.etmess);
            TextView txtcontcats = (TextView) sms_dialog.findViewById(R.id.txtcontacts);


//            etmess.setVisibility(View.GONE);
            etmess.setText(smsContent);
            etmess.setEnabled(false);
            txtcontcats.setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    // TODO Auto-generated method stub
                    Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                    startActivity(intent);
                    return true;
                }
            });

            imgsend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        sending(transactionHeaderRepository, databaseHelper,

                                etphn.getText().toString(), tblvisH, smsContent);
                    } catch (Exception e) {
                        e.printStackTrace();
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }
                }
            });
            imgcancel.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    sms_dialog.cancel();
                    //21Ma2021 Arpitha
                    Intent intent = new Intent(PncHomeVisit.this, HomeVisitListActivity.class);
                    intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    intent.putExtra("woman", woman);
                    startActivity(intent);
                }
            });
            sms_dialog.setCancelable(false);
        } catch (Exception e) {
            e.printStackTrace();
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }

    }

    void sending(TransactionHeaderRepository transRepo, DatabaseHelper databaseHelper,
                 String phnNo, TblVisitHeader tblvisH, String smsCont) throws Exception {
        String smsContent = smsCont;


        InserttblMessageLog(phnNo,
                smsContent, user.getUserId(), transRepo, databaseHelper);

        //21Ma2021 Arpitha
        Intent intent = new Intent(PncHomeVisit.this, HomeVisitListActivity.class);
        intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
        intent.putExtra("woman", woman);
        startActivity(intent);

        if (isMessageLogsaved) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.sending_sms),
                    Toast.LENGTH_LONG).show();
            sendSMSFunction();
        }

    }

    public void InserttblMessageLog(String phoneno, String content, String user_id,
                                    TransactionHeaderRepository transactionHeaderRepository, DatabaseHelper databaseHelper) throws Exception {
        final ArrayList<MessageLogPojo> mlpArr = new ArrayList<MessageLogPojo>();

        if (phoneno.length() > 0) {
            String[] phn = phoneno.split("\\,");
            for (int i = 0; i < phn.length; i++) {
                String num = phn[i];
                if (num != null && num.length() > 0) {
                    MessageLogPojo mlp =
                            new MessageLogPojo();
                    mlp.setUserId(user_id);
                    mlp.setMsgPhoneNo(num);
                    mlp.setWomanId(woman.getWomanId());
                    mlp.setMsgBody(content);
                    mlp.setMsgPriority(1);
                    mlp.setMsgNoOfFailures(0);
                    mlp.setTransId(transId);
                    mlp.setMsgSentDate(DateTimeUtil.getTodaysDate());
                    mlp.setMsgSent(0);
                    mlp.setMsgStage("Home visit - PNC");
                    mlp.setMsgUserType(appState.userType);
                    mlp.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
                    mlp.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());//21May2021 Arpitha
                    mlpArr.add(mlp);
                }
            }
        }
        int addMessage = 0;
        SMSRepository smsRepository = new SMSRepository(databaseHelper);
        if (mlpArr != null) {
            for (final MessageLogPojo mlpp : mlpArr) {
                addMessage = smsRepository.insertIntoMessageLog(mlpp, databaseHelper);
            }
            if (addMessage > 0) {
                isMessageLogsaved = true;
                boolean addRegTrans = transactionHeaderRepository.iNewRecordTransNew(user.getUserId(), transId,
                        "tblmessagelog", databaseHelper);
            }
        }
    }

    private void sendSMSFunction() throws Exception {
        messageSent = true;
        new SendSMS(this).checkAndSendMsg(databaseHelper);
    }

    //  07Dec2019 Bindu
    public int checkSimState() {
        int simState = -1;
        // This is to know the status of the Sim
        TelephonyManager telMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        simState = telMgr.getSimState();

        return simState;
    }

    //08Apr2021 Bindu set fac type and fac name - referral table
    private void setFacilityType() throws Exception {
        reffacilitytypelist = new ArrayList<>();
        RefFacilityRepository facilityRepo = new RefFacilityRepository(databaseHelper);
        mapRefFacilityType = facilityRepo.getRefFacilityType();
        reffacilitytypelist.add(getResources().getString(R.string.selectfacilityname));
        for (Map.Entry<String, String> village : mapRefFacilityType.entrySet())
            reffacilitytypelist.add(village.getValue());
        ArrayAdapter<String> FacTypeAdapter = new ArrayAdapter<String>(PncHomeVisit.this,
                R.layout.simple_spinner_dropdown_item, reffacilitytypelist);
        FacTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        aq.id(R.id.spnfactype).adapter(FacTypeAdapter);
    }

    private void setRefFacilityName(String factype) throws Exception {
        factype = aq.id(R.id.spnfactype).getSelectedItem().toString();
        reffacilitynamelist = new ArrayList<>();
        RefFacilityRepository facilityRepo = new RefFacilityRepository(databaseHelper);
        mapRefFacilityname = facilityRepo.getRefFacilityName(factype);
        reffacilitynamelist.add(getResources().getString(R.string.selectfacilityname));
        for (Map.Entry<String, String> village : mapRefFacilityname.entrySet())
            reffacilitynamelist.add(village.getValue());
        ArrayAdapter<String> FacTypeAdapter = new ArrayAdapter<String>(PncHomeVisit.this,
                R.layout.simple_spinner_dropdown_item, reffacilitynamelist);
        FacTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        aq.id(R.id.spnfacnameHv).adapter(FacTypeAdapter);
    }

    public void opencloseAccordion(View v, TextView txt) {
        try {
            if (v.getVisibility() == View.VISIBLE) {
                v.setVisibility(View.GONE);
                if (txt.getCurrentTextColor() == getResources().getColor(R.color.red))
                    txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.collapse, 0);
                else
                    txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
            } else {
                v.setVisibility(View.VISIBLE);
                if (txt.getCurrentTextColor() == getResources().getColor(R.color.red))
                    txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
                else
                    txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.expand, 0);

                //     txt.setTextColor(getResources().getColor(R.color.red));
            }
        } catch (Exception e) {

        }
    }

    //    16May2021 Bindu health symtpoms and covid vaccine
    private void hideshowdetailsanyhealthissuesymtpoms(TextView txt) {
        if (aq.id(R.id.rd_healthissuesYes).isChecked()) {
            aq.id(R.id.trHealthIssueYes).visible();
//            aq.id(R.id.trCovidResult).visible();
            aq.id(R.id.llCovidQs).getView().setVisibility(View.GONE); //18May2021 Bindu
            txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
            txt.setTextColor(getResources().getColor(R.color.red));
        } else if (aq.id(R.id.rd_healthissuesNo).isChecked()) {
            aq.id(R.id.trHealthIssueYes).gone();
            String[] healthproblems = getResources().getStringArray(R.array.healthproblemsuffer);
            spnHealthProblemshv.setItems(healthproblems);
            aq.id(R.id.llCovidQs).getView().setVisibility(View.GONE); //18May2021 Bindu
            aq.id(R.id.etVisitHealthIssuesYesOthers).text("");
            aq.id(R.id.trHealthProblemYesOthers).gone();
            aq.id(R.id.rd_CovidTestYes).checked(false);
            rgCovidTest.clearCheck(); //21May2021 Bindu
            hideshowcovidTestdetails();
//            aq.id(R.id.trCovidResult).gone();
            txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
            txt.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        }
    }

    private void hideshowcovidvaccinedetails(TextView txt) {
        if (aq.id(R.id.rd_CovidVaccineYes).isChecked()) {
            aq.id(R.id.trCovidVaccinDose).visible();
            txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
            txt.setTextColor(getResources().getColor(R.color.red));
        } else if (aq.id(R.id.rd_CovidVaccineNo).isChecked()) {
            aq.id(R.id.trCovidVaccinDose).gone();
            aq.id(R.id.trCovidFirstDoseDate).getView().setVisibility(View.GONE);
            aq.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.GONE);
            aq.id(R.id.spnVisitVaccineDose).setSelection(0);
            txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
            txt.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        }
    }

    //    16May2021 Bindu
    public static void hideshowcovidTestdetails() {
        if (aq.id(R.id.rd_CovidTestYes).isChecked()) {
            aq.id(R.id.trCovidTesDate).visible();
            aq.id(R.id.trCovidResult).visible();
            rgCovidResult.clearCheck();
        } else if (aq.id(R.id.rd_CovidTestNo).isChecked()) {
            aq.id(R.id.trCovidTesDate).gone();
            aq.id(R.id.trCovidResult).gone();
            aq.id(R.id.etVisitCovidTestDate).text("");
            aq.id(R.id.rd_CovidPositive).checked(false);
            aq.id(R.id.rd_CovidNegative).checked(false);
        } else {
            aq.id(R.id.trCovidTesDate).gone();
            aq.id(R.id.trCovidResult).gone();
        }
    }

    //    16May2021 Bindu covid healthissuecheck
    private boolean validatecovidhealthissue() throws Exception {
        boolean isvalid = true;
        if (!(isMotherMortality || isWomanDeactivated)) {
            if (!(aq.id(R.id.rd_healthissuesYes).isChecked() || aq.id(R.id.rd_healthissuesNo).isChecked())) {
                AlertDialogUtil.displayAlertMessage(
                        getResources().getString(R.string.plsenterecenthealthdetails)
                        , act);
                opencloseAccordion(aq.id(R.id.llhealthdetails).getView(), aq.id(R.id.txthealthdet).getTextView());
                aq.id(R.id.llhealthdetails).backgroundColor(getResources().getColor(R.color.lightgray));
                return false;
            }

            if (aq.id(R.id.rd_healthissuesYes).isChecked()) {
                String healthproblems = spnHealthProblemshv.getSelectedItemsAsString();
                /*|| healthproblems.contains(getResources().getString(R.string.select)*/
                if (healthproblems != null && healthproblems.length() <= 0) {
                    AlertDialogUtil.displayAlertMessage(
                            getResources().getString(R.string.plsselhealthissues)
                            , act);
                    return false;
                }
                if (!(aq.id(R.id.rd_CovidTestYes).isChecked() || aq.id(R.id.rd_CovidTestNo).isChecked())) {
                    AlertDialogUtil.displayAlertMessage(
                            getResources().getString(R.string.plsselectcovidtesttaken)
                            , act);
                    return false;
                }
            }

            if (aq.id(R.id.rd_CovidTestYes).isChecked()) {
                if (!(aq.id(R.id.rd_CovidPositive).isChecked() || aq.id(R.id.rd_CovidNegative).isChecked())) {
                    AlertDialogUtil.displayAlertMessage(
                            getResources().getString(R.string.plsselectcovidresult)
                            , act);
                    return false;
                }
                if (aq.id(R.id.etVisitCovidTestDate).getText().toString().trim().length() <= 0) {
                    AlertDialogUtil.displayAlertMessage(
                            getResources().getString(R.string.plsselectcovidtestdate)
                            , act);
                    return false;
                }
            }


        }
        return isvalid;
    }

    // insert to audi trail table
    private String checkForAuditTrail(int transId) throws Exception {

        String upSql = null;
       /* String addString = "";
        String editedValues = "";

        //09Apr2021 - Bindu UID, type, Date
        if (!covVacItems.get(0).getCovidVaccinated().equalsIgnoreCase(tblcovvac.getCovidVaccinated().toString())) {
            if (addString == "")
                addString = addString + " CovidVaccinated = " + (char) 34 +tblcovvac.getCovidVaccinated() + (char) 34 + "";
            else
                addString = addString + " ,CovidVaccinated = " + (char) 34 + tblcovvac.getCovidVaccinated() + (char) 34 + "";
            InserttblAuditTrailCovidVac("CovidVaccinated",
                    String.valueOf(covVacItems.get(0).getCovidVaccinated()),
                    String.valueOf(tblcovvac.getCovidVaccinated()),
                    transId);
        }
        if (!covVacItems.get(0).getCovidFirstDoseDate().equalsIgnoreCase(tblcovvac.getCovidFirstDoseDate().toString())) {
            if (addString == "")
                addString = addString + " CovidFirstDoseDate = " + (char) 34 +tblcovvac.getCovidFirstDoseDate() + (char) 34 + "";
            else
                addString = addString + " ,CovidFirstDoseDate = " + (char) 34 + tblcovvac.getCovidFirstDoseDate() + (char) 34 + "";
            InserttblAuditTrailCovidVac("CovidVaccinated",
                    String.valueOf(covVacItems.get(0).getCovidFirstDoseDate()),
                    String.valueOf(tblcovvac.getCovidFirstDoseDate()),
                    transId);
        }
        if (!covVacItems.get(0).getCovidSecondDoseDate().equalsIgnoreCase(tblcovvac.getCovidSecondDoseDate().toString())) {
            if (addString == "")
                addString = addString + " CovidSecondDoseDate = " + (char) 34 +tblcovvac.getCovidSecondDoseDate() + (char) 34 + "";
            else
                addString = addString + " ,CovidSecondDoseDate = " + (char) 34 + tblcovvac.getCovidSecondDoseDate() + (char) 34 + "";
            InserttblAuditTrailCovidVac("CovidSecondDoseDate",
                    String.valueOf(covVacItems.get(0).getCovidSecondDoseDate()),
                    String.valueOf(tblcovvac.getCovidSecondDoseDate()),
                    transId);
        }

        if (addString == "")
            addString = addString + " RecordUpdatedDate = " + (char) 34 + tblcovvac.getRecordUpdatedDate() + (char) 34 + "";
        else
            addString = addString + " ,RecordUpdatedDate = " + (char) 34 + tblcovvac.getRecordUpdatedDate() + (char) 34 + "";
        InserttblAuditTrailCovidVac("RecordUpdatedDate",
                String.valueOf(covVacItems.get(0).getRecordUpdatedDate()),
                String.valueOf(tblcovvac.getRecordUpdatedDate()),
                transId);

        if (addString != null && addString.length() > 0) {
            upSql = "UPDATE tblCovidVaccineDetails SET ";
            upSql = upSql + addString + " WHERE WomenId = '"
                    + appState.selectedWomanId + "' and UserId = '" + woman.getUserId() + "'";
        }*/
        return upSql;
    }

    private boolean InserttblAuditTrailCovidVac(String ColumnName, String Old_Value, String New_Value, int transId
    ) throws Exception {
        AuditPojo APJ = new AuditPojo();
        APJ.setUserId(appState.sessionUserId);
        APJ.setWomanId(woman.getWomanId());
        APJ.setTblName("tblCovidVaccineDetails");
        APJ.setDateChanged(DateTimeUtil.getTodaysDate());
        APJ.setColumnName(ColumnName);
        APJ.setOld_Value(Old_Value);
        APJ.setNew_Value(New_Value);
        APJ.setTransId(transId);
        APJ.setRecordcreateddate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
        AuditRepository auditRepository = new AuditRepository(databaseHelper);
        return auditRepository.insertTotblAuditTrail(APJ);
    }

    /*private void prepareCovidVacinedata() throws Exception{
        tblcovvac = new tblCovidVaccineDetails();
        tblcovvac.setUserId(user.getUserId());
        tblcovvac.setWomenId(woman.getWomanId());
        tblcovvac.setCovidVaccinated(aq.id(R.id.rd_CovidVaccineYes).isChecked() ? getResources().getString(R.string.yesshortform) : getResources().getString(R.string.noshortform));
        tblcovvac.setCovidFirstDoseDate(aq.id(R.id.etVisitCovidFirstDoseDate).getText().toString());
        tblcovvac.setCovidSecondDoseDate(aq.id(R.id.etVisitCovidSecondDoseDate).getText().toString());
        tblcovvac.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
        tblcovvac.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
    }*/

    private void prepareCovidVacinedata() throws Exception {
        tblcovvac = new tblCovidVaccineDetails();
        insertCovidVaccineVal = new LinkedHashMap<>();
        tblcovvac.setUserId(user.getUserId());
        tblcovvac.setBeneficiaryId(woman.getWomanId());
        tblcovvac.setBeneficiaryType("LM"); //20May2021 Bindu
//            tblcovvac.setCovidVaccinated(aq.id(R.id.rd_CovidVaccineYes).isChecked() ? getResources().getString(R.string.yesshortform) : getResources().getString(R.string.noshortform));
//            tblcovvac.setCovidFirstDoseDate(aq.id(R.id.etVisitCovidFirstDoseDate).getText().toString());
//            tblcovvac.setCovidSecondDoseDate(aq.id(R.id.etVisitCovidSecondDoseDate).getText().toString());
        tblcovvac.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
        tblcovvac.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());

        if (aq.id(R.id.rd_CovidVaccineYes).isChecked() && covVacItems != null && covVacItems.size() < 3) { // less than 2 doses only
            /*if (covVacItems != null && covVacItems.size() <= 0) {
                if (aq.id(R.id.etVisitCovidFirstDoseDate).getText().toString().trim().length() > 0)
                    insertCovidVaccineVal.put("1", aq.id(R.id.etVisitCovidFirstDoseDate).getText().toString());
                if (aq.id(R.id.etVisitCovidSecondDoseDate).getText().toString().trim().length() > 0)
                    insertCovidVaccineVal.put("2", aq.id(R.id.etVisitCovidSecondDoseDate).getText().toString());
            } else if (covVacItems != null && covVacItems.size() == 1) {
                if (aq.id(R.id.etVisitCovidSecondDoseDate).getText().toString().trim().length() > 0)
                    insertCovidVaccineVal.put("2", aq.id(R.id.etVisitCovidSecondDoseDate).getText().toString());
            }*/
            if (covVacItems != null && covVacItems.size() == 0) {
                if (aq.id(R.id.etVisitCovidFirstDoseDate).getText().toString().trim().length() > 0)
                    insertCovidVaccineVal.put("1", aq.id(R.id.etVisitCovidFirstDoseDate).getText().toString());
                if (aq.id(R.id.etVisitCovidSecondDoseDate).getText().toString().trim().length() > 0)
                    insertCovidVaccineVal.put("2", aq.id(R.id.etVisitCovidSecondDoseDate).getText().toString());
            }
            if (covVacItems != null && covVacItems.size() == 1) {
                if (aq.id(R.id.etVisitCovidSecondDoseDate).getText().toString().trim().length() > 0)
                    insertCovidVaccineVal.put("2", aq.id(R.id.etVisitCovidSecondDoseDate).getText().toString());
            }
        }/*else {
            if (aq.id(R.id.etVisitCovidFirstDoseDate).getText().toString().trim().length() > 0)
                insertCovidVaccineVal.put("1", aq.id(R.id.etVisitCovidFirstDoseDate).getText().toString());
            if (aq.id(R.id.etVisitCovidSecondDoseDate).getText().toString().trim().length() > 0)
                insertCovidVaccineVal.put("2", aq.id(R.id.etVisitCovidSecondDoseDate).getText().toString());
        }*/
    }

    private void prepareCovidTestdata() throws Exception {
        tblcovtest = new tblCovidTestDetails();
        tblcovtest.setUserId(user.getUserId());
        tblcovtest.setWomenId(woman.getWomanId());
        tblcovtest.setBeneficiaryType("LM");
        tblcovtest.setVisitNum("" + visitNum);
        /*tblcovtest.setHealthIssues(spnHealthProblemshv.getSelectedItemsAsString());
        tblcovtest.setHealthIssuesOthers(aq.id(R.id.etVisitHealthIssuesYesOthers).getEditText().getText().toString());*/
//        18May2021 Bindu
        String healthproblems = "";
        if (aq.id(R.id.rd_healthissuesYes).isChecked()) { //23May2021 Bindu change cond
            healthproblems = healthproblems + spnHealthProblemshv.getSelectedIndicies().toString();
            healthproblems = healthproblems.replace("[", "");
            healthproblems = healthproblems.replace("]", "");
            tblcovtest.setHealthIssues(healthproblems);
        }
        if (spnHealthProblemshv.getSelectedItemsAsString().contains(getResources().getString(R.string.bothers))) {
            tblcovtest.setHealthIssuesOthers(aq.id(R.id.etVisitHealthIssuesYesOthers).getEditText().getText().toString());
        } else {
            tblcovtest.setHealthIssuesOthers("");
        }
        tblcovtest.setCovidTest(aq.id(R.id.rd_CovidTestYes).isChecked() ? getResources().getString(R.string.yesshortform) : getResources().getString(R.string.noshortform));
        tblcovtest.setCovidResult(aq.id(R.id.rd_CovidPositive).isChecked() ? getResources().getString(R.string.positivetxt) : getResources().getString(R.string.negativetxt));
        tblcovtest.setCovidResultDate(aq.id(R.id.etVisitCovidTestDate).getText().toString());
        tblcovtest.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
        tblcovtest.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
                File file = new File(mCurrentPhotoPath);
                FileUtils fileUtils = new FileUtils(getApplicationContext());
                file = new File(fileUtils.getPath(Uri.fromFile(file)));
                file = new Compressor(getApplicationContext()).compressToFile(file);
                Bitmap bitmap = MediaStore.Images.Media
                        .getBitmap(getApplicationContext().getContentResolver(), Uri.fromFile(file));
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100 /*ignored for PNG*/, bos);
                byte[] bitmapdata = bos.toByteArray();
                String timeStamp = new SimpleDateFormat("HHmmssSSS").format(new Date());
                String fn;
                String dir;
                fn = "IMG_" + woman.getWomanId() + "_HV" + visitNum + timeStamp + ".jpeg";
                dir = AppState.imgDirRef;
                File fileParent = new File(dir);

                String fileName = fileParent + "/" + fn;

                ImagePojo imagePojo = new ImagePojo();
                imagePojo.setImage(bitmapdata);
                imagePojo.setImagePath(fileName);
                imagePojo.setImageBitmap(bitmap);

                AllImagePojo.add(imagePojo);

                tblImageStore imageStore = new tblImageStore();
                imageStore.setUserId(user.getUserId());
                imageStore.setBeneficiaryId(woman.getWomanId());
                imageStore.setImageName(fn.replace(".jpeg", ""));
                imageStore.setImagePath(fileName);
                imageStore.setTransId(transId);
                imageStore.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
                imageStore.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
                AllImagesBeforeSave.add(imageStore);
                AsyncImage asyncImage = new AsyncImage();
                asyncImage.execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class AsyncImage extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {

            Collections.sort(AllImagesBeforeSave, (file1, file2) -> {
                long k = new File(file1.getImagePath()).lastModified() - new File(file2.getImagePath()).lastModified();
                if (k < 0) {
                    return 1;
                } else if (k == 0) {
                    return 0;
                } else {
                    return -1;
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            startRecyclerView();
        }
    }

    public void startRecyclerView() {
        try {
            recyclerViewImage = findViewById(R.id.recyclerImage);
            if (AllImagesBeforeSave.size() == 0) {
                aq.id(R.id.lladdphotos).gone();
            } else {
                aq.id(R.id.lladdphotos).visible();
                recyclerViewImage.setVisibility(View.VISIBLE);
                aq.id(R.id.txtnoimageadded).getTextView().setVisibility(View.GONE);
                DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
                float screenWidthDp = displayMetrics.widthPixels / displayMetrics.density;
                int noOfColumns = (int) (screenWidthDp / 125 + 0.5); // +0.5 for correct rounding to int.
                GridLayoutManager layoutManager = new GridLayoutManager(PncHomeVisit.this, noOfColumns);
                recyclerViewImage.setLayoutManager(layoutManager);
                recyclerViewImage.setNestedScrollingEnabled(false);
                recyclerViewImage.setHasFixedSize(true);
                RecyclerViewAdapter recyclerViewAdapter = new RecyclerViewAdapter(PncHomeVisit.this, AllImagesBeforeSave, databaseHelper, AllImagePojo, aq);
                recyclerViewImage.setAdapter(recyclerViewAdapter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static Bitmap rotateImageIfRequired(Bitmap img, Uri selectedImage) throws IOException {

        ExifInterface ei = new ExifInterface(selectedImage.getPath());
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return rotateImage(img, 90);
        }
    }

    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }

    private class AsyncFileUpload extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            try {
                OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                        .connectTimeout(60, TimeUnit.SECONDS)
                        .readTimeout(60, TimeUnit.SECONDS)
                        .writeTimeout(60, TimeUnit.SECONDS)
                        .build();

                for (tblImageStore s : AllImagesBeforeSave) {
                    String imagepath = AppState.imgDirRef +"/"+ s.getImageName() + ".jpeg";
                    File file = new File(imagepath);

                    RequestBody requestBody = new MultipartBody.Builder()
                            .setType(MultipartBody.FORM)
                            .addFormDataPart("filename", s.getImageName())
                            .addFormDataPart(s.getImageName(), s.getImageName(),
                                    RequestBody.create(MediaType.parse("image/jpg"), file))
                            .build();
                    String endpoint = "http://" + appState.webServiceIpAddress + appState.webServicePath + "image";
                    Request request = new Request.Builder()
                            .url(endpoint)
                            .post(requestBody)
                            .build();
                    try (Response r2 = okHttpClient.newCall(request).execute()) {
                        String responseString2 = r2.body().string();
                        Log.i("Image Upload", responseString2);
                        updateImageStatus(s.getImageName());
                    }catch (Exception e){
                        Log.i("Image Upload",e.getMessage());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
    private void fileUpload() {
        try {
            AsyncFileUpload asyncFileUpload = new AsyncFileUpload();
            asyncFileUpload.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void updateImageStatus(String imageName) throws Exception {
        ImageStoreRepository imageStoreRepository = new ImageStoreRepository(databaseHelper);
        int update = imageStoreRepository.updateImageStatusof(imageName);
        if (update > 0) {
            Log.e("Image", "Image uploaded");
        } else {
            Log.e("Image", "Image Not uploaded");
        }
    }

    private void prepareAuditTrail() throws Exception {

        if (!(hmvisitregdetails.getIsCompl() != null && hmvisitregdetails.getIsCompl().equalsIgnoreCase(woman.getIsCompl()))) {
            InserttblAuditTrail("isCompl", woman.getIsCompl(), hmvisitregdetails.getIsCompl(), transId, null);
        }
        if (!(hmvisitregdetails.getLastAncVisitDate() != null && hmvisitregdetails.getLastAncVisitDate().equalsIgnoreCase(woman.getLastAncVisitDate()))) {
            InserttblAuditTrail("LastAncVisitDate", woman.getLastAncVisitDate(), hmvisitregdetails.getLastAncVisitDate(), transId, null);
        }
        if (!(hmvisitregdetails.getLastPncVisitDate() != null && hmvisitregdetails.getLastPncVisitDate().equalsIgnoreCase(woman.getLastPncVisitDate()))) {
            InserttblAuditTrail("LastPncVisitDate", woman.getLastPncVisitDate(), hmvisitregdetails.getLastPncVisitDate(), transId, null);
        }
        if (!(hmvisitregdetails.getIsReferred() != null && hmvisitregdetails.getIsReferred().equalsIgnoreCase(woman.getIsReferred()))) {
            InserttblAuditTrail("isReferred", woman.getIsReferred(), hmvisitregdetails.getIsReferred(), transId, null);
        }
        if (woman.getRegUserType() != null && !woman.getRegUserType().equalsIgnoreCase(appState.userType))
            InserttblAuditTrail("regUserType", woman.getRegUserType(), appState.userType, transId, null);

        if (!(woman.getRegrecommendedPlaceOfDelivery() != null && hmvisitregdetails.getRegrecommendedPlaceOfDelivery()!=null && hmvisitregdetails.getRegrecommendedPlaceOfDelivery().equalsIgnoreCase(woman.getRegrecommendedPlaceOfDelivery()))) {
            InserttblAuditTrail("regrecommendedPlaceOfDelivery", woman.getRegrecommendedPlaceOfDelivery(), hmvisitregdetails.getRegrecommendedPlaceOfDelivery(), transId, null);
        }

        InserttblAuditTrail("RecordUpdatedDate", woman.getRecordUpdatedDate(), hmvisitregdetails.getRecordUpdatedDate(), transId, null);

    }

    /**
     * Setting AuditTrail pojo, calls - InserttblAuditTrail()
     *
     * @param transId
     */
    private boolean InserttblAuditTrail(String ColumnName, String Old_Value, String New_Value, int transId,
                                        byte[] image) throws Exception {
        AuditPojo APJ = new AuditPojo();
        APJ.setUserId(appState.sessionUserId);
        APJ.setWomanId(woman.getWomanId());
        APJ.setTblName("tblregisteredwomen");
        APJ.setDateChanged(DateTimeUtil.getTodaysDate());
        APJ.setColumnName(ColumnName);
        APJ.setOld_Value(Old_Value);
        APJ.setNew_Value(New_Value);
        APJ.setTransId(transId);
        APJ.setRecordcreateddate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
        AuditRepository auditRepository = new AuditRepository(databaseHelper);
        return auditRepository.insertTotblAuditTrail(APJ);
    }

}
