package com.sc.stmansi.HomeVisit.VisitHistoryFragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.HomeVisit.HomeVisitActivityByCc;
import com.sc.stmansi.HomeVisit.HomeVisitListAdapterCc;
import com.sc.stmansi.R;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.recyclerview.ClickListener;
import com.sc.stmansi.repositories.CaseMgmtRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.tables.TblCaseMgmt_ANCPNCHv;
import com.sc.stmansi.tables.TblChildInfo;
import com.sc.stmansi.tables.tblCaseMgmt;
import com.sc.stmansi.tables.tblregisteredwomen;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class WomanHVFragmentCC extends Fragment implements ClickListener {
    String womanId;
    //    List<tblCaseMgmt> tblCaseMgmtList;
    private View parentview;
    private AppState appState;
    private DatabaseHelper databaseHelper;
    private RecyclerView recyclerView;
    private tblregisteredwomen woman;
    private AQuery aq;
    private RecyclerView.Adapter homeVisitViewAdapter;
    private int itemPosition;
    private tblCaseMgmt visitDetails;
    ArrayList<TblCaseMgmt_ANCPNCHv> tblCaseMgmtList;
    TblChildInfo tblChildInfo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        parentview = inflater.inflate(R.layout.fragment_womanhv_ccfragment, container, false);
        Bundle bundle = this.getArguments();
        appState = bundle.getParcelable("appState");
//        womanId = this.getArguments().getString("womanId");
//        womanId = appState.selectedWomanId;

        //mani 30Aug2021
        Boolean str = this.getArguments().getBoolean("fromchild");
        if (str)
        {
            tblChildInfo = (TblChildInfo) bundle.getSerializable("tblChildInfo");
            womanId=tblChildInfo.getWomanId();
        }else {
            womanId = appState.selectedWomanId;
        }


        recyclerView = parentview.findViewById(R.id.rvHomevisitCC);

        return parentview;
    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(parentview.getContext(), DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initialValues();
        setData();

    }

    private void setData() {
        try {
            CaseMgmtRepository caseMgmtRepository = new CaseMgmtRepository(databaseHelper);
//            tblCaseMgmtList = caseMgmtRepository.getAllCasesWomanHV(womanId, appState.sessionUserId,getResources().getString(R.string.ccusertype));
            tblCaseMgmtList = caseMgmtRepository.getCaseMgmtWomanANCPNCHV_CC(womanId, appState.sessionUserId,getResources().getString(R.string.ccusertype));
            if (tblCaseMgmtList != null && tblCaseMgmtList.size() != 0) {
//                homeVisitViewAdapter=new HomeVisitListAdapterCc(getActivity(),this,tblCaseMgmtList);
                homeVisitViewAdapter=new HomeVisitListAdapterCc(getActivity(),this,tblCaseMgmtList);
                recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                recyclerView.setAdapter(homeVisitViewAdapter);
            } else {
                aq.id(R.id.txtNoRecordsCC).visible();
            }
        } catch (Exception e) {
            e.printStackTrace();
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private void initialValues() {
        databaseHelper = getHelper();
        aq = new AQuery(parentview);
        try {
            WomanRepository womanRepo = new WomanRepository(databaseHelper);
            woman = womanRepo.getRegistartionDetails(womanId, appState.sessionUserId);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }


    @Override
    public void onClick(View v) {
        setSelectedVisitDetails(v);
        Intent intent = new Intent(getContext(), HomeVisitActivityByCc.class);
        intent.putExtra("globalState", this.getArguments());
        intent.putExtra("pos", itemPosition);
//        intent.putExtra("womanId", womanId);
        startActivity(intent);
    }

    //    set selected Visit details id
    private void setSelectedVisitDetails(View v) {
        itemPosition = recyclerView.getChildLayoutPosition(v);
//        visitDetails = tblCaseMgmtList.get(itemPosition);
    }

    @Override
    public boolean onLongClick(View v) {
        return false;
    }
}