package com.sc.stmansi.HomeVisit;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.R;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.recyclerview.ClickListener;
import com.sc.stmansi.recyclerview.HomeVisitListViewHolder;
import com.sc.stmansi.repositories.HomeVisitRepository;
import com.sc.stmansi.tables.TblCaseMgmt_ANCPNCHv;
import com.sc.stmansi.tables.TblSymptomMaster;
import com.sc.stmansi.tables.tblCaseMgmt;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HomeVisitListAdapterCc extends  RecyclerView.Adapter<HomeVisitListViewHolder>{
    private Context context;
    private ClickListener viewClickListener;
    private List<tblCaseMgmt> tblCaseMgmtList;
    DatabaseHelper databaseHelper;
    private List<TblCaseMgmt_ANCPNCHv> tblCaseMgmtListN;


    public HomeVisitListAdapterCc(Context context, Fragment fragment, List<tblCaseMgmt> tblCaseMgmtList) {
        this.context = context;
        this.viewClickListener = (ClickListener) fragment;
        this.tblCaseMgmtList = tblCaseMgmtList;
        databaseHelper = getHelper();
    }

    public HomeVisitListAdapterCc(Context context, Fragment fragment, ArrayList<TblCaseMgmt_ANCPNCHv> tblCaseMgmtList) {
        this.context = context;
        this.viewClickListener = (ClickListener) fragment;
        this.tblCaseMgmtListN = tblCaseMgmtList;
        databaseHelper = getHelper();
    }

    //    initialize db object
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @NonNull
    @Override
    public HomeVisitListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.homevisit_recycler_item_cc, viewGroup, false);
        linearLayout.setOnClickListener(viewClickListener);
        return new HomeVisitListViewHolder(linearLayout);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeVisitListViewHolder homeVisitViewHolder, int i) {
        TblCaseMgmt_ANCPNCHv caseMgmtdet = tblCaseMgmtListN.get(i);
//        homeVisitViewHolder.btnactiontaken.setVisibility(View.GONE);
        homeVisitViewHolder.txtVisitNum.setText("# " + tblCaseMgmtListN.get(i).getVisitNum());
        homeVisitViewHolder.txtVisitDate.setText(tblCaseMgmtListN.get(i).getVisitDate());

        try {
            if (tblCaseMgmtListN.get(i).getVisitMode().equalsIgnoreCase("ANC")) {
                homeVisitViewHolder.imgWomanStatusHv.setImageResource(R.drawable.ic_pregnant);
                homeVisitViewHolder.txtStatus.setText(context.getResources().getString(R.string.statusatvisit) + " : " + tblCaseMgmtListN.get(i).getStatusAtVisit());
            } else if (tblCaseMgmtListN.get(i).getVisitMode().equalsIgnoreCase("PNC")) {
                homeVisitViewHolder.imgWomanStatusHv.setImageResource(R.drawable.ic_mother);
                homeVisitViewHolder.txtStatus.setText(context.getResources().getString(R.string.txtDaysSinceDeliveryName) + " : " + caseMgmtdet.getStatusAtVisit());
            }

//        Delayed visit
            if(!(caseMgmtdet.getVisitType().equalsIgnoreCase("Direct"))) {
                String visitdateMM = caseMgmtdet.getVisitDatebyMM();
                String visitdateCC = caseMgmtdet.getVisitDate();
                try {
                    int days = DateTimeUtil.countWorkingDays(visitdateCC, visitdateMM); //21Jul2021 Bindu exclude weekends
                    if (days > 5) {
                        homeVisitViewHolder.txtdelayedfollowup.setVisibility(View.VISIBLE);
                    } else {
                        homeVisitViewHolder.txtdelayedfollowup.setVisibility(View.GONE);
                    }
                }catch (Exception e) {
                    e.printStackTrace();
                }
            }

            boolean strCompl = false;

            if (tblCaseMgmtListN.get(i).getVisitReferredtoFacType() != null && tblCaseMgmtListN.get(i).getVisitReferredtoFacType().length() > 0) {
                homeVisitViewHolder.imgVisitOutcome.setImageResource(R.drawable.ic_refer);
                strCompl = true; //13Aug2021 Bindu
            }
            else if ((tblCaseMgmtListN.get(i).getVisitBeneficiaryDangerSigns() != null && tblCaseMgmtListN.get(i).getVisitBeneficiaryDangerSigns().trim().length() > 0) ||
                    (tblCaseMgmtListN.get(i).getVisitBeneficiaryDangerSigns() != null && tblCaseMgmtListN.get(i).getVisitBeneficiaryDangerSigns().trim().length() > 0 ) ||
                    (tblCaseMgmtListN.get(i).getChildDangersigns() != null && tblCaseMgmtListN.get(i).getChildDangersigns().trim().length() > 0 ) ||
                    (tblCaseMgmtListN.get(i).getChildDangersignsSec() != null && tblCaseMgmtListN.get(i).getChildDangersignsSec().trim().length() > 0 ) ||
                    (tblCaseMgmtListN.get(i).getChildDangersignsThird() != null && tblCaseMgmtListN.get(i).getChildDangersignsThird().trim().length() > 0 ) ||
                    (tblCaseMgmtListN.get(i).getChildDangersignsFourth() != null && tblCaseMgmtListN.get(i).getChildDangersignsFourth().trim().length() > 0 ) ||
                    (tblCaseMgmtListN.get(i).getChildDangersignsOthers() != null && tblCaseMgmtListN.get(i).getChildDangersignsOthers().trim().length() > 0 )){
                homeVisitViewHolder.imgVisitOutcome.setImageResource(R.drawable.ic_hvcompl);
                strCompl = true;
            }
            else
                homeVisitViewHolder.imgVisitOutcome.setImageResource(R.drawable.ic_hvnocompl);


            if ((tblCaseMgmtListN.get(i).getVisitReferredtoFacType() != null && tblCaseMgmtListN.get(i).getVisitReferredtoFacType().length() > 0) || (tblCaseMgmtListN.get(i).getVisitReferredtoFacName() != null && tblCaseMgmtListN.get(i).getVisitReferredtoFacName().length() > 0)) //04Jul2021 Bindu - check null
                homeVisitViewHolder.txtReferredToFacname.setText(context.getResources().getString(R.string.reffacilityname) + tblCaseMgmtListN.get(i).getVisitReferredtoFacType() + " - " + tblCaseMgmtListN.get(i).getVisitReferredtoFacName());
            else
                homeVisitViewHolder.txtReferredToFacname.setText("");

            boolean finalStrCompl = strCompl;
            homeVisitViewHolder.imgVisitOutcome.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {


                        if (finalStrCompl) {
                            String title = context.getResources().getString(R.string.symptomscc);
                            boolean isPnc = false;
                            if (caseMgmtdet.getVisitMode().equalsIgnoreCase(context.getResources().getString(R.string.strpnc))) {
                                title = context.getResources().getString(R.string.dangersigns);
                                isPnc = true;
                            }


                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                            View convertView = LayoutInflater.from(context).inflate(R.layout.custom_hv_dialog, null, false);
                            TextView txtmotherdangersigns = convertView.findViewById(R.id.txtmotherdangersignval);
                            TextView txtnbdangersigns = convertView.findViewById(R.id.txtnbdangersignval);
                            LinearLayout llnbdangersigns = convertView.findViewById(R.id.llnbds);
                            LinearLayout llwomandangersigns = convertView.findViewById(R.id.llmotherds);

                            if (caseMgmtdet.getVisitBeneficiaryDangerSigns() != null && caseMgmtdet.getVisitBeneficiaryDangerSigns().length() > 0) {
                                txtmotherdangersigns.setText(setWomanCompl(caseMgmtdet.getVisitBeneficiaryDangerSigns()));//15May2021 Arpitha
                                llwomandangersigns.setVisibility(View.VISIBLE);
                            } else {
                                llwomandangersigns.setVisibility(View.GONE);
                            }

                            if (isPnc) {

                                String babyDangerSign = "";
                                //20Jul2021 Bindu
                                if(caseMgmtdet.getChildDangersigns() != null && caseMgmtdet.getChildDangersigns().length() > 0) {
                                    babyDangerSign = context.getResources().getString(R.string.baby1)+ " - " + setChildCompl(caseMgmtdet.getChildDangersigns());
                                }
                                if(caseMgmtdet.getChildDangersignsSec() != null && caseMgmtdet.getChildDangersignsSec().length() > 0) {
                                    babyDangerSign = babyDangerSign + "\n" + context.getResources().getString(R.string.baby2)+ " - " + setChildCompl(caseMgmtdet.getChildDangersignsSec());

                                }
                                if(caseMgmtdet.getChildDangersignsThird() != null && caseMgmtdet.getChildDangersignsThird().length() > 0) {
                                    babyDangerSign = babyDangerSign + "\n" +context.getResources().getString(R.string.baby3)+ " - " + setChildCompl(caseMgmtdet.getChildDangersignsThird());

                                }
                                if(caseMgmtdet.getChildDangersignsFourth() != null && caseMgmtdet.getChildDangersignsFourth().length() > 0) {
                                    babyDangerSign = babyDangerSign + "\n" +context.getResources().getString(R.string.baby4)+ " - " + setChildCompl(caseMgmtdet.getChildDangersignsFourth());

                                }
                                if(caseMgmtdet.getChildDangersignsOthers() != null && caseMgmtdet.getChildDangersignsOthers().length() > 0) {
                                    babyDangerSign = babyDangerSign + "\n" + caseMgmtdet.getChildDangersignsOthers();
                                }

                                txtnbdangersigns.setText(babyDangerSign);
                                llnbdangersigns.setVisibility(View.VISIBLE);
                            } else {
                                llnbdangersigns.setVisibility(View.GONE);
                            }

                            alertDialog.setView(convertView).setCancelable(true).setTitle(title)
                                    .setPositiveButton((context.getResources().getString(R.string.ok)), new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    });
                            // lv.setAdapter(adapter);
                            alertDialog.show();
                        }
                    } catch (Exception e) {

                    }
                }
            });

        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return tblCaseMgmtListN.size();
    }


    private String setWomanCompl(String dangersign) throws SQLException {


        ArrayList<String> list = new ArrayList<>();
        String symptom = "";
        if (dangersign != null && dangersign.length() > 0) {
            list = new ArrayList<String>(Arrays.asList(dangersign.split(",")));

            if (list != null && list.size() > 0) {

                for (int i = 0; i < list.size(); i++) {
                    String strSy = list.get(i);
                    strSy = strSy.replaceAll(" ", "");
                    strSy = strSy.toLowerCase().trim();

                    //check directly and replace - any changes to be updated in HomeVisitActivityByCC also
                    if(strSy.contains("notrelieved"))
                        strSy = "constipationnotrelieved"; //15Jul2021 Bindu
                    if(strSy.contains("bleedingp/v"))
                        strSy = "bleedingpv"; //15Jul2021 Bindu
                    if(strSy.contains("leakingofwatery"))
                        strSy = "leakingofwateryfluidspv"; //15Jul2021 Bindu
                    if(strSy.contains("twin/multiplepregnancy"))
                        strSy = "neardeltwinormultiplepregnancy";
                    if(strSy.contains("laborpainsmorethan12hours"))
                        strSy = "laborpainmorethan12hours";
                    if(strSy.contains("motherswithcpd/contractedpelvis"))
                        strSy = "neardelmotherscpdorcontpelvis";
                    if(strSy.contains("foetalmovementdecreased"))
                        strSy = "decreasedfetalmovements";
                    if(strSy.contains("foetalmovementabsent"))
                        strSy = "fetalmovementabsent";
                    if(strSy.contains("iugr"))
                        strSy = "neardeliugr";

//                    for pnc
                    if(strSy.contains("inabilitytocontroldefecation"))
                        strSy = "inabilitytocontroldefecationorurine";
                    if(strSy.contains("bleedingnipples"))
                        strSy = "breastsproblems";
                    if(strSy.contains("swollenhands"))
                        strSy = "swollenhands";

                    int identifier = context.getResources().getIdentifier
                            (strSy,
                                    "string", context.getPackageName()); //25Jun2021 Bindu get package name from app
                    if (identifier > 0) {
                        if (i == list.size() - 1)
                            symptom = symptom + context.getResources().getString(identifier);
                        else
                            symptom = symptom + context.getResources().getString(identifier) + ",";
                    } else {
                        if (i == list.size() - 1)
                            symptom = symptom + strSy;
                        else
                            symptom = symptom + strSy + ",";
                    }

                }
            }
        }return symptom;
    }

    private String setChildCompl(String dangersign) throws SQLException {

        ArrayList<String> symptoms = new ArrayList<>();
        ArrayList<String> list = new ArrayList<>();
        String symptom = "";
        if (dangersign != null && dangersign.length() > 0) {
            list = new ArrayList<String>(Arrays.asList(dangersign.split(",")));

            if (list != null && list.size() > 0) {

                for (int i = 0; i < list.size(); i++) {
                    String strSy = list.get(i);

                    List<TblSymptomMaster> symptomMasterList = databaseHelper.getTblSymptomMasterDao().queryBuilder()
                            .selectColumns("symXmlStringName").where()
                            .eq("symSymptomName", strSy).query();

                    if (symptomMasterList != null && symptomMasterList.size() > 0) {
                        symptoms.add(symptomMasterList.get(0).getSymXmlStringName());
                    } else {
                        if (strSy.toLowerCase().trim().contains("grunting")) //21Jul2021 Bindu manually add
                            symptoms.add("nbfastbreathing");
                        if (strSy.toLowerCase().trim().contains("diarrhea")) //21Jul2021 Bindu manually add
                            symptoms.add("nbdiarrhoea");
                    }

                }

                if(symptoms != null) {
                    String strSy = "";
                    for(int i=0; i< symptoms.size();i++) {
                        strSy = symptoms.get(i);
                        strSy = strSy.replaceAll(" ", "");
                        strSy = strSy.toLowerCase().trim();
                        int identifier = context.getResources().getIdentifier
                                (strSy,
                                        "string", context.getPackageName()); //25Jun2021 Bindu get package name from app
                        if (identifier > 0) {
                            if (i == list.size() - 1)
                                symptom = symptom + context.getResources().getString(identifier);
                            else
                                symptom = symptom + context.getResources().getString(identifier) + ",";
                        } else {
                            if (i == list.size() - 1)
                                symptom = symptom + strSy;
                            else
                                symptom = symptom + strSy + ",";
                        }
                    }
                }
            }
        }return symptom;
    }
}
