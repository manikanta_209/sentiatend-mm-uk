package com.sc.stmansi.HomeVisit;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.legacy.app.ActionBarDrawerToggle;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.R;
import com.sc.stmansi.common.AlertDialogUtil;
import com.sc.stmansi.common.CommonDatePickerFragment;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.common.NavDrawerItem;
import com.sc.stmansi.common.NavDrawerListAdapter;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.configuration.SyncState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.deactivate.WomanDeactivateActivity;
import com.sc.stmansi.delivery.DeliveryInfoActivity;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.repositories.CaseMgmtRepository;
import com.sc.stmansi.repositories.CovidDetailsRepository;
import com.sc.stmansi.repositories.LoginRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.tables.TblCaseMgmt_ANCPNCHv;
import com.sc.stmansi.tables.TblSymptomMaster;
import com.sc.stmansi.tables.tblCovidVaccineDetails;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.viewprofile.ViewProfileActivity;
import com.sc.stmansi.womanlist.RegisteredWomenActionTabs;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class HomeVisitActivityByMC extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

//    List<tblCaseMgmt> tblCaseMgmtList;
    private AppState appState;
    private DatabaseHelper databaseHelper;
    private tblregisteredwomen woman;
    private int itempos = 0;
    private int index = 0;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private ArrayList<NavDrawerItem> navDrawerItems;
    private ListView mDrawerList;
    private AQuery aq;
    int visitNum;
    ArrayList<TblCaseMgmt_ANCPNCHv> tblCaseMgmtList;
    //01Jul2021 Bindu
    RadioButton rdbtnsms , rdbtndirect;
    LinearLayout llsmscontent; EditText etvisitno, etvisitdate;
    AQuery aqhvdialog;
    Boolean wantToCloseDialog = false;
    //mani 19July2021
    private List<tblCovidVaccineDetails> covVacItems;
    private SyncState syncState;
    private int indexCC = 0;//05Aug2021 Arpitha
    public static Typeface font = null;//09Aug2021 Arpitha

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_visit_by_mc);
        try {


            aq = new AQuery(this);
            databaseHelper = getHelper();
            Bundle bundle = getIntent().getBundleExtra("globalState");
            appState = bundle.getParcelable("appState");
            syncState = bundle.getParcelable("syncState");

            WomanRepository womanRepo = new WomanRepository(databaseHelper);
            woman = womanRepo.getRegistartionDetails(appState.selectedWomanId, appState.sessionUserId);

            CaseMgmtRepository caseMgmtRepository = new CaseMgmtRepository(databaseHelper);
            //Bindu - get visitnum
            visitNum = getIntent().getIntExtra("complVisitNum",0);
          //  tblCaseMgmtList = caseMgmtRepository.getCaseMgmtDetailsWoman(appState.selectedWomanId, appState.sessionUserId, ""+visitNum); //add visit num

            tblCaseMgmtList = caseMgmtRepository.getCaseMgmtWomanANCPNCHV_MC(appState.selectedWomanId, getResources().getString(R.string.mcusertype));

            itempos = getIntent().getIntExtra("pos", 0);
            index = itempos;

            indexCC = getIntent().getIntExtra("ccpos", 0);

            initiateDrawer();

            aq.id(R.id.btnnextvisit).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {

                        if(indexCC>0)
                        {
                            indexCC = indexCC + 1;
                            //nextObj = visitItems.get(index - 1);
                            if (indexCC < tblCaseMgmtList.size()) {
                                setData(tblCaseMgmtList, indexCC);
                            } else {
                                indexCC = indexCC - 1;
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.norecords), Toast.LENGTH_LONG).show();
                                // aq.id(R.id.txtVisitNum).text("Visit Num" + visitItems.get(index).getVisitNum());
                            }
                        }
                        else {
                            index = index + 1;
                            //nextObj = visitItems.get(index - 1);
                            if (index < tblCaseMgmtList.size()) {
                                setData(tblCaseMgmtList, index);
                            } else {
                                index = index - 1;
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.norecords), Toast.LENGTH_LONG).show();
                                // aq.id(R.id.txtVisitNum).text("Visit Num" + visitItems.get(index).getVisitNum());
                            }
                        }

                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            });

            aq.id(R.id.btnpreviousvisit).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                    /*prevObj = visitItems.get(index + 1);
                    aq.id(R.id.txtVisitNum).text("Visit Num" + prevObj.getVisitNum());*/

                        index = index - 1;
                        if (index >= 0) {
                            setData(tblCaseMgmtList, index);
                        } else {
                            index = index + 1;
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.norecords), Toast.LENGTH_LONG).show();
                        }
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                }
            });

            if(indexCC>0)
                setData(tblCaseMgmtList, indexCC);
            else
            setData(tblCaseMgmtList, index);
            // Symptoms drwable on touch listener
            aq.id(R.id.txtDangSignsL).getTextView().setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if(event.getAction() == MotionEvent.ACTION_UP) {
                        if(event.getRawX() <= aq.id(R.id.txtDangSignsL).getTextView().getTotalPaddingLeft()) {
                            opencloseAccordionDangerSigns(aq.id(R.id.lldangersigns).getView(), aq.id(R.id.txtDangSignsL).getTextView());
                            return true;
                        }
                    }
                    return true;
                }
            });

            aq.id(R.id.txtadvisegiven).getTextView().setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if(event.getAction() == MotionEvent.ACTION_UP) {
                        if(event.getRawX() <= aq.id(R.id.txtadvisegiven).getTextView().getTotalPaddingLeft()) {
                            opencloseAccordionDangerSigns(aq.id(R.id.llacttaken).getView(), aq.id(R.id.txtadvisegiven).getTextView());
                            return true;
                        }
                    }
                    return true;
                }
            });


        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    // Expand and Collapse Danger Signs
    public void opencloseAccordionDangerSigns(View v, TextView txt) {
        try {

            if (v.getVisibility() == View.VISIBLE) {
                v.setVisibility(View.GONE);
                txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_expsym, 0, 0, 0);
            }
            else {
                v.setVisibility(View.VISIBLE);
                txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_colsym, 0, 0, 0);
            }
        } catch (Exception e) {

        }
    }

    private void setData(List<TblCaseMgmt_ANCPNCHv> tblCaseMgmtList, int indexVal) throws Exception{
      //  int visit = 0;
       /* int index ;
        if(indexVal>0)
                index = indexVal;
        else
            index = indexCC;

        for(int i = 0;i<tblCaseMgmtList.size();i++)
        {
            int visitNo = 0;

            if(indexVal > 0 )
            visitNo = tblCaseMgmtList.get(i).getVisitNumbyMM();
            else
                visitNo = tblCaseMgmtList.get(i).getVisitNum();

            if(visitNo == index ) {

                index = i;16Sep2021 ARpitha*/

        //visit = tblCaseMgmtList.get(0).getVisitNum();
        for(int i = 0;i<tblCaseMgmtList.size();i++)
        {
        aq.id(R.id.txtvisitnum).text(""+tblCaseMgmtList.get(index).getVisitNum());
        aq.id(R.id.txtVisitDateval).text(tblCaseMgmtList.get(index).getVisitDate());
        aq.id(R.id.txtgestageval).text(tblCaseMgmtList.get(index).getStatusAtVisit());

        aq.id(R.id.txtDangSigns).text(setWomanCompl(tblCaseMgmtList.get(index).getVisitBeneficiaryDangerSigns()));


        if(tblCaseMgmtList.get(index).getVisitMode().equalsIgnoreCase("PNC")) {
            aq.id(R.id.txtNbDangSigns).gone();
            aq.id(R.id.txtnbdangersignsL).visible();
            aq.id(R.id.txtNbDangSigns2).gone();
            aq.id(R.id.txtNbDangSigns3).gone();
            aq.id(R.id.txtNbDangSigns4).gone();
            aq.id(R.id.txtNbDangSignsOther).gone();
            aq.id(R.id.txtmotherdangersignsL).visible();

            if(tblCaseMgmtList.get(index).getChildDangersigns() != null && tblCaseMgmtList.get(index).getChildDangersigns().length() > 0) {
                aq.id(R.id.txtNbDangSigns).text(getResources().getString(R.string.baby1)+ " - " + setChildCompl(tblCaseMgmtList.get(index).getChildDangersigns()));
                aq.id(R.id.txtNbDangSigns).visible();
            }
            if(tblCaseMgmtList.get(index).getChildDangersignsSec() != null && tblCaseMgmtList.get(index).getChildDangersignsSec().length() > 0) {
                aq.id(R.id.txtNbDangSigns2).text(getResources().getString(R.string.baby2)+ " - " + setChildCompl(tblCaseMgmtList.get(index).getChildDangersignsSec()));
                aq.id(R.id.txtNbDangSigns2).visible();
            }
            if(tblCaseMgmtList.get(index).getChildDangersignsThird() != null && tblCaseMgmtList.get(index).getChildDangersignsThird().length() > 0) {
                aq.id(R.id.txtNbDangSigns3).text(getResources().getString(R.string.baby3)+ " - " + setChildCompl(tblCaseMgmtList.get(index).getChildDangersignsThird()));
                aq.id(R.id.txtNbDangSigns3).visible();
            }
            if(tblCaseMgmtList.get(index).getChildDangersignsFourth() != null && tblCaseMgmtList.get(index).getChildDangersignsFourth().length() > 0) {
                aq.id(R.id.txtNbDangSigns4).text(getResources().getString(R.string.baby4)+ " - " + setChildCompl(tblCaseMgmtList.get(index).getChildDangersignsFourth()));
                aq.id(R.id.txtNbDangSigns4).visible();
            }
            if(tblCaseMgmtList.get(index).getChildDangersignsOthers() != null && tblCaseMgmtList.get(index).getChildDangersignsOthers().length() > 0) {
                aq.id(R.id.txtNbDangSignsOther).text(tblCaseMgmtList.get(index).getChildDangersignsOthers());
                aq.id(R.id.txtNbDangSignsOther).visible();
            }

        } else {
            aq.id(R.id.txtNbDangSigns).gone();
            aq.id(R.id.txtnbdangersignsL).gone();
            aq.id(R.id.txtNbDangSigns2).gone();
            aq.id(R.id.txtNbDangSigns3).gone();
            aq.id(R.id.txtNbDangSigns4).gone();
            aq.id(R.id.txtNbDangSignsOther).gone();
            aq.id(R.id.txtmotherdangersignsL).gone();
        }

                aq.id(R.id.tvadvise).text(tblCaseMgmtList.get(index).getHcmAdviseGiven());
                aq.id(R.id.tvbencondatvisit).text(tblCaseMgmtList.get(index).getHcmBeneficiaryCondatVisit());
                aq.id(R.id.tvactiontobetakenbyMM).text(tblCaseMgmtList.get(index).getHcmActionToBeTakenClient());
                aq.id(R.id.tvcommentstomc).text(tblCaseMgmtList.get(index).getHcmInputToNextLevel());
        /*aq.id(R.id.tvanmawareofcompl).text(tblCaseMgmtList.get(index).getHcmIsANMInformedAbtCompl());
        aq.id(R.id.tvanmawareofref).text(tblCaseMgmtList.get(index).getHcmANMAwareOfRef());*/
                aq.id(R.id.txtcommentsval).text(tblCaseMgmtList.get(index).getHcmActTakComments());

                if (tblCaseMgmtList.get(index).getVisitReferredtoFacType() != null && tblCaseMgmtList.get(index).getVisitReferredtoFacType().length() > 0) {
                    aq.id(R.id.tblrefdetails).visible();
                    aq.id(R.id.txtreffacilitynameval).text("" + tblCaseMgmtList.get(index).getVisitReferredtoFacName());
                    aq.id(R.id.txtreffacilitytypeval).text("" + tblCaseMgmtList.get(index).getVisitReferredtoFacType());
                    aq.id(R.id.txtrefslipnumberval).text("" + tblCaseMgmtList.get(index).getVisitReferralSlipNumber());
                } else
                    aq.id(R.id.tblrefdetails).gone();

        /*if (tblCaseMgmtList.get(index).getHcmBeneficiaryVisitFac().equalsIgnoreCase("Y")) {

            aq.id(R.id.benvisitedrefvalue).text(tblCaseMgmtList.get(index).getHcmBeneficiaryVisitFac());
            aq.id(R.id.llvisitactiontakendetails).visible();
            aq.id(R.id.llreasonnovisit).gone();
            aq.id(R.id.tvactiontakenBy).text(tblCaseMgmtList.get(index).getHcmActTakByUserType());
            aq.id(R.id.tvactiontakenusername).text(tblCaseMgmtList.get(index).getHcmActTakByUserName());
            aq.id(R.id.tvactiontakenfactype).text(tblCaseMgmtList.get(index).getHcmActTakAtFacility());
            aq.id(R.id.tvactiontakenfacname).text(tblCaseMgmtList.get(index).getHcmActTakFacilityName());
            aq.id(R.id.tvacttakendate).text(tblCaseMgmtList.get(index).getHcmActTakDate());
            aq.id(R.id.tvactiontakentime).text(tblCaseMgmtList.get(index).getHcmActTakTime());
            aq.id(R.id.tvactiontaken).text(tblCaseMgmtList.get(index).getHcmActTakForCompl());
            aq.id(R.id.tvmedications).text(tblCaseMgmtList.get(index).getHcmActMedicationsPres());

            aq.id(R.id.tvStatus).text(tblCaseMgmtList.get(index).getHcmActTakStatus());

            if(tblCaseMgmtList.get(index).getHcmActTakStatus().equalsIgnoreCase("Closed")) {//11Aug2021 Arpitha - compare direct stirng
                aq.id(R.id.txtstatusdate).text(getResources().getString(R.string.closeddate));
                aq.id(R.id.txtactionstatustime).text(getResources().getString(R.string.statusclosedtime));
                aq.id(R.id.trstatusreffactype).gone();
                aq.id(R.id.trstatusreffacname).gone();
            }else if(tblCaseMgmtList.get(index).getHcmActTakStatus().equalsIgnoreCase("Refer")) { //15Jul2021 Bindu - string change //11Aug2021 Arpitha - compare direct stirng
                aq.id(R.id.txtstatusdate).text(getResources().getString(R.string.statusrefdate));
                aq.id(R.id.txtactionstatustime).text(getResources().getString(R.string.statusreftime));
                aq.id(R.id.trstatusreffactype).visible();
                aq.id(R.id.trstatusreffacname).visible();
            }

            aq.id(R.id.tvactionstatusdate).text(tblCaseMgmtList.get(index).getHcmActTakStatusDate());
            aq.id(R.id.tvactionstatustime).text(tblCaseMgmtList.get(index).getHcmActTakStatusTime());
            aq.id(R.id.tvstatusfactype).text(tblCaseMgmtList.get(index).getHcmActTakReferredToFacility());
            aq.id(R.id.tvstatusfacnameHv).text(tblCaseMgmtList.get(index).getHcmActTakReferredToFacilityName());
        } else {
            aq.id(R.id.benvisitedrefvalue).text(tblCaseMgmtList.get(index).getHcmBeneficiaryVisitFac());
            aq.id(R.id.llreasonnovisit).visible();
            aq.id(R.id.llvisitactiontakendetails).gone();
            if(tblCaseMgmtList.get(index).getHcmReasonForNoVisit() != null && tblCaseMgmtList.get(index).getHcmReasonForNoVisit().toString().length() > 0) {

                String[] reasonfornovisit = getResources().getStringArray(R.array.arrnovisitreason);
                StringBuilder strreason = new StringBuilder();
                String indices = tblCaseMgmtList.get(index).getHcmReasonForNoVisit();
                if (indices.contains(",")) {
                    String[] indicesvalues = indices.split(",");
                    for (String s : indicesvalues) {
                        strreason.append(reasonfornovisit[Integer.parseInt(s.trim())]).append(", ");
                    }
                    strreason = strreason.deleteCharAt(strreason.lastIndexOf(","));
                    aq.id(R.id.reasonfornovisit).text(strreason);
                } else {
                    aq.id(R.id.reasonfornovisit).text(reasonfornovisit[Integer.parseInt(tblCaseMgmtList.get(index).getHcmReasonForNoVisit())]);
                }

                if(tblCaseMgmtList.get(index).getHcmReasonOthers() != null && tblCaseMgmtList.get(index).getHcmReasonOthers().toString().length() > 0) {
                    aq.id(R.id.trnovisitotherreason).visible();
                    aq.id(R.id.reasonfornovisitother).text(tblCaseMgmtList.get(index).getHcmReasonOthers());
                } else{
                    aq.id(R.id.trnovisitotherreason).gone();
                }
            }
        }*/
           // }
        }
    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    /**
     * Initiates the Navigation drawer
     */
    private void initiateDrawer() {
        try {
            mDrawerLayout = findViewById(R.id.ett_drawer_layout);
            mDrawerList = findViewById(R.id.ett_left_drawer);


            navDrawerItems = new ArrayList<NavDrawerItem>();

//             Mani Jun 1 2021 Removed UnplannedServicesListActivity & ServicesSummaryActivity

            // adding nav drawer items to array
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.sp_WRegEdit),
                    R.drawable.registration));

            //14Sep2019 - Bindu - Add Homevisit
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.homevisitadd),
                    R.drawable.ic_homevisit));

            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.womanhomevisithistory),
                    R.drawable.prenatalhomevisit));

            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deliveryHeading), R.drawable.delivery_info));//01Oct2019 Arpitha

            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deactivatenew),
                    R.drawable.deactivate));//28Nov2019 Arpitha

            // set a custom shadow that overlays the main content when the drawer
            // opens


            mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

            mDrawerList.setAdapter(new NavDrawerListAdapter(getApplicationContext(), navDrawerItems));
            mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

            // ActionBarDrawerToggle ties together the the proper interactions
            // between the sliding drawer and the action bar app icon
            mDrawerToggle = new ActionBarDrawerToggle(this, /* host Activity */
                    mDrawerLayout, /* DrawerLayout object */
                    R.drawable.ic_drawer, /*
             * nav drawer image to replace 'Up'
             * caret
             */
                    R.string.drawer_open, /*
             * "open drawer" description for
             * accessibility
             */
                    R.string.drawer_close /*
             * "close drawer" description for
             * accessibility
             */
            ) {
                public void onDrawerClosed(View view) {
                    try {
                        displayDrawerItems();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                public void onDrawerOpened(View drawerView) {
                    try {
                        displayDrawerItems();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            mDrawerLayout.setDrawerListener(mDrawerToggle);
        } catch (Exception e) {

        }
    }

    // 18Sep2019 - Bindu Display drawer open close items
    private void displayDrawerItems() throws Exception{

        invalidateOptionsMenu(); // creates call to
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer1);
        getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);


        aq.id(R.id.ivWomanImg).getImageView().setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                mDrawerLayout.closeDrawer(mDrawerList);
            }
        });

        aq.id(R.id.tvWomanName).text((woman.getRegWomanName() + "\t"));
        if (woman.getRegADDate() != null && woman.getRegADDate().length() > 0) {
            aq.id(R.id.tvInfo1).text(woman.getRegADDate());
        }else {
            int noOfWeeks = DateTimeUtil.getNumberOfWeeksFromLMP(woman.getRegLMP());
            aq.id(R.id.tvInfo1).text(getResources().getString(R.string.tvlmp) + woman.getRegLMP() + "  , GA : " + noOfWeeks + "W");
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        try {
            switch (item.getItemId()) {
                case android.R.id.home:
                    Intent home = new Intent(com.sc.stmansi.HomeVisit.HomeVisitActivityByMC.this, MainMenuActivity.class);
                    home.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                    /*startActivity(home);*/
                    displayAlert(getResources().getString(R.string.exit), home);
                    break;

                case R.id.wlist:
                    Intent wlist = new Intent(com.sc.stmansi.HomeVisit.HomeVisitActivityByMC.this, RegisteredWomenActionTabs.class);
                    wlist.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                    /*startActivity(wlist);*/
                    displayAlert(getResources().getString(R.string.exit), wlist);
                    break;

                case R.id.logout:
                    Intent logout = new Intent(com.sc.stmansi.HomeVisit.HomeVisitActivityByMC.this, LoginActivity.class);
                    displayAlert(getResources().getString(R.string.m111), logout);
                    /*startActivity(logout);
                    CommonClass.updateLogoutTime();*/
                    break;

                case R.id.home:
                    Intent intent = new Intent(com.sc.stmansi.HomeVisit.HomeVisitActivityByMC.this, MainMenuActivity.class);
                    intent.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                    /*startActivity(intent);*/
                    displayAlert(getResources().getString(R.string.exit), intent);
                    break;
                //30Sep2019 - Bindu
                //mani7JUNE2021
//                case R.id.about: {
//                    Intent goToScreen = new Intent(HomeVisitActivityByCc.this, AboutActivity.class);
//                    goToScreen.putExtra("globalState",getIntent().getBundleExtra("globalState"));
//                    displayAlert(getResources().getString(R.string.exit), goToScreen);
//                    return true;
//                }
                //mani 19July2021
                case R.id.covvaccine:
                    displaycovidvaccinedetails();
                    break;
            }
        } catch (Exception e) {

        }
        return super.onOptionsItemSelected(item);
    }

    private void displayAlert(final String spanText2, final Intent goToScreen) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set dialog message
        alertDialogBuilder.setMessage(spanText2).setCancelable(false).setNegativeButton(
                (getResources().getString(R.string.m121)), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (goToScreen != null) {
                            try {
                                startActivity(goToScreen);
                                if(spanText2.contains("logout"))
                                {
                                    try {
                                        LoginRepository loginRepo = new LoginRepository(databaseHelper);
                                        loginRepo.updateLogoutTime(appState.loginAuditId, appState.sessionUserId);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            dialog.cancel();
                        }
                    }
                }).setPositiveButton((getResources().getString(R.string.m122)),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    /**
     * Initializes the Action Bar items
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        try {
            //16Sep2019 - Bindu
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer1);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);

            aq.id(R.id.tvWomanName).text(woman.getRegWomanName());
            if (woman.getRegADDate() != null && woman.getRegADDate().length() > 0) {
                aq.id(R.id.tvInfo1).text(woman.getRegADDate());
            }else {
                int noOfWeeks = DateTimeUtil.getNumberOfWeeksFromLMP(woman.getRegLMP());
                aq.id(R.id.tvInfo1).text(getResources().getString(R.string.tvlmp) + woman.getRegLMP() + "  , GA : " + noOfWeeks + "W");
            }

            aq.id(R.id.ivWomanImg).gone();

            getMenuInflater().inflate(R.menu.servicesmenu, menu);

            menu.findItem(R.id.logout).setTitle((getResources().getString(R.string.action_logout)));
            menu.findItem(R.id.covvaccine).setVisible(true);//17May2021 Bindu

        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Sync the toggle state after onRestoreInstanceState has occurred.
//        mDrawerToggle.syncState();
    }

    /**
     * The click listner for ListView in the navigation drawer
     */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            mDrawerLayout.closeDrawer(mDrawerList);
            //   if (woman.getRegADDate() == null || woman.getRegADDate().length() == 0) {
            try {
                if (position == 0) {
                    Intent viewprof = new Intent(com.sc.stmansi.HomeVisit.HomeVisitActivityByMC.this, ViewProfileActivity.class);
                    viewprof.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                    startActivity(viewprof);
                }
                //             Mani Jun 1 2021 Removed UnplannedServicesListActivity & ServicesSummaryActivity
                else if (position == 1) {
                    if(woman.getRegPregnantorMother() == 2 ){ //02Dec2019 - Bindu - add condition - ANC/PNC Home visit
                        Intent nextScreen = new Intent(com.sc.stmansi.HomeVisit.HomeVisitActivityByMC.this, PncHomeVisit.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        displayHomevisitConfirmation(woman, nextScreen);
//                        startActivity(nextScreen);
                    } else {
                        Intent homevisit = new Intent(com.sc.stmansi.HomeVisit.HomeVisitActivityByMC.this, HomeVisit.class);
                        homevisit.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        displayHomevisitConfirmation(woman, homevisit);
                    }
                   /* if(woman.getRegPregnantorMother() == 2 ){ //02Dec2019 - Bindu - add condition - ANC/PNC Home visit
                        Intent nextScreen = new Intent(HomeVisitActivityByCc.this, PncHomeVisit.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        startActivity(nextScreen);
                    } else {
                        int daysDiff = DateTimeUtil.getDaysBetweenDates(DateTimeUtil.getTodaysDate(), woman.getRegLMP()); //02Dec2019 - Bindu - chk for nearing del
                        if (daysDiff > 210) {
//                            displayNearingDelAlertMessage(woman);
                        } else {
                            Intent homevisit = new Intent(HomeVisitActivityByCc.this, HomeVisit.class);
                            homevisit.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(homevisit);
                        }
                    }*/
                }else if (position == 2) {
                    Intent homevisit = new Intent(com.sc.stmansi.HomeVisit.HomeVisitActivityByMC.this, HomeVisitListActivity.class);
                    homevisit.putExtra("woman", woman);
                    homevisit.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                    startActivity(homevisit);
                }//01Oct2019 Arpitha
                else if(position ==3 ) {
                    if(woman.getRegPregnantorMother()  == 1)
                    {
                        Toast.makeText(getApplicationContext(),
                                getResources().getString(R.string.m067), Toast.LENGTH_LONG).show();

                    }else {
                        if (woman.getRegPregnantorMother() == 2 || DateTimeUtil.getNumberOfDaysFromLMP(woman.getRegLMP()) > 210) {
                            Intent del = new Intent(com.sc.stmansi.HomeVisit.HomeVisitActivityByMC.this, DeliveryInfoActivity.class);
                            del.putExtra("woman", woman);
                            del.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(del);
                        } else
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.m132), Toast.LENGTH_LONG).show();
                    }
                }
//                28Nov2019 Arpitha
                //mani:1july2021 added deactivation condition
                else if(position == 4)
                {
                    if (woman.getDateDeactivated() != null && woman.getDateDeactivated().trim().length() > 0)
                    {
                        Intent deact = new Intent(com.sc.stmansi.HomeVisit.HomeVisitActivityByMC.this, WomanDeactivateActivity.class);
                        deact.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        deact.putExtra("woman", woman);
                        startActivity(deact);
                    }else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.notdeactivated), Toast.LENGTH_SHORT).show();
                    }
                }
            } catch (Exception e) {

            }

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        /*
         * You'll need this in your class to release the helper when done.
         */
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

    //02Dec2019 - Bindu - Alert message nearing del
    public  void displayHomevisitConfirmation(tblregisteredwomen regwoman, Intent nextScreen) throws Exception{
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        View convertView = LayoutInflater.from(this).inflate(R.layout.activity_homevisitdirect, null, false);

//        String alertmsg =  regwoman.getRegWomanName() + ", " + " EDD : " + regwoman.getRegEDD() ;
        String alertmsg =  regwoman.getRegWomanName();
        String msg = "<font color='#CC3333'> Home visit - </font>"+alertmsg;

        aqhvdialog = new AQuery(convertView);

        rdbtnsms = convertView.findViewById(R.id.rdbtnsms);
        rdbtndirect = convertView.findViewById(R.id.rdbtndirect);
        llsmscontent = convertView.findViewById(R.id.llsmscontent);
        etvisitno = convertView.findViewById(R.id.etmmvisitnum);
        etvisitdate = convertView.findViewById(R.id.etmmvisitdate);
        etvisitdate.setText(DateTimeUtil.getTodaysDate());
        llsmscontent.setVisibility(View.GONE);
        rdbtnsms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                llsmscontent.setVisibility(View.VISIBLE);
            }
        });

        rdbtndirect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                llsmscontent.setVisibility(View.GONE);
            }
        });

        aqhvdialog.id(R.id.etmmvisitdate).getEditText().setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                try {
                    if (MotionEvent.ACTION_UP == event.getAction()) {
                        assignEditTextAndCallDatePicker(aqhvdialog.id(R.id.etmmvisitdate).getEditText());
                    }
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                    crashlytics.log(e.getMessage());

                }
                return true;
            }
        });

        aqhvdialog.id(R.id.imgcleardate).getImageView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aqhvdialog.id(R.id.etmmvisitdate).text(DateTimeUtil.getTodaysDate());
            }
        });

        alertDialogBuilder.setView(convertView);

        alertDialogBuilder.setCancelable(true)
                .setTitle(Html.fromHtml(msg))
                .setPositiveButton(getResources().getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                            }
                        })
                .setNegativeButton(getResources().getString(R.string.cancel), //01Oct2019 - Bindu - Add cancel button
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.cancel();
                            }
                        });
        final AlertDialog dialog = alertDialogBuilder.create();
        if (dialog != null)
            dialog.show();


        // Overriding the handler immediately after show is probably a
        // better
        // approach than OnShowListener as described below
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if(validateHomevisit()) {
                        wantToCloseDialog = true;
                        int vno = 0;
                        if(aqhvdialog.id(R.id.etmmvisitnum).getText().toString().trim().length() > 0) {
                            vno = Integer.parseInt(aqhvdialog.id(R.id.etmmvisitnum).getText().toString()) ;
                        }
                        Bundle b = new Bundle();
                        b.putInt("complVisitNum", vno);
                        b.putString("complVisitDate",aqhvdialog.id(R.id.etmmvisitdate).getText().toString());
                        if(rdbtnsms.isChecked()) {
                            b.putString("complvisittype", "SMS");
                        }else
                            b.putString("complvisittype", "Direct");

                        /*Intent nextScreen = new Intent(HomeVisitListActivity.this, HomeVisit.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));*/
                        nextScreen.putExtra("woman", woman);
                        nextScreen.putExtras(b); //01Jul2021 Bindu
                        startActivity(nextScreen);
                        dialog.cancel();
                    }
                    if (wantToCloseDialog)
                        dialog.dismiss();
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                    crashlytics.log(e.getMessage());
                }

            }
        });
    }

    // Validate Home visit check
    private boolean validateHomevisit() {

        if(!(rdbtnsms.isChecked() || rdbtndirect.isChecked())){
//            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsselectvisittype), this );
            Toast.makeText( getApplicationContext(), getResources().getString(R.string.plsselectvisittype), Toast.LENGTH_SHORT).show();
            wantToCloseDialog = false;
            return false;
        }

        if(rdbtnsms.isChecked()) {
            if (aqhvdialog.id(R.id.etmmvisitnum).getText().toString().trim().length() <= 0) {
//            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsentervisitnum), this);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.plsentervisitnum), Toast.LENGTH_SHORT).show();
                aqhvdialog.id(R.id.etmmvisitnum).getEditText().requestFocus();
                wantToCloseDialog = false;
                return false;
            }
       if (aqhvdialog.id(R.id.etmmvisitdate).getText().toString().trim().length() <= 0) {
//            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsentervisitdatemm), this);
            Toast.makeText( getApplicationContext(), getResources().getString(R.string.plsentervisitdatemm), Toast.LENGTH_SHORT).show();
            aqhvdialog.id(R.id.etmmvisitdate).getEditText().requestFocus();
           wantToCloseDialog = false;

            return false;
        }
        }
        return true;
    }

    /**
     * This method opens the Date Dialog and sets Date to respective EditText
     */
    private void assignEditTextAndCallDatePicker(EditText editText) {
        String currDate = (editText.getText().length() > 0) ? editText.getText().toString() : "";
        CommonDatePickerFragment.getCommonDate(this, currDate);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        try {
            aqhvdialog.id(R.id.imgcleardate).background(R.drawable.brush1);
            if (view.isShown()) {
                String str = String.format("%02d", day) + "-" + String.format("%02d", month + 1) + "-"
                        + year;
                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                Date seldate = format.parse(str);

                if (seldate.after(new Date())) {
                    AlertDialogUtil.displayAlertMessage(
                            getResources().getString(R.string.date_cannot_af_currentdate)
                            , this);
                    aqhvdialog.id(R.id.etmmvisitdate).text("");
                } else {
                    aqhvdialog.id(R.id.etmmvisitdate).text(str);
                    aqhvdialog.id(R.id.imgcleardate).visible();
                }

            }

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private String setWomanCompl(String dangersign) throws SQLException {


        ArrayList<String> list = new ArrayList<>();
        String symptom = "";
        if (dangersign != null && dangersign.length() > 0) {
            list = new ArrayList<String>(Arrays.asList(dangersign.split("[,-]")));

            if (list != null && list.size() > 0) {

                for (int i = 0; i < list.size(); i++) {
                    String strSy = list.get(i);
                    strSy = strSy.replaceAll(" ", "");
                    strSy = strSy.toLowerCase().trim();

                    //check directly and replace
                    if(strSy.contains("notrelieved"))
                        strSy = "constipationnotrelieved"; //15Jul2021 Bindu
                    if(strSy.contains("bleedingp/v"))
                        strSy = "bleedingpv"; //15Jul2021 Bindu
                    if(strSy.contains("leakingofwatery"))
                        strSy = "leakingofwateryfluidspv"; //15Jul2021 Bindu
                    if(strSy.contains("twin/multiplepregnancy"))
                        strSy = "neardeltwinormultiplepregnancy";
                    if(strSy.contains("laborpainsmorethan12hours"))
                        strSy = "laborpainmorethan12hours";
                    if(strSy.contains("motherswithcpd/contractedpelvis"))
                        strSy = "neardelmotherscpdorcontpelvis";
                    if(strSy.contains("foetalmovementdecreased"))
                        strSy = "decreasedfetalmovements";
                    if(strSy.contains("foetalmovementabsent"))
                        strSy = "fetalmovementabsent";
                    if(strSy.contains("iugr"))
                        strSy = "neardeliugr";
                  /*  if(strSy.contains("engorged"))
                        strSy = "painfulengorged";
                    if(strSy.contains("swollen"))
                        strSy = "swollen";*/


//                    for pnc
                    if(strSy.contains("inabilitytocontroldefecation"))
                        strSy = "inabilitytocontroldefecationorurine";
                    if(strSy.contains("bleedingnipples"))
                        strSy = "painfulengorged";
                    if(strSy.contains("swollenhands"))
                        strSy = "swollen";
                    if(strSy.contains("painful") ||
                            strSy.contains("engorgedbreasts") || strSy.contains("cracked") || strSy.contains("faceandlegs"))
                        strSy = "";
                    if(strSy.trim().length()>0) {
                        int identifier = getResources().getIdentifier
                                (strSy,
                                        "string", getPackageName()); //25Jun2021 Bindu get package name from app
                        if (identifier > 0) {
                            if (i == list.size() - 1)
                                symptom = symptom + getResources().getString(identifier);
                            else
                                symptom = symptom + getResources().getString(identifier) + ",";
                        } else {
                            if (i == list.size() - 1)
                                symptom = symptom + strSy;
                            else
                                symptom = symptom + strSy + ",";
                        }
                    }

                }
            }
        }return symptom;
    }

    private String setChildCompl(String dangersign) throws SQLException {

        ArrayList<String> symptoms = new ArrayList<>();
        ArrayList<String> list = new ArrayList<>();
        String symptom = "";
        if (dangersign != null && dangersign.length() > 0) {
            list = new ArrayList<String>(Arrays.asList(dangersign.split(",")));

            if (list != null && list.size() > 0) {

                for (int i = 0; i < list.size(); i++) {
                    String strSy = list.get(i);

                    List<TblSymptomMaster> symptomMasterList = databaseHelper.getTblSymptomMasterDao().queryBuilder()
                            .selectColumns("symXmlStringName").where()
                            .eq("symSymptomName", strSy).query();

                    if (symptomMasterList != null && symptomMasterList.size() > 0) {
                        symptoms.add(symptomMasterList.get(0).getSymXmlStringName());
                    }else {
                        if (strSy.toLowerCase().trim().contains("grunting")) //21Jul2021 Bindu manually add
                            symptoms.add("nbfastbreathing");
                        if (strSy.toLowerCase().trim().contains("diarrhea")) //21Jul2021 Bindu manually add
                            symptoms.add("nbdiarrhoea");
                    }
                }

                if(symptoms != null) {
                    String strSy = "";
                    for(int i=0; i< symptoms.size();i++) {
                        strSy = symptoms.get(i);
                        strSy = strSy.replaceAll(" ", "");
                        strSy = strSy.toLowerCase().trim();
                        int identifier = getResources().getIdentifier
                                (strSy,
                                        "string", getPackageName()); //25Jun2021 Bindu get package name from app
                        if (identifier > 0) {
                            if (i == list.size() - 1)
                                symptom = symptom + getResources().getString(identifier);
                            else
                                symptom = symptom + getResources().getString(identifier) + ",";
                        } else {
                            if (i == list.size() - 1)
                                symptom = symptom + strSy;
                            else
                                symptom = symptom + strSy + ",";
                        }
                    }
                }
            }
            symptom = symptom.replace(getResources().getString(R.string.nbanybirthdefects),
                    getResources().getString(R.string.nbanybirthdefectsdisp));
        }return symptom;
    }

    //mani 19july2021
    private void displaycovidvaccinedetails() {
        try {

            //Arpitha 09Aug2021
            Locale locale = null;
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

            if (prefs.getBoolean("isEnglish", false)) {
                locale = new Locale("en");
                font = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
            } else if (prefs.getBoolean("isKannada", false)) {  //10Nov2019 - Bindu - Add kannada
                locale = new Locale("kn");
                font = Typeface.createFromAsset(getAssets(), "Lohit-Kannada.ttf");
            } else if (prefs.getBoolean("isTelugu", false)) {  //02Apr2021 - Add Telugu
                locale = new Locale("te");
                font = Typeface.createFromAsset(getAssets(), "Lohit-Kannada.ttf");
            } else {
                locale = new Locale("en");
                font = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf"); // 04Feb2021 - Bindu - change default to english
            }

            Configuration config =  getResources().getConfiguration();
            config.locale = locale;
            getResources().updateConfiguration(config,  getResources().getDisplayMetrics());



            final AlertDialog.Builder alert = new AlertDialog.Builder(this,
                    AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
            final View customView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.covid_vaccine,
                    null);
            alert.setView(customView);

            AQuery aq = new AQuery(customView);

            getCovidVaccineDetails(aq);
//          09Aug2021 Arpitha
            aq.id(R.id.rd_CovidVaccineYes).enabled(false);
            aq.id(R.id.rd_CovidVaccineNo).enabled(false);


            alert.setCancelable(true)
                    .setTitle(getResources().getString(R.string.coviddetails))
                    .setNegativeButton(getResources().getString(R.string.cancel),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    dialog.cancel();
                                }
                            });
            final AlertDialog dialog = alert.create();
            if (dialog != null)
                dialog.show();

            dialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }


    //mani 19july2021
    private void getCovidVaccineDetails(AQuery aq) throws Exception{
        CovidDetailsRepository covRepo = new CovidDetailsRepository(databaseHelper);
        covVacItems = covRepo.getVisitCovidVaccineDetails(appState.selectedWomanId);
        if(covVacItems != null && covVacItems.size() > 0) {
            for(int i=0; i < covVacItems.size(); i++) {
//                if(covVacItems.get(i).getCovidVaccinated().equalsIgnoreCase(getResources().getString(R.string.yesshortform))) {
//                    isvaccinated = true;
                aq.id(R.id.rd_CovidVaccineYes).enabled(false);
                aq.id(R.id.rd_CovidVaccineNo).enabled(false);
                aq.id(R.id.rd_CovidVaccineYes).checked(true);
                    /*if((covVacItems.get(i).getCovidFirstDoseDate().toString().length() > 0) && covVacItems.get(i).getCovidSecondDoseDate().toString().length() > 0){
                        aq.id(R.id.trCovidFirstDoseDate).getView().setVisibility(View.VISIBLE);
                        aq.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.VISIBLE);
                        aq.id(R.id.etVisitCovidFirstDoseDate).text(covVacItems.get(i).getCovidFirstDoseDate());
                        aq.id(R.id.etVisitCovidSecondDoseDate).text(covVacItems.get(i).getCovidSecondDoseDate());
                    }
                    else if(covVacItems.get(i).getCovidFirstDoseDate().toString().length() > 0 && covVacItems.get(i).getCovidSecondDoseDate().toString().length() <= 0) {
                        aq.id(R.id.etVisitCovidFirstDoseDate).text(covVacItems.get(i).getCovidFirstDoseDate());
                        aq.id(R.id.trCovidFirstDoseDate).getView().setVisibility(View.VISIBLE);
                        aq.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.VISIBLE);
                    }*/

                if (covVacItems.get(i).getCovidVaccinatedNo().equalsIgnoreCase("1")) {
                    aq.id(R.id.trCovidFirstDoseDate).getView().setVisibility(View.VISIBLE);
                    aq.id(R.id.etVisitCovidFirstDoseDate).text(covVacItems.get(i).getCovidVaccinatedDate());
                    aq.id(R.id.etVisitCovidFirstDoseDate).enabled(false).background(R.drawable.edittext_disable);
                }
                if (covVacItems.get(i).getCovidVaccinatedNo().equalsIgnoreCase("2")) {
                    aq.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.VISIBLE);
                    aq.id(R.id.etVisitCovidSecondDoseDate).text(covVacItems.get(i).getCovidVaccinatedDate());
                    aq.id(R.id.etVisitCovidSecondDoseDate).enabled(false).background(R.drawable.edittext_disable);
                }
               /* }else {
                    aq.id(R.id.rd_CovidVaccineNo).checked(true);
                    aq.id(R.id.trCovidFirstDoseDate).getView().setVisibility(View.GONE);
                }*/
            }
        }else {
            aq.id(R.id.rd_CovidVaccineNo).checked(true);
            aq.id(R.id.trCovidFirstDoseDate).getView().setVisibility(View.GONE);
            aq.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.GONE); //21May2021 Bindu
        }
    }
}