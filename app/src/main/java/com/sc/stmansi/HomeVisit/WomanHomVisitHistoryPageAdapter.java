package com.sc.stmansi.HomeVisit;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.sc.stmansi.HomeVisit.VisitHistoryFragments.WomanHVFragmentCC;
import com.sc.stmansi.HomeVisit.VisitHistoryFragments.WomanHVFragmentMC;
import com.sc.stmansi.HomeVisit.VisitHistoryFragments.WomanHVFragmentMM;
import com.sc.stmansi.R;
import com.sc.stmansi.tables.TblChildInfo;


public class WomanHomVisitHistoryPageAdapter extends FragmentPagerAdapter {
    private final Context context;
    private final Bundle bundle;
    private final  String womanId;



    public WomanHomVisitHistoryPageAdapter(@NonNull FragmentManager fm, Context context, Bundle bundle, String womanId) {
        super(fm);
        this.context = context;
        this.bundle = bundle;
        this.womanId = womanId;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        try{
            //mani 30Aug2021
            switch (position){
                case 0:
                    WomanHVFragmentMM fragmentMM = new WomanHVFragmentMM();
                    bundle.putString("womanId",womanId);
                    fragmentMM.setArguments(bundle);
                    return fragmentMM;
                case 1:
                    WomanHVFragmentCC fragmentCC = new WomanHVFragmentCC();
                    bundle.putString("womanId",womanId);
                    fragmentCC.setArguments(bundle);
                    return fragmentCC;
                case 2:
                    WomanHVFragmentMC fragmentMC = new WomanHVFragmentMC();
                    bundle.putString("womanId",womanId);
                    fragmentMC.setArguments(bundle);
                    return fragmentMC;
            }
        }catch (Exception e){
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position)
    {
        String title = null;
        if (position == 0)
            title = context.getResources().getString(R.string.userlevel_mm);
        else if (position == 1)
            title = context.getResources().getString(R.string.userlevel_cc);
        else if (position == 2)
            title = context.getResources().getString(R.string.userlevel_mc);
        return title;
    }
}
