package com.sc.stmansi.HomeVisit;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.legacy.app.ActionBarDrawerToggle;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.androidquery.AQuery;
import com.google.android.material.tabs.TabLayout;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.About.AboutActivity;
import com.sc.stmansi.R;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.common.NavDrawerItem;
import com.sc.stmansi.common.NavDrawerListAdapter;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.configuration.SyncState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.deactivate.WomanDeactivateActivity;
import com.sc.stmansi.delivery.DeliveryInfoActivity;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.recyclerview.ClickListener;
import com.sc.stmansi.repositories.LoginRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.tables.TblChildInfo;
import com.sc.stmansi.tables.TblVisitHeader;
import com.sc.stmansi.tables.tblCovidVaccineDetails;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.viewprofile.ViewProfileActivity;
import com.sc.stmansi.womanlist.RegisteredWomenActionTabs;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class HomeVisitListActivity extends AppCompatActivity implements ClickListener {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter homeVisitViewAdapter;

    private List<TblVisitHeader> visitItems;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private ArrayList<NavDrawerItem> navDrawerItems;
    private ListView mDrawerList;
    private tblregisteredwomen woman;
    private AQuery aq;
    private TblVisitHeader visitDetails;
    private int itemPosition;

    private AppState appState;
    private DatabaseHelper databaseHelper;
    private List<tblCovidVaccineDetails> covVacItems;
    private boolean isvaccinated = false;


    private ViewPager viewPagerHV;
    private WomanHomVisitHistoryPageAdapter mAdapterHv;
    private int currentTab;
    private TabLayout tablayoutHv;
    private SyncState syncState;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_homevisit_viewlist);
//mani 22june2021 : Fragments created and recycler view will be displayed using fragmenet
//        recyclerView = findViewById(R.id.homevisit_recycler_view);
//        layoutManager = new LinearLayoutManager(this);
//        recyclerView.setLayoutManager(layoutManager);
        try {

            aq = new AQuery(this);
            databaseHelper = getHelper();
            Bundle bundle = getIntent().getBundleExtra("globalState");
            appState = bundle.getParcelable("appState");
            syncState = bundle.getParcelable("syncState");


            WomanRepository womanRepo = new WomanRepository(databaseHelper);
            String womanId;

            if(appState.selectedWomanId == null || appState.selectedWomanId.isEmpty()) {
                womanId = getIntent().getStringExtra("womanId");
            } else {
                womanId = appState.selectedWomanId;
                }




            woman = womanRepo.getRegistartionDetails(womanId, appState.sessionUserId);

            initiateDrawer();


//mani 22june2021
            viewPagerHV = findViewById(R.id.pagerHV);
            viewPagerHV.setOffscreenPageLimit(3);

            mAdapterHv=new WomanHomVisitHistoryPageAdapter(getSupportFragmentManager(),getApplicationContext(),bundle,womanId);
            viewPagerHV.setAdapter(mAdapterHv);
            tablayoutHv=findViewById(R.id.tablayoutWomanHv);
            tablayoutHv.setupWithViewPager(viewPagerHV);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }


    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }
    //mani 22june2021
    @Override
    public void onClick(View v) {
//        try {
//            setSelectedVisitDetails(v);
//            Intent intent = new Intent(getApplicationContext(), HomeVisitViewActivity.class);
//            intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
//            intent.putExtra("pos", itemPosition);
//            startActivity(intent);
//        }catch (Exception e)
//        {
//            e.printStackTrace();
//        }
    }

    //    set selected Visit details id
    private void setSelectedVisitDetails(View v)  throws Exception{
        itemPosition = recyclerView.getChildLayoutPosition(v);
        visitDetails = visitItems.get(itemPosition);

    }


    /**
     * Initiates the Navigation drawer
     */
    private void initiateDrawer() {
        try {
            mDrawerLayout = findViewById(R.id.ett_drawer_layout);
            mDrawerList = findViewById(R.id.ett_left_drawer);

//             Mani Jun 1 2021 Removed UnplannedServicesListActivity & ServicesSummaryActivity

            navDrawerItems = new ArrayList<NavDrawerItem>();


            // adding nav drawer items to array
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.sp_WRegEdit), R.drawable.registration));

            //14Sep2019 - Bindu - Add Homevisit
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.homevisitadd),
                    R.drawable.prenatalhomevisit));

            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deliveryHeading), R.drawable.delivery_info));//01Oct2019 Arpitha



            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deactivate),
                    R.drawable.deactivate));//28Nov2019 Arpitha


            // set a custom shadow that overlays the main content when the drawer
            // opens
            mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
            mDrawerList.setAdapter(new NavDrawerListAdapter(getApplicationContext(), navDrawerItems));
            mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

            // ActionBarDrawerToggle ties together the the proper interactions
            // between the sliding drawer and the action bar app icon
            mDrawerToggle = new ActionBarDrawerToggle(this, /* host Activity */
                    mDrawerLayout, /* DrawerLayout object */
                    R.drawable.ic_drawer, /*
             * nav drawer image to replace 'Up'
             * caret
             */
                    R.string.drawer_open, /*
             * "open drawer" description for
             * accessibility
             */
                    R.string.drawer_close /*
             * "close drawer" description for
             * accessibility
             */
            ) {
                public void onDrawerClosed(View view) {
                    try {
                        displayDrawerItems();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                public void onDrawerOpened(View drawerView) {
                    try {
                        displayDrawerItems();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            mDrawerLayout.setDrawerListener(mDrawerToggle);
        } catch (Exception e) {

        }
    }

    // 18Sep2019 - Bindu Display drawer open close items
    private void displayDrawerItems() throws Exception{

        invalidateOptionsMenu(); // creates call to
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer1);
        getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);


        aq.id(R.id.ivWomanImg).getImageView().setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                mDrawerLayout.closeDrawer(mDrawerList);
            }
        });

        aq.id(R.id.tvWomanName).text((woman.getRegWomanName() + "\t"));
        if (woman.getRegADDate() != null && woman.getRegADDate().length() > 0) {
            aq.id(R.id.tvInfo1).text(woman.getRegADDate());
        }else {
            int noOfWeeks = DateTimeUtil.getNumberOfWeeksFromLMP(woman.getRegLMP());
            aq.id(R.id.tvInfo1).text(getResources().getString(R.string.tvlmp) + woman.getRegLMP() + "  , GA : " + noOfWeeks + "W");
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        try {
            switch (item.getItemId()) {
                case android.R.id.home:
                    Intent home = new Intent(HomeVisitListActivity.this, MainMenuActivity.class);
                    home.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                    /*startActivity(home);*/
                    displayAlert(getResources().getString(R.string.exit), home);
                    break;

                case R.id.wlist:
                    Intent wlist = new Intent(HomeVisitListActivity.this, RegisteredWomenActionTabs.class);
                    wlist.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                    /*startActivity(wlist);*/
                    displayAlert(getResources().getString(R.string.exit), wlist);
                    break;

                case R.id.logout:
                    Intent logout = new Intent(HomeVisitListActivity.this, LoginActivity.class);
                    displayAlert(getResources().getString(R.string.m111), logout);
                    break;

                case R.id.home:
                    Intent intent = new Intent(HomeVisitListActivity.this, MainMenuActivity.class);
                    intent.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                    displayAlert(getResources().getString(R.string.exit), intent);
                    break;
                //30Sep2019 - Bindu
                //mani2021june7
                case R.id.about: {
                    Intent goToScreen = new Intent(HomeVisitListActivity.this, AboutActivity.class);
                    goToScreen.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                    displayAlert(getResources().getString(R.string.exit), goToScreen);
                    return true;
                }
//              17May2021 Bindu

                //mani june 17
//                case R.id.covvaccine:
//                    displaycovidvaccinedetails();
//                    break;
            }
        } catch (Exception e) {

        }
        return super.onOptionsItemSelected(item);
    }

    private void displayAlert(final String spanText2, final Intent goToScreen) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set dialog message
        alertDialogBuilder.setMessage(spanText2).setCancelable(false).setNegativeButton(
                (getResources().getString(R.string.m121)), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (goToScreen != null) {
                            try {
                                startActivity(goToScreen);
                                if(spanText2.contains("logout"))
                                {
                                    try {
                                        LoginRepository loginRepo = new LoginRepository(databaseHelper);
                                        loginRepo.updateLogoutTime(appState.loginAuditId, appState.sessionUserId);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            dialog.cancel();
                        }
                    }
                }).setPositiveButton((getResources().getString(R.string.m122)),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    /**
     * Initializes the Action Bar items
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        try {
            //16Sep2019 - Bindu
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer1);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);

            aq.id(R.id.tvWomanName).text(woman.getRegWomanName());
            if (woman.getRegADDate() != null && woman.getRegADDate().length() > 0) {
                aq.id(R.id.tvInfo1).text(woman.getRegADDate());
            }else {
                int noOfWeeks = DateTimeUtil.getNumberOfWeeksFromLMP(woman.getRegLMP());
                aq.id(R.id.tvInfo1).text(getResources().getString(R.string.tvlmp) + woman.getRegLMP() + "  , GA : " + noOfWeeks + "W");
            }

            aq.id(R.id.ivWomanImg).gone();

            getMenuInflater().inflate(R.menu.servicesmenu, menu);

            menu.findItem(R.id.logout).setTitle((getResources().getString(R.string.action_logout)));

            //mani june17 2021
            menu.findItem(R.id.covvaccine).setVisible(false);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        //mani 22june2021
        // Sync the toggle state after onRestoreInstanceState has occurred.
//        mDrawerToggle.syncState();
    }

    @Override
    public boolean onLongClick(View view) {
        return false;
    }


    /**
     * The click listner for ListView in the navigation drawer
     */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            mDrawerLayout.closeDrawer(mDrawerList);
            //   if (woman.getRegADDate() == null || woman.getRegADDate().length() == 0) {
            try {
                if (position == 0) {
                    Intent viewprof = new Intent(HomeVisitListActivity.this, ViewProfileActivity.class);
                    viewprof.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                    startActivity(viewprof);
                }
                //14Sep2019 - Bindu - Add Home visit
                else if (position == 1) {
                    if(woman.getRegPregnantorMother() == 2 ){ //02Dec2019 - Bindu - add condition - ANC/PNC Home visit

                            Intent nextScreen = new Intent(HomeVisitListActivity.this, PncHomeVisit.class);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(nextScreen);


                    } else {
                        int daysDiff = DateTimeUtil.getDaysBetweenDates(DateTimeUtil.getTodaysDate(), woman.getRegLMP()); //02Dec2019 - Bindu - check for nearing del
                        if (daysDiff > 210) {
                            displayNearingDelAlertMessage(woman);
                        } else {
                            Intent homevisit = new Intent(HomeVisitListActivity.this, HomeVisit.class);
                            homevisit.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(homevisit);
                        }
                    }
                }//01Oct2019 Arpitha
                else if(position ==2) {
                    if (woman.getRegPregnantorMother() == 2 || DateTimeUtil.getNumberOfDaysFromLMP(woman.getRegLMP()) > 210) {
                        Intent del = new Intent(HomeVisitListActivity.this, DeliveryInfoActivity.class);
                        del.putExtra("woman", woman);
                        del.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        startActivity(del);
                    } else
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.m132), Toast.LENGTH_LONG).show();
                }
                //                  28Nov2019 Arpitha
                else if(position == 3) {
                    Intent deact = new Intent(HomeVisitListActivity.this, WomanDeactivateActivity.class);
                    deact.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    deact.putExtra("woman", woman);
                    startActivity(deact);
                }
            } catch (Exception e) {

            }

        }
    }

    @Override
    public void onBackPressed() {

    }

    //02Dec2019 - Bindu - Alert message nearing del
    public  void displayNearingDelAlertMessage(tblregisteredwomen regwoman) throws Exception{
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        String alertmsg =  regwoman.getRegWomanName() + ", " + " EDD : " + regwoman.getRegEDD() ;
        //alertDialogBuilder.setMessage(message).setCancelable(false).setTitle(Html.fromHtml("<font color='#FF7F27'>Alert -- </font><br><br><p>"+alertmsg + "</p>")).setNegativeButton(
        String msg = "<font color='#CC3333'> Nearing Delivery - </font>"+alertmsg;
        alertDialogBuilder.setMessage(getResources().getText(R.string.alerthomevisitnearingdelmsg)).setCancelable(false).setTitle(Html.fromHtml(msg)).setNegativeButton(
                getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent nextScreen = new Intent(HomeVisitListActivity.this, HomeVisit.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        nextScreen.putExtra("woman", woman);
                        startActivity(nextScreen);
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    //    get covid vaccine details
    //mani 2021 june 17:removed covid vaccine details
}
