package com.sc.stmansi.HomeVisit;

public class VisitDetailsPojo {
    private String ChildId;
    private String Symptomsname;
    private String RefTo;
    private String sympid;

    //30Nov2019 - Bindu
    private String weight;
    private String weightdate;
    private String temp;
    private String tempdate;
    private String resprate;
    private String respratedate;
    private boolean childregatgovtfac;
    private String childregid;
    private String childregdate;

    private int ChildNo;

    //mani 14Sep2021
    private String ifataken;
    private String ifaconsume;
    private String ifacount;
    private String ifanotgivenreason;
    private Integer ifagivenby;
    private  String dewormingtablet;

    public String getIfataken() {
        return ifataken;
    }

    public void setIfataken(String ifataken) {
        this.ifataken = ifataken;
    }

    public String getIfaconsume() {
        return ifaconsume;
    }

    public void setIfaconsume(String ifaconsume) {
        this.ifaconsume = ifaconsume;
    }

    public String getIfacount() {
        return ifacount;
    }

    public void setIfacount(String ifacount) {
        this.ifacount = ifacount;
    }

    public String getIfanotgivenreason() {
        return ifanotgivenreason;
    }

    public void setIfanotgivenreason(String ifanotgivenreason) {
        this.ifanotgivenreason = ifanotgivenreason;
    }

    public Integer getIfagivenby() {
        return ifagivenby;
    }

    public void setIfagivenby(Integer ifagivenby) {
        this.ifagivenby = ifagivenby;
    }

    public String getDewormingtablet() {
        return dewormingtablet;
    }

    public void setDewormingtablet(String dewormingtablet) {
        this.dewormingtablet = dewormingtablet;
    }

    public Integer getChildVisitNumber() {
        return childVisitNumber;
    }

    public void setChildVisitNumber(Integer childVisitNumber) {
        this.childVisitNumber = childVisitNumber;
    }

    private Integer childVisitNumber;

    //30Nov2019 Bindu
    public VisitDetailsPojo(String id, String wt, String wtdt, String temp, String tempdt, String rr, String rrdt, boolean childregatgovfac, String childregid,String childregdate, int ChlNo) {
       this.ChildId = id;
       this.weight = wt;
       this.weightdate = wtdt;
       this.temp = temp;
       this.tempdate = tempdt;
       this.resprate = rr;
       this.respratedate = rrdt;
       this.childregatgovtfac = childregatgovfac;
       this.childregid = childregid;
       this.childregdate = childregdate;
       this.ChildNo = ChlNo;
    }

    //12Sep2021 Mani
    public VisitDetailsPojo(String id, String wt, String wtdt, String temp, String tempdt, String rr, String rrdt, boolean childregatgovfac, String childregid,String childregdate, int ChlNo, Integer visitNumber,String ifataken, String ifaconsume,String ifacount,String ifanotgivenreason,Integer ifagivenby, String dewormingtablet ) {
        this.ChildId = id;
        this.weight = wt;
        this.weightdate = wtdt;
        this.temp = temp;
        this.tempdate = tempdt;
        this.resprate = rr;
        this.respratedate = rrdt;
        this.childregatgovtfac = childregatgovfac;
        this.childregid = childregid;
        this.childregdate = childregdate;
        this.ChildNo = ChlNo;
        this.childVisitNumber=visitNumber;
        this.ifataken=ifataken;
        this.ifaconsume=ifaconsume;
        this.ifacount=ifacount;
        this.ifanotgivenreason=ifanotgivenreason;
        this.ifagivenby=ifagivenby;
        this.dewormingtablet=dewormingtablet;
    }

    //01Dec2019 Bindu
    public VisitDetailsPojo(int chlno, String wt, String wtdt, String temp, String tempdt, String rr, String rrdt, boolean childregatgovfac, String childregid,String childregdate) {
        this.ChildNo = chlno;
        this.weight = wt;
        this.weightdate = wtdt;
        this.temp = temp;
        this.tempdate = tempdt;
        this.resprate = rr;
        this.respratedate = rrdt;
        this.childregatgovtfac = childregatgovfac;
        this.childregid = childregid;
        this.childregdate = childregdate;
    }

    public VisitDetailsPojo(String id, String sym, String ref) {
        this.sympid = id;
        this.Symptomsname = sym;
        this.RefTo = ref;
    }

    public VisitDetailsPojo(String id, String sym, String ref,String childId) {
        this.sympid = id;
        this.Symptomsname = sym;
        this.RefTo = ref;
        this.ChildId = childId;
    }

    public VisitDetailsPojo(String sym, String ref) {
        this.Symptomsname = sym;
        this.RefTo = ref;
    }

    public String getChildId() {
        return ChildId;
    }

    public void setChildId(String childId) {
        this.ChildId = childId;
    }

    public String getSymptomsname() {
        return Symptomsname;
    }

    public void setSymptomsname(String symptomsname) {
        this.Symptomsname = symptomsname;
    }

    public String getRefTo() {
        return RefTo;
    }

    public void setRefTo(String refTo) {
        this.RefTo = refTo;
    }

    public String getSympid() {
        return sympid;
    }

    public void setSympid(String sympid) {
        this.sympid = sympid;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getWeightdate() {
        return weightdate;
    }

    public void setWeightdate(String weightdate) {
        this.weightdate = weightdate;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getTempdate() {
        return tempdate;
    }

    public void setTempdate(String tempdate) {
        this.tempdate = tempdate;
    }

    public String getResprate() {
        return resprate;
    }

    public void setResprate(String resprate) {
        this.resprate = resprate;
    }

    public String getRespratedate() {
        return respratedate;
    }

    public void setRespratedate(String respratedate) {
        this.respratedate = respratedate;
    }

    public boolean isChildregatgovtfac() {
        return childregatgovtfac;
    }

    public void setChildregatgovtfac(boolean childregatgovtfac) {
        this.childregatgovtfac = childregatgovtfac;
    }

    public String getChildregid() {
        return childregid;
    }

    public void setChildregid(String childregid) {
        this.childregid = childregid;
    }

    public String getChildregdate() {
        return childregdate;
    }

    public void setChildregdate(String childregdate) {
        this.childregdate = childregdate;
    }

    public int getChildNo() {
        return ChildNo;
    }

    public void setChildNo(int childNo) {
        ChildNo = childNo;
    }
}
