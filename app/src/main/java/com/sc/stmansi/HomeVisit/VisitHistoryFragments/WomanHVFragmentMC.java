package com.sc.stmansi.HomeVisit.VisitHistoryFragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.HomeVisit.HomeVisitActivityByMC;
import com.sc.stmansi.HomeVisit.HomeVisitListAdapterMC;
import com.sc.stmansi.R;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.configuration.SyncState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.recyclerview.ClickListener;
import com.sc.stmansi.repositories.CaseMgmtRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.tables.TblCaseMgmt_ANCPNCHv;
import com.sc.stmansi.tables.tblCaseMgmt;
import com.sc.stmansi.tables.tblregisteredwomen;

import java.sql.SQLException;
import java.util.ArrayList;

public class WomanHVFragmentMC extends Fragment implements ClickListener {
    String womanId;
    //    List<tblCaseMgmt> tblCaseMgmtList;
    private View parentview;
    private AppState appState;
    private DatabaseHelper databaseHelper;
    private RecyclerView recyclerView;
    private tblregisteredwomen woman;
    private AQuery aq;
    private RecyclerView.Adapter homeVisitViewAdapter;
    private int itemPosition;
    private tblCaseMgmt visitDetails;
    //11Jul2021 Bindu
    ArrayList<TblCaseMgmt_ANCPNCHv> tblCaseMgmtList;
    private SyncState syncState;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        parentview = inflater.inflate(R.layout.fragment_womanhv_mcfragment, container, false);
        Bundle bundle = this.getArguments();
        appState = bundle.getParcelable("appState");
        syncState = bundle.getParcelable("syncState");
//        womanId = this.getArguments().getString("womanId");
        womanId = appState.selectedWomanId;

        recyclerView = parentview.findViewById(R.id.rvHomevisitMC);

        return parentview;
    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(parentview.getContext(), DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initialValues();
        setData();

    }

    private void setData() {
        try {
            CaseMgmtRepository caseMgmtRepository = new CaseMgmtRepository(databaseHelper);
//            tblCaseMgmtList = caseMgmtRepository.getAllCasesWomanHV(womanId, appState.sessionUserId);
            tblCaseMgmtList = caseMgmtRepository.getCaseMgmtWomanANCPNCHV_MC(womanId, getResources().getString(R.string.mcusertype));
            if (tblCaseMgmtList.size() != 0) {
//                homeVisitViewAdapter=new HomevisitListAdapterMC(getActivity(),this,tblCaseMgmtList);
                homeVisitViewAdapter=new HomeVisitListAdapterMC(getActivity(),this,tblCaseMgmtList);
                recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                recyclerView.setAdapter(homeVisitViewAdapter);
            } else {
                aq.id(R.id.txtNoRecordsMC).visible();
            }
        } catch (Exception e) {
            e.printStackTrace();
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private void initialValues() {
        databaseHelper = getHelper();
        aq = new AQuery(parentview);
        try {
            WomanRepository womanRepo = new WomanRepository(databaseHelper);
            woman = womanRepo.getRegistartionDetails(womanId, appState.sessionUserId);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }


    @Override
    public void onClick(View v) {
        setSelectedVisitDetails(v);
        Intent intent = new Intent(getContext(), HomeVisitActivityByMC.class);
        intent.putExtra("globalState", this.getArguments());
//        intent.putExtra("pos", itemPosition);
        intent.putExtra("pos",tblCaseMgmtList.get(itemPosition).getVisitNum()-1);//17Sep2021 Arpitha
//        intent.putExtra("womanId", womanId);
        startActivity(intent);
    }

    //    set selected Visit details id
    private void setSelectedVisitDetails(View v) {
        itemPosition = recyclerView.getChildLayoutPosition(v);
//        visitDetails = tblCaseMgmtList.get(itemPosition); //11Jul2021 Bindu comment
    }

    @Override
    public boolean onLongClick(View v) {
        return false;
    }
}