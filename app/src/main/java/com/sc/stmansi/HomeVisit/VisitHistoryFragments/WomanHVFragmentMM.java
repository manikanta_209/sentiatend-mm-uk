package com.sc.stmansi.HomeVisit.VisitHistoryFragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.legacy.app.ActionBarDrawerToggle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidquery.AQuery;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.HomeVisit.HomeVisitViewActivity;
import com.sc.stmansi.R;
import com.sc.stmansi.common.NavDrawerItem;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.configuration.MyApplication;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.recyclerview.ClickListener;
import com.sc.stmansi.recyclerview.HomeVisitListAdapter;
import com.sc.stmansi.repositories.HomeVisitRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.tables.TblChildInfo;
import com.sc.stmansi.tables.TblVisitHeader;
import com.sc.stmansi.tables.tblregisteredwomen;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class WomanHVFragmentMM extends Fragment implements ClickListener {

    String womanId;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter homeVisitViewAdapter;
    private View parentview;
    private List<TblVisitHeader> visitItems;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private ArrayList<NavDrawerItem> navDrawerItems;
    private ListView mDrawerList;
    private tblregisteredwomen woman;
    private AQuery aq;
    private TblVisitHeader visitDetails;
    private int itemPosition;
    private AppState appState;
    private DatabaseHelper databaseHelper;
    TblChildInfo tblChildInfo;
    Boolean str;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        parentview = inflater.inflate(R.layout.fragment_womanhv_mmfragment, container, false);

        Bundle bundle = this.getArguments();
        appState = bundle.getParcelable("appState");

        //mani 30Aug2021
         str = this.getArguments().getBoolean("fromchild");
        if (str)
        {
            tblChildInfo = (TblChildInfo) bundle.getSerializable("tblChildInfo");
            womanId=tblChildInfo.getWomanId();
        }else {
            womanId = this.getArguments().getString("womanId");
        }

        recyclerView = parentview.findViewById(R.id.rvHomevisitMM);


        return parentview;
    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(parentview.getContext(), DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initialValues();
        setData();
    }

    private void setData() {
        try {
            HomeVisitRepository homeRepo = new HomeVisitRepository(databaseHelper);
            visitItems = homeRepo.getVisitDetailsForwoman(woman.getWomanId(), appState.sessionUserId);
            if (visitItems.size()!=0){
                homeVisitViewAdapter = new HomeVisitListAdapter(getActivity(), visitItems,this, appState);
                recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                recyclerView.setAdapter(homeVisitViewAdapter);
            }else {
                aq.id(R.id.txtNoRecordsMM).visible();
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private void initialValues() {
        databaseHelper = getHelper();
        aq = new AQuery(parentview);
        try {
            WomanRepository womanRepo = new WomanRepository(databaseHelper);
            woman = womanRepo.getRegistartionDetails(womanId, appState.sessionUserId);

            //26Jul2021 Bindu check value and set if null or empty
            if(MyApplication.mapSymptoms == null || MyApplication.mapSymptoms.isEmpty()) {
                HomeVisitRepository homeRepo = new HomeVisitRepository(databaseHelper);

                MyApplication.mapSymptoms = homeRepo.getSymptomsNames();
                MyApplication.mapInterpretations = homeRepo.getInterpretationNames();
                MyApplication.mapDiagnosis = homeRepo.getDiagnosisNames();

            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        //mani 30Aug2021 Checking condition for new born
        setSelectedVisitDetails(v);
        if (str){
            Intent intent = new Intent(getContext(), HomeVisitViewActivity.class);
            intent.putExtra("globalState", this.getArguments());
            intent.putExtra("pos", itemPosition);
            intent.putExtra("fromchild",str);
            intent.putExtra("tblChildInfo",tblChildInfo);
            startActivity(intent);
        }else {
            Intent intent = new Intent(getContext(), HomeVisitViewActivity.class);
            intent.putExtra("globalState", this.getArguments());
            intent.putExtra("pos", itemPosition);
            startActivity(intent);
        }

    }

    //    set selected Visit details id
    private void setSelectedVisitDetails(View v) {
        itemPosition = recyclerView.getChildLayoutPosition(v);
        visitDetails = visitItems.get(itemPosition);
    }

    @Override
    public boolean onLongClick(View v) {
        return false;
    }


}