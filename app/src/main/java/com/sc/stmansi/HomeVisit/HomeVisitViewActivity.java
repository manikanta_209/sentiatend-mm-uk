package com.sc.stmansi.HomeVisit;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.legacy.app.ActionBarDrawerToggle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.About.AboutActivity;
import com.sc.stmansi.ImageStore.RecyclerViewAdapter;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.common.NavDrawerItem;
import com.sc.stmansi.common.NavDrawerListAdapter;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.deactivate.WomanDeactivateActivity;
import com.sc.stmansi.delivery.DeliveryInfoActivity;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.repositories.CovidDetailsRepository;
import com.sc.stmansi.repositories.HomeVisitRepository;
import com.sc.stmansi.repositories.ImageStoreRepository;
import com.sc.stmansi.repositories.LoginRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.services.ServicesSummaryActivity;
import com.sc.stmansi.services.UnplannedServicesListActivity;
import com.sc.stmansi.tables.TblChildInfo;
import com.sc.stmansi.tables.TblVisitChildHeader;
import com.sc.stmansi.tables.TblVisitHeader;
import com.sc.stmansi.tables.tblCovidTestDetails;
import com.sc.stmansi.tables.tblCovidVaccineDetails;
import com.sc.stmansi.tables.tblImageStore;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.viewprofile.ViewProfileActivity;
import com.sc.stmansi.womanlist.RegisteredWomenActionTabs;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.sc.stmansi.R;

public class HomeVisitViewActivity extends AppCompatActivity {
    private List<TblVisitHeader> visitItems;
    private AQuery aq;
    private int index = 0;
    private TblVisitHeader prevObj,nextObj;
    private TblVisitHeader visitListData;
    private ArrayList<String> aList;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private ArrayList<NavDrawerItem> navDrawerItems;
    private ListView mDrawerList;
    private tblregisteredwomen woman;
    private int itempos = 0;
    private List<tblregisteredwomen> wVisitDetails;

    private AppState appState;
    private DatabaseHelper databaseHelper;
    private List<TblVisitChildHeader> visitChildItems;
    LinkedHashMap<String, VisitDetailsPojo> getchildHeaderMap = null;
//    17MAy2021 Bindu
    private List<tblCovidVaccineDetails> covVacItems;
    private boolean isvaccinated = false;
    private List<tblCovidTestDetails> covvactestdetails;

    public HomeVisitViewActivity() {
    }

    //25Jul2021 Bindu
    public static Map<String, String> mapSymptoms;
    public static  Map<String, String> mapInterpretations;
    public static Map<String, String> mapDiagnosis;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_homevisit_view);
            aq = new AQuery(this);

            databaseHelper = getHelper();
            Bundle bundle = getIntent().getBundleExtra("globalState");
            appState = bundle.getParcelable("appState");

            WomanRepository womanRepo = new WomanRepository(databaseHelper);

            HomeVisitRepository homeRepo = new HomeVisitRepository(databaseHelper);


                woman = womanRepo.getRegistartionDetails(appState.selectedWomanId, appState.sessionUserId);
                visitItems = homeRepo.getVisitDetailsForwoman(appState.selectedWomanId, appState.sessionUserId);
                //25Jul2021 Bindu
                mapSymptoms = homeRepo.getSymptomsNames();
                mapInterpretations = homeRepo.getInterpretationNames();
                mapDiagnosis = homeRepo.getDiagnosisNames();
                if(woman.getRegPregnantorMother() == 2)
                    visitChildItems = homeRepo.getVisitDetailsForChild(appState.selectedWomanId,appState.sessionUserId);



            itempos = getIntent().getIntExtra("pos",0);
            index = itempos;

            initiateDrawer();

            aq.id(R.id.btnnextvisit).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        index = index + 1;
                        //nextObj = visitItems.get(index - 1);
                        if (index < visitItems.size()) {
                            setData(visitItems, index);
                        } else {
                            index = index - 1;
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.norecords), Toast.LENGTH_LONG).show();
                            // aq.id(R.id.txtVisitNum).text("Visit Num" + visitItems.get(index).getVisitNum());
                        }

                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            });

            aq.id(R.id.btnpreviousvisit).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                    /*prevObj = visitItems.get(index + 1);
                    aq.id(R.id.txtVisitNum).text("Visit Num" + prevObj.getVisitNum());*/

                        index = index - 1;
                        if (index >= 0) {
                            setData(visitItems, index);
                        } else {
                            index = index + 1;
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.norecords), Toast.LENGTH_LONG).show();
                        }
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                }
            });

          //  setInitialValues();
            setData(visitItems,itempos);
            // Symptoms drwable on touch listener
            aq.id(R.id.txtDangSignsL).getTextView().setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if(event.getAction() == MotionEvent.ACTION_UP) {
                        if(event.getRawX() <= aq.id(R.id.txtDangSignsL).getTextView().getTotalPaddingLeft()) {
                            opencloseAccordionDangerSigns(aq.id(R.id.txtDangSigns).getView(), aq.id(R.id.txtDangSignsL).getTextView());
                            return true;
                        }
                    }
                    return true;
                }
            });

            // Advise drwable on touch listener
            aq.id(R.id.txtadvisegiven).getTextView().setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if(event.getAction() == MotionEvent.ACTION_UP) {
                        if(event.getRawX() <= aq.id(R.id.txtadvisegiven).getTextView().getTotalPaddingLeft()) {
                            opencloseAccordionDangerSigns(aq.id(R.id.txtadvisesumm).getView(), aq.id(R.id.txtadvisegiven).getTextView());
                            return true;
                        }
                    }
                    return true;
                }
            });

//            Ramesh 1-6-2021
            aq.id(R.id.txtPhotos).getTextView().setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if(event.getAction() == MotionEvent.ACTION_UP) {
                        if(event.getRawX() <= aq.id(R.id.txtPhotos).getTextView().getTotalPaddingLeft()) {
                            opencloseAccordionDangerSigns(aq.id(R.id.recyclerImage).getView(), aq.id(R.id.txtPhotos).getTextView());
                            return true;
                        }
                    }
                    return true;
                }
            });
            aq.id(R.id.txtPhotospnc).getTextView().setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if(event.getAction() == MotionEvent.ACTION_UP) {
                        if(event.getRawX() <= aq.id(R.id.txtPhotospnc).getTextView().getTotalPaddingLeft()) {
                            opencloseAccordionDangerSigns(aq.id(R.id.recyclerImagepnc).getView(), aq.id(R.id.txtPhotospnc).getTextView());
                            return true;
                        }
                    }
                    return true;
                }
            });

            //03Oct2019 - Bindu - Get Visit details from reg table

            wVisitDetails = womanRepo.getVisitDetailsFrmReg(appState.selectedWomanId, appState.sessionUserId);

        }catch(Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    // Set Initial Values
    private void setInitialValues() {
        aq.id(R.id.txtVisitNum).text(""+visitListData.getVisitNum());
        aq.id(R.id.txtVisitDateval).text(visitListData.getVisHVisitDate());

        if(visitListData.getVisHVisitType().equalsIgnoreCase(getResources().getString(R.string.stranc))) {
            aq.id(R.id.txtgestage).text(getResources().getString(R.string.statusatvisit));
            aq.id(R.id.txtgestageval).text(visitListData.getVisHStatusAtVisit());
        }

        aq.id(R.id.txtDangSigns).text(visitListData.getVisHResult());
        aq.id(R.id.txtbpval).text(visitListData.getVisHBP());
        aq.id(R.id.txtpulseval).text(""+visitListData.getVisHPulse());
        aq.id(R.id.txttempval).text(""+visitListData.getVisHTemperature());
        aq.id(R.id.txthbval).text(""+visitListData.getVisHHb());
        aq.id(R.id.txtproteinuriaval).text(""+visitListData.getVisHUrineprotein());

        if(visitListData.getVisHVisitIsAdviseGiven().equalsIgnoreCase(getResources().getString(R.string.yesshortform)))
            aq.id(R.id.chkisAdviceGiven).checked(true);
        if(visitListData.getVisHVisitParacetamolGiven().equalsIgnoreCase(getResources().getString(R.string.yesshortform)))
            aq.id(R.id.chkparacetamol).checked(true);
        if(visitListData.getVisHVisitAlbendazoleGiven().equalsIgnoreCase(getResources().getString(R.string.yesshortform)))
            aq.id(R.id.chkalbedazolegiven).checked(true);
        if(visitListData.getVisHVisitIFAGiven().equalsIgnoreCase(getResources().getString(R.string.yesshortform)))
            aq.id(R.id.chkifa).checked(true);

        if(visitListData.getVisHVisitIsReferred().equalsIgnoreCase(getResources().getString(R.string.yesshortform))) {
            aq.id(R.id.chkisreferred).checked(true);
            aq.id(R.id.txtreffacilitynameval).text(""+visitListData.getVisHVisitReferredToFacilityName());
        }

        aq.id(R.id.etvisitcomments).text(""+visitListData.getVisHVisitComments());

        aq.id(R.id.txtadvisesumm).text(visitListData.getVisHAdvise());
    }

    // Set Data for view based on item
    private void setData(List<TblVisitHeader> visitItems, int index) throws SQLException, Exception {
       // aq.id(R.id.txtheading).text(getResources().getString(R.string.homevisit) + " View "+ " - " + " # "+ visitItems.get(index).getVisitNum());



        if(visitItems.get(index).getVisHVisitType().equalsIgnoreCase(getResources().getString(R.string.stranc))) {

            //            17MAy2021 Bindu get covidtestdetails
            getcovidtestdetails(visitItems.get(index).getVisitNum(), appState.selectedWomanId);

            aq.id(R.id.scrhomevisitsummarypnc).gone();
            aq.id(R.id.scrhomevisitsummary).visible();

            aq.id(R.id.txtVisitNum).text("" + visitItems.get(index).getVisitNum());
            aq.id(R.id.txtVisitDateval).text(visitItems.get(index).getVisHVisitDate());

            if (visitItems.get(index).getVisHVisitType().equalsIgnoreCase(getResources().getString(R.string.stranc))) {
                aq.id(R.id.txtgestage).text(getResources().getString(R.string.statusatvisit));
                aq.id(R.id.txtgestageval).text(visitItems.get(index).getVisHStatusAtVisit());
            }
//            aq.id(R.id.txtDangSigns).text(visitItems.get(index).getVisHResult());10May2021 Arpitha

//            10May2021 Arpitha
           /* HomeVisitRepository homeVisitRepository = new HomeVisitRepository(databaseHelper);
            ArrayList<String> list = homeVisitRepository.getHomeVisitSymptoms(woman.getUserId(),woman.getWomanId(),visitItems.get(index).getVisitNum());
           String symptom = "";
            if(list!=null && list.size()>0) {

                for(int i=0;i<list.size();i++) {
//                    String strSy = list.get(i);
//                    int identifier = getResources().getIdentifier
//                            (strSy,
//                                    "string", "com.sc.stmansi");
//                    if(i == list.size()-1)
//                        symptom = symptom + getResources().getString(identifier);
//                    else
//                    symptom = symptom + getResources().getString(identifier)+",";
                    String strSy = list.get(i);
                    strSy = strSy.replaceAll(" ", "");
                    strSy = strSy.toLowerCase().trim();

                    //check directly and replace - any changes to be updated in HomeVisitActivityByCC also
                    if(strSy.contains("notrelieved"))
                        strSy = "constipationnotrelieved"; //15Jul2021 Bindu
                    if(strSy.contains("bleedingp/v"))
                        strSy = "bleedingpv"; //15Jul2021 Bindu
                    if(strSy.contains("leakingofwatery"))
                        strSy = "leakingofwateryfluidspv"; //15Jul2021 Bindu
                    if(strSy.contains("twin/multiplepregnancy"))
                        strSy = "neardeltwinormultiplepregnancy";
                    if(strSy.contains("laborpainsmorethan12hours"))
                        strSy = "laborpainmorethan12hours";
                    if(strSy.contains("motherswithcpd/contractedpelvis"))
                        strSy = "neardelmotherscpdorcontpelvis";
                    if(strSy.contains("foetalmovementdecreased"))
                        strSy = "decreasedfetalmovements";
                    if(strSy.contains("foetalmovementabsent"))
                        strSy = "fetalmovementabsent";
                    if(strSy.contains("iugr"))
                        strSy = "neardeliugr";

//                    for pnc
                    if(strSy.contains("inabilitytocontroldefecation"))
                        strSy = "inabilitytocontroldefecationorurine";
                    if(strSy.contains("bleedingnipples"))
                        strSy = "breastsproblems";
                    if(strSy.contains("swollenhands"))
                        strSy = "swollenhands";

                    int identifier = getResources().getIdentifier
                            (strSy,
                                    "string", getPackageName()); //25Jun2021 Bindu get package name from app
                    if (identifier > 0) {
                        if (i == list.size() - 1)
                            symptom = symptom + getResources().getString(identifier);
                        else
                            symptom = symptom + getResources().getString(identifier) + ",";
                    } else {
                        if (i == list.size() - 1)
                            symptom = symptom + strSy;
                        else
                            symptom = symptom + strSy + ",";
                    }

                }
            }*/ //25Jul2021
//            aq.id(R.id.txtDangSigns).text(symptom);// 10May2021 Arpitha

           // aq.id(R.id.txtDangSigns).text(setWomaCompl(index));

            aq.id(R.id.txtDangSigns).text(setWomanCompl(index));

            aq.id(R.id.txtbpval).text(visitItems.get(index).getVisHBP());
            aq.id(R.id.txtpulseval).text("" + visitItems.get(index).getVisHPulse());
            aq.id(R.id.txttempval).text("" + visitItems.get(index).getVisHTemperature());
            aq.id(R.id.txthbval).text("" + visitItems.get(index).getVisHHb());
            aq.id(R.id.txtproteinuriaval).text("" + visitItems.get(index).getVisHUrineprotein());

            aq.id(R.id.txtbpdonedate).text(visitItems.get(index).getVisHBPDate());
            aq.id(R.id.txtpulsedate).text("" + visitItems.get(index).getVisHPulseDate());
            aq.id(R.id.txttempdate).text("" + visitItems.get(index).getVisHTemperatureDate());
            aq.id(R.id.txthbdate).text("" + visitItems.get(index).getVisHHbDate());
            aq.id(R.id.txtproteinuriadate).text("" + visitItems.get(index).getVisHUrineproteinDate());

            if (visitItems.get(index).getVisHVisitIsAdviseGiven().equalsIgnoreCase(getResources().getString(R.string.yesshortform))) {
                // aq.id(R.id.chkisAdviceGiven).checked(true);
                aq.id(R.id.txtadvisegivenval).text(getResources().getString(R.string.yes));
            } else {
                // aq.id(R.id.chkisAdviceGiven).checked(false);
            }

            //mani AUG-18-2021
            if (visitItems.get(index).getVisHAshaAvailable().equalsIgnoreCase("Y"))
                    aq.id(R.id.ashavailability).text(getResources().getString(R.string.yes));
            else
                if (visitItems.get(index).getVisHAshaAvailable().equalsIgnoreCase("N"))
                    aq.id(R.id.ashavailability).text(getResources().getString(R.string.no));
                else
                if (visitItems.get(index).getVisHAshaAvailable().equalsIgnoreCase("Dont Know"))
                    aq.id(R.id.ashavailability).text(getResources().getString(R.string.dontknow));

            if (visitItems.get(index).getVisHVisitParacetamolGiven().equalsIgnoreCase(getResources().getString(R.string.yesshortform))
                    || visitItems.get(index).getVisHVisitAlbendazoleGiven().equalsIgnoreCase(getResources().getString(R.string.yesshortform))
                    || visitItems.get(index).getVisHVisitIFAGiven().equalsIgnoreCase(getResources().getString(R.string.yesshortform))) {
                aq.id(R.id.tbltabletdetails).visible();
            } else {
                aq.id(R.id.tbltabletdetails).gone();
            }


            if (visitItems.get(index).getVisHVisitParacetamolGiven().equalsIgnoreCase(getResources().getString(R.string.yesshortform))) {
                aq.id(R.id.chkparacetamol).checked(true);
                aq.id(R.id.txtparacetamolgivenval).text("" + visitItems.get(index).getVisHVisitParacetamolQty());
                aq.id(R.id.txtparacetamolnotgivenval).text("" + visitItems.get(index).getVisHVisitParacetamolNotGivenReason() == null ? "" : visitItems.get(index).getVisHVisitParacetamolNotGivenReason());
                aq.id(R.id.trparacetamol).visible();
            } else {
                aq.id(R.id.chkparacetamol).checked(false);
                aq.id(R.id.trparacetamol).gone();
            }

            if (visitItems.get(index).getVisHVisitAlbendazoleGiven().equalsIgnoreCase(getResources().getString(R.string.yesshortform))) {
                aq.id(R.id.chkalbedazolegiven).checked(true);
                aq.id(R.id.txtalbendazolegivenval).text("" + visitItems.get(index).getVisHVisitAlbendazoleQty());
                aq.id(R.id.txtalbendazolenotgivenval).text("" + visitItems.get(index).getVisHVisitAlbendazoleNotGivenReason() == null ? "" : visitItems.get(index).getVisHVisitAlbendazoleNotGivenReason());
                aq.id(R.id.tralbendazole).visible();
            } else {
                aq.id(R.id.chkalbedazolegiven).checked(false);
                aq.id(R.id.tralbendazole).gone();
            }

            if (visitItems.get(index).getVisHVisitIFAGiven().equalsIgnoreCase(getResources().getString(R.string.yesshortform))) {
                aq.id(R.id.chkifa).checked(true);
                aq.id(R.id.txtifagivenval).text("" + visitItems.get(index).getVisHVisitIFAQty());
                aq.id(R.id.txtifanotgivenval).text("" + visitItems.get(index).getVisHVisitIFANotGivenReason() == null ? "" : visitItems.get(index).getVisHVisitIFANotGivenReason());
                aq.id(R.id.trifa).visible();
            } else {
                aq.id(R.id.chkifa).checked(false);
                aq.id(R.id.trifa).gone();
            }

        /*if(visitItems.get(index).getVisHVisitFSFAGiven().equalsIgnoreCase(getResources().getString(R.string.yesshortform))) {
            aq.id(R.id.chkfsfa).checked(true);
            aq.id(R.id.txtfsfagivenval).text(""+visitItems.get(index).getVisHVisitFSFAQty());
            aq.id(R.id.txtfsfanotgivenval).text(""+ visitItems.get(index).getVisHVisitFSFANotGivenReason() == null ? "" : visitItems.get(index).getVisHVisitFSFANotGivenReason());
            aq.id(R.id.trfsfa).visible();
        }
        else {
            aq.id(R.id.chkfsfa).checked(false);
            aq.id(R.id.trfsfa).gone();
        }*/ //16Nov2019 - Bindu - Remve fsfa chk

            if (visitItems.get(index).getVisHVisitIsReferred().equalsIgnoreCase(getResources().getString(R.string.yesshortform))) {
                //aq.id(R.id.chkisreferred).checked(true);
                aq.id(R.id.tblrefdetails).visible();
                aq.id(R.id.txtreffacilitynameval).text("" + visitItems.get(index).getVisHVisitReferredToFacilityName());
                aq.id(R.id.txtreffacilitytypeval).text("" + visitItems.get(index).getVisHVisitReferredToFacilityType());
                aq.id(R.id.txtrefslipnumberval).text("" + visitItems.get(index).getVisHReferralSlipNumber()); //08Apr2021 Bindu
            } else
                //aq.id(R.id.chkisreferred).checked(false);
                aq.id(R.id.tblrefdetails).gone();

            aq.id(R.id.etvisitcomments).text("" + visitItems.get(index).getVisHVisitComments());

          //  aq.id(R.id.txtadvisesumm).text(visitItems.get(index).getVisHAdvise());//10May2021 Arpitha

            String advice = "";
            if(visitItems.get(index).getVisHAdvise() != null && visitItems.get(index).getVisHAdvise().length() > 0)
                advice = setAdvise(visitItems.get(index).getVisHAdvise());

            aq.id(R.id.txtadvisesumm).text(advice);


            //02Oct2019 - Bindu - Weight
            aq.id(R.id.txtweightval).text("" + visitItems.get(index).getVisHWeight());
            aq.id(R.id.txtweightdate).text(visitItems.get(index).getVisHWeightDate());


            expandcollapseSymptoms();
            expandcollapseAdvise();
        }
        else if(visitItems.get(index).getVisHVisitType().equalsIgnoreCase(getResources().getString(R.string.strpnc))) {


            //            17MAy2021 Bindu get covidtestdetails


            getcovidtestdetailsPNC(visitItems.get(index).getVisitNum(), appState.selectedWomanId);
            aq.id(R.id.scrhomevisitsummarypnc).visible();
            aq.id(R.id.scrhomevisitsummary).gone();

            aq.id(R.id.txtVisitNum).text("" + visitItems.get(index).getVisitNum());
            aq.id(R.id.txtVisitDateval).text(visitItems.get(index).getVisHVisitDate());

            aq.id(R.id.txtgestage).text(getResources().getString(R.string.txtDaysSinceDeliveryName));
            aq.id(R.id.txtgestageval).text(visitItems.get(index).getVisHStatusAtVisit());

           /* aq.id(R.id.txtDangSignspnc).text(visitItems.get(index).getVisHResult());
            aq.id(R.id.txtDangSignsNb).text(visitItems.get(index).getVisHChildSymptoms());15May2021 Arpitha*/
            setPncAndNbSigns();//15May2021 Arpitha
            aq.id(R.id.txtbpvalpnc).text(visitItems.get(index).getVisHBP());
            aq.id(R.id.txtpulsevalpnc).text("" + visitItems.get(index).getVisHPulse());
            aq.id(R.id.txttempvalpnc).text("" + visitItems.get(index).getVisHTemperature());
            aq.id(R.id.txthbvalpnc).text("" + visitItems.get(index).getVisHHb());

            aq.id(R.id.txtbpdonedatepnc).text(visitItems.get(index).getVisHBPDate());
            aq.id(R.id.txtpulsedatepnc).text("" + visitItems.get(index).getVisHPulseDate());
            aq.id(R.id.txttempdatepnc).text("" + visitItems.get(index).getVisHTemperatureDate());
            aq.id(R.id.txthbdatepnc).text("" + visitItems.get(index).getVisHHbDate());

            //mani AUG-18-2021
            if (visitItems.get(index).getVisHAshaAvailable().equalsIgnoreCase("Y"))
                aq.id(R.id.ashavailabilitypnc).text(getResources().getString(R.string.yes));
            else
            if (visitItems.get(index).getVisHAshaAvailable().equalsIgnoreCase("N"))
                aq.id(R.id.ashavailabilitypnc).text(getResources().getString(R.string.no));
            else
            if (visitItems.get(index).getVisHAshaAvailable().equalsIgnoreCase("Dont Know"))
                aq.id(R.id.ashavailabilitypnc).text(getResources().getString(R.string.dontknow));

            aq.id(R.id.txtactionsumm).text("" + visitItems.get(index).getVisHAdvise());

            if (visitItems.get(index).getVisHVisitIsReferred().equalsIgnoreCase(getResources().getString(R.string.yesshortform))) {
                aq.id(R.id.tblrefdetailspnc).visible();
                aq.id(R.id.txtreffacilitynamevalpnc).text("" + visitItems.get(index).getVisHVisitReferredToFacilityName());
                aq.id(R.id.txtreffacilitytypevalpnc).text("" + visitItems.get(index).getVisHVisitReferredToFacilityType());
                aq.id(R.id.txtrefslipnumbervalpnc).text("" + visitItems.get(index).getVisHReferralSlipNumber()); //13Apr2021 Bindu
            } else
                aq.id(R.id.tblrefdetailspnc).gone();

            aq.id(R.id.etvisitcommentspnc).text("" + visitItems.get(index).getVisHVisitComments());

            if(visitChildItems != null && visitChildItems.size() > 0) {
                for(int i=0; i< visitChildItems.size();i++) {
                    if(visitChildItems.get(i).getVisitNum() == visitItems.get(index).getVisitNum()) {
                        if(visitChildItems.get(i).getChildNo() == 1) {
                            aq.id(R.id.txtweightvalpncbaby1).text(""+visitChildItems.get(i).getVisCHWeight());
                            aq.id(R.id.txttweightdatevalpncbaby1).text(""+visitChildItems.get(i).getVisCHWeightDate());
                            aq.id(R.id.txttempvalpncbaby1).text(""+visitChildItems.get(i).getVisCHTemperature());
                            aq.id(R.id.txttempdatepncbaby1).text(""+visitChildItems.get(i).getVisCHTemperatureDate());
                            aq.id(R.id.txtrespratevalpncbaby1).text(""+visitChildItems.get(i).getVisCHResprate());
                            aq.id(R.id.txtrespratedatepncbaby1).text(""+visitChildItems.get(i).getVisCHResprateDate());
                            aq.id(R.id.tblexaminationschild1).visible();
                            if(visitChildItems.get(i).isVisCHRegAtGovtFac() == true) {
                                aq.id(R.id.txtchildregidvalbaby1).text("" + visitChildItems.get(i).getVisCHRegGovtFacId());
                                aq.id(R.id.txtchildregdatevalbaby1).text("" + visitChildItems.get(i).getVisCHRegAtGovtFacDate());
                                aq.id(R.id.tblregatgovtfacchild1).visible();
                            }else{
                                aq.id(R.id.txtchildregidvalbaby1).text("");
                                aq.id(R.id.txtchildregdatevalbaby1).text("");
                                aq.id(R.id.tblregatgovtfacchild1).gone();
                            }

                            // Mani 13Sep2021:tablet details
                            aq.id(R.id.tabdetbaby1).visible();
                            aq.id(R.id.tabletdetailsbaby1).visible();

                            aq.id(R.id.txt_AVV_UsingIFA1).getTextView().setText(visitChildItems.get(i).getChildvisHIsIFATaken().equals("Yes") ? getResources().getString(R.string.m121) : getResources().getString(R.string.m122));
                            if (visitChildItems.get(i).getChildvisHIsIFATaken().equals("Yes")) {
                                aq.id(R.id.ll_AVV_IFA1).visible();
                                aq.id(R.id.txt_AVV_ConsumeIFA1).getTextView().setText(visitChildItems.get(i).getChildvisHIFADaily().equals("Yes") ? getResources().getString(R.string.m121) : getResources().getString(R.string.m122));
                                if (visitChildItems.get(i).getChildvisHIFADaily().equals("Yes")) {
                                    TableRow trTabletsConsumePerDay = findViewById(R.id.trTabletsConsumePerDay1);
                                    trTabletsConsumePerDay.setVisibility(View.VISIBLE);
                                    aq.id(R.id.txt_AVV_ConsumePerDay1).getTextView().setText(visitChildItems.get(i).getChildvisHIFATabletsCount());
                                } else {
                                    TableRow trIFAReason = findViewById(R.id.trIFAReason1);
                                    trIFAReason.setVisibility(View.VISIBLE);
                                    String[] ifaReasons = getResources().getStringArray(R.array.ifareasons);
                                    String reasons = visitChildItems.get(i).getChildvisHIFANotTakenReason();
                                    String[] reasonAfterSplit = reasons.split(",");
                                    StringBuilder origonalReasons = new StringBuilder();
                                    if (reasons.length() > 0) {
                                        for (String s : reasonAfterSplit) {
                                            origonalReasons.append(ifaReasons[Integer.parseInt(s.trim())]).append(",");
                                        }
                                    }
                                    origonalReasons = origonalReasons.deleteCharAt(origonalReasons.lastIndexOf(","));
                                    aq.id(R.id.txt_AVV_IFAReason1).getTextView().setText(origonalReasons.toString());
                                }
                                int whogivenIndex = visitChildItems.get(i).getChildvisHIFAGivenBy();
                                String[] whogave = getResources().getStringArray(R.array.whogaveifatablets);
                                aq.id(R.id.txt_AVV_WhoGaveIFA1).getTextView().setText(whogave[whogivenIndex]);
                            } else {
                                aq.id(R.id.ll_AVV_IFA1).gone();
                            }

                            switch (visitChildItems.get(i).getChildvisHDewormingtab()) {
                                case "Yes":
                                    aq.id(R.id.txt_AVV_Deworming1).getTextView().setText(getResources().getString(R.string.m121));
                                    break;
                                case "No":
                                    aq.id(R.id.txt_AVV_Deworming1).getTextView().setText(getResources().getString(R.string.m122));
                                    break;
                                default:
                                    aq.id(R.id.txt_AVV_Deworming1).getTextView().setText(getResources().getString(R.string.dontknow));
                            }




                        }else if(visitChildItems.get(i).getChildNo() == 2) {
                            aq.id(R.id.txtweightvalpncbaby2).text(""+visitChildItems.get(i).getVisCHWeight());
                            aq.id(R.id.txttweightdatevalpncbaby2).text(""+visitChildItems.get(i).getVisCHWeightDate());
                            aq.id(R.id.txttempvalpncbaby2).text(""+visitChildItems.get(i).getVisCHTemperature());
                            aq.id(R.id.txttempdatepncbaby2).text(""+visitChildItems.get(i).getVisCHTemperatureDate());
                            aq.id(R.id.txtrespratevalpncbaby2).text(""+visitChildItems.get(i).getVisCHResprate());
                            aq.id(R.id.txtrespratedatepncbaby2).text(""+visitChildItems.get(i).getVisCHResprateDate());
                            aq.id(R.id.tblexaminationschild2).visible();
                            if(visitChildItems.get(i).isVisCHRegAtGovtFac() == true) {
                                aq.id(R.id.txtchildregidvalbaby2).text("" + visitChildItems.get(i).getVisCHRegGovtFacId());
                                aq.id(R.id.txtchildregdatevalbaby2).text("" + visitChildItems.get(i).getVisCHRegAtGovtFacDate());
                                aq.id(R.id.tblregatgovtfacchild2).visible();
                            }else{
                                aq.id(R.id.txtchildregidvalbaby2).text("");
                                aq.id(R.id.txtchildregdatevalbaby2).text("");
                                aq.id(R.id.tblregatgovtfacchild2).gone();
                            }


                            // Mani 13Sep21:tablet details
                            aq.id(R.id.tabletdetailsbaby2).visible();
                            aq.id(R.id.tabdetbaby2).visible();

                            aq.id(R.id.txt_AVV_UsingIFA2).getTextView().setText(visitChildItems.get(i).getChildvisHIsIFATaken().equals("Yes") ? getResources().getString(R.string.m121) : getResources().getString(R.string.m122));

                            if (visitChildItems.get(i).getChildvisHIsIFATaken().equals("Yes")) {
                                aq.id(R.id.ll_AVV_IFA2).visible();
                                aq.id(R.id.txt_AVV_ConsumeIFA2).getTextView().setText(visitChildItems.get(i).getChildvisHIFADaily().equals("Yes") ? getResources().getString(R.string.m121) : getResources().getString(R.string.m122));
                                if (visitChildItems.get(i).getChildvisHIFADaily().equals("Yes")) {
                                    TableRow trTabletsConsumePerDay = findViewById(R.id.trTabletsConsumePerDay2);
                                    trTabletsConsumePerDay.setVisibility(View.VISIBLE);
                                    aq.id(R.id.txt_AVV_ConsumePerDay2).getTextView().setText(visitChildItems.get(i).getChildvisHIFATabletsCount());
                                } else {
                                    TableRow trIFAReason = findViewById(R.id.trIFAReason2);
                                    trIFAReason.setVisibility(View.VISIBLE);
                                    String[] ifaReasons = getResources().getStringArray(R.array.ifareasons);
                                    String reasons = visitChildItems.get(i).getChildvisHIFANotTakenReason();
                                    String[] reasonAfterSplit = reasons.split(",");
                                    StringBuilder origonalReasons = new StringBuilder();
                                    if (reasons.length() > 0) {
                                        for (String s : reasonAfterSplit) {
                                            origonalReasons.append(ifaReasons[Integer.parseInt(s.trim())]).append(",");
                                        }
                                    }
                                    origonalReasons = origonalReasons.deleteCharAt(origonalReasons.lastIndexOf(","));
                                    aq.id(R.id.txt_AVV_IFAReason2).getTextView().setText(origonalReasons.toString());
                                }
                                int whogivenIndex = visitChildItems.get(i).getChildvisHIFAGivenBy();
                                String[] whogave = getResources().getStringArray(R.array.whogaveifatablets);
                                aq.id(R.id.txt_AVV_WhoGaveIFA2).getTextView().setText(whogave[whogivenIndex]);
                            } else {
                                aq.id(R.id.ll_AVV_IFA2).gone();
                            }

                            switch (visitChildItems.get(i).getChildvisHDewormingtab()) {
                                case "Yes":
                                    aq.id(R.id.txt_AVV_Deworming2).getTextView().setText(getResources().getString(R.string.m121));
                                    break;
                                case "No":
                                    aq.id(R.id.txt_AVV_Deworming2).getTextView().setText(getResources().getString(R.string.m122));
                                    break;
                                default:
                                    aq.id(R.id.txt_AVV_Deworming2).getTextView().setText(getResources().getString(R.string.dontknow));
                            }
                        }else if(visitChildItems.get(i).getChildNo() == 3) {
                            aq.id(R.id.txtweightvalpncbaby3).text(""+visitChildItems.get(i).getVisCHWeight());
                            aq.id(R.id.txttweightdatevalpncbaby3).text(""+visitChildItems.get(i).getVisCHWeightDate());
                            aq.id(R.id.txttempvalpncbaby3).text(""+visitChildItems.get(i).getVisCHTemperature());
                            aq.id(R.id.txttempdatepncbaby3).text(""+visitChildItems.get(i).getVisCHTemperatureDate());
                            aq.id(R.id.txtrespratevalpncbaby3).text(""+visitChildItems.get(i).getVisCHResprate());
                            aq.id(R.id.txtrespratedatepncbaby3).text(""+visitChildItems.get(i).getVisCHResprateDate());
                            aq.id(R.id.tblexaminationschild3).visible();
                            if(visitChildItems.get(i).isVisCHRegAtGovtFac() == true) {
                                aq.id(R.id.txtchildregidvalbaby3).text("" + visitChildItems.get(i).getVisCHRegGovtFacId());
                                aq.id(R.id.txtchildregdatevalbaby3).text("" + visitChildItems.get(i).getVisCHRegAtGovtFacDate());
                                aq.id(R.id.tblregatgovtfacchild3).visible();
                            }else{
                                aq.id(R.id.txtchildregidvalbaby3).text("");
                                aq.id(R.id.txtchildregdatevalbaby3).text("");
                                aq.id(R.id.tblregatgovtfacchild3).gone();
                            }

                            //Mani 13Sep21:tablet details
                            aq.id(R.id.tabdetbaby3).visible();
                            aq.id(R.id.tabletdetailsbaby3).visible();
                            aq.id(R.id.txt_AVV_UsingIFA3).getTextView().setText(visitChildItems.get(i).getChildvisHIsIFATaken().equals("Yes") ? getResources().getString(R.string.m121) : getResources().getString(R.string.m122));
                            if (visitChildItems.get(i).getChildvisHIsIFATaken().equals("Yes")) {
                                aq.id(R.id.ll_AVV_IFA3).visible();                                aq.id(R.id.txt_AVV_ConsumeIFA3).getTextView().setText(visitChildItems.get(index).getChildvisHIFADaily().equals("Yes") ? getResources().getString(R.string.m121) : getResources().getString(R.string.m122));
                                if (visitChildItems.get(i).getChildvisHIFADaily().equals("Yes")) {
                                    TableRow trTabletsConsumePerDay = findViewById(R.id.trTabletsConsumePerDay3);
                                    trTabletsConsumePerDay.setVisibility(View.VISIBLE);
                                    aq.id(R.id.txt_AVV_ConsumePerDay3).getTextView().setText(visitChildItems.get(i).getChildvisHIFATabletsCount());
                                } else {
                                    TableRow trIFAReason = findViewById(R.id.trIFAReason3);
                                    trIFAReason.setVisibility(View.VISIBLE);
                                    String[] ifaReasons = getResources().getStringArray(R.array.ifareasons);
                                    String reasons = visitChildItems.get(i).getChildvisHIFANotTakenReason();
                                    String[] reasonAfterSplit = reasons.split(",");
                                    StringBuilder origonalReasons = new StringBuilder();
                                    if (reasons.length() > 0) {
                                        for (String s : reasonAfterSplit) {
                                            origonalReasons.append(ifaReasons[Integer.parseInt(s.trim())]).append(",");
                                        }
                                    }
                                    origonalReasons = origonalReasons.deleteCharAt(origonalReasons.lastIndexOf(","));
                                    aq.id(R.id.txt_AVV_IFAReason3).getTextView().setText(origonalReasons.toString());
                                }
                                int whogivenIndex = visitChildItems.get(i).getChildvisHIFAGivenBy();
                                String[] whogave = getResources().getStringArray(R.array.whogaveifatablets);
                                aq.id(R.id.txt_AVV_WhoGaveIFA3).getTextView().setText(whogave[whogivenIndex]);
                            } else {
                                aq.id(R.id.ll_AVV_IFA3).gone();                            }

                            switch (visitChildItems.get(i).getChildvisHDewormingtab()) {
                                case "Yes":
                                    aq.id(R.id.txt_AVV_Deworming3).getTextView().setText(getResources().getString(R.string.m121));
                                    break;
                                case "No":
                                    aq.id(R.id.txt_AVV_Deworming3).getTextView().setText(getResources().getString(R.string.m122));
                                    break;
                                default:
                                    aq.id(R.id.txt_AVV_Deworming3).getTextView().setText(getResources().getString(R.string.dontknow));
                            }
                        }else if(visitChildItems.get(i).getChildNo() == 4) {
                            aq.id(R.id.txtweightvalpncbaby4).text(""+visitChildItems.get(i).getVisCHWeight());
                            aq.id(R.id.txttweightdatevalpncbaby4).text(""+visitChildItems.get(i).getVisCHWeightDate());
                            aq.id(R.id.txttempvalpncbaby4).text(""+visitChildItems.get(i).getVisCHTemperature());
                            aq.id(R.id.txttempdatepncbaby4).text(""+visitChildItems.get(i).getVisCHTemperatureDate());
                            aq.id(R.id.txtrespratevalpncbaby4).text(""+visitChildItems.get(i).getVisCHResprate());
                            aq.id(R.id.txtrespratedatepncbaby4).text(""+visitChildItems.get(i).getVisCHResprateDate());
                            aq.id(R.id.tblexaminationschild4).visible();
                            if(visitChildItems.get(i).isVisCHRegAtGovtFac() == true) {
                                aq.id(R.id.txtchildregidvalbaby4).text("" + visitChildItems.get(i).getVisCHRegGovtFacId());
                                aq.id(R.id.txtchildregdatevalbaby4).text("" + visitChildItems.get(i).getVisCHRegAtGovtFacDate());
                                aq.id(R.id.tblregatgovtfacchild4).visible();
                            }else{
                                aq.id(R.id.txtchildregidvalbaby4).text("");
                                aq.id(R.id.txtchildregdatevalbaby4).text("");
                                aq.id(R.id.tblregatgovtfacchild4).gone();
                            }

                            //Mani 13Sep21:tablet details
                            aq.id(R.id.tabletdetailsbaby4).visible();
                            aq.id(R.id.tabdetbaby4).visible();
                            aq.id(R.id.txt_AVV_UsingIFA4).getTextView().setText(visitChildItems.get(i).getChildvisHIsIFATaken().equals("Yes") ? getResources().getString(R.string.m121) : getResources().getString(R.string.m122));
                            if (visitChildItems.get(index).getChildvisHIsIFATaken().equals("Yes")) {
                                aq.id(R.id.ll_AVV_IFA4).visible();
                                aq.id(R.id.txt_AVV_ConsumeIFA4).getTextView().setText(visitChildItems.get(i).getChildvisHIFADaily().equals("Yes") ? getResources().getString(R.string.m121) : getResources().getString(R.string.m122));
                                if (visitChildItems.get(index).getChildvisHIFADaily().equals("Yes")) {
                                    TableRow trTabletsConsumePerDay = findViewById(R.id.trTabletsConsumePerDay4);
                                    trTabletsConsumePerDay.setVisibility(View.VISIBLE);
                                    aq.id(R.id.txt_AVV_ConsumePerDay4).getTextView().setText(visitChildItems.get(i).getChildvisHIFATabletsCount());
                                } else {
                                    TableRow trIFAReason = findViewById(R.id.trIFAReason4);
                                    trIFAReason.setVisibility(View.VISIBLE);
                                    String[] ifaReasons = getResources().getStringArray(R.array.ifareasons);
                                    String reasons = visitChildItems.get(i).getChildvisHIFANotTakenReason();
                                    String[] reasonAfterSplit = reasons.split(",");
                                    StringBuilder origonalReasons = new StringBuilder();
                                    if (reasons.length() > 0) {
                                        for (String s : reasonAfterSplit) {
                                            origonalReasons.append(ifaReasons[Integer.parseInt(s.trim())]).append(",");
                                        }
                                    }
                                    origonalReasons = origonalReasons.deleteCharAt(origonalReasons.lastIndexOf(","));
                                    aq.id(R.id.txt_AVV_IFAReason4).getTextView().setText(origonalReasons.toString());
                                }
                                int whogivenIndex = visitChildItems.get(i).getChildvisHIFAGivenBy();
                                String[] whogave = getResources().getStringArray(R.array.whogaveifatablets);
                                aq.id(R.id.txt_AVV_WhoGaveIFA4).getTextView().setText(whogave[whogivenIndex]);
                            } else {
                                aq.id(R.id.ll_AVV_IFA4).gone();                            }

                            switch (visitChildItems.get(i).getChildvisHDewormingtab()) {
                                case "Yes":
                                    aq.id(R.id.txt_AVV_Deworming4).getTextView().setText(getResources().getString(R.string.m121));
                                    break;
                                case "No":
                                    aq.id(R.id.txt_AVV_Deworming4).getTextView().setText(getResources().getString(R.string.m122));
                                    break;
                                default:
                                    aq.id(R.id.txt_AVV_Deworming4).getTextView().setText(getResources().getString(R.string.dontknow));
                            }
                        }
                    }
                }
            }
        }
        getImages();
    }

    //25Jul2021 Bindu - set advise
    private String setAdvise(String visHAdvise) {

        String a [] = visHAdvise.split("\\*");
        String advice ="";

        for(int i=1;i<a.length;i++)
        {
            String b[] = a[i].split("\\-");

            String str1 = "", str2="";
            if(b.length>=1)
                str1  = b[0].toLowerCase().trim().replace("\\s+","");
            str1 = str1.replaceAll(" ","");
            str1 = str1.replaceAll("\n","");
            str1 = str1.replaceAll(",","");
//                str2 = str2.replaceAll(".","");
            str1 = str1.replaceAll("\\/","");



            if(b.length>=2)
                str2 = b[1].replace("\\s+","").toLowerCase();
            str2 = str2.replaceAll(" ","");
            str2 = str2.replaceAll("\n","");
            str2 = str2.replaceAll(",","");
            //  str2 = str2.replaceAll(".","");
            str2 = str2.replaceAll("\\/","");



            int identifier1 = getResources().getIdentifier(
                    str1  ,
                    "string", getPackageName());

            int identifier2 = 0; String s3 = "", s4 = "" ;
            if(str2.contains("eatsmallfreq")) {// 25Jul2021 Bindu
                str2 = "eatsmallfrequentfoods";
                identifier2 = getResources().getIdentifier
                        (str2,
                                "string", getPackageName());
                s3 = getResources().getString(R.string.avoidoilyfood);

                s4 = getResources().getString(R.string.eatveganddrinkfluids);

            } else if(str2.contains("startthewoman")) {// 25Jul2021 Bindu
                str2 = "startifa";
                identifier2 = getResources().getIdentifier
                        (str2,
                                "string", getPackageName());
                s3 = getResources().getString(R.string.gethbtested);

                s4 = getResources().getString(R.string.givealbendazole);

            } else if(str2.contains("refertodhah")) {
                str2 = "refertocemoc";
                identifier2 = getResources().getIdentifier
                        (str2,
                                "string", getPackageName());
            }
            else
                identifier2 = getResources().getIdentifier
                        (str2,
                                "string", getPackageName());



            String s1 = "",s2 ="";
            if(identifier1>0)
                s1 =  getResources().getString(identifier1);
            if(identifier2>0)
                s2 =  getResources().getString(identifier2);

            advice = advice + "* " +s1 +" - "+s2+  (s3.trim().length()>0 ?  s3+", " : "" ) + s4   + "\n"; //25Jul2021 Bindu format advise and add missing

        }
        return advice;
    }


    // Expand or Collapse Symtpoms drawable
    private void expandcollapseSymptoms() {
        if(aq.id(R.id.txtDangSigns).getText().toString().length() > 0) {
            aq.id(R.id.txtDangSignsL).getTextView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_colsym, 0, 0, 0);
        }else{
            aq.id(R.id.txtDangSignsL).getTextView().setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
    }

    // Expand or Collapse Advise drawable
    private void expandcollapseAdvise() {
        if(aq.id(R.id.txtadvisesumm).getText().toString().length() > 0) {
            aq.id(R.id.txtadvisegiven).getTextView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_colsym, 0, 0, 0);
        }else{
            aq.id(R.id.txtadvisegiven).getTextView().setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
    }

    // Expand and Collapse Danger Signs
    public void opencloseAccordionDangerSigns(View v, TextView txt) {
        try {

            if (v.getVisibility() == View.VISIBLE) {
                v.setVisibility(View.GONE);
                txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_expsym, 0, 0, 0);
            }
            else {
                v.setVisibility(View.VISIBLE);
                txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_colsym, 0, 0, 0);
            }
        } catch (Exception e) {

        }
    }

    /**
     * Initiates the Navigation drawer
     */
    private void initiateDrawer() {
        try {
            mDrawerLayout = findViewById(R.id.ett_drawer_layout);
            mDrawerList = findViewById(R.id.ett_left_drawer);


            navDrawerItems = new ArrayList<NavDrawerItem>();


            // adding nav drawer items to array
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.sp_WRegEdit),
                    R.drawable.registration));

            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.services),
                    R.drawable.anm_pending_activities));


            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.unplannedserviceslist),
                    R.drawable.anm_pending_activities));

            //14Sep2019 - Bindu - Add Homevisit
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.homevisitadd),
                    R.drawable.ic_homevisit));

            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.womanhomevisithistory),
                    R.drawable.prenatalhomevisit));

            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deliveryHeading), R.drawable.delivery_info));//01Oct2019 Arpitha

            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deactivate),
                    R.drawable.deactivate));//28Nov2019 Arpitha

            // set a custom shadow that overlays the main content when the drawer
            // opens


            mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

           mDrawerList.setAdapter(new NavDrawerListAdapter(getApplicationContext(), navDrawerItems));
            mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

            // ActionBarDrawerToggle ties together the the proper interactions
            // between the sliding drawer and the action bar app icon
            mDrawerToggle = new ActionBarDrawerToggle(this, /* host Activity */
                    mDrawerLayout, /* DrawerLayout object */
                    R.drawable.ic_drawer, /*
             * nav drawer image to replace 'Up'
             * caret
             */
                    R.string.drawer_open, /*
             * "open drawer" description for
             * accessibility
             */
                    R.string.drawer_close /*
             * "close drawer" description for
             * accessibility
             */
            ) {
                public void onDrawerClosed(View view) {
                    try {
                        displayDrawerItems();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                public void onDrawerOpened(View drawerView) {
                    try {
                        displayDrawerItems();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            mDrawerLayout.setDrawerListener(mDrawerToggle);
        } catch (Exception e) {

        }
    }

    // 18Sep2019 - Bindu Display drawer open close items
    private void displayDrawerItems() throws Exception{

        invalidateOptionsMenu(); // creates call to
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer1);
        getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);


        aq.id(R.id.ivWomanImg).getImageView().setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                mDrawerLayout.closeDrawer(mDrawerList);
            }
        });

        aq.id(R.id.tvWomanName).text((woman.getRegWomanName() + "\t"));
        if (woman.getRegADDate() != null && woman.getRegADDate().length() > 0) {
            aq.id(R.id.tvInfo1).text(woman.getRegADDate());
        }else {
            int noOfWeeks = DateTimeUtil.getNumberOfWeeksFromLMP(woman.getRegLMP());
            aq.id(R.id.tvInfo1).text(getResources().getString(R.string.tvlmp) + woman.getRegLMP() + "  , GA : " + noOfWeeks + "W");
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        try {
            switch (item.getItemId()) {
                case android.R.id.home:
                    Intent home = new Intent(HomeVisitViewActivity.this, MainMenuActivity.class);
                    home.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                    /*startActivity(home);*/
                    displayAlert(getResources().getString(R.string.exit), home);
                    break;

                case R.id.wlist:
                    Intent wlist = new Intent(HomeVisitViewActivity.this, RegisteredWomenActionTabs.class);
                    wlist.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                    /*startActivity(wlist);*/
                    displayAlert(getResources().getString(R.string.exit), wlist);
                    break;

                case R.id.logout:
                    Intent logout = new Intent(HomeVisitViewActivity.this, LoginActivity.class);
                    displayAlert(getResources().getString(R.string.m111), logout);
                    /*startActivity(logout);
                    CommonClass.updateLogoutTime();*/
                    break;

                case R.id.home:
                    Intent intent = new Intent(HomeVisitViewActivity.this, MainMenuActivity.class);
                    intent.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                    /*startActivity(intent);*/
                    displayAlert(getResources().getString(R.string.exit), intent);
                    break;
                //30Sep2019 - Bindu
                case R.id.about: {
                    Intent goToScreen = new Intent(HomeVisitViewActivity.this, AboutActivity.class);
                    goToScreen.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                    displayAlert(getResources().getString(R.string.exit), goToScreen);
                    return true;
                }
                //              17May2021 Bindu
                case R.id.covvaccine:
                    displaycovidvaccinedetails();
                    break;
            }
        } catch (Exception e) {

        }
        return super.onOptionsItemSelected(item);
    }

    private void displayAlert(final String spanText2, final Intent goToScreen) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set dialog message
        alertDialogBuilder.setMessage(spanText2).setCancelable(false).setNegativeButton(
                (getResources().getString(R.string.m121)), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (goToScreen != null) {
                            try {
                                startActivity(goToScreen);
                                if(spanText2.contains("logout"))
                                {
                                    try {
                                        LoginRepository loginRepo = new LoginRepository(databaseHelper);
                                        loginRepo.updateLogoutTime(appState.loginAuditId, appState.sessionUserId);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            dialog.cancel();
                        }
                    }
                }).setPositiveButton((getResources().getString(R.string.m122)),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    /**
     * Initializes the Action Bar items
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        try {
            //16Sep2019 - Bindu
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer1);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);

            aq.id(R.id.tvWomanName).text(woman.getRegWomanName());
            if (woman.getRegADDate() != null && woman.getRegADDate().length() > 0) {
                aq.id(R.id.tvInfo1).text(woman.getRegADDate());
            }else {
                int noOfWeeks = DateTimeUtil.getNumberOfWeeksFromLMP(woman.getRegLMP());
                aq.id(R.id.tvInfo1).text(getResources().getString(R.string.tvlmp) + woman.getRegLMP() + "  , GA : " + noOfWeeks + "W");
            }

            aq.id(R.id.ivWomanImg).gone();

            getMenuInflater().inflate(R.menu.servicesmenu, menu);

            menu.findItem(R.id.logout).setTitle((getResources().getString(R.string.action_logout)));
            menu.findItem(R.id.covvaccine).setVisible(true);//17May2021 Bindu

        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    /**
     * The click listner for ListView in the navigation drawer
     */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            mDrawerLayout.closeDrawer(mDrawerList);
            //   if (woman.getRegADDate() == null || woman.getRegADDate().length() == 0) {
            try {
                if (position == 0) {
                    Intent viewprof = new Intent(HomeVisitViewActivity.this, ViewProfileActivity.class);
                    viewprof.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                    startActivity(viewprof);
                } else if (position == 1) {
                    Intent unplanned = new Intent(HomeVisitViewActivity.this, ServicesSummaryActivity.class);
                    unplanned.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                    startActivity(unplanned);
                } else if (position == 2) {
                    Intent unplanned = new Intent(HomeVisitViewActivity.this, UnplannedServicesListActivity.class);
                    unplanned.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                    startActivity(unplanned);
                }//14Sep2019 - Bindu - Add Home visit
                else if (position == 3) {
                    if(woman.getRegPregnantorMother() == 2 ){ //02Dec2019 - Bindu - add condition - ANC/PNC Home visit
                        Intent nextScreen = new Intent(HomeVisitViewActivity.this, PncHomeVisit.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        startActivity(nextScreen);
                    } else {
                        int daysDiff = DateTimeUtil.getDaysBetweenDates(DateTimeUtil.getTodaysDate(), woman.getRegLMP()); //02Dec2019 - Bindu - chk for nearing del
                        if (daysDiff > 210) {
                            displayNearingDelAlertMessage(woman);
                        } else {
                            Intent homevisit = new Intent(HomeVisitViewActivity.this, HomeVisit.class);
                            homevisit.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(homevisit);
                        }
                    }
                }else if (position == 4) {
                    //if(woman.getLastAncVisitDate().length() > 0 || woman.getLastPncVisitDate().length() > 0) {
                  //  if(wVisitDetails != null && wVisitDetails.get(0).getLastAncVisitDate().length() > 0 || wVisitDetails.get(0).getLastPncVisitDate().length() > 0 )  {
                        Intent homevisit = new Intent(HomeVisitViewActivity.this, HomeVisitListActivity.class);
                        homevisit.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                        startActivity(homevisit);
                   /* }else {
                        Toast.makeText(HomeVisitViewActivity.this, getResources().getString(R.string.norecords), Toast.LENGTH_SHORT).show();
                    }*/
                }//01Oct2019 Arpitha
                else if(position ==5 ) {
                    if (woman.getRegPregnantorMother() == 2 || DateTimeUtil.getNumberOfDaysFromLMP(woman.getRegLMP()) > 210) {
                        Intent del = new Intent(HomeVisitViewActivity.this, DeliveryInfoActivity.class);
                        del.putExtra("woman", woman);
                        del.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        startActivity(del);
                    } else
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.m132), Toast.LENGTH_LONG).show();

                }
//                28Nov2019 Arpitha
                else if(position == 6)
                {
                        Intent deact = new Intent(HomeVisitViewActivity.this, WomanDeactivateActivity.class);
                        deact.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        deact.putExtra("woman", woman);
                        startActivity(deact);
                }
            } catch (Exception e) {

            }

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        /*
         * You'll need this in your class to release the helper when done.
         */
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

    @Override
    public void onBackPressed() {

    }

    //02Dec2019 - Bindu - Alert message nearing del
    public  void displayNearingDelAlertMessage(tblregisteredwomen regwoman) throws Exception{
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        String alertmsg =  regwoman.getRegWomanName() + ", " + " EDD : " + regwoman.getRegEDD() ;
        //alertDialogBuilder.setMessage(message).setCancelable(false).setTitle(Html.fromHtml("<font color='#FF7F27'>Alert -- </font><br><br><p>"+alertmsg + "</p>")).setNegativeButton(
        String msg = "<font color='#CC3333'> Nearing Delivery - </font>"+alertmsg;
        alertDialogBuilder.setMessage(getResources().getText(R.string.alerthomevisitnearingdelmsg)).setCancelable(false).setTitle(Html.fromHtml(msg)).setNegativeButton(
                getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent nextScreen = new Intent(HomeVisitViewActivity.this, HomeVisit.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        nextScreen.putExtra("woman", woman);
                        startActivity(nextScreen);
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

//    15May2021 Arpitha
private void setPncAndNbSigns() throws SQLException {

    HomeVisitRepository homeVisitRepository = new HomeVisitRepository(databaseHelper);
    /*ArrayList<String> list = homeVisitRepository.getHomeVisitSymptoms(woman.getUserId(),woman.getWomanId(),visitItems.get(index).getVisitNum());
    String symptom = "";
    if(list!=null && list.size()>0) {

        for(int i=0;i<list.size();i++) {
            String strSy = list.get(i);
            int identifier = getResources().getIdentifier
                    (strSy,
                            "string", getPackageName());
            if(identifier>0) {
                if (i == list.size() - 1)
                    symptom = symptom + getResources().getString(identifier);
                else
                    symptom = symptom + getResources().getString(identifier) + ",";
            }else  {
                if (i == list.size() - 1)
                    symptom = symptom + strSy;
                else
                    symptom = symptom + strSy + ",";
            }

        }
    }*/


    /*ArrayList<String> listChild = homeVisitRepository.getHomeVisitSymptomsForChild(woman.getUserId(),woman.getWomanId(),visitItems.get(index).getVisitNum());
    String symptomChild = "";
    if(listChild!=null && listChild.size()>0) {

        for(int i=0;i<listChild.size();i++) {
            String strSy = listChild.get(i);
            if(strSy.contains("save"))
                strSy = strSy.replace("save","");
            int identifier = getResources().getIdentifier
                    (strSy,
                            "string", getPackageName());
            if(identifier>0) {
                if (i == listChild.size() - 1)
                    symptomChild = symptomChild + getResources().getString(identifier);
                else
                    symptomChild = symptomChild + getResources().getString(identifier) + ",";
            }else {
                if (i == listChild.size() - 1)
                    symptomChild = symptomChild + strSy;
                else
                    symptomChild = symptomChild + strSy + ",";
            }

        }
    }*/
//    aq.id(R.id.txtDangSignspnc).text(symptom);
//    aq.id(R.id.txtDangSignsNb).text(symptomChild);
    String c = setWomanCompl(index); //25Jul2021 Bindu - change method
    String b = setChildCompl(index);
    aq.id(R.id.txtDangSignspnc).text(c);
    aq.id(R.id.txtDangSignsNb).text(b);
}

//17May2021 Bindu
        private void getcovidtestdetails(int visitNum, String selectedWomanId) throws Exception{
            CovidDetailsRepository covRepo = new CovidDetailsRepository(databaseHelper);
            covvactestdetails = covRepo.getVisitCovidTestDetails(""+visitNum, selectedWomanId);
            if(covvactestdetails != null && covvactestdetails.size() > 0) {
//                aq.id(R.id.txt_AVV_HealthIssues).text(covvactestdetails.get(0).getHealthIssues()); //18May2021 change it to indices and compare
                if(covvactestdetails.get(0).getHealthIssues() != null && covvactestdetails.get(0).getHealthIssues().toString().length() > 0) {
                    String[] healthissues = getResources().getStringArray(R.array.healthproblemsuffer);
                    StringBuilder Hissues = new StringBuilder();
                    String indices = covvactestdetails.get(0).getHealthIssues();
                    if (indices.contains(",")) {
                        String[] indicesvalues = indices.split(",");
                        for (String s : indicesvalues) {
                            Hissues.append(healthissues[Integer.parseInt(s.trim())]).append(", ");
                        }
                        Hissues = Hissues.deleteCharAt(Hissues.lastIndexOf(","));
                        aq.id(R.id.txt_AVV_HealthIssues).getTextView().setText(Hissues);
                    } else {
                        aq.id(R.id.txt_AVV_HealthIssues).getTextView().setText(healthissues[Integer.parseInt(covvactestdetails.get(0).getHealthIssues())]);
                    }
                } else {
                    aq.id(R.id.txt_AVV_HealthIssues).text(getResources().getString(R.string.adolno));
                }
             if(covvactestdetails.get(0).getHealthIssuesOthers() != null && covvactestdetails.get(0).getHealthIssuesOthers().toString().length() > 0) {
                 aq.id(R.id.txt_AVV_HealthIssuesOthers).text(covvactestdetails.get(0).getHealthIssuesOthers());
                 aq.id(R.id.tr_covidhealthissueother).visible();
             }
             else
                 aq.id(R.id.tr_covidhealthissueother).gone();

          if(covvactestdetails.get(0).getHealthIssues() != null && covvactestdetails.get(0).getHealthIssues().toString().length() > 0) {
                    aq.id(R.id.txt_AVV_CovidTest).text(covvactestdetails.get(0).getCovidTest());
                 aq.id(R.id.tr_CovidTest).visible();
                     if(covvactestdetails.get(0).getCovidTest().toString().equalsIgnoreCase(getResources().getString(R.string.yesshortform))) {
                         aq.id(R.id.txt_AVV_CovidTestResult).text(covvactestdetails.get(0).getCovidResult());
                         aq.id(R.id.txt_AVV_CovidTestResultDate).text(covvactestdetails.get(0).getCovidResultDate());
                         aq.id(R.id.tr_CovidTestResultDate).visible();
                         aq.id(R.id.tr_CovidTestResult).visible();
                     }else {
                         aq.id(R.id.tr_CovidTestResultDate).gone();
                         aq.id(R.id.tr_CovidTestResult).gone();
                     }
                }else {
                 aq.id(R.id.tr_CovidTest).gone();
                 aq.id(R.id.tr_CovidTestResultDate).gone();
                 aq.id(R.id.tr_CovidTestResult).gone();
             }
            }else { //23May2021 Bindu - if null then hide
                aq.id(R.id.tr_CovidTest).gone();
                aq.id(R.id.tr_CovidTestResultDate).gone();
                aq.id(R.id.tr_CovidTestResult).gone();
                aq.id(R.id.tr_covidhealthissueother).gone();
            }
        }

    private void getcovidtestdetailsPNC(int visitNum, String selectedWomanId) throws Exception{

        //Mani 7sep2021 Added "-" for symptoms
        aq.id(R.id.txt_AVV_HealthIssuespnc).getTextView().setText("-"); //23May2021 Bindu
        CovidDetailsRepository covRepo = new CovidDetailsRepository(databaseHelper);
        covvactestdetails = covRepo.getVisitCovidTestDetails(""+visitNum, selectedWomanId);
        if(covvactestdetails != null && covvactestdetails.size() > 0) {
//            aq.id(R.id.txt_AVV_HealthIssuespnc).text(covvactestdetails.get(0).getHealthIssues());
            if(covvactestdetails.get(0).getHealthIssues() != null && covvactestdetails.get(0).getHealthIssues().toString().length() > 0) {
                String[] healthissues = getResources().getStringArray(R.array.healthproblemsuffer);
                StringBuilder Hissues = new StringBuilder();
                String indices = covvactestdetails.get(0).getHealthIssues();
                if (indices.contains(",")) {
                    String[] indicesvalues = indices.split(",");
                    for (String s : indicesvalues) {
                        Hissues.append(healthissues[Integer.parseInt(s.trim())]).append(", ");
                    }
                    Hissues = Hissues.deleteCharAt(Hissues.lastIndexOf(","));
                    aq.id(R.id.txt_AVV_HealthIssuespnc).getTextView().setText(Hissues);
                } else {
                    aq.id(R.id.txt_AVV_HealthIssuespnc).getTextView().setText(healthissues[Integer.parseInt(covvactestdetails.get(0).getHealthIssues())]);
                }
            }else {
                aq.id(R.id.txt_AVV_HealthIssuespnc).text(getResources().getString(R.string.adolno));
            }
            if(covvactestdetails.get(0).getHealthIssuesOthers() != null && covvactestdetails.get(0).getHealthIssuesOthers().toString().length() > 0) {
                aq.id(R.id.txt_AVV_HealthIssuesOtherspnc).text(covvactestdetails.get(0).getHealthIssuesOthers());
                aq.id(R.id.tr_CovidTestpnc).visible();
            }
            else
                aq.id(R.id.tr_covidhealthissueotherpnc).gone();

            if(covvactestdetails.get(0).getHealthIssues() != null && covvactestdetails.get(0).getHealthIssues().toString().length() > 0){
                aq.id(R.id.txt_AVV_CovidTestpnc).text(covvactestdetails.get(0).getCovidTest());
                aq.id(R.id.tr_CovidTestpnc).visible();
                if(covvactestdetails.get(0).getCovidTest().toString().equalsIgnoreCase(getResources().getString(R.string.yesshortform))) {
                    aq.id(R.id.txt_AVV_CovidTestResultpnc).text(covvactestdetails.get(0).getCovidResult());
                    aq.id(R.id.txt_AVV_CovidTestResultDatepnc).text(covvactestdetails.get(0).getCovidResultDate());
                    aq.id(R.id.tr_CovidTestResultDatepnc).visible();
                    aq.id(R.id.tr_CovidTestResultpnc).visible();
                }else {
                    aq.id(R.id.tr_CovidTestResultDatepnc).gone();
                    aq.id(R.id.tr_CovidTestResultpnc).gone();
                }
            }else {
                aq.id(R.id.tr_CovidTestpnc).gone();
                aq.id(R.id.tr_CovidTestResultDatepnc).gone();
                aq.id(R.id.tr_CovidTestResultpnc).gone();
            }
        }else {
            aq.id(R.id.tr_CovidTestpnc).gone();
            aq.id(R.id.tr_CovidTestResultDatepnc).gone();
            aq.id(R.id.tr_CovidTestResultpnc).gone();
            aq.id(R.id.tr_covidhealthissueotherpnc).gone(); //23May2021 Bindu add else condition
        }
    }

    //    get covid vaccine details
   /* private void getCovidVaccineDetails(AQuery aq) throws Exception{
        CovidDetailsRepository covRepo = new CovidDetailsRepository(databaseHelper);
        covVacItems = covRepo.getVisitCovidVaccineDetails(appState.selectedWomanId);
        if(covVacItems != null && covVacItems.size() > 0) {
            for(int i=0; i < covVacItems.size(); i++) {
                if(covVacItems.get(i).getCovidVaccinated().equalsIgnoreCase(getResources().getString(R.string.yesshortform))) {
                    isvaccinated = true;
                    aq.id(R.id.rd_CovidVaccineYes).enabled(false);
                    aq.id(R.id.rd_CovidVaccineNo).enabled(false);
                    aq.id(R.id.rd_CovidVaccineYes).checked(true);
                    if((covVacItems.get(i).getCovidFirstDoseDate().toString().length() > 0) && covVacItems.get(i).getCovidSecondDoseDate().toString().length() > 0){
                        aq.id(R.id.trCovidFirstDoseDate).getView().setVisibility(View.VISIBLE);
                        aq.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.VISIBLE);
                        aq.id(R.id.etVisitCovidFirstDoseDate).text(covVacItems.get(i).getCovidFirstDoseDate());
                        aq.id(R.id.etVisitCovidSecondDoseDate).text(covVacItems.get(i).getCovidSecondDoseDate());
                    }
                    else if(covVacItems.get(i).getCovidFirstDoseDate().toString().length() > 0 && covVacItems.get(i).getCovidSecondDoseDate().toString().length() <= 0) {
                        aq.id(R.id.etVisitCovidFirstDoseDate).text(covVacItems.get(i).getCovidFirstDoseDate());
                        aq.id(R.id.trCovidFirstDoseDate).getView().setVisibility(View.VISIBLE);
                        aq.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.VISIBLE);
                    }
                }else {
                    aq.id(R.id.rd_CovidVaccineNo).checked(true);
                    aq.id(R.id.trCovidFirstDoseDate).getView().setVisibility(View.GONE);
                }
            }
        }else {
            aq.id(R.id.rd_CovidVaccineNo).checked(true);
            aq.id(R.id.trCovidFirstDoseDate).getView().setVisibility(View.GONE);
        }
    }*/

    private void getCovidVaccineDetails(AQuery aq) throws Exception{
        CovidDetailsRepository covRepo = new CovidDetailsRepository(databaseHelper);
        covVacItems = covRepo.getVisitCovidVaccineDetails(appState.selectedWomanId);
        if(covVacItems != null && covVacItems.size() > 0) {
            for(int i=0; i < covVacItems.size(); i++) {
//                if(covVacItems.get(i).getCovidVaccinated().equalsIgnoreCase(getResources().getString(R.string.yesshortform))) {
//                    isvaccinated = true;
                aq.id(R.id.rd_CovidVaccineYes).enabled(false);
                aq.id(R.id.rd_CovidVaccineNo).enabled(false);
                aq.id(R.id.rd_CovidVaccineYes).checked(true);
                    /*if((covVacItems.get(i).getCovidFirstDoseDate().toString().length() > 0) && covVacItems.get(i).getCovidSecondDoseDate().toString().length() > 0){
                        aq.id(R.id.trCovidFirstDoseDate).getView().setVisibility(View.VISIBLE);
                        aq.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.VISIBLE);
                        aq.id(R.id.etVisitCovidFirstDoseDate).text(covVacItems.get(i).getCovidFirstDoseDate());
                        aq.id(R.id.etVisitCovidSecondDoseDate).text(covVacItems.get(i).getCovidSecondDoseDate());
                    }
                    else if(covVacItems.get(i).getCovidFirstDoseDate().toString().length() > 0 && covVacItems.get(i).getCovidSecondDoseDate().toString().length() <= 0) {
                        aq.id(R.id.etVisitCovidFirstDoseDate).text(covVacItems.get(i).getCovidFirstDoseDate());
                        aq.id(R.id.trCovidFirstDoseDate).getView().setVisibility(View.VISIBLE);
                        aq.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.VISIBLE);
                    }*/

                if (covVacItems.get(i).getCovidVaccinatedNo().equalsIgnoreCase("1")) {
                    aq.id(R.id.trCovidFirstDoseDate).getView().setVisibility(View.VISIBLE);
                    aq.id(R.id.etVisitCovidFirstDoseDate).text(covVacItems.get(i).getCovidVaccinatedDate());
                    aq.id(R.id.etVisitCovidFirstDoseDate).enabled(false).background(R.drawable.edittext_disable);
                    aq.id(R.id.imgclearfirstdosedate).gone();
                }
                if (covVacItems.get(i).getCovidVaccinatedNo().equalsIgnoreCase("2")) {
                    aq.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.VISIBLE);
                    aq.id(R.id.etVisitCovidSecondDoseDate).text(covVacItems.get(i).getCovidVaccinatedDate());
                    aq.id(R.id.etVisitCovidSecondDoseDate).enabled(false).background(R.drawable.edittext_disable);
                    aq.id(R.id.imgclearsecdosedate).gone();
                }
               /* }else {
                    aq.id(R.id.rd_CovidVaccineNo).checked(true);
                    aq.id(R.id.trCovidFirstDoseDate).getView().setVisibility(View.GONE);
                }*/
            }
        }else {
            aq.id(R.id.rd_CovidVaccineNo).checked(true);
            aq.id(R.id.trCovidFirstDoseDate).getView().setVisibility(View.GONE);
            aq.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.GONE); //21May2021 Bindu
        }
    }

    private void displaycovidvaccinedetails() {
        try {
            final AlertDialog.Builder alert = new AlertDialog.Builder(this,
                    AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
            final View customView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.covid_vaccine,
                    null);
            alert.setView(customView);

            AQuery aq = new AQuery(customView);
            getCovidVaccineDetails(aq);

            aq.id(R.id.rd_CovidVaccineYes).enabled(false);
            aq.id(R.id.rd_CovidVaccineNo).enabled(false);


            alert.setCancelable(true)
                    .setTitle(getResources().getString(R.string.coviddetails))
                    .setNegativeButton(getResources().getString(R.string.cancel),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    dialog.cancel();
                                }
                            });
            final AlertDialog dialog = alert.create();
            if (dialog != null)
                dialog.show();

            dialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }
    private void getImages() {
        try{
            RecyclerView recyclerViewImage;
            LinearLayout llphotos;
            if (visitItems.get(index).getVisHVisitType().equalsIgnoreCase(getResources().getString(R.string.strpnc))){
                recyclerViewImage = findViewById(R.id.recyclerImagepnc);
                llphotos = findViewById(R.id.llPhotospnc);
            }else {
                recyclerViewImage = findViewById(R.id.recyclerImage);
                llphotos = findViewById(R.id.llPhotos);
            }

            ImageStoreRepository imageStoreRepository = new ImageStoreRepository(databaseHelper);
            String str = woman.getWomanId()+"_HV"+(index+1);
            ArrayList<tblImageStore> allImages = imageStoreRepository.getAllImagesofIDandHomeVisit(str);
            if (allImages.size()!=0){
                recyclerViewImage.setVisibility(View.VISIBLE);
                DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
                float screenWidthDp = displayMetrics.widthPixels / displayMetrics.density;
                int noOfColumns = (int) (screenWidthDp / 125 + 0.5); // +0.5 for correct rounding to int.
                GridLayoutManager layoutManager = new GridLayoutManager(HomeVisitViewActivity.this, noOfColumns);
                recyclerViewImage.setLayoutManager(layoutManager);
                recyclerViewImage.setNestedScrollingEnabled(false);
                recyclerViewImage.setHasFixedSize(true);
                RecyclerViewAdapter recyclerViewAdapter = new RecyclerViewAdapter(HomeVisitViewActivity.this,allImages,databaseHelper);
                recyclerViewImage.setAdapter(recyclerViewAdapter);
            }else {
                llphotos.setVisibility(View.GONE);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    //25Jul2021 Bindu - Set woman complications
    private String setWomanCompl(int pos) throws SQLException{
        HomeVisitRepository homeVisitRepository = new HomeVisitRepository(databaseHelper);
        ArrayList<String> list = null;
        if(visitItems.get(pos).getVisHVisitType().equalsIgnoreCase(getResources().getString(R.string.stranc))) {
             list = homeVisitRepository.getHomeVisitSymptomsNew(visitItems.get(pos).getUserId(),
                    visitItems.get(pos).getWomanId(), visitItems.get(pos).getVisitNum(), getApplicationContext());
        }else {
             list = homeVisitRepository.getHomeVisitSymptomsPNC(visitItems.get(pos).getUserId(),
                    visitItems.get(pos).getWomanId(), visitItems.get(pos).getVisitNum(), getApplicationContext());
        }
        String symptom = "";
        if(list != null && list.size() > 0) {
            ArrayList<String> a = new ArrayList<>();
            for (int i = 0; i < list.size(); i++) {
                if (i == list.size() - 1)
                    symptom = symptom + list.get(i);
                else
                    symptom = symptom + list.get(i) + "\n*";
               // symptom = symptom + list.get(i);
            }
        }

        return symptom;
    }


    //    16May2021 Arpitha
    private String setWomaCompl(int pos) throws SQLException{
        HomeVisitRepository homeVisitRepository = new HomeVisitRepository(databaseHelper);
        ArrayList<String> list = homeVisitRepository.getHomeVisitSymptoms(visitItems.get(pos).getUserId(),
                visitItems.get(pos).getWomanId(),visitItems.get(pos).getVisitNum());
        String symptom = "";
        if(list!=null && list.size()>0) {

            for(int i=0;i<list.size();i++) {
                String[] compl = list.get(i).split("-");
                for(int k=0;k<compl.length;k++) {
                    String strSy = compl[k];
                    //  if(strSy!=null && strSy.trim().length()>0 && strSy.contains(":"))
                    {
                        String[] com =  strSy.split(":");
                        for(int p=0;p<com.length;p++)
                        {
                            String complication = "";
                            if(p<com.length)
                                complication = com[p];

                   /* strSy = strSy.replace(",","");
                    strSy = strSy.replace(",,","");
                    strSy = strSy.replace(",","");
                    strSy = strSy.replace(", ","");*/
                            if(complication!=null && complication.trim().length()>0) {
                                int identifier = getResources().getIdentifier
                                        (complication,
                                                "string", getPackageName());
                                if (identifier > 0) {
                                    if (i == list.size() - 1)
                                        symptom = symptom + getResources().getString(identifier);
                                    else
                                        symptom = symptom + getResources().getString(identifier) + " , ";
                                } else {
                                    if (i == list.size() - 1)
                                        symptom = symptom + complication;
                                    else
                                        symptom = symptom + complication + ",";
                                }
                            }  }
                    }
                }

            }
            symptom = symptom.replace(",,","");
            symptom = symptom.replace(", ,","");
            //   symptom = symptom.replace(", ","");
        }return symptom;
    }
    /*{
        HomeVisitRepository homeVisitRepository = new HomeVisitRepository(databaseHelper);
        ArrayList<String> list = homeVisitRepository.getHomeVisitSymptoms(visitItems.get(pos).getUserId(),
                visitItems.get(pos).getWomanId(),visitItems.get(pos).getVisitNum());
        String symptom = "";
        if(list!=null && list.size()>0) {

            for(int i=0;i<list.size();i++) {
                String[] compl = list.get(i).split("-");
                for(int k=0;k<compl.length;k++) {
                    String strSy = compl[k];
                    int identifier = getResources().getIdentifier
                            (strSy,
                                    "string", "com.sc.stmansi");
                    if (identifier > 0) {
                        if (i == list.size() - 1)
                            symptom = symptom + getResources().getString(identifier);
                        else
                            symptom = symptom + getResources().getString(identifier) + " , ";
                    } else {
                        if (i == list.size() - 1)
                            symptom = symptom + strSy;
                        else
                            symptom = symptom + strSy + ",";
                    }
                }

            }
        }return symptom;
    }*/

    //    16May2021 Arpitha
    private String setChildCompl(int pos) throws SQLException {
        HomeVisitRepository homeVisitRepository = new HomeVisitRepository(databaseHelper);
        ArrayList<String> listChild = homeVisitRepository.getHomeVisitSymptomsForChild(visitItems.get(pos).getUserId(),
                visitItems.get(pos).getWomanId(),visitItems.get(pos).getVisitNum());
        ArrayList<String> chl1 = new ArrayList<>();
        ArrayList<String> chl2 = new ArrayList<>();
        ArrayList<String> chl3 = new ArrayList<>();
        ArrayList<String> chl4 = new ArrayList<>();
        // 26Jul2021 Bindu
        String strother = "";
        for(int j=0;j<listChild.size();j++)
        {
            if(listChild.get(j).toLowerCase().contains("baby1"))
                chl1.add(listChild.get(j));
            if(listChild.get(j).toLowerCase().contains("baby2"))
                chl2.add(listChild.get(j));
            if(listChild.get(j).toLowerCase().contains("baby3"))
                chl3.add(listChild.get(j));
            if(listChild.get(j).toLowerCase().contains("baby4"))
                chl4.add(listChild.get(j));

            //26Jul2021 Bindu
            if(listChild.get(j).contains("-babyAll-")) {
                strother = listChild.get(j);
                strother = strother.replace("-babyAll-", "");
            }
        }
        String symptomChild = "";
        if(chl1!=null && chl1.size()>0) {

            String[] str = new String[0];
            for(int i=0;i<chl1.size();i++) {
                String[] compl = chl1.get(i).split("-");
                //  for (int k = 0; k < compl.length; k++)
                {

                    // }
                    String strSy = compl[0].trim();
                    if (strSy.contains("save"))
                        strSy = strSy.replace("save", "");
                    if (strSy.contains("-")) {
                        str = strSy.split("-");
                        strSy = str[0].trim();
                    }
                    int identifier = getResources().getIdentifier
                            (strSy,
                                    "string", getPackageName());
              /*  String baby = "";
                if(str.length>1)
                    baby = str[1];*/

                    if (identifier > 0) {
                        if (i == chl1.size() - 1) {
                            String comp = getResources().getString(identifier) ;
                            symptomChild = symptomChild + comp;
                        } else {
                            String comp = getResources().getString(identifier);
                            symptomChild = symptomChild + comp + ",";
                        }
                    } else {
                        if (i == chl1.size() - 1)
                            symptomChild = symptomChild + strSy;
                        else
                            symptomChild = symptomChild + strSy + ",";
                    }
                }


            }

            symptomChild = symptomChild.replace(getResources().getString(R.string.nbanybirthdefects),
                    getResources().getString(R.string.nbanybirthdefectsdisp));

            symptomChild = "Baby1 : "+symptomChild;
        }




        if(chl2!=null && chl2.size()>0) {

            String symptomChild2 ="";

            String[] str = new String[0];
            for(int i=0;i<chl2.size();i++) {
                String[] compl = chl2.get(i).split(",");
                for (int k = 0; k < compl.length; k++) {

                    // }
                    String strSy = compl[k].trim();
                    if (strSy.contains("save"))
                        strSy = strSy.replace("save", "");
                    if (strSy.contains("-")) {
                        str = strSy.split("-");
                        strSy = str[0].trim();
                    }
                    int identifier = getResources().getIdentifier
                            (strSy,
                                    "string", getPackageName());
              /*  String baby = "";
                if(str.length>1)
                    baby = str[1];*/

                    if (identifier > 0) {
                        if (i == chl2.size() - 1) {
                            String comp = getResources().getString(identifier) ;
                            symptomChild2 = symptomChild2 + comp;
                        } else {
                            String comp = getResources().getString(identifier);
                            symptomChild2 = symptomChild2 + comp + ",";
                        }
                    } else {
                        if (i == chl2.size() - 1)
                            symptomChild2 = symptomChild2 + strSy;
                        else
                            symptomChild2 = symptomChild2 + strSy + ",";
                    }
                }


            }
            symptomChild2 = "\n\nBaby2 : "+symptomChild2;

            symptomChild = symptomChild+symptomChild2;
        }


        if(chl3!=null && chl3.size()>0) {

            String symptomChild3 ="";

            String[] str = new String[0];
            for(int i=0;i<chl3.size();i++) {
                String[] compl = chl3.get(i).split(",");
                for (int k = 0; k < compl.length; k++) {

                    // }
                    String strSy = compl[k].trim();
                    if (strSy.contains("save"))
                        strSy = strSy.replace("save", "");
                    if (strSy.contains("-")) {
                        str = strSy.split("-");
                        strSy = str[0].trim();
                    }
                    int identifier = getResources().getIdentifier
                            (strSy,
                                    "string", getPackageName());
              /*  String baby = "";
                if(str.length>1)
                    baby = str[1];*/

                    if (identifier > 0) {
                        if (i == chl3.size() - 1) {
                            String comp = getResources().getString(identifier) ;
                            symptomChild3 = symptomChild3 + comp;
                        } else {
                            String comp = getResources().getString(identifier);
                            symptomChild3 = symptomChild3 + comp + ",";
                        }
                    } else {
                        if (i == chl3.size() - 1)
                            symptomChild3 = symptomChild3 + strSy;
                        else
                            symptomChild3 = symptomChild3 + strSy + ",";
                    }
                }


            }

            symptomChild3 = "\n\nBaby3 : "+symptomChild3;

            symptomChild = symptomChild+symptomChild3;
        }

        if(chl4!=null && chl4.size()>0) {

            String symptomChld4 = "";
            String[] str = new String[0];
            for(int i=0;i<chl4.size();i++) {

                String[] compl = chl4.get(i).split(",");
                for (int k = 0; k < compl.length; k++) {

                    // }
                    String strSy = compl[k].trim();
                    if (strSy.contains("save"))
                        strSy = strSy.replace("save", "");
                    if (strSy.contains("-")) {
                        str = strSy.split("-");
                        strSy = str[0].trim();
                    }
                    int identifier = getResources().getIdentifier
                            (strSy,
                                    "string", getPackageName());
              /*  String baby = "";
                if(str.length>1)
                    baby = str[1];*/

                    if (identifier > 0) {
                        if (i == chl4.size() - 1) {
                            String comp = getResources().getString(identifier) ;
                            symptomChld4= symptomChld4 + comp;
                        } else {
                            String comp = getResources().getString(identifier);
                            symptomChld4 = symptomChld4 + comp + ",";
                        }
                    } else {
                        if (i == chl4.size() - 1)
                            symptomChld4 = symptomChld4 + strSy;
                        else
                            symptomChld4 = symptomChld4 + strSy + ",";
                    }
                }


            }

            symptomChld4 = "\n\nBaby4 : "+symptomChld4;

            symptomChild = symptomChild+symptomChld4;
        }


        return symptomChild + "\n\n" +strother; //26Jul2021 Bindu add strother
    }
}
