package com.sc.stmansi.HomeVisit;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.core.content.FileProvider;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.DialogFragment;
import androidx.legacy.app.ActionBarDrawerToggle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.misc.TransactionManager;
import com.sc.stmansi.About.AboutActivity;
import com.sc.stmansi.ImageStore.FileUtils;
import com.sc.stmansi.ImageStore.ImagePojo;
import com.sc.stmansi.ImageStore.RecyclerViewAdapter;
import com.sc.stmansi.R;
import com.sc.stmansi.common.AlertDialogUtil;
import com.sc.stmansi.common.CommonDatePickerFragment;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.common.MultiSelectionSpinner;
import com.sc.stmansi.common.NavDrawerItem;
import com.sc.stmansi.common.NavDrawerListAdapter;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.configuration.SyncState;
import com.sc.stmansi.database.DBHelper;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.deactivate.WomanDeactivateActivity;
import com.sc.stmansi.delivery.DeliveryInfoActivity;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.repositories.AdolescentRepository;
import com.sc.stmansi.repositories.AuditRepository;
import com.sc.stmansi.repositories.CovidDetailsRepository;
import com.sc.stmansi.repositories.FacilityRepository;
import com.sc.stmansi.repositories.HomeVisitRepository;
import com.sc.stmansi.repositories.ImageStoreRepository;
import com.sc.stmansi.repositories.LoginRepository;
import com.sc.stmansi.repositories.RefFacilityRepository;
import com.sc.stmansi.repositories.SMSRepository;
import com.sc.stmansi.repositories.SettingsRepository;
import com.sc.stmansi.repositories.TransactionHeaderRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.services.ServicesSummaryActivity;
import com.sc.stmansi.services.UnplannedServicesListActivity;
import com.sc.stmansi.sms.SendSMS;
import com.sc.stmansi.tables.AuditPojo;
import com.sc.stmansi.tables.MessageLogPojo;
import com.sc.stmansi.tables.TblContactDetails;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.TblVisitDetails;
import com.sc.stmansi.tables.TblVisitHeader;
import com.sc.stmansi.tables.tblCovidTestDetails;
import com.sc.stmansi.tables.tblCovidVaccineDetails;
import com.sc.stmansi.tables.tblImageStore;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.viewprofile.ViewProfileActivity;
import com.sc.stmansi.womanlist.RegisteredWomenActionTabs;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HomeVisit extends AppCompatActivity implements
        View.OnClickListener, View.OnTouchListener, DatePickerDialog.OnDateSetListener {
    static int week;
    ArrayList<String> arrLSymptoms;
    boolean isVisitDate = false;
    //13Nov2019-Bindu
    ArrayList<String> arrLSymptomsSave;
    ArrayList<String> arrLSymptomsadviseSave;
    String strfeverandothersave = "";
    //04Dec2019 - Bindu
    EditText etphn;
    boolean isMessageLogsaved;
    boolean messageSent;
    private tblregisteredwomen woman;
    private TblInstusers user;
    static public AQuery aq;
    private DBHelper newdb;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private ArrayList<NavDrawerItem> navDrawerItems;
    private ListView mDrawerList;
    private int cemocCnt = 0, phcCnt = 0, phc247Cnt = 0, visitNum = 0, transId;
    /*ArrayList<String> arrLSymptomsId;*/
    private TblVisitHeader tblvisH;
    private TblVisitDetails tblvisD;
    private LinkedHashMap<String, ArrayList<String>> insertSignsInvestVal;
    private Activity act;
    private boolean isBtnSummaryClicked = false;
    private Dao<TblVisitHeader, Integer> tblVisHeaderDao;
    private Dao<TblVisitDetails, Integer> tblVisDetailDao;
    private RuntimeExceptionDao<tblregisteredwomen, Integer> tblregDao;
    private tblregisteredwomen hmvisitregdetails;
    private LinkedHashMap<String, String> insertSignsInterpretationVal;
    private LinkedHashMap<String, String> insertDiagnosisVal;
    private String strGA = "";
    private ArrayList<String> facilitynamelist = new ArrayList<String>();
    private Map<String, String> mapFacilityname;
    private boolean isSevereheadachesymChecked = false, isBlurredVisionSymChecked = false, isMildfeverSymChecked = false, isHighfeverSymChecked = false;
    private String urinationpbm = "";
    private EditText currentEditTextViewforDate;
    private ImageView currentImageView;
    private ArrayList<String> arrLSymptomsadvise;
    private LinkedHashMap<String, String> insertSymRefVal;
    private String strfeverandother = "";
    private boolean isRefImm = false, isRef = false;
    private AppState appState;
    private SyncState syncState;
    private DatabaseHelper databaseHelper;
    //08Apr2021 Bindu
    private ArrayList<String> reffacilitynamelist = new ArrayList<String>();
    private ArrayList<String> reffacilitytypelist = new ArrayList<String>();
    private Map<String, String> mapRefFacilityType;
    private Map<String, String> mapRefFacilityname;
    //    16May2021 Bindu
    boolean isCoviddetDt = false;
    private tblCovidTestDetails tblcovtest;
    private tblCovidVaccineDetails tblcovvac;
    private Dao<tblCovidTestDetails, Integer> tblCovidTestDao;
    private Dao<tblCovidVaccineDetails, Integer> tblCovidVaccineDao;
    //    16MAy2021 Bindu
    public static MultiSelectionSpinner spnHealthProblemshv;
    static RadioGroup rgCovidResult, rgCovidTest;
    private List<tblCovidVaccineDetails> covVacItems;
    private boolean isvaccinated = false;
    //20May2021 Bindu
    private LinkedHashMap<String, String> insertCovidVaccineVal;

    //1-6-2021 Rames
    private static final int REQUEST_IMAGE_CAPTURE = 2;
    private RecyclerView recyclerViewImage;
    private String mCurrentPhotoPath;
    private ArrayList<tblImageStore> AllImagesBeforeSave = new ArrayList<>();
    private ArrayList<ImagePojo> AllImagePojo = new ArrayList<>();
    private Dao<tblImageStore, Integer> tblImageStoreDao;
    List<tblCovidVaccineDetails> adolCovVacItems = new ArrayList<>();//04Sep2021 Arpitha

    // Returns SpannableStringBuilder for a given String
    public static SpannableStringBuilder getSSC(String msg) {

        SpannableStringBuilder SS;
        SS = new SpannableStringBuilder(msg);

        return SS;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homevisit);
        try {
            databaseHelper = getHelper();
            Bundle bundle = getIntent().getBundleExtra("globalState");
            appState = bundle.getParcelable("appState");
            syncState = bundle.getParcelable("syncState");

            aq = new AQuery(this);
            WomanRepository womanRepo = new WomanRepository(databaseHelper);
            woman = womanRepo.getRegistartionDetails(appState.selectedWomanId, appState.sessionUserId);

            UserRepository userRepo = new UserRepository(databaseHelper);
            user = userRepo.getOneAuditedUser(appState.ashaId);
            act = this;

            initiateDrawer();
            initializeScreen(aq.id(R.id.rlhomevisit).getView());
            aq.id(R.id.btnhomevisitdetails).getImageView()
                    .setBackgroundColor(getResources().getColor(R.color.gray));

            initialView();

            aq.id(R.id.etvisitdate).getEditText().setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (MotionEvent.ACTION_UP == event.getAction()) {
                        HomeVisit.DatePickerFragment newFragment = HomeVisit.DatePickerFragment.getInstance();
                        newFragment.setWoman(woman);
                        newFragment.show(getSupportFragmentManager(), "datePicker");
                    }
                    return true;
                }
            });

            aq.id(R.id.imgcleardate).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    aq.id(R.id.etvisitdate).text(DateTimeUtil.getTodaysDate());
                    calculateGA(aq.id(R.id.etvisitdate).getText().toString());
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.dateresettotodaysdate), Toast.LENGTH_LONG).show();//21Aug2019 Arpitha

//                    aqUnplSv.id(R.id.imgcleardate).gone();
                }
            });

// Facility name on select listener
            aq.id(R.id.spnfacnameHv).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    try {
                        String selected = adapterView.getItemAtPosition(i).toString();
                        if (selected.equalsIgnoreCase(getResources().getString(R.string.other))) {
                            aq.id(R.id.tretfacname).visible();
                            //aq.id(R.id.etreffacilityname).text("");
                        } else {
                            aq.id(R.id.tretfacname).gone();
                            aq.id(R.id.etreffacilityname).text("");
                        }
                    } catch (Exception e) {
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            //08Apr2021 Bindu add ref fac type listener
            aq.id(R.id.spnfactype).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    try {
                        String selected = adapterView.getItemAtPosition(i).toString();
                        if (selected.equalsIgnoreCase(getResources().getString(R.string.selectfacilityname))) {
                            aq.id(R.id.trspnfacname).gone();
                            aq.id(R.id.spnfacname).setSelection(0);
                        } else {
                            aq.id(R.id.trspnfacname).visible();
                            setRefFacilityName(selected);
                        }
                    } catch (Exception e) {
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            // Symptoms drwable on touch listener
            aq.id(R.id.txtDangSignsL).getTextView().setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (event.getRawX() <= aq.id(R.id.txtDangSignsL).getTextView().getTotalPaddingLeft()) {
                            opencloseAccordionDangerSigns(aq.id(R.id.txtDangSigns).getView(), aq.id(R.id.txtDangSignsL).getTextView());
                            return true;
                        }
                    }
                    return true;
                }
            });

            // Advise drwable on touch listener
            aq.id(R.id.txtadvisegiven).getTextView().setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (event.getRawX() <= aq.id(R.id.txtadvisegiven).getTextView().getTotalPaddingLeft()) {
                            opencloseAccordionDangerSigns(aq.id(R.id.txtadvisesumm).getView(), aq.id(R.id.txtadvisegiven).getTextView());
                            return true;
                        }
                    }
                    return true;
                }
            });

            //20Sep2019 - Bindu - Date On touch
            aq.id(R.id.etbpdonedate).getEditText().setOnTouchListener(this);
            aq.id(R.id.etpulsedate).getEditText().setOnTouchListener(this);
            aq.id(R.id.ethbdonedate).getEditText().setOnTouchListener(this);
            aq.id(R.id.ettempdonedate).getEditText().setOnTouchListener(this);
            aq.id(R.id.etproteinuriadonedate).getEditText().setOnTouchListener(this);
            //02oct2019 - bindu - set weight date listener
            aq.id(R.id.etweightdate).getEditText().setOnTouchListener(this);

            aq.id(R.id.ethbpalp).getEditText().addTextChangedListener(watcher);
            aq.id(R.id.etnoofparacetamolgiven).getEditText().addTextChangedListener(watcher);
            aq.id(R.id.etnoofalbendazolegiven).getEditText().addTextChangedListener(watcher);
            aq.id(R.id.etnoofifagiven).getEditText().addTextChangedListener(watcher);
            aq.id(R.id.etTempf).getEditText().addTextChangedListener(watcher);
            aq.id(R.id.etbpsystolic).getEditText().addTextChangedListener(watcher);
            aq.id(R.id.etbpdiastolic).getEditText().addTextChangedListener(watcher);
            aq.id(R.id.etnooffsfagiven).getEditText().addTextChangedListener(watcher);

            aq.id(R.id.etTemp).getEditText().addTextChangedListener(watcher);
            aq.id(R.id.etbpsystolicsumm).getEditText().addTextChangedListener(watcher);
            aq.id(R.id.etbpdiastolicsumm).getEditText().addTextChangedListener(watcher);
            aq.id(R.id.ethb).getEditText().addTextChangedListener(watcher);
            aq.id(R.id.etweight).getEditText().addTextChangedListener(watcher);
            aq.id(R.id.etpulse).getEditText().addTextChangedListener(watcher);

            //16May2021 Bindu
            aq.id(R.id.etVisitCovidTestDate).getEditText().setOnTouchListener(this);
            aq.id(R.id.etVisitCovidFirstDoseDate).getEditText().setOnTouchListener(this);
            aq.id(R.id.etVisitCovidSecondDoseDate).getEditText().setOnTouchListener(this);
            aq.id(R.id.tr_CovidTest).gone();

            rgCovidResult = findViewById(R.id.rgCovidResult);
            rgCovidTest = findViewById(R.id.rgCovidTest); //21May2021 Bindu
            spnHealthProblemshv = findViewById(R.id.spnVisitHealthProblems);
            String[] healthproblems = getResources().getStringArray(R.array.healthproblemsuffer);
            spnHealthProblemshv.setItems(healthproblems);

            if (spnHealthProblemshv.getSelectedItemsAsString().contains("Others")) {
                aq.id(R.id.trHealthProblemYesOthers).visible();
            } else {
                aq.id(R.id.trHealthProblemYesOthers).gone();
            }

            spnHealthProblemshv.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (spnHealthProblemshv.getSelectedIndicies().size() != 0) {
                        aq.id(R.id.llCovidQs).getView().setVisibility(View.VISIBLE);
                    } else {
                        aq.id(R.id.llCovidQs).getView().setVisibility(View.GONE);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            aq.id(R.id.spnVisitVaccineDose).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                    if(!isvaccinated) {
                    if (covVacItems != null && covVacItems.size() == 0) { //20May2021 Bindu
                        aq.id(R.id.etVisitCovidFirstDoseDate).text("");
                        aq.id(R.id.etVisitCovidSecondDoseDate).text("");
                        aq.id(R.id.trCovidFirstDoseDate).getView().setVisibility(View.GONE);
                        aq.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.GONE);
                    /*if (i == 0) {
                        aq.id(R.id.trCovidFirstDoseDate).getView().setVisibility(View.GONE);
                        aq.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.GONE);
                    } else*/

                        if (i == 1) {
                            aq.id(R.id.trCovidFirstDoseDate).getView().setVisibility(View.VISIBLE);
                            aq.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.GONE);
                        } else if (i == 2) {
                            aq.id(R.id.trCovidFirstDoseDate).getView().setVisibility(View.VISIBLE);
                            aq.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.VISIBLE);
                        }
                    }
//                    04Sep2021 Arpitha
                   else if (adolCovVacItems != null && adolCovVacItems.size() == 0) { //20May2021 Bindu
                        aq.id(R.id.etVisitCovidFirstDoseDate).text("");
                        aq.id(R.id.etVisitCovidSecondDoseDate).text("");
                        aq.id(R.id.trCovidFirstDoseDate).getView().setVisibility(View.GONE);
                        aq.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.GONE);
                    /*if (i == 0) {
                        aq.id(R.id.trCovidFirstDoseDate).getView().setVisibility(View.GONE);
                        aq.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.GONE);
                    } else*/

                        if (i == 1) {
                            aq.id(R.id.trCovidFirstDoseDate).getView().setVisibility(View.VISIBLE);
                            aq.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.GONE);
                        } else if (i == 2) {
                            aq.id(R.id.trCovidFirstDoseDate).getView().setVisibility(View.VISIBLE);
                            aq.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.VISIBLE);
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            if ((woman.getDateDeactivated() != null &&
                    woman.getDateDeactivated().trim().length() > 0)
                    || user.getIsDeactivated() == 1) {
                disableScreen();//26Nov2019 Arpitha

            }

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    // Iniital View
    private void initialView() throws Exception {
        aq.id(R.id.scrhomevisitsummary).gone();
        aq.id(R.id.imgSummaryAddPhoto).gone();
        HomeVisitRepository homeRepo = new HomeVisitRepository(databaseHelper);
        visitNum = homeRepo.getLastHomeVisitNoWoman(appState.sessionUserId, woman.getWomanId());

        aq.id(R.id.txtheading).text(getResources().getString(R.string.homevisit) + " - " + " # " + visitNum);
        aq.id(R.id.etvisitdate).text(DateTimeUtil.getTodaysDate());

        //17Oct2019 - Bindu - nearing del msg
        int daysDiff = DateTimeUtil.getDaysBetweenDates(DateTimeUtil.getTodaysDate(), woman.getRegLMP());
        if (daysDiff > 210) {
            aq.id(R.id.txtnearingdelnotes).visible();
        } else
            aq.id(R.id.txtnearingdelnotes).gone();

        calculateGA(DateTimeUtil.getTodaysDate());

        // Disable Spinner Tab -para/ifa/albendazole/fsfa
        aq.id(R.id.spnparacetamolnotgivenreason).enabled(false);
        aq.id(R.id.spnparacetamolnotgivenreason).background(R.drawable.spinner_bg_disabled);
        aq.id(R.id.spnalbendazolenotgivenreason).enabled(false);
        aq.id(R.id.spnalbendazolenotgivenreason).background(R.drawable.spinner_bg_disabled);
        aq.id(R.id.spnifanotgivenreason).enabled(false);
        aq.id(R.id.spnifanotgivenreason).background(R.drawable.spinner_bg_disabled);
        aq.id(R.id.spnfsfanotgivenreason).enabled(false);
        aq.id(R.id.spnfsfanotgivenreason).background(R.drawable.spinner_bg_disabled);

        aq.id(R.id.etweightdate).enabled(false).background(R.drawable.editextdisable_none);
        aq.id(R.id.etbpdonedate).enabled(false).background(R.drawable.editextdisable_none);
        aq.id(R.id.etpulsedate).enabled(false).background(R.drawable.editextdisable_none);
        aq.id(R.id.ettempdonedate).enabled(false).background(R.drawable.editextdisable_none);
        aq.id(R.id.ethbdonedate).enabled(false).background(R.drawable.editextdisable_none);
        aq.id(R.id.etproteinuriadonedate).enabled(false).background(R.drawable.editextdisable_none);
        aq.id(R.id.chkabdominalpain).enabled(false); //19Oct2019 - Bindu Abdominal pain - disable
        //16Nov2019 - Bindu - Const not relieved disable
        aq.id(R.id.chkconstipationnotrelieved).enabled(false);
        //        16May2021 Bindu hide covid result
        aq.id(R.id.trCovidResult).gone();
//        17May2021 Bindu
        getCovidVaccineDetails();

    }

    //    get covid vaccine details
    private void getCovidVaccineDetails() throws Exception {
        CovidDetailsRepository covRepo = new CovidDetailsRepository(databaseHelper);
        covVacItems = covRepo.getVisitCovidVaccineDetails(appState.selectedWomanId);
//        04Sep2021 Arpitha

      String adolId = new AdolescentRepository(databaseHelper).getAdolId(woman.getRegAadharCard());
       if(adolId!=null && adolId.trim().length()>0)
        adolCovVacItems = covRepo.getAdolVisitCovidVaccinDetails(adolId);


        if (covVacItems != null && covVacItems.size() > 0) {
            if (covVacItems.size() > 0) { // if both the doses are completed
                opencloseAccordion(aq.id(R.id.llcovidvaccinedetails).getView(), aq.id(R.id.txtcovidvaccine).getTextView());
                aq.id(R.id.llcovidvaccinedetails).backgroundColor(getResources().getColor(R.color.lightgray));
                aq.id(R.id.trCovidVaccinDose).gone();
                aq.id(R.id.rd_CovidVaccineYes).enabled(false);
                aq.id(R.id.rd_CovidVaccineNo).enabled(false);
                aq.id(R.id.rd_CovidVaccineYes).checked(true);
            }
            for (int i = 0; i < covVacItems.size(); i++) {
                isvaccinated = true;

                if (covVacItems.get(i).getCovidVaccinatedNo().equalsIgnoreCase("1")) {
                    aq.id(R.id.trCovidFirstDoseDate).getView().setVisibility(View.VISIBLE);
                    aq.id(R.id.etVisitCovidFirstDoseDate).text(covVacItems.get(i).getCovidVaccinatedDate());
                    aq.id(R.id.etVisitCovidFirstDoseDate).enabled(false).background(R.drawable.edittext_disable);
                    aq.id(R.id.imgclearfirstdosedate).gone();
                    aq.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.VISIBLE);
                }
                if (covVacItems.get(i).getCovidVaccinatedNo().equalsIgnoreCase("2")) {
                    aq.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.VISIBLE);
                    aq.id(R.id.etVisitCovidSecondDoseDate).text(covVacItems.get(i).getCovidVaccinatedDate());
                    aq.id(R.id.etVisitCovidSecondDoseDate).enabled(false).background(R.drawable.edittext_disable);
                    aq.id(R.id.imgclearsecdosedate).gone();
                }

            }
        }else if(adolCovVacItems != null && adolCovVacItems.size() > 0)//04Sep2021 Arpitha
            {
            if (adolCovVacItems.size() > 0) { // if both the doses are completed
                opencloseAccordion(aq.id(R.id.llcovidvaccinedetails).getView(), aq.id(R.id.txtcovidvaccine).getTextView());
                aq.id(R.id.llcovidvaccinedetails).backgroundColor(getResources().getColor(R.color.lightgray));
                aq.id(R.id.trCovidVaccinDose).gone();
                aq.id(R.id.rd_CovidVaccineYes).enabled(false);
                aq.id(R.id.rd_CovidVaccineNo).enabled(false);
                aq.id(R.id.rd_CovidVaccineYes).checked(true);
            }
            for (int i = 0; i < adolCovVacItems.size(); i++) {
                isvaccinated = true;

                if (adolCovVacItems.get(i).getCovidVaccinatedNo().equalsIgnoreCase("1")) {
                    aq.id(R.id.trCovidFirstDoseDate).getView().setVisibility(View.VISIBLE);
                    aq.id(R.id.etVisitCovidFirstDoseDate).text(adolCovVacItems.get(i).getCovidVaccinatedDate());
                    aq.id(R.id.etVisitCovidFirstDoseDate).enabled(false).background(R.drawable.edittext_disable);
                    aq.id(R.id.imgclearfirstdosedate).gone();
                    aq.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.VISIBLE);
                }
                if (adolCovVacItems.get(i).getCovidVaccinatedNo().equalsIgnoreCase("2")) {
                    aq.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.VISIBLE);
                    aq.id(R.id.etVisitCovidSecondDoseDate).text(adolCovVacItems.get(i).getCovidVaccinatedDate());
                    aq.id(R.id.etVisitCovidSecondDoseDate).enabled(false).background(R.drawable.edittext_disable);
                    aq.id(R.id.imgclearsecdosedate).gone();
                }

            }
        }
    }

    // Display symtoms based on GA
    private void displaySymptomsbasedonGA() {
        if (aq.id(R.id.chkpalpitations).isChecked() && aq.id(R.id.chkeasyfatiguability).isChecked() && aq.id(R.id.chkbreathlessnessatrest).isChecked()) {
            if (week > 13) {         // Based on 2nd trimester give albendazole
                aq.id(R.id.txtpalpactionalbendazole).visible();
                aq.id(R.id.chkalbedazolegiven).visible();
                aq.id(R.id.chkalbedazolegiven).checked(true);
            } else {
                aq.id(R.id.txtpalpactionalbendazole).gone();
                aq.id(R.id.chkalbedazolegiven).gone();
                aq.id(R.id.chkalbedazolegiven).checked(false);
            }
        }

        if (week > 12) {
            aq.id(R.id.chkurinationafter12wks).visible();
            aq.id(R.id.v1urinationafter12wks).visible();
            aq.id(R.id.trurinationafter12wks).visible();
        } else {
            aq.id(R.id.chkurinationafter12wks).gone();
            aq.id(R.id.v1urinationafter12wks).gone();
            aq.id(R.id.trurinationafter12wks).gone();
            aq.id(R.id.chkurinationafter12wks).checked(false);
            aq.id(R.id.txtbleedingpvdiagnosisval).text(""); //14Nov2019 - Bindu - set bleeding pv diag and adv to blank
            aq.id(R.id.txtbleedingpvaction).text("");
        }

        if (week < 20) {
            aq.id(R.id.chkbleedingpvbefore20wks).visible();
            aq.id(R.id.chkbleedingpvafter20wks).gone();
            aq.id(R.id.chkbleedingpvafter20wks).checked(false);
            aq.id(R.id.txtbleedingpvdiagnosisval).text(""); //14Nov2019 - Bindu - set bleeding pv diag and adv to blank
            aq.id(R.id.txtbleedingpvaction).text("");
        } else {
            aq.id(R.id.chkbleedingpvbefore20wks).gone();
            aq.id(R.id.chkbleedingpvafter20wks).visible();
            aq.id(R.id.chkbleedingpvbefore20wks).checked(false);
            aq.id(R.id.txtbleedingpvdiagnosisval).text(""); //14Nov2019 - Bindu - set bleeding pv diag and adv to blank
            aq.id(R.id.txtbleedingpvaction).text("");
        }

        if (week >= 14) {
            aq.id(R.id.lltwins).visible();
            aq.id(R.id.lliugr).visible();
        } else {
            aq.id(R.id.lltwins).gone();
            aq.id(R.id.lliugr).gone();
            aq.id(R.id.chktwinsmultiplepregHV).checked(false);
            aq.id(R.id.chkiugrHV).checked(false);
        }

        if (week > 22)
            aq.id(R.id.lllaborpaingt12hrs).visible();
        else {
            aq.id(R.id.lllaborpaingt12hrs).gone();
            aq.id(R.id.chklaborpaingt12hrs).checked(false);
        }
    }

    /**
     * This is a Recursive method that traverse through the group and subgroup
     * of view Sets text
     *
     * @throws Exception
     */
    private ArrayList<View> initializeScreen(View v) throws Exception {
        if (!(v instanceof ViewGroup)) {
            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);

            // TextView - Assign Labels
            if (v.getClass() == TextView.class) {
                aq.id(v.getId()).text(getSS(aq.id(v.getId()).getText().toString()));
            }
            if (v.getClass() == RadioButton.class || v.getClass() == AppCompatRadioButton.class) {
                aq.id(v.getId()).getButton().setOnClickListener(this);
                aq.id(v.getId()).text(getSS(aq.id(v.getId()).getText().toString()));
            }

            // EditText hint - Assign Labels
            if (v.getClass() == EditText.class) {
                if (aq.id(v.getId()).getEditText().getHint() != null)
                    aq.id(v.getId()).getEditText().setHint(getSS(aq.id(v.getId()).getEditText().getHint() + ""));
            }

            // ImageButton/ImageView - OnClickListener commonClick
            if (v.getClass() == ImageButton.class || v.getClass() == ImageView.class || v.getClass() == AppCompatImageButton.class || v.getClass() == AppCompatImageView.class) {
                aq.id(v.getId()).getImageView().setOnClickListener(this);
            }

            // Checkbox - OnClickListener commonClick
            if (v.getClass() == CheckBox.class || v.getClass() == AppCompatCheckBox.class) {
                aq.id(v.getId()).getCheckBox().setOnClickListener(this);

            }

            return viewArrayList;
        }


        ArrayList<View> result = new ArrayList<View>();

        ViewGroup viewGroup = (ViewGroup) v;
        for (int i = 0; i < viewGroup.getChildCount(); i++) {

            View child = viewGroup.getChildAt(i);

            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);
            viewArrayList.addAll(initializeScreen(child));

            result.addAll(viewArrayList);
        }
        return result;
    }

    /**
     * Returns SpannableStringBuilder for a given String
     */
    public SpannableStringBuilder getSS(String srcText) {
        return getSSC(srcText);
    }

    // Calculate Gest age
    private void calculateGA(String selectedDate) {
        try {
            int noOfDays = DateTimeUtil.getNumberOfDaysFromLMPVisitDate(woman.getRegLMP(), selectedDate);
            week = noOfDays / 7;
            int days = noOfDays % 7;
            if (days > 0) {
               /* if (days == 1)
                    aq.id(R.id.txtgestage).text(act.getResources().getString(R.string.txtgestageatvisit) + "- " + week + "W " + days + "D");
                else*/
                aq.id(R.id.txtgestage).text(act.getResources().getString(R.string.txtgestageatvisit) + "- " + week + "W " + days + "D");
                strGA = week + "W " + days + "D";
            } else {
                aq.id(R.id.txtgestage).text(act.getResources().getString(R.string.txtgestageatvisit) + "- " + week + "W ");
                strGA = week + "W ";
            }

            displaySymptomsbasedonGA();
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    @Override
    public void onClick(View view) {
        try {
            switch (view.getId()) {
                case R.id.imgSummaryAddPhoto:
                    if (AllImagesBeforeSave!=null && AllImagesBeforeSave.size()<2){
                        callCamera();
                    }else {
                        AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.maxlimitreached),HomeVisit.this);
                    }
                    break;

                case R.id.llvomitingid:
                    opencloseAccordion(aq.id(R.id.llvomdetails).getView(), aq.id(R.id.txtvomiting).getTextView());
                    aq.id(R.id.llvomdetails).backgroundColor(getResources().getColor(R.color.lightgray));
                    break;

                case R.id.llpalpid:
                    opencloseAccordion(aq.id(R.id.llpalpdetails).getView(), aq.id(R.id.tvpalplbl).getTextView());
                    aq.id(R.id.llpalpdetails).backgroundColor(getResources().getColor(R.color.lightgray));
                    break;

                case R.id.lledemaid:
                    opencloseAccordion(aq.id(R.id.lledemadetails).getView(), aq.id(R.id.txtedema).getTextView());
                    aq.id(R.id.lledemadetails).backgroundColor(getResources().getColor(R.color.lightgray));
                    break;
                case R.id.llheartburnandnauseaid:
                    opencloseAccordion(aq.id(R.id.llheartburnandnauseadetails).getView(), aq.id(R.id.txtheartburnandnausea).getTextView());
                    aq.id(R.id.llheartburnandnauseadetails).backgroundColor(getResources().getColor(R.color.lightgray));
                    break;
            /*case R.id.llfreqofurinationid:
                opencloseAccordion(aq.id(R.id.llfreqofurinationdetails).getView(), aq.id(R.id.txtfreqofurination).getTextView());
                aq.id(R.id.llfreqofurinationdetails).backgroundColor(getResources().getColor(R.color.lightgray));
                break;*/
                case R.id.llconstipationid:
                    opencloseAccordion(aq.id(R.id.llconstipationdetails).getView(), aq.id(R.id.txtconstipation).getTextView());
                    aq.id(R.id.llconstipationdetails).backgroundColor(getResources().getColor(R.color.lightgray));
                    break;
                case R.id.llbleedingpvid:
                    opencloseAccordion(aq.id(R.id.llbleedingpvdetails).getView(), aq.id(R.id.txtbleedingpv).getTextView());
                    aq.id(R.id.llbleedingpvdetails).backgroundColor(getResources().getColor(R.color.lightgray));
                    break;
                case R.id.llfeverid:
                    opencloseAccordion(aq.id(R.id.llfeverdetails).getView(), aq.id(R.id.txtfever).getTextView());
                    aq.id(R.id.llfeverdetails).backgroundColor(getResources().getColor(R.color.lightgray));
                    break;
                case R.id.llfoetalmovementid:
                    opencloseAccordion(aq.id(R.id.llfoetalmovementdetails).getView(), aq.id(R.id.txtfoetalmovement).getTextView());
                    aq.id(R.id.llfoetalmovementdetails).backgroundColor(getResources().getColor(R.color.lightgray));

                    break;
                case R.id.llvaginaldischargeid:
                    opencloseAccordion(aq.id(R.id.llvaginaldischargedetails).getView(), aq.id(R.id.txtvaginaldischarge).getTextView());
                    aq.id(R.id.llvaginaldischargedetails).backgroundColor(getResources().getColor(R.color.lightgray));
                    break;
                case R.id.llwateryfluidsid:
                    //  opencloseAccordion(aq.id(R.id.llwateryfluidsdetails).getView(), aq.id(R.id.txtwateryfluids).getTextView());
                    //15Oct2019 - Bindu - Change options
                    opencloseAccordionCheckbox(aq.id(R.id.llwateryfluidsdetails).getView(), aq.id(R.id.chkwateryfluids).getCheckBox());
                    aq.id(R.id.llwateryfluidsdetails).backgroundColor(getResources().getColor(R.color.lightgray));
                    break;
                //15Oct2019 - Bindu - add leaking of watery fluids checkbox
                case R.id.chkleakingofwateryfluidspv: {
                    opencloseAccordionCheckbox(aq.id(R.id.llwateryfluidsdetails).getView(), aq.id(R.id.chkleakingofwateryfluidspv).getCheckBox());
                    aq.id(R.id.llwateryfluidsdetails).backgroundColor(getResources().getColor(R.color.lightgray));
                    break;
                }
                case R.id.llsevereheadacheid:
                    opencloseAccordion(aq.id(R.id.llsevereheadachedetails).getView(), aq.id(R.id.txtsevereheadache).getTextView());
                    aq.id(R.id.llsevereheadachedetails).backgroundColor(getResources().getColor(R.color.lightgray));

                    break;
                case R.id.llfitsid:
                    opencloseAccordion(aq.id(R.id.llfitsdetails).getView(), aq.id(R.id.txtfits).getTextView());
                    aq.id(R.id.llfitsdetails).backgroundColor(getResources().getColor(R.color.lightgray));

                    break;
//               16May2021 Bindu  add recent health and covid vaccine
                case R.id.llhealthdetid:
                    opencloseAccordion(aq.id(R.id.llhealthdetails).getView(), aq.id(R.id.txthealthdet).getTextView());
                    aq.id(R.id.llhealthdetails).backgroundColor(getResources().getColor(R.color.lightgray));
                    break;
                //               16May2021 Bindu
                case R.id.lladdphotosid:
                    opencloseAccordion(aq.id(R.id.lladdphotosdetails).getView(), aq.id(R.id.txtaddphotos).getTextView());
                    aq.id(R.id.lladdphotosdetails).backgroundColor(getResources().getColor(R.color.lightgray));
                    if (aq.id(R.id.lladdphotosdetails).getView().getVisibility()==View.VISIBLE){
                        AsyncImage asyncImage = new AsyncImage();
                        asyncImage.execute();
                    }
                    break;
                case R.id.llcovidvaccineid:
                    opencloseAccordion(aq.id(R.id.llcovidvaccinedetails).getView(), aq.id(R.id.txtcovidvaccine).getTextView());
                    aq.id(R.id.llcovidvaccinedetails).backgroundColor(getResources().getColor(R.color.lightgray));
                    break;

                case R.id.lllaborpaingt12hrsid:
                    // opencloseAccordion(aq.id(R.id.lllaborpaingt12hrsdetails).getView(), aq.id(R.id.txtlaborpaingt12hrs).getTextView());
                    opencloseAccordionCheckbox(aq.id(R.id.lllaborpaingt12hrsdetails).getView(), aq.id(R.id.chklaborpaingt12hrs).getCheckBox());

                    aq.id(R.id.lllaborpaingt12hrsdetails).backgroundColor(getResources().getColor(R.color.lightgray));

                    break;
                case R.id.llcontinuousabdominalpainid:
                    opencloseAccordionCheckbox(aq.id(R.id.llcontinuousabdominalpaindetails).getView(), aq.id(R.id.chkcontinuousabdominalpain).getCheckBox());
                    break;
                case R.id.llmedicalillnessid:
                    opencloseAccordion(aq.id(R.id.llmedicalillnessdetails).getView(), aq.id(R.id.txtmedicalillness).getTextView());
                    aq.id(R.id.llmedicalillnessdetails).backgroundColor(getResources().getColor(R.color.lightgray));
                    break;

                case R.id.chkprevcsorsurgery:
                    opencloseAccordionCheckbox(aq.id(R.id.llprevcsorsurgerydetails).getView(), aq.id(R.id.chkprevcsorsurgery).getCheckBox());
                    break;
                case R.id.chkcpd:
                    opencloseAccordionCheckbox(aq.id(R.id.llcpddetails).getView(), aq.id(R.id.chkcpd).getCheckBox());
                    break;
                case R.id.chkbadobh:
                    opencloseAccordionCheckbox(aq.id(R.id.llbadobhdetails).getView(), aq.id(R.id.chkbadobh).getCheckBox());
                    break;
                case R.id.chktwinsmultiplepregHV:
                    if (aq.id(R.id.chktwinsmultiplepregHV).isChecked()) {
                        aq.id(R.id.chktwinsmultiplepregHV).textColor(getResources().getColor(R.color.red));
                    } else {
                        aq.id(R.id.chktwinsmultiplepregHV).textColor(getResources().getColor(R.color.colorPrimaryDark));
                    }
                    break;
                case R.id.chkmalpresentation:
                    if (aq.id(R.id.chkmalpresentation).isChecked()) {
                        aq.id(R.id.chkmalpresentation).textColor(getResources().getColor(R.color.red));
                    } else {
                        aq.id(R.id.chkmalpresentation).textColor(getResources().getColor(R.color.colorPrimaryDark));
                    }
                    break;
                case R.id.chkiugrHV:
                    if (aq.id(R.id.chkiugrHV).isChecked()) {
                        aq.id(R.id.chkiugrHV).textColor(getResources().getColor(R.color.red));
                    } else {
                        aq.id(R.id.chkiugrHV).textColor(getResources().getColor(R.color.colorPrimaryDark));
                    }
                    break;
                case R.id.btnhomevisitdetails: {
                    aq.id(R.id.txtselectedbtnheading).text(getResources().getString(R.string.healthissues));
                    hidetabs();
                    aq.id(R.id.btnhomevisitdetails).getImageView().setBackgroundColor(getResources().getColor(R.color.gray));
                    aq.id(R.id.scr).visible();
                    aq.id(R.id.scrhomevisitsummary).gone();
                    aq.id(R.id.imgSummaryAddPhoto).gone();//Ramesh 1-6-2021
                    aq.id(R.id.btnsavehomevisit).getImageView().setImageResource(R.drawable.ic_arrow_forward);
                    isBtnSummaryClicked = false;
                    break;
                }

                case R.id.btnSummary: {

//        16May2021 Bindu check
                    if (validatecovidhealthissue())
                        displayBtnSummaryClickedAction();

                    break;
                }

                case R.id.chkvomitingduringfirst3months:

                case R.id.chkexcvomitingduringfirst3months:

                case R.id.chkexcvomitingafter3months: {
                    hideshowdetailsvomiting(aq.id(R.id.txtvomiting).getTextView());
                    if (aq.id(R.id.chkvomitingduringfirst3months).isChecked()) {
                        showVomittingDialog();
                    }
                    break;
                }

                case R.id.chkdrytongue:
                case R.id.chkfeeblepulse:
                case R.id.chkdrydryconjunctiva:
                case R.id.chkbplow:
                case R.id.chkdecurineoutput:
                    displayvomitingsigninterpretation();
                    break;
                case R.id.chkpalpitations: {
                    hideshowdetailspalpfatiguebreathlessness(aq.id(R.id.tvpalplbl).getTextView());
                    if (aq.id(R.id.chkpalpitations).isChecked() && !(aq.id(R.id.chkfastordiffbreathing).isChecked()))
                        showPalpfaitgueBreathlessnessDialog();
                    break;
                }
                case R.id.chkeasyfatiguability:
                    hideshowdetailspalpfatiguebreathlessness(aq.id(R.id.tvpalplbl).getTextView());
                    if (aq.id(R.id.chkeasyfatiguability).isChecked() && !(aq.id(R.id.chkfastordiffbreathing).isChecked()))
                        showPalpfaitgueBreathlessnessDialog();
                    break;

                case R.id.chkbreathlessnessatrest:
                    hideshowdetailspalpfatiguebreathlessness(aq.id(R.id.tvpalplbl).getTextView());
                    if (aq.id(R.id.chkbreathlessnessatrest).isChecked() && !(aq.id(R.id.chkfastordiffbreathing).isChecked()))
                        showPalpfaitgueBreathlessnessDialog();
                    break;

                case R.id.chkpuffinessofface: {
                    hideshowdetailsedema(aq.id(R.id.txtedema).getTextView());
                    if (aq.id(R.id.chkpuffinessofface).isChecked() && (!(aq.id(R.id.chksevereheadache).isChecked() && aq.id(R.id.chkblurredvision).isChecked())))
                        showEdemaDialog();
                }
                break;

                case R.id.chkgenbodyoedema: {
                    hideshowdetailsedema(aq.id(R.id.txtedema).getTextView());
                    if (aq.id(R.id.chkgenbodyoedema).isChecked() && (!(aq.id(R.id.chksevereheadache).isChecked() && aq.id(R.id.chkblurredvision).isChecked())))
                        showEdemaDialog();
                    break;
                }

            /*case R.id.chkgenbodyoedema:
                hideshowdetailsedema(aq.id(R.id.txtedema).getTextView());
                break;*/

                case R.id.chkheartburn:

                case R.id.chknausea:
                    hideshowdetailsheartburnnausea(aq.id(R.id.txtheartburnandnausea).getTextView());
                    break;

                case R.id.chkincreasedurination:
                    //  hideshowdetailsurinationpbm(aq.id(R.id.txtfreqofurination).getTextView());
                /*if (aq.id(R.id.chkincreasedurination).isChecked() && !(aq.id(R.id.chkpaininabdomen).isChecked() && aq.id(R.id.chkfever).isChecked()) )
                    showUrinationProblemDialog();*/
                    hideshowdetailsfeverandothersymtoms(aq.id(R.id.txtfever).getTextView());
                    break;
                case R.id.chkurinationafter12wks:
                    //  hideshowdetailsurinationpbm(aq.id(R.id.txtfreqofurination).getTextView() );
                /*if (aq.id(R.id.chkurinationafter12wks).isChecked() && !(aq.id(R.id.chkpaininabdomen).isChecked() && aq.id(R.id.chkfever).isChecked()))
                    showUrinationProblemDialog();*/
                    hideshowdetailsfeverandothersymtoms(aq.id(R.id.txtfever).getTextView());
                    break;
                case R.id.chkburningurination:
                    //  hideshowdetailsurinationpbm(aq.id(R.id.txtfreqofurination).getTextView());
               /* if (aq.id(R.id.chkburningurination).isChecked() && !(aq.id(R.id.chkpaininabdomen).isChecked() && aq.id(R.id.chkfever).isChecked()))
                    showUrinationProblemDialog();*/
                    hideshowdetailsfeverandothersymtoms(aq.id(R.id.txtfever).getTextView());
                    break;
                case R.id.chkfever:
                /*if(aq.id(R.id.chkfever).isChecked())
                    aq.id(R.id.rdgfeveru).visible();
                else {
                    aq.id(R.id.rdgfeveru).gone();
                    aq.id(R.id.rdmildfeveru).checked(false);
                    aq.id(R.id.rdhighfeveru).checked(false);
                    aq.id(R.id.rdmoderatefeveru).checked(false);
                }
                hideshowdetailsurinationpbm(aq.id(R.id.txtfreqofurination).getTextView());*/
                    if (aq.id(R.id.chkfever).isChecked())
                        showFeverandOtherSymptomsDialog();
                    hideshowdetailsfeverandothersymtoms(aq.id(R.id.txtfever).getTextView());
                    break;
                case R.id.chkpaininabdomen:
                /*boolean ispaininabdomen = aq.id(R.id.chkpaininabdomen).isChecked();
                aq.id(R.id.chkfeverandabdominalpain).checked(ispaininabdomen);
                hideshowdetailsurinationpbm(aq.id(R.id.txtfreqofurination).getTextView());*/
                    hideshowdetailsfeverandothersymtoms(aq.id(R.id.txtfever).getTextView());
                    break;
                case R.id.chkcough:
                case R.id.chkcold:
                case R.id.chkjaundice:
                case R.id.chkwhitedischarge:
                case R.id.chkothersymptoms:
                    hideshowdetailsfeverandothersymtoms(aq.id(R.id.txtfever).getTextView());
                    break;

                case R.id.chkconstipation:
                case R.id.chkconstipationnotrelieved:
                    hideshowdetailsconstipation(aq.id(R.id.txtconstipation).getTextView());
                    break;

                case R.id.chkbleedingpvbefore20wks:

                case R.id.chkbleedingpvafter20wks:
                    hideshowdetailsbleedingpv(aq.id(R.id.txtbleedingpv).getTextView());
                    break;


            /*case R.id.chkfeverandabdominalpain:
                hideshowdetailsfever(aq.id(R.id.txtfever).getTextView());
                if(aq.id(R.id.chkfeverandabdominalpain).isChecked()) {
                    aq.id(R.id.chkpaininabdomen).checked(true);
                    aq.id(R.id.chkpaininabdomen).enabled(false);
                }
                else {
                    aq.id(R.id.chkpaininabdomen).checked(false);
                    aq.id(R.id.chkpaininabdomen).enabled(true);
                }
                break;*/
                case R.id.chkheartdiseaseHV:
                case R.id.chkDiabetesHV:
                case R.id.chkAsthamaHV:
                case R.id.chkTBHV:
                case R.id.chkneurologicalHv:
                case R.id.chkpsychiatricHv:
                case R.id.chkRenalHv:
                case R.id.chkCancerHv:
                case R.id.chkothermedicalillness:
                    hideshowdetailsmedicalillness(aq.id(R.id.txtmedicalillness).getTextView());
                    break;
                case R.id.chkcontinuousabdominalpain:
                    //hideshowdetailsabdominalpain(aq.id(R.id.txtcontinuousabdominalpain).getTextView());
                    opencloseAccordionCheckbox(aq.id(R.id.llcontinuousabdominalpaindetails).getView(), aq.id(R.id.chkcontinuousabdominalpain).getCheckBox());
                    break;
                case R.id.chklaborpaingt12hrs:
                    //   hideshowdetailslaborpain(aq.id(R.id.txtlaborpaingt12hrs).getTextView());
                    opencloseAccordionCheckbox(aq.id(R.id.lllaborpaingt12hrsdetails).getView(), aq.id(R.id.chklaborpaingt12hrs).getCheckBox());

                    break;
                case R.id.chkfits:
                    hideshowdetailsfitsorlossofconsciousness(aq.id(R.id.txtfits).getTextView());
                    break;
//                    16May2021 Bindu add covid health and covid vaccine
                case R.id.rd_healthissuesYes:
                    hideshowdetailsanyhealthissuesymtpoms(aq.id(R.id.txthealthdet).getTextView());
                    break;
                case R.id.rd_healthissuesNo:
                    hideshowdetailsanyhealthissuesymtpoms(aq.id(R.id.txthealthdet).getTextView());
                    break;
                case R.id.rd_CovidTestYes:
                    hideshowcovidTestdetails();
                    break;
                case R.id.rd_CovidTestNo:
                    hideshowcovidTestdetails();
                    break;
                case R.id.rd_CovidVaccineYes:
                    hideshowcovidvaccinedetails(aq.id(R.id.txtcovidvaccine).getTextView());
                    break;
                case R.id.rd_CovidVaccineNo:
                    hideshowcovidvaccinedetails(aq.id(R.id.txtcovidvaccine).getTextView());
                    break;
                case R.id.chksevereheadache:
                /*if(aq.id(R.id.chksevereheadache).isChecked()) {
                    if(!isSevereheadachesymChecked)
                        aq.id(R.id.chksevereheadachesymp).checked(true);
                    else
                        aq.id(R.id.chksevereheadachesymp).checked(false);
                }*/
                    boolean chksevheadache = aq.id(R.id.chksevereheadache).isChecked();
                    aq.id(R.id.chksevereheadachesymp).checked(chksevheadache);
                    hideshowdetailssevereheadachewithblurredvision(aq.id(R.id.txtsevereheadache).getTextView());
                    break;
                case R.id.chkblurredvision:
                    boolean chkblur = aq.id(R.id.chkblurredvision).isChecked();
                    aq.id(R.id.chkblurredvisionsymp).checked(chkblur);
                    hideshowdetailssevereheadachewithblurredvision(aq.id(R.id.txtsevereheadache).getTextView());
                    break;
                case R.id.chksevereheadachesymp:
                /*if(aq.id(R.id.chksevereheadachesymp).isChecked())
                    isSevereheadachesymChecked = true;
                else
                    isSevereheadachesymChecked = false;*/
                    hideshowdetailssevereheadachewithblurredvision(aq.id(R.id.txtsevereheadache).getTextView());
                    break;
                case R.id.chkblurredvisionsymp:
                    hideshowdetailssevereheadachewithblurredvision(aq.id(R.id.txtsevereheadache).getTextView());
                    break;
                case R.id.chkwateryfluids:
                    hideshowdetailswateryfluids(aq.id(R.id.txtwateryfluids).getTextView());
                    break;
                case R.id.chkpads:
                    hideshowdetailswateryfluids(aq.id(R.id.txtwateryfluids).getTextView());
                    break;
                case R.id.chkvaginaldischarge:
                    hideshowdetailsvaginaldischarge(aq.id(R.id.txtvaginaldischarge).getTextView());
                    break;
                case R.id.rdfoetalmovementdecreased:
                    hideshowdetailsfoetalmovement(aq.id(R.id.txtfoetalmovement).getTextView());
                    break;
                case R.id.rdfoetalmovementabsent:
                    hideshowdetailsfoetalmovement(aq.id(R.id.txtfoetalmovement).getTextView());
                    break;
                case R.id.rdfoetalmovementpresent:
                    hideshowdetailsfoetalmovement(aq.id(R.id.txtfoetalmovement).getTextView());
                    break;
                case R.id.btnsavehomevisit: {
                    //        16May2021 Bindu check covid healthissue
                    if (validatecovidhealthissue()) {
                        if (isBtnSummaryClicked) {
                            if (validateHomeVisit())
                                confirmAlert();
                        } else
                            displayBtnSummaryClickedAction();
                    }
                    break;
                }
                case R.id.btnviewhomevisit: {
                   // if (visitNum > 1) {
                        Intent homevisitlist = new Intent(HomeVisit.this, HomeVisitListActivity.class);
                        homevisitlist.putExtra("woman", woman);
                        homevisitlist.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        displayAlert(getResources().getString(R.string.exit), homevisitlist);
                        // startActivity(homevisitlist);
                    /*} else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.norecords), Toast.LENGTH_SHORT).show();
                    }*/
                    break;
                }
                case R.id.chkisreferred: {
                    showreferaldetails();
                    break;
                }
                //20Sep2019 - Bindu - clear dates
                case R.id.imgcleardatebp: {
                    clearDate(aq.id(R.id.etbpdonedate).getEditText(), aq.id(R.id.imgcleardatebp).getImageView());
                    break;
                }
                case R.id.imgcleardatepulse: {
                    clearDate(aq.id(R.id.etpulsedate).getEditText(), aq.id(R.id.imgcleardatepulse).getImageView());
                    break;
                }
                case R.id.imgcleardatehb: {
                    clearDate(aq.id(R.id.ethbdonedate).getEditText(), aq.id(R.id.imgcleardatehb).getImageView());
                    break;
                }
                case R.id.imgcleardatetemp: {
                    clearDate(aq.id(R.id.ettempdonedate).getEditText(), aq.id(R.id.imgcleardatetemp).getImageView());
                    break;
                }
                case R.id.imgcleardateproteinuria: {
                    clearDate(aq.id(R.id.etproteinuriadonedate).getEditText(), aq.id(R.id.imgcleardateproteinuria).getImageView());
                    break;
                }
                case R.id.imgcleardateweight: {
                    clearDate(aq.id(R.id.etweightdate).getEditText(), aq.id(R.id.imgcleardateweight).getImageView());
                    break;
                }
//                16May2021 Bindu
                case R.id.imgclearcovidresultdate: {
                    clearDate(aq.id(R.id.etVisitCovidTestDate).getEditText(), aq.id(R.id.imgclearcovidresultdate).getImageView());
                    break;
                }
                case R.id.imgclearfirstdosedate: {
                    clearDate(aq.id(R.id.etVisitCovidFirstDoseDate).getEditText(), aq.id(R.id.imgclearfirstdosedate).getImageView());
                    break;
                }
                case R.id.imgclearsecdosedate: {
                    clearDate(aq.id(R.id.etVisitCovidSecondDoseDate).getEditText(), aq.id(R.id.imgclearsecdosedate).getImageView());
                    break;
                }
                case R.id.chkparacetamol:
                case R.id.chkalbedazolegiven:
                case R.id.chkifa:
                case R.id.chkfsfa:
                    displayTabletDetailstable();
                    break;

                case R.id.rdproteinuriaabsent:
                    if (aq.id(R.id.rdproteinuriaabsent).isChecked())
                        aq.id(R.id.rdproteinuriaabsentsumm).checked(true);
                    else
                        aq.id(R.id.rdproteinuriaabsentsumm).checked(false);
                    enabledisableproteinuriadatefields();
                    break;
                case R.id.rdproteinuriapresent:
                    if (aq.id(R.id.rdproteinuriapresent).isChecked())
                        aq.id(R.id.rdproteinuriapresentsumm).checked(true);
                    else
                        aq.id(R.id.rdproteinuriapresentsumm).checked(false);
                    enabledisableproteinuriadatefields();
                    break;
                case R.id.rdproteinuriaabsentsumm:
                case R.id.rdproteinuriapresentsumm:
                    enabledisableproteinuriadatefields();
                    break;

            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }

    }

    private void callCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            try {
                File photoFile = null;
                photoFile = createImagePath();
                // Continue only if the File was successfully created
                if (photoFile != null) {
                    Uri photoURI = FileProvider.getUriForFile(
                            getApplicationContext(),
                            getApplicationContext()
                                    .getPackageName() + ".provider", photoFile);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private File createImagePath() {
        try {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = "JPEG_" + timeStamp + "_";
            File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            File image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );
            mCurrentPhotoPath = image.getAbsolutePath();
            return image;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void disableFeverSigns() {
        /*aq.id(R.id.chkfever).enabled(false);
        aq.id(R.id.rdmildfeveru).enabled(false);
        aq.id(R.id.rdhighfeveru).enabled(false);
        aq.id(R.id.rdmoderatefeveru).enabled(false);*/
    }

    private void enableFeverSigns() {
        /*aq.id(R.id.chkfever).enabled(true);
        aq.id(R.id.rdmildfeveru).enabled(true);
        aq.id(R.id.rdhighfeveru).enabled(true);
        aq.id(R.id.rdmoderatefeveru).enabled(true);*/
    }

    // Show Referral details
    private void showreferaldetails() throws Exception {
        if (aq.id(R.id.chkisreferred).isChecked()) {
            aq.id(R.id.trspnfacname).visible();
            aq.id(R.id.tretfacname).gone();
            //08Apr2021 Bindu
            aq.id(R.id.trspnfactype).visible();
            aq.id(R.id.trrefslipno).visible();
            setFacilityType();
        } else {
            aq.id(R.id.spnfacnameHv).setSelection(0);
            aq.id(R.id.trspnfacname).gone();
            aq.id(R.id.tretfacname).gone();
            aq.id(R.id.etreffacilityname).text("");
            //08Apr2021 Bindu
            aq.id(R.id.spnfactype).setSelection(0);
            aq.id(R.id.trspnfactype).gone();
            aq.id(R.id.trrefslipno).gone();
            aq.id(R.id.etrefslipnumber).text("");
        }
    }

    // Button summary click action
    private void displayBtnSummaryClickedAction() {
        try {
            aq.id(R.id.txtselectedbtnheading).text(getResources().getString(R.string.homevisitsummary));
            hidetabs();
            aq.id(R.id.btnSummary).getImageView().setBackgroundColor(getResources().getColor(R.color.gray));
            aq.id(R.id.scr).gone();
            aq.id(R.id.scrhomevisitsummary).visible();
            aq.id(R.id.imgSummaryAddPhoto).visible();
            aq.id(R.id.btnsavehomevisit).getImageView().setImageResource(R.drawable.save);
            isBtnSummaryClicked = true;
            setSymtpomsSummary();

            if ((woman.getDateDeactivated() != null &&
                    woman.getDateDeactivated().trim().length() > 0)
                    || user.getIsDeactivated() == 1) {
                disableScreen();//26Nov2019 Arpitha

            }
        } catch (Exception e) {

        }
    }

    private void hideshowdetailsmedicalillness(TextView txt) {
        if (aq.id(R.id.chkDiabetesHV).isChecked() || aq.id(R.id.chkheartdiseaseHV).isChecked() || aq.id(R.id.chkAsthamaHV).isChecked() || aq.id(R.id.chkTBHV).isChecked() || aq.id(R.id.chkothermedicalillness).isChecked()
                || aq.id(R.id.chkneurologicalHv).isChecked() || aq.id(R.id.chkpsychiatricHv).isChecked() || aq.id(R.id.chkRenalHv).isChecked() || aq.id(R.id.chkCancerHv).isChecked()) {
            aq.id(R.id.llmedicalillnesssumm).visible();
            if (aq.id(R.id.chkothermedicalillness).isChecked()) {
                aq.id(R.id.etothermedicalillness).visible();
            } else {
                aq.id(R.id.etothermedicalillness).gone();
                aq.id(R.id.etothermedicalillness).text("");
            }
            txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
            txt.setTextColor(getResources().getColor(R.color.red));
        } else {
            if (!(aq.id(R.id.chkothermedicalillness).isChecked())) {
                aq.id(R.id.etothermedicalillness).gone();
                aq.id(R.id.etothermedicalillness).text("");
            }
            aq.id(R.id.llmedicalillnesssumm).gone();
            txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
            txt.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        }
    }

    // Save Data
    private void savedata() {
        try {

            tblVisHeaderDao = databaseHelper.getTblVisitHeaderDao();
            tblVisDetailDao = databaseHelper.getTblVisitDetailDao();

//            16May2021 Bindu covid data
            tblCovidTestDao = databaseHelper.getTblCovidTestDao();
            tblCovidVaccineDao = databaseHelper.getTblCovidVaccineDao();

            tblImageStoreDao = databaseHelper.getTblImageStoreDao();

            prepareSignsandInvestigationData();
            prepareVisitHeaderData();
            prepareVisitDetailsData();

//            16May2021 Bindu
            if (aq.id(R.id.rd_CovidTestYes).isChecked() || aq.id(R.id.rd_CovidTestNo).isChecked()) //23May2021 Bindu put cond
                prepareCovidTestdata();
            prepareCovidVacinedata();

            // newdb = OpenHelperManager.getHelper(this, DBHelper.class);

            final TransactionHeaderRepository transRepo = new TransactionHeaderRepository(databaseHelper);
            transId = transRepo.iCreateNewTrans(appState.sessionUserId, databaseHelper);
            tblvisH.setTransid(transId);
            tblvisD.setTransid(transId);
//16May2021 Bindu
            if (tblcovtest != null) //23May2021 Bindu
                tblcovtest.setTransId(transId);
            tblcovvac.setTransId(transId);

            prepareAuditTrail();

            TransactionManager.callInTransaction(databaseHelper.getConnectionSource(), new Callable<Void>() {
                public Void call() throws Exception {

                    int add;
                    boolean added;
                    String stradd = "";
                    add = tblVisHeaderDao.create(tblvisH);

                    if (add > 0) {
                        added = transRepo.iNewRecordTransNew(appState.sessionUserId, transId,
                                tblVisHeaderDao.getTableName(), databaseHelper);

                        if (added) {
                            // if(arrLSymptoms.size() > 0) {

                            if (insertSignsInvestVal != null && insertSignsInvestVal.size() > 0) {
                                for (Map.Entry<String, ArrayList<String>> entry : insertSignsInvestVal.entrySet()) {
                                    tblvisD.setVisDSymptoms(entry.getKey());
                                    tblvisD.setVisDSignsInvestigations(entry.getValue().toString().replaceAll("\\[|\\]", ""));
                                    tblvisD.setVisDSignsInvestInterpretation(insertSignsInterpretationVal.get(entry.getKey()));
                                    tblvisD.setVisDDiagnosis(insertDiagnosisVal.get(entry.getKey()));
                                    tblvisD.setVisDReferredToFacilityType(insertSymRefVal.get(entry.getKey()));
                                    if (entry.getKey().equals("141") && aq.id(R.id.etothermedicalillness).getText().toString().length() > 0) {
                                        tblvisD.setVisDOtherSymptoms(aq.id(R.id.etothermedicalillness).getText().toString());
                                    } else if (entry.getKey().equals("149") && aq.id(R.id.etothersymptoms).getText().toString().length() > 0) {
                                        tblvisD.setVisDOtherSymptoms(aq.id(R.id.etothersymptoms).getText().toString());
                                    } else
                                        tblvisD.setVisDOtherSymptoms("");
                                    add = tblVisDetailDao.create(tblvisD);

                                }
                                if (add > 0) {
                                    added = transRepo.iNewRecordTransNew(appState.sessionUserId, transId,
                                            tblVisDetailDao.getTableName(), databaseHelper);
                                }
                            }/*else if(arrLSymptomsId.size() > 0) {
                                    for (String strSym : arrLSymptomsId) {
                                        tblvisD.setVisDSymptoms(strSym);

                                        add = tblVisDetailDao.create(tblvisD);
                                        if(add > 0)
                                            added = DBMethods.iNewRecordTransSram(TblInstusers.getUserId(), transId, tblVisDetailDao.getTableName(), newdb);
                                    }
                                }*/
                            // }
                        }

                        if (added) {
                            HomeVisitRepository homeRepo = new HomeVisitRepository(databaseHelper);
                            int update = homeRepo.updateVisitDetailsWomanInReg(woman.getWomanId(), woman.getUserId(), hmvisitregdetails, transId, databaseHelper);

                            added = transRepo.iNewRecordTransNew(appState.sessionUserId, transId,
                                   "tblaudittrail", databaseHelper);//Arpitha 24July2021

                            if (update > 0) {

//                                16May2021 Bindu - add cov test details
                                if (tblcovtest != null) { //23May2021 Bindu
                                    add = tblCovidTestDao.create(tblcovtest);
                                    if (add > 0) {
                                        added = transRepo.iNewRecordTransNew(appState.sessionUserId, transId,
                                                tblCovidTestDao.getTableName(), databaseHelper);
                                    }
                                }

                                if (AllImagesBeforeSave.size()!=0 && AllImagePojo.size()!=0){
                                    for (tblImageStore s: AllImagesBeforeSave){
                                        s.setTransId(transId);
                                        s.setImagePath("");
                                        add = tblImageStoreDao.create(s);
                                    }
                                    if (add>0){
                                        added = transRepo.iNewRecordTransNew(appState.sessionUserId, transId,
                                                tblImageStoreDao.getTableName(), databaseHelper);
                                    }
                                    for (ImagePojo s:AllImagePojo){
                                        fileUpload();
                                        FileOutputStream fileOutputStream = new FileOutputStream(s.getImagePath());
                                        fileOutputStream.write(s.getImage());
                                    }
                                }

                                if (added) {

//                                        if(!isvaccinated) {
                                    if (insertCovidVaccineVal != null && insertCovidVaccineVal.size() > 0) {
                                        for (Map.Entry<String, String> entry : insertCovidVaccineVal.entrySet()) {
                                            tblcovvac.setCovidVaccinatedNo(entry.getKey());
                                            tblcovvac.setCovidVaccinatedDate(entry.getValue());
                                            add = tblCovidVaccineDao.create(tblcovvac);
                                        }
                                        if (add > 0) {
                                            added = transRepo.iNewRecordTransNew(appState.sessionUserId, transId,
                                                    tblCovidVaccineDao.getTableName(), databaseHelper);
                                        }

                                    }
                                        /*else {
                                            String upSql = checkForAuditTrail(transId);
                                            if (upSql != null) {
                                                transRepo.iNewRecordTransNew(appState.sessionUserId, transId, "tblaudittrail", databaseHelper);

                                                CovidDetailsRepository covrepo = new CovidDetailsRepository(databaseHelper);
                                                add = covrepo.updateCovidVaccine(woman.getUserId(), upSql,transId, databaseHelper);
                                            }
                                        }*/
                                    if (added) {

                                           /* Intent intent = new Intent(HomeVisit.this, HomeVisitListActivity.class);
                                            intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                            intent.putExtra("woman", woman);
                                            startActivity(intent);21May2021 Arpitha*/

                                        if (tblvisH.getVisHCompl().equalsIgnoreCase("Y")) { //If compl, then send sms
                                            //04Dec2019 - Bindu
                                            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(HomeVisit.this);

                                            //mani 15 april 2021 uncommented code
                                            if (checkSimState() == TelephonyManager.SIM_STATE_READY &&
                                                    prefs.getBoolean("sms", false)) {
                                                sendSMS(transRepo, databaseHelper, tblvisH);
                                            } else//21May2021 Arpitha
                                            {
                                                Intent intent = new Intent(HomeVisit.this, HomeVisitListActivity.class);
                                                intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                                intent.putExtra("woman", woman);
                                                startActivity(intent);
                                            }
                                        } else//21May2021 Arpitha
                                        {
                                            Intent intent = new Intent(HomeVisit.this, HomeVisitListActivity.class);
                                            intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                            intent.putExtra("woman", woman);
                                            startActivity(intent);
                                        }
                                    } else {
                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.failedtoaddhomevisit), Toast.LENGTH_LONG).show();
                                    } // covid vaccine  details
                                } else {
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.failedtoaddhomevisit), Toast.LENGTH_LONG).show();
                                }
                                /*}else {
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.failedtoaddhomevisit), Toast.LENGTH_LONG).show();
                                } // covid health issue details //23May2021*/
                            } else {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.failedtoaddhomevisit), Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                    return null;
                }
            });

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private void fileUpload() {
        try {
            AsyncFileUpload asyncFileUpload = new AsyncFileUpload();
            asyncFileUpload.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void prepareCovidVacinedata() throws Exception {
        tblcovvac = new tblCovidVaccineDetails();
        insertCovidVaccineVal = new LinkedHashMap<>();
        tblcovvac.setUserId(user.getUserId());
        tblcovvac.setBeneficiaryId(woman.getWomanId());
        tblcovvac.setBeneficiaryType("PW"); //20May2021 Bindu
//            tblcovvac.setCovidVaccinated(aq.id(R.id.rd_CovidVaccineYes).isChecked() ? getResources().getString(R.string.yesshortform) : getResources().getString(R.string.noshortform));
//            tblcovvac.setCovidFirstDoseDate(aq.id(R.id.etVisitCovidFirstDoseDate).getText().toString());
//            tblcovvac.setCovidSecondDoseDate(aq.id(R.id.etVisitCovidSecondDoseDate).getText().toString());
        tblcovvac.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
        tblcovvac.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());

        if (aq.id(R.id.rd_CovidVaccineYes).isChecked() && covVacItems != null && covVacItems.size() < 3) { // less than 2 doses only
            /*if (covVacItems != null && covVacItems.size() <= 0) {
                if (aq.id(R.id.etVisitCovidFirstDoseDate).getText().toString().trim().length() > 0)
                    insertCovidVaccineVal.put("1", aq.id(R.id.etVisitCovidFirstDoseDate).getText().toString());
                if (aq.id(R.id.etVisitCovidSecondDoseDate).getText().toString().trim().length() > 0)
                    insertCovidVaccineVal.put("2", aq.id(R.id.etVisitCovidSecondDoseDate).getText().toString());
            } else if (covVacItems != null && covVacItems.size() == 1) {
                if (aq.id(R.id.etVisitCovidSecondDoseDate).getText().toString().trim().length() > 0)
                    insertCovidVaccineVal.put("2", aq.id(R.id.etVisitCovidSecondDoseDate).getText().toString());
            }*/
            if (covVacItems != null && covVacItems.size() == 0) {
                if (aq.id(R.id.etVisitCovidFirstDoseDate).getText().toString().trim().length() > 0)
                    insertCovidVaccineVal.put("1", aq.id(R.id.etVisitCovidFirstDoseDate).getText().toString());
                if (aq.id(R.id.etVisitCovidSecondDoseDate).getText().toString().trim().length() > 0)
                    insertCovidVaccineVal.put("2", aq.id(R.id.etVisitCovidSecondDoseDate).getText().toString());
            }
            if (covVacItems != null && covVacItems.size() == 1) {
                if (aq.id(R.id.etVisitCovidSecondDoseDate).getText().toString().trim().length() > 0)
                    insertCovidVaccineVal.put("2", aq.id(R.id.etVisitCovidSecondDoseDate).getText().toString());
            }
        }/*else {
            if (aq.id(R.id.etVisitCovidFirstDoseDate).getText().toString().trim().length() > 0)
                insertCovidVaccineVal.put("1", aq.id(R.id.etVisitCovidFirstDoseDate).getText().toString());
            if (aq.id(R.id.etVisitCovidSecondDoseDate).getText().toString().trim().length() > 0)
                insertCovidVaccineVal.put("2", aq.id(R.id.etVisitCovidSecondDoseDate).getText().toString());
        }*/
    }

    private void prepareCovidTestdata() throws Exception {
        tblcovtest = new tblCovidTestDetails();
        tblcovtest.setUserId(user.getUserId());
        tblcovtest.setWomenId(woman.getWomanId());
        tblcovtest.setBeneficiaryType("PW");
        tblcovtest.setVisitNum("" + visitNum);
//        tblcovtest.setHealthIssues(spnHealthProblemshv.getSelectedItemsAsString());
//        18May2021 Bindu
        String healthproblems = "";
        if (aq.id(R.id.rd_healthissuesYes).isChecked()) { //23May2021 Bindu change cond
            healthproblems = healthproblems + spnHealthProblemshv.getSelectedIndicies().toString();
            healthproblems = healthproblems.replace("[", "");
            healthproblems = healthproblems.replace("]", "");
            tblcovtest.setHealthIssues(healthproblems);
        }
        if (spnHealthProblemshv.getSelectedItemsAsString().contains(getResources().getString(R.string.bothers))) {
            tblcovtest.setHealthIssuesOthers(aq.id(R.id.etVisitHealthIssuesYesOthers).getEditText().getText().toString());
        } else {
            tblcovtest.setHealthIssuesOthers("");
        }
        // tblcovtest.setHealthIssuesOthers(aq.id(R.id.etVisitHealthIssuesYesOthers).getEditText().getText().toString());
        tblcovtest.setCovidTest(aq.id(R.id.rd_CovidTestYes).isChecked() ? getResources().getString(R.string.yesshortform) : getResources().getString(R.string.noshortform));
        tblcovtest.setCovidResult(aq.id(R.id.rd_CovidPositive).isChecked() ? getResources().getString(R.string.positivetxt) : getResources().getString(R.string.negativetxt));
        tblcovtest.setCovidResultDate(aq.id(R.id.etVisitCovidTestDate).getText().toString());
        tblcovtest.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
        tblcovtest.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());

    }

    private void prepareAuditTrail() throws Exception {

        if (!(hmvisitregdetails.getIsCompl() != null && hmvisitregdetails.getIsCompl().equalsIgnoreCase(woman.getIsCompl()))) {
            InserttblAuditTrail("isCompl", woman.getIsCompl(), hmvisitregdetails.getIsCompl(), transId, null);
        }
        if (!(hmvisitregdetails.getLastAncVisitDate() != null && hmvisitregdetails.getLastAncVisitDate().equalsIgnoreCase(woman.getLastAncVisitDate()))) {
            InserttblAuditTrail("LastAncVisitDate", woman.getLastAncVisitDate(), hmvisitregdetails.getLastAncVisitDate(), transId, null);
        }
        if (!(hmvisitregdetails.getLastPncVisitDate() != null && hmvisitregdetails.getLastPncVisitDate().equalsIgnoreCase(woman.getLastPncVisitDate()))) {
            InserttblAuditTrail("LastPncVisitDate", woman.getLastPncVisitDate(), hmvisitregdetails.getLastPncVisitDate(), transId, null);
        }
        if (!(hmvisitregdetails.getIsReferred() != null && hmvisitregdetails.getIsReferred().equalsIgnoreCase(woman.getIsReferred()))) {
            InserttblAuditTrail("isReferred", woman.getIsReferred(), hmvisitregdetails.getIsReferred(), transId, null);
        }
        if (woman.getRegUserType() != null && !woman.getRegUserType().equalsIgnoreCase(appState.userType))
            InserttblAuditTrail("regUserType", woman.getRegUserType(), appState.userType, transId, null);

        if (!(woman.getRegrecommendedPlaceOfDelivery() != null && hmvisitregdetails.getRegrecommendedPlaceOfDelivery().equalsIgnoreCase(woman.getRegrecommendedPlaceOfDelivery()))) {
            InserttblAuditTrail("regrecommendedPlaceOfDelivery", woman.getRegrecommendedPlaceOfDelivery(), hmvisitregdetails.getRegrecommendedPlaceOfDelivery(), transId, null);
        }

        InserttblAuditTrail("RecordUpdatedDate", woman.getRecordUpdatedDate(), hmvisitregdetails.getRecordUpdatedDate(), transId, null);

    }

    private void hideshowdetailsfeverandothersymtoms(TextView txt) {
        calculateGA(aq.id(R.id.etvisitdate).getText().toString());
        boolean isFeverorOtherOptionSel = false;
        isRef = false;
        isRefImm = false;

        if (aq.id(R.id.chkfever).isChecked() || aq.id(R.id.chkpaininabdomen).isChecked() || aq.id(R.id.chkincreasedurination).isChecked() || aq.id(R.id.chkurinationafter12wks).isChecked() || aq.id(R.id.chkburningurination).isChecked()
                || aq.id(R.id.chkcough).isChecked() || aq.id(R.id.chkcold).isChecked() || aq.id(R.id.chkjaundice).isChecked() || aq.id(R.id.chkwhitedischarge).isChecked() || aq.id(R.id.chkothersymptoms).isChecked()) {
            aq.id(R.id.llfeversumm).visible();
            txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
            txt.setTextColor(getResources().getColor(R.color.red));
            isFeverorOtherOptionSel = true;
            if (aq.id(R.id.chkothersymptoms).isChecked()) {
                aq.id(R.id.etothersymptoms).visible();
            } else {
                aq.id(R.id.etothersymptoms).gone();
                aq.id(R.id.etothersymptoms).text("");
            }
        } else {
            isFeverorOtherOptionSel = false;
            if (!(aq.id(R.id.chkothersymptoms).isChecked())) {
                aq.id(R.id.etothersymptoms).gone();
                aq.id(R.id.etothersymptoms).text("");
            }
        }

        aq.id(R.id.etTempf).enabled(false).background(R.drawable.edittext_disable);


        if (aq.id(R.id.chkfever).isChecked()) {

            aq.id(R.id.etTempf).enabled(true).background(R.drawable.edittext_style);
            aq.id(R.id.txtadvisegiveparacetamol).visible();
            aq.id(R.id.txtadviseliquidfluids).gone();

            //14Nov2019 - Bindu - savein eng
            if (aq.id(R.id.chkpaininabdomen).isChecked()) {
                aq.id(R.id.txtfeverdiagnosisval).text(getResources().getString(R.string.utiorsepsis));
                aq.id(R.id.txtfeveraction).text(getResources().getString(R.string.referimmediately));
                aq.id(R.id.txtfeverdiagnosisvalsave).text(getResources().getString(R.string.utiorsepsissave));
                aq.id(R.id.txtfeveractionsave).text(getResources().getString(R.string.referimmediatelysave));
                aq.id(R.id.txtfeveraction).visible();
                isRefImm = true;
            } else if (aq.id(R.id.chkincreasedurination).isChecked() || aq.id(R.id.chkurinationafter12wks).isChecked() || aq.id(R.id.chkburningurination).isChecked()) {
                aq.id(R.id.txtfeverdiagnosisval).text(getResources().getString(R.string.uti));
                aq.id(R.id.txtfeveraction).text(getResources().getString(R.string.referimmediately));
                aq.id(R.id.txtfeverdiagnosisvalsave).text(getResources().getString(R.string.utisave));
                aq.id(R.id.txtfeveractionsave).text(getResources().getString(R.string.referimmediatelysave));
                aq.id(R.id.txtfeveraction).visible();
                isRefImm = true;
            } else if ((!aq.id(R.id.chkpaininabdomen).isChecked()) && aq.id(R.id.chkcough).isChecked() || aq.id(R.id.chkcold).isChecked() || aq.id(R.id.chkjaundice).isChecked() || aq.id(R.id.chkwhitedischarge).isChecked() || aq.id(R.id.chkothersymptoms).isChecked()) {
                aq.id(R.id.txtfeverdiagnosisval).text("");
                aq.id(R.id.txtfeveraction).text(getResources().getString(R.string.refer));
                aq.id(R.id.txtfeverdiagnosisvalsave).text("");
                aq.id(R.id.txtfeveractionsave).text(getResources().getString(R.string.refersave));
                aq.id(R.id.txtfeveraction).visible();
                isRef = true;
            } else {
                aq.id(R.id.txtfeverdiagnosisval).text("");
                aq.id(R.id.txtfeveraction).text("");
                aq.id(R.id.txtfeverdiagnosisvalsave).text("");
                aq.id(R.id.txtfeveractionsave).text("");
                aq.id(R.id.txtfeveraction).gone();
                isRef = false;
                isRefImm = false;
            }

        } else if (!aq.id(R.id.chkfever).isChecked() && isFeverorOtherOptionSel) {
            aq.id(R.id.etTempf).text("");
            aq.id(R.id.etTempf).enabled(false).background(R.drawable.edittext_disable);
            aq.id(R.id.txtadvisegiveparacetamol).gone();

            if (aq.id(R.id.chkpaininabdomen).isChecked() || aq.id(R.id.chkincreasedurination).isChecked() || aq.id(R.id.chkurinationafter12wks).isChecked() || aq.id(R.id.chkburningurination).isChecked()) {
                aq.id(R.id.txtadviseliquidfluids).visible();
                aq.id(R.id.txtadviseliquidfluids).text(getResources().getString(R.string.plentyoforalfluids));
                aq.id(R.id.txtfeveraction).text(getResources().getString(R.string.refer));
                aq.id(R.id.txtfeveractionsave).text(getResources().getString(R.string.refersave));
                aq.id(R.id.txtfeveraction).visible();
                isRef = true;
            } else {
                aq.id(R.id.txtadviseliquidfluids).gone();
                aq.id(R.id.txtadviseliquidfluids).text("");
                if (aq.id(R.id.chkcough).isChecked() || aq.id(R.id.chkcold).isChecked() || aq.id(R.id.chkjaundice).isChecked() || aq.id(R.id.chkwhitedischarge).isChecked() || aq.id(R.id.chkothersymptoms).isChecked()) {
                    aq.id(R.id.txtfeverdiagnosisval).text("");
                    aq.id(R.id.txtfeveraction).text(getResources().getString(R.string.refer));
                    aq.id(R.id.txtfeverdiagnosisvalsave).text("");
                    aq.id(R.id.txtfeveractionsave).text(getResources().getString(R.string.refersave));
                    aq.id(R.id.txtfeveraction).visible();
                    isRef = true;
                }
            }

        } else {
            aq.id(R.id.llfeversumm).gone();
            txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
            txt.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
            unCheckFeverFields();
            aq.id(R.id.txtfeveraction).text("");
            aq.id(R.id.txtfeveraction).gone();
            aq.id(R.id.txtadviseliquidfluids).gone();
            aq.id(R.id.txtfeverdiagnosisval).text("");
            aq.id(R.id.txtadviseliquidfluids).text("");
            aq.id(R.id.txtfeverdiagnosisvalsave).text("");
            aq.id(R.id.txtfeveractionsave).text("");

            isRef = false;
            isRefImm = false;
        }
    }

    private void unCheckFeverFields() {
        aq.id(R.id.chkfever).checked(false);
        aq.id(R.id.chkpaininabdomen).checked(false);
        aq.id(R.id.chkcold).checked(false);
        aq.id(R.id.chkcough).checked(false);
        aq.id(R.id.chkjaundice).checked(false);
        aq.id(R.id.chkwhitedischarge).checked(false);
        aq.id(R.id.chkincreasedurination).checked(false);
        aq.id(R.id.chkburningurination).checked(false);
        aq.id(R.id.chkurinationafter12wks).checked(false);
        aq.id(R.id.chkothersymptoms).checked(false);
        aq.id(R.id.etTempf).text("");
        aq.id(R.id.etTempf).enabled(false).background(R.drawable.edittext_disable);
    }

    private void setFeverOtherSymtpoms() {
        //14Nov2019 - Bindu - eng save
        strfeverandother = "";
        strfeverandothersave = "";
        if (aq.id(R.id.chkfever).isChecked()) {
            strfeverandother = aq.id(R.id.chkfever).getText().toString();
            strfeverandothersave = getResources().getString(R.string.feversave);
        }

        if (aq.id(R.id.chkcough).isChecked()) {
            strfeverandother = strfeverandother.length() > 0 ? strfeverandother + ", " + aq.id(R.id.chkcough).getText().toString() : aq.id(R.id.chkcough).getText().toString();
            strfeverandothersave = strfeverandothersave.length() > 0 ? strfeverandothersave + ", " + getResources().getString(R.string.coughsave) : getResources().getString(R.string.coughsave);
        }
        if (aq.id(R.id.chkcold).isChecked()) {
            strfeverandother = strfeverandother.length() > 0 ? strfeverandother + ", " + aq.id(R.id.chkcold).getText().toString() : aq.id(R.id.chkcold).getText().toString();
            strfeverandothersave = strfeverandothersave.length() > 0 ? strfeverandothersave + ", " + getResources().getString(R.string.coldsave) : getResources().getString(R.string.coldsave);
        }
        if (aq.id(R.id.chkjaundice).isChecked()) {
            strfeverandother = strfeverandother.length() > 0 ? strfeverandother + ", " + aq.id(R.id.chkjaundice).getText().toString() : aq.id(R.id.chkjaundice).getText().toString();
            strfeverandothersave = strfeverandothersave.length() > 0 ? strfeverandothersave + ", " + getResources().getString(R.string.jaundicesave) : getResources().getString(R.string.jaundicesave);
        }
        if (aq.id(R.id.chkwhitedischarge).isChecked()) {
            strfeverandother = strfeverandother.length() > 0 ? strfeverandother + ", " + aq.id(R.id.chkwhitedischarge).getText().toString() : aq.id(R.id.chkwhitedischarge).getText().toString();
            strfeverandothersave = strfeverandothersave.length() > 0 ? strfeverandothersave + ", " + getResources().getString(R.string.whitedischargesave) : getResources().getString(R.string.whitedischargesave);
        }
        if (aq.id(R.id.chkpaininabdomen).isChecked()) {
            strfeverandother = strfeverandother.length() > 0 ? strfeverandother + ", " + aq.id(R.id.chkpaininabdomen).getText().toString() : aq.id(R.id.chkpaininabdomen).getText().toString();
            strfeverandothersave = strfeverandothersave.length() > 0 ? strfeverandothersave + ", " + getResources().getString(R.string.paininabdomensave) : getResources().getString(R.string.paininabdomensave);
        }
        if (aq.id(R.id.chkincreasedurination).isChecked()) {
            strfeverandother = strfeverandother.length() > 0 ? strfeverandother + ", " + aq.id(R.id.chkincreasedurination).getText().toString() : aq.id(R.id.chkincreasedurination).getText().toString();
            strfeverandothersave = strfeverandothersave.length() > 0 ? strfeverandothersave + ", " + getResources().getString(R.string.increasedfrequrinationsave) : getResources().getString(R.string.increasedfrequrinationsave);
        }
        if (aq.id(R.id.chkurinationafter12wks).isChecked()) {
            strfeverandother = strfeverandother.length() > 0 ? strfeverandother + ", " + aq.id(R.id.chkurinationafter12wks).getText().toString() : aq.id(R.id.chkurinationafter12wks).getText().toString();
            strfeverandothersave = strfeverandothersave.length() > 0 ? strfeverandothersave + ", " + getResources().getString(R.string.urinationafter12wkssave) : getResources().getString(R.string.urinationafter12wkssave);
        }
        if (aq.id(R.id.chkburningurination).isChecked()) {
            strfeverandother = strfeverandother.length() > 0 ? strfeverandother + ", " + aq.id(R.id.chkburningurination).getText().toString() : aq.id(R.id.chkburningurination).getText().toString();
            strfeverandothersave = strfeverandothersave.length() > 0 ? strfeverandothersave + ", " + getResources().getString(R.string.burningurinationsave) : getResources().getString(R.string.burningurinationsave);
        }
        if (aq.id(R.id.chkothersymptoms).isChecked()) {
            strfeverandother = strfeverandother.length() > 0 ? strfeverandother + ", " + aq.id(R.id.chkothersymptoms).getText().toString() : aq.id(R.id.chkothersymptoms).getText().toString();
            strfeverandothersave = strfeverandothersave.length() > 0 ? strfeverandothersave + ", " + getResources().getString(R.string.otherssave) : getResources().getString(R.string.otherssave);
            if (aq.id(R.id.etothersymptoms).getText().toString().trim().length() > 0) {
                strfeverandother = strfeverandother + " (" + aq.id(R.id.etothersymptoms).getText().toString() + ") ";
                strfeverandothersave = strfeverandothersave + " (" + aq.id(R.id.etothersymptoms).getText().toString() + ") ";
            }
        }
        if (strfeverandother.length() > 0)
            strfeverandother = aq.id(R.id.txtfeverdiagnosisval).getText().toString().length() > 0 ? strfeverandother + " - " + aq.id(R.id.txtfeverdiagnosisval).getText().toString() : strfeverandother;
        strfeverandothersave = aq.id(R.id.txtfeverdiagnosisvalsave).getText().toString().length() > 0 ? strfeverandothersave + " - " + aq.id(R.id.txtfeverdiagnosisvalsave).getText().toString() : strfeverandothersave;
    }

    private void hideshowdetailsbleedingpv(TextView txt) {

        /*if (aq.id(R.id.chkbleedingpvbefore20wks).isChecked()) {
            aq.id(R.id.llbleedingpvsumm).visible();
            aq.id(R.id.txtbleedingpvdiagnosisval).text(getResources().getString(R.string.bleedingpvabortiondiagnosis));
        } else if (aq.id(R.id.chkbleedingpvafter20wks).isChecked()) {
            aq.id(R.id.llbleedingpvsumm).visible();
            aq.id(R.id.txtbleedingpvdiagnosisval).text(getResources().getString(R.string.antepartumheamorrhage));
        } else
            aq.id(R.id.llbleedingpvsumm).gone();*/

        calculateGA(aq.id(R.id.etvisitdate).getText().toString());

        if (aq.id(R.id.chkbleedingpvbefore20wks).isChecked()) {
            aq.id(R.id.llbleedingpvsumm).visible();
            txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
            txt.setTextColor(getResources().getColor(R.color.red));
            aq.id(R.id.txtbleedingpvdiagnosisval).text(getResources().getString(R.string.bleedingpvabortiondiagnosis));
            //   aq.id(R.id.txtbleedingpvaction).text(getResources().getString(R.string.referimmediately) + " - PHC");
            aq.id(R.id.txtbleedingpvaction).text(getResources().getString(R.string.referimmediately));

        } else if (aq.id(R.id.chkbleedingpvafter20wks).isChecked()) {
            aq.id(R.id.llbleedingpvsumm).visible();
            txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
            txt.setTextColor(getResources().getColor(R.color.red));
            aq.id(R.id.txtbleedingpvdiagnosisval).text(getResources().getString(R.string.antepartum_haemorrahage));
            //  aq.id(R.id.txtbleedingpvaction).text(getResources().getString(R.string.referimmediately) + " - CEmOC");
            aq.id(R.id.txtbleedingpvaction).text(getResources().getString(R.string.referimmediately));

        } else {
            aq.id(R.id.llbleedingpvsumm).gone();
            txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
            txt.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
            aq.id(R.id.txtbleedingpvdiagnosisval).text("");
            aq.id(R.id.txtbleedingpvaction).text("");
        }
    }

    private void hideshowdetailsconstipation(TextView txt) {
        if (aq.id(R.id.chkconstipation).isChecked()) {
            aq.id(R.id.llconstipationsumm).visible();
            txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
            txt.setTextColor(getResources().getColor(R.color.red));

            aq.id(R.id.chkconstipationnotrelieved).enabled(true);  //16Nov2019 - Bindu- constipation not relievd enable

            if (aq.id(R.id.chkconstipationnotrelieved).isChecked()) {
                aq.id(R.id.txtconstipationaction2).visible();
            } else
                aq.id(R.id.txtconstipationaction2).gone();
        } else {
            aq.id(R.id.llconstipationsumm).gone();
            txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
            txt.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
            aq.id(R.id.chkconstipationnotrelieved).enabled(false); //16Nov2019 - Bindu const not rel - disable and uncheck
            aq.id(R.id.chkconstipationnotrelieved).checked(false);
        }
    }

    // Urination Problem
    private void hideshowdetailsurinationpbm(TextView txt) {
        calculateGA(aq.id(R.id.etvisitdate).getText().toString());

        /*if (aq.id(R.id.chkincreasedurination).isChecked() || aq.id(R.id.chkurinationafter12wks).isChecked() || aq.id(R.id.chkburningurination).isChecked()) {
            aq.id(R.id.llfreqofurinationsumm).visible();
            txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
            txt.setTextColor(getResources().getColor(R.color.red));
        } else {
            aq.id(R.id.llfreqofurinationsumm).gone();
            txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
            txt.setTextColor(getResources().getColor(R.color.colorPrimaryDark));

            if(!aq.id(R.id.chkfeverandabdominalpain).isChecked())
                aq.id(R.id.chkpaininabdomen).checked(false);

        }

        if (aq.id(R.id.chkpaininabdomen).isChecked() || (aq.id(R.id.chkfever).isChecked() && (aq.id(R.id.rdmildfeveru).isChecked()) || (aq.id(R.id.rdhighfeveru).isChecked()))){
            aq.id(R.id.txtfreqofurinationdiagnosisval1).gone();
            aq.id(R.id.txtfreqofurinationdiagnosisval2).visible();
            aq.id(R.id.txtfreqofurinationaction2).visible();
            aq.id(R.id.txtfreqofurinationaction1).gone();
        }else if(aq.id(R.id.chkincreasedurination).isChecked() && week < 12){
            aq.id(R.id.txtfreqofurinationdiagnosisval1).visible();
            aq.id(R.id.txtfreqofurinationdiagnosisval2).gone();
            aq.id(R.id.txtfreqofurinationaction2).gone();
            aq.id(R.id.txtfreqofurinationaction1).visible();
        }else if(aq.id(R.id.chkincreasedurination).isChecked() && week > 12){
            aq.id(R.id.txtfreqofurinationdiagnosisval1).gone();
            aq.id(R.id.txtfreqofurinationdiagnosisval2).visible();
            aq.id(R.id.txtfreqofurinationaction2).visible();
            aq.id(R.id.txtfreqofurinationaction1).gone();
        }

        hideshowdetailsfever(aq.id(R.id.txtfever).getTextView());*/
    }

    private void hideshowdetailsheartburnnausea(TextView txt) {
        // Heartburn and nausea
        if (aq.id(R.id.chkheartburn).isChecked() && aq.id(R.id.chknausea).isChecked()) {
            aq.id(R.id.llheartburnnauseasumm).visible();
            txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
            txt.setTextColor(getResources().getColor(R.color.red));
        } else {
            aq.id(R.id.llheartburnnauseasumm).gone();
            txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
            txt.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        }
    }

    private void hideshowdetailsedema(TextView txt) {
        //Edema
        if (aq.id(R.id.chkpuffinessofface).isChecked() || aq.id(R.id.chkgenbodyoedema).isChecked()) {
            aq.id(R.id.lledemasumm).visible();
            txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
            txt.setTextColor(getResources().getColor(R.color.red));
        } else {
            aq.id(R.id.lledemasumm).gone();
            txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
            txt.setTextColor(getResources().getColor(R.color.colorPrimaryDark));

            //aq.id(R.id.chkbp).checked(false);
            if (!aq.id(R.id.chksevereheadachesymp).isChecked())
                aq.id(R.id.chksevereheadache).checked(false);
            if (!aq.id(R.id.chkblurredvisionsymp).isChecked())
                aq.id(R.id.chkblurredvision).checked(false);
            aq.id(R.id.etbpsystolic).text("");
            aq.id(R.id.etbpdiastolic).text("");
            aq.id(R.id.rdproteinuriaabsent).checked(false);
            aq.id(R.id.rdproteinuriapresent).checked(false);
        }
    }

    private void hideshowdetailspalpfatiguebreathlessness(TextView txt) {
        //  if(aq.id(R.id.chkpalpitations).isChecked() && aq.id(R.id.chkeasyfatiguability).isChecked() && aq.id(R.id.chkbreathlessnessatrest).isChecked()) {
        //15Oct2019 - Bindu - Check any 2 conditions instead of 3
        int i = 0;
        if (aq.id(R.id.chkpalpitations).isChecked())
            i++;
        if (aq.id(R.id.chkeasyfatiguability).isChecked())
            i++;
        if (aq.id(R.id.chkbreathlessnessatrest).isChecked())
            i++;

        if (i >= 2) {
            aq.id(R.id.llpalpsumm).visible();
            txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
            txt.setTextColor(getResources().getColor(R.color.red));
            calculateGA(aq.id(R.id.etvisitdate).getText().toString());
        } else {
            aq.id(R.id.llpalpsumm).gone();
            txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
            txt.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
            aq.id(R.id.chkifa).checked(false);
            aq.id(R.id.chkalbedazolegiven).checked(false);
        }

        if (!(aq.id(R.id.chkpalpitations).isChecked() || aq.id(R.id.chkeasyfatiguability).isChecked() || aq.id(R.id.chkbreathlessnessatrest).isChecked())) {
            aq.id(R.id.chkfastordiffbreathing).checked(false);
        }
    }

    // Display Tablet Details
    private void displayTabletDetails() {

        aq.id(R.id.chkparacetamol).checked(false);
        aq.id(R.id.chkifa).checked(false);
        aq.id(R.id.chkalbedazolegiven).checked(false);
        aq.id(R.id.chkparacetamol).enabled(true);
        aq.id(R.id.chkifa).enabled(true);
        aq.id(R.id.chkalbedazolegiven).enabled(true);

        //if(aq.id(R.id.chkpalpitations).isChecked() && aq.id(R.id.chkeasyfatiguability).isChecked() && aq.id(R.id.chkbreathlessnessatrest).isChecked()) {
        //15Oct2019 - bindu - check for any 2 symptoms
        int i = 0;
        if (aq.id(R.id.chkpalpitations).isChecked())
            i++;
        if (aq.id(R.id.chkeasyfatiguability).isChecked())
            i++;
        if (aq.id(R.id.chkbreathlessnessatrest).isChecked())
            i++;

        if (i >= 2) {
            aq.id(R.id.chkifa).checked(true);
            aq.id(R.id.chkifa).enabled(false);
            if (week > 13) {
                aq.id(R.id.chkalbedazolegiven).checked(true);
                aq.id(R.id.chkalbedazolegiven).enabled(false);
            }
        }/*else {
            aq.id(R.id.chkifa).checked(false);
            aq.id(R.id.chkalbedazolegiven).checked(false);
        }*/

        if (aq.id(R.id.chkfever).isChecked()) {
            aq.id(R.id.chkparacetamol).checked(true);
            aq.id(R.id.chkparacetamol).enabled(false);
        }


        displayTabletDetailstable();
    }

    private void displayvomitingsigninterpretation() {

        int i = 0;
        if (aq.id(R.id.chkdrytongue).isChecked())
            i++;
        if (aq.id(R.id.chkfeeblepulse).isChecked())
            i++;
        if (aq.id(R.id.chkdrydryconjunctiva).isChecked())
            i++;
        if (aq.id(R.id.chkbplow).isChecked())
            i++;
        if (aq.id(R.id.chkdecurineoutput).isChecked())
            i++;

        if (i >= 3) {
            aq.id(R.id.llvomdetails1).visible();
            aq.id(R.id.txtvomitinginterpretationval).text(getResources().getString(R.string.dehydration));
            aq.id(R.id.txtvomitinginterpretationvalsave).text(getResources().getString(R.string.dehydrationsave)); //14Nov2019 - Bindu - save in eng hidden field
            aq.id(R.id.txtvomitingdiagnosisval).gone();
            aq.id(R.id.txtvomitingdiagnosisval).text("");
            aq.id(R.id.txtvomitingdiagnosisvalsave).text(""); //14Nov2019 - Bindu
            aq.id(R.id.txtvomitingadvise2).gone();
            aq.id(R.id.txtvomitingadvise3).gone();
            if (week > 13) {
                aq.id(R.id.txtvomitingaction).text(getResources().getString(R.string.referimmediately));
                aq.id(R.id.txtvomitingadvise1).text(getResources().getString(R.string.admtophc));
                phc247Cnt++;
            } else {
                aq.id(R.id.txtvomitingaction).text(getResources().getString(R.string.refer));
                aq.id(R.id.txtvomitingadvise1).text(getResources().getString(R.string.eatdryfoods));
                phcCnt++;
            }
        } else if (i > 0 && i < 3) {
            aq.id(R.id.llvomdetails1).visible();
            aq.id(R.id.txtvomitinginterpretationval).text(getResources().getString(R.string.nodehydration));
            aq.id(R.id.txtvomitinginterpretationvalsave).text(getResources().getString(R.string.nodehydrationsave)); //14Nov2019 - Bindu
            aq.id(R.id.txtvomitingdiagnosisval).visible();
            aq.id(R.id.txtvomitingdiagnosisval).text(getResources().getString(R.string.physiologicalsickness));
            aq.id(R.id.txtvomitingdiagnosisvalsave).text(getResources().getString(R.string.physiologicalsicknesssave)); //14Nov2019 - Bindu
            aq.id(R.id.txtvomitingadvise1).text(getResources().getString(R.string.eatsmallfrequentfoods));
            aq.id(R.id.txtvomitingadvise2).visible();
            aq.id(R.id.txtvomitingadvise3).visible();
            aq.id(R.id.txtvomitingaction).text(getResources().getString(R.string.homeadvise));
        } else {
            aq.id(R.id.txtvomitinginterpretationval).text("");
            aq.id(R.id.txtvomitingdiagnosisval).text("");
            aq.id(R.id.txtvomitinginterpretationvalsave).text(""); //14Nov2019 - Bindu
            aq.id(R.id.txtvomitingdiagnosisvalsave).text(""); //14Nov2019 - Bindu
            aq.id(R.id.txtvomitingdiagnosisval).gone();
            aq.id(R.id.txtvomitingadvise1).text("");
            aq.id(R.id.txtvomitingaction).text("");
            aq.id(R.id.llvomdetails1).gone();
        }
    }

    private void hideshowdetailsvomiting(TextView txt) {

        if (aq.id(R.id.chkvomitingduringfirst3months).isChecked()) {
            aq.id(R.id.llvomitingsumm).visible();
            txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
            txt.setTextColor(getResources().getColor(R.color.red));
        } /*else if (aq.id(R.id.chkexcvomitingduringfirst3months).isChecked()) {
            aq.id(R.id.llvomitingsumm).visible();
            txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
            txt.setTextColor(getResources().getColor(R.color.red));
        } else if (aq.id(R.id.chkexcvomitingafter3months).isChecked()) {
            aq.id(R.id.llvomitingsumm).visible();
            txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
            txt.setTextColor(getResources().getColor(R.color.red));
        }*/ else {
            aq.id(R.id.llvomitingsumm).gone();
            txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
            txt.setTextColor(getResources().getColor(R.color.colorPrimaryDark));

            aq.id(R.id.chkdrytongue).checked(false);
            aq.id(R.id.chkfeeblepulse).checked(false);
            aq.id(R.id.chkdrydryconjunctiva).checked(false);
            aq.id(R.id.chkbplow).checked(false);
            aq.id(R.id.chkdecurineoutput).checked(false);

        }

    }

    private void hideshowdetailsfoetalmovement(TextView txt) {
        if (aq.id(R.id.rdfoetalmovementabsent).isChecked() || aq.id(R.id.rdfoetalmovementdecreased).isChecked()) {
            aq.id(R.id.llfoetalmovementsumm).visible();
            txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
            txt.setTextColor(getResources().getColor(R.color.red));
        } else {
            aq.id(R.id.llfoetalmovementsumm).gone();
            txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
            txt.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        }
    }

    private void hideshowdetailsvaginaldischarge(TextView txt) {
        if (aq.id(R.id.chkvaginaldischarge).isChecked()) {
            aq.id(R.id.llvaginaldischargesumm).visible();
            txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
            txt.setTextColor(getResources().getColor(R.color.red));

            aq.id(R.id.chkabdominalpain).enabled(true); //19Oct2019 - Bindu Abdominal pain - enable

        } else {
            aq.id(R.id.llvaginaldischargesumm).gone();
            txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
            txt.setTextColor(getResources().getColor(R.color.colorPrimaryDark));

            aq.id(R.id.chkabdominalpain).enabled(false); //19Oct2019 - Bindu Abdominal pain - disable and uncheck
            aq.id(R.id.chkabdominalpain).checked(false);
        }
    }

    private void hideshowdetailswateryfluids(TextView txt) {
        /*if (aq.id(R.id.chkwateryfluids).isChecked() && aq.id(R.id.chkpads).isChecked()) {
            aq.id(R.id.llwateryfluidssumm).visible();
            aq.id(R.id.llwateryfluidsdiagnosis).visible();
            txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
            txt.setTextColor(getResources().getColor(R.color.red));
        } /*else */
        if (aq.id(R.id.chkwateryfluids).isChecked()) {
            aq.id(R.id.llwateryfluidssumm).visible();
            aq.id(R.id.llwateryfluidsdiagnosis).visible();
            txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
            txt.setTextColor(getResources().getColor(R.color.red));
        } else {
            aq.id(R.id.llwateryfluidssumm).gone();
            txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
            txt.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        }
    }

    private void hideshowdetailssevereheadachewithblurredvision(TextView txt) {
        if (aq.id(R.id.chksevereheadachesymp).isChecked() || aq.id(R.id.chkblurredvisionsymp).isChecked()) {
            aq.id(R.id.llsevereheadachesumm).visible();
            txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
            txt.setTextColor(getResources().getColor(R.color.red));
        } else {
            aq.id(R.id.llsevereheadachesumm).gone();
            txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
            txt.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        if (aq.id(R.id.chksevereheadachesymp).isChecked()) {
            aq.id(R.id.chksevereheadache).checked(true);
            aq.id(R.id.chksevereheadache).enabled(false);
        } else if (!(aq.id(R.id.chksevereheadachesymp).isChecked())) {
            aq.id(R.id.chksevereheadache).checked(false);
            aq.id(R.id.chksevereheadache).enabled(true);
        }

        if (aq.id(R.id.chkblurredvisionsymp).isChecked()) {
            aq.id(R.id.chkblurredvision).checked(true);
            aq.id(R.id.chkblurredvision).enabled(false);
        } else if (!(aq.id(R.id.chkblurredvisionsymp).isChecked())) {
            aq.id(R.id.chkblurredvision).checked(false);
            aq.id(R.id.chkblurredvision).enabled(true);
        }
    }

    private void hideshowdetailsfitsorlossofconsciousness(TextView txt) {
        if (aq.id(R.id.chkfits).isChecked() || aq.id(R.id.chklossofconsciousness).isChecked()) {
            aq.id(R.id.llfitssumm).visible();
            txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
            txt.setTextColor(getResources().getColor(R.color.red));
        } else {
            aq.id(R.id.llfitssumm).gone();
            txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
            txt.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        }
    }

    //    16May2021 Bindu health symtpoms and covid vaccine
    private void hideshowdetailsanyhealthissuesymtpoms(TextView txt) {
        if (aq.id(R.id.rd_healthissuesYes).isChecked()) {
            aq.id(R.id.trHealthIssueYes).visible();
//            aq.id(R.id.trCovidResult).visible();
            aq.id(R.id.llCovidQs).getView().setVisibility(View.GONE); //18May2021 Bindu
            txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
            txt.setTextColor(getResources().getColor(R.color.red));
        } else if (aq.id(R.id.rd_healthissuesNo).isChecked()) {
            aq.id(R.id.trHealthIssueYes).gone();
            String[] healthproblems = getResources().getStringArray(R.array.healthproblemsuffer);
            spnHealthProblemshv.setItems(healthproblems);
            aq.id(R.id.llCovidQs).getView().setVisibility(View.GONE); //18May2021 Bindu
            aq.id(R.id.etVisitHealthIssuesYesOthers).text("");
            aq.id(R.id.trHealthProblemYesOthers).gone();
            aq.id(R.id.rd_CovidTestYes).checked(false);
            rgCovidTest.clearCheck(); //21May2021 Bindu
            hideshowcovidTestdetails();
//            aq.id(R.id.trCovidResult).gone();
            txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
            txt.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        }
    }

    private void hideshowcovidvaccinedetails(TextView txt) {
        if (aq.id(R.id.rd_CovidVaccineYes).isChecked()) {
            aq.id(R.id.trCovidVaccinDose).visible();
            txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
            txt.setTextColor(getResources().getColor(R.color.red));
        } else if (aq.id(R.id.rd_CovidVaccineNo).isChecked()) {
            aq.id(R.id.trCovidVaccinDose).gone();
            aq.id(R.id.trCovidFirstDoseDate).getView().setVisibility(View.GONE);
            aq.id(R.id.trCovidSecondDoseDate).getView().setVisibility(View.GONE);
            aq.id(R.id.spnVisitVaccineDose).setSelection(0);
            txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
            txt.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        }
    }

    //    16May2021 Bindu
    public static void hideshowcovidTestdetails() {
        if (aq.id(R.id.rd_CovidTestYes).isChecked()) {
            aq.id(R.id.trCovidTesDate).visible();
            aq.id(R.id.trCovidResult).visible();
            rgCovidResult.clearCheck();
        } else if (aq.id(R.id.rd_CovidTestNo).isChecked()) {
            aq.id(R.id.trCovidTesDate).gone();
            aq.id(R.id.trCovidResult).gone();
            aq.id(R.id.etVisitCovidTestDate).text("");
            aq.id(R.id.rd_CovidPositive).checked(false);
            aq.id(R.id.rd_CovidNegative).checked(false);
        } else {
            aq.id(R.id.trCovidTesDate).gone();
            aq.id(R.id.trCovidResult).gone();
        }
    }


    private void hideshowdetailslaborpain(TextView txt) {
        if (aq.id(R.id.chklaborpaingt12hrs).isChecked()) {
            aq.id(R.id.lllaborpaingt12hrssumm).visible();
            txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
            txt.setTextColor(getResources().getColor(R.color.red));
        } else {
            aq.id(R.id.lllaborpaingt12hrssumm).gone();
            txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
            txt.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        }
    }

    private void hideshowdetailsabdominalpain(TextView txt) {
        if (aq.id(R.id.chkcontinuousabdominalpain).isChecked()) {
            aq.id(R.id.llcontinuousabdominalpainsumm).visible();
            txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
            txt.setTextColor(getResources().getColor(R.color.red));
        } else {
            aq.id(R.id.llcontinuousabdominalpainsumm).gone();
            txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
            txt.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        }
    }

    // Hide tabs and set the proper selected tab
    private void hidetabs() {
        aq.id(R.id.btnSummary).getImageView().setBackgroundResource(R.drawable.gray_button);
        aq.id(R.id.btnhomevisitdetails).getImageView().setBackgroundResource(R.drawable.gray_button);
    }

    // Hide or show Details
    private void hideshowdetails(TextView txt) {


        // Frequency of urination
       /* if (aq.id(R.id.chkincreasedurination).isChecked() || aq.id(R.id.chkurinationafter12wks).isChecked() || aq.id(R.id.chkburningurination).isChecked()) {
            aq.id(R.id.llfreqofurinationsumm).visible();
        } else
            aq.id(R.id.llfreqofurinationsumm).gone();

        if (aq.id(R.id.chkconstipation).isChecked()) {
            aq.id(R.id.llconstipationsumm).visible();
        } else
            aq.id(R.id.llconstipationsumm).gone();*/


    }

    public void opencloseAccordion(View v, TextView txt) {
        try {
            if (v.getVisibility() == View.VISIBLE) {
                v.setVisibility(View.GONE);
                if (txt.getCurrentTextColor() == getResources().getColor(R.color.red))
                    txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.collapse, 0);
                else
                    txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
            } else {
                // aq.id(R.id.llvomdetails).gone();
                if (aq.id(R.id.txtvomiting).getTextView().getCurrentTextColor() == getResources().getColor(R.color.red))
                    aq.id(R.id.txtvomiting).getTextView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.collapse, 0);
                else
                    aq.id(R.id.txtvomiting).getTextView().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);


                //  aq.id(R.id.llpalpdetails).gone();
                if (aq.id(R.id.tvpalplbl).getTextView().getCurrentTextColor() == getResources().getColor(R.color.red))
                    aq.id(R.id.tvpalplbl).getTextView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.collapse, 0);
                else
                    aq.id(R.id.tvpalplbl).getTextView().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);


                //   aq.id(R.id.lledemadetails).gone();
                if (aq.id(R.id.txtedema).getTextView().getCurrentTextColor() == getResources().getColor(R.color.red))
                    aq.id(R.id.txtedema).getTextView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.collapse, 0);
                else
                    aq.id(R.id.txtedema).getTextView().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);


                //  aq.id(R.id.llheartburnandnauseadetails).gone();
                if (aq.id(R.id.txtheartburnandnausea).getTextView().getCurrentTextColor() == getResources().getColor(R.color.red))
                    aq.id(R.id.txtheartburnandnausea).getTextView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.collapse, 0);
                else
                    aq.id(R.id.txtheartburnandnausea).getTextView().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);

                //  aq.id(R.id.llfreqofurinationdetails).gone();
               /* if(aq.id(R.id.txtfreqofurination).getTextView().getCurrentTextColor() ==  getResources().getColor(R.color.red))
                    aq.id(R.id.txtfreqofurination).getTextView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.collapse, 0);
                else
                aq.id(R.id.txtfreqofurination).getTextView().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
*/
                v.setVisibility(View.VISIBLE);
                if (txt.getCurrentTextColor() == getResources().getColor(R.color.red))
                    txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
                else
                    txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.expand, 0);

                //     txt.setTextColor(getResources().getColor(R.color.red));
            }
        } catch (Exception e) {

        }
    }

    public void opencloseAccordionCheckbox(View v, CheckBox chk) {
        try {
            if (chk.isChecked()) {
                v.setVisibility(View.VISIBLE);
                chk.setTextColor(getResources().getColor(R.color.red));
                chk.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_selected, 0, R.drawable.expand, 0);
            } else {
                v.setVisibility(View.GONE);
                chk.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                chk.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.collapse, 0);
            }
        } catch (Exception e) {

        }
    }

    // Expand and Collapse Danger Signs
    public void opencloseAccordionDangerSigns(View v, TextView txt) {
        try {
            if (v.getVisibility() == View.VISIBLE) {
                v.setVisibility(View.GONE);
                txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_expsym, 0, 0, 0);
            } else {
                v.setVisibility(View.VISIBLE);
                txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_colsym, 0, 0, 0);
            }
        } catch (Exception e) {

        }
    }

    /**
     * Initiates the Navigation drawer
     */
    private void initiateDrawer() {
        try {
            mDrawerLayout = findViewById(R.id.ett_drawer_layout);
            mDrawerList = findViewById(R.id.ett_left_drawer);
          /*  mDrawerToggle = new ActionBarDrawerToggle
                    (
                            this,
                            mDrawerLayout,
                            R.string.drawer_open,
                            R.string.drawer_close
                    )
            {
            };
            mDrawerLayout.setDrawerListener(mDrawerToggle);
            mDrawerToggle.syncState(); */

            navDrawerItems = new ArrayList<NavDrawerItem>();

            // adding nav drawer items to array
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.sp_WRegEdit),
                    R.drawable.registration));

            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.services),
                    R.drawable.anm_pending_activities));


            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.unplannedserviceslist),
                    R.drawable.anm_pending_activities));
            //14Sep2019 - Bindu - Add Homevisit
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.womanhomevisithistory),
                    R.drawable.prenatalhomevisit));
            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deliveryHeading), R.drawable.delivery_info));//31Oct2019 Arpitha

            navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deactivate),
                    R.drawable.deactivate));//28Nov2019 Arpitha

            // set a custom shadow that overlays the main content when the drawer
            // opens
            mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
            mDrawerList.setAdapter(new NavDrawerListAdapter(getApplicationContext(), navDrawerItems));
            mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

            // ActionBarDrawerToggle ties together the the proper interactions
            // between the sliding drawer and the action bar app icon
            mDrawerToggle = new ActionBarDrawerToggle(this, /* host Activity */
                    mDrawerLayout, /* DrawerLayout object */
                    R.drawable.ic_drawer, /*
             * nav drawer image to replace 'Up'
             * caret
             */
                    R.string.drawer_open, /*
             * "open drawer" description for
             * accessibility
             */
                    R.string.drawer_close /*
             * "close drawer" description for
             * accessibility
             */
            ) {
                public void onDrawerClosed(View view) {
                    /*invalidateOptionsMenu(); // creates call to
                    getSupportActionBar().setDisplayShowHomeEnabled(false);
                    getSupportActionBar().setDisplayShowTitleEnabled(false);
                    getSupportActionBar().setDisplayShowCustomEnabled(false);
                    int noOfWeeks;
                    try {
                        if (woman.getRegADDate() != null && woman.getRegADDate().length() > 0) {
                            getSupportActionBar().setTitle((woman.getRegWomanName() + "\t"));
                        } else {
                            noOfWeeks = CommonClass.getNumberOfWeeksFromLMP(woman.getRegLMP());
                            getSupportActionBar().setTitle((woman.getRegWomanName() + " (" + noOfWeeks + " "
                                    + getResources().getString(R.string.weeks) + ")"));
                        }*/
                    try {
                        displayDrawerItems();
                    } catch (Exception e) {

                    }
                }

                public void onDrawerOpened(View drawerView) {
                    try {

                        displayDrawerItems();
                    } catch (Exception e) {

                    }
                }
            };
            mDrawerLayout.addDrawerListener(mDrawerToggle);
        } catch (Exception e) {

        }
    }

    // Display drawer open close items
    private void displayDrawerItems() throws Exception {

        invalidateOptionsMenu(); // creates call to
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer1);
        getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);


        aq.id(R.id.ivWomanImg).getImageView().setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                mDrawerLayout.closeDrawer(mDrawerList);
            }
        });

        aq.id(R.id.tvWomanName).text((woman.getRegWomanName() + "\t"));
        if (woman.getRegADDate() != null && woman.getRegADDate().length() > 0) {
            aq.id(R.id.tvInfo1).text(woman.getRegADDate());
        } else {
            int noOfWeeks = DateTimeUtil.getNumberOfWeeksFromLMP(woman.getRegLMP());
            aq.id(R.id.tvInfo1).text(getResources().getString(R.string.tvlmp) + woman.getRegLMP() + "  , GA : " + noOfWeeks + "W");
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle != null && mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        try {
            switch (item.getItemId()) {
                case android.R.id.home:
                    Intent home = new Intent(HomeVisit.this, MainMenuActivity.class);
                    home.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    startActivity(home);
                    break;

                case R.id.wlist:
                    Intent wlist = new Intent(HomeVisit.this, RegisteredWomenActionTabs.class);
                    wlist.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    /*startActivity(wlist);*/
                    displayAlert(getResources().getString(R.string.exit), wlist);
                    break;

                case R.id.logout:
                    Intent logout = new Intent(HomeVisit.this, LoginActivity.class);
                    displayAlert(getResources().getString(R.string.m111), logout);
                    /*startActivity(logout);
                    CommonClass.updateLogoutTime();*/
                    break;

                case R.id.home:
                    Intent intent = new Intent(HomeVisit.this, MainMenuActivity.class);
                    intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    startActivity(intent);
                    break;
                //30Sep2019 - Bindu
                case R.id.about: {
                    Intent goToScreen = new Intent(HomeVisit.this, AboutActivity.class);
                    goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    displayAlert(getResources().getString(R.string.exit), goToScreen);
                    return true;
                }
            }
        } catch (Exception e) {

        }
        return super.onOptionsItemSelected(item);
    }


    /**
     * Initializes the Action Bar items
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        try {
            /* if (woman.getRegADDate() != null && woman.getRegADDate().length() > 0) {
                getSupportActionBar().setTitle((woman.getRegWomanName() + "\t"));
            } else {
                int noOfWeeks = CommonClass.getNumberOfWeeksFromLMP(woman.getRegLMP());
                getSupportActionBar().setTitle((woman.getRegWomanName() + " (" + noOfWeeks + " "
                        + getResources().getString(R.string.weeks) + ")"));
            }*/
           /* getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer);*/

            //16Sep2019 - Bindu
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer1);
            getSupportActionBar().setDisplayShowCustomEnabled(true);


            getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);

            aq.id(R.id.tvWomanName).text(woman.getRegWomanName());
            if (woman.getRegADDate() != null && woman.getRegADDate().length() > 0) {
                aq.id(R.id.tvInfo1).text(woman.getRegADDate());
            } else {
                int noOfWeeks = DateTimeUtil.getNumberOfWeeksFromLMP(woman.getRegLMP());
                aq.id(R.id.tvInfo1).text(getResources().getString(R.string.tvlmp) + woman.getRegLMP() + "  , GA : " + noOfWeeks + "W");
            }

            aq.id(R.id.ivWomanImg).gone();

            getMenuInflater().inflate(R.menu.servicesmenu, menu);

            menu.findItem(R.id.logout).setTitle((getResources().getString(R.string.action_logout)));


        } catch (Exception e) {

        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        // mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    private void setSymtpomsSummary() throws Exception {

        //20Sep2019 - Bindu
        displayTabletDetails();

        arrLSymptoms = new ArrayList<>();
        cemocCnt = 0;
        phc247Cnt = 0;
        phcCnt = 0;
        /*arrLSymptomsId = new ArrayList<>();*/
        //13Nov2019 - Bindu
        arrLSymptomsSave = new ArrayList<>();

        setFeverOtherSymtpoms();
        // Fever and other symptoms
        if (strfeverandother.length() > 0) {
            arrLSymptoms.add(strfeverandother);
            arrLSymptomsSave.add(strfeverandothersave);
            if (isRefImm || isRef)
                phcCnt++;
        }

        //Fits
        if (aq.id(R.id.chkfits).isChecked()) {
            arrLSymptoms.add(aq.id(R.id.chkfits).getText().toString());
            arrLSymptomsSave.add(getResources().getString(R.string.fitssave));
            cemocCnt++;
        }

        // Loss of consciousness
        if (aq.id(R.id.chklossofconsciousness).isChecked()) {
            arrLSymptoms.add(aq.id(R.id.chklossofconsciousness).getText().toString());
            arrLSymptomsSave.add(getResources().getString(R.string.lossofconsciousnesssave));
            cemocCnt++;
        }

        //Severe headache
        if (aq.id(R.id.chksevereheadachesymp).isChecked()) {
            arrLSymptoms.add(aq.id(R.id.chksevereheadachesymp).getText().toString());
            arrLSymptomsSave.add(getResources().getString(R.string.severeheadachesave));
            cemocCnt++;
        }

        //Blurred vision
        if (aq.id(R.id.chkblurredvisionsymp).isChecked()) {
            arrLSymptoms.add(aq.id(R.id.chkblurredvisionsymp).getText().toString());
            arrLSymptomsSave.add(getResources().getString(R.string.blurredvisionsave));
            cemocCnt++;
        }

        //Heart burn and nausea
        if (aq.id(R.id.chkheartburn).isChecked() && aq.id(R.id.chknausea).isChecked()) {
            arrLSymptoms.add(aq.id(R.id.chkheartburn).getText().toString() + "," + aq.id(R.id.chknausea).getText().toString() + " - " + aq.id(R.id.txtheartburnnauseainterpretationval).getText().toString() + " - " + aq.id(R.id.txtheartburndiagnosisval).getText().toString());
            arrLSymptomsSave.add(getResources().getString(R.string.heartburnsave) + "," + getResources().getString(R.string.nauseasave) + " - " + getResources().getString(R.string.refluxsave) + " - " + getResources().getString(R.string.hypertensivedisorderofpregsave));
            phcCnt++;
        } else {
            if (aq.id(R.id.chkheartburn).isChecked()) {
                arrLSymptoms.add(aq.id(R.id.chkheartburn).getText().toString());
                arrLSymptomsSave.add(getResources().getString(R.string.heartburnsave));
                phcCnt++;
            }

            if (aq.id(R.id.chknausea).isChecked()) {
                arrLSymptoms.add(aq.id(R.id.chknausea).getText().toString());
                arrLSymptomsSave.add(getResources().getString(R.string.nauseasave));
                phcCnt++;
            }
        }

        // Vomiting
        if (aq.id(R.id.chkvomitingduringfirst3months).isChecked()) {
            String txtinterpretation = aq.id(R.id.txtvomitinginterpretationval).getText().toString().length() > 0 ? " - " + aq.id(R.id.txtvomitinginterpretationval).getText().toString() : "";
            String txtdiagnosis = aq.id(R.id.txtvomitingdiagnosisval).getText().toString().length() > 0 ? " - " + aq.id(R.id.txtvomitingdiagnosisval).getText().toString() : "";
            arrLSymptoms.add(aq.id(R.id.chkvomitingduringfirst3months).getText().toString() + txtinterpretation + txtdiagnosis);
            //14Nov2019 - Bindu
            String txtinterpretationsave = aq.id(R.id.txtvomitinginterpretationvalsave).getText().toString().length() > 0 ? " - " + aq.id(R.id.txtvomitinginterpretationvalsave).getText().toString() : "";
            String txtdiagnosissave = aq.id(R.id.txtvomitingdiagnosisvalsave).getText().toString().length() > 0 ? " - " + aq.id(R.id.txtvomitingdiagnosisvalsave).getText().toString() : "";
            arrLSymptomsSave.add(getResources().getString(R.string.vomittingsave) + txtinterpretationsave + txtdiagnosissave);

            displayvomitingsigninterpretation();

        }

        //Palpitations/Easy Fatiguability/Breathlessness at rest
        if (aq.id(R.id.chkpalpitations).isChecked() && aq.id(R.id.chkeasyfatiguability).isChecked() && aq.id(R.id.chkbreathlessnessatrest).isChecked()) {
            arrLSymptoms.add(aq.id(R.id.chkpalpitations).getText().toString() + "," + aq.id(R.id.chkeasyfatiguability).getText().toString() + ", " + aq.id(R.id.chkbreathlessnessatrest).getText().toString() + " - " + " - " + aq.id(R.id.txtpalpfatbreathdiagnosisval).getText().toString());
            arrLSymptomsSave.add(getResources().getString(R.string.palpitationssave) + "," + getResources().getString(R.string.easyfatiguabilitysave) + ", " + getResources().getString(R.string.breathlessnessatrestsave) + " - " + " - " + aq.id(R.id.txtpalpfatbreathdiagnosisvalsave).getText().toString());

            cemocCnt++;
        } else {
            //15Oct2019 - Bindu - 2 symp check
            if (aq.id(R.id.chkpalpitations).isChecked() && aq.id(R.id.chkeasyfatiguability).isChecked()) {
                arrLSymptoms.add(getResources().getString(R.string.palpitations) + ", " + getResources().getString(R.string.easyfatiguability) + " - " + " - " + aq.id(R.id.txtpalpfatbreathdiagnosisval).getText().toString());
                arrLSymptomsSave.add(getResources().getString(R.string.palpitationssave) + ", " + getResources().getString(R.string.easyfatiguabilitysave) + " - " + " - " + aq.id(R.id.txtpalpfatbreathdiagnosisvalsave).getText().toString());
            } else if (aq.id(R.id.chkpalpitations).isChecked() && aq.id(R.id.chkbreathlessnessatrest).isChecked()) {
                arrLSymptoms.add(getResources().getString(R.string.palpitations) + ", " + getResources().getString(R.string.breathlessnessatrest) + " - " + " - " + aq.id(R.id.txtpalpfatbreathdiagnosisval).getText().toString());
                arrLSymptomsSave.add(getResources().getString(R.string.palpitationssave) + ", " + getResources().getString(R.string.breathlessnessatrestsave) + " - " + " - " + aq.id(R.id.txtpalpfatbreathdiagnosisvalsave).getText().toString());
            } else if (aq.id(R.id.chkeasyfatiguability).isChecked() && aq.id(R.id.chkbreathlessnessatrest).isChecked()) {
                arrLSymptoms.add(getResources().getString(R.string.easyfatiguability) + ", " + getResources().getString(R.string.breathlessnessatrest) + " - " + " - " + aq.id(R.id.txtpalpfatbreathdiagnosisval).getText().toString());
                arrLSymptoms.add(getResources().getString(R.string.easyfatiguabilitysave) + ", " + getResources().getString(R.string.breathlessnessatrestsave) + " - " + " - " + aq.id(R.id.txtpalpfatbreathdiagnosisvalsave).getText().toString());
            } else {
                if (aq.id(R.id.chkpalpitations).isChecked()) {
                    arrLSymptoms.add(aq.id(R.id.chkpalpitations).getText().toString());
                    arrLSymptomsSave.add(getResources().getString(R.string.palpitationssave));
                    cemocCnt++;
                }
                if (aq.id(R.id.chkeasyfatiguability).isChecked()) {
                    arrLSymptoms.add(aq.id(R.id.chkeasyfatiguability).getText().toString());
                    arrLSymptomsSave.add(getResources().getString(R.string.easyfatiguabilitysave));
                    cemocCnt++;
                }
                if (aq.id(R.id.chkbreathlessnessatrest).isChecked()) {
                    arrLSymptoms.add(aq.id(R.id.chkbreathlessnessatrest).getText().toString());
                    arrLSymptomsSave.add(getResources().getString(R.string.breathlessnessatrestsave));
                    cemocCnt++;
                }
            }
        }

        //Puffiness of the face
        if (aq.id(R.id.chkpuffinessofface).isChecked() && aq.id(R.id.chkgenbodyoedema).isChecked()) {
            arrLSymptoms.add(aq.id(R.id.chkpuffinessofface).getText().toString() + "," + aq.id(R.id.chkgenbodyoedema).getText().toString() + " - " + " - " + aq.id(R.id.txtedemadiagnosisval).getText().toString());
            arrLSymptomsSave.add(getResources().getString(R.string.puffinessofthefacesave) + "," + getResources().getString(R.string.generalizedbodyodemasave) + " - " + " - " + getResources().getString(R.string.hypertensionsave));
            cemocCnt++;
            phc247Cnt++;
        } else {
            if (aq.id(R.id.chkpuffinessofface).isChecked()) {
                arrLSymptoms.add(aq.id(R.id.chkpuffinessofface).getText().toString() + " - " + " - " + aq.id(R.id.txtedemadiagnosisval).getText().toString());
                arrLSymptomsSave.add(getResources().getString(R.string.puffinessofthefacesave) + " - " + " - " + getResources().getString(R.string.hypertensionsave));
                cemocCnt++;
                phc247Cnt++;
            } else if (aq.id(R.id.chkgenbodyoedema).isChecked()) {
                arrLSymptoms.add(aq.id(R.id.chkgenbodyoedema).getText().toString() + " - " + " - " + aq.id(R.id.txtedemadiagnosisval).getText().toString());
                arrLSymptomsSave.add(getResources().getString(R.string.generalizedbodyodemasave) + " - " + " - " + getResources().getString(R.string.hypertensionsave));
                cemocCnt++;
                phc247Cnt++;
            }
        }

        // Continuous abdominal pain
        if (aq.id(R.id.chkcontinuousabdominalpain).isChecked()) {
            arrLSymptoms.add(aq.id(R.id.chkcontinuousabdominalpain).getText().toString());
            arrLSymptomsSave.add(getResources().getString(R.string.continuoussevereabdominalpainsave));

            cemocCnt++;
        }

        /*if(aq.id(R.id.chkfever).isChecked()){
            arrLSymptoms.add(aq.id(R.id.chkfever).getText().toString());
        }

        if(aq.id(R.id.chkincreasedurination).isChecked()) {
            arrLSymptoms.add(aq.id(R.id.chkincreasedurination).getText().toString());
            arrLSymptomsId.add("108");
        }

        if(aq.id(R.id.chkurinationafter12wks).isChecked()) {
            arrLSymptoms.add(aq.id(R.id.chkurinationafter12wks).getText().toString());
            phcCnt++;
        }

        if(aq.id(R.id.chkburningurination).isChecked()) {
            arrLSymptoms.add(aq.id(R.id.chkburningurination).getText().toString());
            arrLSymptomsId.add("109");
        }*/

        // Constipation
        if (aq.id(R.id.chkconstipation).isChecked()) {
            if (aq.id(R.id.chkconstipationnotrelieved).isChecked()) {
                arrLSymptoms.add(aq.id(R.id.chkconstipation).getText().toString() + "(" + aq.id(R.id.chkconstipationnotrelieved).getText().toString() + ") " + " - " + " - " + aq.id(R.id.txtconstipationdiagnosisval).getText().toString());
                arrLSymptomsSave.add(getResources().getString(R.string.constipationsave) + "(" + getResources().getString(R.string.notrelievedsave) + ") " + " - " + " - " + getResources().getString(R.string.physiologicalsave));

                phcCnt++;
            } else {
                arrLSymptoms.add(aq.id(R.id.chkconstipation).getText().toString() + " - " + " - " + aq.id(R.id.txtconstipationdiagnosisval).getText().toString());
                arrLSymptomsSave.add(getResources().getString(R.string.constipationsave) + " - " + " - " + getResources().getString(R.string.physiologicalsave));
            }
        }

        //Foetal Movement
        if (aq.id(R.id.rdfoetalmovementdecreased).isChecked()) {
            arrLSymptoms.add(aq.id(R.id.txtfoetalmovement).getText().toString() + " " + aq.id(R.id.rdfoetalmovementdecreased).getText().toString());
            arrLSymptomsSave.add(getResources().getString(R.string.foetalmovementsave) + " " + getResources().getString(R.string.rdfoetalmovementdecreasedsave));
            cemocCnt++;
        }


        if (aq.id(R.id.rdfoetalmovementabsent).isChecked()) {
            arrLSymptoms.add(aq.id(R.id.txtfoetalmovement).getText().toString() + " " + aq.id(R.id.rdfoetalmovementabsent).getText().toString());
            arrLSymptomsSave.add(getResources().getString(R.string.foetalmovementsave) + " " + getResources().getString(R.string.rdfoetalmovementabsentsave));
            cemocCnt++;
        }

//Bleeding PV
        if (aq.id(R.id.chkbleedingpvbefore20wks).isChecked()) {
            arrLSymptoms.add(aq.id(R.id.chkbleedingpvbefore20wks).getText().toString() + " -  " + " - " + aq.id(R.id.txtbleedingpvdiagnosisval).getText().toString());
            arrLSymptomsSave.add(getResources().getString(R.string.bleedingpvbefore20wksofgestsave) + " - " + " - " + getResources().getString(R.string.bleedingpvabortiondiagnosissave));
            phcCnt++;
        }

        if (aq.id(R.id.chkbleedingpvafter20wks).isChecked()) { //pending
            arrLSymptoms.add(aq.id(R.id.chkbleedingpvafter20wks).getText().toString() + " -  " + " - " + aq.id(R.id.txtbleedingpvdiagnosisval).getText().toString());
            arrLSymptomsSave.add(getResources().getString(R.string.bleedingpvafter20wksofgestsave) + " - " + " - " + getResources().getString(R.string.antepartum_haemorrahagesave));
            cemocCnt++;
        }

        // Vaginal discharge and abdominal pain
        // 15oct2019 - bindu - change vag disc tp white discharge pv
        if (aq.id(R.id.chkvaginaldischarge).isChecked()) { //pending
            if (aq.id(R.id.chkabdominalpain).isChecked()) {
                arrLSymptoms.add(getResources().getString(R.string.whitedischargewithabdominalpain) + " - " + " - " + aq.id(R.id.txtvaginaldischargediagnosisval).getText().toString());
                arrLSymptomsSave.add(getResources().getString(R.string.whitedischargewithabdominalpainsave) + " - " + " - " + getResources().getString(R.string.rtistisave));
            } else {
                arrLSymptoms.add(getResources().getString(R.string.whitedischargewithoutabdominalpain) + " - " + " - " + aq.id(R.id.txtvaginaldischargediagnosisval).getText().toString());
                arrLSymptomsSave.add(getResources().getString(R.string.whitedischargewithoutabdominalpainsave) + " - " + " - " + getResources().getString(R.string.rtistisave));
            }
            phcCnt++;
        }


        // Watery fluids
        if (aq.id(R.id.chkleakingofwateryfluidspv).isChecked()) { //15Oct2019 - Bindu - change chkbx listener
            arrLSymptoms.add(aq.id(R.id.chkleakingofwateryfluidspv).getText().toString() + " - " + " - " + aq.id(R.id.txtwateryfluidsdiagnosisval).getText().toString());
            arrLSymptomsSave.add(getResources().getString(R.string.leakingofwateryfluidspvsave) + " - " + " - " + getResources().getString(R.string.prematureorprelaborsave));

            cemocCnt++;
        }

        //To be corrected
        /*if(aq.id(R.id.rdfoetalmovementpresent).isChecked()) {
            arrLSymptoms.add(aq.id(R.id.txtfoetalmovement).getText().toString() + " " + aq.id(R.id.rdfoetalmovementpresent).getText().toString());
        }*/

        /*if(aq.id(R.id.chkvaginaldischarge).isChecked() && aq.id(R.id.chkabdominalpain).isChecked()) {
            arrLSymptomsId.add("116");
        }else if(aq.id(R.id.chkvaginaldischarge).isChecked() && !(aq.id(R.id.chkabdominalpain).isChecked())) {
            arrLSymptomsId.add("115");
        }*/

        // Malpresentation
        if (aq.id(R.id.chkmalpresentation).isChecked()) {
            arrLSymptoms.add(aq.id(R.id.chkmalpresentation).getText().toString());
            arrLSymptomsSave.add(getResources().getString(R.string.malpresentationsave));

            phcCnt++;
        }

        // Twins
        if (aq.id(R.id.chktwinsmultiplepregHV).isChecked()) {
            arrLSymptoms.add(aq.id(R.id.chktwinsmultiplepregHV).getText().toString());
            arrLSymptomsSave.add(getResources().getString(R.string.twinmultiplepregsave));

            phcCnt++;
        }

        //IUGR
        if (aq.id(R.id.chkiugrHV).isChecked()) {
            arrLSymptoms.add(aq.id(R.id.chkiugrHV).getText().toString());
            arrLSymptomsSave.add(getResources().getString(R.string.iugrsave));
            phcCnt++;
        }

        if (aq.id(R.id.chkcpd).isChecked()) {
            arrLSymptoms.add(getResources().getString(R.string.cpdorcontractedpelvis));
            arrLSymptomsSave.add(getResources().getString(R.string.cpdorcontractedpelvissave));
            cemocCnt++;
        }

        if (aq.id(R.id.chklaborpaingt12hrs).isChecked()) {
            arrLSymptoms.add(aq.id(R.id.chklaborpaingt12hrs).getText().toString());
            arrLSymptomsSave.add(getResources().getString(R.string.laborpainmorethan12hourssave));
            cemocCnt++;
        }

        if (aq.id(R.id.chkprevcsorsurgery).isChecked()) {
            arrLSymptoms.add(aq.id(R.id.chkprevcsorsurgery).getText().toString());
            arrLSymptomsSave.add(getResources().getString(R.string.previouscsorsurgerysave));
            cemocCnt++;
        }

        if (aq.id(R.id.chkbadobh).isChecked()) {
            arrLSymptoms.add(getResources().getString(R.string.badobh));
            arrLSymptomsSave.add(getResources().getString(R.string.badobhsave));
            cemocCnt++;
        }

        if (aq.id(R.id.chkDiabetesHV).isChecked()) {
            arrLSymptoms.add(aq.id(R.id.chkDiabetesHV).getText().toString());
            arrLSymptomsSave.add(getResources().getString(R.string.diabetetsmellitussave));
            cemocCnt++;
        }

        if (aq.id(R.id.chkheartdiseaseHV).isChecked()) {
            arrLSymptoms.add(aq.id(R.id.chkheartdiseaseHV).getText().toString());
            arrLSymptomsSave.add(getResources().getString(R.string.chkheartdiseaseHVsave));
            cemocCnt++;
        }

        if (aq.id(R.id.chkAsthamaHV).isChecked()) {
            arrLSymptoms.add(aq.id(R.id.chkAsthamaHV).getText().toString());
            arrLSymptomsSave.add(getResources().getString(R.string.chkAsthamaHVsave));
            cemocCnt++;
        }

        if (aq.id(R.id.chkTBHV).isChecked()) {
            arrLSymptoms.add(aq.id(R.id.chkTBHV).getText().toString());
            arrLSymptomsSave.add(getResources().getString(R.string.chkTBHVsave));
            cemocCnt++;
        }

        if (aq.id(R.id.chkCancerHv).isChecked()) {
            arrLSymptoms.add(aq.id(R.id.chkCancerHv).getText().toString());
            arrLSymptomsSave.add(getResources().getString(R.string.cancersave));
            cemocCnt++;
        }

        if (aq.id(R.id.chkneurologicalHv).isChecked()) {
            arrLSymptoms.add(aq.id(R.id.chkneurologicalHv).getText().toString());
            arrLSymptomsSave.add(getResources().getString(R.string.neurologicaldisordersave));
            cemocCnt++;
        }

        if (aq.id(R.id.chkpsychiatricHv).isChecked()) {
            arrLSymptoms.add(aq.id(R.id.chkpsychiatricHv).getText().toString());
            arrLSymptomsSave.add(getResources().getString(R.string.psychiatricdisordersave));
            cemocCnt++;
        }

        if (aq.id(R.id.chkRenalHv).isChecked()) {
            arrLSymptoms.add(aq.id(R.id.chkRenalHv).getText().toString());
            arrLSymptomsSave.add(getResources().getString(R.string.renaldisordersave));
            cemocCnt++;
        }

        if (aq.id(R.id.chkothermedicalillness).isChecked()) {
            arrLSymptoms.add(getResources().getString(R.string.medicallillness) + " " + aq.id(R.id.chkothermedicalillness).getText().toString() + " : " + aq.id(R.id.etothermedicalillness).getText().toString());
            arrLSymptomsSave.add(getResources().getString(R.string.medicallillnesssave) + " " + getResources().getString(R.string.othersave) + " : " + aq.id(R.id.etothermedicalillness).getText().toString());
            cemocCnt++;
        }

        aq.id(R.id.txtDangSigns).text("");
        if (arrLSymptoms.size() > 0) {
            aq.id(R.id.llsummsymtoms).visible();
            aq.id(R.id.txtDangSigns).visible();
            String strPlace[] = (String[]) arrLSymptoms.toArray(new String[arrLSymptoms.size()]);
            for (int j = 0; j < strPlace.length; j++) {
                if (j == 0)
                    aq.id(R.id.txtDangSigns).getTextView().append(" * " + strPlace[j]);
                else
                    aq.id(R.id.txtDangSigns).getTextView().append(strPlace[j]);
                if (j + 1 != arrLSymptoms.size())
                    //aq.id(R.id.txtDangSigns).getTextView().append(", ");
                    aq.id(R.id.txtDangSigns).getTextView().append("\n * ");

            }

            //13Nov2019 - Bindu
            /*int identifier = getResources().getIdentifier(getResources().getResourceEntryName(R.id.txtvomitingdiagnosisval).toLowerCase(),
                    "string", "com.sc.stmansi");
            Toast.makeText(act, "Val" + val, Toast.LENGTH_SHORT).show();*/

            aq.id(R.id.txtDangSignsEng).text("");
            String strdangersigns[] = arrLSymptomsSave.toArray(new String[arrLSymptomsSave.size()]);
            for (int j = 0; j < strdangersigns.length; j++) {
                if (j == 0)
                    aq.id(R.id.txtDangSignsEng).getTextView().append(" * " + strdangersigns[j]);
                else
                    aq.id(R.id.txtDangSignsEng).getTextView().append(strdangersigns[j]);
                if (j + 1 != arrLSymptomsSave.size())
                    //aq.id(R.id.txtDangSigns).getTextView().append(", ");
                    aq.id(R.id.txtDangSignsEng).getTextView().append("\n * ");

            }
        } else {
            aq.id(R.id.llsummsymtoms).gone();
            aq.id(R.id.txtDangSigns).gone();
        }

        expandcollapseSymptoms();

        //08Apr2021 Bindu
       /* if(cemocCnt > 0) {
            aq.id(R.id.txtapprefferalcentre).text(getResources().getString(R.string.referwomantocemoc));
            aq.id(R.id.txtapprefferalcentre).visible();
            startAnimation();
            setFacilityNames("cemoc");
        }
        else if(phc247Cnt > 0 || phcCnt > 0) {
            aq.id(R.id.txtapprefferalcentre).text(getResources().getString(R.string.referwomantobemoc));
            aq.id(R.id.txtapprefferalcentre).visible();
            startAnimation();
            setFacilityNames("phc");
        }*/ //08Apr2021 Bindu hide referral

        /*else if(phc247Cnt > 0) {
            aq.id(R.id.txtapprefferalcentre).text(getResources().getString(R.string.referwomantophc247));
            aq.id(R.id.txtapprefferalcentre).visible();
            startAnimation();
            setFacilityNames("phc247");
        }
        else if(phcCnt > 0) {
            aq.id(R.id.txtapprefferalcentre).text(getResources().getString(R.string.referwomantophc));
            aq.id(R.id.txtapprefferalcentre).visible();
            startAnimation();
            setFacilityNames("phc");
        }*/
        //08Apr2021 Bindu referral flow
        if (cemocCnt > 0 || phc247Cnt > 0 || phcCnt > 0) {
            aq.id(R.id.txtapprefferalcentre).text(getResources().getString(R.string.referwoman));
            aq.id(R.id.txtapprefferalcentre).visible();
            startAnimation();
            setFacilityType();
        } else {
            aq.id(R.id.txtapprefferalcentre).text("");
            aq.id(R.id.txtapprefferalcentre).gone();
            aq.id(R.id.chkisreferred).checked(false);
            aq.id(R.id.spnfacnameHv).setSelection(0);
            aq.id(R.id.etfacname).text("");
            aq.id(R.id.trspnfacname).gone();
            aq.id(R.id.tretfacname).gone();
            //08Apr2021 Bindu
            aq.id(R.id.spnfactype).setSelection(0);
            aq.id(R.id.trspnfactype).gone();
            aq.id(R.id.trrefslipno).gone();
            aq.id(R.id.etrefslipnumber).text("");

            setRefFacilityName("");
            //setFacilityNames("");
        }

        setAdvise();
        /*prepareSignsandInvestigationData();

        prepareVisitHeaderData();
        prepareVisitDetailsData();*/

    }

    // Set Advise array
    private void setAdvise() {
        arrLSymptomsadvise = new ArrayList<>();
        //14Nov2019 - Bindu
        arrLSymptomsadviseSave = new ArrayList<>();

        if (aq.id(R.id.chkvomitingduringfirst3months).isChecked() && aq.id(R.id.txtvomitinginterpretationval).getText().toString().equalsIgnoreCase(getResources().getString(R.string.nodehydration))) {
            arrLSymptomsadvise.add(getResources().getString(R.string.vomitting) + " - " + getResources().getString(R.string.eatsmallfrequentfoods) + ", " + getResources().getString(R.string.avoidoilyfood) + ", " + getResources().getString(R.string.eatveganddrinkfluids));
            arrLSymptomsadviseSave.add(getResources().getString(R.string.vomittingsave) + " - " + getResources().getString(R.string.eatsmallfrequentfoodssave) + ", " + getResources().getString(R.string.avoidoilyfoodsave) + ", " + getResources().getString(R.string.eatveganddrinkfluidssave));
        }

        if (aq.id(R.id.chkfever).isChecked()) {
            arrLSymptomsadvise.add(getResources().getString(R.string.fever) + " - " + getResources().getString(R.string.giveparacetamol));
            arrLSymptomsadviseSave.add(getResources().getString(R.string.feversave) + " - " + getResources().getString(R.string.giveparacetamolsave));
        }

        if (aq.id(R.id.chkheartburn).isChecked() && aq.id(R.id.chknausea).isChecked()) {
            arrLSymptomsadvise.add(getResources().getString(R.string.heartburnandnausea) + " - " + getResources().getString(R.string.advisespicyfood) + ", " + getResources().getString(R.string.takecoldmilk));
            arrLSymptomsadviseSave.add(getResources().getString(R.string.heartburnandnauseasave) + " - " + getResources().getString(R.string.advisespicyfoodsave) + ", " + getResources().getString(R.string.takecoldmilksave));
        }

        if (aq.id(R.id.chkpalpitations).isChecked() && aq.id(R.id.chkeasyfatiguability).isChecked() && aq.id(R.id.chkbreathlessnessatrest).isChecked()) {
            arrLSymptomsadvise.add(getResources().getString(R.string.palpfatbreathe) + " - " + getResources().getString(R.string.startifa) + ", " + getResources().getString(R.string.gethbtested) + (week > 13 ? ", " + getResources().getString(R.string.givealbendazole) : ""));
            arrLSymptomsadviseSave.add(getResources().getString(R.string.palpfatbreathesave) + " - " + getResources().getString(R.string.startifasave) + ", " + getResources().getString(R.string.gethbtestedsave) + (week > 13 ? ", " + getResources().getString(R.string.givealbendazolesave) : ""));
        } else {     //15Oct2019 - Bindu - check for any 2 symptoms checked
            if (aq.id(R.id.chkpalpitations).isChecked() && aq.id(R.id.chkeasyfatiguability).isChecked()) {
                arrLSymptomsadvise.add(getResources().getString(R.string.palpitations) + ", " + getResources().getString(R.string.easyfatiguability) + " - " + getResources().getString(R.string.startifa) + ", " + getResources().getString(R.string.gethbtested) + (week > 13 ? ", " + getResources().getString(R.string.givealbendazole) : ""));
                arrLSymptomsadviseSave.add(getResources().getString(R.string.palpitationssave) + ", " + getResources().getString(R.string.easyfatiguabilitysave) + " - " + getResources().getString(R.string.startifasave) + ", " + getResources().getString(R.string.gethbtestedsave) + (week > 13 ? ", " + getResources().getString(R.string.givealbendazolesave) : ""));
            }
            if (aq.id(R.id.chkpalpitations).isChecked() && aq.id(R.id.chkbreathlessnessatrest).isChecked()) {
                arrLSymptomsadvise.add(getResources().getString(R.string.palpitations) + ", " + getResources().getString(R.string.breathlessnessatrest) + " - " + getResources().getString(R.string.startifa) + ", " + getResources().getString(R.string.gethbtested) + (week > 13 ? ", " + getResources().getString(R.string.givealbendazole) : ""));
                arrLSymptomsadviseSave.add(getResources().getString(R.string.palpitationssave) + ", " + getResources().getString(R.string.breathlessnessatrestsave) + " - " + getResources().getString(R.string.startifasave) + ", " + getResources().getString(R.string.gethbtestedsave) + (week > 13 ? ", " + getResources().getString(R.string.givealbendazolesave) : ""));
            }
            if (aq.id(R.id.chkeasyfatiguability).isChecked() && aq.id(R.id.chkbreathlessnessatrest).isChecked()) {
                arrLSymptomsadvise.add(getResources().getString(R.string.easyfatiguability) + ", " + getResources().getString(R.string.breathlessnessatrest) + " - " + getResources().getString(R.string.startifa) + ", " + getResources().getString(R.string.gethbtested) + (week > 13 ? ", " + getResources().getString(R.string.givealbendazole) : ""));
                arrLSymptomsadviseSave.add(getResources().getString(R.string.easyfatiguabilitysave) + ", " + getResources().getString(R.string.breathlessnessatrestsave) + " - " + getResources().getString(R.string.startifasave) + ", " + getResources().getString(R.string.gethbtestedsave) + (week > 13 ? ", " + getResources().getString(R.string.givealbendazolesave) : ""));
            }
        }

        String edema = "", edemasave = ""; //14Nov2019 - Bindu
        if (aq.id(R.id.chkpuffinessofface).isChecked()) {
            edema = getResources().getString(R.string.puffinessoftheface);
            edemasave = getResources().getString(R.string.puffinessofthefacesave);
        }

        if (aq.id(R.id.chkgenbodyoedema).isChecked()) {
            edema = edema.length() > 0 ? edema + ", " + getResources().getString(R.string.generalizedbodyodema) : getResources().getString(R.string.generalizedbodyodema);
            edemasave = edemasave.length() > 0 ? edemasave + ", " + getResources().getString(R.string.generalizedbodyodemasave) : getResources().getString(R.string.generalizedbodyodemasave);
        }

        if (edema.length() > 0) {
            arrLSymptomsadvise.add(edema + " - " + getResources().getString(R.string.adviserestandregularfollowup));
            arrLSymptomsadviseSave.add(edemasave + " - " + getResources().getString(R.string.adviserestandregularfollowupsave));
        }

        if (aq.id(R.id.chkconstipation).isChecked()) {
            arrLSymptomsadvise.add(getResources().getString(R.string.constipation) + " - " + getResources().getString(R.string.adviseleafyveg));
            arrLSymptomsadviseSave.add(getResources().getString(R.string.constipationsave) + " - " + getResources().getString(R.string.adviseleafyvegsave));

        }

        if (aq.id(R.id.chkincreasedurination).isChecked() || aq.id(R.id.chkurinationafter12wks).isChecked() || aq.id(R.id.chkburningurination).isChecked()) {
            arrLSymptomsadvise.add(getResources().getString(R.string.urinationproblem) + " - " + getResources().getString(R.string.plentyoforalfluids));
            arrLSymptomsadviseSave.add(getResources().getString(R.string.urinationproblemsave) + " - " + getResources().getString(R.string.plentyoforalfluidssave));
        }

        if (cemocCnt > 0) {
            arrLSymptomsadvise.add("ACTION -  " + getResources().getString(R.string.refertocemoc));
            arrLSymptomsadviseSave.add("ACTION -  " + getResources().getString(R.string.refertocemoc));
        } else if (phcCnt > 0 || phc247Cnt > 0) {
            arrLSymptomsadvise.add("ACTION -  " + getResources().getString(R.string.refertobemoc));
            arrLSymptomsadviseSave.add("ACTION -  " + getResources().getString(R.string.refertobemoc)); //03MAy2021 Bindu - change to bemoc from cemoc
        }
        /*else if(phc247Cnt > 0) {
            arrLSymptomsadvise.add("ACTION -  " + getResources().getString(R.string.referwomantophc247));
        }else if(phcCnt > 0) {
            arrLSymptomsadvise.add("ACTION -  " + getResources().getString(R.string.refertophc));
        }*/


        // Advise summary
        aq.id(R.id.txtadvisesumm).text("");
        aq.id(R.id.txtadvisesummEng).text("");
        if (arrLSymptomsadvise.size() > 0) {
            aq.id(R.id.llsummadvise).visible();
            aq.id(R.id.txtadvisesumm).visible();
            String strPlace[] = (String[]) arrLSymptomsadvise.toArray(new String[arrLSymptomsadvise.size()]);
            String stradv[] = arrLSymptomsadviseSave.toArray(new String[arrLSymptomsadviseSave.size()]);
            for (int j = 0; j < strPlace.length; j++) {
                if (j == 0)
                    aq.id(R.id.txtadvisesumm).getTextView().append(" * " + strPlace[j]);
                else
                    aq.id(R.id.txtadvisesumm).getTextView().append(strPlace[j]);
                if (j + 1 != arrLSymptomsadvise.size())
                    aq.id(R.id.txtadvisesumm).getTextView().append("\n * ");
            }

            for (int j = 0; j < stradv.length; j++) {
                if (j == 0)
                    aq.id(R.id.txtadvisesummEng).getTextView().append(" * " + stradv[j]);
                else
                    aq.id(R.id.txtadvisesummEng).getTextView().append(stradv[j]);
                if (j + 1 != arrLSymptomsadviseSave.size())
                    aq.id(R.id.txtadvisesummEng).getTextView().append("\n * ");
            }
        } else {
            aq.id(R.id.llsummadvise).gone();
            aq.id(R.id.txtadvisesumm).gone();
        }
        expandcollapseAdvise();
    }

    // Expand or Collapse Symtpoms drawable
    private void expandcollapseSymptoms() {
        if (aq.id(R.id.txtDangSigns).getText().toString().length() > 0) {
            aq.id(R.id.txtDangSignsL).getTextView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_colsym, 0, 0, 0);
        } else {
            aq.id(R.id.txtDangSignsL).getTextView().setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
    }

    // Expand or Collapse Advise drawable
    private void expandcollapseAdvise() {
        if (aq.id(R.id.txtadvisesumm).getText().toString().length() > 0) {
            aq.id(R.id.txtadvisegiven).getTextView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_colsym, 0, 0, 0);
        } else {
            aq.id(R.id.txtadvisegiven).getTextView().setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
    }

    private void prepareSignsandInvestigationData() {
        insertSignsInvestVal = new LinkedHashMap<>();
        insertSignsInterpretationVal = new LinkedHashMap<>();
        insertDiagnosisVal = new LinkedHashMap<>();
        ArrayList<String> arrSigns = new ArrayList<>();

        insertSymRefVal = new LinkedHashMap<>();

        //Fever and other symptoms

        if (aq.id(R.id.chkfever).isChecked())
            insertSignsInvestVal.put("142", arrSigns);

        if (aq.id(R.id.chkcough).isChecked())
            insertSignsInvestVal.put("143", arrSigns);

        if (aq.id(R.id.chkcold).isChecked())
            insertSignsInvestVal.put("144", arrSigns);

        if (aq.id(R.id.chkjaundice).isChecked())
            insertSignsInvestVal.put("145", arrSigns);

        if (aq.id(R.id.chkwhitedischarge).isChecked())
            insertSignsInvestVal.put("146", arrSigns);

        if (aq.id(R.id.chkpaininabdomen).isChecked()) {
            insertSignsInvestVal.put("147", arrSigns);
            insertDiagnosisVal.put("147", "3011");
        }

        if (aq.id(R.id.chkothersymptoms).isChecked())
            insertSignsInvestVal.put("149", arrSigns);


        //Vomiting
        arrSigns = new ArrayList<>();
        int i = 0;
        if (aq.id(R.id.chkdrytongue).isChecked()) {
            arrSigns.add("1001");
            i++;
        }
        if (aq.id(R.id.chkfeeblepulse).isChecked()) {
            arrSigns.add("1002");
            i++;
        }
        if (aq.id(R.id.chkdrydryconjunctiva).isChecked()) {
            arrSigns.add("1003");
            i++;
        }
        if (aq.id(R.id.chkbplow).isChecked()) {
            arrSigns.add("1004");
            i++;
        }
        if (aq.id(R.id.chkdecurineoutput).isChecked()) {
            arrSigns.add("1005");
            i++;
        }

        if (arrSigns.size() > 0) {
            insertSignsInvestVal.put("100", arrSigns);
            if (i >= 3) {
                insertSignsInterpretationVal.put("100", "2002");

                if (week < 14)
                    insertSymRefVal.put("100", getResources().getString(R.string.strphc));
                else
                    insertSymRefVal.put("100", getResources().getString(R.string.strphc247));
            } else if (i > 0 && i < 3) {
                insertSignsInterpretationVal.put("100", "2001");
                insertDiagnosisVal.put("100", "3001");
            }
        } else {
            if (aq.id(R.id.chkvomitingduringfirst3months).isChecked())
                insertSignsInvestVal.put("100", arrSigns);
        }

        //Palpitations
        arrSigns = new ArrayList<>();
        if (aq.id(R.id.chkfastordiffbreathing).isChecked()) {
            arrSigns.add("1006");
        }

        // if(arrSigns.size() > 0) {
        int j = 0;
        if (aq.id(R.id.chkpalpitations).isChecked()) {
            insertSignsInvestVal.put("101", arrSigns);
            j++;
        }
        if (aq.id(R.id.chkeasyfatiguability).isChecked()) {
            insertSignsInvestVal.put("102", arrSigns);
            j++;
        }
        if (aq.id(R.id.chkbreathlessnessatrest).isChecked()) {
            insertSignsInvestVal.put("103", arrSigns);
            j++;
        }

        if (j == 3) {
            if (aq.id(R.id.ethbpalp).getText().toString().length() > 0) {
                double hb = Double.parseDouble(aq.id(R.id.ethbpalp).getText().toString());
                if (hb > 0 && (hb > 6 || hb < 10)) {
                    insertDiagnosisVal.put("101", "3003");
                    insertDiagnosisVal.put("102", "3003");
                    insertDiagnosisVal.put("103", "3003");
                }
            } else {
                insertDiagnosisVal.put("101", "3002");
                insertDiagnosisVal.put("102", "3002");
                insertDiagnosisVal.put("103", "3002");
            }

            insertSymRefVal.put("101", getResources().getString(R.string.strcemoc));
            insertSymRefVal.put("102", getResources().getString(R.string.strcemoc));
            insertSymRefVal.put("103", getResources().getString(R.string.strcemoc));
        }
       /* }else {
            if(aq.id(R.id.chkpalpitations).isChecked())
                insertSignsInvestVal.put("101", arrSigns);
            if(aq.id(R.id.chkeasyfatiguability).isChecked())
                insertSignsInvestVal.put("102", arrSigns);
            if(aq.id(R.id.chkbreathlessnessatrest).isChecked())
                insertSignsInvestVal.put("103", arrSigns);
        }*/

        //Edema
        arrSigns = new ArrayList<>();
        if (aq.id(R.id.chkbp).isChecked())
            arrSigns.add("1007");
        if (aq.id(R.id.chksevereheadache).isChecked())
            arrSigns.add("1008");
        if (aq.id(R.id.chkblurredvision).isChecked())
            arrSigns.add("1009");

        //  if(arrSigns.size() > 0) {
        if (aq.id(R.id.chkpuffinessofface).isChecked()) {
            insertSignsInvestVal.put("104", arrSigns);
            insertDiagnosisVal.put("104", "3003");
            insertSymRefVal.put("104", getResources().getString(R.string.strphc247orcemoc));
        }
        if (aq.id(R.id.chkgenbodyoedema).isChecked()) {
            insertSignsInvestVal.put("105", arrSigns);
            insertDiagnosisVal.put("105", "3003");
            insertSymRefVal.put("105", getResources().getString(R.string.strphc247orcemoc));
        }

        // }


        //heartburn and nausea
        arrSigns = new ArrayList<>();
        if (aq.id(R.id.chkheartburn).isChecked() && aq.id(R.id.chknausea).isChecked()) {
            arrSigns.add("1010");
            insertSignsInterpretationVal.put("106", "2003");
            insertDiagnosisVal.put("106", "3004");
            insertSymRefVal.put("106", getResources().getString(R.string.phc));
        }
        //if(arrSigns.size() > 0) {  //14Nov2019 - Bindu
        if (aq.id(R.id.chkheartburn).isChecked())
            insertSignsInvestVal.put("106", arrSigns);  //Heartburn
        if (aq.id(R.id.chknausea).isChecked())
            insertSignsInvestVal.put("107", arrSigns);  //Nausea
        //}

        //Urination pbm
        arrSigns = new ArrayList<>();
        /*if(aq.id(R.id.chkpaininabdomen).isChecked())
            arrSigns.add("1011");*/
        /*if(aq.id(R.id.chkfever).isChecked() && aq.id(R.id.rdmildfeveru).isChecked())
            arrSigns.add("1012");
        if(aq.id(R.id.chkfever).isChecked() && aq.id(R.id.rdhighfeveru).isChecked())
            arrSigns.add("1013");*/

        // if(arrSigns.size() > 0)
        if (aq.id(R.id.chkincreasedurination).isChecked()) {
            insertSignsInvestVal.put("108", arrSigns);
            if (aq.id(R.id.chkfever).isChecked())
                insertDiagnosisVal.put("108", "3005");
        }
        if (aq.id(R.id.chkurinationafter12wks).isChecked()) {
            insertSignsInvestVal.put("109", arrSigns);
            if (aq.id(R.id.chkfever).isChecked())
                insertDiagnosisVal.put("109", "3005");
        }
        if (aq.id(R.id.chkburningurination).isChecked()) {
            insertSignsInvestVal.put("110", arrSigns);
            if (aq.id(R.id.chkfever).isChecked())
                insertDiagnosisVal.put("110", "3005");
        }

        //Constipation
        arrSigns = new ArrayList<>();
        if (aq.id(R.id.chkconstipation).isChecked()) {
            if (aq.id(R.id.chkconstipationnotrelieved).isChecked()) {
                insertSymRefVal.put("111", getResources().getString(R.string.strphc));
                insertDiagnosisVal.put("111", "3006"); //25Jul2021 Bindu
                arrSigns.add("1014");
            }
            insertSignsInvestVal.put("111", arrSigns);
        }


        // Bleeding pv
        arrSigns = new ArrayList<>();
        if (aq.id(R.id.chkbleedingpvbefore20wks).isChecked()) {
            insertSignsInvestVal.put("112", arrSigns);
            insertSymRefVal.put("112", getResources().getString(R.string.strphc));
            insertDiagnosisVal.put("112", "3008");
        } else if (aq.id(R.id.chkbleedingpvafter20wks).isChecked()) {
            insertSignsInvestVal.put("113", arrSigns);
            insertSymRefVal.put("113", getResources().getString(R.string.strcemoc));
            insertDiagnosisVal.put("113", "3007"); //25Jul2021 Bindu - change to 3007 from 3009
        }

        //Fever
        /*if(aq.id(R.id.chkfever).isChecked() && aq.id(R.id.rdmildfever).isChecked())
            insertSignsInvestVal.put("114", arrSigns);
        else if(aq.id(R.id.chkfever).isChecked() && aq.id(R.id.rdhighfever).isChecked())
            insertSignsInvestVal.put("115", arrSigns);*/

        //Fetal movement
        if (aq.id(R.id.rdfoetalmovementdecreased).isChecked()) {
            insertSignsInvestVal.put("116", arrSigns);
            insertSymRefVal.put("116", getResources().getString(R.string.strcemoc));
        } else if (aq.id(R.id.rdfoetalmovementabsent).isChecked()) {
            insertSignsInvestVal.put("117", arrSigns);
            insertSymRefVal.put("117", getResources().getString(R.string.strcemoc));
        }

        //vaginal discharge
        if (aq.id(R.id.chkvaginaldischarge).isChecked() && aq.id(R.id.chkabdominalpain).isChecked()) {
            insertSignsInvestVal.put("118", arrSigns);
            insertSymRefVal.put("118", getResources().getString(R.string.strphc));
            insertDiagnosisVal.put("118", "3009"); //25Jul2021 Bindu
        } else if (aq.id(R.id.chkvaginaldischarge).isChecked() && !(aq.id(R.id.chkabdominalpain).isChecked())) {
            insertSignsInvestVal.put("119", arrSigns);
            insertSymRefVal.put("119", getResources().getString(R.string.strphc));
            insertDiagnosisVal.put("118", "3009"); //25Jul2021 Bindu
        }

        //Leaking of watery fluids
        arrSigns = new ArrayList<>();
        if (aq.id(R.id.chkpads).isChecked())
            arrSigns.add("1015");
        if (aq.id(R.id.chkleakingofwateryfluidspv).isChecked()) { //24Dec2019 - Bindu - change from watery fluids to Leakingofwaterfyfluid - wrong id was mapped
            insertSignsInvestVal.put("120", arrSigns);
            insertSymRefVal.put("120", getResources().getString(R.string.strcemoc));
            insertDiagnosisVal.put("120", "3010");
        }

        //Severe  headache and blurred vision
        arrSigns = new ArrayList<>();
        if (aq.id(R.id.chksevereheadachesymp).isChecked()) {
            insertSignsInvestVal.put("121", arrSigns);
            insertSymRefVal.put("121", getResources().getString(R.string.strcemoc));
        }
        if (aq.id(R.id.chkblurredvisionsymp).isChecked()) {
            insertSignsInvestVal.put("122", arrSigns);
            insertSymRefVal.put("122", getResources().getString(R.string.strcemoc));
        }

        // Fits/loss of consciousness
        if (aq.id(R.id.chkfits).isChecked()) {
            insertSignsInvestVal.put("123", arrSigns);
            insertSymRefVal.put("123", getResources().getString(R.string.strcemoc));
        }
        if (aq.id(R.id.chklossofconsciousness).isChecked()) {
            insertSignsInvestVal.put("124", arrSigns);
            insertSymRefVal.put("124", getResources().getString(R.string.strcemoc));
        }

        // Labor pain
        if (aq.id(R.id.chklaborpaingt12hrs).isChecked()) {
            insertSignsInvestVal.put("125", arrSigns);
            insertSymRefVal.put("125", getResources().getString(R.string.strcemoc));
        }
        //continuos abdominal pain
        if (aq.id(R.id.chkcontinuousabdominalpain).isChecked()) {
            insertSignsInvestVal.put("126", arrSigns);
            insertSymRefVal.put("126", getResources().getString(R.string.strcemoc));
        }


//        //moderte anemia - remove moderate anemia symptom
//        if(aq.id(R.id.chkmoderateanemia).isChecked())
//            insertSignsInvestVal.put("127", arrSigns);
        //previous cs or surgery
        if (aq.id(R.id.chkprevcsorsurgery).isChecked()) {
            insertSignsInvestVal.put("128", arrSigns);
            insertSymRefVal.put("128", getResources().getString(R.string.strcemoc));
        }
        //mothers with cpd or contracted pelvis
        if (aq.id(R.id.chkcpd).isChecked()) {
            insertSignsInvestVal.put("129", arrSigns);
            insertSymRefVal.put("129", getResources().getString(R.string.strcemoc));
        }
        //bad obh
        if (aq.id(R.id.chkbadobh).isChecked()) {
            insertSignsInvestVal.put("130", arrSigns);
            insertSymRefVal.put("130", getResources().getString(R.string.strcemoc));
        }
        //malpresentation
        if (aq.id(R.id.chkmalpresentation).isChecked()) {
            insertSignsInvestVal.put("148", arrSigns);
            insertSymRefVal.put("148", getResources().getString(R.string.strbemoc));
        }
        //twinsormultiplepreg
        if (aq.id(R.id.chktwinsmultiplepregHV).isChecked()) {
            insertSignsInvestVal.put("131", arrSigns);
            insertSymRefVal.put("131", getResources().getString(R.string.strbemoc));
        }

        //IUGR
        if (aq.id(R.id.chkiugrHV).isChecked()) {
            insertSignsInvestVal.put("132", arrSigns);
            insertSymRefVal.put("132", getResources().getString(R.string.strbemoc));
        }

        //Medical illness
        if (aq.id(R.id.chkDiabetesHV).isChecked()) {
            insertSignsInvestVal.put("133", arrSigns);
            insertSymRefVal.put("133", getResources().getString(R.string.strcemoc));
        }
        if (aq.id(R.id.chkheartdiseaseHV).isChecked()) {
            insertSignsInvestVal.put("134", arrSigns);
            insertSymRefVal.put("134", getResources().getString(R.string.strcemoc));
        }
        if (aq.id(R.id.chkAsthamaHV).isChecked()) {
            insertSignsInvestVal.put("135", arrSigns);
            insertSymRefVal.put("135", getResources().getString(R.string.strcemoc));
        }
        if (aq.id(R.id.chkTBHV).isChecked()) {
            insertSignsInvestVal.put("136", arrSigns);
            insertSymRefVal.put("136", getResources().getString(R.string.strcemoc));
        }
        if (aq.id(R.id.chkneurologicalHv).isChecked()) {
            insertSignsInvestVal.put("137", arrSigns);
            insertSymRefVal.put("137", getResources().getString(R.string.strcemoc));
        }
        if (aq.id(R.id.chkpsychiatricHv).isChecked()) {
            insertSignsInvestVal.put("138", arrSigns);
            insertSymRefVal.put("138", getResources().getString(R.string.strcemoc));
        }
        if (aq.id(R.id.chkRenalHv).isChecked()) {
            insertSignsInvestVal.put("139", arrSigns);
            insertSymRefVal.put("139", getResources().getString(R.string.strcemoc));
        }
        if (aq.id(R.id.chkCancerHv).isChecked()) {
            insertSignsInvestVal.put("140", arrSigns);
            insertSymRefVal.put("140", getResources().getString(R.string.strcemoc));
        }
        if (aq.id(R.id.chkothermedicalillness).isChecked()) {
            insertSignsInvestVal.put("141", arrSigns);
            insertSymRefVal.put("141", getResources().getString(R.string.strcemoc));
        }
    }

    //Prepare data for Visit Details
    private void prepareVisitDetailsData() {
        try {
            tblvisD = new TblVisitDetails();
            tblvisD.setUserId(user.getUserId());
            tblvisD.setWomanId(woman.getWomanId());
            tblvisD.setVisitNum(visitNum);
            tblvisD.setVisDVisitType(woman.getRegPregnantorMother() == 1 ? getResources().getString(R.string.stranc) : getResources().getString(R.string.strpnc));
            tblvisD.setVisDVisitDate(aq.id(R.id.etvisitdate).getText().toString());

            tblvisD.setVisDReferredToFacilityType(""); //To be corrected

            tblvisD.setVisHUserType(appState.userType);
            tblvisD.setRecordcreateddate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
            tblvisD.setRecordupdateddate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
        } catch (Exception e) {

        }
    }

    //Prepare data for Visit Header
    private void prepareVisitHeaderData() {
        try {
            //14Nov2019 - Bindu - get one string from eng
            String[] strtabnotgivenreason = getResources().getStringArray(R.array.tabnotgivenreasonsave);
            tblvisH = new TblVisitHeader();
            hmvisitregdetails = new tblregisteredwomen();

            tblvisH.setUserId(user.getUserId());
            tblvisH.setWomanId(woman.getWomanId());
            tblvisH.setVisitNum(visitNum);
            tblvisH.setVisHVisitType(woman.getRegPregnantorMother() == 1 ? getResources().getString(R.string.stranc) : getResources().getString(R.string.strpnc));
            tblvisH.setVisHVisitDate(aq.id(R.id.etvisitdate).getText().toString());
            tblvisH.setVisHStatusAtVisit(strGA);
            tblvisH.setVisHVisitIsAdviseGiven(aq.id(R.id.chkisAdviceGiven).isChecked() ? getResources().getString(R.string.yesshortform) : getResources().getString(R.string.noshortform));

            //mani 18June2021
            String ashaavailability="";
            if (aq.id(R.id.ashavailableYes).isChecked())
                ashaavailability= "Y";
            else if (aq.id(R.id.ashavailableNo).isChecked())
                ashaavailability="N";
            else if (aq.id(R.id.ashavailableDontKnow).isChecked())
                ashaavailability="Dont Know";
            tblvisH.setVisHAshaAvailable(ashaavailability);

            tblvisH.setVisHVisitParacetamolGiven(aq.id(R.id.chkparacetamol).isChecked() ? getResources().getString(R.string.yesshortform) : getResources().getString(R.string.noshortform));
            //22Sep2019 - Bindu - set qty and reason
            tblvisH.setVisHVisitParacetamolQty("" + aq.id(R.id.etnoofparacetamolgiven).getText().toString());

            //16Nov2019 - Bindu - str compare to int
            //if(aq.id(R.id.etnoofparacetamolgiven).getText().toString().equals("0")) {
            if (aq.id(R.id.etnoofparacetamolgiven).getText().toString().trim().length() > 0) {
                int val = Integer.parseInt(aq.id(R.id.etnoofparacetamolgiven).getText().toString());
                if (val == 0) {
                    String notgivenreason = strtabnotgivenreason[aq.id(R.id.spnparacetamolnotgivenreason).getSelectedItemPosition()];
                    //tblvisH.setVisHVisitParacetamolNotGivenReason(aq.id(R.id.spnparacetamolnotgivenreason).getSelectedItem().toString());
                    tblvisH.setVisHVisitParacetamolNotGivenReason(notgivenreason);
                }
            }


            tblvisH.setVisHVisitIFAGiven(aq.id(R.id.chkifa).isChecked() ? getResources().getString(R.string.yesshortform) : getResources().getString(R.string.noshortform));
            tblvisH.setVisHVisitIFAQty("" + aq.id(R.id.etnoofifagiven).getText().toString());
            //16Nov2019 - Bindu - str compare to int
            //if(aq.id(R.id.etnoofifagiven).getText().toString().equals("0")) {
            if (aq.id(R.id.etnoofifagiven).getText().toString().trim().length() > 0) {
                int val = Integer.parseInt(aq.id(R.id.etnoofifagiven).getText().toString());
                if (val == 0) {
                    String notgivenreason = strtabnotgivenreason[aq.id(R.id.spnifanotgivenreason).getSelectedItemPosition()];
                    //tblvisH.setVisHVisitIFANotGivenReason(aq.id(R.id.spnifanotgivenreason).getSelectedItem().toString());
                    tblvisH.setVisHVisitIFANotGivenReason(notgivenreason);
                }
            }
            /*tblvisH.setVisHVisitFSFAGiven(aq.id(R.id.chkfsfa).isChecked() ? getResources().getString(R.string.yesshortform) : getResources().getString(R.string.noshortform));
            tblvisH.setVisHVisitFSFAQty(""+aq.id(R.id.etnooffsfagiven).getText().toString());
            if(aq.id(R.id.etnooffsfagiven).getText().toString().equals("0")) {
                //tblvisH.setVisHVisitFSFANotGivenReason(aq.id(R.id.spnfsfanotgivenreason).getSelectedItem().toString());
                String notgivenreason = strtabnotgivenreason[aq.id(R.id.spnfsfanotgivenreason).getSelectedItemPosition()];
                tblvisH.setVisHVisitFSFANotGivenReason(notgivenreason);
            }*/ //16Nov2019 -Bindu -fsfa details remove

            tblvisH.setVisHVisitAlbendazoleGiven(aq.id(R.id.chkalbedazolegiven).isChecked() ? getResources().getString(R.string.yesshortform) : getResources().getString(R.string.noshortform));
            tblvisH.setVisHVisitAlbendazoleQty("" + aq.id(R.id.etnoofalbendazolegiven).getText().toString());
            //16Nov2019 - Bindu - str compare to int
            //if(aq.id(R.id.etnoofalbendazolegiven).getText().toString().equals("0")) {
            if (aq.id(R.id.etnoofalbendazolegiven).getText().toString().trim().length() > 0) {
                int val = Integer.parseInt(aq.id(R.id.etnoofalbendazolegiven).getText().toString());
                if (val == 0) {
                    String notgivenreason = strtabnotgivenreason[aq.id(R.id.spnalbendazolenotgivenreason).getSelectedItemPosition()];
                    tblvisH.setVisHVisitAlbendazoleNotGivenReason(notgivenreason);
                    //tblvisH.setVisHVisitAlbendazoleNotGivenReason(aq.id(R.id.spnalbendazolenotgivenreason).getSelectedItem().toString());
                }
            }

            String bpsys = aq.id(R.id.etbpsystolicsumm).getText().toString();
            String bpdias = aq.id(R.id.etbpdiastolicsumm).getText().toString();

            if (bpsys.trim().length() > 0 && bpdias.trim().length() > 0) {
                tblvisH.setVisHBP(bpsys + "/" + bpdias);
                //22Sep2019 - BP done date
                tblvisH.setVisHBPDate(aq.id(R.id.etbpdonedate).getText().toString());
            }

            tblvisH.setVisHTemperature(aq.id(R.id.etTemp).getText().toString());
            //22Sep2019 - temp done date
            tblvisH.setVisHTemperatureDate(aq.id(R.id.ettempdonedate).getText().toString());

            String proteinuria = "";
            if (aq.id(R.id.rdproteinuriaabsentsumm).isChecked())
                proteinuria = getResources().getString(R.string.strabsent); //16Nov2019 - Bindu - eng str
            else if (aq.id(R.id.rdproteinuriapresentsumm).isChecked())
                proteinuria = getResources().getString(R.string.strpresent); //16Nov2019 - Bindu - eng str

            tblvisH.setVisHUrineprotein(proteinuria);
            //22Sep2019 - protein uria done date
            tblvisH.setVisHUrineproteinDate(aq.id(R.id.etproteinuriadonedate).getText().toString());

            tblvisH.setVisHHb(aq.id(R.id.ethb).getText().toString());
            //22Sep2019 - hb done date
            tblvisH.setVisHHbDate(aq.id(R.id.ethbdonedate).getText().toString());

            tblvisH.setVisHPulse(aq.id(R.id.etpulse).getText().toString());
            tblvisH.setVisHPulseDate(aq.id(R.id.etpulsedate).getText().toString());

            //02Oct2019 - Bindu - add weight
            tblvisH.setVisHWeight(aq.id(R.id.etweight).getText().toString());
            tblvisH.setVisHWeightDate(aq.id(R.id.etweightdate).getText().toString());

            tblvisH.setVisHVisitIsReferred(aq.id(R.id.chkisreferred).isChecked() ? getResources().getString(R.string.yesshortform) : getResources().getString(R.string.noshortform));


            //  tblvisH.setVisHVisitReferredToFacilityType(""); //To be corrected

            String sel = "";
            if (aq.id(R.id.spnfacnameHv).getSelectedItem() != null)
                sel = aq.id(R.id.spnfacnameHv).getSelectedItem().toString();
            if (sel.trim().length() > 0 && !(sel.equalsIgnoreCase(getResources().getString(R.string.select)) || sel.equalsIgnoreCase(getResources().getString(R.string.other)))) {
                tblvisH.setVisHVisitReferredToFacilityName(aq.id(R.id.spnfacnameHv).getSelectedItem().toString());
                tblvisH.setVisHVisitReferredToFacilityType(aq.id(R.id.spnfactype).getSelectedItem().toString()); //08Apr2021
                /*for (Map.Entry<String, String> village : mapFacilityname.entrySet()) {
                    if(aq.id(R.id.spnfacnameHv).getSelectedItem().toString().equalsIgnoreCase(village.getValue()))
                        tblvisH.setVisHVisitReferredToFacilityType(village.getKey());
                }*/ //08Apr2021 Bindu
            } else if (sel.equalsIgnoreCase(getResources().getString(R.string.other))
                    && aq.id(R.id.etreffacilityname).getText().toString().trim().length() <= 0) {
                tblvisH.setVisHVisitReferredToFacilityName(aq.id(R.id.etreffacilityname).getText().toString());
                tblvisH.setVisHVisitReferredToFacilityType(getResources().getString(R.string.strother)); //14Nov2019 - Bindu
            } else {
                tblvisH.setVisHVisitReferredToFacilityName(aq.id(R.id.etreffacilityname).getText().toString());
                if (aq.id(R.id.etreffacilityname).getText().toString().length() > 0)
                    tblvisH.setVisHVisitReferredToFacilityType(getResources().getString(R.string.strother)); //14Nov2019 - Bindu
                else
                    tblvisH.setVisHVisitReferredToFacilityType("");
            }

            tblvisH.setVisHVisitComments(aq.id(R.id.etvisitcomments).getText().toString());
            if (insertSignsInvestVal.size() > 0)
                tblvisH.setVisHCompl(getResources().getString(R.string.yesshortform));
            else
                tblvisH.setVisHCompl(getResources().getString(R.string.noshortform));
            tblvisH.setVisHResult(aq.id(R.id.txtDangSignsEng).getText().toString()); //14Nov2019 - Bindu
            tblvisH.setVisHAdvise(aq.id(R.id.txtadvisesummEng).getText().toString()); //22Sep2019 - Bindu - Advise //14Nov2019 Bindu- save eng text
            tblvisH.setVisHUserType(appState.userType);
            tblvisH.setRecordcreateddate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
            tblvisH.setRecordupdateddate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
            tblvisH.setVisHReferralSlipNumber("" + aq.id(R.id.etrefslipnumber).getText().toString()); //08Apr2021 Bindu add referal slip number

            // Update tblregisteredwomen details related to home visit date
            if (woman.getRegPregnantorMother() == 1) {
                hmvisitregdetails.setLastAncVisitDate(aq.id(R.id.etvisitdate).getText().toString());
                hmvisitregdetails.setLastPncVisitDate("");
            } else if (woman.getRegPregnantorMother() == 2) {
                hmvisitregdetails.setLastPncVisitDate(aq.id(R.id.etvisitdate).getText().toString());
                hmvisitregdetails.setLastAncVisitDate(woman.getLastAncVisitDate());
            }

            if (woman.getIsCompl() != null && woman.getIsCompl().equalsIgnoreCase(getResources().getString(R.string.yesshortform))) {
                hmvisitregdetails.setIsCompl(getResources().getString(R.string.yesshortform));
            } else {
                if (insertSignsInvestVal.size() > 0)
                    hmvisitregdetails.setIsCompl(getResources().getString(R.string.yesshortform));
                else {
                    /*if (woman.getIsCompl() != null && woman.getIsCompl().equalsIgnoreCase(getResources().getString(R.string.yesshortform)))
                        hmvisitregdetails.setIsCompl(getResources().getString(R.string.yesshortform));
                    else*/
                    hmvisitregdetails.setIsCompl(getResources().getString(R.string.noshortform));
                }
            }

            //27Sep2019 -bindu -Delivery plan
            if (woman.getRegrecommendedPlaceOfDelivery() != null && (woman.getRegrecommendedPlaceOfDelivery().length() <= 0 || woman.getRegrecommendedPlaceOfDelivery().equalsIgnoreCase(getResources().getString(R.string.strbemoc)))) {
                if (cemocCnt > 0)
                    hmvisitregdetails.setRegrecommendedPlaceOfDelivery(getResources().getString(R.string.strcemoc));
                else if (phcCnt > 0 || phc247Cnt > 0)
                    hmvisitregdetails.setRegrecommendedPlaceOfDelivery(getResources().getString(R.string.strbemoc));
                else
                    hmvisitregdetails.setRegrecommendedPlaceOfDelivery(woman.getRegrecommendedPlaceOfDelivery());
            } else {
                hmvisitregdetails.setRegrecommendedPlaceOfDelivery(woman.getRegrecommendedPlaceOfDelivery());
            }

            if (woman.getIsReferred() != null && woman.getIsReferred().equalsIgnoreCase(getResources().getString(R.string.yesshortform)))
                hmvisitregdetails.setIsReferred(getResources().getString(R.string.yesshortform));
            else {
                if (aq.id(R.id.chkisreferred).isChecked())
                    hmvisitregdetails.setIsReferred(getResources().getString(R.string.yesshortform));
                else {
                    /*if (woman.getIsReferred() != null && woman.getIsReferred().equalsIgnoreCase(getResources().getString(R.string.yesshortform)))
                        hmvisitregdetails.setIsReferred(getResources().getString(R.string.yesshortform));
                    else*/
                    hmvisitregdetails.setIsReferred(getResources().getString(R.string.noshortform));
                }
            }

            hmvisitregdetails.setRegUserType(appState.userType);
            hmvisitregdetails.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());

        } catch (Exception e) {

        }

    }

    // Blink the textview
    private void startAnimation() {
        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(500); // manage the blinking time with this parameter
        anim.setStartOffset(20);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        aq.id(R.id.txtapprefferalcentre).getTextView().startAnimation(anim);
    }

    // alert to ask for vomiting symptoms
    protected void showVomittingDialog() {
        final boolean checked_state[] = {aq.id(R.id.chkdrytongue).isChecked(),
                aq.id(R.id.chkfeeblepulse).isChecked(), aq.id(R.id.chkdrydryconjunctiva).isChecked(), aq.id(R.id.chkbplow).isChecked(), aq.id(R.id.chkdecurineoutput).isChecked()};

        final SpannableStringBuilder[] colors_check = {getSS(getResources().getString(R.string.drytongue)),
                getSS(getResources().getString(R.string.feeblepulse)), getSS(getResources().getString(R.string.dryconjunctiva)), getSS(getResources().getString(R.string.bplow)), getSS(getResources().getString(R.string.decreasedurineoutput))};

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this)
                .setTitle(getSS(getResources().getString(R.string.signsinvestigations)))
                .setMultiChoiceItems(colors_check, checked_state, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        // storing the checked state of the items in an array
                        checked_state[which] = isChecked;
                    }
                }).setPositiveButton(getSS(getResources().getString(R.string.ok)),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                if (checked_state[0] == true) {
                                    aq.id(R.id.chkdrytongue).checked(true);
                                } else {
                                    aq.id(R.id.chkdrytongue).checked(false);
                                }

                                if (checked_state[1] == true)
                                    aq.id(R.id.chkfeeblepulse).checked(true);
                                else
                                    aq.id(R.id.chkfeeblepulse).checked(false);

                                if (checked_state[2] == true)
                                    aq.id(R.id.chkdrydryconjunctiva).checked(true);
                                else
                                    aq.id(R.id.chkdrydryconjunctiva).checked(false);

                                if (checked_state[3] == true)
                                    aq.id(R.id.chkbplow).checked(true);
                                else
                                    aq.id(R.id.chkbplow).checked(false);

                                if (checked_state[4] == true)
                                    aq.id(R.id.chkdecurineoutput).checked(true);
                                else
                                    aq.id(R.id.chkdecurineoutput).checked(false);

                                displayvomitingsigninterpretation();
                                // used to dismiss the dialog upon user
                                // selection.
                                dialog.dismiss();
                            }
                        });
        AlertDialog alertdialog1 = builder1.create();
        alertdialog1.show();
    }

    // Show Edema Dialog
    protected void showEdemaDialog() {
        final boolean checked_state[] = {
                aq.id(R.id.chksevereheadache).isChecked(), aq.id(R.id.chkblurredvision).isChecked()};

        final SpannableStringBuilder[] colors_check = {
                getSS(getResources().getString(R.string.severeheadache)), getSS(getResources().getString(R.string.blurredvision))};

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this)
                .setTitle(getSS(getResources().getString(R.string.signsinvestigations)))
                .setMultiChoiceItems(colors_check, checked_state, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        // storing the checked state of the items in an array
                        checked_state[which] = isChecked;
                    }
                }).setPositiveButton(getSS(getResources().getString(R.string.ok)),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                /*if (checked_state[0] == true) {
                                    aq.id(R.id.chkbp).checked(true);
                                } else {
                                    aq.id(R.id.chkbp).checked(false);
                                }*/


                                if (checked_state[0] == true) {
                                    aq.id(R.id.chksevereheadache).checked(true);
                                    aq.id(R.id.chksevereheadachesymp).checked(true);
                                } else {
                                    if (aq.id(R.id.chksevereheadachesymp).isChecked()) {
                                        aq.id(R.id.chksevereheadache).checked(true);
                                        aq.id(R.id.chksevereheadache).enabled(false);
                                    } else {
                                        aq.id(R.id.chksevereheadache).checked(false);
                                        aq.id(R.id.chksevereheadachesymp).checked(false);
                                    }
                                }

                                if (checked_state[1] == true) {
                                    aq.id(R.id.chkblurredvision).checked(true);
                                    aq.id(R.id.chkblurredvisionsymp).checked(true);
                                } else {
                                    if (aq.id(R.id.chkblurredvisionsymp).isChecked()) {
                                        aq.id(R.id.chkblurredvision).checked(true);
                                        aq.id(R.id.chkblurredvision).enabled(false);
                                    } else {
                                        aq.id(R.id.chkblurredvision).checked(false);
                                        aq.id(R.id.chkblurredvisionsymp).checked(false);
                                    }
                                }

                                hideshowdetailsedema(aq.id(R.id.txtedema).getTextView());
                                hideshowdetailssevereheadachewithblurredvision(aq.id(R.id.txtsevereheadache).getTextView());
                                // used to dismiss the dialog upon user
                                // selection.
                                dialog.dismiss();
                            }
                        });
        AlertDialog alertdialog1 = builder1.create();
        alertdialog1.show();
    }

    // Show Palp Dialog
    protected void showPalpfaitgueBreathlessnessDialog() {
        final boolean checked_state[] = {aq.id(R.id.chkfastordiffbreathing).isChecked()};

        final SpannableStringBuilder[] colors_check = {getSS(getResources().getString(R.string.fastordiffbreathing))
        };

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this)
                .setTitle(getSS(getResources().getString(R.string.signsinvestigations)))
                .setMultiChoiceItems(colors_check, checked_state, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        // storing the checked state of the items in an array
                        checked_state[which] = isChecked;
                    }
                }).setPositiveButton(getSS(getResources().getString(R.string.ok)),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                if (checked_state[0] == true) {
                                    aq.id(R.id.chkfastordiffbreathing).checked(true);
                                } else {
                                    aq.id(R.id.chkfastordiffbreathing).checked(false);
                                }


                                hideshowdetailspalpfatiguebreathlessness(aq.id(R.id.tvpalplbl).getTextView());
                                // used to dismiss the dialog upon user
                                // selection.
                                dialog.dismiss();
                            }
                        });
        AlertDialog alertdialog1 = builder1.create();
        alertdialog1.show();
    }

    // Show Urination Dialog
    protected void showUrinationProblemDialog() {
        final boolean checked_state[] = {aq.id(R.id.chkpaininabdomen).isChecked(),
                aq.id(R.id.chkfever).isChecked()};

        final SpannableStringBuilder[] colors_check = {getSS(getResources().getString(R.string.paininabdomen)),
                getSS(getResources().getString(R.string.fever))};

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this)
                .setTitle(getSS(getResources().getString(R.string.signsinvestigations)))
                .setMultiChoiceItems(colors_check, checked_state, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        // storing the checked state of the items in an array
                        checked_state[which] = isChecked;
                    }
                }).setPositiveButton(getSS(getResources().getString(R.string.ok)),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                if (checked_state[0] == true) {
                                    aq.id(R.id.chkpaininabdomen).checked(true);
                                    // aq.id(R.id.chkfeverandabdominalpain).checked(true);
                                } else {
                                    aq.id(R.id.chkpaininabdomen).checked(false);
                                    // aq.id(R.id.chkfeverandabdominalpain).checked(false);
                                }

                                if (checked_state[1] == true)
                                    aq.id(R.id.chkfever).checked(true);
                                else
                                    aq.id(R.id.chkfever).checked(false);

                                //   hideshowdetailsurinationpbm(aq.id(R.id.txtfreqofurination).getTextView());
                                // used to dismiss the dialog upon user
                                // selection.
                                dialog.dismiss();
                            }
                        });
        AlertDialog alertdialog1 = builder1.create();
        alertdialog1.show();
    }

    /**
     * Setting AuditTrail pojo, calls - InserttblAuditTrail()
     *
     * @param transId
     */
    private boolean InserttblAuditTrail(String ColumnName, String Old_Value, String New_Value, int transId,
                                        byte[] image) throws Exception {
        AuditPojo APJ = new AuditPojo();
        APJ.setUserId(appState.sessionUserId);
        APJ.setWomanId(woman.getWomanId());
        APJ.setTblName("tblregisteredwomen");
        APJ.setDateChanged(DateTimeUtil.getTodaysDate());
        APJ.setColumnName(ColumnName);
        APJ.setOld_Value(Old_Value);
        APJ.setNew_Value(New_Value);
        APJ.setTransId(transId);
        APJ.setRecordcreateddate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
        AuditRepository auditRepository = new AuditRepository(databaseHelper);
        return auditRepository.insertTotblAuditTrail(APJ);
    }

    private void setFacilityNames(String factype) throws Exception {
        facilitynamelist = new ArrayList<>();
        FacilityRepository facilityRepo = new FacilityRepository(databaseHelper);
        mapFacilityname = facilityRepo.getReferralFacilityNames(factype, appState.sessionUserId);
        facilitynamelist.add(getResources().getString(R.string.selectfacilityname));
        for (Map.Entry<String, String> village : mapFacilityname.entrySet())
            //facilitynamelist.add(village.getKey());
            facilitynamelist.add(village.getValue());
        facilitynamelist.add(getResources().getString(R.string.other));
        ArrayAdapter<String> FacNameAdapter = new ArrayAdapter<String>(HomeVisit.this,
                R.layout.simple_spinner_dropdown_item, facilitynamelist);
        FacNameAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        aq.id(R.id.spnfacnameHv).adapter(FacNameAdapter);
    }

    /**
     * This method handle touch listners, calls - displayDatePicker()
     */
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (MotionEvent.ACTION_UP == event.getAction()) {
            try {
                switch (v.getId()) {
//                    16MAy2021 Bindu - add covid details
                    case R.id.etVisitCovidTestDate:
                        isVisitDate = false;
                        isCoviddetDt = true;
                        assignEditTextAndCallDatePicker(aq.id(R.id.etVisitCovidTestDate).getEditText(), aq.id(R.id.imgclearcovidresultdate).getImageView());
                        break;
                    case R.id.etVisitCovidFirstDoseDate:
                        isVisitDate = false;
                        isCoviddetDt = true;
                        assignEditTextAndCallDatePicker(aq.id(R.id.etVisitCovidFirstDoseDate).getEditText(), aq.id(R.id.imgclearfirstdosedate).getImageView());
                        break;
                    case R.id.etVisitCovidSecondDoseDate:
                        isVisitDate = false;
                        isCoviddetDt = true;
                        assignEditTextAndCallDatePicker(aq.id(R.id.etVisitCovidSecondDoseDate).getEditText(), aq.id(R.id.imgclearsecdosedate).getImageView());
                        break;

                    case R.id.etvisitdate: {
                        isVisitDate = true;
                        isCoviddetDt = false; //16MAy2021 Bindu
                        assignEditTextAndCallDatePicker(aq.id(R.id.etvisitdate).getEditText(), aq.id(R.id.imgcleardate).getImageView());
                        break;
                    }
                    case R.id.etbpdonedate: {
                        isVisitDate = false;
                        isCoviddetDt = false; //16MAy2021 Bindu
                        assignEditTextAndCallDatePicker(aq.id(R.id.etbpdonedate).getEditText(), aq.id(R.id.imgcleardatebp).getImageView());
                        break;
                    }
                    case R.id.etpulsedate: {
                        isVisitDate = false;
                        isCoviddetDt = false; //16MAy2021 Bindu
                        assignEditTextAndCallDatePicker(aq.id(R.id.etpulsedate).getEditText(), aq.id(R.id.imgcleardatepulse).getImageView());
                        break;
                    }
                    case R.id.ethbdonedate: {
                        isVisitDate = false;
                        isCoviddetDt = false; //16MAy2021 Bindu
                        assignEditTextAndCallDatePicker(aq.id(R.id.ethbdonedate).getEditText(), aq.id(R.id.imgcleardatehb).getImageView());
                        break;
                    }
                    case R.id.ettempdonedate: {
                        isVisitDate = false;
                        isCoviddetDt = false; //16MAy2021 Bindu
                        assignEditTextAndCallDatePicker(aq.id(R.id.ettempdonedate).getEditText(), aq.id(R.id.imgcleardatetemp).getImageView());
                        break;
                    }
                    case R.id.etproteinuriadonedate: {
                        isVisitDate = false;
                        isCoviddetDt = false; //16MAy2021 Bindu
                        assignEditTextAndCallDatePicker(aq.id(R.id.etproteinuriadonedate).getEditText(), aq.id(R.id.imgcleardateproteinuria).getImageView());
                        break;
                    }
                    case R.id.etweightdate: {
                        isVisitDate = false;
                        isCoviddetDt = false; //16MAy2021 Bindu
                        assignEditTextAndCallDatePicker(aq.id(R.id.etweightdate).getEditText(), aq.id(R.id.imgcleardateweight).getImageView());
                        break;
                    }
                    default:
                        break;
                }
            } catch (Exception e) {

            }
        }
        return true;
    }

    /**
     * This method opens the Date Dialog and sets Date to respective EditText
     */
    private void assignEditTextAndCallDatePicker(EditText editText, ImageView imageView) {
        currentEditTextViewforDate = editText;
        currentImageView = imageView;
        String currDate = (editText.getText().length() > 0) ? editText.getText().toString() : "";
        CommonDatePickerFragment.getCommonDate(HomeVisit.this, currDate);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        if (view.isShown()) {
            try {

                String SelectedDate = String.format("%02d", day) + "-" + String.format("%02d", month + 1) + "-" + year;

                Date lmpDate = null;
                Date firstdosedate = null, secdosedate = null;
                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                Date seldate = format.parse(SelectedDate);

                if (woman.getRegLMP() != null && woman.getRegLMP().trim().length() > 0)
                    lmpDate = format.parse(woman.getRegLMP());

//                16May2021 Bindu add first and sec dose date
                if (aq.id(R.id.etVisitCovidFirstDoseDate).getText().toString().length() > 0)
                    firstdosedate = format.parse(aq.id(R.id.etVisitCovidFirstDoseDate).getText().toString());

                if (aq.id(R.id.etVisitCovidSecondDoseDate).getText().toString().length() > 0)
                    secdosedate = format.parse(aq.id(R.id.etVisitCovidSecondDoseDate).getText().toString());


                if (isVisitDate) {
                    if (seldate.after(new Date())) {
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.date_cannot_af_currentdate), this);
                        aq.id(R.id.etvisitdate).text(DateTimeUtil.getTodaysDate());
                    } else if (lmpDate != null && seldate.before(lmpDate)) {

                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.date_cannot_bf_lmp)
                                , this);
                        aq.id(R.id.etvisitdate).text(DateTimeUtil.getTodaysDate());
                    } else {
                        aq.id(R.id.etvisitdate).text(SelectedDate);
                        aq.id(R.id.imgcleardate).visible();
                        calculateGA(SelectedDate);
                    }
                } else {
                    if (seldate.after(new Date())) {
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.date_cannot_af_currentdate), this);
                        currentEditTextViewforDate.setText("");
                        currentEditTextViewforDate.requestFocus();
                        currentEditTextViewforDate.setError("Error");
                        aq.id(currentImageView).gone();
                    } else if (currentEditTextViewforDate == aq.id(R.id.etVisitCovidFirstDoseDate).getEditText()) {
                        if (secdosedate != null && seldate.after(secdosedate)) { // first dose cant be after sec dose
                            AlertDialogUtil.displayAlertMessage(
                                    getResources().getString(R.string.datecantbeaftersecdosedate)
                                    , this);
                            currentEditTextViewforDate.setText("");
                            currentEditTextViewforDate.requestFocus();
                            currentEditTextViewforDate.setError("Error");
                            aq.id(currentImageView).gone();
                        } else {
                            currentEditTextViewforDate.setText(SelectedDate);
                            aq.id(currentImageView).visible();
                            currentEditTextViewforDate.setError(null);
                        }
                    } else if (currentEditTextViewforDate == aq.id(R.id.etVisitCovidSecondDoseDate).getEditText()) {
                        if (firstdosedate != null && seldate.before(firstdosedate)) { // sec dose cant be bef first dose
                            AlertDialogUtil.displayAlertMessage(
                                    getResources().getString(R.string.datecantbebefrefirstdosedate)
                                    , this);
                            currentEditTextViewforDate.setText("");
                            currentEditTextViewforDate.requestFocus();
                            currentEditTextViewforDate.setError("Error");
                            aq.id(currentImageView).gone();
                        } else if (firstdosedate != null && seldate.equals(firstdosedate)) { // sec dose cant be equals first dose
                            AlertDialogUtil.displayAlertMessage(
                                    getResources().getString(R.string.seconddosedatecannotbeequaltofirstdose)
                                    , this);
                            currentEditTextViewforDate.setText("");
                            currentEditTextViewforDate.requestFocus();
                            currentEditTextViewforDate.setError("Error");
                            aq.id(currentImageView).gone();
                        } else {
                            currentEditTextViewforDate.setText(SelectedDate);
                            aq.id(currentImageView).visible();
                            currentEditTextViewforDate.setError(null);
                        }
                    } else if (lmpDate != null && seldate.before(lmpDate) && !isCoviddetDt) { //16May2021 - except for covid details

                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.date_cannot_bf_lmp)
                                , this);
                        //aq.id(R.id.etvisitdate).text(CommonClass.getTodaysDate());
                        currentEditTextViewforDate.setText("");
                        currentEditTextViewforDate.requestFocus();
                        currentEditTextViewforDate.setError("Error");
                        aq.id(currentImageView).gone();
                    } else {
                       /* aq.id(R.id.etvisitdate).text(SelectedDate);
                        aq.id(R.id.imgcleardate).visible();
*/
                        currentEditTextViewforDate.setText(SelectedDate);
                        aq.id(currentImageView).visible();
                        currentEditTextViewforDate.setError(null);
                    }
                }
            } catch (Exception e) {

            }
        }
    }

    // 20Sep2019 - Bindu - Clear Date for specfic Edittext and hide or display brush
    private void clearDate(EditText editText, ImageView imageView) {
        try {
            editText.setText("");
            aq.id(imageView).gone();
        } catch (Exception e) {

        }
    }

    private void displayTabletDetailstable() {
        try {
            if (aq.id(R.id.chkparacetamol).isChecked() || aq.id(R.id.chkalbedazolegiven).isChecked() || aq.id(R.id.chkifa).isChecked() || aq.id(R.id.chkfsfa).isChecked()) {
                aq.id(R.id.tbltabletdetails).visible();
                //Paracetamol
                if (aq.id(R.id.chkparacetamol).isChecked()) {
                    aq.id(R.id.trparacetamol).visible();
                } else {
                    aq.id(R.id.trparacetamol).gone();
                    aq.id(R.id.etnoofparacetamolgiven).text("");
                    aq.id(R.id.spnparacetamolnotgivenreason).setSelection(0);
                }
                //Albendazole
                if (aq.id(R.id.chkalbedazolegiven).isChecked()) {
                    aq.id(R.id.tralbendazole).visible();
                } else {
                    aq.id(R.id.tralbendazole).gone();
                    aq.id(R.id.etnoofalbendazolegiven).text("");
                    aq.id(R.id.spnalbendazolenotgivenreason).setSelection(0);
                }

                //IFA
                if (aq.id(R.id.chkifa).isChecked()) {
                    aq.id(R.id.trifa).visible();
                } else {
                    aq.id(R.id.trifa).gone();
                    aq.id(R.id.etnoofifagiven).text("");
                    aq.id(R.id.spnifanotgivenreason).setSelection(0);
                }

                //FSFA
                if (aq.id(R.id.chkfsfa).isChecked()) {
                    aq.id(R.id.trfsfa).visible();
                } else {
                    aq.id(R.id.trfsfa).gone();
                    aq.id(R.id.etnooffsfagiven).text("");
                    aq.id(R.id.spnfsfanotgivenreason).setSelection(0);
                }

            } else {
                aq.id(R.id.tbltabletdetails).gone();
                //30Sep2019 - Bindu - If none of the option is selected then unset all the values
                aq.id(R.id.etnoofparacetamolgiven).text("");
                aq.id(R.id.spnparacetamolnotgivenreason).setSelection(0);
                aq.id(R.id.etnoofalbendazolegiven).text("");
                aq.id(R.id.spnalbendazolenotgivenreason).setSelection(0);
                aq.id(R.id.etnoofifagiven).text("");
                aq.id(R.id.spnifanotgivenreason).setSelection(0);
                aq.id(R.id.etnooffsfagiven).text("");
                aq.id(R.id.spnfsfanotgivenreason).setSelection(0);
            }
        } catch (Exception e) {

        }
    }

    //02Oct2019 - Bindu - enable disable date fields
    private void enabledisabledatefields(EditText etval, EditText etvaldate, ImageView imgcleardate) {
        if (etval.getText().toString().trim().length() > 0) {
            etvaldate.setEnabled(true);
            etvaldate.setBackgroundResource(R.drawable.editext_none);
        } else {
            etvaldate.setEnabled(false);
            etvaldate.setText("");
            imgcleardate.setVisibility(View.GONE);
            etvaldate.setBackgroundResource(R.drawable.editextdisable_none);
        }
    }

    private void enabledisableproteinuriadatefields() {
        if (aq.id(R.id.rdproteinuriapresentsumm).isChecked() || aq.id(R.id.rdproteinuriaabsentsumm).isChecked()) {
            aq.id(R.id.etproteinuriadonedate).enabled(true).background(R.drawable.editext_none);
        } else {
            aq.id(R.id.etproteinuriadonedate).text("").enabled(false).background(R.drawable.editextdisable_none);
            aq.id(R.id.imgcleardateproteinuria).gone();
        }
    }

    // Enable or Disable tab not given reason based on input
    private void EnableDisableTabNotGivenReason(EditText et, Spinner sp) {
        if (et.getText().toString().length() > 0) {
            sp.setEnabled(true);
            sp.setBackground(getResources().getDrawable(R.drawable.spinner_bg_enabled));
            int nooftab = (et.getText().toString().length() > 0 ? Integer.parseInt(et.getText().toString()) : 0);
            if (nooftab <= 0) {
                if (sp.getSelectedItem().toString().equalsIgnoreCase("Select")) {
                    if (sp.getSelectedView().toString() != null) {

                        TextView errorText = (TextView) sp.getSelectedView();
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);//just to highlight that this is an error
                        errorText.setText(getResources().getString(R.string.selectreason));//changes the selected item text to this
                        errorText.setFocusable(true);
                        et.requestFocus();
                        et.setFocusableInTouchMode(true); //set error and focus
                    }
                }
            } else {
                sp.setEnabled(false);
                sp.setBackground(getResources().getDrawable(R.drawable.spinner_bg_disabled));
                sp.setSelection(0);
                TextView errorText = (TextView) sp.getSelectedView();
                errorText.setError(null);
                errorText.setText(getResources().getString(R.string.select));
                errorText.setTextColor(Color.GRAY);
            }
        } else {
            sp.setEnabled(false);
            sp.setSelection(0);
            TextView errorText = (TextView) sp.getSelectedView();
            errorText.setError(null);
            errorText.setText(getResources().getString(R.string.select));
            errorText.setTextColor(Color.GRAY);
            sp.setBackground(getResources().getDrawable(R.drawable.spinner_bg_disabled));
        }
    }

    /**
     * Confirm Alert on Click save
     */
    private void confirmAlert() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set dialog message
        String mess = getResources().getString(R.string.m099);
        alertDialogBuilder.setMessage(mess).setCancelable(false)
                .setNegativeButton((getResources().getString(R.string.m121)),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                try {
                                    savedata();
                                } catch (Exception e) {

                                }
                            }
                        })
                .setPositiveButton((getResources().getString(R.string.m122)),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    // Show Urination Dialog
    protected void showFeverandOtherSymptomsDialog() {
        final boolean checked_state[] = {aq.id(R.id.chkfever).isChecked(),
                aq.id(R.id.chkcough).isChecked(), aq.id(R.id.chkcold).isChecked(), aq.id(R.id.chkjaundice).isChecked(), aq.id(R.id.chkpaininabdomen).isChecked(),
                aq.id(R.id.chkwhitedischarge).isChecked(), aq.id(R.id.chkincreasedurination).isChecked(), aq.id(R.id.chkurinationafter12wks).isChecked(),
                aq.id(R.id.chkburningurination).isChecked(), aq.id(R.id.chkothersymptoms).isChecked()};

        final SpannableStringBuilder[] colors_check = {getSS(getResources().getString(R.string.fever)),
                getSS(getResources().getString(R.string.cough)), getSS(getResources().getString(R.string.cold)), getSS(getResources().getString(R.string.jaundice
        )), getSS(getResources().getString(R.string.paininabdomen)), getSS(getResources().getString(R.string.whitedischarge)), getSS(getResources().getString(R.string.increasedfrequrination)), getSS(getResources().getString(R.string.urinationafter12wks)), getSS(getResources().getString(R.string.burningurination)), getSS(getResources().getString(R.string.others))};

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this)
                .setTitle(getSS(getResources().getString(R.string.signsinvestigations)))
                .setMultiChoiceItems(colors_check, checked_state, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        // storing the checked state of the items in an array
                        checked_state[which] = isChecked;
                    }
                }).setPositiveButton(getSS(getResources().getString(R.string.ok)),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                if (checked_state[0] == true) {
                                    aq.id(R.id.chkfever).checked(true);
                                    aq.id(R.id.etTempf).enabled(true).background(R.drawable.edittext_style);
                                } else {
                                    aq.id(R.id.chkfever).checked(false);
                                    aq.id(R.id.etTempf).enabled(false).background(R.drawable.edittext_disable);
                                }

                                if (checked_state[1] == true)
                                    aq.id(R.id.chkcough).checked(true);
                                else
                                    aq.id(R.id.chkcough).checked(false);

                                if (checked_state[2] == true)
                                    aq.id(R.id.chkcold).checked(true);
                                else
                                    aq.id(R.id.chkcold).checked(false);

                                if (checked_state[3] == true)
                                    aq.id(R.id.chkjaundice).checked(true);
                                else
                                    aq.id(R.id.chkjaundice).checked(false);

                                if (checked_state[4] == true)
                                    aq.id(R.id.chkpaininabdomen).checked(true);
                                else
                                    aq.id(R.id.chkpaininabdomen).checked(false);

                                if (checked_state[5] == true)
                                    aq.id(R.id.chkwhitedischarge).checked(true);
                                else
                                    aq.id(R.id.chkwhitedischarge).checked(false);

                                if (checked_state[6] == true)
                                    aq.id(R.id.chkincreasedurination).checked(true);
                                else
                                    aq.id(R.id.chkincreasedurination).checked(false);

                                if (checked_state[7] == true)
                                    aq.id(R.id.chkurinationafter12wks).checked(true);
                                else
                                    aq.id(R.id.chkurinationafter12wks).checked(false);

                                if (checked_state[8] == true)
                                    aq.id(R.id.chkburningurination).checked(true);
                                else
                                    aq.id(R.id.chkburningurination).checked(false);

                                if (checked_state[9] == true)
                                    aq.id(R.id.chkothersymptoms).checked(true);
                                else
                                    aq.id(R.id.chkothersymptoms).checked(false);

                                hideshowdetailsfeverandothersymtoms(aq.id(R.id.txtfever).getTextView());
                                dialog.dismiss();
                            }
                        });
        AlertDialog alertdialog1 = builder1.create();
        alertdialog1.show();
    }

    //    16May2021 Bindu covid healthissuecheck
    private boolean validatecovidhealthissue() throws Exception {
        boolean isvalid = true;
        if (!(aq.id(R.id.rd_healthissuesYes).isChecked() || aq.id(R.id.rd_healthissuesNo).isChecked())) {
            AlertDialogUtil.displayAlertMessage(
                    getResources().getString(R.string.plsenterecenthealthdetails)
                    , act);
            opencloseAccordion(aq.id(R.id.llhealthdetails).getView(), aq.id(R.id.txthealthdet).getTextView());
            aq.id(R.id.llhealthdetails).backgroundColor(getResources().getColor(R.color.lightgray));
            return false;
        }

        if (aq.id(R.id.rd_healthissuesYes).isChecked()) {
            String healthproblems = spnHealthProblemshv.getSelectedItemsAsString();
            /*|| healthproblems.contains(getResources().getString(R.string.select)*/
            if (healthproblems != null && healthproblems.length() <= 0) {
                AlertDialogUtil.displayAlertMessage(
                        getResources().getString(R.string.plsselhealthissues)
                        , act);
                return false;
            }
            if (!(aq.id(R.id.rd_CovidTestYes).isChecked() || aq.id(R.id.rd_CovidTestNo).isChecked())) {
                AlertDialogUtil.displayAlertMessage(
                        getResources().getString(R.string.plsselectcovidtesttaken)
                        , act);
                return false;
            }
        }

        if (aq.id(R.id.rd_CovidTestYes).isChecked()) {
            if (!(aq.id(R.id.rd_CovidPositive).isChecked() || aq.id(R.id.rd_CovidNegative).isChecked())) {
                AlertDialogUtil.displayAlertMessage(
                        getResources().getString(R.string.plsselectcovidresult)
                        , act);
                return false;
            }
            if (aq.id(R.id.etVisitCovidTestDate).getText().toString().trim().length() <= 0) {
                AlertDialogUtil.displayAlertMessage(
                        getResources().getString(R.string.plsselectcovidtestdate)
                        , act);
                return false;
            }
        }


        return isvalid;
    }

    private boolean validateHomeVisit() throws Exception {
        boolean isvalid = true;

        //23 Aug2021 Mani
        if (!(aq.id(R.id.ashavailableYes).isChecked() || aq.id(R.id.ashavailableNo).isChecked() || aq.id(R.id.ashavailableDontKnow).isChecked() )) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plselectavailabilityasha), act);
            return false;
        }
//        16May2021 Bindu check for covid vaccine
        if (!(aq.id(R.id.rd_CovidVaccineYes).isChecked() || aq.id(R.id.rd_CovidVaccineNo).isChecked())) {
            AlertDialogUtil.displayAlertMessage(
                    getResources().getString(R.string.plsentercovidvaccinedetails)
                    , act);
            opencloseAccordion(aq.id(R.id.llcovidvaccinedetails).getView(), aq.id(R.id.txtcovidvaccine).getTextView());
            aq.id(R.id.llcovidvaccinedetails).backgroundColor(getResources().getColor(R.color.lightgray));
            return false;
        }

        if (aq.id(R.id.rd_CovidVaccineYes).isChecked()) {
            if (aq.id(R.id.spnVisitVaccineDose).getSelectedItemPosition() == 0 && !isvaccinated) {
                AlertDialogUtil.displayAlertMessage(
                        getResources().getString(R.string.plsselectdosecompleted)
                        , act);
                return false;
            } else if (aq.id(R.id.spnVisitVaccineDose).getSelectedItemPosition() == 1) {
                if (aq.id(R.id.etVisitCovidFirstDoseDate).getText().toString().trim().length() <= 0) {
                    AlertDialogUtil.displayAlertMessage(
                            getResources().getString(R.string.plsselectfirstdosedate)
                            , act);
                    return false;
                }
            } else if (aq.id(R.id.spnVisitVaccineDose).getSelectedItemPosition() == 2) {
                if (aq.id(R.id.etVisitCovidFirstDoseDate).getText().toString().trim().length() <= 0) {
                    AlertDialogUtil.displayAlertMessage(
                            getResources().getString(R.string.plsselectfirstdosedate)
                            , act);
                    return false;
                }
                if (aq.id(R.id.etVisitCovidSecondDoseDate).getText().toString().trim().length() <= 0) {
                    AlertDialogUtil.displayAlertMessage(
                            getResources().getString(R.string.plsselectsecdosedate)
                            , act);
                    return false;
                }
            }
        }

        if (aq.id(R.id.chkparacetamol).isChecked() && aq.id(R.id.etnoofparacetamolgiven).getText().toString().trim().length() <= 0) {
            AlertDialogUtil.displayAlertMessage(
                    getResources().getString(R.string.plsenterparacetamolqty)
                    , act);
            aq.id(R.id.etnoofparacetamolgiven).getEditText().requestFocus();
            return false;
        }

        if (aq.id(R.id.etnoofparacetamolgiven).getText().toString().trim().length() > 0) {
            int val = Integer.parseInt(aq.id(R.id.etnoofparacetamolgiven).getText().toString());
            if (val == 0 && aq.id(R.id.spnparacetamolnotgivenreason).getSelectedItemPosition() == 0) {
                AlertDialogUtil.displayAlertMessage(
                        getResources().getString(R.string.plsselectnotgivenreason)
                        , act);
                aq.id(R.id.etnoofparacetamolgiven).getEditText().requestFocus();
                return false;
            }
        }

        if (aq.id(R.id.chkalbedazolegiven).isChecked() && aq.id(R.id.etnoofalbendazolegiven).getText().toString().trim().length() <= 0) {
            AlertDialogUtil.displayAlertMessage(
                    getResources().getString(R.string.plsenteralbendazoleqty)
                    , act);
            aq.id(R.id.etnoofalbendazolegiven).getEditText().requestFocus();
            return false;
        }

        if (aq.id(R.id.etnoofalbendazolegiven).getText().toString().trim().length() > 0) {
            int val = Integer.parseInt(aq.id(R.id.etnoofalbendazolegiven).getText().toString());
            if (val == 0 && aq.id(R.id.spnalbendazolenotgivenreason).getSelectedItemPosition() == 0) {
                AlertDialogUtil.displayAlertMessage(
                        getResources().getString(R.string.plsselectnotgivenreason)
                        , act);
                aq.id(R.id.etnoofalbendazolegiven).getEditText().requestFocus();
                return false;
            }
        }

        if (aq.id(R.id.chkifa).isChecked() && aq.id(R.id.etnoofifagiven).getText().toString().trim().length() <= 0) {
            AlertDialogUtil.displayAlertMessage(
                    getResources().getString(R.string.plsenterifaqty)
                    , act);
            aq.id(R.id.etnoofifagiven).getEditText().requestFocus();
            return false;
        }

        if (aq.id(R.id.etnoofifagiven).getText().toString().trim().length() > 0) {
            int val = Integer.parseInt(aq.id(R.id.etnoofifagiven).getText().toString());
            if (val == 0 && aq.id(R.id.spnifanotgivenreason).getSelectedItemPosition() == 0) {
                AlertDialogUtil.displayAlertMessage(
                        getResources().getString(R.string.plsselectnotgivenreason)
                        , act);
                aq.id(R.id.etnoofifagiven).getEditText().requestFocus();
                return false;
            }
        }

        if (aq.id(R.id.chkfsfa).isChecked() && aq.id(R.id.etnooffsfagiven).getText().toString().trim().length() <= 0) {
            AlertDialogUtil.displayAlertMessage(
                    getResources().getString(R.string.plsenterfsfaqty)
                    , act);
            aq.id(R.id.etnooffsfagiven).getEditText().requestFocus();
            return false;
        }

        if (aq.id(R.id.etnooffsfagiven).getText().toString().trim().length() > 0) {
            int val = Integer.parseInt(aq.id(R.id.etnooffsfagiven).getText().toString());
            if (val == 0 && aq.id(R.id.spnfsfanotgivenreason).getSelectedItemPosition() == 0) {
                AlertDialogUtil.displayAlertMessage(
                        getResources().getString(R.string.plsselectnotgivenreason)
                        , act);
                aq.id(R.id.etnooffsfagiven).getEditText().requestFocus();
                return false;
            }
        }

        //30Sep2019 -bindu - Validate Examination value
        if (aq.id(R.id.etbpsystolicsumm).getText().toString().trim().length() > 0 && aq.id(R.id.etbpdiastolicsumm).getText().toString().trim().length() <= 0) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.enter_diastolic), act);
            aq.id(R.id.etbpdiastolicsumm).getEditText().requestFocus();
            return false;
        } else if (aq.id(R.id.etbpsystolicsumm).getText().toString().trim().length() <= 0 && aq.id(R.id.etbpdiastolicsumm).getText().toString().trim().length() > 0) {
            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.enter_systolic), act);
            aq.id(R.id.etbpsystolicsumm).getEditText().requestFocus();
            return false;
        }

        if (aq.id(R.id.chkisreferred).isChecked()) {

            //08Apr2021 Bindu add ref fac type and ref slip num validation
            if (aq.id(R.id.spnfactype).getSelectedItemPosition() == 0) {
                AlertDialogUtil.displayAlertMessage(
                        getResources().getString(R.string.plsselreffactype)
                        , act);
                TextView errorText = (TextView) aq.id(R.id.spnfactype).getSpinner().getSelectedView();
                errorText.setError("");
                errorText.setTextColor(Color.RED);
                errorText.setText(getResources().getString(R.string.plsselreffactype));
                errorText.setFocusable(true);
                return false;
            }

            if (aq.id(R.id.spnfacnameHv).getSelectedItemPosition() == 0) {
                AlertDialogUtil.displayAlertMessage(
                        getResources().getString(R.string.plsselreffac)
                        , act);
                TextView errorText = (TextView) aq.id(R.id.spnfacnameHv).getSpinner().getSelectedView();
                errorText.setError("");
                errorText.setTextColor(Color.RED);
                errorText.setText(getResources().getString(R.string.plsselreffac));
                errorText.setFocusable(true);
                return false;
            }

            if (aq.id(R.id.etrefslipnumber).getText().toString().trim().length() <= 0) {
                AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.plsenterrefslipnum), act);
                aq.id(R.id.etrefslipnumber).getEditText().requestFocus();
                return false;
            }
        }

        return isvalid;
    }

    private void displayAlert(final String spanText2, final Intent goToScreen) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set dialog message
        alertDialogBuilder.setMessage(spanText2).setCancelable(false).setNegativeButton(
                (getResources().getString(R.string.m121)), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (goToScreen != null) {
                            try {
                                startActivity(goToScreen);
                                if (spanText2.contains("logout")) {
                                    try {
                                        LoginRepository loginRepo = new LoginRepository(databaseHelper);
                                        loginRepo.updateLogoutTime(appState.loginAuditId, appState.sessionUserId);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            dialog.cancel();
                        }
                    }
                }).setPositiveButton((getResources().getString(R.string.m122)),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        /*
         * You'll need this in your class to release the helper when done.
         */
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

    @Override
    public void onBackPressed() {

    }

    //04Dec2019 - Bindu
    private void sendSMS(TransactionHeaderRepository transRepo, DatabaseHelper databaseHelper, TblVisitHeader tblvisH) {
        try {
            String p_no = "";
            List<TblContactDetails> values;

            SettingsRepository settingsRepository = new SettingsRepository(databaseHelper);

            values = settingsRepository.getSpecificPhoneNumber("ANC Home Visit");//13May2021 Arpitha
            p_no = "";
            for (int i = 0; i < values.size(); i++) {

                String s = values.get(i).getContactNumber();
                p_no = p_no + values.get(i).getContactNumber() + ",";
            }


            //13May2021 Arpitha
            String uniqueId = "";
            if (woman.getRegUIDNo() != null && woman.getRegUIDNo().trim().length() > 0) {
                uniqueId = "MCP:" + woman.getRegUIDNo();
            } else if (woman.getRegAadharCard() != null && woman.getRegAadharCard().trim().length() > 0) {
                uniqueId = "A:" + woman.getRegAadharCard();
            } else if (woman.getRegPhoneNumber() != null && woman.getRegPhoneNumber().trim().length() > 0) {
                uniqueId = "Ph:" + woman.getRegPhoneNumber();
            }

            String smsContent = "";
            String iscompl = "", isref = "", compl = "";
            if (aq.id(R.id.chkisreferred).isChecked())
                isref = " , Ref - " + tblvisH.getVisHVisitIsReferred() + " , To - " + tblvisH.getVisHVisitReferredToFacilityName();
            // 11Dec2019 - Bindu  Get Hamlet name
            FacilityRepository facilityRepo = new FacilityRepository(databaseHelper);
            String hamletname = facilityRepo.getHamletName(woman.getRegVillage());


//        String ref = aq.id(R.id.chkisreferred).isChecked() ? getResources().getString(R.string.yesshortform) : getResources().getString(R.string.noshortform);

            if (woman.getRegPregnantorMother() == 1)
            /*smsContent = "Sentiatend:" + appState.userType + ", " + uniqueId +
                    ", " + woman.getWomanId() + ", " + "PW-"
                    + woman.getRegWomanName()+",V # :" +visitNum+", V Dt: "+
                    aq.id(R.id.etvisitdate).getText().toString()
                    +", Ref:"+ref
                    + ", " + user.getFacilityName()
                    + ", HV";*/
                //16May2021 Bindu change sms content
                smsContent = "Sentiatend :-" + woman.getRegWomanName() + ", " + uniqueId +
                        ", WID- " + woman.getWomanId() + ", V# :" + visitNum + ", V Dt: " + aq.id(R.id.etvisitdate).getText().toString()
                        + ", PW - " + tblvisH.getVisHStatusAtVisit()
                        + ", " + hamletname + isref + ", " + appState.userType + "- " + user.getUserName()
                        + " ," + user.getUserMobileNumber() + "- (HV)";

            if (p_no.length() <= 0) {
                display_messagedialog(transRepo, databaseHelper, tblvisH, smsContent);
            } else
                sending(transRepo, databaseHelper, p_no, tblvisH, smsContent);

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    // 13Oct2016 Arpitha
    public void display_messagedialog(final TransactionHeaderRepository
                                              transactionHeaderRepository, final
                                      DatabaseHelper databaseHelper, final TblVisitHeader tblvisH, String smsContent) throws Exception {
        try {
            final Dialog sms_dialog = new Dialog(this);
//            sms_dialog.setTitle(getResources().getString(R.string.plsentervalidphoneno));
            sms_dialog.setContentView(R.layout.sms_dialog);
            sms_dialog.show();

            ImageButton imgsend = (ImageButton) sms_dialog.findViewById(R.id.imgsend);
            ImageButton imgcancel = (ImageButton) sms_dialog.findViewById(R.id.imgcancel);
            etphn = (EditText) sms_dialog.findViewById(R.id.etphno);
            final EditText etmess = (EditText) sms_dialog.findViewById(R.id.etmess);
            TextView txtcontcats = (TextView) sms_dialog.findViewById(R.id.txtcontacts);


//            etmess.setVisibility(View.GONE);
            etmess.setText(smsContent);
            etmess.setEnabled(false);
            txtcontcats.setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    // TODO Auto-generated method stub
                    Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                    startActivity(intent);
                    return true;
                }
            });

            imgsend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        sending(transactionHeaderRepository, databaseHelper,
                                etphn.getText().toString(), tblvisH, smsContent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            imgcancel.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    sms_dialog.cancel();
                    //21May2021 Arpitha

                    Intent intent = new Intent(HomeVisit.this, HomeVisitListActivity.class);
                    intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    intent.putExtra("woman", woman);
                    startActivity(intent);

                }
            });
            sms_dialog.setCancelable(false);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    void sending(TransactionHeaderRepository transRepo, DatabaseHelper databaseHelper,
                 String phnNo, TblVisitHeader tblvisH, String smsCon) throws Exception {
        String smsContent = smsCon;

        String PregnantorMother = "";

        //mani 19 april 2021 added new field
        String iscompl = "", isref = "", compl = "";

//        iscompl = " , C - " + tblvisH.getVisHCompl();
        /*if (tblvisH.getVisHCompl().equalsIgnoreCase(getResources().getString(R.string.yesshortform))) {
            if (arrLSymptomsSave != null && arrLSymptomsSave.size() > 0) {
                if (arrLSymptomsSave.size() > 1)
                    iscompl = iscompl + " : " + arrLSymptomsSave.get(0) + "," + arrLSymptomsSave.get(1) + " ...";
                else if (arrLSymptomsSave.size() == 1)
                    iscompl = iscompl + " - " + arrLSymptomsSave.get(0);
            }
        }*/ //16May2021 Bindu comment


        //11DecBindu - Add GA and Hamlet and first 2 compl for sms
        //Mani 19 april 2021- Changed content of sms
       /* smsContent = "Sentiatend :-" + "User Type-" + woman.getRegUserType() + getUniqueIdOfWoman() + "," + woman.getRegWomanName() + ", " + tblvisH.getVisHStatusAtVisit()
                + ", " + hamletname + ", Visit No.- " + visitNum + ", Visit Date-" + aq.id(R.id.etvisitdate).getEditText() + compl + isref;//24Dec2019 - bindu - add asha name
*/


        InserttblMessageLog(phnNo,
                smsContent, user.getUserId(), transRepo, databaseHelper);

//        21May2021 Arpitha
        Intent intent = new Intent(HomeVisit.this, HomeVisitListActivity.class);
        intent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
        intent.putExtra("woman", woman);
        startActivity(intent);

        if (isMessageLogsaved) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.sending_sms),
                    Toast.LENGTH_LONG).show();
            sendSMSFunction();
        }

    }

    //mani april 19 2021
    private String getUniqueIdOfWoman() {
        String id = null;
        if (woman.getRegUIDNo() != null && woman.getRegUIDType() == "RCHID") {
            id = ", RCHID:" + woman.getRegUIDNo();
        } else if (woman.getRegUIDNo() != null && woman.getRegUIDType() == "Thayi") {
            id = ", Thayicard:" + woman.getRegUIDNo();
        } else if (woman.getRegPhoneNumber() != null) {
            id = ", Phone Number:" + woman.getRegPhoneNumber();
        } else if (woman.getRegAadharCard() != null) {
            id = ", Adhar:" + woman.getRegAadharCard();
        }
        return id;
    }

    public void InserttblMessageLog(String phoneno, String content, String user_id,
                                    TransactionHeaderRepository transactionHeaderRepository, DatabaseHelper databaseHelper) throws Exception {
        final ArrayList<MessageLogPojo> mlpArr = new ArrayList<MessageLogPojo>();

        if (phoneno.length() > 0) {
            String[] phn = phoneno.split("\\,");
            for (int i = 0; i < phn.length; i++) {
                String num = phn[i];
                if (num != null && num.length() > 0) {
                    MessageLogPojo mlp =
                            new MessageLogPojo();
                    mlp.setUserId(user_id);
                    mlp.setMsgPhoneNo(num);
                    mlp.setMsgBody(content);
                    mlp.setMsgPriority(1);
                    mlp.setWomanId(woman.getWomanId());
                    mlp.setMsgNoOfFailures(0);
                    mlp.setTransId(transId);
                    mlp.setMsgSentDate(DateTimeUtil.getTodaysDate());
                    mlp.setMsgSent(0);
                    mlp.setMsgStage("Home visit - ANC");
                    mlp.setMsgUserType(appState.userType);
                    mlp.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
                    mlp.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());//21May2021 Arpitha
                    mlpArr.add(mlp);
                }
            }
        }
        int addMessage = 0;
        SMSRepository smsRepository = new SMSRepository(databaseHelper);
        if (mlpArr != null) {
            for (final MessageLogPojo mlpp : mlpArr) {
                addMessage = smsRepository.insertIntoMessageLog(mlpp, databaseHelper);
            }
            if (addMessage > 0) {
                isMessageLogsaved = true;
                boolean addRegTrans = transactionHeaderRepository.iNewRecordTransNew(user.getUserId(), transId,
                        "tblmessagelog", databaseHelper);
            }
        }
    }

    private void sendSMSFunction() throws Exception {
        messageSent = true;
        new SendSMS(this).checkAndSendMsg(databaseHelper);
    }

    //  07Dec2019 Bindu
    public int checkSimState() {
        int simState = -1;
        // This is to know the status of the Sim
        TelephonyManager telMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        simState = telMgr.getSimState();

        return simState;
    }

    //    Arpitha 03Jan2020
    void disableScreen() {
        ScrollView srlWomanBasicInfo = findViewById(R.id.scr);
        ScrollView srlhealthhistory = findViewById(R.id.scrhomevisitsummary);
        aq.id(R.id.btnsavehomevisit).enabled(false);
       /* aq.id(R.id.chkparacetamol).enabled(false);
        aq.id(R.id.chkalbedazolegiven).enabled(false);
        aq.id(R.id.chkifa).enabled(false);
        aq.id(R.id.chkfsfa).enabled(false);*/

        disableEnableControls(false, srlWomanBasicInfo);
        disableEnableControls(false, srlhealthhistory);
    }

    //    Arpitha 03Jan2020
    private void disableEnableControls(boolean enable, ViewGroup vg) {
        for (int i = 0; i < vg.getChildCount(); i++) {
            View child = vg.getChildAt(i);
            child.setEnabled(enable);
            if (child instanceof ViewGroup) {
                disableEnableControls(enable, (ViewGroup) child);
            }
        }
    }

    //08Apr2021 Bindu set fac type and fac name - referral table
    private void setFacilityType() throws Exception {
        reffacilitytypelist = new ArrayList<>();
        RefFacilityRepository facilityRepo = new RefFacilityRepository(databaseHelper);
        mapRefFacilityType = facilityRepo.getRefFacilityType();
        reffacilitytypelist.add(getResources().getString(R.string.selectfacilityname));
        for (Map.Entry<String, String> village : mapRefFacilityType.entrySet())
            reffacilitytypelist.add(village.getValue());
        ArrayAdapter<String> FacTypeAdapter = new ArrayAdapter<String>(HomeVisit.this,
                R.layout.simple_spinner_dropdown_item, reffacilitytypelist);
        FacTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        aq.id(R.id.spnfactype).adapter(FacTypeAdapter);
    }

    private void setRefFacilityName(String factype) throws Exception {
        factype = aq.id(R.id.spnfactype).getSelectedItem().toString();
        reffacilitynamelist = new ArrayList<>();
        RefFacilityRepository facilityRepo = new RefFacilityRepository(databaseHelper);
        mapRefFacilityname = facilityRepo.getRefFacilityName(factype);
        reffacilitynamelist.add(getResources().getString(R.string.selectfacilityname));
        for (Map.Entry<String, String> village : mapRefFacilityname.entrySet())
            reffacilitynamelist.add(village.getValue());
        ArrayAdapter<String> FacTypeAdapter = new ArrayAdapter<String>(HomeVisit.this,
                R.layout.simple_spinner_dropdown_item, reffacilitynamelist);
        FacTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        aq.id(R.id.spnfacnameHv).adapter(FacTypeAdapter);
    }

    @SuppressLint("SimpleDateFormat")
    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        private AQuery aq;
        private tblregisteredwomen woman;
        private HomeVisit homeVisitActivity;

        static DatePickerFragment getInstance() {
            return new DatePickerFragment();
        }

        public void setWoman(tblregisteredwomen woman) {
            this.woman = woman;
        }

        public void setHomeVisitActivity(HomeVisit activity) {
            this.homeVisitActivity = activity;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            int year = 0, month = 0, day = 0;
            try {
                aq = new AQuery(getActivity());
                Calendar c = Calendar.getInstance();
                String defaultDate = aq.id(R.id.etvisitdate).getText().toString(); //09Aug2019 - Cal set date of Provided date
                if (defaultDate != null && defaultDate.length() > 0) {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    Date sendDate = sdf.parse(defaultDate);
                    c.setTime(sendDate);
                    year = c.get(Calendar.YEAR);
                    month = c.get(Calendar.MONTH);
                    day = c.get(Calendar.DAY_OF_MONTH);
                } else {
                    c.setTime(new Date());
                    year = c.get(Calendar.YEAR);
                    month = c.get(Calendar.MONTH);
                    day = c.get(Calendar.DAY_OF_MONTH);
                }

            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }

            // Create a new instance of DatePickerDialog and return it
            DatePickerDialog dpDialog;
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                // Do something for lollipop and above versions
                dpDialog = new DatePickerDialog(getActivity(), R.style.DialogTheme, this, year, month, day);
            } else {
                // do something for phones running an SDK before lollipop
                dpDialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, this, year, month, day);
                dpDialog.setTitle("Set a Date");
                dpDialog.getDatePicker().setCalendarViewShown(false);
            }
            return dpDialog;
        }


        @Override
        public void onDateSet(DatePicker view, int year, int month, int day) {
            if (view.isShown()) {
                try {

                    String SelectedDate = String.format("%02d", day) + "-" +
                            String.format("%02d", month + 1) + "-"
                            + year;


                    Date lmpDate = null;
                    SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                    Date seldate = format.parse(SelectedDate);


                    if (woman.getRegLMP() != null && woman.getRegLMP().trim().length() > 0)
                        lmpDate = format.parse(woman.getRegLMP());

                    if (seldate.after(new Date())) {
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.date_cannot_af_currentdate)
                                , getActivity());
                        aq.id(R.id.etvisitdate).text(DateTimeUtil.getTodaysDate());
                    } else if (lmpDate != null && seldate.before(lmpDate)) {

                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.date_cannot_bf_lmp)
                                , getActivity());
                        aq.id(R.id.etvisitdate).text(DateTimeUtil.getTodaysDate());
                    } else {
                        aq.id(R.id.etvisitdate).text(SelectedDate);
                        aq.id(R.id.imgcleardate).visible();
                        homeVisitActivity.calculateGA(SelectedDate);
                    }
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                    crashlytics.log(e.getMessage());
                }
            }
        }


    }

    /**
     * The click listner for ListView in the navigation drawer
     */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            mDrawerLayout.closeDrawer(mDrawerList);
            //   if (woman.getRegADDate() == null || woman.getRegADDate().length() == 0) {
            try {
                if (position == 0) {
                    Intent viewprof = new Intent(HomeVisit.this, ViewProfileActivity.class);
                    viewprof.putExtra("woman", woman); //26Jul2019 - Bindu
                    viewprof.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    startActivity(viewprof);
                } else if (position == 1) {
                    Intent unplanned = new Intent(HomeVisit.this, ServicesSummaryActivity.class);
                    unplanned.putExtra("woman", woman); //26Jul2019 - Bindu
                    unplanned.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    startActivity(unplanned);
                } else if (position == 2) {
                    Intent unplanned = new Intent(HomeVisit.this,
                            UnplannedServicesListActivity.class);
                    unplanned.putExtra("woman", woman); //26Jul2019 - Bindu
                    unplanned.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    startActivity(unplanned);
                } else if (position == 3) {
                    //if(woman.getLastAncVisitDate().length() > 0 || woman.getLastPncVisitDate().length() > 0) {
                 //   if (visitNum > 1) {
                        Intent homevisit = new Intent(HomeVisit.this, HomeVisitListActivity.class);
                        homevisit.putExtra("woman", woman);
                        homevisit.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        startActivity(homevisit);
                    /*} else {
                        Toast.makeText(HomeVisit.this, getResources().getString(R.string.norecords), Toast.LENGTH_SHORT).show();
                    }*/
                }//01Oct2019 Arpitha
                else if (position == 4) {
                    if (woman.getRegPregnantorMother() == 2 || DateTimeUtil.getNumberOfDaysFromLMP(woman.getRegLMP()) > 210) {
                        Intent del = new Intent(HomeVisit.this, DeliveryInfoActivity.class);
                        del.putExtra("woman", woman);
                        del.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        startActivity(del);
                    } else
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.m132), Toast.LENGTH_LONG).show();
                }
                //                  28Nov2019 Arpitha
                else if (position == 5) {
                    Intent deact = new Intent(HomeVisit.this, WomanDeactivateActivity.class);
                    deact.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    deact.putExtra("woman", woman);
                    startActivity(deact);
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block

            }
           /* } else {
            }*/
        }
    }

    // Text watcher
    TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            // Temp in fever - set to Summ fever and bp
            if (s == aq.id(R.id.etTempf).getEditable()) {
                aq.id(R.id.etTemp).text(s.toString());
                if (aq.id(R.id.etTempf).getText().toString().length() > 0
                        && Double.parseDouble(aq.id(R.id.etTempf).getText().toString()) <= 0) {
                    aq.id(R.id.etTempf).getEditText()
                            .setError(getResources().getString(R.string.incorrectinput));
                    aq.id(R.id.etTemp).getEditText().setError(getResources().getString(R.string.incorrectinput));
                } else {
                    aq.id(R.id.etTempf).getEditText().setError(null);
                    aq.id(R.id.etTemp).getEditText().setError(null);
                }
            } else if (s == aq.id(R.id.etbpsystolic).getEditable()) {
                aq.id(R.id.etbpsystolicsumm).text(s.toString());
                if (aq.id(R.id.etbpsystolic).getText().toString().length() > 0
                        && Integer.parseInt(aq.id(R.id.etbpsystolic).getText().toString()) <= 0) {
                    aq.id(R.id.etbpsystolic).getEditText().setError(getResources().getString(R.string.incorrectinput));
                    aq.id(R.id.etbpsystolicsumm).getEditText().setError(getResources().getString(R.string.incorrectinput));
                } else {
                    aq.id(R.id.etbpsystolic).getEditText().setError(null);
                    aq.id(R.id.etbpsystolicsumm).getEditText().setError(null);
                }
            } else if (s == aq.id(R.id.etbpdiastolic).getEditable()) {
                aq.id(R.id.etbpdiastolicsumm).text(s.toString());
                if (aq.id(R.id.etbpdiastolic).getText().toString().length() > 0
                        && Integer.parseInt(aq.id(R.id.etbpdiastolic).getText().toString()) <= 0) {
                    aq.id(R.id.etbpdiastolic).getEditText().setError(getResources().getString(R.string.incorrectinput));
                    aq.id(R.id.etbpdiastolicsumm).getEditText().setError(getResources().getString(R.string.incorrectinput));
                } else {
                    aq.id(R.id.etbpdiastolic).getEditText().setError(null);
                    aq.id(R.id.etbpdiastolicsumm).getEditText().setError(null);
                }
            } else if (s == aq.id(R.id.ethbpalp).getEditable()) {
                aq.id(R.id.ethb).text(s.toString());
                if (aq.id(R.id.ethbpalp).getText().toString().length() > 0
                        && Double.parseDouble(aq.id(R.id.ethbpalp).getText().toString()) <= 0) {
                    aq.id(R.id.ethbpalp).getEditText().setError(getResources().getString(R.string.incorrectinput));
                    aq.id(R.id.ethb).getEditText().setError(getResources().getString(R.string.incorrectinput));
                } else {
                    aq.id(R.id.ethbpalp).getEditText().setError(null);
                    aq.id(R.id.ethb).getEditText().setError(null);
                }
            } else if (s == aq.id(R.id.etTemp).getEditable()) {
                enabledisabledatefields(aq.id(R.id.etTemp).getEditText(), aq.id(R.id.ettempdonedate).getEditText(), aq.id(R.id.imgcleardatetemp).getImageView());
                if (aq.id(R.id.etTemp).getText().toString().length() > 0
                        && Double.parseDouble(aq.id(R.id.etTemp).getText().toString()) <= 0) {
                    aq.id(R.id.etTemp).getEditText().setError(getResources().getString(R.string.incorrectinput));
                } else {
                    aq.id(R.id.etTemp).getEditText().setError(null);
                }
            } else if (s == aq.id(R.id.etbpsystolicsumm).getEditable()) {
                enabledisabledatefields(aq.id(R.id.etbpsystolicsumm).getEditText(), aq.id(R.id.etbpdonedate).getEditText(), aq.id(R.id.imgcleardatebp).getImageView());
                if (aq.id(R.id.etbpsystolicsumm).getText().toString().length() > 0
                        && Integer.parseInt(aq.id(R.id.etbpsystolicsumm).getText().toString()) <= 0) {
                    aq.id(R.id.etbpsystolicsumm).getEditText().setError(getResources().getString(R.string.incorrectinput));
                } else {
                    aq.id(R.id.etbpsystolicsumm).getEditText().setError(null);
                }
            } else if (s == aq.id(R.id.etbpdiastolicsumm).getEditable()) {
                enabledisabledatefields(aq.id(R.id.etbpdiastolicsumm).getEditText(), aq.id(R.id.etbpdonedate).getEditText(), aq.id(R.id.imgcleardatebp).getImageView());
                if (aq.id(R.id.etbpdiastolicsumm).getText().toString().length() > 0
                        && Integer.parseInt(aq.id(R.id.etbpdiastolicsumm).getText().toString()) <= 0) {
                    aq.id(R.id.etbpdiastolicsumm).getEditText().setError(getResources().getString(R.string.incorrectinput));
                } else {
                    aq.id(R.id.etbpdiastolicsumm).getEditText().setError(null);
                }
            } else if (s == aq.id(R.id.ethb).getEditable()) {
                enabledisabledatefields(aq.id(R.id.ethb).getEditText(), aq.id(R.id.ethbdonedate).getEditText(), aq.id(R.id.imgcleardatehb).getImageView());
                if (aq.id(R.id.ethb).getText().toString().length() > 0
                        && Double.parseDouble(aq.id(R.id.ethb).getText().toString()) <= 0) {
                    aq.id(R.id.ethb).getEditText().setError(getResources().getString(R.string.incorrectinput));
                } else {
                    aq.id(R.id.ethb).getEditText().setError(null);
                }
            } else if (s == aq.id(R.id.etweight).getEditable()) {
                enabledisabledatefields(aq.id(R.id.etweight).getEditText(), aq.id(R.id.etweightdate).getEditText(), aq.id(R.id.imgcleardateweight).getImageView());
                if (aq.id(R.id.etweight).getText().toString().length() > 0
                        && Double.parseDouble(aq.id(R.id.etweight).getText().toString()) <= 0) {
                    aq.id(R.id.etweight).getEditText().setError(getResources().getString(R.string.incorrectinput));
                } else {
                    aq.id(R.id.etweight).getEditText().setError(null);
                }
            } else if (s == aq.id(R.id.etpulse).getEditable()) {      //04Oct2019 - Bindu  - add pulse
                enabledisabledatefields(aq.id(R.id.etpulse).getEditText(), aq.id(R.id.etpulsedate).getEditText(), aq.id(R.id.imgcleardatepulse).getImageView());
                if (aq.id(R.id.etpulse).getText().toString().length() > 0
                        && Integer.parseInt(aq.id(R.id.etpulse).getText().toString()) <= 0) {
                    aq.id(R.id.etpulse).getEditText().setError(getResources().getString(R.string.incorrectinput));
                } else {
                    aq.id(R.id.etpulse).getEditText().setError(null);
                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            try {
                if (s == aq.id(R.id.etnoofparacetamolgiven).getEditText().getEditableText()) {
                    EnableDisableTabNotGivenReason(aq.id(R.id.etnoofparacetamolgiven).getEditText(), aq.id(R.id.spnparacetamolnotgivenreason).getSpinner());
                } else if (s == aq.id(R.id.etnoofifagiven).getEditText().getEditableText()) {
                    EnableDisableTabNotGivenReason(aq.id(R.id.etnoofifagiven).getEditText(), aq.id(R.id.spnifanotgivenreason).getSpinner());
                } else if (s == aq.id(R.id.etnooffsfagiven).getEditText().getEditableText()) {
                    EnableDisableTabNotGivenReason(aq.id(R.id.etnooffsfagiven).getEditText(), aq.id(R.id.spnfsfanotgivenreason).getSpinner());
                } else if (s == aq.id(R.id.etnoofalbendazolegiven).getEditText().getEditableText()) {
                    EnableDisableTabNotGivenReason(aq.id(R.id.etnoofalbendazolegiven).getEditText(), aq.id(R.id.spnalbendazolenotgivenreason).getSpinner());
                } else if (s == aq.id(R.id.ethbpalp).getEditText().getEditableText()) {
                    double hbval = (aq.id(R.id.ethbpalp).getText().toString().length() > 0 ? Double.parseDouble(aq.id(R.id.ethbpalp).getText().toString()) : 0);
                    if (hbval > 6 && hbval < 10) {
                        aq.id(R.id.txtpalpfatbreathdiagnosisval).text(getResources().getString(R.string.moderateanemia));
                        aq.id(R.id.txtpalpfatbreathdiagnosisvalsave).text(getResources().getString(R.string.moderateanemiasave)); //14Nov2019 - Bindu - to save only eng hidden val
                    } else if (hbval >= 0 || hbval < 7) {          //15Oct2019 - bindu - change cond
                        aq.id(R.id.txtpalpfatbreathdiagnosisval).text(getResources().getString(R.string.severeanemia));
                        aq.id(R.id.txtpalpfatbreathdiagnosisvalsave).text(getResources().getString(R.string.severeanemiasave)); //14Nov2019 - Bindu - to save only eng hidden val
                    }
                }




                /*if (aq.id(R.id.etnoofparacetamolgiven).getText().toString().length() > 0) {
                    aq.id(R.id.spnparacetamolnotgivenreason).enabled(true);
                    int nooftab = (aq.id(R.id.etnoofparacetamolgiven).getText().toString().length() > 0 ? Integer.parseInt(aq.id(R.id.etnoofparacetamolgiven).getText().toString()) : 0);
                    if(nooftab <=0) {
                        if (aq.id(R.id.spnparacetamolnotgivenreason).getSelectedItem().toString().equalsIgnoreCase("Select")) {
                            if (aq.id(R.id.spnparacetamolnotgivenreason).getSpinner().getSelectedView().toString() != null) { // 04Aug2019 - Bindu - default mapto service type

                                TextView errorText = (TextView) aq.id(R.id.spnparacetamolnotgivenreason).getSpinner().getSelectedView();
                                errorText.setError("");
                                errorText.setTextColor(Color.RED);//just to highlight that this is an error
                                errorText.setText(getResources().getString(R.string.selectreason));//changes the selected item text to this
                                errorText.setFocusable(true);

                                aq.id(R.id.etnoofparacetamolgiven).getEditText().requestFocus();
                                aq.id(R.id.etnoofparacetamolgiven).getEditText().setFocusableInTouchMode(true); //set error and focus
                            }
                        }
                    }else {
                        aq.id(R.id.spnparacetamolnotgivenreason).enabled(false);
                        aq.id(R.id.spnparacetamolnotgivenreason).setSelection(0);
                        TextView errorText = (TextView) aq.id(R.id.spnparacetamolnotgivenreason).getSpinner().getSelectedView();
                        errorText.setError(null);
                    }
                }else {
                    aq.id(R.id.spnparacetamolnotgivenreason).enabled(false);
                    aq.id(R.id.spnparacetamolnotgivenreason).setSelection(0);
                    TextView errorText = (TextView) aq.id(R.id.spnparacetamolnotgivenreason).getSpinner().getSelectedView();
                    errorText.setError(null);
                    errorText.setText(getResources().getString(R.string.select));
                    errorText.setTextColor(Color.GRAY);
                }*/
            } catch (Exception e) {

            }
        }
    };

    // insert to audi trail table
    private String checkForAuditTrail(int transId) throws Exception {
        String upSql = null;
        /*
        String addString = "";
        String editedValues = "";

        //09Apr2021 - Bindu UID, type, Date
        if (!covVacItems.get(0).getCovidVaccinated().equalsIgnoreCase(tblcovvac.getCovidVaccinated().toString())) {
            if (addString == "")
                addString = addString + " CovidVaccinated = " + (char) 34 +tblcovvac.getCovidVaccinated() + (char) 34 + "";
            else
                addString = addString + " ,CovidVaccinated = " + (char) 34 + tblcovvac.getCovidVaccinated() + (char) 34 + "";
            InserttblAuditTrailCovidVac("CovidVaccinated",
                    String.valueOf(covVacItems.get(0).getCovidVaccinated()),
                    String.valueOf(tblcovvac.getCovidVaccinated()),
                    transId);
        }
        if (!covVacItems.get(0).getCovidFirstDoseDate().equalsIgnoreCase(tblcovvac.getCovidFirstDoseDate().toString())) {
            if (addString == "")
                addString = addString + " CovidFirstDoseDate = " + (char) 34 +tblcovvac.getCovidFirstDoseDate() + (char) 34 + "";
            else
                addString = addString + " ,CovidFirstDoseDate = " + (char) 34 + tblcovvac.getCovidFirstDoseDate() + (char) 34 + "";
            InserttblAuditTrailCovidVac("CovidVaccinated",
                    String.valueOf(covVacItems.get(0).getCovidFirstDoseDate()),
                    String.valueOf(tblcovvac.getCovidFirstDoseDate()),
                    transId);
        }
        if (!covVacItems.get(0).getCovidSecondDoseDate().equalsIgnoreCase(tblcovvac.getCovidSecondDoseDate().toString())) {
            if (addString == "")
                addString = addString + " CovidSecondDoseDate = " + (char) 34 +tblcovvac.getCovidSecondDoseDate() + (char) 34 + "";
            else
                addString = addString + " ,CovidSecondDoseDate = " + (char) 34 + tblcovvac.getCovidSecondDoseDate() + (char) 34 + "";
            InserttblAuditTrailCovidVac("CovidSecondDoseDate",
                    String.valueOf(covVacItems.get(0).getCovidSecondDoseDate()),
                    String.valueOf(tblcovvac.getCovidSecondDoseDate()),
                    transId);
        }

        if (addString == "")
            addString = addString + " RecordUpdatedDate = " + (char) 34 + tblcovvac.getRecordUpdatedDate() + (char) 34 + "";
        else
            addString = addString + " ,RecordUpdatedDate = " + (char) 34 + tblcovvac.getRecordUpdatedDate() + (char) 34 + "";
        InserttblAuditTrailCovidVac("RecordUpdatedDate",
                String.valueOf(covVacItems.get(0).getRecordUpdatedDate()),
                String.valueOf(tblcovvac.getRecordUpdatedDate()),
                transId);

        if (addString != null && addString.length() > 0) {
            upSql = "UPDATE tblCovidVaccineDetails SET ";
            upSql = upSql + addString + " WHERE WomenId = '"
                    + appState.selectedWomanId + "' and UserId = '" + woman.getUserId() + "'";
        }*/
        return upSql;
    }

    private boolean InserttblAuditTrailCovidVac(String ColumnName, String Old_Value, String New_Value, int transId
    ) throws Exception {
        AuditPojo APJ = new AuditPojo();
        APJ.setUserId(appState.sessionUserId);
        APJ.setWomanId(woman.getWomanId());
        APJ.setTblName("tblCovidVaccineDetails");
        APJ.setDateChanged(DateTimeUtil.getTodaysDate());
        APJ.setColumnName(ColumnName);
        APJ.setOld_Value(Old_Value);
        APJ.setNew_Value(New_Value);
        APJ.setTransId(transId);
        APJ.setRecordcreateddate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
        AuditRepository auditRepository = new AuditRepository(databaseHelper);
        return auditRepository.insertTotblAuditTrail(APJ);
    }

    //Ramesh 1-6-2021
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
                File file = new File(mCurrentPhotoPath);
                FileUtils fileUtils = new FileUtils(getApplicationContext());
                file = new File(fileUtils.getPath(Uri.fromFile(file)));
                file = new Compressor(getApplicationContext()).compressToFile(file);
                Bitmap bitmap = MediaStore.Images.Media
                        .getBitmap(getApplicationContext().getContentResolver(), Uri.fromFile(file));
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100 /*ignored for PNG*/, bos);
                byte[] bitmapdata = bos.toByteArray();
                String timeStamp = new SimpleDateFormat("HHmmssSSS").format(new Date());
                String fn;
                String dir;
                fn = "IMG_" + woman.getWomanId() + "_HV" + visitNum + timeStamp + ".jpeg";
//                dir = "/Sentiacare/sentiatendAsha/Images_Dir";
                dir = AppState.imgDirRef;
                File fileParent = new File(dir);

                String fileName = fileParent + "/" + fn;

                ImagePojo imagePojo = new ImagePojo();
                imagePojo.setImage(bitmapdata);
                imagePojo.setImagePath(fileName);
                imagePojo.setImageBitmap(bitmap);

                AllImagePojo.add(imagePojo);

                tblImageStore imageStore = new tblImageStore();
                imageStore.setUserId(user.getUserId());
                imageStore.setBeneficiaryId(woman.getWomanId());
                imageStore.setImageName(fn.replace(".jpeg", ""));
                imageStore.setImagePath(fileName);
                imageStore.setTransId(transId);
                imageStore.setRecordCreatedDate(DateTimeUtil.getTodaysDate()+" "+DateTimeUtil.getCurrentTime());
                imageStore.setRecordUpdatedDate(DateTimeUtil.getTodaysDate()+" "+DateTimeUtil.getCurrentTime());
                AllImagesBeforeSave.add(imageStore);
                AsyncImage asyncImage = new AsyncImage();
                asyncImage.execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class AsyncImage extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {

            Collections.sort(AllImagesBeforeSave, (file1, file2) -> {
                long k = new File(file1.getImagePath()).lastModified() - new File(file2.getImagePath()).lastModified();
                if (k < 0) {
                    return 1;
                } else if (k == 0) {
                    return 0;
                } else {
                    return -1;
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            startRecyclerView();
        }
    }

    public void startRecyclerView() {
        try {
            recyclerViewImage = findViewById(R.id.recyclerImage);
            if (AllImagesBeforeSave.size() == 0) {
                aq.id(R.id.lladdphotos).gone();
            } else {
                aq.id(R.id.lladdphotos).visible();
                recyclerViewImage.setVisibility(View.VISIBLE);
                aq.id(R.id.txtnoimageadded).getTextView().setVisibility(View.GONE);
                DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
                float screenWidthDp = displayMetrics.widthPixels / displayMetrics.density;
                int noOfColumns = (int) (screenWidthDp / 125 + 0.5); // +0.5 for correct rounding to int.
                GridLayoutManager layoutManager = new GridLayoutManager(HomeVisit.this, noOfColumns);
                recyclerViewImage.setLayoutManager(layoutManager);
                recyclerViewImage.setNestedScrollingEnabled(false);
                recyclerViewImage.setHasFixedSize(true);
                RecyclerViewAdapter recyclerViewAdapter = new RecyclerViewAdapter(HomeVisit.this, AllImagesBeforeSave, databaseHelper, AllImagePojo, aq);
                recyclerViewImage.setAdapter(recyclerViewAdapter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static Bitmap rotateImageIfRequired(Bitmap img, Uri selectedImage) throws IOException {

        ExifInterface ei = new ExifInterface(selectedImage.getPath());
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return rotateImage(img, 90);
        }
    }

    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }

    private class AsyncFileUpload extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            try {
                OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                        .connectTimeout(60, TimeUnit.SECONDS)
                        .readTimeout(60, TimeUnit.SECONDS)
                        .writeTimeout(60, TimeUnit.SECONDS)
                        .build();

                for (tblImageStore s : AllImagesBeforeSave) {
                    String imagepath = AppState.imgDirRef +"/"+ s.getImageName() + ".jpeg";
                    File file = new File(imagepath);

                    RequestBody requestBody = new MultipartBody.Builder()
                            .setType(MultipartBody.FORM)
                            .addFormDataPart("filename", s.getImageName())
                            .addFormDataPart(s.getImageName(), s.getImageName(),
                                    RequestBody.create(MediaType.parse("image/jpg"), file))
                            .build();
                    String endpoint = "http://" + appState.webServiceIpAddress + appState.webServicePath + "image";
                    Request request = new Request.Builder()
                            .url(endpoint)
                            .post(requestBody)
                            .build();
                    try (Response r2 = okHttpClient.newCall(request).execute()) {
                        String responseString2 = r2.body().string();
                        Log.i("Image Upload", responseString2);
                        updateImageStatus(s.getImageName());
                    }catch (Exception e){
                        Log.i("Image Upload", e.getMessage());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    private void updateImageStatus(String imageName) throws Exception {
        ImageStoreRepository imageStoreRepository = new ImageStoreRepository(databaseHelper);
        int update = imageStoreRepository.updateImageStatusof(imageName);
        if (update > 0) {
            Log.e("Image", "Image uploaded");
        } else {
            Log.e("Image", "Image Not uploaded");
        }
    }
}
