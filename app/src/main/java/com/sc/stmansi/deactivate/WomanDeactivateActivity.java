package com.sc.stmansi.deactivate;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.legacy.app.ActionBarDrawerToggle;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.misc.TransactionManager;
import com.sc.stmansi.About.AboutActivity;
import com.sc.stmansi.HomeVisit.HomeVisitListActivity;
import com.sc.stmansi.HomeVisit.PncHomeVisit;
import com.sc.stmansi.common.CommonDatePickerFragment;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.common.NavDrawerItem;
import com.sc.stmansi.common.NavDrawerListAdapter;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.delivery.DeliveryInfoActivity;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.repositories.AuditRepository;
import com.sc.stmansi.repositories.ChildRepository;
import com.sc.stmansi.repositories.DeliveryRepository;
import com.sc.stmansi.repositories.LoginRepository;
import com.sc.stmansi.repositories.TransactionHeaderRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.services.ServicesSummaryActivity;
import com.sc.stmansi.services.UnplannedServicesActivity;
import com.sc.stmansi.services.UnplannedServicesListActivity;
import com.sc.stmansi.tables.AuditPojo;
import com.sc.stmansi.tables.TblChildInfo;
import com.sc.stmansi.tables.TblDeactivate;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.viewprofile.ViewProfileActivity;
import com.sc.stmansi.womanlist.RegisteredWomenActionTabs;
import com.sc.stmansi.womanlist.WomanListActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

import com.sc.stmansi.R;

public class WomanDeactivateActivity extends AppCompatActivity
        implements View.OnClickListener , DatePickerDialog.OnDateSetListener,View.OnTouchListener{


    private AQuery aqDel;
    private AppState appState;
    private DatabaseHelper databaseHelper;
    private TblInstusers user;
    private tblregisteredwomen woman;
    TblChildInfo tblChildInfoFromDb;
    TblChildInfo tblChildInfo;
    TblDeactivate tblDeactivate;
    AuditPojo APJ;
    tblregisteredwomen womanFromDb;
    List<tblregisteredwomen> regList;
    EditText curEditText;
    boolean deactivateChild;
    List<TblChildInfo>   tblChildInfoFromDbList;
    int transId;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private List<tblregisteredwomen> wVisitDetails;
    RadioGroup rgdeactchild;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deactivatewoman);

        try {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(false);

            Bundle bundle = getIntent().getBundleExtra("globalState");
            appState = bundle.getParcelable("appState");
            databaseHelper = getHelper();

            womanFromDb = (tblregisteredwomen) getIntent().getSerializableExtra("woman");

            UserRepository userRepository = new UserRepository(databaseHelper);
            user = userRepository.getOneAuditedUser(appState.ashaId);
            aqDel = new AQuery(this);

            rgdeactchild = findViewById(R.id.rgdeactchild);

            initiateDrawer();

            aqDel.id(R.id.txtheadingdeac).text(getResources().getString(R.string.deactivating) + " " +
                    womanFromDb.getRegWomanName()+" ("+getResources().getString(R.string.woman)+")");

            aqDel.id(R.id.txtname).text(getResources().getString(R.string.reg_date) + " "
                    + womanFromDb.getRegRegistrationDate());
            if(womanFromDb.getRegPregnantorMother()==2)
            aqDel.id(R.id.txtdate).text(getResources().getString(R.string.txtADD) + ": "
                    + womanFromDb.getRegADDate());
            else
            aqDel.id(R.id.txtdate).text(getResources().getString(R.string.tvlmp) + ": "
                    + womanFromDb.getRegLMP());

            String[] reason;

            if (womanFromDb.getRegpregormotheratreg() == 1) {
                WomanRepository womanRepository = new WomanRepository(databaseHelper);

                wVisitDetails = womanRepository.getVisitDetailsFrmReg(womanFromDb.getWomanId(), appState.sessionUserId);
            }

            if(womanFromDb.getRegPregnantorMother()!=2)
            reason = getResources().getStringArray(R.array.deactwreason);
            else
                reason = getResources().getStringArray(R.array.deactwreasonMother);

            ArrayAdapter adapter = new ArrayAdapter(this, R.layout.simple_spinner_item,reason);

            aqDel.id(R.id.spndeacreason).getSpinner().setAdapter(adapter);

            getDeactivationData();

            aqDel.id(R.id.spndeacreason).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    aqDel.id(R.id.trdeactchild).gone();
                    rgdeactchild.clearCheck();

                    /* strings from strings file - 06May2021 Arpitha*/

                    if(aqDel.id(R.id.spndeacreason).getSelectedItem()
                            .toString().equalsIgnoreCase(getResources().getString(R.string.dereaothers)))
                    {
                        aqDel.id(R.id.trotherdeactreason).visible();
                        aqDel.id(R.id.trreasondate).gone();
                        aqDel.id(R.id.etreasondate).text("");
                    }
                    else if(aqDel.id(R.id.spndeacreason).getSelectedItem()
                            .toString().equalsIgnoreCase(getResources().getString(R.string.derearelocated))||
                            aqDel.id(R.id.spndeacreason).getSelectedItem()
                                    .toString().equalsIgnoreCase(getResources().getString(R.string.dereaabortion))||
                            aqDel.id(R.id.spndeacreason).getSelectedItem()
                                    .toString().equalsIgnoreCase(getResources().getString(R.string.dereamortality)))
                    {
                        aqDel.id(R.id.trreasondate).visible();
                        if(aqDel.id(R.id.spndeacreason).getSelectedItem()
                                .toString().equalsIgnoreCase(getResources().getString(R.string.dereaabortion)))
                        aqDel.id(R.id.txtreasondate).text(getResources().getString(R.string.abortiondate));
                       else if(aqDel.id(R.id.spndeacreason).getSelectedItem()
                                .toString().equalsIgnoreCase(getResources().getString(R.string.derearelocated))) {
                            aqDel.id(R.id.txtreasondate).text(getResources().getString(R.string.relocateddate));
                            if(womanFromDb.getRegPregnantorMother()==2)
                            aqDel.id(R.id.trdeactchild).visible();
                        }

                       else if(aqDel.id(R.id.spndeacreason).getSelectedItem()
                                .toString().equalsIgnoreCase(getResources().getString(R.string.dereamortality)))
                            aqDel.id(R.id.txtreasondate).text(getResources().getString(R.string.mortalitydate));


                        aqDel.id(R.id.trotherdeactreason).gone();
                        aqDel.id(R.id.etotherreason).text("");

                    }
                    else
                    {
                        aqDel.id(R.id.trotherdeactreason).gone();
                        aqDel.id(R.id.etotherreason).text("");
                        aqDel.id(R.id.trreasondate).gone();
                        aqDel.id(R.id.etreasondate).text("");
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            //            //26Nov2019 Arpitha
            if(user.getIsDeactivated()==1)
            {
                disableScreen();
            }

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }


    }


    //    initialize db object
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void onBackPressed() {

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        try {
            int noOfWeeks = 0;
//            getSupportActionBar().setIcon(R.drawable.ett);
            if (womanFromDb.getRegADDate() != null && womanFromDb.getRegADDate().length() > 0) {
                getSupportActionBar().setTitle((womanFromDb.getRegWomanName() + "\t"));
            } else {

                noOfWeeks = DateTimeUtil.getNumberOfWeeksFromLMP(womanFromDb.getRegLMP());
                getSupportActionBar().setTitle((womanFromDb.getRegWomanName() + " (" + noOfWeeks + " "
                        + getResources().getString(R.string.weeks) + ")"));
            }
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);


            aqDel.id(R.id.tvWomanName).text(womanFromDb.getRegWomanName() + " (" + (noOfWeeks > 0 ? noOfWeeks : "") + " "
                    + getResources().getString(R.string.weeks) + ")");
//            aqServ.id(R.id.tvInfo1).text(getResources().getString(R.string.tvlmp) + " - " + woman.getRegLMP());
            if (womanFromDb.getRegADDate() != null && womanFromDb.getRegADDate().length() > 0) {
                aqDel.id(R.id.tvInfo1).text((getResources().getString(R.string.tvadd) + " " + womanFromDb.getRegADDate()));
                aqDel.id(R.id.tvWomanName).text(womanFromDb.getRegWomanName());
            } else {
                int noOfWeeks1 = DateTimeUtil.getNumberOfWeeksFromLMP(womanFromDb.getRegLMP());
                // aqDel.id(R.id.tvInfo1).text((noOfWeeks1 + getResources().getString(R.string.weeks)));
                aqDel.id(R.id.tvInfo1).text((getResources().getString(R.string.tvlmp) + ": " + womanFromDb.getRegLMP()));
                aqDel.id(R.id.tvWomanName).text(womanFromDb.getRegWomanName() + " (" + (noOfWeeks1 > 0 ? noOfWeeks1 : "") + " "
                        + getResources().getString(R.string.weeks) + ")");
            }
            aqDel.id(R.id.ivWomanImg).gone();

            getMenuInflater().inflate(R.menu.servicesmenu, menu);

        }catch (Exception e)
        {
FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (womanFromDb.getRegpregormotheratreg() != 2) {
            if (mDrawerToggle.onOptionsItemSelected(item)) {
                return true;
            }
        }

        switch (item.getItemId()) {
            case R.id.logout: {
                Intent goToScreen = new Intent(WomanDeactivateActivity.this, LoginActivity.class);
                displayAlert(getResources().getString(R.string.m111), goToScreen,false);
                return true;
            }
            case R.id.home: {
                Intent home = new Intent(WomanDeactivateActivity.this, MainMenuActivity.class);
                home.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                startActivity(home);
                return true;
            }
            case R.id.wlist: {
                Intent goToScreen = new Intent(WomanDeactivateActivity.this, RegisteredWomenActionTabs.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen,false);
                return true;
            }
            case R.id.about: {
                Intent goToScreen = new Intent(WomanDeactivateActivity.this, AboutActivity.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen,false);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    /**
     * Save Confirmation Alert
     *
     * @param goToScreen
     */
    private void displayAlert(final String spanText2, final Intent goToScreen, final boolean isDeactivate) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        String strOkMess;
        if(goToScreen!=null)
            strOkMess = getResources().getString(R.string.yes);
        else
            strOkMess = getResources().getString(R.string.ok);


        // set dialog message
        alertDialogBuilder.setMessage(spanText2).setCancelable(false).setNegativeButton(
                strOkMess, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        try {
                            if (goToScreen != null) {

                                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));


                                if (spanText2.contains(getResources().getString(R.string.save))) {
                                    saveDeactivateData();
                                }

                            else if (isDeactivate) {
                                    int updateChild = 0;
                                    for (int i = 0; i < tblChildInfoFromDbList.size(); i++) {
                                        tblChildInfoFromDb = tblChildInfoFromDbList.get(i);
                                        String sqlChildData = updateChildData(transId, tblChildInfoFromDb);
                                        ChildRepository childRepository = new ChildRepository(databaseHelper);
                                        updateChild = childRepository.updateChildData(user.getUserId(),
                                                sqlChildData, transId, databaseHelper);
                                    }
                                    if (updateChild > 0) {
                                        Intent wlist = new Intent(WomanDeactivateActivity.this, WomanListActivity.class);
                                        wlist.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                        wlist.putExtra("screen", "deact");
                                        startActivity(wlist);
                                    }

                                }else {

                                    startActivity(goToScreen);
                                    LoginRepository loginRepository = new LoginRepository(databaseHelper);
                                    loginRepository.updateLogoutTime(appState.loginAuditId, appState.sessionUserId);


                                }
                            }
                            else
                                dialog.cancel();
                        } catch (Exception e) {
                            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                            //}
                        }
                    }
                });
        if(goToScreen!=null) {
            alertDialogBuilder.setPositiveButton(getResources().getString(R.string.m122),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            if(spanText2.contains("Deactive"))
                            {
                                Intent wlist = new Intent(WomanDeactivateActivity.this, WomanListActivity.class);
                                wlist.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                wlist.putExtra("screen", "deact");
                                startActivity(wlist);
                            }
                            dialog.cancel();
                        }
                    });
        }
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }



    @Override
    public void onClick(View v) {

        try {
            switch (v.getId()) {

                case R.id.btnsave:
                    if(validateDeactFields()) {
                        Intent intent = new Intent(WomanDeactivateActivity.this,
                                WomanListActivity.class);
                        displayAlert(getResources().getString(R.string.m099), intent, false);
                    }
                    break;

                case R.id.btncancel:
                    Intent home = new Intent(WomanDeactivateActivity.this,
                            MainMenuActivity.class);
                    home.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    displayAlert(getResources().getString(R.string.exit), home,false);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    void saveDeactivateData() throws Exception {




        final TransactionHeaderRepository transactionHeaderRepository = new TransactionHeaderRepository(databaseHelper);
       transId = transactionHeaderRepository.iCreateNewTrans(user.getUserId(), databaseHelper);
//        .setTransId(transId);

        TransactionManager.callInTransaction(databaseHelper.getConnectionSource(), new Callable<Object>() {


            @Override
            public Object call() throws Exception {


                String sql = updateDelData(transId);

                WomanRepository womanRepository = new WomanRepository(databaseHelper);
                boolean added =  transactionHeaderRepository.iNewRecordTransNew(appState.sessionUserId, transId, "tblaudittrail", databaseHelper);

                if(added) {
                int updateReg = womanRepository.updateWomenRegDataNew(user.getUserId(), sql, transId, null, databaseHelper);


                if (updateReg > 0) {
                           if (aqDel.id(R.id.rddeactchildyes).isChecked() && womanFromDb.getRegPregnantorMother()==2)
                           {

                               ChildRepository childRepository = new ChildRepository(databaseHelper);
                               tblChildInfoFromDbList =
                                       childRepository.getChildForMother(womanFromDb.getWomanId(), womanFromDb.getUserId());

                               if(tblChildInfoFromDbList!=null && tblChildInfoFromDbList.size()>0) {
                                   for (int i = 0; i < tblChildInfoFromDbList.size(); i++) {
                                       tblChildInfoFromDb = tblChildInfoFromDbList.get(i);
                                       String sqlChildData = updateChildData(transId, tblChildInfoFromDb);
                                       int updateChild = childRepository.updateChildData(user.getUserId(),
                                               sqlChildData, transId, databaseHelper);

                                       if(updateChild>0)
                                       {
                                           Intent wlist = new Intent(WomanDeactivateActivity.this, WomanListActivity.class);
                                           wlist.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                           wlist.putExtra("screen", "deact");
                                           startActivity(wlist);
                                       }
                                   }

                               }else {
                                   Intent wlist = new Intent(WomanDeactivateActivity.this, WomanListActivity.class);
                                   wlist.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                   wlist.putExtra("screen", "deact");
                                   startActivity(wlist);
                               }



                           }else {
                               Intent wlist = new Intent(WomanDeactivateActivity.this, WomanListActivity.class);
                               wlist.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                               wlist.putExtra("screen", "deact");
                               startActivity(wlist);
                           }
                   }
                }


                return null;
            }
        });

    }


    private String updateDelData(int transId) throws Exception {

        woman = new tblregisteredwomen();
        woman.setDateDeactivated(aqDel.id(R.id.etdeactivationdate).getText().toString());

       String[] arrReason = getResources().getStringArray(R.array.deactwreasonsave);

        int pos = new ArrayList<String>(Arrays.asList(arrReason)).indexOf(aqDel.id(R.id.spndeacreason).getSelectedItem().toString());
        woman.setReasonforDeactivation(String.
                valueOf(pos));
        woman.setOtherDeactivationReason(aqDel.id(R.id.etotherreason).getText().toString());
        woman.setDeacComments(aqDel.id(R.id.etcommentsdeact).getText().toString());
        woman.setRecordUpdatedDate(DateTimeUtil.getTodaysDate()+" "+ DateTimeUtil.getCurrentTime());


        if(aqDel.id(R.id.spndeacreason).getSelectedItem()
                .toString().equalsIgnoreCase(getResources().getString(R.string.dereaabortion)))
            woman.setDateAborted(aqDel.id(R.id.etreasondate).getText().toString());
        else if(aqDel.id(R.id.spndeacreason).getSelectedItem()
                .toString().equalsIgnoreCase(getResources().getString(R.string.derearelocated)))
            woman.setDateTransferred(aqDel.id(R.id.etreasondate).getText().toString());

        else if(aqDel.id(R.id.spndeacreason).getSelectedItem()
                .toString().equalsIgnoreCase(getResources().getString(R.string.dereamortality)))
            woman.setDateExpired(aqDel.id(R.id.etreasondate).getText().toString());

        woman.setCurrentWomenStatus("deact");




        return checkForAudit(transId);
    }

    String checkForAudit(int transId) throws Exception {
        String addString = "", delSql = "";

        if (addString == "")
            addString = addString + " DateDeactivated = " + (char) 34 + woman.getDateDeactivated() + (char) 34 + "";
        else
            addString = addString + " ,DateDeactivated = " + (char) 34 + woman.getDateDeactivated() + (char) 34 + "";
        InserttblAuditTrail("DateDeactivated",
                womanFromDb.getDateDeactivated(),
                woman.getDateDeactivated(),
                transId);

        if (addString == "")
            addString = addString + " ReasonforDeactivation = " + (char) 34 + woman.getReasonforDeactivation() + (char) 34 + "";
        else
            addString = addString + " ,ReasonforDeactivation = " + (char) 34 + woman.getReasonforDeactivation() + (char) 34 + "";
        InserttblAuditTrail("ReasonforDeactivation",
                womanFromDb.getReasonforDeactivation(),
                woman.getReasonforDeactivation(),
                transId);

        if (addString == "")
            addString = addString + " OtherDeactivationReason = " + (char) 34 + woman.getOtherDeactivationReason() + (char) 34 + "";
        else
            addString = addString + " ,OtherDeactivationReason = " + (char) 34 + woman.getOtherDeactivationReason() + (char) 34 + "";
        InserttblAuditTrail("OtherDeactivationReason",
                womanFromDb.getOtherDeactivationReason(),
                woman.getOtherDeactivationReason(),
                transId);


        if (addString == "")
            addString = addString + " DeacComments = " + (char) 34 + woman.getDeacComments() + (char) 34 + "";
        else
            addString = addString + " ,DeacComments = " + (char) 34 + woman.getDeacComments() + (char) 34 + "";
        InserttblAuditTrail("DeacComments",
                womanFromDb.getDeacComments(),
                woman.getDeacComments(),
                transId);


        if(aqDel.id(R.id.spndeacreason).getSelectedItem()
                .toString().equalsIgnoreCase(getResources().getString(R.string.derearelocated))) {
            if (addString == "")
                addString = addString + " DateTransferred = " + (char) 34 + woman.getDateTransferred() + (char) 34 + "";
            else
                addString = addString + " ,DateTransferred = " + (char) 34 + woman.getDateTransferred() + (char) 34 + "";
            InserttblAuditTrail("DateTransferred",
                    womanFromDb.getDateTransferred(),
                    woman.getDateTransferred(),
                    transId);
        }else  if(aqDel.id(R.id.spndeacreason).getSelectedItem()
                .toString().equalsIgnoreCase(getResources().getString(R.string.dereamortality))) {
            if (addString == "")
                addString = addString + " DateExpired = " + (char) 34 + woman.getDateExpired() + (char) 34 + "";
            else
                addString = addString + " ,DateExpired = " + (char) 34 + woman.getDateExpired() + (char) 34 + "";
            InserttblAuditTrail("DateExpired",
                    womanFromDb.getDateExpired(),
                    woman.getDateExpired(),
                    transId);
        }else  if(aqDel.id(R.id.spndeacreason).getSelectedItem()
                .toString().equalsIgnoreCase(getResources().getString(R.string.dereaabortion))) {
            if (addString == "")
                addString = addString + " DateAborted = " + (char) 34 + woman.getDateAborted() + (char) 34 + "";
            else
                addString = addString + " ,DateAborted = " + (char) 34 + woman.getDateAborted() + (char) 34 + "";
            InserttblAuditTrail("DateAborted",
                    womanFromDb.getDateAborted(),
                    woman.getDateAborted(),
                    transId);
        }


        if (addString == "")
            addString = addString + " CurrentWomenStatus = " + (char) 34 + woman.getCurrentWomenStatus() + (char) 34 + "";
        else
            addString = addString + " ,CurrentWomenStatus = " + (char) 34 + woman.getCurrentWomenStatus() + (char) 34 + "";
        InserttblAuditTrail("CurrentWomenStatus",
                womanFromDb.getCurrentWomenStatus(),
                woman.getCurrentWomenStatus(),
                transId);


        if (addString == "")
            addString = addString + " RecordUpdatedDate = " + (char) 34 + woman.getRecordUpdatedDate() + (char) 34 + "";
        else
            addString = addString + " ,RecordUpdatedDate = " + (char) 34 + woman.getRecordUpdatedDate() + (char) 34 + "";
        InserttblAuditTrail("RecordUpdatedDate",
                womanFromDb.getRecordUpdatedDate(),
                woman.getRecordUpdatedDate(),
                transId);

        if (addString != null && addString.length() > 0) {
            delSql = "UPDATE tblregisteredwomen SET ";
            delSql = delSql + addString + " WHERE WomanId = '"
                    + appState.selectedWomanId + "' and UserId = '" + user.getUserId() + "'";
        }
        return delSql;
    }


    /**
     * Setting AuditTrail pojo, calls - InserttblAuditTrail()
     *
     * @param transId
     */
    private boolean InserttblAuditTrail(String ColumnName, String Old_Value, String
            New_Value, int transId) throws Exception {

        APJ = new AuditPojo();
        APJ.setUserId(user.getUserId());
        APJ.setWomanId(womanFromDb.getWomanId());//17July2021 Arpitha
        APJ.setTblName("tblregisteredwomen");
        APJ.setDateChanged(DateTimeUtil.getTodaysDate());
        APJ.setColumnName(ColumnName);
        APJ.setOld_Value(Old_Value);
        APJ.setNew_Value(New_Value);
        APJ.setTransId(transId);
        APJ.setRecordcreateddate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());

        AuditRepository auditRepository = new AuditRepository(databaseHelper);
        return auditRepository.insertTotblAuditTrailNew(APJ, databaseHelper);

    }

    private void getDeactivationData() {
        try {
            WomanRepository womanRepository = new WomanRepository(databaseHelper);
            regList = womanRepository.getDeactivationData(womanFromDb.getWomanId(), user.getUserId());

            if(regList!=null && regList.size()>0)
            {
                tblregisteredwomen tblregisteredwomen = regList.get(0);

                aqDel.id(R.id.etdeactivationdate).text(tblregisteredwomen.getDateDeactivated());
                aqDel.id(R.id.etotherreason).text(tblregisteredwomen.getOtherDeactivationReason());
                aqDel.id(R.id.etcommentsdeact).text(tblregisteredwomen.getDeacComments());

                int reason = Integer.parseInt(tblregisteredwomen.
                        getReasonforDeactivation());
                String[] se =getResources().getStringArray(R.array.deactwreasonsave);


                String sel = new ArrayList<String>(Arrays.asList(se)).get
                        (reason);

                String[] sele;

                if(womanFromDb.getRegPregnantorMother()==1)
                sele = getResources().getStringArray(R.array.deactwreason);
                else
                    sele = getResources().getStringArray(R.array.deactwreasonMother);

                int pos = new ArrayList<String>(Arrays.asList(sele)).indexOf(sel);

                aqDel.id(R.id.spndeacreason).setSelection(pos);



                if(aqDel.id(R.id.spndeacreason).getSelectedItem()
                        .toString().equalsIgnoreCase(getResources().getString(R.string.dereaabortion)))
                    aqDel.id(R.id.etreasondate).text(tblregisteredwomen.getDateAborted());
                else if(aqDel.id(R.id.spndeacreason).getSelectedItem()
                        .toString().equalsIgnoreCase(getResources().getString(R.string.derearelocated)))
                    aqDel.id(R.id.etreasondate).text(tblregisteredwomen.getDateTransferred());

                else if(aqDel.id(R.id.spndeacreason).getSelectedItem()
                        .toString().equalsIgnoreCase(getResources().getString(R.string.dereamortality)))
                    aqDel.id(R.id.etreasondate).text(tblregisteredwomen.getDateExpired());


                TableLayout tblDeact = findViewById(R.id.tbldeact);
                disableEnableControls(false,tblDeact);
                aqDel.id(R.id.btnsave).enabled(false);
                aqDel.id(R.id.btnsave).gone();//01May2021


            }else{
                aqDel.id(R.id.etdeactivationdate).getEditText().setOnTouchListener(this);
                aqDel.id(R.id.etdeactivationdate).text(DateTimeUtil.getTodaysDate());
                aqDel.id(R.id.etreasondate).getEditText().setOnTouchListener(this);

                DeliveryRepository deactivateRepository = new DeliveryRepository(databaseHelper);

               if( deactivateRepository.checkIsMotherAlive(womanFromDb.getWomanId()))
                {
                    if(womanFromDb.getRegPregnantorMother()==2)
                    aqDel.id(R.id.spndeacreason).setSelection(1);
                    else
                        aqDel.id(R.id.spndeacreason).setSelection(2);

                    aqDel.id(R.id.spndeacreason).enabled(false);
                    aqDel.id(R.id.spndeacreason).background(R.drawable.edittext_disable);
                    aqDel.id(R.id.etreasondate).enabled(false);

                    aqDel.id(R.id.etreasondate).text(womanFromDb.getRegADDate());

                }


            }

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }

    }

    private void disableEnableControls(boolean enable, ViewGroup vg) {
        for (int i = 0; i < vg.getChildCount(); i++) {
            View child = vg.getChildAt(i);
            child.setEnabled(enable);
            if (child instanceof ViewGroup) {
                disableEnableControls(enable, (ViewGroup) child);
            }
        }
    }



    @Override
    public boolean onTouch(View v, MotionEvent event) {

        if (MotionEvent.ACTION_UP == event.getAction()) {
            try {
                switch (v.getId()) {
                    case R.id.etdeactivationdate: {

                        if (event.getRawX() >= (aqDel.id(R.id.etdeactivationdate).getEditText().getRight()
                                - aqDel.id(R.id.etdeactivationdate).getEditText().getCompoundDrawables()
                                [2].getBounds().width())) {

                            aqDel.id(R.id.etdeactivationdate).text(DateTimeUtil.getTodaysDate());
                        } else
                            assignEditTextAndCallDatePicker(aqDel.id(R.id.etdeactivationdate).getEditText());
                        break;
                    } case R.id.etreasondate: {

                        if (event.getRawX() >= (aqDel.id(R.id.etreasondate).getEditText().getRight()
                                - aqDel.id(R.id.etreasondate).getEditText().getCompoundDrawables()
                                [2].getBounds().width())) {

                            aqDel.id(R.id.etreasondate).text("");
                        } else
                            assignEditTextAndCallDatePicker(aqDel.id(R.id.etreasondate).getEditText());
                        break;
                    }

                    default:
                        break;
                }
            } catch (Exception e) {

                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            }
        }
        return true;
    }


    /**
     * This method opens the Date Dialog sets Date to respective EditText
     */
    private void assignEditTextAndCallDatePicker(EditText editText) {
        curEditText = editText;
        String currDate = (editText.getText().length() > 0) ? editText.getText().toString() : "";
        CommonDatePickerFragment.getCommonDate(this, currDate);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {

        try {
            if (view.isShown()) {
                String bldSmpldate = null;
                bldSmpldate = String.format("%02d", day) + "-" + String.format("%02d", month + 1) + "-"
                        + year;

                Date selectedDate = new SimpleDateFormat("dd-MM-yyyy").parse(bldSmpldate);

                Date regDate = null, delDate = null;

                if (womanFromDb!=null && womanFromDb.getRegRegistrationDate() != null && womanFromDb.getRegRegistrationDate().trim().length() > 0)
                    regDate = new SimpleDateFormat("dd-MM-yyyy").parse(womanFromDb.getRegRegistrationDate());

                if (womanFromDb!=null && womanFromDb.getRegADDate() != null &&
                        womanFromDb.getRegADDate().trim().length() > 0)
                    delDate = new SimpleDateFormat("dd-MM-yyyy").
                            parse(womanFromDb.getRegADDate());





                if(curEditText == aqDel.id(R.id.etdeactivationdate).getEditText()) {
                    if (selectedDate.after(new Date())) {
                        displayAlert(getResources().getString(R.string.date_cannot_af_currentdate), null,false);
                        aqDel.id(R.id.etdeactivationdate).text("");
                    } else if (regDate != null && selectedDate.before(regDate)) {
                        displayAlert(getResources().getString(R.string.date_cannot_bf_regdate), null,false);
                        aqDel.id(R.id.etdeactivationdate).text("");
                    }else if (delDate != null && selectedDate.before(delDate)) {
                        displayAlert(getResources().getString(R.string.date_cannot_bf_deldate), null,false);
                        aqDel.id(R.id.etdeactivationdate).text("");
                    }
                    else
                        aqDel.id(R.id.etdeactivationdate).text(bldSmpldate);
                }else if(curEditText == aqDel.id(R.id.etreasondate).getEditText())
                {
                    {
                        if (selectedDate.after(new Date())) {
                            displayAlert(getResources().getString(R.string.date_cannot_af_currentdate), null,false);
                            aqDel.id(R.id.etreasondate).text("");
                        } else if (regDate != null && selectedDate.before(regDate)) {
                            displayAlert(getResources().getString(R.string.date_cannot_bf_regdate), null,false);
                            aqDel.id(R.id.etreasondate).text("");
                        } else if (delDate != null && selectedDate.before(delDate)) {
                            displayAlert(getResources().getString(R.string.date_cannot_bf_deldate), null,false);
                            aqDel.id(R.id.etreasondate).text("");
                        }else
                            aqDel.id(R.id.etreasondate).text(bldSmpldate);
                    }
                }


            }
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    //    Arpitha 26Nov2019
    void disableScreen()
    {
        TableLayout tbldeact  = findViewById(R.id.tbldeact);
        aqDel.id(R.id.btnsave).enabled(false);
        disableEnableControls(false,tbldeact);

    }




    private String updateChildData(int transId , TblChildInfo tblChildInfoFromList ) throws Exception {

        tblChildInfo = new TblChildInfo();
        tblChildInfo.setChlDeactDate(aqDel.id(R.id.etdeactivationdate).getText().toString());
        tblChildInfo.setChlDeactReason(String.valueOf(aqDel.id(R.id.spndeacreason).getSelectedItemPosition()));
        tblChildInfo.setChlDeactOtherReason(aqDel.id(R.id.etotherreason).getText().toString());
        tblChildInfo.setChlDeactComments(aqDel.id(R.id.etcommentsdeact).getText().toString());

        if(aqDel.id(R.id.spndeacreason).getSelectedItem()
                .toString().equalsIgnoreCase(getResources().getString(R.string.derearelocated)))
            tblChildInfo.setChlDeactRelocatedDate(aqDel.id(R.id.etreasondate).getText().toString());

        else if(aqDel.id(R.id.spndeacreason).getSelectedItem()
                .toString().equalsIgnoreCase(getResources().getString(R.string.dereamortality)))
            tblChildInfo.setChlDeactMortalityDate(aqDel.id(R.id.etreasondate).getText().toString());


        tblChildInfo.setRecordUpdatedDate(DateTimeUtil.getTodaysDate()+" "+ DateTimeUtil.getCurrentTime());
        return checkForAuditChild(transId , tblChildInfoFromList);
    }

    String checkForAuditChild(int transId, TblChildInfo tblChildInfoFromList) throws Exception {
        String addString = "", delSql = "";

        if (addString == "")
            addString = addString + " chlDeactDate = " + (char) 34 +  tblChildInfo.getChlDeactDate() + (char) 34 + "";
        else
            addString = addString + " ,chlDeactDate = " + (char) 34 +  tblChildInfo.getChlDeactDate() + (char) 34 + "";
        InserttblAuditTrail("chlDeactDate",
                tblChildInfoFromDb.getChlDeactDate(),
                tblChildInfo.getChlDeactDate(),
                transId);

        if (addString == "")
            addString = addString + " chlDeactReason = " + (char) 34 + tblChildInfo.getChlDeactReason() + (char) 34 + "";
        else
            addString = addString + " ,chlDeactReason = " + (char) 34 + tblChildInfo.getChlDeactReason() + (char) 34 + "";
        InserttblAuditTrail("chlDeactReason",
                tblChildInfoFromDb.getChlDeactReason(),
                tblChildInfo.getChlDeactReason(),
                transId);

        if (addString == "")
            addString = addString + " chlDeactOtherReason = " + (char) 34 + tblChildInfo.getChlDeactOtherReason() + (char) 34 + "";
        else
            addString = addString + " ,chlDeactOtherReason = " + (char) 34 + tblChildInfo.getChlDeactOtherReason() + (char) 34 + "";
        InserttblAuditTrail("chlDeactOtherReason",
                tblChildInfoFromDb.getChlDeactOtherReason(),
                tblChildInfo.getChlDeactOtherReason(),
                transId);


        if (addString == "")
            addString = addString + " chlDeactComments = " + (char) 34 + tblChildInfo.getChlDeactComments() + (char) 34 + "";
        else
            addString = addString + " ,chlDeactComments = " + (char) 34 + tblChildInfo.getChlDeactComments() + (char) 34 + "";
        InserttblAuditTrail("chlDeactComments",
                tblChildInfoFromDb.getChlDeactComments(),
                tblChildInfo.getChlDeactComments(),
                transId);


        if(aqDel.id(R.id.spndeacreason).getSelectedItem()
                .toString().equalsIgnoreCase(getResources().getString(R.string.derearelocated))) {
            if (addString == "")
                addString = addString + " chlDeactRelocatedDate = " + (char) 34 + tblChildInfo.getChlDeactRelocatedDate() + (char) 34 + "";
            else
                addString = addString + " ,chlDeactRelocatedDate = " + (char) 34 + tblChildInfo.getChlDeactRelocatedDate() + (char) 34 + "";
            InserttblAuditTrail("chlDeactRelocatedDate",
                    tblChildInfoFromDb.getChlDeactRelocatedDate(),
                    tblChildInfo.getChlDeactRelocatedDate(),
                    transId);
        }else  if(aqDel.id(R.id.spndeacreason).getSelectedItem()
                .toString().equalsIgnoreCase(getResources().getString(R.string.dereamortality))) {
            if (addString == "")
                addString = addString + " chlDeactMortalityDate = " + (char) 34 + tblChildInfo.getChlDeactMortalityDate() + (char) 34 + "";
            else
                addString = addString + " ,chlDeactMortalityDate = " + (char) 34 + tblChildInfo.getChlDeactMortalityDate() + (char) 34 + "";
            InserttblAuditTrail("chlDeactMortalityDate",
                    tblChildInfoFromDb.getChlDeactMortalityDate(),
                    tblChildInfo.getChlDeactMortalityDate(),
                    transId);
        }


        if (addString == "")
            addString = addString + " RecordUpdatedDate = " + (char) 34 + tblChildInfo.getRecordUpdatedDate() + (char) 34 + "";
        else
            addString = addString + " ,RecordUpdatedDate = " + (char) 34 + tblChildInfo.getRecordUpdatedDate() + (char) 34 + "";
        InserttblAuditTrail("RecordUpdatedDate",
                tblChildInfoFromDb.getRecordUpdatedDate(),
                tblChildInfo.getRecordUpdatedDate(),
                transId);

        if (addString != null && addString.length() > 0) {
            delSql = "UPDATE tblchildinfo SET ";
            delSql = delSql + addString + " WHERE chlID = '"
                    + tblChildInfoFromList.getChildID() + "' and UserId = '" + user.getUserId() + "'";
        }
        return delSql;
    }


//    06May2021 Arpitha - replace deact reason with strings
    private boolean validateDeactFields() {

        if(aqDel.id(R.id.etdeactivationdate).getText().toString().trim().length()<=0)
        {
            displayAlert(getResources().getString(R.string.enterdeactdate),null,false);
            aqDel.id(R.id.etdeactivationdate).getEditText().requestFocus();
            return false;
        }else if(aqDel.id(R.id.spndeacreason).getSelectedItemPosition()==0)
        {
            displayAlert(getResources().getString(R.string.selectdeactreason),null,false);

            TextView errorText = (TextView) aqDel.id(R.id.spndeacreason).getSpinner().getSelectedView();
            errorText.setError("");
            errorText.setTextColor(Color.RED);//just to highlight that this is an error
            errorText.setText(getResources().getString(R.string.selectdeactreason));//changes the selected item text to this
            errorText.setFocusable(true);            return false;
        }
        /* strings from strings file - 06May2021 Arpitha*/
        else if(aqDel.id(R.id.etotherreason).getText().toString().trim().length()<=0
                && aqDel.id(R.id.spndeacreason).getSelectedItem().toString()
                .equalsIgnoreCase(getResources().getString(R.string.dereaothers)))
        {
            displayAlert(getResources().getString(R.string.enterotherreason),null,false);
            aqDel.id(R.id.etotherreason).getEditText().requestFocus();
            return false;
        }else if(aqDel.id(R.id.etreasondate).getText().toString().trim().length()<=0 && aqDel.id(R.id.spndeacreason).getSelectedItem().toString()
                .equalsIgnoreCase(getResources().getString(R.string.derearelocated)) )
        {
            displayAlert(getResources().getString(R.string.enterrelocateddate),null,false);
            aqDel.id(R.id.etreasondate).getEditText().requestFocus();
            return false;
        }else if(aqDel.id(R.id.etreasondate).getText().toString().trim().length()<=0 &&
                aqDel.id(R.id.spndeacreason).getSelectedItem().toString()
                        .equalsIgnoreCase(getResources().getString(R.string.dereamortality)) )
        {
            displayAlert(getResources().getString(R.string.entermortality),null,false);
            aqDel.id(R.id.etreasondate).getEditText().requestFocus();
            return false;
        }else if(aqDel.id(R.id.etreasondate).getText().toString().trim().length()<=0 &&
                aqDel.id(R.id.spndeacreason).getSelectedItem().toString()
                        .equalsIgnoreCase(getResources().getString(R.string.dereaabortion)) )
        {
            displayAlert(getResources().getString(R.string.enterabortion),null,false);
            aqDel.id(R.id.etreasondate).getEditText().requestFocus();
            return false;
        }
        return true;
    }



    private void initiateDrawer() throws Exception {
        mDrawerLayout = findViewById(R.id.ett_drawer_layout);
        mDrawerList = findViewById(R.id.ett_left_drawer);


        ArrayList<NavDrawerItem> navDrawerItems = new ArrayList<>();
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.sp_WRegEdit), R.drawable.registration));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.services), R.drawable.anm_pending_activities));//01Oct2019 Arpitha

        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.addunplannedservices), R.drawable.anm_pending_activities));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.unplannedserviceslist), R.drawable.anm_pending_activities));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.homevisitadd),
                R.drawable.prenatalhomevisit));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.womanhomevisithistory),
                R.drawable.prenatalhomevisit));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deliveryHeading), R.drawable.delivery_info));//01Oct2019 Arpitha

       /* navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deactivate),
                R.drawable.deactivate));//28Nov2019 Arpitha
*/

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        mDrawerList.setAdapter(new NavDrawerListAdapter(getApplicationContext(), navDrawerItems));

//        if (woman.getRegpregormotheratreg() != 2) {//27Nov2019 Arpitha
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer,
                R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu(); // creates call to
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                getSupportActionBar().setDisplayShowTitleEnabled(false);
                getSupportActionBar().setDisplayShowCustomEnabled(false);

                int noOfWeeks;
                try {
                    if (womanFromDb.getRegADDate() != null && womanFromDb.getRegADDate().length() > 0) {
                        getSupportActionBar().setTitle((womanFromDb.getRegWomanName() + "\t"));
                    } else {
                        noOfWeeks = DateTimeUtil.getNumberOfWeeksFromLMP(womanFromDb.getRegLMP());
                        getSupportActionBar().setTitle((womanFromDb.getRegWomanName() + " (" + noOfWeeks + " "
                                + getResources().getString(R.string.weeks) + ")"));
                    }
                } catch (ParseException e) {


                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                try {
                    invalidateOptionsMenu(); // creates call to
                    getSupportActionBar().setDisplayShowHomeEnabled(false);
                    getSupportActionBar().setDisplayShowTitleEnabled(false);
                    getSupportActionBar().setDisplayShowCustomEnabled(true);
                    getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);

                    aqDel.id(R.id.ivWomanImg).getImageView().setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            mDrawerLayout.closeDrawer(mDrawerList);
                        }
                    });

                    aqDel.id(R.id.tvWomanName).text((womanFromDb.getRegWomanName() + "\t"));
                    if (womanFromDb.getRegADDate() != null && womanFromDb.getRegADDate().length() > 0) {
                        aqDel.id(R.id.tvInfo1).text((getResources().getString(R.string.tvadd) + " " + womanFromDb.getRegADDate()));
                    } else {
                        int noOfWeeks = DateTimeUtil.getNumberOfWeeksFromLMP(womanFromDb.getRegLMP());
                        aqDel.id(R.id.tvInfo1).text((noOfWeeks + getResources().getString(R.string.weeks)));
                    }
                } catch (Exception e) {


                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }
            }
        };
        // }
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    /**
     * This method is to sync drawerToggle and to replace up caret icon to menu
     * icon in Action Bar
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }


    /**
     * The click listener for ListView in the navigation drawer
     */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            mDrawerLayout.closeDrawer(mDrawerList);
            try {

                switch (position) {
                    case 0:
                        Intent viewProfile = new Intent(WomanDeactivateActivity.this, ViewProfileActivity.class);
                        viewProfile.putExtra("woman", womanFromDb);
                        displayAlert(getResources().getString(R.string.exit), viewProfile,false);

                        break;
                    case 1:
                        Intent services = new Intent(WomanDeactivateActivity.this, ServicesSummaryActivity.class);
                        services.putExtra("woman", womanFromDb);
                        displayAlert(getResources().getString(R.string.exit), services,false);

                        break;

                    case 2:
                        Intent unplanned = new Intent(WomanDeactivateActivity.this, UnplannedServicesActivity.class);
                        unplanned.putExtra("woman", womanFromDb);
                        displayAlert(getResources().getString(R.string.exit), unplanned,false);

                        break;
                    case 3:
                        Intent unPlannedList = new Intent(WomanDeactivateActivity.this, UnplannedServicesListActivity.class);
                        unPlannedList.putExtra("woman", womanFromDb);
                        displayAlert(getResources().getString(R.string.exit), unPlannedList,false);

                        break;
                    case 4:

                        if (womanFromDb!=null && womanFromDb.getRegPregnantorMother() == 2) { //02Dec2019 - Bindu - add condition - PNC Home visit only
                            Intent homeVisit = new Intent(WomanDeactivateActivity.this, PncHomeVisit.class);
                            displayAlert(getResources().getString(R.string.exit), homeVisit,false);
                        }
                        break;
                    case 5:
                      //  if (wVisitDetails != null && wVisitDetails.get(0).getLastAncVisitDate().length() > 0 || wVisitDetails.get(0).getLastPncVisitDate().length() > 0) {
                            Intent homeVisitHistory = new Intent(WomanDeactivateActivity.this, HomeVisitListActivity.class);
                            displayAlert(getResources().getString(R.string.exit), homeVisitHistory,false);

                       /* } else {
                            Toast.makeText(WomanDeactivateActivity.this, getResources().getString(R.string.norecords), Toast.LENGTH_SHORT).show();
                        }*/
                        break;

                    case 6:
                        if (womanFromDb.getRegPregnantorMother() == 2 || DateTimeUtil.getNumberOfDaysFromLMP(womanFromDb.getRegLMP()) > 210) {
                            Intent del = new Intent(WomanDeactivateActivity.this, DeliveryInfoActivity.class);
                            del.putExtra("woman", womanFromDb);
                            del.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(del);
                        } else
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.m132), Toast.LENGTH_LONG).show();
                        break;

                }
            } catch (Exception e) {


                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            }
        }
    }

}
