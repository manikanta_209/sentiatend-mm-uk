package com.sc.stmansi.deactivate;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.legacy.app.ActionBarDrawerToggle;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.misc.TransactionManager;
import com.sc.stmansi.About.AboutActivity;
import com.sc.stmansi.R;
import com.sc.stmansi.childregistration.AddSiblingsActivity;
import com.sc.stmansi.childregistration.EditChildActivity;
import com.sc.stmansi.childregistration.EditRegChildInfoActivity;
import com.sc.stmansi.childregistration.RegisteredChildList;
import com.sc.stmansi.childregistration.ViewParentDetails;
import com.sc.stmansi.common.CommonDatePickerFragment;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.common.NavDrawerItem;
import com.sc.stmansi.common.NavDrawerListAdapter;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.immunization.ImmunizationListActivity;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.repositories.AuditRepository;
import com.sc.stmansi.repositories.ChildRepository;
import com.sc.stmansi.repositories.LoginRepository;
import com.sc.stmansi.repositories.TransactionHeaderRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.tables.AuditPojo;
import com.sc.stmansi.tables.TblChildInfo;
import com.sc.stmansi.tables.TblDeactivate;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.viewprofile.ViewProfileActivity;
import com.sc.stmansi.womanlist.RegisteredWomenActionTabs;
import com.sc.stmansi.womanlist.WomanListActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

public class ChildDeactivateActivity extends AppCompatActivity
        implements View.OnClickListener, DatePickerDialog.OnDateSetListener, View.OnTouchListener {


    private AQuery aqDel;
    private AppState appState;
    private DatabaseHelper databaseHelper;
    private TblInstusers user;
    TblChildInfo tblChildInfo;
    TblDeactivate tblDeactivate;
    AuditPojo APJ;
    TblChildInfo tblChildInfoFromDb;
    List<TblChildInfo> regList;
    EditText curEditText;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deactivatewoman);

        try {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(false);

            Bundle bundle = getIntent().getBundleExtra("globalState");
            appState = bundle.getParcelable("appState");
            databaseHelper = getHelper();

            tblChildInfoFromDb = (TblChildInfo) getIntent().getSerializableExtra("tblChildInfo");

            UserRepository userRepository = new UserRepository(databaseHelper);
            user = userRepository.getOneAuditedUser(appState.ashaId);
            aqDel = new AQuery(this);


            aqDel.id(R.id.txtheadingdeac).text(getResources().getString(R.string.deactivating) + " " +
                    tblChildInfoFromDb.getChlBabyname() + " (" + getResources().getString(R.string.child) + ")");

            aqDel.id(R.id.txtname).text(getResources().getString(R.string.dob) + " "
                    + tblChildInfoFromDb.getChlDateTimeOfBirth());
            aqDel.id(R.id.txtdate).text(getResources().getString(R.string.tvchildsex) + ": "
                    + tblChildInfoFromDb.getChlGender());

            initiateDrawer();


            String[] reason = getResources().getStringArray(R.array.deactcreason);
            ArrayAdapter adapter = new ArrayAdapter(this, R.layout.simple_spinner_item, reason);

            aqDel.id(R.id.spndeacreason).getSpinner().setAdapter(adapter);


            getDeactivationData();

            aqDel.id(R.id.spndeacreason).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    /* strings from strings file - 17May2021 Arpitha*/
                    if (aqDel.id(R.id.spndeacreason).getSelectedItem()
                            .toString().equalsIgnoreCase(getResources().getString(R.string.dereaothers))) {
                        aqDel.id(R.id.trotherdeactreason).visible();
                        aqDel.id(R.id.trreasondate).gone();
                        aqDel.id(R.id.etreasondate).text("");
                    } else if (aqDel.id(R.id.spndeacreason).getSelectedItem()
                            .toString().equalsIgnoreCase(getResources().getString(R.string.derearelocated)) ||
                            aqDel.id(R.id.spndeacreason).getSelectedItem()
                                    .toString().equalsIgnoreCase(getResources().getString(R.string.dereamortality))) {
                        aqDel.id(R.id.trreasondate).visible();

                        if (aqDel.id(R.id.spndeacreason).getSelectedItem()
                                .toString().equalsIgnoreCase(getResources().getString(R.string.derearelocated)))
                            aqDel.id(R.id.txtreasondate).text(getResources().getString(R.string.relocateddate));

                        else if (aqDel.id(R.id.spndeacreason).getSelectedItem()
                                .toString().equalsIgnoreCase(getResources().getString(R.string.dereamortality)))
                            aqDel.id(R.id.txtreasondate).text(getResources().getString(R.string.mortalitydate));


                        aqDel.id(R.id.trotherdeactreason).gone();
                        aqDel.id(R.id.etotherreason).text("");

                    } else {
                        aqDel.id(R.id.trotherdeactreason).gone();
                        aqDel.id(R.id.etotherreason).text("");
                        aqDel.id(R.id.trreasondate).gone();
                        aqDel.id(R.id.etreasondate).text("");
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

//            //26Nov2019 Arpitha
            if (user.getIsDeactivated() == 1) {
                disableScreen();
            }

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }


    }


    //    initialize db object
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void onBackPressed() {

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.servicesmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.logout: {
                Intent goToScreen = new Intent(ChildDeactivateActivity.this, LoginActivity.class);
                displayAlert(getResources().getString(R.string.m111), goToScreen);
                return true;
            }
            case R.id.home: {
                Intent home = new Intent(ChildDeactivateActivity.this, MainMenuActivity.class);
                home.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                startActivity(home);
                return true;
            }
            case R.id.wlist: {
                Intent goToScreen = new Intent(ChildDeactivateActivity.this, RegisteredWomenActionTabs.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }
            case R.id.about: {
                Intent goToScreen = new Intent(ChildDeactivateActivity.this, AboutActivity.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    /**
     * Save Confirmation Alert
     *
     * @param goToScreen
     */
    private void displayAlert(final String spanText2, final Intent goToScreen) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        String strOkMess;
        if (goToScreen != null)
            strOkMess = getResources().getString(R.string.yes);
        else
            strOkMess = getResources().getString(R.string.ok);

        // set dialog message
        alertDialogBuilder.setMessage(spanText2).setCancelable(false).setNegativeButton(
                strOkMess, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        try {
                            if (goToScreen != null) {

                                if (spanText2.contains(getResources().getString(R.string.save))) {
                                    saveDeactivateData();
                                } else {

                                    startActivity(goToScreen);
                                    LoginRepository loginRepository = new LoginRepository(databaseHelper);
                                    loginRepository.updateLogoutTime(appState.loginAuditId, appState.sessionUserId);


                                }
                            } else
                                dialog.cancel();
                        } catch (Exception e) {
                            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                            crashlytics.log(e.getMessage());
                            //}
                        }
                    }
                });
        if (goToScreen != null) {
            alertDialogBuilder.setPositiveButton(getResources().getString(R.string.m122),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
        }
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    @Override
    public void onClick(View v) {

        try {
            switch (v.getId()) {

                case R.id.btnsave:
                    if (validateDeactFields()) {
                        Intent intent = new Intent(ChildDeactivateActivity.this,
                                WomanListActivity.class);


                        displayAlert(getResources().getString(R.string.m099), intent);
                    }
                    break;

                case R.id.btncancel:
                    Intent home = new Intent(ChildDeactivateActivity.this,
                            MainMenuActivity.class);
                    home.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    displayAlert(getResources().getString(R.string.exit), home);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private boolean validateDeactFields() {

        if (aqDel.id(R.id.etdeactivationdate).getText().toString().trim().length() <= 0) {
            displayAlert(getResources().getString(R.string.enterdeactdate), null);
            aqDel.id(R.id.etdeactivationdate).getEditText().requestFocus();
            return false;
        } else if (aqDel.id(R.id.spndeacreason).getSelectedItemPosition() == 0) {
            displayAlert(getResources().getString(R.string.selectdeactreason), null);

            TextView errorText = (TextView) aqDel.id(R.id.spndeacreason).getSpinner().getSelectedView();
            errorText.setError("");
            errorText.setTextColor(Color.RED);//just to highlight that this is an error
            errorText.setText(getResources().getString(R.string.selectdeactreason));//changes the selected item text to this
            errorText.setFocusable(true);
            return false;
        } else if (aqDel.id(R.id.etotherreason).getText().toString().trim().length() <= 0 && aqDel.id(R.id.spndeacreason).getSelectedItem().toString()
                .equalsIgnoreCase(getResources().getString(R.string.dereaothers))) {
            displayAlert(getResources().getString(R.string.enterotherreason), null);
            aqDel.id(R.id.etotherreason).getEditText().requestFocus();
            return false;
        } else if (aqDel.id(R.id.etreasondate).getText().toString().trim().length() <= 0 && aqDel.id(R.id.spndeacreason).getSelectedItem().toString()
                .equalsIgnoreCase(getResources().getString(R.string.derearelocated))) {
            displayAlert(getResources().getString(R.string.enterrelocateddate), null);
            aqDel.id(R.id.etreasondate).getEditText().requestFocus();
            return false;
        } else if (aqDel.id(R.id.etreasondate).getText().toString().trim().length() <= 0 &&
                aqDel.id(R.id.spndeacreason).getSelectedItem().toString()
                        .equalsIgnoreCase(getResources().getString(R.string.dereamortality))) {
            displayAlert(getResources().getString(R.string.entermortality), null);
            aqDel.id(R.id.etreasondate).getEditText().requestFocus();
            return false;
        }
        return true;
    }

    void saveDeactivateData() throws Exception {


        final TransactionHeaderRepository transactionHeaderRepository = new TransactionHeaderRepository(databaseHelper);
        final int transId = transactionHeaderRepository.iCreateNewTrans(user.getUserId(), databaseHelper);
//        .setTransId(transId);

        TransactionManager.callInTransaction(databaseHelper.getConnectionSource(), new Callable<Object>() {


            @Override
            public Object call() throws Exception {

//                DeactivateRepository deactivateRepository = new DeactivateRepository(databaseHelper);
//                deactivateRepository.insertIntoTblDeactivate(tblDeactivate);
                //  (addChildData > 0) {
                String sql = updateDelData(transId);
                transactionHeaderRepository.iNewRecordTransNew(appState.sessionUserId, transId, "tblaudittrail", databaseHelper);

                ChildRepository womanRepository = new ChildRepository(databaseHelper);
                int updateReg = womanRepository.updateChildData(user.getUserId(),
                        sql, transId, databaseHelper);


                if (updateReg > 0) {


                    Intent wlist = new Intent(ChildDeactivateActivity.this, RegisteredChildList.class);
                    wlist.putExtra("cond", "deact");
                    wlist.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    wlist.putExtra("screen", "deact");
                    startActivity(wlist);
                }


                return null;
            }
        });

    }


    private String updateDelData(int transId) throws Exception {

        tblChildInfo = new TblChildInfo();
        tblChildInfo.setChlDeactDate(aqDel.id(R.id.etdeactivationdate).getText().toString());
        tblChildInfo.setChlDeactReason(String.valueOf(aqDel.id(R.id.spndeacreason).getSelectedItemPosition()));
        tblChildInfo.setChlDeactOtherReason(aqDel.id(R.id.etotherreason).getText().toString());
        tblChildInfo.setChlDeactComments(aqDel.id(R.id.etcommentsdeact).getText().toString());

        if (aqDel.id(R.id.spndeacreason).getSelectedItem()
                .toString().equalsIgnoreCase(getResources().getString(R.string.derearelocated)))
            tblChildInfo.setChlDeactRelocatedDate(aqDel.id(R.id.etreasondate).getText().toString());

        else if (aqDel.id(R.id.spndeacreason).getSelectedItem()
                .toString().equalsIgnoreCase(getResources().getString(R.string.dereamortality)))
            tblChildInfo.setChlDeactMortalityDate(aqDel.id(R.id.etreasondate).getText().toString());


        tblChildInfo.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
        return checkForAudit(transId);
    }

    String checkForAudit(int transId) throws Exception {
        String addString = "", delSql = "";

        if (addString == "")
            addString = addString + " chlDeactDate = " + (char) 34 + tblChildInfo.getChlDeactDate() + (char) 34 + "";
        else
            addString = addString + " ,chlDeactDate = " + (char) 34 + tblChildInfo.getChlDeactDate() + (char) 34 + "";
        InserttblAuditTrail("chlDeactDate",
                tblChildInfoFromDb.getChlDeactDate(),
                tblChildInfo.getChlDeactDate(),
                transId);

        if (addString == "")
            addString = addString + " chlDeactReason = " + (char) 34 + tblChildInfo.getChlDeactReason() + (char) 34 + "";
        else
            addString = addString + " ,chlDeactReason = " + (char) 34 + tblChildInfo.getChlDeactReason() + (char) 34 + "";
        InserttblAuditTrail("chlDeactReason",
                tblChildInfoFromDb.getChlDeactReason(),
                tblChildInfo.getChlDeactReason(),
                transId);

        if (addString == "")
            addString = addString + " chlDeactOtherReason = " + (char) 34 + tblChildInfo.getChlDeactOtherReason() + (char) 34 + "";
        else
            addString = addString + " ,chlDeactOtherReason = " + (char) 34 + tblChildInfo.getChlDeactOtherReason() + (char) 34 + "";
        InserttblAuditTrail("chlDeactOtherReason",
                tblChildInfoFromDb.getChlDeactOtherReason(),
                tblChildInfo.getChlDeactOtherReason(),
                transId);


        if (addString == "")
            addString = addString + " chlDeactComments = " + (char) 34 + tblChildInfo.getChlDeactComments() + (char) 34 + "";
        else
            addString = addString + " ,chlDeactComments = " + (char) 34 + tblChildInfo.getChlDeactComments() + (char) 34 + "";
        InserttblAuditTrail("chlDeactComments",
                tblChildInfoFromDb.getChlDeactComments(),
                tblChildInfo.getChlDeactComments(),
                transId);


        if (aqDel.id(R.id.spndeacreason).getSelectedItem()
                .toString().equalsIgnoreCase(getResources().getString(R.string.derearelocated))) {
            if (addString == "")
                addString = addString + " chlDeactRelocatedDate = " + (char) 34 + tblChildInfo.getChlDeactRelocatedDate() + (char) 34 + "";
            else
                addString = addString + " ,chlDeactRelocatedDate = " + (char) 34 + tblChildInfo.getChlDeactRelocatedDate() + (char) 34 + "";
            InserttblAuditTrail("chlDeactRelocatedDate",
                    tblChildInfoFromDb.getChlDeactRelocatedDate(),
                    tblChildInfo.getChlDeactRelocatedDate(),
                    transId);
        } else if (aqDel.id(R.id.spndeacreason).getSelectedItem()
                .toString().equalsIgnoreCase(getResources().getString(R.string.dereamortality))) {
            if (addString == "")
                addString = addString + " chlDeactMortalityDate = " + (char) 34 + tblChildInfo.getChlDeactMortalityDate() + (char) 34 + "";
            else
                addString = addString + " ,chlDeactMortalityDate = " + (char) 34 + tblChildInfo.getChlDeactMortalityDate() + (char) 34 + "";
            InserttblAuditTrail("chlDeactMortalityDate",
                    tblChildInfoFromDb.getChlDeactMortalityDate(),
                    tblChildInfo.getChlDeactMortalityDate(),
                    transId);
        }


        if (addString == "")
            addString = addString + " RecordUpdatedDate = " + (char) 34 + tblChildInfo.getRecordUpdatedDate() + (char) 34 + "";
        else
            addString = addString + " ,RecordUpdatedDate = " + (char) 34 + tblChildInfo.getRecordUpdatedDate() + (char) 34 + "";
        InserttblAuditTrail("RecordUpdatedDate",
                tblChildInfoFromDb.getRecordUpdatedDate(),
                tblChildInfo.getRecordUpdatedDate(),
                transId);

        if (addString != null && addString.length() > 0) {
            delSql = "UPDATE tblchildinfo SET ";
            delSql = delSql + addString + " WHERE chlID = '"
                    + tblChildInfoFromDb.getChildID() + "' and UserId = '" + user.getUserId() + "'";
        }
        return delSql;
    }


    /**
     * Setting AuditTrail pojo, calls - InserttblAuditTrail()
     *
     * @param transId
     */
    private boolean InserttblAuditTrail(String ColumnName, String Old_Value, String
            New_Value, int transId) throws Exception {

        APJ = new AuditPojo();
        APJ.setUserId(user.getUserId());
        APJ.setWomanId(tblChildInfoFromDb.getChildID());
        APJ.setTblName("tblchildinfo");
        APJ.setDateChanged(DateTimeUtil.getTodaysDate());
        APJ.setColumnName(ColumnName);
        APJ.setOld_Value(Old_Value);
        APJ.setNew_Value(New_Value);
        APJ.setTransId(transId);
        APJ.setRecordcreateddate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());

        AuditRepository auditRepository = new AuditRepository(databaseHelper);
        return auditRepository.insertTotblAuditTrailNew(APJ, databaseHelper);

    }

    private void getDeactivationData() {
        try {
            ChildRepository womanRepository = new ChildRepository(databaseHelper);
            regList = womanRepository.getChildDeactivationData(tblChildInfoFromDb.getChildID(), user.getUserId());

            if (regList != null && regList.size() > 0) {
                TblChildInfo tblChildInfo = regList.get(0);

                aqDel.id(R.id.etdeactivationdate).text(tblChildInfo.getChlDeactDate());
                aqDel.id(R.id.etotherreason).text(tblChildInfo.getChlDeactOtherReason());
                aqDel.id(R.id.etcommentsdeact).text(tblChildInfo.getChlDeactComments());
                aqDel.id(R.id.spndeacreason).setSelection(Integer.parseInt(tblChildInfo.getChlDeactReason()));

                if (aqDel.id(R.id.spndeacreason).getSelectedItem()
                        .toString().equalsIgnoreCase(getResources().getString(R.string.derearelocated)))
                    aqDel.id(R.id.etreasondate).text(tblChildInfo.getChlDeactRelocatedDate());

                else if (aqDel.id(R.id.spndeacreason).getSelectedItem()
                        .toString().equalsIgnoreCase(getResources().getString(R.string.dereamortality)))
                    aqDel.id(R.id.etreasondate).text(tblChildInfo.getChlDeactMortalityDate());


                TableLayout tblDeact = findViewById(R.id.tbldeact);
                disableEnableControls(false, tblDeact);
                aqDel.id(R.id.btnsave).enabled(false);

            } else {
                aqDel.id(R.id.etdeactivationdate).getEditText().setOnTouchListener(this);
                aqDel.id(R.id.etdeactivationdate).text(DateTimeUtil.getTodaysDate());
                aqDel.id(R.id.etreasondate).getEditText().setOnTouchListener(this);


                ChildRepository childRepository = new ChildRepository(databaseHelper);
                if (tblChildInfoFromDb.getChlReg() != 1) { //11Apr2021 Bindu - Check condition whether direct or indirect child
                    if (!childRepository.checkSelectedChildAlive(tblChildInfoFromDb.getChildID())) {
                        aqDel.id(R.id.spndeacreason).setSelection(1);

                        aqDel.id(R.id.spndeacreason).enabled(false);
                        aqDel.id(R.id.spndeacreason).background(R.drawable.edittext_disable);
                        aqDel.id(R.id.etreasondate).enabled(false);

                        aqDel.id(R.id.etreasondate).text(tblChildInfoFromDb.getChlInfantDeathDate());
                    }
                }
            }

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }

    }

    private void disableEnableControls(boolean enable, ViewGroup vg) {
        for (int i = 0; i < vg.getChildCount(); i++) {
            View child = vg.getChildAt(i);
            child.setEnabled(enable);
            if (child instanceof ViewGroup) {
                disableEnableControls(enable, (ViewGroup) child);
            }
        }
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {

        if (MotionEvent.ACTION_UP == event.getAction()) {
            try {
                switch (v.getId()) {
                    case R.id.etdeactivationdate: {

                        if (event.getRawX() >= (aqDel.id(R.id.etdeactivationdate).getEditText().getRight()
                                - aqDel.id(R.id.etdeactivationdate).getEditText().getCompoundDrawables()
                                [2].getBounds().width())) {

                            aqDel.id(R.id.etdeactivationdate).text(DateTimeUtil.getTodaysDate());
                        } else
                            assignEditTextAndCallDatePicker(aqDel.id(R.id.etdeactivationdate).getEditText());
                        break;
                    }
                    case R.id.etreasondate: {

                        if (event.getRawX() >= (aqDel.id(R.id.etreasondate).getEditText().getRight()
                                - aqDel.id(R.id.etreasondate).getEditText().getCompoundDrawables()
                                [2].getBounds().width())) {

                            aqDel.id(R.id.etreasondate).text("");
                        } else
                            assignEditTextAndCallDatePicker(aqDel.id(R.id.etreasondate).getEditText());
                        break;
                    }
                    default:
                        break;
                }
            } catch (Exception e) {

                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
        }
        return true;
    }


    /**
     * This method opens the Date Dialog and sets Date to respective EditText
     */
    private void assignEditTextAndCallDatePicker(EditText editText) {
        curEditText = editText;
        String currDate = (editText.getText().length() > 0) ? editText.getText().toString() : "";
        CommonDatePickerFragment.getCommonDate(this, currDate);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {

        try {
            if (view.isShown()) {
                String bldSmpldate = null;
                bldSmpldate = String.format("%02d", day) + "-" + String.format("%02d", month + 1) + "-"
                        + year;

                Date selectedDate = new SimpleDateFormat("dd-MM-yyyy").parse(bldSmpldate);

                Date regDate = null, dob = null;

                if (tblChildInfoFromDb != null && tblChildInfoFromDb.getChlDateTimeOfBirth() != null && tblChildInfoFromDb.getChlDateTimeOfBirth().trim().length() > 0)
                    regDate = new SimpleDateFormat("dd-MM-yyyy").parse(tblChildInfoFromDb.getChlDateTimeOfBirth());

                if (tblChildInfo != null && tblChildInfo.getChlDateTimeOfBirth() != null) {
                    dob = new SimpleDateFormat("dd-MM-yyyy").
                            parse(tblChildInfo.getChlDateTimeOfBirth());
                }

                if (curEditText == aqDel.id(R.id.etdeactivationdate).getEditText()) {

                    if (selectedDate.after(new Date())) {
                        displayAlert(getResources().getString(R.string.date_cannot_af_currentdate), null);
                        aqDel.id(R.id.etdeactivationdate).text(DateTimeUtil.getTodaysDate());
                    } else if (regDate != null && selectedDate.before(regDate)) {
                        displayAlert(getResources().getString(R.string.dob_cannot_before_regdate), null);
                        aqDel.id(R.id.etdeactivationdate).text(DateTimeUtil.getTodaysDate());
                    } else if (dob != null && selectedDate.before(dob)) {
                        displayAlert(getResources().getString(R.string.date_cannot_before_dob), null);
                        aqDel.id(R.id.etdeactivationdate).text(DateTimeUtil.getTodaysDate());
                    } else
                        aqDel.id(R.id.etdeactivationdate).text(bldSmpldate);

                } else if (curEditText == aqDel.id(R.id.etreasondate).getEditText()) {
                    {
                        if (selectedDate.after(new Date())) {
                            displayAlert(getResources().getString(R.string.date_cannot_af_currentdate), null);
                            aqDel.id(R.id.etreasondate).text(DateTimeUtil.getTodaysDate());
                        } else if (regDate != null && selectedDate.before(regDate)) {
                            displayAlert(getResources().getString(R.string.date_cannot_bf_regdate), null);
                            aqDel.id(R.id.etreasondate).text(DateTimeUtil.getTodaysDate());
                        } else if (dob != null && selectedDate.before(dob)) {
                            displayAlert(getResources().getString(R.string.date_cannot_before_dob), null);
                            aqDel.id(R.id.etreasondate).text(DateTimeUtil.getTodaysDate());
                        } else
                            aqDel.id(R.id.etreasondate).text(bldSmpldate);
                    }
                }


            }
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    //    Arpitha 26Nov2019
    void disableScreen() {
        TableLayout tbldeact = findViewById(R.id.tbldeact);
        aqDel.id(R.id.btnsave).enabled(false);
        disableEnableControls(false, tbldeact);

    }


    private void initiateDrawer() throws Exception {
        mDrawerLayout = findViewById(R.id.ett_drawer_layout);
        mDrawerList = findViewById(R.id.ett_left_drawer);
        ArrayList<NavDrawerItem> navDrawerItems = new ArrayList<>();
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.editchildinfo), R.drawable.ic_edit_60));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.immunizationlist), R.drawable.immunisation));

        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.addsibling), R.drawable.general_examination_baby));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.viewparentdetails), R.drawable.ic_mother));


        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        mDrawerList.setAdapter(new NavDrawerListAdapter(getApplicationContext(), navDrawerItems));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer,
                R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu(); // creates call to
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                getSupportActionBar().setDisplayShowTitleEnabled(false);
                getSupportActionBar().setDisplayShowCustomEnabled(false);

                int noOfWeeks;
                try {
                    if (tblChildInfoFromDb != null) {
                        getSupportActionBar().setTitle((tblChildInfoFromDb.getChlBabyname() + "\t"));
                    }
                } catch (Exception e) {


                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                    crashlytics.log(e.getMessage());
                }
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                try {
                    invalidateOptionsMenu(); // creates call to
                    getSupportActionBar().setDisplayShowHomeEnabled(false);
                    getSupportActionBar().setDisplayShowTitleEnabled(false);
                    getSupportActionBar().setDisplayShowCustomEnabled(true);
                    getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);

                    aqDel.id(R.id.ivWomanImg).getImageView().setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            mDrawerLayout.closeDrawer(mDrawerList);
                        }
                    });

                    aqDel.id(R.id.tvWomanName).text((tblChildInfoFromDb.getChlBabyname() + "\t"));
                    if (tblChildInfoFromDb.getChlDateTimeOfBirth().trim().length() > 0) {
                        aqDel.id(R.id.tvInfo1).text(
                                (getResources().getString(R.string.dob) + " " +
                                        tblChildInfoFromDb.getChlDateTimeOfBirth()));
                    }
                } catch (Exception e) {


                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                    crashlytics.log(e.getMessage());
                }
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    /**
     * This method is to sync draw,erToggle and to replace up caret icon to menu
     * icon in Action Bar
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    /**
     * The click listener for ListView in the navigation drawer
     */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            mDrawerLayout.closeDrawer(mDrawerList);
            try {

                switch (position) {

                    case 0:
                        if (tblChildInfoFromDb.getChlReg() == 1) {
                            Intent nextScreen = new Intent(ChildDeactivateActivity.this, EditRegChildInfoActivity.class);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                            nextScreen.putExtra("tblChildInfo", tblChildInfoFromDb);
                            startActivity(nextScreen);
                        } else {
                            Intent nextScreen = new Intent(ChildDeactivateActivity.this, EditChildActivity.class);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                            nextScreen.putExtra("tblChildInfo", tblChildInfoFromDb);
                            startActivity(nextScreen);
                        }
                        break;


                    case 1: {
                        if (tblChildInfoFromDb.getChlDeliveryResult() <= 1) {
                            Intent addSibling = new Intent(ChildDeactivateActivity.this, ImmunizationListActivity.class);
                            addSibling.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            addSibling.putExtra("appState", getIntent().getBundleExtra("appState"));
                            addSibling.putExtra("tblChildInfo", tblChildInfoFromDb);
                            startActivity(addSibling);
                        } else
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.m053), Toast.LENGTH_LONG).show();

                        break;
                    }
                    case 2: {
                        Intent viewParent = new Intent(ChildDeactivateActivity.this, AddSiblingsActivity.class);
                        viewParent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        viewParent.putExtra("appState", getIntent().getBundleExtra("appState"));
                        if (tblChildInfoFromDb.getChlParentId() != null && tblChildInfoFromDb.getChlParentId().trim().length() > 0)
                            viewParent.putExtra("chlParentId", tblChildInfoFromDb.getChlParentId());
                        else
                            viewParent.putExtra("WomanId", tblChildInfoFromDb.getWomanId());
                        startActivity(viewParent);
                        break;
                    }
                    case 3: {
                        if (tblChildInfoFromDb.getChlReg() == 1) {
                            Intent viewParent = new Intent(ChildDeactivateActivity.this, ViewParentDetails.class);
                            viewParent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            viewParent.putExtra("appState", getIntent().getBundleExtra("appState"));
                            viewParent.putExtra("tblChildInfo", tblChildInfoFromDb);
                            startActivity(viewParent);
                        } else {
                            WomanRepository womanRepository = new WomanRepository(databaseHelper);
                            tblregisteredwomen woman = womanRepository.getRegistartionDetails(tblChildInfoFromDb.getWomanId(), tblChildInfoFromDb.getUserId());
                            appState.selectedWomanId = tblChildInfoFromDb.getWomanId();
                            Intent nextScreen = new Intent(ChildDeactivateActivity.this, ViewProfileActivity.class);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                            nextScreen.putExtra("woman", woman);
                            startActivity(nextScreen);
//                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.ntapplicable), Toast.LENGTH_LONG).show();
                        }
                        break;
                    }
                }

            } catch (Exception e) {


                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());
            }
        }
    }

}
