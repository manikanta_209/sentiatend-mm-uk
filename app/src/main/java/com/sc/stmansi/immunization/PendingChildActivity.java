package com.sc.stmansi.immunization;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.misc.TransactionManager;
import com.sc.stmansi.About.AboutActivity;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.pendingservices.IndicatorViewClickListener;
import com.sc.stmansi.quickAction.ActionItem;
import com.sc.stmansi.recyclerview.ClickListener;
import com.sc.stmansi.recyclerview.PendingChildAdapter;
import com.sc.stmansi.recyclerview.RecyclerViewMargin;
import com.sc.stmansi.repositories.FacilityRepository;
import com.sc.stmansi.repositories.ImmunizationRepository;
import com.sc.stmansi.repositories.LoginRepository;
import com.sc.stmansi.repositories.TransactionHeaderRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.repositories.WomanServiceRepository;
import com.sc.stmansi.tables.ChildImmunization;
import com.sc.stmansi.tables.TblChildImmunization;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.womanlist.CareType;
import com.sc.stmansi.womanlist.QueryParams;
import com.sc.stmansi.womanlist.RegisteredWomenActionTabs;

import java.lang.ref.WeakReference;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import com.sc.stmansi.R;

public class PendingChildActivity extends AppCompatActivity implements ClickListener,
        IndicatorViewClickListener,AdapterView.OnItemSelectedListener {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    AQuery aqChildList;
    private AppState appState;
    private DatabaseHelper databaseHelper;
    List<ChildImmunization> childList;
    private static final int ID_CEDIT = 1;
    private static final int ID_IMMU = 2;
    ChildImmunization tblChildInfo;
    private String wFilterStr = "";
    private CareType careType;
    private Map<String, Integer> villageCodeForName;
    private boolean isUpdating;
    private List<ChildImmunization> filteredRowItems;
    private RecyclerView.Adapter  registeredWomanAdapter;
    private int nextItemPosition;
    private Spinner villageSpinner;
    private boolean spinnerSelectionChanged = false;
    static boolean fromDate;
    EditText filterTextView;
    private String immType = null;
    private int tribalHamlet,tribalHamletpos;
    String strFromDate, strToDate;
    private AQuery aqPMSel, aqServ;
    private AlertDialog alertdialog = null;
    private String serviceAddedUserType = "", serviceupdateddate = "";
    static String strMinDate = "";
    String strMaxDate = "";
    String strWomanId;
    private ArrayList<String> facilitynamelist;
    private Map<String, String> mapFacilityname;
    int serviceId = 0;
    boolean pending;
    private AlertDialog.Builder alert;




    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registeredchildlist);

        try {

            Bundle bundle = getIntent().getBundleExtra("globalState");
            appState = bundle.getParcelable("appState");

            tribalHamlet = getIntent().getIntExtra("tribalHamlet", 0);
            tribalHamletpos = getIntent().getIntExtra("tribalHamletpos", 0);
            immType = getIntent().getStringExtra("immType");
            strFromDate = getIntent().getStringExtra("fromDate");
            strToDate = getIntent().getStringExtra("toDate");
            pending = getIntent().getBooleanExtra("pending",false);



            databaseHelper = getHelper();


            aqChildList = new AQuery(this);

            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

            getSupportActionBar().setCustomView(R.layout.ejanani_village_filter);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            villageSpinner = aqChildList.id(R.id.spVillageFilter).getSpinner();
            villageSpinner.setOnItemSelectedListener(this);
            populateSpinnerVillage();

            aqChildList.id(R.id.llFilter).gone();
            aqChildList.id(R.id.imgBtnWomanFilterSearch).invisible();


            aqChildList.id(R.id.etWomanFilter).invisible();
            int identifier = getResources().getIdentifier(immType.toLowerCase(), "string", "com.sc.stmansi");
            aqChildList.id(R.id.txtlabell).text(getResources().getString(identifier));
            aqChildList.id(R.id.txtlabell).getTextView().setTextSize(20);
            aqChildList.id(R.id.llheading).getView().setPadding(10,10,10,10);
            recyclerView = (RecyclerView) aqChildList.id(R.id.child_recycler_view).getView();

            aqChildList.id(R.id.imgresetfilter).visible();
            recyclerView.setHasFixedSize(true);
            // use a linear layout manager
            layoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(layoutManager);
            RecyclerViewMargin decoration = new RecyclerViewMargin(1, 1);
            recyclerView.addItemDecoration(decoration);
            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                    DividerItemDecoration.VERTICAL);
            DividerItemDecoration dividerItemDecoration1 = new DividerItemDecoration(recyclerView.getContext(),
                    DividerItemDecoration.HORIZONTAL);
            recyclerView.addItemDecoration(dividerItemDecoration);
            recyclerView.addItemDecoration(dividerItemDecoration1);





            filteredRowItems = prepareRowItemsForDisplay();
            isUpdating = true;
            registeredWomanAdapter = new PendingChildAdapter( this,filteredRowItems);
            recyclerView.setAdapter(registeredWomanAdapter);
            updateLabels();
            registeredWomanAdapter.notifyItemRangeInserted(0, filteredRowItems.size());
            nextItemPosition = filteredRowItems.size() + 1;

//            recyclerView.addOnScrollListener(new OnRecyclerScrollListener());
            isUpdating = false;

            updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr);


//            getData();


            String str = getResources().getString(R.string.viewprofile);
            ActionItem cEdit = new ActionItem(ID_CEDIT, str, ContextCompat.getDrawable(this,R.drawable.ic_edit_60));

            str = getResources().getString(R.string.sp_imm);
            ActionItem cImmunization = new ActionItem(ID_IMMU, str, ContextCompat.getDrawable(this,R.drawable.immunisation));






            filterTextView = aqChildList.id(R.id.etWomanFilter).getEditText();
            filterTextView.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
//                    aqChildList.id(R.id.imgresetfilter).getImageView().setBackgroundResource(R.drawable.ic_reset);
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            filterTextView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (v.getText() != null) {
                        wFilterStr = v.getText().toString();
                        updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr);
                    }
                    return true;
                }
            });



            aqChildList.id(R.id.imgresetfilter).getImageView().setOnClickListener(new
                    FilterResetListener());


            aqChildList.id(R.id.txtfromdate).getTextView().setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (MotionEvent.ACTION_UP == event.getAction()) {
                        fromDate = true;
                        DialogFragment newFragment = new DatePickerFragment(aqChildList);
                        newFragment.show(getSupportFragmentManager(), "datePicker");

                    }
                    return true;
                }
            });

            aqChildList.id(R.id.txttodate).getTextView().setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (MotionEvent.ACTION_UP == event.getAction()) {
                        fromDate = false;
                        DialogFragment newFragment = new DatePickerFragment(aqChildList);
                        newFragment.show(getSupportFragmentManager(), "datePicker");

                    }
                    return true;
                }
            });

            aqChildList.id(R.id.btnok).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

//                    if(aqChildList.id(R.id.spndatefilter).getSelectedItemPosition()==2
//                            && (strFromDate.length()<=0|| strToDate.length()<=0))
//                    {
//                        displayAlert(getResources().getString(R.string.selectdate),null);
//                    }else {
//                        wFilterStr = filterTextView.getText().toString();
//                        updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr);
//                    }


                }
            });



        }catch (Exception e)
        {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }


   /* public void getData() throws SQLException {
        ChildRepository childRepository = new ChildRepository(databaseHelper);
         childList = childRepository.getChildData(queryParams);
        if(childList!=null && childList.size()>0)
        {
             registeredWomanAdapter = new
                    ChildAdapter(RegisteredChildList.this,childList);
            recyclerView.setAdapter(registeredWomanAdapter);
            aqChildList.id(R.id.txtnodata).gone();
            recyclerView.setVisibility(View.VISIBLE);
        }
        else
        {
            aqChildList.id(R.id.txtnodata).visible();
            recyclerView.setVisibility(View.GONE);
        }

        aqChildList.id(R.id.txtcountwl).text(getResources().getString(R.string.recordcount,
                childList.size()));

    }*/

    @Override
    public void onClick(View v) {


        try {


            setSelectedChildId(v);
//              26nOV2019 arpitha
            UserRepository userRepository= new UserRepository(databaseHelper);
            TblInstusers user = userRepository.getOneAuditedUser(appState.ashaId);
            if((tblChildInfo.getChlDeactDate()!=null &&
                    tblChildInfo.getChlDeactDate().trim().length()>0)|| user.getIsDeactivated()==1)
            {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.userdeactivated), Toast.LENGTH_LONG).show();

            }else {
                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                Date seldate = format.parse(DateTimeUtil.getTodaysDate());
                Date mindate = null;


                WomanServiceRepository womanServiceRepository = new WomanServiceRepository(databaseHelper);
                int minduration = womanServiceRepository.getDaysforImmunization("DaysDiffStart", immType);


                strMinDate = getNextDate(tblChildInfo.getChlDateofBirth(),
                        minduration, tblChildInfo.getChlDateofBirth(), false);

                if (strMinDate != null && strMinDate.trim().length() > 0)
                    mindate = format.parse(strMinDate);

                if(!pending)
                {

                    addServiceDataOLD(this,null);

                }else {
                    if (mindate != null && seldate.before(mindate)) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.immunizationtapplibeforemindate), Toast.LENGTH_LONG).show();
                    } else {
                        addServiceData(this, immType, tblChildInfo);

                    }
                }
            }
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }

    }

    @Override
    public boolean onLongClick(View v) {
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)

    {
        getMenuInflater().inflate(R.menu.servicesmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId())
        {
            case android.R.id.home:
                Intent back = new Intent(PendingChildActivity.this, ChildImmunizationActivity.class);
                back.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                startActivity(back);
                return true;
            case R.id.logout: {
                Intent goToScreen = new Intent(PendingChildActivity.this, LoginActivity.class);
                displayAlert(getResources().getString(R.string.m111), goToScreen);
                return true;
            }
            case R.id.home: {
                Intent home = new Intent(PendingChildActivity.this, MainMenuActivity.class);
                home.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                startActivity(home);
                return true;
            }
            case R.id.wlist: {
                Intent goToScreen = new Intent(PendingChildActivity.this, RegisteredWomenActionTabs.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }
            case R.id.about: {
            Intent goToScreen = new Intent(PendingChildActivity.this, AboutActivity.class);
            goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
            displayAlert(getResources().getString(R.string.exit), goToScreen);
            return true;
        }
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    /**
     * Save Confirmation Alert
     *
     * @param goToScreen
     * @param spanText2
     */
    private void displayAlert(final String spanText2, final Intent goToScreen) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        String strmess = "";

        if(goToScreen!=null)
            strmess = getResources().getString(R.string.yes);
        else
            strmess = getResources().getString(R.string.ok);

        alertDialogBuilder.setMessage(spanText2).setCancelable(false).setNegativeButton(
                strmess, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (goToScreen != null) {

                            goToScreen.putExtra("globalState",getIntent().getBundleExtra("globalState"));
                            startActivity(goToScreen);
                            if (spanText2.contains("logout")) {
                                try {
                                    LoginRepository loginRepository = new LoginRepository(databaseHelper);
                                    loginRepository.updateLogoutTime(appState.loginAuditId, appState.sessionUserId);
                                } catch (Exception e) {
                                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                                }
                            }
                        } else
                            dialog.cancel();
                    }
                });

        if(goToScreen!=null) {
            alertDialogBuilder.setPositiveButton(getResources().getString(R.string.m122),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
        }
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }
    //    initialize db object
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void onBackPressed() {

    }


    private void setSelectedChildId(View v) {
        int itemPosition = recyclerView.getChildLayoutPosition(v);
        tblChildInfo = childList.get(itemPosition);

        appState.selectedWomanId = tblChildInfo.getChlId();
    }

    //    upadate list
    public void updateList(String villageTitle, Map<String, Integer> villageNameForCode, String textViewFilter) {
        try {
            QueryParams queryParams = new QueryParams();
            queryParams.nextItemPosition = 0;
            queryParams.selectedVillageTitle = villageTitle;
            queryParams.textViewFilter = textViewFilter;
            queryParams.careType = this.careType;
            queryParams.noVisitSelected = aqChildList.id(R.id.chknovisitnotdone).isChecked();
            queryParams.userId = appState.sessionUserId;
            queryParams.selectedDateFilter = aqChildList.id(R.id.spndatefilter).getSelectedItemPosition();

            this.wFilterStr = textViewFilter;
          /*  queryParams.fromDate = strFromDate;
            queryParams.toDate = strToDate;*/


            new LoadPregnantList(this, queryParams, true, villageNameForCode).execute();
            isUpdating = true;
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }

   /* @Override
    public void displayAlert(int selectedItemPosition) {
        ArrayList<String> compList;
        String complication = filteredRowItems.get(selectedItemPosition)
                .getChlComplications();
        if (complication != null && complication.length() > 0) {
            compList = new ArrayList<>();
            // compList.add(complication.replace(",", ", ").replace("PH:", "\nPH:"));


            String[] chlCompl;

            if(filteredRowItems.get(selectedItemPosition).getChlReg()!=null &&
                    filteredRowItems.get(selectedItemPosition).getChlReg().equalsIgnoreCase("1"))
                chlCompl = getResources().getStringArray(R.array.childComplications);
            else
                chlCompl = getResources().getStringArray(R.array.newborncompl);

            String[] selectedCompl = complication.split(",");

            ArrayList<String> complL = new ArrayList<>();

            for(int i=1;i<=selectedCompl.length;i++)
            {
                if(selectedCompl[i-1]!=null && selectedCompl[i-1].trim().length()>0)
                    complL.add(chlCompl[Integer.parseInt(selectedCompl[i-1].trim())]);
            }


            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            View convertView = LayoutInflater.from(this).inflate(R.layout.complicated_reasons_list, null, false);

            ListView lv = convertView.findViewById(R.id.lvCompliatedReasonslist);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.listitem, complL);

            alertDialog.setView(convertView).setCancelable(true).setTitle((this.getResources().getString(R.string.complications)))
                    .setPositiveButton((this.getResources().getString(R.string.ok)), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
            lv.setAdapter(adapter);
            alertDialog.show();
        }
    }*/

    @Override
    public void displayAlert(int selectedItemPosition) {
        ArrayList<String> compList;
        String complication = filteredRowItems.get(selectedItemPosition)
                .getChlComplications();
        if (complication != null && complication.length() > 0
                || (Integer.parseInt(filteredRowItems.get(selectedItemPosition).getChlReg())==0
                && filteredRowItems.get(selectedItemPosition).getWeight()!=null
                && filteredRowItems.get(selectedItemPosition).getWeight()
                .trim().length()>0
                && Double.parseDouble(filteredRowItems.get(selectedItemPosition).getWeight())<2500) ) {
            compList = new ArrayList<>();
            // compList.add(complication.replace(",", ", ").replace("PH:", "\nPH:"));


            String[] chlCompl;

            if(Integer.parseInt(filteredRowItems.get(selectedItemPosition).getChlReg())==1)
                chlCompl = getResources().getStringArray(R.array.childComplications);
            else
                chlCompl = getResources().getStringArray(R.array.newborncompl);

            String[] selectedCompl = complication.split(",");

            ArrayList<String> complL = new ArrayList<>();

            for(int i=1;i<=selectedCompl.length;i++)
            {
                if(selectedCompl[i-1]!=null && selectedCompl[i-1].trim().length()>0 && !selectedCompl[i-1].equalsIgnoreCase("0"))
                    complL.add(chlCompl[Integer.parseInt(selectedCompl[i-1].trim())]);
            }

            if(Integer.parseInt(filteredRowItems.get(selectedItemPosition).getChlReg())
                    ==0 && filteredRowItems.get(selectedItemPosition)
                    .getWeight()!=null
                    && filteredRowItems.get(selectedItemPosition)
                    .getWeight().trim().length()>0
                    && Double.parseDouble(filteredRowItems.get(selectedItemPosition)
                    .getWeight())<2500 )
                complL.add(getResources().getString(R.string.weightlessthan2500gram));



            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            View convertView = LayoutInflater.from(this).inflate(R.layout.complicated_reasons_list, null, false);

            ListView lv = convertView.findViewById(R.id.lvCompliatedReasonslist);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.listitem, complL);

            alertDialog.setView(convertView).setCancelable(true).setTitle((this.getResources().getString(R.string.complications)))
                    .setPositiveButton((this.getResources().getString(R.string.ok)), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
            lv.setAdapter(adapter);
            alertDialog.show();
        }
    }

    @Override
    public void displayHomeVisitList(int selectedItemPosition) {

    }

    @Override
    public void displayDelComplicationAlert(int selectedItemPosition) {

    }


    //    load list data
    static class LoadPregnantList extends AsyncTask<Void, Void, Void> {

        private WeakReference<PendingChildActivity> weakReference;
        private QueryParams queryParams;
        private boolean firstLoad;
        private Map<String, Integer> villageCodeForName;

        LoadPregnantList(PendingChildActivity viewsUpdateCallback, QueryParams queryParams, boolean firstLoad, Map<String, Integer> villageCodeForName) {
            this.weakReference = new WeakReference<>(viewsUpdateCallback);
            this.queryParams = queryParams;
            this.firstLoad = firstLoad;
            this.villageCodeForName = villageCodeForName;
        }

        LoadPregnantList(PendingChildActivity viewsUpdateCallback, QueryParams queryParams, boolean firstLoad) {
            this.weakReference = new WeakReference<>(viewsUpdateCallback);
            this.queryParams = queryParams;
            this.firstLoad = firstLoad;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            int selectedCode;
            // This means second constructor was used (called by scrollListener)
            if (villageCodeForName == null) selectedCode = queryParams.selectedVillageCode;
            else {
                String selectedVillageTitle = queryParams.selectedVillageTitle;
                if (selectedVillageTitle == null)
                    selectedCode = 0;
                else {
                    Integer code = villageCodeForName.get(selectedVillageTitle);
                    selectedCode = (code == null) ? 0 : code;
                }
            }
            try {
                PendingChildActivity activity = weakReference.get();
                List<ChildImmunization> rows = activity.prepareRowItemsForDisplay();
                if (firstLoad) {
                    int prevSize = activity.filteredRowItems.size();
                    activity.filteredRowItems.clear();
                    activity.registeredWomanAdapter.notifyItemRangeRemoved(0, prevSize);
                    firstLoad = false;
                }
                if (rows.size() > 0) {
                    activity.filteredRowItems.addAll(rows);
                    activity.nextItemPosition = activity.filteredRowItems.size() + 1;
                    if (rows.size() > 0) {
                        activity.registeredWomanAdapter.notifyItemRangeInserted(firstLoad ? 0 : activity.nextItemPosition,
                                activity.filteredRowItems.size());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            try {
                PendingChildActivity activity = weakReference.get();
                activity.updateLabels();
                if (!firstLoad && activity.filteredRowItems.size() > 0)
                    activity.layoutManager.scrollToPosition(activity.registeredWomanAdapter.getItemCount());
                activity.isUpdating = false;
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

            }
        }
    }

    class FilterResetListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            try {
//            aqChildList.id(R.id.imgresetfilter).getImageView().setBackgroundResource(R.drawable.ic_reset_gray);
                aqChildList.id(R.id.etWomanFilter).text("");
                wFilterStr = "";
                CareType careType = CareType.GENERAL;
                villageSpinner.setSelection(0);
                aqChildList.id(R.id.spndatefilter).setSelection(0);
//            strFromDate = "";
//            strToDate= "";
                updateList("", villageCodeForName, wFilterStr);
            }catch (Exception e)
            {
               e.printStackTrace();
            }
        }
    }

    //   data to display
    private List<ChildImmunization> prepareRowItemsForDisplay() throws Exception {

        ImmunizationRepository childRepository = new ImmunizationRepository(databaseHelper);

        if(!pending)
        childList = childRepository.getCompletedChild(appState.sessionUserId,tribalHamlet,immType,strFromDate,strToDate);
        else
        childList = childRepository.getPendingChild(appState.sessionUserId,tribalHamlet,immType,strFromDate,strToDate);
        return childList;
    }

    //    update labels based on data
    private void updateLabels() {
        if (filteredRowItems.size() == 0) {
            aqChildList.id(R.id.txtcountwl).text(getResources().getString(R.string.recordcount,
                    filteredRowItems.size()));

            aqChildList.id(R.id.txtnodata).text(getResources().getString(R.string.no_data));
            aqChildList.id(R.id.txtnodata).visible();
            return;
        }
        aqChildList.id(R.id.txtnodata).gone();
        aqChildList.id(R.id.txtcountwl).text(getResources().getString(R.string.recordcount,
                filteredRowItems.size()));
        aqChildList.id(R.id.txtcountwl).visible();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        /*if(view.getId() == villageSpinner.getId()) {
            appState.selectedVillageTitle = (String) parent.getItemAtPosition(position);
            if (spinnerSelectionChanged) {
                updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr);
            }
            spinnerSelectionChanged = true;
        }*/

        switch (parent.getId())
        {
            case R.id.spVillageFilter:
                appState.selectedVillageTitle = (String) parent.getItemAtPosition(position);
                if (spinnerSelectionChanged) {
                    if(position>0)
                    tribalHamlet = villageCodeForName.get(appState.selectedVillageTitle);
                    else
                        tribalHamlet = 0;
                    updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr);

                }
                spinnerSelectionChanged = true;
                break;

           /* case R.id.spndatefilter:
//                strFromDate = "";
//                strToDate = DateTimeUtil.getTodaysDate();
                if(position==2) {

//                    strToDate = DateTimeUtil.getTodaysDate();
                    aqChildList.id(R.id.txtfromdate).visible();
                    aqChildList.id(R.id.txttodate).visible();
                    aqChildList.id(R.id.txtfromdate).getEditText().setHint(getResources().getString(R.string.fromdate));
                    aqChildList.id(R.id.txttodate).text(DateTimeUtil.getTodaysDate());

                }else
                {
                    aqChildList.id(R.id.txtfromdate).gone();
                    aqChildList.id(R.id.txtfromdate).text("");
                    aqChildList.id(R.id.txttodate).gone();
                    aqChildList.id(R.id.txttodate).text(DateTimeUtil.getTodaysDate());
                    updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr);
                }
                break;*/
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    /**
     * populate Village to Spinner
     */
    private void populateSpinnerVillage() throws Exception {
        FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
        villageCodeForName = facilityRepository.getPlaceMap();

        ArrayAdapter<CharSequence> villageSpinnerAdapter = new ArrayAdapter<>(this, R.layout.actionbar_spinner_list_item);
//        villageSpinnerAdapter.add("All");06May2021 Arpitha
        villageSpinnerAdapter.add(getResources().getString(R.string.all));//06May2021 Arpitha
        for (Map.Entry<String, Integer> village : villageCodeForName.entrySet()) {
            villageSpinnerAdapter.add(village.getKey());
        }
        villageSpinnerAdapter.setDropDownViewResource(R.layout.actionbar_spinner_layout_view);
        villageSpinner.setAdapter(villageSpinnerAdapter);

        aqChildList.id(R.id.spVillageFilter).setSelection(tribalHamletpos);

    }


    @SuppressLint("SimpleDateFormat")
    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        private AQuery aqPMSel;
        private tblregisteredwomen woman;

        public DatePickerFragment() {
        }

        public DatePickerFragment(AQuery aQuery) {
            aqPMSel = aQuery;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            int year = 0, month = 0, day = 0;
            try {

                Calendar c = Calendar.getInstance();
                String defaultDate = DateTimeUtil.getTodaysDate(); //09Aug2019 - Cal set date of Provided date
                if (defaultDate.length() > 0) {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    Date sendDate = sdf.parse(defaultDate);
                    c.setTime(sendDate);
                    year = c.get(Calendar.YEAR);
                    month = c.get(Calendar.MONTH);
                    day = c.get(Calendar.DAY_OF_MONTH);
                } else {
                    c.setTime(new Date());
                    year = c.get(Calendar.YEAR);
                    month = c.get(Calendar.MONTH);
                    day = c.get(Calendar.DAY_OF_MONTH);
                }
            } catch (Exception e) {


                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            }

            // Create a new instance of DatePickerDialog and return it
            DatePickerDialog dpDialog;
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                // Do something for lollipop and above versions
                dpDialog = new DatePickerDialog(getActivity(), R.style.DialogTheme, this, year, month, day);
            } else {
                // do something for phones running an SDK before lollipop
                dpDialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, this, year, month, day);
                dpDialog.setTitle("Set a Date");
                dpDialog.getDatePicker().setCalendarViewShown(false);
            }
            return dpDialog;
        }

        @Override
        public void onDateSet(DatePicker view, int year, int month, int day) {
            if (view.isShown()) {
                try {
                    String SelectedDate = String.format("%02d", day) + "-" +
                            String.format("%02d", month + 1) + "-"
                            + year;
//                    Date lmpDate = null;
                    SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                    Date seldate = format.parse(SelectedDate);
                    Date mindate = null;


                    if(strMinDate!=null && strMinDate.trim().length()>0)
                        mindate = format.parse(strMinDate);

                    if(seldate.after(new Date())) {
                        Toast.makeText(getActivity(), getResources().getString(R.string.date_cannot_af_currentdate), Toast.LENGTH_LONG).show();
                        aqPMSel.id(R.id.etdate).text(DateTimeUtil.getTodaysDate());
                    }
                    else if(mindate!=null && seldate.before(mindate))
                    {
                        Toast.makeText(getActivity(), getResources().getString(R.string.immunizationtapplibeforemindate), Toast.LENGTH_LONG).show();
                        aqPMSel.id(R.id.etdate).text(DateTimeUtil.getTodaysDate());
                    }
                    else {
                        aqPMSel.id(R.id.etdate).text(SelectedDate);
                    }


//                    if(strFromDate!=null && strFromDate.trim().length()>0)
//                        fromdate = format.parse(strFromDate);
//
                  /*  if(fromDate)
                    {
                        if(seldate.after(new Date())) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.date_cannot_af_currentdate), Toast.LENGTH_LONG).show();
                            strFromDate = "";
                            aqPMSel.id(R.id.txtfromdate).text(getResources().getString(R.string.fromdate));
                        }else {
                            strFromDate = SelectedDate;
                            aqPMSel.id(R.id.txtfromdate).text(strFromDate);
                        }
                    }else {

                        if(seldate.after(new Date())) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.date_cannot_af_currentdate), Toast.LENGTH_LONG).show();
                            strToDate = "";
                            aqPMSel.id(R.id.txttodate).text(getResources().getString(R.string.todate));
                        }else if(fromdate!=null && seldate.before(fromdate)) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.date_cannot_af_currentdate), Toast.LENGTH_LONG).show();
                            strToDate = "";
                            aqPMSel.id(R.id.txttodate).text(getResources().getString(R.string.todate));
                        }
                        else {
                            strToDate = SelectedDate;
                            aqPMSel.id(R.id.txttodate).text(strToDate);
                        }
                    }*/

                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

                }
            }
        }
    }



    //    pop up which captures services data
     void addServiceData(final Context context,
                                final String serviceType,

                                final ChildImmunization tblChildInfo) throws Exception {

        AlertDialog.Builder alert = new AlertDialog.Builder(context, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        final View customView = LayoutInflater.from(context).inflate(R.layout.add_services_info, null);

        aqPMSel = new AQuery(customView);
        initializeScreen(aqPMSel.id(R.id.lladdservices).getView(), context);

        String strServType = serviceType;
        final String  chlId = tblChildInfo.getChlId();
        final  int immuNumber = 0;


        if (serviceId == 48 || serviceId == 49 ||
                serviceId == 50 || serviceId == 51 || serviceId == 52 || serviceId == 53 ||
                serviceId == 54 || serviceId == 16 || serviceId == 17 ||
                serviceId == 18 || serviceId == 19 || serviceId == 20 || serviceId == 21 ||
                serviceId == 22 || serviceId == 7 || serviceId == 8 || serviceId == 9 || serviceId == 10
                || serviceId == 4 || serviceId == 5 || serviceId == 6 || serviceId == 1 || serviceId == 2
                || serviceId == 11 || serviceId == 12 || serviceId == 13) {

            strServType = serviceType;

            //04Aug2019 - Bindu - set name from strings
            int identifier = context.getResources().getIdentifier(serviceType,
                    "string", "com.sc.stmansi");
            strServType = (context.getResources().getString(identifier));
        }
         int identifier = context.getResources().getIdentifier(serviceType.toLowerCase(),
                 "string", "com.sc.stmansi");
        // strServType = (context.getResources().getString(identifier));

        aqPMSel.id(R.id.txtheading).text(getResources().getString(R.string.addplannedservice) + ": " + context.getResources().getString(identifier));
        aqPMSel.id(R.id.etdate).text(DateTimeUtil.getTodaysDate());
        if(tblChildInfo!=null) {
            aqPMSel.id(R.id.txtNameval).text(tblChildInfo.getChlName());
            aqPMSel.id(R.id.txtlmpval).text(tblChildInfo.getChlDateofBirth());
            aqPMSel.id(R.id.tvWname).text(getResources().getString(R.string.txtNameL));
            strWomanId = tblChildInfo.getWomanId();
            serviceId  = tblChildInfo.getServiceId();
            aqPMSel.id(R.id.tvlmp).text(getResources().getString(R.string.dob));
        }



        WomanServiceRepository womanServiceRepository = new
                WomanServiceRepository(databaseHelper);
        int minduration  = womanServiceRepository.getDaysforImmunization("DaysDiffStart",strServType);
        int maxduration  = womanServiceRepository.getDaysforImmunization("DaysDiffEnd",strServType);

        strMaxDate = getNextDate(tblChildInfo.getChlDateofBirth(),maxduration,tblChildInfo.getChlDateofBirth(),true);
        strMinDate = getNextDate(tblChildInfo.getChlDateofBirth(),minduration,tblChildInfo.getChlDateofBirth(),false);

         aqPMSel.id(R.id.tvdurationvalmin).text(strMinDate);

         aqPMSel.id(R.id.tvdurationvalmax).text(strMaxDate);


         aqPMSel.id(R.id.trspnfacname).gone();
        aqPMSel.id(R.id.tblservicesdetails).gone();
        aqPMSel.id(R.id.txtservicedeatils).gone();

        //15Aug2019 - Bindu
//        serviceAddedUserType = service.getServiceUserType();
//        serviceupdateddate = service.getRecordUpdatedDate();


        //20Aug2019 Arpitha
        aqPMSel.id(R.id.imgcleardate).getImageView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aqPMSel.id(R.id.etdate).text(DateTimeUtil.getTodaysDate());
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.dateresettotodaysdate), Toast.LENGTH_LONG).show();//21Aug2019 Arpitha
            }
        });

        //25Jul2019 - Bindu

      /*  aqPMSel.id(R.id.spnfactype).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    String selected = adapterView.getItemAtPosition(i).toString();
                    if (!(selected.equalsIgnoreCase(getResources().getString(R.string.select)) || selected.equalsIgnoreCase(getResources().getString(R.string.bhome)) || selected.equalsIgnoreCase(getResources().getString(R.string.bsc)) ||
                            selected.equalsIgnoreCase(getResources().getString(R.string.bothers)))) {

                        if (tblChildInfo == null) {
                            aqPMSel.id(R.id.trspnfacname).visible();
                            aqPMSel.id(R.id.tvfacname).text(getResources().getString(R.string.facilitynameothers));
                        }
                        facilitynamelist = new ArrayList<>();

                        FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
                        mapFacilityname = facilityRepository.getFacilityNames(aqPMSel.id(R.id.spnfactype).getSelectedItem().toString(), appState.sessionUserId);
                        facilitynamelist.add(getResources().getString(R.string.selectfacilityname));
                        for (Map.Entry<String, String> village : mapFacilityname.entrySet())
                            facilitynamelist.add(village.getKey());
                        facilitynamelist.add(getResources().getString(R.string.other));
                        ArrayAdapter<String> FacNameAdapter = new ArrayAdapter<String>(PendingChildActivity.this,
                                R.layout.simple_spinner_dropdown_item, facilitynamelist);
                        FacNameAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        aqPMSel.id(R.id.spnfacname).adapter(FacNameAdapter);
                        aqPMSel.id(R.id.spnfacname).visible();
                    } else {
                        aqPMSel.id(R.id.trspnfacname).gone();
                        aqPMSel.id(R.id.tretfacname).visible();
                        aqPMSel.id(R.id.tvfacname).text(getResources().getString(R.string.facilityname));
                    }
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/

         aqPMSel.id(R.id.spnfactype).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
             @Override
             public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                 try {
                     String selected = adapterView.getItemAtPosition(i).toString();
                     if (!(selected.equalsIgnoreCase(getResources().getString(R.string.select)) || selected.equalsIgnoreCase(getResources().getString(R.string.bhome)) || selected.equalsIgnoreCase(getResources().getString(R.string.bsc)) ||
                             selected.equalsIgnoreCase(getResources().getString(R.string.bothers)))) {

                         //if (tblChildInfo == null) {
                             aqPMSel.id(R.id.trspnfacname).visible();
                             aqPMSel.id(R.id.tvfacname).text(getResources().getString(R.string.facilitynameothers));
                         //}
                         facilitynamelist = new ArrayList<>();

                         FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
                         mapFacilityname = facilityRepository.getFacilityNames(aqPMSel.id(R.id.spnfactype).getSelectedItem().toString(), appState.sessionUserId);
                         facilitynamelist.add(getResources().getString(R.string.selectfacilityname));
                         for (Map.Entry<String, String> village : mapFacilityname.entrySet())
                             facilitynamelist.add(village.getKey());
                         facilitynamelist.add(getResources().getString(R.string.strother));
                         ArrayAdapter<String> FacNameAdapter = new ArrayAdapter<String>(PendingChildActivity.this,
                                 R.layout.simple_spinner_dropdown_item, facilitynamelist);
                         FacNameAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                         aqPMSel.id(R.id.spnfacname).adapter(FacNameAdapter);
                     } else {
                         aqPMSel.id(R.id.trspnfacname).gone();
                         aqPMSel.id(R.id.tretfacname).visible();
                         aqPMSel.id(R.id.tvfacname).text(getResources().getString(R.string.facilityname));
                     }
                 } catch (Exception e) {
                     FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                 }
             }

             @Override
             public void onNothingSelected(AdapterView<?> adapterView) {

             }
         });

        // Facility name on select listener
        aqPMSel.id(R.id.spnfacname).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    String selected = adapterView.getItemAtPosition(i).toString();
                    if (selected.equalsIgnoreCase(getResources().getString(R.string.strother))) {
                        aqPMSel.id(R.id.tretfacname).visible();
                        aqPMSel.id(R.id.etfacname).text("");
                    } else {
                        aqPMSel.id(R.id.tretfacname).gone();
                        aqPMSel.id(R.id.etfacname).text("");
                    }
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

       /* if (service.getServiceDueDateMin() != null
                && service.getServiceDueDateMax() != null
                && service.getServiceDueDateMin().length() > 0
                && service.getServiceDueDateMax().length() > 0) {
            aqPMSel.id(R.id.tvdurationval).text(service.getServiceDueDateMin() + "   -  " + service.getServiceDueDateMax());
        }*/

       /* WomanServiceRepository womanServiceRepository = new WomanServiceRepository(databaseHelper);
        tblChildInfo = womanServiceRepository.getService(womanId, serviceId,service.getServiceNumber());
        if (tblChildInfo != null) {
            try {
                aqPMSel.id(R.id.etdate).text(tblChildInfo.getServiceActualDateofAction());
                aqPMSel.id(R.id.etcomments).text(tblChildInfo.getServiceComments());
                aqPMSel.id(R.id.etfacname).text(tblChildInfo.getServiceProvidedFacName());
                aqPMSel.id(R.id.etdate).enabled(false);
                aqPMSel.id(R.id.etfacname).enabled(false);
                aqPMSel.id(R.id.spnfactype).enabled(false);
                aqPMSel.id(R.id.etcomments).enabled(false);
                aqPMSel.id(R.id.btnsave).gone();

                aqPMSel.id(R.id.spnfactype).setSelection(Arrays.asList(getResources().getStringArray(R.array.facilitytype)).indexOf(arrVal.getServiceProvidedFacType())); //12Aug2019 - Bindu set spinner value

                //22Jul2019 - Bindu
                aqPMSel.id(R.id.txtheading).text(getResources().getString(R.string.view)
                        + " : " + arrVal.getServiceType() + " " + arrVal.getServiceNumber());
                aqPMSel.id(R.id.trspnfacname).gone(); //25Jul2019 - Bindu
                aqPMSel.id(R.id.tvfacname).text(getResources().getString(R.string.facilityname));

            } catch (Exception e) {

                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            }
        }*/


        aqPMSel.id(R.id.btnsave).getImageView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (validateServiceFields())
                        confirmAlert(serviceType, serviceId, chlId,immuNumber);
                } catch (Exception e) {


                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }

            }
        });

        aqPMSel.id(R.id.btncancel).getImageView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    confirmAlert(null, 0,null,0);
                } catch (Exception e) {


                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }

            }
        });

        aqPMSel.id(R.id.etdate).getEditText().setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEvent.ACTION_UP == event.getAction()) {
                    DialogFragment newFragment = new DatePickerFragment(aqPMSel);
                    newFragment.show(getSupportFragmentManager(), "datePicker");

                }
                return true;
            }
        });

        alert.setView(customView).setTitle("").setPositiveButton(
                "", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        alertdialog.setCancelable(false);
                        alertdialog.show();
                        try {
                            confirmAlert(serviceType, serviceId,null,0);
                        } catch (Exception e) {

                            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                        }
                    }
                });

        alertdialog = alert.create();
        alertdialog.show();

        alert.setCancelable(false);

        alertdialog.setTitle(context.getResources().getString(R.string.ok));

        alert.setCancelable(true);
        alertdialog.show();
        alertdialog.setCancelable(true);
    }

    //    update captured data to database
    private void addServiceData(String serviceType, int serviceId,String chlId, int serviceNumber) {
        try {
            final TblChildImmunization tblChildImmunization = new TblChildImmunization();
            final TransactionHeaderRepository transRepo = new TransactionHeaderRepository(databaseHelper);
            final int transId = transRepo.iCreateNewTrans(appState.sessionUserId,databaseHelper);

            tblChildImmunization.setImmActualDateofAction(aqPMSel.id(R.id.etdate).getText().toString());
            tblChildImmunization.setImmChildId(chlId);
            tblChildImmunization.setImmComments(aqPMSel.id(R.id.etcomments).getText().toString());

            String[] delTypeby = getResources().getStringArray(R.array.facilitytypesave);
            String posofDeltype = "";
            posofDeltype = delTypeby[aqPMSel.id(R.id.spnfactype).getSelectedItemPosition()];

            tblChildImmunization.setImmFacType(posofDeltype);
            tblChildImmunization.setImmMaxDate(strMaxDate);
            tblChildImmunization.setImmMinDate(strMinDate);

            if(serviceType.contains("VitaminAsyrup") || serviceType.contains("Measles") ) {
                String sNum  =  serviceType.substring(serviceType.length() - 1);
                if(sNum!=null && sNum.trim().length()>0)
                    serviceNumber = Integer.parseInt(sNum);
            }
            else
                serviceNumber = 1;
            tblChildImmunization.setImmNumber(serviceNumber);
            tblChildImmunization.setImmId(serviceId);
            tblChildImmunization.setImmType(serviceType);
            tblChildImmunization.setImmUserType(appState.userType);
            tblChildImmunization.setUserId(appState.sessionUserId);
            tblChildImmunization.setRecordCreatedDate(DateTimeUtil.getTodaysDate()+" "+ DateTimeUtil.getCurrentTime());
            tblChildImmunization.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());//21May2021 Arpitha
            tblChildImmunization.setTransId(transId);
            tblChildImmunization.setWomenId(strWomanId);



            String sel = "";
            if (aqPMSel.id(R.id.spnfacname).getSelectedItem() != null)
                sel = aqPMSel.id(R.id.spnfacname).getSelectedItem().toString();


            if (sel.equalsIgnoreCase(getResources().getString(R.string.select)))
                sel = getResources().getString(R.string.strselect);
            else if (sel.equalsIgnoreCase(getResources().getString(R.string.other)))
                sel = getResources().getString(R.string.strother);

            if (sel.trim().length() > 0 && !(sel.equalsIgnoreCase("select")
                    || sel.equalsIgnoreCase("other")))
                tblChildImmunization.setImmProvidedFacName(sel);
            else if (sel.equalsIgnoreCase("other")
                    && aqPMSel.id(R.id.etfacname).getText().toString().trim().length() <= 0)
                tblChildImmunization.setImmProvidedFacName("Other");
            else
                tblChildImmunization.setImmProvidedFacName(aqPMSel.id(R.id.etfacname).getText().toString());


            TransactionManager.callInTransaction(databaseHelper.getConnectionSource(), new Callable<Void>() {
                public Void call() throws Exception {//29Aug2019 Arpitha - transaction manager

                    ImmunizationRepository immunizationRepository = new ImmunizationRepository(databaseHelper);
                   int add =  immunizationRepository.addImmunization(databaseHelper,tblChildImmunization);
                    if(add>0) {
                        boolean added =     transRepo.iNewRecordTrans(appState.sessionUserId, transId, "tblchildimmunization", databaseHelper);

                        if(added) {
                            Toast.makeText(PendingChildActivity.this, "Service Details Updated Successfully!!", Toast.LENGTH_LONG).show();
                            alertdialog.cancel();
                        }else
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.serviceupdatefailed), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha

                    }else
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.serviceupdatefailed), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha



                    return null;
                }
            });
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.serviceupdatefailed), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    /**
     * This is a Recursive method that traverse through the group and subgroup
     * of view Sets text, assigns clickListners
     */
    private ArrayList<View> initializeScreen(View v, Context context) {

        if (!(v instanceof ViewGroup)) {
            ArrayList<View> viewArrayList = new ArrayList<>();
            viewArrayList.add(v);

            // TextView - Assign Labels
            if (v.getClass() == TextView.class || v.getClass() == RadioButton.class) {
            }

            if (v.getClass() == RadioButton.class) {
                aqPMSel.id(v.getId()).clicked(context, "commonClick");
            }

            return viewArrayList;
        }

        if (v instanceof Spinner) {
        }

        ArrayList<View> result = new ArrayList<View>();

        ViewGroup viewGroup = (ViewGroup) v;
        for (int i = 0; i < viewGroup.getChildCount(); i++) {

            View child = viewGroup.getChildAt(i);

            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);
            viewArrayList.addAll(initializeScreen(child, context));

            result.addAll(viewArrayList);
        }
        return result;
    }



    boolean validateServiceFields() {
//         if (aqChildImmunization.id(R.id.spnimmu).getSelectedItemPosition() > 1) {
//            Toast.makeText(getApplicationContext(), getResources().getString(R.string.selectservtype), Toast.LENGTH_LONG).show();
//            return false;
//        }
//       else
        if (aqPMSel.id(R.id.etdate).getText().toString().trim().length() <= 0) {
            Toast.makeText(getApplicationContext(), "Enter Service Completed Date", Toast.LENGTH_LONG).show();
            return false;
        } else if (aqPMSel.id(R.id.spnfactype).getSelectedItemPosition() == 0) {
            Toast.makeText(getApplicationContext(), "Select Service Provided Facility Type", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    //   get Next date
    public String getNextDate(String srcDate, int interval, String td, boolean isMaxDt)
            throws Exception {


        SimpleDateFormat xSimpleDate = new SimpleDateFormat("dd-MM-yyyy");
        String nxtDt = null;

        if (srcDate!=null && srcDate.length() != 0) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(xSimpleDate.parse(srcDate));
            cal.add(Calendar.DATE, interval);
            Date xDt = cal.getTime();

            Date eddDt = xSimpleDate.parse(td);
            int i = xDt.compareTo(eddDt);
            nxtDt = xSimpleDate.format(xDt);
            return nxtDt;
        }
        return nxtDt;
    }

    /**
     * Confirm Alert on Click save
     */
    private void confirmAlert(final String serviceType, final int serviceId,
                              final String chlId, final int immuNumber) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        String message;
        if (serviceType != null || serviceId != 0)
            message = getResources().getString(R.string.m099);
        else {
//            if (arrVal != null)
//                message = getResources().getString(R.string.areyousuretoexit);
//            else
            message = getResources().getString(R.string.m110);
        }

        alertDialogBuilder.setMessage(message).setCancelable(false)
                .setNegativeButton(getResources().getString(R.string.m121),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                try {
                                    if (serviceType != null || serviceId != 0) {
                                        addServiceData(serviceType,serviceId,chlId, immuNumber);
                                        updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr);
                                   } else
                                        alertdialog.cancel();

                                } catch (Exception e) {



                                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                                }
                            }
                        })
                .setPositiveButton((getResources().getString(R.string.m122)),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }



    public void addServiceDataOLD(final Context context,
                                  List<TblChildImmunization> tblChildImmunizationsList) throws SQLException {
        final View customView = LayoutInflater.from(context).inflate(R.layout.add_services_info, null);

        aqPMSel = new AQuery(customView);

        aqPMSel.id(R.id.etdate).text(DateTimeUtil.getTodaysDate());

        ImmunizationRepository immunizationRepository = new ImmunizationRepository(databaseHelper);
        tblChildImmunizationsList =
                immunizationRepository.getImmunizationData(immType, tblChildInfo.getChlId());


        int identifier = getResources().getIdentifier(tblChildImmunizationsList.get(0).getImmType().toLowerCase(), "string", "com.sc.stmansi");

        aqPMSel.id(R.id.txtheading).text(getResources().getString(R.string.addplannedservice) + ": " + context.getResources().getString(identifier));
        aqPMSel.id(R.id.etdate).text(DateTimeUtil.getTodaysDate());
        if(tblChildInfo!=null) {
            aqPMSel.id(R.id.txtNameval).text(tblChildInfo.getChlName());
            aqPMSel.id(R.id.txtlmpval).text(tblChildInfo.getDateTimeOfBirth());
            aqPMSel.id(R.id.tvWname).text(getResources().getString(R.string.tvchildname));
            strWomanId = tblChildInfo.getWomanId();
            serviceId  = serviceId;
        }



        aqPMSel.id(R.id.trspnfacname).gone();
        aqPMSel.id(R.id.tblservicesdetails).gone();
        aqPMSel.id(R.id.txtservicedeatils).gone();


        final List<TblChildImmunization> finalTblChildImmunizationsList = tblChildImmunizationsList;
        aqPMSel.id(R.id.spnfactype).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    String selected = adapterView.getItemAtPosition(i).toString();
                    if (!(selected.equalsIgnoreCase(getResources().getString(R.string.select)) || selected.equalsIgnoreCase(getResources().getString(R.string.bhome)) || selected.equalsIgnoreCase(getResources().getString(R.string.bsc)) ||
                            selected.equalsIgnoreCase(getResources().getString(R.string.bothers)))) {

                        if (finalTblChildImmunizationsList == null) {
                            aqPMSel.id(R.id.trspnfacname).visible();
                            aqPMSel.id(R.id.tvfacname).text(getResources().getString(R.string.facilitynameothers));
                        }
                        facilitynamelist = new ArrayList<>();

                        FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
                        mapFacilityname = facilityRepository.getFacilityNames(aqPMSel.id(R.id.spnfactype).getSelectedItem().toString(), appState.sessionUserId);
                        //facilitynamelist.add(getResources().getString(R.string.selectfacilityname));
                        facilitynamelist.add(getResources().getString(R.string.strselect)); //15Nov2019 - Bindu selectfacilityname  to strselect
                        for (Map.Entry<String, String> village : mapFacilityname.entrySet())
                            facilitynamelist.add(village.getKey());
                        //facilitynamelist.add(getResources().getString(R.string.other));
                        facilitynamelist.add(getResources().getString(R.string.strother)); //15Nov2019 - Bindu other  to strother
                        ArrayAdapter<String> FacNameAdapter = new ArrayAdapter<String>(PendingChildActivity.this,
                                R.layout.simple_spinner_dropdown_item, facilitynamelist);
                        FacNameAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        aqPMSel.id(R.id.spnfacname).adapter(FacNameAdapter);
                    } else {
                        aqPMSel.id(R.id.trspnfacname).gone();
                        aqPMSel.id(R.id.tretfacname).visible();
                        aqPMSel.id(R.id.tvfacname).text(getResources().getString(R.string.facilityname));
                    }
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        // Facility name on select listener
        aqPMSel.id(R.id.spnfacname).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    String selected = adapterView.getItemAtPosition(i).toString();
                    if (selected.equalsIgnoreCase(getResources().getString(R.string.strother))) {  //15Nov2019 - Bindu other  to strother
                        aqPMSel.id(R.id.tretfacname).visible();
                        aqPMSel.id(R.id.etfacname).text("");
                    } else {
                        aqPMSel.id(R.id.tretfacname).gone();
                        aqPMSel.id(R.id.etfacname).text("");
                    }
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        if (tblChildImmunizationsList != null && tblChildImmunizationsList.size() > 0) {
            try {
                TblChildImmunization tblChildImmunization = tblChildImmunizationsList.get(0);
                aqPMSel.id(R.id.etdate).text(tblChildImmunization.getImmActualDateofAction());
                aqPMSel.id(R.id.etcomments).text(tblChildImmunization.getImmComments());
                aqPMSel.id(R.id.etfacname).text(tblChildImmunization.getImmProvidedFacName());
                aqPMSel.id(R.id.spnfactype).setSelection(Arrays.asList(getResources().getStringArray(R.array.facilitytype))
                        .indexOf(tblChildImmunization.getImmFacType())); //12Aug2019 - Bindu set spinner value
                aqPMSel.id(R.id.etdate).enabled(false);
                aqPMSel.id(R.id.etfacname).enabled(false);
                aqPMSel.id(R.id.spnfactype).enabled(false);
                aqPMSel.id(R.id.etcomments).enabled(false);
                aqPMSel.id(R.id.btnsave).enabled(false);
                //22Jul2019 - Bindu
                aqPMSel.id(R.id.btnsave).gone();
                aqPMSel.id(R.id.trduration).gone();
                aqPMSel.id(R.id.trspnfacname).gone(); //25Jul2019 - Bindu
                aqPMSel.id(R.id.tvfacname).text(getResources().getString(R.string.facilityname));
                //15Aug2019 -Bindu
                aqPMSel.id(R.id.txtheading).text(getResources().getString(R.string.view) + " : " + tblChildImmunization.getImmType());
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            }
        }

       /* aqPMSel.id(R.id.btnsave).getImageView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    confirmAlert(selectedService.getServiceType(), selectedService.getServiceId());
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }
            }
        });*/

        aqPMSel.id(R.id.btncancel).getImageView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    confirmAlert(null, 0,null,0);
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }
            }
        });

        alert = new AlertDialog.Builder(context, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        alert.setView(customView).setTitle("").setPositiveButton(
                "", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        alertdialog.setCancelable(false);
                        alertdialog.show();
                        try {
//                            confirmAlert(selectedService.getServiceType(), selectedService.getServiceId());
                        } catch (Exception e) {
                            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                        }
                    }
                });

        alertdialog = alert.create();
//        alertdialog.setTitle(context.getResources().getString(R.string.m123));
        alertdialog.show();
    }

    @Override
    public void displayLMPConfirmation(int selectedItemPosition) {

    }
}


