package com.sc.stmansi.immunization;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.DialogFragment;
import androidx.legacy.app.ActionBarDrawerToggle;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.misc.TransactionManager;
import com.sc.stmansi.About.AboutActivity;
import com.sc.stmansi.childgrowthmonitor.CGMList;
import com.sc.stmansi.childgrowthmonitor.CGMNewVisit;
import com.sc.stmansi.childhomevisit.ChildHomeVisit;
import com.sc.stmansi.childhomevisit.ChildHomeVisitHistory;
import com.sc.stmansi.childregistration.AddSiblingsActivity;
import com.sc.stmansi.childregistration.EditChildActivity;
import com.sc.stmansi.childregistration.EditRegChildInfoActivity;
import com.sc.stmansi.childregistration.ViewParentDetails;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.common.NavDrawerItem;
import com.sc.stmansi.common.NavDrawerListAdapter;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.deactivate.ChildDeactivateActivity;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.recyclerview.ChildImmunizationAdapter;
import com.sc.stmansi.recyclerview.ClickListener;
import com.sc.stmansi.repositories.FacilityRepository;
import com.sc.stmansi.repositories.ImmunizationRepository;
import com.sc.stmansi.repositories.TransactionHeaderRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.repositories.WomanServiceRepository;
import com.sc.stmansi.services.CustomSpinnerAdapter;
import com.sc.stmansi.tables.ChildImmunization;
import com.sc.stmansi.tables.TblChildImmunization;
import com.sc.stmansi.tables.TblChildInfo;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.viewprofile.ViewProfileActivity;
import com.sc.stmansi.womanlist.RegisteredWomenActionTabs;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import com.sc.stmansi.R;

public class ImmunizationListActivity extends AppCompatActivity implements ClickListener {

    DatabaseHelper databaseHelper;
    RecyclerView recyclerView;
    TblChildInfo tblChildInfo;
    AQuery aqImm;
    AppState appState;
//    TblChildInfo tblChildInfoFromDb;
    private AQuery aqPMSel;
    private AlertDialog alertdialog = null;
    private AlertDialog.Builder alert;
    TblChildImmunization tblChildImmunization;
    List<ChildImmunization> list;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private List<tblregisteredwomen> wVisitDetails;

    private AQuery  aqServ;
    private String serviceAddedUserType = "", serviceupdateddate = "";
    static String strMinDate = "";
    String strMaxDate = "";
    String strWomanId;
    private ArrayList<String> facilitynamelist;
    private Map<String, String> mapFacilityname;
    int serviceId = 0;
    TblInstusers user;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servicessummary);

        try {

            Bundle bundle = getIntent().getBundleExtra("globalState");
            appState = bundle.getParcelable("appState");
            databaseHelper = getHelper();
//            tblChildInfoFromDb = (TblChildInfo) getIntent().getSerializableExtra("tblChildInfo");


            tblChildInfo = (TblChildInfo) getIntent().getSerializableExtra("tblChildInfo");

            aqImm = new AQuery(this);

            aqImm.id(R.id.txtpending30).text(getResources().getString(R.string.immunizationlist));

            UserRepository userRepository = new UserRepository(databaseHelper);
             user =  userRepository.getOneAuditedUser(appState.ashaId);

            initiateDrawer();

            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(true);


            if (tblChildInfo != null) {
                getSupportActionBar().setTitle(tblChildInfo.getChlBabyname() + "("+tblChildInfo.getChlGender()+")");
            }




            recyclerView = findViewById(R.id.woman_services_recycler_view);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));

            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                    DividerItemDecoration.VERTICAL);
            DividerItemDecoration dividerItemDecoration1 = new DividerItemDecoration(recyclerView.getContext(),
                    DividerItemDecoration.HORIZONTAL);
            recyclerView.addItemDecoration(dividerItemDecoration);
            recyclerView.addItemDecoration(dividerItemDecoration1);

            getData();

            int[] spnFilterImages = new int[]{R.drawable.ic_all, R.drawable.warning, R.drawable.notdone, R.drawable.upcoming, R.drawable.tick};
            CustomSpinnerAdapter mCustomAdapter = new CustomSpinnerAdapter(this, getResources().getStringArray(R.array.servicefilter), spnFilterImages);
            aqImm.id(R.id.spnfilter).adapter(mCustomAdapter);

            aqImm.id(R.id.spnfilter).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        getData();
                    } catch (Exception e) {
                       FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        } catch (Exception e) {
           FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }

    }

    private void getData() throws Exception {

        ImmunizationRepository immunizationRepository = new ImmunizationRepository(databaseHelper);

        if (immunizationRepository.getChildImmunizationStatus(appState.sessionUserId,
                tblChildInfo.getChildID(), aqImm.id(R.id.spnfilter).
                        getSelectedItemPosition(), tblChildInfo.
                        getChlDateTimeOfBirth()) != null && immunizationRepository.getChildImmunizationStatus(appState.sessionUserId,
                tblChildInfo.getChildID(), aqImm.id(R.id.spnfilter).
                        getSelectedItemPosition(), tblChildInfo.
                        getChlDateTimeOfBirth()).size() > 0) {

            list = immunizationRepository.getChildImmunizationStatus
                    (appState.sessionUserId,
                            tblChildInfo.getChildID(), aqImm.id(R.id.spnfilter).
                                    getSelectedItemPosition(), tblChildInfo.
                                    getChlDateTimeOfBirth());
            ChildImmunizationAdapter childImmunizationAdapter = new
                    ChildImmunizationAdapter(list
                    , this,
                    tblChildInfo, tblChildInfo.getChlDateTimeOfBirth());
            recyclerView.setAdapter(childImmunizationAdapter);
            recyclerView.setVisibility(View.VISIBLE);
            aqImm.id(R.id.txtnodatapending).gone();
        } else {
            recyclerView.setVisibility(View.GONE);
            aqImm.id(R.id.txtnodatapending).visible();
        }
    }

    //    initialize db object
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void onClick(View v) {


        try {

//            26nov2019 Arpitha

            String strImmType = list.get(recyclerView.getChildAdapterPosition(v)).getServiceType();
            ImmunizationRepository immunizationRepository = new ImmunizationRepository(databaseHelper);
            List<TblChildImmunization> tblChildImmunizationsList =
                    immunizationRepository.getImmunizationData(strImmType, tblChildInfo.getChildID());


            if(((tblChildInfo.getChlDeactDate()!=null &&
                    tblChildInfo.getChlDeactDate().trim().length()>0)
                    ||user.getIsDeactivated()==1) && tblChildImmunizationsList.size()<=0)

                Toast.makeText(getApplicationContext(),getResources().getString(R.string.userdeactivated),Toast.LENGTH_LONG).show();
                else {


                WomanServiceRepository womanServiceRepository = new WomanServiceRepository(databaseHelper);
                int minduration = womanServiceRepository.getDaysforImmunization("DaysDiffStart", strImmType);


                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                Date seldate = format.parse(DateTimeUtil.getTodaysDate());
                Date mindate = null;

                strMinDate = getNextDate(tblChildInfo.getChlDateTimeOfBirth(),
                        minduration, tblChildInfo.getChlDateTimeOfBirth(), false);

                if (strMinDate != null && strMinDate.trim().length() > 0)
                    mindate = format.parse(strMinDate);


                if (mindate != null && seldate.before(mindate)) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.immunizationtapplibeforemindate), Toast.LENGTH_LONG).show();
                } else {
                    if (tblChildImmunizationsList != null && tblChildImmunizationsList.size() > 0)
                        addServiceDataOLD(ImmunizationListActivity.this,
                                tblChildImmunizationsList);
                    else
                        addServiceData(ImmunizationListActivity.this,
                                strImmType, list.get(recyclerView.getChildAdapterPosition(v))
                                        .getServiceId(), tblChildInfo);
                }
            }

        } catch (SQLException e) {
           FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        } catch (Exception e) {
           FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }


    }

    @Override
    public boolean onLongClick(View v) {
        return false;
    }

    @Override
    public void onBackPressed() {

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportActionBar().setTitle((tblChildInfo.getChlBabyname() +
                " (" +
                tblChildInfo.getChlGender() + ")"));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer1); //27Sep2019 - Bindu - set drawer and enable true home btn
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);

        aqImm.id(R.id.tvWomanName).text((tblChildInfo.getChlBabyname() +
                " (" +
                tblChildInfo.getChlGender() + ")"));
//            aqServ.id(R.id.tvInfo1).text(getResources().getString(R.string.tvlmp) + " - " + woman.getRegLMP());
        if (tblChildInfo.getChlDateTimeOfBirth()!=null && tblChildInfo.getChlDateTimeOfBirth().length() > 0)
            aqImm.id(R.id.tvInfo1).text((getResources().getString(R.string.age)
                    + " " + DateTimeUtil.calculateAge(tblChildInfo.getChlDateTimeOfBirth())));

        aqImm.id(R.id.ivWomanImg).gone();
        getMenuInflater().inflate(R.menu.servicesmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            case R.id.logout: {
                Intent goToScreen = new Intent(ImmunizationListActivity.this, LoginActivity.class);
                startActivity(goToScreen);
                return true;
            }
            case R.id.home: {
                Intent home = new Intent(ImmunizationListActivity.this, MainMenuActivity.class);
                home.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                startActivity(home);
                return true;
            }
            case R.id.wlist: {
                Intent goToScreen = new Intent(ImmunizationListActivity.this, RegisteredWomenActionTabs.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                startActivity(goToScreen);
                return true;
            }
            case R.id.about: {
                Intent goToScreen = new Intent(ImmunizationListActivity.this, AboutActivity.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));

                startActivity(goToScreen);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void addServiceDataOLD(final Context context,
                                  final List<TblChildImmunization> tblChildImmunizationsList) throws SQLException {
        final View customView = LayoutInflater.from(context).inflate(R.layout.add_services_info, null);

        aqPMSel = new AQuery(customView);

        aqPMSel.id(R.id.etdate).text(DateTimeUtil.getTodaysDate());

        aqPMSel.id(R.id.txtheading).text(getResources().getString(R.string.addplannedservice) + ": " + tblChildImmunizationsList.get(0).getImmType());
        aqPMSel.id(R.id.etdate).text(DateTimeUtil.getTodaysDate());
        if(tblChildInfo!=null) {
            aqPMSel.id(R.id.txtNameval).text(tblChildInfo.getChlBabyname());
            aqPMSel.id(R.id.txtlmpval).text(tblChildInfo.getChlDateTimeOfBirth());
            aqPMSel.id(R.id.tvWname).text(getResources().getString(R.string.tvchildname));
            strWomanId = tblChildInfo.getWomanId();
            serviceId  = serviceId;
        }

        aqPMSel.id(R.id.trspnfacname).gone();
        aqPMSel.id(R.id.tblservicesdetails).gone();
        aqPMSel.id(R.id.txtservicedeatils).gone();


        aqPMSel.id(R.id.spnfactype).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    String selected = adapterView.getItemAtPosition(i).toString();
                    if (!(selected.equalsIgnoreCase(getResources().getString(R.string.select)) || selected.equalsIgnoreCase(getResources().getString(R.string.bhome)) || selected.equalsIgnoreCase(getResources().getString(R.string.bsc)) ||
                            selected.equalsIgnoreCase(getResources().getString(R.string.bothers)))) {

                        if (tblChildImmunizationsList == null) {
                            aqPMSel.id(R.id.trspnfacname).visible();
                            aqPMSel.id(R.id.tvfacname).text(getResources().getString(R.string.facilitynameothers));
                        }
                        facilitynamelist = new ArrayList<>();

                        FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
                        mapFacilityname = facilityRepository.getFacilityNames(aqPMSel.id(R.id.spnfactype).getSelectedItem().toString(), user.getUserId());
                        //facilitynamelist.add(getResources().getString(R.string.selectfacilityname));
                        facilitynamelist.add(getResources().getString(R.string.strselect)); //15Nov2019 - Bindu selectfacilityname  to strselect
                        for (Map.Entry<String, String> village : mapFacilityname.entrySet())
                            facilitynamelist.add(village.getKey());
                        //facilitynamelist.add(getResources().getString(R.string.other));
                        facilitynamelist.add(getResources().getString(R.string.strother)); //15Nov2019 - Bindu other  to strother
                        ArrayAdapter<String> FacNameAdapter = new ArrayAdapter<String>(ImmunizationListActivity.this,
                                R.layout.simple_spinner_dropdown_item, facilitynamelist);
                        FacNameAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        aqPMSel.id(R.id.spnfacname).adapter(FacNameAdapter);
                    } else {
                        aqPMSel.id(R.id.trspnfacname).gone();
                        aqPMSel.id(R.id.tretfacname).visible();
                        aqPMSel.id(R.id.tvfacname).text(getResources().getString(R.string.facilityname));
                    }
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        // Facility name on select listener
        aqPMSel.id(R.id.spnfacname).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    String selected = adapterView.getItemAtPosition(i).toString();
                    if (selected.equalsIgnoreCase(getResources().getString(R.string.strother))) {  //15Nov2019 - Bindu other  to strother
                        aqPMSel.id(R.id.tretfacname).visible();
                        aqPMSel.id(R.id.etfacname).text("");
                    } else {
                        aqPMSel.id(R.id.tretfacname).gone();
                        aqPMSel.id(R.id.etfacname).text("");
                    }
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        if (tblChildImmunizationsList != null && tblChildImmunizationsList.size() > 0) {
            try {
                TblChildImmunization tblChildImmunization = tblChildImmunizationsList.get(0);
                aqPMSel.id(R.id.etdate).text(tblChildImmunization.getImmActualDateofAction());
                aqPMSel.id(R.id.etcomments).text(tblChildImmunization.getImmComments());
                aqPMSel.id(R.id.etfacname).text(tblChildImmunization.getImmProvidedFacName());
                aqPMSel.id(R.id.spnfactype).setSelection(Arrays.asList(getResources().getStringArray(R.array.facilitytype))
                        .indexOf(tblChildImmunization.getImmFacType())); //12Aug2019 - Bindu set spinner value
                aqPMSel.id(R.id.etdate).enabled(false);
                aqPMSel.id(R.id.etfacname).enabled(false);
                aqPMSel.id(R.id.spnfactype).enabled(false);
                aqPMSel.id(R.id.etcomments).enabled(false);
                aqPMSel.id(R.id.btnsave).enabled(false);
                //22Jul2019 - Bindu
                aqPMSel.id(R.id.btnsave).gone();
                aqPMSel.id(R.id.trduration).gone();
                aqPMSel.id(R.id.trspnfacname).gone(); //25Jul2019 - Bindu
                aqPMSel.id(R.id.tvfacname).text(getResources().getString(R.string.facilityname));
                //15Aug2019 -Bindu
                aqPMSel.id(R.id.txtheading).text(getResources().getString(R.string.view) + " : " + tblChildImmunization.getImmType());
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            }
        }

       /* aqPMSel.id(R.id.btnsave).getImageView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    confirmAlert(selectedService.getServiceType(), selectedService.getServiceId());
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }
            }
        });*/

        aqPMSel.id(R.id.btncancel).getImageView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    confirmAlert(null, 0);
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }
            }
        });

        alert = new AlertDialog.Builder(context, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        alert.setView(customView).setTitle("").setPositiveButton(
                "", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        alertdialog.setCancelable(false);
                        alertdialog.show();
                        try {
//                            confirmAlert(selectedService.getServiceType(), selectedService.getServiceId());
                        } catch (Exception e) {
                            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                        }
                    }
                });

        alertdialog = alert.create();
//        alertdialog.setTitle(context.getResources().getString(R.string.m123));
        alertdialog.show();
    }

    /**
     * Confirm Alert on Click save
     */
    private void confirmAlert(final String serviceType, final int serviceId) throws Exception {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        String message = getResources().getString(R.string.areyousuretoexit); //22Jul2019 -Bindu
        alertDialogBuilder.setMessage(message).setCancelable(false)
                .setNegativeButton(getResources().getString(R.string.m121),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                try {

                                    alertdialog.cancel();

                                } catch (Exception e) {


                                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                                }
                            }
                        })
                .setPositiveButton((getResources().getString(R.string.m122)),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    private void initiateDrawer() throws Exception {
        mDrawerLayout = findViewById(R.id.ett_drawer_layout);
        mDrawerList = findViewById(R.id.ett_left_drawer);

        ArrayList<NavDrawerItem> navDrawerItems = new ArrayList<>();
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.editchildinfo), R.drawable.ic_edit_60));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.addsibling), R.drawable.general_examination_baby));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.viewparentdetails), R.drawable.ic_mother));
//        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deactivate),R.drawable.deactivate));

        //Mani 16 april 2021 added CGM
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.gcmadd), R.drawable.registration)); //15MAy2021 Bindu
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.gcmregisterlist), R.drawable.child_growth));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.chlhv), R.drawable.ic_homevisit));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.chlhvhistory), R.drawable.prenatalhomevisit));// 8Aug2021 Mani
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deactivate),R.drawable.deactivate));
        /*navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.services), R.drawable.anm_pending_activities));//01Oct2019 Arpitha

        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.addunplannedservices), R.drawable.anm_pending_activities));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.unplannedserviceslist), R.drawable.anm_pending_activities));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.homevisitadd),
                R.drawable.prenatalhomevisit));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.womanhomevisithistory),
                R.drawable.prenatalhomevisit));*/


        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        mDrawerList.setAdapter(new NavDrawerListAdapter(getApplicationContext(), navDrawerItems));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer,
                R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu(); // creates call to
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                getSupportActionBar().setDisplayShowTitleEnabled(false);
                getSupportActionBar().setDisplayShowCustomEnabled(false);

                int noOfWeeks;
                try {
                    if (tblChildInfo != null) {
                        getSupportActionBar().setTitle((tblChildInfo.getChlBabyname() + "\t"));
                    }
                } catch (Exception e) {


                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                try {
                    invalidateOptionsMenu(); // creates call to
                    getSupportActionBar().setDisplayShowHomeEnabled(false);
                    getSupportActionBar().setDisplayShowTitleEnabled(false);
                    getSupportActionBar().setDisplayShowCustomEnabled(true);
                    getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);

                    aqImm.id(R.id.ivWomanImg).getImageView().setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            mDrawerLayout.closeDrawer(mDrawerList);
                        }
                    });

                    aqImm.id(R.id.tvWomanName).text((tblChildInfo.getChlBabyname() + "("+tblChildInfo.getChlGender()+")"));
                    if (tblChildInfo.getChlDateTimeOfBirth().trim().length() > 0) {
                        aqImm.id(R.id.tvInfo1).text(
                                (getResources().getString(R.string.age) + " " +
                                        DateTimeUtil.calculateAge(tblChildInfo.getChlDateTimeOfBirth())));
                    }
                } catch (Exception e) {


                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    /**
     * This method is to sync draw,erToggle and to replace up caret icon to menu
     * icon in Action Bar
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    /**
     * The click listener for ListView in the navigation drawer
     */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            mDrawerLayout.closeDrawer(mDrawerList);
            try {

                switch (position) {

                    case 0:
                        if(tblChildInfo.getChlReg()==1) {
                            Intent nextScreen = new Intent(ImmunizationListActivity.this, EditRegChildInfoActivity.class);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                            nextScreen.putExtra("tblChildInfo", tblChildInfo);
                            startActivity(nextScreen);
                        }else
                        {
                            Intent nextScreen = new Intent(ImmunizationListActivity.this, EditChildActivity.class);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                            nextScreen.putExtra("tblChildInfo", tblChildInfo);
                            startActivity(nextScreen);
                        }
                        break;

                    case 1: {
                        if(user.getIsDeactivated()==1
                        )
                            Toast.makeText(getApplicationContext(),getResources().getString(R.string.userdeactivated),Toast.LENGTH_LONG).show();
                        else {
                            Intent addSibling = new Intent(ImmunizationListActivity.this, AddSiblingsActivity.class);
                            addSibling.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            addSibling.putExtra("appState", getIntent().getBundleExtra("appState"));
//                            if (tblChildInfo.getChlReg() == 1)
                            String strParentId = tblChildInfo.getChlParentId();//17Jun2021 Arpitha
                            if(strParentId != null && strParentId.trim().length() > 0)//17Jun2021 Arpitha
                                addSibling.putExtra("chlParentId", tblChildInfo.getChlParentId());
                            else
                                addSibling.putExtra("WomanId", tblChildInfo.getWomanId());
                            startActivity(addSibling);
                        }
                        break;
                    }
                    case 2: {
                        if (tblChildInfo.getChlParentId() != null && tblChildInfo.getChlParentId().trim().length() > 0) {
                        Intent viewParent = new Intent(ImmunizationListActivity.this, ViewParentDetails.class);
                        viewParent.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        viewParent.putExtra("appState", getIntent().getBundleExtra("appState"));
                        viewParent.putExtra("tblChildInfo", tblChildInfo);
                        startActivity(viewParent);
                        }else {
                            WomanRepository womanRepository = new WomanRepository(databaseHelper);
                            tblregisteredwomen woman = womanRepository.getRegistartionDetails(tblChildInfo.getWomanId(),tblChildInfo.getUserId());
                            appState.selectedWomanId = tblChildInfo.getWomanId();
                            Intent nextScreen = new Intent(ImmunizationListActivity.this, ViewProfileActivity.class);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                            nextScreen.putExtra("woman", woman);
                            startActivity(nextScreen);
//                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.ntapplicable), Toast.LENGTH_LONG).show();
                        }
                        break;
                    }
                    //mani 17 June 2021 Checking above 6 years
                    case 3:
                    {
                        if (((tblChildInfo.getChlDeactDate() != null &&
                                tblChildInfo.getChlDeactDate().trim().length() > 0)
                                || user.getIsDeactivated() == 1)) {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.userdeactivated), Toast.LENGTH_LONG).show();
                        } else {
                            //Mani 17 June 2021
                            if ((DateTimeUtil.calculateAgeInMonths(tblChildInfo.getChlDateTimeOfBirth()) >= 60)||tblChildInfo.getChlDeliveryResult()>1)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.aboveagewarning), Toast.LENGTH_LONG).show();
                            else {
                                Intent goToScreen = new Intent(ImmunizationListActivity.this, CGMNewVisit.class);
                                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                goToScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                                goToScreen.putExtra("tblChildInfo", tblChildInfo);
                                startActivity(goToScreen);
                            }

                        }
                        break;
                    }

                    //mani 16 april 2021 added CGM
                    case 4: {
                        Intent nextScreen = new Intent(ImmunizationListActivity.this, CGMList.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                        nextScreen.putExtra("tblChildInfo", tblChildInfo);
                        startActivity(nextScreen);
                        break;
                    }
                    case 7: {
                        Intent nextScreen = new Intent(ImmunizationListActivity.this, ChildDeactivateActivity.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                        nextScreen.putExtra("tblChildInfo", tblChildInfo);
                        startActivity(nextScreen);

                        break;
                    }
                    case 6: {
                        Intent nextScreen = new Intent(ImmunizationListActivity.this, ChildHomeVisitHistory.class);
                        nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                        nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                        nextScreen.putExtra("tblChildInfo", tblChildInfo);
                        startActivity(nextScreen);

                        break;
                    }


                    case 5: {
                        if (((tblChildInfo.getChlDeactDate() != null && tblChildInfo.getChlDeactDate().trim().length() > 0) || user.getIsDeactivated() == 1)) {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.userdeactivated), Toast.LENGTH_LONG).show();
                        }else {
                            Intent nextScreen = new Intent(ImmunizationListActivity.this, ChildHomeVisit.class);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            nextScreen.putExtra("appState", getIntent().getBundleExtra("appState"));
                            nextScreen.putExtra("tblChildInfo", tblChildInfo);
                            startActivity(nextScreen);
                        }

                        break;
                    }
                }

            } catch (Exception e) {


                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            }
        }
    }


    //    pop up which captures services data
    void addServiceData(final Context context,
                        final String serviceType,
int serviceId,
                        final TblChildInfo tblChildInfo) throws Exception {

        AlertDialog.Builder alert = new AlertDialog.Builder(context, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        final View customView = LayoutInflater.from(context).inflate(R.layout.add_services_info, null);

        aqPMSel = new AQuery(customView);
        initializeScreen(aqPMSel.id(R.id.lladdservices).getView(), context);

        String strServType = serviceType;
        final String  chlId = tblChildInfo.getChildID();
        final  int immuNumber = 0;


        if (serviceId == 48 || serviceId == 49 ||
                serviceId == 50 || serviceId == 51 || serviceId == 52 || serviceId == 53 ||
                serviceId == 54 || serviceId == 16 || serviceId == 17 ||
                serviceId == 18 || serviceId == 19 || serviceId == 20 || serviceId == 21 ||
                serviceId == 22 || serviceId == 7 || serviceId == 8 || serviceId == 9 || serviceId == 10
                || serviceId == 4 || serviceId == 5 || serviceId == 6 || serviceId == 1 || serviceId == 2
                || serviceId == 11 || serviceId == 12 || serviceId == 13) {

            strServType = serviceType;

            //04Aug2019 - Bindu - set name from strings
//            int identifier = context.getResources().getIdentifier(serviceType,
//                    "string", "com.sc.stmansi");
//            strServType = (context.getResources().getString(identifier)) + " " + service.getServiceNumber();
        }

        aqPMSel.id(R.id.txtheading).text(getResources().getString(R.string.addplannedservice) + ": " + strServType);
        aqPMSel.id(R.id.etdate).text(DateTimeUtil.getTodaysDate());
        if(tblChildInfo!=null) {
            aqPMSel.id(R.id.txtNameval).text(tblChildInfo.getChlBabyname());
            aqPMSel.id(R.id.txtlmpval).text(tblChildInfo.getChlDateTimeOfBirth());
            aqPMSel.id(R.id.tvWname).text(getResources().getString(R.string.tvchildname));
            strWomanId = tblChildInfo.getWomanId();
            aqPMSel.id(R.id.tvlmp).text(getResources().getString(R.string.dob));
            serviceId  = serviceId;
        }



        WomanServiceRepository womanServiceRepository = new
                WomanServiceRepository(databaseHelper);
        int minduration  = womanServiceRepository.getDaysforImmunization("DaysDiffStart",strServType);
        int maxduration  = womanServiceRepository.getDaysforImmunization("DaysDiffEnd",strServType);

        strMaxDate = getNextDate(tblChildInfo.getChlDateTimeOfBirth(),maxduration,tblChildInfo.getChlDateTimeOfBirth(),true);
        strMinDate = getNextDate(tblChildInfo.getChlDateTimeOfBirth(),minduration,tblChildInfo.getChlDateTimeOfBirth(),false);

        aqPMSel.id(R.id.tvdurationvalmin).text(strMinDate);

        aqPMSel.id(R.id.tvdurationvalmax).text(strMaxDate);


        aqPMSel.id(R.id.trspnfacname).gone();
        aqPMSel.id(R.id.tblservicesdetails).gone();
        aqPMSel.id(R.id.txtservicedeatils).gone();

        //15Aug2019 - Bindu
//        serviceAddedUserType = service.getServiceUserType();
//        serviceupdateddate = service.getRecordUpdatedDate();


        //20Aug2019 Arpitha
        aqPMSel.id(R.id.imgcleardate).getImageView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aqPMSel.id(R.id.etdate).text(DateTimeUtil.getTodaysDate());
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.dateresettotodaysdate), Toast.LENGTH_LONG).show();//21Aug2019 Arpitha
            }
        });

        //25Jul2019 - Bindu

        aqPMSel.id(R.id.spnfactype).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    String selected = adapterView.getItemAtPosition(i).toString();
                    if (!(selected.equalsIgnoreCase(getResources().getString(R.string.select)) || selected.equalsIgnoreCase(getResources().getString(R.string.bhome)) || selected.equalsIgnoreCase(getResources().getString(R.string.bsc)) ||
                            selected.equalsIgnoreCase(getResources().getString(R.string.bothers)))) {

//                        if (tblChildInfo == null) {
                            aqPMSel.id(R.id.trspnfacname).visible();
                            aqPMSel.id(R.id.tvfacname).text(getResources().getString(R.string.facilitynameothers));
//                        }
                        facilitynamelist = new ArrayList<>();

                        FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
                        mapFacilityname = facilityRepository.getFacilityNames(aqPMSel.id(R.id.spnfactype).getSelectedItem().toString(), appState.sessionUserId);
                        facilitynamelist.add(getResources().getString(R.string.selectfacilityname));
                        for (Map.Entry<String, String> village : mapFacilityname.entrySet())
                            facilitynamelist.add(village.getKey());
                        facilitynamelist.add(getResources().getString(R.string.other));
                        ArrayAdapter<String> FacNameAdapter = new ArrayAdapter<String>(ImmunizationListActivity.this,
                                R.layout.simple_spinner_dropdown_item, facilitynamelist);
                        FacNameAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        aqPMSel.id(R.id.spnfacname).adapter(FacNameAdapter);
                    } else {
                        aqPMSel.id(R.id.trspnfacname).gone();
                        aqPMSel.id(R.id.tretfacname).visible();
                        aqPMSel.id(R.id.tvfacname).text(getResources().getString(R.string.facilityname));
                    }
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        // Facility name on select listener
        aqPMSel.id(R.id.spnfacname).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    String selected = adapterView.getItemAtPosition(i).toString();
                    if (selected.equalsIgnoreCase(getResources().getString(R.string.other))) {
                        aqPMSel.id(R.id.tretfacname).visible();
                        aqPMSel.id(R.id.etfacname).text("");
                    } else {
                        aqPMSel.id(R.id.tretfacname).gone();
                        aqPMSel.id(R.id.etfacname).text("");
                    }
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

       /* if (service.getServiceDueDateMin() != null
                && service.getServiceDueDateMax() != null
                && service.getServiceDueDateMin().length() > 0
                && service.getServiceDueDateMax().length() > 0) {
            aqPMSel.id(R.id.tvdurationval).text(service.getServiceDueDateMin() + "   -  " + service.getServiceDueDateMax());
        }*/

       /* WomanServiceRepository womanServiceRepository = new WomanServiceRepository(databaseHelper);
        tblChildInfo = womanServiceRepository.getService(womanId, serviceId,service.getServiceNumber());
        if (tblChildInfo != null) {
            try {
                aqPMSel.id(R.id.etdate).text(tblChildInfo.getServiceActualDateofAction());
                aqPMSel.id(R.id.etcomments).text(tblChildInfo.getServiceComments());
                aqPMSel.id(R.id.etfacname).text(tblChildInfo.getServiceProvidedFacName());
                aqPMSel.id(R.id.etdate).enabled(false);
                aqPMSel.id(R.id.etfacname).enabled(false);
                aqPMSel.id(R.id.spnfactype).enabled(false);
                aqPMSel.id(R.id.etcomments).enabled(false);
                aqPMSel.id(R.id.btnsave).gone();

                aqPMSel.id(R.id.spnfactype).setSelection(Arrays.asList(getResources().getStringArray(R.array.facilitytype)).indexOf(arrVal.getServiceProvidedFacType())); //12Aug2019 - Bindu set spinner value

                //22Jul2019 - Bindu
                aqPMSel.id(R.id.txtheading).text(getResources().getString(R.string.view)
                        + " : " + arrVal.getServiceType() + " " + arrVal.getServiceNumber());
                aqPMSel.id(R.id.trspnfacname).gone(); //25Jul2019 - Bindu
                aqPMSel.id(R.id.tvfacname).text(getResources().getString(R.string.facilityname));

            } catch (Exception e) {

                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            }
        }*/


        final int finalServiceId = serviceId;
        aqPMSel.id(R.id.btnsave).getImageView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (validateServiceFields())
                        confirmAlert(serviceType, finalServiceId, chlId,immuNumber);
                } catch (Exception e) {


                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }

            }
        });

        aqPMSel.id(R.id.btncancel).getImageView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    confirmAlert(null, 0,null,0);
                } catch (Exception e) {


                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }

            }
        });

        aqPMSel.id(R.id.etdate).getEditText().setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEvent.ACTION_UP == event.getAction()) {
                    DialogFragment newFragment = new DatePickerFragment(aqPMSel);
                    newFragment.show(getSupportFragmentManager(), "datePicker");

                }
                return true;
            }
        });

        alert.setView(customView).setTitle("").setPositiveButton(
                "", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        alertdialog.setCancelable(false);
                        alertdialog.show();
                        try {
                            confirmAlert(serviceType, finalServiceId,null,0);
                        } catch (Exception e) {

                            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                        }
                    }
                });

        alertdialog = alert.create();
        alertdialog.show();

        alert.setCancelable(false);

        alertdialog.setTitle(context.getResources().getString(R.string.ok));

        alert.setCancelable(true);
        alertdialog.show();
        alertdialog.setCancelable(true);
    }

    //    update captured data to database
    private void addServiceData(String serviceType, int serviceId,String chlId, int serviceNumber) {
        try {
            final TblChildImmunization tblChildImmunization = new TblChildImmunization();
            final TransactionHeaderRepository transRepo = new TransactionHeaderRepository(databaseHelper);
            final int transId = transRepo.iCreateNewTrans(appState.sessionUserId,databaseHelper);

            tblChildImmunization.setImmActualDateofAction(aqPMSel.id(R.id.etdate).getText().toString());
            tblChildImmunization.setImmChildId(chlId);
            tblChildImmunization.setImmComments(aqPMSel.id(R.id.etcomments).getText().toString());
            String[] delTypeby = getResources().getStringArray(R.array.facilitytypesave);
            String posofDeltype = "";
            posofDeltype = delTypeby[aqPMSel.id(R.id.spnfactype).getSelectedItemPosition()];

            tblChildImmunization.setImmFacType(posofDeltype);
            tblChildImmunization.setImmMaxDate(strMaxDate);
            tblChildImmunization.setImmMinDate(strMinDate);

            if(serviceType.contains("VitaminAsyrup") || serviceType.contains("Measles") ) {
                String sNum  =  serviceType.substring(serviceType.length() - 1);
                if(sNum!=null && sNum.trim().length()>0)
                    serviceNumber = Integer.parseInt(sNum);
            }
            else
                serviceNumber = 1;
            tblChildImmunization.setImmNumber(serviceNumber);
            tblChildImmunization.setImmId(serviceId);
            tblChildImmunization.setImmType(serviceType);
            tblChildImmunization.setImmUserType(appState.userType);
            tblChildImmunization.setUserId(appState.sessionUserId);
            tblChildImmunization.setRecordCreatedDate(DateTimeUtil.getTodaysDate()+" "+ DateTimeUtil.getCurrentTime());
            tblChildImmunization.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());//21May2021 Arpitha

            tblChildImmunization.setTransId(transId);
            tblChildImmunization.setWomenId(strWomanId);




            String sel = "";
            if (aqPMSel.id(R.id.spnfacname).getSelectedItem() != null)
                sel = aqPMSel.id(R.id.spnfacname).getSelectedItem().toString();


            if (sel.equalsIgnoreCase(getResources().getString(R.string.select)))
                sel = getResources().getString(R.string.strselect);
            else if (sel.equalsIgnoreCase(getResources().getString(R.string.other)))
                sel = getResources().getString(R.string.strother);

            if (sel.trim().length() > 0 && !(sel.equalsIgnoreCase("select")
                    || sel.equalsIgnoreCase("other")))
                tblChildImmunization.setImmProvidedFacName(sel);
            else if (sel.equalsIgnoreCase("other")
                    && aqPMSel.id(R.id.etfacname).getText().toString().trim().length() <= 0)
                tblChildImmunization.setImmProvidedFacName("Other");
            else
                tblChildImmunization.setImmProvidedFacName(aqPMSel.id(R.id.etfacname).getText().toString());


            TransactionManager.callInTransaction(databaseHelper.getConnectionSource(), new Callable<Void>() {
                public Void call() throws Exception {//29Aug2019 Arpitha - transaction manager

                    ImmunizationRepository immunizationRepository = new ImmunizationRepository(databaseHelper);
                 int add =    immunizationRepository.addImmunization(databaseHelper,tblChildImmunization);
                   if(add>0) {
                   boolean added =     transRepo.iNewRecordTrans(appState.sessionUserId, transId, "tblchildimmunization", databaseHelper);

                   if(added) {
                       Toast.makeText(ImmunizationListActivity.this, "Service Details Updated Successfully!!", Toast.LENGTH_LONG).show();
                       alertdialog.cancel();
                   }else
                       Toast.makeText(getApplicationContext(), getResources().getString(R.string.serviceupdatefailed), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha

                   }else
                       Toast.makeText(getApplicationContext(), getResources().getString(R.string.serviceupdatefailed), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha

                    return null;
                }
            });
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.serviceupdatefailed), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    /**
     * This is a Recursive method that traverse through the group and subgroup
     * of view Sets text, assigns clickListners
     */
    private ArrayList<View> initializeScreen(View v, Context context) {

        if (!(v instanceof ViewGroup)) {
            ArrayList<View> viewArrayList = new ArrayList<>();
            viewArrayList.add(v);

            // TextView - Assign Labels
            if (v.getClass() == TextView.class || v.getClass() == RadioButton.class) {
            }

            if (v.getClass() == RadioButton.class) {
                aqPMSel.id(v.getId()).clicked(context, "commonClick");
            }

            return viewArrayList;
        }

        if (v instanceof Spinner) {
        }

        ArrayList<View> result = new ArrayList<View>();

        ViewGroup viewGroup = (ViewGroup) v;
        for (int i = 0; i < viewGroup.getChildCount(); i++) {

            View child = viewGroup.getChildAt(i);

            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);
            viewArrayList.addAll(initializeScreen(child, context));

            result.addAll(viewArrayList);
        }
        return result;
    }



    boolean validateServiceFields() {
//         if (aqChildImmunization.id(R.id.spnimmu).getSelectedItemPosition() > 1) {
//            Toast.makeText(getApplicationContext(), getResources().getString(R.string.selectservtype), Toast.LENGTH_LONG).show();
//            return false;
//        }
//       else
        if (aqPMSel.id(R.id.etdate).getText().toString().trim().length() <= 0) {
            Toast.makeText(getApplicationContext(), "Enter Service Completed Date", Toast.LENGTH_LONG).show();
            return false;
        } else if (aqPMSel.id(R.id.spnfactype).getSelectedItemPosition() == 0) {
            Toast.makeText(getApplicationContext(), "Select Service Provided Facility Type", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    //   get Next date
    public String getNextDate(String srcDate, int interval, String td, boolean isMaxDt)
            throws Exception {


        SimpleDateFormat xSimpleDate = new SimpleDateFormat("dd-MM-yyyy");
        String nxtDt = null;

        if (srcDate!=null && srcDate.length() != 0) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(xSimpleDate.parse(srcDate));
            cal.add(Calendar.DATE, interval);
            Date xDt = cal.getTime();

            Date eddDt = xSimpleDate.parse(td);
            int i = xDt.compareTo(eddDt);
            nxtDt = xSimpleDate.format(xDt);
            return nxtDt;
        }
        return nxtDt;
    }

    /**
     * Confirm Alert on Click save
     */
    private void confirmAlert(final String serviceType, final int serviceId,
                              final String chlId, final int immuNumber) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        String message;
        if (serviceType != null || serviceId != 0)
            message = getResources().getString(R.string.m099);
        else {
//            if (arrVal != null)
//                message = getResources().getString(R.string.areyousuretoexit);
//            else
            message = getResources().getString(R.string.m110);
        }

        alertDialogBuilder.setMessage(message).setCancelable(false)
                .setNegativeButton(getResources().getString(R.string.m121),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                try {
                                    if (serviceType != null || serviceId != 0) {

                                        addServiceData(serviceType,serviceId,chlId, immuNumber);
                                        getData();                                    } else
                                        alertdialog.cancel();

                                } catch (Exception e) {



                                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                                }
                            }
                        })
                .setPositiveButton((getResources().getString(R.string.m122)),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }



    @SuppressLint("SimpleDateFormat")
    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        private AQuery aqPMSel;
        private tblregisteredwomen woman;

        public DatePickerFragment() {
        }

        public DatePickerFragment(AQuery aQuery) {
            aqPMSel = aQuery;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            int year = 0, month = 0, day = 0;
            try {

                Calendar c = Calendar.getInstance();
                String defaultDate = DateTimeUtil.getTodaysDate(); //09Aug2019 - Cal set date of Provided date
                if (defaultDate.length() > 0) {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    Date sendDate = sdf.parse(defaultDate);
                    c.setTime(sendDate);
                    year = c.get(Calendar.YEAR);
                    month = c.get(Calendar.MONTH);
                    day = c.get(Calendar.DAY_OF_MONTH);
                } else {
                    c.setTime(new Date());
                    year = c.get(Calendar.YEAR);
                    month = c.get(Calendar.MONTH);
                    day = c.get(Calendar.DAY_OF_MONTH);
                }
            } catch (Exception e) {


                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            }

            // Create a new instance of DatePickerDialog and return it
            DatePickerDialog dpDialog;
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                // Do something for lollipop and above versions
                dpDialog = new DatePickerDialog(getActivity(), R.style.DialogTheme, this, year, month, day);
            } else {
                // do something for phones running an SDK before lollipop
                dpDialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, this, year, month, day);
                dpDialog.setTitle("Set a Date");
                dpDialog.getDatePicker().setCalendarViewShown(false);
            }
            return dpDialog;
        }

        @Override
        public void onDateSet(DatePicker view, int year, int month, int day) {
            if (view.isShown()) {
                try {
                    String SelectedDate = String.format("%02d", day) + "-" +
                            String.format("%02d", month + 1) + "-"
                            + year;
//                    Date lmpDate = null;
                    SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                    Date seldate = format.parse(SelectedDate);
                    Date mindate = null;


                    if(strMinDate!=null && strMinDate.trim().length()>0)
                        mindate = format.parse(strMinDate);

                    if(seldate.after(new Date())) {
                        Toast.makeText(getActivity(), getResources().getString(R.string.date_cannot_af_currentdate), Toast.LENGTH_LONG).show();
                        aqPMSel.id(R.id.etdate).text(DateTimeUtil.getTodaysDate());
                    }
                    else if(mindate!=null && seldate.before(mindate))
                    {
                        Toast.makeText(getActivity(), getResources().getString(R.string.immunizationtapplibeforemindate), Toast.LENGTH_LONG).show();
                        aqPMSel.id(R.id.etdate).text(DateTimeUtil.getTodaysDate());
                    }
                    else {
                        aqPMSel.id(R.id.etdate).text(SelectedDate);
                    }


//                    if(strFromDate!=null && strFromDate.trim().length()>0)
//                        fromdate = format.parse(strFromDate);
//
                  /*  if(fromDate)
                    {
                        if(seldate.after(new Date())) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.date_cannot_af_currentdate), Toast.LENGTH_LONG).show();
                            strFromDate = "";
                            aqPMSel.id(R.id.txtfromdate).text(getResources().getString(R.string.fromdate));
                        }else {
                            strFromDate = SelectedDate;
                            aqPMSel.id(R.id.txtfromdate).text(strFromDate);
                        }
                    }else {

                        if(seldate.after(new Date())) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.date_cannot_af_currentdate), Toast.LENGTH_LONG).show();
                            strToDate = "";
                            aqPMSel.id(R.id.txttodate).text(getResources().getString(R.string.todate));
                        }else if(fromdate!=null && seldate.before(fromdate)) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.date_cannot_af_currentdate), Toast.LENGTH_LONG).show();
                            strToDate = "";
                            aqPMSel.id(R.id.txttodate).text(getResources().getString(R.string.todate));
                        }
                        else {
                            strToDate = SelectedDate;
                            aqPMSel.id(R.id.txttodate).text(strToDate);
                        }
                    }*/

                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

                }
            }
        }
    }
}



