package com.sc.stmansi.immunization;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.misc.TransactionManager;
import com.sc.stmansi.About.AboutActivity;
import com.sc.stmansi.common.AlertDialogUtil;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.pendingservices.IndicatorViewClickListener;
import com.sc.stmansi.pendingservices.PendingServicesAdapter;
import com.sc.stmansi.recyclerview.ClickListener;
import com.sc.stmansi.recyclerview.RecyclerViewMargin;
import com.sc.stmansi.repositories.FacilityRepository;
import com.sc.stmansi.repositories.ImmunizationRepository;
import com.sc.stmansi.repositories.LoginRepository;
import com.sc.stmansi.repositories.TransactionHeaderRepository;
import com.sc.stmansi.tables.TblChildImmunization;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.womanlist.RegisteredWomenActionTabs;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Callable;

import com.sc.stmansi.R;

public class ChildImmunizationActivity extends AppCompatActivity implements
        ClickListener, AdapterView.OnItemSelectedListener,
        IndicatorViewClickListener {

    private DatabaseHelper databaseHelper;
    List<String[]> childList;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    AQuery aqChildImmunization;
    private AppState appState;
    Map<Integer, String> spinnerImmunList;
    PendingServicesAdapter registeredWomanAdapter;
    private AQuery aqPMSel, aqServ;
    private AlertDialog alertdialog = null;
    private String serviceAddedUserType = "", serviceupdateddate = "";
    private ArrayList<String> facilitynamelist;
    private Map<String, String> mapFacilityname;
    String selectedChlId;
    private Spinner villageSpinner;
    private Map<String, Integer> villageCodeForName;
    private boolean spinnerSelectionChanged = false;
    String strMinDate = "", strMaxDate = "";
    String strWomanId;
    static String strFromDate;
    static String strToDate;
    static boolean fromDate;
    static boolean toDate;
    int villageCode;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_immunization);

        try {

            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

            Bundle bundle = getIntent().getBundleExtra("globalState");
            appState = bundle.getParcelable("appState");

            databaseHelper = getHelper();
            aqChildImmunization = new AQuery(this);
            recyclerView = (RecyclerView) aqChildImmunization.id(R.id.recyclerviewimmunization).getView();

            recyclerView.setHasFixedSize(true);
            // use a linear layout manager
            layoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(layoutManager);
            RecyclerViewMargin decoration = new RecyclerViewMargin(1, 1);
            recyclerView.addItemDecoration(decoration);

            strToDate = DateTimeUtil.getTodaysDate();
            ImmunizationRepository childRepository = new ImmunizationRepository(databaseHelper);
            strFromDate = childRepository.getEarliestDate();
            getSupportActionBar().setCustomView(R.layout.ejanani_village_filter);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            villageSpinner = aqChildImmunization.id(R.id.spVillageFilter).getSpinner();
            villageSpinner.setOnItemSelectedListener(this);
            populateSpinnerVillage();


            getFilteredData();

            aqChildImmunization.id(R.id.txtfromdate).text(strFromDate);
            aqChildImmunization.id(R.id.txttodate).text(strToDate);



            aqChildImmunization.id(R.id.btnok).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {

                        getFilteredData();
                    } catch (SQLException e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                    }
                }
            });

            aqChildImmunization.id(R.id.txtfromdate).getTextView().setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (MotionEvent.ACTION_UP == event.getAction()) {
                        fromDate = true;
                        toDate = false;
                        DialogFragment newFragment = new DatePickerFragment(aqChildImmunization);
                        newFragment.show(getSupportFragmentManager(), "datePicker");

                    }
                    return true;
                }
            });

            aqChildImmunization.id(R.id.txttodate).getTextView().setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (MotionEvent.ACTION_UP == event.getAction()) {
                        fromDate = false;
                        toDate = true;
                        DialogFragment newFragment = new DatePickerFragment(aqChildImmunization);
                        newFragment.show(getSupportFragmentManager(), "datePicker");

                    }
                    return true;
                }
            });

            aqChildImmunization.id(R.id.rdimmpending).checked(true);

            aqChildImmunization.id(R.id.rdimmcompleted).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        getFilteredData();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            });

            aqChildImmunization.id(R.id.rdimmpending).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        getFilteredData();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (SQLException e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }


    //    initialize db object
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }


    @Override
    public void onClick(View v) {
        int itemPosition = recyclerView.getChildLayoutPosition(v);

        try {


            if (Integer.parseInt(childList.get(recyclerView.getChildAdapterPosition(v))[1]) > 0) {
                Intent pendingChild = new Intent(ChildImmunizationActivity.this, PendingChildActivity.class);
                pendingChild.putExtra("fromDate", strFromDate);
                pendingChild.putExtra("toDate", strToDate);
                pendingChild.putExtra("immType", childList.get(recyclerView.getChildAdapterPosition(v))[0]);
                pendingChild.putExtra("tribalHamlet", villageCode);
                pendingChild.putExtra("tribalHamletpos", villageSpinner.getSelectedItemPosition());
                pendingChild.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                pendingChild.putExtra("appState", getIntent().getBundleExtra("appState"));
                pendingChild.putExtra("pending", aqChildImmunization.id(R.id.rdimmpending).isChecked());
                startActivity(pendingChild);
            } else
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_data), Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private void getFilteredData() throws SQLException {
        ImmunizationRepository childRepository = new ImmunizationRepository(databaseHelper);


        if(aqChildImmunization.id(R.id.rdimmcompleted).isChecked())
        {
            childList = childRepository.getCompletedImmuCount(
                    appState.sessionUserId, villageCode);
        }

        else
        childList = childRepository.getPendingChildCountForImmunization(
                appState.sessionUserId, villageCode,
                strFromDate, strToDate);

        if (childList != null && childList.size() > 0) {
            registeredWomanAdapter = new
                    PendingServicesAdapter(ChildImmunizationActivity.this, childList, this);
            recyclerView.setAdapter(registeredWomanAdapter);
            aqChildImmunization.id(R.id.txtnodata).gone();
            recyclerView.setVisibility(View.VISIBLE);
        } else {
            aqChildImmunization.id(R.id.txtnodata).visible();
            recyclerView.setVisibility(View.GONE);
        }

    }

    @Override
    public boolean onLongClick(View v) {
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.servicesmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.logout: {
                Intent goToScreen = new Intent(ChildImmunizationActivity.this, LoginActivity.class);
                displayAlert(getResources().getString(R.string.m111), goToScreen);
                return true;
            }
            case R.id.home: {
                Intent home = new Intent(ChildImmunizationActivity.this, MainMenuActivity.class);
                home.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                startActivity(home);
                return true;
            }
            case R.id.wlist: {
                Intent goToScreen = new Intent(ChildImmunizationActivity.this, RegisteredWomenActionTabs.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }
            case R.id.about: {
                Intent goToScreen = new Intent(ChildImmunizationActivity.this, AboutActivity.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                displayAlert(getResources().getString(R.string.exit), goToScreen);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    /**
     * Save Confirmation Alert
     *
     * @param goToScreen
     * @param spanText2
     */
    private void displayAlert(final String spanText2, final Intent goToScreen) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        String strPosButton = "";
        if (goToScreen != null)
            strPosButton = getResources().getString(R.string.yes);
        else
            strPosButton = getResources().getString(R.string.ok);

        alertDialogBuilder.setMessage(spanText2).setCancelable(false).setNegativeButton(
                strPosButton, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (goToScreen != null) {

                            goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(goToScreen);
                            if (spanText2.contains("logout")) {
                                try {
                                    LoginRepository loginRepository = new LoginRepository(databaseHelper);
                                    loginRepository.updateLogoutTime(appState.loginAuditId, appState.sessionUserId);
                                } catch (Exception e) {
                                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                                }
                            }
                        } else
                            dialog.cancel();
                    }
                });

        if (goToScreen != null) {
            alertDialogBuilder.setPositiveButton(getResources().getString(R.string.m122),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
        }


        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    @Override
    public void onBackPressed() {

    }


    public static <T, E> T getKeysByValue(Map<T, E> map, E value) {
        for (Map.Entry<T, E> entry : map.entrySet()) {
            if (Objects.equals(value, entry.getValue())) {
                return entry.getKey();
            }
        }
        return null;
    }




    //    update captured data to database
    private void addServiceData(String serviceType, int serviceId, String chlId, int serviceNumber) {
        try {
            final TblChildImmunization tblChildImmunization = new TblChildImmunization();
            TransactionHeaderRepository transRepo = new TransactionHeaderRepository(databaseHelper);
            final int transId = transRepo.iCreateNewTrans(appState.sessionUserId, databaseHelper);

            tblChildImmunization.setImmActualDateofAction(aqPMSel.id(R.id.etdate).getText().toString());
            tblChildImmunization.setImmChildId(chlId);
            tblChildImmunization.setImmComments(aqPMSel.id(R.id.etcomments).getText().toString());
            tblChildImmunization.setImmFacType(aqPMSel.id(R.id.spnfactype).getSelectedItem().toString());
            tblChildImmunization.setImmMaxDate(strMaxDate);
            tblChildImmunization.setImmMinDate(strMinDate);

            if (serviceType.contains("VitaminAsyrup") || serviceType.contains("Measles")) {
                String sNum = serviceType.substring(serviceType.length() - 2);
                if (sNum != null && sNum.trim().length() > 0)
                    serviceNumber = Integer.parseInt(sNum);
            } else
                serviceNumber = 1;
            tblChildImmunization.setImmNumber(serviceNumber);
            tblChildImmunization.setImmId(serviceId);
            tblChildImmunization.setImmType(serviceType);
            tblChildImmunization.setImmUserType(appState.userType);
            tblChildImmunization.setUserId(appState.sessionUserId);
            tblChildImmunization.setRecordCreatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());
            tblChildImmunization.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());//21May2021 Arpitha

            tblChildImmunization.setTransId(transId);
            tblChildImmunization.setWomenId(strWomanId);


            String sel = "";
            if (aqPMSel.id(R.id.spnfacname).getSelectedItem() != null)
                sel = aqPMSel.id(R.id.spnfacname).getSelectedItem().toString();

            if (sel.trim().length() > 0 && !(sel.equalsIgnoreCase("select")
                    || sel.equalsIgnoreCase("other")))
                tblChildImmunization.setImmProvidedFacName(sel);
            else if (sel.equalsIgnoreCase("other")
                    && aqPMSel.id(R.id.etfacname).getText().toString().trim().length() <= 0)
                tblChildImmunization.setImmProvidedFacName("Other");
            else
                tblChildImmunization.setImmProvidedFacName(aqPMSel.id(R.id.etfacname).getText().toString());


            TransactionManager.callInTransaction(databaseHelper.getConnectionSource(), new Callable<Void>() {
                public Void call() throws Exception {//29Aug2019 Arpitha - transaction manager

                    ImmunizationRepository immunizationRepository = new ImmunizationRepository(databaseHelper);
                    immunizationRepository.addImmunization(databaseHelper, tblChildImmunization);

                    Toast.makeText(ChildImmunizationActivity.this, "Service Details Updated Successfully!!", Toast.LENGTH_LONG).show();
                    alertdialog.cancel();

                    return null;
                }
            });
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.serviceupdatefailed), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    /**
     * This is a Recursive method that traverse through the group and subgroup
     * of view Sets text, assigns clickListners
     */
    private ArrayList<View> initializeScreen(View v, Context context) {

        if (!(v instanceof ViewGroup)) {
            ArrayList<View> viewArrayList = new ArrayList<>();
            viewArrayList.add(v);

            // TextView - Assign Labels
            if (v.getClass() == TextView.class || v.getClass() == RadioButton.class) {
            }

            if (v.getClass() == RadioButton.class) {
                aqPMSel.id(v.getId()).clicked(context, "commonClick");
            }

            return viewArrayList;
        }

        if (v instanceof Spinner) {
        }

        ArrayList<View> result = new ArrayList<View>();

        ViewGroup viewGroup = (ViewGroup) v;
        for (int i = 0; i < viewGroup.getChildCount(); i++) {

            View child = viewGroup.getChildAt(i);

            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);
            viewArrayList.addAll(initializeScreen(child, context));

            result.addAll(viewArrayList);
        }
        return result;
    }






    /**
     * populate Village to Spinner
     */
    private void populateSpinnerVillage() throws Exception {
        FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
        villageCodeForName = facilityRepository.getPlaceMap();

        ArrayAdapter<CharSequence> villageSpinnerAdapter = new ArrayAdapter<>(this, R.layout.actionbar_spinner_list_item);
//        villageSpinnerAdapter.add("All");06May2021 Arpitha
        villageSpinnerAdapter.add(getResources().getString(R.string.all));//06May2021 Arpitha
        for (Map.Entry<String, Integer> village : villageCodeForName.entrySet()) {
            villageSpinnerAdapter.add(village.getKey());
        }
        villageSpinnerAdapter.setDropDownViewResource(R.layout.actionbar_spinner_layout_view);
        villageSpinner.setAdapter(villageSpinnerAdapter);
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        try {
            appState.selectedVillageTitle = (String) parent.getItemAtPosition(position);
            if (spinnerSelectionChanged) {


                if (position > 0) {
                    appState.selectedVillageCode = villageCodeForName.get(appState.selectedVillageTitle);
                    villageCode = villageCodeForName.get(appState.selectedVillageTitle);
                } else {
                    appState.selectedVillageCode = 0;
                    villageCode = 0;
                }
                getFilteredData();
                appState.selectedVillagePosition = position;

            }
            spinnerSelectionChanged = true;
        } catch (SQLException e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void displayAlert(int selectedItemPosition) {

    }

    @Override
    public void displayHomeVisitList(int selectedItemPosition) {

    }

    @Override
    public void displayDelComplicationAlert(int selectedItemPosition) {

    }

    @SuppressLint("SimpleDateFormat")
    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        private AQuery aqPMSel;
        private tblregisteredwomen woman;

        public DatePickerFragment() {
        }

        public DatePickerFragment(AQuery aQuery) {
            aqPMSel = aQuery;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            int year = 0, month = 0, day = 0;
            try {

                Calendar c = Calendar.getInstance();
                String defaultDate = DateTimeUtil.getTodaysDate(); //09Aug2019 - Cal set date of Provided date
                if (defaultDate.length() > 0) {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    Date sendDate = sdf.parse(defaultDate);
                    c.setTime(sendDate);
                    year = c.get(Calendar.YEAR);
                    month = c.get(Calendar.MONTH);
                    day = c.get(Calendar.DAY_OF_MONTH);
                } else {
                    c.setTime(new Date());
                    year = c.get(Calendar.YEAR);
                    month = c.get(Calendar.MONTH);
                    day = c.get(Calendar.DAY_OF_MONTH);
                }
            } catch (Exception e) {


                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            }

            // Create a new instance of DatePickerDialog and return it
            DatePickerDialog dpDialog;
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                // Do something for lollipop and above versions
                dpDialog = new DatePickerDialog(getActivity(), R.style.DialogTheme, this, year, month, day);
            } else {
                // do something for phones running an SDK before lollipop
                dpDialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, this, year, month, day);
                dpDialog.setTitle("Set a Date");
                dpDialog.getDatePicker().setCalendarViewShown(false);
            }
            return dpDialog;
        }

        @Override
        public void onDateSet(DatePicker view, int year, int month, int day) {
            if (view.isShown()) {
                try {
                    String SelectedDate = String.format("%02d", day) + "-" +
                            String.format("%02d", month + 1) + "-"
                            + year;
                    Date lmpDate = null;
                    SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                    Date seldate = format.parse(SelectedDate);

                    Date fromdate = null, todate = null;

                    if (strFromDate != null && strFromDate.trim().length() > 0)
                        fromdate = format.parse(strFromDate);

                    if (strToDate != null && strToDate.trim().length() > 0)
                        todate = format.parse(strToDate);

                    if (fromDate) {
                       /* if(seldate.after(new Date())) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.date_cannot_af_currentdate), Toast.LENGTH_LONG).show();
                            strFromDate = "";
                            aqPMSel.id(R.id.txtfromdate).text(getResources().getString(R.string.fromdate));
                        }else {*/

                        if (todate != null && seldate.after(todate)) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.date_cannot_after_todate), Toast.LENGTH_LONG).show();
                            strFromDate = "";
                            aqPMSel.id(R.id.txtfromdate).text(getResources().getString(R.string.fromdate));

                        } else {
                            strFromDate = SelectedDate;
                            aqPMSel.id(R.id.txtfromdate).text(strFromDate);
                        }
//                        }
                    } else if (toDate) {

                        /*if(seldate.after(new Date())) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.date_cannot_af_currentdate), Toast.LENGTH_LONG).show();
                            strToDate = "";
                            aqPMSel.id(R.id.txttodate).text(getResources().getString(R.string.todate));
                        }else */
                        if (fromdate != null && seldate.before(fromdate)) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.date_cannot_before_fromdate), Toast.LENGTH_LONG).show();
                            strToDate = "";
                            aqPMSel.id(R.id.txttodate).text(getResources().getString(R.string.todate));
                        } else {
                            strToDate = SelectedDate;
                            aqPMSel.id(R.id.txttodate).text(strToDate);
                        }
                    } else {
                        if (seldate.after(new Date())) {
                            AlertDialogUtil.displayAlertMessage(
                                    getResources().getString(R.string.date_cannot_af_currentdate)
                                    , getActivity());
                            aqPMSel.id(R.id.etdate).text(DateTimeUtil.getTodaysDate());
                        } else if (lmpDate != null && seldate.before(lmpDate)) {
                            AlertDialogUtil.displayAlertMessage(
                                    getResources().getString(R.string.date_cannot_bf_lmp)
                                    , getActivity());
                            aqPMSel.id(R.id.etdate).text(DateTimeUtil.getTodaysDate());
                        }/* else if (selmindate != null && seldate.before(selmindate)) {   //09Aug2019 - Bindu - chk for min date
                        AlertDialogUtil.displayAlertMessage(
                                getResources().getString(R.string.servntapplibeforemindate)
                                , getActivity());
                        aqPMSel.id(R.id.etdate).text(DateTimeUtil.getTodaysDate());//22Aug2019 Arpitha
                    }*/ else {
                            aqPMSel.id(R.id.etdate).text(SelectedDate);
                            aqPMSel.id(R.id.imgcleardate).visible();//20Aug2019 Arpitha
                        }
                    }
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

                }
            }
        }
    }

    //   get Next date
    public String getNextDate(String srcDate, int interval, String td, boolean isMaxDt)
            throws Exception {


        SimpleDateFormat xSimpleDate = new SimpleDateFormat("dd-MM-yyyy");
        String nxtDt = null;

        if (srcDate != null && srcDate.length() != 0) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(xSimpleDate.parse(srcDate));
            cal.add(Calendar.DATE, interval);
            Date xDt = cal.getTime();

            Date eddDt = xSimpleDate.parse(td);
            int i = xDt.compareTo(eddDt);
            nxtDt = xSimpleDate.format(xDt);
            return nxtDt;
        }
        return nxtDt;
    }

    @Override
    public void displayLMPConfirmation(int selectedItemPosition) {

    }
}
