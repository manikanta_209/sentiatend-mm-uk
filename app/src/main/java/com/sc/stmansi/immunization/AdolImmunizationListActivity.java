package com.sc.stmansi.immunization;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.DialogFragment;
import androidx.legacy.app.ActionBarDrawerToggle;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.misc.TransactionManager;
import com.sc.stmansi.About.AboutActivity;
import com.sc.stmansi.HomeVisit.HomeVisit;
import com.sc.stmansi.R;
import com.sc.stmansi.SanNap.List_SanNap;
import com.sc.stmansi.adolescent.AdolDeactivation;
import com.sc.stmansi.adolescent.AdolParticipatedTrainings;
import com.sc.stmansi.adolescent.AdolRegistration;
import com.sc.stmansi.adolescent.AdolVisitHistory;
import com.sc.stmansi.adolescent.AdolVisit_New;
import com.sc.stmansi.childregistration.ChildRegistrationActivity;
import com.sc.stmansi.common.AlertDialogUtil;
import com.sc.stmansi.common.CommonDatePickerFragment;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.common.NavDrawerItem;
import com.sc.stmansi.common.NavDrawerListAdapter;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.configuration.SyncState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.recyclerview.ChildImmunizationAdapter;
import com.sc.stmansi.recyclerview.ClickListener;
import com.sc.stmansi.registration.RegistrationActivity;
import com.sc.stmansi.repositories.AdolescentRepository;
import com.sc.stmansi.repositories.FacilityRepository;
import com.sc.stmansi.repositories.ImmunizationRepository;
import com.sc.stmansi.repositories.TransactionHeaderRepository;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.repositories.WomanServiceRepository;
import com.sc.stmansi.services.CustomSpinnerAdapter;
import com.sc.stmansi.tables.ChildImmunization;
import com.sc.stmansi.tables.TblChildImmunization;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblAdolReg;
import com.sc.stmansi.tables.tblregisteredwomen;
import com.sc.stmansi.viewprofile.ViewProfileActivity;
import com.sc.stmansi.womanlist.RegisteredWomenActionTabs;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Callable;

public class AdolImmunizationListActivity extends AppCompatActivity implements ClickListener {

    DatabaseHelper databaseHelper;
    RecyclerView recyclerView;
    tblAdolReg currentAdolDetail;
    AQuery aqImm;
    AppState appState;
//    TblChildInfo tblChildInfoFromDb;
    private AQuery aq;
    private AlertDialog alertdialog = null;
    private AlertDialog.Builder alert;
    TblChildImmunization tblChildImmunization;
    List<ChildImmunization> list;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private List<tblregisteredwomen> wVisitDetails;

    private AQuery  aqServ;
    private String serviceAddedUserType = "", serviceupdateddate = "";
    static String strMinDate = "";
    String strMaxDate = "";
    String strWomanId;
    private ArrayList<String> facilitynamelist;
    private Map<String, String> mapFacilityname;
    int serviceId = 0;
    TblInstusers user;
    private String womanId;
    private String AdolID;

    private AQuery aqPMSel;
    private Activity activity;
    RadioGroup radGrpEDDorLmp;
    private boolean isRegDate;
    Boolean wantToCloseDialog = false;
    private int noOfDays;
    private tblregisteredwomen woman;
    private SyncState syncState;
    private EditText currentEditText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servicessummary);

        try {

            Bundle bundle = getIntent().getBundleExtra("globalState");
            appState = bundle.getParcelable("appState");
            databaseHelper = getHelper();
//            tblChildInfoFromDb = (TblChildInfo) getIntent().getSerializableExtra("tblChildInfo");


            currentAdolDetail = (tblAdolReg) getIntent().getSerializableExtra("tblChildInfo");
            AdolID = getIntent().getStringExtra("adolID");
            aqImm = new AQuery(this);

            aqImm.id(R.id.txtpending30).text(getResources().getString(R.string.immunizationlist));

            UserRepository userRepository = new UserRepository(databaseHelper);
             user =  userRepository.getOneAuditedUser(appState.ashaId);

            initiateDrawer();

            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(true);


            if (currentAdolDetail != null) {
                getSupportActionBar().setTitle(currentAdolDetail.getRegAdolName() + "("+ currentAdolDetail.getRegAdolGender()+")");
            }




            recyclerView = findViewById(R.id.woman_services_recycler_view);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));

            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                    DividerItemDecoration.VERTICAL);
            DividerItemDecoration dividerItemDecoration1 = new DividerItemDecoration(recyclerView.getContext(),
                    DividerItemDecoration.HORIZONTAL);
            recyclerView.addItemDecoration(dividerItemDecoration);
            recyclerView.addItemDecoration(dividerItemDecoration1);

            getData();

            int[] spnFilterImages = new int[]{R.drawable.ic_all, R.drawable.warning, R.drawable.notdone, R.drawable.upcoming, R.drawable.tick};
            CustomSpinnerAdapter mCustomAdapter = new CustomSpinnerAdapter(this, getResources().getStringArray(R.array.servicefilter), spnFilterImages);
            aqImm.id(R.id.spnfilter).adapter(mCustomAdapter);

            aqImm.id(R.id.spnfilter).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        getData();
                    } catch (Exception e) {
                       FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        } catch (Exception e) {
           FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }

    }

    private void getData() throws Exception {

        ImmunizationRepository immunizationRepository = new ImmunizationRepository(databaseHelper);

        if (immunizationRepository.getChildImmunizationStatus(appState.sessionUserId,
                currentAdolDetail.getAdolID(), aqImm.id(R.id.spnfilter).
                        getSelectedItemPosition(), currentAdolDetail.
                        getRegAdolDoB()) != null && immunizationRepository.getChildImmunizationStatus(appState.sessionUserId,
                currentAdolDetail.getAdolID(), aqImm.id(R.id.spnfilter).
                        getSelectedItemPosition(), currentAdolDetail.
                        getRegAdolDoB()).size() > 0) {

            list = immunizationRepository.getChildImmunizationStatus
                    (appState.sessionUserId,
                            currentAdolDetail.getAdolID(), aqImm.id(R.id.spnfilter).
                                    getSelectedItemPosition(), currentAdolDetail.
                                    getRegAdolDoB());
            ChildImmunizationAdapter childImmunizationAdapter = new
                    ChildImmunizationAdapter(list
                    , this,
                    null, currentAdolDetail.getRegAdolDoB());
            recyclerView.setAdapter(childImmunizationAdapter);
            recyclerView.setVisibility(View.VISIBLE);
            aqImm.id(R.id.txtnodatapending).gone();
        } else {
            recyclerView.setVisibility(View.GONE);
            aqImm.id(R.id.txtnodatapending).visible();
        }
    }

    //    initialize db object
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void onClick(View v) {


        try {

//            26nov2019 Arpitha

            String strImmType = list.get(recyclerView.getChildAdapterPosition(v)).getServiceType();
            ImmunizationRepository immunizationRepository = new ImmunizationRepository(databaseHelper);
            List<TblChildImmunization> tblChildImmunizationsList =
                    immunizationRepository.getImmunizationData(strImmType, currentAdolDetail.getAdolID());


            if(((currentAdolDetail.getRegAdolDeactivateDate()!=null &&
                    currentAdolDetail.getRegAdolDeactivateDate().trim().length()>0)
                    ||user.getIsDeactivated()==1) && tblChildImmunizationsList.size()<=0)

                Toast.makeText(getApplicationContext(),getResources().getString(R.string.userdeactivated),Toast.LENGTH_LONG).show();
                else {


                WomanServiceRepository womanServiceRepository = new WomanServiceRepository(databaseHelper);
                int minduration = womanServiceRepository.getDaysforImmunization("DaysDiffStart", strImmType);


                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                Date seldate = format.parse(DateTimeUtil.getTodaysDate());
                Date mindate = null;

                strMinDate = getNextDate(currentAdolDetail.getRegAdolDoB(),
                        minduration, currentAdolDetail.getRegAdolDoB(), false);

                if (strMinDate != null && strMinDate.trim().length() > 0)
                    mindate = format.parse(strMinDate);


                if (mindate != null && seldate.before(mindate)) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.immunizationtapplibeforemindate), Toast.LENGTH_LONG).show();
                } else {
                    if (tblChildImmunizationsList != null && tblChildImmunizationsList.size() > 0)
                        addServiceDataOLD(AdolImmunizationListActivity.this,
                                tblChildImmunizationsList);
                    else
                        addServiceData(AdolImmunizationListActivity.this,
                                strImmType, list.get(recyclerView.getChildAdapterPosition(v))
                                        .getServiceId(), currentAdolDetail);
                }
            }

        } catch (SQLException e) {
           FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        } catch (Exception e) {
           FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }


    }

    @Override
    public boolean onLongClick(View v) {
        return false;
    }

    @Override
    public void onBackPressed() {

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportActionBar().setTitle((currentAdolDetail.getRegAdolName() +
                " (" +
                currentAdolDetail.getRegAdolGender() + ")"));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer1); //27Sep2019 - Bindu - set drawer and enable true home btn
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);

        aqImm.id(R.id.tvWomanName).text((currentAdolDetail.getRegAdolName() +
                " (" +
                currentAdolDetail.getRegAdolGender() + ")"));
//            aqServ.id(R.id.tvInfo1).text(getResources().getString(R.string.tvlmp) + " - " + woman.getRegLMP());
        if (currentAdolDetail.getRegAdolDoB()!=null && currentAdolDetail.getRegAdolDoB().length() > 0)
            aqImm.id(R.id.tvInfo1).text((getResources().getString(R.string.age)
                    + " " + DateTimeUtil.calculateAge(currentAdolDetail.getRegAdolDoB())));

        aqImm.id(R.id.ivWomanImg).gone();
        getMenuInflater().inflate(R.menu.servicesmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            case R.id.logout: {
                Intent goToScreen = new Intent(AdolImmunizationListActivity.this, LoginActivity.class);
                startActivity(goToScreen);
                return true;
            }
            case R.id.home: {
                Intent home = new Intent(AdolImmunizationListActivity.this, MainMenuActivity.class);
                home.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                startActivity(home);
                return true;
            }
            case R.id.wlist: {
                Intent goToScreen = new Intent(AdolImmunizationListActivity.this, RegisteredWomenActionTabs.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                startActivity(goToScreen);
                return true;
            }
            case R.id.about: {
                Intent goToScreen = new Intent(AdolImmunizationListActivity.this, AboutActivity.class);
                goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));

                startActivity(goToScreen);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void addServiceDataOLD(final Context context,
                                  final List<TblChildImmunization> tblChildImmunizationsList) throws SQLException {
        final View customView = LayoutInflater.from(context).inflate(R.layout.add_services_info, null);

        aq = new AQuery(customView);

        aq.id(R.id.etdate).text(DateTimeUtil.getTodaysDate());

        aq.id(R.id.txtheading).text(getResources().getString(R.string.addplannedservice) + ": " + tblChildImmunizationsList.get(0).getImmType());
        aq.id(R.id.etdate).text(DateTimeUtil.getTodaysDate());
        if(currentAdolDetail !=null) {
            aq.id(R.id.txtNameval).text(currentAdolDetail.getRegAdolName());
            aq.id(R.id.txtlmpval).text(currentAdolDetail.getRegAdolDoB());
            aq.id(R.id.tvWname).text(getResources().getString(R.string.tvchildname));
            strWomanId = currentAdolDetail.getAdolID();
            serviceId  = serviceId;
        }

        aq.id(R.id.trspnfacname).gone();
        aq.id(R.id.tblservicesdetails).gone();
        aq.id(R.id.txtservicedeatils).gone();


        aq.id(R.id.spnfactype).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    String selected = adapterView.getItemAtPosition(i).toString();
                    if (!(selected.equalsIgnoreCase(getResources().getString(R.string.select)) || selected.equalsIgnoreCase(getResources().getString(R.string.bhome)) || selected.equalsIgnoreCase(getResources().getString(R.string.bsc)) ||
                            selected.equalsIgnoreCase(getResources().getString(R.string.bothers)))) {

                        if (tblChildImmunizationsList == null) {
                            aq.id(R.id.trspnfacname).visible();
                            aq.id(R.id.tvfacname).text(getResources().getString(R.string.facilitynameothers));
                        }
                        facilitynamelist = new ArrayList<>();

                        FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
                        mapFacilityname = facilityRepository.getFacilityNames(aq.id(R.id.spnfactype).getSelectedItem().toString(), user.getUserId());
                        //facilitynamelist.add(getResources().getString(R.string.selectfacilityname));
                        facilitynamelist.add(getResources().getString(R.string.strselect)); //15Nov2019 - Bindu selectfacilityname  to strselect
                        for (Map.Entry<String, String> village : mapFacilityname.entrySet())
                            facilitynamelist.add(village.getKey());
                        //facilitynamelist.add(getResources().getString(R.string.other));
                        facilitynamelist.add(getResources().getString(R.string.strother)); //15Nov2019 - Bindu other  to strother
                        ArrayAdapter<String> FacNameAdapter = new ArrayAdapter<String>(AdolImmunizationListActivity.this,
                                R.layout.simple_spinner_dropdown_item, facilitynamelist);
                        FacNameAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        aq.id(R.id.spnfacname).adapter(FacNameAdapter);
                    } else {
                        aq.id(R.id.trspnfacname).gone();
                        aq.id(R.id.tretfacname).visible();
                        aq.id(R.id.tvfacname).text(getResources().getString(R.string.facilityname));
                    }
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        // Facility name on select listener
        aq.id(R.id.spnfacname).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    String selected = adapterView.getItemAtPosition(i).toString();
                    if (selected.equalsIgnoreCase(getResources().getString(R.string.strother))) {  //15Nov2019 - Bindu other  to strother
                        aq.id(R.id.tretfacname).visible();
                        aq.id(R.id.etfacname).text("");
                    } else {
                        aq.id(R.id.tretfacname).gone();
                        aq.id(R.id.etfacname).text("");
                    }
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        if (tblChildImmunizationsList != null && tblChildImmunizationsList.size() > 0) {
            try {
                TblChildImmunization tblChildImmunization = tblChildImmunizationsList.get(0);
                aq.id(R.id.etdate).text(tblChildImmunization.getImmActualDateofAction());
                aq.id(R.id.etcomments).text(tblChildImmunization.getImmComments());
                aq.id(R.id.etfacname).text(tblChildImmunization.getImmProvidedFacName());
                aq.id(R.id.spnfactype).setSelection(Arrays.asList(getResources().getStringArray(R.array.facilitytype))
                        .indexOf(tblChildImmunization.getImmFacType())); //12Aug2019 - Bindu set spinner value
                aq.id(R.id.etdate).enabled(false);
                aq.id(R.id.etfacname).enabled(false);
                aq.id(R.id.spnfactype).enabled(false);
                aq.id(R.id.etcomments).enabled(false);
                aq.id(R.id.btnsave).enabled(false);
                //22Jul2019 - Bindu
                aq.id(R.id.btnsave).gone();
                aq.id(R.id.trduration).gone();
                aq.id(R.id.trspnfacname).gone(); //25Jul2019 - Bindu
                aq.id(R.id.tvfacname).text(getResources().getString(R.string.facilityname));
                //15Aug2019 -Bindu
                aq.id(R.id.txtheading).text(getResources().getString(R.string.view) + " : " + tblChildImmunization.getImmType());
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            }
        }

       /* aqPMSel.id(R.id.btnsave).getImageView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    confirmAlert(selectedService.getServiceType(), selectedService.getServiceId());
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }
            }
        });*/

        aq.id(R.id.btncancel).getImageView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    confirmAlert(null, 0);
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }
            }
        });

        alert = new AlertDialog.Builder(context, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        alert.setView(customView).setTitle("").setPositiveButton(
                "", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        alertdialog.setCancelable(false);
                        alertdialog.show();
                        try {
//                            confirmAlert(selectedService.getServiceType(), selectedService.getServiceId());
                        } catch (Exception e) {
                            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                        }
                    }
                });

        alertdialog = alert.create();
//        alertdialog.setTitle(context.getResources().getString(R.string.m123));
        alertdialog.show();
    }

    /**
     * Confirm Alert on Click save
     */
    private void confirmAlert(final String serviceType, final int serviceId) throws Exception {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        String message = getResources().getString(R.string.areyousuretoexit); //22Jul2019 -Bindu
        alertDialogBuilder.setMessage(message).setCancelable(false)
                .setNegativeButton(getResources().getString(R.string.m121),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                try {

                                    alertdialog.cancel();

                                } catch (Exception e) {


                                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                                }
                            }
                        })
                .setPositiveButton((getResources().getString(R.string.m122)),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    private void initiateDrawer() throws Exception {
        mDrawerLayout = findViewById(R.id.ett_drawer_layout);
        mDrawerList = findViewById(R.id.ett_left_drawer);

        ArrayList<NavDrawerItem> navDrawerItems = new ArrayList<>();
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.editadolescent), R.drawable.ic_edit_icon));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.homevisitadd), R.drawable.ic_homevisit));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.adolvisithistory), R.drawable.anm_pending_activities));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.trainingparticipated), R.drawable.presentation));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.mhmform), R.drawable.form_icon));
        String strAddPregnancy = "";
        if (new AdolescentRepository(databaseHelper).
                isAdolANC(currentAdolDetail.getRegAdolAadharNo(),currentAdolDetail.getAdolID()))
            strAddPregnancy = getResources().getString(R.string.viewpregancny);
        else
            strAddPregnancy = getResources().getString(R.string.addpregancny);
        navDrawerItems.add(new NavDrawerItem(strAddPregnancy, R.drawable.anc));
        womanId = new AdolescentRepository(databaseHelper).getAdolANCWomanId(currentAdolDetail.getRegAdolAadharNo(),currentAdolDetail.getAdolID());
        navDrawerItems.add(new NavDrawerItem(getResources()
                .getString(R.string.anchomevisit), R.drawable.ic_homevisit));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deactivate), R.drawable.deactivate));
        
//        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.editchildinfo), R.drawable.ic_edit_60));
//        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.addsibling), R.drawable.general_examination_baby));
//        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.viewparentdetails), R.drawable.ic_mother));
//
//        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.gcmadd), R.drawable.registration)); //15MAy2021 Bindu
//        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.gcmregisterlist), R.drawable.child_growth));
//        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.chlhvhistory), R.drawable.prenatalhomevisit));// 8Aug2021 Mani
//        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.deactivate),R.drawable.deactivate));
//        
        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        mDrawerList.setAdapter(new NavDrawerListAdapter(getApplicationContext(), navDrawerItems));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer,
                R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu(); // creates call to
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                getSupportActionBar().setDisplayShowTitleEnabled(false);
                getSupportActionBar().setDisplayShowCustomEnabled(false);

                int noOfWeeks;
                try {
                    if (currentAdolDetail != null) {
                        getSupportActionBar().setTitle((currentAdolDetail.getRegAdolName() + "\t"));
                    }
                } catch (Exception e) {


                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                try {
                    invalidateOptionsMenu(); // creates call to
                    getSupportActionBar().setDisplayShowHomeEnabled(false);
                    getSupportActionBar().setDisplayShowTitleEnabled(false);
                    getSupportActionBar().setDisplayShowCustomEnabled(true);
                    getSupportActionBar().setCustomView(R.layout.custom_actionbar_womaninfo);

                    aqImm.id(R.id.ivWomanImg).getImageView().setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            mDrawerLayout.closeDrawer(mDrawerList);
                        }
                    });

                    aqImm.id(R.id.tvWomanName).text((currentAdolDetail.getRegAdolName() + "("+ currentAdolDetail.getRegAdolGender()+")"));
                    if (currentAdolDetail.getRegAdolDoB().trim().length() > 0) {
                        aqImm.id(R.id.tvInfo1).text(
                                (getResources().getString(R.string.age) + " " +
                                        DateTimeUtil.calculateAge(currentAdolDetail.getRegAdolDoB())));
                    }
                } catch (Exception e) {


                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    /**
     * This method is to sync draw,erToggle and to replace up caret icon to menu
     * icon in Action Bar
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    /**
     * The click listener for ListView in the navigation drawer
     */
    private class DrawerItemClickListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            try {
                switch (i) {
                    case 0:
                        mDrawerLayout.closeDrawer(GravityCompat.END);
                        break;
                    case 1:
                        if (currentAdolDetail.getRegAdolDeactivateDate().length() != 0) {
                            AlertDialogUtil.displayAlertMessage(getResources().getString(R.string.adolescentdeactivated), AdolImmunizationListActivity.this);
                            mDrawerLayout.closeDrawer(GravityCompat.END);
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(AdolImmunizationListActivity.this);
                            builder.setMessage(getResources().getString(R.string.m110));
                            builder.setCancelable(false);
                            builder.setNegativeButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Intent intent = new Intent(AdolImmunizationListActivity.this, AdolVisit_New.class);
                                    intent.putExtra("globalState", prepareBundle());
                                    intent.putExtra("adolID", AdolID);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);

                                }
                            }).setPositiveButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            }).create().show();
                        }
                        break;
                    case 2:
                        AlertDialog.Builder builder = new AlertDialog.Builder(AdolImmunizationListActivity.this);
                        builder.setMessage(getResources().getString(R.string.m110));
                        builder.setCancelable(false);
                        builder.setNegativeButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent(AdolImmunizationListActivity.this, AdolVisitHistory.class);
                                intent.putExtra("globalState", prepareBundle());
                                intent.putExtra("adolID", AdolID);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);

                            }
                        }).setPositiveButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).create().show();
                        break;
                    case 3:
                        builder = new AlertDialog.Builder(AdolImmunizationListActivity.this);
                        builder.setMessage(getResources().getString(R.string.m110));
                        builder.setCancelable(false);
                        builder.setNegativeButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent(AdolImmunizationListActivity.this, AdolParticipatedTrainings.class);
                                intent.putExtra("globalState", prepareBundle());
                                intent.putExtra("adolID", AdolID);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);

                            }
                        }).setPositiveButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).create().show();
                        break;
                    case 4:
                        if (currentAdolDetail != null && currentAdolDetail.getRegAdolGender().equals("Female") && currentAdolDetail.getRegAdolisPeriod().equals("Yes")) {
                            Intent intent = new Intent(AdolImmunizationListActivity.this, List_SanNap.class);
                            builder = new AlertDialog.Builder(AdolImmunizationListActivity.this);
                            builder.setMessage(getResources().getString(R.string.m110));
                            builder.setCancelable(false);
                            builder.setNegativeButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    intent.putExtra("globalState", prepareBundle());
                                    intent.putExtra("adolID", AdolID);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);

                                }
                            }).setPositiveButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            }).create().show();
                        } else {
                            Toast.makeText(getApplicationContext(),getResources().getString(R.string.mhmfromnotavailforadol),Toast.LENGTH_LONG).show();
                        }
                        break;

                    case 5:

                        if (currentAdolDetail.getRegAdolPregnant() != null && currentAdolDetail.getRegAdolPregnant().trim().length() > 0
                                && currentAdolDetail.getRegAdolPregnant().equalsIgnoreCase("Yes")) {
                            if (new AdolescentRepository(databaseHelper)
                                    .isAdolANC(currentAdolDetail.getRegAdolAadharNo(),currentAdolDetail.getAdolID())) {
                                appState.selectedWomanId = womanId;
                                Intent intent1 = new Intent(AdolImmunizationListActivity.this, ViewProfileActivity.class);
                                intent1.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                startActivity(intent1);
                            } else
                                showPregnantOrMotherSelectionDialog();

                        } else
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.ntapplicable),
                                    Toast.LENGTH_LONG).show();
                        break;

                    case 6:
                        if (new AdolescentRepository(databaseHelper)
                                .isAdolANC(currentAdolDetail.getRegAdolAadharNo(),currentAdolDetail.getAdolID())) {
                            appState.selectedWomanId = womanId;
                            Intent hv = new Intent(AdolImmunizationListActivity.this, HomeVisit.class);
                            hv.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(hv);
                        } else
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.ntapplicable),
                                    Toast.LENGTH_LONG).show();
                        break;

                    case 7:
                        builder = new AlertDialog.Builder(AdolImmunizationListActivity.this);
                        builder.setMessage(getResources().getString(R.string.m110));
                        builder.setCancelable(false);
                        builder.setNegativeButton(getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent(AdolImmunizationListActivity.this, AdolDeactivation.class);
                                intent.putExtra("globalState", prepareBundle());
                                if (currentAdolDetail.getRegAdolDeactivateDate().length() != 0) {
                                    intent.putExtra("isView", true);
                                }
                                intent.putExtra("adolID", AdolID);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);

                            }
                        }).setPositiveButton(getResources().getString(R.string.m122), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).create().show();
                        break;

                }
            } catch (Exception e) {
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.log(e.getMessage());

            }
        }
    }


    //    pop up which captures services data
    void addServiceData(final Context context,
                        final String serviceType,
int serviceId,
                        final tblAdolReg tblChildInfo) throws Exception {

        AlertDialog.Builder alert = new AlertDialog.Builder(context, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        final View customView = LayoutInflater.from(context).inflate(R.layout.add_services_info, null);

        aq = new AQuery(customView);
        initializeScreen(aq.id(R.id.lladdservices).getView(), context);

        String strServType = serviceType;
        final String  chlId = tblChildInfo.getAdolID();
        final  int immuNumber = 0;


        if (serviceId == 48 || serviceId == 49 ||
                serviceId == 50 || serviceId == 51 || serviceId == 52 || serviceId == 53 ||
                serviceId == 54 || serviceId == 16 || serviceId == 17 ||
                serviceId == 18 || serviceId == 19 || serviceId == 20 || serviceId == 21 ||
                serviceId == 22 || serviceId == 7 || serviceId == 8 || serviceId == 9 || serviceId == 10
                || serviceId == 4 || serviceId == 5 || serviceId == 6 || serviceId == 1 || serviceId == 2
                || serviceId == 11 || serviceId == 12 || serviceId == 13) {

            strServType = serviceType;

            //04Aug2019 - Bindu - set name from strings
//            int identifier = context.getResources().getIdentifier(serviceType,
//                    "string", "com.sc.stmansi");
//            strServType = (context.getResources().getString(identifier)) + " " + service.getServiceNumber();
        }

        aq.id(R.id.txtheading).text(getResources().getString(R.string.addplannedservice) + ": " + strServType);
        aq.id(R.id.etdate).text(DateTimeUtil.getTodaysDate());
        if(tblChildInfo!=null) {
            aq.id(R.id.txtNameval).text(tblChildInfo.getRegAdolName());
            aq.id(R.id.txtlmpval).text(tblChildInfo.getRegAdolDoB());
            aq.id(R.id.tvWname).text(getResources().getString(R.string.tvchildname));
            strWomanId = tblChildInfo.getAdolID();
            aq.id(R.id.tvlmp).text(getResources().getString(R.string.dob));
            serviceId  = serviceId;
        }



        WomanServiceRepository womanServiceRepository = new
                WomanServiceRepository(databaseHelper);
        int minduration  = womanServiceRepository.getDaysforImmunization("DaysDiffStart",strServType);
        int maxduration  = womanServiceRepository.getDaysforImmunization("DaysDiffEnd",strServType);

        strMaxDate = getNextDate(tblChildInfo.getRegAdolDoB(),maxduration,tblChildInfo.getRegAdolDoB(),true);
        strMinDate = getNextDate(tblChildInfo.getRegAdolDoB(),minduration,tblChildInfo.getRegAdolDoB(),false);

        aq.id(R.id.tvdurationvalmin).text(strMinDate);

        aq.id(R.id.tvdurationvalmax).text(strMaxDate);


        aq.id(R.id.trspnfacname).gone();
        aq.id(R.id.tblservicesdetails).gone();
        aq.id(R.id.txtservicedeatils).gone();

        //15Aug2019 - Bindu
//        serviceAddedUserType = service.getServiceUserType();
//        serviceupdateddate = service.getRecordUpdatedDate();


        //20Aug2019 Arpitha
        aq.id(R.id.imgcleardate).getImageView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aq.id(R.id.etdate).text(DateTimeUtil.getTodaysDate());
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.dateresettotodaysdate), Toast.LENGTH_LONG).show();//21Aug2019 Arpitha
            }
        });

        //25Jul2019 - Bindu

        aq.id(R.id.spnfactype).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    String selected = adapterView.getItemAtPosition(i).toString();
                    if (!(selected.equalsIgnoreCase(getResources().getString(R.string.select)) || selected.equalsIgnoreCase(getResources().getString(R.string.bhome)) || selected.equalsIgnoreCase(getResources().getString(R.string.bsc)) ||
                            selected.equalsIgnoreCase(getResources().getString(R.string.bothers)))) {

//                        if (tblChildInfo == null) {
                            aq.id(R.id.trspnfacname).visible();
                            aq.id(R.id.tvfacname).text(getResources().getString(R.string.facilitynameothers));
//                        }
                        facilitynamelist = new ArrayList<>();

                        FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
                        mapFacilityname = facilityRepository.getFacilityNames(aq.id(R.id.spnfactype).getSelectedItem().toString(), appState.sessionUserId);
                        facilitynamelist.add(getResources().getString(R.string.selectfacilityname));
                        for (Map.Entry<String, String> village : mapFacilityname.entrySet())
                            facilitynamelist.add(village.getKey());
                        facilitynamelist.add(getResources().getString(R.string.other));
                        ArrayAdapter<String> FacNameAdapter = new ArrayAdapter<String>(AdolImmunizationListActivity.this,
                                R.layout.simple_spinner_dropdown_item, facilitynamelist);
                        FacNameAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        aq.id(R.id.spnfacname).adapter(FacNameAdapter);
                    } else {
                        aq.id(R.id.trspnfacname).gone();
                        aq.id(R.id.tretfacname).visible();
                        aq.id(R.id.tvfacname).text(getResources().getString(R.string.facilityname));
                    }
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        // Facility name on select listener
        aq.id(R.id.spnfacname).getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    String selected = adapterView.getItemAtPosition(i).toString();
                    if (selected.equalsIgnoreCase(getResources().getString(R.string.other))) {
                        aq.id(R.id.tretfacname).visible();
                        aq.id(R.id.etfacname).text("");
                    } else {
                        aq.id(R.id.tretfacname).gone();
                        aq.id(R.id.etfacname).text("");
                    }
                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

       /* if (service.getServiceDueDateMin() != null
                && service.getServiceDueDateMax() != null
                && service.getServiceDueDateMin().length() > 0
                && service.getServiceDueDateMax().length() > 0) {
            aqPMSel.id(R.id.tvdurationval).text(service.getServiceDueDateMin() + "   -  " + service.getServiceDueDateMax());
        }*/

       /* WomanServiceRepository womanServiceRepository = new WomanServiceRepository(databaseHelper);
        tblChildInfo = womanServiceRepository.getService(womanId, serviceId,service.getServiceNumber());
        if (tblChildInfo != null) {
            try {
                aqPMSel.id(R.id.etdate).text(tblChildInfo.getServiceActualDateofAction());
                aqPMSel.id(R.id.etcomments).text(tblChildInfo.getServiceComments());
                aqPMSel.id(R.id.etfacname).text(tblChildInfo.getServiceProvidedFacName());
                aqPMSel.id(R.id.etdate).enabled(false);
                aqPMSel.id(R.id.etfacname).enabled(false);
                aqPMSel.id(R.id.spnfactype).enabled(false);
                aqPMSel.id(R.id.etcomments).enabled(false);
                aqPMSel.id(R.id.btnsave).gone();

                aqPMSel.id(R.id.spnfactype).setSelection(Arrays.asList(getResources().getStringArray(R.array.facilitytype)).indexOf(arrVal.getServiceProvidedFacType())); //12Aug2019 - Bindu set spinner value

                //22Jul2019 - Bindu
                aqPMSel.id(R.id.txtheading).text(getResources().getString(R.string.view)
                        + " : " + arrVal.getServiceType() + " " + arrVal.getServiceNumber());
                aqPMSel.id(R.id.trspnfacname).gone(); //25Jul2019 - Bindu
                aqPMSel.id(R.id.tvfacname).text(getResources().getString(R.string.facilityname));

            } catch (Exception e) {

                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            }
        }*/


        final int finalServiceId = serviceId;
        aq.id(R.id.btnsave).getImageView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (validateServiceFields())
                        confirmAlert(serviceType, finalServiceId, chlId,immuNumber);
                } catch (Exception e) {


                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }

            }
        });

        aq.id(R.id.btncancel).getImageView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    confirmAlert(null, 0,null,0);
                } catch (Exception e) {


                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                }

            }
        });

        aq.id(R.id.etdate).getEditText().setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEvent.ACTION_UP == event.getAction()) {
                    DialogFragment newFragment = new DatePickerFragment(aq);
                    newFragment.show(getSupportFragmentManager(), "datePicker");

                }
                return true;
            }
        });

        alert.setView(customView).setTitle("").setPositiveButton(
                "", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        alertdialog.setCancelable(false);
                        alertdialog.show();
                        try {
                            confirmAlert(serviceType, finalServiceId,null,0);
                        } catch (Exception e) {

                            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                        }
                    }
                });

        alertdialog = alert.create();
        alertdialog.show();

        alert.setCancelable(false);

        alertdialog.setTitle(context.getResources().getString(R.string.ok));

        alert.setCancelable(true);
        alertdialog.show();
        alertdialog.setCancelable(true);
    }

    //    update captured data to database
    private void addServiceData(String serviceType, int serviceId,String chlId, int serviceNumber) {
        try {
            final TblChildImmunization tblChildImmunization = new TblChildImmunization();
            final TransactionHeaderRepository transRepo = new TransactionHeaderRepository(databaseHelper);
            final int transId = transRepo.iCreateNewTrans(appState.sessionUserId,databaseHelper);

            tblChildImmunization.setImmActualDateofAction(aq.id(R.id.etdate).getText().toString());
            tblChildImmunization.setImmChildId(chlId);
            tblChildImmunization.setImmComments(aq.id(R.id.etcomments).getText().toString());
            String[] delTypeby = getResources().getStringArray(R.array.facilitytypesave);
            String posofDeltype = "";
            posofDeltype = delTypeby[aq.id(R.id.spnfactype).getSelectedItemPosition()];

            tblChildImmunization.setImmFacType(posofDeltype);
            tblChildImmunization.setImmMaxDate(strMaxDate);
            tblChildImmunization.setImmMinDate(strMinDate);

            if(serviceType.contains("VitaminAsyrup") || serviceType.contains("Measles") ) {
                String sNum  =  serviceType.substring(serviceType.length() - 1);
                if(sNum!=null && sNum.trim().length()>0)
                    serviceNumber = Integer.parseInt(sNum);
            }
            else
                serviceNumber = 1;
            tblChildImmunization.setImmNumber(serviceNumber);
            tblChildImmunization.setImmId(serviceId);
            tblChildImmunization.setImmType(serviceType);
            tblChildImmunization.setImmUserType(appState.userType);
            tblChildImmunization.setUserId(appState.sessionUserId);
            tblChildImmunization.setRecordCreatedDate(DateTimeUtil.getTodaysDate()+" "+ DateTimeUtil.getCurrentTime());
            tblChildImmunization.setRecordUpdatedDate(DateTimeUtil.getTodaysDate() + " " + DateTimeUtil.getCurrentTime());//21May2021 Arpitha

            tblChildImmunization.setTransId(transId);
            tblChildImmunization.setWomenId(strWomanId);




            String sel = "";
            if (aq.id(R.id.spnfacname).getSelectedItem() != null)
                sel = aq.id(R.id.spnfacname).getSelectedItem().toString();


            if (sel.equalsIgnoreCase(getResources().getString(R.string.select)))
                sel = getResources().getString(R.string.strselect);
            else if (sel.equalsIgnoreCase(getResources().getString(R.string.other)))
                sel = getResources().getString(R.string.strother);

            if (sel.trim().length() > 0 && !(sel.equalsIgnoreCase("select")
                    || sel.equalsIgnoreCase("other")))
                tblChildImmunization.setImmProvidedFacName(sel);
            else if (sel.equalsIgnoreCase("other")
                    && aq.id(R.id.etfacname).getText().toString().trim().length() <= 0)
                tblChildImmunization.setImmProvidedFacName("Other");
            else
                tblChildImmunization.setImmProvidedFacName(aq.id(R.id.etfacname).getText().toString());


            TransactionManager.callInTransaction(databaseHelper.getConnectionSource(), new Callable<Void>() {
                public Void call() throws Exception {//29Aug2019 Arpitha - transaction manager

                    ImmunizationRepository immunizationRepository = new ImmunizationRepository(databaseHelper);
                 int add =    immunizationRepository.addImmunization(databaseHelper,tblChildImmunization);
                   if(add>0) {
                   boolean added =     transRepo.iNewRecordTrans(appState.sessionUserId, transId, "tblchildimmunization", databaseHelper);

                   if(added) {
                       Toast.makeText(AdolImmunizationListActivity.this, "Service Details Updated Successfully!!", Toast.LENGTH_LONG).show();
                       alertdialog.cancel();
                   }else
                       Toast.makeText(getApplicationContext(), getResources().getString(R.string.serviceupdatefailed), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha

                   }else
                       Toast.makeText(getApplicationContext(), getResources().getString(R.string.serviceupdatefailed), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha

                    return null;
                }
            });
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.serviceupdatefailed), Toast.LENGTH_LONG).show();//30Aug2019 Arpitha

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    /**
     * This is a Recursive method that traverse through the group and subgroup
     * of view Sets text, assigns clickListners
     */
    private ArrayList<View> initializeScreen(View v, Context context) {

        if (!(v instanceof ViewGroup)) {
            ArrayList<View> viewArrayList = new ArrayList<>();
            viewArrayList.add(v);

            // TextView - Assign Labels
            if (v.getClass() == TextView.class || v.getClass() == RadioButton.class) {
            }

            if (v.getClass() == RadioButton.class) {
                aq.id(v.getId()).clicked(context, "commonClick");
            }

            return viewArrayList;
        }

        if (v instanceof Spinner) {
        }

        ArrayList<View> result = new ArrayList<View>();

        ViewGroup viewGroup = (ViewGroup) v;
        for (int i = 0; i < viewGroup.getChildCount(); i++) {

            View child = viewGroup.getChildAt(i);

            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);
            viewArrayList.addAll(initializeScreen(child, context));

            result.addAll(viewArrayList);
        }
        return result;
    }



    boolean validateServiceFields() {
//         if (aqChildImmunization.id(R.id.spnimmu).getSelectedItemPosition() > 1) {
//            Toast.makeText(getApplicationContext(), getResources().getString(R.string.selectservtype), Toast.LENGTH_LONG).show();
//            return false;
//        }
//       else
        if (aq.id(R.id.etdate).getText().toString().trim().length() <= 0) {
            Toast.makeText(getApplicationContext(), "Enter Service Completed Date", Toast.LENGTH_LONG).show();
            return false;
        } else if (aq.id(R.id.spnfactype).getSelectedItemPosition() == 0) {
            Toast.makeText(getApplicationContext(), "Select Service Provided Facility Type", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    //   get Next date
    public String getNextDate(String srcDate, int interval, String td, boolean isMaxDt)
            throws Exception {


        SimpleDateFormat xSimpleDate = new SimpleDateFormat("dd-MM-yyyy");
        String nxtDt = null;

        if (srcDate!=null && srcDate.length() != 0) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(xSimpleDate.parse(srcDate));
            cal.add(Calendar.DATE, interval);
            Date xDt = cal.getTime();

            Date eddDt = xSimpleDate.parse(td);
            int i = xDt.compareTo(eddDt);
            nxtDt = xSimpleDate.format(xDt);
            return nxtDt;
        }
        return nxtDt;
    }

    /**
     * Confirm Alert on Click save
     */
    private void confirmAlert(final String serviceType, final int serviceId,
                              final String chlId, final int immuNumber) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        String message;
        if (serviceType != null || serviceId != 0)
            message = getResources().getString(R.string.m099);
        else {
//            if (arrVal != null)
//                message = getResources().getString(R.string.areyousuretoexit);
//            else
            message = getResources().getString(R.string.m110);
        }

        alertDialogBuilder.setMessage(message).setCancelable(false)
                .setNegativeButton(getResources().getString(R.string.m121),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                try {
                                    if (serviceType != null || serviceId != 0) {

                                        addServiceData(serviceType,serviceId,chlId, immuNumber);
                                        getData();                                    } else
                                        alertdialog.cancel();

                                } catch (Exception e) {



                                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                                }
                            }
                        })
                .setPositiveButton((getResources().getString(R.string.m122)),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }



    @SuppressLint("SimpleDateFormat")
    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        private AQuery aqPMSel;
        private tblregisteredwomen woman;

        public DatePickerFragment() {
        }

        public DatePickerFragment(AQuery aQuery) {
            aqPMSel = aQuery;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            int year = 0, month = 0, day = 0;
            try {

                Calendar c = Calendar.getInstance();
                String defaultDate = DateTimeUtil.getTodaysDate(); //09Aug2019 - Cal set date of Provided date
                if (defaultDate.length() > 0) {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    Date sendDate = sdf.parse(defaultDate);
                    c.setTime(sendDate);
                    year = c.get(Calendar.YEAR);
                    month = c.get(Calendar.MONTH);
                    day = c.get(Calendar.DAY_OF_MONTH);
                } else {
                    c.setTime(new Date());
                    year = c.get(Calendar.YEAR);
                    month = c.get(Calendar.MONTH);
                    day = c.get(Calendar.DAY_OF_MONTH);
                }
            } catch (Exception e) {


                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            }

            // Create a new instance of DatePickerDialog and return it
            DatePickerDialog dpDialog;
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                // Do something for lollipop and above versions
                dpDialog = new DatePickerDialog(getActivity(), R.style.DialogTheme, this, year, month, day);
            } else {
                // do something for phones running an SDK before lollipop
                dpDialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, this, year, month, day);
                dpDialog.setTitle("Set a Date");
                dpDialog.getDatePicker().setCalendarViewShown(false);
            }
            return dpDialog;
        }

        @Override
        public void onDateSet(DatePicker view, int year, int month, int day) {
            if (view.isShown()) {
                try {
                    String SelectedDate = String.format("%02d", day) + "-" +
                            String.format("%02d", month + 1) + "-"
                            + year;
//                    Date lmpDate = null;
                    SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                    Date seldate = format.parse(SelectedDate);
                    Date mindate = null;


                    if(strMinDate!=null && strMinDate.trim().length()>0)
                        mindate = format.parse(strMinDate);

                    if(seldate.after(new Date())) {
                        Toast.makeText(getActivity(), getResources().getString(R.string.date_cannot_af_currentdate), Toast.LENGTH_LONG).show();
                        aqPMSel.id(R.id.etdate).text(DateTimeUtil.getTodaysDate());
                    }
                    else if(mindate!=null && seldate.before(mindate))
                    {
                        Toast.makeText(getActivity(), getResources().getString(R.string.immunizationtapplibeforemindate), Toast.LENGTH_LONG).show();
                        aqPMSel.id(R.id.etdate).text(DateTimeUtil.getTodaysDate());
                    }
                    else {
                        aqPMSel.id(R.id.etdate).text(SelectedDate);
                    }


//                    if(strFromDate!=null && strFromDate.trim().length()>0)
//                        fromdate = format.parse(strFromDate);
//
                  /*  if(fromDate)
                    {
                        if(seldate.after(new Date())) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.date_cannot_af_currentdate), Toast.LENGTH_LONG).show();
                            strFromDate = "";
                            aqPMSel.id(R.id.txtfromdate).text(getResources().getString(R.string.fromdate));
                        }else {
                            strFromDate = SelectedDate;
                            aqPMSel.id(R.id.txtfromdate).text(strFromDate);
                        }
                    }else {

                        if(seldate.after(new Date())) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.date_cannot_af_currentdate), Toast.LENGTH_LONG).show();
                            strToDate = "";
                            aqPMSel.id(R.id.txttodate).text(getResources().getString(R.string.todate));
                        }else if(fromdate!=null && seldate.before(fromdate)) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.date_cannot_af_currentdate), Toast.LENGTH_LONG).show();
                            strToDate = "";
                            aqPMSel.id(R.id.txttodate).text(getResources().getString(R.string.todate));
                        }
                        else {
                            strToDate = SelectedDate;
                            aqPMSel.id(R.id.txttodate).text(strToDate);
                        }
                    }*/

                } catch (Exception e) {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

                }
            }
        }
    }
    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        b.putParcelable("syncState", syncState);
        return b;
    }
    private void showPregnantOrMotherSelectionDialog() {
        try {
            final AlertDialog.Builder alert = new AlertDialog.Builder(activity,
                    AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
            final View customView = LayoutInflater.from(this).inflate(R.layout.pregnant_mother_selection,
                    null);

            aqPMSel = new AQuery(customView);
            radGrpEDDorLmp = customView.findViewById(R.id.radEDDorLmp);

            initializeScreen(aqPMSel.id(R.id.llPregOrMothMainLayout).getView());
            setInitialView();

            aqPMSel.id(R.id.llmother).gone();
            aqPMSel.id(R.id.llchild).gone();
            aqPMSel.id(R.id.llAdol).gone();


            aqPMSel.id(R.id.imgclearedddate).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    aqPMSel.id(R.id.etlmpdate).text("");
                }
            });
            aqPMSel.id(R.id.imgclearlmp).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    aqPMSel.id(R.id.etMotherAdd).text("");
                    aqPMSel.id(R.id.etlmpdate).text("");
                    aqPMSel.id(R.id.etgest).text("");
                    aqPMSel.id(R.id.imgclearlmp).gone();
                }
            });
            aqPMSel.id(R.id.imgclearregadate).getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());
                    Toast.makeText(activity, getResources().getString(R.string.dateresettotodaysdate), Toast.LENGTH_LONG).show();//21Aug2019 Arpitha
                }
            });//   Arpitha - 20Aug2019

//            aqPMSel.id(R.id.etMotherAdd).text("20-05-2021");
            aqPMSel.id(R.id.eteddmother).enabled(false);

            aqPMSel.id(R.id.etMotherAdd).getEditText().setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    try {
                        if (MotionEvent.ACTION_UP == event.getAction()) {
                            isRegDate = false;
                            assignEditTextAndCallDatePicker(aqPMSel.id(R.id.etMotherAdd).getEditText());
                        }
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());

                    }
                    return true;
                }
            });

            aqPMSel.id(R.id.etregdate).getEditText().setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    try {
                        if (MotionEvent.ACTION_UP == event.getAction()) {
                            isRegDate = true;
                            assignEditTextAndCallDatePicker(aqPMSel.id(R.id.etregdate).getEditText());
                        }
                    } catch (Exception e) {

                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }
                    return true;
                }
            });

            aqPMSel.id(R.id.radLMP).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        setLMP();
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }
                }
            });

            aqPMSel.id(R.id.radEDD).getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        setEDD();
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }
                }
            });

            alert.setView(customView);

            alert.setCancelable(true)
                    .setTitle(getResources().getString(R.string.registrationHeading))
                    .setPositiveButton(getResources().getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {

                                }
                            })
                    .setNegativeButton(getResources().getString(R.string.cancel), //01Oct2019 - Bindu - Add cancel button
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    dialog.cancel();
                                }
                            });
            final AlertDialog dialog = alert.create();
            if (dialog != null)
                dialog.show();

            // Overriding the handler immediately after show is probably a
            // better
            // approach than OnShowListener as described below
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        validateAndRegister();
                        if (wantToCloseDialog)
                            dialog.dismiss();
                    } catch (Exception e) {
                        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                        crashlytics.log(e.getMessage());
                    }

                }
            });
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }


    //calculate gestation based on LMP
    private void calculateEddGestation(String lmp) {
        try {

            if (lmp.equalsIgnoreCase("lmp")) {
                if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0
                        && aqPMSel.id(R.id.etMotherAdd).getText().length() > 0) {

                    noOfDays = DateTimeUtil
                            .getNumberOfDaysFromLMP(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                    int noOfWeeks = noOfDays / 7;
                    int days = noOfDays % 7;

                    String strdays = "";

                    if (days > 1)
                        strdays = days + " " + activity.getResources().getString(R.string.days);
                    else if (days == 1)
                        strdays = days + " " + activity.getResources().getString(R.string.day);

                    aqPMSel.id(R.id.etgest)
                            .text(noOfWeeks + " " + activity.getResources().getString(R.string.weeks) + " " + strdays);
                }

                String edd = populateEDD(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                aqPMSel.id(R.id.etlmpdate).text(edd);

            } else {

                String strLmp = CalculateLMP(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                aqPMSel.id(R.id.etlmpdate).text(strLmp);

                if (aqPMSel.id(R.id.etlmpdate).getText().length() > 0
                        && aqPMSel.id(R.id.etlmpdate).getText().length() > 0) {

                    noOfDays = DateTimeUtil
                            .getNumberOfDaysFromLMP(aqPMSel.id(R.id.etlmpdate).getText().toString());
                    int noOfWeeks = noOfDays / 7;
                    int days = noOfDays % 7;

                    String strdays = "";

                    if (days > 1)
                        strdays = days + " " + activity.getResources().getString(R.string.days);
                    else if (days == 1)
                        strdays = days + " " + activity.getResources().getString(R.string.day);

                    aqPMSel.id(R.id.etgest)
                            .text(noOfWeeks + " " + activity.getResources().getString(R.string.weeks) + " " + strdays);
                }

            }
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }

    }

    // Calculate EDD and display in EDD Edit text
    private String populateEDD(String xLMP) throws Exception {
        String stredddate = "";
        try {

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());

            Date LmpDate;
            LmpDate = sdf.parse(xLMP);
            Calendar cal = Calendar.getInstance();
            cal.setTime(LmpDate);
            cal.add(Calendar.DAY_OF_MONTH, 280);

            int day = cal.get(Calendar.DAY_OF_MONTH);
            int month = cal.get(Calendar.MONTH) + 1;
            int year = cal.get(Calendar.YEAR);

            stredddate =
                    String.format("%02d", day) + "-" + String.format("%02d", month) + "-" + String.format("%02d", year);

        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        return stredddate;
    }

    //	calculate LMP based on EDD
    private String CalculateLMP(String EDDate) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        Date LMP;
        LMP = sdf.parse(EDDate);
        Calendar cal = Calendar.getInstance();
        cal.setTime(LMP);
        cal.add(Calendar.DAY_OF_MONTH, -280);

        int day = cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH) + 1;
        int year = cal.get(Calendar.YEAR);

        String lmpDate = String.format("%02d", day) + "-" + String.format("%02d", month) + "-" + year;
        return lmpDate;
    }

    /**
     * This method opens the Date Dialog and sets Date to respective EditText
     */
  /*  private void assignEditTextAndCallDatePicker(EditText editText) {
        String currDate = (editText.getText().length() > 0) ? editText.getText().toString() : "";
        CommonDatePickerFragment.getCommonDate(activity, currDate);
    }*/

  /*  @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        try {
            aqPMSel.id(R.id.imgclearlmp).background(R.drawable.brush1);
            if (view.isShown()) {
                String str = String.format("%02d", day) + "-" + String.format("%02d", month + 1) + "-"
                        + year;

                int days = DateTimeUtil.getDaysBetweenDates(DateTimeUtil.getTodaysDate(), str);

                if (!aqPMSel.id(R.id.radEDD).isChecked()) {
                    if (days >= 0) {
                        if (isRegDate) {
                            if (days < 30) {
                                aqPMSel.id(R.id.etregdate).text(str);
                                aqPMSel.id(R.id.imgclearregadate).visible();//20AUg2019 Arpitha
                            } else if (days > 120) {
                                aqPMSel.id(R.id.etregdate).text("");
                                aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                                Toast.makeText(activity, getResources().getString(R.string.date_val_2_toast),
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha

                            } else {
                                aqPMSel.id(R.id.etregdate).text(str);
                                aqPMSel.id(R.id.imgclearregadate).visible();//20AUg2019 Arpitha
                            }

                        } else if (aqPMSel.id(R.id.radLMP).isChecked()) {
                            String lmporadd = str;
                            int daysdiff = 0;
                            String regdate = aqPMSel.id(R.id.etregdate).getText().toString();

                            if (regdate != null && regdate.trim().length() > 0) {
                                daysdiff = DateTimeUtil.getDaysBetweenDates(regdate, lmporadd);

                                int daysdifffromCUrr = DateTimeUtil
                                        .getDaysBetweenDates(DateTimeUtil.getTodaysDate(), lmporadd);

                                if (daysdiff <= 30) {
                                    Toast.makeText(activity,
                                            getResources().getString(R.string.date_val_3_toast),
                                            Toast.LENGTH_LONG).show();
                                    aqPMSel.id(R.id.etlmpdate).text("");
                                    aqPMSel.id(R.id.etgest).text("");
                                    aqPMSel.id(R.id.etMotherAdd).text("");
                                    aqPMSel.id(R.id.imgclearlmp).gone();//20Aug2019 Arpitha
                                } else if (daysdifffromCUrr > 280) {
                                    Toast.makeText(activity,
                                            getResources().getString(R.string.date_val_4_toast),
                                            Toast.LENGTH_LONG).show();
                                    aqPMSel.id(R.id.etlmpdate).text("");
                                    aqPMSel.id(R.id.etgest).text("");
                                    aqPMSel.id(R.id.etMotherAdd).text("");
                                    aqPMSel.id(R.id.imgclearlmp).gone();//20Aug2019 Arpitha
                                } else {
                                    aqPMSel.id(R.id.etMotherAdd).text(str);
                                    calculateEddGestation("lmp");
                                    aqPMSel.id(R.id.imgclearlmp).visible();//20Aug2019 Arpitha
                                }
                            } else
                                Toast.makeText(activity, getResources().getString(R.string.plz_enter_reg_dt),
                                        Toast.LENGTH_LONG).show();
                        } else if (aqPMSel.id(R.id.rdbMother).isChecked()) {

                            int daysdiff = DateTimeUtil
                                    .getDaysBetweenDates(aqPMSel.id(R.id.etregdate).getText().toString(), str);
                            if (daysdiff > 365) {// Arpitha 28Jun2018 - 23Dec2019 Arpitha - allow max 1 year

                                Toast.makeText(activity, (getResources().getString(R.string.datecannotbeafteroneyearfromcurrentdate)), // 11July2018
                                        // Arpitha
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etMotherAdd).text("");
                            } else
                                aqPMSel.id(R.id.etMotherAdd).text(str);
                        } else if (aqPMSel.id(R.id.rdbchild).isChecked()) {

                            int daysdiff = DateTimeUtil
                                    .getDaysBetweenDates(aqPMSel.id(R.id.etregdate).getText().toString(), str);
                            if (daysdiff > 5844) {// Arpitha 28Jun2018

                                Toast.makeText(activity, (getResources().getString(R.string.date_val_7_toast)), // 11July2018
                                        // Arpitha
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etMotherAdd).text("");
                            } else
                                aqPMSel.id(R.id.etMotherAdd).text(str);
                        }
                    } else {
                        if (isRegDate) {
                            aqPMSel.id(R.id.etregdate).text("");
                            aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                            Toast.makeText(activity,
                                    getResources().getString(R.string.reg_date_cannot_greater_current_date),
                                    Toast.LENGTH_LONG).show();
                            aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha
                        } else {
                            aqPMSel.id(R.id.etMotherAdd).text("");
                            aqPMSel.id(R.id.imgclearlmp).gone();//20AUg2019 Arpitha
                            aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                            if (aqPMSel.id(R.id.rdbPreg).isChecked()) {
                                Toast.makeText(activity,
                                        getResources()
                                                .getString(R.string.lmp_date_time_cannot_be_after_curre_datetime), // 11July2018

                                        Toast.LENGTH_LONG).show();

                                aqPMSel.id(R.id.etlmpdate).text("");
                                aqPMSel.id(R.id.etgest).text("");
                            } else {
                                Toast.makeText(activity,
                                        getResources()
                                                .getString(R.string.del_date_cannot_be_after_current_date_time), // 11July2018
                                        Toast.LENGTH_LONG).show();
                            }

                        }
                    }
                } else {
                    if (!isRegDate) {
                        int days1 = 0;
                        if (aqPMSel.id(R.id.etregdate).getText().toString().trim().length() > 0)
                            days1 = DateTimeUtil.getDaysBetweenDates(str,
                                    aqPMSel.id(R.id.etregdate).getText().toString());

                        if (days1 > 0 && days1 < 273) {
                            aqPMSel.id(R.id.etMotherAdd).text(str);
                            calculateEddGestation("edd");
                            aqPMSel.id(R.id.imgclearlmp).visible();//20Aug2019 Arpitha
                        } else {
                            if (days1 > 280) {
                                aqPMSel.id(R.id.etMotherAdd).text("");
                                aqPMSel.id(R.id.imgclearlmp).gone();//21Aug2019 Arpitha
                                aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                                Toast.makeText(activity, getResources().getString(R.string.date_val_6_toast),

                                        Toast.LENGTH_LONG).show();

                                aqPMSel.id(R.id.etlmpdate).text("");
                                aqPMSel.id(R.id.etgest).text("");

                            } else {
                                aqPMSel.id(R.id.etMotherAdd).text("");
                                aqPMSel.id(R.id.imgclearlmp).gone();//21Aug2019 Arpitha
                                aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                                Toast.makeText(activity, getResources().getString(R.string.date_val_5_toast),
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etlmpdate).text("");
                                aqPMSel.id(R.id.etgest).text("");
                            }
                        }
                    } else {
                        days = DateTimeUtil.getDaysBetweenDates(DateTimeUtil.getTodaysDate(), str);
                        if (days >= 0) {

                            if (days > 90) {
                                aqPMSel.id(R.id.etregdate).text("");
                                aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                                Toast.makeText(activity, getResources().getString(R.string.date_val_2_toast),

                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha
                            } else {
                                aqPMSel.id(R.id.etregdate).text(str);
                            }

                        } else {
                            aqPMSel.id(R.id.etregdate).text("");
                            aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                            Toast.makeText(activity, getResources().getString(R.string.reg_date_cannot_greater_current_date),

                                    Toast.LENGTH_LONG).show();
                            aqPMSel.id(R.id.imgclearregadate).gone();//20AUg2019 Arpitha
                        }
                    }
                }

            }

        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }*/

    /**
     * This is a Recursive method that traverse through the group and subgroup
     * of view Sets text, assigns clickListners
     *
     * @throws Exception
     */
    private ArrayList<View> initializeScreen(View v) throws Exception {
        if (!(v instanceof ViewGroup)) {
            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);

            // TextView - Assign Labels
            if (v.getClass() == TextView.class || v.getClass() == RadioButton.class) {
                // aqPMSel.id(v.getId()).text(getSS(aqPMSel.id(v.getId()).getText().toString()));
            }

            if (v.getClass() == RadioButton.class) {
                aqPMSel.id(v.getId()).clicked(this, "commonClick");
            }

            return viewArrayList;
        }

        ArrayList<View> result = new ArrayList<View>();

        ViewGroup viewGroup = (ViewGroup) v;
        for (int i = 0; i < viewGroup.getChildCount(); i++) {

            View child = viewGroup.getChildAt(i);

            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);
            viewArrayList.addAll(initializeScreen(child));

            result.addAll(viewArrayList);
        }
        return result;
    }


    //	set initial view of the pregnant or mother pop up
    private void setInitialView() {
        aqPMSel.id(R.id.rdbPreg).checked(true);
        aqPMSel.id(R.id.tvMotherAddHeading).text(getResources().getString(R.string.tvPregLmpHeading));
        aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());
        aqPMSel.id(R.id.radLMP).checked(true);
        aqPMSel.id(R.id.etregdate).getEditText().setKeyListener(null);
        aqPMSel.id(R.id.etMotherAdd).getEditText().setKeyListener(null);
        aqPMSel.id(R.id.lllmpdate).visible();
        aqPMSel.id(R.id.llgest).visible();
        aqPMSel.id(R.id.vHLine4).visible();
        aqPMSel.id(R.id.vHLine6).visible();
        aqPMSel.id(R.id.llnote).visible();

        //13Apr2021 Bindu - set here cos not translated from xml
        aqPMSel.id(R.id.anclbl).text(getResources().getString(R.string.anclbl));
        aqPMSel.id(R.id.txtpnclbl).text(getResources().getString(R.string.pnclbl));
        aqPMSel.id(R.id.lmp_date).text(getResources().getString(R.string.tvedd));
        aqPMSel.id(R.id.txtgest).text(getResources().getString(R.string.tvGestationalageL));
        aqPMSel.id(R.id.txtchildreglbl).text(getResources().getString(R.string.childreg));
        aqPMSel.id(R.id.tvFirstPregHeading).text(getResources().getString(R.string.tvFirstPregHeading));
        aqPMSel.id(R.id.reg_date).text(getResources().getString(R.string.reg_date));
        aqPMSel.id(R.id.instructions).text(getResources().getString(R.string.instruction));
        aqPMSel.id(R.id.regdt).text(getResources().getString(R.string.reg_date));
        aqPMSel.id(R.id.txt1).text(getResources().getString(R.string.date_val_0));
        aqPMSel.id(R.id.txt2).text(getResources().getString(R.string.date_val_1));
        aqPMSel.id(R.id.txt3).text(getResources().getString(R.string.date_val_2));
        aqPMSel.id(R.id.txtlmp).text(getResources().getString(R.string.tvPregLmpHeading));
        aqPMSel.id(R.id.txt4).text(getResources().getString(R.string.date_val_3));
        aqPMSel.id(R.id.txt5).text(getResources().getString(R.string.date_val_4));
        aqPMSel.id(R.id.txt6).text(getResources().getString(R.string.date_val_5));
        aqPMSel.id(R.id.txt7).text(getResources().getString(R.string.date_val_6));
        aqPMSel.id(R.id.txt8).text(getResources().getString(R.string.date_val_7));
        aqPMSel.id(R.id.radLMP).text(getResources().getString(R.string.tvlmp));
        aqPMSel.id(R.id.radEDD).text(getResources().getString(R.string.tvedd));
//22May2021 Bindu set adol reg lbl
        aqPMSel.id(R.id.adolreg).text(getResources().getString(R.string.adolescentreg));

    }

    // capture EDD
    private void setEDD() throws Exception {
        aqPMSel.id(R.id.tvMotherAddHeading).text(getResources().getString(R.string.tvedd));
        aqPMSel.id(R.id.etMotherAdd).text("");
        aqPMSel.id(R.id.imgclearlmp).gone();//20AUg2019 Arpitha
        aqPMSel.id(R.id.lmp_date).text(getResources().getString(R.string.tvlmp));
        aqPMSel.id(R.id.etlmpdate).text("");
        aqPMSel.id(R.id.etgest).text("");
        aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());
    }

    // capture LMP
    private void setLMP() throws Exception {
        aqPMSel.id(R.id.tvMotherAddHeading).text(getResources().getString(R.string.tvlmp));
        aqPMSel.id(R.id.etMotherAdd).text("");
        aqPMSel.id(R.id.imgclearlmp).gone();//20AUg2019 Arpitha
        aqPMSel.id(R.id.lmp_date).text(getResources().getString(R.string.tvedd));
        aqPMSel.id(R.id.etlmpdate).text("");
        aqPMSel.id(R.id.etgest).text("");
    }


    /**
     * Common method invokes when any button clicked This method handles
     * Edittext, save clicks. Validate mandatory fields
     */
    public void commonClick(View v) throws Exception {
        switch (v.getId()) {
            case R.id.rdbPreg: {
                aqPMSel.id(R.id.rdbMother).checked(false);
                aqPMSel.id(R.id.tvFirstPregHeading).enabled(true);
                aqPMSel.id(R.id.rdbchild).checked(false);
                aqPMSel.id(R.id.rdbAdol).checked(false);//Ramesh 13-may2021
                aqPMSel.id(R.id.chkFirstPreg).enabled(true);
                aqPMSel.id(R.id.radEDDorLmp).visible();
                //27Nov2018 - Bindu - radiogrp clear chk to reset radiobtn
                aqPMSel.id(R.id.vHLine5).visible();
                aqPMSel.id(R.id.etMotherAdd).text("");
                aqPMSel.id(R.id.radLMP).checked(true);
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.tvPregLmpHeading)));
                // 27Jun2018 Arpitha
                aqPMSel.id(R.id.etregdate).enabled(true);
                aqPMSel.id(R.id.etregdate).textColor(getResources().getColor(R.color.black));
//				aqPMSel.id(R.id.llregdate).background(R.drawable.editext_none);
                aqPMSel.id(R.id.lllmpdate).visible();
                aqPMSel.id(R.id.llgest).visible();
                aqPMSel.id(R.id.vHLine4).visible();
                aqPMSel.id(R.id.vHLine6).visible();

                aqPMSel.id(R.id.llMotherAdd).visible();
                aqPMSel.id(R.id.llFirstPreg).visible();
                aqPMSel.id(R.id.llnote).visible();
                aqPMSel.id(R.id.vHLine5).visible();
//				19Nov2019 Arpitha
                aqPMSel.id(R.id.llancinstruction).visible();
                aqPMSel.id(R.id.llpncinstruction).gone();

                break;
            }
            case R.id.rdbMother: {
                aqPMSel.id(R.id.rdbPreg).checked(false);
                aqPMSel.id(R.id.rdbchild).checked(false);
                aqPMSel.id(R.id.rdbAdol).checked(false);//Ramesh 13-may2021
                //27Nov2018 - Bindu radiogrp clear chk to reset radiobtn
                radGrpEDDorLmp.clearCheck();
                aqPMSel.id(R.id.radLMP).checked(false);
                aqPMSel.id(R.id.radEDD).checked(false);
                aqPMSel.id(R.id.tvFirstPregHeading).enabled(false);
                aqPMSel.id(R.id.chkFirstPreg).enabled(false);
                aqPMSel.id(R.id.chkFirstPreg).checked(false);
                aqPMSel.id(R.id.radEDDorLmp).gone();
                aqPMSel.id(R.id.vHLine5).gone();
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.deldate)));
                aqPMSel.id(R.id.etMotherAdd).text("");
                // 27Jun2018 Arpitha
                aqPMSel.id(R.id.etregdate).enabled(false);
                aqPMSel.id(R.id.etregdate).textColor(R.color.fontcolor_disable);
                //	aqPMSel.id(R.id.llregdate).background(R.drawable.edittext_disable);
                aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());//21March2019 Arpitha
                aqPMSel.id(R.id.lllmpdate).gone();
                aqPMSel.id(R.id.llgest).gone();
                aqPMSel.id(R.id.vHLine4).gone();
                aqPMSel.id(R.id.vHLine6).gone();

                aqPMSel.id(R.id.llMotherAdd).visible();
                aqPMSel.id(R.id.llFirstPreg).visible();
                aqPMSel.id(R.id.llnote).visible();
                aqPMSel.id(R.id.vHLine5).visible();
                //				19Nov2019 Arpitha
                aqPMSel.id(R.id.llancinstruction).gone();
                aqPMSel.id(R.id.llpncinstruction).visible();
                aqPMSel.id(R.id.llFirstPreg).gone();
                aqPMSel.id(R.id.imgclearlmp).visible();
                break;
            }
            case R.id.radEDD: {

                //	aqPMSel.id(R.id.radEDD).checked(true); //27Nov2018 - Bindu- comment the line
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.deldate)));
                aqPMSel.id(R.id.etMotherAdd).text("");
                aqPMSel.id(R.id.lmp_date).text((getResources().getString(R.string.tvlmp)));// 28Jun2018
                // Arpitha
                // 11July2018 Arpitha
                aqPMSel.id(R.id.etlmpdate).text("");
                aqPMSel.id(R.id.etgest).text("");
                break;
            }
            case R.id.rdbchild: {
                aqPMSel.id(R.id.rdbPreg).checked(false);
                aqPMSel.id(R.id.rdbMother).checked(false);
                aqPMSel.id(R.id.rdbAdol).checked(false);//Ramesh 13-may2021

                //27Nov2018 - Bindu radiogrp clear chk to reset radiobtn
                radGrpEDDorLmp.clearCheck();//Arpitha2021
                aqPMSel.id(R.id.radLMP).checked(false);
                aqPMSel.id(R.id.radEDD).checked(false);
                aqPMSel.id(R.id.tvFirstPregHeading).enabled(false);
                aqPMSel.id(R.id.chkFirstPreg).enabled(false).checked(false);
                aqPMSel.id(R.id.radEDDorLmp).gone();
                aqPMSel.id(R.id.vHLine5).gone();
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.deldate)));
                aqPMSel.id(R.id.etMotherAdd).text("");
                aqPMSel.id(R.id.llMotherAdd).gone();
                aqPMSel.id(R.id.llFirstPreg).gone();
                aqPMSel.id(R.id.llnote).gone();
                // 27Jun2018 Arpitha
                aqPMSel.id(R.id.etregdate).enabled(false);
                aqPMSel.id(R.id.etregdate).textColor(R.color.fontcolor_disable);
                //	aqPMSel.id(R.id.llregdate).background(R.drawable.edittext_disable);
                aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());//21March2019 Arpitha
                aqPMSel.id(R.id.lllmpdate).gone();
                aqPMSel.id(R.id.llgest).gone();
                aqPMSel.id(R.id.vHLine4).gone();
                aqPMSel.id(R.id.vHLine6).gone();
                break;
            }
            case R.id.radLMP: {
                //	aqPMSel.id(R.id.radLMP).checked(true); //27Nov2018 - Bindu- comment the line
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.tvlmp)));
                aqPMSel.id(R.id.etMotherAdd).text("");
                aqPMSel.id(R.id.lmp_date).text((getResources().getString(R.string.tvedd)));// 28Jun2018
                // Arpitha

                // 11July2018 Arpitha
                aqPMSel.id(R.id.etlmpdate).text("");
                aqPMSel.id(R.id.etgest).text("");
                break;
            }

            case R.id.gridView1:
                break;
            case R.id.rdbAdol://Ramesh 13-may-2021
                aqPMSel.id(R.id.rdbPreg).checked(false);
                aqPMSel.id(R.id.rdbMother).checked(false);
                aqPMSel.id(R.id.rdbchild).checked(false);

                radGrpEDDorLmp.clearCheck();//Arpitha2021
                aqPMSel.id(R.id.radLMP).checked(false);
                aqPMSel.id(R.id.radEDD).checked(false);
                aqPMSel.id(R.id.tvFirstPregHeading).enabled(false);
                aqPMSel.id(R.id.chkFirstPreg).enabled(false).checked(false);
                aqPMSel.id(R.id.radEDDorLmp).gone();
                aqPMSel.id(R.id.vHLine5).gone();
                aqPMSel.id(R.id.tvMotherAddHeading).text((getResources().getString(R.string.deldate)));
                aqPMSel.id(R.id.etMotherAdd).text("");
                aqPMSel.id(R.id.llMotherAdd).gone();
                aqPMSel.id(R.id.llFirstPreg).gone();
                aqPMSel.id(R.id.llnote).gone();
                aqPMSel.id(R.id.etregdate).enabled(false);
                aqPMSel.id(R.id.etregdate).textColor(R.color.fontcolor_disable);
                aqPMSel.id(R.id.etregdate).text(DateTimeUtil.getTodaysDate());//21March2019 Arpitha
                aqPMSel.id(R.id.lllmpdate).gone();
                aqPMSel.id(R.id.llgest).gone();
                aqPMSel.id(R.id.vHLine4).gone();
                aqPMSel.id(R.id.vHLine6).gone();
                break;
            default: {
                break;
            }
        }
    }

    private void validateAndRegister() throws Exception {
        try {
            Intent nextScreen;
            String lmpOrAdd = aqPMSel.id(R.id.etMotherAdd).getText().toString();
            String regDate = aqPMSel.id(R.id.etregdate).getText().toString();

            if ((lmpOrAdd != null && lmpOrAdd.length() > 0) && (regDate != null && regDate.length() > 0)) {

                if (aqPMSel.id(R.id.rdbPreg).isChecked()) {
                    if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0) {
                        int days = DateTimeUtil.getDaysBetweenDates(regDate, lmpOrAdd);
                        int daysdiff = DateTimeUtil
                                .getDaysBetweenDates(DateTimeUtil.getTodaysDate(), lmpOrAdd);
                        if (aqPMSel.id(R.id.radEDD).isChecked()) {
                            if (days >= 0) {
                                wantToCloseDialog = false;
                                Toast.makeText(getApplicationContext(),
                                        getResources().getString(R.string.date_val_5_toast),

                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etMotherAdd).text("");
                            } else if (noOfDays < 30) {
                                wantToCloseDialog = false;
                                Toast.makeText(getApplicationContext(),
                                        getResources().getString(R.string.gestation_must_be_30days),
                                        Toast.LENGTH_LONG).show();
                            } else if (noOfDays > 280) {
                                wantToCloseDialog = false;
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.gest_280),
                                        Toast.LENGTH_LONG).show();
                            } else {
                                wantToCloseDialog = true;
                                woman = new tblregisteredwomen();
                                nextScreen = new Intent(getApplicationContext(), RegistrationActivity.class);
                                nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                nextScreen.putExtra("adolAadhar", currentAdolDetail.getRegAdolAadharNo());//01Sep2021 Arpitha
                                woman.setRegPregnantorMother(1);
                                int gravida = 0;
                                if (aqPMSel.id(R.id.chkFirstPreg).isChecked())
                                    gravida = 1;
                                woman.setRegGravida(gravida);
                                woman.setRegRegistrationDate(aqPMSel.id(R.id.etregdate).getText().toString());
                                woman.setRegEDD(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                                nextScreen.putExtra("woman", woman);
                                startActivity(nextScreen);
                            }
                        } else {
                            if (days < 30) {
                                wantToCloseDialog = false;
                                Toast.makeText(getApplicationContext(),
                                        getResources().getString(R.string.date_val_3_toast),

                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etlmpdate).text("");
                                aqPMSel.id(R.id.etMotherAdd).text("");
                            } else if (daysdiff > 280) {

                                wantToCloseDialog = false;
                                Toast.makeText(getApplicationContext(),
                                        activity.getResources().getString(R.string.date_val_4_toast),
                                        Toast.LENGTH_LONG).show();
                                aqPMSel.id(R.id.etlmpdate).text("");
                                aqPMSel.id(R.id.etMotherAdd).text("");
                            } else {
                                wantToCloseDialog = true;
                                woman = new tblregisteredwomen();
                                nextScreen = new Intent(activity, RegistrationActivity.class);
                                nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                                nextScreen.putExtra("adolAadhar", currentAdolDetail.getRegAdolAadharNo());//01Sep2021 Arpitha


                                woman.setRegPregnantorMother(1);
                                int gravida = 0;
                                if (aqPMSel.id(R.id.chkFirstPreg).isChecked())
                                    gravida = 1;
                                woman.setRegGravida(gravida);
                                woman.setRegRegistrationDate(aqPMSel.id(R.id.etregdate).getText().toString());
                                if (aqPMSel.id(R.id.radEDD).isChecked())
                                    woman.setRegEDD(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                                else
                                    woman.setRegLMP(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                                nextScreen.putExtra("woman", woman);
                                startActivity(nextScreen);
                            }
                        }

                    }
                } else if (aqPMSel.id(R.id.rdbMother).isChecked()) {
                    if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0) {
                        int days = DateTimeUtil.getDaysBetweenDates(regDate, lmpOrAdd);
                        if (days < 0) {
                            wantToCloseDialog = false;
                            Toast.makeText(activity, (getResources().getString(R.string.m077)),
                                    Toast.LENGTH_LONG).show();
                            // } else if (days > 549) {
                        } else if (days > 5844) {// Arpitha
                            // 28Jun2018
                            wantToCloseDialog = false;
                            Toast.makeText(activity,
                                    (getResources().getString(R.string.date_val_7_toast)), // 11july2018
                                    // Arpitha
                                    Toast.LENGTH_LONG).show();
                        } else {
                            wantToCloseDialog = true;
                            woman = new tblregisteredwomen();
                            nextScreen = new Intent(activity, RegistrationActivity.class);
                            nextScreen.putExtra("adolAadhar", currentAdolDetail.getRegAdolAadharNo());//01Sep2021 Arpitha
                            woman.setRegPregnantorMother(2);
                            woman.setRegADDate(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                            woman.setRegRegistrationDate(aqPMSel.id(R.id.etregdate).getText().toString());
                            nextScreen.putExtra("woman", woman);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(nextScreen);
                        }
                    }
                } else if (aqPMSel.id(R.id.rdbchild).isChecked()) {
                    if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0) {
                        int days = DateTimeUtil.getDaysBetweenDates(regDate, lmpOrAdd);
                        if (days < 0) {
                            wantToCloseDialog = false;
                            Toast.makeText(activity, (getResources().getString(R.string.m077)),
                                    Toast.LENGTH_LONG).show();
                            // } else if (days > 549) {
                        } else if (days > 5844) {// Arpitha
                            // 28Jun2018
                            wantToCloseDialog = false;
                            Toast.makeText(activity,
                                    (getResources().getString(R.string.date_val_7_toast)), // 11july2018
                                    // Arpitha
                                    Toast.LENGTH_LONG).show();
                        } else {
                            wantToCloseDialog = true;
                            woman = new tblregisteredwomen();
                            nextScreen = new Intent(activity, ChildRegistrationActivity.class);
//							woman.setRegPregnantorMother(1);
                            woman.setRegADDate(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                            woman.setRegRegistrationDate(aqPMSel.id(R.id.etregdate).getText().toString());
                            nextScreen.putExtra("woman", woman);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(nextScreen);
                        }
                    }
                } else if (aqPMSel.id(R.id.rdbAdol).isChecked()) { //Ramesh 13-may-2021
                    if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0) {
                        int days = DateTimeUtil.getDaysBetweenDates(regDate, lmpOrAdd);
                        if (days < 0) {
                            wantToCloseDialog = false;
                            Toast.makeText(activity, (getResources().getString(R.string.m077)),
                                    Toast.LENGTH_LONG).show();
                            // } else if (days > 549) {
                        } else if (days > 5844) {// Arpitha
                            // 28Jun2018
                            wantToCloseDialog = false;
                            Toast.makeText(activity,
                                    (getResources().getString(R.string.date_val_7_toast)), // 11july2018
                                    // Arpitha
                                    Toast.LENGTH_LONG).show();
                        } else {
                            wantToCloseDialog = true;
                            tblAdolReg adolReg = new tblAdolReg();
                            nextScreen = new Intent(activity, AdolRegistration.class);
                            adolReg.setRecordCreatedDate(aqPMSel.id(R.id.etregdate).getText().toString());
                            nextScreen.putExtra("AdolReg", adolReg);
                            nextScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                            startActivity(nextScreen);
                        }
                    }
                }
            } else {
                wantToCloseDialog = false;
                if (aqPMSel.id(R.id.rdbPreg).isChecked()) {
                    if (aqPMSel.id(R.id.etMotherAdd).getText().toString().trim().length() <= 0) {
                        aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                        if (aqPMSel.id(R.id.radEDD).isChecked())
                            Toast.makeText(activity,
                                    (activity.getResources().getString(R.string.m170)),
                                    Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(activity,
                                    activity.getResources().getString(R.string.m074),
                                    Toast.LENGTH_LONG).show();
                    } else if (aqPMSel.id(R.id.etregdate).getText().toString().trim().length() <= 0) {
                        aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                        Toast.makeText(getApplicationContext(),
                                (activity.getResources().getString(R.string.m157)),
                                Toast.LENGTH_LONG).show();
                    }
                } else if (aqPMSel.id(R.id.rdbMother).isChecked()) {
                    if (aqPMSel.id(R.id.etMotherAdd).getText().toString().trim().length() <= 0) {
                        aqPMSel.id(R.id.etMotherAdd).getEditText().requestFocus();
                        Toast.makeText(activity,
                                (activity.getResources().getString(R.string.m066)),
                                Toast.LENGTH_LONG).show();
                    } else if (aqPMSel.id(R.id.etregdate).getText().toString().trim().length() <= 0) {
                        aqPMSel.id(R.id.etregdate).getEditText().requestFocus();
                        Toast.makeText(activity,
                                activity.getResources().getString(R.string.m157),
                                Toast.LENGTH_LONG).show();
                    }
                } else if (aqPMSel.id(R.id.rdbchild).isChecked()) {
                    wantToCloseDialog = true;
//					if (aqPMSel.id(R.id.etMotherAdd).getText().length() > 0)
                    {
//						int days = DateTimeUtil.getDaysBetweenDates(regDate, lmpOrAdd);
						/*if (days < 0) {
							wantToCloseDialog = false;
							Toast.makeText(activity, (getResources().getString(R.string.m077)),
									Toast.LENGTH_LONG).show();
							// } else if (days > 549) {
						} else if (days > 5844) {// Arpitha
							// 28Jun2018
							wantToCloseDialog = false;
							Toast.makeText(activity,
									(getResources().getString(R.string.date_val_7_toast)), // 11july2018
									// Arpitha
									Toast.LENGTH_LONG).show();
						} else {*/
                        wantToCloseDialog = true;
                        woman = new tblregisteredwomen();
                        nextScreen = new Intent(activity, ChildRegistrationActivity.class);
                        woman.setRegPregnantorMother(1);
                        woman.setRegADDate(aqPMSel.id(R.id.etMotherAdd).getText().toString());
                        woman.setRegRegistrationDate(aqPMSel.id(R.id.etregdate).getText().toString());
                        nextScreen.putExtra("woman", woman);
                        nextScreen.putExtra("globalState", activity.getIntent().getBundleExtra("globalState"));
                        startActivity(nextScreen);
//						}
                    }
                } else if (aqPMSel.id(R.id.rdbAdol).isChecked()) { //Ramesh 13-may-2021

                    wantToCloseDialog = true;
                    tblAdolReg adolReg = new tblAdolReg();
                    nextScreen = new Intent(activity, AdolRegistration.class);
                    adolReg.setRecordCreatedDate(aqPMSel.id(R.id.etregdate).getText().toString());
                    nextScreen.putExtra("AdolReg", adolReg);
                    nextScreen.putExtra("globalState", activity.getIntent().getBundleExtra("globalState"));
                    startActivity(nextScreen);


                }
            }

        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    private void assignEditTextAndCallDatePicker(EditText editText) {
        currentEditText = editText;
        String currDate = (editText.getText().length() > 0) ? editText.getText().toString() : "";
        CommonDatePickerFragment.getCommonDate(this, currDate);
    }
}



