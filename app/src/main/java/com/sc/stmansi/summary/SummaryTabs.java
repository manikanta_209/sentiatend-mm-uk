package com.sc.stmansi.summary;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.androidquery.AQuery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.About.AboutActivity;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.login.LoginActivity;
import com.sc.stmansi.mainmenu.MainMenuActivity;
import com.sc.stmansi.recyclerview.RecyclerViewUpdateListener;
import com.sc.stmansi.repositories.FacilityRepository;
import com.sc.stmansi.repositories.LoginRepository;
import com.sc.stmansi.tables.tblregisteredwomen;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.sc.stmansi.R;

public class SummaryTabs extends AppCompatActivity implements
        ActionBar.OnNavigationListener, AdapterView.OnItemSelectedListener
     {

    private AQuery aq = null;

    private ViewPager viewPager;
    private SummaryTabsPagerAdapter mAdapter;

    // Tab titles
  /*  private Integer[] tabs = {R.drawable.reg_women_small,
            R.drawable.ic_pregnancy_services_small_white};*/



    private int currentTab = 0;

    private List<Fragment> callbacks  = new ArrayList<>(2);;

    private Spinner villageSpinner;
    private ArrayAdapter<CharSequence> villageSpinnerAdapter;
    private Map<String, Integer> villageCodeForName;
    private boolean spinnerSelectionChanged = false;

    public String wFilterStr = "";

    private DatabaseHelper databaseHelper;
    private AppState appState;
    private AQuery aqPMSel;
    private RadioGroup radGrpEDDorLmp;
    private Activity activity;
    private boolean isRegDate;
    private boolean wantToCloseDialog = false;
    private int noOfDays;
    private tblregisteredwomen woman;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ejanani_women_list_tabhost);

        getIntent().putExtra("wFilterStr", wFilterStr);

        try {
            aq = new AQuery(this);

            Bundle bundle = getIntent().getBundleExtra("globalState");
            appState = bundle.getParcelable("appState");

            databaseHelper = getHelper();

            activity = SummaryTabs.this;

            aq.id(R.id.llWomanFilter).gone();
            aq.id(R.id.chklmpnotconfirmed).gone(); //06Apr2021 Bindu - hide display LMP not confirmed

            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setNavigationMode(androidx.appcompat.app.ActionBar.NAVIGATION_MODE_TABS);

            getSupportActionBar().setCustomView(R.layout.ejanani_village_filter);
            getSupportActionBar().setDisplayShowCustomEnabled(true);

            viewPager = findViewById(R.id.pager);
            viewPager.setOffscreenPageLimit(2);

            mAdapter = new SummaryTabsPagerAdapter(getSupportFragmentManager());
            viewPager.setAdapter(mAdapter);
            viewPager.addOnPageChangeListener(new WomenViewPagerListener());
             String[] strtabs = {getResources().getString(R.string.activitysummary),
                    getResources().getString(R.string.servcicessumary)};
            for (String tab_name : strtabs) {
                getSupportActionBar().addTab(getSupportActionBar().newTab()
                        .setText(tab_name).setTabListener(new TabSelectionListener()));
            }

            aq.id(R.id.imgresetfilter).getImageView().setOnClickListener(new FilterResetListener());

            villageSpinner = aq.id(R.id.spVillageFilter).getSpinner();
            villageSpinner.setOnItemSelectedListener(this);
            populateSpinnerVillage();

            EditText filterTextView = aq.id(R.id.etWomanFilter).getEditText();
            filterTextView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (v.getText() != null) {
                        aq.id(R.id.imgresetfilter).getImageView().setBackgroundResource(R.drawable.ic_reset);
                        wFilterStr = v.getText().toString();
                        for (Fragment activeFragment : callbacks) {
                            RecyclerViewUpdateListener f = (RecyclerViewUpdateListener) activeFragment;
                            f.updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr, aq.id(R.id.chklmpnotconfirmed).isChecked());
                        }
                    }
                    return true;
                }
            });
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    //    initialize db object
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    //    view pager class
    class WomenViewPagerListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageSelected(int position) {
            currentTab = position;
            getSupportActionBar().setSelectedNavigationItem(position);
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {
        }
    }

    //    filter class
    class FilterResetListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            aq.id(R.id.imgresetfilter).getImageView().setBackgroundResource(R.drawable.ic_reset_gray);
            aq.id(R.id.etWomanFilter).text("");
            wFilterStr = "";
            for (Fragment activeFragment : callbacks) {
                RecyclerViewUpdateListener f = (RecyclerViewUpdateListener) activeFragment;
                f.updateList(appState.selectedVillageTitle, villageCodeForName, wFilterStr, aq.id(R.id.chklmpnotconfirmed).isChecked());
            }
        }
    }

    //    tab listener class
    class TabSelectionListener implements androidx.appcompat.app.ActionBar.TabListener {

        @Override
        public void onTabSelected(androidx.appcompat.app.ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
            currentTab = tab.getPosition();
            viewPager.setCurrentItem(tab.getPosition());
        }

        @Override
        public void onTabUnselected(androidx.appcompat.app.ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

        }

        @Override
        public void onTabReselected(androidx.appcompat.app.ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }



    /**
     * /** Actionbar navigation item select listener
     */
    @Override
    public boolean onNavigationItemSelected(int itemPosition, long itemId) {
        try {
            CharSequence villageTitle = villageSpinnerAdapter.getItem(itemPosition);
            appState.selectedVillageCode = villageCodeForName.get(villageTitle);
            mAdapter = new SummaryTabsPagerAdapter(getSupportFragmentManager());
            invalidateOptionsMenu();
            return true;
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            return false;
        }
    }

    /**
     * on back click send to tabmenu activity
     */
    @Override
    public void onBackPressed() {
    }



      @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.servicesmenu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        try {

            switch (item.getItemId()) {
                // Respond to the action bar's Up/Home button
                case android.R.id.home:
                    NavUtils.navigateUpFromSameTask(this);
                    return true;

                case R.id.logout:
                    Intent goToScreen = new Intent(SummaryTabs.this, LoginActivity.class);
                    goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    displayAlert(getResources().getString(R.string.m111), goToScreen);
                    return true;


                case R.id.about:

                    Intent about = new Intent(SummaryTabs.this, AboutActivity.class);
                    about.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    displayAlert(getResources().getString(R.string.exit), about);
                    return true;


                case R.id.home:
                    Intent home = new Intent(SummaryTabs.this, MainMenuActivity.class);
                    home.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    displayAlert(getResources().getString(R.string.exit), home);
                    return true;


            }
        } catch (Exception e) {
           FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
        return super.onOptionsItemSelected(item);
    }




    //    prepare bundle
    private Bundle prepareBundle() {
        Bundle b = new Bundle();
        b.putParcelable("appState", appState);
        return b;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }


    /**
     * This method Calls the idle timeout
     */ //09Aug2019 - Bindu
    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        //ActivitiesStack.delayedIdle(Integer.parseInt(CommonClass.properties.getProperty("idleTimeOut")));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        /*
         * You'll need this in your class to release the helper when done.
         */
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }














    public void addFragment(Fragment f) {
        if(this.callbacks == null) {
            this.callbacks = new ArrayList<>(3);
        }
        this.callbacks.add(f);
    }




    /**
     * populate Village to Spinner
     */
    private void populateSpinnerVillage() throws Exception {
        FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
        villageCodeForName = facilityRepository.getPlaceMap();

        ArrayAdapter<CharSequence> villageSpinnerAdapter = new ArrayAdapter<>(this, R.layout.actionbar_spinner_list_item);
        villageSpinnerAdapter.add("All");
        for (Map.Entry<String, Integer> village : villageCodeForName.entrySet()) {
            villageSpinnerAdapter.add(village.getKey());
        }
        villageSpinnerAdapter.setDropDownViewResource(R.layout.actionbar_spinner_layout_view);
        villageSpinner.setAdapter(villageSpinnerAdapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        try {
            appState.selectedVillageTitle = (String) parent.getItemAtPosition(position);


            if (position > 0)
                appState.selectedVillageCode = villageCodeForName.get(appState.selectedVillageTitle);
            else
                appState.selectedVillageCode = 0;
            appState.selectedVillagePosition = position;

            ServicesSummaryActivity.displayServicesCount();
            Summary_Activity.displayCurrentYearSummary();


        } catch (Exception e) {
           FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    /**
     * Save Confirmation Alert
     *
     * @param goToScreen
     */
    private void displayAlert(final String spanText2, final Intent goToScreen) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set dialog message
        alertDialogBuilder.setMessage(spanText2).setCancelable(false).setNegativeButton(
                getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {


                        if (goToScreen != null) {
                            try {
                                startActivity(goToScreen);
                                if(spanText2.contains("logout")) {
                                    LoginRepository loginRepository = new LoginRepository(databaseHelper);
                                    loginRepository.updateLogoutTime(appState.loginAuditId, appState.sessionUserId);
                                }
                            } catch (Exception e) {
                                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                            }
                        } else
                            dialog.cancel();

                    }
                }).setPositiveButton(getResources().getString(R.string.m122),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }


}
