package com.sc.stmansi.summary;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.recyclerview.RecyclerViewUpdateListener;
import com.sc.stmansi.repositories.WomanServiceRepository;
import com.sc.stmansi.tables.tblservicestypemaster;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.sc.stmansi.R;

public class ServicesSummaryActivity extends Fragment
        implements AdapterView.OnItemSelectedListener, RecyclerViewUpdateListener {


    static DatabaseHelper databaseHelper;
    static LinearLayout llLay;
    static LinearLayout llLayHeading;
    private Spinner villageSpinner;
    private Map<String, Integer> villageCodeForName;
    static AppState appState;
    static Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        SummaryTabs r = (SummaryTabs) context;
        r.addFragment(this);
    }

    /**
     * Initializes the RegisteredWomenList screen
     * Sets the ejanani_women_list.xml to this class
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_ashaservicessummary, container, false);

        llLay = rootView.findViewById(R.id.llserv);
        llLayHeading = rootView.findViewById(R.id.llservHeading);
        return rootView;
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        {

            try {

                Bundle bundle = getActivity().getIntent().getParcelableExtra("globalState");
                appState = bundle.getParcelable("appState");


                databaseHelper = getHelper();



                context = getActivity();
                displayServicesCount();

            }catch (Exception e)
            {
               FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
            }
        }
    }


    static void displayServicesCount() throws Exception{


        llLay.removeAllViews();
        llLayHeading.removeAllViews();
        WomanServiceRepository womanServiceRepository = new WomanServiceRepository(databaseHelper);
        List<tblservicestypemaster> listServices =   womanServiceRepository.getServiceName();


        TextView txtServiceName = null;



        String[] heading  =  context.getResources().getStringArray(R.array.servicefiltersumm);



        TableRow trServHeading = new TableRow(context);
        TableRow.LayoutParams tvParlbl = new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT, 1f);

        trServHeading.setBackgroundColor(context.getResources().getColor(R.color.m2));
        trServHeading.setLayoutParams(new ViewGroup.LayoutParams
                (ViewGroup.LayoutParams.MATCH_PARENT, 45));

        TextView  txtServiceNameHead = new TextView(context);
        txtServiceNameHead.setLayoutParams(tvParlbl);
        txtServiceNameHead.setGravity(Gravity.CENTER);
        txtServiceNameHead.setPadding(1,1,1,1);
        txtServiceNameHead.setText(heading[0]);
        txtServiceNameHead.setTextSize(16);
        txtServiceNameHead.setTextColor(context.getResources().getColor(R.color.white));
        txtServiceNameHead.setTypeface(Typeface.DEFAULT_BOLD);
        trServHeading.addView(txtServiceNameHead);


        TextView txtServiceExpiredCountHead = new TextView(context);
        txtServiceExpiredCountHead.setLayoutParams(tvParlbl);
        txtServiceExpiredCountHead.setGravity(Gravity.CENTER);
        txtServiceExpiredCountHead.setPadding(1,1,1,1);
        txtServiceExpiredCountHead.setText
                (heading[1]);
        txtServiceExpiredCountHead.setTextSize(16);
        txtServiceExpiredCountHead.setTypeface(Typeface.DEFAULT_BOLD);
        txtServiceExpiredCountHead.setTextColor(context.getResources().getColor(R.color.white));
        trServHeading.addView(txtServiceExpiredCountHead);



        TextView txtServicePlannedCountHead = new TextView(context);
        txtServicePlannedCountHead.setLayoutParams(tvParlbl);
        txtServicePlannedCountHead.setGravity(Gravity.CENTER);
        txtServicePlannedCountHead.setPadding(1,1,1,1);
        txtServicePlannedCountHead.setText
                (heading[2]);
        txtServicePlannedCountHead.setTextSize(16);
        txtServicePlannedCountHead.setTextColor(context.getResources().getColor(R.color.white));
        txtServicePlannedCountHead.setTypeface(Typeface.DEFAULT_BOLD);
        trServHeading.addView(txtServicePlannedCountHead);



        TextView txtServiceComplCountHead = new TextView(context);
        txtServiceComplCountHead.setLayoutParams(tvParlbl);
        txtServiceComplCountHead.setGravity(Gravity.CENTER);
        txtServiceComplCountHead.setPadding(1,1,1,1);
        txtServiceComplCountHead.setText
                (heading[3]);
        txtServiceComplCountHead.setTextSize(16);
        txtServiceComplCountHead.setTextColor(context.getResources().getColor(R.color.white));
        txtServiceComplCountHead.setTypeface(Typeface.DEFAULT_BOLD);
        trServHeading.addView(txtServiceComplCountHead);





        TextView txtServiceUpcomingCountHead = new TextView(context);
        txtServiceUpcomingCountHead.setLayoutParams(tvParlbl);
        txtServiceUpcomingCountHead.setGravity(Gravity.CENTER);
        txtServiceUpcomingCountHead.setPadding(1,1,1,1);
        txtServiceUpcomingCountHead.setText
                (heading[4]);
        txtServiceUpcomingCountHead.setTextSize(16);
        txtServiceUpcomingCountHead.setTextColor(context.getResources().getColor(R.color.white));
        txtServiceUpcomingCountHead.setTypeface(Typeface.DEFAULT_BOLD);
        trServHeading.addView(txtServiceUpcomingCountHead);

        llLayHeading.addView(trServHeading);



        for(int i=0;i<listServices.size();i++) {



            TableRow trServ = new TableRow(context);
            TableRow.LayoutParams tvPar = new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT, 1f);

            trServ.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 31));

            txtServiceName = new TextView(context);
            /*txtServiceName.setWidth(150);
            txtServiceName.setHeight(27);*/
            txtServiceName.setLayoutParams(tvPar);
            txtServiceName.setGravity(Gravity.CENTER);
            txtServiceName.setPadding(1,1,1,2);
           // txtServiceName.setText(listServices.get(i).getServiceType());
            int identifier = context.getResources().getIdentifier(listServices.get(i).getServiceType().toLowerCase(),
                    "string", "com.sc.stmansi");  //05Dec2019 - Bindu
            txtServiceName.setText(context.getResources().getString(identifier));
            txtServiceName.setTextColor(context.getResources().getColor(R.color.darkbrown));
            trServ.addView(txtServiceName);



            TextView txtServiceExpiredCount = new TextView(context);
            /*txtServiceExpiredCount.setWidth(120);
            txtServiceExpiredCount.setHeight(23);*/
            txtServiceExpiredCount.setLayoutParams(tvPar);
            txtServiceExpiredCount.setGravity(Gravity.CENTER);
            txtServiceExpiredCount.setPadding(30,1,1,1);
            if(i<=7)
                txtServiceExpiredCount.setText(""+womanServiceRepository.getExpiredServiceCount(listServices.get(i).getServiceType(),appState.selectedVillageCode));
            else
                txtServiceExpiredCount.setText(""+womanServiceRepository.getExpiredImmuCount(listServices.get(i).getServiceType(),appState.selectedVillageCode));

            txtServiceExpiredCount.setTextColor(context.getResources().getColor(R.color.black));
            trServ.addView(txtServiceExpiredCount);

            TextView txtServiceUpcomingCount = new TextView(context);
            /*txtServiceUpcomingCount.setWidth(130);
            txtServiceUpcomingCount.setHeight(23);*/
            txtServiceUpcomingCount.setLayoutParams(tvPar);
            txtServiceUpcomingCount.setGravity(Gravity.CENTER);
            txtServiceUpcomingCount.setPadding(30,1,1,1);
            if(i<=7)
                txtServiceUpcomingCount.setText(""+womanServiceRepository.getUpcomingServiceCount(listServices.get(i).getServiceType(),appState.selectedVillageCode));
            else
                txtServiceUpcomingCount.setText(""+womanServiceRepository.getUpcomingImmuCount(listServices.get(i).getServiceType(),appState.selectedVillageCode));

            txtServiceUpcomingCount.setTextColor(context.getResources().getColor(R.color.black));
            trServ.addView(txtServiceUpcomingCount);


            TextView txtServicePlannedCount = new TextView(context);
            /*txtServicePlannedCount.setWidth(120);
            txtServicePlannedCount.setHeight(23);*/
            txtServicePlannedCount.setLayoutParams(tvPar);
            txtServicePlannedCount.setGravity(Gravity.CENTER);
            txtServicePlannedCount.setPadding(30,1,1,1);
            if(i<=7)
                txtServicePlannedCount.setText(""+womanServiceRepository.getPlannedServiceCount(listServices.get(i).getServiceType(),appState.selectedVillageCode));
            else {
                 String count = womanServiceRepository.getPlannedImmuCount(listServices.get(i)
                         .getServiceType(),appState.selectedVillageCode);
                txtServicePlannedCount.setText(count);
            }

            txtServicePlannedCount.setTextColor(context.getResources().getColor(R.color.black));
            trServ.addView(txtServicePlannedCount);





            TextView txtServiceComplCount = new TextView(context);
            /*txtServiceComplCount.setWidth(140);
            txtServiceComplCount.setHeight(23);*/
            txtServiceComplCount.setLayoutParams(tvPar);
            txtServiceComplCount.setGravity(Gravity.CENTER);
            txtServiceComplCount.setPadding(30,1,1,1);
            if(i<=7)
                txtServiceComplCount.setText(""+womanServiceRepository.getCount(listServices.get(i).getServiceType()));
            else
                txtServiceComplCount.setText(""+womanServiceRepository.getImmunozationCount(listServices.get(i).getServiceType()));

            txtServiceComplCount.setTextColor(context.getResources().getColor(R.color.black));
            trServ.addView(txtServiceComplCount);



            View view = new View(context);
            view.setMinimumWidth(500);
            view.setMinimumHeight(1);
            view.setBackgroundColor(context.getResources().getColor(R.color.LightGrey));

            llLay.addView(trServ);
            llLay.addView(view);



        }
    }

    //	intialize db
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(getActivity(), DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        try {
            appState.selectedVillageTitle = (String) parent.getItemAtPosition(position);
          //  if (spinnerSelectionChanged) {


                if(position>0)
                    appState.selectedVillageCode = villageCodeForName.get(appState.selectedVillageTitle);
                else
                    appState.selectedVillageCode = 0;
               displayServicesCount();
                appState.selectedVillagePosition = position;

           // }
            //spinnerSelectionChanged = true;
        } catch (SQLException e) {
           FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        } catch (Exception e) {
           FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void updateList(String villageTitle, Map<String, Integer> villageNameForCode, String textViewFilter, boolean checked) {

        try {
            displayServicesCount();
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }
}
