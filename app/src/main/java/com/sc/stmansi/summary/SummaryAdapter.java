package com.sc.stmansi.summary;/*
package com.sc.stmansi.summary;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import android.app.Activity;
import android.content.Context;
import android.content.res.Resources.NotFoundException;
import android.database.DataSetObserver;
import android.graphics.Typeface;
import android.support.v7.widget.ListPopupWindow;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.sc.stmansi.R;
import com.sc.stmansi.common.DateTimeUtil;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.repositories.UserRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.tables.TblChildInfo;
import com.sc.stmansi.tables.TblInstusers;
import com.sc.stmansi.tables.tblregisteredwomen;

public class SummaryAdapter extends BaseExpandableListAdapter {

	private Activity context;
	private Map<String, ArrayList<tblregisteredwomen>> CategoryList;
	DatabaseHelper dbh;

	ArrayList<String> years;

	String[] heading;
	WomenListItem holder = null;
	String month_name;
	String[] months;
	int month;
	String startDay, endDay;

	public SummaryAdapter(Summary_Activity context, ArrayList<String> years,
			HashMap<String, ArrayList<tblregisteredwomen>>
					categoryDataCollection, DatabaseHelper dbh) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.years = years;
		this.dbh = dbh;
		this.CategoryList = categoryDataCollection;

	}

	private class WomenListItem {

		TableLayout tblsummary;

	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {

		ArrayList<String> lKeys = new ArrayList<String>(this.CategoryList.keySet());
		String xKey = lKeys.get(groupPosition);
		return xKey;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		int returnvalue = 0;
		try {

			returnvalue = 1;
		} catch (Exception e) {

			e.printStackTrace();
		}
		return returnvalue;
	}

	@Override
	public Object getGroup(int groupPosition) {
		Object returnvalue = null;

		try {

			ArrayList<String> lKeys = new ArrayList<String>(years);
			String xKey = lKeys.get(groupPosition);
			returnvalue = xKey;
		} catch (Exception e) {

			e.printStackTrace();
		}
		return returnvalue;
	}

	public int getGroupCount() {
		return this.years.size();
	}

	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLAstchild, View convertView,
			ViewGroup parent) {

		// TODO Auto-generated method stub
		try {

			LayoutInflater inflater = context.getLayoutInflater();

			if (convertView == null) {
				convertView = inflater.inflate(R.layout.new_womenlist_adapter, null);

				holder = new WomenListItem();

				convertView.setTag(holder);
			} else
				holder = (WomenListItem) convertView.getTag();

			holder.tblsummary = (TableLayout) convertView.findViewById(R.id.tblsummary);

			holder.tblsummary.removeAllViews();

            try {
                DateTimeUtil.getCurrentWeek();
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            heading = this.context.getResources().getStringArray(R.array.summary);


			month_name = DateTimeUtil.getCureentMonthName();
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat currentYear = new SimpleDateFormat("yyyy");

			String todaysDate = DateTimeUtil.getTodaysDate("MM");
			month = Integer.parseInt(todaysDate);

			months = context.getResources().getStringArray(R.array.month);
			
			startDay = DateTimeUtil.getPreviousDate("yyyy-MM-dd", 7);//11April2017 Arpitha
			
			endDay = DateTimeUtil.getPreviousDate("yyyy-MM-dd", 1);//11April2017 Arpitha

            try{
            displayCurrentYearSummary(0);
        } catch (Exception e) {
            e.printStackTrace();
        }

		} catch (Exception e) {

			e.printStackTrace();
		}

		return convertView;
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

		// TODO Auto-generated method stub

		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.group_item, null);
		}

		String yearNme = (String) getGroup(groupPosition);
		TextView item = (TextView) convertView.findViewById(R.id.txtMnthName);
//		item.setTypeface(null, Typeface.BOLD);

		item.setText(yearNme);

        ExpandableListView mExpandableListView = (ExpandableListView) parent;
        mExpandableListView.expandGroup(0);
        mExpandableListView.setEnabled(false);

        try {
          //  displayCurrentYearSummary(groupPosition);
        } catch (Exception e) {
            e.printStackTrace();
        }


        return convertView;

	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return true;
	}

	public void unregisterDataSetObserver(DataSetObserver observer) {
		if (observer != null) {
			super.unregisterDataSetObserver(observer);
		}
	}

	void displayCurrentYearSummary(int groupPosition)  {
	    try {
            TableRow trlabel = new TableRow(this.context);
            trlabel.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));

            TextView textmenu = new TextView(this.context);
            textmenu.setTextSize(18);
            textmenu.setPadding(10, 10, 10, 10);
            textmenu.setTextColor(this.context.getResources().getColor(R.color.colorPrimary));
            textmenu.setTextSize(18);
            textmenu.setTypeface(textmenu.getTypeface(), Typeface.BOLD);
            textmenu.setGravity(Gravity.LEFT);
            trlabel.addView(textmenu);

           */
/* TextView texttoday = new TextView(this.context);
            texttoday.setTextSize(18);
            texttoday.setPadding(10, 10, 10, 10);
            texttoday.setTextColor(this.context.getResources().getColor(R.color.colorPrimary));
            texttoday.setTextSize(15);
            texttoday.setGravity(Gravity.CENTER);
            texttoday.setTypeface(textmenu.getTypeface(), Typeface.BOLD);
            texttoday.setText(this.context.getResources()
                    .getString(R.string.count));
            trlabel.addView(texttoday);*//*



            holder.tblsummary.addView(trlabel);

            TableRow trlabelVal = null;

            for (int j = 1; j <= heading.length; j++) {

                List<tblregisteredwomen> val = new ArrayList<>();

                WomanRepository womanRepository = new WomanRepository(dbh);
                val = womanRepository.getSummary(j,null,
						"10001",null,2);

                trlabelVal = new TableRow(this.context);
                trlabelVal.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));

                TextView textmenuval = new TextView(this.context);
                textmenuval.setTextSize(18);
                textmenuval.setPadding(20, 10, 20, 10);
                textmenuval.setTextColor(this.context.getResources().getColor(R.color.colorPrimary));
                textmenuval.setTextSize(20);
                textmenuval.setText(heading[j - 1]);
                textmenuval.setGravity(Gravity.LEFT);
                trlabelVal.addView(textmenuval);


                */
/*View view = new View(context);
                view.setMinimumWidth(ListPopupWindow.MATCH_PARENT);
				view.setMinimumHeight(2);

				trlabel.addView(view);*//*


                TextView texttodayval = new TextView(this.context);
                texttodayval.setTextSize(18);
                texttodayval.setPadding(20, 10, 20, 10);
                texttodayval.setTextColor(this.context.getResources().getColor(R.color.black));
                texttodayval.setTextSize(20);
                texttodayval.setText(val.get(0).getRegRegistrationDate());
                texttodayval.setGravity(Gravity.RIGHT);
                trlabelVal.addView(texttodayval);

				View view1 = new View(context);
				view1.setMinimumWidth(ListPopupWindow.MATCH_PARENT);
				view1.setMinimumHeight(2);

				trlabelVal.addView(view1);

                holder.tblsummary.addView(trlabelVal);

//				holder.tblsummary.addView(view1);
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
	}


}
*/
