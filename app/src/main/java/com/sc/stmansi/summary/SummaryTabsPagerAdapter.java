package com.sc.stmansi.summary;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.google.firebase.crashlytics.FirebaseCrashlytics;

public class SummaryTabsPagerAdapter extends FragmentPagerAdapter {

    FragmentManager fm;
    public SummaryTabsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int index) {
        try {
            switch (index) {
                case 0:
                    Summary_Activity f1 = new Summary_Activity();

                    return f1;
                case 1:
                    ServicesSummaryActivity f2 = new ServicesSummaryActivity();
                    return f2;

            }
        } catch (Exception e) {

            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }

        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

}
