package com.sc.stmansi.summary;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sc.stmansi.configuration.AppState;
import com.sc.stmansi.datasource.DatabaseHelper;
import com.sc.stmansi.recyclerview.RecyclerViewUpdateListener;
import com.sc.stmansi.repositories.FacilityRepository;
import com.sc.stmansi.repositories.LoginRepository;
import com.sc.stmansi.repositories.WomanRepository;
import com.sc.stmansi.tables.tblregisteredwomen;

import java.util.List;
import java.util.Map;

import com.sc.stmansi.R;

public class Summary_Activity extends Fragment
        implements AdapterView.OnItemSelectedListener , RecyclerViewUpdateListener {

    static TableLayout tblsummary;
    static DatabaseHelper databaseHelper;
    static String[] heading;
    public static int summary;
    static int monthPos;
    int yearPos;
    static String selectedYear;
    static AppState appState;
    List<String> years;
    private Spinner villageSpinner;
    private Map<String, Integer> villageCodeForName;
    Spinner spnYear,spnMonth;
    static Context context;



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        SummaryTabs r = (SummaryTabs) context;
        r.addFragment(this);
    }

    /**
     * Initializes the RegisteredWomenList screen
     * Sets the ejanani_women_list.xml to this class
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.new_womenlist_adapter, container, false);

         spnYear = rootView.findViewById(R.id.spnyearsumm);
        spnMonth = rootView.findViewById(R.id.spnmonthsumm);
        tblsummary = rootView.findViewById(R.id.tblsummary);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {

            Bundle bund = getActivity().getIntent().getBundleExtra("globalState");
            appState = bund.getParcelable("appState");
            databaseHelper = getHelper();

            WomanRepository womanRepository = new WomanRepository(databaseHelper);
            years = womanRepository.getYears();
//            years.add(0, getResources().getString(R.string.all));
            years.add(0, "All");

            ArrayAdapter adapter = new ArrayAdapter(getActivity(), R.layout.simple_spinner_item, years);
            spnYear.setAdapter(adapter);

            context = getActivity();

            heading = getResources().getStringArray(R.array.summary);


            displayCurrentYearSummary();


            spnMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    monthPos = position;
                    displayCurrentYearSummary();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            spnYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    selectedYear = spnYear.getSelectedItem().toString();
                    yearPos = years.indexOf(selectedYear);
                    displayCurrentYearSummary();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }catch (Exception e){
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

 /*   @Override
    protected void onCreate(Bundle bundle) {
        // TODO Auto-generated method stub
        super.onCreate(bundle);

        setContentView(R.layout.new_womenlist_adapter);

        try {

            Bundle bund = getIntent().getBundleExtra("globalState");
            appState = bund.getParcelable("appState");
            databaseHelper = getHelper();


          *//*  getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

            getSupportActionBar().setCustomView(R.layout.ejanani_village_filter);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            villageSpinner = findViewById(R.id.spVillageFilter);
            villageSpinner.setOnItemSelectedListener(getActivity());
            populateSpinnerVillage();*//*





            WomanRepository womanRepository = new WomanRepository(databaseHelper);
            years = womanRepository.getYears();
            years.add(0, getResources().getString(R.string.all));
            ArrayAdapter adapter = new ArrayAdapter(getActivity(), R.layout.simple_spinner_item, years);
            spnYear.setAdapter(adapter);



            displayCurrentYearSummary();


            spnMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    monthPos = position;
                    displayCurrentYearSummary();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            spnYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    selectedYear = spnYear.getSelectedItem().toString();
                    yearPos = years.indexOf(selectedYear);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }*/

  /*  @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.servicesmenu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        try {

            switch (item.getItemId()) {
                // Respond to the action bar's Up/Home button
                case android.R.id.home:
                    NavUtils.navigateUpFromSameTask(getActivity());
                    return true;

                case R.id.logout:
                    Intent goToScreen = new Intent(Summary_Activity.getActivity(), LoginActivity.class);
                    goToScreen.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    displayAlert(getResources().getString(R.string.m111), goToScreen);
                    return true;


                case R.id.about:

                    Intent about = new Intent(Summary_Activity.getActivity(), AboutActivity.class);
                    about.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    displayAlert(getResources().getString(R.string.exit), about);
                    return true;


                case R.id.home:
                    Intent home = new Intent(Summary_Activity.getActivity(), MainMenuActivity.class);
                    home.putExtra("globalState", getIntent().getBundleExtra("globalState"));
                    displayAlert(getResources().getString(R.string.exit), home);
                    return true;


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.onOptionsItemSelected(item);
    }

    *//**
     * This method Calls the idle timeout
     *//*
    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }*/


    static void displayCurrentYearSummary() {
        try {
            tblsummary.removeAllViews();



            WomanRepository womanRepository = new WomanRepository(databaseHelper);
            for (int j = 1; j <= heading.length; j++) {
                List<tblregisteredwomen> val = womanRepository.getSummary(j, "" + monthPos,
                        appState.sessionUserId, selectedYear, appState.selectedVillageCode);

                TableRow trlabelVal = new TableRow(context);
                trlabelVal.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, 40));

                TextView textmenuval = new TextView(context);
                textmenuval.setTextSize(18);
                textmenuval.setPadding(20, 10, 20, 10);
                textmenuval.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                textmenuval.setTextSize(20);
                textmenuval.setText(heading[j - 1]);
                textmenuval.setGravity(Gravity.LEFT);
                textmenuval.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
                trlabelVal.addView(textmenuval);

                TextView texttodayval = new TextView(context);
                texttodayval.setTextSize(18);
                texttodayval.setPadding(20, 10, 20, 10);
                texttodayval.setTextColor(context.getResources().getColor(R.color.black));
                texttodayval.setTextSize(20);
                texttodayval.setText(val.get(0).getRegRegistrationDate());
                texttodayval.setGravity(Gravity.RIGHT);
                trlabelVal.addView(texttodayval);


                tblsummary.addView(trlabelVal);
            }
        } catch (Exception e) {
           FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    /**
     * populate Village to Spinner
     */
    private void populateSpinnerVillage() throws Exception {
        FacilityRepository facilityRepository = new FacilityRepository(databaseHelper);
        villageCodeForName = facilityRepository.getPlaceMap();

        ArrayAdapter<CharSequence> villageSpinnerAdapter = new ArrayAdapter<>(getActivity(), R.layout.actionbar_spinner_list_item);
        villageSpinnerAdapter.add("All");
        for (Map.Entry<String, Integer> village : villageCodeForName.entrySet()) {
            villageSpinnerAdapter.add(village.getKey());
        }
        villageSpinnerAdapter.setDropDownViewResource(R.layout.actionbar_spinner_layout_view);
        villageSpinner.setAdapter(villageSpinnerAdapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        try {
            appState.selectedVillageTitle = (String) parent.getItemAtPosition(position);


            if (position > 0)
                appState.selectedVillageCode = villageCodeForName.get(appState.selectedVillageTitle);
            else
                appState.selectedVillageCode = 0;
            displayCurrentYearSummary();
            appState.selectedVillagePosition = position;


        } catch (Exception e) {
           FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    /**
     * Save Confirmation Alert
     *
     * @param goToScreen
     */
    private void displayAlert(final String spanText2, final Intent goToScreen) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

        // set dialog message
        alertDialogBuilder.setMessage(spanText2).setCancelable(false).setNegativeButton(
                getResources().getString(R.string.m121), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {


                        if (goToScreen != null) {
                            try {
                                startActivity(goToScreen);
                                if(spanText2.contains("logout")) {
                                    LoginRepository loginRepository = new LoginRepository(databaseHelper);
                                    loginRepository.updateLogoutTime(appState.loginAuditId, appState.sessionUserId);
                                }
                            } catch (Exception e) {
                                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());
                            }
                        } else
                            dialog.cancel();

                    }
                }).setPositiveButton(getResources().getString(R.string.m122),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog1 = alertDialogBuilder.create();
        alertDialog1.show();
    }

    //	intialize db
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(getActivity(), DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void updateList(String villageTitle, Map<String, Integer> villageNameForCode, String textViewFilter, boolean checked) {

        try {
            displayCurrentYearSummary();
        } catch (Exception e) {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(e.getMessage());

        }
    }
}
